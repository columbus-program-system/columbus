Code development
================
Code development happens via a [public repository](https://gitlab.com/columbus-program-system/columbus) on gitlab.
Contributions to the code are very welcome.
To contribute, please use the following procedure:

Registered developers
---------------------

* Clone the repository

`git clone git@gitlab.com:columbus-program-system/columbus.git`

* Create a branch

`git checkout -b <mybranch>`

* Perform your code development and commit the code
* Verify and document the code (see below)
* Merge the current master branch into your code

`git fetch`

`git merge origin/master`

* Push an updated version of your branch to gitlab

`git push <mybranch> origin`

* Submit a merge request via gitlab.

* Once you push your branch, an automatic [testing pipeline](https://gitlab.com/columbus-program-system/columbus/-/pipelines) is started. If all tests complete successfully, then your code can be merged into master.

For anyone
----------

* Create a fork
* Perform your code development and commit the code
* Verify and document the code (see below)
* Merge the current master branch into your code

`git pull https://gitlab.com/columbus-program-system/columbus.git master`

* Push an updated version of your repository to gitlab

`git push`

* Submit a merge request via gitlab

Please note down any major changes in the `CHANGELOG` file

Verification
============

Verification of the code proceeds via the `runtests` script.
The following should always work:

`$COLUMBUS/runtests STANDARD`

To test PARALLEL and/or MOLCAS modules (requires specific installation):

`$COLUMBUS/runtests PARALLEL MOLCAS`

*Note:* The tests should always be run in a clean directory.
Please always remove the files from previous runs before calling `runtests`.

Documentation and web page
==========================
* The html files for the [documentation](https://columbus-program-system.gitlab.io/columbus/doc.html) and [web page](https://columbus-program-system.gitlab.io/columbus/) are located in `Columbus/docs`.

* Please edit these files appropriately and push to them to the GIT server along with the code developed.

* For quick changes you can use the Web IDE on gitlab.

* Upload to the web server occurs automatically via CI/CD once the code is merged to the master branch.

Usage of GIT
============

* Before submitting any perl scripts, make sure that all the perl headers are reset:

`reset_perl.sh`

After this simply copy $COLUMBUS/colperl into your path and you can use the default headers to run COLUMBUS (do not call install.automatic cpan).

Release of new version (for developers)
=======================================

* Before creating a new release, update the version information in install.automatic, runc (2 locations), colinp

* Update `CHANGELOG`

* Update version information in `runc`, `install.automatic`

* Run `maketar.sh`

* Push release files to `columbus-web` repository
