#!/bin/bash
# This script can be used for unit testing of a branch

### general settings
ORIG=git@gitlab.com:columbus-program-system/columbus.git
CONFIG=/home/lunet/cmfp2/programs/Columbus/GITlab/install.config
#GALIB=/home/cm/cmfp2/programs/Columbus/GITlab/Columbus/lib[a,g]*a
###
SDIR=`pwd`

echo "$0 <branch>"
if [ $# -ne 1 ]
then
   echo "Give branch as argument"
   exit 1
fi
BRANCH=$1
DT=`date "+%d-%H%M%S"`

git clone $ORIG $BRANCH.$DT
cd $SDIR/$BRANCH.$DT
git pull origin $BRANCH || exit 1

export COLUMBUS=$SDIR/$BRANCH.$DT/Columbus

# Installation
cd $COLUMBUS/.. || exit 1
cp $CONFIG .
#cp $GALIB $COLUMBUS
./install.automatic -p linux64.ifc cpan standard grad #parallel

# Verification
mkdir $COLUMBUS/../TESTS || exit 1
cd    $COLUMBUS/../TESTS

echo "Running tests for version:"
echo $COLUMBUS

CHK=0
$COLUMBUS/runtests STANDARD &> STANDARD.out
CHK=$((CHK+$?))

cat *.out | grep -c PASSED
echo "Errcode: $CHK"
grep FAILED *.out || exit 1

exit $CHK
