!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      PROGRAM GEOMETRIC
      IMPLICIT REAL*8 (A-H,O-Z)
      integer NCOR
      parameter (NCOR=1500000)
      DIMENSION RS8(NCOR)
      DIMENSION IRS8(NCOR)
      REAL*4 XT0,XU0,XS0,XT1,XU1,XS1,XT,XU,XS
      CHARACTER*(24) FDATE
      DATA IFAIL/0/
C
cmd   CALL GETSIZ(ICORE)
cmd   CALL GETMEM(ICORE,IRS8,IAP,IFAIL)
cmd    CALL GETMEM(ICORE,IRS8,IAP)
cmd   IF(IFAIL.NE.0)GO TO 900
cmd   NCOR=ICORE-1
cmd   IAP=(IAP+2)/2
C
cmd   NPFLG=2
cmd   CALL SYSTIME(XT0,XU0,XS0)
cmd   WRITE(6,*)' GEOMETRIC CLOCK STARTS: ',FDATE()
      CALL VMAIN(RS8,RS8,NCOR)
cmd   WRITE(6,*)' GEOMETRIC CLOCK ENDS:   ',FDATE()
cmd   CALL SYSTIME(XT1,XU1,XS1)
C
      XT=XT1-XT0
      XU=XU1-XU0
      XS=XS1-XS0
      WRITE(6,10) XT,XU,XS
   10 FORMAT(//,' FINAL TIMING RESULTS ',/,
     1 '  TOTAL TIME (SEC.S)       USER TIME          SYSTEM TIME ',/,
     2 3(3X,F10.3,7X))
      call  bummer('continue search', 0, 3 )
      STOP 'end of contraint'
  900 continue
      STOP ' MEMORY ALLOCATION FAILURE '
      END
c
c*******************************************8
c
      SUBROUTINE VMAIN(A,IA,NCOR)
      IMPLICIT INTEGER (A-Z)
      DIMENSION A(NCOR),IA(NCOR)
C
      open(74,file='geom')
      REWIND 74
      CALL NUMATM(74,0,NATOMS)
      IF(NATOMS.GT.30)STOP ' NATOMS > 10 '
C
      MFIXDIR=30
      MATOMS=MAX0(NATOMS,10)
      M3=NATOMS*3
      N3=MATOMS*3
      NDIM=N3+MFIXDIR
      ND1=NDIM*(NDIM+2)
      N31=(N3+1)*N3
      N3S=N3*N3
      NPREC=INTOWP(1)
      WRITE(6,1001)MATOMS,N3,N31,N3S,NPREC
      NACME=  1
      SCR=    NACME+N3*NPREC
      HESSS=  SCR+ND1*NPREC
      EGRADH= HESSS+ND1/2*NPREC
      GGRADH= EGRADH+N31*2*NPREC
      XGRADH= GGRADH+N31*2*NPREC
      HESS=   XGRADH+N31*2*NPREC
      GRAD=   HESS+ND1*NPREC
      EVAL=   GRAD+NDIM*NPREC
      ROOT=   EVAL+N3*NPREC
      VECT=   ROOT+N3*NPREC
      MACME=  VECT+N3S*NPREC
      LACME=  MACME+N3*NPREC
      RVECT=  LACME+N3*NPREC
      BVECT=  RVECT+N3*3*NPREC
      SCR1=   BVECT+N3S*NPREC
      SCR2=   SCR1+ND1*NPREC
      TVECT=  SCR2+ND1*NPREC
      VVECT=  TVECT+N31*NPREC
      LAMBDA= VVECT+N31*NPREC
      KAMBDA= LAMBDA+MFIXDIR*2*NPREC
      ICHAR=  KAMBDA+MFIXDIR
      ISYMTR= ICHAR + N3*11*NPREC
      ITOP=   ISYMTR+ N3S*NPREC
C
      WRITE(6,1000)NACME,SCR,HESSS,EGRADH,GGRADH,XGRADH,HESS,GRAD,
     1             EVAL,ROOT,VECT,MACME,LACME,RVECT,BVECT,SCR1,SCR2,
     2             TVECT,VVECT,LAMBDA,KAMBDA,ICHAR,ISYMTR,ITOP
      IF(ITOP.GT.NCOR)STOP 'INSUFFICIENT MEMORY IN GEOMETRIC'
      CALL POLYHSD(IA(NACME),IA(NACME)
     1      ,IA(SCR),IA(HESSS),IA(EGRADH),IA(GGRADH),IA(XGRADH)
     2      ,IA(HESS),IA(GRAD),IA(EVAL),IA(ROOT),IA(VECT)
     3      ,IA(MACME),IA(MACME),IA(LACME),IA(LACME),IA(RVECT)
     4      ,IA(BVECT),IA(SCR1),IA(SCR2),IA(TVECT),IA(VVECT)
     5      ,IA(LAMBDA),IA(KAMBDA),IA(ICHAR),IA(ISYMTR)
     6      ,N3,M3,MATOMS,MFIXDIR)
      RETURN
 1000 FORMAT(/2X,'NACME SCR HESSS EGRADH GGRADH XGRADH HESS GRAD',/8I6,
     1       /2X,'EVAL ROOT VECT MACME LACME RVECT BVECT   SCR1',/8I6,
     2       /2X,'SCR2 TVECT VVECT LAMBDA KAMBDA ICHAR ISYMTR ITOP',
     3       /8I6)
 1001 FORMAT(/2X,'MEMORY LOCATION PARAMETERS:MATOMS=',I3,' N3=',I5,
     1      ' N31=',I5,' N3S=',I5,' NPREC=',I1)
      END
      SUBROUTINE POLYHSD(NACME,RNACME
     1      ,SCR,HESSS,EGRADH,GGRADH,XGRADH
     2      ,HESS,GRAD,EVAL,ROOT,VECT
     3      ,MACME,RMACME,LACME,RLACME,RVECT
     4      ,BVECT,SCR1,SCR2,TVECT,VVECT,LAMBDA,KLASS
     5      ,CHAR,SYMTRN
     6      ,N3,M3,MATOMS,MFIXDIR)
      IMPLICIT REAL*8(A-H,O-Z)
      CHARACTER*6 VERB
      CHARACTER*19 KMES(3)
      LOGICAL SAME
      INTEGER SIMULT,ACROSS,GMFILE,RESTART,ACCEL,SADDLE,
     1        ATOM(100),STATE1,STATE2,KILLER(30),CDIF,ORIGIN,
     2        FIXDIR(6,30),KLASS(MFIXDIR),ACTUAL
C       EQUIVALENT (NACME,RNACME)  (MACME,RMACME)  (LACME,RLACME)
      REAL*8 MASS(10),GM(3,10),DISP(30),DAMPV(30),xgradr(100)
     1      ,SCR(2),HESSS(2),EGRADH(N3,2),GGRADH(N3,2),dipolr(50)
     2      ,HESS(2),GRAD(N3),EVAL(N3),ROOT(N3),VECT(2)
     3      ,NORM,MACME(3,MATOMS),RMACME(N3),LAMBDA(MFIXDIR,2)
     4      ,RVECT(2),BVECT(2),SCR1(2),SCR2(2),VALDIR(30)
     5      ,TVECT(2),VVECT(2),NACME(3,MATOMS),RNACME(N3)
     6      ,LACME(3,MATOMS),RLACME(N3),XGRADH(N3,2)
     7      ,SYMTRN(M3,M3),CHAR(M3,11)
      DIMENSION GRAD1(3),GRAD2(3),GRAD3(3), GRD(3,3),ipflag(5)
      EQUIVALENCE(GRD(1,1),GRAD1),(GRD(1,2),GRAD2),(GRD(1,3),GRAD3)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      NAMELIST/GEOMCON/VALDIR,fixdir,dpsc,ipsc,ncgnt
C         NEWTON-RAPHSON CONVERGENCE CRITERIA
      DATA CONV/1.0D-06/,MAXIT/20/,NSFIG/5/,ipflag/5*0/,ncgnt/2/
C
      DATA FIXDIR/180*0/,VALDIR/30*0.D0/
      DATA ICIU1,ICIU2/31,41/
C
C      FIXDIR       FIXDIR(1,*)=I FIXDIR(2,*)=J - R(I,J) FIXED AT VALDIR
C                   FIXDIR(1,*)=I FIXDIR(2,*)=J  FIXDIR(3,*)=K -
C                   ANGLE(I,J,K ) FIXED AT VALDIR(I)
C                   FIXDIR(I,J,K,0,0,L) ANGLE(I,J,K,L)
C                   TO INTRODUCE THIS OPTION WITH THE HESSIAN USER MUST
C                   REGENERATE WORKING HESSIAN USING RESTART
C                      FILE USAGE
C      I70       PROPERTY UNIT                  70
C      I73       DEPENDENT INPUT UNIT           73
C      I74       geom                           74
C      I75       constraint tape                75
C      I99       TEMPORARY UNIT                 99
C      N32       PROGRESS FILE                  32
C
C         UNITS
      ITAP10=10
      ITAP11=11
      ITAP14=14
      INP=5
      IOUT=6
      I70=70
      I73=73
      I74=74
      I75=75
      I76=76
      I77=77
      I99=99
C
      REWIND I74
      CALL NUMATM(I74,0,NATOMS)

C
      do 10 i=1,natoms
   10 atom(i)=i
      PI=ACOS(-1.D0)
      NORR=1
C
C
      open(inp,file='polyhesin')
      REWIND INP
      read(inp,geomcon)
      close(inp)
C
C
      NTDEGF=NATOMS*3
      NDEGF=NATOMS*3-3
      NDEGFI=NDEGF-3
C
      NROT=  NDEGF-NDEGFI
C
      maxnew=0
      NFIXDIR=0
      DO 3 I=1,30
      IF(FIXDIR(1,I).EQ.0)GO TO 3
      NFIXDIR=I
      do 44 j=1,6
   44 if(fixdir(j,i).gt.natoms)maxnew=max0(fixdir(j,i),maxnew)
    3 CONTINUE
      maxnew=maxnew-natoms
      leq=0
      if((nfixdir.eq.1).and.(ndegfi.eq.3).and.(lineq.eq.1))leq=1
      if(maxnew.gt.0)call mkpscent(natoms,maxnew,psc,dpsc,ipsc,mass,
     x    iout)
C
      NINTI=    NDEGFI+NFIXDIR+NCGNT
      NINT=     NINTI+NROT
      NDEGFL=   NDEGF+NCGNT
      NDEGFIL=  NDEGFI+NCGNT
C
      NINT2=    NINT*(NINT+1)/2
      NINTI2=   NINTI*(NINTI+1)/2
      NDEGFL2=  NDEGFL*(NDEGFL+1)/2
      NDEGFIL2= NDEGFIL*(NDEGFIL+1)/2
      NDEGFI2=  NDEGFI*(NDEGFI+1)/2
      NDEGF2=   NDEGF*(NDEGF+1)/2
C
      WRITE(IOUT,1043)NDEGF,NROT,NFIXDIR,NINTI,NINT
      IF(NFIXDIR.EQ.0)GO TO 5
      DO 4 I=1,NFIXDIR
      KLASS(I)=1
      IF(FIXDIR(3,I).NE.0)GO TO 14
      WRITE(IOUT,1045)I,FIXDIR(1,I),FIXDIR(2,I),VALDIR(I)
      GO TO 4
   14 CONTINUE
      IF(FIXDIR(4,I).NE.0)GO TO 15
        IF(FIXDIR(6,I).NE.0)THEN
        KLASS(I)=5
        WRITE(IOUT,1062)I,FIXDIR(1,I),FIXDIR(2,I),FIXDIR(3,I),
     X               FIXDIR(6,I),VALDIR(I)
        GO TO 4
        ENDIF
      KLASS(I)=2
      WRITE(IOUT,1046)I,FIXDIR(1,I),FIXDIR(2,I),FIXDIR(3,I),VALDIR(I)
      GO TO 4
   15 CONTINUE
      IF(FIXDIR(6,I).NE.0)GO TO 17
      KLASS(I)=3
      WRITE(IOUT,1047)I,FIXDIR(1,I),FIXDIR(2,I),FIXDIR(3,I),FIXDIR(4,I)
      GO TO 4
   17 CONTINUE
      KLASS(I)=4
      WRITE(IOUT,1048)I,FIXDIR(1,I),FIXDIR(2,I),FIXDIR(3,I),
     X    FIXDIR(4,I),FIXDIR(5,I),FIXDIR(6,I)
    4 CONTINUE
    5 CONTINUE
C
C
      rewind i74
      call readcolg(I74,gm,NATOMS,iout)
      WRITE(IOUT,1002)(J,(GM(I,J),I=1,3),J=1,NATOMS)
C
C        CORRECT GRADIENT FOR CONSTRAINTS
      CALL FREEZG(NFIXDIR,FIXDIR,VALDIR,NATOMS*3,NATOMS,
     X         GM,SCR,Grad,KLASS,IOUT)
C
C
c       constraint gradients
       open(i75,file='cicgrad',form='unformatted')
       rewind i75
       write(i75)nfixdir,(klass(k),k=1,nfixdir)
       write(iout,1039)nfixdir,(klass(k),k=1,nfixdir)
       write(i75)(grad(k),k=1,nfixdir)
       write(iout,1040)(grad(k),k=1,nfixdir)
       close(I75)
       open(i75,file='cartcgrd',form='formatted')
       ihi=0
       do 280 i=1,nfixdir
       low=ihi+1
       ihi=ihi+natoms*3
       write(iout,1041)(scr(k),k=low,ihi)
  280  write(i75,1070)(scr(k),k=low,ihi)
  300 CONTINUE
       close(i75)
c
      return
C          R-R constraints needed

  900 CONTINUE
      WRITE(IOUT,1019)NCIU
      CALL EXIT(100)
  910 CONTINUE
      WRITE(IOUT,1042)LOOPON
      CALL EXIT(100)
  920 WRITE(IOUT,*)'BAD NAMELIST FILE=',I73
      CALL EXIT(100)
  921 WRITE(IOUT,*)'BAD NAMELIST FILE=',INP
      CALL EXIT(100)
 1000 FORMAT(5X,'NO NAMELIST INPUT- DEFAULT MASSES USED ' )
 1001 FORMAT(/15X,'SEARCH ALGORITHM FOR MINIMUM ENERGY CROSSING ' )
 1002 FORMAT(/5X,' CURRENT GEOMETRY ',/(1X,I2,1X,3F15.8) )
 1003 FORMAT(/5X,'RAW ',A6,' GRAD',/7X,'X',14X,'Y',14X,'Z'/(1X,3F15.9))
 1010 FORMAT(' POINTERS TO GEOMETRY AND NACMES:',2I6 )
 1011 FORMAT('  HESSIAN CONSTRUCTION MODE: NEW DIRECTION=',I3)
 1012 FORMAT(' MASS COMBINATION:',5(' M(',I2,')=',F12.6),
     1       (/20X,5(' M(',I2,')=',F12.6) ))
 1015 FORMAT(5X,'DISPLACEMENT VECTOR GENERATED FROM H ! D> = -G> ' )
 1016 FORMAT('  CI-ENERGIES FROM UNIT ',I4,
     1       /5X,' E1=',E20.12,' E2=',E20.12,' E2-E1=',E20.12)
 1017 FORMAT(/5X,'DEL-E SCALED GRADIENTS',/7X,'X',14X,'Y',/(1X,2E16.9) )
 1018 FORMAT(/5X,'EIGENVALUES AND EIGENVECTORS OF HESSIAN' )
 1019 FORMAT(/5X,'CANT READ CI ENERGY FROM HEADER:UNIT=',I3)
 1020 FORMAT(/5X,'SCALE FACTOR FOR GRADIENT',E15.8,5X,' NORM=',E15.8,
     1       /5X,' UNSCALED',' GRADIENTS ',(/1X,5E15.8))
 1021 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
 1022 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1023 FORMAT(5X,'**** NO CURRENT GEOMETRY FILE ****' )
 1024 FORMAT(/5X,'**** NO GRADIENT POINTER ON TAPE=',I2,' ******' )
 1025 FORMAT(/5X,'ITERATION STATUS  ',A20)
 1026 FORMAT('  CI-ENERGIES FROM UNITS ',2I4,' E2-E1=',E20.12)
 1027 FORMAT(5X,'RESTART SEARCH FROM PROGRESS FILE=',I3)
 1029 FORMAT(5X,' NO INPUT FOUND ON INPUT UNIT=',I5)
 1030 FORMAT(5X,' NO INPUT ON UNIT=',I5,' ABORT' )
 1031 FORMAT(5X,'ATOM MAP=',10I3)
 1032 FORMAT(5X,'SOLUTION IN INTERNAL BASIS:',/,(2X,6F12.6))
 1033 FORMAT(5X,'LAGRANGE MULIPLIER:',A3,'=',30F12.6)
 1034 FORMAT(5X,'RESTART HESSIAN CONSTRUCT FROM PROGRESS FILE='
     1     ,I3,' IDISP=',I3)
 1035 FORMAT(5X,'STATE ENERGIES: ECI=',2E20.12,'  ENUC=',E20.12)
 1036 FORMAT(/,' ROOT=',I3,' EVAL=',E15.8,
     1       /,' EVECT=',6F12.6,/,(7X,6F12.6))
 1037 FORMAT(5X,'NUCLEAR DISPLACEMENTS:',/,(2X,6F12.6))
 1038 FORMAT(5X,'**** UNIT HESSIAN USED ****' )
 1039 FORMAT(5X,' KLASS VECTOR:',/' NDIM=',i3,' KLASS=',(10i3))
 1040 FORMAT(5X,' CONSTRAINT RESIDUALS=',(5E12.5))
 1041 FORMAT(5X, 6f10.6)
 1042 FORMAT(5X,'SCHMO FAILURE IN IBASIS-MVIBV',I3)
 1043 FORMAT(5X,'DIMENSIONS:NDEGF=',I3,' NROT=',I3,' NFIX=',I3,' IVAR=',
     1       I3,' IVAR+ROT=',I3)
 1045 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' R(',I2,',',I2,
     X       ')=     ',F12.6)
 1046 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,')=',F12.6)
 1047 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' R(',I2,',',I2,
     X       ')=         R(',I2,',',I2,')')
 1048 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,')=ANG(',I2,',',I2,',',I2,')')
 1049 FORMAT(3X,A8,1X,A8)
 1050 FORMAT(5X,'NORM OF RHS: ORIG BASIS ',E15.8,' INT BASIS ',E15.8)
 1051 FORMAT(5X,'GRADIENT: ORIGINAL BASIS',(/5X,5E15.8))
 1052 FORMAT(15X,'GRADIENT DECOMPOSITION')
 1053 FORMAT(3X,'I=',I2,' E=',F11.5,' G=',F11.5,' L1=',F11.5,
     X                  ' H=',F11.5,' L2=',F11.5)

 1057 FORMAT(/15X,'SCALE RHS METHOD: ',A20,' GRADIENT')
 1058 FORMAT(/15X,'CANONICAL COORDINATES',/13X,'r',12X,'R',12X,'GMA',
     X       /3X,'g   ',3F12.6,/3X,'h   ',3F12.6,/3X,'seam',3F12.6)
 1059 FORMAT(/15X,'JACOBI ANALYSIS OF G-H PLANE ')
 1062  FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,',',i2,' )= ',f12.6)
 1070 format (3d15.7)
      END
      SUBROUTINE ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      DATA ZERO/0.D0/,TWO/2.D0/,PI/3.141592654D0/
C         LAW OF COSINES
      R13=ZERO
      R12=ZERO
      R23=ZERO
      DO 10 K=1,3
      R12=R12+(GM(K,II)-GM(K,JJ))**2
      R23=R23+(GM(K,JJ)-GM(K,KK))**2
      R13=R13+(GM(K,II)-GM(K,KK))**2
   10 CONTINUE
      CA=-(R13-R12-R23)/(TWO*DSQRT(R12*R23))
      ANG=DACOS(CA)*180.D0/PI
      R12=DSQRT(R12)
      R23=DSQRT(R23)
      R13=DSQRT(R13)
      RETURN
      END
      SUBROUTINE BLDCHES(NFIXDIR,FIXDIR,VALDIR,NDEGF,HESS,NATOMS,
     1                  GM,KILLER,SCR,ISCR,RLAMBDA,KLASS,IOUT,iprint)
      IMPLICIT REAL*8(A-H,O-Z)
      LOGICAL LIK,LJK,LKK,LIL,LJL,LKL
      INTEGER FIXDIR(6,2),THESS,ISCR(2),KLASS(2)
      DIMENSION KILLER(2),VALDIR(2),HESS(2),SCR(2),RLAMBDA(2)
      DIMENSION GM(3,2)
      COMMON/CHESS/NORR
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/
C
C        STATEMENT FUNCTION GRADIENTS
      C1(X12,X23,R12,R23)=-(X23/R23+CA*X12/R12)/R12
      C2(X12,X23,R12,R23)=(X23-X12)/(R12*R23)
     1   +CA*(X12/R12**2-(X23/R23**2))
      C3(X12,X23,R12,R23)=(X12/R12+CA*X23/R23)/R23
C
      rl=1.d0
      if(maxnew.eq.0)go to 99
      do 40 i=1,maxnew
      do 41 k=1,3
   41 gm(k,natoms+i)=0
      do 42 k=1,3
      do 42 j=1,natoms
   42 gm(k,natoms+i)=gm(k,natoms+i)+gm(k,j)*psc(j,i)
   40 continue
   99 continue
C
      IF(NFIXDIR.EQ.0)GO TO 900
      N3=NATOMS*3
      THESS=NDEGF*(NDEGF+1)/2
C
      if(iprint.lt.2)go to 52
      WRITE(IOUT,1027)'WITHOUT','PRIMITIVE',NDEGF
      IHI=0
      DO 51 I=1,NDEGF
      LOW=IHI+1
      IHI=IHI+I
      WRITE(IOUT,1028)I,(HESS(K),K=LOW,IHI)
   51 CONTINUE
   52 continue
C
C            R-LAMBDA BLOCK
      DO 699 KFIXDIR=1,NFIXDIR
      DO 101 I=1,N3
  101 SCR(I)=ZERO
      II=FIXDIR(1,KFIXDIR)
      JJ=FIXDIR(2,KFIXDIR)
      GO TO (400,600,400,600,399),KLASS(KFIXDIR)
      STOP ' ILLEGAL KLASS '
  400 CONTINUE
C         DISTANCE CONSTRAINT
      call concon0(natoms,gm,scr,ii,jj,rl,zz)
      IF(KLASS(KFIXDIR).EQ.1)GO TO 200
C         DISTANCE DIFFERNECE CONSTRAINT
      II=FIXDIR(3,KFIXDIR)
      JJ=FIXDIR(4,KFIXDIR)
      call concon0(natoms,gm,scr,ii,jj,-rl,zz)
      GO TO 200
C
 399  CONTINUE
C         dihedral ANGLE CONSTRAINT
      KK=FIXDIR(3,KFIXDIR)
      LL=FIXDIR(6,KFIXDIR)
      call concon2(NATOMS,GM,SCR,II,JJ,KK,LL,RL)
      GO TO 200
  600 CONTINUE
C         ANGLE CONSTRAINT
      KK=FIXDIR(3,KFIXDIR)
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      call concon1(NATOMS,GM,SCR,II,JJ,KK,RL)
      IF(KLASS(KFIXDIR).EQ.2)GO TO 200
C         ANGLE DIFFERENCE CONSTRAINT
      II=FIXDIR(4,KFIXDIR)
      JJ=FIXDIR(5,KFIXDIR)
      KK=FIXDIR(6,KFIXDIR)
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      call concon1(NATOMS,GM,SCR,II,JJ,KK,-RL)
C
  200 CONTINUE
      KOUNT=0
      DO 140 I=1,N3
      IF(KILLER(I).EQ.1)GO TO 140
      KOUNT=KOUNT+1
      HESS(THESS+KOUNT)=SCR(I)
  140 CONTINUE
      DO 141 I=KOUNT+1,KOUNT+KFIXDIR
      HESS(THESS+I)=ZERO
  141 CONTINUE
      THESS=THESS+KOUNT+KFIXDIR
  699 CONTINUE
C
      if(maxnew.ne.0)go to 901
C            R-R BLOCK
      KOUNT=0
      DO 250 I=1,N3
      ISCR(I)=0
      IF(KILLER(I).EQ.1)GO TO 250
      KOUNT=KOUNT+1
      ISCR(I)=KOUNT
  250 CONTINUE
C
      DO 899 KFIXDIR=1,NFIXDIR
      II=FIXDIR(1,KFIXDIR)
      JJ=FIXDIR(2,KFIXDIR)
      GO TO (700,800,700,800,899),KLASS(KFIXDIR)
      STOP ' ILLEGAL KLASS '
  700 CONTINUE
C         DISTANCE CONSTRAINT
      DO 260 K=1,3
      IIK=ISCR((II-1)*3+K)
      JJK=ISCR((JJ-1)*3+K)
      LIK=IIK.EQ.0
      LJK=JJK.EQ.0
      IJ=IIK*(IIK-1)/2 + JJK
      JI=JJK*(JJK-1)/2 + IIK
      IF(LIK.OR.LJK)GO TO 243
      IF(II.GT.JJ)HESS(IJ)=HESS(IJ)-RLAMBDA(KFIXDIR)*TWO
      IF(JJ.GE.II)HESS(JI)=HESS(JI)-RLAMBDA(KFIXDIR)*TWO
  243 CONTINUE
      IF(LIK)GO TO 244
      IJ=IIK*(IIK+1)/2
      HESS(IJ)=HESS(IJ)+RLAMBDA(KFIXDIR)*TWO
  244 CONTINUE
      IF(LJK)GO TO 260
      IJ=JJK*(JJK+1)/2
      HESS(IJ)=HESS(IJ)+RLAMBDA(KFIXDIR)*TWO
  260 CONTINUE
      IF(KLASS(KFIXDIR).EQ.1)GO TO 899
C         DISTANCE DIFFERNECE CONSTRAINT
      II=FIXDIR(3,KFIXDIR)
      JJ=FIXDIR(4,KFIXDIR)
      DO 270 K=1,3
      IIK=ISCR((II-1)*3+K)
      JJK=ISCR((JJ-1)*3+K)
      LIK=IIK.EQ.0
      LJK=JJK.EQ.0
      IJ=IIK*(IIK-1)/2 + JJK
      JI=JJK*(JJK-1)/2 + IIK
      IF(LIK.OR.LJK)GO TO 283
      IF(II.GT.JJ)HESS(IJ)=HESS(IJ)+RLAMBDA(KFIXDIR)*TWO
      IF(JJ.GE.II)HESS(JI)=HESS(JI)+RLAMBDA(KFIXDIR)*TWO
  283 CONTINUE
      IF(LIK)GO TO 284
      IJ=IIK*(IIK+1)/2
      HESS(IJ)=HESS(IJ)-RLAMBDA(KFIXDIR)*TWO
  284 CONTINUE
      IF(LJK)GO TO 270
      IJ=JJK*(JJK+1)/2
      HESS(IJ)=HESS(IJ)-RLAMBDA(KFIXDIR)*TWO
  270 CONTINUE
C
      GO TO 899
  800 CONTINUE
C         ANGLE CONSTRAINT
C        NEXT LINE PERMITS SKIPPING OF RR ANGLE CONTRIBUTION
      IF(NORR.EQ.1)GO TO 899
      KK=FIXDIR(3,KFIXDIR)
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      DO 370 K=1,3
      IIK=ISCR((II-1)*3+K)
      JJK=ISCR((JJ-1)*3+K)
      KKK=ISCR((KK-1)*3+K)
      LIK=IIK.NE.0
      LJK=JJK.NE.0
      LKK=KKK.NE.0
      X12=GM(K,II)-GM(K,JJ)
      X23=GM(K,JJ)-GM(K,KK)
C         X(I)X(I) Y(I)Y(I) Z(I) Z(I) DERIVATIVES
C         X(J)X(J) Y(J)Y(J) Z(J) Z(J) DERIVATIVES
C         X(K)X(K) Y(K)Y(K) Z(K) Z(K) DERIVATIVES
      IF(LIK)THEN
      IJ=IIK*(IIK+1)/2
      ZZ=-C1(X12,X23,R12,R23)*TWO*X12/R12**2 - CA/R12**2
     1    + CA*(X12/R12**2)**2
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LKK)THEN
      IJ=KKK*(KKK+1)/2
      ZZ=-C3(X12,X23,R12,R23)*TWO*X23/R23**2 - CA/R23**2
     1    + CA*(X23/R23**2)**2
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJK)THEN
      IJ=JJK*(JJK+1)/2
      ZZ=TWO/(R12*R23)+(X12-X23)*(-X12/R12**2+X23/R23**2)/(R23*R12)
     1  -C2(X12,X23,R12,R23)*(-X12/R12**2+X23/R23**2)+CA*
     2  (TWO*X12**2/R12**4+TWO*X23**2/R23**4-ONE/R12**2-ONE/R23**2)
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
C
C          X(I)X(J)  Y(I)Y(J)  Z(I)Z(J) DERIVATIVES
C          X(J)X(K)  Y(J)Y(K)  Z(J)Z(K) DERIVATIVES
C          X(I)X(K)  Y(I)Y(K)  Z(I)Z(K) DERIVATIVES
C
      IF(LIK.AND.LJK)THEN
      IF(IIK.GT.JJK)THEN
      IJ=IIK*(IIK-1)/2+JJK
      ELSE
      IJ=JJK*(JJK-1)/2+IIK
      ENDIF
      ZZ=-ONE/(R12*R23)-(-X12+X23)*X12/(R23*R12**3)-
     1  CA*(-ONE/R12**2-X12**2/R12**4)-C1(X12,X23,R12,R23)*(-X12/R12**2
     2  +X23/R23**2)
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJK.AND.LKK)THEN
      IF(JJK.GT.KKK)THEN
      IJ=JJK*(JJK-1)/2+KKK
      ELSE
      IJ=KKK*(KKK-1)/2+JJK
      ENDIF
      ZZ=-ONE/(R12*R23)+(-X12+X23)*X23/(R12*R23**3)-
     1  CA*(-ONE/R23**2+X23**2/R23**4)-C3(X12,X23,R12,R23)*(-X12/R12**2
     2  +X23/R23**2)
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LIK.AND.LKK)THEN
      IF(IIK.GT.KKK)THEN
      IJ=IIK*(IIK-1)/2+KKK
      ELSE
      IJ=KKK*(KKK-1)/2+IIK
      ENDIF
      ZZ=ONE/(R12*R23)-X12**2/(R23*R12**3)
     1    +C1(X12,X23,R12,R23)*(X23/R23**2)
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      DO 371 L=1,3
      IF(K.EQ.L)GO TO 371
      IIL=ISCR((II-1)*3+L)
      JJL=ISCR((JJ-1)*3+L)
      KKL=ISCR((KK-1)*3+L)
      LIL=IIL.NE.0
      LJL=JJL.NE.0
      LKL=KKL.NE.0
      X12L=GM(L,II)-GM(L,JJ)
      X23L=GM(L,JJ)-GM(L,KK)
C
C          X(I)Y(J)   X(J)Y(K)   X(I)Y(K)  DERIVATIVES
C
      IF(LJL.AND.LIK)THEN
C  K(1)  L(2)     X1 W2
      IF(JJL.GT.IIK)THEN
      IJ=JJL*(JJL-1)/2+IIK
      ELSE
      IJ=IIK*(IIK-1)/2+JJL
      ENDIF
      ZZ=-(-X12L+X23L)*X12/(R23*R12**3)+
     1    C1(X12,X23,R12,R23)*(-X12L/R12**2 + X23L/R23**2)-
     2    CA*TWO*X12*X12L/R12**4
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJL.AND.LKK)THEN
C  K(3)  L(2)  X3 W2
      IF(JJL.GT.KKK)THEN
      IJ=JJL*(JJL-1)/2+KKK
      ELSE
      IJ=KKK*(KKK-1)/2+JJL
      ENDIF
      ZZ=-(-X12L+X23L)*X23/(R12*R23**3)+
     1    C3(X12,X23,R12,R23)*(-X12L/R12**2 + X23L/R23**2)+
     2    CA*TWO*X23*X23L/R23**4
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LKL.AND.LIK)THEN
C   K(1)  L(3)   X1 W3
      IF(KKL.GT.IIK)THEN
      IJ=KKL*(KKL-1)/2+IIK
      ELSE
      IJ=IIK*(IIK-1)/2+KKL
      ENDIF
      ZZ=-X12*X12L/(R23*(R12**3))+C1(X12,X23,R12,R23)*(X23L/R23**2)
     1    -(X23/(R12*R23)-CA*X12/R12**2)*X23L/R23**2
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(L.GT.K)GO TO 371
C
C          X(I)Y(I) X(J)Y(J) X(K)Y(K) DERIVATIVES
C
      IF(LIK.AND.LIL)THEN
C  K(1)  L(1)   W1  X1
      IJ=IIK*(IIK-1)/2+IIL
      ZZ=X23*X12L/(R23*R12**3)-C1(X12L,X23L,R12,R23)*X12/R12**2
     1   +TWO*CA*X12*X12L/R12**4
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJK.AND.LJL)THEN
C  K(2)  L(2)   W2  X2
      IJ=JJK*(JJK-1)/2+JJL
      ZZ=-(-X12+X23)*(X23L/R23**2-X12L/R12**2)/(R12*R23)+
     1    C2(X12L,X23L,R12,R23)*(X12/R12**2-X23/R23**2)+
     2    CA*TWO*(X12*X12L/R12**4+X23*X23L/R23**4)
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LKK.AND.LKL)THEN
C  K(3)  L(3)   W3  X3
      IJ=KKK*(KKK-1)/2+KKL
      ZZ=X12*X23L/(R12*R23**3)+C3(X12L,X23L,R12,R23)*X23/R23**2
     1   +TWO*CA*X23*X23L/R23**4
      HESS(IJ)=HESS(IJ)+ZZ*RLAMBDA(KFIXDIR)
      ENDIF
  371 CONTINUE
  370 CONTINUE
      IF(KLASS(KFIXDIR).EQ.2)GO TO 899
C         ANGLE DIFFERENCE CONSTRAINT
      II=FIXDIR(4,KFIXDIR)
      JJ=FIXDIR(5,KFIXDIR)
      KK=FIXDIR(6,KFIXDIR)
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      DO 380 K=1,3
      IIK=ISCR((II-1)*3+K)
      JJK=ISCR((JJ-1)*3+K)
      KKK=ISCR((KK-1)*3+K)
      LIK=IIK.NE.0
      LJK=JJK.NE.0
      LKK=KKK.NE.0
      X12=GM(K,II)-GM(K,JJ)
      X23=GM(K,JJ)-GM(K,KK)
C         X(I)X(I) Y(I)Y(I) Z(I) Z(I) DERIVATIVES
C         X(J)X(J) Y(J)Y(J) Z(J) Z(J) DERIVATIVES
C         X(K)X(K) Y(K)Y(K) Z(K) Z(K) DERIVATIVES
      IF(LIK)THEN
      IJ=IIK*(IIK+1)/2
      ZZ=-C1(X12,X23,R12,R23)*TWO*X12/R12**2 - CA/R12**2
     1    + CA*(X12/R12**2)**2
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LKK)THEN
      IJ=KKK*(KKK+1)/2
      ZZ=-C3(X12,X23,R12,R23)*TWO*X23/R23**2 - CA/R23**2
     1    + CA*(X23/R23**2)**2
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJK)THEN
      IJ=JJK*(JJK+1)/2
      ZZ=TWO/(R12*R23)+(X12-X23)*(-X12/R12**2+X23/R23**2)/(R23*R12)
     1  -C2(X12,X23,R12,R23)*(-X12/R12**2+X23/R23**2)+CA*
     2  (TWO*X12**2/R12**4+TWO*X23**2/R23**4-ONE/R12**2-ONE/R23**2)
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
C
C          X(I)X(J)  Y(I)Y(J)  Z(I)Z(J) DERIVATIVES
C          X(J)X(K)  Y(J)Y(K)  Z(J)Z(K) DERIVATIVES
C          X(I)X(K)  Y(I)Y(K)  Z(I)Z(K) DERIVATIVES
C
      IF(LIK.AND.LJK)THEN
      IF(IIK.GT.JJK)THEN
      IJ=IIK*(IIK-1)/2+JJK
      ELSE
      IJ=JJK*(JJK-1)/2+IIK
      ENDIF
      ZZ=-ONE/(R12*R23)-(-X12+X23)*X12/(R23*R12**3)-
     1  CA*(-ONE/R12**2-X12**2/R12**4)-C1(X12,X23,R12,R23)*(-X12/R12**2
     2  +X23/R23**2)
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJK.AND.LKK)THEN
      IF(JJK.GT.KKK)THEN
      IJ=JJK*(JJK-1)/2+KKK
      ELSE
      IJ=KKK*(KKK-1)/2+JJK
      ENDIF
      ZZ=-ONE/(R12*R23)+(-X12+X23)*X23/(R12*R23**3)-
     1  CA*(-ONE/R23**2+X23**2/R23**4)-C3(X12,X23,R12,R23)*(-X12/R12**2
     2  +X23/R23**2)
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LIK.AND.LKK)THEN
      IF(IIK.GT.KKK)THEN
      IJ=IIK*(IIK-1)/2+KKK
      ELSE
      IJ=KKK*(KKK-1)/2+IIK
      ENDIF
      ZZ=ONE/(R12*R23)-X12**2/(R23*R12**3)
     1    +C1(X12,X23,R12,R23)*(X23/R23**2)
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      DO 381 L=1,3
      IF(K.EQ.L)GO TO 381
      IIL=ISCR((II-1)*3+L)
      JJL=ISCR((JJ-1)*3+L)
      KKL=ISCR((KK-1)*3+L)
      LIL=IIL.NE.0
      LJL=JJL.NE.0
      LKL=KKL.NE.0
      X12L=GM(L,II)-GM(L,JJ)
      X23L=GM(L,JJ)-GM(L,KK)
C
C          X(I)Y(J)   X(J)Y(K)   X(I)Y(K)  DERIVATIVES
C
      IF(LJL.AND.LIK)THEN
C  K(1)  L(2)     X1 W2
      IF(JJL.GT.IIK)THEN
      IJ=JJL*(JJL-1)/2+IIK
      ELSE
      IJ=IIK*(IIK-1)/2+JJL
      ENDIF
      ZZ=-(-X12L+X23L)*X12/(R23*R12**3)-
     1    C1(X12,X23,R12,R23)*(-X12L/R12**2 + X23L/R23**2)-
     2    CA*TWO*X12*X12L/R12**4
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJL.AND.LKK)THEN
C  K(3)  L(2)  X3 W2
      IF(JJL.GT.KKK)THEN
      IJ=JJL*(JJL-1)/2+KKK
      ELSE
      IJ=KKK*(KKK-1)/2+JJL
      ENDIF
      ZZ=(-X12L+X23L)*X23/(R12*R23**3)-
     1    C3(X12,X23,R12,R23)*(-X12L/R12**2 + X23L/R23**2)+
     2    CA*TWO*X23*X23L/R23**4
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LKL.AND.LIK)THEN
C   K(1)  L(3)   X1 W3
      IF(KKL.GT.IIK)THEN
      IJ=KKL*(KKL-1)/2+IIK
      ELSE
      IJ=IIK*(IIK-1)/2+KKL
      ENDIF
      ZZ=-X12*X12L/(R23*(R12**3))+C1(X12,X23,R12,R23)*(X23L/R23**2)
     1    -(X23/(R12*R23)-CA*X12/R12**2)*X23L/R23**2
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(L.GT.K)GO TO 381
C
C          X(I)Y(I) X(J)Y(J) X(K)Y(K) DERIVATIVES
C
      IF(LIK.AND.LIL)THEN
C  K(1)  L(1)   W1  X1
      IJ=IIK*(IIK-1)/2+IIL
      ZZ=X23*X12L/(R23*R12**3)-C1(X12L,X23L,R12,R23)*X12/R12**2
     1   +TWO*CA*X12*X12L/R12**4
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LJK.AND.LJL)THEN
C  K(2)  L(2)   W2  X2
      IJ=JJK*(JJK-1)/2+JJL
      ZZ=-(-X12+X23)*(X23L/R23**2-X12L/R12**2)/(R12*R23)+
     1    C2(X12L,X23L,R12,R23)*(X12/R12**2-X23/R23**2)+
     2    CA*TWO*(X12*X12L/R12**4+X23*X23L/R23**4)
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
      IF(LKK.AND.LKL)THEN
C  K(3)  L(3)   W3  X3
      IJ=KKK*(KKK-1)/2+KKL
      ZZ=X12*X23L/(R12*R23**3)+C3(X12L,X23L,R12,R23)*X23/R23**2
     1   +TWO*CA*X23*X23L/R23**4
      HESS(IJ)=HESS(IJ)-ZZ*RLAMBDA(KFIXDIR)
      ENDIF
  381 CONTINUE
  380 CONTINUE
  899 CONTINUE
C
  901 continue
      if(iprint.lt.1)go to 851
      L=NDEGF+NFIXDIR
      WRITE(IOUT,1027)'WITH','ORIGINAL ',L
      IHI=0
      DO 850 I=1,L
      LOW=IHI+1
      IHI=IHI+I
      WRITE(IOUT,1028)I,(HESS(K),K=LOW,IHI)
  850 CONTINUE
  851 continue
C
  900 CONTINUE
      IF(KOUNT.NE.NDEGF)GO TO 999
      RETURN
  999 CONTINUE
      WRITE(IOUT,1024)KOUNT,NDEGF
      STOP ' ERROR BLDCHES '
 1024 FORMAT(5X,'PRIMITIVE NUCLEAR DEGREE OF FREEDOM ERROR:LOCAL=',
     1      I3,' PASSED=',I3)
 1025 FORMAT(2X,I3,(8F12.6),(/5X,8F12.6))
 1026 FORMAT(5X,'R-LAMBDA HESSIAN BLOCK: PRIMITIVE BASIS DIM=',I3 )
 1027 FORMAT(5X,'HESSIAN ',A7,' CONSTRAINTS:',A9,
     1    ' BASIS DIMENSION=',I3)
 1028 FORMAT(5X,I3,7E15.8,(/8X,7E15.8))
      END
      SUBROUTINE CNSTRV(NFIXDIR,FIXDIR,VALDIR,NDEGF,GRAD,NATOMS,
     1                  GM,KILLER,KLASS,IOUT,rhs)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR(6,2),KLASS(2)
      DIMENSION KILLER(2),VALDIR(2),GRAD(NDEGF)
      DIMENSION GM(3,2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/
      rl=1.d0
C
      if(maxnew.eq.0)go to 99
      do 40 i=1,maxnew
      do 41 k=1,3
   41 gm(k,natoms+i)=0
      do 42 k=1,3
      do 42 j=1,natoms
   42 gm(k,natoms+i)=gm(k,natoms+i)+gm(k,j)*psc(j,i)
   40 continue
   99 continue
C
      N3=NATOMS*3
      DO 101 I=1,NDEGF
  101 grad(I)=ZERO
C
      MYCLASS=0
      ZNORM=ZERO
      IF(NFIXDIR.NE.1)STOP ' CONSTRV ERROR '
      DO 200 I=1,NFIXDIR
      II=FIXDIR(1,I)
      JJ=FIXDIR(2,I)
      IIU=(II-1)*3
      JJU=(JJ-1)*3
      GO TO (400,600,400,600,700),KLASS(I)
      STOP ' ILLEGAL KLASS '
  400 CONTINUE
C         DISTANCE CONTRAINT
      MYCLASS=1
      call concon0(natoms,gm,grad,ii,jj,rl,x)
      IF(KLASS(I).EQ.3)GO TO 150
      WRITE(IOUT,1028)II,JJ,DSQRT(X),VALDIR(I)
      RHS=X-VALDIR(I)**2
      ZNORM=ZNORM+RHS**2
      GO TO 200
  150 CONTINUE
C         DISTANCE DIFFERENCE CONSTRAINT
      KK=FIXDIR(3,I)
      LL=FIXDIR(4,I)
      IIU=(KK-1)*3
      JJU=(LL-1)*3
      call concon0(natoms,gm,grad,kk,ll,-rl,y)
      WRITE(IOUT,1027)II,JJ,DSQRT(X),KK,LL,DSQRT(Y)
      RHS=X-Y
      ZNORM=ZNORM+RHS**2
      GO TO 200
  700 CONTINUE
C         dihedral ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      LL=FIXDIR(6,I)
      KKU=(KK-1)*3
      LLU=(LL-1)*3
      CALL DIHED1(GM(1,II),GM(1,JJ),GM(1,KK),GM(1,LL),DIH1)
      call concon2(NATOMS,GM,SCR,II,JJ,KK,LL,RL)
      WRITE(IOUT,1030)II,JJ,KK,LL,acos(DIH1)*180.d0/pi,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      RHS=DIH1-DCOS(XX)
      GO TO 200
  600 CONTINUE
C         ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      KKU=(KK-1)*3
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      call concon1(NATOMS,GM,SCR,II,JJ,KK,RL)
      IF(KLASS(I).EQ.4)GO TO 170
      WRITE(IOUT,1029)II,JJ,KK,ANG,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      RHS=CA-DCOS(XX)
      GO TO 200
  170 CONTINUE
C         ANGLE DIFFERENCE
      IIO=II
      JJO=JJ
      KKO=KK
      II=FIXDIR(4,I)
      JJ=FIXDIR(5,I)
      KK=FIXDIR(6,I)
      IIU=(II-1)*3
      JJU=(JJ-1)*3
      KKU=(KK-1)*3
      CALL ANGL(GM,II,JJ,KK,ANGB,CB,R13,R12,R23)
      CAT=CA
      CA=CB
      call concon1(NATOMS,GM,SCR,II,JJ,KK,-RL)
      WRITE(IOUT,1026)IIO,JJO,KKO,ANG,II,JJ,KK,ANGB
      RHS=CAT-CB
  200 CONTINUE
      ZNORM=DSQRT(ZNORM)
C
      KOUNT=0
      DO 800 I=1,N3
      IF(KILLER(I).EQ.1)GO TO 800
      KOUNT=KOUNT+1
      GRAD(KOUNT)=grad(I)
  800 CONTINUE
C
      WRITE(IOUT,1000)Ndegf,(GRAD(I),I=1,Ndegf)
      IF(MYCLASS.EQ.1)WRITE(IOUT,1001)ZNORM
  900 CONTINUE
      RETURN
 1000 FORMAT(5X,' CONSTRAINTS:DIMENSION=',I3,
     X     (/5X,8F12.6))
 1001 FORMAT(/5X,'DISTANCE NORM=',E15.8)
 1026 FORMAT(5X,'ANG(',I2,',',I2,',',I2,')  ='
     X       ,F12.6,' ANG(',I2,',',I2,',',I2,')  =',F12.6)
 1027 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' |R(',I2,') - R(',I2,')|=',F12.6)
 1028 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' CONSTRAINT VALUE=',F12.6)
 1029 FORMAT(5X,'ANG(',I2,',',I2,
     X       ',',I2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
 1030 FORMAT(5X,'DIH ANG(',I2,',',I2,
     X       ',',I2,',',i2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
      END
      SUBROUTINE CONCON0(NATOMS,GM,SCR,II,JJ,RLAMBDA,X)
      implicit real*8(a-h,o-z)
      dimension gm(3,10),scr(2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      data two/2.d0/
C
      X = (GM(1,II)-GM(1,JJ))**2 + (GM(2,II)-GM(2,JJ))**2+
     1         (GM(3,II)-GM(3,JJ))**2
      DO 128 K=1,3
      if(ii.le.natoms)then
      iiu=(ii-1)*3
      SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))
      else
      i=ii-natoms
      do 111 j=1,natoms
      iiu=(j-1)*3
 111  SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))*psc(j,i)
      endif
C
      if(jj.le.natoms)then
      jju=(jj-1)*3
      SCR(JJU+K)=SCR(JJU+K)-RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))
      else
      i=jj-natoms
      do 112 j=1,natoms
      jju=(j-1)*3
  112 SCR(JJU+K)=SCR(JJU+K)-RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))*psc(j,i)
      endif
  128 CONTINUE
      return
      end
      SUBROUTINE CONCON1(NATOMS,GM,SCR,II,JJ,KK,RLAMBDA)
      implicit real*8(a-h,o-z)
      dimension gm(3,10),scr(2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      data two/2.d0/
C        STATEMENT FUNCTION GRADIENTS
      C1(X12,X23,R12,R23)=-(X23/R23+CA*X12/R12)/R12
      C2(X12,X23,R12,R23)=(X23-X12)/(R12*R23)
     1   +CA*(X12/R12**2-(X23/R23**2))
      C3(X12,X23,R12,R23)=(X12/R12+CA*X23/R23)/R23
C
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      DO 138 K=1,3
C
      X12=GM(K,II)-GM(K,JJ)
      X23=GM(K,JJ)-GM(K,KK)
      if(ii.le.natoms)then
      iiu=(ii-1)*3
      SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*C1(X12,X23,R12,R23)
      else
      i=ii-natoms
      do 111 j=1,natoms
      iiu=(j-1)*3
 111  SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*C1(X12,X23,R12,R23)*psc(j,i)
      endif
C
      if(kk.le.natoms)then
      KKU=(KK-1)*3
      SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*C3(X12,X23,R12,R23)
      else
      i=kk-natoms
      do 112 j=1,natoms
      kku=(j-1)*3
 112  SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*C3(X12,X23,R12,R23)*psc(j,i)
      endif
C
      if(jj.le.natoms)then
      jju=(jj-1)*3
      SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*C2(X12,X23,R12,R23)
      else
      i=jj-natoms
      do 113 j=1,natoms
      jju=(j-1)*3
 113  SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*C2(X12,X23,R12,R23)*psc(j,i)
      endif
C
  138 CONTINUE
C
      return
      end
      SUBROUTINE CONCON2(NATOMS,GM,SCR,II,JJ,KK,LL,RLAMBDA)
      implicit real*8(a-h,o-z)
      dimension gm(3,10),scr(2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      data two/2.d0/
C

      DO 138 K=1,3
C
      call gdih(gm(1,II),gm(1,JJ),gm(1,kk),gm(1,ll),1,k,gradiik)
      if(ii.le.natoms)then
      iiu=(ii-1)*3
      SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*gradiik
      else
      i=ii-natoms
      do 111 j=1,natoms
      iiu=(j-1)*3
 111  SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*gradiik*psc(j,i)
      endif
C
      CALL GDIH(GM(1,II),GM(1,JJ),GM(1,KK),GM(1,LL),3,K,GRADIIK)
      if(kk.le.natoms)then
      KKU=(KK-1)*3
      SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*gradIIk
      else
      i=kk-natoms
      do 112 j=1,natoms
      kku=(j-1)*3
 112  SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*gradiik*psc(j,i)
      endif
C
      call gdih(gm(1,II),gm(1,JJ),gm(1,kk),gm(1,ll),2,k,gradiik)
      if(jj.le.natoms)then
      jju=(jj-1)*3
      SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*gradiik
      else
      i=jj-natoms
      do 113 j=1,natoms
      jju=(j-1)*3
 113  SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*gradiik*psc(j,i)
      endif
C
      call gdih(gm(1,II),gm(1,JJ),gm(1,kk),gm(1,ll),4,k,gradiik)
      if(ll.le.natoms)then
      llu=(ll-1)*3
      SCR(LLU+K)=SCR(LLU+K)+RLAMBDA*gradiik
      else
      i=ll-natoms
      do 114 l=1,natoms
      llu=(l-1)*3
 114  SCR(LLU+K)=SCR(LLU+L)+RLAMBDA*gradiik*psc(j,i)
      endif
C
  138 CONTINUE
C
      return
      end

      subroutine  gDIH(C1,C2,C3,C4,ii,kk,grad)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      data h/0.001/
      do 10 i=1,3
      gm(i,1)=c1(i)
      gm(i,2)=c2(i)
      gm(i,3)=c3(i)
      gm(i,4)=c4(i)
 10   continue
      gm(kk,ii)=gm(kk,ii)+h
      call dihed1(gm(1,1),gm(1,2),gm(1,3),gm(1,4),dihp)
      gm(kk,ii)=gm(kk,ii)-h*2.d0
      call dihed1(gm(1,1),gm(1,2),gm(1,3),gm(1,4),dihm)
      grad=(dihp-dihm)/(2*h)
      return
      end

      SUBROUTINE DIHEDx(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      A1=ATAN(GM(2,1)/GM(1,1))
      if(gm(1,1).lt.0)a1=a1+pi
      A4=ATAN(GM(2,4)/GM(1,4))
      if(gm(1,4).lt.0)a4=a4+pi
      DIH=(A4-A1)/PI*180.
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END
      SUBROUTINE DIHED1(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      rl1=0
      rl2=0
      dot=0
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      dot=dot+gm(i,1)*(gm(i,4)-gm(i,3))
      rl1=rl1+gm(i,1)**2
      rl2=rl2+(gm(i,4)-gm(i,3))**2
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      do 31 i=3,3
      rl1=rl1-gm(i,1)**2
      rl2=rl2-(gm(i,4)-gm(i,3))**2
      dot=dot-gm(i,1)*(gm(i,4)-gm(i,3))
   31 continue
c  (-) for consistency with other dih method
      dih=-dot/sqrt(rl1*rl2)
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END
      SUBROUTINE DIHED(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      rl1=0
      rl2=0
      dot=0
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      dot=dot+gm(i,1)*(gm(i,4)-gm(i,3))
      rl1=rl1+gm(i,1)**2
      rl2=rl2+(gm(i,4)-gm(i,3))**2
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      do 31 i=3,3
      rl1=rl1-gm(i,1)**2
      rl2=rl2-(gm(i,4)-gm(i,3))**2
      dot=dot-gm(i,1)*(gm(i,4)-gm(i,3))
   31 continue
c  (-) for consistency with other dih method
      dih=-dot/sqrt(rl1*rl2)
      dih=180.d0/pi*acos(dih)
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END

      SUBROUTINE DIST(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,NATOMS)
      DATA TOA/0.52917715D0/
      DIJ(I,J)=DSQRT((GM(1,I)-GM(1,J))**2+(GM(2,I)-GM(2,J))**2+
     1         (GM(3,I)-GM(3,J))**2)
      WRITE(IOUT,1001)'AU      '
      DO 10 I=2,NATOMS
      WRITE(IOUT,1000)I,(DIJ(I,J),J=1,I-1)
   10 CONTINUE
      WRITE(IOUT,1001)'ANGSTROM'
      DO 20 I=2,NATOMS
      WRITE(IOUT,1000)I,(DIJ(I,J)*TOA,J=1,I-1)
   20 CONTINUE
      RETURN
 1000 FORMAT(2X,I3,2X,6F12.6,/,(7X,6F12.6))
 1001 FORMAT(/5X,'INTERNUCLEAR DISTANCES IN ',A8)
      END
C
      SUBROUTINE FANGL(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      WRITE(IOUT,1000)
      DO 10 I=1,NATOMS-2
      DO 11 J=I+1,NATOMS-1
      DO 11 K=J+1,NATOMS
      CALL ANGL(GM,I,J,K,ANG1,W,X,Y,Z)
      CALL ANGL(GM,J,K,I,ANG2,W,X,Y,Z)
      CALL ANGL(GM,K,I,J,ANG3,W,X,Y,Z)
      WRITE(IOUT,1001)I,J,K,ANG1, J,K,I,ANG2, K,I,J,ANG3
   11 CONTINUE
   10 CONTINUE
      RETURN
 1000 FORMAT(/5X,'INTERNUCLEAR ANGLES IN DEGREES',/)
 1001 FORMAT(2X,3(3(I2,1X),F6.1,2X))
      END
      SUBROUTINE FDANGL(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      WRITE(IOUT,1000)
      IEVE=0
      DO 10 I=1,NATOMS-3
      DO 11 J=I+1,NATOMS-2
      DO 11 K=J+1,NATOMS-1
      DO 11 L=K+1,NATOMS
      I0=I
      J0=J
      K0=K
      L0=L
      CALL DIHED(GM(1,I),GM(1,J),GM(1,K),GM(1,L),DIH)
      IEVE=IEVE+1
      IF(IAND(IEVE,1).NE.0)THEN
      I1=I
      J1=J
      K1=K
      L1=L
      DIH1=DIH
      GO TO 11
      ENDIF
      WRITE(IOUT,1001)I1,J1,K1,L1,DIH1,I,J,K,L,DIH
   11 CONTINUE
      IF(IAND(IEVE,1).NE.0)WRITE(IOUT,1001)I0,J0,K0,L0,DIH
   10 CONTINUE
      RETURN
 1000 FORMAT(/5X,'INTERNUCLEAR DIHEDRAL IN DEGREES',/)
 1001 FORMAT(2X,3(4(I2,1X),F6.1,2X))
      END
      SUBROUTINE FREEZG(NFIXDIR,FIXDIR,VALDIR,NCART,NATOMS,
     1                  GM,SCR,rhs,KLASS,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR(6,2),KLASS(2)
      DIMENSION VALDIR(2),RHS(nfixdir),SCR(NCART,2)
      DIMENSION GM(3,2)
      COMMON/PSCENT/MAXNEW,PSC(30,10),DPSC(30,10),IPSC(30,10)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/,one/1.d0/
C
      IF(MAXNEW.EQ.0)GO TO 99
      DO 40 I=1,MAXNEW
      DO 41 K=1,3
   41 GM(K,NATOMS+I)=0
      DO 42 K=1,3
      DO 42 J=1,NATOMS
   42 GM(K,NATOMS+I)=GM(K,NATOMS+I)+GM(K,J)*PSC(J,I)
   40 CONTINUE
   99 CONTINUE
C
      IF(NFIXDIR.EQ.0)GO TO 900
      NF=NCART+NFIXDIR
      N3=NATOMS*3
      nlong=ncart*nfixdir
      DO 101 I=1,Nfixdir
      rhs(i)=zero
      do 101 k=1,ncart
  101 SCR(k,I)=zero
C
      MYCLASS=0
      ZNORM=ZERO
      DO 200 I=1,NFIXDIR
      II=FIXDIR(1,I)
      JJ=FIXDIR(2,I)
      IIU=(II-1)*3
      JJU=(JJ-1)*3
      GO TO (400,600,400,600,700),KLASS(I)
      STOP ' ILLEGAL KLASS '
  400 CONTINUE
C         DISTANCE CONTRAINT
      MYCLASS=1
      call concon0(natoms,gm,scr(1,i),ii,jj,one,x)
      IF(KLASS(I).EQ.3)GO TO 150
      WRITE(IOUT,1028)II,JJ,DSQRT(X),VALDIR(I)
      RHS(I)=X-VALDIR(I)**2
      ZNORM=ZNORM+RHS(I)**2
      GO TO 200
  150 CONTINUE
C         DISTANCE DIFFERENCE CONSTRAINT
      KK=FIXDIR(3,I)
      LL=FIXDIR(4,I)
      call concon0(natoms,gm,scr(1,i),kk,ll,-one,y)
      WRITE(IOUT,1027)II,JJ,DSQRT(X),KK,LL,DSQRT(Y)
      RHS(I)=X-Y
      ZNORM=ZNORM+RHS(I)**2
      GO TO 200
  700 CONTINUE
C         dihedral ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      LL=FIXDIR(6,I)
      KKU=(KK-1)*3
      LLU=(LL-1)*3
      CALL DIHED1(GM(1,II),GM(1,JJ),GM(1,KK),GM(1,LL),DIH1)
      call concon2(NATOMS,GM,SCR(1,I),II,JJ,KK,LL,ONE)
      WRITE(IOUT,1030)II,JJ,KK,LL,acos(DIH1)*180.d0/pi,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      RHS(I)=dih1-DCOS(XX)
      GO TO 200
  600 CONTINUE
C         ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      KKU=(KK-1)*3
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      call concon1(NATOMS,GM,SCR(1,I),II,JJ,KK,ONE)
      IF(KLASS(I).EQ.4)GO TO 170
      WRITE(IOUT,1029)II,JJ,KK,ANG,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      RHS(I)=CA-DCOS(XX)
      GO TO 200
 170  continue
C         ANGLE DIFFERENCE
      IIO=II
      JJO=JJ
      KKO=KK
      II=FIXDIR(4,I)
      JJ=FIXDIR(5,I)
      KK=FIXDIR(6,I)
      IIU=(II-1)*3
      JJU=(JJ-1)*3
      KKU=(KK-1)*3
      CALL ANGL(GM,II,JJ,KK,ANGB,CB,R13,R12,R23)
      CAT=CA
      CA=CB
      call concon1(NATOMS,GM,SCR(1,I),II,JJ,KK,-ONE)
      WRITE(IOUT,1026)IIO,JJO,KKO,ANG,II,JJ,KK,ANGB
      RHS(I)=CAT-CB
  200 CONTINUE
      ZNORM=DSQRT(ZNORM)
C
  800 CONTINUE
C
  900 CONTINUE
      RETURN
 1000 FORMAT(5X,'GRADIENT ',A7,' CONSTRAINTS:DIMENSION=',I3,
     X     (/5X,8F12.6))
 1001 FORMAT(/5X,'DISTANCE NORM=',E15.8)
 1026 FORMAT(5X,'ANG(',I2,',',I2,',',I2,')  ='
     X       ,F12.6,' ANG(',I2,',',I2,',',I2,')  =',F12.6)
 1027 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' |R(',I2,') - R(',I2,')|=',F12.6)
 1028 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' CONSTRAINT VALUE=',F12.6)
 1029 FORMAT(5X,'ANG(',I2,',',I2,
     X       ',',I2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
 1030 FORMAT(5X,'DIH ANG(',I2,',',I2,
     X       ',',I2,',',i2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
      END
      SUBROUTINE GETGM(IPRU,NATOMS,GMC,IOUTU)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR
      REAL*8 LAMBDA(30),LAMBDAR(30)
      DIMENSION GMC(2)
      DIMENSION GM(30),GGRAD(30),HESS(465),EGRAD(30),XGRAD(30)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      NAMELIST/PROGRS/GM,EDIF,NEWDIR,EGRAD,GGRAD,SCALE,HESS,
     1                LAMBDA,RHSN,ESTATE1,LAMBDAR,XGRAD
C
      REWIND IPRU
      READ(IPRU,PROGRS,END=950)
      DO 105 I=1,3*NATOMS
      GMC(I)=GM(I)
  105 CONTINUE
      RETURN
  950 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
C
      SUBROUTINE GETIGM(IPRU,NATOMS,GMC,IOUTU)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBD0(2)
      DIMENSION GMC(2),GGRADC(2),HESSC(2),EGRADC(2)
      DIMENSION GM(30),GGRAD(30),EGRAD(30)
      DIMENSION GGRADH(930),EGRADH(930),DISP(30),XGRADH(930)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      NAMELIST/PROGRSH/GGRADH,EGRADH,IDISP,GM,EDIF,DISP,LAMBD0,EREF
     X                ,XGRADH
C
      REWIND IPRU
      READ(IPRU,PROGRSH,END=950)
      DO 109 I=1,3*NATOMS
      GMC(I)=GM(I)
  109 CONTINUE
      RETURN
  950 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
C
      SUBROUTINE GETLM(IPRU,NFIXDIR,LAMBDAC,IOUT,IRSTRT)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBDA(30),LAMBDAC(2),LAMBDAR(30)
      DIMENSION GM(30),GGRAD(30),HESS(465),EGRAD(30),XGRAD(30)
      NAMELIST/PROGRS/GM,EDIF,NEWDIR,EGRAD,GGRAD,SCALE,HESS,
     1                LAMBDA,RHSN,ESTATE1,LAMBDAR,XGRAD
      REWIND IPRU
      READ(IPRU,PROGRS,END=950)
      DO 105 I=1,NFIXDIR
      LAMBDAC(I)=LAMBDA(I)
      IF(IRSTRT.EQ.0)GO TO 105
      LAMBDAC(I)=LAMBDAR(I)
  105 CONTINUE
      RETURN
  950 CONTINUE
      WRITE(IOUT,1001)
      DO 106 I=1,NFIXDIR
      LAMBDAC(I)=0.D0
  106 CONTINUE
      RETURN
 1001 FORMAT(5X,'NO PROGRESS FILE- LM SET TO 0' )
      END
      SUBROUTINE GETHPRG(IPRU,NATOMS,GMC,IDISPC,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBD0(2)
      DIMENSION GMC(2),GGRADC(2),HESSC(2),EGRADC(2)
      DIMENSION GM(30),GGRAD(30),HESS(465),EGRAD(30)
      DIMENSION GGRADH(930),EGRADH(930),DISP(30),XGRADH(930)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      NAMELIST/PROGRSH/GGRADH,EGRADH,IDISP,GM,EDIF,DISP,LAMBD0,EREF
     X                ,XGRADH
C
      REWIND IPRU
      READ(IPRU,PROGRSH,END=900)
      DO 110 I=1,3*NATOMS
  110 GMC(I)=GM(I)
      IDISPC=IDISP
      RETURN
  900 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
C
      SUBROUTINE GETPRG(IPRU,NATOMS,NDEGF,GMC,EGRADC,GGRADC,XGRADC,
     1                  EDIFC,ES1,LAMBDAC,IOUT,NFIXDIR)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR
      REAL*8 LAMBDA(30),LAMBDAC(2),LAMBDAR(30)
      DIMENSION GMC(2),GGRADC(2),HESSC(2),EGRADC(2),XGRADC(2)
      DIMENSION GM(30),GGRAD(30),HESS(465),EGRAD(30),XGRAD(30)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      NAMELIST/PROGRS/GM,EDIF,NEWDIR,EGRAD,GGRAD,XGRAD,
     1                SCALE,HESS,LAMBDA,LAMBDAR,RHSN,ESTATE1
C
C            RESTART PATH USE OLD LAGRANGE MULTIPLIERS
      REWIND IPRU
      READ(IPRU,PROGRS,END=900)
      ES1=ESTATE1
      DO 106 I=1,NDEGF
      XGRADC(I)=XGRAD(I)
      EGRADC(I)=EGRAD(I)
  106 GGRADC(I)=GGRAD(I)
      DO 107 I=1,3*NATOMS
  107 GMC(I)=GM(I)
      DO 108 I=1,NFIXDIR+2
  108 LAMBDAC(I)=LAMBDAR(I)
      EDIFC=EDIF
      RETURN
  900 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
C
      SUBROUTINE GETHES(IPRU,NDEGF,HESSC,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBDA(30),LAMBDAR(30)
      DIMENSION HESSC(2)
      DIMENSION GM(30),GGRAD(30),HESS(465),EGRAD(30),XGRAD(30)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      NAMELIST/PROGRS/GM,EDIF,NEWDIR,EGRAD,GGRAD,XGRAD,SCALE,HESS,
     1                LAMBDA,RHSN,ESTATE1,LAMBDAR
C
      REWIND IPRU
      READ(IPRU,PROGRS,END=950)
      NDEGF2=NDEGF*(NDEGF+1)/2
      DO 108 IJ=1,NDEGF2
      HESSC(IJ)=HESS(IJ)
  108 CONTINUE
      RETURN
  950 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
      SUBROUTINE GETSYMT(NATOM,N3,SYMTRN,CHAR,SCR,I76,IOUT,KSEC,
     X                  IORIGIN,NTSYM,NTSYMNT,NOP)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION JSEC(50),SYMTRN(N3,N3),CHAR(N3,11),SCR(2)
      MASK=2*16-1
C
      OPEN(I76,FORM='FORMATTED')
      REWIND I76
      READ(I76,1001,END=500)MDF,NATOMS
      READ(I76,1001)(IZ,I=1,3*NATOMS)
      READ(I76,1001)(IZ,I=1,MDF)
      READ(I76,1001)(IZ,I=1,MDF)
      IF(NATOM.NE.NATOMS)GO TO 900
C
C         SYMMETRY EQUIVALENT CENTER INSTRUCTIONS
      KSEC=0
      DO 310 I=1,NATOMS
      READ(I76,1001)NDO
      KSEC=KSEC+NDO
      IF(NDO.EQ.0)GO TO 310
      READ(I76,1002)(JSEC(J),J=1,NDO)
  310 CONTINUE
      IF(KSEC.EQ.0)GO TO 500
C        GET SYMTRN AND CHARACTERS
C        REORDER SYMMETRIC BASIS FUNCTIONS TO PUT FIXED CENTER LAST
      READ(I76,1003)NATM3,NOP,NTSYM,NNTTSYM
      NTSYMNT=0
      IX=(IORIGIN-1)*3
      ISA=0
      ISB=NTSYM
      DO 10 I=1,NATM3
C         READ CHARACTERS
      READ(I76,1006)I2,(SCR(J),J=1,NOP+3)
      DO 311 J=1,NOP
  311 IF(DABS(SCR(J)).GT.1.0D-04)GO TO 350
      ISA=ISA+1
      DO 312 J=1,NOP+3
  312 CHAR(ISA,J)=SCR(J)
      READ(I76,1004)(SYMTRN(J,ISA),J=1,NATM3)
      DO 29 K=1,3
      IF(SYMTRN(IX+K,ISA).NE.0.)GO TO 250
   29 CONTINUE
      GO TO 10
  250 CONTINUE
C        KILLED CENTER PUT IT AT END OF TOTALLY SYMMETRIC GROUP
      DO 240 J=1,N3
  240 SYMTRN(J,NTSYM-NTSYMNT)=SYMTRN(J,ISA)
      DO 241 J=1,NOP+3
  241 CHAR(NTSYM-NTSYMNT,J)=CHAR(ISA,J)
      NTSYMNT=NTSYMNT+1
      ISA=ISA-1
      GO TO 10
  350 CONTINUE
C        NON TOTAL SYMMETRIC BASIS FUNCTION
      ISB=ISB+1
      DO 351 J=1,NOP+3
  351 CHAR(ISB,J)=SCR(J)
      READ(I76,1004)(SYMTRN(J,ISB),J=1,NATM3)
   10 CONTINUE
      IF(ISA+NTSYMNT.NE.NTSYM)STOP ' COUNTING PROBLEM IN GETSYMT '
      NTSYMNT=NTSYM-NTSYMNT
C
  500 CONTINUE
      WRITE(IOUT,1005)NATM3,NOP,NTSYM,NNTTSYM,NTSYMNT
      RETURN
C
  900 CONTINUE
      WRITE(IOUT,1000)I76,NATOM,NATOMS
 1000 FORMAT(5X,'UNIT=',I3,' ERROR NATOM=',I3,' NATOMS=',I3)
 1001 FORMAT(16I4)
 1002 FORMAT(8(1X,Z8))
 1003 FORMAT(14I5)
 1004 FORMAT(7F10.6)
 1005 FORMAT(/15X,'FROM GETSYMT:N3=',I3,' NOP=',I2,' NTSYM=',I3,
     X        ' NNTTSYM=',I3,' NTSYMNT=',I3)
 1006 FORMAT(I3,(7F10.6))
      END
      SUBROUTINE GMWRIT(NATOMS,COORD,IOUT,NSFIG,I75)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION COORD(3,2),GM(3,30)
      NAMELIST/PATH/GM
      DO 10 J=1,NATOMS
      DO 20 I=1,3
   20 GM(I,J)=COORD(I,J)
   10 CONTINUE
      WRITE(IOUT,1000)
      WRITE(IOUT,1001)((GM(J,I),J=1,3),I=1,NATOMS)
      OPEN(I75,FORM='FORMATTED')
      REWIND I75
      WRITE(I75,1003)
      WRITE(I75,1004)'PATH    '
      IF(NSFIG.EQ.3)WRITE(I75,1009)
     X'      GM',((GM(J,I),J=1,3),I=1,NATOMS)
      IF(NSFIG.EQ.4)WRITE(I75,1008)
     X'      GM',((GM(J,I),J=1,3),I=1,NATOMS)
      IF(NSFIG.EQ.5)WRITE(I75,1006)
     X'      GM',((GM(J,I),J=1,3),I=1,NATOMS)
      IF(NSFIG.EQ.6)WRITE(I75,1007)
     X'      GM',((GM(J,I),J=1,3),I=1,NATOMS)
       IF((NSFIG.GT.6).OR.(NSFIG.EQ.0))WRITE(I75,1010)
     X'      GM',((GM(J,I),J=1,3),I=1,NATOMS)
      WRITE(I75,1005)
      WRITE(IOUT,1002)I75,NSFIG
      RETURN
 1000 FORMAT(/10X,'NAMELIST INPUT FOR CURRENT GEOMETRY',/)
 1001 FORMAT(3X,'GM=',3(F15.8,','),/(6X,3(F15.8,',')))
 1002 FORMAT(5X,'PATH NAMELIST WRITTEN TO UNIT=',I3,' NSFIG=',I2)
 1003 FORMAT('****PATH')
 1004 FORMAT(1X,'&',A8)
 1005 FORMAT(1X,'&END')
CDRY 1006 FORMAT(1X,A8,'=',5(E20.10,',')/(11X,(5(E20.10,','))))
 1006 FORMAT(1X,A8,'=',3(F12.5,',')/(11X,(3(F12.5,','))))
 1007 FORMAT(1X,A8,'=',3(F12.6,',')/(11X,(3(F12.6,','))))
 1008 FORMAT(1X,A8,'=',3(F12.4,',')/(11X,(3(F12.4,','))))
 1009 FORMAT(1X,A8,'=',3(F12.3,',')/(11X,(3(F12.3,','))))
 1010 FORMAT(1X,A8,'=',3(E20.10,',')/(11X,(3(E20.10,','))))
      END
      SUBROUTINE JNTMO(NATOMS,GM,VECT,XDSP)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(2),VECT(2)
      DO 10 I=1,NATOMS*3
      GM(I)=GM(I)+XDSP*VECT(I)
   10 CONTINUE
      RETURN
      END
      subroutine mkcp(killer,natoms,iorg,ipru,iout)
      implicit real*8(a-h,o-z)
      dimension gvs2(30),gv(30),hv(30),sv(30),killer(3,10),
     x          gvn(30),hvn(30)
      pi=acos(-1.d0)
      nat3=natoms*3
      call gtfgh(IPRU,IOUT,GVs2,GV,HV,SV,IORG,EREF,
     X                  KILLER,NATOMS)
      call mkRGH(SV,GVS2,HV,GVN,HVN,NAT3,beta)
      DPGHN=DOT(GVN,HVN,NAT3)
      GVND=SQRT(DOT(GVN,GVN,NAT3))
      HVND=SQRT(DOT(HVN,HVN,NAT3))
      HXR=DOT(HVN,GVN,NAT3)/GVND
      HYR=DOT(HVN,HVN,NAT3)/HVND
      GXR=DOT(GVN,GVN,NAT3)/GVND
      GYR=DOT(GVN,HVN,NAT3)/HVND
      SXR=DOT(SV,GVN,NAT3)/GVND
      SYR=DOT(SV,HVN,NAT3)/HVND
      WRITE(iout,1044)BETA/PI*180.,DPGHN,GVND,HVND, GVND*HVND
      WRITE(iout,1014)'ROTD',SXR,SYR,0.,GXR,GYR,HXR,HYR
      WRITE(iout,1027)'HR',(HVN(J),J=1,NAT3)
      WRITE(iout,1027)'GR',(GVN(J),J=1,NAT3)
 1014 FORMAT(2X,A6,' SX=',F7.4,' SY=',F7.4,' SZ=',F7.4,
     X    /8X, ' GX=',F9.6,' GY=',F9.6,' HX=',F9.6,' HY=',F9.6)
 1027 FORMAT(/3X,A3,'=',3(F15.8,','),/,(6X,3(F15.8,',')))
 1044 FORMAT(/15X,'ROTATION TO ON  G AND H:  BETA=',F12.5,
     X    /5X, ' <G|H> NEW =',E12.5,
     X    /5X, ' |G|/2 NEW =',E10.5, 5X,  ' |H|   NEW =',E10.5,
     X    /5X, ' GXH/2 NEW =',E10.5)
      return
      end
      SUBROUTINE gtfgh(IPRU,IOUT,EGV,GV,HV,SV,IORG,EREF,
     X                  KILLER,NATOMS)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBDA(30),LAMBD0(2),LAMBDAR(30),DIPOLCIP(2)
      DIMENSION EGV(30),GV(30),HV(30),GMP(30),DIPOLCI(50),KILLER(3,10)
      DIMENSION GM(30),EGRAD(30),GGRAD(30),HESS(465),XGRAD(30),
     1          X1(3),X2(3),X3(3),SV(30)
      NAMELIST/PROGRS/EGRAD,GGRAD,HESS,NEWDIR,SCALE,GM,EDIF,
     1                LAMBDA,RHSN,ESTATE1,LAMBDAR,XGRAD
      NAMELIST/PRPRTY/DIPOLCI,XGRAD
      rewind ipru
      READ(IPRU,PROGRS,END=100)
      READ(IPRU,PRPRTY,END=102)
  102 CONTINUE
      EREF=ESTATE1
      NAT3=NATOMS*3
      DO 5 I=1,NAT3
      GV(I)=0.0
      HV(I)=0.0
   5  CONTINUE
      DO 19 I=1,3
      X1(I)=0
      X2(I)=0
      X3(I)=0
  19  CONTINUE
      IPT=0
C        I=1 X    I=2 Y   I=3 Z
      DO 20 J=1,NATOMS
      DO 20 I=1,3
C         J=ATOMS
      IF(J.EQ.IORG)GO TO 20
      IF(KILLER(I,J).EQ.1)GO TO 20
      IPT=IPT+1
      X1(I)=X1(I)+EGRAD(IPT)
      X2(I)=X2(I)+GGRAD(IPT)
      X3(I)=X3(I)+XGRAD(IPT)
      IPUT=(J-1)*3+I
      EGV(IPUT)=EGRAD(IPT)
      GV(IPUT)=GGRAD(IPT)
      HV(IPUT)=XGRAD(IPT)
      SV(IPUT)=EGV(IPUT)+GV(IPUT)/2
   20 CONTINUE
      DO 10 I=1,3
      IPUT=(IORG-1)*3+I
      EGV(IPUT)=-X1(I)
      GV(IPUT)=-X2(I)
      HV(IPUT)=-X3(I)
      SV(IPUT)=EGV(IPUT)+GV(IPUT)/2
   10 CONTINUE
      do 11 i=1,nat3
   11 egv(i)=gv(i)/2.
      WRITE(IOUT,1001)
      RETURN
  100 CONTINUE
      WRITE(IOUT,1000)
 1000 FORMAT(5X,'NO PROGRESS FILE')
 1001 FORMAT(5X,'**** EGV GV AND HV FROM PROGRESS FILE ****')
      RETURN
      END
      SUBROUTINE mkrGH(SV,GVS2,HV,GVN,HVN,NAT3,beta0)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GVS2(2),HV(2),GVN(2),HVN(2),sv(2),scr(100)
      do 10 i=1,nat3
      scr(i)=gvs2(i)
   10 scr(i+nat3)=hv(i)
      CALL SCHMOM(nat3,2,SCR,6,IFAIL)
      HX=DOT(SCR(1),HV,NAT3)
      HY=DOT(SCR(NAT3+1),HV,NAT3)
      Gx=DOT(SCR(1),GVS2,NAT3)
      GY=DOT(SCR(NAT3+1),GVS2,NAT3)
      SX=DOT(SCR(1),SV,NAT3)
      SY=DOT(SCR(NAT3+1),SV,NAT3)
C
      DENOB=DOT(HV,HV,NAT3)-DOT(GVS2,GVS2,NAT3)
      BETA0=0.25*ATAN(2.*DOT(GVS2,HV,NAT3)/DENOB)
      CALL RBB(NAT3,BETA0,GVS2,HV,GVN,HVN)
 200  continue
      return
      end
      subroutine mkpscent(natoms,maxnew,psc,dpsc,ipsc,mass,iout)
      IMPLICIT REAL*8(A-H,O-Z)
      dimension psc(30,2),dpsc(30,2),ipsc(30,2)
      real*8 mass(2),masst
      write(iout,1001)
      do 20 i=1,maxnew
      masst=0
      do 30 j=1,natoms
   30 psc(j,i)=0.d0
      do 31 j=1,natoms
      jj=ipsc(j,i)
      if(jj.eq.0)go to 31
      if(jj.gt.0)then
      cf=dpsc(j,i)
      else
      cf=mass(iabs(jj))
      masst=masst+cf
      endif
      psc(iabs(jj),i)=cf
   31 continue
      if(masst.lt.0.01)masst=1
      do 32 j=1,natoms
   32 psc(j,i)=psc(j,i)/masst
      write(iout,1000)i,(j,psc(j,i),j=1,natoms)
   20 continue
      return
 1000 format(2x,i2,6(1x,i2,f9.5))
 1001 format(/15x,'PSEUDO CENTERS')
      end

      SUBROUTINE NUMATM(IUNIT,IP,NATOMS)
      IMPLICIT REAL*8(A-H,O-Z)
      real *8 mass
      character*3 name
      DIMENSION GM(3,100),name(100),char(100),mass(100)
C
      READ(IUNIT,*,END=250)name(1),char(1),(gm(j,1),j=1,3),mass(1)
      natoms=1
      DO 252 I=2,100
      READ(IUNIT,*,END=253)name(i),char(i),(gm(j,i),j=1,3),mass(i)
      NATOMS=I
  252 CONTINUE
  253 CONTINUE
      IF(IP.LE.0)GO TO 260
      WRITE(IP,1004)IUNIT
      WRITE(IP,1005)(J,(GM(I,J),I=1,3),J=1,NATOMS)
      GO TO 260
  250 CONTINUE
      WRITE(IP,1003)
      CALL EXIT(100)
  260 CONTINUE
      RETURN
 1003 FORMAT(5X,'**** NO CURRENT GEOMETRY FILE ****' )
 1004 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1005 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
      END
      SUBROUTINE readcolg(IUNIT,gm,NATOMS,ip)
      IMPLICIT REAL*8(A-H,O-Z)
      real *8 mass
      character*3 name
      DIMENSION GM(3,100),name(100),char(100),mass(100)
C
      DO 252 I=1,natoms
      READ(IUNIT,*,END=250)name(i),char(i),(gm(j,i),j=1,3),mass(i)
  252 CONTINUE
      return
  250 CONTINUE
      write(ip,1003)
      CALL EXIT(100)
 1003 FORMAT(5X,'**** NO CURRENT GEOMETRY FILE ****' )
 1004 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1005 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
      END
      SUBROUTINE RDT30(N30,E,NSTATE,IOUTU,IERR)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION E(2)
      IERR=0
      REWIND N30
      READ(N30,END=60)N,NSTATE,E0
   10 READ(N30,END=20)IT,IUNIT,(E(I),I=1,NSTATE)
      GO TO 10
   20 CONTINUE
C      DO 21 I=1,NSTATE
C   21 E(I)=E(I)+E0
      WRITE(IOUTU,1001)(E(I),I=1,NSTATE)
      RETURN
   60 CONTINUE
      WRITE(IOUTU,1000)N30
      IERR=1
      RETURN
 1000 FORMAT(5X,'UNIT ',I3,' EMPTY ')
 1001 FORMAT(5X,'CI ENERGIES FROM ALCHEMY TAPE',(/2X,5E20.13))
      END
      SUBROUTINE rptcon(NFIXDIR,FIXDIR,VALDIR,NDEGF,NATOMS,
     1                  GM,KILLER,KLASS,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR(6,2),KLASS(2)
      DIMENSION KILLER(2),VALDIR(2)
      DIMENSION GM(3,2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/
      if(maxnew.eq.0)go to 99
      do 40 i=1,maxnew
      do 41 k=1,3
   41 gm(k,natoms+i)=0
      do 42 k=1,3
      do 42 j=1,natoms
   42 gm(k,natoms+i)=gm(k,natoms+i)+gm(k,j)*psc(j,i)
   40 continue
   99 continue
C
      IF(NFIXDIR.EQ.0)GO TO 900
      NF=NDEGF+NFIXDIR
      N3=NATOMS*3
C
      MYCLASS=0
      ZNORM=ZERO
      DO 200 I=1,NFIXDIR
      II=FIXDIR(1,I)
      JJ=FIXDIR(2,I)
      GO TO (400,600,400,600,700),KLASS(I)
      STOP ' ILLEGAL KLASS '
  400 CONTINUE
C         DISTANCE CONTRAINT
      MYCLASS=1
      X = (GM(1,II)-GM(1,JJ))**2 + (GM(2,II)-GM(2,JJ))**2+
     1         (GM(3,II)-GM(3,JJ))**2
      IF(KLASS(I).EQ.3)GO TO 150
      WRITE(IOUT,1028)II,JJ,DSQRT(X),VALDIR(I)
      ZNORM=ZNORM+(X-VALDIR(I)**2)**2
      GO TO 200
  150 CONTINUE
C         DISTANCE DIFFERENCE CONSTRAINT
      kk=FIXDIR(3,I)
      LL=FIXDIR(4,I)
      y = (GM(1,kk)-GM(1,LL))**2 + (GM(2,kk)-GM(2,LL))**2+
     1         (GM(3,kk)-GM(3,LL))**2
      WRITE(IOUT,1027)II,JJ,DSQRT(X),KK,LL,DSQRT(Y)
      GO TO 200
  700 CONTINUE
C         dihedral ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      LL=FIXDIR(6,I)
      KKU=(KK-1)*3
      LLU=(LL-1)*3
      CALL DIHED1(GM(1,II),GM(1,JJ),GM(1,KK),GM(1,LL),DIH1)
      WRITE(IOUT,1030)II,JJ,KK,LL,acos(DIH1)*180.d0/pi,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      GO TO 200
  600 CONTINUE
C         ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      IF(KLASS(I).EQ.4)GO TO 170
      WRITE(IOUT,1029)II,JJ,KK,ANG,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      GO TO 200
  170 CONTINUE
C         ANGLE DIFFERENCE
      IIO=II
      JJO=JJ
      KKO=KK
      II=FIXDIR(4,I)
      JJ=FIXDIR(5,I)
      KK=FIXDIR(6,I)
      CALL ANGL(GM,II,JJ,KK,ANGB,CB,R13,R12,R23)
      CAT=CA
      CA=CB
      WRITE(IOUT,1026)IIO,JJO,KKO,ANG,II,JJ,KK,ANGB
  200 CONTINUE
      ZNORM=DSQRT(ZNORM)
      IF(MYCLASS.EQ.1)WRITE(IOUT,1001)ZNORM
C
  900 CONTINUE
      RETURN
 1000 FORMAT(5X,'GRADIENT ',A7,' CONSTRAINTS:DIMENSION=',I3,
     X     (/5X,8F12.6))
 1001 FORMAT(/5X,'DISTANCE NORM=',E15.8)
 1026 FORMAT(5X,'ANG(',I2,',',I2,',',I2,')  ='
     X       ,F12.6,' ANG(',I2,',',I2,',',I2,')  =',F12.6)
 1027 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' |R(',I2,') - R(',I2,')|=',F12.6)
 1028 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' CONSTRAINT VALUE=',F12.6)
 1029 FORMAT(5X,'ANG(',I2,',',I2,
     X       ',',I2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
 1030 FORMAT(5X,'DIH ANG(',I2,',',I2,
     X       ',',I2,',',i2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
      END
      SUBROUTINE SCHMO(N,V,IOUT,IFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION V(N,N)
      IFAIL=0
      DO 100 I=1,N
      Z=0.D0
      DO 90 J=1,N
   90 Z=Z+V(J,I)**2
      Z=DSQRT(Z)
      DO 80 J=1,N
   80 V(J,I)=V(J,I)/Z
  100 CONTINUE
C
      DO 500 I=2,N
      DO 300 J=1,I-1
      DP=DOT(V(1,I),V(1,J),N)
      DO 250 K=1,N
  250 V(K,I)=V(K,I)-DP*V(K,J)
  300 CONTINUE
      Z=0.D0
      DO 260 K=1,N
  260 Z=Z+V(K,I)**2
      Z=DSQRT(Z)
      IF(Z.LT.1.0D-06)THEN
      WRITE(IOUT,1000)I,Z
      IFAIL=I
      RETURN
      ENDIF
      DO 270 K=1,N
  270 V(K,I)=V(K,I)/Z
  500 CONTINUE
      RETURN
 1000 FORMAT(5X,'BAD VECTOR IN SCHMO:I=',I3,' NORM=',E15.6)
      END
      SUBROUTINE SCHMOM(N,M,V,IOUT,IFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION V(N,M)
      IFAIL=0
      DO 100 I=1,M
      Z=0.D0
      DO 90 J=1,N
   90 Z=Z+V(J,I)**2
      Z=DSQRT(Z)
      DO 80 J=1,N
   80 V(J,I)=V(J,I)/Z
  100 CONTINUE
C
      DO 500 I=2,M
      DO 300 J=1,I-1
      DP=DOT(V(1,I),V(1,J),N)
      DO 250 K=1,N
  250 V(K,I)=V(K,I)-DP*V(K,J)
  300 CONTINUE
      Z=0.D0
      DO 260 K=1,N
  260 Z=Z+V(K,I)**2
      Z=DSQRT(Z)
      IF(Z.LT.1.0D-06)THEN
      WRITE(IOUT,1000)I,Z
      IFAIL=I
      RETURN
      ENDIF
      DO 270 K=1,N
  270 V(K,I)=V(K,I)/Z
  500 CONTINUE
      RETURN
 1000 FORMAT(5X,'BAD VECTOR IN SCHMO:I=',I3,' NORM=',E15.6)
      END
      SUBROUTINE SGNHIJ(IPRU,NDEGF,HIJP,IOUT,CHANGE)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBDA(30),LAMBD0(2),LAMBDAR(30)
      DIMENSION HIJP(2)
      DIMENSION GM(30),EGRAD(30),GGRAD(30),HESS(465),XGRAD(30)
      NAMELIST/PROGRS/EGRAD,GGRAD,HESS,NEWDIR,SCALE,GM,EDIF,
     1                LAMBDA,RHSN,ESTATE1,LAMBDAR,XGRAD
      NAMELIST/STATUS/ISTATUS
C
      CHANGE=1.D0
      DOT=0.D0
      RNORM1=0.D0
      RNORM2=0.D0
      REWIND IPRU
      READ(IPRU,PROGRS,END=800)
      DO 100 I=1,NDEGF
      RNORM1=RNORM1+XGRAD(I)**2
      RNORM2=RNORM2+HIJP(I)**2
      DOT=DOT+HIJP(I)*XGRAD(I)
  100 CONTINUE
      RNORM1=DSQRT(RNORM1)
      RNORM2=DSQRT(RNORM2)
      IF(DOT.GE.0.0D0)GO TO 800
      WRITE(IOUT,1000)NDEGF,DOT,RNORM1,RNORM2
      CHANGE=-1.D0
      DO 101 I=1,NDEGF
  101 HIJP(I)=-HIJP(I)
  800 CONTINUE
      REWIND IPRU
      RETURN
 1000 FORMAT(5X,'CHANGING SIGN OF HIJ: LENGTH=',I3,
     X      /5X,'OVERLAP=',E12.6,' NORMS=',2E12.6)
      END
      SUBROUTINE TBAK(NL,NS,T,X,Y,IGO)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(NL,NS),X(2),Y(2)
      DO 10 I=1,NL
   10 Y(I)=0.D0
      DO 20 I=1,NS
      DO 20 J=1,NL
      Y(J)=Y(J)+T(J,I)*X(I)
   20 CONTINUE
      IF(IGO.EQ.1)RETURN
      DO 30 I=1,NL
   30 X(I)=Y(I)
      RETURN
      END
      SUBROUTINE WRTPGS(IPRU,NATOMS,NDEGF,GM,EDIF,NEWDIR,
     1                  EGRAD,GGRAD,XGRAD,SCALE,HESS,
     2                  LAMBDA,LAMBDAR,RHSN,ESTATE1,NFIXDIR)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(2),EGRAD(2),GGRAD(2),XGRAD(2),HESS(2),
     1          GGRADH(2),EGRADH(2),XGRADH(2),DISP(2)
      REAL*8 LAMBDA(2),LAMBD0(2),LAMBDAR(2)
      NDEGF2=NDEGF*(NDEGF+1)/2
      WRITE(IPRU,1000)'PROGRS  '
      WRITE(IPRU,1002)'      GM',(GM(I),I=1,3*NATOMS)
      WRITE(IPRU,1002)'   EGRAD',(EGRAD(I),I=1,NDEGF)
      WRITE(IPRU,1002)'   GGRAD',(GGRAD(I),I=1,NDEGF)
      WRITE(IPRU,1002)'   XGRAD',(XGRAD(I),I=1,NDEGF)
      WRITE(IPRU,1002)'    HESS',(HESS(I),I=1,NDEGF2)
      WRITE(IPRU,1002)'  LAMBDA',(LAMBDA(I),I=1,NFIXDIR)
      WRITE(IPRU,1002)' LAMBDAR',(LAMBDAR(I),I=1,NFIXDIR)
      WRITE(IPRU,1002)'   SCALE',SCALE
      WRITE(IPRU,1002)'    EDIF',EDIF
      WRITE(IPRU,1002)'    RHSN',RHSN
      WRITE(IPRU,1002)' ESTATE1',ESTATE1
      WRITE(IPRU,1003)'  NEWDIR',NEWDIR
      WRITE(IPRU,1001)
      RETURN
      ENTRY WRTPGSH(IPRU,NATOMS,NDEGF,EGRADH,GGRADH,XGRADH,GM,DISP,
     1              EDIF,IDISP,LAMBD0,EREF)
      NWRIT=(2*NDEGF+1)*NDEGF
      WRITE(IPRU,1000)'PROGRSH '
      WRITE(IPRU,1002)'      GM',(GM(I),I=1,3*NATOMS)
      WRITE(IPRU,1002)'  EGRADH',(EGRADH(I),I=1,NWRIT)
      WRITE(IPRU,1002)'  GGRADH',(GGRADH(I),I=1,NWRIT)
      WRITE(IPRU,1002)'  XGRADH',(XGRADH(I),I=1,NWRIT)
      WRITE(IPRU,1002)'    DISP',(DISP(I),I=1,NDEGF)
      WRITE(IPRU,1002)'  LAMBD0',(LAMBD0(I),I=1,2)
      WRITE(IPRU,1002)'    EDIF',EDIF
      WRITE(IPRU,1002)'    EREF',EREF
      WRITE(IPRU,1003)'  IDISP',IDISP
      WRITE(IPRU,1001)
      RETURN
 1000 FORMAT(1X,'&',A8)
 1001 FORMAT(1X,'&END')
 1002 FORMAT(1X,A8,'=',5(E20.10,',')/(11X,(5(E20.10,','))))
 1003 FORMAT(1X,A8,'=',15(I5,',')/(11X,(15(I5,','))))
      END
      SUBROUTINE EXIT(I)
      IF(I.EQ.0)STOP 0
      IF(I.EQ.50)STOP 50
      IF(I.EQ.100)STOP 100
      IF(I.EQ.1)STOP 1
      STOP ' INVALID EXIT CODE '
      END
      SUBROUTINE RBB(N,BETA,A,B,AN,BN)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(N),B(N),AN(N),BN(N)
      C2B=COS(2*BETA)
      S2B=SIN(2*BETA)
      DO 10 I=1,N
      AN(I)=C2B*A(I)-S2B*B(I)
 10   BN(I)=S2B*A(I)+C2B*B(I)
      RETURN
      END

