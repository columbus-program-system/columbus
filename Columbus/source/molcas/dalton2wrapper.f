!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine mo2out_dalton_init 

      implicit none
      integer idummy
      logical oldunit
      include 'col_dalton.inc'

      oldunit=.false.
      call gpopen(luscr,'AODENS_D','UNKNOWN',' ',' ',IDUMMY,oldunit)
      rewind(luscr)
      nbuf=0
      return
      end


      subroutine mo2out_dalton_fin
      implicit none
      include 'col_dalton.inc'
      if (nbuf.gt.0) then
       dibuffer(nbuf+1)=nbuf
c      call writei(luscr,dbuffer,dibuffer)
      endif
      call gpclose(luscr,'KEEP')
      return
      end

      subroutine mo2out_dalton (valbuf,lbuf,nval)
      implicit none
      include 'col_dalton.inc'

      integer nval
      real*8 valbuf(nval)
      integer lbuf(4,nval)
      integer i,l1,l2,l3,l4,ltmp

      do i=1,nval
        l1=lbuf(1,i)
        l2=lbuf(2,i)
        l3=lbuf(3,i)
        l4=lbuf(4,i)
c       canonical ordering necessary ???

        if (l1.lt.l2) then
        ltmp=l1
        l1=l2
        l2=ltmp
        endif
        if (l3.lt.l4) then
        ltmp=l3
        l3=l4
        l4=ltmp
        endif

        nbuf=nbuf+1
        dbuffer(nbuf)=valbuf(i)
        dibuffer(nbuf)= 
     .     l1*2**(3*ifield)+l2*2**(2*ifield)+l3*2**(ifield)+l4
        if (nbuf.ge.mx2buf-1) then
           dibuffer(mx2buf)=nbuf
           call bummer('bug in mo2out_dalton',0,2)
c          call writei(luscr,l2buf,dalbuffer)
           nbuf=0
        endif 
       enddo
       return
       end
           
        

       
 

