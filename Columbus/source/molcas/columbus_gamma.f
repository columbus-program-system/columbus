!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*fordeck aces_gamma $Revision: 2.1 $ 
      Subroutine Columbus_Gamma()
      Implicit Real*8 (a-h,o-z)
#include "itmax.fh"
#include "info.fh"
      Include 'shinf.inc'
      Include 'setup.inc'
      Include 'WrkSpc.inc'
      Include 'aces_gamma.inc'
*                                                                      *
************************************************************************
*                                                                      *
      iTable(i,j)=iWork(ipiTab-1+(j-1)*6+i)
*                                                                      *
************************************************************************
*                                                                      *
      nShell=mSkal
      nPair=nShell*(nShell+1)/2
      nQuad=nPair*(nPair+1)/2
*                                                                      *
************************************************************************
*                                                                      *
*---- Allocate Table Of Content for half sorted gammas.
*
      Call GetMem('G_Toc','Allo','Real',ipG_Toc,nQuad)
*
*---- Tabel SO to contigeous index
*
      Call GetMem('SO2cI','Allo','Inte',ipSO2cI,2*nSOs)
*
*     Both should be deallocated in CloseP!
*
*---- Allocate memory for read buffer
*
      Call GetMem('Buff','Allo','Real',ipBuf,nReq)
*                                                                      *
************************************************************************
*                                                                      *
*---- Allocate bins for shell quadruplets
*
      lBin=Min(MaxMem/(2*nQuad),1024)
      Call GetMem('Bin','Allo','Real',ipBin,2*lBin*nQuad)
*                                                                      *
************************************************************************
*                                                                      *
*---- Open the bin file with half sorted gammas.
*
      LuGamma=60
      LuGamma=isfreeunit(LuGamma)
      Call DaName_MF(LuGamma,'GAMMA')
*                                                                      *
************************************************************************
*                                                                      *
*---- Read  2e density in sifs format  
*     The second half sort is done on the fly as needed.
*
      Call Read_cid2fl(iWork(ipiTab),nBlocks,nBas,nIrrep,
     &                 iOffSO,Work(ipBuf),nReq,
     &                 iWork(ipSOSh),nSOs,Work(ipBin),lBin,nQuad,
     &                 Work(ipG_Toc),iWork(ipSO2cI),CutInt) 
*                                                                      *
*                                                                      *
************************************************************************
*                                                                      *
*---- Deallocate memory
*
      Call GetMem('Bin','Free','Real',ipBin,2*lBin*nQuad)
*                                                                      *
************************************************************************
*                                                                      *
*---- Allocate buffer for reading the bins
*
      Call GetMem('Bin','Allo','Real',ipBin,2*lBin)
*                                                                      *
************************************************************************
*                                                                      *
      Return
      End
