!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*fordeck alaska_super_driver $Revision: 2.3 $           
      Subroutine Alaska_Super_Driver(iRC)
      Implicit Real*8 (a-h,o-z)
      Character*8 Method
      Logical Do_Cholesky, Numerical, Do_DF, Do_ESPF, StandAlone, Exist,
     &        Do_Numerical_Cholesky, Do_1CCD
      Character FileName*128, Line*180, KSDFT*16, Label*80
#include "warnings.fh"
      Include "real.inc"
*                                                                      *
************************************************************************
*                                                                      *
      iPL=iPrintLevel(-1)
      iTemp=1
      Call Getenvf("MOLCAS_ITER",Label)
      Read (Label,'(I80)') iTemp
      Call Getenvf("MOLCAS_REDUCE_PRT",Label)
      if(Label(1:1).eq.'N') iTemp=1
      If (iPL.ge.3) iTemp=1
      If (iTemp.gt.1.or.iPL.eq.0) Then
         iPL=0
      End If
*                                                                      *
************************************************************************
*                                                                      *
*
*     Check the input for the numerical keyword
*
*
*     copy STDINP to LuSpool
*
      LuSpool=37
      Call SpoolInp(LuSpool)
      Call Chk_Numerical(LuSpool,Numerical)
*
*     Call the numerical procedure if numerical option is available.
*     Otherwise hope that the analytic code know how to handle the
*     case.
*
      Call Get_cArray('Relax Method',Method,8)
*
*                                                                      *
************************************************************************
*                                                                      *
*     Default for Cholesky or RI/DF is to do the numerical procedure.
*     However, for pure DFT we have analytic gradients.
*
      Call DecideOnCholesky(Do_Cholesky)
      Call DecideOnDF      (Do_DF)
      Call DecideOn1CCD    (Do_1CCD)
      Call Get_iScalar('NSYM',nSym)
*
      Do_Numerical_Cholesky = Do_Cholesky .or. Do_DF
*
      If (Method.eq.'KS-DFT  '.and.Do_Numerical_Cholesky) Then
         Call Get_cArray('DFT functional',KSDFT,16)
         ExFac=Get_ExFac(KSDFT)
*        Currently implemented only for pure DFT with RI or 1C-CD
         If (ExFac.eq.Zero .and.                          ! Pure DFT
     &       (Do_DF         .or.                          ! RI/DF
     &       (Do_Cholesky.and.Do_1CCD.and.nSym.eq.1))     ! 1C-CD
     &                                ) Then
            Do_Numerical_Cholesky=.False.
         End If
      End If
*                                                                      *
************************************************************************
*                                                                      *
      Call DecideOnESPF(Do_ESPF)
*                                                                      *
************************************************************************
*                                                                      *
      If (Numerical              .OR.
     &    Do_Numerical_Cholesky  .OR.
     &    Method .eq. 'RASSCFSA' .OR.
     &    Method .eq. 'CASPT2'   .OR.
     &    Method .eq. 'MBPT2'    .OR.
     &    Method .eq. 'CCSDT'    ) Then
*                                                                      *
************************************************************************
*                                                                      *
*        Numerical gradients to be used!
*        Alaska will automatically generate the input for CASPT2_Gradient
*        and signal to AUTO (iRC=2) to run the input file Stdin.2.
*
         If (iPL.ge.3) Then
            Write (6,*) 
            Write (6,*) ' Alaska requests the Numerical_Gradient'
     &                //' module to be executed!'
            Write (6,*) 
         End If
*
         LuInput=11
         LuInput=IsFreeUnit(LuInput)
         Call Molcas_Open(LuInput,'Stdin.2')
C        Open(unit=LuInput,
C    &        file='Stdin.2',
C    &        form='FORMATTED',
C    &        status='UNKNOWN',
C    &        err=910)
         Write (LuInput,'(A)') ' &NUMERICAL_GRADIENT &End'
         Write (LuInput,'(A)') 'End of Input'
         Close(LuInput)
         Call Finish(_RC_INVOKED_OTHER_MODULE_)
*                                                                      *
************************************************************************
*                                                                      *
      Else If (Method.eq.'CASSCFSA') Then
*                                                                      *
************************************************************************
*                                                                      *
*        State-Average CASSCF 
*
         Call Get_iScalar('SA ready',iGo)
         If (iGO.ge.1) Then
            Call Alaska(LuSpool,iRC)
*
*        Add ESPF contribution
*
            If (Do_ESPF) Then
               StandAlone=.False.
               Call ESPF(iReturn,StandAlone)
               If (iReturn.ne.0) Then
                  Write (6,*) 'Alaska: ESPF finish with non-zero return'
     &                      //' code!'
                  Call Abend()
               End If 
            End If
         Else If (iGO.eq.-1) Then
            Write (6,*) 'Gradients not implemented for SA-CASSCF'//
     &                  ' with non-equivalent weights!'
            Call Abend()
         Else
            If (iPL.ge.3) Then
               Write (6,*) 
               Write (6,*) 
     &           ' Alaska requests MCLR to be run before it starts'
     &           //' again!'
               Write (6,*) 
            End If
*
            Call Get_iScalar('Relax CASSCF root',irlxroot)
*
            LuInput=11
            LuInput=IsFreeUnit(LuInput)
            Call Molcas_open(LuInput,'Stdin.2')
C           Open(unit=LuInput,
C    &           file='Stdin.2',
C    &           form='FORMATTED',
C    &           status='UNKNOWN',
C    &           err=910)
            Write (LuInput,'(A)') ' &MCLR &End'
            Write (LuInput,'(A)') 'SALA'
            Write (LuInput,'(I4)') irlxroot
            Write (LuInput,'(A)') 'End of Input'
            Write (LuInput,'(A)') ' '
*
            FileName='ALASKINP'
            Call f_inquire(Filename,Exist)
*
            If (Exist) Then
               LuSpool2 = 77
               LuSpool2 = IsFreeUnit(LuSpool2)
               Call Molcas_Open(LuSpool2, Filename)
*
 100           Continue
               Read (LuSpool2,'(A)',End=900) Line
               Write(LuInput,'(A)') Line
               Go To 100
 900           Continue
*
               Close(LuSpool2)
*
            Else
*
               Write (LuInput,'(A)') ' &Alaska &End'
               Write (LuInput,'(A)') 'Show'
               Write (LuInput,'(A)') 'CutOff'
               Write (LuInput,'(A)') '1.0D-7'
               Write (LuInput,'(A)') 'End of Input'
*
            End If
*
            Write (LuInput,'(A)') '!rm -f $Project.MckInt'
            Close(LuInput)
            Call Finish(_RC_INVOKED_OTHER_MODULE_)
*
           End If


************ columbus interface ****************************************
                                                                          
       elseif (method.eq.'MR-CISD ') then                                
                                                                        
*                                                                     
*       COLUMBUS MR-CI gradient:                                   
*           effective density matrix on RUNFILE 'densao_var'  
*           effective fock matrix  on RUNFILE   'FockOcc'  
*           effective  D2          on GAMMA               
*           G_TOC indices          on binary file gtoc   
*           RUNFILE read in drvh1                     
*           GAMMA read in pget0                    
*           gtoc read in drvg1                    
                                                                              
*          write(6,*) 'COLUMBUS MR-CISD gradients using ALASKA '            
                                                                          
           call Alaska(LuSpool,iRC)                                     
                                                                    


*                                                                      *
************************************************************************
*                                                                      *
      Else
*                                                                      *
************************************************************************
*                                                                      *
*        Go ahead and compute the gradients
*
         Call Alaska(LuSpool,iRC)
*
*        Add ESPF contribution
*
         If (Do_ESPF) Then
            StandAlone=.False.
            Call ESPF(iReturn,StandAlone)
            If (iReturn.ne.0) Then
               Write (6,*) 'Alaska: ESPF finish with non-zero return'
     &                   //' code!'
               Call Abend()
            End If 
         End If
        
*                                                                      *
************************************************************************
*                                                                      *
      End If
*                                                                      *
************************************************************************
*                                                                      *
      Return
      End
