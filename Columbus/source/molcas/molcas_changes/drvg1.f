!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*fordeck drvg1 $Revision: 2.3 $            
CStart Molcas
      SubRoutine Drvg1(Grad,Temp,nGrad)
celse
c;      SubRoutine Drvg1(Grad,Temp,nGrad,fock,fock1,d1ao,d1ao1)
cend
************************************************************************
*                                                                      *
*  Object: driver for two-electron integrals. The four outermost loops *
*          will controll the type of the two-electron integral, eg.    *
*          (ss|ss), (sd|pp), etc. The next four loops will generate    *
*          list of symmetry distinct centers that do have basis func-  *
*          tions of the requested type.                                *
*                                                                      *
* Called from: Alaska                                                  *
*                                                                      *
* Calling    : QEnter                                                  *
*              SetUp_Ints                                              *
*              GetMem                                                  *
*              DCopy   (ESSL)                                          *
*              Swap                                                    *
*              MemRg1                                                  *
*              PSOAO1                                                  *
*              PGet0                                                   *
*              TwoEl                                                   *
*              QExit                                                   *
*                                                                      *
*     Author: Roland Lindh, IBM Almaden Research Center, San Jose, CA  *
*             March '90                                                *
*                                                                      *
* Copyright 1990 IBM                                                   *
*                                                                      *
*             Roland Lindh, Dept. of Theoretical Chemistry, University *
*             of Lund, SWEDEN.                                         *
*             Modified for k2 loop. August '91                         *
*             Modified for gradient calculation. January '92           *
*             Modified for SetUp_Ints. January '00                     *
*                                                                      *
* (c) 2000 Dept. of Chem. Phys., Univ. of Lund, Sweden                 *
* All rights reserved.                                                 *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
      External King, Rsv_GTList, MPP
      Include 'real.inc'
#include "itmax.fh"
#include "info.fh"
      Include 'WrkSpc.inc'
      Include 'print.inc'
      Include 'disp.inc'
      Include 'nsd.inc'
      Include 'setup.inc'
cStart Molpro
c;      logical lprn,node0
c;      common/prnprn/lprn(40)
c;      common/codeplus/luhf
c;      common/uhfflag/inuhf
c;      dimension fock(*),fock1(*),d1ao(*),d1ao1(*)
cEnd
*     Local arrays
      Real*8  Coor(3,4), Grad(nGrad), Temp(nGrad)
      Integer iAnga(4), iCmpa(4), iShela(4),iShlla(4),
     &        iAOV(4), istabs(4), iAOst(4), JndGrd(3,4), iFnc(4)
      Integer nHrrTb(0:iTabMx,0:iTabMx,2)
      Logical EQ, Shijij, AeqB, CeqD, lDummy,
     &        DoGrad, DoFock, Indexation,
     &        JfGrad(3,4), ABCDeq, No_Batch, King, Rsv_GTList, MPP,
     &        FreeK2, Verbose, Triangular
      Character*7 Format*72
************ columbus interface ****************************************
      Character*8 Method 
*
      Integer iSD4(0:nSD,4)
      save MemPrm
*                                                                      *
************************************************************************
*                                                                      *
*     Statement functions
*
      TMax(i,j)=Work((j-1)*nSkal+i+ipTMax-1)
*                                                                      *
************************************************************************
*                                                                      *
      iRout = 9
      iPrint = nPrint(iRout)
      iFnc(1)=0
      iFnc(2)=0
      iFnc(3)=0
      iFnc(4)=0
      PMax=Zero
      idum=0
      idum1=0
CStart Molpro
c;      if(qtimes) Call QEnter('Drvg1')
CElse
      Call QEnter('Drvg1')
CEnd
      Call DCopy(nGrad,Zero,0,Temp,1)
*                                                                      *
************************************************************************
*                                                                      *
*-----Precompute k2 entities.
*
      Call Get_cArray('Relax Method',Method,8)                                     
      Indexation=.False.                                                                             
************ columbus interface ****************************************
* in order to access the half-sorted density matrix file 
* some index arrays must be generated
      If (Method.eq.'MR-CISD ') Indexation=.True.  
      DoFock=.False.
      DoGrad=.True.
      ThrAO=Zero
      Call SetUp_Ints(nSkal,Indexation,ThrAO,DoFock,DoGrad)
      mSkal=nSkal
      nPairs=nSkal*(nSkal+1)/2
      nQuad =nPairs*(nPairs+1)/2
      Pren = Zero
      Prem = Zero
*                                                                      *
************************************************************************
*                                                                      *
*-----Prepare handling of two-particle density.
*
CStart Molcas
      Call PrepP
CEnd
*                                                                      *
************************************************************************
*                                                                      *
      MxPrm = 0
      Do iAng = 0, iAngMx
         MxPrm = Max(MxPrm,MaxPrm(iAng))
      End Do
      nZeta = MxPrm * MxPrm
      nEta  = MxPrm * MxPrm
*
************************************************************************
*                                                                      *
*---  Compute entities for prescreening at shell level
*
      Call GetMem('TMax','Allo','Real',ipTMax,nSkal**2)
      Call Shell_MxSchwz(nSkal,Work(ipTMax))
      TMax_all=Zero
      Do iS = 1, nSkal
         Do jS = 1, iS
            TMax_all=Max(TMax_all,TMax(iS,jS))
         End Do
      End Do
*                                                                      *
************************************************************************
*                                                                      *
*     Create list of non-vanishing pairs
*
      Call GetMem('ip_ij','Allo','Inte',ip_ij,nSkal*(nSkal+1))
      nij=0
      Do iS = 1, nSkal
         Do jS = 1, iS
            If (TMax_All*TMax(iS,jS).ge.CutInt) Then
               nij = nij + 1
               iWork((nij-1)*2+ip_ij  )=iS
               iWork((nij-1)*2+ip_ij+1)=jS
            End If
         End Do
      End Do
      P_Eff=nij
*                                                                      *
************************************************************************
*                                                                      *
*-------Compute FLOP's for the transfer equation.
*
        Do iAng = 0, iAngMx
           Do jAng = 0, iAng
              nHrrab = 0
              Do i = 0, iAng+1
                 Do j = 0, jAng+1
                    If (i+j.le.iAng+jAng+1) Then
                       ijMax = Min(iAng,jAng)+1
                       nHrrab = nHrrab + ijMax*2+1
                    End If
                 End Do
              End Do
              nHrrTb(iAng,jAng,1)=nHrrab
              nHrrTb(jAng,iAng,1)=nHrrab
           End Do
        End Do
*                                                                      *
************************************************************************
*                                                                      *
CStart Molpro
c;      TCP1=0
CEnd
      Triangular=.True.
      Call Alloc_TList(Triangular,P_Eff)
      Call Init_TList(Triangular,P_Eff)
      Call Init_PPList
      Call Init_GTList
      iOpt=0
*                                                                      *
************************************************************************
*                                                                      *
*     In MPP case dispatch one processor to do 1-el gradients first
*
CStart Molpro
c;      If (MPP().and.node0()) Then
c;         Call Drvh1(Grad,Temp,nGrad,fock,fock1,d1ao,d1ao1)
CElse
      If (MPP().and.King()) Then
            Call Drvh1(Grad,Temp,nGrad)
CEnd
*        If (nPrint(1).ge.15)
*    &   Call PrGrad(' Gradient excluding two-electron contribution',
*    &               Grad,lDisp(0),lIrrep,ChDisp,5)
         Call DCopy(nGrad,Zero,0,Temp,1)
      End If
*                                                                      *
************************************************************************
*                                                                      *
      Call GetMem('MemMax','Max','Real',iDum,MemMax)
      Call GetMem('MemMax','Allo','Real',ipMem1,MemMax)
*                                                                      *
************************************************************************
*                                                                      *
*     big loop over individual tasks, distributed over individual nodes
   10 Continue
*     make reservation of a task on global task list and get task range
*     in return. Function will be false if no more tasks to execute.
      If (.Not.Rsv_GTList(TskLw,TskHi,iOpt,lDummy)) Go To 11
*
*     Now do a quadruple loop over shells
*
      ijS = Int((One+DSqrt(Eight*TskLw-Three))/Two)
      iS = iWork((ijS-1)*2+ip_ij)
      jS = iWork((ijS-1)*2+ip_ij+1)
      klS = Int(TskLw-DBLE(ijS)*(DBLE(ijS)-One)/Two)
      kS = iWork((klS-1)*2+ip_ij)
      lS = iWork((klS-1)*2+ip_ij+1)
      Count=TskLw
      Call CWTime(TCpu1,TWall1)
  13  Continue
         If (Count-TskHi.gt.1.0D-10) Go To 12
*
         Aint=TMax(iS,jS)*TMax(kS,lS)
         If (AInt.lt.CutInt) Go To 14
         If (iPrint.ge.15) Write (6,*) 'iS,jS,kS,lS=',iS,jS,kS,lS
*
************************************************************************
*                                                                      *
         Call Gen_iSD4(iS, jS, kS, lS,iWork(ipiSD),nSD,iSD4)
         Call Size_SO_block_g(iSD4,nSD,Petite,nSO,No_batch)
         If (No_batch) Go To 140
*
         Call Int_Prep_g(iSD4,nSD,Coor,Shijij,iAOV,iStabs)
*
*                                                                      *
************************************************************************
*                                                                      *
*       --------> Memory Managment <--------
*
*        Compute memory request for the primitives, i.e.
*        how much memory is needed up to the transfer
*        equation.
*
         Call MemRys_g(iSD4,nSD,nRys,MemPrm)
*                                                                      *
************************************************************************
*                                                                      *
         ABCDeq=EQ(Coor(1,1),Coor(1,2)) .and.
     &          EQ(Coor(1,1),Coor(1,3)) .and.
     &          EQ(Coor(1,1),Coor(1,4))
         ijklA=iSD4(1,1)+iSD4(1,2)
     &        +iSD4(1,3)+iSD4(1,4)
         If (nIrrep.eq.1.and.ABCDeq.and.Mod(ijklA,2).eq.1)
     &      Go To 140
*                                                                      *
************************************************************************
*                                                                      *
*        Decide on the partioning of the shells based on the
*        available memory and the requested memory.
*
*        Now check if all blocks can be computed and stored at
*        once.
*

         Call SOAO_g(iSD4,nSD,nSO,
     &               MemPrm, MemMax,
     &               nExp,nBasis,MxShll,
     &               iBsInc,jBsInc,kBsInc,lBsInc,
     &               iPrInc,jPrInc,kPrInc,lPrInc,
     &               ipMem1,ipMem2,ipMem3,ipMem4,ipMend,
     &                 Mem1,  Mem2,  Mem3,  Mem4,  Mend,
     &               iPrint,iFnc,
     &               MemScr,MemPSO)
         iBasi    = iSD4(3,1)
         jBasj    = iSD4(3,2)
         kBask    = iSD4(3,3)
         lBasl    = iSD4(3,4)
*                                                                      *
************************************************************************
*                                                                      *
         Call Int_Parm_g(iSD4,nSD,iAnga,
     &                 iCmpa,iShlla,iShela,
     &                 iPrimi,jPrimj,kPrimk,lPriml,
     &                 ipCffi,jpCffj,kpCffk,lpCffl,
     &                 nExp,ipExp,ipCff,MxShll,
     &                 indij,k2ij,nDCRR,k2kl,nDCRS,
     &                 mdci,mdcj,mdck,mdcl,AeqB,CeqD,
     &                 nZeta,nEta,ipZeta,ipZI,
     &                 ipP,ipEta,ipEI,ipQ,ipiZet,ipiEta,
     &                 ipxA,ipxB,ipxG,ipxD,l2DI,nab,nHmab,ncd,nHmcd,
     &                 nIrrep)
*                                                                      *
************************************************************************
*                                                                      *
*        Scramble arrays (follow angular index)
*
         Do iCar = 1, 3
            Do iSh = 1, 4
               JndGrd(iCar,iSh) = iSD4(15+iCar,iSh)
               If (iAnd(iSD4(15,iSh),2**(iCar-1)) .eq.
     &             2**(iCar-1)) Then
                  JfGrad(iCar,iSh) = .True.
               Else
                  JfGrad(iCar,iSh) = .False.
               End If
            End Do
         End Do
*
         Do 400 iBasAO = 1, iBasi, iBsInc
           iBasn=Min(iBsInc,iBasi-iBasAO+1)
           iAOst(1) = iBasAO-1
         Do 410 jBasAO = 1, jBasj, jBsInc
           jBasn=Min(jBsInc,jBasj-jBasAO+1)
           iAOst(2) = jBasAO-1
*
         Do 420 kBasAO = 1, kBask, kBsInc
           kBasn=Min(kBsInc,kBask-kBasAO+1)
           iAOst(3) = kBasAO-1
         Do 430 lBasAO = 1, lBasl, lBsInc
           lBasn=Min(lBsInc,lBasl-lBasAO+1)
           iAOst(4) = lBasAO-1
*
*----------Get the 2nd order density matrix in SO basis.
*
CStart Molpro
c;                    If(luhf.eq.0) then
c;                    inuhf=0
CEnd
           ipMem2 = ipMem1+Mem1
           nijkl = iBasn*jBasn*kBasn*lBasn
           Call PGet0(iCmpa,iShela,
     &                iBasn,jBasn,kBasn,lBasn,Shijij,
     &                iAOV,iAOst,nijkl,Work(ipMem1),nSO,
     &                iFnc(1)*iBasn,iFnc(2)*jBasn,
     &                iFnc(3)*kBasn,iFnc(4)*lBasn,MemPSO,
     &                ipMem2,iS,jS,kS,lS,nQuad,PMax)
            If (AInt*PMax.lt.CutInt) Go To 430
CStart Molpro
c;           Else
c;           ipMem2 = ipMem1+Mem1
c;           nijkl = iBasn*jBasn*kBasn*lBasn
c;           call dcopy(nSO*nijkl,Zero,0,Work(ipmem1),1)
c;           do iloop=1,4
c;             inuhf=iloop
c;             Call PGet0(iCmpa,iShela,
c;     &                  iBasn,jBasn,kBasn,lBasn,Shijij,
c;     &                  iAOV,iAOst,nijkl,Work(ipMem1),nSO,
c;     &                  iFnc(1)*iBasn,iFnc(2)*jBasn,
c;     &                  iFnc(3)*kBasn,iFnc(4)*lBasn,MemPSO,
c;     &                  ipMem2,iS,jS,kS,lS,nQuad,PMax)
c;           end do
c;*          If (AInt*PMax.lt.CutInt) Go To 430 !
c;           End If
CEnd
*
*----------Compute gradients of shell quadruplet
*
           ipMem3 = ipMem2 + Mem2
           ipMem4 = ipMem2 + Mem2 - Mem4
*
           Call TwoEl_g(Coor,
     &          iAnga,iCmpa,iShela,iShlla,iAOV,
     &          mdci,mdcj,mdck,mdcl,nRys,
     &          Work(k2ij),nab,nHmab,nDCRR,
     &          Work(k2kl),ncd,nHmcd,nDCRS,Pren,Prem,
     &          iPrimi,iPrInc,jPrimj,jPrInc,
     &          kPrimk,kPrInc,lPriml,lPrInc,
     &          Work(ipCffi+(iBasAO-1)*iPrimi),iBasn,
     &          Work(jpCffj+(jBasAO-1)*jPrimj),jBasn,
     &          Work(kpCffk+(kBasAO-1)*kPrimk),kBasn,
     &          Work(lpCffl+(lBasAO-1)*lPriml),lBasn,
     &  Work(ipZeta),Work(ipZI),Work(ipP),nZeta,
     &  Work(ipEta), Work(ipEI),Work(ipQ),nEta,
     &  Work(ipxA),Work(ipxB),Work(ipxG),Work(ipxD),Temp,nGrad,
     &       JfGrad,JndGrd,Work(ipMem1), nSO,Work(ipMem2),Mem2,
     &          Work(ipMem3),Mem3,Work(ipMem4),Mem4,
     &          Work(ipAux),nAux,Shijij)
*
*
            If (iPrint.ge.15)
     &         Call PrGrad(' In Drvg1: Grad',
     &                  Temp,nGrad,lIrrep,ChDisp,5)
*
 430     Continue
 420     Continue
*
 410     Continue
 400     Continue
*
 140     Continue
*
 14      Continue
         Count=Count+One
         klS = klS + 1
         If (klS.gt.ijS) Then
            ijS = ijS + 1
            klS = 1
         End If
         iS = iWork((ijS-1)*2+ip_ij  )
         jS = iWork((ijS-1)*2+ip_ij+1)
         kS = iWork((klS-1)*2+ip_ij  )
         lS = iWork((klS-1)*2+ip_ij+1)
         Go To 13
*
*     Task endpoint
 12   Continue
      Call CWTime(TCpu2,TWall2)
      Call SavTim(4,TCpu2-TCpu1,TWall2-Twall1)
      Call SavStat(1,One,'+')
      Call SavStat(2,TskHi-TskLw+One,'+')
      Go To 10
 11   Continue
*     End of big task loop
*                                                                      *
************************************************************************
*                                                                      *
#ifdef _MOLCAS_MPP_
      Call GADGOP(Temp,nGrad,'+')
#endif
*                                                                      *
************************************************************************
*                                                                      *
*                         E P I L O G U E                              *
*                                                                      *
************************************************************************
*                                                                      *
      Call GetMem('MemMax','Free','Real',ipMem1,MemMax)
      Call Free_GTList
      Call Free_PPList
      Call Free_TList
      Call GetMem('ip_ij','Free','Inte',ip_ij,nSkal*(nSkal+1))
      Call GetMem('TMax','Free','Real',ipTMax,nSkal**2)
*                                                                      *
************************************************************************
*                                                                      *
CStart Molcas
      Call CloseP
CEnd
      Verbose = .False.
      FreeK2=.True.
      Call Term_Ints(Verbose,FreeK2)
*                                                                      *
************************************************************************
*                                                                      *
CStart Molpro
c;      if (lprn(1)) then
CEnd
*
      Call Sync_Data(Pren,Prem,nBtch,mBtch,kBtch)
*
      iPren=3+Max(1,Int(Log10(Pren+0.001D+00)))
      iPrem=3+Max(1,Int(Log10(Prem+0.001D+00)))
      Write (Format,'(A,I2,A,I2,A)') '(A,F',iPren,
     &           '.0,A,F',iPrem,'.0,A)'
      If (iPrint.ge.6) Then
      Write (6,Format)
     &   ' A total of', Pren,' entities were prescreened and',
     &                  Prem,' were kept.'
      End If
*
CStart Molpro
c;      End If
CEnd
*                                                                      *
************************************************************************
*                                                                      *
CStart Molpro
c;      if(qtimes) Call QExit('Drvg1')
CElse
      Call QExit('Drvg1')
CEnd
      Return
      End
