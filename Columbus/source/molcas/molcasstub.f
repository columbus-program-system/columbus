!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*@ifdef molcas_ext
*        subroutine drvk2_(l1,l2,l3)
*        return
*        end
**
**
*        subroutine cmpctr_(l1,l2,l3)
*        return
*        end
**
*        subroutine cmpcts_(l1,l2,l3)
*        return
*        end
**
*        subroutine crapso_(l1,l2,l3)
*        return
*        end
*        subroutine dorys_(l3)
*        return
*        end
**
*        subroutine izero_(n,iarr,inc)
*          integer n, iarr(*),inc
*          call izero_wr(n,iarr,inc)
*        return
*        end subroutine izero_
**
*@else
        subroutine drvk2(l1,l2,l3)
        return
        end
*
*
        subroutine cmpctr(l1,l2,l3)
        return
        end
*
        subroutine cmpcts(l1,l2,l3)
        return
        end
*
        subroutine crapso(l1,l2,l3)
        return
        end
        subroutine dorys(l3)
        return
        end
        subroutine izero(n,iarr,inc)
          integer n, iarr(*),inc
          call izero_wr(n,iarr,inc)
        return
        end subroutine izero
*@endif
