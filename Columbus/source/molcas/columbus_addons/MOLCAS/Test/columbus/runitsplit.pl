#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use Shell qw( pwd ln );
$ENV{COLUMBUS}="/dist_software/Columbus_C70_beta/Columbus";
if ( $#ARGV <0 ) {die("usage ./runit.split  project name \n");}
$MPROJ=shift @ARGV;
$MPROJ=~s/\.input$//;
print "Project name=$MPROJ\n"; 
$CURRDIR=pwd(); chop $CURRDIR; $CURRDIR.= "/CTMP"; 
$ENV{WorkDir}="$CURRDIR/WORK";
$ENV{ThisDir}="$CURRDIR";
$ENV{Project}="$MPROJ";

if ( -d "$CURRDIR" ) { system("rm -rf $CURRDIR"); }
system("mkdir -p $CURRDIR/WORK"); 
chdir("$CURRDIR");

open FIN,"<../$MPROJ.input" || die ("could not find $MPROJ.input\n"); 
@project=<FIN>;
chomp @project;

# assume a structure like
#
#  >> SET MAXITER 10
#  seward
#  >> IF ( ITER = 1 ) THEN
#   scf,rasscf
#  >> ENDIF
#  columbus
#  slapaf 
#


foreach $i (@project) { if ($i=~/SET *MAXITER/i) { $i=~s/^.*MAXITER//i; s/ //g; $maxcounter=$i; 
                                              print "set maxcounter to $maxcounter\n"; last;}}

##  start main loop ###

$iter=0;

  while ($iter < $maxcounter)
  { $iter++;
    print "starting iteration cycle $iter in directory", pwd(),"\n"; 
    $input=extract_precol();
    print_input($input,"$ENV{Project}.input");

    system("molcas $ENV{Project}.input ");
    $ret=checkreturncode(); 
    if ($ret>0 ) {die("error occured in precol step: $ret\n");}
    $input=extract_col();
    chdir("$CURRDIR/WORK"); 
    print "preparing COLUMBUSINP in directory", pwd(),"\n"; 
    print_input($input,"COLUMBUSINP");
    if ( ! -l "RUNFILE" ){ ln ("-s","$MPROJ.RunFile","RUNFILE");}
    if ( ! -l "ORDINT"  ){ ln ("-s","$MPROJ.OrdInt","ORDINT");}
    if ( ! -l "ONEINT"  ){ ln ("-s","$MPROJ.OneInt","ONEINT");}
    if ( ! -l "ONEREL"  ){ ln ("-s","$MPROJ.OneRel","ONEREL");}
    system("/dist_software/SMOLCAS77/sbin/columbus.pl "); 
    chdir ($CURRDIR);
    $ret=checkreturncode(); 
    if ($ret >0 ) {die("error occured in col step: $ret\n");}
    $input=extract_postcol();
    print_input($input,"$ENV{Project}.input");
    system("molcas $ENV{Project}.input > slapafoutput");
    $ret=checkreturncode(); 
     open SLAPAF,"<slapafoutput"; 
     while (<SLAPAF>) {if (/Convergence not reached yet/) { $ret=96; last;}}
     system ("cat slapafoutput");
    if ($ret !=0 && $ret != 96 ) {die("error occured in postcol step:$ret:\n");}
    if ($ret == 0 ) {last;}
   }


 exit;

 sub extract_precol
 { my @temp;
   my @temp2;
   @temp=@project; 
   $section=1;
   while ($#temp > -1) 
    { $_ = shift @temp; 
      if (/>>/) {  if (/MAXITER/i) {next;}   #ignore
                   if (/>>* IF/i) {  s/^.*ITER = *//i; ($a,$b)=split(/\s+/,$_); 
                                   if ($a != $iter) { $section=0;next;}}
                   if (/>>* ENDIF/) {$section=1; next;}
                    next;}
      if (/\&COLUMBUS/) {$section=0;}
      if ($section) {push @temp2,$_;}
    }
#  print "precol: got ",join(':',@temp2),"\n"; 
   return \@temp2;
  } 


 
 sub extract_col
 { my @temp;
   my @temp2;
   @temp=@project;
   $section=0;
   while ($#temp > -1)
    { $_=shift @temp;
        if (/>>/) {  next;}  
      if (/\&COLUMBUS/) {$section=1;}
      if (/End of input/i) {$section=0;}
      if ($section) {push @temp2,$_;}
    }
#  print "col: got ",join(':',@temp2),"\n"; 
   return \@temp2;
  }

 sub extract_postcol
 { my @temp;
   my @temp2;
   @temp=@project;
   $section=0;
   while ($#temp > -1)
    { $_=shift @temp; 
       if (/>>/) {  next;}  
      if (/\&COLUMBUS/) {$section=1;}
      if (/End of input/i && $section == 1) {$section=2;next;}
      if ($section == 2 ) {push @temp2,$_;}
    }
#  print "postcol: got ",join(':',@temp2),"\n"; 
   return \@temp2;
  }


  sub checkreturncode
  { open FF,"<WORK/returncode";
    $ret=<FF>; chop $ret; close FF; $ret=~s/ //g; print "RETURNCODE: $ret \n"; return $ret;}


  sub print_input
   { ($inpdata,$filename)=@_;
     open FOUT,">$filename";
      while ($#$inpdata){ $_=shift @$inpdata; print FOUT  "$_ \n"; }
      close FOUT; return;}


