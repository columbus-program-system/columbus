\chapter{Introduction to COLUMBUS}
%\index{MOLCAS@\molcas!introduction}\index{Introduction}
\section{COLUMBUS, Quantum Chemistry Package}
\program{COLUMBUS} is a general purpose quantum-chemical program package 
specialized on generally applicable one and two-component multi-reference methods, 
in particular MCSCF, MR-SDCI, and MR-AQCC. The availability of general analytical 
gradients and the corresponding non-adiabatic coupling vectors for
one-component MCSCF, SA-MCSCF, MR-CISD and MR-AQCC calculations is 
an out-standing feature. The choice of MCSCF and MRCI reference
spaces is completely {\em independent} from each other and not restricted to a
particular form such as CAS or RAS type CSF spaces. 

\program{COLUMBUS} operates in its entiety within the framework of the GUGA
approach and, hence, in a basis of spin-adapted configuration state functions. 
This leads in particular to certain advantages for two-component MR-CISD 
calculations within the perturbational approach. Spin-orbit CI calculations
may be based on spin-orbit pseudo potentials[Dolg, Christiansen,Pitzer]
or on the (abinitio) DKH/AMFI[DKH,AMFI] approach. In combination with MOLCAS
only the DKH/AMFI approach is accessible because the MOLCAS integral code does
not support spin-orbit pseudo potentials.

With the growing number of correlated electrons, the size of the configuration
space increases rapidly and quickly reaches ${\cal O}(N^8)$ CSFs and more,
so that an efficient parallel implementation is essential. \program{COLUMBUS}
parallelization scheme is utilizing the Global Arrays Toolkit for dynamic
load balancing and one-sided communication and is capable of running MRCI
calculations with dimensions up to 3 billion CSFs with  - for today's standards
- modest ressource requirements (cf. performance issues). 

\section{The COLUMBUS/MOLCAS interface}

The philosphy behind the interface is to transparently exchange 
(transferable) data between MOLCAS and COLUMBUS without ever
converting these files between different proprietory formats. 
The implemented scheme is quite flexible in constructing a 
workflow which mixes COLUMBUS and MOLCAS modules. Originally,
the control was through the COLUMBUS main driver utility, which
has now been extended to the use of the molcas main driver control
in addition to an optional MOLCAS style input file. Since we do not
want to burden the user with the full flexibility of the COLUMBUS
input, we supply access to standard calculations through the MOLCAS
style input plus the optional generation and usage of a template
file for full control over most COLUMBUS specific options.
Expert users may modify this template file to access particular options
or specialized configuration spaces for both MCSCF and MRCI methods.

The basic layout of the interface is depicted in Figure 1. Here we
consider transferable data as those which are well-defined within
the framework of multi-reference methodology, i.e. integrals, 
density matrices, molecular orbitals, molecular geometries and 
gradients etc. In contrast non-transferable quantities are the
wavefunction itself. This is due to the quite arbitrary definition
of the wavefunction (CSFs or determinants, phases, implicit or explicit
ordering etc.) which would make the construction of a stable interface
very case and program system specific. Similarily, mixing of integrals
from different sources is not supported, since phases, symmetry-adaptation,
ordering, normalization may vary between different program packages, but
once this is fixed everything else is consistent. 


{\includegraphics[scale=0.77,angle=270,trim=0 150 100 100]{interoperability.ps}}

{\vspace{-2cm}}
{\bf Fig. 1} Flowchart for the interoperability between COLUMBUS, MOLCAS and DALTON modules

\section{Parallel performance issues}

This section of the documentation provides the background for the parallelization
and load-balancing schemes used in COLUMBUS [cf Chromiumpaper].
 

\subsection{Parallel MRCISD}
In the direct MRCI methodology, the lowest eigenvectors of the CI matrix are constructed
iteratively using a subspace expansion technique without ever explicitly constructing this huge matrix. 

Parallelization of the central matrix-vector product is straightforward by using a blocked
matrix-vector product which can be used to decompose the task of constructing the sigma
vector into an arbitrary number of independent partial matrix vector products at the expense
of increased number of contributions to the final sigma vector. 

\fbox{
\begin{minipage}{\hsize}
{\small
\setlength{\unitlength}{0.8cm}
\noindent\begin{picture}(15,6)
\multiput(0,0)(0,1.2){5}{\line(1,0){4.8}}
\multiput(0,0)(1.2,0){5}{\line(0,1){4.8}}

\put(0.6,4.2){\makebox(0,0){1,1}}
\put(1.8,4.2){\makebox(0,0){1,2}}
\put(3.0,4.2){\makebox(0,0){1,3}}
\put(4.2,4.2){\makebox(0,0){1,4}}

\put(0.6,3.0){\makebox(0,0){2,1}}
\put(1.8,3.0){\makebox(0,0){2,2}}
\put(3.0,3.0){\makebox(0,0){2,3}}
\put(4.2,3.0){\makebox(0,0){2,4}}

\put(0.6,1.8){\makebox(0,0){3,1}}
\put(1.8,1.8){\makebox(0,0){3,2}}
\put(3.0,1.8){\makebox(0,0){3,3}}
\put(4.2,1.8){\makebox(0,0){3,4}}

\put(0.6,0.6){\makebox(0,0){4,1}}
\put(1.8,0.6){\makebox(0,0){4,2}}
\put(3.0,0.6){\makebox(0,0){4,3}}
\put(4.2,0.6){\makebox(0,0){4,4}}


\multiput(5.5,0)(0,1.2){5}{\line(1,0){1.2}}
\multiput(5.5,0)(1.2,0){2}{\line(0,1){4.8}}

\put(6.1,4.2){\makebox(0,0){1}}
\put(6.1,3.0){\makebox(0,0){2}}
\put(6.1,1.8){\makebox(0,0){3}}
\put(6.1,0.6){\makebox(0,0){4}}


\put(7.9,2.4){\makebox(0,0){=}}


\multiput(9.0,0)(0,1.2){5}{\line(1,0){1.2}}
\multiput(9.0,0)(1.2,0){2}{\line(0,1){4.8}}


\put(9.6,4.2){\makebox(0,0){1}}
\put(9.6,3.0){\makebox(0,0){2}}
\put(9.6,1.8){\makebox(0,0){3}}
\put(9.6,0.6){\makebox(0,0){4}}


\put(11.6,4.2){\makebox(0,0)[l]{$H_{1,1}v_1+H_{1,2}v_2+H_{1,3}v_3+H_{1,4}v_4 = w_1$}}
\put(11.6,3.0){\makebox(0,0)[l]{$H_{2,1}v_1+H_{2,2}v_2+H_{2,3}v_3+H_{2,4}v_4 = w_2$}}
\put(11.6,1.8){\makebox(0,0)[l]{$H_{3,1}v_1+H_{3,2}v_2+H_{3,3}v_3+H_{3,4}v_4 = w_3$}}
\put(11.6,0.6){\makebox(0,0)[l]{$H_{4,1}v_1+H_{4,2}v_2+H_{4,3}v_3+H_{4,4}v_4 = w_4$}}

\end{picture}


 $$ \sum_{B}{\sum_{j\in B}{H_{ij}v_{j}}}= w_i  \hspace{1cm} \forall i \in A $$

 with $H_{ij}=H_{ji}$:\hspace{1cm} $ H_{AB} v_A  \longrightarrow w_B$  and $ H_{AB} v_B \longrightarrow_A$

 Strategy: \\[-0.5cm]
\begin{enumerate}
 \item  loop over all block pairs (A,B) and accumulate $w_A = w_A+ H_{A,B}v_B$ \\
 \item  loop over all block pairs ($A\ge B$)\\
      accumulate $w_A = w_A+ H_{A,B}v_B$ and $w_B= w_B+H_{A,B}v_A$ if $A\ne B$
\end{enumerate}
}
\end{minipage}
} 

Not all integrals contribute to all sections of the CI matrix and, hence, a task is 
a non-vanishing contribution to the CI matrix identified by a segment and integral
class id.  

\begin{center}
\noindent \parbox{6.5cm}{
 \begin{center}\tiny\bf (ij$\vert$kl), h$_{ij}$ \end{center}
\setlength{\unitlength}{0.55cm}
\psset{unit=0.55cm}
\begin{picture}(10,10)
\put(0,9.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.5,0.5)}
\put(0.5,8.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](1.0,1.0)}
\put(1.5,4.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](4.0,4.0)}
\put(5.5,0){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](4.5,4.5)}
\put(0,0){\line(0,1){10}}
\put(0,0){\line(1,0){10}}
\put(10,0){\line(0,1){10}}
\put(0,10){\line(1,0){10}}

\put(0.5,0){\line(0,1){10}}
\put(0,9.5){\line(1,0){10}}

\put(1.5,0){\line(0,1){10}}
\put(0,8.5){\line(1,0){10}}

\put(5.5,0){\line(0,1){10}}
\put(0,4.5){\line(1,0){10}}

\put(-0.5, 9.75){\makebox(0,0){\small\bf z}}
\put(-0.5, 9.00){\makebox(0,0){\small\bf y}}
\put(-0.5, 6.50){\makebox(0,0){\small\bf x}}
\put(-0.5, 2.25){\makebox(0,0){\small\bf w}}

\put(0.25,-0.5){\makebox(0,0){\small\bf z}}
\put(1.00,-0.5){\makebox(0,0){\small\bf y}}
\put(3.50, -0.5){\makebox(0,0){\small\bf x}}
\put(7.75,-0.5 ){\makebox(0,0){\small\bf w}}

\end{picture}
}
\parbox{6.5cm}{
 \begin{center}\tiny\bf (ij$\vert$ka) \end{center}
\setlength{\unitlength}{0.55cm}
\psset{unit=0.55cm}
\begin{picture}(10,10)
\put(0.0,8.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.5,1.0)}
\put(0.5,9.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](1.0,0.5)}
\put(0.5,0.0){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](1.0,8.5)}
\put(1.5,8.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](8.5,1.0)}
\put(0,0){\line(0,1){10}}
\put(0,0){\line(1,0){10}}
\put(10,0){\line(0,1){10}}
\put(0,10){\line(1,0){10}}

\put(0.5,0){\line(0,1){10}}
\put(0,9.5){\line(1,0){10}}

\put(1.5,0){\line(0,1){10}}
\put(0,8.5){\line(1,0){10}}

\put(5.5,0){\line(0,1){10}}
\put(0,4.5){\line(1,0){10}}

\put(-0.5, 9.75){\makebox(0,0){\small\bf z}}
\put(-0.5, 9.00){\makebox(0,0){\small\bf y}}
\put(-0.5, 6.50){\makebox(0,0){\small\bf x}}
\put(-0.5, 2.25){\makebox(0,0){\small\bf w}}

\put(0.25,-0.5){\makebox(0,0){\small\bf z}}
\put(1.00,-0.5){\makebox(0,0){\small\bf y}}
\put(3.50, -0.5){\makebox(0,0){\small\bf x}}
\put(7.75,-0.5 ){\makebox(0,0){\small\bf w}}

\end{picture}
}


\parbox{6.5cm}{
 \begin{center}\tiny\bf (ij$\vert$ab) \end{center}
\setlength{\unitlength}{0.55cm}
\psset{unit=0.55cm}
\begin{picture}(10,10)
\put(0.5,8.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](1.0,1.0)}
\put(1.5,0.0){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](8.5,8.5)}
\put(0.0,0.0){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.5,8.5)}
\put(1.5,9.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](8.5,0.5)}
\put(0,0){\line(0,1){10}}
\put(0,0){\line(1,0){10}}
\put(10,0){\line(0,1){10}}
\put(0,10){\line(1,0){10}}

\put(0.5,0){\line(0,1){10}}
\put(0,9.5){\line(1,0){10}}

\put(1.5,0){\line(0,1){10}}
\put(0,8.5){\line(1,0){10}}

\put(5.5,0){\line(0,1){10}}
\put(0,4.5){\line(1,0){10}}

\put(-0.5, 9.75){\makebox(0,0){\small\bf z}}
\put(-0.5, 9.00){\makebox(0,0){\small\bf y}}
\put(-0.5, 6.50){\makebox(0,0){\small\bf x}}
\put(-0.5, 2.25){\makebox(0,0){\small\bf w}}

\put(0.25,-0.5){\makebox(0,0){\small\bf z}}
\put(1.00,-0.5){\makebox(0,0){\small\bf y}}
\put(3.50, -0.5){\makebox(0,0){\small\bf x}}
\put(7.75,-0.5 ){\makebox(0,0){\small\bf w}}

%\put(1.5,8.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](8.5,1.0)}
\end{picture}
}
\noindent\parbox{6.5cm}{
 \begin{center}\tiny\bf (ia$\vert$bc) \end{center}
\setlength{\unitlength}{0.55cm}
\psset{unit=0.55cm}
\begin{picture}(10,10)
\put(0.5,0.0){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](1.0,8.5)}
\put(1.5,8.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](8.5,1.0)}
\put(0,0){\line(0,1){10}}
\put(0,0){\line(1,0){10}}
\put(10,0){\line(0,1){10}}
\put(0,10){\line(1,0){10}}

\put(0.5,0){\line(0,1){10}}
\put(0,9.5){\line(1,0){10}}

\put(1.5,0){\line(0,1){10}}
\put(0,8.5){\line(1,0){10}}

\put(5.5,0){\line(0,1){10}}
\put(0,4.5){\line(1,0){10}}

\put(-0.5, 9.75){\makebox(0,0){\small\bf z}}
\put(-0.5, 9.00){\makebox(0,0){\small\bf y}}
\put(-0.5, 6.50){\makebox(0,0){\small\bf x}}
\put(-0.5, 2.25){\makebox(0,0){\small\bf w}}

\put(0.25,-0.5){\makebox(0,0){\small\bf z}}
\put(1.00,-0.5){\makebox(0,0){\small\bf y}}
\put(3.50, -0.5){\makebox(0,0){\small\bf x}}
\put(7.75,-0.5 ){\makebox(0,0){\small\bf w}}

%\put(0.0,0.0){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.5,8.5)}
%\put(1.5,9.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](8.5,0.5)}
%\put(1.5,8.5){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](8.5,1.0)}
\end{picture}
}

\noindent\parbox{6.5cm}{
 \begin{center}\tiny\bf (ab$\vert$cd), h$_{ab}$ \end{center}
\setlength{\unitlength}{0.55cm}
\psset{unit=0.55cm}
\begin{picture}(10,10)

\put(0.,9.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(0.25,9.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(0.50,9.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(0.75,9.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(1.,  8.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(1.25,8.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(1.50,8.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(1.75,8.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(2.,  7.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(2.25,7.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(2.50,7.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(2.75,7.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(3.,  6.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(3.25,6.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(3.50,6.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(3.75,6.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(4.,  5.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(4.25,5.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(4.50,5.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(4.75,5.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(5.,  4.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(5.25,4.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(5.50,4.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(5.75,4.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(6.,  3.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(6.25,3.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(6.50,3.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(6.75,3.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(7.,  2.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(7.25,2.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(7.50,2.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(7.75,2.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(8.,  1.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(8.25,1.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(8.50,1.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(8.75,1.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

\put(9.,  0.75){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(9.25,0.50){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(9.50,0.25){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}
\put(9.75,0.00){\psframe[linewidth=0pt,fillstyle=solid,fillcolor=blue](0.25,0.25)}

 \put(0,0){\line(0,1){10}}
\put(0,0){\line(1,0){10}}
\put(10,0){\line(0,1){10}}
\put(0,10){\line(1,0){10}}

\put(0.5,0){\line(0,1){10}}
\put(0,9.5){\line(1,0){10}}

\put(1.5,0){\line(0,1){10}}
\put(0,8.5){\line(1,0){10}}

\put(5.5,0){\line(0,1){10}}
\put(0,4.5){\line(1,0){10}}

\put(-0.5, 9.75){\makebox(0,0){\small\bf z}}
\put(-0.5, 9.00){\makebox(0,0){\small\bf y}}
\put(-0.5, 6.50){\makebox(0,0){\small\bf x}}
\put(-0.5, 2.25){\makebox(0,0){\small\bf w}}

\put(0.25,-0.5){\makebox(0,0){\small\bf z}}
\put(1.00,-0.5){\makebox(0,0){\small\bf y}}
\put(3.50, -0.5){\makebox(0,0){\small\bf x}}
\put(7.75,-0.5 ){\makebox(0,0){\small\bf w}}

\end{picture}
}
\noindent\parbox{6.5cm}{
 \begin{center}\tiny\bf (aa$\vert$bb), h$_{bb}$ \end{center}
\setlength{\unitlength}{0.55cm}
\psset{unit=0.55cm}
\begin{picture}(10,10)
\put(0,0){\psline[linewidth=2pt,linecolor=blue](0,10)(10,0)}
\put(0,0){\line(0,1){10}}
\put(0,0){\line(1,0){10}}
\put(10,0){\line(0,1){10}}
\put(0,10){\line(1,0){10}}

\put(0.5,0){\line(0,1){10}}
\put(0,9.5){\line(1,0){10}}

\put(1.5,0){\line(0,1){10}}
\put(0,8.5){\line(1,0){10}}

\put(5.5,0){\line(0,1){10}}
\put(0,4.5){\line(1,0){10}}

\put(-0.5, 9.75){\makebox(0,0){\small\bf z}}
\put(-0.5, 9.00){\makebox(0,0){\small\bf y}}
\put(-0.5, 6.50){\makebox(0,0){\small\bf x}}
\put(-0.5, 2.25){\makebox(0,0){\small\bf w}}

\put(0.25,-0.5){\makebox(0,0){\small\bf z}}
\put(1.00,-0.5){\makebox(0,0){\small\bf y}}
\put(3.50, -0.5){\makebox(0,0){\small\bf x}}
\put(7.75,-0.5 ){\makebox(0,0){\small\bf w}}
\end{picture}
}
\end{center}

To this end choosing a segmentation of the CI and sigma vector such as to 
meet the local memory requirements is straightforward, however, load balancing
is usually not guaranteed. This is illustrated in the following pictures 
displaying the relative computational cost for a small portion of the CI
matrix. The enormous inhomogeneity of the work load prevents simple segmentation
schems from achieving good load balancing and thus good performance. Simply,
increasing the number of segments may worsen it. 

\begin{minipage}{\hsize}
\begin{center} {\bf Contributions of 0-external integrals} \end{center}
\newlength{\graphicsize}
\newlength{\graphicsizetwo}
\setlength{\graphicsize}{0.45\hsize}
\setlength{\graphicsizetwo}{0.65\hsize}
\addtolength{\graphicsize}{1.2mm}
\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\setlength{\unitlength}{1cm}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/zz0x.ps}}} \\
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf z }
\put(-0.7,2){\rotatebox{90}{\small\bf  z }}
\end{picture}
%\begin{center} 0-external zz \end{center}
}
\hspace{1cm}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/yy0x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf y }
\put(-0.7,2){\rotatebox{90}{\small\bf  y }}
\end{picture}
}

\vspace{1cm}

\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/xx0x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf x }
\put(-0.7,2){\rotatebox{90}{\small\bf  x }}
\end{picture}
}
\hspace{1cm}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\setlength{\unitlength}{1cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/ww0x.ps}}} \\
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf w }
\put(-0.7,2){\rotatebox{90}{\small\bf  w }}
\end{picture}
}

\end{minipage} 

\vspace{1cm}

\begin{minipage}{\hsize}
\begin{center} {\bf Contributions of 1-external integrals} \end{center}
\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\setlength{\unitlength}{1cm}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/yz1x.ps}}} \\
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf y }
\put(-0.7,2){\rotatebox{90}{\small\bf  z }}
\end{picture}
%\begin{center} 0-external zz \end{center}
}
\hspace{1cm}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/yw1x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf y }
\put(-0.7,2){\rotatebox{90}{\small\bf  w }}
\end{picture}
}

\vspace{1cm}

\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/yx1x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf y }
\put(-0.7,2){\rotatebox{90}{\small\bf  x }}
\end{picture}
}
\end{minipage}



\begin{minipage}{\hsize}
\begin{center} {\bf Contributions of 2-external integrals} \end{center}
\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\setlength{\unitlength}{1cm}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/yy2x.ps}}} \\
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf y }
\put(-0.7,2){\rotatebox{90}{\small\bf  x }}
\end{picture}
%\begin{center} 0-external zz \end{center}
}
\hspace{1cm}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/xx2x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf x }
\put(-0.7,2){\rotatebox{90}{\small\bf  x }}
\end{picture}
}

\vspace{1cm}

\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\setlength{\unitlength}{1cm}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/ww2x.ps}}} \\
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf w }
\put(-0.7,2){\rotatebox{90}{\small\bf  w }}
\end{picture}
%\begin{center} 0-external zz \end{center}
}
\hspace{1cm}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/wz2x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf w }
\put(-0.7,2){\rotatebox{90}{\small\bf  z }}
\end{picture}
}

\vspace{1cm}

\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\setlength{\unitlength}{1cm}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/xz2x.ps}}} \\
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf x }
\put(-0.7,2){\rotatebox{90}{\small\bf  z }}
\end{picture}
%\begin{center} 0-external zz \end{center}
}
\hspace{1cm}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/wx2x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf w }
\put(-0.7,2){\rotatebox{90}{\small\bf  x }}
\end{picture}
}

\end{minipage}

\vspace{1.5cm}
\input{legende}

\newpage 

\begin{minipage}{\hsize}
\begin{center} {\bf Contributions of 3-external integrals} \end{center}
\setlength{\fboxsep}{0pt}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\setlength{\unitlength}{1cm}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/yw3x.ps}}} \\
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf y }
\put(-0.7,2){\rotatebox{90}{\small\bf  w }}
\end{picture}
%\begin{center} 0-external zz \end{center}
}
\hspace{1cm}
\parbox{0.42\hsize}{
\setlength{\graphicsize}{\hsize}
\psset{unit=1.00cm}
\fbox{\parbox{\hsize}{\hspace{0.01mm}\includegraphics[width=\graphicsize]{colorprint/yx3x.ps}}} \\
\setlength{\unitlength}{1cm}
\begin{picture}(0.1,0.1)(0,0)
\put( -0.2,0.5){\psline[linewidth=2pt]{->}(0,0)(0,3.5)}
\put(0,-0.1){\psline[linewidth=2pt]{->}(0,0)(6.5,0)}
\put(3,-0.5){\small\bf y }
\put(-0.7,2){\rotatebox{90}{\small\bf  x }}
\end{picture}
}
\end{minipage}

\vspace{1.5cm}

Symmetric segement pairs have only the upper diagonal half displayed and the four-external integrals
contribute uniformly to single segment only - predominantly to x and w. 

Since the actual load distribution patterns depend on both hardware characteristics (relative performance
for scalar, BLAS1, BLAS2, BLAS3 operations) and the particular structure of the configurations space, 
a performance model is used to estimate the load distribution based on the configuration space structure 
of the specific case and  hardware characteristics must be taken into account by using the timing statistics
of the parallel MRCI code for the same or a similar case. This is a boot strapping problem.











