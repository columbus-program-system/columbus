!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program getrunfileinfo
c
c     extracts information from the RUNFILE            
c
      implicit logical(a-z)
       integer,parameter :: nmax=4096
       integer,parameter :: nmaxatoms=100
       
*@ifdef MOLCAS_INT64
       integer*8 mlen,mscalar, marray(nmax),nsym,nbas(8)
       integer*8 uniqueatoms,ncenter,oz(nmaxatoms)
       integer*8 nbft,cindex(nmax),otype(nmax),nushell
       integer*8 syminfo(12,nmax),irrcmp(nmax),iAOtSO(8,nmax)
       integer*8 iao,koff(8),kao(8),myao2sao(8,nmax),sao2ao(8,nmax)
       integer*8 iunique,inunique, equivatoms(nmaxatoms)
       integer*8 tmp(nmaxatoms),ndegen(nmax)
*@else
*       integer*4 mlen,mscalar,marray(nmax)
*@endif
        real*8 allcoords(3,nmaxatoms) 
        real*8 ucoords(3,nmaxatoms) 
        character*8 carray(nmax),basislbl(nmaxatoms)
        character*4 marker,atomlabel(nmaxatoms) 
        integer symdegen(nmaxatoms),shelldegen(nmaxatoms),
     .          ucenter(nmaxatoms),ushells(2,nmaxatoms)
        integer ibfn,ishell,n,ntmp,i,j,k,l,iaosave,newiao 
        integer shelltobfn(nmax),order(nmax),invorder(nmax)
        real*8 d 
        character*80 line

c
c 
c alle MOLCAS Variablen, die in die MOLCAS Bibliothek
c f�hren sind mit mc_ bezeichnet.

      call link_it()
      call getenvinit()
      call FIOInit()
c     call Fastio('TRACE=ON')
      call NameRun('RUNFILE')
c
      open(unit=6,file='runinfols',form='formatted')
      write(6,*) '############### ISCALAR #######################'
      call getiscalarinfo
      write(6,*) '############### DSCALAR #######################'
      call getdscalarinfo
      write(6,*) '############### IARRAY  #######################'
      call getiarrayinfo
      write(6,*) '############### CARRAY  #######################'
      call getcarrayinfo
      write(6,*) '############### DARRAY  #######################'
      call getdarrayinfo

      write(6,*) '************* Collected Final Info ************'
      call get_iscalar('Unique atoms',uniqueatoms)
      call get_iscalar('LP_nCenter',ncenter)
      call get_darray('LP_Coor',allcoords,ncenter*3)
      call get_iarray('LP_A',oz,ncenter)
      call get_carray('LP_L',carray,ncenter*8)
      call get_darray('Unique Coordinates',ucoords,uniqueatoms*3)
      do i=1,ncenter
        ucenter(i)=-1
        do j=1,uniqueatoms
          d=  (allcoords(1,i)-ucoords(1,j))**2  
     .                 + (allcoords(2,i)-ucoords(2,j))**2  
     .                  + (allcoords(3,i)-ucoords(3,j))**2  
          d=sqrt(d)
          if (abs(d).lt.1.d-8) then 
             if (ucenter(i).gt.0) stop 'error'
             ucenter(i)=j
          endif
       enddo
      enddo  

       do i=2,ncenter
          if (ucenter(i).gt.0) tmp(ucenter(i)-1)=i-1
       enddo
          tmp(uniqueatoms)=ncenter
       equivatoms(1)=tmp(1)
       do i=2,uniqueatoms
          equivatoms(i)=tmp(i)-tmp(i-1)
       enddo 
       write(6,*) 'Coordinates of unique centers (x,y,z' //
     .  ' degeneracy'
       do i=1,uniqueatoms
         write(6,49) ucoords(1:3,i),equivatoms(i)
  49   format(3f15.7,3x,i4) 
       enddo 
        

      write(6,*) 'Coordinates of all centers  (x,y,z, atomic number' //
     .    ' atom label  unique center)'
      do i=1,ncenter
       atomlabel(i)=carray(i)(1:4)
       write(6,50) allcoords(1:3,i),oz(i),atomlabel(i),ucenter(i)
      enddo
 50   format(3f15.7,3x, i4,3x,a4,i4)

       

      call get_iscalar('nSym',nsym)
      call get_iarray('nbas',nbas,nsym)
      nbft=0
      do i=1,nsym
        nbft=nbft+nbas(i)
      enddo
      call get_iarray('Center Index',cindex,nbft)
c   was ist otype valence, polarization etc. see legend in SCF output
c     call get_iarray('Orbital Type',otype,nbft)
      call get_carray('Unique Basis Nam',basislbl,nbft*8)

       cartesian=.true.
       k=0
       do i=1,nsym 
       do j=1,nbas(i)
         k=k+1
         write(6,51)  k, basislbl(k)
         if (basislbl(k)(7:7).eq.'0') cartesian=.false.
 51    format('SAO#',i4,2x,'label=',a8)
       enddo
       enddo
       if (cartesian) then 
       write(6,*) 'Cartesian basis set '
       else
       write(6,*) 'Spherical basis set '
       endif


       iunique=0
       inunique=0
       do i=1,nbft
        if (ucenter(cindex(i)).gt.0) then
          ndegen(i)=equivatoms(ucenter(cindex(i)))
          write(6,'(a,i4,a,i4,a)') 'Basisfunktion ',i,
     .    ' UNIQUE (',ndegen(i),')'
          iunique=iunique+1
        else
          ndegen(i)=-1
          write(6,'(a,i4,a)') 'Basisfunktion ',i,' NOT UNIQUE'
          inunique=inunique+1
        endif
       enddo
       write(6,'(a,i4,a,i4,a)') 'found ', iunique, 'UNIQUE and ',
     .   inunique, 'NON-UNIQUE basis functions'


       write(6,*) 'unique basis functions ordered by unique center'
       ishell=0
       ibfn=0
       order=0
       do i=1,uniqueatoms
        do j=1,nbft
          if (ucenter(cindex(j)).eq.i) then
            ishell=ishell+1
c SAO ordering
            shelltobfn(ishell)=ibfn+1
c order(unique-center-ordered) = CAO input order 
            do k=1,ndegen(j)
              order(k+ibfn)=j+k-1
            enddo
            write(6,'(a,i4,a,i4,a,i4)') 'CAO#',j,'(',ndegen(j),
     .       ') shell#', ishell
            ibfn=ibfn+ndegen(j)
          endif
        enddo
       enddo
            
       write(6,*) ' map vector CAO to UNIQUE CAO '
       write(6,*) '      # UNIQUE CAO         #  CAO '
       do i=1,nbft
          invorder(order(i))=i
          write(6,'(i8,3x,i8)') i,order(i)
       enddo


       nushell=1
       call get_iarray('IrrCmp',irrcmp,nushell)
       call get_iarray('IrrCmp',irrcmp,nushell)
       call get_iarray('iAOtSO',iAOtSO,nushell*8)
       do i=1,nushell
        write(6,'("iaotso #",i4,8(i8,2x))') i,iaotso(1:8,i)
       enddo 

       symdegen=0
       do i=1,nushell
         do j=1,8 
          if (iaotso(j,i).gt.0) symdegen(i)=symdegen(i)+1
         enddo
       enddo
       shelldegen=0
       do j=1,8
        do i=1,nushell
          if (iaotso(j,i).gt.0) then
               ntmp=iaotso(j,i)
               do k=i+1,nushell
                if (iaotso(j,k).gt.0) then
                   shelldegen(i)=iaotso(j,k)-ntmp
                   exit
                endif
               enddo
               if (shelldegen(i).eq.0) 
     .             shelldegen(i)=nbas(j)+1-ntmp
          endif
         enddo
        enddo 
     
       ushells(1,1)=1
       ushells(2,1)=ushells(1,1)+shelldegen(1)-1
       do i=2,nushell
         ushells(1,i)=ushells(2,i-1)+1
         ushells(2,i)=ushells(1,i)+shelldegen(i)-1
       enddo 

       do i=1,nushell
       write(6,'(a,i4,a,i4,a,i4,a,2i4)') 
     .  'iaotso #',i,' symdegen=',symdegen(i),
     .  ' shelldegen=',shelldegen(i),' range=',ushells(1:2,i) 
       enddo 
       myao2sao=0
       kao(1:8)=0 
       koff(1)=0
       do i=2,nsym
         koff(i)=koff(i-1)+nbas(i-1)
       enddo 
       iao=0 
       newiao=0
c  order = run over all unique atoms
c             run over all unique shells


         do i=1,nushell
           do j=ushells(1,i),ushells(2,i)
            iao=shelltobfn(j)
c           iao=invorder(iao)
            iao=order(iao)
            write(6,'(a,i4,a,i4,a,i4,a,i4,a,i4,a,i4)') 
     .   'unique shell=',i,'shell=',j,'bfn=',shelltobfn(j),
     .   'invbfn=',iao, 'symdegen=',symdegen(i),' ndegen=',ndegen(iao)
            do k=1,8
             if (iaotso(k,i).gt.0) then 
                if (ndegen(iao).ne.symdegen(i)) then
                   write(6,*) 'iao,i=',iao,i,'ndegen,symdegen=',
     .                  ndegen(iao),symdegen(i)
                 endif 
                do l=1,symdegen(i)
               myao2sao(k,iao+l-1)=iaotso(k,i)+koff(k)+j-ushells(1,i)
                enddo
             endif
            enddo 
           enddo
         enddo

       write(6,*) 'CAO (initial ordering) contributes to SAOs '
       write(6,110) (i,i=1,8)
 110   format(' iCAO    ',8('----',i1,'----',2x))

       do i=1,nbft  
        write(6,'(i4,4x,8(i8,2x))') i,myao2sao(1:8,i)
       enddo 
       sao2ao=0
       tmp=0
       do i=1,nbft
        do j=1,8
          k=myao2sao(j,i)
          if (k.gt.0) then 
             tmp(k)=tmp(k)+1
             sao2ao(tmp(k),k)=i 
          endif
        enddo
       enddo

       write(6,*) 'SAO composed of  iCAOs '                         
       write(6,*) ' #SAO     ------  iCAO  ------- '
       do i=1,nbft  
        write(6,'(i4,4x,8(i8,2x))') i,sao2ao(1:8,i)
       enddo 
   
       open(file='molcas.SymInfo',unit=12,form='formatted')
c      skip first two header lines
       read(12,*)
       read(12,*)
       do i=1,nbft
       read(12,'(a)') line
       read(line,*) k,syminfo(1:4,i)
       k=syminfo(4,i)
       read(line,*) tmp(1:5),syminfo(5:4+k,i)
       enddo
       close(12)

       write(6,*) 'SAO composed of  iCAOs (phase =sign of bfn number)'
       write(6,*) '#SAO  irrep  bfn  ndegen     contributions ' 
       iao=0
       do i=1,nsym
        do j=1,nbas(i)
        iao=iao+1
        k=syminfo(4,iao)
         do l=1,k
          syminfo(4+l,iao)=syminfo(4+l,iao)*sao2ao(l,iao)
         enddo
        write(6,112) iao,i,j, k,syminfo(5:4+k,iao)
 112    format(4(i4,2x),3x,8(i4,2x))
        enddo
       enddo
      close(6)

6040  format('nsym:',i6,' nmot:',i6)
6050  format('symmetry:',8(3x,a3))
6060  format('AO_basis:',8i6)
6061  format('frozen  :',8i6)
6062  format('deleted :',8i6)
6063  format('MO_basis:',8i6)

      stop
      end

      subroutine getiscalarinfo
      implicit none
*@ifdef MOLCAS_INT64 
       integer*8 mscalar
*@else
*       integer*4 mscalar
*@endif
      integer, parameter:: nlabels=54
      integer i
      character*40   Reclab(nlabels) 
      logical valid(nlabels)

      RecLab(  1)='Multiplicity'
      RecLab(  2)='nMEP'
      RecLab(  3)='No of Internal coordinates'
      RecLab(  4)='nSym'
      RecLab(  5)='PCM info length'
      RecLab(  6)='Relax CASSCF root'
      RecLab(  7)='System BitSwitch'
      RecLab(  8)='Unique atoms'
      RecLab(  9)='LP_nCenter'
      RecLab( 10)='ChoIni'
      RecLab( 11)='Unit Cell NAtoms'
      RecLab( 12)='Cholesky Reorder'
      RecLab( 13)='ChoVec Address'
      RecLab( 14)='SA ready'
      RecLab( 15)='NumGradRoot'
      RecLab( 16)='Number of roots'
      RecLab( 17)='LoProp Restart'
      RecLab( 18)='MpProp nOcOb'
      RecLab( 19)='Highest Mltpl'
      RecLab( 20)='nActel'
      RecLab( 21)='Run_Mode'
      RecLab( 22)='Grad ready'
      RecLab( 23)='ISPIN'
      RecLab( 24)='SCF mode'
      RecLab( 25)='MkNemo.nMole'
      RecLab( 26)='N ZMAT'
      RecLab( 27)='Bfn Atoms'
      RecLab( 28)='FMM'
      RecLab( 29)='Pseudo atoms'
      RecLab( 30)='nChDisp'
      RecLab( 31)='iOff_Iter'
      RecLab( 32)='Columbus'
      RecLab( 33)='ColGradMode'
      RecLab( 34)='IRC'
         RecLab( 35)='MaxHops'
         RecLab( 36)='nRasHole'
         RecLab( 37)='nRasElec'
         RecLab( 38)='Rotational Symmetry Number'
         RecLab( 39)='Saddle Iter'
         RecLab( 40)='iMass'
         RecLab( 41)='mp2prpt' !True(=1) if mbpt2 was run with prpt
         RecLab( 42)='NJOB_SINGLE'
         RecLab( 43)='MXJOB_SINGLE'
         RecLab( 44)='NSS_SINGLE'
         RecLab( 45)='NSTATE_SINGLE'
         RecLab( 46)='LDF Status' ! Initialized or not
         RecLab( 47)='DF Mode' ! Local (1) or non-local (0) DF
         RecLab( 48)='agrad' ! Forces analytical gradients
         RecLab( 49)='LDF Constraint' ! Constraint type for LDF
         RecLab( 50)='OptimType' !Optimization type in hyper
         RecLab( 51)='LSYM' ! symmetry of the CAS root(s)
         RecLab( 52)='RF CASSCF root'
         RecLab( 53)='RF0CASSCF root'
         RecLab( 54)='nCoordFiles' ! number of xyz-files in gateway



      do i=1,nlabels
        mscalar=-9999
        call get_iscalar(Reclab(i)(1:16),mscalar)
        if (mscalar.eq.-9999) then 
           write(6,'(a,a)') Reclab(i), ' UNDEFINED ' 
        else
           write(6,'(a,i4)') Reclab(i),mscalar
        endif
      enddo
      return
      end


      subroutine getdscalarinfo
      implicit none
      real*8 dscalar
      integer, parameter:: nlabels=29
      integer i
      character*40   Reclab(nlabels)
         RecLab(  1)='CASDFT energy'
         RecLab(  2)='CASPT2 energy'
         RecLab(  3)='CASSCF energy'
         RecLab(  4)='Ener_ab'
         RecLab(  5)='KSDFT energy'
         RecLab(  6)='Last energy'
         RecLab(  7)='PC Self Energy'
         RecLab(  8)='PotNuc'
         RecLab(  9)='RF Self Energy'
         RecLab( 10)='SCF energy'
         RecLab( 11)='Thrs'
         RecLab( 12)='UHF energy'
         RecLab( 13)='E_0_NN'
         RecLab( 14)='W_or_el'
         RecLab( 15)='W_or_Inf'
         RecLab( 16)='EThr'
         RecLab( 17)='Cholesky Threshold'
         RecLab( 18)='Total Nuclear Charge'
         RecLab( 19)='Numerical Gradient rDelta'
         RecLab( 20)='MpProp Energy'
         RecLab( 21)='UHFSPIN'
         RecLab( 22)='S delete thr'
         RecLab( 23)='T delete thr'
         RecLab( 24)='MD_Etot0'
         RecLab( 25)='MD_Time'
         RecLab( 26)='LDF Accuracy'
         RecLab( 27)='NAD dft energy'
         RecLab( 28)='GradLim'
         RecLab( 29)='StepFactor'



      do i=1,nlabels
        dscalar=-9999.999d0
        call get_dscalar(Reclab(i)(1:16),dscalar)
        if (dscalar.eq.-9999.999d0) then
           write(6,'(a,a)') Reclab(i), ' UNDEFINED '
        else
           write(6,'(a,e18.6)') Reclab(i),dscalar
        endif
      enddo

      return
      end

      subroutine getiarrayinfo
      implicit none
       integer,parameter :: nmax=4096 
*@ifdef MOLCAS_INT64
       integer*8 mlen,mscalar, marray(nmax)
*@else
*       integer*4 mlen,mscalar,marray(nmax)
*@endif
      integer, parameter:: nlabels=75
      integer i
      character*40   Reclab(nlabels)
      logical valid(nlabels)

         RecLab(  1)='Center Index'
         RecLab(  2)='nAsh'
         RecLab(  3)='nBas'
         RecLab(  4)='nDel'
         RecLab(  5)='nFro'
         RecLab(  6)='nIsh'
         RecLab(  7)='nIsh beta'
         RecLab(  8)='nOrb'
         RecLab(  9)='Orbital Type'
         RecLab( 10)='Slapaf Info 1'
         RecLab( 11)='Symmetry operations'
         RecLab( 12)='nIsh_ab'
         RecLab( 13)='nStab'
         RecLab( 14)='Quad_c'
         RecLab( 15)='Quad_i'
         RecLab( 16)='RFcInfo'
         RecLab( 17)='RFiInfo'
         RecLab( 18)='RFlInfo'
         RecLab( 19)='SCFInfoI'
         RecLab( 20)='SewCInfo'
         RecLab( 21)='SewIInfo'
         RecLab( 22)='SewLInfo'
         RecLab( 23)='SCFInfoI_ab'
         RecLab( 24)='nExp'
         RecLab( 25)='nBasis'
         RecLab( 26)='ipCff'
         RecLab( 27)='ipExp'
         RecLab( 28)='IndS'
         RecLab( 29)='ip_Occ'
         RecLab( 30)='ipAkl'
         RecLab( 31)='ipBk'
         RecLab( 32)='nOpt'
         RecLab( 33)='Prjct'
         RecLab( 34)='Transf'
         RecLab( 35)='iCoSet'
         RecLab( 36)='LP_A'
         RecLab( 37)='NumCho' ! Number of Cholesky vectors.
         RecLab( 38)='nFroPT' ! Total Number of Frozen for PT methods
         RecLab( 39)='nDelPT' ! Total Number of Deleted for PT methods
         RecLab( 40)='BasType'
         RecLab( 41)='Spread of Coord.'
         RecLab( 42)='Unit Cell Atoms'
         RecLab( 43)='iSOShl'
         RecLab( 44)='Non valence orbitals'
         RecLab( 45)='LoProp nInts'
         RecLab( 46)='LoProp iSyLbl'
         RecLab( 47)='nDel_go'
         RecLab( 48)='nBas_Prim'
         RecLab( 49)='IsMM'
         RecLab( 50)='Atom -> Basis'
         RecLab( 51)='nBasis_Cntrct'
         RecLab( 52)='ipCff_Cntrct'
         RecLab( 53)='ipCff_Prim'
         RecLab( 54)='SCF nOcc'
         RecLab( 55)='SCF nOcc_ab'
         RecLab( 56)='ipFockOp'
         RecLab( 57)='IrrCmp'
         RecLab( 58)='iAOtSO'
         RecLab( 59)='iSOInf'
         RecLab( 60)='FragShell'
         RecLab( 61)='AuxShell'
         RecLab( 62)='nVec_RI'
         RecLab( 63)='MkNemo.hDisp'
         RecLab( 64)='Index ZMAT'
         RecLab( 65)='NAT ZMAT'
         RecLab( 66)='NrDisp'
         RecLab( 67)='nDisp'
         RecLab( 68)='DegDisp'
         RecLab( 69)='LBList'
         RecLab( 71)='Ctr Index Prim'
         RecLab( 72)='MLTP_SINGLE'
         RecLab( 73)='JBNUM_SINGLE'
         RecLab( 74)='LROOT_SINGLE'
         RecLab( 75)='GeoInfo'


      do i=1,nlabels

        mlen=1
        call get_iarray(Reclab(i)(1:16),marray,mlen)
        if (mlen.gt.nmax) then
          write(6,'(a,i8,a)') Reclab(i), mlen, 'too large'
        elseif (mlen.lt.0) then
          write(6,'(a,a)') Reclab(i), 'UNDEFINED'
        else
          call get_iarray(Reclab(i)(1:16),marray,mlen)
          write(6,'(a,i8)') Reclab(i), mlen
          write(6,'(5i12)') marray(1:mlen)
        endif
      enddo


        return
        end


      subroutine getcarrayinfo
      implicit none
       integer,parameter :: nmax=4096
*@ifdef MOLCAS_INT64
       integer*8 mlen,mscalar
*@else
*       integer*4 mlen,mscalar
*@endif
      character*8 carray(nmax)
      integer, parameter:: nlabels=15
      integer i
      character*40   Reclab(nlabels)
      logical valid(nlabels)

        RecLab(  1)='DFT functional  '
        RecLab(  2)='Irreps  '
        RecLab(  3)='Relax Method    '
        RecLab(  4)='Seward Title    '
        RecLab(  5)='Slapaf Info 3   '
c  char*4
        RecLab(  6)='Unique Atom Name'
c  char*8
        RecLab(  7)='Unique Basis Nam'
c  loprop char*8
        RecLab(  8)='LP_L    '
        RecLab(  9)='MkNemo.lMole    '
        RecLab( 10)='MkNemo.lCluster '
        RecLab( 11)='MkNemo.lEnergy  '
        RecLab( 12)='Symbol ZMAT     '
        RecLab( 13)='Tinker Name     '
        RecLab( 14)='ESPF Filename   '
        RecLab( 15)='ChDisp  '

      do i=1,nlabels
        carray(1:nmax)='        '
        carray(1)='XXXXXXXX'
        mlen=8
        call get_carray(Reclab(i)(1:16),carray,mlen)
        if (mlen.gt.nmax*8) then 
          write(6,'(a,i8,a)') Reclab(i), mlen, 'too large'
        elseif (mlen.lt.0) then 
          write(6,'(a,a)') Reclab(i), 'UNDEFINED'
        else
          call get_carray(Reclab(i)(1:16),carray,mlen)
          write(6,'(a,i8)') Reclab(i), mlen
          write(6,'(5(a8,":"))') carray(1:mlen/8)
        endif
      enddo


      return
      end

      subroutine getdarrayinfo
      implicit none
       integer,parameter :: nmax=4096
*@ifdef MOLCAS_INT64
       integer*8 mlen,mscalar
*@else
*       integer*4 mlen,mscalar
*@endif
      double precision darray(nmax)
      integer, parameter:: nlabels=105
      integer i
      character*40   Reclab(nlabels)



         RecLab(  1)='Analytic Hessian'
         RecLab(  2)='Center of Charge'
         RecLab(  3)='Center of Mass'
         RecLab(  4)='CMO_ab'
         RecLab(  5)='D1ao'
         RecLab(  6)='D1ao_ab'
         RecLab(  7)='D1aoVar'
         RecLab(  8)='D1av'
         RecLab(  9)='D1mo'
         RecLab( 10)='D1sao'
         RecLab( 11)='D2av'
         RecLab( 12)='dExcdRa'
         RecLab( 13)='DLAO'
         RecLab( 14)='DLMO'
         RecLab( 15)='Effective nuclear Charge'
         RecLab( 16)='FockO_ab'
         RecLab( 17)='FockOcc'
         RecLab( 18)='GeoNew'
         RecLab( 19)='GeoNewPC'
         RecLab( 20)='GRAD'
         RecLab( 21)='Hess'
         RecLab( 22)='HF-forces'
         RecLab( 23)='Last orbitals'
         RecLab( 24)='LCMO'
         RecLab( 25)='MEP-Coor    '
         RecLab( 26)='MEP-Energies'
         RecLab( 27)='MEP-Grad    '
         RecLab( 28)='MP2 restart'
         RecLab( 29)='Mulliken Charge'
         RecLab( 30)='NEMO TPC'
         RecLab( 31)='Nuclear charge'
         RecLab( 32)='OrbE'
         RecLab( 33)='OrbE_ab'
         RecLab( 34)='P2MO'
         RecLab( 35)='PCM Charges'
         RecLab( 36)='PCM Info'
         RecLab( 37)='PLMO'
         RecLab( 38)='RASSCF orbitals'
         RecLab( 39)='Reaction field'
         RecLab( 40)='SCFInfoR'
         RecLab( 41)='SCF orbitals'
         RecLab( 42)='Slapaf Info 2'
         RecLab( 43)='Unique Coordinates'
         RecLab( 44)='Vxc_ref'
         RecLab( 45)='PotNuc00'
         RecLab( 46)='h1_raw'
         RecLab( 47)='h1    XX'
         RecLab( 48)='HEFF'
         RecLab( 49)='PotNucXX'
         RecLab( 50)='Quad_r'
         RecLab( 51)='RCTFLD'
         RecLab( 52)='RFrInfo'
         RecLab( 53)='SewRInfo'
         RecLab( 54)='SewTInfo'
         RecLab( 55)='SewXInfo'
         RecLab( 56)='Last orbitals_ab'
         RecLab( 57)='SCFInfoI_ab'
         RecLab( 58)='SCFInfoR_ab'
         RecLab( 59)='Transverse'
         RecLab( 60)='SM'
         RecLab( 61)='LP_Coor'
         RecLab( 62)='LP_Q'
         RecLab( 63)='DFT_TwoEl'
         RecLab( 64)='Unit Cell Vector'
         RecLab( 65)='SCF orbitals_ab'
         RecLab( 66)='Guessorb'
         RecLab( 67)='Guessorb energies'
         RecLab( 68)='Last energies'
         RecLab( 69)='LoProp Dens 0'
         RecLab( 70)='LoProp Dens 1'
         RecLab( 71)='LoProp Dens 2'
         RecLab( 72)='LoProp Dens 3'
         RecLab( 73)='LoProp Dens 4'
         RecLab( 74)='LoProp Dens 5'
         RecLab( 75)='LoProp Dens 6'
         RecLab( 76)='LoProp Integrals'
         RecLab( 77)='MpProp Orb Ener'
         RecLab( 78)='LoProp H0'
         RecLab( 79)='Dipole moment'
         RecLab( 80)='GeoPC'
         RecLab( 81)='BMtrx'
         RecLab( 82)='CList'
         RecLab( 83)='DList'
         RecLab( 84)='RMax_Shll'
         RecLab( 85)='MkNemo.vDisp'
         RecLab( 86)='MkNemo.tqCluster'
         RecLab( 87)='MkNemo.Energies'
         RecLab( 88)='MMHessian'
         RecLab( 89)='Bfn Coordinates'
         RecLab( 90)='Pseudo Coordinates'
         RecLab( 91)='Pseudo Charge'
         RecLab( 92)='RASSCF OrbE'
         RecLab( 93)='Ref_Geom'
         RecLab( 94)='LoProp Charge'
         RecLab( 95)='Initial Coordinates'
         RecLab( 96)='Grad State1'
         RecLab( 97)='Grad State2'
         RecLab( 98)='NADC'
         RecLab( 99)='MR-CISD energy'
         RecLab(100)='Saddle'
         RecLab(101)='Reaction Vector'
         RecLab(102)='IRC-Coor    '
         RecLab(103)='IRC-Energies'
         RecLab(104)='IRC-Grad    '
         RecLab(105)='MM Grad'
         RecLab(106)='Velocities'
         RecLab(107)='FC-Matrix'
         RecLab(108)='umass'
         RecLab(109)='ESO_SINGLE'
         RecLab(110)='UMATR_SINGLE'
         RecLab(111)='UMATI_SINGLE'
         RecLab(112)='ANGM_SINGLE'
         RecLab(113)='TanVec'
         RecLab(114)='Nuc Potential'
         RecLab(115)='RF CASSCF Vector'



        do i=1,nlabels
        mlen=1
        call get_darray(Reclab(i)(1:16),darray,mlen)
        if (mlen.gt.nmax) then
          write(6,'(a,i8,a)') Reclab(i), mlen, 'too large'
        elseif (mlen.lt.0) then
          write(6,'(a,a)') Reclab(i), 'UNDEFINED'
        else
          call get_darray(Reclab(i)(1:16),darray,mlen)
          write(6,'(a,i8)') Reclab(i), mlen
          write(6,'(5(e14.6))') darray(1:mlen)
        endif
        enddo
        return
        end

