!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c deck lcb4x2
      subroutine lcb4x2(
     & incnt,  numin,  num,    pints,
     & wxints, dabuf,  idabuf, cntw3,
     & cntw12, cntx3,  cntx12, ksym,
     & l1s,    l1f,    k1s,    k1f,
     & nepsy,  ioffs,  maxbl4, sqrt2 )
c
c  4-external integral combinations for ksym=lsym.
c
c  input:
c  incnt = integral offset for pints(*).
c  numin = total number of 4-external ints remaining to be processed.
c  num = number of 4-external ints in the current pints(*) segment.
c  pints(*) = holds a segment of integrals in triple sort order.
c  dabuf(*),idabuf(*) = output buffers.
c  cntw3,cntw12,cntx3,cntx12 = number of integral combinations to be
c                              formed of various types.
c  ksym,l1s,l1f,k1s,k1f = indexing ranges.
c  nepsy(*) = number of external orbitals of each symmetry.
c  ioffs = number of working precision words required for block headers.
c  maxbl4 = space allowed for integral combinations.
c  sqrt2 = sqrt(2.).
c
c  output:
c  wxints(*) = w- and x-type integral combinations and packed headers.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
c     # dummy:
      integer incnt, numin, num, cntw3, cntw12, cntx3, cntx12, ksym,
     & l1s, l1f, k1s, k1f, ioffs, maxbl4
      real*8 pints(nipsg), wxints(*), dabuf(*), sqrt2
      integer idabuf(*), nepsy(8)
c
c     # local:
      integer strw12, strtw3, strx12, strtx3, strtwf, strtxf, stfw12,
     & stx12, stx3, stxf, stfx12, l, k1strt, k1fin, k, jsym,
     & nj, j, iind, i, iindw, iindx, jm
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      strw12 = ioffs
      strtw3 = strw12+cntw12
      strx12 = strtw3+cntw3+ioffs
      strtx3 = strx12+cntx12
      strtwf = strx12-ioffs
      strtxf = strtx3+cntx3
      stfw12 = strw12
c
c     # strtwf = total number of + combinations(w paths)
c     # stxf   = total number of - combinations(x paths)
c     # strw12 = starting address for 1a,b and 2a,b w comb.
c     # stx12  = starting address for 1a,b and 2a,b x comb.
c     # strtw3 = starting address for 3a,b w comb.
c     # stx3   = starting address for 3a,b x comb.
c

      call code4x(
     & wxints, strtwf, strw12, strtw3,
     & 0,      ksym,   ksym,   l1s,
     & l1f,    k1s,    k1f,    0 )
      stx12 = ioffs
      stx3  = stx12+cntx12
      stxf  = stx3+cntx3
      call code4x(
     & wxints( strx12-2 ), stxf,   stx12,  stx3,
     & 0,                  ksym,   ksym,   l1s,
     & l1f,                k1s,    k1f,    0 )
      stfx12 = strx12
c
      if(strtxf.gt.maxbl4)then
         write(nlist,20000)strtxf,maxbl4
         call bummer('lcb4x2: (strtxf-maxbl4)=',(strtxf-maxbl4),faterr)
      endif
c
      do 100 l = l1s,l1f
         k1strt=1
         if(l.eq.l1s)k1strt=k1s
         k1fin=l
         if(l.eq.l1f)k1fin=k1f
c
         do 110 k=k1strt,k1fin
            if(k.eq.l)go to 300
c
c           # isym = jsym < ksym = lsym
c
            do 120 jsym=1,(ksym-1)
               nj=nepsy(jsym)
               if(nj.eq.0)go to 120
               do 130 j=1,nj
                  iind=j
                  do 140 i=1,j
c
                     if(incnt+3.gt.num)then
                        numin = numin - num
                        num   = min( numin, nipsg )
                        incnt = 0
                        call rdbks( pints, dabuf, idabuf )
                     endif
c                    # 1a,b 4a,b w,x
c                    # element(j,i)
                     wxints(strw12+i)=pints(incnt+1)+pints(incnt+3)
                     wxints(strx12+i)=pints(incnt+1)-pints(incnt+3)
c
                     if(i.ne.j)then
c                       # 2a,b w,x
c                       # element (i,j)
                        wxints(stfw12+iind) =
     &                   pints(incnt+1)+pints(incnt+2)
                        wxints(stfx12+iind) =
     &                   pints(incnt+1)-pints(incnt+2)
c                       # 3a,b w,x
                        wxints(strtw3+i)=pints(incnt+2)+pints(incnt+3)
                        wxints(strtx3+i)=pints(incnt+2)-pints(incnt+3)
                     else
c                       # 5 w
                        wxints(strtw3+i)=pints(incnt+2)*sqrt2
                     endif
                     iind=iind+nj
                     incnt=incnt+3
c
c                    # end if i loop.
140               continue
c
                  strw12=strw12+nj
                  strx12=strx12+nj
                  strtw3=strtw3+j
                  strtx3=strtx3+j-1
c
c                 # end of j loop.
130            continue
               stfw12=stfw12+nj*nj
               stfx12=stfx12+nj*nj
c
c              # end of jsym loop.
120         continue
c
c           # lsym = ksym = jsym = isym
c
            if(k.eq.1)go to 110
c
            do 210 j=1,k
               iindw=j
               iindx=j
c
               if(j.ne.k)then
c
                  do 220 i=1,j
c
                     if(incnt+3.gt.num)then
                        numin = numin - num
                        num   = min( numin, nipsg )
                        incnt = 0
                        call rdbks( pints, dabuf, idabuf )
                     endif
c                    # 1a,b 4a,b w,x
c                    # element (j,i)
                     wxints(strw12+i)=pints(incnt+1)+pints(incnt+3)
                     wxints(strx12+i)=pints(incnt+1)-pints(incnt+3)
c
                     if(i.ne.j)then
c                       # 2a,b w,x
c                       # element (i,j)
                        wxints(stfw12+iindw) = pints(incnt+1)
     &                   +                     pints(incnt+2)
                        wxints(stfx12+iindx) = pints(incnt+1)
     &                   -                     pints(incnt+2)
c                       # 3a,b w,x
                        wxints(strtw3+i) = pints(incnt+2)+pints(incnt+3)
                        wxints(strtx3+i) = pints(incnt+2)-pints(incnt+3)
                     else
c                       # 5 w
                        wxints(strtw3+i) = pints(incnt+2) * sqrt2
                     endif
                     iindw=iindw+k
                     iindx=iindx+k-1
                     incnt=incnt+3
c
c                    # end of i loop.
220               continue
c
                  strw12=strw12+k
                  strx12=strx12+k-1
                  strtw3=strtw3+j
                  strtx3=strtx3+j-1
c
               else
c
c                 # j = k case.
c
                  jm=j-1
                  do 260 i=1,jm
c
                     if(incnt+3.gt.num)then
                        numin = numin - num
                        num   = min( numin, nipsg )
                        incnt = 0
                        call rdbks( pints, dabuf, idabuf )
                     endif
c                    # 6a,b w,x
                     wxints(strtw3+i)=pints(incnt+1)+pints(incnt+2)
                     wxints(strtx3+i)=-pints(incnt+1)+pints(incnt+2)
c                    # 7 w
                     wxints(stfw12+iindw)=pints(incnt+2)*sqrt2
                     iindw=iindw+k
                     incnt=incnt+3
c
c                    # end of i loop.
260               continue
c
                  strtw3=strtw3+jm
                  strtx3=strtx3+jm
c
               endif
c
c              # end of j loop.
210         continue
c
            stfw12=strw12
            stfx12=strx12
            go to 110
c
300         continue
c
c           # k = l
c           # isym = jsym < ksym = lsym
c
            do 320 jsym=1,(ksym-1)
               nj=nepsy(jsym)
               if(nj.eq.0)go to 320
               do 330 j=1,nj
                  do 340 i=1,j
c
                     if(incnt+3.gt.num)then
                        numin = numin - num
                        num   = min( numin, nipsg )
                        incnt = 0
                        call rdbks( pints, dabuf, idabuf )
                     endif
c
                     if(i.ne.j)then
c                       # 8a,b w,x
                        wxints(strw12+i)=pints(incnt+1)+pints(incnt+2)
                        wxints(strx12+i)=pints(incnt+1)-pints(incnt+2)
c                       # 10 w
                        wxints(strtw3+i)=pints(incnt+2)*sqrt2
                     else
c                       # 13 w
                        wxints(strtw3+i)=pints(incnt+2)
                     endif
                     incnt=incnt+3
c
c                    # end of i loop.
340               continue
c
                  strw12=strw12+j-1
                  strx12=strx12+j-1
                  strtw3=strtw3+j
c
c                 # end of j loop.
330            continue
c
               stfw12=strw12
               stfx12=strx12
c
c              # end of jsym loop.
320         continue
c
c           # isym = jsym = ksym = lsym
c
            if(k.eq.1)go to 110
            do 410 j=1,k
               if(j.eq.k)go to 440
               do 420 i=1,j
c
                  if(incnt+3.gt.num)then
                     numin = numin - num
                     num   = min( numin, nipsg )
                     incnt = 0
                     call rdbks( pints, dabuf, idabuf )
                  endif
c
                  if(i.ne.j)then
c                    # 8 a,b w,x
                     wxints(strw12+i)=pints(incnt+1)+pints(incnt+2)
                     wxints(strx12+i)=pints(incnt+1)-pints(incnt+2)
c                    # 10 w
                     wxints(strtw3+i)=pints(incnt+2)*sqrt2
                  else
c                    # 13 w
                     wxints(strtw3+i)=pints(incnt+2)
                  endif
                  incnt=incnt+3
c
c                 # end of i loop.
420            continue
c
               strw12=strw12+j-1
               strx12=strx12+j-1
               strtw3=strtw3+j
               go to 410
c
c              # j = k
c
440            continue
               jm=j-1
               do 450 i=1,jm
c
                  if(incnt+3.gt.num)then
                     numin = numin - num
                     num   = min( numin, nipsg )
                     incnt = 0
                     call rdbks( pints, dabuf, idabuf )
                  endif
c                 # 12a w
                  wxints(strw12+i)=pints(incnt+1)*sqrt2
c                 # 12b w
                  wxints(strtw3+i)=pints(incnt+2)*sqrt2
                  incnt=incnt+3
c
c                 # end of i loop.
450            continue
c
               strw12=strw12+jm
               strtw3=strtw3+jm
c
c              # end of j loop.
410         continue
c
            stfw12=strw12
            stfx12=strx12
c
c           # end of k loop.
110      continue
c
c        # end of l loop.
100   continue
c
      strw12 = strw12 - ioffs
      strtw3 = strtw3 - ioffs
      strx12 = strx12 - ioffs * 2
      strtx3 = strtx3 - ioffs * 2
      strtxf = strtxf - ioffs * 2
c
      if(strw12.ne.cntw12 .or. strtw3.ne.cntw12+cntw3
     & .or. strx12.ne.cntw12+cntw3+cntx12 .or. strtx3.ne.strtxf)then
         write(nlist,20010)cntw12,cntw3,cntx12,cntx3,strw12,strtw3,
     &    strx12,strtx3,strtxf
         call bummer('lcb4x2: strw12, strtw3, strx12, strtx3 error',
     &    0,faterr)
      endif
c
      return
20000 format(' error in lcb4x2'/' space requested',i10,
     & ' space available',i10)
20010 format(' error in lcb4x2'/' cntw12,cntw3,cntx12,cntx3',4i6,
     & /,' strw12,strtw3,strx12,strtx3,strtxf',5i6)
      end
c deck prep3e
      subroutine prep3e(
     & pints,  wxints, bufw,   bufx,
     & dabuf,  idabuf, nmsym,  nepsy,
     & maxbl3, ilevsm, niot,   maxbuf,
     & incnt,  numin,  num )
c
c  determine the block structure of the 3-external integrals and write
c  the blocks to the appropriate file.  the integrals are first read
c  into a segment buffer in triple-sort order into the array pints(*).
c  linear combinations of these integrals are formed and placed into
c  the array wxints(*).  the combinations are then moved into the
c  buffers bufw(*) and bufx(*) and written out as they are filled.
c  each combination block consists of all possible i and j indices
c  consistent with the parameters ksym, kstart, and kfinal.  these
c  parameters are written at the beginning of the block into two
c  working precision words.  the array wxints(*) must hold at least one
c  set of ij indices and should hold several so as to minimize the two-
c  word overhead and the subsequent processing overhead in the
c  diagonalization program.
c  blocks are not split between buffers.  the expected wasted space in
c  each buffer is therefore half of the average block size.  see the
c  put*34() routines for more details of the da buffer processing.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer        mult
      common /cmult/ mult(8,8)
c
      integer         srtopt
      common /option/ srtopt(5)
c
c     # dummy:
      integer nmsym, maxbl3, niot, maxbuf, incnt, numin, num
      real*8 pints(*),wxints(maxbl3),sqrt2
      real*8 dabuf(*),bufw(maxbuf),bufx(maxbuf)
      integer idabuf(*),nepsy(8),ilevsm(niot)
c
c     # local:
      integer ioffs, mx3int, iblock, i, k1strt, finbl, numout,
     & icontw, icontx, ictodw, ictodx, n3ext, cntx12, cntw3, cntw12,
     & cntx3, cntwx, cw3od, cw12od, cx12od, cx3od, cwxod, l1,
     & ls, lsym, ksym, nk, ks, kls, k1, symchk, jsym, nj, js,
     & is, isym, ni, nij, nl, kfinod, icow, icox
      integer sym12(8),sym21(8)
c
      real*8 two
      parameter(two=2d0)
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
c  ioffs = number of words in each block to reserve for block
c          parameters.
c  mx3int = maximum number of w-type or x-type integral combinations
c           that can be placed in wxints(*).
c
      ioffs  = 2
      mx3int = maxbl3/2-ioffs
c
      iblock = 0
c
      do 1 i = 1, 8
         sym12(i) = i
         sym21(i) = i
1     continue
      sqrt2 = sqrt(two)
      k1strt=1
      finbl=0
      numout=0
      icontw=0
      icontx=0
      iblock=0
      ictodw=0
      ictodx=0
      n3ext=numin-incnt
      if(srtopt(1).ge.1)write(nlist,21010)
21010 format(' block structure of 3-ext. integrals'//
     & '    lsym      l   ksym kstart kfinal  #intw  #intx'//)
      cntx12=0
      cntw3=0
      cntw12=0
      cntx3=0
      cntwx=0
      cw3od=0
      cw12od=0
      cx12od=0
      cx3od=0
      cwxod=0
c
      do 10 l1=1,niot
c
         ls=ilevsm(l1)
         lsym=sym21(ls)
c
         do 20 ksym=1,nmsym
c
            nk=nepsy(ksym)
            if(nk.eq.0)go to 20
            ks=sym12(ksym)
            if(ksym.eq.lsym)go to 110
            if(ksym.eq.1)go to 20
            kls=mult(ks,ls)
c
            do 30 k1=1,nk
c
               symchk=0
c
               do 40 jsym=1,(ksym-1)
c
                  nj=nepsy(jsym)
                  if(nj.eq.0)go to 40
                  js=sym12(jsym)
                  is=mult(js,kls)
                  isym=sym21(is)
                  if(isym.ge.jsym)go to 40
c
c                 # isym < jsym < ksym
c
                  ni=nepsy(isym)
                  if(ni.eq.0)go to 40
                  nij=ni*nj
                  cntwx=cntwx+nij
                  icontw=icontw+3*nij
                  icontx=icontx+3*nij
                  symchk=1
c
c                 # end of jsym loop.
40             continue
c
               if(ksym.le.lsym)go to 50
c
c              # isym = lsym , jsym = ksym
c
               nl=nepsy(lsym)
               if(nl.eq.0)go to 50
               symchk=1
c
c              # 2*k1*nl + (k1-1)*nl
c
               icontw=icontw+nl*(3*k1-1)
c
c              # k1*nl + 2*(k1-1)*nl
c
               icontx=icontx+nl*(3*k1-2)
c
50             continue
               if(symchk.eq.0)go to 20
               if(finbl.ne.0)then
                  finbl=0
                  k1strt=k1
               endif
               if(icontw-mx3int) 60,70,80
60             continue
               if(icontx-mx3int) 65,70,80
65             continue
c
c              # both w-block and x-block fit into the internal buffer.
c              # save the valid block parameters.
c
               kfinod=k1
               ictodw=icontw
               ictodx=icontx
               cwxod=cntwx
               go to 30
c
c              # both w-block and x-block fit.  one block (w-blocks
c              # are larger than x-blocks) fits exactly so go ahead
c              # and form combinations and move to the output buffers.
c
70             continue
               iblock=iblock+1
               call lcb3x1(
     &          incnt,  numin,  num,    pints,
     &          wxints, dabuf,  idabuf, cntwx,
     &          icontw, icontx, lsym,   l1,
     &          ksym,   kls,    k1strt, k1,
     &          nepsy,  sym12,  sym21,  ioffs,
     &          sqrt2 )
               if(srtopt(1).ge.1) write(nlist,9010)
     &          lsym,l1,ksym,k1strt,k1,
     &          icontw,icontx
9010           format(1x,20i7)
c
               icow=icontw
               icox=icontx
c
               call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
               cntwx=0
               ictodw=0
               ictodx=0
               numout=numout+icontw+icontx
               icontw=0
               icontx=0
               cntwx=0
               finbl=1
               kfinod=-999999
               go to 30
c
c              # at least one block is too large to fit into the
c              # remaining internal buffer space.  form combinations
c              # for the last valid block parameters and move to the
c              # output buffers.
c
80             iblock=iblock+1
               call lcb3x1(
     &          incnt,  numin,  num,    pints,
     &          wxints, dabuf,  idabuf, cwxod,
     &          ictodw, ictodx, lsym,   l1,
     &          ksym,   kls,    k1strt, kfinod,
     &          nepsy,  sym12,  sym21,  ioffs,
     &          sqrt2  )
               if(srtopt(1).ge.1)write(nlist,9020)
     &          lsym,l1,ksym,k1strt,kfinod,
     &          ictodw,ictodx
9020           format(1x,20i7)
c
               icow=ictodw
               icox=ictodx
c
               call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
               numout=numout+ictodw+ictodx
               kfinod=k1
               k1strt=k1
               ictodw=icontw-ictodw
               ictodx=icontx-ictodx
               icontw=ictodw
               icontx=ictodx
               cntwx=cntwx-cwxod
               cwxod=cntwx
c
c              # end of k1 loop.
30          continue
c
            if(ictodw.eq.0.and.ictodx.eq.0)go to 36
c
c           # process the last block for this ksym.
c
            iblock=iblock+1
            call lcb3x1(
     &       incnt,  numin,  num,    pints,
     &       wxints, dabuf,  idabuf, cntwx,
     &       icontw, icontx, lsym,   l1,
     &       ksym,   kls,    k1strt, nk,
     &       nepsy,  sym12,  sym21,  ioffs,
     &       sqrt2  )
            if(srtopt(1).ge.1)write(nlist,9030)
     &       lsym,l1,ksym,k1strt,nk,ictodw,
     &       ictodx
9030        format(1x,20i7)
c
            icow=ictodw
            icox=ictodx
c
            call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
            numout=numout+ictodw+ictodx
c
36          continue
            icontw=0
            icontx=0
            ictodw=0
            ictodx=0
            cntwx=0
            cwxod=0
            finbl=1
            go to 20
c
110         continue
c
c           # ksym = lsym cases:
c
            do 120 k1=1,nk
c
               do 140 jsym=1,(ksym-1)
c
c                 # isym = jsym < ksym = lsym
c
                  nj=nepsy(jsym)
                  if(nj.eq.0)go to 140
                  cntw12=cntw12+nj*nj
                  cntx12=cntx12+nj*nj
                  cntw3=cntw3+nj*(nj+1)/2
                  cntx3=cntx3+nj*(nj-1)/2
140            continue
c
c              # isym = jsym = ksym = lsym
c
               cntw3=cntw3+k1*(k1+1)/2-1
               cntx3=cntx3+k1*(k1-1)/2
               cntw12=cntw12+(k1-1)*k1
               cntx12=cntx12+(k1-1)*(k1-1)
               icontw=cntw12+cntw3
               icontx=cntx12+cntx3
               if(finbl.ne.0)then
                  finbl=0
                  k1strt=k1
               endif
               if(icontw-mx3int) 160,170,180
160            continue
               if(icontx-mx3int) 161,170,180
161            continue
c
c              # both w-block and x-block fit into the internal buffer.
c              # save the valid block parameters.
c
               kfinod=k1
               ictodw=icontw
               ictodx=icontx
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               go to 120
c
c              # both w-block and x-block fit.  one block (w-blocks are
c              # larger than x-blocks) bits exactly so go ahead and
c              # form combinations and move to output buffers.
c
170            iblock=iblock+1
               call lcb3x2(
     &          incnt,  numin,  num,    pints,
     &          wxints, dabuf,  idabuf, cntw3,
     &          cntw12, cntx3,  cntx12, maxbl3,
     &          l1,     ksym,   k1strt, k1,
     &          nepsy,  ioffs,  sqrt2  )
               if(srtopt(1).ge.1) write(nlist,9060)
     &          lsym,l1,ksym,k1strt,k1,
     &          icontw,icontx
9060           format(1x,20i7)
c
               icow=icontw
               icox=icontx
c
               call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
               numout=numout+icontw+icontx
               finbl=1
               icontw=0
               icontx=0
               ictodw=0
               ictodx=0
               kfinod=-999999
               cntw12=0
               cntw3=0
               cntx12=0
               cntx3=0
               cw12od=-999999
               cw3od=-999999
               cx12od=-999999
               cx3od=-999999
               go to 120
c
c              # at least one block is too large to fit into the
c              # remaining internal buffer space.  form combinations
c              # for the last valid block parameters and move to the
c              # output buffers.
c
180            continue
               if(cw3od.le.0)then
                  write(nlist,21110)cw3od
                  call bummer('prep3e: cw3od=',cw3od,faterr)
               endif
c
               iblock=iblock+1
               call lcb3x2(
     &          incnt,  numin,  num,    pints,
     &          wxints, dabuf,  idabuf, cw3od,
     &          cw12od, cx3od,  cx12od, maxbl3,
     &          l1,     ksym,   k1strt, kfinod,
     &          nepsy,  ioffs,  sqrt2  )
               if(srtopt(1).ge.1)write(nlist,9070)
     &          lsym,l1,ksym,k1strt,kfinod,
     &          ictodw,ictodx
9070           format(1x,20i7)
c
               icow=ictodw
               icox=ictodx
c
               call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
               numout=numout+ictodw+ictodx
               kfinod=k1
               k1strt=k1
               ictodw=icontw-ictodw
               ictodx=icontx-ictodx
               cntw12=cntw12-cw12od
               cntw3=cntw3-cw3od
               cntx12=cntx12-cx12od
               cntx3=cntx3-cx3od
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               icontw=0
               icontx=0
c
c              # end of k1 loop.
120         continue
c
            if(ictodw.eq.0 .and. ictodx.eq.0) go to 20
c
c           # process the last block for this ksym.
c
            iblock=iblock+1
            call lcb3x2(
     &       incnt,  numin,  num,    pints,
     &       wxints, dabuf,  idabuf, cntw3,
     &       cntw12, cntx3,  cntx12, maxbl3,
     &       l1,     ksym,   k1strt, nk,
     &       nepsy,  ioffs,  sqrt2  )
            if(srtopt(1).ge.1)write(nlist,9130)
     &       lsym,l1,ksym,k1strt,nk,ictodw,
     &       ictodx
9130        format(1x,20i7)
c
            icow=ictodw
            icox=ictodx
c
            call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
            numout=numout+ictodw+ictodx
            finbl=1
            icontw=0
            icontx=0
            ictodw=0
            ictodx=0
            kfinod=-999999
            cntw12=0
            cntw3=0
            cntx12=0
            cntx3=0
            cw12od=-999999
            cw3od=-999999
            cx12od=-999999
            cx3od=-999999
c
c           # end of ksym loop.
20       continue
c
c        # end of l1 loop.
10    continue
c
      if(ictodw.ne.0 .or. ictodx.ne.0)then
         write(nlist,22000) ictodw,ictodx
         call bummer('prep3e: ictodw, ictodx error',0,faterr)
      endif
c
c     # process the final block.
c
      icow=ictodw
      icox=ictodx
c
      call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
c     # dump the last w- and x-buffers.
c
      call putd34( bufw, bufx )
c
      if(numin.ne.incnt)then
         write(nlist,21120)numin,incnt
         call bummer('prep3e: (numin-incnt)=',(numin-incnt),faterr)
      endif
c
c     # all done with 3-external integrals.
c
      write(nlist,21020)iblock
      write(nlist,20090)numout,n3ext
c
      return
21120 format(' *** error *** prep3e: incorrect number of integrals',
     & ' processed. numin,incnt=',2i8)
22000 format(' error in prep3e, ictodw,ictodx',2i6)
21110 format(' error in prep3e,statement label 210,cw3od=',i10)
21020 format(' prep3e:',i6,' blocks of linear combinations of',
     & ' 3-external integrals processed.')
20090 format(' number of sorted 3-external integrals  ',i9,/,
     & ' number of original 3-external integrals',i9/)
      end
c deck lcb3x1
      subroutine lcb3x1(
     & incnt,  numin,  num,    pints,
     & wxints, dabuf,  idabuf, cntwx,
     & icontw, icontx, lsym,   l1,
     & ksym,   kls,    k1s,    k1f,
     & nepsy,  sym12,  sym21,  ioffs,
     & sqrt2  )
c
c  3-external linear combinations for ksym .ne. lsym.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      integer        mult
      common /cmult/ mult(8,8)
c
c     # dummy:
      integer incnt, numin, num, cntwx, icontw, icontx, lsym, l1,
     & ksym, kls, k1s, k1f, ioffs
      real*8 pints(nipsg),wxints(*),dabuf(*),sqrt2
      integer idabuf(*),nepsy(8),sym12(8),sym21(8)
c
c     # local:
      integer nl, summ, sump, icntw1, icntw2, icntw3, icntx1, icntx2,
     & icntx3, icntf, isvw1, isvw2, isvw3, isvx1, isvx2, isvx3, k1,
     & jsym, nj, js, is, isym, ni, j1, i1, sumw, sumx, cntwx1, iscr
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      nl = nepsy(lsym)
      if ( ksym .gt. lsym ) then
         summ = ( (k1f + k1s - 2) * (k1f - k1s + 1) * nl ) / 2
         sump = ( (k1f + k1s) * (k1f - k1s + 1) * nl) / 2
      else
         summ = 0
         sump = 0
      endif
      icntw1 = ioffs
      icntw2 = icntw1 + cntwx + summ
      icntw3 = icntw2 + cntwx + sump
      icntx1 = icntw3 + ioffs + cntwx + sump
      icntx2 = icntx1 + cntwx + summ
      icntx3 = icntx2 + cntwx + summ
      icntf  = icntx3 + cntwx + sump
      isvw1  = icntw1
      isvw2  = icntw2
      isvw3  = icntw3
      isvx1  = icntx1
      isvx2  = icntx2
      isvx3  = icntx3
c
      if ( icntf .ne. (icontw + icontx + 2 * ioffs) )then
         write(nlist,9010)  icntw1, icntw2, icntw3, icntx1, icntx2,
     &    icntx3, icntf, cntwx, summ, sump, k1s, k1f
         call bummer('lcb3x1: icntf=',icntf,faterr)
      endif
c
      do 200 k1=k1s,k1f
         do 210 jsym=1,(ksym-1)
            nj=nepsy(jsym)
            if(nj.eq.0) go to 210
            js=sym12(jsym)
            is=mult(js,kls)
            isym=sym21(is)
            if(isym.ge.jsym) go to 210
            ni=nepsy(isym)
            if(ni.eq.0) go to 210
c
            do 220 j1=1,nj
               do 230 i1=1,ni
                  if(incnt+3.gt.num)then
                     numin = numin - num
                     num   = min( numin, nipsg )
                     incnt = 0
                     call rdbks( pints, dabuf, idabuf )
                  endif
c                 # 1a,b  wy,xy
                  wxints(icntw1+i1)=pints(incnt+1)+pints(incnt+3)
                  wxints(icntx1+i1)=pints(incnt+1)-pints(incnt+3)
c                 # 2a,b  wy,xy
                  wxints(icntw2+i1)=pints(incnt+1)+pints(incnt+2)
                  wxints(icntx2+i1)=pints(incnt+1)-pints(incnt+2)
c                 # 3a,b  wy,xy
                  wxints(icntw3+i1)=pints(incnt+2)+pints(incnt+3)
                  wxints(icntx3+i1)=pints(incnt+2)-pints(incnt+3)
c
                  incnt=incnt+3
230            continue
c
               icntw1 =icntw1 + ni
               icntw2 =icntw2 + ni
               icntw3 =icntw3 + ni
               icntx1 =icntx1 + ni
               icntx2 =icntx2 + ni
               icntx3 =icntx3 + ni
c
220         continue
210      continue
c
         if(ksym.le.lsym) go to 300
c
c        # ksym = jsym , isym = lsym
c
         ni=nepsy(lsym)
         do 310 j1=1,k1
            do 320 i1=1,ni
               if(incnt+3.gt.num)then
                  numin = numin - num
                  num   = min( numin, nipsg )
                  incnt = 0
                  call rdbks( pints, dabuf, idabuf )
               endif
               if(j1.ne.k1)then
c                 # 1a,b  wy,xy
                  wxints(icntw1+1)=pints(incnt+1)+pints(incnt+3)
                  wxints(icntx1+1)=pints(incnt+1)-pints(incnt+3)
c                 # 2a,b  wy,xy
                  wxints(icntw2+1)=pints(incnt+1)+pints(incnt+2)
                  wxints(icntx2+1)=pints(incnt+1)-pints(incnt+2)
c                 # 3a,b  wy,xy
                  wxints(icntw3+1)=pints(incnt+2)+pints(incnt+3)
                  wxints(icntx3+1)=pints(incnt+2)-pints(incnt+3)
c
                  icntw1 = icntw1 + 1
                  icntw2 = icntw2 + 1
                  icntw3 = icntw3 + 1
                  icntx1 = icntx1 + 1
                  icntx2 = icntx2 + 1
                  icntx3 = icntx3 + 1
c
                  incnt = incnt + 3
               else
c                 # 7  wy
                  wxints(icntw2+1)=pints(incnt+2)*sqrt2
c                 # 6a,b  wy,xy
                  wxints(icntw3+1)=pints(incnt+1)+pints(incnt+2)
                  wxints(icntx3+1)=-pints(incnt+1)+pints(incnt+2)
c
                  icntw2 = icntw2 + 1
                  icntw3 = icntw3 + 1
                  icntx3 = icntx3 + 1
                  incnt = incnt + 3
               endif
c
320         continue
310      continue
300      continue
200   continue
c
      if(icntw1.ne.isvw2 .or. icntw2.ne.isvw3 .or.
     & icntw3.ne.isvx1-ioffs .or. icntx1.ne.isvx2 .or.
     & icntx2.ne.isvx3 .or. icntx3.ne.icntf)then
         write(nlist,9040) icntw1,icntw2,icntw3,icntx1,icntx2,icntx3,
     &    isvw1,isvw2,isvw3,isvx1,isvx2,isvx3,icntf
         call bummer('lcb3x1: icnt error',0,faterr)
      endif
c
      sumw   = icntw3
      sumx   = icntf - icntw3
      isvx1  = isvx1 - sumw
      isvx2  = isvx2 - sumw
      isvx3  = isvx3 - sumw
      cntwx1 = cntwx / (k1f-k1s+1)
c
c     # sumw  = total number of + combinations (wy loops)
c     # sumx  = total number of - combinations (xy loops)
c     # isvw1 = starting adress for loop 1a,b  wy
c     # isvw2 = starting adress for loop 2a,b  wy
c     # isvw3 = starting adress for loop 3a,b  wy
c     # isvx1 = starting adress for loop 1a,b  xy
c     # isvx2 = starting adress for loop 2a,b  xy
c     # isvx3 = starting adress for loop 3a,b  xy
c     # cntwx = number of integrals for lmd(i).ne.lmd(j).ne.lmd(k)
c
      iscr = 0
      call code3x(
     & wxints, sumw,   isvw1,  isvw2,
     & isvw3,  cntwx1, ksym,   k1s,
     & k1f,    l1,     iscr )
      call code3x(
     & wxints( icntw3+1 ), sumx,   isvx1,  isvx2,
     & isvx3,              cntwx1, ksym,   k1s,
     & k1f,                l1,     iscr )
c
      return
9010  format(' lcb3x1 9010 :',12i8)
9040  format(' lcb3x1 9040:',13i7)
      end
c deck lcb3x2
      subroutine lcb3x2(
     & incnt,  numin,  num,    pints,
     & wxints, dabuf,  idabuf, cntw3,
     & cntw12, cntx3,  cntx12, maxbl3,
     & l1,     ksym,   k1s,    k1f,
     & nepsy,  ioffs,  sqrt2 )
c
c  3-external linear combinations for ksym = lsym.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
c     # dummy:
      integer incnt, numin, num, cntw3, cntw12, cntx3, cntx12, maxbl3,
     & l1, ksym, k1s, k1f, ioffs
      real*8 pints(nipsg),wxints(*),dabuf(*),sqrt2
      integer idabuf(*),nepsy(8)
c
c     # local:
      integer strw12, strtw3, strx12, strtx3, strtwf, strtxf, stfw12,
     & stfx12, stx12, stx3, stxf, iscr, k1, jsym, nj, j1, iind, i1,
     & iindw, iindx, jm
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      strw12=ioffs
      strtw3=strw12+cntw12
      strx12=strtw3+cntw3+ioffs
      strtx3=strx12+cntx12
      strtwf=strx12-ioffs
      strtxf=strtx3+cntx3
      stfw12=strw12
      stfx12=strx12
c
c     # strtwf  total number of + combinations(wy loops)
c     # stxf    total number of - combinations(xy loops)
c     # strw12  starting address for 1a,b and 2a,b + comb.  wy loops
c     # stx12   starting address for 1a,b and 2a,b - comb. xy loops
c     # strtw3  starting address for 3a,b          + comb.  wy loops
c     # stx3    starting address for 3a,b          - comb.  xy loops
c
      stx12=ioffs
      stx3=stx12+cntx12
      stxf=stx3+cntx3
      iscr=1
      call code3x(
     & wxints, strtwf, strw12, 0,
     & strtw3, 0,      ksym,   k1s,
     & k1f,    l1,     iscr )
      call code3x(
     & wxints( strx12-1 ), stxf,   stx12,  0,
     & stx3,               0,      ksym,   k1s,
     & k1f,                l1,     iscr )
c
      if(strtxf.gt.maxbl3)then
         write(nlist,20000)strtxf,maxbl3
         call bummer('lcb3x2: (strtxf-maxbl3)',(strtxf-maxbl3),faterr)
      endif
c
      do 110 k1=k1s,k1f
c
         do 120 jsym=1,(ksym-1)
c
c           # jsym < ksym
c
            nj=nepsy(jsym)
            if(nj.eq.0)go to 120
            do 130 j1=1,nj
               iind=j1
               do 140 i1=1,j1
                  if(incnt+3.gt.num)then
                     numin = numin - num
                     num   = min( numin, nipsg )
                     incnt = 0
                     call rdbks( pints, dabuf, idabuf )
                  endif
c                 # 1a,b  4a,b wy,xy
c                 # element(j,i)
                  wxints(strw12+i1)=pints(incnt+1)+pints(incnt+3)
                  wxints(strx12+i1)=pints(incnt+1)-pints(incnt+3)
c
                  if(i1.ne.j1)then
c                    # 2a,b  wy,xy
c                    # element (i,j)
                     wxints(stfw12+iind)=pints(incnt+1)+pints(incnt+2)
                     wxints(stfx12+iind)=pints(incnt+1)-pints(incnt+2)
c                    # 3a,b  wy,xy
                     wxints(strtw3+i1)=pints(incnt+2)+pints(incnt+3)
                     wxints(strtx3+i1)=pints(incnt+2)-pints(incnt+3)
                  else
c                    # 5  wy
                     wxints(strtw3+i1)=pints(incnt+2)*sqrt2
                  endif
170               continue
                  iind=iind+nj
                  incnt=incnt+3
140            continue
               strw12=strw12+nj
               strx12=strx12+nj
               strtw3=strtw3+j1
               strtx3=strtx3+j1-1
130         continue
            stfw12=stfw12+nj*nj
            stfx12=stfx12+nj*nj
120      continue
c
c        # jsym = ksym
c
         if(k1.eq.1)go to 110
         do 210 j1=1,k1
            iindw=j1
            iindx=j1
            if(j1.eq.k1) go to 250
            do 220 i1=1,j1
               if(incnt+3.gt.num)then
                  numin = numin - num
                  num   = min( numin, nipsg )
                  incnt = 0
                  call rdbks( pints, dabuf, idabuf )
               endif
c              # 1a,b  4a,b  wy,xy
c              # element (j,i)
               wxints(strw12+i1)=pints(incnt+1)+pints(incnt+3)
               wxints(strx12+i1)=pints(incnt+1)-pints(incnt+3)
c
               if(i1.ne.j1)then
c                 # 2a,b  wy,xy
c                 # element (i,j)
                  wxints(stfw12+iindw)=pints(incnt+1)+pints(incnt+2)
                  wxints(stfx12+iindx)=pints(incnt+1)-pints(incnt+2)
c                 # 3a,b  wy,xy
                  wxints(strtw3+i1)=pints(incnt+2)+pints(incnt+3)
                  wxints(strtx3+i1)=pints(incnt+2)-pints(incnt+3)
               else
c                 # 5  wy
                  wxints(strtw3+i1)=pints(incnt+2)*sqrt2
               endif
240            continue
               iindw=iindw+k1
               iindx=iindx+k1-1
               incnt=incnt+3
c
c              # end of i1 loop.
220         continue
            strw12=strw12+k1
            strx12=strx12+k1-1
            strtw3=strtw3+j1
            strtx3=strtx3+j1-1
            go to 210
c
c           # j1 = k1 case
c
250         continue
            jm=j1-1
            do 260 i1=1,jm
               if(incnt+3.gt.num)then
                  numin = numin - num
                  num   = min( numin, nipsg )
                  incnt = 0
                  call rdbks( pints, dabuf, idabuf )
               endif
c              # 6a,b  wy,xy
               wxints(strtw3+i1)=pints(incnt+1)+pints(incnt+2)
               wxints(strtx3+i1)=-pints(incnt+1)+pints(incnt+2)
c              # 7  wy
               wxints(stfw12+iindw)=pints(incnt+2)*sqrt2
c
               iindw=iindw+k1
               incnt=incnt+3
260         continue
            strtw3=strtw3+jm
            strtx3=strtx3+jm
c
c           # end of j1 loop.
210      continue
         stfw12=strw12
         stfx12=strx12
c
c        # end of k1 loop.
110   continue
      strw12=strw12-ioffs
      strtw3=strtw3-ioffs
      strx12=strx12-ioffs*2
      strtx3=strtx3-ioffs*2
      strtxf=strtxf-ioffs*2
c
      if(strw12.ne.cntw12 .or. strtw3.ne.cntw12+cntw3
     & .or. strx12.ne.cntw12+cntw3+cntx12
     & .or. strtx3.ne.strtxf)then
         write(nlist,20010)cntw12,cntw3,cntx12,cntx3,strw12,strtw3,
     &    strx12,strtx3,strtxf
         call bummer('lcb3x2: str* error',0,faterr)
      endif
c
      return
20010 format(' error in lcb3x2'/' cntw12,cntw3,cntx12,cntx3',4i6,
     & /,' strw12,strtw3,strx12,strtx3,strtxf',5i6)
20000 format(' error in lcb3x2'/' space requested',i10,
     & ' space available',i10)
      end
c deck puto
c deck puti
c deck putd
c deck putf
c**********************************************************************
      subroutine puto( bufp, a, n )
c**********************************************************************
c
c  copy n words from a(*) to bufp(*), dumping bufp(*) when necessary.
c  this routine and its entry points are used for writing 0-, 1-, and
c  2-external integrals and diagonal integrals to the appropriate
c  direct access files.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer        ifilep, lbufp, nbufp, ibp
      common /cputo/ ifilep, lbufp, nbufp, ibp
      save   /cputo/
c
c     # dummy:
      integer n, ifile, lbuf
      real*8 bufp(lbufp), a(*)
      character*(*) fname
c
c     # local:
      integer iap, naleft, nbleft
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      iap    = 1
      naleft = n
      nbleft = lbufp - ibp + 1
10    if ( naleft .ge. nbleft ) then
         call dcopy_wr( nbleft,    a(iap), 1,    bufp(ibp), 1 )
         nbufp = nbufp + 1
         call writda( ifilep, nbufp, bufp, lbufp )
         ibp = 1
         iap = iap + nbleft
         naleft = naleft - nbleft
         nbleft = lbufp
         go to 10
      endif
c
      if ( naleft .gt. 0 ) then
         call dcopy_wr( naleft,    a(iap), 1,    bufp(ibp), 1 )
         ibp = ibp + naleft
      endif
c
      return
c
c**********************************************************************
      entry puti( ifile, lbuf, fname )
c**********************************************************************
c
c     # initialize local variables for subsequent calls to puto.
c
      ifilep = ifile
      lbufp  = lbuf
      nbufp  = 0
      ibp    = 1
      call openda( ifilep, fname, lbufp, 'keep', 'seqwrt' )
      return
c
c**********************************************************************
      entry putd( bufp )
c**********************************************************************
c
c     # dump any remaining elements in bufp(*).
c
      if ( ibp .gt. 1 ) then
         nbufp = nbufp + 1
         call writda( ifilep, nbufp, bufp, lbufp )
         ibp = 1
      endif
      return
c
c**********************************************************************
      entry putf
c**********************************************************************
c
c     # check to make sure the last buffer was dumped
c     # and write out stats.
c
      if ( ibp .gt. 1 ) then
         write(nlist,6080) ibp, ifilep
         call bummer('puto: ibp=',ibp,faterr)
      endif
      write(nlist,6010) nbufp, lbufp, ifilep
      call closda( ifilep, 'keep', 2 )
c
      return
6010  format(/' putf:',i8,' buffers of length',i8,
     & ' written to file',i3)
6080  format(' *** error *** putf: ibp=',i8,' on file',i3)
      end
c deck puto34
c deck puti34
c deck putd34
c deck putf34
c**********************************************************************
      subroutine puto34( nw, nx, ioffwx, wxints, bufw, bufx )
c**********************************************************************
c
c  copy nw integrals from wxints(*) to bufw(*) and nx integrals from
c  wxints(*) to bufx(*), writing when necessary.  this routine and its
c  entry points are used for writing the 3- and 4-external integral
c  combinations to the appropriate sequential output file and the
c  corresponding record info to the appropriate sequential files.
c  note that this routine and its entry points are somewhat less general
c  and more special purpose than the analogous puto() entry points.
c
c  input:
c  nw        = number in w-type combinations in the w-block.
c  nx        = number in x-type combinations in the x-block.
c  ioffwx    = number of header words for each block.
c  wxints(*) = contains one block of w-combinations and one block of
c              x-combinations.
c  bufw(*)   = buffer for w-combinations.
c  bufx(*)   = buffer for x-combinations.
c
c  03-nov-89 packing of negative integers removed. -rls
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
c     # local:
      integer         lbufp, nrecw, nrecx, iwp, ixp, nblkw, nblkx,
     & info,    iwstat, ixstat, nfilw, nfilx
      common /cput34/ lbufp, nrecw, nrecx, iwp, ixp, nblkw, nblkx,
     & info(2), iwstat, ixstat, nfilw, nfilx
      save   /cput34/
c
c     # dummy:
      integer nw, nx, ioffwx, lbuf, nfil1, nfil2
      real*8 wxints(*), bufw(lbufp), bufx(lbufp)
c
      real*8    hundrd,       wstat, xstat
      parameter(hundrd=100d0)
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
c     # iwxp = pointer to the first usable integral location in
c     #        each buffer.
c
      integer   iwxp
      parameter(iwxp=2)
c
c     # w-type integral combinations.
c
      if ( nw .ne. 0 ) then
c
         if ( iwp+(nw+ioffwx)-1 .gt. lbufp ) then
            iwstat = iwstat + iwp - 1
            nrecw = nrecw + 1
            info(1) = 0
            info(2) = nblkw
            call plab32( bufw(1), info, 2 )
c           write(nfilw) bufw
            call writda( nfilw, nrecw, bufw, lbufp )
            iwp = iwxp
            nblkw = 0
         endif
c
         nblkw = nblkw + 1
         call dcopy_wr( (nw+ioffwx), wxints, 1, bufw( iwp ), 1 )
         iwp = iwp + (nw + ioffwx)
c
      endif
c
c     # x-type integral combinations.
c
      if ( nx .ne. 0 ) then
c
         if ( ixp+(nx+ioffwx)-1 .gt. lbufp ) then
            ixstat = ixstat + ixp - 1
            nrecx = nrecx + 1
            info(1) = 0
            info(2) = nblkx
            call plab32( bufx(1), info, 2 )
c           write(nfilx) bufx
            call writda( nfilx, nrecx, bufx, lbufp )
            ixp = iwxp
            nblkx = 0
         endif
c
         nblkx = nblkx + 1
         call dcopy_wr( (nx+ioffwx),
     &    wxints((nw+ioffwx)+1), 1,    bufx(ixp), 1 )
         ixp = ixp + (nx + ioffwx)
c
      endif
c
      return
c
c**********************************************************************
      entry puti34( lbuf, nfil1, nfil2 )
c**********************************************************************
c
c     # initialize local variables for subsequent calls to puto34().
c
      nfilx = nfil1
      nfilw = nfil2
      lbufp = lbuf
      nrecw = 0
      nrecx = 0
      iwp = iwxp
      ixp = iwxp
      nblkw = 0
      nblkx = 0
      info(1) = 0
      info(2) = 0
      iwstat = 0
      ixstat = 0
c     rewind(nfilx)
c     rewind(nfilw)
      return
c
c**********************************************************************
      entry putd34( bufw, bufx )
c**********************************************************************
c
c     # dump any remaining elements in bufw(*) and bufx(*).
c     # this entry should be called after processing the 4-external
c     # integrals and again after processing the 3-external integrals.
c
cz    # the following line is commented out in order to force one record
cz    # always to be dumped to fil4x/fil4w even if there are no integral
cz
cz     if ( iwp .ne. iwxp ) then
         iwstat = iwstat + iwp - 1
         nrecw = nrecw + 1
         info(1) = 1
         info(2) = nblkw
         call plab32( bufw(1), info, 2 )
c        write(nfilw) bufw
         call writda( nfilw, nrecw, bufw, lbufp )
         iwp = iwxp
         nblkw = 0
cz     endif
cz     if ( ixp .ne. iwxp ) then
         ixstat = ixstat + ixp - 1
         nrecx = nrecx + 1
         info(1) = 1
         info(2) = nblkx
         call plab32( bufx(1), info, 2 )
c        write(nfilx) bufx
         call writda( nfilx, nrecx, bufx, lbufp )
         ixp = iwxp
         nblkx = 0
cz     endif
c
c     # compute the percentage of file space used compared to that
c     # allocated. the difference is due to blocking of the integral
c     # combinations.
c
      if ( nrecw .ne. 0 ) then
         wstat = (hundrd * ((iwstat - 1) / lbufp + 1)) / nrecw
      else
         wstat = hundrd
      endif
      if ( nrecx .ne. 0 ) then
         xstat = (hundrd * ((ixstat - 1) / lbufp + 1)) / nrecx
      else
         xstat = hundrd
      endif
      write(nlist,6020) nrecw, nrecx, lbufp, wstat, xstat
      return
c
c**********************************************************************
      entry putf34( nfil1, nfil2 )
c**********************************************************************
c
c     # check to make sure the last buffers were dumped
c     # and write out stats.
c
      if ( iwp .ne. iwxp .or. ixp .ne. iwxp ) then
         write(nlist,6080) iwp, ixp, nfilw, nfilx
         call bummer('puto34: i*p error',0,faterr)
      endif
      write(nlist,6030) nfilw, nfilx, nrecw, nrecx, lbufp
c
c     close(unit = nfil2)
c     close(unit = nfil1)
      call closda(nfil2,'keep',2)
      call closda(nfil1,'keep',2)
c
      return
6020  format(/' putd34:',i6,' records of integral w-combinations and',
     & /'        ',i6,' records of integral x-combinations',
     & ' of length=',i6,' have been written.'/
     & ' wstat,xstat=',2f8.2)
6030  format(/' putf34: external integral file complete.',' nfilw=',i6,
     & ' nfilx=',i6,' nrecw=',i6,' nrecx=',i6,' lbufp=',i6)
6080  format(' *** error *** putf34: iwp,ixp,nfl34w,nfl34x=',4i8)
      end
c deck rdbks
      subroutine rdbks( pints, dabuf, idabuf )
c
c  read a set of chained direct access records from the intermediate
c  direct access file and update pointer info.
c
c  iblkst = negative offset for addressing integrals in the current
c           segment.
c  maxrd = the number of integrals in the current segment.
c  ntot2e = the total number of integrals remaining to be processed.
c  ibuk = segment pointer for the current segment.
c  nipsg = maximum number of integrals in each segment.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      ndascr
      equivalence (ndascr,iunits(11))
c
      integer         ibuk, iblkst, ntot2e, maxrd
      common /crdbks/ ibuk, iblkst, ntot2e, maxrd
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      real*8 pints(*), dabuf(lendar)
      integer idabuf(nipbk)
c
c     # local:
      integer irecn, num, i
      integer info(2)
c
c     # diagonal integrals (ibuk=1) should be read with a zero offset.
c
      if ( ibuk .eq. 1 ) then
         iblkst = 0
      else
         iblkst = - (ibuk-2) * nipsg
      endif
c
      maxrd = min( nipsg, ntot2e )
      call wzero( maxrd, pints, 1 )
      ntot2e = ntot2e - maxrd
c
c     # process buffers.
c
      irecn = irecpt(ibuk)
c
c     # do while (irecn .ne. 0 ) ...
100   if ( irecn .ne. 0 ) then
c
         call readda( ndascr, irecn, dabuf, lendar )
c
c        # unpack record info.
c
         call ulab32( dabuf(lendar), info, 2 )
         irecn = info(1)
         num   = info(2)
c
c        # unpack labels.
c
         call ulab32( dabuf(nipbk+1), idabuf, num )
c
c        # process integrals in the buffer.
c
         do 10 i = 1, num
            pints( idabuf(i) + iblkst ) = dabuf(i)
10       continue
c
c        # next buffer.
c
         go to 100
      endif
c
c     # update the bucket pointer for the next call.
c
      ibuk = ibuk + 1
c
      return
      end
c deck indxdg
      subroutine indxdg(
     & nsym,   levsym, idiagx, numint,
     & numext, numhlf, numitr )
c
c  this subroutine sets up the addressing arrays for the
c  integrals that contribute to the diagonal elements
c  of the hamiltonian.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      integer         srtopt
      common /option/ srtopt(5)
c
      integer         nepsy
      common /iextnm/ nepsy(8)
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
c     # dummy:
      integer nsym, numint, numext, numhlf, numitr
      integer istsym(8),idiagx(nnlev),levsym(nlevel)
c
c     # local:
      integer ipos, hstuoc, itp, itsym, iorb1, jsym, jmin, jmax,
     & isym, imin, imax, j, jstrt, i, indx1
c
      ipos=1
      hstuoc=lowinl-1
c
c     # case 1 : j and i are both external orbitals
c
      do 50 itp=1,nsym
         istsym(itp)=1
         nepsy(itp)=0
50    continue
      itsym=1
      do 150 iorb1=1,hstuoc
         if(levsym(iorb1).eq.itsym) go to 100
         itsym=levsym(iorb1)
         istsym(itsym)=iorb1
100      nepsy(itsym)=nepsy(itsym)+1
150   continue
c
      if(srtopt(1).gt.1)then
         write (nlist,175) (itp,istsym(itp),nepsy(itp),itp=1,nsym)
175      format(/' indxdg:'/'    isym  istsym  nepsy'/(3i8))
      endif
c
      do 400 jsym=1,nsym
         if(nepsy(jsym).eq.0) go to 400
         jmin=istsym(jsym)
         jmax=istsym(jsym)+nepsy(jsym)-1
         do 350 isym=1,jsym
            if(nepsy(isym).eq.0) go to 350
            imin=istsym(isym)
            imax=istsym(isym)+nepsy(isym)-1
            do 300 j=jmin,jmax
               jstrt=iim1(j)
               if(isym.eq.jsym) imax=j
               do 250 i=imin,imax
                  idiagx(jstrt+i)=ipos
                  ipos=ipos+2
250            continue
300         continue
350      continue
400   continue
c
c     # case 2 : j is internal; i is external
c
      do 500 j = lowinl, nlevel
         jstrt = iim1(j)
         do 450 i = 1, hstuoc
            idiagx(jstrt+i) = ipos
            ipos=ipos+2
450      continue
500   continue
c
c     # case 3 : j and i are both internal
c
      do 600 j = lowinl, nlevel
         jstrt = iim1(j)
         do 550 i = lowinl, j
            idiagx(jstrt+i) = ipos
            ipos = ipos + 2
550      continue
600   continue
c
c     # calculate number of each type of integrals
c
      indx1=iim1(nlevel) + nlevel
      numint=idiagx(indx1)+1
      indx1=iim1(hstuoc) + hstuoc
      numext=idiagx(indx1)+1
      indx1=iim1(nlevel) + hstuoc
      numhlf=idiagx(indx1)+1-numext
      numitr=numint-numhlf-numext
      write (nlist,650) numint,numext,numhlf,numitr
650   format(/' indxdg: diagonal integral statistics.'/
     & ' total number of integrals contributing to diagonal',
     & ' matrix elements:',i10/
     & ' number with all external indices:',i10/
     & ' number with half external - half internal indices:',i10/
     & ' number with all internal indices:',i10)
c
      if(srtopt(1).ge.1)then
         write (nlist,700) idiagx
      endif
c
      return
700   format(' idiagx(*) ='/(10i10))
      end
c deck indxof
      subroutine indxof(
     & nsym,   lk432x, lk1ex,  lk0ex,
     & ijkl,   ijsym,  levsym  )
c
c  this subroutine sets up the addressing arrays for the
c  integrals that contribute to the off-diagonal elements
c  of the hamiltonian.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      integer         srtopt
      common /option/ srtopt(5)
c
*@ifdef spinorbit
      integer         istrtx,   numx
      common /ctypei/ istrtx(8),numx(8)
*@else
*      integer         istrtx,   numx
*      common /ctypei/ istrtx(5),numx(5)
*@endif
c
      integer     mx4x, mx3x, mx2x
      common /c9/ mx4x, mx3x, mx2x
c
      integer        mult
      common /cmult/ mult(8,8)
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir
      common /solxyz/ spnorb, spnodd, lxyzir(3)
      integer lxyz
*@endif
c
c     # dummy:
      integer nsym
      integer levsym(nlevel),strtij(8),lk432x(nnlev),
     & lk1ex(*),lk0ex(*),ijkl(*),ijsym(nnlev)
c
c     # *** change to ijkl(nnlev,nsym) and eliminate strtij(*) -rls.
c
c     # local:
      integer hstuoc, norbin, nnobin, icnt, i, nndx, j, jsym, isym, sym,
     & ipos, strt, imax, jstrt, offskl, l, k, idif, lstrt, lksym, nndxa,
     & l1, l1strt, kkm, nndx1, nndx2, numint, m, itp
c
*@ifdef spinorbit
c
      integer nmbpr(8), lmda(8,8), lmdb(8,8), nmbj
c
      integer         nepsy
      common /iextnm/ nepsy(8)
c
*@endif
c
      hstuoc = lowinl - 1
      norbin = nlevel - lowinl + 1
      nnobin = (norbin*(norbin+1))/2
c
c     # calculate divisions of ijkl
c     # *** this should be changed to a 2-d array. ***
c
      icnt=0
      do 100 i=1,nsym
         strtij(i) = icnt
         icnt=icnt+nnlev
100   continue
c
c     # zero out arrays
c
      call izero_wr( nnlev, lk432x, 1 )
c
*@ifdef spinorbit
      call izero_wr( 8, numx, 1)
*@endif
c
c     # setup ijsym
c
      nndx = 1
      do 250 j = 1, nlevel
         jsym = levsym(j)
         do 200 i=1,j
            isym = levsym(i)
            ijsym(nndx) = mult(jsym,isym)
            nndx = nndx + 1
200      continue
250   continue
c
c     # calculate ijkl for all irreps
c
      do 8100 sym = 1, nsym
         ipos = 1
         strt = strtij(sym)
c
c        # case 1: i is external; j is external or internal
c
         do 840 j = 1, nlevel
            imax = min(j,hstuoc)
            if (j .eq. lowinl) ipos = 1
            jstrt=iim1(j)
            do 830 i=1,imax
               nndx=jstrt+i
               ijkl(strt+nndx)=ipos
               if(ijsym(nndx).eq.sym) ipos=ipos+3
830         continue
840      continue
c
c        # case 2: i and j are internal
c
         ipos=1
         do 860 j=lowinl,nlevel
            jstrt=iim1(j)
            do 850 i=lowinl,j
               nndx=jstrt+i
               ijkl(strt+nndx)=ipos
               if(ijsym(nndx).eq.sym) ipos=ipos+3
850         continue
860      continue
8100  continue
c
      offskl=0
c
c     # calculate lk432x for 4 external
c
      mx4x=0
      nndx=0
      do 8130 l=1,hstuoc
         do 8120 k=1,l
            nndx=nndx+1
            lk432x(nndx)=offskl
            if(k.eq.1) go to 8120
            idif=ijkl(strtij(ijsym(nndx))+iim1(k+1))-1
            mx4x=max(mx4x,idif)
            offskl=offskl+idif
8120     continue
8130  continue
c
c     # calculate lk432x for 3 external
c
      mx3x=0
      do 8150 l = lowinl, nlevel
         lstrt=iim1(l)
         do 8140 k=1,hstuoc
            nndx=lstrt+k
            lk432x(nndx)=offskl
            if(k.eq.1) go to 8140
            idif=ijkl(strtij(ijsym(nndx))+iim1(k+1))-1
            mx3x=max(mx3x,idif)
            offskl=offskl+idif
8140     continue
8150  continue
c
c     # calculate lk432x for 2 external
c
      mx2x=0
      do 8192 l = lowinl, nlevel
         lstrt=iim1(l)
         do 8190 k=lowinl,l
            nndx=lstrt+k
            lk432x(nndx)=offskl
            lksym=ijsym(nndx)
            do 8172 j=1,hstuoc
               jstrt=iim1(j)
               do 8170 i=1,j
                  nndxa=jstrt+i
                  if(ijsym(nndxa).eq.lksym) offskl=offskl+3
8170           continue
8172        continue
            mx2x=max(mx2x,offskl-lk432x(nndx))
8190     continue
8192  continue
c
c     # calculate lk1ex for 1 external
c
      do 8250 l = lowinl, nlevel
         l1=l-hstuoc
         l1strt=iim1(l1)
         lstrt=iim1(l)
         do 8240 k=lowinl,l
            lk1ex(l1strt+k-hstuoc)=offskl
            lksym=ijsym(lstrt+k)
            do 8230 j=lowinl,k
               jstrt=iim1(j)
               do 8220 i=1,hstuoc
                  nndxa=jstrt+i
                  if(ijsym(nndxa).eq.lksym) offskl=offskl+3
8220           continue
8230        continue
8240     continue
8250  continue
c
c     # calculate lk0ex for all internal indices
c
      do 8300 l = lowinl, nlevel
         l1=l-hstuoc
         l1strt=iim1(l1)
         lstrt=iim1(l)
         do 8290 k=lowinl,l
            lk0ex(l1strt+k-hstuoc)=offskl
            if(k.eq.lowinl) go to 8290
            nndx=lstrt+k
            kkm=strtij(ijsym(nndx))+iim1(k)+k
            offskl=offskl+ijkl(kkm)-1
8290     continue
8300  continue
c
*@ifdef spinorbit
c
      if(.not.spnorb)goto 8301
c
c     # calculate the number of 0 external spin orbit integrals
c
c
      do 6000 j = lowinl, nlevel
         write(*,*)"j=", j
         jsym = levsym(j)
         do 6100 i = lowinl, j-1
            isym = levsym(i)
            lksym = mult(isym, jsym)
            do 6200 lxyz = 1, 3
               if( lxyzir(lxyz) .eq. lksym )then
                   numx(6) = numx(6) + 1
               endif
6200        continue
6100     continue
6000  continue
c
c     # calculate the number of 1 external spin orbit integrals
c
      do 5000 j = lowinl, nlevel
         jsym = levsym(j)
         do 5100 lxyz = 1, 3
            isym = mult(jsym, lxyzir(lxyz))
            if( nepsy(isym) .eq.0 )goto 5100
            numx(7) = numx(7) + nepsy(isym)
5100     continue
5000  continue
c
c     # lmdb(i,j) lmda(i,j) symmetry pairs defined as
c     # lmd(i)=lmdb(i,j)*lmda(i,j), j=nmdpr(i)
c     # lmdb(i,j).ge.lmda(i,j), lmdb(i,j).lt.lmdb(i,j+1)
c     # nmbpr(i) number of pairs
c
      do 9300 i = 1, nsym
         nmbj=0
         do 9301 j = 1, nsym
            if(nepsy(j) .eq. 0)goto 9301
            lksym = mult(i, j)
            if(nepsy(lksym) .eq. 0) goto 9301
            if(lksym .gt. j)goto 9301
            nmbj = nmbj + 1
            lmdb(i,nmbj) = j
            lmda(i,nmbj) = lksym
9301     continue
         nmbpr(i) = nmbj
9300  continue
c
c     # calculate the number of 2 external spin orbit integrals
c
      do 7000 lxyz = 1, 3
         lksym = lxyzir(lxyz)
         if(nmbpr(lksym) .eq. 0)goto 7000
         do 7001 k = 1, nmbpr(lksym)
            isym = lmda(lksym, k)
            jsym = lmdb(lksym, k)
            if(isym .eq. jsym)then
               numx(8) = numx(8) + nepsy(isym) * (nepsy(isym) + 1) / 2
            else
               numx(8) = numx(8) + nepsy(isym) * nepsy(jsym)
            endif
7001     continue
7000  continue
8301  continue
*@endif
c
c     # calculate beginning addresses for each type of integral
c     # (istrtx(1:8)) and the number of each type (numx(1:8)).
c
      nndx1=iim1(lowinl) + 1
      nndx2=iim1(lowinl)+lowinl
      istrtx(1)=1
      istrtx(2)=lk432x(nndx1)+1
      numx(1)=istrtx(2)-1
      istrtx(3)=lk432x(nndx2)+1
      numx(2)=istrtx(3)-istrtx(2)
      istrtx(4)=lk1ex(1)+1
      numx(3)=istrtx(4)-istrtx(3)
      istrtx(5)=lk0ex(1)+1
      numx(4)=istrtx(5)-istrtx(4)
      numx(5)=offskl+1-istrtx(5)
      numint=offskl
      write(nlist,65)
      do 68 i=1,5
         m=5-i
         write (nlist,67) m,numx(i),istrtx(i)
68    continue
      write (nlist,69) numint
      if(nsym.gt.1)then
c
c        # compress ijkl
c
         nndx=1
         do 1200 i = 1, nlevel
            do 1150 j = 1, i
               isym=ijsym(nndx)
               strt=strtij(isym)
               ijkl(nndx)=ijkl(nndx+strt)
               nndx=nndx+1
1150        continue
1200     continue
c
      endif
c
*@ifdef spinorbit
c
      if(.not.spnorb)goto 91
      istrtx(6)=numint+1
      istrtx(7)=istrtx(6)+numx(6)
      istrtx(8)=istrtx(7)+numx(7)
c
      write(nlist,55)
      do 58 i=6,8
         m=i-6
         istrtx(i) = istrtx(i - 1) + numx(i - 1)
         write (nlist,57) m,numx(i),istrtx(i)
58    continue
c
55    format(/' indxof: spin-orbit integral statistics.')
57    format(1x,i4,'-external so integrals: num=',i11,' strt=',i11)
c
91    continue
c
*@endif
c
      if(srtopt(1).ge.1)then
c              # write out indexing arrays
         write (nlist,90) (ijkl(i),i=1,nnlev)
         write (nlist,80) lk432x
         write (nlist,82) (lk1ex(itp),itp=1,nnobin)
         write (nlist,83) (lk0ex(itp),itp=1,nnobin)
      endif
c
      return
65    format(/' indxof: off-diagonal integral statistics.')
67    format(1x,i4,'-external integrals: num=',i11,' strt=',i11)
69    format(/' total number of off-diagonal integrals:',i12//)
90    format(' offext: ijkl(*)='/(10i10))
80    format(' lk432x(*)='/(10i10))
82    format(' lk1ex(*)='/(10i10))
83    format(' lk0ex(*)='/(10i10))
      end
