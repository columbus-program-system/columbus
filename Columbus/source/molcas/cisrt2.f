!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c  things to do in this program:
c  * update comments and other documentation to properly describe
c        the files, record length parameters, etc.
c  # use async da i/o routines.
c
c deck block data
      block data
c
      implicit logical(a-z)
c
      integer        mult
      common /cmult/ mult(8,8)
c
      integer         ldamin, ldamax, ldainc
      common /cldain/ ldamin, ldamax, ldainc
c
      integer         maxbuf, maxbl3, maxbl4, intmxo
      common /cbufsz/ maxbuf, maxbl3, maxbl4, intmxo
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      ndrt
      equivalence (ndrt,iunits(3))
      integer      moints
      equivalence (moints,iunits(4))
      integer      ndgint
      equivalence (ndgint,iunits(5))
      integer      nodint
      equivalence (nodint,iunits(6))
      integer      nfl4w
      equivalence (nfl4w,iunits(7))
      integer      nfl4x
      equivalence (nfl4x,iunits(8))
      integer      nfl3w
      equivalence (nfl3w,iunits(9))
      integer      nfl3x
      equivalence (nfl3x,iunits(10))
      integer      ndascr
      equivalence (ndascr,iunits(11))
      integer      nsrtif
      equivalence (nsrtif,iunits(12))
c
c     # default file unit numbers and filenames:
c
c     # listing file.
      data nlist    /6/
      data fname(1) /'cisrtls'/
c
c     # input file.
      data nin      /5/
      data fname(2) /'cisrtin'/
c
c     # cidrt file.
      data ndrt     /10/
      data fname(3) /'cidrtfl'/
c
c     # transformed integral file.
      data moints   /11/
      data fname(4) /'moints'/
c
c     # sorted diagonal integrals.
      data ndgint   /35/
*@ifdef ibmcms
*      data fname(5) /'diagint g'/
*@else
      data fname(5) /'diagint'/
*@endif
c
c     # sorted off-diagonal integrals (0,1,2 external).
      data nodint   /36/
*@ifdef ibmcms
*      data fname(6) /'ofdgint g'/
*@else
      data fname(6) /'ofdgint'/
*@endif
c
c     # four-external plus combination.
      data nfl4w    /41/
      data fname(7) /'fil4w'/
c
c     # four-external minus combinations.
      data nfl4x    /42/
      data fname(8) /'fil4x'/
c
c     # three-external plus combination.
      data nfl3w    /43/
      data fname(9) /'fil3w'/
c
c     # three-external minus combinations.
      data nfl3x     /44/
      data fname(10) /'fil3x'/
c
c     # temporary direct access file used for sorting.
      data ndascr    /20/
*@ifdef ibmcms
*      data fname(11) /'srtscr g'/
*@else
c     # this filename may not actually be used on all machines.
      data fname(11) /'srtscr'/
*@endif
c
c     # sort information file.
      data nsrtif    /50/
      data fname(12) /'cisrtif'/
c
c     # 2-e integral file [fsplit=2].
      data iunits(13) /12/
      data fname(13)  /'moints2'/
c
c     # standard irrep multiplication table.
c
      data mult /
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c     # buffer and record sizes for output files:
c     # maxbuf = record length for 3-, 4-external integrals.
c     # maxbl3 = 3-external internal buffer length.
c     # maxbl4 = 4-external internal buffer length.
c     #          usually maxbl4=maxbl3.
c     #          note that ( maxbuf.ge.maxbl4/2+2 ) must be satisfied.
c     # intmxo = 0-, 1-, and 2-external sequential file record length.
c     #          this is also used for the integral records of the
c     #          diagonal ints file.
c     #
c     #          note: intmxo is used as the record length to open the
c     #          diagonal and off diagonal d/a integral files. there
c     #          must be the same value in the diagonalization program
c     #          to correctly open these files.
c
*@if (defined sun && defined osu) || (defined cray && defined  osu)
*       data maxbuf/32767/, maxbl3/32767/, maxbl4/32767/, intmxo/765/
*@elif defined  ibmcms
*      data maxbuf/30000/, maxbl3/30000/, maxbl4/30000/, intmxo/4095/
*@elif defined  ibmmvs
*      data maxbuf/2383/, maxbl3/2383/, maxbl4/2383/, intmxo/1070/
*@else
       data maxbuf/32767/, maxbl3/16383/, maxbl4/16383/, intmxo/4095/
*@endif
c
c      # temporary da file record length paramaters.
c
*@ifdef ibmcms
*      data ldamin/512/, ldamax/4096/, ldainc/512/
*@elif defined  ibmmvs
*c     # fixed da record lengths.  mvs inherently requires this.
*      data ldamin/2932/, ldamax/2932/, ldainc/1/
*@else
c     # vax maximum is 4095
      data ldamin/127/, ldamax/4095/, ldainc/64/
*@endif
c
      end
c*deck driver
      subroutine driver( core, lencor, mem1, ifirst )
c
c  main driver routine for program "cisrt".
c
c  this program sorts the transformed integrals into a form suitable
c  for the unitary group approach multireference single and double
c  excitation direct configuration interaction program "ciudg".
c
c  written (in part) and maintained by:
c      ron shepard
c      theoretical chemistry group
c      chemistry division
c      argonne national laboratory
c      argonne, il 60439
c      internet: shepard@tcg.anl.gov
c
c  version log:
c  04-jul-01 fil3ex & fil4ex format changed for larger records. -rls
c  17-dec-90 support the new cidrtfl format. -rls
c  13-dec-90 SIFS version. -rls
c  31-may-90 fixed ifnot mdc blocks -eas
c  03-nov-89 ntitle added back to info file. -rls
c  02-feb-89 modified cms file names (hl)
c  03-oct-88 modified drt read to read irrep labels (eas)
c  30-sep-88 replaced i/o with unified primitives (eas)
c  07-sep-88 moved sort info from diag file to new sortinfo file (eas)
c  18-aug-88 extracted library routines (eas)
c  30-mar-88 extra (sequential) files for 4w,4x,3w,3x  (p.sz)
c  01-dec-87 osu integral file, ibm, unicos support added (dcc)
c  12-nov-87 nfrzc typo corrected (rls/fbb).
c  17-jun-87 ntitle added in setdg, timer+getime replaced (rls).
c  01-may-87 reorganization of some routines and data allocation (rls).
c  01-apr-87 vax version with cosmetic changes and cmdc blocks (-rls).
c  16-feb-87 cray cos version of osu program "trmain" adapted for the
c            current version of the cidrt 2.0 program. program "trmain"
c            performed both integral transformation and mrsdci sort and
c            was originally written by frank b. brown at osu and later
c            modified by reinhart ahlrichs, hans lischka, and others
c            for the cyber 205.  the cyber version was then modified by
c            isaiah shavitt, don comeau, and melanie pepper at osu in
c            creating a vectorized cray version of the mrsdci
c            diagonalization program.  -ron shepard
c
c  cmdc information:
c  keyword       description
c  ------------  -----------------------
c  argonne       argonne specific code
c  cray          cray code.
c  crayctss      ctss specific cray code.
c  vax           vax code.
c  fps           fps code.
c  ibm           ibm code.
c  ibmcms        specific cms code.
c  osu           ohio state specific code.
c  nonamelist    for non-namelist input machines.
c  sun           sun (3/4) specific code.
c
c  general comments:
c  * this program uses the nonstandard real*8 declarations for working
c  precision data types.  this works correctly on most machines and
c  avoids the requirement of changing "real" to "double precision" when
c  going from short precision machines to full precision machines.
c  * real parameter constants are specified with double precision
c  exponents.  this gives correct results in all cases, the truncation
c  being done at compile time, if necessary, instead of runtime.
c  * nonstandard namelist input is used in this program.  this is
c  supported on all of the anl machines.  it is relatively simple to
c  replace the namelist input with a list-directed read statement if
c  necessary.  such changes should be local to subroutine readin.
c
c**********************************************************************
c
c   the following computer programs contain work performed
c   partially or completely by the argonne national laboratory
c   theoretical chemistry group under the auspices of the office
c   of basic energy sciences, division of chemical sciences,
c   u.s. department of energy, under contract w-31-109-eng-38.
c
c   these programs may not be (re)distributed without the
c   written consent of the argonne theoretical chemistry group.
c
c   since these programs are under development, correct results
c   are not guaranteed
c
c**********************************************************************
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      ndrt
      equivalence (ndrt,iunits(3))
      integer      moints
      equivalence (moints,iunits(4))
      integer      ndgint
      equivalence (ndgint,iunits(5))
      integer      nodint
      equivalence (nodint,iunits(6))
      integer      nfl4w
      equivalence (nfl4w,iunits(7))
      integer      nfl4x
      equivalence (nfl4x,iunits(8))
      integer      nfl3w
      equivalence (nfl3w,iunits(9))
      integer      nfl3x
      equivalence (nfl3x,iunits(10))
      integer      ndascr
      equivalence (ndascr,iunits(11))
      integer      nsrtif
      equivalence (nsrtif,iunits(12))
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      character*60 fnamex
      equivalence(fname(nfilmx),fnamex)
c
      integer         ldamin, ldamax, ldainc
      common /cldain/ ldamin, ldamax, ldainc
c
      integer         maxbuf, maxbl3, maxbl4, intmxo
      common /cbufsz/ maxbuf, maxbl3, maxbl4, intmxo
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
      integer        mult
      common /cmult/ mult(8,8)
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      real*8          thresh
      common /thresh/ thresh
c
      integer         srtopt
      common /option/ srtopt(5)
      integer      prnopt
      equivalence( prnopt, srtopt(1) )
      integer      molcas
      equivalence( molcas, srtopt(3) )
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
*@ifdef spinorbit
      integer         istrtx,   numx
      common /ctypei/ istrtx(8),numx(8)
      logical spnorb, spnodd
      integer lxyzir
      common /solxyz/ spnorb, spnodd, lxyzir(3)
*@else
*      integer         istrtx,   numx
*      common /ctypei/ istrtx(5),numx(5)
*@endif
c
      integer     mx4x, mx3x, mx2x
      common /c9/ mx4x, mx3x, mx2x
c
c     # dummy:
      integer lencor, ifirst, mem1
      real*8 core(lencor)
c
c     # local:
      integer nmot, nsym, icisrt, itimer, i, nfile, ntitle, ierr, nmap,
     & nenrgy, ninfo, l1rec, n1max, l2rec, n2max, niot, nerror,
     & maxblo, itotal, iiorb, ndgtot, ndg0x, ndg1x, ndg2x, lcore1,
     & lcore2, numtot, moint2
      integer levsym(nmomx), map(nmomx)
      integer nmpsy(8)
c
      real*8 eref
c
c     # SIFS related arrays.
c
      character      slabel*4,  molab*8
      common /clabs/ slabel(8), molab(nmomx)
c
      integer    ntitmx
      parameter( ntitmx=21 )
      character*80 title(ntitmx)
c
      integer    nengmx
      parameter( nengmx=30 )
      integer ietype(nengmx)
      real*8 energy(nengmx)
c
      integer    ninfmx
      parameter( ninfmx=10 )
      integer info(ninfmx)
c
      integer mapin(nmomx)
      real*8 score, hcore
c
      integer idummy(1)
c
c     # pointers for array allocation.
      integer icd(50)
*@ifdef spinorbit
      integer  icd0, icdso, icdsox, icdsoy, icdsoz
*@endif
c
c     # timer operations.
      integer   tmin0
      parameter(tmin0=0)
      integer   tminit,  tmprt,  tmrein,  tmclr,  tmsusp,  tmresm
      parameter(tminit=1,tmprt=2,tmrein=3,tmclr=4,tmsusp=5,tmresm=6)
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      real*8     zero
      parameter( zero=0d0 )
c
      integer    two18
      parameter (two18 = 2**18)
c
c     # external functions used for space allocation.
c
      integer  forbyt, atebyt
      external forbyt, atebyt
c
      real*8   sifsce
      external sifsce
c
c molcas
c alle MOLCAS Variablen, die in die MOLCAS Bibliothek
c f�hren sind mit mc_ bezeichnet.
c
*@ifdef molcas_int64
      integer*8 mc_ntratoc, mc_ntrabuf, mc_ntibuf
      integer*8  mc_luonemo, mc_two, mc_idisk, mc_tconemo(1024),mc_sixf
      integer*8  mc_nsym, mc_nbpsy(8), mc_nmpsy(8), mc_nfro(8),
     .           mc_ndel(8)
      integer*8  mc_maxsym,mc_maxdim, mc_norbt
      integer    ntibuf
*@else
*      integer mc_ntratoc, mc_ntrabuf, mc_ntibuf
*      integer  mc_luonemo, mc_two, mc_idisk, mc_tconemo(1024),mc_sixf
*      integer  mc_nsym, mc_nbpsy(8), mc_nmpsy(8), mc_nfro(8),
*     .         mc_ndel(8)
*      integer  mc_maxsym,mc_maxdim, mc_norbt
*      integer  ntibuf
*@endif
      real*8 mc_ecor
      Parameter (mc_maxsym=8, mc_maxdim=2*4*2000)
      Parameter (mc_two=2, mc_sixf=64)
      Parameter(mc_nTraToc=106,mc_nTraBuf=9600,mc_ntibuf=mc_ntrabuf)
      integer nbpsy(8)
      integer i1,i2,i3,norbt
      character*4 bslbl(2,2000)


c
*@ifdef crayctss
*c     # create the dropfile and connect tty.
*      call link('unit99=tty//')
*@endif
c
      call ibummr( nlist )
c
      call timer(' ', tmin0,  icisrt, nlist )
      call timer(' ', tminit, itimer, nlist )
c
c     # set up the iim1(*) array.
      do 10 i = 1, nmomx
         iim1(i) = (i * (i - 1)) / 2
 10    continue
c
c     # set up the identity orbital mapping vector.
      do 20 i = 1, nmomx
         mapin(i) = i
 20    continue
c
c     # perform any necessary filename translations.
      nfile = 13
      call trnfln( nfile, fname )
c
c     # open the input and listing files.
*@ifdef ibm
*c     # use preconnected unit.
*@else
      open( unit=nlist, file=fname(1),  status='unknown' )
*@endif
      open( unit=nin,   file=fname(2),  status='old'     )
c
      write(nlist,6010)
 6010  format(/' program cisrt      '
     & //' transformed integral sort for the unitary group'
     & /' approach mrsdci diagonalization program.'
     & //' programmed (in part) by: ron shepard'
     & //' version date: 04-jul-01'/)
c
      call who2c( 'CISRT', nlist )
      call headwr (nlist, 'CISRT','5.6     ')
c
c     # mem1 and ifirst are not used in this routine, but are printed
c     # for informational purposes.
c
      write(nlist,6020) lencor, mem1, ifirst
 6020  format(/' workspace allocation information:',
     & ' lencor=',i10,' mem1=',i10,' ifirst=',i10)
c
c     # read the user input.
c
      call readin
      close( unit=nin )
c
c     # open the required sequential files.
c
      call openfl(maxbuf)
c
c     # read the header info from the transformed integral file.
c
c

      if (molcas.eq.0) then


      call sifrh1(
     & moints, ntitle, nsym,   nmot,
     & ninfo,  nenrgy, nmap,   ierr )
c
      if ( ierr .ne. 0 ) then
         call bummer('cisrt: from sifrh1, ierr=', ierr, faterr )
      elseif ( nmot .gt. nmomx ) then
         call bummer('cisrt: (nmot-nmomx)=', (nmot-nmomx), faterr )
       elseif ( ntitle .gt. ntitmx ) then
         call bummer('cisrt: (ntitle-ntitmx)=', (ntitle-ntitmx),faterr)
      elseif ( ninfo .gt. ninfmx ) then
         call bummer('cisrt: (ninfo-ninfmx)=', (ninfo-ninfmx), faterr )
      elseif ( nenrgy .gt. nengmx ) then
         call bummer('cisrt: (nenrgy-nengmx)=', (nenrgy-nengmx),faterr)
      endif
c
c     # ignore map vectors.
      nmap = 0
c
      call sifrh2(
     & moints, ntitle, nsym,   nmot,
     & ninfo,  nenrgy, nmap,   title,
     & nmpsy,  slabel, info,   molab,
     & ietype, energy, idummy, idummy,
     & ierr  )
c
      if ( ierr .ne. 0 ) then
         call bummer('cisrt: from sifrh2, ierr=', ierr, faterr )
      endif
c
      write(nlist,6100)'input integral file header information:'
      write(nlist,'(1x,a)') (title(i), i=1, ntitle)
c
      write(nlist,6100)'input energy(*) values:'
      call sifpre( nlist, nenrgy, energy, ietype )
c
      else         ! molcas
*@ifdef molcas
c include into molcas.inc
*
c
c from start.f
c
        call link_it()
        call getenvinit()
        call FIOInit()
c       call Fastio('TRACE=ON')
        call NameRun('RUNFILE')
        mc_luonemo=80
*@ifdef molcas_ext
*        call daname_(mc_luonemo,'TRAONE')
*@else
        call daname(mc_luonemo,'TRAONE')
*@endif
        mc_idisk=0
c  read header info from 1e MO file
*@ifdef molcas_ext
*         CALL WR_MOTRA_Info_(mc_luonemo,mc_two,mc_iDisk,
*     &                   mc_TCONEMO,mc_sixf,mc_ECOR,mc_NSYM,
*     &                   mc_nbpsy,mc_nmpsy,mc_NFRO,mc_NDEL,mc_maxsym,
*@else
         CALL WR_MOTRA_Info(mc_luonemo,mc_two,mc_iDisk,
     &                   mc_TCONEMO,mc_sixf,mc_ECOR,mc_NSYM,
     &                   mc_nbpsy,mc_nmpsy,mc_NFRO,mc_NDEL,mc_maxsym,
*@endif
     .                   BSLBL,mc_maxdim)
c  ecor = Enuc+Veff(core)+V1(core)+T1(core)
c  tconemo(1)= mo coefficient file
c  tconemo(2)= complete Hone including 2e contrib.
c  tconemo(3)= kinetic energy
c
        ntitle=1
        title(1)='MOTRA derived Integral File'
        nmot=0
        norbt=0
        nsym=mc_nsym
        do i=1,nsym
          nmpsy(i)=mc_nmpsy(i)
          nbpsy(i)=mc_nbpsy(i)
          nmot=nmot+nmpsy(i)
          norbt=norbt+nmpsy(i)*(nmpsy(i)+1)/2
        enddo
*
        call get_dscalar('PotNuc',energy(1))
        ietype(1)=-1
        ietype(2)=6
        energy(2)=mc_ecor-energy(1)
        hcore=mc_ecor-energy(1)
        nenrgy=2
c
c  create some symmetry labels
c
        do i=1,nsym
          write(slabel(i),'(a3,i1)') 'SYM',i
        enddo
*
c
c  create some basis labels
c
        do i=1,nmot
          write(molab(i),'(a2,1x,i3)') 'MO',i
        enddo
*@else
*      call bummer('not compiled for molcas',0,2)
*@endif

      endif
      write(nlist,6200)'total core energy =',
     & sifsce( nenrgy, energy, ietype )
c
      write(nlist,6040) nsym, nmot
c
      write(nlist,6050)'symmetry  =',(i,i=1,nsym)
      write(nlist,6060)'slabel(*) =',(slabel(i),i=1,nsym)
      write(nlist,6050)'nmpsy(*)  =',(nmpsy(i),i=1,nsym)
c
      if (molcas.eq.0)
     & write(nlist,6100)'info(*) =',(info(i),i=1,ninfo)
c
      write(nlist,6100)'orbital labels, i:molab(i)='
      write(nlist,6080)(i,molab(i),i=1,nmot)
c
c     # copy some record parameters for local use.
c
      if (molcas.eq.0) then
         l1rec = info(2)
         n1max = info(3)
         l2rec = info(4)
         n2max = info(5)
*@ifdef molcas
      else
         l1rec = norbt
         n1max = norbt
         l2rec = 1
         n2max = 1
*@endif
      endif
c
      write(nlist,6030) prnopt, ldamin, ldamax, ldainc,
     & maxbuf, maxbl3, maxbl4, intmxo
 6030  format(/' input parameters:'/
     & ' prnopt=',i3/
     & ' ldamin=',i8,' ldamax=',i8,' ldainc=',i8/
     & ' maxbuf=',i8,' maxbl3=',i8,' maxbl4=',i8,' intmxo=',i8 )
c
c     # read the required drt information.
c     # 1: ibuf
c
      icd(1) = 1
      ntitle = min( ntitmx, ntitle + 1 )
*@ifdef spinorbit
      call rddrt(
     & ndrt,   nsym,          nmot,   nlevel,
     & lowinl, niot,          levsym, map,
     & nmpsy,  title(ntitle), rmu,    niomx,
     & nmomx,  core(icd(1)),  spnorb, spnodd,
     & lxyzir  )
*@else
*      call rddrt(
*     & ndrt,   nsym,          nmot,   nlevel,
*     & lowinl, niot,          levsym, map,
*     & nmpsy,  title(ntitle), rmu,    niomx,
*     & nmomx,  core(icd(1)) )
*@endif
      close(unit=ndrt)
c
c     # eliminate redundant titles.
c
      call unique( ntitle, title )
c
c     # check buffer sizes for errors.
c
      nerror = 0
c
      if ( maxbl4 .ge. two18 ) then
       nerror = nerror + 1
       write(nlist,*)'maxbl4=',maxbl4,'must be less than 2**18=',two18
      endif
c
      if ( maxbl3 .ge. two18 ) then
       nerror = nerror + 1
       write(nlist,*)'maxbl3=',maxbl3,'must be less than 2**18=',two18
      endif
c
      if ( maxbuf*2 .lt. maxbl4+4 ) then
       nerror = nerror + 1
       write(nlist,*)
     &  '2*maxbuf must be .ge. maxbl4+4. maxbuf,maxbl4=',maxbuf,maxbl4
      endif
c
      if ( maxbuf*2 .lt. maxbl3+4 ) then
       nerror = nerror + 1
       write(nlist,*)
     &  '2*maxbuf must be .ge. maxbl3+4. maxbuf,maxbl3=',maxbuf,maxbl3
      endif
c
      if ( nerror .ne. 0 ) then
         write(nlist,*)'program stopping due to errors in buffer sizes'
         call bummer('cisrt: nerror=', nerror, faterr )
      endif
c
      maxblo = max( maxbl3, maxbl4, intmxo )
c
c     # read in one-electron hamiltonian and overlap matrices.
c     # 1:h, 2:s1, 3:h1, 4:buffer, 5:values, 6:labels, 7:symb
c
      nnlev = (nlevel * (nlevel + 1)) / 2
c
*@ifdef spinorbit
c
      icd0 = 1
      icdsox = icd0
      icdsoy = icdsox + atebyt( nnlev )
      icdsoz = icdsoy + atebyt( nnlev )
      icdso   = icdsoz + atebyt( nnlev )
      icd(1) = icd0
      if(spnorb)icd(1) = icdso
*@else
*      icd(1) = 1
*@endif
c
      icd(2) = icd(1) + atebyt( nnlev )
      icd(3) = icd(2) + atebyt( nnlev )
      icd(4) = icd(3) + atebyt( nnlev )
      icd(5) = icd(4) + atebyt( l1rec )
      icd(6) = icd(5) + atebyt( n1max )
      icd(7) = icd(6) + forbyt( 2*n1max )
c
      itotal = icd(7) + forbyt( nlevel ) - 1
c
      if ( itotal .gt. lencor ) then
         write(nlist,6090)'sifrsh',(icd(i),i=1,7),itotal
         call bummer('cisrt: sifrsh() (itotal-lencor)=',
     &    (itotal-lencor), faterr )
      endif

      if (molcas.eq.1) then
*@ifdef molcas
          mc_idisk=mc_tconemo(2)
c
        mc_norbt=norbt
*@ifdef molcas_ext
*        call ddafile_(mc_LUONEMO,mc_two,core(icd(2)+nnlev),
*     .             mc_norbt,mc_idisk)
*        call daclos_(mc_LUONEMO,'TRAONE')
*@else
        call ddafile(mc_LUONEMO,mc_two,core(icd(2)+nnlev),
     .             mc_norbt,mc_idisk)
        call daclos(mc_LUONEMO,'TRAONE')
*@endif
c    fill smatrix with unity matrix
          i3=-1
          do i1=1,nsym
           do i2=1,nmpsy(i1)
             i3=i3+i2
              core(icd(2)+i3)=1.0d0
           enddo
          enddo
c        if ( nenrgy .lt. nengmx ) then
c           nenrgy         = nenrgy + 1
c           energy(nenrgy) = energy(2)-energy(1)
c           ietype(nenrgy) = 6
            write(nlist,6140)'the integral file: h1_core=',
     .                       energy(2)
c        else
c           call bummer('cisrt: energy(*) overrun, nengmx=',
c    &       nengmx, faterr )
c        endif
c
*@endif
      else
      call sifrsh(
     & moints,       info,         core(icd(4)), core(icd(5)),
     & core(icd(6)), nsym,         nmpsy,        mapin,
     & nnlev,        core(icd(2)), score,        hcore,
     & core(icd(7)), ierr )
c
      if ( hcore .ne. zero ) then
c        # add hcore to the core energy list.
         if ( nenrgy .lt. nengmx ) then
            nenrgy         = nenrgy + 1
            energy(nenrgy) = hcore
            ietype(nenrgy) = 6
            write(nlist,6140)'the integral file: h1_core=', hcore
         else
            call bummer('cisrt: energy(*) overrun, nengmx=',
     &       nengmx, faterr )
         endif
      endif
      endif
c       call plblks('h1core ',core(icd(2)+nnlev),nsym,
c    .                nmpsy, 'MO',3,6)
c       call plblks('s1core ',core(icd(2)),nsym,
c    .                nmpsy, 'MO',3,6)
c
*@ifdef spinorbit
      if(.not.spnorb)goto 111
c
c     #  zero out the arrays to be used for the spin orbit integrals
c
      call wzero(nnlev, core(icdsox), 1)
      call wzero(nnlev, core(icdsoy), 1)
      call wzero(nnlev, core(icdsoz), 1)
c
c     #  read in the spin orbit integrals
c
      rewind moints
      call sifskh( moints, ierr)
      if(ierr .ne. 0)then
         call bummer('repositioning 1-e integral file, ierr= ',
     &    ierr, faterr)
      endif
c
      call rdso(
     & moints,       info,         nlevel,       map,
     & core(icd(4)), core(icd(5)), core(icd(6)),
     & core(icdsox), core(icdsoy), core(icdsoz) )

      write(nlist,112)(core(i), i = icdsox, icdsox + 3*nnlev -1)
112   format(1x, 10f8.5)
c
 111  continue
*@endif
c
c     # check s1(*) for validity, and compute h(*) indexed by
c     # level.
      call chksh(
     & nlist,        nlevel,       nsym,   nmpsy,
     & core(icd(2)), core(icd(3)), map,    iim1,
     & thresh,       core(icd(1)) )
c
c     # set up space for in-core arrays needed for the sort.
c     # 1:h:, 2:u, 3:idiagx, 4:lk432x, 5:lk1ex, 6:lk0ex, 7:ijkl, 8:ijsym
c
      iiorb  = (niot*(niot+1))/2
c
      icd(3) = icd(2) + atebyt( nnlev )
      icd(4) = icd(3) + forbyt( nnlev )
      icd(5) = icd(4) + forbyt( nnlev )
      icd(6) = icd(5) + forbyt( iiorb )
      icd(7) = icd(6) + forbyt( iiorb )
      icd(8) = icd(7) + forbyt( nsym*nnlev )
c
      itotal = icd(8) + forbyt(nnlev) - 1
      if ( itotal .gt. lencor ) then
         write(nlist,6090)'indxdg',(icd(i),i=1,8),itotal
         call bummer('cisrt: indxdg() (itotal-lencor)=',
     &    (itotal-lencor),faterr)
      endif
c
c     # set up indexing arrays for diagonal-contributing integrals.
c     # 1:h, 2:u, 3:idiagx, 4:lk432x, 5:lk1ex, 6:lk0ex, 7:ijkl, 8:ijsym
c
      call indxdg(nsym,levsym,core(icd(3)),ndgtot,ndg2x,ndg1x,ndg0x)
c
c     # set up indexing arrays for off-diagonal-contributing integrals.
c     # 1:h, 2:u, 3:idiagx, 4:lk432x, 5:lk1ex, 6:lk0ex, 7:ijkl, 8:ijsym
c
      call indxof(
     & nsym,         core(icd(4)), core(icd(5)), core(icd(6)),
     & core(icd(7)), core(icd(8)), levsym )
c
c     # compute the amount of core available during the first half-sort
c     # 1:h ... 7:ijkl ... bufin,vbufin,ibufin
c
c     lcore1 = lencor - (icd(7) + forbyt(nnlev) + atebyt(l2rec)
c    & +                 atebyt(n2max) + forbyt(4*n2max)) + 1
      lcore1 = lencor - (icd(7) + forbyt(nnlev) + atebyt(l2rec)
     & +                 atebyt(n2max) + forbyt(4*n2max))
c
c     # compute the core available during the second half-sort.
c     # 1:h, 2:outbuf, 3:bufw, 4:bufx ...
c
c     lcore2 = lencor
c    & - (icd(2) + atebyt(maxblo) + 2 * atebyt(maxbuf)) + 1
      lcore2 = lencor
     & - (icd(2) + atebyt(maxblo) + 2 * atebyt(maxbuf))
c
c     # compute the total number of off-diagonal integrals.
c
      numtot = numx(1) + numx(2) + numx(3) + numx(4) + numx(5)
c
c     # determine the da sorting parameters.
c
      call dalen( lcore1, lcore2, numtot, nbuk, lendar, nipbk, nipsg )
c
      write(nlist,6240) nbuk, lendar, nipbk, nipsg
 6240  format(/' intermediate da file sorting parameters:'/
     & ' nbuk=',i4,' lendar=',i8,' nipbk=',i8,' nipsg=',i10)
c
      call timer('setup required', tmrein, itimer, nlist )
c
c     # initialize the sorting file and sorting arrays.
c
c     # open the scratch da file.
      call openda( ndascr, fname(11), lendar, 'work', 'seqwrt' )
      call indast

      if (molcas.eq.0) then
c
c       # open and position the 2-e integral file.
        call sifo2f( moints, iunits(13), fname(13), info, moint2, ierr )
        if ( ierr .ne. 0 ) then
          call bummer('cisrt: sif2of, ierr=', ierr, faterr )
        endif
      endif
c
c    # begin the first half of the integral sort:
c    # read in and write out the transformed two-electron integrals and
c     # compute the repartitioning matrix u(*).
c     # 1:h, 2:u, 3:idiagx, 4:lk432x, 5:lk1ex, 6:lk0ex, 7:ijkl, 8:buk
c     # 9:lbuk, 10:dabuf, 11:bufin, 12:vbufin, 13:ibufin
c
      icd( 8) = icd( 7) + forbyt( nnlev )
      icd( 9) = icd( 8) + atebyt( nipbk*nbuk )
      icd(10) = icd( 9) + forbyt( nipbk*nbuk )
      icd(11) = icd(10) + atebyt( lendar )
      icd(12) = icd(11) + atebyt( l2rec )
      icd(13) = icd(12) + atebyt( n2max )
c
      itotal = icd(13) + forbyt(4*n2max) - 1
      if ( itotal .gt. lencor ) then
         write(nlist,6090)'pro2e',(icd(i),i=1,13),itotal
         call bummer('cisrt: pro2e() (itotal-lencor)=',
     &    (itotal-lencor),faterr)
      endif
c
      if (molcas.eq.0) then
      call pro2e(
     & map,           core(icd(2)),  core(icd(3)),  core(icd(4)),
     & core(icd(5)),  core(icd(6)),  core(icd(7)),  core(icd(8)),
     & core(icd(9)),  core(icd(10)), core(icd(11)), core(icd(12)),
     & core(icd(13)), moint2,        info )
c

c     # close the 2-e integral file.
      call sifc2f( moint2, info, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('cisrt: sifc2f: ierr=', ierr, faterr )
      endif
c
c     # close the 1-e integral file.
      close(unit=moints)
*@ifdef molcas
      else
      ntibuf=mc_ntibuf
      itotal = icd(11) + atebyt(ntibuf)
      if ( itotal .gt. lencor ) then
         write(nlist,6090)'pro2emolcas',(icd(i),i=1,11),itotal
         call bummer('cisrt: pro2emolcas() (itotal-lencor)=',
     &    (itotal-lencor),faterr)
      endif
      call pro2emolcas(
     & map,           core(icd(2)),  core(icd(3)),  core(icd(4)),
     & core(icd(5)),  core(icd(6)),  core(icd(7)),  core(icd(8)),
     & core(icd(9)),  core(icd(10)), core(icd(11)),nmpsy,nsym )
*@endif
      endif
c
c     # modify and write out the transformed one-electron integrals.
c
*@ifdef spinorbit
      call pro1e(
     & eref,         core(icd(1)), core(icd(2)),  core(icd(3)),
     & core(icd(4)), core(icd(5)), core(icd(6)),  core(icd(7)),
     & core(icd(8)), core(icd(9)), core(icd(10)), levsym,
     & core(icdsox), nsym )
*@else
*      call pro1e(
*     & eref,         core(icd(1)), core(icd(2)),  core(icd(3)),
*     & core(icd(4)), core(icd(5)), core(icd(6)),  core(icd(7)),
*     & core(icd(8)), core(icd(9)), core(icd(10)), levsym)
*@endif
c
      call fndast( core(icd(8)), core(icd(9)), core(icd(10)) )
      call closda( ndascr, 'keep' , 2 )
c
      call timer('first half-sort required', tmrein, itimer, nlist )
c
c     # write out the header info describing the sorted integral lists
c     # to the info file.
c
      if ( eref .ne. zero ) then
c        # add eref to the core energy list.
         if ( nenrgy .lt. nengmx ) then
            nenrgy         = nenrgy + 1
            energy(nenrgy) = eref
            ietype(nenrgy) = 5
            write(nlist,6140)
     &       'the hamiltonian repartitioning, eref=', eref
         else
            call bummer('cisrt: energy(*) overrun, nengmx=',
     &       nengmx, faterr )
         endif
      endif
c
      call wrtinf(
     & ntitle, title,  nenrgy, energy,
     & ietype, nsrtif, intmxo, ndg2x,
     & ndg1x,  ndg0x,  maxbuf )
c
      close (unit=nsrtif)
c
      call puti( ndgint, intmxo, fname(5) )
c
c     # begin the second half sort of the integrals:
c     # open the intermediate da file for reading.
c
      call openda( ndascr, fname(11), lendar, 'work', 'random' )
c
c     # write out the sorted diagonal integrals.
c     # 1:h, 2:u, 3:idiagx, 4:outbuf, 5:dabuf, 6:idabuf, 7:pints
c
      icd(5) =icd(4) + atebyt( intmxo )
      icd(6) =icd(5) + atebyt( lendar )
ctm cf. pcisrt2.f
ctm   icd(7) =icd(6) + atebyt( nipbk  )
c     icd(7) =icd(6) + atebyt( ((nipbk+1)/2)*2 )
      icd(7) =icd(6) + atebyt( nipbk+1 )
ctm
c
      call putdg(
     & core(icd(7)), core(icd(1)), core(icd(3)), ndg2x,
     & ndg1x,        ndg0x,        core(icd(4)), core(icd(5)),
     & core(icd(6)) )
c
      call putf
c
      close(unit=ndgint)
c
      write(nlist,*)'diagonal integral file completed.'
c
c     # write out the sorted off-diagonal integrals.
c     # 1:h, 2:outbuf, 3:dabuf, 4:idabuf, 5:bufw, 6:bufx, 7:pints
c
      icd(3) =icd(2) + atebyt( maxblo )
      icd(4) =icd(3) + atebyt( lendar )
cmd   icd(5) =icd(4) + forbyt( nipbk  )
      icd(5) =icd(4) + forbyt( nipbk+1)
cmd
      icd(6) =icd(5) + atebyt( maxbuf )
      icd(7) =icd(6) + atebyt( maxbuf )
c
      itotal = icd(7) + atebyt(nipsg) -1
      if(itotal.gt.lencor)then
         write(nlist,6090)'ptofdg',(icd(i),i=1,7),itotal
         call bummer('cisrt: ptofdg() (itotal-lencor)=',
     &    (itotal-lencor),faterr)
      endif
c
c     # set up sorting files:
c     # nodint for 0-,1-, and 2-external
c     # nfl34c for 3- and 4-external integrals.
c
      call puti( nodint, intmxo, fname(6) )
c
      call ptofdg(
     & nsym,         maxbl4,       maxbl3,       intmxo,
     & niot,         maxbuf,       core(icd(1)), core(icd(3)),
     & core(icd(4)), core(icd(2)), core(icd(5)), core(icd(6)),
     & core(icd(7)), levsym(lowinl) )
c
c     # all done. close files and return.
c
      call closda( ndascr, 'delete', idummy )
      call putf
      write(nlist,*)'off-diagonal files sort completed.'
c
c
      call timer('second half-sort required', tmclr, itimer, nlist )
      call timer('cisrt complete',            tmclr, icisrt, nlist )
      return
c
 6040  format(/' nsym =',i2,' nmot=',i4/)
 6050  format(1x,a,8i5)
 6060  format(1x,a,8(1x,a4))
 6080  format(10(i4,':',a8))
 6090  format(' program stopping due to space allocation error.'/
     & 1x,a,(1x,10i8))
 6100  format(/1x,a,(1x,10i10))
 6140  format(/' new core energy added to the energy(*) list.'
     & /' from ',a,1pe20.12)
 6200  format(/1x,a,(1x,1p4e20.12))
      end
*@ifdef spinorbit
c*deck rdso
      subroutine rdso(
     & moints, info,   nlevel, map,
     & buff1,  value1, label1,
     & hx,     hy,     hz)
c
c  read in the soin orbit integrals, store the level indexed integral
c  in core(icdsox), core(icdsoy) and core(icdsoz)
c
c     input:
c     moints  = spin orbit integral file
c     nlevel = number of levels.  also the number of orbitals.
c     nndx(1:nmot) = i*(i-1)/2 addressing array.
c     map(1:nmot) = input_orbital-to-level mapping vector.
c
c     output:
c     hx, hy, hz = spin orbit integrals indexed by level
c
      implicit logical(a-z)
c
      integer moints, nlevel
c     integer nndx(nlevel), map(*), label1(2,*), info(*)
      integer nndx(200), map(*), label1(2,*), info(*)
      real*8 hx(*), hy(*), hz(*), buff1(*), value1(*), fcorex
c
      integer nipv, iretbv, num, last, itypea, itypeb, ifmt,
     & ibvtyp, idummy(2), ierr
c
      integer ilab, jlab, milab, mjlab, i
c
c     local
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      integer   msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
c
c
c     # set up indexing array
c
      do 100 i = 1, nlevel
         nndx(i) = i * (i -1) /2
100   continue
c
      nipv = 2
      iretbv = 0
      last = msame
c
c  read in the spin orbit from unit moints
c
c  do while ( last .ne. nomore )
c
   10 if ( last .ne. nomore ) then
        call sifrd1(moints,info,nipv,iretbv,buff1,num,last,itypea,
     &    itypeb,ibvtyp,value1,label1,fcorex,idummy,ierr)
        if( ierr.ne.0 ) then
          call bummer(' from sifrd1, ierr =', ierr, faterr )
        endif
c
        if(itypea.eq.2) then
           if(itypeb.eq.0)then
              do 30 i = 1,num
                 ilab = label1(1,i)
                 jlab = label1(2,i)
                 milab = map(ilab)
                 mjlab = map(jlab)
                 if ( milab .le. mjlab ) then
                    hx(nndx(mjlab) + milab) = value1(i)
                 else
                    hx(nndx(milab) + mjlab) = -value1(i)
                 endif
   30         continue
c
           elseif(itypeb.eq.1)then
              do 40 i = 1,num
                 ilab = label1(1,i)
                 jlab = label1(2,i)
                 milab = map(ilab)
                 mjlab = map(jlab)
                 if ( milab .le. mjlab ) then
                    hy(nndx(mjlab) + milab) = value1(i)
                 else
                    hy(nndx(milab) + mjlab) = -value1(i)
                 endif
   40         continue
c
           elseif(itypeb.eq.2)then
              do 50 i = 1,num
                 ilab = label1(1,i)
                 jlab = label1(2,i)
                 milab = map(ilab)
                 mjlab = map(jlab)
                 if ( milab .le. mjlab ) then
                    hz(nndx(mjlab) + milab) = value1(i)
                 else
                    hz(nndx(milab) + mjlab) = -value1(i)
                 endif
   50         continue
c
           else
              goto 10
           endif
        endif
        go to 10
      endif
      return
      end
*@endif
c deck indast
      subroutine indast
c
c  initialize direct access sorting arrays.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      integer i
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      if(nbuk.gt.nbkmx)then
         write(nlist,6010)nbkmx,nbuk
         call bummer('indast: (nbuk-nbkmx)=',(nbuk-nbkmx),faterr)
      endif
c
      do 10 i = 1, nbkmx
         ibufpt(i) = 0
         irecpt(i) = 0
10    continue
c
c     # initialize irec to point to the first available record.
      irec = 1
c
      return
6010  format(/' *** error *** indast: nbkmx,nbuk=',2i8)
      end
c deck fndast
      subroutine fndast( buf, ibuf, dabuf )
c
c  dump any nonempty buckets.
c
      implicit logical(a-z)
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      real*8 buf(nipbk,nbuk), dabuf(lendar)
      integer ibuf(nipbk,nbuk)
c
      integer ibuk
c
      do 10 ibuk = 1, nbuk
         if ( ibufpt(ibuk) .ne. 0 ) then
            call wrtda( ibuk, buf, ibuf, dabuf )
         endif
10    continue
c
      return
      end
c deck wrtda
      subroutine wrtda( ibuk, buf, ibuf, dabuf )
c
c  put integrals and labels in the da buffer and write it out.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      ndascr
      equivalence (ndascr,iunits(11))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
c     # dummy:
      integer ibuk
      real*8 buf(nipbk,nbuk),dabuf(lendar)
      integer ibuf(nipbk,nbuk)
c
c     # local:
      integer unpack(2), num, last
      equivalence (unpack(1),last), (unpack(2),num)
c
      num  = ibufpt(ibuk)
      last = irecpt(ibuk)
c
c     # copy the integrals.
c
c     call dcopyx( num, buf(1,ibuk), 1, dabuf, 1 )
      call wzero(num,dabuf,1)
c     dabuf=0.0d0
      call dcopy_wr( num, buf(1,ibuk), 1, dabuf, 1 )
c
c     # pack the labels.
c
      call plab32( dabuf(nipbk+1), ibuf(1,ibuk), num )
c
c     # pack the record info into the last buffer word.
c
      call plab32( dabuf(lendar), unpack, 2 )
c
c     # write the da buffer to the next record and update pointers.
c
      irecpt(ibuk) = irec
c
      call writda( ndascr, irec, dabuf, lendar )
c
c     # writda() does not increment irec, so do it here.
      irec = irec + 1
c
      ibufpt(ibuk) = 0
c
      return
      end


      subroutine dcopyx(n,x,incx,y,incy)
      implicit none 
       integer n,incx,incy
      real*8 x(*),y(*)
      integer i,j
       if (incx.ne.1 .or. incy.ne.1) then
         call bummer('dopyx: incx,incy<>1',0,2)
       endif
       do i=1,n
         y(i)=x(i)
       enddo
       return
       end



c deck dalen
      subroutine dalen(
     & lcore1, lcore2, numtot, nbuk, lendar, nipbk, nipsg )
c
c  determine the direct access sorting parameters.
c
c  input:
c  lcore1 = amount of space available for integral buckets, label
c           buckets, and a single da buffer during first half sort.
c  lcore2 = amount of space available for integrals, a single da
c           buffer and unpacked label buffer during second half sort.
c  numtot = total number of off-diagonal integrals to be sorted.
c
c  output:
c  nbuk = number of buckets to be allocated for the sort.
c  lendar = direct access record length.
c  nipbk = number of integerals in each da record.
c  nipsg = number of integrals in each segment. must be divisible by 3.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer         ldamin, ldamax, ldainc
      common /cldain/ ldamin, ldamax, ldainc
c
c     # dummy:
      integer lcore1, lcore2, numtot, nbuk, lendar, nipbk, nipsg
c
c     # local:
      integer len, av2, req1
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
c     # make sure the record length is between ldamin and ldamax.  use
c     # ldainc as record length increment.
c
c     # external functions:
c     # nipbkf(l) = the number of integrals and packed labels that fit
c     #             in a record of length=l.
c     # atebyt(n) = number of words required to hold n integrals.
c     # forbyt(n) = number of words required to hold n unpacked labels.
c
c     # loop over the record lengths backwards and return the first time
c     # everything fits.
c
      integer  nipbkf, forbyt, atebyt
      external nipbkf, forbyt, atebyt
c
      do 10 len = ldamax, ldamin, -ldainc
         lendar = len
         nipbk = nipbkf(lendar)
         av2 = lcore2 -(atebyt(lendar) + forbyt(nipbk))
         if ( av2 .le. 0 ) go to 10
         nipsg = (av2/3)*3
         nbuk = (numtot-1) / nipsg + 2
         req1 = atebyt(nipbk*nbuk) + forbyt(nipbk*nbuk) + atebyt(lendar)
c        # check for success.
         if ( req1 .le. lcore1 ) return
10    continue
c     # exit from loop means not enough space is available.
c
      write(nlist,6010) lcore1, lcore2, lendar, req1, nbuk
      call bummer('dalen: (req1-lcore1)=',(req1-lcore1),faterr)
c
6010  format(/' *** error *** dalen: larger workspace required for',
     & ' efficient integral sorting:'/
     & ' lcore1 =',i8/
     & ' lcore2 =',i8/
     & ' ldamin =',i8/
     & ' req1   =',i8/
     & ' nbuk   =',i8)
      end
c deck nipbkf
      integer function nipbkf(l)
c
c  compute the number of integrals that can be held in a da buffer of
c  length=l given in working precision words.  the buffer must hold
c  integrals, packed_integer labels, and two packed_integer values for
c  record and chaining info.
c
      implicit logical(a-z)
      integer l
c
      nipbkf = ((l - 1) * 2) / 3
c
      return
      end
c deck rddrt
*@ifdef spinorbit
      subroutine rddrt(
     & ndrt,   nsym,   nmot,   nlevel,
     & lowinl, niot,   levsym, map,
     & nmpsy,  title,  rmu,    niomx,
     & nmomx,  ibuf,   spnorb, spnodd,
     & lxyzir )
*@else
*      subroutine rddrt(
*     & ndrt,   nsym,   nmot,   nlevel,
*     & lowinl, niot,   levsym, map,
*     & nmpsy,  title,  rmu,    niomx,
*     & nmomx,  ibuf)
*@endif
c
c  read the first few records of the formatted drt file to
c  extract the sorting information.  redundant info is checked
c  for consistency.
c
c  input:
c  ndrt = drt unit number.
c  nsym = number of symmetry blocks on the integral file.
c  nmot = total number of orbitals from the integral file.
c  nmpsy(*) = number of orbitals in each symmetry block.
c  ibuf(*) = integer buffer for reading drt arrays.
c
c  output:
c  nlevel = number of levels.
c  lowinl = lowest internal level.
c  niot = number of internal levels.
c  levsym(*) = symmetry of each level.
c  map(*) = orbital-to-level mapping vector.
c  title = drt title.
c  rmu(*) = reference occupations.
c
c  14-dec-90 version=3 drt changes. -rls
c  05-jan-90 nmomx check added. -rls
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer         srtopt
      common /option/ srtopt(5)
c
c     # dummy:
      integer ndrt, nsym, nmot, nlevel, lowinl, niot, niomx, nmomx
      character*80 title
      integer map(*), nmpsy(8), levsym(nmot), ibuf(*)
      real*8 rmu(*)
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif
c
c     # local:
      integer nerror, lidrt, lenbuf, vrsion, ioerr, nmotd, nsymd,
     & nfctd, nfvtd, i, nfctx, nfvtx, ipt, isym
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      nerror = 0
c
      rewind ndrt
      read(ndrt,'(a)',iostat=ioerr)title
      if ( ioerr .ne. 0 ) then
         call bummer('rddrt: title ioerr=',ioerr,faterr)
      endif
*@ifdef spinorbit
      read(ndrt,'(2L4,3I4)',iostat=ioerr)spnorb, spnodd, lxyzir
      if ( ioerr .ne. 0 ) then
         call bummer('rddrt: title ioerr=',ioerr,faterr)
      endif
*@endif
c
c     # get the number of header integers.
c
      call rddbl( ndrt, 3, ibuf, 3 )
      vrsion = ibuf(1)
      lenbuf = ibuf(2)
      lidrt  = ibuf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 4 ) then
         write(nlist,*)
     &    '*** warning: rdhdrt: drt version .ne. 4 *** vrsion=',vrsion
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning: rdhdrt: drt version .ne. 3 *** vrsion=',vrsion
*      endif
*@endif
c
c     # read the header integers.
c
      call rddbl( ndrt, lenbuf, ibuf, lidrt )
c
c     # check some variables for consistency.
c
      nmotd = ibuf(1)
      if ( nmot .gt. nmotd ) then
         write(nlist,6010)'nmot',nmot,nmotd
         nerror = nerror + 1
      endif
      if ( nmotd .gt. nmomx ) then
         write(nlist,6010)'nmomx',nmot,nmomx
         nerror = nerror + 1
      endif
c
      nsymd = ibuf(6)
      if ( nsym .ne. nsymd ) then
         write(nlist,6011)'nsym',nsym,nsymd
         nsym   = nsymd
         nerror = nerror + 1
      endif
c
      niot = ibuf(2)
*@ifdef spinorbit
      if(spnodd)niot = niot - 1
*@endif
      if ( niot .gt. niomx ) then
         write(nlist,6010)'niot',niot,niomx
         nerror = nerror + 1
      endif
c
      nfctd = ibuf(3)
      nfvtd = ibuf(4)
c
c     # skip over symmetry labels.
      call rddbl( ndrt, lenbuf, ibuf, -nsym )
c
c     # read the orbital-to-level mapping vector, map(*).
c
      call rddbl( ndrt, lenbuf, map, nmotd )
c
c     # read the reference occupations for the internal orbitals mu(*).
c     # use ibuf(*) as a buffer then convert into rmu(*).
c
      call rddbl( ndrt, lenbuf, ibuf, niot )
      do 10 i = 1, niot
         rmu(i) = ( ibuf(i) )
 10    continue
c
c     # read nmpsy(*) on the drt (includes frozen orbitals) and compare
c     # to the input values.
c     # use ibuf(*) as a buffer.
c
      call rddbl( ndrt, lenbuf, ibuf, nsym )
      do 20 i = 1, nsym
         if ( nmpsy(i) .gt. ibuf(i) ) then
            write(nlist,6010)'nmpsy(*)',i,nmpsy(i),ibuf(i)
            nerror = nerror + 1
         endif
 20    continue
c
c     # map(*) includes frozen core and frozen virtual orbitals.
c     # compress to reference only the retained orbitals that are to be
c     # sorted and store in ibuf(*).
c
      nfctx = 0
      nfvtx = 0
      nlevel = 0
      do 30 i = 1, nmotd
         if ( map(i) .lt. 0 ) then
            nfctx = nfctx + 1
         elseif ( map(i) .eq. 0 ) then
            nfvtx = nfvtx + 1
         else
            nlevel       = nlevel + 1
            ibuf(nlevel) = map(i)
         endif
 30    continue
c
c     # check that the number of levels agrees with the number of
c     # orbitals on the transformed integrals file.
c
      if ( nlevel .ne. nmot ) then
         write(nlist,6010)'nlevel',nlevel,nmot
         nerror = nerror + 1
      endif
c
c     # check frozen orbitals for internal consistency.
c
      if ( nfvtd .ne. nfvtx ) then
         write(nlist,6010)'nfvt',nfvtd,nfvtx
         nerror = nerror + 1
      endif
      if ( nfctd .ne. nfctx ) then
         write(nlist,6010)'nfct',nfctd,nfctx
         nerror = nerror + 1
      endif
      if ( niot .le. 0 ) then
         write(nlist,6010)'niot',niot
         nerror = nerror + 1
      endif
c
      lowinl = nmot - niot + 1
c
c     # use the compressed map(*) to compute levsym(*).
c
      ipt = 0
      do 50 isym = 1, nsym
         do 40 i = 1, nmpsy(isym)
            ipt = ipt + 1
            levsym(ibuf(ipt)) = isym
 40       continue
 50    continue
c
c     # write out drt info.
c
      write(nlist,6030) title
      write(nlist,6040) nmotd, nfctd, nfvtd, nmot, nlevel, niot, lowinl
      write(nlist,6050)'orbital-to-level map(*)',(map(i),i=1,nmotd)
      write(nlist,6050)'compressed map(*)',(ibuf(i),i=1,nlevel)
      write(nlist,6050)'levsym(*)',(levsym(i),i=1,nlevel)
      write(nlist,6060)(rmu(i),i=1,niot)
c
c     # return the compressed map(*).
      do 60 i = 1, nlevel
         map(i) = ibuf(i)
 60    continue
c
      if ( nerror .ne. 0 ) then
         write(nlist,6020)
         call bummer('rddrt: nerror=', nerror, faterr )
      endif
c
      return
c
 6010  format(' *** rddrt error *** ',a,3i8)
 6011  format(' *** rddrt warning *** ',a,3i8)
 6020  format(/' program stopping due to inconsistencies on the',
     & ' drt file.')
 6030  format(/' drt information:'/(1x,a))
 6040  format(' nmotd =',i4,' nfctd =',i4,' nfvtc =',i4,' nmot  =',i4/
     & ' nlevel =',i4,' niot  =',i4,' lowinl=',i4)
 6050  format(1x,a/(1x,20i4))
 6060  format(' repartitioning mu(*)='/(1x,20f4.0))
      end
c*deck chksh
      subroutine chksh(
     & nlist,  nlevel, nsym,   nmpsy,
     & s1,     h1,     map,    nndx,
     & thresh, hout )
c
c  check the overlap matrix for errors, and copy the symmetry-blocked
c  h1(*) array to the level-indexed hout(*) array.
c
c  input:
c  nlist  = output listing file.
c  nlevel = number of levels.  also the number of orbitals.
c  nsym   = number of symmetry blocks.
c  nmpsy(1:nsym) = number of orbitals in each symmetry block.
c  s1(*) = input overlap matrix.
c  h1(*) = input hamiltonian matrix.
c  map(1:nmot) = input_orbital-to-level mapping vector.
c  nndx(1:nmot) = i*(i-1)/2 addressing array.
c  thresh = overlap matrix element accuracy threshold.  this value is
c           actually scaled by nmot to allow for random errors in
c           computing the overlap matrix.  consequently, thresh is
c           consistent with the accuracy of the original overlap
c           matrix in the ao basis.
c
c  output:
c  hout(*) = h1(*) after application of map(*).
c
c  14-dec-90 written by ron shepard.
c
      implicit logical(a-z)
c
      integer nlist, nlevel, nsym
      integer nmpsy(nsym), map(nlevel), nndx(nlevel)
      real*8 thresh, s1(*), h1(*), hout(*)
c
c     # local:
      integer nerror, p0, isym, p, mp, q, mq, pq, mpq
      real*8 small
c
      real*8     one
      parameter( one=1d0 )
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
c     # set the tolerance level.
      small = (nlevel) * thresh
c
c     # initialize hout(*).
      call wzero( ((nlevel * (nlevel + 1)) / 2), hout, 1 )
c
      nerror = 0
      pq     = 0
      p0     = 0
      do 30 isym = 1, nsym
         do 20 p = 1, nmpsy(isym)
            mp = map( p0 + p )
            do 10 q = 1, (p-1)
c              # off-diagonal terms.
               pq = pq + 1
               if ( abs(s1(pq)) .gt. small ) then
                  write(nlist,6010) isym, p, q, s1(pq)
                  nerror = nerror + 1
               endif
               mq = map( p0 + q )
               mpq = nndx( max( mp, mq ) ) + min( mp, mq )
               hout(mpq) = h1(pq)
 10          continue
c           # diagonal terms.
            pq = pq + 1
            if ( abs( s1(pq) - one ) .gt. small ) then
               write(nlist,6010) isym, p, p, s1(pq)
               nerror = nerror + 1
            endif
            mpq = nndx( mp ) +  mp
            hout(mpq) = h1(pq)
 20       continue
         p0 = p0 + nmpsy(isym)
 30    continue
c
      if ( nerror .ne. 0 ) then
         call bummer('chksh: nerror=', nerror, faterr )
      endif
c
      return
 6010  format(' isym,p,q,s1(pq)=',3i4,1pe25.15)
      end
c*deck openfl
      subroutine openfl(maxbuf)
      implicit integer ( a-z )
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      integer      maxbuf
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      ndrt
      equivalence (ndrt,iunits(3))
      integer      moints
      equivalence (moints,iunits(4))
      integer      ndgint
      equivalence (ndgint,iunits(5))
      integer      nfl4w
      equivalence (nfl4w,iunits(7))
      integer      nfl4x
      equivalence (nfl4x,iunits(8))
      integer      nfl3w
      equivalence (nfl3w,iunits(9))
      integer      nfl3x
      equivalence (nfl3x,iunits(10))
      integer      nsrtif
      equivalence (nsrtif,iunits(12))
c
      character*60 fnamex
      equivalence(fname(nfilmx),fnamex)
c
c     # open some of the sequential files required by the program.
c     # filenames are printed for all files.
c
      write(nlist,*)
      inquire(unit=nlist,name=fnamex)
      write(nlist,*) 'openfl: maxbuf set to ',maxbuf

      write(nlist,6010)nlist,'listing file',fnamex
c
      write(nlist,6010)nlist,'input file',fname(2)
c
      open(unit=ndrt,file=fname(3),status='old')
      inquire(unit=ndrt,name=fnamex)
      write(nlist,6010)ndrt,'cidrt file',fnamex
c
      open(unit=moints,file=fname(4),status='old',form='unformatted')
      inquire(unit=moints,name=fnamex)
      write(nlist,6010)moints,'transformed integrals file',fnamex
c
      write(nlist,6010)nlist,'diagonal integral file',fname(5)
c
      write(nlist,6010)nlist,'off-diagonal integral file',fname(6)
      call openda(nfl4w, fname(7), maxbuf, 'keep', 'seqwrt' )
        inquire(unit=nfl4w,name=fnamex)
        write(nlist,6010)nfl4w,'4-external w integrals file',fnamex
      call openda(nfl4x, fname(8), maxbuf, 'keep', 'seqwrt' )
        inquire(unit=nfl4x,name=fnamex)
        write(nlist,6010)nfl4x,'4-external x integrals file',fnamex
      call openda(nfl3w, fname(9), maxbuf, 'keep', 'seqwrt' )
        inquire(unit=nfl3w,name=fnamex)
        write(nlist,6010)nfl3w,'3-external w integrals file',fnamex
      call openda(nfl3x, fname(10), maxbuf, 'keep', 'seqwrt' )
        inquire(unit=nfl3x,name=fnamex)
        write(nlist,6010)nfl3x,'3-external x integrals file',fnamex
c
      write(nlist,6010)nlist,'scratch da sorting file',fname(11)
c
      open(unit=nsrtif,file=fname(12),status='unknown',
     &form='unformatted')
      inquire(unit=nsrtif,name=fnamex)
      write(nlist,6010)nsrtif,'cisrt info file',fnamex
c
      write(nlist,6010)nlist,'2-e integral file [fsplit=2]',fname(13)
c
      return
 6010  format(' (',i2,') ',a,':',t40,a)
      end
c*deck readin
      subroutine readin
c
c  read program input and file names.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
c     # note that the last filename is used for temporary scratch.
c
      character*60 fnamex
      equivalence(fname(nfilmx),fnamex)
c
      integer         srtopt
      common /option/ srtopt(5)
      integer      prnopt
      equivalence( prnopt, srtopt(1) )
      integer      molcas
      equivalence( molcas, srtopt(3) )
c
      integer         ldamin, ldamax, ldainc
      common /cldain/ ldamin, ldamax, ldainc
c
      integer         maxbuf, maxbl3, maxbl4, intmxo
      common /cbufsz/ maxbuf, maxbl3, maxbl4, intmxo
c
      real*8          thresh
      common /thresh/ thresh
c
c     # local:
      integer i, ierr, ifile
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
*@ifndef nonamelist
      namelist /input/
     & srtopt, ldamin, ldamax, ldainc,
     & maxbuf, maxbl3, maxbl4, intmxo,
     & prnopt, molcas, thresh
*@endif
c
*@ifdef crayctss
*c     # setup for normal namelist.
*      call ddiopon('ibm')
*@endif
c
c     # set the default values for input options.
      do 10 i = 1, 5
         srtopt(i)=0
 10    continue
c
c     # prnopt == srtopt(1)
c     # prnopt = 0    normal print.
c     #        .ge.1  print additional indexing arrays.
c     #        .ge.2  print one-electron arrays, s1(*),h1(*),u(*).
c     #
c     # srtopt(2)     obsolete option. [previously anl/osu file format.
c     #
c     # srtopt(3)     1   read MOLCAS integral files
c     #               0   default use sifs files
c     # srtopt(3:5)   reserved for future use.
c
c     # set the default overlap matrix threshold [see chksh()].
      thresh = 1.d-12
c
      write(nlist,6010)
 6010  format(/' echo of the input for program cisrt:')
c
      call echoin( nin, nlist, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('readin: from echoin, ierr=', ierr, faterr )
      endif

      if (molcas.eq.1) then
         write(6,*) ' reading MOLCAS integral files .... '
      endif
c
      rewind nin
c
*@ifdef nonamelist
*c all defaults taken, example input
*c       /prnopt
*c , ,   /ldamin, ldamax, ldainc
*c , , , /maxbuf, maxbl3, maxbl4, intmxo
*c       /thresh
*c
*      read (nin,*) prnopt
*      read (nin,*) ldamin, ldamax, ldainc
*      read (nin,*) maxbuf, maxbl3, maxbl4, intmxo
*      read (nin,*) thresh
*@elif defined  nml_required
*      read( nin, nml=input, end=3 )
*@else
      read( nin, input, end=3 )
*@endif
c
c     # read new filenames if any.
c
*@if defined( unix) || defined ( vms) || (defined (sun) && defined( os4))
c   and vms codes use filename translations.
c  sun4 compiler bug prevents reading filenames. -rls
*@else
*100   continue
*         read (nin,*,end=3)ifile, fnamex
*         if(ifile.ge.3 .and. ifile.lt.(nfilmx-1) .and. fnamex.ne.' ')
*     &    fname(ifile) = fnamex
*      go to 100
*@endif
c
 3     continue
c
      return
      end
c*deck pro1e
*@ifdef spinorbit
      subroutine pro1e(
     & eref,   h,      u,      idiagx,
     & lk432x, lk1ex,  lk0ex,  ijkl,
     & buf,    ibuf,   dabuf,  levsym,
     & soint,  nsym )
*@else
*      subroutine pro1e(
*     & eref,   h,      u,      idiagx,
*     & lk432x, lk1ex,  lk0ex,  ijkl,
*     & buf,    ibuf,   dabuf,  levsym)
*@endif
c
c  process the 1-electron integrals.
c
      implicit logical(a-z)
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      integer         srtopt
      common /option/ srtopt(5)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
*@ifdef spinorbit
c
      integer         istrtx,   numx
      common /ctypei/ istrtx(8),numx(8)
c
      integer        mult
      common /cmult/ mult(8,8)
c
      integer         nepsy
      common /iextnm/ nepsy(8)
c
      integer nmbpr(8), lmda(8,8), lmdb(8,8), nmbj
c
      integer isym, jsym, lksym, j
c
      integer lxyz
      logical spnorb, spnodd
      integer lxyzir
      common /solxyz/ spnorb, spnodd, lxyzir(3)
c
      integer istsym(8), imin, imax, jmin, jmax
c
      integer nsym
      real*8 soint(*)
c
c
c
*@endif
c     # dummy:
      real*8 eref,h(nnlev), u(nnlev), buf(nipbk,nbuk), dabuf(lendar)
      integer idiagx(nnlev), lk432x(nnlev), lk1ex(*), lk0ex(*),
     & ijkl(nnlev), ibuf(nipbk,nbuk), levsym(nlevel)
c
c     # local:
      integer hstuoc, i, ilev, nndx, kl, k, l, nndxij, ijadd, nndxlk,
     & lad, ibuk
      real*8 valx
c
      real*8    zero,     two,     half
      parameter(zero=0d0, two=2d0, half=5d-1)
c
      hstuoc = lowinl - 1
c
c     # calculate eref
c
      eref = zero
      i = 1
      do 401 ilev = lowinl, nlevel
         nndx = iim1(ilev) + ilev
         eref = eref + rmu(i) * (h(nndx) + h(nndx) + u(nndx))
         eref = eref - rmu(i) * (two - rmu(i)) * g(i)
         i = i + 1
 401   continue
      eref = half * eref
      write (nlist,'(a,1pe25.15)') ' pro1e: eref =',eref
c
c     # modify the one-electron integrals  (mh(i)=h(i)+u(i))
c
      do 410 i = 1, nnlev
         h(i) = h(i) + u(i)
 410   continue
c
      if(srtopt(1).ge.2)call plblkt(
     & 'modified h1(*) integrals', h, nlevel, 'lev ', 1, nlist )
c
c     # write out the 1-e integrals to the intermediate da file.
c
      kl=0
      do 400 k = 1, nlevel
         do 390 l =1, k
            kl=kl+1
            if(levsym(l).ne.levsym(k))go to 390
            valx=h(kl)
            if(k.eq.l) go to 505
            nndxij=iim1(k)+l
            ijadd=ijkl(nndxij)+2
            if(k.gt.hstuoc) go to 450
            nndxlk=iim1(k)+k
            lad=lk432x(nndxlk)+ijadd
            go to 475
 450         nndxlk=iim1(k-hstuoc)+k-hstuoc
            if(l.gt.hstuoc) go to 460
            lad=lk1ex(nndxlk)+ijadd
            go to 475
 460         lad=lk0ex(nndxlk)+ijadd
 475         ibuk=(lad-1)/nipsg+2
            ibufpt(ibuk)=ibufpt(ibuk)+1
            buf(ibufpt(ibuk),ibuk)=valx
            ibuf(ibufpt(ibuk),ibuk)=lad
            if(ibufpt(ibuk).eq.nipbk) then
               call wrtda( ibuk, buf, ibuf, dabuf )
            endif
            go to 390
 505         ibuk=1
            nndx=iim1(k)+k
            lad=idiagx(nndx)+1
            ibufpt(ibuk)=ibufpt(ibuk)+1
            buf(ibufpt(ibuk),ibuk)=valx
            ibuf(ibufpt(ibuk),ibuk)=lad
            if(ibufpt(ibuk).eq.nipbk) then
               call wrtda( ibuk, buf, ibuf, dabuf )
            endif
 390      continue
 400   continue
*@ifdef spinorbit
      if(.not.spnorb)return
c
c     # lmdb(i,j) lmda(i,j) symmetry pairs defined as
c     # lmd(i)=lmdb(i,j)*lmda(i,j), j=nmdpr(i)
c     # lmdb(i,j).ge.lmda(i,j), lmdb(i,j).lt.lmdb(i,j+1)
c     # nmbpr(i) number of pairs
c
      do 9300 i = 1, nsym
         nmbj=0
         do 9301 j = 1, nsym
            if(nepsy(j) .eq. 0)goto 9301
            lksym = mult(i, j)
            if(nepsy(lksym) .eq. 0) goto 9301
            if(lksym .gt. j)goto 9301
            nmbj = nmbj + 1
            lmdb(i,nmbj) = j
            lmda(i,nmbj) = lksym
9301     continue
         nmbpr(i) = nmbj
9300  continue
c
c     #set up the offset of each external symmetry block
c
      istsym(1) = 1
      do 101 i = 1, nsym
         if(i .eq. 1)goto 101
         istsym(i) = istsym(i-1) + nepsy(i-1)
101   continue
c
c     # write out the 0-ext so integrals to the intermediate da file.
c
      lad = istrtx(6) - 1
c      write(*,*)"********soint********"
c      write(*,"(10f8.5)")(soint(i), i=1,3*nnlev)
      if(srtopt(1).ge.2)then
         call plblkt(
     &      'hsox(*)', soint,            nlevel, 'lev ', 1, nlist )
         call plblkt(
     &      'hsoy(*)', soint(nnlev+1),   nlevel, 'lev ', 1, nlist )
         call plblkt(
     &      'hsoz(*)', soint(2*nnlev+1), nlevel, 'lev ', 1, nlist )
      endif
      do 6000 j = lowinl, nlevel
         jsym = levsym(j)
         do 6100 i = lowinl, j-1
            isym = levsym(i)
            lksym = mult(isym, jsym)
            do 6200 lxyz = 1, 3
               if( lxyzir(lxyz) .ne. lksym )goto 6200
               lad = lad + 1
               nndxij = iim1(j) + i + (lxyz - 1) * nnlev
               valx = soint(nndxij)
               ibuk=(lad-1)/nipsg+2
               ibufpt(ibuk)=ibufpt(ibuk)+1
               buf(ibufpt(ibuk),ibuk)=valx
               ibuf(ibufpt(ibuk),ibuk)=lad
               if(ibufpt(ibuk).eq.nipbk) then
                  call wrtda( ibuk, buf, ibuf, dabuf )
               endif
6200        continue
6100     continue
6000  continue
c
c     # write out the 1-ext so integrals to the intermediate da file.
c
      lad = istrtx(7) - 1
      do 5000 j = lowinl, nlevel
         jsym = levsym(j)
         do 5100 lxyz = 1, 3
            isym = mult(jsym, lxyzir(lxyz))
            if( nepsy(isym) .eq.0 )goto 5100
            imin = istsym(isym)
            imax = istsym(isym) + nepsy(isym) - 1
            do 5200 i = imin, imax
               lad = lad + 1
               nndxij = (lxyz - 1) * nnlev + iim1(j) + i
               valx = soint(nndxij)
               ibuk=(lad-1)/nipsg+2
               ibufpt(ibuk)=ibufpt(ibuk)+1
               buf(ibufpt(ibuk),ibuk)=valx
               ibuf(ibufpt(ibuk),ibuk)=lad
               if(ibufpt(ibuk).eq.nipbk) then
                  call wrtda( ibuk, buf, ibuf, dabuf )
               endif
5200        continue
5100     continue
5000  continue
c
c     # write out the 2-ext so integrals to the intermediate da file.
c
      lad = istrtx(8) - 1
      do 7000 lxyz = 1, 3
         lksym = lxyzir(lxyz)
         if(nmbpr(lksym) .eq. 0)goto 7000
         do 7001 k = 1, nmbpr(lksym)
            isym = lmda(lksym, k)
            jsym = lmdb(lksym, k)
            jmin = istsym(jsym)
            jmax = istsym(jsym) + nepsy(jsym) - 1
            imin = istsym(isym)
            imax = istsym(isym) + nepsy(isym) - 1
            do 7002 j = jmin, jmax
               if(isym .eq. jsym)then
                  imax = j
               endif
               do 7003 i = imin, imax
                  lad = lad + 1
                  nndxij = (lxyz - 1) * nnlev + iim1(j) + i
                  valx = soint(nndxij)
                  ibuk=(lad-1)/nipsg+2
                  ibufpt(ibuk)=ibufpt(ibuk)+1
                  buf(ibufpt(ibuk),ibuk)=valx
                  ibuf(ibufpt(ibuk),ibuk)=lad
                  if(ibufpt(ibuk).eq.nipbk) then
                     call wrtda( ibuk, buf, ibuf, dabuf )
                  endif
7003           continue
7002        continue
7001     continue
7000  continue
*@endif
      return
      end
c*deck pro2e
      subroutine pro2e(
     & map,    u,      idiagx, lk432x,
     & lk1ex,  lk0ex,  ijkl,   buk,
     & lbuk,   dabuf,  bufin,  vbufin,
     & lbufin, moint2, info  )
c
c  read the 2-e integrals from the input file, and write them to
c  the intermediate da file.
c
c  14-dec-90 SIFS version with async i/o. -rls
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      integer         srtopt
      common /option/ srtopt(5)
c
*@ifdef spinorbit
      integer         istrtx,   numx
      common /ctypei/ istrtx(8),numx(8)
*@else
*      integer         istrtx,   numx
*      common /ctypei/ istrtx(5),numx(5)
*@endif
c
      integer    nipv
      parameter( nipv=4 )
c
c     # dummy:
      integer moint2
      integer map(nlevel), idiagx(nnlev), lk432x(nnlev),
     & lk1ex(*), lk0ex(*), ijkl(nnlev), lbuk(nipbk,nbuk),
     & lbufin(nipv,*), info(*)
      real*8 u(nnlev), buk(nipbk,nbuk), bufin(*), vbufin(*),
     & dabuf(lendar)
c
c     # local:
      integer hstuoc, nrec, numtot, reqnum, ierr, num, ibvtyp, ifmt,
     & itypea, itypeb, last, iint, i, j, k, l, ito, ii, jj, kk, ll,
     & iijj, kkll, temp, nndx, lad, nndxij, ijadd, nndxlk, nndxr,
     & ibuk, itox
      integer idummy(1)
      real*8 valx
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      integer    iwait,   iretbv,   msame
      parameter( iwait=0, iretbv=0, msame=0 )
c
      real*8    half
      parameter(half=5d-1)
c
c     # initialize some variables.
c
      call wzero( nnlev, u, 1 )
      hstuoc = lowinl - 1
      nrec   = 0
      numtot = 0
c
c     # initiate the read of the first record.
c
       call sifr2( moint2, iwait, info, bufin, reqnum, ierr )
       if ( ierr .ne. 0 ) then
         call bummer('pro2e: initial sifr2, ierr=', ierr, faterr )
       endif
c
c     # loop over integral records.
c
 100   continue
c
c     # wait for the previous read to complete.
      call sif2w8( moint2, info, reqnum, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('pro2e: from sif2w8, ierr=', ierr, faterr )
      endif

c
c     # decode the integral buffer.
      call sifd2(
     & info,   nipv,   iretbv, bufin,
     & num,    last,   itypea, itypeb,
     & ibvtyp, vbufin, lbufin,
     & idummy, ierr )
c
c     # intitate the next read.
      if ( last .eq. msame ) then
         call sifr2( moint2, iwait, info, bufin, reqnum, ierr )
         if ( ierr .ne. 0 ) then
            call bummer('pro2e: sifr2, ierr=', ierr, faterr )
         endif
      endif

c
      nrec   = nrec + 1
      numtot = numtot + num
c
      do 2000 iint = 1, num
c
         i    = lbufin(1,iint)
         j    = lbufin(2,iint)
         k    = lbufin(3,iint)
         l    = lbufin(4,iint)
         valx = vbufin(iint)
c
c        # map the orbital labels and canonicalize.
         ito = 1
         ii = max(map(i),map(j))
         jj = min(map(i),map(j))
         kk = max(map(k),map(l))
         ll = min(map(k),map(l))
c
         iijj = iim1(ii)+jj
         kkll = iim1(kk)+ll
         if(iijj.lt.kkll)then
            temp = ii
            ii = kk
            kk = temp
            temp = jj
            jj = ll
            ll = temp
         endif
c
         k = kk
         l = ll
         if ( ii .ne. jj ) go to 1300
         if ( k  .ne. l  ) go to 1200
         if ( ii .ne. k  ) go to 1100
c
c        # (ii:ii)
c
         nndx = iim1(ii)+ii
         lad = idiagx(nndx)
         ibuk = 1
         ibufpt(ibuk) = ibufpt(ibuk)+1
         buk(ibufpt(ibuk),ibuk) = valx
         lbuk(ibufpt(ibuk),ibuk) = lad
         if(ibufpt(ibuk).eq.nipbk) then
            call wrtda( ibuk, buk, lbuk, dabuf )
         endif
         if(ii.le.hstuoc) go to 2000
         g(ii-hstuoc) = half*valx
         u(nndx)  =  u(nndx) + rmu(ii-hstuoc) * g(ii-hstuoc)
         go to 2000
c
c        # (ii:ll)
c
 1100     nndx = iim1(ii)+k
         lad = idiagx(nndx)
         ibuk = 1
         ibufpt(ibuk) = ibufpt(ibuk)+1
         buk(ibufpt(ibuk),ibuk) = valx
         lbuk(ibufpt(ibuk),ibuk) = lad
         if(ibufpt(ibuk).eq.nipbk) then
            call wrtda( ibuk, buk, lbuk, dabuf )
         endif
         nndxij = iim1(k)+k
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1120
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         if(ii.le.hstuoc) go to 1800
         u(nndxij) = u(nndxij) + valx * rmu(ii-hstuoc)
         go to 1800
 1120     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxlk = iim1(ii)+ii
         u(nndxlk) = u(nndxlk) + rmu( k-hstuoc) * valx
         u(nndxij) = u(nndxij) + rmu(ii-hstuoc) * valx
         go to 1800
c
 1200     if(ii.ne.k) go to 1260
c
c        # (ii:il)
c
         nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)+1
         if(ii.gt.hstuoc) go to 1220
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1220     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         u(nndxij) = u(nndxij) + half * valx * rmu(ii-hstuoc)
         if(l.gt.hstuoc) go to 1225
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1225     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (ii,kl)
c
 1260     nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)
         if(ii.le.hstuoc) go to 1265
         u(nndxij) = u(nndxij) + rmu(ii-hstuoc) * valx
         if(k.gt.hstuoc) go to 1270
 1265     nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1270     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         if(l.gt.hstuoc) go to 1280
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1280     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
 1300     if(k.ne.l) go to 1400
         if(jj-k) 1340,1320,1350
c
c        # (il:ll)
c
 1320     nndxij = iim1(ii)+l
         ijadd = ijkl(nndxij)
         if(ii.gt.hstuoc) go to 1330
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1330     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         if(l.gt.hstuoc) go to 1335
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1335     lad = lk0ex(nndxlk)+ijadd
         u(nndxij) = u(nndxij) + half * valx * rmu(l-hstuoc)
         go to 1800
c
c        # (il:kk)
c
 1340     nndxij = iim1(k)+jj
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1344
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1344     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         nndxr = iim1(ii)+jj
         u(nndxr) = u(nndxr) + valx * rmu(k-hstuoc)
         if(jj.gt.hstuoc) go to 1348
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1348     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (ij:ll)
c
 1350     nndxij = iim1(k)+k
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1360
         nndxlk = iim1(ii)+jj
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1360     nndxlk = iim1(ii-hstuoc)+jj-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxr = iim1(ii)+jj
         u(nndxr) = u(nndxr) + rmu(k-hstuoc) * valx
         go to 1800
c
 1400     if(jj.ne.l) go to 1500
         if(ii.ne.k) go to 1450
c
c        # (il:il)
c
         nndx = iim1(ii)+jj
         lad = idiagx(nndx)+1
         ibuk = 1
         ibufpt(ibuk) = ibufpt(ibuk)+1
         buk(ibufpt(ibuk),ibuk) = valx
         lbuk(ibufpt(ibuk),ibuk) = lad
         if(ibufpt(ibuk).eq.nipbk) then
            call wrtda( ibuk, buk, lbuk, dabuf )
         endif
         nndxij = iim1(jj)+jj
         ijadd = ijkl(nndxij)+1
         ito = 2
         if(ii.le.hstuoc) go to 1415
         u(nndxij) = u(nndxij) - half * valx * rmu(ii-hstuoc)
         if(jj.gt.hstuoc) go to 1420
 1415     nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1420     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxr = iim1(ii)+ii
         u(nndxr) = u(nndxr) - half * valx * rmu(jj-hstuoc)
         go to 1800
c
c        # (il:jl)
c
 1450     nndxij = iim1(jj)+jj
         ijadd = ijkl(nndxij)+1
         ito = 2
         if(jj.gt.hstuoc)  go to 1460
         if(k.le.hstuoc) ito = 2
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1460     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxr = iim1(ii)+k
         u(nndxr) = u(nndxr) - half * valx * rmu(l-hstuoc)
         go to 1800
c
 1500     if(jj-k) 1600,1520,1540
c
c        # (ik:kl)
c
 1520     nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)+1
         if(k.gt.hstuoc) go to 1530
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         ito = 2
         go to 1800
 1530     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         nndxr = iim1(ii)+l
         u(nndxr) = u(nndxr) - half * valx * rmu(k-hstuoc)
         ito = 2
         if(l.gt.hstuoc) go to 1535
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1535     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (ij:kl)
c
 1540     nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1544
         nndxlk = iim1(ii)+jj
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1544     nndxlk = iim1(ii-hstuoc)+jj-hstuoc
         if(l.gt.hstuoc) go to 1548
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1548     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
 1600     if(ii.ne.k) go to 1650
c
c        # (ik:il)
c
         nndxij = iim1(jj)+l
         ijadd = ijkl(nndxij)+1
         ito = 2
         if(ii.le.hstuoc) go to 1605
         u(nndxij) = u(nndxij) - half * valx * rmu(ii-hstuoc)
         if(jj.gt.hstuoc) go to 1610
 1605     if(ii.le.hstuoc) ito = 2
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1610     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         if(l.gt.hstuoc) go to 1615
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1615     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
 1650     if(jj.lt.l) go to 1700
c
c        # (ik:jl)
c
         nndxij = iim1(jj)+l
         ijadd = ijkl(nndxij)+1
         if(jj.gt.hstuoc) go to 1660
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1660     nndxlk = iim1(ii-hstuoc) + k-hstuoc
         if(l.gt.hstuoc) go to 1665
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1665     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (il:jk)
c
 1700     nndxij = iim1(l)+jj
         ijadd = ijkl(nndxij)+2
         if(l.gt.hstuoc) go to 1710
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1710     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         if(jj.gt.hstuoc) go to 1715
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1715     lad = lk0ex(nndxlk)+ijadd
c
c        # process the integrals.
c
 1800     continue
         ibuk = (lad - 1) / nipsg + 2
         do 1900 itox = 1, ito
            ibufpt(ibuk)            = ibufpt(ibuk) + 1
            buk(ibufpt(ibuk),ibuk)  = valx
            lbuk(ibufpt(ibuk),ibuk) = lad
            if ( ibufpt(ibuk) .eq. nipbk ) then
               call wrtda( ibuk, buk, lbuk, dabuf )
            endif
            lad = lad + 1
 1900     continue
c
 2000  continue
c
c     # another buffer?.
      if( last .eq. msame ) go to 100
c
      write (nlist,6010) numtot, nrec
 6010  format(/' pro2e:',i10,' integrals read in',i6,' records.')
c
      if ( srtopt(1) .ge. 2 ) then
         write (nlist,6020) (g(i), i = 1, (nlevel - hstuoc) )
         call plblkt('u(*) matrix', u, nlevel,'lev ', 1, nlist)
      endif
c
      return
 6020  format(' pro2e: g(*) = ',/(10f13.8))
      end
c*deck wrtinf
      subroutine wrtinf(
     & ntitle, title,  nenrgy, energy,
     & ietype, nsrtif, intmxo, ndg2x,
     & ndg1x,  ndg0x,  maxbuf )
c
c  write the sorted integral info file.
c
c  14-dec-90 nenrgy,energy(*),ietype(*) added. -rls
c  03-nov-89 ntitle added back to the first record. -rls
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer         srtopt
      common /option/ srtopt(5)
c
      integer         nepsy
      common /iextnm/ nepsy(8)
c
*@ifdef spinorbit
      integer         istrtx,   numx
      common /ctypei/ istrtx(8),numx(8)
*@else
*      integer         istrtx,   numx
*      common /ctypei/ istrtx(5),numx(5)
*@endif
c
      integer     mx4x, mx3x, mx2x
      common /c9/ mx4x, mx3x, mx2x
c
c     # dummy:
c
      integer ntitle, nenrgy, nsrtif, intmxo,
     & ndg2x, ndg1x, ndg0x, maxbuf
c
      character*80 title(ntitle)
c
      integer ietype(nenrgy)
c
      real*8 energy(nenrgy)
c
c     # local:
c
c     # vrsion is an internal version number for the output files.
c     # this parameter should be incremented whenever possible
c     # incompatibilities between cisrt and ciudg are introduced.
c     #
c     # vrsion history:
c     # 14-dec-90 1: nenrgy, energy(*), and ietype(*) added. -rls
c
      integer    vrsion
      parameter( vrsion=1 )
c
c     # titles and energies.
c
      write (nsrtif) vrsion, ntitle, nenrgy
      write (nsrtif) title, energy, ietype
c
c     # write out the third record with integral count info.
c
      write (nsrtif)
     & intmxo, ndg2x, ndg1x, ndg0x, numx,
     & mx4x,   mx3x,  mx2x,  nepsy, maxbuf
c
      if ( srtopt(1) .ge. 1 ) then
         write (nlist,'(a,i8)') ' maxbuf 4ext,3ext', maxbuf
         write (nlist,6010) ndg2x, ndg1x, ndg0x, numx,
     &    mx4x, mx3x, mx2x, nepsy
      endif
c
      return
 6010  format(' setdg: diag 2x,1x,0x =',3i10/
     & ' off-diag 4x,3x,2x,1x,0x =',5i10/' mx4x,mx3x,mx2x =',3i10/
     & ' nepsy =',8i10)
      end
ccdeck putdg
      subroutine putdg(
     & pints,  h,     idiagx, ndg2x,
     & ndg1x,  ndg0x, outbuf, dabuf,
     & idabuf  )
c
c  this subroutine writes out the integrals that contribute to diagonal
c  hamiltonian matrix elements.
c  the output file must be initialized and positioned prior to the call
c  to this routine.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
      integer         ibuk, iblkst, ntot2e, maxrd
      common /crdbks/ ibuk, iblkst, ntot2e, maxrd
c
c     # dummy:
      integer ndg2x, ndg1x, ndg0x
      real*8 pints(*), h(*), dabuf(*), outbuf(*)
      integer idiagx(*), idabuf(*)
c
      integer ihuoc, i, j, jstrt, ipos
      real*8 temp
c
      ihuoc = lowinl - 1
c
c     # collect the diagonal 1-electron integrals, move them to the
c     # output buffer, and dump the output buffer.
c
      do 10 i = 1, nlevel
         pints(i) = h(iim1(i+1))
 10    continue
      call puto( outbuf, pints, nlevel )
      call putd( outbuf )
c
c     # read in the diagonal 2-e integrals stored in the first
c     # sorted integral segment.
c
      ibuk   = 1
      ntot2e = ndg2x + ndg1x + ndg0x
      call rdbks( pints, dabuf, idabuf )
c
c     # replace d=(ii|jj) and x=(ij|ij) ints with the (d+x) and (d-x)
c     # linear combinations for all external indices.
c
      do 82 j = 2, ihuoc
         jstrt = iim1(j)
         do 80 i = 1, (j-1)
            ipos = idiagx(jstrt+i)
            temp = pints(ipos)
            pints(ipos)   = temp + pints(ipos+1)
            pints(ipos+1) = temp - pints(ipos+1)
 80       continue
 82    continue
c
c     # write out all-external integrals.
c
      call puto( outbuf, pints, ndg2x )
      call putd( outbuf )
c
c     # write out half-external integrals.
c
      ipos = ndg2x + 1
      call puto( outbuf, pints(ipos), ndg1x )
      call putd( outbuf )
c
c     # write out all-internal integrals
c
      ipos = ipos + ndg1x
      call puto( outbuf, pints(ipos), ndg0x )
      call putd( outbuf )
c
      return
      end
c*deck ptofdg
      subroutine ptofdg(
     & nsym,   maxbl4, maxbl3, intmxo,
     & niot,   maxbuf, h,      dabuf,
     & idabuf, outint, bufw,   bufx,
     & pints,  ilevsm )
c
c  this subroutine controls the writing of the off-diagonal
c  sorted integral files.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nfl4w
      equivalence (nfl4w,iunits(7))
      integer      nfl4x
      equivalence (nfl4x,iunits(8))
      integer      nfl3w
      equivalence (nfl3w,iunits(9))
      integer      nfl3x
      equivalence (nfl3x,iunits(10))
c
      integer         ibuk, iblkst, ntot2e, maxrd
      common /crdbks/ ibuk, iblkst, ntot2e, maxrd
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
      integer        mult
      common /cmult/ mult(8,8)
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      integer         nepsy
      common /iextnm/ nepsy(8)
c
*@ifdef spinorbit
      integer         istrtx,   numx
      common /ctypei/ istrtx(8),numx(8)
*@else
*      integer         istrtx,   numx
*      common /ctypei/ istrtx(5),numx(5)
*@endif
c
c     # note:  outint(*) is of length intmxo in this routine, of length
c     #        maxbl4 in prep4e, and of length maxbl3 in prep3e.  the
c     #        space actually allocated should accomodate all of these
c     #        cases.
c
c     # dummy:
      integer nsym, maxbl4, maxbl3, intmxo, niot, maxbuf
      real*8 outint(intmxo)
      real*8 h(nnlev), dabuf(*), pints(nipsg)
      real*8 bufw(maxbuf), bufx(maxbuf)
      integer idabuf(*), ilevsm(niot)
c
c     # local:
      integer num1e, maxorb, isym, minorb, l, lstrt, k, itppt,
     & incnt, numin, num, ipos
c
*@ifdef spinorbit
c
      logical spnorb, spnodd
      integer lxyzir
      common /solxyz/ spnorb, spnodd, lxyzir(3)
      integer ntppt, i
c
*@endif
c
c     # compress external 1-e integrals into symmetry-packed order.
c
      num1e  = 0
      maxorb = 0
      do 60 isym = 1, nsym
         if ( nepsy(isym) .eq. 0 ) go to 60
         minorb = maxorb + 1
         maxorb = maxorb + nepsy(isym)
         do 40 l = minorb, maxorb
            lstrt = iim1(l)
            do 30 k = minorb, l
               num1e = num1e + 1
               h( num1e ) = h( lstrt + k )
 30          continue
 40       continue
c
 60    continue
c
c     # read in and write out the sorted two-electron integrals.
c     # 0-, 1-, and 2-external integrals go to a sequential file and ar
c     # read using sequential io in the diagonalization program.
c     # 3-, and 4-external integrals are written to separate sequential
c     # files
c
      ntot2e = numx(1) + numx(2) + numx(3) + numx(4) + numx(5)
c
*@ifdef spinorbit
c
      if(spnorb)ntot2e = ntot2e + numx(6) + numx(7) + numx(8)
c
*@endif
c
c     # ibuk=1 ints have already been processed.  start with the ibuk=2
c     # segment and continue processing until all nbuk segments have
c     # been transferred from the intermediate direct access file to
c     # the final output files.
c
      ibuk  = 2
      itppt = 1
 1010  call rdbks( pints, dabuf, idabuf )
      if(itppt.gt.2)go to 610
c
c     # initialize output file for 4-external integral combinations.
c
      call puti34( maxbuf, nfl4x, nfl4w )
c
c     # write out the sorted 4-external integral combinations.
c
c     # incnt = offset for the first integral in the current segment.
c     # numin = address of the last integral biased by the current
c     #         iblkst.
c     # num = address of the last integral in the current segment.
c
      incnt = 0
      numin = numx(1)
      num   = min( numin, maxrd )
      call prep4e(
     & pints,  outint, bufw,   bufx,
     & dabuf,  idabuf, nsym,   nepsy,
     & maxbl4, maxbuf, incnt,  numin,
     & num  )
c
c     # close the output file for 4-external integral combinations.
c
      call putf34( nfl4x, nfl4w )
c
c     # initialize output file for 3-external integral combinations.
c
      call puti34( maxbuf, nfl3x, nfl3w )
c
c     # write out the sorted 3-external integral combinations.
c
      numin = incnt + numx(2)
      num   = min( numin, maxrd )
c
      call prep3e(
     & pints,  outint, bufw,   bufx,
     & dabuf,  idabuf, nsym,   nepsy,
     & maxbl3, ilevsm, niot,   maxbuf,
     & incnt,  numin,  num )
c
c     # close the output file for 3-external integral combinations
c
      call putf34( nfl3x, nfl3w )
c
c     # write out the sorted one-electron integrals.
c
      call puto( outint, h, num1e )
      call putd( outint )
c
      itppt = 3
c
c     # write out the sorted 2-,1-, and 0-external integrals.
*@ifdef spinorbit
c     # and the so integerals
c
      ntppt = 5
      if(spnorb)ntppt = 8
c
*@endif
c
c
 600   istrtx(itppt) = istrtx(itppt) + iblkst
 610   if( istrtx(itppt)+numx(itppt)-1 .le. maxrd ) go to 650
      num         = maxrd - istrtx(itppt) + 1
      numx(itppt) = numx(itppt) - num
      ipos        = istrtx( itppt )
*@ifdef spinorbit
      if(itppt .ge. 6)then
c         write(*,"(10f8.5)")(pints(i), i = ipos, ipos + num - 1)
      endif
*@endif
      call puto( outint, pints(ipos), num )
      istrtx(itppt) = 1
      go to 1000
c
 650   num=numx(itppt)
      ipos = istrtx( itppt )
*@ifdef spinorbit
      if(itppt .ge. 6)then
c         write(*,"(10f8.5)")(pints(i), i = ipos, ipos + num - 1)
      endif
*@endif
      call puto( outint, pints(ipos), num )
      call putd( outint )
      itppt = itppt + 1
*@ifdef spinorbit
      if ( itppt .le. ntppt ) go to 600
*@else
*      if ( itppt .le. 5 ) go to 600
*@endif
 1000  continue
      if ( ibuk .le. nbuk ) go to 1010
      return
      end
c*deck prep4e
      subroutine prep4e(
     & pints,  wxints, bufw,   bufx,
     & dabuf,  idabuf, nmsym,  nepsy,
     & maxbl4, maxbuf, incnt,  numin,
     & num )
c
c  determine the block structure of the 4-external integrals and write
c  the blocks to the appropriate file.  the integrals are first read
c  into a segment buffer in triple-sort order into the array pints(*).
c  linear combinations of these integrals are formed and placed into
c  the array wxints(*).  the combinations are then moved into the
c  buffers bufw(*) and bufx(*) and written out as they are filled.
c  each combination block consists of all possible i and j indices
c  consistent with the parameters lsym, ksym, lstart, lfinal,
c  kstart, and kfinal.  these parameters are written at the beginning
c  of the block into two working precision words.  the array wxints(*)
c  must hold at least one set of ij indices and should hold several so
c  as to minimize the two-word overhead and the subsequent processing
c  overhead in the diagonalization program.
c  blocks are not split between buffers.  the expected wasted space in
c  each buffer is therefore half of the average block size.  see the
c  put*34() routines for more details of the da buffer processing.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer        mult
      common /cmult/ mult(8,8)
c
      integer         srtopt
      common /option/ srtopt(5)
c
c     # dummy:
      integer maxbl4, maxbuf, incnt, numin, num
      real*8 pints(*),wxints(maxbl4),dabuf(*),bufw(maxbuf),bufx(maxbuf)
      integer sym12(8),sym21(8),nepsy(8),idabuf(*)
c
c     # local:
      integer ioffs, mx4int, i, l1strt, k1strt, finbl, numout,
     & icontw, icontx, iblock, ictodw, ictodx, n4ext, cntx12, cntw3,
     & cntw12, cntx3, cntwx, cw3od, cw12od, cx12od, cx3od, cwxod,
     & lsym, nmsym, nl, l1, smabcd, ksym, nk, ks, ls, kls,
     & k1, symchk, jsym, nj, js, is, isym, ni, nij, lfinod, kfinod,
     & icow, icox
c
      real*8    two
      parameter(two=2d0)
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      real*8  sqrt2
c
c     # ioffs = number of words in each block to reserve for block
c     #         parameters.
c     # mx4int = maximum number of w-type or x-type integral
c     #          combinations that can be placed in wxints(*).
c
      ioffs=3
      mx4int=maxbl4/2-ioffs
c
      do 1 i=1,8
         sym12(i)=i
         sym21(i)=i
 1     continue
      sqrt2=sqrt(two)
      l1strt=1
      k1strt=1
      finbl=0
      numout=0
      icontw=0
      icontx=0
      iblock=0
      ictodw=0
      ictodx=0
      n4ext=numin
      if(srtopt(1).ge.1)write(nlist,21010)
21010 format(' block structure of 4-ext. integrals'//
     & '    lsym   ksym lstart lfinal kstart kfinal  #intw  #intx'//)
      cntx12=0
      cntw3=0
      cntw12=0
      cntx3=0
      cntwx=0
      cw3od=0
      cw12od=0
      cx12od=0
      cx3od=0
      cwxod=0
c
      do 10 lsym=1,nmsym
c
         nl=nepsy(lsym)
         if(nl.eq.0)go to 10
c
         do 20 l1=1,nl
c
            smabcd=0
c
            do 30 ksym=1,(lsym-1)
c
               nk=nepsy(ksym)
               if(nk.eq.0)go to 30
c
c
c              # isym < jsym < ksym < lsym
c
               if(ksym.eq.1)go to 30
               ks=sym12(ksym)
               ls=sym12(lsym)
               kls=mult(ks,ls)
c
               do 40 k1=1,nk
c
                  symchk=0
c
                  do 50 jsym=1,(ksym-1)
c
                     nj=nepsy(jsym)
                     if(nj.eq.0)go to 50
                     js=sym12(jsym)
                     is=mult(js,kls)
                     isym=sym21(is)
                     if(isym.ge.jsym)go to 50
                     ni=nepsy(isym)
                     if(ni.eq.0)go to 50
c
                     symchk=1
                     smabcd=1
                     nij=ni*nj
                     cntwx=cntwx+nij
                     icontw=icontw+3*nij
                     icontx=icontx+3*nij
 50                continue
c
                  if(symchk.eq.0)go to 30
                  if(finbl.ne.0)then
                     finbl=0
                     l1strt=l1
                     k1strt=k1
                  endif
                  if(icontw-mx4int)70,80,90
 70                continue
                  if(icontx-mx4int)75,80,90
 75                continue
c
c                 # both w-block and x-block fit into the internal
c                 # buffer. save the valid block parameters.
c
                  lfinod=l1
                  kfinod=k1
                  ictodw=icontw
                  ictodx=icontx
                  cwxod=cntwx
                  go to 40
c
c                 # both w-block and x-block fit.  one block (w-blocks
c                 # are larger than x-blocks) fits exactly so go ahead
c                 # and form combinations and move to da buffers.
c
 80                iblock=iblock+1
                  call lcb4x1(
     &             incnt,  numin,  num,    pints,
     &             wxints, dabuf,  idabuf, cntwx,
     &             lsym,   ksym,   kls,    l1strt,
     &             l1,     k1strt, k1,     nepsy,
     &             sym12,  sym21,  ioffs)
                  icow=icontw
                  icox=icontx
c
                  call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
                  cntwx=0
                  if(srtopt(1).ge.1)write(nlist,21000)
     &             lsym,ksym,l1strt,l1,k1strt,k1,
     &             icontw,icontx
21000             format(1x,10i7)
                  ictodw=0
                  ictodx=0
                  numout=numout+icontw+icontx
                  icontw=0
                  icontx=0
                  finbl=1
                  lfinod=-999999
                  kfinod=-999999
                  cwxod=-999999
                  go to 40
c
c                 # at least one block is too large to fit into the
c                 # remaining internal buffer space.  form combinations
c                 # for the last valid block parameters and move to da
c                 # buffers.
c
 90                continue
c
                  if(cwxod.eq.0)then
                     write(nlist,21100)cwxod
                     call bummer('prep4e: cwxod=',cwxod,faterr)
                  endif
c
                  iblock=iblock+1
                  call lcb4x1(
     &             incnt,  numin,  num,    pints,
     &             wxints, dabuf,  idabuf, cwxod,
     &             lsym,   ksym,   kls,    l1strt,
     &             lfinod, k1strt, kfinod, nepsy,
     &             sym12,  sym21,  ioffs )
                  icow=ictodw
                  icox=ictodx
c
                  call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
                  if(srtopt(1).ge.1)write(nlist,21000)
     &             lsym,ksym,l1strt,lfinod,
     &             k1strt,kfinod,ictodw,ictodx
                  numout=numout+ictodw+ictodx
                  cntwx=cntwx-cwxod
                  lfinod=l1
                  kfinod=k1
                  cwxod=cntwx
                  l1strt=l1
                  k1strt=k1
                  ictodw=icontw-ictodw
                  ictodx=icontx-ictodx
                  icontw=ictodw
                  icontx=ictodx
c
c                 # .end of k1 loop.
 40             continue
c
               if(ictodw.eq.0.and.ictodx.eq.0)go to 45
c
c              # process the last block for this ksym.
c
               iblock=iblock+1
               call lcb4x1(
     &          incnt,  numin,  num,    pints,
     &          wxints, dabuf,  idabuf, cwxod,
     &          lsym,   ksym,   kls,    l1strt,
     &          l1,     k1strt, nk,     nepsy,
     &          sym12,  sym21,  ioffs )
               icow=ictodw
               icox=ictodx
c
               call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
               if(srtopt(1).ge.1)write(nlist,21000)
     &          lsym,ksym,l1strt,l1,k1strt,
     &          nk,ictodw,ictodx
               numout=numout+ictodw+ictodx
 45             continue
               cntwx=0
               icontw=0
               icontx=0
               ictodw=0
               ictodx=0
               cwxod=-999999
               finbl=1
c
c              # .end of ksym loop.
 30          continue
c
c           # ksym =lsym case (isym = jsym).
c
            ksym=lsym
c
            do 150 k1=1,l1
c
               symchk=0
c
c              # isym = jsym < ksym = lsym
c
               do 160 jsym=1,(ksym-1)
c
                  nj=nepsy(jsym)
                  if(nj.eq.0)go to 160
                  symchk=1
                  cntw3=cntw3+nj*(nj+1)/2
                  if(k1.ne.l1)then
                     cntw12=cntw12+nj*nj
                     cntx12=cntx12+nj*nj
                     cntx3=cntx3+nj*(nj-1)/2
                  else
                     cntw12=cntw12+nj*(nj-1)/2
                     cntx12=cntx12+nj*(nj-1)/2
                  endif
 160            continue
c
c              # isym = jsym = ksym = lsym
c
               if(k1.gt.1)then
                  symchk=1
                  if(k1.ne.l1)then
c                    # (k1-1)**2+k1-1
                     cntw12=cntw12+k1*(k1-1)
c                    # (k1-1)**2
                     cntx12=cntx12+(k1-1)*(k1-1)
c                    # k1*(k1-1)/2+k1-1
                     cntw3=cntw3+k1*(k1+1)/2-1
c                    # (k1-1)*(k1-2)/2+k1-1
                     cntx3=cntx3+(k1-1)*k1/2
                  else
c                    # k1*(k1-1)/2+k1-1
                     cntw3=cntw3+k1*(k1+1)/2-1
                     cntw12=cntw12+k1*(k1-1)/2
                     if(k1.gt.2)cntx12=cntx12+(k1-1)*(k1-2)/2
                  endif
c
               endif
c
               icontw=cntw3+cntw12
               icontx=cntx3+cntx12
               if(symchk.eq.0)go to 150
               if(finbl.ne.0)then
                  finbl=0
                  l1strt=l1
                  k1strt=k1
               endif
               if(icontw-mx4int)190,200,210
 190            continue
               if(icontx-mx4int)195,200,210
 195            continue
c
c              # both w-block and x-block fit into the internal buffer.
c              # save the valid block parameters.
c
               lfinod=l1
               kfinod=k1
               ictodw=icontw
               ictodx=icontx
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               go to 150
c
c              # both w-block and x-block fit.  one block (w-blocks are
c              # larger than x-blocks) fits exactly so go ahead and
c              # form combinations and move to da buffers.
c
 200            iblock=iblock+1
               call lcb4x2(
     &          incnt,  numin,  num,    pints,
     &          wxints, dabuf,  idabuf, cntw3,
     &          cntw12, cntx3,  cntx12, ksym,
     &          l1strt, l1,     k1strt, k1,
     &          nepsy,  ioffs,  maxbl4, sqrt2 )
               icow=icontw
               icox=icontx
c
               call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
               if(srtopt(1).ge.1)write(nlist,21000)
     &          lsym,ksym,l1strt,l1,k1strt,k1,
     &          icontw,icontx
               numout=numout+icontw+icontx
               finbl=1
               icontw=0
               icontx=0
               ictodw=0
               ictodx=0
               lfinod=-999999
               kfinod=-999999
               cntw12=0
               cntw3=0
               cntx12=0
               cntx3=0
               cw12od=-999999
               cw3od=-999999
               cx12od=-999999
               cx3od=-999999
               go to 150
c
c              # at least one block is too large to fit into the
c              # remaining internal buffer space.  form combinations
c              # for the last valid block parameters and move to
c              # output buffers.
c
 210            iblock=iblock+1
c
               if(cw3od.eq.0)then
                  write(nlist,21110)cw3od
                  call bummer('prep4e: cw3od=',cw3od,faterr)
               endif
21110         format(' error in prep4e,statement label 210,cw3od=',i10)
c
               call lcb4x2(
     &          incnt,  numin,  num,    pints,
     &          wxints, dabuf,  idabuf, cw3od,
     &          cw12od, cx3od,  cx12od, ksym,
     &          l1strt, lfinod, k1strt, kfinod,
     &          nepsy,  ioffs,  maxbl4, sqrt2 )
               icow=ictodw
               icox=ictodx
c
               call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
               if(srtopt(1).ge.1)write(nlist,21000)
     &          lsym,ksym,l1strt,lfinod,
     &          k1strt,kfinod,ictodw,ictodx
               numout=numout+ictodw+ictodx
               lfinod=l1
               kfinod=k1
               l1strt=l1
               k1strt=k1
               ictodw=icontw-ictodw
               ictodx=icontx-ictodx
               cntw12=cntw12-cw12od
               cntw3=cntw3-cw3od
               cntx12=cntx12-cx12od
               cntx3=cntx3-cx3od
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               icontw=0
               icontx=0
c
c              # .end of k1 loop.
 150         continue
c
            if(smabcd.eq.0)go to 20
            if(ictodw.eq.0.and.ictodx.eq.0)go to 20
            iblock=iblock+1
            call lcb4x2(
     &       incnt,  numin,  num,    pints,
     &       wxints, dabuf,  idabuf, cntw3,
     &       cntw12, cntx3,  cntx12, ksym,
     &       l1strt, lfinod, k1strt, kfinod,
     &       nepsy,  ioffs,  maxbl4, sqrt2 )
            icow=ictodw
            icox=ictodx
c
            call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
            if(srtopt(1).ge.1)
     .       write(nlist,21000)lsym,lsym,l1strt,lfinod
     &      ,k1strt,kfinod,ictodw,ictodx
            numout=numout+ictodw+ictodx
            finbl=1
            icontw=0
            icontx=0
            ictodw=0
            ictodx=0
            lfinod=-999999
            kfinod=-999999
            cntw12=0
            cntw3=0
            cntx12=0
            cntx3=0
            cw12od=-999999
            cw3od=-999999
            cx12od=-999999
            cx3od=-999999
            cwxod=-999999
c
c           # end of l1 loop.
 20       continue
c
         if(ictodw.eq.0.and.ictodx.eq.0)go to 130
c
c        # process the last block for this lsym.
c
         iblock=iblock+1
         call lcb4x2(
     &    incnt,  numin,  num,    pints,
     &    wxints, dabuf,  idabuf, cntw3,
     &    cntw12, cntx3,  cntx12, ksym,
     &    l1strt, lfinod, k1strt, kfinod,
     &    nepsy,  ioffs,  maxbl4, sqrt2 )
         icow=ictodw
         icox=ictodx
c
         call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
         if(srtopt(1).ge.1)write(nlist,21000)lsym,lsym,l1strt,lfinod,
     &    k1strt,kfinod,ictodw,ictodx
         numout=numout+ictodw+ictodx
 130      continue
         finbl=1
         icontw=0
         icontx=0
         ictodw=0
         ictodx=0
         lfinod=-999999
         kfinod=-999999
         cntw12=0
         cntw3=0
         cntx12=0
         cntx3=0
         cw12od=-999999
         cw3od=-999999
         cx12od=-999999
         cx3od=-999999
         cwxod=-999999
c
c        # end of lsym loop.
 10    continue
c
c     # process the final block.
c
      icow=ictodw
      icox=ictodx
c
      call puto34( icow, icox, ioffs, wxints, bufw, bufx )
c
c     # dump the last w- and x-buffers.
c
      call putd34( bufw, bufx )
c
      if(numin.ne.incnt)then
         write(nlist,21120)numin,incnt
         call bummer('prep4e: (numin-incnt)=',(numin-incnt),faterr)
      endif
c
c     # all done with 4-external integrals.
c
      write(nlist,21020)iblock
      write(nlist,20090)numout,n4ext
c
      return
21100 format(' error in prep4e,statement label 90,cwxod=',i10)
21120 format(' *** error *** prep4e: incorrect number of integrals',
     & ' processed. numin,incnt=',2i8)
21020 format(' prep4e:',i6,' blocks of linear combinations of',
     & ' 4-external integrals processed.')
20090 format(' number of sorted 4-external integrals  ',i9,/,
     & ' number of original 4-external integrals',i9/)
      end
c*deck lcb4x1
      subroutine lcb4x1(
     & incnt,  numin,  num,    pints,
     & wxints, dabuf,  idabuf, cntwx,
     & lsym,   ksym,   kls,    l1s,
     & l1f,    k1s,    k1f,    nepsy,
     & sym12,  sym21,  ioffs )
c
c  4-external linear combinations for ksym < lsym.
c
c  input:
c  incnt = integral offset for pints(*).
c  numin = total number of 4-external ints remaining to be processed.
c  num = number of 4-external ints in the current pints(*) segment.
c  pints(*) = holds a segment of integrals in triple sort order.
c  dabuf(*),idabuf(*) = da buffers.
c  cntwx = number of integral combinations to be formed.
c  lsym,ksym,kls,l1s,l1f,k1s,k1f = indexing ranges.
c  nepsy(*) = number of external orbitals of each symmetry.
c  sym12(*),sym21(*) = symmetry mapping vectors.
c  ioffs = number of working precision words required for block headers
c
c  output:
c  wxints(*) = w- and x-type integral combinations and packed headers.
c
      implicit logical(a-z)
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer        mult
      common /cmult/ mult(8,8)
c
c     # dummy:
      integer incnt, numin, num, cntwx, lsym, ksym, kls, l1s, l1f,
     & k1s, k1f, ioffs
      real*8 pints(nipsg),wxints(*),dabuf(*)
      integer idabuf(*),nepsy(8),sym12(8),sym21(8)
c
c     # local:
      integer icntw, icntx, stw121, ksymin, nk, k1l1, l, k1strt,
     & k1fin, k, jsym, nj, js, is, isym, ni, j, i,
     & nintkl, stw122, stw3, stx121, stx122, stx3, strtwf, strtxf
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      icntw  = ioffs
      icntx  = icntw + 3 * cntwx + ioffs
      stw121 = icntw
      ksymin = ksym - 1
      nk     = nepsy(ksym)
      k1l1   = 0
c
      do 10 l=l1s,l1f
         k1strt=1
         if(l.eq.l1s)k1strt=k1s
         k1fin=nk
         if(l.eq.l1f)k1fin=k1f
c
         do 20 k=k1strt,k1fin
            k1l1=k1l1+1
c
            do 30 jsym=1,ksymin
               nj=nepsy(jsym)
               if(nj.eq.0)go to 30
               js=sym12(jsym)
               is=mult(js,kls)
               isym=sym21(is)
               if(isym.ge.jsym)go to 30
               ni=nepsy(isym)
               if(ni.eq.0)go to 30
c
               do 51 j=1,nj
                  do 52 i=1,ni
                     if(incnt+3.gt.num)then
                        numin = numin - num
                        num   = min( numin, nipsg )
                        incnt = 0
                        call rdbks( pints, dabuf, idabuf )
                     endif
c                    # 1a,b w,x
                     wxints(icntw+i)=pints(incnt+1)+pints(incnt+3)
                     wxints(icntx+i)=pints(incnt+1)-pints(incnt+3)
c                    # 2a,b w,x
                   wxints(icntw+cntwx+i)=pints(incnt+1)+pints(incnt+2)
                   wxints(icntx+cntwx+i)=pints(incnt+1)-pints(incnt+2)
c                    # 3a,b w,x
                     wxints(icntw+2*cntwx+i) =
     &                pints(incnt+2)+pints(incnt+3)
                     wxints(icntx+2*cntwx+i) =
     &                pints(incnt+2)-pints(incnt+3)
                     incnt=incnt+3
 52                continue
                  icntw=icntw+ni
                  icntx=icntx+ni
 51             continue
 30          continue
 20       continue
 10    continue
c
      if(icntw.ne.cntwx+ioffs .or. icntx.ne.icntw+ioffs+3*cntwx)then
         write(nlist,20000) icntw, icntx, cntwx
         call bummer('lcb4x1: icntw icntx error',0,faterr)
      endif
c
      nintkl=(icntw-ioffs)/k1l1
      stw122=stw121+cntwx
      stw3=stw122+cntwx
      stx121=ioffs
      stx122=stx121+cntwx
      stx3=stx122+cntwx
      strtwf=stw3+cntwx
      strtxf=stx3+cntwx
c
c     # pack header info for both blocks:
c     #
c     # strtwf  total number of + combinations(w paths)
c     # strtxf  total number of - combinations(x paths)
c     # stw121  starting address for 1a,b w comb.
c     # stx121  starting address for 1a,b x comb.
c     # stw3    starting address for 3a,b w comb.
c     # stx3    starting address for 3a,b x comb.
c     # k1l1    number of (k,l) pairs
c     # nintkl  number of integrals for a single (k,l) pair
c

      call code4x(
     & wxints, strtwf, stw121, stw3,
     & nintkl, lsym,   ksym,   l1s,
     & l1f,    k1s,    k1f,    k1l1 )

      call code4x(
     & wxints( strtwf+1 ), strtxf, stx121, stx3,
     & nintkl,             lsym,   ksym,   l1s,
     & l1f,                k1s,    k1f,    k1l1 )
c
      return
20000 format(' error in lcb4x1'/' icntw, icntx, cntwx =',
     & 20i6)
      end


*@ifdef molcas
      subroutine pro2emolcas(
     & map,    u,      idiagx, lk432x,
     & lk1ex,  lk0ex,  ijkl,   buk,
     & lbuk,   dabuf, tibuf, nmpsy,nsym    )
*
c
c  read the 2-e integrals from the input file, and write them to
c  the intermediate da file.
c
c  14-dec-90 SIFS version with async i/o. -rls
c
      implicit logical(a-z)
c
      integer nsym,nmpsy(nsym),norboff(8)
      integer   nfilmx
      parameter(nfilmx=20)
c
      integer         iunits
      common /cfiles/ iunits(nfilmx)
c
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer   nbkmx
      parameter(nbkmx=200)
c
      integer          ibufpt,         irecpt,
     & nipbk,  nipsg,  lendar,         nbuk,        irec
      common /csort/   ibufpt(nbkmx),  irecpt(nbkmx),
     & nipbk,  nipsg,  lendar,         nbuk,        irec
c
      integer   niomx
      parameter(niomx=50)
      real*8      g,        rmu
      integer                           nlevel, nnlev, lowinl
      common /ci/ g(niomx), rmu(niomx), nlevel, nnlev, lowinl
c
      integer   nmomx
      parameter(nmomx=511)
      integer       iim1
      common /indx/ iim1(nmomx)
c
      integer        mul
      common /cmult/ mul(8,8)
c
      integer         srtopt
      common /option/ srtopt(5)
c
*@ifdef spinorbit
      integer         istrtx,   numx
      common /ctypei/ istrtx(8),numx(8)
*@else
*      integer         istrtx,   numx
*      common /ctypei/ istrtx(5),numx(5)
*@endif
c     # dummy:
      integer moint2
      integer map(nlevel), idiagx(nnlev), lk432x(nnlev),
     & lk1ex(*), lk0ex(*), ijkl(nnlev), lbuk(nipbk,nbuk)
*
      real*8 u(nnlev), buk(nipbk,nbuk), dabuf(lendar)
c
c     # local:
      integer hstuoc, nrec, numtot, reqnum, ierr, num, ibvtyp, ifmt,
     & itypea, itypeb, last, iint, i, j, k, l, ito, ii, jj, kk, ll,
     & iijj, kkll, temp, nndx, lad, nndxij, ijadd, nndxlk, nndxr,
     & ibuk, itox
      integer idummy(1)
      real*8 valx
*
cmolcas
       integer nsp,nop,nsq,nsr,nss,noq,ntm,nxm,nor,nos
       integer ni,nj,nk,nl,nt,nu,nv,nx,iout,numin,numax,norbp
       integer  nspqr,nssm,nspq,m
*
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      integer    iwait,   iretbv,   msame
      parameter( iwait=0, iretbv=0, msame=0 )
c
      real*8    half
      parameter(half=5d-1)
*@ifdef molcas_int64
      integer*8 mc_ntratoc,mc_ntrabuf,mc_ntibuf
      Parameter(mc_nTraToc=106,mc_nTraBuf=9600,mc_ntibuf=mc_ntrabuf)
      integer*8 mc_itratoc(mc_ntratoc)
      integer*8 mc_lutra,mc_two,mc_ida50
      real*8 tibuf(mc_ntibuf)
*@else
*      integer mc_ntratoc,mc_ntrabuf,mc_ntibuf
*      Parameter(mc_nTraToc=106,mc_nTraBuf=9600,mc_ntibuf=mc_ntrabuf)
*      integer mc_itratoc(mc_ntratoc)
*      integer mc_lutra,mc_two,mc_ida50
*      real*8 tibuf(mc_ntibuf)
*@endif
      parameter (mc_two=2)
c
c     # initialize some variables.
c
      call wzero( nnlev, u, 1 )
      hstuoc = lowinl - 1
      nrec   = 0
      numtot = 0
      norboff(1)=0
       do i=2,nsym
        norboff(i)=norboff(i-1)+nmpsy(i-1)
       enddo
      mc_lutra=80
*@ifdef molcas_ext
*      call daname_mf_(mc_LUTRA,'TRAINT')
*@else
      call daname_mf(mc_LUTRA,'TRAINT')
*@endif
      mc_IDA50=0
*@ifdef molcas_ext
*      CALL iDAFILE_(mc_LUTRA,mc_two,mc_iTraToc,mc_nTraToc,mc_Ida50)
*@else
      CALL iDAFILE(mc_LUTRA,mc_two,mc_iTraToc,mc_nTraToc,mc_Ida50)
*@endif
*
c
c     # loop over integral records.
c
 100   continue
      DO 313 NSP=1,NSYM
       NOP=nmpsy(NSP)
       DO 312 NSQ=1,NSP
        NSPQ=MUL(NSP,NSQ)
        NOQ=nmpsy(NSQ)
        DO 311 NSR=1,NSP
         NSPQR=MUL(NSPQ,NSR)
         NOR=nmpsy(NSR)
         NSSM=NSR
         IF(NSR.EQ.NSP)NSSM=NSQ
         DO 310 NSS=1,NSSM
          IF(NSS.NE.NSPQR)GO TO 310
          NOS=nmpsy(NSS)
          NORBP=NOP*NOQ*NOR*NOS
          IF(NORBP.EQ.0)GO TO 310
*@ifdef molcas_ext
*          CALL dDAFILE_(mc_LUTRA,mc_two,TIBUF,mc_NTIBUF,mc_Ida50)
*@else
          CALL dDAFILE(mc_LUTRA,mc_two,TIBUF,mc_NTIBUF,mc_Ida50)
*@endif
          IOUT=0
          DO 309 NV=1,NOR
           NXM=NOS
           IF(NSR.EQ.NSS)NXM=NV
          do   308 NX=1,NXM
            NTM=1
            IF(NSP.EQ.NSR)NTM=NV
            DO 307 NT=NTM,NOP
             NUMIN=1
             IF(NSP.EQ.NSR.AND.NT.EQ.NV)NUMIN=NX
             NUMAX=NOQ
             IF(NSP.EQ.NSQ)NUMAX=NT
             DO 306 NU=NUMIN,NUMAX
              IOUT=IOUT+1
              IF(IOUT.GT.mc_NTIBUF) THEN
*@ifdef molcas_ext
*                CALL dDAFILE_(mc_LUTRA,mc_two,TIBUF,mc_NTIBUF,mc_Ida50)
*@else
                CALL dDAFILE(mc_LUTRA,mc_two,TIBUF,mc_NTIBUF,mc_Ida50)
*@endif
                IOUT=1
              END IF
              VALX=TIBUF(IOUT)
              NI=NORBOFF(NSP)+NT
              IF(NI.LE.0) GOTO 306
              NJ=NORBOFF(NSQ)+NU
              IF(NJ.LE.0) GOTO 306
              NK=NORBOFF(NSR)+NV
              IF(NK.LE.0) GOTO 306
              NL=NORBOFF(NSS)+NX
              IF(NL.LE.0) GOTO 306
*
c        # map the orbital labels and canonicalize.
         ito = 1
         ii = max(map(ni),map(nj))
         jj = min(map(ni),map(nj))
         kk = max(map(nk),map(nl))
         ll = min(map(nk),map(nl))
         numtot=numtot+1
c        write(6,225) numtot,ni,nj,nk,nl,valx
 225     format('MO (',i6,')=',4i4,f12.6)
c
         iijj = iim1(ii)+jj
         kkll = iim1(kk)+ll
         if(iijj.lt.kkll)then
            temp = ii
            ii = kk
            kk = temp
            temp = jj
            jj = ll
            ll = temp
         endif
c
         k = kk
         l = ll
         if ( ii .ne. jj ) go to 1300
         if ( k  .ne. l  ) go to 1200
         if ( ii .ne. k  ) go to 1100
c
c        # (ii:ii)
c
         nndx = iim1(ii)+ii
         lad = idiagx(nndx)
         ibuk = 1
         ibufpt(ibuk) = ibufpt(ibuk)+1
         buk(ibufpt(ibuk),ibuk) = valx
         lbuk(ibufpt(ibuk),ibuk) = lad
         if(ibufpt(ibuk).eq.nipbk) then
            call wrtda( ibuk, buk, lbuk, dabuf )
         endif
         if(ii.le.hstuoc) go to 2000
         g(ii-hstuoc) = half*valx
         u(nndx)  =  u(nndx) + rmu(ii-hstuoc) * g(ii-hstuoc)
         go to 2000
c
c        # (ii:ll)
c
 1100     nndx = iim1(ii)+k
         lad = idiagx(nndx)
         ibuk = 1
         ibufpt(ibuk) = ibufpt(ibuk)+1
         buk(ibufpt(ibuk),ibuk) = valx
         lbuk(ibufpt(ibuk),ibuk) = lad
         if(ibufpt(ibuk).eq.nipbk) then
            call wrtda( ibuk, buk, lbuk, dabuf )
         endif
         nndxij = iim1(k)+k
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1120
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         if(ii.le.hstuoc) go to 1800
         u(nndxij) = u(nndxij) + valx * rmu(ii-hstuoc)
         go to 1800
 1120     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxlk = iim1(ii)+ii
         u(nndxlk) = u(nndxlk) + rmu( k-hstuoc) * valx
         u(nndxij) = u(nndxij) + rmu(ii-hstuoc) * valx
         go to 1800
c
 1200     if(ii.ne.k) go to 1260
c
c        # (ii:il)
c
         nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)+1
         if(ii.gt.hstuoc) go to 1220
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1220     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         u(nndxij) = u(nndxij) + half * valx * rmu(ii-hstuoc)
         if(l.gt.hstuoc) go to 1225
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1225     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (ii,kl)
c
 1260     nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)
         if(ii.le.hstuoc) go to 1265
         u(nndxij) = u(nndxij) + rmu(ii-hstuoc) * valx
         if(k.gt.hstuoc) go to 1270
 1265     nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1270     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         if(l.gt.hstuoc) go to 1280
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1280     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
 1300     if(k.ne.l) go to 1400
         if(jj-k) 1340,1320,1350
c
c        # (il:ll)
c
 1320     nndxij = iim1(ii)+l
         ijadd = ijkl(nndxij)
         if(ii.gt.hstuoc) go to 1330
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1330     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         if(l.gt.hstuoc) go to 1335
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1335     lad = lk0ex(nndxlk)+ijadd
         u(nndxij) = u(nndxij) + half * valx * rmu(l-hstuoc)
         go to 1800
c
c        # (il:kk)
c
 1340     nndxij = iim1(k)+jj
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1344
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1344     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         nndxr = iim1(ii)+jj
         u(nndxr) = u(nndxr) + valx * rmu(k-hstuoc)
         if(jj.gt.hstuoc) go to 1348
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1348     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (ij:ll)
c
 1350     nndxij = iim1(k)+k
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1360
         nndxlk = iim1(ii)+jj
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1360     nndxlk = iim1(ii-hstuoc)+jj-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxr = iim1(ii)+jj
         u(nndxr) = u(nndxr) + rmu(k-hstuoc) * valx
         go to 1800
c
 1400     if(jj.ne.l) go to 1500
         if(ii.ne.k) go to 1450
c
c        # (il:il)
c
         nndx = iim1(ii)+jj
         lad = idiagx(nndx)+1
         ibuk = 1
         ibufpt(ibuk) = ibufpt(ibuk)+1
         buk(ibufpt(ibuk),ibuk) = valx
         lbuk(ibufpt(ibuk),ibuk) = lad
         if(ibufpt(ibuk).eq.nipbk) then
            call wrtda( ibuk, buk, lbuk, dabuf )
         endif
         nndxij = iim1(jj)+jj
         ijadd = ijkl(nndxij)+1
         ito = 2
         if(ii.le.hstuoc) go to 1415
         u(nndxij) = u(nndxij) - half * valx * rmu(ii-hstuoc)
         if(jj.gt.hstuoc) go to 1420
 1415     nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1420     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxr = iim1(ii)+ii
         u(nndxr) = u(nndxr) - half * valx * rmu(jj-hstuoc)
         go to 1800
c
c        # (il:jl)
c
 1450     nndxij = iim1(jj)+jj
         ijadd = ijkl(nndxij)+1
         ito = 2
         if(jj.gt.hstuoc)  go to 1460
         if(k.le.hstuoc) ito = 2
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1460     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         lad = lk0ex(nndxlk)+ijadd
         nndxr = iim1(ii)+k
         u(nndxr) = u(nndxr) - half * valx * rmu(l-hstuoc)
         go to 1800
c
 1500     if(jj-k) 1600,1520,1540
c
c        # (ik:kl)
c
 1520     nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)+1
         if(k.gt.hstuoc) go to 1530
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         ito = 2
         go to 1800
 1530     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         nndxr = iim1(ii)+l
         u(nndxr) = u(nndxr) - half * valx * rmu(k-hstuoc)
         ito = 2
         if(l.gt.hstuoc) go to 1535
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1535     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (ij:kl)
c
 1540     nndxij = iim1(k)+l
         ijadd = ijkl(nndxij)
         if(k.gt.hstuoc) go to 1544
         nndxlk = iim1(ii)+jj
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1544     nndxlk = iim1(ii-hstuoc)+jj-hstuoc
         if(l.gt.hstuoc) go to 1548
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1548     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
 1600     if(ii.ne.k) go to 1650
c
c        # (ik:il)
c
         nndxij = iim1(jj)+l
         ijadd = ijkl(nndxij)+1
         ito = 2
         if(ii.le.hstuoc) go to 1605
         u(nndxij) = u(nndxij) - half * valx * rmu(ii-hstuoc)
         if(jj.gt.hstuoc) go to 1610
 1605     if(ii.le.hstuoc) ito = 2
         nndxlk = iim1(ii)+ii
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1610     nndxlk = iim1(ii-hstuoc)+ii-hstuoc
         if(l.gt.hstuoc) go to 1615
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1615     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
 1650     if(jj.lt.l) go to 1700
c
c        # (ik:jl)
c
         nndxij = iim1(jj)+l
         ijadd = ijkl(nndxij)+1
         if(jj.gt.hstuoc) go to 1660
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1660     nndxlk = iim1(ii-hstuoc) + k-hstuoc
         if(l.gt.hstuoc) go to 1665
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1665     lad = lk0ex(nndxlk)+ijadd
         go to 1800
c
c        # (il:jk)
c
 1700     nndxij = iim1(l)+jj
         ijadd = ijkl(nndxij)+2
         if(l.gt.hstuoc) go to 1710
         nndxlk = iim1(ii)+k
         lad = lk432x(nndxlk)+ijadd
         go to 1800
 1710     nndxlk = iim1(ii-hstuoc)+k-hstuoc
         if(jj.gt.hstuoc) go to 1715
         lad = lk1ex(nndxlk)+ijadd
         go to 1800
 1715     lad = lk0ex(nndxlk)+ijadd
c
c        # process the integrals.
c
 1800     continue
         ibuk = (lad - 1) / nipsg + 2
         do 1900 itox = 1, ito
            ibufpt(ibuk)            = ibufpt(ibuk) + 1
            buk(ibufpt(ibuk),ibuk)  = valx
            lbuk(ibufpt(ibuk),ibuk) = lad
            if ( ibufpt(ibuk) .eq. nipbk ) then
               call wrtda( ibuk, buk, lbuk, dabuf )
            endif
            lad = lad + 1
 1900     continue
c
 2000  continue
c
 306   continue
 307   continue
 308   continue
 309   continue
 310   continue
 311   continue
 312   continue
 313   continue
*
      write (nlist,6010) numtot
 6010  format(/' pro2e:',i10,' integrals read.')
c
      if ( srtopt(1) .ge. 2 ) then
         write (nlist,6020) (g(i), i = 1, (nlevel - hstuoc) )
         call plblkt('u(*) matrix', u, nlevel,'lev ', 1, nlist)
      endif
c
*@ifdef molcas_ext
*      call daclos_(mc_LUTRA)
*@else
      call daclos(mc_LUTRA)
*@endif
*
      return
 6020  format(' pro2e: g(*) = ',/(10f13.8))
      end
*
*
*
*
*
*
       subroutine link_it
       implicit none
c----------------------------------------------------------------------
c
c     Define default processing options
c
c----------------------------------------------------------------------
c     Include 'global.inc'
ccomdeck global.INC $Revision: 2.20.8.3 $
c----------------------------------------------------------------------
*
cMolcas.fh $Revision: 2.20.8.3 $
Cstart Molpro
*@ifdef molcas_int64
        integer*8 maxbfn,mxatom,mxroot,mxact,mxina,mxbas,
     .             mxorb,mxsym,mxatms,mtit,mxtitl,kbuf
*@else
*        integer maxbfn,mxatom,mxroot,mxact,mxina,mxbas,
*     .             mxorb,mxsym,mxatms,mtit,mxtitl,kbuf
*@endif
*
ccomdeck maxbfn.inc $Revision: 2.20.8.3 $
       Parameter(maxbfn=2000)
       Parameter (mxAtom = 500)
       Parameter (mxroot = 100)
       Parameter (mxact  =  50)
       Parameter (mxina  = 100)
       Parameter (mxbas = maxbfn)
       Parameter (mxOrb = maxbfn)
       Parameter (mxSym = 8)
*
      Parameter ( mxAtms  = mxatom      )
*@ifdef molcas_int64
      Integer*8   nSym,nAtoms
      Integer*8   nBas(mxSym)
      Integer*8   nOrb(mxSym)
      Integer*8   iOper(mxSym)
      Integer*8   nDel(mxSym)
      Integer*8   nFro(mxSym)
*@else
*      Integer   nSym,nAtoms
*      Integer   nBas(mxSym)
*      Integer   nOrb(mxSym)
*      Integer   iOper(mxSym)
*      Integer   nDel(mxSym)
*      Integer  nFro(mxSym)
*@endif
      Character*4 AtLbl(mxAtms)
      Character*4 BsLbl(2,mxOrb)
      Real*8 Coor(3,MxAtms)
      Real*8 PotNuc
      Common /Info  / PotNuc,Coor,AtLbl,BsLbl,
     *                nAtoms,nSym,nBas,nOrb,nDel,nFro,iOper
c
c
c----------------------------------------------------------------------
c
c     Allocate space to store the MO-coefficients and occupations
c
c----------------------------------------------------------------------
c
      Real*8 Occ(mxOrb)
      Common /MObas/ Occ
c
c----------------------------------------------------------------------
c
c     Allocate space to store the one-electron inntegral file header
c     and the header of the input source of MO coefficients
c
c----------------------------------------------------------------------
c
      Character*144 Header
      Character*80  VecTit
      Common /Head  / Header,VecTit
c
c----------------------------------------------------------------------
c
c     Allocate space to store the title
c
c----------------------------------------------------------------------
c
      Parameter ( mxTitl = 10      )
c
      Character*72 Title(mxTitl)
      Common /Tit   / mTit,Title
c
c----------------------------------------------------------------------
c
c     Allocate space to store logical switches for routing and
c     printing
c
c----------------------------------------------------------------------
c
*@ifdef molcas_int64
      Integer*8   Debug
      Integer*8   iPrint
      Integer*8   iOneOnly
      Integer*8   iVecTyp
      Integer*8   iSPdel
      Integer*8   iAutoCut
      Integer*8   iRFpert
*@else
*      Integer   Debug
*      Integer   iPrint
*      Integer   iOneOnly
*      Integer   iVecTyp
*      Integer   iSPdel
*      Integer   iAutoCut
*      Integer   iRFpert
*@endif
      Common /Switches/
     &Debug,iPrint,iOneOnly,iVecTyp,iSPdel,iAutoCut,iRFpert
c
c----------------------------------------------------------------------
c
c     Save cutting threshold for AUTO cut option
c
c----------------------------------------------------------------------
c
      Real*8 CutThrs(mxSym)
      Common /Cut/ CutThrs
*
c----------------------------------------------------------------------
c
c     Definee file names and unit numbers.
c
c----------------------------------------------------------------------
*
cfordeck files.inc $Revision: 2.20.8.3 $
c
c----------------------------------------------------------------------
c
c     Store file names and unit numbers.
c
c----------------------------------------------------------------------
c
c     Input files for MO coefficients
      Character*8   FnInpOrb,FnJobIph
c     one- and two-electron integrals in AO basis
      Character*8   FnOneAO,FnTwoAO
c     one- and two-electron integrals in MO basis
      Character*8   FnOneMO,FnTwoMO
c     half transformed two-electron integrals
      Character*8   FnHalf
c     EXTRACT file
      Character*8   FnExt
c     COMFILE file
      Character*8   FnCom
*@ifdef molcas_int64
      Integer*8       LuCom
      Integer*8       LuExt
      Integer*8       LuHalf
      Integer*8       LuOneMO,LuTwoMO
      Integer*8       LuOneAO,LuTwoAO
      Integer*8       LuInpOrb,LuJobIph
*@else
*      Integer       LuCom
*      Integer       LuExt
*      Integer       LuHalf
*      Integer       LuOneMO,LuTwoMO
*      Integer       LuOneAO,LuTwoAO
*      Integer       LuInpOrb,LuJobIph
*@endif
      Common /Files/
     &  FnInpOrb,FnJobIph,FnOneAO,FnTwoAO,
     &  FnOneMO,FnTwoMO,FnHalf,FnExt,FnCom,
     &  LuInpOrb,LuJobIph,LuOneAO,LuTwoAO,
     &  LuOneMO,LuTwoMO,LuHalf,LuExt,LuCom
c
c----------------------------------------------------------------------
c
c     allocate space to store the table of contents of various files
c
c----------------------------------------------------------------------
c
*@ifdef molcas_int64
      Integer*8   TcJobIph(1024)
      Integer*8   TcOneMO(1024)
*@else
*      Integer   TcJobIph(1024)
*      Integer   TcOneMO(1024)
*@endif
      Common /Tocs/ TcJobIph,TcOneMO
c----------------------------------------------------------------------
c
c     Define TOC for the electron repulsion integrals in MO basis
c
c     Define the buffer size of the electron repulsion
c     integrals in MO basis
c----------------------------------------------------------------------
*
ccomdeck tratoc.inc $Revision: 2.20.8.3 $
*@ifdef molcas_int64
      Integer*8 nTraToc,nTraBuf
      Parameter(nTraToc=106,nTraBuf=9600)
      Integer*8 iTraToc(nTraToc)
*@else
*      Integer nTraToc,nTraBuf
*      Parameter(nTraToc=106,nTraBuf=9600)
*      Integer   iTraToc(nTraToc)
*@endif
      Common /TraToc/ iTraToc
*
*
*
      Parameter ( kBuf = nTraBuf )
*
      FnInpOrb='INPORB'
      FnJobIph='JOBIPH'
      LuInpOrb=10
      LuJobIph=15
      FnOneAO='ONEINT'
      FnTwoAO='ORDINT'
      LuOneAO=20
      LuTwoAO=40
      FnOneMO='TRAONE'
      FnTwoMO='TRAINT'
      LuOneMO=30
      LuTwoMO=50
      FnHalf='TEMP1'
      LuHalf=60
      FnExt='EXTRACT'
      LuExt=18
      FnCom='COMFILE'
      LuCom=22
c----------------------------------------------------------------------
*
*
      Debug=0
      iPrint=0
      iOneOnly=0
      iSPdel=0
      iVecTyp=2
      iAutoCut=0
      iRFpert=0
*
      End
*
*@endif
