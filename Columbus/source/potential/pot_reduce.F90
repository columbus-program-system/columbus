!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       program pot_reduce
       implicit none
       real*8, allocatable ::cxyz(:,:)
       character*4, allocatable :: symb(:)
       real*8, allocatable :: pos_crxyzi(:,:),neg_crxyzi(:,:)
       integer npoints,get_numptcharge,ipos,ineg,i,j,ntot
       real*8 orign(3),temp(3),r,maxneg,maxpos,scaleneg,scalepos,tmp
        integer iboxt,iboxp,iboxr,nt,np,nr
        real*8 , allocatable :: posbox(:),negbox(:),compcharges(:,:)
        integer, allocatable :: orderneg(:),orderpos(:)
        real*8  dtheta,dphi,dr,pi,minr,totpos,totneg
        integer ioff,ioffstart,idx,idxn,icomp

       npoints=get_numptcharge('potential.xyz')
       allocate(cxyz(1:4,npoints),symb(npoints))
       allocate(pos_crxyzi(1:9,npoints),neg_crxyzi(1:9,npoints))

       call read_ptcharge('potential.xyz',cxyz,symb,npoints)

!      # sort into positive and negative charges
!      # assume orign = (0,0,0) 
        orign(1:3)=0.0d0
        temp (1:3)=0.0d0
       ipos=0
       ineg=0
       totneg=0.0d0
       totpos=0.0d0
       do i=1,npoints
         forall(j=1:3) temp(j)=cxyz(j+1,i)-orign(j)
!        r=dot_product(cxyz(2:4,i),cxyz(2:4,i)) 
         r=dot_product(temp,temp) 
         r=sqrt(r)
         if (cxyz(1,i).lt.0.0d0) then
            ineg=ineg+1
            neg_crxyzi(1,ineg)=cxyz(1,i)
            neg_crxyzi(2,ineg)= r
            neg_crxyzi(3:5,ineg)=cxyz(2:4,i)
            neg_crxyzi(6,ineg)=i
            totneg=totneg+cxyz(1,i)
         else
            ipos=ipos+1
            pos_crxyzi(1,ipos)=cxyz(1,i)
            pos_crxyzi(2,ipos)= r
            pos_crxyzi(3:5,ipos)=cxyz(2:4,i)
            pos_crxyzi(6,ipos)=i
            totpos=totpos+cxyz(1,i)
         endif 
        enddo

!   determine spatial extent of distribution in radial direction

       maxneg=maxval(neg_crxyzi(2,1:ineg))
       maxpos=maxval(pos_crxyzi(2,1:ipos))

        write(6,*) ' read a total number of ',npoints, ' point charges'
        write(6,*) ipos, ' positive and ', ineg,' negative charges'
        write(6,*) 'total positive charge=',totpos
        write(6,*) 'total negative charge=',totneg
        write(6,*) 'max positive radius=',maxpos
        write(6,*) 'max negative radius=',maxneg

        call get_polarcoords(ineg,9,neg_crxyzi)
        call get_polarcoords(ipos,9,pos_crxyzi)

!  assign charges to the boxes [1:nr,1:nt,1:np]
!  compute a compound boxindex
!  ibox# = ((it*100+ip)*100+ir)
!  ibox# = (it*np+ ip)*nr +ir 

       nt=32  ! Abstand ca. R*2* sin(theta/2) = R*theta = R *pi/32  = 0.1*R
       np=64  !                                           R *2pi/64 = 0.1*R
!   total number of sphere segments  32*64=2048 
       nr=8
!   minimum distance from origin: twice the maximum diameter of the molecule,e.g. 
       minr=  7.0d0 
       open(unit=11,file='potential_red.xyz',form='formatted')
        ntot=0
       call combine_charges(ineg,9,neg_crxyzi,maxneg,minr,nr,nt,np,symb,ntot)
       call combine_charges(ipos,9,pos_crxyzi,maxpos,minr,nr,nt,np,symb,ntot)
       write(6,*) 'total number of charges written:',ntot
       close (11)
       open(unit=12,file='potential_info',form='unformatted')
       write(12) npoints,ipos,ineg
       write(12) cxyz
       write(12) pos_crxyzi
       write(12) neg_crxyzi
       close(12)

       end


       integer function get_numptcharge(fname)
       implicit none
       character*(*) fname
       integer npoints

       open(unit=10,file=trim(fname),form='formatted') 
       read(10,*,err=170) npoints
       close(10)
       get_numptcharge=npoints
       return
 170   call bummer('could not read first line of ' // trim(fname),0,2)
       end

       subroutine read_ptcharge(fname,cxyz,symb,npoints)
       implicit none
       character*(*) fname
       character*80  line
       integer l,i,np,npoints
       real*8  cxyz(4,npoints)
       character*4 symb(npoints)
       open(unit=10,file=trim(fname),form='formatted') 
       read(10,*,err=170) np
       if (np.ne.npoints) call bummer('read_ptcharge inconsistent files',0,2)
       do i=1,npoints
        read(10,'(a)') line
!  find 
        line=adjustl(line)
!  find first space
        l=index(line,' ')
        symb(i)='    '
        symb(i)(1:l)=line(1:l)
        read(line(l:),*) cxyz(1:4,i)
       enddo
       close(10)
       return
 170   call bummer('failed in read_ptcharge',0,2)
       end

        
      

      
        subroutine combine_charges(n,m,crxyzi,maxrange,minr,nr,nt,np,symb,ntot)
        implicit none
        integer  n,m,nr,nt,np,ntot
        real*8    crxyzi(m,n),maxrange,minr
        real*8,allocatable :: cpoints(:,:)
        integer, allocatable :: negbox(:)
        integer j,i,iboxt,iboxr,iboxp,iboxidx
        real*8 dtheta,dphi,dr,pi,ch
        character*4 symb(*)

      pi=3.141592654d0
      allocate(cpoints(6,nr*nt*np),negbox(n))
      cpoints=0.0d0
      negbox=-1
      dr = (maxrange-minr)/dble(nr)
      dtheta = (pi)/dble(nt)
      dphi = (2.0d0*pi)/dble(np)

      do i=1,n
        iboxt=min(nt,int(crxyzi(7,i)/dtheta)+1)
        iboxp=min(np,int((crxyzi(8,i)+pi)/dphi)+1)
        if (crxyzi(2,i).le.minr) cycle
        iboxr=min(nr,int((crxyzi(2,i)-minr)/dr)+1)
        iboxidx= ((iboxt-1)*np+iboxp-1)*nr+iboxr
        if (iboxt.lt.1) call bummer('iboxt < 0 ',iboxt,2)
        if (iboxp.lt.1) call bummer('iboxp < 0 ',iboxt,2)
        if (iboxr.lt.1) call bummer('iboxr < 0 ',iboxt,2)
        if (iboxidx.gt.nr*nt*np)  then 
         call bummer('iboxidx too large ',iboxidx,2)
        endif 
        negbox(i)=iboxidx
! cpoints( charge, r**2, x,y,z, npoints
        ch=crxyzi(1,i)
        cpoints(2,iboxidx)=cpoints(2,iboxidx)*cpoints(1,iboxidx)+crxyzi(2,i)*crxyzi(2,i)*ch
        forall(j=3:5) cpoints(j,iboxidx)=cpoints(j,iboxidx)*cpoints(1,iboxidx)+crxyzi(j,i)*ch
        cpoints(1,iboxidx)=cpoints(1,iboxidx)+ch
        forall(j=2:5) cpoints(j,iboxidx)=cpoints(j,iboxidx)/cpoints(1,iboxidx)
        cpoints(6,iboxidx)=cpoints(6,iboxidx)+1.0d0
      enddo

!  finally compute square root of r2
      where (cpoints(2,1:nr*nt*np).gt.1.d-6) cpoints(2,1:nr*nt*np)=sqrt(cpoints(2,1:np*nr*nt))
!  now we can compute the distribution 


!  now we can dump all of the original retained data points to file
!  followed by all compound objects 

      do i=1,n
       if (negbox(i).eq.-1) then
! this is a retained point
       write(11,75) nint(crxyzi(6,i)),symb(nint(crxyzi(6,i))),crxyzi(1,i),crxyzi(3:5,i)  
 75    format(i8,a4,1x,f12.8,1x,f12.8,1x,f12.8,1x,f12.8,1x)
       ntot=ntot+1
       endif
      enddo

       do i=1,nt*np*nr
         if (cpoints(2,i).gt.0.01d0) then
          write(11,75) -999,' CMP',cpoints(1,i),cpoints(3:5,i)
          ntot=ntot+1
         endif
       enddo
       return
       end 


       subroutine get_polarcoords(n,m,crxyzi)

        implicit none
        integer n,m
        real*8 crxyzi(m,n)
        integer i
        real*8 tmp  

      do i=1,n
          tmp= sqrt(crxyzi(3,i)**2+crxyzi(4,i)**2)
! theta = arctan(sqrt(x**2+y**2)/z)
          crxyzi(7,i)= atan2(tmp,crxyzi(5,i))
! phi   = arctan(y/x)  x<0   +pi; x=0 y>0  pi/2   x=0 y<0 -pi/2  x=0=y  unbestimmt
          crxyzi(8,i)= atan2(crxyzi(4,i),crxyzi(3,i))
      enddo

        return
        end

