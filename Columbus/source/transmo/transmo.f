!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program transmo
c
c    written by: Thomas Mueller
c                Institute for Theoretical Chemistry
c                University of Vienna
c                Waehringer Str. 17
c                A-1090 Vienna
c                Austria
c
c    experimental version
c

c
c transforms MO coefficients from one point group into another
c including interchanging the coordinate axis and additionally
c provides a molden interface.
c currently limited to dalton
c
c
c motype  =  0  SCF-MOs  : mos, occup, energy
c motype  =  1  MCSCF-NOs: nos,occup
c motype  =  2  any MOs  : mos, order occording to moorder
c
c moorder = pairs of numbers (irrep #, mo#)
c           the mos will be placed in this order
c           if less than the total number of MOs is given, the
c           remaining ones will be put at the end in arbitrary
c           order (blocked by irrep)
c
c ixyz    =  123  xyz => xyz
c            231  xyz => zxy
c            132  xyz => xzy
c            213  xyz => yxz
c            312  xyz => yzx
c            321  xyz => zyx
c
c molden  =  0               no molden output
c molden  =  1               molden output based on
c                            mocoef/soinfo.dat information
c                            in this case no transformation is carried
c                            out
c
c motype =   0   mocoef file contains MOs, energies, occupation numbers
c                (SCF-MOs)
c            1   mocoef file contains MOs, occupation numbers
c                (CI-NOs, MCSCF-NOs)
c            2   mocoef file contains MOs
c                (any mocoef file in SIFS format)
c                you need to specify moorder
c
c  printlevel= 0,1,2
c
c  input files: soinfo.high dalton info file produced for the high
c                           symmetry case
c
c               soinfo.low  dalton info file produced for the low
c                           symmetry case
c
c               transmoin   name list input for transmo
c
c               mocoef.high MOs for the high symmetry case
c
c               mocoef      MOs for molden interface
c
c               daltaoin    dalton input file
c
c  output files:
c
c               mocoef.new  MOs transformed to the low symmetry case
c                            (if molden=0)
c
c               molden      MOs and basis set information in MOLDEN
c                           proprietory format
c                            (if molden=1)
c
c
c   ...standard input for the generation of a MOLDEN input file:
c
c       change to the WORK directory
c
c        transmoin:   &input
c                      molden=1
c                      caotype=1
c                      motype=2
c                     &end           (spherical GTOs,MOs ordered
c                                     as in mocoef file)
c
c


      implicit none 
       integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       integer max_contr_blocks
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)
       parameter (max_contr_blocks = 20)

       integer xsocao(maxbfn,8),xnbf(8),xmaxdim(maxbfn),xsonr
      real*8 xsosign(maxbfn,8)
      integer nbfread(8), sonr
      integer socao(maxbfn,8),maxdim(maxbfn),caoso(maxbfn,8)
      integer maxdimcao(maxbfn)
      real*8 invmatrix(maxbfn,8)
      integer index(maxbfn)
      integer count(maxcaoperatom),atd2h(maxcaoperatom,maxatoms),
     .        atc2v(maxcaoperatom,maxatoms)
      real*8 sosign(maxbfn,8)
      integer maxlen,i,xnsym
      integer nsym,nbfd2h(8),nsymd2h,ii,iii,j
      real*8 mocoefd2h(maxbfn*maxbfn),mocoefcao(maxbfn*maxbfn)
      real*8 xmocoef(maxbfn*maxbfn)
      character*80 d2hinfo(maxatoms),c2vinfo(maxatoms)
      character*4 label(5)
       data label /'  A ',' B  ',' C  ','  D ','  E '/
        real*8 scratch(maxbfn,8),symmthres,minprt
      integer molden,caotype,printlevel,ierr,ixyz,imapping
      integer atfac,moorder,motype
      common /inpdata/ molden,moorder(2*maxbfn),symmthres,
     .                 caotype,printlevel,ixyz,imapping
     .                 ,atfac(maxatoms),motype,minprt

       integer index2
       integer nmooff(8)
       real*8 energie,occnum
       character*2 atomlabel
       character*8 symlabel
       integer ordzahl
       common /moldeninfo/ ordzahl(maxatoms),index2(maxbfn+1),
     .  occnum(maxbfn),energie(maxbfn),atomlabel(maxatoms)
     .  ,symlabel(maxbfn)


       call printheader

       call readinput
       do i=1,maxatoms
        d2hinfo(i)=' '
        c2vinfo(i)=' '
       enddo
c   Schritt 1: baue CAO->SAO-Matrix auf
c
c soinfo:
c 2i4  ntot , nsym
c 20i4 mappingcao(i) : welches CAO korrespondierte zu welchem CAO in d2h
c Anhand eines Vergleichs  der beiden dalton.outputs laesst sich das lei
c da die absoluten CAOs identisch sind
c 20i4 mappingsao(i) : sorgt dafuer, dass nach der Ruecktransformation d
c                      in SOs , die SOs dicht liegen (nur bei achsenvert
c                      erforderlich)
c Wenn beispielsweise die x und y Achse vertauscht wurden, sind jetzt (i
c die SOs px <-> py  usw. vertauscht werden.
c eventuell einfach Vergleich von dalton.output aus c2v-Symmetrie mit gl
c gewuenschter Orientierung : waere gut, wenn es sich automatisieren lies
c
c von dalton

      if (molden.eq.1) then
      write(*,*) 'using soinfo.dat'
      open(unit=10,file='soinfo.dat',status='unknown')
      else
      write(*,*) 'using soinfo.high'
      open(unit=10,file='soinfo.high',status='unknown')
      endif

c  socao(so,i)  contains the CAOs i, which contribute to SO so
c  sosign(so,i) contains the contribution of CAO i to SO so
c  nbfread      contains the readin number of basis functions per irrep
c  nsymread     contains the readin number of irreps
c  sonr         contains the total number of SOs
c  maxdim(i)    contains the number of CAOs which contribute to SO i

      call buildsocao(nsym,
     . 10,socao,sosign,nbfread,maxdim,sonr,.true.,.false.,
     . d2hinfo,c2vinfo,atd2h,atc2v,count)
      close(10)
        if (printlevel.gt.1) then
        j=0
        do i=1,nsym
        do ii=1,nbfread(i)
         j=j+1
          write(*,1000) j,(socao(j,iii),iii=1,maxdim(j))
 1000    format('socao: #',i3,':',8i4)
        enddo
        enddo
        endif


c Schritt 2: Aufsplitten der Matrix in Submatrizen der maximalen
c            Dimension nsym (8) , invertieren dieser Submatrizen
c            und anschliessendes Eintragen in die invertierte
c            Matrix

c  maxdimcao(i) enthaelt die Zahl der SOs zu denen CAO i beitraegt (redu
c  invmatrix(cao,so) enthaelt die Vorfaktoren, um das CAO cao aus den SO
c  caoso(cao,so) enthaelt die SOs die zu CAO cao beitragen

      call calcinvmatrix(socao,sosign,maxdim,sonr,
     .  maxdimcao,invmatrix,caoso)
      maxlen=maxbfn*maxbfn

c Schritt 3: Lese die mocoef in d2h symetrie ein und
c            transformiere sie in die CAO-Basis

      if (molden.eq.0) then
      call readmocoef(11,'mocoef.high',mocoefd2h,nsymd2h,nbfd2h,maxlen,
     .       index)
      else
      call readmocoef(11,'mocoef',mocoefd2h,nsymd2h,nbfd2h,maxlen,
     .       index)
      endif
      call prblks('Input MO coefficients',mocoefd2h,nsymd2h,nbfd2h,
     .            nbfd2h,'SAO','MO',1,6)


       do i=1,nsymd2h
        if (nbfd2h(i).ne.nbfread(i)) then
         call bummer( 'nbfd2h.ne.nbfread, i =',i,2)
        endif
      enddo

      call transformtocao(mocoefd2h,mocoefcao,maxdimcao,caoso,
     .    invmatrix,nbfd2h,nsymd2h,index)
      call prblks('MOs transformed to atom-centered basis',
     .            mocoefcao,1,sonr,sonr,'AO','MO',1,6)

       if (molden.eq.1) then
       call printmolden(mocoefcao,d2hinfo,atd2h,nsymd2h,nbfd2h)
       call bummer('normal termination',0,3)
       stop
       endif
c
c  print a rectangular-packed blocked matrix.
c
c  input:
c  title  = character title to be printed before each block.
c  z(*)   = blocked rectangular matrix to be printed.
c  nblk   = number of blocks in the matrix z(*).
c  nrow(*)= number of rows in each block.
c  ncol(*)= number of columns in each block.
c  labr   = character*4 row label.
c  labc   = character*4 column label.
c  ifmt   = format type.
c  nlist  = output unit number.


c Schritt 4: Baue die socao-Matrizen fuer die andere Punktgruppe auf
c            und baue aus den mos in der cao basis die entsprechenden
c            mos in der sao basis auf.

c
c     originally  low= c2v , high=d2h
c
       write(*,*) 'reading soinfo.low'
       open(unit=10,file='soinfo.low',status='unknown')
       call buildsocao
     . (xnsym,10,xsocao,
     .  xsosign,xnbf,xmaxdim,xsonr,.false.,.true.,
     .  c2vinfo,d2hinfo ,atc2v,atd2h,count)
       close(10)
       if (printlevel.gt.1) then
        j=0
        do i=1,xnsym
        do ii=1,xnbf(i)
         j=j+1
          write(*,1000) j,(xsocao(j,iii),iii=1,xmaxdim(j))
        enddo
        enddo
       endif

c
       call transformtosao(mocoefcao,
     .  xmocoef,xmaxdim,xsocao,xsosign,xnbf,xnsym)

      call prblks('mocoef transformed to new point group',
     .        xmocoef,xnsym,xnbf,xnbf,'SAO','MO',1,6)
c
      maxlen=0
      do i=1,xnsym
       maxlen=maxlen+xnbf(i)**2
      enddo
       if (printlevel.gt.0) then
       write(*,*) 'xnsym=',xnsym
       write(*,*) 'xnbf(*)=',(xnbf(i),i=1,xnsym)
       endif
      
       call writemocoef(13,'mocoef.new',xmocoef,xnsym,xnbf,maxlen,
     .         motype,occnum,index2)
c
       call bummer('normal termination',0,3)
        stop
        end

      subroutine buildsocao
     .    (nsym,unit,socao,sosign,nbfread,maxdim,sonr,invert,
     .    mapcao,atominfo,saveinfo,atfeld1,atfeld2,count)
        implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

       integer ii,newcentr,unit,sonr,k1,loc,i,j,jj
       integer iy,ix,atfeld1(maxcaoperatom,maxatoms),
     .       atfeld2(maxcaoperatom,maxatoms),count(maxcaoperatom)
       integer socao(maxbfn,8),nbfread(8),maxdim(maxbfn),nsym
       real*8 sosign(maxbfn,8),f
       logical invert,mapcao
       character*1 scr(8)
       character*80 chrstr
       character*5 oldlabel,newlabel
       character*7 chrstr2
       character*80 atominfo(maxatoms),saveinfo(maxatoms)
       integer ntot,mappingcao(maxbfn),mappingsao(maxbfn),tmp,t1,m1
       integer mapd2h,icao
       integer degencao ,typcao
       integer upperbound(60),lowerbound(60),iatom
       common /d2hmapping/ mapd2h(maxbfn),typcao(maxbfn),
     .      degencao(maxbfn),iatom
       integer maptype(20,7)
       integer sphtype(16,7)
       integer start,ende,mapatoms(60)
       integer woistatom(20,2),icentr,mapoffset(10)
       real*8 faccao(maxbfn),x,y,z,sx,sy,sz,symmthres,minprt
       integer atfac,motype
      integer molden,moorder,caotype,printlevel,ierr,ixyz,imapping
      common /inpdata/ molden,moorder(2*maxbfn),symmthres,
     .                 caotype,printlevel,ixyz,imapping
     .                 ,atfac(maxatoms),motype,minprt



       integer index2
       integer ismall,icap,nmooff(8)
       real*8 energie,occnum
       character*2 atomlabel
       character*8 symlabel
       integer ordzahl
       common /moldeninfo/ ordzahl(maxatoms),index2(maxbfn+1),
     .  occnum(maxbfn),energie(maxbfn),atomlabel(maxatoms)
     .  ,symlabel(maxbfn)



c map 1 3 4 2 means s=>s  px => py py=> pz pz=> px
c
c the last row fixes the ordering in molden
c
       data mapoffset /0,1,1,1,4,4,4,4,4,4 /
       data maptype /1,2,3,4,5,6,7,8,9,10,
     .                      11,12,13,14,15,16,17,18,19,20,
     .               1,4,2,3,10,7,9,5,6,8,
     .                      20,16,19,13,15,18,11,12,14,17,
     .               1,2,4,3,5,7,6,10,9,8,
     .                      11,13,12,16,15,14,20,19,18,17,
     .               1,3,2,4,8,6,9,5,7,10,
     .                      17,14,18,12,15,19,11,13,16,20,
     .               1,3,4,2,8,9,6,10,7,5,
     .                      17,18,14,19,15,12,20,16,13,11,
     .               1,4,3,2,10,9,7,8,6,5,
     .                      20,19,16,18,15,13,17,14,12,11,
     .               1,2,3,4, 5,8, 9,6,10,7,
     .                      11,15,16,14,20,17,12,19,18,13/

       data sphtype /1,2,3,4,5,6,9,8,7,
     .                     10,11,12,13,14,15,16,
     .               1,4,2,3,8,5,9,6,7,
     .                     10,11,12,13,14,15,16,
     .               1,2,4,3,8,6,9,5,7,
     .                     10,11,12,13,14,15,16,
     .               1,3,2,4,5,8,9,6,7,
     .                     10,11,12,13,14,15,16,
     .               1,3,4,2,6,8,9,5,7,
     .                     10,11,12,13,14,15,16,
     .               1,4,3,2,6,5,9,8,7,
     .                     10,11,12,13,14,15,16,
     .               1,2,3,4,9,7,5,6,8,
     .                     10,11,12,13,14,15,16/
900   format(a80)
c 1061 FORMAT(5x,I3,7(2X,A,1X,I3)) Reed Nieman: changed 6-26-2023
 1061 FORMAT(5x,I4,7(2X,A,1X,I4))
 1060 format(i3,2x)
 1062 Format(2X,A7)
 1063 format(20i4)
 1064 format(5x,a)
 1065 format(16x,i2)
 1066 format(a7)
 1067 format(16x,i1,4x,i4)
 1068 format(15x,a)
 1069 format(8x,i3)
 1070 format(3(f15.10,1x))
 1071 format(80(' '))

       call find_collating(ismall,icap)

       iatom=0
 65    read(unit,900) chrstr
        if (chrstr(1:7).ne.'GTOSCAL') goto 72
        goto 65
 70   read(unit,900) chrstr
 72    read(chrstr,1066) chrstr2
       if (chrstr2.ne.'ATOMINF') goto 71
      iatom=iatom+1
       if (iatom.gt.maxatoms)
     .   call bummer('too many atoms, increase maxatoms ',iatom,2)
       read(chrstr,1068) atominfo(iatom)
       read(chrstr,'(9x,a3)') chrstr2
       j=0
       atomlabel(iatom)='  '
       do i=1,3
c  C. W. McCurdy change to include lower case letters in element symbols
c   Change that allows two letter symbols and correct
c     atomic numbers in molden file (and allows identification of Li):
          if ((iachar(chrstr2(i:i)).ge.icap
     .   .and. iachar(chrstr2(i:i)).lt.icap+26)
     .   .or.(iachar(chrstr2(i:i)).ge.ismall
     .   .and. iachar(chrstr2(i:i)).lt.ismall+26)) then
           j=j+1
           atomlabel(iatom)(j:j)=chrstr2(i:i)
        endif
       enddo

       goto 70
 71   continue
c     read(chrstr,*) ixyz
      if (mapcao) then
        do i=1,iatom
        read(atominfo(i),*) x,y,z
        write(atominfo(i),1071)
        write(atominfo(i),1070) x,y,z
        enddo
       else
         goto (11,12,13,14,15,16,11) imapping
         call bummer ('invalid imapping, ',imapping,2)
 11      do i=1,iatom
          read (atominfo(i),*) x,y,z
          write(atominfo(i),1071)
          write(atominfo(i),1070) x,y,z
         enddo
          goto 18
 12      do i=1,iatom
          read (atominfo(i),*) x,y,z
          write(atominfo(i),1071)
          write(atominfo(i),1070) y,z,x
c         write(atominfo(i),1070) z,x,y
         enddo
         goto 18
 13       do i=1,iatom
          read (atominfo(i),*) x,y,z
          write(atominfo(i),1071)
          write(atominfo(i),1070) x,z,y
          enddo
         goto 18
 14       do i=1,iatom
          read (atominfo(i),*) x,y,z
          write(atominfo(i),1071)
          write(atominfo(i),1070) y,x,z
          enddo
         goto 18
 15       do i=1,iatom
          read (atominfo(i),*) x,y,z
          write(atominfo(i),1071)
          write(atominfo(i),1070) z,x,y
c         write(atominfo(i),1070) y,z,x
          enddo
          goto 18
 16       do i=1,iatom
          read (atominfo(i),*) x,y,z
          write(atominfo(i),1071)
          write(atominfo(i),1070) z,y,x
          enddo
 18       continue
      endif

c  ixyz: 123  xyz -> xyz
c        231  xyz -> yzx  etc.
c        132  xyz -> xzy
c        213  xyz -> yxz
c        312  xyz -> zxy
c        321  xyz -> zyx

      icao=0
      icentr=0
      ix=0
      call izero_wr(maxcaoperatom,count,1)
       goto 82
 80   read(unit,900) chrstr
 82   read (chrstr,1066) chrstr2
       if (chrstr2.ne.'CAOINFO') goto 81
      icao=icao+1
      read (chrstr,1069) newcentr
c
c     caocentre(icao)=newcentr
c
      if (newcentr.ne.icentr) then
         if (icentr.gt.0) ix =ix+degencao(icao-1)
         icentr=newcentr
      endif
      read (chrstr,1067) degencao(icao),typcao(icao)
       iy=degencao(icao)+ix
       count(iy)=count(iy)+1
       if (count(iy).gt.maxcaoperatom)
     . call bummer('increase maxcaoperatom',count(iy),2)
       atfeld1(count(iy),iy)=icao
       do i=1,degencao(icao)-1
        degencao(icao-i)=degencao(icao)
       enddo
      goto 80
 81   read(chrstr,1063) ntot,nsym
      if (caotype.eq.0) then
      if (printlevel.gt.1)
     .write(*,*) 'carttype(*,imapping)',imapping
      do i=1,icao
       t1=typcao(i)
       m1=maptype(t1,imapping)
      if (t1.eq.m1 ) then
        mapd2h(i)=i
      else
        mapd2h(i)=(m1-t1)*degencao(i)+i
      endif
      enddo
      else
      if (printlevel.gt.1)
     .write(*,*) 'sphtype(*,imapping)=',
     .   (sphtype(i,imapping),i=1,10)
      do i=1,icao
       t1=typcao(i)
       m1=sphtype(t1,imapping)
      if (t1.eq.m1 ) then
        mapd2h(i)=i
      else
        mapd2h(i)=(m1-t1)*degencao(i)+i
      endif
      enddo
      endif

      if (printlevel.gt.0) then
         write(*,*) 'mapd2h:'
         write(*,1063) (mapd2h(i),i=1,ntot)
         write(*,*) 'typcao:'
         write(*,1063) (typcao(i),i=1,ntot)
         write(*,*) 'degencao:'
         write(*,1063) (degencao(i),i=1,ntot)
         do i=1,iatom
         write(*,1019) i, (atfeld1(j,i),j=1,maxcaoperatom)
1019     format('atom',i3,' atfeld1(*)=',50i4)
         enddo
      endif

c at this point:  icentr(symmetry independent centres)
c                 typecao: s px,py ...
c                 dengencao: number of symmetry dependent centres
c
c       mapd2h    icentr     centre    typecao
c
c          1         1         1         s
c          2         1         2         s           degencao(i) = 2 her
c          3         1         1         px
c          4         1         2         px
c          5         1         1         py
c          6         1         2         py
c          7         1         1         pz
c          8         1         2         pz
c
c
c


c mapping(i) means: cao i corresponds to cao(i) under d2h
c if (mapcao) mapping der cao's
c if (not.mapcao) mapping der so's

      call izero_wr(8,nbfread,1)
      call izero_wr(maxbfn,maxdim,1)
      sonr=0
 110  read(unit,900,end=100) chrstr
      read(chrstr,1062) chrstr2
      if (chrstr2.eq.'Symmetr') then
       read(chrstr,1065) jj
      else
       sonr=sonr+1
       nbfread(jj)=nbfread(jj)+1
       read (chrstr,1060) maxdim(sonr)
       scr(1)='+'

c Originalorienteriung in d2horient
c neue Orientierung in neworient


       read (chrstr,1061) socao(sonr,1),(scr(i),
     .   socao(sonr,i),i=2,maxdim(sonr))
c
c 
c dalton normalizes SAOs to (number of GTOs)**2
       f=1.0d0/dble(maxdim(sonr))
       do k1=1,maxdim(sonr)
        if (scr(k1).eq.'+') then
          sosign(sonr,k1)=f
         else
         sosign(sonr,k1)=-f
         endif
        if (molden.eq.1) then
        if (caotype.eq.0) then
c
c dalton normalizes xx,yy,zz to 3 and xy,xz,yz to 1
c use normalization to unity throughout
c
        if (typcao(socao(sonr,k1)).eq.5 .or.
     .      typcao(socao(sonr,k1)).eq.8 .or.
     .      typcao(socao(sonr,k1)).eq.10 ) then
          sosign(sonr,k1)=sosign(sonr,k1)/sqrt(3.0d0)
         endif
c
c dalton:  xxx,yyy,zzz normalized to 15
c          xxy,xxz,xyy,xzz,yyz,yzz normalized to 3
c          xyz normalized to 1
c use normalization to unity throughout
c
        if (typcao(socao(sonr,k1)).eq.11 .or.
     .      typcao(socao(sonr,k1)).eq.17 .or.
     .      typcao(socao(sonr,k1)).eq.20 )
     .   sosign(sonr,k1)=sosign(sonr,k1)/sqrt(15.0d0)

        if (typcao(socao(sonr,k1)).eq.12 .or.
     .      typcao(socao(sonr,k1)).eq.13 .or.
     .      typcao(socao(sonr,k1)).eq.14 .or.
     .      typcao(socao(sonr,k1)).eq.16 .or.
     .      typcao(socao(sonr,k1)).eq.18 .or.
     .      typcao(socao(sonr,k1)).eq.19 )
     .   sosign(sonr,k1)=sosign(sonr,k1)/sqrt(3.0d0)
        else
c
c  dalton:  d-2,d-1,d1 normalized to 1
c           d0  normalized to 12 
c           d2  normalized to 4 
c use normalization to unity throughout
c
        if (typcao(socao(sonr,k1)).eq.7) 
     .       sosign(sonr,k1)=sosign(sonr,k1)/sqrt(12.0d0) 

        if (typcao(socao(sonr,k1)).eq.9 )
     .   sosign(sonr,k1)=sosign(sonr,k1)/2.0d0
c
c  dalton:  f-3  normalized to 24
c           f-2  normalized to  1
c           f-1  normalized to  40
c           f-0  normalized to  20/3
c           f1   normalized to  40 
c           f2   normalized to  4 
c           f3   normalized to  8/3
c
        if (typcao(socao(sonr,k1)).eq.10)
     .   sosign(sonr,k1)=sosign(sonr,k1)/sqrt(24.0d0)
        if (typcao(socao(sonr,k1)).eq.12)
     .   sosign(sonr,k1)=sosign(sonr,k1)/sqrt(40.0d0)
        if (typcao(socao(sonr,k1)).eq.13)
     .   sosign(sonr,k1)=sosign(sonr,k1)/sqrt(20.0d0/3.0d0)
        if (typcao(socao(sonr,k1)).eq.14)
     .   sosign(sonr,k1)=sosign(sonr,k1)/sqrt(40.0d0)
        if (typcao(socao(sonr,k1)).eq.15)
     .   sosign(sonr,k1)=sosign(sonr,k1)/2.0d0
        if (typcao(socao(sonr,k1)).eq.16)
     .   sosign(sonr,k1)=sosign(sonr,k1)/sqrt(8.0d0/3.0d0)
        endif
        endif
       enddo
       endif
       goto 110
 100  continue
       loc=0
       do k1=1,nsym
       loc=loc+nbfread(k1)
        enddo
       if (loc.ne.sonr) then
        call bummer ('build..., loc.ne.sonr, loc=',loc,2)
       endif

       if (printlevel.gt.2) then
       do i=1,iatom
       write(*,*) 'i=',i,' ',':', atominfo(i),':'
       write(*,*) ':',saveinfo(i),':'
       enddo
       endif
c apply mapping
       if (mapcao) then
       do i=1,iatom
          read (atominfo(i),*) x,y,z
          do j=1,iatom
            read (saveinfo(j),*) sx,sy,sz
            if ((abs(x-sx).le. 1.d-8).and.
     &          (abs(y-sy).le. 1.d-8).and.
     &          (abs(z-sz).le. 1.d-8)) then
                mapatoms(i)=j
                write(*,*) 'match:'
                write(*,*) x,y,z
                write(*,*) sx,sy,sz
                exit     
            endif
          enddo
        enddo
c
        do i=1,iatom
          if (mapatoms(i).eq.0)then
            write(*,*) 'The geometries are not matching!'
            write(*,*) 'Please check the geometries!'
            call bummer('mapatoms(i) invalid, i=',i,2)
          endif
        enddo
c
        if (printlevel.gt.0) then
        write(*,*)'mapatoms:',(mapatoms(i),i=1,iatom)
        write(*,*) ' d2h-atomfeld'
        do i=1,iatom
        write(*,1009) (atfeld2(j,i),j=1,20)
        enddo
        write(*,*) ' c2v-atomfeld'
        do i=1,iatom
        write(*,1009) (atfeld1(j,i),j=1,20)
        enddo
        write(*,*) 'count(*)=',(count(i),i=1,iatom)
        endif
1009    format(20i4)

        call izero_wr(maxbfn,mappingcao,1)
         ii=0
        do i=1,iatom
         do j=1,count(i)
           ii=ii+1
           ix=atfeld1(j,mapatoms(i))
           mappingcao(ix)=atfeld2(j,i)
c note atfac set to 1 througout !!
           faccao(ix)=dble(atfac(i))
         enddo
        enddo

        if (printlevel.gt.0) then
         write(*,*) 'mappingcao='
         write(*,*) (mappingcao(i),i=1,sonr)
        endif

       do i=1,sonr
         do j=1,maxdim(i)
          socao(i,j)=mappingcao(socao(i,j))
c         write(*,*) 'socao(i,j)=',socao(i,j),i,j
c         write(*,*)  faccao(socao(i,j))
c attach correction for normalization
          sosign(i,j)=sosign(i,j)*faccao(socao(i,j))

         enddo
       enddo
       endif
      return
      end

      subroutine calcinvmatrix(socao,sosign,maxdim,sonr,
     .  maxdimcao,invmatrix,caoso)
       implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

       integer socao(maxbfn,8),caoso(maxbfn,8)
       integer ipiv(8),ierr
       integer maxdim(maxbfn),maxdimcao(maxbfn)
       real*8 sosign(maxbfn,8),invmatrix(maxbfn,8)
       integer naux
       parameter (naux=2000)
       real*8 smallmatrix(64),rcond,det(10),aux(naux)
       logical sodone(maxbfn)
       integer i,j,k,sonr,k1,k2,so,mapso(8)
       real*8 f,symmthres,minprt
      integer molden,moorder,ixyz,caotype,printlevel,imapping
      integer motype,atfac
      common /inpdata/ molden,moorder(2*maxbfn),symmthres,
     .                 caotype,printlevel,ixyz,imapping
     .                ,atfac(maxatoms),motype,minprt

      do i=1,maxbfn
       sodone(i)=.false.
      enddo
       do 750 i=1,sonr
        if (sodone(i)) goto 750
        if (maxdim(i).eq.1) then
         j=socao(i,1)
         invmatrix(j,1)=1.0d0/sosign(i,1)
         maxdimcao(j)=maxdimcao(j)+1
         caoso(j,maxdimcao(j))=i
         sodone(i)=.true.
        else
          so=0
          call wzero(maxdim(i)*maxdim(i),smallmatrix,1)
         do j=i,sonr
           if (socao(j,1).eq.socao(i,1) .and. (.not.sodone(j))) then
            so=so+1
            mapso(so)=j
            do k=1,maxdim(i)
            smallmatrix(so+(k-1)*maxdim(i))=sosign(j,k)
            maxdimcao(socao(j,k))=maxdimcao(socao(j,k))+1
            caoso(socao(j,k),maxdimcao(socao(j,k)) )=j
            sodone(j)=.true.
            enddo
           endif
         enddo

c       call dgeicd(smallmatrix,maxdim(i),maxdim(i),0,
c    .     rcond,det,aux,naux)
        call dgetrf_wr(maxdim(i),maxdim(i),smallmatrix,maxdim(i),
     .       ipiv, ierr)
        if (ierr.ne.0)
     .  call bummer('dgetrf, ierr=',ierr,2)
          ierr=0
        call dgetri_wr(maxdim(i),smallmatrix,maxdim(i),ipiv,aux,
     .    naux,ierr)
        if (ierr.ne.0)
     .  call bummer('dgetri, ierr=',ierr,2)

         do j=1,maxdim(i)
          do k=1,maxdim(i)
           invmatrix(socao(mapso(j),k),j)=
     .         smallmatrix(k+(j-1)*maxdim(i))
          enddo
         enddo
        endif
 750   continue

        if (printlevel.gt.1) then
        write(*,*) 'CAO - to - SO'
        do i=1,sonr
         write(*,700) i,(caoso(i,j),j=1,8)
        enddo
 700     format('CAO =',i4,'SOs:',8i4)
 701     format('SO  =',i4,'CAOs:',8i4)
 699     format('invmatrix cao=',i4,'arr=',8f8.5)
         do i=1,sonr
         write(*,699) i,(invmatrix(i,j),j=1,8)
         enddo

        write(*,*) 'SO  - to - CAO'
        do i=1,sonr
         write(*,701) i,(socao(i,j),j=1,8)
        enddo
        endif

        return
        end


       subroutine readmocoef(unit,fname,mocoef,nsym,nbf,maxlen,
     .  index)
        implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

       integer index(maxbfn)
       integer mxtitle,ntitle,nsym,nbf(8),nmo(8),unit,clen,maxlen
       integer nbftot
       parameter (mxtitle=20)
       character*80 afmt,titles(mxtitle)
       character*(*) fname
        integer symmap(8)
       common /momap/symmap

       character*4 labels(8)
       real*8 mocoef(*),symmthres,minprt
       integer ii,jj,ierr,i,filerr,syserr
      integer molden,moorder
      integer ixyz,caotype,printlevel,imapping,atfac,motype
      common /inpdata/ molden,moorder(2*maxbfn),symmthres,
     .                 caotype,printlevel,ixyz,imapping,
     .                 atfac(maxatoms),motype,minprt

      namelist /input/ printlevel,ixyz,atfac,motype,molden,
     .                 moorder,symmthres,caotype,minprt


       integer index2
       integer nmooff(8)
       real*8 energie,occnum
       character*2 atomlabel
       character*8 symlabel
       integer ordzahl
       common /moldeninfo/ ordzahl(maxatoms),index2(maxbfn+1),
     .  occnum(maxbfn),energie(maxbfn),atomlabel(maxatoms)
     .  ,symlabel(maxbfn)


      write(*,*) 'mocoef file:',fname,' unitno=',unit
      open(unit,file=fname,status='unknown')
      clen=100
      call moread(unit,10,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbf,nmo,labels,clen,mocoef)

      write(*,*) 'Titles from mocoef file:'
      write(*,*) (titles(i),i=1,ntitle)
      write(*,900) nsym
      write(*,901) (nbf(i),i=1,nsym)
      write(*,902) (nmo(i),i=1,nsym)
      nbftot=0
      nmooff(1)=0
      do i=1,nsym
       nbftot=nbftot+nbf(i)
       if (i.lt.nsym) nmooff(i+1)=nmooff(i)+nmo(i)
      enddo

 900  format(' number of irreducible representations:',i4)
 901  format(' number of basis functions per irrep:',8i4)
 902  format(' number of molecular orbitals per irrep:',8i4)
 903  format(' irrep ',i3,' MO ', i3, 'incorrect value:',i4)

      ierr=0
      clen=0
      do i=1,nsym
       clen=clen+nmo(i)*nbf(i)
      enddo
      if (clen.gt.maxlen) then
        call bummer('readmocoef: clen > maxlen',clen,2)
      endif

      write(*,*) 'reading mocoefficients'
      call moread(unit,20,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbf,nmo,labels,clen,mocoef)

      clen=0
      do i=1,nsym
       clen=clen+nmo(i)
      enddo

      if (motype.eq.0 ) then

      write(*,*) 'reading occupation numbers'
      call moread(unit,40,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbf,nmo,labels,clen,occnum)
      write(*,*) 'reading energies'
      call moread(unit,30,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbf,nmo,labels,clen,energie)
       write(*,*) 'original energie'
       write(*,1001) (energie(i),i=1,nbftot)
c sort MOs into ascending energy order
       call sort(.true.,energie ,index,nbftot,index2)
       write(*,*) 'energie(index(i)) '
       write(*,1002) (energie(index(i)),i=1,nbftot)
       write(*,*) 'occnum(index(i)) '
       write(*,1002) (occnum(index(i)),i=1,nbftot)

       elseif(motype.eq.1) then

       write(*,*) 'reading occupation numbers '
       call wzero(clen,energie,1)
       call moread(unit,40,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbf,nmo,labels,clen,occnum)
c sort NOs in descending occupation number order
       write(*,*) 'original occnumbers'
       write(*,1000) (occnum(i),i=1,nbftot)
       call sort(.false.,occnum,index,nbftot,index2)
       write(*,*) 'occnum(index(i)) '
       write(*,1000) (occnum(index(i)),i=1,nbftot)
       write(*,*) 'occnum(index2(i)) '
       write(*,1000) (occnum(index2(i)),i=1,nbftot)
       else
c
c convert the sorting indices
c
       call wzero(nbftot,energie,1)
c formally set occupation numbers to 1 throughout
       do ii=1,nbftot
         occnum(ii)=1.0d0
       enddo

       jj=1
       do ii=1,nbftot
        if ((moorder(jj+1).eq.0).or.(moorder(jj).eq.0)) goto 100
        energie(nmooff(moorder(jj))+moorder(jj+1))=ii
        jj=jj+2
       enddo
 100   continue
       ii=(jj-1)/2
       do jj=1,nbftot
        if (energie(jj).lt.1.0d0) then
            ii=ii+1
            energie(jj)=ii
        endif
       enddo
       call sort(.true.,energie ,index,nbftot,index2)
       write(*,1002) (index(i),i=1,nbftot)
       endif
c
c  add symlabels
c
        i=0
        do ii=1,nsym
         do jj=1,nmo(ii)
           i=i+1
           write(symlabel(i),101) jj,labels(ii)
         enddo
        enddo
       do ii=1,i
        do jj=2,8
         if (symlabel(ii)(jj:jj).eq.' ')
     .       symlabel(ii)(jj:jj)='_'
        enddo
       enddo


101     format(1x,i3,a4)

1000   format('occnum(*)',10f8.5)
1001   format('energy(*)',10f8.4)
1002   format('index(*)',20i4)

      close(unit)
       return
       end


       subroutine sort(up,tosort,index,n,index2)
c  selection sort
c  index(*) contains the correct order of the elements

        implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)
       logical up

         integer k,i,j,n,t
        real*8 tosort(n)
         integer index(n)
         integer index2(maxbfn),itmp(maxbfn)

        do i=1,n
        index(i)=i
        enddo

        if (up) then
        do i=1,n-1
          do j=i+1,n
          if (tosort(index(j)).lt.tosort(index(i))) then
             t=index(i)
             index(i)=index(j)
             index(j)=t
          endif
          enddo
        enddo
        else
        do i=1,n-1
          do j=i+1,n
          if (tosort(index(j)).gt.tosort(index(i))) then
             t=index(i)
             index(i)=index(j)
             index(j)=t
          endif
          enddo
        enddo
        endif

c now index(x) means  MO (index(x)) at the xth position
c invert this
        do i=1,n
        index2(i)=index(i)
        itmp(index(i))=i
        enddo
        do i=1,n
        index(i)=itmp(i)
        enddo
c index(i) means ith position of MO index(i)
        return
        end


      subroutine transformtocao(mocoef,mocoefcao,maxdimcao,caoso,
     .    invmatrix,nbf,nsym,index)
       implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

      real*8 tmp,mocoef(*),mocoefcao(*),invmatrix(maxbfn,8)
      integer maxdimcao(maxbfn),caoso(maxbfn,8),nbf(8),nsym
      integer index(*)
       real*8 scratch(maxbfn*maxbfn)

      integer icao,iso,so,mo, soirrep(maxbfn),nbftot
       integer noffset(9),noffset2(9)
       integer offsetcao,offsetsao,endmo,startmo,i,j,k,ix
       real*8 wtmp(4),symmthres,minprt
        integer symmap(8)
       common /momap/symmap
       integer mapd2h,typcao,degencao,iatom
       common /d2hmapping/ mapd2h(maxbfn),typcao(maxbfn),
     .      degencao(maxbfn),iatom
      integer molden,moorder,caotype,printlevel,ierr,ixyz,imapping
      integer atfac,motype
      common /inpdata/ molden,moorder(2*maxbfn),symmthres,
     .                 caotype,printlevel,ixyz,imapping
     .                ,atfac(maxatoms),motype,minprt

       noffset(1)=0
       noffset2(1)=0
       k=0
       do i=1,nsym
       if (i.gt.1) then
        noffset(i)=noffset(i-1)+nbf(i-1)
        noffset2(i)=noffset2(i-1)+nbf(i-1)*nbf(i-1)
       endif
         do j=1,nbf(i)
             k=k+1
            soirrep(k)=i
         enddo
       enddo
       nbftot=noffset(nsym)+nbf(nsym)
       noffset(nsym+1)=noffset(nsym)+nbf(nsym)
       noffset2(nsym+1)=noffset2(nsym)+nbf(nsym)*nbf(nsym)
       if (printlevel.gt.1) then
       write(*,900) (noffset(i),i=1,nsym)
       write(*,901) (noffset2(i),i=1,nsym)
       write(*,902) nbftot,nsym,(nbf(i),i=1,nsym)
       endif
 900   format('noffset:',8i4)
 901   format('noffset2:',8i4)
 902   format('nbftot,nsym,=',2i4,'nbf',8i4)



       call wzero(nbftot*nbftot,mocoefcao,1)
       do icao=1,nbftot
         do iso = 1, maxdimcao(icao)
          so = caoso(icao,iso)
c          startmo=noffset(symmap(soirrep(so)))+1
c          endmo  =noffset(symmap(soirrep(so)))+
c    .   nbf(symmap(soirrep(so)))
           startmo=noffset((soirrep(so)))+1
           endmo  =noffset((soirrep(so)))+
     .   nbf((soirrep(so)))
           ix = noffset(soirrep(so))
c sort MOs in proper order!
           offsetsao=noffset2(soirrep(so))
           do mo=startmo,endmo
           offsetcao=(index(mo)-1)*nbftot
            mocoefcao(offsetcao+icao)=mocoefcao(offsetcao+icao)+
     .       mocoef(offsetsao+so-ix)*invmatrix(icao,iso)
             offsetsao=offsetsao+nbf(soirrep(so))
           enddo
         enddo
        enddo
c  vertausche die Zeilen wie in mapd2h(i)

c
ctmp
c      return
       if (caotype.eq.0) then
        do i=1,nbftot
           offsetcao=0
          do j= 1, nbftot
           scratch(offsetcao+mapd2h(i))=mocoefcao(offsetcao+i)
           offsetcao=offsetcao+nbftot
          enddo
         enddo
       else
c  falls sphaerisch, dann muessen noch linearkombinationen gebildet
c  werden
c  Bei spharisch ist itype=10 = f-3!!!
        do i=1,nbftot
          if (typcao(i).gt.10) goto 107
          if (typcao(i).eq.10) then
        call ftransform(scratch(i),mocoefcao(i),nbftot,imapping,
     .   degencao(i))
          goto 107
          endif
          if (typcao(i).gt.5) goto 107
          if (typcao(i).eq.5) then
        call dtransform(scratch(i),mocoefcao(i),nbftot,imapping,
     .   degencao(i))
          goto 107
          endif
 108       offsetcao=0
           do j=1,nbftot
            scratch(offsetcao+mapd2h(i))=mocoefcao(offsetcao+i)
            offsetcao=offsetcao+nbftot
           enddo

 107      continue
         enddo

       endif
        call dcopy_wr (nbftot*nbftot,scratch  ,1,mocoefcao,1)
         return
         end


       subroutine transformtosao(mocoef,
     .  xmocoef,maxdim,socao,sosign,nbf,nsym)
         implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

        real*8 mocoef(*), xmocoef(*),sosign(maxbfn,8)
        real*8 scratch(maxbfn*maxbfn)
        integer maxdim(maxbfn),socao(maxbfn,8),nbf(8),nsym

        integer nbftot,noffset(9),noffset2(9)
        integer sao,isym,icao,cao,offset,offset2
        integer mosort(maxbfn)
        integer mobf(8)
         logical modone(maxbfn)
         real*8 fac, symmthres,minprt
         integer i,mo,bf
       integer mapd2h,typcao,degencao,iatom
       common /d2hmapping/ mapd2h(maxbfn),typcao(maxbfn),
     .      degencao(maxbfn),iatom
      integer molden,moorder,ixyz,caotype,printlevel,imapping
      integer atfac,motype
      common /inpdata/ molden,moorder(2*maxbfn),symmthres,
     .                 caotype,printlevel,ixyz,imapping
     .                ,atfac(maxatoms),motype,minprt

       noffset(1)=0
       noffset2(1)=0
       do i=1,nsym
       if (i.gt.1) then
        noffset(i)=noffset(i-1)+nbf(i-1)
        noffset2(i)=noffset2(i-1)+nbf(i-1)*nbf(i-1)
       endif
       enddo
       nbftot=noffset(nsym)+nbf(nsym)
       noffset(nsym+1)=noffset(nsym)+nbf(nsym)
       noffset2(nsym+1)=noffset2(nsym)+nbf(nsym)*nbf(nsym)

        call wzero(noffset2(nsym+1),xmocoef,1)
        call wzero(noffset(nsym+1)**2,scratch,1)
c Annahme SOs liegen in dichter Folger der irreps vor!
        sao=0
        do isym=1,nsym
          do bf=1,nbf(isym)
           sao=sao+1
         if (printlevel.gt.1) then
           write(*,*) 'socao(',sao,',*)=',
     . (socao(sao,icao),icao=1,maxdim(sao))
         endif
           do icao=1,maxdim(sao)
            cao=socao(sao,icao)
            fac=sosign(sao,icao)
             offset=noffset2(isym)+bf
             offset2=0
            do mo=1,nbftot
      scratch(offset2+(sao))=scratch(offset2+(sao))
     .    +fac*mocoef(offset2+cao)
             offset2=offset2+nbftot
            enddo
           enddo
          enddo
         enddo
c scratch enthaelt die MOs, die noch geblockt und sortiert werden muesse
      if (printlevel.gt.0) then
      call prblks('scratch',scratch,1,nbftot,nbftot,'A','A',1,6)
      endif

        call izero_wr(8,mobf,1)
        call izero_wr(maxbfn,mosort,1)
        do i=1,maxbfn
          modone(i)=.false.
        enddo
          do isym=1,nsym
           offset=0
           do 150 mo=1,nbftot
           if (modone(mo)) goto 150
             sao=noffset(isym)
           do 155 bf=1,nbf(isym)
             sao=sao+1
             if (abs(scratch(offset+sao)).gt.1.d-8) then
              mobf(isym)=mobf(isym)+1
              mosort(mobf(isym)+noffset(isym))=mo
              modone(mo)=.true.
              goto 150
             endif
 155        continue
 150          offset=offset+nbftot
 151        continue
           enddo
          if (printlevel.gt.0 ) then
         write(*,500) (mosort(i),i=1,sao)
 500    format('mosort',20i4)
         write(*,511) (socao(i,1),i=1,sao)
 511    format('socao',20i4)
          endif

           sao=0
          do isym=1,nsym
             do bf=1,nbf(isym)
               sao=sao+1
               do i=1,nbf(isym)
              xmocoef(noffset2(isym)+bf+(i-1)*nbf(isym))=
     .          scratch((mosort(i+noffset(isym))-1)*nbftot+sao)
              enddo
             enddo
           enddo


         return
         end



       subroutine writemocoef(unit,fname,mocoef,nsym,nbf,maxlen,
     .   motype,occnum,index)

        implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

       integer mxtitle,ntitle,nsym,nbf(8),nmo(8),unit,clen,maxlen
       integer nbftot
       integer motype,index(*)
       real*8 occnum(*) 
       parameter (mxtitle=20)
       character*80 afmt,titles(mxtitle)
       character*(*) fname
       real*8 orbenrg(maxbfn)

       character*4 labels(8)
       real*8 mocoef(*)
       integer ierr,i,filerr,syserr
       data labels /'SYM1','SYM2','SYM3','SYM4',
     & 'SYM5','SYM6','SYM7','SYM8'/

        call wzero(maxbfn,orbenrg,1)

      write(*,*) 'mocoef file:',fname,' unitno=',unit
c      afmt='(4f18.12)'
       afmt='(1p3d25.15)'
      ntitle=1
      titles(1)='transformed mo coefficients '
      call siftdy(titles(1)(41:80))
      open(unit,file=fname,status='unknown')
      call mowrit(unit,10,filerr,syserr,ntitle,titles,
     . afmt,nsym,nbf,nbf,labels,mocoef)

 900  format(' number of irreducible representations:',i4)
 901  format(' number of basis functions per irrep:',8i4)
 902  format(' number of molecular orbitals per irrep:',8i4)
 903  format(' irrep ',i3,' MO ', i3, 'incorrect value:',i4)

      ierr=0

      write(*,*) 'writing mocoefficients'
      call mowrit(unit,20,filerr,syserr,ntitle,titles,
     . afmt,nsym,nbf,nbf,labels,mocoef)

       if (motype.eq.1) then 
        do i=1,sum(nbf(1:nsym))
         mocoef(i)=occnum(index(i))
        enddo 
      write(*,*) 'writing occupation numbers (assuming C1)'
      call mowrit(unit,40,filerr,syserr,ntitle,titles,
     . afmt,nsym,nbf,nbf,labels,mocoef)
       else

      write(*,*) 'writing mo enrgies dummies'
      call mowrit(unit,30,filerr,syserr,ntitle,titles,
     . afmt,nsym,nbf,nbf,labels,orbenrg)
       endif
      close(unit)
      return
      end


      subroutine readinput

       implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       real*8 symmthres
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)


      integer i,ixyz,caotype,printlevel,ierr,imapping,atfac,motype
      integer molden,moorder(2*maxbfn)
      real*8  minprt
      common /inpdata/ molden,moorder,symmthres,
     .   caotype,printlevel,ixyz,imapping,atfac(maxatoms),motype,
     .   minprt

      namelist /input/ printlevel,ixyz,atfac,motype,molden,
     .                 moorder,symmthres,caotype,minprt

      character*1 chr
c     namelist defaults


      molden=0
      caotype=-1
      printlevel=0
      ixyz=-1
      motype=-1
      do i=1,maxatoms
       atfac(i)=1.0d0
      enddo
      symmthres=1.0d-6
      minprt = -1.0d0 

      call izero_wr(2*maxbfn,moorder,1)
      open (unit=8,file='transmoin',status='unknown')
      call echoin(8,6,ierr)
      rewind(8)

      read (8,input,end=100)
 100  continue
      close(8)

      open (unit=8,file='daltaoin',ERR=999,status='unknown')
      read(8,'(a1)') chr
      read(8,'(a1)') chr
      read(8,'(a1)') chr
      read(8,'(a1)') chr

      if (chr.eq.'s') then
        caotype=1
        elseif (chr.eq.'c') then
          caotype=0
          else
          call bummer('invalid daltaoin file structure',0,2)
      endif
      close(8)

      if (caotype.eq.0) then
       write(*,*) 'assuming cartesian basis functions '
      elseif (caotype.eq.1) then
       write(*,*) 'assuming spherical basis functions '
      else
      call bummer ('invalid caotype ', caotype,2)
      endif

      if (molden.ne.0 .and. molden.ne.1)
     . call bummer ('invalid molden ',molden,2)

      if (motype.lt.0 .or. motype.gt.2)
     . call bummer ('invalid motype ',motype,2)


      if (molden.eq.1) then
           imapping=7
       elseif (ixyz.eq.123) then
          imapping=1
       elseif (ixyz.eq.231) then
          imapping=2
       elseif (ixyz.eq.132) then
          imapping=3
       elseif (ixyz.eq.213) then
          imapping=4
       elseif (ixyz.eq.312) then
           imapping=5
       elseif (ixyz.eq.321) then
           imapping=6
        else
       call bummer ('invalid ixyz=',ixyz,2)
       endif

      return
 999   call bummer('daltaoin file missing',0,2)
      return
      end


c        ftransform(scratch(i),mocoefcao(i),nbftot,imapping,
c    .   degencao(i))

         subroutine ftransform( scr,moc,nbftot,icode,idegen)
          implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

         real*8 scr(*),moc(*)
         integer nbftot,icode,idegen
         integer offset,j,k

c  f3- scr(1)
c  f2- scr(1+idegen)
c  f1- scr(1+2*idegen)
c  f0  scr(1+3*idegen)
c  f1  scr(1+4*idegen)
c  f2  scr(1+5*idegen)
c  f3  scr(1+6*idegen)

          offset=1

          goto (10,50,30,40,20,60,70) icode

          call bummer ('ftransform invalid icode=',icode,2)

c
c  normal order
c
 10      do j=1,nbftot
           do k=0,6
           scr(offset+k*idegen)=moc(offset+k*idegen)
           enddo
          offset=offset+nbftot
         enddo
         return
c
c  molden order
c
 70      do j=1,nbftot
           scr(offset+6*idegen)=moc(offset+0*idegen)
           scr(offset+4*idegen)=moc(offset+1*idegen)
           scr(offset+2*idegen)=moc(offset+2*idegen)
           scr(offset+0*idegen)=moc(offset+3*idegen)
           scr(offset+1*idegen)=moc(offset+4*idegen)
           scr(offset+3*idegen)=moc(offset+5*idegen)
           scr(offset+5*idegen)=moc(offset+6*idegen)
           offset=offset+nbftot
         enddo
         return
 40      do j=1,nbftot
           scr(offset)= moc(offset+6*idegen)/3.0d0
           scr(offset+idegen )=  moc(offset+idegen)
           scr(offset+2*idegen )=moc(offset+4*idegen)
           scr(offset+3*idegen )=moc(offset+3*idegen)
           scr(offset+4*idegen )=moc(offset+2*idegen)
           scr(offset+5*idegen )=-moc(offset+5*idegen)
           scr(offset+6*idegen )=moc(offset)*3.0d0
           offset=offset+nbftot
          enddo
          return
 20      do j=1,nbftot
           scr(offset)= 5.0d0/60.0d0*moc(offset+6*idegen)
     .                    +1.25d0*moc(offset+4*idegen)
           scr(offset+idegen)   = moc(offset+idegen)
           scr(offset+2*idegen) = -0.25d0*moc(offset+4*idegen)
     .                            +0.25d0*moc(offset+6*idegen)
           scr(offset+3*idegen) =  1.50d0*moc(offset+0*idegen)
     .                            +1.50d0*moc(offset+2*idegen)
           scr(offset+4*idegen) = 0.25d0*moc(offset+3*idegen)
     .                            -0.25d0*moc(offset+5*idegen)
           scr(offset+5*idegen) =-1.50d0*moc(offset+0*idegen)
     .                           +2.50d0*moc(offset+2*idegen)
           scr(offset+6*idegen) = 1.25d0*moc(offset+3*idegen)
     .                           +0.75d0*moc(offset+5*idegen)
          offset=offset+nbftot
          enddo
          return

 30      do j=1,nbftot
           scr(offset)=   +10.0d0/24.0d0*moc(offset+3*idegen)
     .                    +0.25d0*moc(offset+5*idegen)
           scr(offset+idegen)   = moc(offset+idegen)
           scr(offset+2*idegen) =  0.25d0*moc(offset+3*idegen)
     .                            -0.25d0*moc(offset+5*idegen)
           scr(offset+3*idegen) =  1.50d0*moc(offset)
     .                            +1.50d0*moc(offset+2*idegen)
           scr(offset+4*idegen) =-0.25d0*moc(offset+4*idegen)
     .                            +0.25d0*moc(offset+6*idegen)
           scr(offset+5*idegen) = 1.50d0*moc(offset)
     .                           -2.50d0*moc(offset+2*idegen)
           scr(offset+6*idegen) = 3.75d0*moc(offset+4*idegen)
     .                           +0.25d0*moc(offset+6*idegen)
          offset=offset+nbftot
          enddo
          return

 50      do j=1,nbftot
           scr(offset)=   -0.25d0*moc(offset+5*idegen)
     .                    +10.0d0/24.0d0*moc(offset+3*idegen)
           scr(offset+idegen)   = moc(offset+idegen)
           scr(offset+2*idegen) =  0.25d0*moc(offset+3*idegen)
     .                            +0.25d0*moc(offset+5*idegen)
           scr(offset+3*idegen) =  1.50d0*moc(offset+4*idegen)
     .                            +0.50d0*moc(offset+6*idegen)
           scr(offset+4*idegen) =  0.75d0*moc(offset+0*idegen)
     .                            -0.25d0*moc(offset+2*idegen)
           scr(offset+5*idegen) =-2.50d0*moc(offset+4*idegen)
     .                            +0.50d0*moc(offset+6*idegen)
           scr(offset+6*idegen) = 0.75d0*moc(offset+0*idegen)
     .                           +3.75d0*moc(offset+2*idegen)
          offset=offset+nbftot
          enddo
          return

 60      do j=1,nbftot
           scr(offset)=   +0.25d0*moc(offset)
     .                    +1.25d0*moc(offset+2*idegen)
           scr(offset+idegen)   = moc(offset+idegen)
           scr(offset+2*idegen) =  0.75d0*moc(offset)
     .                            -0.25d0*moc(offset+2*idegen)
           scr(offset+3*idegen) =  1.50d0*moc(offset+4*idegen)
     .                            +0.50d0*moc(offset+6*idegen)
           scr(offset+4*idegen) = 0.25d0*moc(offset+3*idegen)
     .                            +0.25d0*moc(offset+5*idegen)
           scr(offset+5*idegen) =2.50d0*moc(offset+4*idegen)
     .                           -0.50d0*moc(offset+6*idegen)
           scr(offset+6*idegen) = 1.25d0*moc(offset+3*idegen)
     .                           -0.75d0*moc(offset+5*idegen)
c          write(*,*) 'j=',j,'idegen=',idegen
c          do k=0,13
c         write(*,1000) offset+k,scr(offset+k),moc(offset+k)
c         enddo
1000     format('offset=',i4,'scr=',f10.8,'moc=',f10.8)
          offset=offset+nbftot

          enddo
          return
          end







       subroutine printmolden(mocoef,d2hinfo,atd2h,nsym,nbf)
        implicit none 
        integer maxatoms
       integer maxbfn,maxcaoperatom
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)
       character*80 d2hinfo(maxatoms)
       integer nsym,nbf(nsym)
       real*8 mocoef(*)
       integer atd2h(maxcaoperatom,maxatoms)
       integer mapd2h,typcao,degencao,iatom
       common /d2hmapping/ mapd2h(maxbfn),typcao(maxbfn),
     .      degencao(maxbfn),iatom

       integer index
       integer nmooff(8)
       real*8 energie,occnum,symmthres
       character*2 atomlabel,atome(0:36)
       character*8 symlabel
       integer ordzahl
       common /moldeninfo/ ordzahl(maxatoms),index(maxbfn+1),
     .  occnum(maxbfn),energie(maxbfn),atomlabel(maxatoms)
     .  ,symlabel(maxbfn)



      integer molden,moorder
      real*8 minprt
      integer ixyz,caotype,printlevel,ierr,imapping,atfac,motype
      common /inpdata/ molden,moorder(2*maxbfn),symmthres,
     .                caotype,printlevel,ixyz,imapping,
     .                atfac(maxatoms),motype,minprt

       real*8 itemp,gtoscale(510)
       character*80 chrstr

       data atome /'X ',
     .             'H ','He','Li','Be','B ','C ','N ','O ','F ','Ne',
     .                       'Na','Mg','Al','Si','P ','S ','Cl','Ar',
     .                       'K','Ca','Sc','Ti','V','Cr','Mn','Fe',
     .                       'Co','Ni','Cu','Zn','Ga','Ge','As','Se',
     .                       'Br','Kr'/


       integer icnt,offmo,mo,i,j,k,l,nbftot,outcnt,igto
       integer fstrlen
       external fstrlen

       do i=1,maxatoms
          ordzahl(i)=-1
       enddo

       call wzero(510,gtoscale,1)
        icnt=0
       open(10,file='soinfo.dat',status='unknown')
 60     read(10,61) chrstr
        if (chrstr(1:8).eq.'GTOSCALE') then
         icnt=icnt+1
         read(chrstr,62) gtoscale(icnt)
         goto 60
        endif
        close(10)
        igto=1

 61    format(a80)
 62    format(10x,f16.10)


       open(unit=20,file='molden',status='unknown')

       nbftot=0
       do i=1,nsym
         nbftot=nbftot+nbf(i)
       enddo

       write(20,'(a)') '[Molden Format]'
       write(20,'(a)') '[Atoms] AU'
       do i=1,iatom
         write(6,'(a,i2,a,a)') 'atomlabel(',i,')=',atomlabel(i)
         do j=0,36
          if (atomlabel(i).eq.atome(j)) ordzahl(i)=j
         enddo
         if (ordzahl(i).eq.-1)
     .     call bummer('printmolden: add atoms beyond Kr',0,2)
         if (fstrlen(d2hinfo(i)).gt.55)
     .     call bummer('change format string 63 in printmolden',
     .     fstrlen(d2hinfo(i)),2)

        write(20,63) atomlabel(i), i, ordzahl(i) ,d2hinfo(i)(1:55)
63    format(2x,a2,2x,i3,2x,i3,2x,a55)
       enddo
       if (caotype.ne.0) then
         write(20,'(a)') '[5D7F]'
       endif
       write(20,'(a)') '[GTO]'
       call readdaltaoin(iatom,gtoscale,igto,d2hinfo)

       write(20,*) ' '
       write(20,'(a)') '[MO]'


       do mo=1,nbftot
       if (occnum(index(mo)).lt. minprt) cycle  
       offmo=((mo)-1)*nbftot
       write(20,*) 'Sym=',symlabel(index(mo))
       write(20,*) 'Ene=',energie(index(mo))
       write(20,*) 'Spin= Alpha'
       write(20,*) 'Occup= ',occnum(index(mo))
       outcnt=0
       do i=1,iatom
        k=1
  305   continue
         if (atd2h(k,i).gt.0) then
           outcnt=outcnt+1
           write(20,*) outcnt,mocoef(offmo+atd2h(k,i))
           goto 299
ctm
c      # automatic transformation from spherical dalton input
c      # to cartesian molden input currently disabled
c
ctm
c          write(20,*) 'typcao(atd2h(k,i)),caotype=',
c    .                  typcao(atd2h(k,i)),caotype
c    .                 ,'atd2h(',k,',',i,')=',atd2h(k,i)
c
c          s,p AOs
c
c ############# something looks wrong here.  the following statements
c ############# cannot possibly be executed. 20-dec-01  -rls
           if (typcao(atd2h(k,i)).lt.5 .or. caotype.eq.0) then
           outcnt=outcnt+1
           write(20,*) outcnt,mocoef(offmo+atd2h(k,i))
           else
c
c          d AOs
c
           if (typcao(atd2h(k,i)).eq.5 .and. caotype.ne.0 ) then
c MOs in molden order: d0,d1,d-1,d2,d-2
c  MOLDEN order spherical d -> cartesian d
c             xx            xy         xz         yy        yz
c  d0    0: -0.1667     0.0000     0.0000    -0.1667     0.0000     0.33
c  d1    1:  0.0000     0.0000     1.0000     0.0000     0.0000     0.00
c  d-1   2:  0.0000     0.0000     0.0000     0.0000     1.0000     0.00
c  d2    3:  0.5000     0.0000     0.0000    -0.5000     0.0000     0.00
c  d-2   4:  0.0000     1.0000     0.0000     0.0000     0.0000     0.00

c xx
c          write(20,*) 'dorb'
           itemp= -1.0d0/6.0d0*mocoef(offmo+atd2h(k,i))*2.0d0
     .            +0.5d0*mocoef(offmo+atd2h(k+3,i))*sqrt(2.0d0)
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c          write(20,*) outcnt,itemp*sqrt(3.0d0)
c yy
           itemp= -1.0d0/6.0d0*mocoef(offmo+atd2h(k,i))*2.0d0
     .            -0.5d0*mocoef(offmo+atd2h(k+3,i))*sqrt(2.0d0)
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c          write(20,*) outcnt,itemp*sqrt(3.0d0)
c zz
           itemp=  1.0d0/3.0d0*mocoef(offmo+atd2h(k,i))*2.0d0
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c          write(20,*) outcnt,itemp*(sqrt(3.0d0))
c xy ok
           itemp= mocoef(offmo+atd2h(k+4,i))*0.5d0
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c xz ok
           itemp=mocoef(offmo+atd2h(k+1,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c yz ok
           itemp=mocoef(offmo+atd2h(k+2,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
           k=k+4
           else
c
c          f AOs
c
c MOLDEN spherical f -> cartesian f
c             xxx         xxy        xxz       xyy         xyz
c  f0    0:  0.0000     0.0000     0.3000     0.0000     0.0000
c  f1    1: -0.1500     0.0000     0.0000    -0.0500     0.0000
c  f-1   2:  0.0000    -0.0500     0.0000     0.0000     0.0000
c  f2    3:  0.0000     0.0000     0.5000     0.0000     0.0000
c  f-2   4:  0.0000     0.0000     0.0000     0.0000     1.0000
c  f3    5: -0.7500     0.0000     0.0000     0.7500     0.0000
c  f-3   6:  0.0000     0.2500     0.0000     0.0000     0.0000
c
c MOLDEN spherical f -> cartesian f
c               xzz       yyy        yyz        yzz        zzz
c  f0    0:    0.0000     0.0000     0.3000     0.0000    -0.6000
c  f1    1:    0.2000     0.0000     0.0000     0.0000     0.0000
c  f-1   2:    0.0000    -0.1500     0.0000     0.2000     0.0000
c  f2    3:    0.0000     0.0000    -0.5000     0.0000     0.0000
c  f-2   4:    0.0000     0.0000     0.0000     0.0000     0.0000
c  f3    5:    0.0000     0.0000     0.0000     0.0000     0.0000
c  f-3   6:    0.0000    -0.2500     0.0000     0.0000     0.0000
c


           if (typcao(atd2h(k,i)).eq.10 .and. caotype.eq.1 ) then
c          write(20,*) 'forb'
c xxx
           itemp= -0.15d0*mocoef(offmo+atd2h(k+1,i))
     .            -0.75d0*mocoef(offmo+atd2h(k+5,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c yyy
           itemp= -0.15d0*mocoef(offmo+atd2h(k+2,i))
     .            -0.25d0*mocoef(offmo+atd2h(k+6,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c zzz
           itemp=  0.60d0*mocoef(offmo+atd2h(k,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c xyy
           itemp= -0.05d0*mocoef(offmo+atd2h(k+1,i))
     .            +0.75d0*mocoef(offmo+atd2h(k+5,i))
           outcnt=outcnt+1
c xxy
           itemp= -0.05d0*mocoef(offmo+atd2h(k+2,i))
     .            +0.25d0*mocoef(offmo+atd2h(k+6,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c xxz
           itemp= +0.30d0*mocoef(offmo+atd2h(k+0,i))
     .            +0.50d0*mocoef(offmo+atd2h(k+3,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c xzz
           itemp=  0.20d0*mocoef(offmo+atd2h(k+1,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c yzz
           itemp=  0.20d0*mocoef(offmo+atd2h(k+2,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c yyz
           itemp=  0.30d0*mocoef(offmo+atd2h(k,i))
     .            -0.50d0*mocoef(offmo+atd2h(k+3,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
c xyz
           itemp=  1.00d0*mocoef(offmo+atd2h(k+4,i))
           outcnt=outcnt+1
           write(20,*) outcnt,itemp
           k=k+6
           endif
           endif
           endif
 299       continue
         endif
         k=k+1
         if (k.le.maxcaoperatom) goto 305
 300     continue
       enddo
      enddo
 400  continue
      close(20)
      return
      end

      subroutine readdaltaoin(iatom,gtoscale,igto,d2hinfo)
       implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       integer max_contr_blocks
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)
       parameter (max_contr_blocks = 20)

      character*80 line,d2hinfo(maxatoms)
       logical lx,ly,lz
      character*80 h1,h2
      character*2 aotype(4)
      integer  iprim(4,max_contr_blocks), icont(4,max_contr_blocks)
      real*8  expcon(4,max_contr_blocks,20,20),charge
      integer kk,i,j,k,l,m,ii,iatom,ll
       integer nofeqat,nofneqat,noflval,lvalues(4),currentatom

       integer igto
       real*8  gtoscale(*)
       integer index
       integer nmooff(8)
       real*8 energie,occnum,x,y,z,xd,yd,zd
       character*2 atomlabel
       character*8 symlabel
       integer ordzahl, nofeqatd
       common /moldeninfo/ ordzahl(maxatoms),index(maxbfn+1),
     .  occnum(maxbfn),energie(maxbfn),atomlabel(maxatoms)
     .  ,symlabel(maxbfn)




      data aotype /' s',' p',' d',' f'/

c
c when producing an daltaoin input with iargos
c there is only one basis set per atom type
c and all atoms are in one group
c

      open(unit=21,file='daltaoin',form='formatted',status='unknown')
      read(21,'(a10)')line
      read(21,'(a10)')line
      read(21,'(a10)')line
      read(21,'(3x,i3)') nofneqat
      currentatom=0
c     write(*,*) 'subroutine readdaltaoin:'
c     nofneqat = number of atom blocks with different basis sets
c     nofeqatd = number of symmetry-non-equivalent atoms within this block 
c     nofeqat  = completed number of atoms within this block 
      do ii=1,nofneqat     
        read(21,'(a70)') line
        read(line,'(5x,f7.3)') charge
        read(line,'(10x,i5)') nofeqatd
        read(line,'(15x,1i5)') noflval
        read(line,'(20x,10i5)') (lvalues(i),i=1,noflval)
c the atom labels *solely* distinguish between blocks of atoms
c this is necessary for symmetry treatment 
        nofeqat=0
        do i=1,nofeqatd
          read(21,'(a70)')line
          read(line(7:70),*) xd,yd,zd
          write(6,*) 'symmetry-non-equivalent atom'
          write(6,'(a)') line 
          write(6,'(a)') 'symmetry-equivalent atoms (idx,charge,x,y,z)'
  100   continue
        if (currentatom+nofeqat+1.gt.iatom) cycle  
c       write(6,*) 'CHECKING ',d2hinfo(currentatom+nofeqat+1)
        read(d2hinfo(currentatom+nofeqat+1),*) x,y,z
        lx=abs(abs(xd)-abs(x)).lt. 1.d-7
        ly=abs(abs(yd)-abs(y)).lt. 1.d-7
        lz=abs(abs(zd)-abs(z)).lt. 1.d-7
        if (lx .and. ly .and. lz) then
           write(6,'(i4,3x,f3.1,3x,a)') currentatom+nofeqat+1,
     .             ordzahl(currentatom+nofeqat+1),
     .             d2hinfo(currentatom+nofeqat+1)
           nofeqat=nofeqat+1
           goto 100
        endif 
        enddo 

c
c  iprim(4,mac_contr_blocks), icont(4,max_contr_blocks)
c  expcon(4,max_contr_blocks,20,20)
c
         do i=1,noflval
          if (noflval.gt.4)
     .       call bummer('up to f-type functions supported, only',0,2)
            do j=1,lvalues(i)
               if (lvalues(i).gt.max_contr_blocks)
     &          call bummer('lvalues(i).gt.20',lvalues(i),2)
               read(21,'(1x,i4,i4)') iprim(i,j),icont(i,j)
               if (iprim(i,j).gt.20)
     .               call bummer('iprim(i,j).gt.20',iprim(i,j),2)
               if (icont(i,j).gt.20)
     .              call bummer('icont(i,j).gt.20',icont(i,j),2)
                  do k=1, iprim(i,j)
                     read(21,*) (expcon(i,j,k,l),l=1,icont(i,j)+1)
                  enddo

                 do l=1,icont(i,j)
                  if (abs(gtoscale(igto)).lt.1.d-6)
     .              call bummer('invalid gtoscale(igto)',igto,2)
                  do k=1,iprim(i,j)
                  expcon(i,j,k,l+1)=expcon(i,j,k,l+1)*gtoscale(igto)
                  enddo
                  igto=igto+1
                 enddo
            enddo
         enddo
         do m=1, nofeqat
           currentatom=currentatom+1
cfp: compatibility with Molekel
           if (currentatom.gt.1) write(20,*) ' '
           write(20,1000) currentatom, 0
 1000  format(i3,i3)
           do i=1,noflval
              do j=1,lvalues(i)
                 do ll=1, icont(i,j)
                 kk=0
                 do k=1,iprim(i,j)
                  if (abs(expcon(i,j,k,ll+1)).gt.1.d-8) kk=kk+1
                 enddo
                 write(20,1001) aotype(i), kk , 1.00
1001  format(2x,a,i6,1x,f4.2)
                 do k=1, iprim(i,j)
                  if (abs(expcon(i,j,k,ll+1)).gt.1.d-8) then
                  write(20,1002) expcon(i,j,k,1),expcon(i,j,k,ll+1)
                  endif
1002  format(2(1x,d13.6))
                 enddo
                enddo
              enddo
           enddo
         enddo
      enddo
       if (currentatom.ne.iatom) then
       call bummer('readdaltaoin: invalid analysis:',currentatom,2)
       endif
      close(21)
      return
      end


       subroutine dtransform( scr,moc,nbftot,icode,idegen)
        implicit none 
        integer maxcaoperatom
       integer maxatoms
       integer maxbfn
       parameter( maxcaoperatom =200)
       parameter (maxatoms = 255 )
       parameter (maxbfn = 1023)

       real*8 scr(*),moc(*)
       integer nbftot,icode,idegen
       integer offset,j,k

c  d2- scr(1)
c  d1- scr(1+idegen)
c  d0- scr(1+2*idegen)
c  d1  scr(1+3*idegen)
c  d2  scr(1+4*idegen)


          offset=1

          goto (10,20,30,40,50,60,70) icode
          call bummer('invalid icode=',icode,2)

c
c        no mapping at all
c
 10      do j=1,nbftot
           do k=0,4
           scr(offset+k*idegen)=moc(offset+k*idegen)
           enddo
          offset=offset+nbftot
         enddo
         return
c
c        special mapping for molden interface
c
 70      do j=1,nbftot
           scr(offset+4*idegen)=moc(offset+0*idegen)
           scr(offset+2*idegen)=moc(offset+1*idegen)
           scr(offset+0*idegen)=moc(offset+2*idegen)
           scr(offset+1*idegen)=moc(offset+3*idegen)
           scr(offset+3*idegen)=moc(offset+4*idegen)
           offset=offset+nbftot
         enddo
         return
c
c        ixyz = 2 3 1
c

 20      do j=1,nbftot
           scr(offset+3*idegen)=moc(offset+0*idegen)
           scr(offset+0*idegen)=moc(offset+1*idegen)
           scr(offset+1*idegen)=moc(offset+3*idegen)
           scr(offset+2*idegen)=-0.5d0*moc(offset+2*idegen)
     .                          -0.5d0*moc(offset+4*idegen)
           scr(offset+4*idegen)= 1.5d0*moc(offset+2*idegen)
     .                          -0.5d0*moc(offset+4*idegen)
           offset=offset+nbftot
          enddo
          return
c
c        ixyz =
c

 30      do j=1,nbftot
           scr(offset+3*idegen)=moc(offset+0*idegen)
           scr(offset+1*idegen)=moc(offset+1*idegen)
           scr(offset+0*idegen)=moc(offset+3*idegen)
           scr(offset+2*idegen)=-0.5d0*moc(offset+2*idegen)
     .                          -0.5d0*moc(offset+4*idegen)
           scr(offset+4*idegen)=-1.5d0*moc(offset+2*idegen)
     .                          +0.5d0*moc(offset+4*idegen)
           offset=offset+nbftot
          enddo
          return
c
c        ixyz =
c

 40      do j=1,nbftot
           scr(offset+0*idegen)=moc(offset+0*idegen)
           scr(offset+3*idegen)=moc(offset+1*idegen)
           scr(offset+1*idegen)=moc(offset+3*idegen)
           scr(offset+2*idegen)= 1.0d0*moc(offset+2*idegen)
           scr(offset+4*idegen)=-1.0d0*moc(offset+4*idegen)
           offset=offset+nbftot
          enddo
          return
c
c        ixyz =
c

 50      do j=1,nbftot
           scr(offset+1*idegen)=moc(offset+0*idegen)
           scr(offset+3*idegen)=moc(offset+1*idegen)
           scr(offset+0*idegen)=moc(offset+3*idegen)
           scr(offset+2*idegen)=-0.5d0*moc(offset+2*idegen)
     .                          -0.5d0*moc(offset+4*idegen)
           scr(offset+4*idegen)= 0.5d0*moc(offset+2*idegen)
     .                          -1.5d0*moc(offset+4*idegen)
           offset=offset+nbftot
          enddo
          return
c
c        ixyz =
c

 60      do j=1,nbftot
           scr(offset+1*idegen)=moc(offset+0*idegen)
           scr(offset+0*idegen)=moc(offset+1*idegen)
           scr(offset+3*idegen)=moc(offset+3*idegen)
           scr(offset+2*idegen)=-0.5d0*moc(offset+2*idegen)
     .                          +0.5d0*moc(offset+4*idegen)
           scr(offset+4*idegen)=+1.5d0*moc(offset+2*idegen)
     .                          +0.5d0*moc(offset+4*idegen)
           offset=offset+nbftot
          enddo
          return

          return
          end

          subroutine printheader

          write(*,100)
100       format(20('----')/)
          write(*,110)
110       format('               program  transmo               '/
     .           ' This program transforms a set of MO coefficients '/
     .           ' from one point group to another and allows for   '/
     .           ' arbitrary interchange of axis.                   '/
     .           ' Further it serves as an interface to MOLDEN and  '/
     .           ' thereby allows graphical wavefunction analysis   '/
     .           ' at the MO level.'/
     .  ' $Id: transmo.f,v 1.4 2015/06/17 10:48:53 cvs Exp $',
     .            //)

          call who2c('TRANSMO',6)
          return
          end

       subroutine find_collating(ismall,icap)
      integer ierror,ismall,icap,i
      character*26 str1,str2


      str1="ABCDEFGHIJKLMNOPQRSTUVWXYZ"   
      str2="abcdefghijklmnopqrstuvwxyz"   

      icap=iachar('A');
      ismall=iachar('a');
      ierror=0
      do i=1,26
       if (iachar(str1(i:i)).ne.icap+i-1)  ierror=ierror+1
       if (iachar(str2(i:i)).ne.ismall+i-1)  ierror=ierror+1
      enddo
      if (ierror.gt.0) 
     .  call bummer('find_collating failed, ierror=',ierror,2)
      return
      end subroutine

   

