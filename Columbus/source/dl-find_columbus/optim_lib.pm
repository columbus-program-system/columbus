  package  optim_lib;
  require Exporter;
  @ISA = qw(Exporter);
  @EXPORT =
      qw(units keyinfile exit_error mcscfconv ciconv callprog columbus_mode lookatcolumbus cptemp err_message addkeyword deletekeyword searchkeyword getkeyword number_of_atoms rm_files save_files my_grep print_STDOUT printf_STDOUT saveoldfile);
  @EXPORT_OK =
    qw($found $nconv $niter $value);
  use Cwd;

#------------------------------------------------------------------------------
  sub units {

#  Constants, parameters and convertion factors
    local ( $key, $value );
    ($key) = @_;

    if ( $key eq "BK" )
    {
      $value = 0.3166813639E-5;
    }    # Boltzmann constant Hartree/kelvin
    elsif ( $key eq "bk_ev" )
    {
      $value = 8.617343E-5;
    }    # Boltzmann constant eV/kelvin
    elsif ( $key eq "proton" )
    {
      $value = 1822.888515;
    }    # (a.m.u.)/(electron mass)
    elsif ( $key eq "timeunit" ) { $value = 24.188843265E-3; }    # femtoseconds
    elsif ( $key eq "au2ev" )    { $value = 27.21138386; }        # au to eV
    elsif ( $key eq "pi" )       { $value = 3.141592653589793; }  # pi
    elsif ( $key eq "au2ang" ) { $value = 0.52917720859; }    # au to angstrom
    elsif ( $key eq "deg2rad" )
    {
      $value = 1.745329252E-2;
    }    # degree to radian (pi/180)
    elsif ( $key eq "au2cm" ) { $value = 219474.625; }       # hartree to cm-1
    elsif ( $key eq "cm2au" ) { $value = 4.55633539E-6; }    # cm-1 to hartree
    elsif ( $key eq "h_planck" ) { $value = 1.5198298508E-16; }    # h hartree.s
    elsif ( $key eq "h_evs" )    { $value = 4.13566733E-15; }      # h eV.s
    elsif ( $key eq "light" ) { $value = 299792458; }    # speed of light m/s
    elsif ( $key eq "e_charge" )
    {
      $value = -1.602176487E-19;
    }                                                    # electron charge C
    elsif ( $key eq "e_mass" ) { $value = 9.10938215E-31; }   # electron mass kg
    elsif ( $key eq "eps0" )
    {
      $value = 8.854187817e-12;
    }    # vacuum permitivity C^2*s^2*kg^-1*m^-3
    else { die " Constant $key was not found (optim_lib)"; }

    return $value;
  } ## end sub units
#------------------------------------------------------------------------------
  sub keyinfile {

#==========================================================
#
# returns the value of the namelist variable $key in file $filename
# namelist variables are separated by , or newline
# the routine stops parsing, when the key is found for the first
# time and all values corresponding to this key are returned.
#==========================================================
    local ( $i, $j, $k, @keysperline, $found, @keyval, $filename, $key );
    ( $filename, $key ) = @_;
    local ($finished);

#
    open( ANYFILE, "$filename" );
    $/      = "\n";
    @keyval = <ANYFILE>;
    close ANYFILE;
    $found = "";
    foreach $i (@keyval)
    {
      chop $i;
      $i =~ s/ *$//g;
      @keysperline = split ',', $i;
      $k           = 0;
      $finished    = 0;
      for ( $j = 0; $j <= $#keysperline; $j++ )
      {
        $_ = $keysperline[$j];

       #if (/$key/i) {$found=$keysperline[$j]; $k=1;next;}
       #if ( (/^$key$/i) or (/^$key\=/i) ) {$found=$keysperline[$j]; $k=1;next;}
        if (/$key/i) { $found = $keysperline[$j]; $k = 1; next; }
        if ( ( !/[\$&A-Z\/]/i ) && $k )
        {
          $found = join ':', $found, $keysperline[$j];
        }
        if ( (/[\$&A-Z\/]/i) && $k ) { $finished = 1; }
      }    # end of: for ( $j= 0 ; $j <= $#keysperline; $j++)
      if ($finished) { last; }
    } ## end foreach $i (@keyval)
    $found =~ s/^.*= *//g;
    chomp($found);
    $found =~ s/\'//g;
    $found =~ s/\"//g;    # clean quotation marks
    return $found;
  } ## end sub keyinfile
#------------------------------------------------------------------------------
  sub exit_error {

#
# Print an error message
# Usage: exit_error(message, parent program)
# Example: exit_error("geom file does not exist",$mdle)
# MB, 2006.
#
    local ( $mdle, $message );
    $message = $_[0];
    $mdle    = $_[1];
    print_STDOUT(
"\n*******************************************************************************\n"
    );
    print_STDOUT("     $mdle <ERROR> $message \n");
    print_STDOUT(
"*******************************************************************************\n"
    );
    die "\n ERROR: Optimizer is dying now ... \n";
  } ## end sub exit_error
#------------------------------------------------------------------------------
  sub mcscfconv {
    ($filein) = @_;
    my ($dum);

#
    $nconv = 0;
    open( FILE, "$filein" ) or die " cannot open file: $filein\n";
    $/ = "\n";
    while (<FILE>)
    {
      if (/final mcscf/)
      {
        $_ = <FILE>;
        chop;
        s/^ *//;
        ( $dum, $niter ) = split( /\s+/, $_, 3 );
        $nconv = /not converged/;
      }
    }
    close FILE;

#
    return $nconv, $niter;

#
  }    # end of: sub mcscfconv
#------------------------------------------------------------------------------
  sub ciconv {
    ($filename) = @_;
    my ( $dum, $niter, $nconv );

#
    $nconv = 0;
    open( FILE, "$filename" ) or die " cannot open file: $filename\n";
    $/ = "\n";
    while (<FILE>)
    {
      if (/iterations/)
      {
        chop;
        s/^ *//;
        ( @dum[ 0 .. 4 ], $niter ) = split( /\s+/, $_, 7 );
        $nconv = /not reached/;
      }
    }
    close FILE;

#
    return $nconv, $niter;

#
  }    # end of: sub ciconv
#======================================================================
# call a program, check for successful execution
# Usage: callprog(mld,prog,mdle)
# mld = directory of the program
# prog = execution command with options
# mdle = parent program
# Example: run-col.pl calling Columbus
# call($COLUMBUS,"runc -m 50000000 > runls","run-col.pl")
#======================================================================
  sub callprog {
    local ( $path, $mdle, $x, $prog, $scrv, $status, $t, $istep );

    $path    = $_[0];
    $prog    = $_[1];
    $mdle    = $_[2];
    $BASEDIR = $_[3];
    if ( !$BASEDIR )
    {
      $BASEDIR = &getcwd();
      chomp($BASEDIR);
    }
    $x = $prog;
    $x =~ s/[\<\>].*$//;

    print_STDOUT(
      "---------------------------------------------------------------\n");
    $started = "Starting $x\n";
    print_STDOUT("$started");
    if ( $path eq "" )
    {
      $scrv = system("$prog 2>> $BASEDIR/DEBUG/run.error");
    }
    else
    {
      $scrv = system("$path/$prog 2>> $BASEDIR/DEBUG/run.error");
    }
    $status = "";
    if ( $scrv != 0 )
    {
      $status = "with ERROR";
    }
    if ( $status ne "with ERROR" )
    {
      $status = "successfuly";
    }

    $finished = "Finished $x $status\n";
    print_STDOUT("$finished");
    print_STDOUT(
      "---------------------------------------------------------------\n");
    $! = 0;
    if ( $status eq "with ERROR" )
    {
      $! = 256;    # for the moment error number 256 is used for anything
      if ( $mdle eq "optimize.pl:" )
      {
        cptemp();
        print_STDOUT("See also error messages in DEBUG/run.error \n");
      }
    }
    return $!;
  } ## end sub callprog
#------------------------------------------------------------------------------
  sub columbus_mode {

# Look at control.run and return
#   mode = ss if ciudg
#   mode = ms if ciudgav
#   mode = mc if no ci program is found
    my ( $value, $found1, $found2 );
    $found1 = searchkeyword( "control.run", "ciudgav" );
    if ( $found1 == 0 )
    {
      $found2 = searchkeyword( "control.run", "ciudg" );
      if ( $found2 == 0 )
      {
        $value = "mc";
      }
      elsif ( $found2 > 0 )
      {
        $value = "ss";
      }
    }
    elsif ( $found1 > 0 )
    {
      $value = "ms";
    }
    return $value;
  } ## end sub columbus_mode
#------------------------------------------------------------------------------
  sub lookatcolumbus {
    $file = $_[0];
    open( RE, "$file" )
      or warn "Cannot open $file!";    # I'm not sure: die or warn?
    while (<RE>)
    {
      if (/bummer/)
      {
        if ( !/warning/i )
        {
          print_STDOUT("ERROR: bummer found in $file. \nThe message is: $_\n");
          $status = "with ERROR";
        }
      }
    }
    close(RE);
  }
#------------------------------------------------------------------------------
  sub cptemp {

# Try to save TEMP before dying, but only once.
    if ( !-e "savetemp" )
    {
      $t = "";
      if ( -e "DEBUG/COL.$t" )
      {
        $direct = "DEBUG/COL.$t";
        rm_files( "WORK", "t", 5 );    # Delete text files above 5 MB
        rm_files( "WORK", "b", 0 );    # Delete all binary files
      }
      else
      {
        $direct = "DEBUG";
#        rm_files( "TEMP", "a", 5 );    # Delete all binary files
      }
#      print_STDOUT("Trying to save TEMP directory to $direct \n");
#      system("cp -rf ../TEMP $direct/.");
#      system("touch savetemp");
    }
  } ## end sub cptemp
#------------------------------------------------------------------------------
sub addkeyword {
# adds a keyline (  key = value) to a file on the first position possible
# use as addkeyword(old_filename,new_filename,key,value)
# if old_filename=new_filename -> the file is overwritten
# if value is omitted -> only the keyword is written
# if the key is already present
# -> either nothing is done (if it is a single key)
# -> changekeyword($file_old,$file_new,$keyword,$value) is used to change value
# -> a single key will replace a key = value pair
# -> a key = value pair will replace a single key


  local($file_old,$file_new,$keyword,$value,$found);
  my $mdle="addkeyword" ;
  if ($#_ == 2) {
    # got three arguments indicating, that a line with "key" has to be inserted
    #this cannot be a namelist file
    #keyword is added in first line for empty files and
    #in second line of files with contents

    ($file_old,$file_new,$keyword)=@_;

    saveoldfile($file_old);

    $found=searchkeyword("tmp-clp",$keyword);

    if ($found == 0){
    # key not existent in old file - adding it in new file
      open TMP, "<tmp-clp" or open_err("tmp-clp","read","die",$mdle);
      open NEW, ">$file_new" or open_err($file_new);

      if (-z "tmp-clp"){
        print NEW "$keyword";
      }
      else{
        $_=<TMP>;
        print NEW $_;
        print NEW "$keyword\n";
        while (<TMP>) {
          print NEW $_;
        }
      }
      close TMP or close_err("tmp-clp");
      close NEW or close_err($file_new);
    }
    else{
    # key exists in old file - copy new file
      open TMP, "<tmp-clp" or open_err("tmp-clp");
      open NEW, ">$file_new" or open_err($file_new);
      while (<TMP>) {
        #if-else neccesary to substitute key = value by key only
        if (/\b$keyword\b/){
          print NEW "$keyword\n";
        }
        else{
          print NEW $_;
        }
      }
      close TMP or close_err("tmp-clp");
      close NEW or close_err($file_new);
    }
  }

  if ($#_ == 3) {
    # got four arguments -> a line with "key = value" has to be inserted or
    # the value changed
    #this can be a namelist file or not
    #keyword = value is added in first line for empty files and
    #in second line to files with contents

    ($file_old,$file_new,$keyword,$value)=@_;

    saveoldfile($file_old);

    $found = searchkeyword("tmp-clp",$keyword);

    if ($found == 0){
    # key not existent in old file - adding it in new file
      open TMP, "<tmp-clp" or open_err("tmp-clp");
      open NEW, ">$file_new" or open_err($file_new);
      if (-z "tmp-clp"){
        print NEW "  $keyword = $value\n";
      }
      else{
        $_=<TMP>;
        print NEW $_;
        print NEW "  $keyword = $value\n";
        while (<TMP>) {
          print NEW $_;
        }
      }
      close TMP or close_err("tmp-clp");
      close NEW or close_err($file_new);
    }
    else{
    # key exists in old file - use changekeyword()
      changekeyword($file_old,$file_new,$keyword,$value);
    }
  }
  system("rm -f tmp-clp");
}
#
#------------------------------------------------------------------------------
#
  sub deletekeyword {
    local ( $file_old, $file_new, $keyword, $found );
    ( $file_old, $file_new, $keyword ) = @_;

#
    $/ = "\n";
    open OLD, "<$file_old";
    open TMP, ">tmp-clp";
    while (<OLD>) { print TMP $_; }
    close OLD;
    close TMP;

#
    open OLD, "<tmp-clp";
    open NEW, ">$file_new";
    $found = 0;
    while (<OLD>)
    {
      if (/\b$keyword\b/i) { $found = 1; }
      else                 { print NEW $_; }
    }
    close NEW;
    close OLD;
    if ( !$found == 1 ) {
      die "keyword $keyword not found in file: $file_old\n";
    }
    return 0;
  } ## end sub deletekeyword
#---------------------------------------------------------------------------
  sub searchkeyword {
    local ( $file, $keyword, $found );
    ( $file, $keyword ) = @_;
    $found = 0;

#
    open( FILE, "<$file" ) or warn "Cannot read $file !";
    while (<FILE>)
    {
      chomp $_;
      if (/\b$keyword\b/i) { $found = $found + 1; }
    }
    close FILE;
    $keyword = $found;
  }
#---------------------------------------------------------------------------
# ====================================================================
# GETKEYWORD: search for a keyword in a file. If it is found, return its
# value, otherwise attribute a default value.
# Usage:
# $keyword=getkeyword("file_name","keyword_pattern","default_value")
# Return: value
# Example:
# $mem=getkeyword($cpar,"mem","100");
#
  sub getkeyword {
    local ( $file, $keyword, $default, $found, $value );
    ( $file, $keyword, $default ) = @_;
    $found = -1;
    if ( -s $file )
    {
      $found = 0;
      $found = searchkeyword( "$file", "$keyword" );
      if ( $found == 0 )
      {
        $value = $default;
      }
      else
      {
        $value = keyinfile( "$file", "$keyword" );
      }
    }
    else
    {
      $value = $default;
    }
    return $value;
  } ## end sub getkeyword
#---------------------------------------------------------------------------
  sub number_of_atoms {

# Counts the number of atoms in geom file. If geom does not exist,
# it returns 0.
# Usage: $Nat=number_of_atoms();

    local ( $file, $file2, $value );
    $file = "geom";
    $value = 0;
    if ( !-s $file )
    {
      return $value;
    } ## end if ( !-s $file )
    else
    {
      open( FL, $file ) or return $value;
      while (<FL>)
      {
        $value++;
      }
      close(FL);
      return $value;
    }
  } ## end sub number_of_atoms
#---------------------------------------------------------------------------
  sub rm_files {

# Delete files
# usage: rm_files(path,type,size)
# path - directory path
# type - B for binary (case insensitive)
#        T for text
#        A for all
# size - remove only files above this size in MB
# Example:
# rm_files("TEMP/WORK","B",1)
# will remove all binary files from TEMP/WORK whose size is
# larger or equal to 1 MB.
# Mario Barbatti, Mar 2007
#
    local (@files);
    local ( $element, $dir_path, $fname, $max_size, $MB2byte, $type, $value );
    ( $dir_path, $type, $value ) = @_;
    $type = lc $type;

    if ( !-e $dir_path )
    {
      return;
    }
    $MB2byte  = 1048576;
    $max_size = $value * $MB2byte;

    opendir( DIR, $dir_path ) or die "Can't open current dir: $dir_path\n";
    @files = grep ( !/^\.\.?$/, readdir(DIR) );
    closedir(DIR);

    for $element (@files)
    {
      $fname = "$dir_path/$element";
      if ( -B $fname )
      {
        if ( -s $fname >= $max_size )
        {
          if ( ( $type eq "b" ) or ( $type eq "a" ) )
          {
            system("rm -rf $fname");    # Delete binary files
          }
        }
      }
      elsif ( -T $fname )
      {
        if ( -s $fname >= $max_size )
        {
          if ( ( $type eq "t" ) or ( $type eq "a" ) )
          {
            system("rm -rf $fname");    # Delete text files
          }
        }
      }
    } ## end for $element (@files)
  } ## end sub rm_files
#------------------------------------------------------------------------------
  sub my_grep {

# This routine works like the system grep with options -A and -B.
# Usage:
# my_grep($file,$file_dest,$pattern,$lines,$direction,$position)
# where $file - file to be searched
#       $file_dest - destination file
#                    "STDOUT" = print to standard output
#       $pattern - the pattern to be searched
#       $lines - number of lines to be printed before or after the matching (0 = default)
#       $direction A - print $lines after matching (default)
#                  B - print $lines before matching
#       $position new - overwrite previous content (default)
#                 append - append to previous content
# Mario Barbatti, Jun 2007
#
    local (
      $file,  $file_dest, $pattern, $lines,   $direction, $position,
      $found, $i,         $j,       @history, $mdle
    );
    $mdle      = "my_grep subroutine: ";
    $lines     = 0;
    $direction = "A";
    $position  = "new";
    $found     = "";
    $file      = "none_in_mygrep";
    $file_dest = "none_in_mygrep";
    ( $file, $file_dest, $pattern, $lines, $direction, $position ) = @_;
    $direction = lc $direction;
    $position  = lc $position;
    open( FL, $file ) or die "Cannot open $file!";

    while (<FL>)
    {
      push( @history, $_ );
      $j++;
      if (/$pattern/)
      {
        $i     = 0;
        $found = $found . $_;
        if ( $lines != 0 )
        {
          if ( $direction eq "a" )
          {
            while (<FL>)
            {
              $i++;
              if ( $i <= $lines )
              {
                $found = $found . $_;
              }
              elsif ( $i == $lines + 1 )
              {
                $found = $found . "\n";
                last;
              }
            }
          }
          elsif ( $direction eq "b" )
          {    # Not tested!
            for ( $i = 1; $i <= $lines; $i++ )
            {
              $found = $history[ $j - $i - 1 ] . $found;
              if ( $j - $i - 1 <= 0 )
              {
                last;
              }
            }
          }
        } ## end if ( $lines != 0 )
      } ## end if (/$pattern/)
    } ## end while (<FL>)
    close(FL);
    if ( $file_dest eq "STDOUT" )
    {
      print_STDOUT($found);
    }
    else
    {
      if ( $position eq "new" )
      {
        open( FL, ">$file_dest" )
          or die "$mdle Cannot open $file_dest to write!";
      }
      elsif ( $position eq "append" )
      {
        open( FL, ">>$file_dest" )
          or die "$mdle Cannot open $file_dest to append!";
      }
      else
      {
        die "$mdle exiting without writing \n";
      }
      print FL $found;
      close(FL);
    }
  } ## end sub my_grep
#------------------------------------------------------------------------------
  sub print_STDOUT {

# Print unformated outputs to moldyn.log and RESULTS/opter.log.
# Usage:
# print_STDOUT($output);            print in any time step
#
    my ( $flag, $eps, $text, $log_file, $n );
    $eps = 1E-9;
    $text = shift(@_);

    print STDOUT "$text";
    if ( -e "RESULTS" )
    {
      $log_file = "RESULTS/opter.log";
      open( SECOUT, ">>$log_file" );
      print SECOUT "$text";
      close(SECOUT);
    }
  } ## end sub print_STDOUT
#------------------------------------------------------------------------------
  sub printf_STDOUT {

# Print formated outputs to moldyn.log and RESULTS/opter.log.
# Usage:
# printf_STDOUT($format,@output);
    my ( $format, $text, $log_file );
    ( $format, @text ) = @_;

    printf STDOUT $format, @text;
    if ( -e "RESULTS" )
    {
      $log_file = "RESULTS/opter.log";
      open( SECOUT, ">>$log_file" );
      printf SECOUT $format, @text;
      close(SECOUT);
    }
  } ## end sub printf_STDOUT
#------------------------------------------------------------------------------
# routines used internally
#------------------------------------------------------------------------------
  sub saveoldfile {

# used by changekeyword, deletekeyword, etc
# copies the contents of the old file to a temp file 'tmp-clp'
# if the old file is not present an empty tmp-clp is created

    local ($file);
    $file = $_[0];
    $/    = "\n";
    if ( open OLD, "<$file" )
    {

      #print_STDOUT("file $file present\n");
      open TMP, ">tmp-clp"
        or die "cannot create temporary file\n sys answer was: $!";
      while (<OLD>)
      {

        #print_STDOUT($_);
        print TMP $_;
      }
      close OLD or die "could not close $file\nsys answer was: $!";
      close TMP or die "could not close temporary file\n sys answer was: $!";
    }
    else
    {

      #print_STDOUT("no file $file - creating a new keyfile\n)";
      system("rm -f tmp-clp");
      system("touch tmp-clp");
    }
  } ## end sub saveoldfile
