#!/usr/bin/perl -w
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


# Driver for the optimization
# Reads in the input file (def: optimize.inp) and calls
# the DL-find optimizer.
# Input filename can be given with -i flag.
# Geometry to be optimized has to be in Columbus-geom
# format.
# Program for gradient calculation is Columbus ver 7 or above.
# For Columbus there has to be a single point gradient setup
# for the desired state in the current directory.

use strict;
use warnings;
use Cwd;
use lib ("/scratch/matruc/dl-find_columbus_ver2.0.1alpha");
use optim_lib;

my ( $file, $inpfile, $retval, $mld, $mdle, $BASED, @line, $keyw, $val );
my ( $restart, @flags, $flag, $arg, $i, $col, $lvprt, $found );
my $installdir = "/scratch/matruc/dl-find_columbus_ver2.0.1alpha";
my $debug      = 0;
$lvprt = 2;

# defaults
$inpfile = 'optimize.inp';
$file    = 'geom';
$restart = 0;

$BASED = &getcwd();

# read command line
while ( $#ARGV > 0 )
{
  $arg = shift(@ARGV);
  if ( $arg =~ /^-/ )
  {
    # look what flags are there
    $arg =~ s/^-//;
    @flags = split( //, $arg );
    for ( $i = 0; $i < @flags; $i++ )
    {
      if ($debug) { print_STDOUT ("DEBUG: found flag $flags[$i]\n"); }
      if ( $flags[$i] eq 'i' )
      {
        if ( ( $i < ( $#flags - 1 ) ) or ( $#ARGV < 0 ) )
        {
          die "Flag -i must be followed by input filename\n";
        }
        $inpfile = shift(@ARGV);
        if ( !-e $inpfile )
        {
          die "No such file $inpfile\n";
        }
        if ($debug) { print_STDOUT("DEBUG: input file set to $inpfile\n"); }
      }
      else
      {
        die "Unknown flag $flags[$i]\n";
      }
    } ## end for ( $i = 0; $i < @flags...
  } ## end if ( $arg =~ /^-/ )
} ## end while ( $#ARGV > 0 )

# read the input file
if ( not -e "$BASED/$inpfile" ) { die "$inpfile not found!\n"; }
open( INP, $inpfile ) or die "cannot read $inpfile\n$!\n";

while (<INP>)
{
  chomp;
  s/^\s+//;
  s/\s+=/=/;
  s/=\s+/=/;
  if( (not /^$/) and (not /^#/) )
  {
    ( $keyw, $val ) = split /=/;
    $keyw = lc($keyw);
    if ( lc($keyw) eq 'restart' )
    {
      $restart = 1;
    }
    elsif ( lc($keyw) eq 'print level' )
    {
      $lvprt = $val;
      if($lvprt > 4) {$debug++;}
    }
  }
}
close(INP);

$mdle = "optimize";
if ( not( $restart > 0 ) )
{
  if ( -f "$BASED/RESULTS" ) { unlink("$BASED/RESULTS"); }
  if ( !-d "$BASED/RESULTS" )
  {
    mkdir("$BASED/RESULTS");
  }
  if ( -f "$BASED/DEBUG" ) { unlink("$BASED/DEBUG"); }
  if ( !-d "$BASED/DEBUG" )
  {
    mkdir("$BASED/DEBUG");
  }

  chdir("$BASED/RESULTS");
  unlink(<*>);
  chdir("$BASED");
  system("cp $installdir/compute_col.pl $BASED/compute_gradient.pl");
}
else
{
  die
"restart option not implemented yet\nbest cp RESULTS/geom.new ./geom \nand start with this structure\n";

  # do nothing at them moment - this will be implemented laterm
}

# save the starting geometry
system("cp -f geom geom.start");

$col = $ENV{"COLUMBUS"};
if ($debug) { print_STDOUT ("DEBUG: col is $col\n"); }

system("cp -f $BASED/$inpfile $BASED/opt_internal.inp");

$retval = &callprog( $installdir, "geom2xyz", $mdle );
if ( $retval != 0 )
{
  die "$mdle is dying now\n";
}

$retval = callprog( $installdir, "optimizer.x", $mdle );
if ( $retval != 0 )
{
  die "$mdle is dying now\n";
}

$retval = callprog( $installdir, "xyz2geom < geom.xyz", $mdle );
if ( $retval != 0 )
{
  die "$mdle is dying now\n";
}

system("mv findpath.xyz finden.dat $BASED/RESULTS");
system("cp geom $BASED/RESULTS/$file.new");
system("rm $BASED/compute_gradient.pl");
system("rm $BASED/opt_internal.inp");
chdir($BASED);
