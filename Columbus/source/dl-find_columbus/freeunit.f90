!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
INTEGER FUNCTION freeunit()
  LOGICAL :: used
  include 'common_params.i'
  include 'errors.i'
  freeunit = 0
  used = .true.
  do while (used)
    freeunit = freeunit + 1
    if (freeunit.ne.iinp .and. freeunit.ne.ioutp) then
      if (freeunit .gt. 99) then
        call fatal(1,'','freeunit()')
      end if
      INQUIRE (unit=freeunit,opened=used)
    end if
  end do
END FUNCTION