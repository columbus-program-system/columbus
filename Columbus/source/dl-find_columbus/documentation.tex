\documentclass{article}

 \usepackage{textcomp}
 \usepackage{graphicx}
 \usepackage{pifont}
 \usepackage{amsmath}
% \usepackage{floatflt} %floating figures
 \usepackage{varioref}
\usepackage{citesort}
\usepackage{comment}

\usepackage{mathptmx}


\begin{document}

\begin{center}
\LARGE
DL-FIND

\Large
A Geometry Optimiser for Atomistic Simulation Codes

\bigskip
\LARGE
Project Plan and User Manual

\bigskip
\normalsize    
$ $Revision: 1.1.4.2 $ $
 
Johannes K\"astner, \\
STFC Daresbury Laboratory,\\
\today
\end{center}

Note: the interface to the calling program is documented in
section~\ref{sec:interface} on page~\pageref{sec:interface}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General aim and target}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DL-FIND is a modern and flexible structure optimiser to be included in
electronic structure codes. It provides a stable way to find reaction energy
differences as well as activation barriers. This can be performed starting
from input structures in the region of the reactant and the product.  The
whole process should require as little action from the user as possible.  The
code should parallelise at least the energy and gradient evaluations and
possibly expensive calculations within the optimiser. All units write restart
information at regular intervals to enable a restart of the optimiser.

This version of DL-FIND incorporates an MPI split-communicator taskfarming 
parallelisation.  
Such taskfarming is only employed in the parallel optimisers at present, but 
could easily be used more widely.

A light-weighted description of some capabilities of DL-FIND can be found in
the 2007 issue of Frontiers:\\
http://www.cse.scitech.ac.uk/ccg/j.kaestner/pdf/DLFIND\_frontiers.pdf.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Functionalities}

\emph{Emphasised:} remains to be implemented

\subsubsection{Coordinate systems:}
\begin{itemize}
\item Cartesians (including frozen atoms and components)
\item Mass-weighted cartesians (including frozen atoms and components)
\item Internals (including all constraints): 
  \begin{itemize}
  \item DLC (delocalised internal coordinates)
  \item DLC-TC (total connection)
  \item HDLC
  \item HDLC-TC
  \end{itemize}
\item \emph{Fractional coordinates and unit cell optimisation?}
\item \emph{Parallel optimisation in internal coordinates?}
\end{itemize}

\emph{A microiterative optimisation in a QM/MM context should be available as
  well as a microiterative TS search. Those can eventually be combined.}

\subsubsection{Combinations of coordinates (images):}
\begin{itemize}
\item NEB (nudged elastic band)
\item \emph{(Growing) String method}
\item Dimer method \cite{hen99,hey05,kae08}
\item \emph{Replica path method following \cite{woo03}}
\end{itemize}
All of the combinations should work with all versions of coordinate systems.

\subsubsection{Optimisers:}
\begin{itemize}
\item steepest descent
\item conjugate gradient
\item Damped dynamics
\item L-BFGS
\item P-RFO Hessian update mechanisms: Powell \cite{pow71} and Bofill
  \cite{bof94}. Hessian either by input or by finite-difference. In the latter
  case either in cartesians (then the update also in cartesians, and one can
  output frequencies), or in internals.

  A criterion can be specified with absolute eigenvalues of the hessian below
  that criterion are frozen (cosidered to be ``soft''). At maximum 6
  eigenmodes are frozen.
\item Random (stochastic) search \cite{brooks57,luusj73}
\item Genetic algorithm \cite{holland75,goldberg89,haupth98}
\item \emph{Reaction path following / IRC ?}
\end{itemize}

\subsubsection{Line search algorithms:}
\begin{itemize}
\item Simple scaling of the proposed step (covering the maximum step length)
\item Trust radius based on energy decrease
\item Trust radius based on the projection of the gradient on the step
\item \emph{Trust radius based on the overlap of the lowest eigenmode (for P-RFO)}
\item \emph{full line search ? which algorithm?}
\end{itemize}
The design allows new methods to be easily implemented.

\subsubsection{A global task manager:}

\emph{Should define which methods are used depending on the input. In case a
  method fails, this should be recognised and another method should be tried.}

\subsubsection{Parallelisation:}

\emph{The energy evaluations and all relevant parts of the optimiser should be
  parallelised.}

The parallel optimisers (random search and genetic algorithm) can be run
in taskfarming mode using MPI (message passing interface), where each taskfarm 
calculates the energy and gradient 
for a non-overlapping subset of the current population.  The load-balancing 
is static, and the number of taskfarms must be a factor of the total number of 
processors for a given job.  If the number of taskfarms requested is less than the
total number of processors, then the single-point energy and gradient calculations 
for each individual in a taskfarm can also be parallelised.  This is handled by
the program that provides the energy and gradient routines.  Thus, two-way 
and two modes of one-way parallelisation are possible, in theory.  

Wrappers for the required MPI routines are located in the file dlf\_mpi.f90, and the 
corresponding ``dummy'' subroutines are in dlf\_serial.f90.  Which of the corresponding 
.o object files is linked should depend on the build option chosen.  For example, the default build 
of the standalone DL-FIND with its test driver program gives a serial executable.
A \texttt{make parallel} command will produce the parallel-enabled standalone executable, 
Pfind.x.  See makefile and Makefile.standalone for details.

%The variable \texttt{glob\%ntasks} stores the number of taskfarms requested.

Notes:
\begin{itemize}
\item If DL-FIND is in charge of disentangling the standard output from all the processors, then the 
variable \texttt{keep\_alloutput} in \texttt{dlf\_global\_module.f90} is used.
Rather than being an input option, 
it is hardwired in the code (so that it can be known before any output occurs).
For example, \texttt{keep\_alloutput} is used in \texttt{main.f90} (when a standalone program is made for testing) in the 
interface subroutine \texttt{dlf\_output} as follows: if true, then for processor $n$ ($n/=0$) in the global communicator (\texttt{MPI\_COMM\_WORLD}), 
the file output.proc$<n>$ is opened on unit \texttt{stdout}; 
if false, then for processor $n$ ($n/=0$), /dev/null is ``opened'' on unit \texttt{stdout}.
Output from the rank-zero processor in either case goes to standard out or a named file on unit \texttt{stdout}.

If such I/O issues are dealt with by the main, calling program, then simply use the subroutine \texttt{dlf\_output} to 
change the DL-FIND defaults (\texttt{stdout} and \texttt{stderr} stored in 
\texttt{dlf\_global\_module.f90}) for the required unit numbers, if necessary.
%Default value is .false.

\item Integer variables \texttt{mpi\_rk} and \texttt{mpi\_ik} are declared in \texttt{dlf\_mpi\_module} and set in 
subroutine \texttt{dlf\_mpi\_initialize} to the values of the MPI datatypes 
\texttt{MPI\_Double\_precision} and \texttt{MPI\_Integer}, respectively.
This matches the use of real(rk) in DL-FIND's declarations, where rk = kind(1.d0).  However, problems will arise if 
a compiler flag is used to change the nature of double precision numbers, as the MPI libraries will 
(probably) have been complied with a different specification for double precision.  Could use \texttt{MPI\_Sizeof} and 
\texttt{MPI\_Type\_match\_size} instead if this turns out to be a common problem.

\item Random numbers are an integral part of the parallel optimisers.
Therefore, the seeding of the random-number generator is dealt with in dlf\_parallel\_opt.f90.  Current policy 
is that only the rank-zero processor generates random numbers for the parallel optimisers.  Only the routines that manipulate the population 
need random numbers, so currently only the rank-zero processor does any work in such routines.
\end{itemize}

\subsubsection{Restart mechanism:}

The optimiser should be fully restartable. Status: everything is restartable.

\subsubsection{Compilation:} The portland compiler version 7.1 and 7.1-1 (and
7.1-2) do not compile the code properly. They do not save the contents of the
hdlc derived type. This is a compiler bug. The portland compilers 6.1 and
7.0-4 do compile it.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Time Line}

A detailed time line for the DL-FIND project is not envisaged. There are,
however, some milestones:
\begin{itemize}
\item Working implementation of NEB and the dimer method in GAMESS-UK and
  ChemShell. To be completed by 31/03/2007. Completed.
\item Implementation of an exhaustive test set. To be completed by 30/09/2007.
\item Implementation of a global task manager to enable automated
  optimisation. To be completed by 31/03/2008.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Quality Assurance Plan}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The software should compile on the following compilers: g95, ifort, gpf95,
gfortran, as well as xlf95. It should pass standard Fortran check programs
(\emph{TODO: to be specified}). For the time being, these are to be checked at
regular intervals by the author (J. K\"astner).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Software Design Plan}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Language: Fortran 95 + TR 15581 (technical report: meaning, allocatable arrays
in derived types are used). Pointers are only to be used where strictly
necessary.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Interface to the Calling Program \label{sec:interface}}

DL-FIND is designed as a library to be linked to quantum chemical or MD codes.
However, for testing purposes, a driver module (\texttt{main.f90}) providing some
analytic energy functions is used. DL-FIND is included in GAMESS-UK and
ChemShell.

The routine to be called by the main program is
\texttt{dl\_find(nvarin,nvarin2,nspec,master)}, in the file \texttt{dl\_find.f90}. It
only returns after the complete geometry optimisation.

The main program has to provide the following routines, which are called from
\texttt{dl\_find.f90} and \texttt{dlf\_formstep.f90}:
\begin{itemize}
\item \texttt{dlf\_get\_params(...)} Provide input parameters. The argument
  list is expected to change as DL-FIND is developed. New arguments should
  only be appended to existing ones which makes it possible to keep interfaces
  up to date. The arguments are documented in \texttt{dlf\_global\_module.f90}.
\item \texttt{dlf\_get\_gradient(nvar,coords,energy,gradient,iimage,status)}
  Calculate the energy and gradient at the position coords.
\item \texttt{dlf\_get\_hessian(nvar,coords,hessian,status)} Calculate the
  Hessian at the position coords.
\item \texttt{dlf\_get\_multistate\_gradients(nvar,coords, energy, gradient,
    coupling, needcoupling, iimage, status)} Calculate gradients of multiple
    (currently implemented: 2) electronic states. Required for conical
    intersection search.
\item \texttt{dlf\_put\_coords(nvar,switch,energy,coords,iam)} Feeds a geometry
  back to the calling program. If switch is 1, coords contains the actual
  geometry. If switch is 2, coords contains the transition mode.
  The presence of iam allows the behaviour on the rank zero processor to be coded 
  differently: for 
  example, turning off the writing of coordinates to a file from all but the
  rank-zero processor.
\item \texttt{dlf\_error()} Error termination. Return to the calling
  program. \texttt{dlf\_error} should not return.
\item \texttt{dlf\_update()} Allows the calling code to update any neighbour
  list, i.e. allows for discontinuities in the potential energy surface. This
  routine is called after a reset of the optimisation algorithm.
\end{itemize}
These may be c routines but have to be Fortran-callable. Examples of interface
routines are available in \texttt{main.f90}, \texttt{dlf.c}, and
\texttt{dlfind\_gamess.m}.

If the taskfarming functionality is required, then the following additions to the 
main program may also be made:
\begin{itemize}
\item call \texttt{dlf\_mpi\_initialize()} to either have DL-FIND set up the global MPI communications, 
      including calling 
      \texttt{MPI\_Init}, or to get the required parameters for the global communications that have 
      already been set up in the 
      main program.  In the former case, this call should be placed before any 
      I/O occurs, as close to the start of the main program as possible, as 
      usual with \texttt{MPI\_Init}.  If the main program sets up the communications, then this 
      call should be made after the rank of each processor and the total number of processors are known 
      (ideally, immediately afterwards).
      This call is an obligatory addition to the main program.
\item call \texttt{dlf\_mpi\_finalize()} to close down MPI before exiting the main program.
      Not necessary if the main program calls \texttt{MPI\_Finalize} already; can be added 
      safely after such a call in the main program (but is redundant).
\item call \texttt{dlf\_output(dlf\_stdout,dlf\_stderr)} passing unit numbers for I/O from 
      the main program to DL-FIND.  The call may be omitted if it is not necessary to change 
      the defaults set in \texttt{dlf\_global\_module.f90}, and if the main program deals with 
      the output from different processors.
\item provide subroutine \texttt{dlf\_output(dlf\_stdout,dlf\_stderr)}, if required, to set 
      the DL-FIND variables stdout 
      and stderr.  If necessary, implement the strategy for dealing with standard output from the 
      different processors here.
\item provide subroutine \texttt{dlf\_put\_procinfo(dlf\_nprocs,dlf\_iam,dlf\_global\_comm)}. 
      Called from \texttt{dlf\_mpi\_initialize} if \texttt{MPI\_Init} was called there.  Passes the 
      total number of processors, 
      the rank of the current process and a variable set to the handle for the 
      global communicator (\texttt{MPI\_COMM\_WORLD)} to the main program. 
\item provide subroutine \texttt{dlf\_get\_procinfo(dlf\_nprocs,dlf\_iam,dlf\_global\_comm)}. 
      Called from \texttt{dlf\_mpi\_initialize} if \texttt{MPI\_Init} had already been called by the 
      main program.  The total number of processors, 
      the rank of the current process and a variable set to the handle for the 
      global communicator (\texttt{MPI\_COMM\_WORLD)} are passed from the main program to 
      DL-FIND. 
\item provide subroutine \texttt{dlf\_put\_taskfarm(dlf\_ntasks,dlf\_nprocs\_per\_task, 
      dlf\_iam\_in\_task,dlf\_mytask,dlf\_task\_comm,dlf\_ax\_tasks\_comm)}.
      Called from \texttt{dlf\_make\_taskfarm} if DL-FIND sets up the split communicators.  
      Passes to the main program the number of farms, the number of processors per 
      farm, the rank of 
      the current process in its farm, the rank of the current process's farm, and 
      variables containing the handles for communicators within each farm and for the 
      rank-$n$ processor in each farm.  The main program indicates to DL-FIND which of the 
      two should set up the split communicators via an argument to the general interface routine 
      \texttt{dlf\_get\_params}.  $\texttt{tdlf\_farm}=0$ means the main program does the setup; 
      $\texttt{tdlf\_farm}\neq 0$ means DL-FIND does.
\item provide subroutine \texttt{dlf\_get\_taskfarm(dlf\_ntasks,dlf\_nprocs\_per\_task,
      dlf\_iam\_in\_task,dlf\_mytask,dlf\_task\_comm,dlf\_ax\_tasks\_comm)}.
      Called from dlf\_make\_taskfarm if the main program sets up the split communicators.  
      The number of farms, the number of processors per farm, the rank of 
      the current process in its farm, the rank of the current process's farm, and 
      variables containing the handles for communicators within each farm and for the 
      rank-$n$ processor in each farm, are all passed from the main program to DL-FIND.
      The main program indicates to DL-FIND which of the
      two should set up the split communicators via an argument to the general interface routine
      \texttt{dlf\_get\_params}.  $\texttt{tdlf\_farm}=0$ means the main program does the setup;
      $\texttt{tdlf\_farm}\neq 0$ means DL-FIND does.
\item ensure either \texttt{MPI\_Abort} or \texttt{dlf\_mpi\_abort()} is called from the 
      \texttt{dlf\_error()} subroutine.
\end{itemize} 

Recipies of how to include DL-FIND in ChemShell and GAMESS-UK are in the file
\texttt{building\_chemshell\_with\_dl-find}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Program Units and Their Contents:}

\subsubsection{Modules}

The code makes use of four modules that can be used from any subroutine
throughout the DL-FIND: \texttt{dlf\_parameter\_module}, which only provides
the real kind \texttt{rk}. It is located in the file
\texttt{dlf\_stat\_module.f90}.  \texttt{dlf\_checkpoint}: variables and
subroutines for reading and writing checkpoint files. \texttt{dlf\_stat}:
statistics. May be deleted and replaced in the future.  \texttt{dlf\_global}:
contains many global parameters and also arrays (Cartesian coordinates,
internal coordinates, the step, ...). All those parameters are part of a
variable \texttt{glob}, which has a derived type (also defined in the module
\texttt{dlf\_global}). \texttt{pi}, \texttt{stdout}, and \texttt{stderr} are
defined there as well.

All other modules must only be used within the file they are defined! If data
of these module should be provided to other files (units), get- and set
routines must be used!

\subsubsection{Main Units:}

\begin{itemize}
\item Main Unit (\texttt{dl\_find.f90})
\item Convergence tester 
\item Coordinate transform
\item Optimisation algorithms (\texttt{dlf\_formstep.f90} and routines called
  from there)
\item Scalestep: line search and trust radius approaches
\item Utility units
\end{itemize}

\subsubsection{Other details:}

File units that are used longer than for an immediate write ($>1000$):
1001 -- 1001+nimage,max 1050 for xyz of NEB (\texttt{dlf\_neb.f90})
     
When arrays are allocated, the variables \texttt{glob\%storage} and
\texttt{glob\%maxstorage} should be adjusted accordingly to enable control
over the memory usage.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Program Documentation}

The documentation is done using the robodoc tool. Each (important) subroutine
should have an entry marking its connection within the code, the input and
output variables of the global module, and its main purpose. An example file is
\texttt{dlf\_dimer.f90}.

Details of the documentation:

Fortran subroutines are to be documented as functions. The header gives the
unit and the full subroutine name ``NAME'' should not be specified
``SYNOPSIS'' should be the actual code statement beginning the subroutine.

Module header names should be something short and descriptive rather than the
actual Fortran name.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Code fragments obtained from external sources}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Part of \texttt{dlf\_lbfgs.f90} were obtained from
\texttt{http://www.ece.northwestern.edu/\~nocedal/lbfgs.html}.

The HDLC part has been taken from the HDLC optimiser in ChemShell. These are
the files: \texttt{dlf\_hdlc\_constraint.f90} \texttt{dlf\_hdlc\_hdlclib.f90}
\texttt{dlf\_hdlc\_interface.f90} \texttt{dlf\_hdlc\_matrixlib.f90} and
\texttt{dlf\_hdlc\_primitive.f90}. Walter Thiel agreed to make them publically
available (even under GNU license), as long as he gets DL-FIND for the MNDO
code:

\begin{quote}
  [...] I have included the coordinate transformation part of the HDLC
  optimiser into DL-find. I expect HDLCs to work with the dimer method, and
  maybe they even work with NEB. I hope there are no objections from you as
  long as it only goes into ChemShell and gamess.  However, I would like to
  make it available more broadly. It would be interesting to include it into
  other Daresbury codes (I mainly think of DL\_POLY and possibly Crystal -- if
  those people are interested, maybe the solid state people (around Walter
  Temmerman) are also interested).  At a later stage, I think it would also be
  worth making DL-find with the HDLC coordinate transform available under the
  GNU license.  Alternatively, I would have to make a DL-find version that
  runs without HDLCs.
  
  What do think about that? I.e. would you give us (DL) permission to
  distribute part of the HDLCopt code more widely?

  Dear Johannes,
  
  all this is fine with me. It is a good idea to make HDLCopt available to a
  wider audience. In return, I would like to include DL-find in the MNDO
  program (with no restrictions concerning its distribution). I had planned to
  add other optimisers to MNDO anyway, and it would obviously be good not to
  duplicate such work.

  Best wishes and Happy New Year,

  Walter Thiel

\end{quote}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Test Plan}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The driver program will enable tests of all functionalities with analytic
energy function. Should noisy gradients be tested as well, a random part can
be added to the gradient. Test cases from analytic 2-dimensional potentials
showing minima and transition states (e.g. the M\"uller--Brown potential
\cite{mul79}) as well as clusters of Lennard--Jones particles will be used.
The latter can be extended to systems with very many degrees of freedom.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Method documentation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This section documents non-standard implementations in DL-FIND. Published
standard methods are not documented here.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Weights}

A list of weights (one for each atom) can be specified in the input array
coords2. It will be remapped onto weights of each degree of freedom (internal
coordinates) to be optimised.

It can be used to restrict the NEB path to a certain set of atoms, or the
direction of the dimer.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Documentation of the Input Options -- User Documentation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

System documentation is available in the source code after the subroutine
headers. The input options will be explained here (User Documentation).

\paragraph{Print level (Variable \texttt{printl}):}
\begin{description}
\item[0] no printout
\item[2] print something
\item[4] be verbose
\item[6] debug
\end{description}

\paragraph{Type of coordinate system (Variable \texttt{icoord}):} 
\begin{description}
\item ``unit place'' means \texttt{icoord} modulo 10
\item[0--9] The whole system is to be treated as one image 
\item[Unit place 0] Cartesians
\item[Unit place 1] HDLC - internals \cite{bil00a}
\item[Unit place 2] HDLC - TC \cite{bil00a}
\item[Unit place 3] DLC - internals \cite{bil00a}
\item[Unit place 4] DLC - TC \cite{bil00a}
\item[Unit place 5] Mass-weighted Cartesians ($\sqrt{m}x$) -- will be deleted:
  use glob\%massweight for that now!
\item[1X] \emph{Lagrange-Newton conical intersection search, with two extra 
coordinates corresponding to the gradient difference vector and 
interstate coupling gradient constraints.}
\item[10X]  NEB with endpoints free
\item[11X] NEB with endpoints moving only perpendicular to their tangent
  direction
\item[12X] NEB with frozen endpoints.
\item[13X]  NEB with endpoints free. Only initialisation in coordiates X,
  optimisation in cartesians.
\item[14X] NEB with endpoints moving only perpendicular to their tangent
  direction. Only initialisation in coordiates X,
  optimisation in cartesians.
\item[15X] NEB with frozen endpoints. Only initialisation in coordiates X,
  optimisation in cartesians.
  
  The NEB version implemented is the ``improved-tangent'' NEB \cite{hen00a}
  (also called ``upwind scheme'') with a climbing image.
  
\item[20X] Dimer method \cite{hen99,hey05}. Translation and rotation of the
  dimer are covered by the optimiser specified trough \texttt{iopt}. Requires
  two energy evaluation per iteration.
\item[21X] Dimer method. Rotation of the dimer is done by a line search within
  the dimer module, two energy calculations are used per rotation. Requires at
  least two energy evaluation per iteration.
\item[22X] Dimer method. Rotation of the dimer is done by a line search within
  the dimer module, one energy calculation is done per iteration, the other
  one is interpolated. Requires at least two energy evaluation per iteration.
  
  In all dimer versions: If a second set of coordinates is provided, it
  determines the dimer direction, if not, the dimer direction is randomised.
\end{description}

\paragraph{Multistate calculations (Variable \texttt{imultistate}):}
\begin{description}
\item[0] Single state calculation (default)
\item[1] Conical intersection optimisation (penalty function algorithm).
\item[2] Conical intersection optimisation (gradient projection algorithm).
\item[3] \emph{Conical intersection optimisation (Lagrange-Newton algorithm).}
\end{description}

\paragraph{Type of optimisation algorithm (Variable \texttt{iopt}):}
\begin{description}
\item[0] Steepest descent
\item[1] Conjugate gradient following Polak--Ribi\`ere \cite{pol69} (with automatic
  restarts based on the criterion by Powell and Beale) that is not coded
  properly ...
\item[2] Conjugate gradient following Polak--Ribi\`ere \cite{pol69} with CG
  restart every 10 steps (hardcoded at the moment)
\item[3] L-BFGS \cite{liu89,noc80}
\item[10] P-RFO \cite{cer81,sim83,ban85,bak86} A switching mechanism for the
  mode to be followed is included, but does not seem to help in any of the
  cases I tried sofar.
\item[11] Just calculate the Hessian and to a thermal analysis (harmonic
  approximatin for entropy, ...) -- implementation only started.
\item[20] Newton--Raphson (mainly implemented to test Hessian-based methods)
\item[30] Damped dynamics using the variables timestep, fric0, fricfac, and
  fricp. The frictions are defined that 0 corresponds to free (undamped)
  dynamics, and 1 corresponds to steepest descent.
\item[51] Random (stochastic) search \cite{brooks57,luusj73}, using the variables po\_pop\_size, po\_radius,
            po\_contraction, po\_tolerance\_r, po\_tolerance\_g, 
            po\_distribution, po\_maxcycle, po\_scalefac.
\item[52] Genetic algorithm \cite{holland75,goldberg89,haupth98}, using the variables po\_pop\_size, po\_radius,
            po\_tolerance\_g, po\_maxcycle, po\_init\_pop\_size, po\_reset, 
            po\_mutation\_rate, po\_death\_rate, po\_nsave
\end{description}

\paragraph{Type of line search or trust radius (Variable \texttt{iline}):}
\begin{description}
\item[0]   simple scaling of the proposed step, taking maxstep into account
\item[1]   Trust radius based on energy as acceptance criterion (recommended
  for L-BFGS optimisation)
\item[2]   Trust radius based on gradient as acceptance criterion (recommended
  for CG optimisation)
\item[3]   Hard-core line search. Does not work at the moment...
\end{description}

\paragraph{Type of initial Hessian (Variable \texttt{inithessian}):}
\begin{description}
\item[0]   Calculate externally using \texttt{dlf\_get\_hessian}. Defaults to two point finite
  difference if an external Hessian is unavailable.
\item[1]   Build by one point finite difference of the gradient
\item[2]   Build by two point finite difference of the gradient
\item[3]   Build a diagonal Hessian with a single one point finite difference
\item[4]   Set the Hessian to be an identity matrix
\end{description}

\paragraph{Hessian update mechanism (Variable \texttt{update}):}
\begin{description}
\item[0]   No update. Always recalculate the Hessian
\item[1]   Powell update \cite{pow71}
\item[2]   Bofill update \cite{bof94}
\item[3]   BFGS update
\end{description}

\paragraph{Fragment and frozen atom information (Variable \texttt{spec}):}
The array \texttt{spec} has an entry for each atom. Meaning:
\begin{description}
\item[$>$0] Fragment (residue) number the atom belongs to. Active in the
  optimisation.
\item[0]   Active, and treated in Cartesian coordinates, even if other atoms
  in the system are covered by (H)DLCs.
\item[-1]  Frozen 
\item[-2]  x-component frozen (Cartesians only)
\item[-3]  y-component frozen (Cartesians only)
\item[-4]  z-component frozen (Cartesians only)
\item[-23]  x and y-components frozen (Cartesians only)
\item[-24]  x and z-components frozen (Cartesians only)
\item[-34]  y and z-components frozen (Cartesians only)
\end{description}
Atoms with \texttt{spec} $<-1$ will be completely frozen if used in HDLCs. One
may use Cartesian constraints there to specify frozen components. Atoms with
\texttt{spec} $>0$ will be free in Cartesian coordinats.

There are numerous other paramers that can be set via the routine
\texttt{dlf\_get\_params}. These are at the moment explained in the global
module, \texttt{dlf\_global\_module.f90}.

\subsubsection{Restarting}

\begin{description}
\item[\texttt{dump}] after how many energy and gradient evaluations is a
  restart file (checkpoint file) to be written? Default: 0 (never).
\item[\texttt{restart}] 0: new run (default), 1: read all checkpoint files and start
  from those.
\end{description}

It is only possible to restart a job with most parameters equal to those in
the checkpoint file. Exeptions (input parameters that may be different from
the ones in the previous run, i.e. that are not written to the checkpoint
file): \texttt{maxcycle}. If different parameters are to be used, the latest
geometry should be used and the optimiser should be started from scratch
(\texttt{restart=0}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Collection ...}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Force weighted intenal coordinates: have a look at
http://www.molpro.net/molpro-user/archive/all/msg00071.html

At the moment, I am experiencing problems with NEB in internal coordinates.
While the structure may change continously, the internals may not. Consider
H$_2$CO separation: it has two dihedrals. In the bound configuration, both are
180\textdegree{}. In the dissociated configuration, one is 0 and one is 180,
as H$_2$ moves to one side of CO. How to deal with that? 

One possible solution: Impropers are automatically put onto atoms with nearly
planar configuration (what if more than 3 connections?). If an improper is
placed on an atom, this one should not be in the middle of a dihedral that
include these atoms. This, however, had be removed again as it leads to an
underdetermined systems when not all atoms next to the planar atom are
monovalent.

svn properties:

\verb'svn propset svn:keywords "URL Author Date Rev Id" dlf_util.f90'

%\bibliographystyle{myunsrt}
%\addcontentsline{toc}{chapter}{Bibliography}
%\bibliography{text}

\begin{thebibliography}{10}

\bibitem{hen99}
G.~Henkelman and H.~J{\'o}nsson:
\newblock A dimer method for finding saddle points on high dimensional
  potential surfaces using only first derivatives.
\newblock {\em J. Chem. Phys.\/}~{\bf 111}, 7010  (1999).

\bibitem{hey05}
A.~Heyden, A.~T. Bell, and F.~J. Keil:
\newblock Efficient methods for finding transition states in chemical
  reactions: Comparison of improved dimer method and partitioned rational
  function optimization method.
\newblock {\em J. Chem. Phys.\/}~{\bf 123}, 224101  (2005).

\bibitem{kae08}
J.~K\"astner and P.~Sherwood:
\newblock Superlinearly converging dimer method for transition state search.
\newblock {\em J. Chem. Phys.\/}~{\bf 128}, 014106  (2008).

\bibitem{woo03}
H.~L. Woodcock, M.~Hodo{\v s}{\v c}ek, P.~Sherwood, Y.~S. Lee, H.~F.
  Schaefer~III, and B.~R. Brooks:
\newblock Exploring the quantum mechanical/molecular mechanical replica path
  method: a pathway optimization of the chorismate to prephenate Claisen
  rearrangement catalyzed by chorismate mutase.
\newblock {\em Theor. Chem. Acc.\/}~{\bf 109}, 140  (2003).

\bibitem{pow71}
M.~J.~D. Powell: .
\newblock {\em Math. Prog.\/}~{\bf 26}, 1  (1971).

\bibitem{bof94}
J.~M. Bofill:
\newblock Updated Hessian matrix and the restricted step method for locating
  transition structures.
\newblock {\em J. Comput. Chem.\/}~{\bf 15}, 1  (1994).

\bibitem{brooks57}
S.~H.~Brooks:
\newblock A discussion of random methods for seeking maxima.
\newblock {\em Operations Research}~\textbf{6}, 244 (1957).

\bibitem{luusj73}
R.~Luus, and T.~H.~I.~Jaakola:
\newblock Optimization by Direct Search and Systematic Reduction of the Size of Search Region.
\newblock {\em AIChE Journal}~\textbf{19}, (1973).

\bibitem{holland75}
J.~H.~Holland:
\newblock Adaptation in Natural and Artificial Systems.
\newblock University of Michigan Press, Ann
  Arbor, (1975).

\bibitem{goldberg89}
D.~Goldberg:
\newblock Genetic Algorithms in Search, Optimization, and Machine Learning.
\newblock Addison-Wesley, New York, (1989).

\bibitem{haupth98}
R.~L.~Haupt, and S.~E.~Haupt:
\newblock Practical genetic algorithms.
\newblock Wiley, New York, (1998).

\bibitem{mul79}
K.~M{\"u}ller and L.~D. Brown:
\newblock Location of saddle points and minimum energy paths by a constrained
  simplex optimization procedure.
\newblock {\em Theor. Chim. Acta\/}~{\bf 53}, 75  (1979).

\bibitem{bil00a}
S.~R. Billeter, A.~J. Turner, and W.~Thiel:
\newblock Linear scaling geometry optimization and transition state search in
  hybrid delocalised internal coordinates.
\newblock {\em Phys. Chem. Chem. Phys.\/}~{\bf 2}, 2177  (2000).

\bibitem{hen00a}
G.~Henkelman and H.~J{\'o}nsson:
\newblock Improved tangent estimate in the nudged elastic band method for
  finding minimum energy paths and saddle points.
\newblock {\em J. Chem. Phys.\/}~{\bf 113}, 9978  (2000).

\bibitem{pol69}
E.~Polak and G.~Ribi\`ere:
\newblock Note sur la convergence de directions conjug\'ees.
\newblock {\em Ref. Fra. Inf. Rech. Op.\/}~{\bf 3}, 35  (1969).

\bibitem{liu89}
D.~C. Liu and J.~Nocedal:
\newblock On the limited memory BFGS method for large scale optimization.
\newblock {\em Math. Program.\/}~{\bf 45}, 503  (1989).

\bibitem{noc80}
J.~Nocedal:
\newblock Updating quasi-Newton matrices with limited storage.
\newblock {\em Math. Comp.\/}~{\bf 35}, 773  (1980).

\bibitem{cer81}
C.~J. Cerjan and W.~H. Miller:
\newblock On finding transition states.
\newblock {\em J. Chem. Phys.\/}~{\bf 75}, 2800  (1981).

\bibitem{sim83}
J.~Simons, P.~J{\o}rgensen, H.~Taylor, and J.~Ozment:
\newblock Walking on potential energy surfaces.
\newblock {\em J. Phys. Chem.\/}~{\bf 87}, 2745  (1983).

\bibitem{ban85}
A.~Banerjee, N.~Adams, J.~Simons, and R.~Shepard:
\newblock Search for stationary points on surfaces.
\newblock {\em J. Phys. Chem.\/}~{\bf 89}, 52  (1985).

\bibitem{bak86}
J.~Baker:
\newblock An algorithm for the location of transition states.
\newblock {\em J. Comput. Chem.\/}~{\bf 7}, 385  (1986).

\end{thebibliography}

\end{document}
