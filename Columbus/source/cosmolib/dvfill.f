!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine dvfill(nppa,dirvec,ierrout)

      implicit real*8 (a-h,o-z)

c ---------------------------------------------------------------------
c     construction of a mesh on a unit sphere
c
c nppa:     number of points per atom
c ierrout:  unit for error output
c dirvec:   normalized direction vectors
c
c ---------------------------------------------------------------------
c

      dimension dirvec(3,nppa)
      integer fset(3,20), kset(2,30)

      data kset/ 1, 2, 1, 3, 1, 4, 1, 5, 1, 6,
     1            12,11,12,10,12, 9,12, 8,12, 7,
     2             2, 3, 3, 4, 4, 5, 5, 6, 6, 2,
     3             7, 8, 8, 9, 9,10,10,11,11, 7,
     4             2,7,7,3,3,8,8,4,4,9,9,5,5,10,10,6,6,11,11,2/
      data fset/ 1, 2, 3, 1, 3, 4, 1, 4, 5, 1, 5, 6, 1, 6, 2,
     1            12,11,10,12,10, 9,12, 9, 8,12, 8, 7,12, 7,11,
     2             2, 3, 7, 3, 4, 8, 4, 5, 9, 5, 6,10, 6, 2,11,
     3             7, 8, 3, 8, 9, 4, 9,10, 5,10,11, 6,11, 7, 2/

      dirvec (1,1) =  -1.d0
      dirvec (2,1) =   0.d0
      dirvec (3,1) =   0.d0
      nd=1
      r=sqrt(0.8d0)
      h=sqrt(0.2d0)
      do 10 i= -1,1,2
         do 10 j= 1,5
            nd=nd+1
            beta=1.d0+ j*1.25663706d0 + (i+1)*0.3141593d0
            dirvec(2,nd)=r*cos(beta)
            dirvec(3,nd)=r*sin(beta)
            dirvec(1,nd)=i*h
   10 continue
      dirvec (2,12) =  0.d0
      dirvec (3,12) =  0.d0
      dirvec (1,12) =  1.d0
      nd=12
c  nppa=10*3**k*4**l+2
      m=(nppa-2)/10
      do 20 k=0,10
         if ((m/3)*3 .ne. m) go to 30
   20 m=m/3
   30 do 40 l=0,10
         if ((m/4)*4 .ne. m) go to 50
   40 m=m/4
   50 if (10*3**k*4**l+2 .ne. nppa) then
       write (ierrout,'(a)') 'ERROR cosmo: value of nppa not allowed:
     & it must be 10*3**k*4**l+2'
      stop
      endif
      kh=k/2
      m=2**l*3**kh
c create on each edge 2**l*3**kh-1 new points
      do 70 i=1,30
         na=kset(1,i)
         nb=kset(2,i)
         do 70 j=1,m-1
            nd=nd+1
            do 60 ix=1,3
   60       dirvec(ix,nd)=dirvec(ix,na)*(m-j)+dirvec(ix,nb)*j
   70 continue
c create points within each triangle
      do 90 i=1,20
         na=fset(1,i)
         nb=fset(2,i)
         nc=fset(3,i)
         do 90 j1=1,m-1
            do 90 j2=1,m-j1-1
               nd=nd+1
               do 80 ix=1,3
   80          dirvec(ix,nd)=dirvec(ix,na)*(m-j1-j2)
     1                     +dirvec(ix,nb)*j1+dirvec(ix,nc)*j2
   90 continue
      if (k .eq. 2*kh) go to 140
c create to additional subgrids
      t=1.0d0/3.0d0
      do 110 i=1,20
         na=fset(1,i)
         nb=fset(2,i)
         nc=fset(3,i)
         do 110 j1=0,m-1
            do 110 j2=0,m-j1-1
               nd=nd+1
               do 100 ix=1,3
  100          dirvec(ix,nd)=dirvec(ix,na)*(m-j1-j2-2.0d0*t)
     1                 +dirvec(ix,nb)*(j1+t)+dirvec(ix,nc)*(j2+t)
  110 continue
      t=2.0d0/3.0d0
      do 130 i=1,20
         na=fset(1,i)
         nb=fset(2,i)
         nc=fset(3,i)
         do 130 j1=0,m-2
            do 130 j2=0,m-j1-2
               nd=nd+1
               do 120 ix=1,3
  120          dirvec(ix,nd)=dirvec(ix,na)*(m-j1-j2-2*t)
     1                  +dirvec(ix,nb)*(j1+t)+dirvec(ix,nc)*(j2+t)
  130 continue

c     --- normalize all vectors
  140 do 170 i=1,nppa
         dist=0.d0
         do 150 ix=1,3
  150    dist=dist+dirvec(ix,i)**2
         dist=1./sqrt(dist)
         do 160 ix=1,3
  160    dirvec(ix,i)=dirvec(ix,i)*dist
  170 continue

      return
      end
c
