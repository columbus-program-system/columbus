!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine cosmoinit(mtype, natoms)

      implicit real*8(a-h,o-z)

      include 'cosinc.h'

      parameter (bohr=0.529177249d0)

c-------------------COMMON BLOCKS---------------------------------------
c
c I/O units
      include 'cosmovar.inc'
      include 'coss.inc'

      save /cosmovar/
      save /coss/


      dimension    mtype(natoms)
      character*80  string

c
c
c--------INITIALIZE PARAMETER AND RADII---------------------------------
c for fepsi = 1 -> eps = inf, eps will be kept as -1.d0
      eps   = -1.d0
c refind: refaction index for excited states
      refind= -1.d0
      fn2   = -1.d0
      nppa  = -1
      nspa  = -1
      disex = -1.d0
      routf = -1.d0
      rsolv = -1.d0
      lcavity = -1
      phsran = -1.d0
      ampran = -1.d0

      do i=1,natoms
         srad(i) = -1.d0
      enddo
c--------READ PARAMETER AND RADII---------------------------------------

      rewind(icosin)
 10   call readinp(icosin,string,80)
      i=0
      i=index(string,' ')
c     string does not contain blancs at pos 1 unless eof reached
      if(i .gt. 1) then
         i=index(string,'epsilon=')
         if(i .gt. 0) then
            read (string(i+8:),*,END=1000,ERR=1000) eps
            goto 10
         endif

         i=index(string,'refraction_index=')
         if(i .gt. 0) then
            read (string(i+17:),*,END=1000,ERR=1000) refind
            goto 10
         endif

         i=index(string,'nppa=')
         if(i .gt. 0) then
            read (string(i+5:),*,END=1000,ERR=1000) nppa
            goto 10
         endif

         i=index(string,'nspa=')
         if(i .gt. 0) then
            read (string(i+5:),*,END=1000,ERR=1000) nspa
            goto 10
         endif

         i=index(string,'disex=')
         if(i .gt. 0) then
            read (string(i+6:),*,END=1000,ERR=1000) disex
            goto 10
         endif

         i=index(string,'rsolv=')
         if(i .gt. 0) then
            read (string(i+6:),*,END=1000,ERR=1000) rsolv
            rsolv = rsolv / bohr
            goto 10
         endif

         i=index(string,'routf=')
         if(i .gt. 0) then
            read (string(i+6:),*,END=1000,ERR=1000) routf
            goto 10
         endif

         i=index(string,'cavity=')
         if(i .gt. 0) then
            read (string(i+7:),*,END=1000,ERR=1000) lcavity
            goto 10
         endif

         i=index(string,'phsran=')
         if(i .gt. 0) then
            read (string(i+7:),*,END=1000,ERR=1000) phsran
            goto 10
         endif

         i=index(string,'ampran=')
         if(i .gt. 0) then
            read (string(i+7:),*,END=1000,ERR=1000) ampran
            goto 10
         endif

c        RADII

         i=index(string,'radius')
         if(i .gt. 0) then
            j=0
            j=index(string,'all')
            if(j .gt. 0) then
              read (string(j+3:),*,END=1000,ERR=1000) nuc, rad
              rad = rad/bohr
              do k=1, natoms
                if(mtype(k) .eq. nuc) then
                  if(srad(k) .ne. -1.d0) then
                    write(icolum,'(a,i5,a,/,a,f14.7)')
     &              'WARNING cosmoinit: radius for atom #',
     &              k,' assigned twice.','Used radius (bohr):',rad
                  endif
                  srad(k) = rad
                endif
              enddo
              goto 10
            else
              read (string(i+6:),*,END=1000,ERR=1000) num, rad
              rad = rad/bohr
              if(num .gt. natoms) then
                write(icolum,'(a,i5,a)')'WARNING cosmoinit: atom #',
     &              num,' not available!!'
                write(icolum,'(a,a)')'cosmoinit: Skip line: ',string
                goto 10
              endif
              if(srad(num) .ne. -1.d0) then
                write(icolum,'(a,i5,a,/,a,f14.7)')
     &          'WARNING cosmoinit: radius for atom #',
     &           num,' assigned twice.','Used radius (bohr):',rad
              endif
              srad(num) = rad
              goto 10
            endif
         endif

         goto 10
      endif

c--------CHECK PARAMETER AND RADII / SET DEFAULT------------------------

      if(eps .lt. 0.d0) then
        fepsi = 1.d0
      else
        fepsi=(eps-1.D0)/(eps+0.5D0)
      endif
c     refind only used in excited state calc. (must not be set)
      if(refind .gt. 0.d0) then
         fn2=((refind**2)-1.D0)/((refind**2)+0.5D0)
      endif
      if(nppa .lt. 0)      nppa = 1082
      if(nspa .lt. 0)      nspa = 92
      if(disex .lt. 0.d0)  disex = 10.d0
      if(routf .lt. 0.d0)  routf = 0.85
      if(rsolv .lt. 0.d0)  rsolv = 1.3/bohr
      if(lcavity .lt. 0)   lcavity = 1
      if(phsran .lt. 0.d0) phsran=0.d0
      if(ampran .lt. 0.d0) ampran=1.d-5

      if(ampran .gt. 1.d-5) then
        write(icolum,'(a,a)')'ERROR cosmoinit: ampran is too big,',
     &      'choose ampran <= 1.d-5'
        stop
      endif

c srad should keep radii including rsolv!

      do i=1,natoms
        if(srad(i) .lt. 0) then
          write(icolum,'(a,i5,a)')'ERROR cosmoinit: radius for atom #'
     &                  ,i,' is missing!!'
          stop
        endif
        srad(i) = srad(i) + rsolv
      enddo

c-----CALCULATE nspa AND nsph-------------------------------------------

      x0=log(nspa*0.1D0-0.199999D0)
      z3=log(3.D0)
      z4=log(4.D0)
      i4=int(x0/z4)

      nspb=0
      do i = 0,i4
          x = x0 - i * z4
          n = 3**int(x/z3) * 4**i
          if (n .gt. nspb) nspb = n
      enddo

      nsph = nspb / 3
      if (mod(nspb,3) .ne. 0) nsph = nspb / 4
      nspa = 10 * nspb + 2
      nsph = max(12,nsph*10+2)


      return

 1000 write(icolum,'(a,a)')'ERROR cosinit: Problems while ',
     &                     'reading cosmo.inp'

      stop
      end

