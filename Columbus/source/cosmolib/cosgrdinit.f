!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine cosgrdinit(icosout,icossu,icolum,lcavity,nps,fepsi,
     &                      nip,sude,isude,cosurf,iatsp,qcos,ar)
c----------------------------------------------------------------------
c     reads segment information from out.cosmo
c     and surface derivative data from sude
c----------------------------------------------------------------------

      implicit real*8 (a-h,o-z)

      include 'cosinc.h'
      parameter(bohr=.529177249d0)

      character*80 string
      dimension sude(4*nip),isude(4*nip)
      dimension cosurf(3,nps), iatsp(nps), qcos(nps), ar(nps)
      logical on

c----------------------------------------------------------------------
c READ FROM out.cosmo
      kseg=0
      on=.false.

      rewind(icosout)

 10   call readinp(icosout,string,80)

      i=0
      i=index(string,' ')
c     string does not contain blancs at pos 1 unless eof reached
      if(i .gt. 1) then
c     read segment information
         if(on) then
            i=index(string,'$')
c           end of seg. section
            if(i .gt. 0) on = .false.
         endif
c        start of seg. section
         i=index(string,'$segment_information')
         if(i .gt. 0)  then
c           read next non-comment line
            on = .true.
            goto 10
         endif
         if(on) then
            kseg=kseg+1
            read(string,*) n,iatsp(kseg),(cosurf(j,kseg),j=1,3),
     &          qcos(kseg),ar(kseg)
            ar(kseg)=ar(kseg)/bohr/bohr
         endif

         goto 10
      endif

c     check values
      if(kseg .ne. nps) then
         write(icolum,'(a,i4,a)')'ERROR cosgrdinit: Cannot read all'
     &                            ,nps,'segments from out.cosmo'
         write(icolum,'(a,i4,a)')'Read',kseg,' segments!'
         stop
      endif

c----------------------------------------------------------------------
c READ FROM sude
      if (lcavity.eq.1) then
          rewind(icossu)
          read(icossu,*,ERR=2000,END=2000) nip
          read(icossu,*,ERR=2000,END=2000) (sude(j),j=1,4*nip)
          read(icossu,*,ERR=2000,END=2000) (isude(j),j=1,4*nip)
      endif
c----------------------------------------------------------------------
      return
 1000 write(icolum,'(2a)')'ERROR cosgrdinit: Problems while ',
     &                     'reading out.cosmo'

      stop
 2000 write(icolum,'(2a)')'ERROR cosgrdinit: Problems while ',
     &                     'reading sude'
      stop
      end

