!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine readinp(inp,string,ilen)
c-----------------------------------------------------------------------
      implicit real*8 (a-h,o-z)
c
      character*(*) string
      character*400 stringbuf, str1, str2
c
 1    read (inp,'(a)',END=99) stringbuf
c     ignore line if first character is '#'
      if(index(stringbuf,'#').eq.1) goto 1
c     delete everything after comment identifier '#'
      ihash=index(stringbuf,'#')
      string(1:)=stringbuf(1:)
      if(ihash.gt.0) string(1:)=stringbuf(1:ihash-1)
c     delete spaces before '='
10    str1=string(1:)
      i=index(str1,' =')
      if (i.gt.0) then
        str2=string(i+1:)
        string(i:)=str2
        goto 10
      endif
c     delete spaces after '='
      i0=0
20    str1=string(i0+1:)
      i=index(str1,'=')
      if (i.gt.0) then
        i1=i
        str2=string(i0+i+1:)
22      j=index(str2,' ')
        if (j.eq.1) then
         i1=i1+1
         str2=string(i0+i1+1:)
         if ((i0+i1+1).ge.132) goto 30
         goto 22
        end if
        string(i0+i+1:)=str2
        i0=i0+i
        goto 20
      else
        goto 30
      endif
c     delete superfluous spaces between keywords
 30   i0=0
 31   str1=string(i0+1:)
      i=index(str1,' ')
      if (i.gt.0) then
        i1=i
        str2=string(i0+i+1:)
 32     j=index(str2,' ')
        if (j.eq.1) then
         i1=i1+1
         str2=string(i0+i1+1:)
         if ((i0+i1+1).ge.ilen) goto 39
         goto 32
        end if
        string(i0+i+1:)=str2
        i0=i0+i
        goto 31
      else
        goto 39
      endif
 39   continue
c kill leading blanc
      i=0
      str1=string(1:)
      i=index(str1,' ')
      if(i.eq.1) string(1:)=str1(2:)
      return
c
c     if end of file is reached, return empty string
99    string=' '
      return
      end
