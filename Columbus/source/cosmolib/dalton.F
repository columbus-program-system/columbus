!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
       block data cosmox
       include 'cosmovar.inc'
c
      data icosin/30/
      data icosa2/35/
c      data icosa3/38/
      data icosa3/56/
      data icosa/40/
      data icosco/43/
      data icossu/44/
      data icosout/51/
      data icolum/55/
      end
c

      subroutine driverDALTON(work,cosurf,phi,qcos,nps
     . ,lwork,iaoption,typecalc)
c
c Silmar - 08-02-02
c input variables: 
c lwork: free memory 
c work: array, with the size lwork
c iaoption: option to be computed: electrostatic potential or
c solvent modified integrals, 0 or 1
c nps: number of point charges
c cosurf: coordinates of the charges
c typecalc = type of calculation (MCSCF or CI), 0 or 1.
c qcos: charges from cosmo
c 
c output:
c phi: electrostatic potential
C Vsolv : solvent-modified one-electron integrals
c
C     NOTE
C
C     This is an experimental code for the evaluation of SCF, CC and
C     MCSCF molecular properties as energy derivatives or response 
C     functions.  The authors accept no responsibility for the 
C     performance of the code or for the correctness of the results.
C
C     The code (in whole or part) is not to be reproduced for
C     further distribution without the written permission of the aut
c    + hors.

C
C     If results obtained with this code are published, an appropriate
C     citation would be:  
C     "DALTON QCP, an electronic structure program".
C
C     This note is not to be removed or altered.
C
#include <implicit.h> 
c
#include <priunit.h>
c
c Silmar - 12/09/01- New common block(Vne) to access the
c Nuclear attraction integrals
c      
      INTEGER LWORK,iaoption
      REAL*8 WORK(LWORK),WRKDLM 
      DATA WRKDLM/8H*WRKDLM*/
#include <vsolv.h>
#include <option.h>
#include <Vne3.h>
#include <Tinteg.h>
#include <vne.h>
#include <Vne2.h>
#include <Vne4.h>
c
c
      integer nsubvnew
      common/erro/nsubvnew
c
      integer ntitle,mxtitle
      parameter (mxtitle = 6)
      parameter (nfilmx =55)
      integer nunits(nfilmx)
      integer filerr,typecalc
      integer noffstri
      integer noffsrec,noffslin(8)
      integer k,info(5) 
      integer dbglvl 
      integer faterr
      integer nbfpsy(8),nmopsy,nps,nrestp
      dimension noffstri(8)
      dimension noffsrec(8)
      dimension nmopsy(8)
      real*8  Vsolvrec(255*255)
      real*8  Vsolvmo(255*255)
      real*8  VCsolv(255*255)
      real*8  Trec(255*255)
      real*8  Tmo(255*255)
      real*8  TC(255*255)
      real*8 result,Vnenuctotal
      real*8 cosurf(3,*)
      real*8 phi(*)
      real*8 qcos(*)
      logical loop
      parameter(faterr=2)
      character*80 title(mxtitle)
      character*80 afmt
      character*4 labels(8)
      integer  forbyt,atebyt,nbmt
      external forbyt,atebyt
c common block for file units
      integer prtab,fmocoef,outputunit
      common/cfiles/nunits
      equivalence(nunits(45),prtab)
      equivalence(nunits(42),fmocoef)
      equivalence(nunits(1),outputunit)
      equivalence(nunits(12),nrestp)
c
c nrestp: unit of mcscf natural orbitals
c
c common block to get the root number      
      integer root
      character*20 fnocoef
      common/rootnumber/root
c
       common/cosmoblock/nbfpsy
c
c In case of MCSCF calculation the nsym to be used should come
c from this common block
c
      integer nxy, mult, nsym,nsymm, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c
c Opens a file containing the coordinates of the charges
c point is the counter for the number of points
c
      ioption = iaoption
c
      dbglvl = 0
c
      do point = 1,nps
c
       if(ioption.eq.0) then
c
      xpot(point)=cosurf(1,point)
      ypot(point)=cosurf(2,point)
      zpot(point)=cosurf(3,point)
c      write(*,*)
c      write(*,*)'point = ',point
c      write(*,*) 'xpot(point)',xpot(point)
c      write(*,*) 'ypot(point)',ypot(point)
c      write(*,*) 'zpot(point)',zpot(point)
c      write(*,*)
c
       else
c     
      xpot(point)=cosurf(1,point)
      ypot(point)=cosurf(2,point)
      zpot(point)=cosurf(3,point)
      chargept(point)=qcos(point)
c      write(*,*)
c      write(*,*)'point = ',point
c      write(*,*) 'xpot(point)',xpot(point)
c      write(*,*) 'ypot(point)',ypot(point)
c      write(*,*) 'zpot(point)',zpot(point)
c      write(*,*) 'chargept(point)',chargept(point)
c      write(*,*)
c
       endif !for ioption
c
      enddo
c
c      open(unit=66,file='chargescoord',status='old')
c
c ptspbl is the number of points per block, in the integrals
c option
c      
c      read(66,*) ptspbl
c
c reads all the points
c
c  444 read(66,*,end=999) xpot(point),
c     & ypot(point),zpot(point),chargept(point) 
c      point = point + 1
c      go to 444
c  999 continue
c      close(unit=66,status='keep')
      point=point-1
c     
      maxsize=lwork

      if(typecalc.ne.0) then
c
c Silmar-05-02-02-read the nocoef_file
c with the subroutine moread in the case typecalc=1,
c i.e, CI calculation
c
       write(*,*)
       write(*,*)'root = ',root
       write(*,*)
      write(fnocoef,1009) root
1009  format('nocoef_ci.',i1)
      open(prtab,file=fnocoef,status='old')
      nlist = lupri
c First read the title section to determine maxvect
c
      call moread(prtab,10,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,0,WORK) 
c      
      endif ! calctype.ne.0
c
c
c     locmo :  start address of mo coefficients
c     lococc:  start address of occupation number vector
c     locdens:  start address of density matrix
c     loctop:  start address of free part
c
c     noffstri(8)  symmetry block offsets for triangular 
c                  stored matrix
c     noffsrec(8)  symmetry block offsets for rectangular
c                  stored matrix
c     i.e. access 3rd symmetry block : core(locmo+noffsrec(3))
c
      maxocc  = 0 
      maxvect = 0
      maxdens = 0 
      do i = 1,nsym
      if (i.eq.1) then
            noffslin(i)=0
            noffstri(i)=0
	    noffsrec(i)=0
	else
	    noffslin(i)=noffslin(i-1)+nbfpsy(i-1)
	    noffstri(i)=noffstri(i-1)+nbfpsy(i-1)*(nbfpsy(i-1)+1)/2
	    noffsrec(i)=noffsrec(i-1)+nbfpsy(i-1)**2
      endif
      maxvect = maxvect + nbfpsy(i)**2
      maxocc  = maxocc  + nbfpsy(i) 
      maxdens = maxdens + nbfpsy(i)*(nbfpsy(i)+1)/2
      enddo
      norb=maxocc
c      
      locmo= 1
      lococc= locmo + maxvect
      locdens= lococc + maxocc
      loctop = locdens + maxdens
c
      if (loctop.gt.maxsize) call bummer('loctop is greater than 
     . maxsize',0,2)
c
        if(typecalc.ne.0) then
c
c reads the molecular orbital coefficients(20)      
c      
      call moread(prtab,20,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,maxvect,WORK(locmo)) 
c       
        else ! calctype.ne.0
c
        call rdlv(nrestp,maxvect,nbmt,work(locmo),'norb',.true.)     
c
c print out the MO's coefficients
               if(dbglvl.eq.1) then
      call prblks('MO coefficients',WORK(locmo),nsym,
     &  nbfpsy,nmopsy,' AO ','  mo',1,60) 
               endif
c
        endif ! calctype.ne.0    
c
          if(typecalc.ne.0) then
c      
c read the occupation numbers(40)      
c      
      call moread(prtab,40,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,maxocc,WORK(lococc)) 
      close(unit=prtab,status='keep')
c
        else ! calctype.ne.0
c
        call rdlv(nrestp,maxocc,nbmt,work(lococc),'nocc',.true.)     
c
        endif ! calctype.ne.0    
c
c      call prblks('NO coefficients',WORK(lococc),nsym,
c     &  nbfpsy,nbfpsy,' AO ','  no',1,60) 
c
c cdens transforms the occupation numbers from linear to a triangula
c    + r form

c
c      call cdens(nbfpsy,WORK(lococc),WORK(locdens),nsym,noffstri)
c
c      call plblks('MOs occupation',WORK(locdens),
c    .               nsym, nbfpsy, 'MO  ',afmt,6)
c
c
c  This subroutine transforms diagonal density to AO density
c
         call trfblock(WORK(lococc),WORK(locmo), WORK(locdens),
     .                 nsym,nbfpsy,noffsrec,noffstri,maxdens,
     .                  maxvect,maxocc)
       if(dbglvl.eq.1) then
       call plblks('AO density  ',WORK(locdens),
     .               nsym, nbfpsy, 'AO  ',afmt,6)
       endif
c               
C
         WRITE(*,'(/A/A,I8,A)')
     &      ' DALTON: user specified work memory size used,',
     &      '          -m = "',LWORK,'"'
      LMWORK=LWORK
      NBYTES = (LMWORK) * 8
      XMBYTES = NBYTES
      XMBYTES = XMBYTES / (1024*1024)
      WRITE(*,'(/A,I12,A,F8.2,A)') ' Work memory size (LMWORK) :',
     &   LMWORK,' =',XMBYTES,' megabytes.'
      IWOFF=0
C
C     Set memory traps.
C
      WORK(IWOFF+0+loctop) = WRKDLM
      WORK(IWOFF+2+LMWORK) = WRKDLM
C
c      call headwr(LUPRI,'DALTON','5.4.0.0 ') 
      CALL GNRLIN(WORK(loctop),LMWORK-loctop+1)
      
             if(ioption.eq.1) then
c      
c Beggining of integrals option (calculation of nuclear attraction i
c    + ntegrals

c taking the point charges into account)
c
      write(*,*)'**Starting calculation of Solv Mod Integrals**'
      write(*,*) '****Module DALTON-COSMO****'           
c
                if(typecalc.ne.0) then ! i.e, only for ci 
c
c First we have to read the mocoef file 
      open(fmocoef,file='mocoef',status='old')
c
      call moread(fmocoef,10,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,0,WORK) 
      
c reads the molecular orbital coefficients(20)      
c      
c
      call moread(fmocoef,20,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,maxvect,WORK(locmo)) 
c
                endif ! for typecalc
c
c Silmar - 10/09/01 - This part calculates the parameters
c necessary to read the points in blocks, in the integral
c option
c input => point: Total number of points.Calculated after reading
c the file containing the charges
c          ptspbl: points per block. Read in the chargescoord file
c output => nrbl: number of blocks
c          number of points in the last block:totalpt-(nrbl-1)*ptspbl
c
c setting points per block for testing purposes
        ptspbl = 15
c
        nrptslst=0
	nrbl=0
	nrbl=(point-1)/ptspbl+1
        nrptslst = point -(nrbl-1)*ptspbl 
c
       call wzero(maxdens,V,1)
       call wzero(maxdens,Vsolv,1)
c
c first controls whether we are in the first cycle or not      
c
c In the first cycle the nuclear attraction integrals are
c calculated considering only the atoms, without the point
c charges. Then the V matrix is filled. From the second
c cycle until the end the Vsolv matrix if filled, taking
c the point charges into account. In this case each cycle
c corresponds to a block
c
c       
       Vnenuc=0.0d0
       currpt=1
       do j=0,nrbl
        lastone=.false. 
        callswrtund=.false.
	first=(.not.(j.gt.0))	    
	  if((j*ptspbl).ge.point) then
c we are in the last block
         ptspbl=nrptslst 
          endif
c      
      call EXEHER(WORK(loctop), LMWORK-loctop+1, IWOFF, WRKDLM) 
c       
       enddo
c      
      call calcnucrep(chargept,savenucind,point,Vnenuc)
      Vnenuctotal = Vnenuc
      write(*,71) ' ** Total nuclear repulsion energy ** = ',Vnenuc 
   71 format(a40,f18.14)   
c
      call calcnucrep(chargept,0,point,Vnenuc)
c
      write(*,*)'screening nuclear repulsion energy ',Vnenuc 
      write(*,*)
       Vnenuc = Vnenuctotal - Vnenuc
      write(*,*)'Total-screening nuclear repulsion energy ',
     . Vnenuc 
c
c      call initsif(maxocc,nsym,nbfpsy,info,Vnenuc) 
c    
c Silmar -Confirmation of Vne integrals at the end      
c      
       
c       if(dbglvl.eq.1) then
       write(*,*)
c       call plblks('final S matrix for testing',S,
c     .               sym, bpsy, 'AO  ',1,1)
c       call plblks('final T matrix for testing',T,
c     .               nsym, nbfpsy, 'AO  ',1,60)
c       call plblks('final V matrix for testing',V,
c     .               sym, bpsy, 'AO  ',1,outputunit)
c       call plblks('final Vsolv matrix for testing',Vsolv,
c     .               nsym, nbfpsy, 'AO  ',1,60)
       write(*,*)
c       endif
       result=0.0d0
        
          if (typecalc.eq.0) goto  12222
c
c
c End of integrals option, in AO basis
c
c For COSMO implementation at CI level we  need to transform 
c the integrals from AO to MO basis
c Remember that in order to write the integrals in sifs format they 
c    + should

c be in triangular form!!
c     
                 do isym=1,sym 
c   Case where nbfpsy(isym)=0
                   if(nbfpsy(isym).ne.0) then
c
c For using the sifs subroutine we have to store all       
c blocks of Vrec, and for this we need an adress
c for each block of Vrec
c       
c
c first expands T,V and Vsolv in a rectangular form       
c       
c If we replace V by S here the result of trace should be 
c the number of electrons. If we replace by T should be
c the expectation value of T
c       
       
       call expnds(nbfpsy(isym),Vsolv(noffstri(isym)+1),
     .  Vsolvrec(noffsrec(isym)+1))
       call expnds(nbfpsy(isym),T(noffstri(isym)+1),
     .  Trec(noffsrec(isym)+1))
          
                if(dbglvl.eq.1) then
       write(*,*)
       write(*,*) 'block ',isym,' of ','Vsolvrec'
      
      call prblks('Vsolvrec matrix of integrals section for testing',
     .  Vsolvrec(noffsrec(isym)+1), 1,bpsy(isym),
     . bpsy(isym),'AO ',' AO',1,outputunit)
       write(*,*)
       write(*,*) 'block ',isym,' of ','Trec'
      
      call prblks('Tec matrix of integrals section for testing',
     .  Trec(noffsrec(isym)+1), 1,bpsy(isym),
     . bpsy(isym),'AO ',' AO',1,outputunit)
                endif
c
c Silmar - Transformation of T and Vsolv from a AO to a MO-basis
c      
       call mxm(Vsolvrec(noffsrec(isym)+1),nbfpsy(isym),
     .   work(locmo+noffsrec(isym)),nbfpsy(isym),
     .  VCsolv(noffsrec(isym)+1),nbfpsy(isym)) 
       call mtxm(work(locmo+noffsrec(isym)),nbfpsy(isym),
     .  VCsolv(noffsrec(isym)+1),nbfpsy(isym),
     .  Vsolvmo(noffsrec(isym)+1),nbfpsy(isym))       
c
       call mxm(Trec(noffsrec(isym)+1),nbfpsy(isym),
     .   work(locmo+noffsrec(isym)),nbfpsy(isym),
     .  TC(noffsrec(isym)+1),nbfpsy(isym)) 
       call mtxm(work(locmo+noffsrec(isym)),nbfpsy(isym),
     .  TC(noffsrec(isym)+1),nbfpsy(isym),
     .  Tmo(noffsrec(isym)+1),nbfpsy(isym))       

c This trace is to test with S and T matrices
c In this way we have the number of electrons and the expectation
c value for T, respectively
c trace performs a multiplication between a square and a linear
c vector
c
c	      
	call mtrace(Vsolvmo(noffsrec(isym)+1),nbfpsy(isym),
     .	work(lococc+noffslin(isym)), result)
c      
c       write(*,*)
c       write(*,*)'printing V in a mo basis'
c       write(*,*)'block',isym,' of ','Vmo'
c       call prblks('V in a mo basis',Vmo(noffsrec(isym)+1),1,
c     .  nbfpsy(isym),nbfpsy(isym),'MO ', ' MO',1,outputunit)
c       write(*,*)
c       write(*,*)
c       write(*,*)'printing T in a mo basis'
c       write(*,*)'block',isym,' of ','Tmo'
c       call prblks('T in a mo basis',Tmo(noffsrec(isym)+1),1,
c     .  nbfpsy(isym),nbfpsy(isym),'MO ', ' MO',1,outputunit)
c       write(*,*)
c       write(*,*)
c       write(*,*)'printing Vsolv in a mo basis'
c       write(*,*)'block',isym,' of ','Vsolv'
c       call prblks('Vsolv in a mo basis',Vsolvmo(noffsrec(isym)+1),1,
c     .  nbfpsy(isym),nbfpsy(isym),'MO ', ' MO',1,outputunit)
c       write(*,*)
c    
                   endif!endif for nbfpsy.ne.0
                 enddo
c       
c	write(*,*) 'trace=',result
c
c Silmar - printout the V in mo-basis in a standard sifs format
c       
c First we have to expand Tmo,Vmo and Vsolvmo back into the triangul
c    + ar form

c
c         write(*,*)'nsym before rectri=',nsym
c         write(*,*)'nbfpsy before rectri=',nbfpsy
c          write(*,*)'maxvect before rectri=',maxvect
c         write(*,*)'maxdens before rectri=',maxdens
c         write(*,*)
c        call wzero(maxdens,T,1)	
c	call rectri(nsym,nbfpsy,maxvect,maxdens,Tmo,noffsrec,
c     .	noffstri,T)        
c        call wzero(maxdens,V,1)	
c	call rectri(nsym,nbfpsy,maxvect,maxdens,Vmo,noffsrec,
c     .	noffstri,V)        
        call wzero(maxdens,Vsolv,1)	
        call wzero(maxdens,T,1)	
	call rectri(nsym,nbfpsy,maxvect,maxdens,Vsolvmo,noffsrec,
     .	noffstri,Vsolv)        
	call rectri(nsym,nbfpsy,maxvect,maxdens,Tmo,noffsrec,
     .	noffstri,T)        
c        call plblks('Vsolv after rectri',Vsolv,
c     .               nsym, nbfpsy, 'MO',1,outputunit)
c        call plblks('T after rectri',T,
c     .               nsym, nbfpsy, 'MO',1,outputunit)
c
12222  continue
c
       write(*,*) 'Adding T+Vsolv ...'
       write(*,*) 'maxdens ',maxdens  
c
         call daxpy_wr(maxdens,1.0d0,T,1,Vsolv,1)
         if(typecalc.ne.0) then
         close(unit=fmocoef)
                 endif
c
	     go to 777 
c
	     else!for ioption
c      
c Beggining of potential ioption(it is always accessed before the in
c    + tegrals option!)

c
      
c      write(*,*) 'Starting ',ioption,' option'
      write(*,*) '*****Module DALTON-COSMO*****'
      write(*,*)'***Electrostatic Potential Calculation***'
       loop =.true.
       first=.false.
c
c  loop for the point charges starts
c       
       currpt = 1
          do i = 1, point
       call wzero(maxvect,V,1)
c       write(*,112) 'potential at point',xpot(currpt),
c     .        ypot(currpt),zpot(currpt)
c          write(*,*)'point = ',i
c          write(*,*)
c
112    format(A,3f10.6)
c
      call EXEHER(WORK(loctop), LMWORK-loctop+1, IWOFF, WRKDLM) 
c
c
c Silmar -Confirmation of Vne integrals at the end      
c      
c       
c       write(*,*)
c       call plblks('final V matrix for testing',V,
c     .               sym, bpsy, 'AO  ',1,outputunit)
c
       result = 0.0d0
c           
c Calculates expectation value of V(potential at the points)
c The subroutine ddot_wr calculates the dot product between two
c vectors(which in our case is the expectation value)
c of the operator. We give V and the density matrix in
c this case. These matrices are both in triangular form 
c      
c
c Calculates the expectation value of V
c       write(*,*)
c       write(*,*)'Calculation of the expectation value of V'
c       call plblks(' V matrix ',V,nsym,
c     .  bpsy,'AO',1,outputunit)
      ii=noffstri(nsym)+(nbfpsy(nsym)+1)*nbfpsy(nsym)/2
      result=2.0d0*ddot_wr(ii,V,1,WORK(locdens),1)
c
c
c      # trace operation : <S> = sum_(mu,nu) D_(mu,nu) * S_(mu,nu)
c                              = sum_(x)  D_(x)*S_(x)   where x= num
c    + ber of elements

c
c	write(*,*) 'electronic contribution to Vne ',result
c	write(*,*) 'Vne total ',result+Vnenuc
c	write(*,*) 
        phi(i)=result+Vnenuc
c
c  loop for points ends
          enddo 
c  endif for option 
             endif 
  777 continue   
      call bummer('normal termination',0,3)
c
      write(*,*) '*** End of DALTON-COSMO calculation ***'
      write(*,*) 
c
c      return
      END
c
C  /* Deck bndchk */
      SUBROUTINE BNDCHK(WORK, LMWORK, IWOFF, WRKDLM, PROG)
#include <implicit.h>
c#if defined (VAR_MPI)
c#include <infmpi.h>
c      INCLUDE 'mpif.h'
c#endif
      DIMENSION WORK(LMWORK)
      CHARACTER*6 PROG
#include <priunit.h>
C
C     Check memory traps. Gives error message if any of the programs
C     have been outside the declared memory area.
C
      IF (WORK(IWOFF + 1) .NE. WRKDLM .OR.
     &    WORK(IWOFF + LMWORK + 2) .NE. WRKDLM) THEN
c         WRITE (LUPRI,'(//A,A6,A)')
c     *      ' >>> WARNING, ',PROG,' has been out of bounds.'
c         IF (WORK(IWOFF + 1) .NE. WRKDLM) WRITE (LUPRI,'(/A)')
c     *      ' >>> WORK(0) has been destroyed'
c         IF (WORK(IWOFF + LMWORK + 2) .NE. WRKDLM) WRITE (LUPRI,'(/A)')
c     *      ' >>> WORK(LMWORK+1) has been destroyed'
         CALL QUIT('WARNING, ' // PROG // ' has been out of bounds.')
c#if defined (VAR_MPI)
c         CALL MPI_ABORT(MPI_COMM_WORLD,IERR,IERR)
c#endif
      END IF
      RETURN
      END
C  /* Deck exeher */
      SUBROUTINE EXEHER(WORK, LMWORK, IWOFF, WRKDLM)
#include <implicit.h>
      DIMENSION WORK(LMWORK)
#include <priunit.h>
C
C     Run Integral section
C
C     We need to open an extra output file needed for CM and shielding
C     polarizabilities, kr and sc, oct-95
C
      OPEN (UNIT=66, FILE = 'DALTON.CM', FORM='FORMATTED')
C
c      WRITE(LUPRI, '(A/)') '    Starting in Integral Section -'
C     The dollar sign in the previous line may not be standard.
C     It causes the End of Record to be skipped.
ct     OPEN(LUCMD, FILE = 'DALTON.INP')
c       OPEN(LUCMD, FILE = 'daltcomm')
      CALL QENTER('HERMIT')
      CALL HERCTL(WORK(IWOFF+2),LMWORK)
      CALL QEXIT('HERMIT')
      CALL BNDCHK(WORK, LMWORK, IWOFF, WRKDLM, 'HERMIT')
c     CLOSE(LUCMD)
      CLOSE(24)
c      WRITE(LUPRI, '(/A)') '- End of Integral Section'
      RETURN
      END
C  /* Deck gnrlin */
      SUBROUTINE GNRLIN(WORK,LWORK)
C
C     GENERAL input
C
#include <implicit.h>
#include <mxcent.h>
#include <mxorb.h>
      DIMENSION WORK(LWORK)
#include <priunit.h>
#include <siripc.h>
#include <gnrinf.h>
#include <abainf.h>
#include <exeinf.h>
#include <huckel.h>
      PARAMETER (NDIR = 7, NTABLE = 23)
                 
C
C     Initialize /SIRIPC/
C
      INPPRC = .FALSE.
C
C     Initialize /ABAINF/
C
      MOLGRD = .FALSE.
      MOLHES = .FALSE.
      DOWALK = .FALSE.
C
C     Initialize /GNRINF/
C
      SEGBAS = .TRUE.
      WALKIN = .FALSE.
      HRINPC = .FALSE.
      SRINPC = .FALSE.
      ABINPC = .FALSE.
      RDINPC = .FALSE.
      RDMLIN = .FALSE.
      TESTIN = .FALSE.
      OPTIMI = .FALSE.
      ITERNR = 0
      ITERMX = 20
      USRIPR = .FALSE.
      RNALL  = .FALSE.
      RNHERM = .FALSE.
      RNSIRI = .FALSE.
      RNABAC = .FALSE.
      RNRESP = .FALSE.
      PARCAL = .FALSE.
      DIRCAL = .FALSE.
      RNINTS = .FALSE.
      DOCCSD = .FALSE.
      MINIMI = .FALSE.
      NEWSYM = .FALSE.
      NEWBAS = .TRUE.
      NEWPRP = .TRUE.
      PANAS  = 0.0D0
C
C     Initialize /EXEINF/
C
      FTRONV = .TRUE.
      FTWOXP = .TRUE.
      FABRHS = .TRUE.
      FTRCTL = .TRUE.
C
C     Initialize /HUCKEL/
C
      ADDSTO = .TRUE.
      HUCCNT = 2.0D0
      CALL IZ000(NHUCAO,8)
      CALL IZ000(IHUCPT,MXSHEL)


      USRIPR=.true.
      RNHERM=.true.
      return
      END
C  /* Deck exeaba */

c
c This subroutine transforms the diagonal density from MO basis to A
c    + O basis

c
        subroutine trfblock(occ,mo,dens,nsym,nbfpsy,noffsrec,noffstri,
     .     maxdens,maxmo,maxocc)
	implicit none
	integer nsym,nbfpsy(nsym),noffsrec(nsym),noffstri(nsym)
	integer maxdens,maxmo,maxocc
	real*8  occ(*),mo(*),dens(*)
	integer isym,icntdens,icntocc,icnt2,icnt3,i,mu,nu
        real*8 fac 
	 call wzero(maxdens,dens,1)

	 do isym=1,nsym
          if(nbfpsy(isym).ne.0) then
	    icntdens=noffstri(isym)
	    if (isym.eq.1) then 
	        icntocc=0
	    else
	        icntocc=icntocc+nbfpsy(isym-1)
	    endif
	    do mu = 1, nbfpsy(isym)
	      do nu = 1, mu
                if (nu.ne.mu) then
	          fac = 1.0d0
	        else
		  fac = 0.5d0
	        endif
		icntdens=icntdens+1
		 do i= 1,nbfpsy(isym)
		 icnt2=noffsrec(isym)+(i-1)*nbfpsy(isym)+mu
		 icnt3=noffsrec(isym)+(i-1)*nbfpsy(isym)+nu
         dens(icntdens)= 
     .        dens(icntdens)+ fac*mo(icnt2)*occ(icntocc+i)*mo(icnt3)
	        enddo
	      enddo
	    enddo
          endif !for nbfpsy.ne.0
      	 enddo
	 return
	 end

        subroutine mtrace( I,idim,D, result)
	implicit none
	integer idim,j
	real*8 I(idim,idim), D(idim),result

	do j=1,idim
	  result=result+I(j,j)*D(j)
	enddo
	return
	end


        subroutine initsif(nbft,nsym,nbfpsy,info,Vnenuc)
	implicit none
	integer nsym
	integer nbft,itype,lrecal,ibvtyp,ifmt,lrec,nmax,ierr
	integer nbfpsy(nsym),moints1
	character*80 title(1)
	character*4 slabel(8)
	character*8 bfnlab(255)
	integer i,ntitle,ninfo
        integer info(*),nenrgy,nmap
	integer lrec2,nmax2
	integer ietype(1)
	real*8 energy(1),Vnenuc
         itype=1
	 lrecal=4096
	 ibvtyp=0
	 call sifcfg(itype,lrecal,nbft,ibvtyp,ifmt,lrec,nmax,ierr)
	 itype=2
	 lrecal=4096
	 ibvtyp=0
	 call sifcfg(itype,lrecal,nbft,ibvtyp,ifmt,lrec2,nmax2,ierr)
	 if (ierr.ne.0) 
     .    call bummer('sifcfg failed,ierr=',ierr,2)

	 write(*,*) 'sifcfg returned: ifmt=',ifmt,
     .               'lrec=',lrec,'nmax=',nmax


         moints1=69
	 open(unit=moints1,file='moints1',form='unformatted',
     .        access='sequential')
	 ntitle=1
	 title(1)=' solvent modified integrals  '
	 ninfo=6
	 info(1)=1
	 info(2)=lrec
	 info(3)=nmax
	 info(4)=lrec2
	 info(5)=nmax2
	 info(6)=ifmt 
	 nenrgy=1
	 ietype(1)=-1
	 energy(1)=Vnenuc
	 nmap=0
	 do i=1,nsym
	  write(slabel(i),'(a3,i1)') 'SYM',i
	 enddo
c        creates: slabel(1) = 'SYM1'
         do i=1,nbft
	   if (i.lt.10) then
	   write(bfnlab(i),'(a7,i1)') 'BFNLB__',i
	   elseif (i.lt.100) then
	   write(bfnlab(i),'(a6,i2)') 'BFNLB_',i
	   else
	   write(bfnlab(i),'(a5,i3)') 'BFNLB',i
	   endif
	 enddo
c        creates: bfnlab(1) = 'BFNLB__1'
c        creates: bfnlab(10) = 'BFNLB_10'
c        creates: bfnlab(100) = 'BFNLB100'


         call sifwh ( moints1,  ntitle,  nsym,    nbft,
     .                ninfo,   nenrgy,  nmap,    title,
     .                nbfpsy,   slabel,  info,    bfnlab,
     .                ietype ,  energy,     0,      0,
     .                ierr )

         if (ierr.ne.0) 
     .    call bummer('sifwh failed,ierr=',ierr,2)

         return
	 end


         subroutine wrtint_cosmo(T,V,Vsolv,sym,bpsy,
     .	 buffer,values,labels,info)
	 implicit none 
	 integer info(5)
	 integer itypea,itypeb,i,ifmt 
	 integer   sym,bpsy(sym),values(*),
     .             labels(2,*)
	 integer ierr,cnt
	 double precision T(*),V(*),buffer(*)
	 double precision Vsolv(*)
	 integer   moints1,lstflg,mapout(255)
	 integer   nrec,numtot,kntout(36),kntin(36)
	 double precision fcore,small

	 moints1=69

          do i=1,255
	    mapout(i)=i
	  enddo 
	  
	  call izero_wr(36,kntin,1)
	  cnt=0
	  do i=1,sym
	   kntin(cnt+i)=1
	   cnt=cnt+i
	  enddo 

c
c         first write Tmo
c
          lstflg=1
	  itypea=0
	  itypeb=1
	  ifmt=0
	  fcore=0.0d0 
	  small=1.d-12
	  nrec=0

         call sifw1x(
     .           moints1,  info,   lstflg,  itypea,
     .           itypeb,   mapout,  T,
     .            sym,    bpsy,   0,  kntin,
     .            buffer,  values,  labels,  fcore,
     .            small,   kntout,  numtot,  nrec,
     .             ierr )

          if (ierr.ne.0) 
     .   call bummer('sifw1x (Tmo) failed',ierr,2)
          write(*,*) numtot,' Tmo integrals written'


c
c         write Vmo
c

          lstflg=1
	  itypea=0
	  itypeb=2
	  ifmt=0

         call sifw1x(
     .           moints1,  info,   lstflg,  itypea,
     .           itypeb,   mapout,  V,
     .            sym,    bpsy,   0,  kntin,
     .            buffer,  values,  labels,  fcore,
     .            small,   kntout,  numtot,  nrec,
     .             ierr )

          if (ierr.ne.0) 
     .   call bummer('sifw1x (t) failed',ierr,2)
          write(*,*) numtot,' Vmo integrals written'


c
c         write Vsolvmo, last integral set
c


          lstflg=2
	  itypea=0
	  itypeb=2
	  ifmt=0

         call sifw1x(
     .           moints1,  info,   lstflg,  itypea,
     .           itypeb,   mapout,  Vsolv,
     .            sym,    bpsy,   0,  kntin,
     .            buffer,  values,  labels,  fcore,
     .            small,   kntout,  numtot,  nrec,
     .             ierr )

          if (ierr.ne.0) 
     .   call bummer('sifw1x (v) failed',ierr,2)
          write(*,*) numtot,' Vsolv integrals written'

         close(moints1)
	 return
	 end
c
c This subroutine expands a rectangular matrix back into
c a triangular form 

         subroutine rectri(nsym,nbfpsy,maxvect,maxdens,
     .	 Vmo,noffsrec,noffstri,V)
         implicit none
         integer k,l
	 integer maxvect,maxdens,icnttri,nsym,nbfpsy(8)
	 integer i,noffsrec(8),noffstri(8)
	 double precision Vmo(maxvect),V(maxdens)
         icnttri=0
         i=0
         k=0
	 l=0
	 do i=1,nsym
	        if(nbfpsy(i).ne.0) then 
           do k=1,nbfpsy(i) 
              do l=1,k
	             
         icnttri=icnttri+1
	 
         V(icnttri)=Vmo(nbfpsy(i)*(l-1)+k+
     . 	  noffsrec(i))
              enddo
	   enddo
                endif
	 enddo
	 return
	 end
         
       subroutine calcnucrep(chargept,Natoms,Npoints,Vnenuc)
       implicit none
       
       integer i,j,mulk,ll,l,m,ii,hkab,MAXREPrep 
       integer ISTBNUrep(3030),kb,n,Natoms,Npoints

       real*8 dp5,distrep,dsrep,d0,ptrep(0:7)
       real*8 chargept(*),Vnenuc
       integer maxpoints
       parameter(maxpoints=2000)
c
#include <Vne2.h>
#include <nucrep.h>
#include <ibtfun.h>              
        
      	ptrep(0)=  1.0d0
        ptrep(1)= -1.0d0
        ptrep(2)= -1.0d0
        ptrep(3)=  1.0d0
        ptrep(4)= -1.0d0
        ptrep(5)=  1.0d0
        ptrep(6)=  1.0d0
        ptrep(7)= -1.0d0
        
	MAXREPrep = 2**nsymoprep - 1

      II = 1
      DO 1000 I = 1, savenontyp+Npoints
         DO 1100 J = 1, NONTrep(I)
            MULK = 0
            LL = 1
            DO 1400 L = 1, NSYMOPrep
                  IF(IBTAND(LL,ISYMAXrep(1,1)) .NE. 0) THEN
                     IF(ABS(xpot(II)).GE.1.0D-06) GOTO 1400
                  endif
                  IF(IBTAND(LL,ISYMAXrep(2,1)) .NE. 0) THEN
                     IF(ABS(ypot(II)).GE.1.0D-06) GOTO 1400
	          endif
                  IF(IBTAND(LL,ISYMAXrep(3,1)) .NE. 0) THEN
                     IF(ABS(zpot(II)).GE.1.0D-06) GOTO 1400
                  END IF
	       MULK = MULK + LL
 1400        LL = 2*LL
            ISTBNUrep(II) = MULK
            II = II + 1
 1100     CONTINUE
 1000  CONTINUE


C     Nuclear repulsion energy
C     ========================
C
      d0 = 0.0d0 
      
      Vnenuc = d0
      
      dp5=0.5d0
      
c      write(*,*)'savenucind,point',savenucind,point
      
  
      DO 3000 N = 1, Natoms+Npoints
 3001 format('n=',i4,'charge=',f6.3,'xyz=',3f10.6)
         DO 3100 M = N, Natoms+Npoints

            DO 3200 KB = 0, MAXREPrep
 3002 format('n=',i3,'m=',i3,'kb=',i1,'istbnurep=',i10)
            IF (IBTAND(KB,ISTBNUrep(M)) .EQ. 0) THEN
               IF (M.EQ.N .AND. KB.EQ.0) GO TO 3200
           DSrep = (xpot(N)-xpot(M)*PTrep(IBTAND(ISYMAXrep(1,1),KB)))**2
     &        + (ypot(N)-ypot(M)*PTrep(IBTAND(ISYMAXrep(2,1),KB)))**2
     &        + (zpot(N)-zpot(M)*PTrep(IBTAND(ISYMAXrep(3,1),KB)))**2
               DISTrep = SQRT(DSrep)
        IF (DISTrep.LT.0.1.AND.CHARGEpt(M)*CHARGEpt(N).NE.D0) then
      write(*,*) 'Warning! Large contrib. to nucrep.The distance is ',
     . distrep
c   stop 9000
	endif 
               IF (IBTAND(KB,ISTBNUrep(N)) .EQ. 0) THEN
                 IF((ABS(CHARGEpt(M)) .gT. 100.0D0) .AND.
     &               (ABS(CHARGEpt(N)) .GT. 100.0D0)) 
     .   call bummer('Large charges. Input error!!!',0,2)
                  IF (DISTrep.GT.D0) THEN

                    HKAB  = FMULTrep(IBTAND(ISTBNUrep(M),ISTBNUrep(N)))
                     IF (M .EQ. N) HKAB = DP5*HKAB
c
                  Vnenuc = Vnenuc + CHARGEpt(M)*CHARGEpt(N)*HKAB/DISTrep
c
                  END IF
               END IF
            END IF
 3200       CONTINUE
 3100    CONTINUE
 3000 CONTINUE
c
 5000 continue
c
      return
      end
         subroutine cosmoinitial(mtype,natoms,xyz,symgrp,nsym)
            implicit none
      integer maxat,natoms,dbglvl
      parameter(maxat=350)
      integer mtype(maxat),nsym,i,j
      real*8 xyz(3,maxat)
      character*3 symgrp
       include 'cosmovar.inc'
c
       natoms=1
       dbglvl = 0
       open(77,file='geom',status='old')
444    read(77,111,end=999) mtype(natoms),xyz(1,natoms)
     . ,xyz(2,natoms),xyz(3,natoms)
       natoms=natoms+1
       go to 444
999    continue
       close(unit=77,status='keep')
       natoms=natoms-1
111    format(6x,i2,2x,3f14.8)
       open(78,file='infofl',status='old')
       read(78,112) symgrp
        if(symgrp.eq.'c1') then
           nsym=0
        else if(symgrp.eq.'cs ') then
           nsym=1
        else if(symgrp.eq.'ci ') then
           nsym=2
        else if(symgrp.eq.'c2 ') then
           nsym=3
        else if(symgrp.eq.'d2 ') then
           nsym=4
        else if(symgrp.eq.'c2v') then
           nsym=5
        else if(symgrp.eq.'c2h') then
           nsym=6
        else if(symgrp.eq.'d2h') then
           nsym=7
        else
         call bummer('wrong sym. group',0,2)
        endif
112     format(a3)
c
c opening files needed for cosmo module
       open(icosin,file='cosmo.inp',form='formatted'
     ., err=333)
       open(icosa2,file='a2mat.tmp',form='unformatted'
     ., status='unknown',err=333)
       open(icosa3,file='a3mat.tmp',form='unformatted'
     ., status='unknown',err=333)
       open(icosa,file='amat',form='unformatted'
     ., status='unknown',err=333)
       open(icosco,file='cosurf'
     ., status='unknown',err=333)
       open(icossu,file='sude'
     ., status='unknown',err=333)
       open(icosout,file='out.cosmo',form='formatted'
     ., status='unknown',err=333)
       open(icolum,file='Col_cosmo',form='formatted'
     ., status='new',err=333)
c
31    go to 334
c
333   call bummer('error while opening cosmo files',0,2)
334   continue
c initialization for cosmo module
          if(dbglvl.eq.1) then
      write(*,*)
      write(*,*)'natoms,nsym = ',natoms,nsym
      write(*,*)'mtype = '
      write(*,*)(mtype(i),i=1,natoms)
      write(*,*)
      write(*,*)'xyz = '
      write(*,*)
      write(*,*)((xyz(i,j),i=1,3),j=1,natoms)
      write(*,*)
          endif !dbglvl
         return
         end

      subroutine rdlv(i,n,na,a,alab,qfskip)
c
c  read the mcscf resart file
c  search unit i for label alab and read the corresponding vector.
c
c  i   = unit number.
c  n   = expected vector length.
c  na  = actual vector length.  na=-1 for unsatisfied search.
c  a(*)= if(na.le.n) the vector is returned.
c  alab= character label used to locate the correct record.
c  qfskip= .true. if first record should be skipped.
c
c  written by ron shepard.
c
c
      implicit none
C
C====>Begin Module RDLV                   File rdlv.f
C---->Makedcls Options: All variables
C
C     Parameter variables
C
C
C     Argument variables
C
      CHARACTER*(*)       alab
C
      INTEGER             i,           n,           na
C
      LOGICAL             qfskip
C
      REAL*8              a(n)
C
C     Local variables
C
      CHARACTER*8         label
C
C====>End Module   RDLV                   File rdlv.f

c  search for the correct label.
      na=0
      rewind i
      if(qfskip)read(i)
   10 read(i,end=20)label,na
      if(alab.ne.label)then
          read(i)
          go to 10
          endif
c
c  ...correct label.  check length.
c
      if(na.ne.n)write(*,6010)alab,na,n
 6010 format(/' rdlv: alab=',a,' na,n=',2i10)
c
      if(na.le.n)call seqrbf(i,a,na)
      return
c
c  ...unsatisfied search for alab.
   20 write(*,6020)alab
 6020 format(/' rdlv: record not found. alab=',a)
      na=-1
      return
      end

