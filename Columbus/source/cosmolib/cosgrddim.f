!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine cosgrddim(icosout,icossu,icolum,lcavity,nps,fepsi,
     &                      nip)
c----------------------------------------------------------------------
c     reads nps, lcavity, and fepsi from out.cosmo
c     and nip from sude
c----------------------------------------------------------------------

      implicit real*8 (a-h,o-z)

      include 'cosinc.h'

      character*80 string

c----------------------------------------------------------------------
c READ FROM out.cosmo
      nps=-1
      fepsi=-1.d0
      lcavity=-1
      nip=-1

      rewind(icosout)

 10   call readinp(icosout,string,80)

      i=0
      i=index(string,' ')
c     string does not contain blancs at pos 1 unless eof reached
      if(i .gt. 1) then
c     read nps
         i=index(string,'nps=')
         if(i .gt. 0) then
            read (string(i+4:),*,END=1000,ERR=1000) nps
            goto 10
         endif
c     read fepsi
         i=index(string,'fepsi=')
         if(i .gt. 0) then
            read (string(i+6:),*,END=1000,ERR=1000) fepsi
         goto 10
         endif
c     read cavity
         i=index(string,'cavity')
         if(i .gt. 0) then
            i=index(string,'open')
            if(i .gt. 0) lcavity=0
            i=index(string,'closed')
            if(i .gt. 0) lcavity=1
         goto 10
         endif

         goto 10
      endif
c----------------------------------------------------------------------
c READ FROM sude
      if (lcavity.eq.1) then
         rewind(icossu)
         read(icossu,*,ERR=2000,END=2000) nip
      endif
c----------------------------------------------------------------------
c CHECK VALUES

      if(nps .lt. 1) then
         write(icolum,'(2a)')'ERROR cosgrdinit: Cannot read proper',
     &                       ' nps value from out.cosmo'
         stop
      endif
      if(lcavity .lt. 0) then
         write(icolum,'(2a)')'ERROR cosgrdinit: Cannot read proper',
     &                       ' lcavity value from out.cosmo'
         stop
      endif
      if(fepsi .lt. 0.d0) then
         write(icolum,'(2a)')'ERROR cosgrdinit: Cannot read proper',
     &                       ' fepsi value from out.cosmo'
         stop
      endif
      if(nip .lt. -1) then
         write(icolum,'(2a)')'ERROR cosgrdinit: Cannot read proper',
     &                       ' nip value from sude'
         stop
      endif

      return
 1000 write(icolum,'(2a)')'ERROR cosgrddim: Problems while ',
     &                     'reading out.cosmo'
      stop
 2000 write(icolum,'(2a)')'ERROR cosgrddim: Problems while ',
     &                     'reading sude'

      stop
      end

