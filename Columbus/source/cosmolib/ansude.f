!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine ansude(ra,rb,d,rs,ara,arb,aar,abr,arad,arbd)

c   This progrom calculates the area of two intersecting spheres
c   with radii ra and rb at a distance d and a solvent probe
c   radius rs. The two areas are calculated separately (ara,arb).
c   For both areas analytic derivatives with respect to the distance
c   d are calculated (arad,arbd).    (written by Andreas Klamt, 9/9/96)

      implicit real*8 (a-h,o-z)

      pi=2.d0*asin(1.d0)
      qa=ra+rs
      qb=rb+rs
      ca=(qa**2+d**2-qb**2)/(2.d0*qa*d)
      cb=(qb**2+d**2-qa**2)/(2.d0*qb*d)
      sa=sqrt(1.d0-ca*ca)
      sb=sqrt(1.d0-cb*cb)
      ta=pi*sa
      tb=pi*sb
      fza=(1.d0-cos(ta))/2
      fzb=(1.d0-cos(tb))/2
      if (sa.lt.0 .or. sb.lt.0) fza=1.d0
      if (sa.lt.0 .or. sb.lt.0) fzb=1.d0
      xa=fzb**1*rs*(ca+cb)
      xb=fza**1*rs*(ca+cb)
      ya=ra*sa-fzb*rb*sb
      yb=rb*sb-fza*ra*sa
      za=sqrt(xa*xa+ya*ya)
      zb=sqrt(xb*xb+yb*yb)
      ara=pi*ra*(2.d0*(1.d0+ca)*ra+sa*za)
      arb=pi*rb*(2.d0*(1.d0+cb)*rb+sb*zb)
      aar=pi*ra*(sa*za)
      abr=pi*rb*(sb*zb)

c now derivatives

      cad=(qb**2+d**2-qa**2)/(2.d0*qa*d*d)
      cbd=(qa**2+d**2-qb**2)/(2.d0*qb*d*d)
      sad=-ca*cad/sa
      sbd=-cb*cbd/sb
      tad=pi*sad
      tbd=pi*sbd
      fzad=sin(ta)*.5d0
      fzbd=sin(tb)*.5d0
      if (sa.lt.0 .or. sb.lt.0) fzad=0.d0
      if (sa.lt.0 .or. sb.lt.0) fzbd=0.d0
      xad=rs*((ca+cb)*fzbd*tbd+fzb*(cad+cbd))
      xbd=rs*((ca+cb)*fzad*tad+fza*(cad+cbd))
      yad=ra*sad-fzbd*tbd*rb*sb-fzb*rb*sbd
      ybd=rb*sbd-fzad*tad*ra*sa-fza*ra*sad
      zad=(xa*xad+ya*yad)/za
      zbd=(xb*xbd+yb*ybd)/zb
      arad=pi*ra*(sad*za+sa*zad+2.d0*ra*cad)
      arbd=pi*rb*(sbd*zb+sb*zbd+2.d0*rb*cbd)

      return
      end
