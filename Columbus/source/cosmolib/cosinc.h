c-----------------------------------------------------------------------
c Parameter for static memory allocation of the cosmo module
c-----------------------------------------------------------------------
      integer maxat, maxnps, maxnspa, maxnsph, maxnppa
      character*80 version
c version of cosmo module
      parameter (version='cosmo columbus 1.0, cavity: 1.0')

c maximum number of atoms
      parameter (maxat = 350)

c maximum number of segments; max. value for both
c nps and npspher
      parameter (maxnps = 2000)

c maximum number of segments per non hydrogen atom (nspa)
      parameter (maxnspa = 400)

c maximum number of segments per hydrogen atom (nsph)
      parameter (maxnsph = 70)

c maximum number of basis grid points per atom
      parameter (maxnppa = 2000)

c maximum number of replicates for symmetry operations
      parameter (maxrepl = 8)

