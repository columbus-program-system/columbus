!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       subroutine constanten(coord, mtype, natoms,
     &     cosurf, iatsp, dirsm, dirsmh,dirvec, nar, ar,
     &     dirtm, xsp, a1mat, isym, nps, npspher)
c
c ---------------------------------------------------------------------
c     this routine constructs or updates the solvent-accessible
c     surface (sas)
c ---------------------------------------------------------------------
c     cavity: final cosmologic

      implicit real*8 (a-h,o-z)

c  update for surface closure
      include 'cosinc.h'

      parameter (nsring=20)
      dimension xd(3),rvx(3),rvy(3),xja(3),xjb(3),xjc(3),xta(3,3)
      dimension fz(2),yx(2),trp(3),
     &          svx(3),svy(3),tvx(3),tvy(3),phiset(50),iset(50),
     &          cc(3),ss(3),em(3),ee(3,3)
      dimension  xx(3),xa(3),xi(3),xj(3),yy(3)
c
c     arrays for coordinates of replicated points
c     srepx,... replicated segment
c     arepx,... replicated atom
c     brepx,... replicated basis grid point
c
      dimension srepx(maxrepl), srepy(maxrepl), srepz(maxrepl)
      dimension arepx(maxrepl), arepy(maxrepl), arepz(maxrepl)
      dimension brepx(maxrepl), brepy(maxrepl), brepz(maxrepl)


      dimension sude(4,6*maxat),isude(4,6*maxat),
     &          nipa(4*maxat),lipa(4*12*maxat)

c-------------------COMMON BLOCKS---------------------------------------
c
c I/O units
      include 'cosmovar.inc'
      include 'coss.inc'
      save /coss/


      logical valid, redseg(maxnps)

      character*8  mtype(maxat)
      dimension    coord(3,natoms)
      dimension    cosurf(3,2*maxnps), iatsp(2*maxnps)
      dimension    dirsm(3,nspa), dirsmh(3,nsph), dirvec(3,nppa)
      dimension    nar(2*maxnps), ar(2*maxnps), dirtm(3,nppa)
      dimension    xsp(3,2*maxnps)

c vorher dynamisch
      dimension   nset(maxat*maxnppa),nsetf(2*maxnps),xyz(3*maxat)
      logical*1   din(maxnppa+1)
      dimension   a23mat(maxnps+maxnps)
c--------
      dimension    a1mat(maxnps*(maxnps+1)/2)

      data pi/3.14159265358979323844d0/, four/4.d0/

      thrsh=1.0e-15
      fpinppa = four*pi/dble(nppa)
      fdiagr=2.1D0*sqrt(pi)
      sqpi=sqrt(pi)

c save coordinates before randomisation
      k=1
      do i=1,natoms
        do j=1,3
          xyz(k)=coord(j,i)
          k=k+1
        enddo
      enddo
c make coordinates a bit oblique for in order to get only triple points
c     k = 4711
c     do 5 i=1,natoms
c         do 5 j=1,3
c   5 coord(j,i)=coord(j,i)+ran(k)*1.0e-5
c
cas   more stable version from A. Klamt
      do 5 i=1,natoms
        do 5 j=1,3
    5   coord(j,i)=coord(j,i)+cos((phsran+5.0d0*i+j)**1.1d0)*ampran
c      write(icolum,'(a)') ' COSMO segments per atom:'
c      write(icolum,'(a,i4)') '   hydrogen:   ',nsph
c      write(icolum,'(a,i4)') '   other atoms:',nspa
c
      sdis=0.d0
      fdiag=1.05d0*sqrt(dble(nppa))
      inset=1
      ilipa=0
      nps = 0
c     --- initialise total surface area and volume
      area=0.d0
      volume=0.d0
      rmean=0.d0
c     --- loop over all atoms
      do 340 i=1,natoms
          npsoff=nps
          nipi=0
          ds=sqrt(4.d0/nspa)
          if (mtype(i)(1:2) .eq. 'h ') ds=2.0d0*ds
          c2ds=cos(2.d0*ds)
          r=srad(i)
          ri=r-rsolv
          fvol=4*pi*ri**2/nppa
          do 20 ix=1,3
   20     xa(ix)=coord(ix,i)
          nps0=nps+1

c search for 3  nearest neighbor atoms
          dist1=1.d20
          dist2=1.d20
          dist3=1.d20
          nn1=0
          nn2=0
          nn3=0
cjwa
          do 70 j=1,natoms
              if (j.eq. i) go to 70
              dist=0.d0
              do 60 ix=1,3
   60         dist=dist+(xa(ix)-coord(ix,j))**2
              if(dist.lt.(r+srad(j))**2) then
                  ilipa=ilipa+1
                  nipi=nipi+1
c                 lipa(ilipa) points on neighbour atom ilipa
                  lipa(ilipa)=j
              end if
              if ((dist+0.05d0-dist3).lt.thrsh) then
                  dist3=dist
                  nn3=j
              end if
              if ((dist3+0.05d0-dist2).lt.thrsh) then
                  dist=dist2
                  dist2=dist3
                  dist3=dist
                  nn3=nn2
                  nn2=j
              end if
              if ((dist2+0.05d0-dist1).lt.thrsh) then
                  dist=dist1
                  dist1=dist2
                  dist2=dist
                  nn2=nn1
                  nn1=j
              end if
   70     continue
c         nipa(i) counts neighbour atoms of atom i
          nipa(i)=nipi
          nn(1,i)=nn1
          nn(2,i)=nn2
          nn(3,i)=nn3
c ---------------------------------------------------------------------
c        build new transformation matrix
c
c        nn1,nn2,nn3 are either atomic indices or 0.
c
c        if they are indices, the direction vectors on tm are built
c        by having the first vector point towards the atom nn1,
c        the second vector lying in the plane of atoms nn1,nn2 and
c        the central atom, and the third vector being the cross
c        product (so nn3 is irrelevant, actually).
c
c        if they are 0, defaults are assumed. note that the default
c        for the second direction is potentially suicidal since if
c        the first direction is 001 (ie nn1.ne.0) but nn2.eq.0, the
c        second direction is ill-defined. enjoy
c ---------------------------------------------------------------------

c         --- the first guy
          if (nn1 .eq. 0) then
              tm(1,1,i)=1.d0
              tm(1,2,i)=0.d0
              tm(1,3,i)=0.d0
          else
              dist1=0.d0
              do 80 ix=1,3
   80         dist1=dist1+(xa(ix)-coord(ix,nn1))**2
              dist=1.0d0/sqrt(dist1)
              tm(1,1,i)=(coord(1,nn1)-xa(1))*dist
              tm(1,2,i)=(coord(2,nn1)-xa(2))*dist
              tm(1,3,i)=(coord(3,nn1)-xa(3))*dist
          end if
c         --- the second guy
   90     if (nn2 .eq. 0) then
              dist=sqrt(9.d0*TM(1,2,I)**2+4.d0*tm(1,1,i)**2+
     &                  tm(1,3,i)**2)
              xx(1)=3.d0*TM(1,2,I)/dist
              xx(2)=TM(1,3,I)/dist
              xx(3)=2.d0*TM(1,1,I)/dist
          else
              dist2=0.d0
              do 100 ix=1,3
  100         dist2=dist2+(xa(ix)-coord(ix,nn2))**2
              dist=1.0d0/sqrt(dist2)
              xx(1)=(coord(1,nn2)-xa(1))*dist
              xx(2)=(coord(2,nn2)-xa(2))*dist
              xx(3)=(coord(3,nn2)-xa(3))*dist
          end if
          sp=xx(1)*tm(1,1,i)+xx(2)*tm(1,2,i)+xx(3)*tm(1,3,i)
          if ((sp*sp- 0.99d0).gt.thrsh) then
              nn2=nn3
              nn3=0
              dist2=dist3
              go to 90
          end if
          sininv=1.d0/sqrt(1.d0-sp*sp)
          tm(2,1,i)=(xx(1)-sp*tm(1,1,i))*sininv
          tm(2,2,i)=(xx(2)-sp*tm(1,2,i))*sininv
          tm(2,3,i)=(xx(3)-sp*tm(1,3,i))*sininv
c         --- the third guy is the cross product of the first two.
          tm(3,1,i)=tm(1,2,i)*tm(2,3,i)-tm(2,2,i)*tm(1,3,i)
          tm(3,2,i)=tm(1,3,i)*tm(2,1,i)-tm(2,3,i)*tm(1,1,i)
          tm(3,3,i)=tm(1,1,i)*tm(2,2,i)-tm(2,1,i)*tm(1,2,i)

c ---------------------------------------------------------------------
c         the atomic transformation matrix is now applied to the
c         vectors of the basic grid.
c ---------------------------------------------------------------------
          do 110 j=1,nppa
              xx(1)=dirvec(1,j)
              xx(2)=dirvec(2,j)
              xx(3)=dirvec(3,j)
              do 110 ix=1,3
                  x=xx(1)*tm(1,ix,i)+xx(2)*tm(2,ix,i)+xx(3)*tm(3,ix,i)
                  dirtm(ix,j)=x
  110     continue

c ---------------------------------------------------------------------
c         find the points of the basic grid on the sas
c         xa = coordinates of current atom
c         r  = radius of sphere around atom
c ---------------------------------------------------------------------
          narea=0
          do 160 j = 1,nppa
              din(j)=.false.
              do 130 ix=1,3
                  xx(ix) = xa(ix) + dirtm(ix,j)* r
  130         continue
c             --- only use points which do not lie inside another atom
c             --- mark those by setting din = .true.
c             --- we need only try those atoms intersecting atom i
              do 150 ik=ilipa-nipa(i)+1,ilipa
                  k = lipa(ik)
                  dist=0.0d0
                  do 140 ix=1,3
                      dist = dist + (xx(ix) - coord(ix,k))**2
  140             continue
                  dist=sqrt(dist)-srad(k)
                  if (dist.lt.0.0d0) go to 160
  150         continue
              narea=narea+1
              volume=volume+fvol*
     &          (dirtm(1,j)*xa(1)+dirtm(2,j)*xa(2)+dirtm(3,j)*xa(3)+ri)
              din(j)=.true.
  160     continue

c         --- narea is the # of basic grid points that survived the
c         --- cruel elimination process.
          if(narea.eq.0) goto 340

c         --- update the area. ri != r (pumpup postprocess)
          area=area+narea*ri*ri

c         --- switch between hydrogen and others.
          if (mtype(i)(1:2) .eq. 'h ') then
              do j=1,nsph
                  nps=nps+1
                  if (nps .gt. maxnps) then
                     write(icolum,'(2a)') 'cosmo consts: nps is',
     &                     ' greater than maxnps - use smaller nspa'
                     stop
                  endif
                  iatsp(nps)=i
                  xx(1)=dirsmh(1,j)
                  xx(2)=dirsmh(2,j)
                  xx(3)=dirsmh(3,j)
                  cosurf(1,nps)=
     &              xx(1)*tm(1,1,i)+xx(2)*tm(2,1,i)+xx(3)*tm(3,1,i)
                  cosurf(2,nps)=
     &              xx(1)*tm(1,2,i)+xx(2)*tm(2,2,i)+xx(3)*tm(3,2,i)
                  cosurf(3,nps)=
     &              xx(1)*tm(1,3,i)+xx(2)*tm(2,3,i)+xx(3)*tm(3,3,i)
              enddo
          else
              do j=1,nspa
                  nps=nps+1
                  if (nps .gt. maxnps) then
                     write(icolum,'(2a)') 'cosmo consts: nps is',
     &                     ' greater than maxnps - use smaller nspa'
                     stop
                  endif
                  iatsp(nps)=i
                  xx(1)=dirsm(1,j)
                  xx(2)=dirsm(2,j)
                  xx(3)=dirsm(3,j)
                  cosurf(1,nps)=
     &              xx(1)*tm(1,1,i)+xx(2)*tm(2,1,i)+xx(3)*tm(3,1,i)
                  cosurf(2,nps)=
     &              xx(1)*tm(1,2,i)+xx(2)*tm(2,2,i)+xx(3)*tm(3,2,i)
                  cosurf(3,nps)=
     &              xx(1)*tm(1,3,i)+xx(2)*tm(2,3,i)+xx(3)*tm(3,3,i)
              enddo
          endif

  200     sdis0=sdis

          do 210 ips=nps0,nps
              nar(ips)=0
              xsp(1,ips)=0.d0
              xsp(2,ips)=0.d0
              xsp(3,ips)=0.d0
  210     continue

c ---------------------------------------------------------------------
c         every segment comprises several basic grid points. the
c         location of the segment is defined as the center of these
c         basic grid points (projected back onto the surface).
c
c         the # of basic grid points for a given segment may change
c         with geometry, resulting in 'jumps' in the segment location.
c         this is a possible problem when comparing analytical against
c         finite difference gradients (cmk).
c
c         the basic grid points enable a more accurate calculation
c         of segment-segment interactions, thereby approximating the
c         surface integrals (see eq.(7) in JCSPT2,799(1993))
c ---------------------------------------------------------------------
          do 250 j=1,nppa
              if (.not. din(j)) go to 250
              spm=-1.0001d0
              x1=dirtm(1,j)
              x2=dirtm(2,j)
              x3=dirtm(3,j)
              do 220 ips=nps0,nps
                  sp=x1*cosurf(1,ips)+x2*cosurf(2,ips)+x3*cosurf(3,ips)
                  if ((sp-spm) .lt.thrsh) go to 220
                  spm=sp
                  ipm=ips
  220         continue
              if ((spm-c2ds).lt.thrsh) then
                  nps=nps+1
                  do 230 ix=1,3
  230             cosurf(ix,nps)=dirtm(ix,j)
                  iatsp(nps)=i
                  go to 200
              end if
              nar(ipm)=nar(ipm)+1
              do 240 ix=1,3
  240         xsp(ix,ipm)=xsp(ix,ipm)+dirtm(ix,j)
  250     continue

          sdis=0.d0
          ips=nps0-1

  260     ips=ips+1
c ---------------------------------------------------------------------
c         if a segment has no basic grid points contributing to it,
c         compress data
c ---------------------------------------------------------------------

  352     if(nar(ips).eq.0)then
              nps=nps-1
              if(nps.lt.ips) goto 200
              do 369 jps=ips,nps
                  nar(jps)=nar(jps+1)
                  xsp(1,jps)=xsp(1,jps+1)
                  xsp(2,jps)=xsp(2,jps+1)
  369             xsp(3,jps)=xsp(3,jps+1)
              goto 352
          endif

          dist=0.d0
          do 280 ix=1,3
              x=xsp(ix,ips)
              dist=dist+x*x
  280     continue
          sdis=sdis+dist
          dist=1.d0/sqrt(dist)

c         --- final result : segment = average of basic grid points.

          do 290 ix=1,3
  290         cosurf(ix,ips)=xsp(ix,ips)*dist

          if(ips.lt.nps) goto 260

          if (abs(sdis-sdis0) .gt. 1.d-5) go to 200

c         --- final assignments of :
c            absolute segment coordinates (xsp)
c            # of basic grid points per segment (nar)
c            offsets of segments wrt nar (nsetf)
c            pointer from all basic grids to generic basic grid (nset)
c            [NOTE : dirvec will be used later on, not dirtm]


          do 310 ips=nps0,nps
              nsetf(ips)=inset
              inset=inset+nar(ips)
              nar(ips)=0
              do 300 ix=1,3
  300         xsp(ix,ips)=xa(ix)+cosurf(ix,ips)*ri
  310     continue

          do 330 j=1,nppa
              if (.not. din(j)) go to 330
              spm=-1.0001d0
              x1=dirtm(1,j)
              x2=dirtm(2,j)
              x3=dirtm(3,j)
              do 320 ips=nps0,nps
                  sp=x1*cosurf(1,ips)+x2*cosurf(2,ips)+x3*cosurf(3,ips)
                  if ((sp-spm) .lt.thrsh) go to 320
                  spm=sp
                  ipm=ips
  320         continue
              if ((spm-c2ds).lt.thrsh) go to 330
              nara=nar(ipm)
              nset(nsetf(ipm)+nara)=j
              nar(ipm)=nara+1
              ar(ipm) = fpinppa*ri*ri*nar(ipm)
  330     continue
          npsat=nps-npsoff
          rmean=rmean+npsat*ri
  340 continue
c     --- end loop over all atoms

c     --- some 4*PI multiplication to get the actual area
      area=area*fpinppa

c     --- calculate disex2
      rmean=rmean/nps
      disex2=4*rmean*rmean*disex*disex
c
c surface closure
      npspher = nps
      if (lcavity.eq.0) goto 3333
      do 2900 i=1,natoms
2900  din(i)=.true.
      do 2901 j=1,nps
2901  din(iatsp(j))=.false.
      nip=0

C generation of segments along the intersection rings
      ilipa=0
      do 3800 ia=1,natoms-1
        if(din(ia)) go to 3800
        ra=srad(ia)
        do 3001 ix=1,3
3001    xta(ix,1)=coord(ix,ia)
        do 3700 iib=ilipa+1,ilipa+nipa(ia)
          ib=lipa(iib)
          if(ib .le.ia) go to 3700
          if(din(ib)) go to 3700
          rb=srad(ib)
          dab=0.d0
          nsab=0
          do 3002 ix=1,3
            xta(ix,2)=coord(ix,ib)
            xx(ix)=xta(ix,2)-xta(ix,1)
3002      dab=dab+xx(ix)**2
          dab=sqrt(dab)
          cosa=(ra**2+dab**2-rb**2)/(2*dab*ra)
          cosb=(rb**2+dab**2-ra**2)/(2*dab*rb)
          sina=sqrt(1.d0-cosa**2)
          da=ra*cosa
          hh=ra*sina
          ddd=rsolv*(cosa+cosb)/dab
          fz(1)=(1.d0-cos(hh*pi/ra))/2
          fz(2)=(1.d0-cos(hh*pi/rb))/2
          if(cosa*cosb .lt. 0d0) fz(1)=1.d0
          if(cosa*cosb .lt. 0d0) fz(2)=1.d0
          cosgz=(ra*ra+rb*rb-dab*dab)/(ra*rb)
          dc=rsolv*sqrt(2-cosgz)
          yx(1)=rsolv/srad(ia)
          yx(2)=rsolv/srad(ib)
          do 3005 ix=1,3
3005      xd(ix)=xta(ix,1)+da*xx(ix)/dab
c  create ring vectors
          rvx(1)=xx(2)*dirtm(3,1)-xx(3)*dirtm(2,1)
          rvx(2)=xx(3)*dirtm(1,1)-xx(1)*dirtm(3,1)
          rvx(3)=xx(1)*dirtm(2,1)-xx(2)*dirtm(1,1)
          dist=sqrt(rvx(1)**2+rvx(2)**2+rvx(3)**2)
          do 3006 ix=1,3
3006      rvx(ix)=hh*rvx(ix)/dist
          rvy(1)=(xx(2)*rvx(3)-xx(3)*rvx(2))/dab
          rvy(2)=(xx(3)*rvx(1)-xx(1)*rvx(3))/dab
          rvy(3)=(xx(1)*rvx(2)-xx(2)*rvx(1))/dab
c now all triple points on the ring are searched
          ntrp=0
          ntrp2=0
          do 3390 iic=ilipa+1,ilipa+nipa(ia)
            ic=lipa(iic)
            if (ic.eq.ib) go to 3390
            rc=srad(ic)
            dabc=0.d0
            sp=0.d0
            do 3302 ix=1,3
              xxx=coord(ix,ic)-xd(ix)
              xta(ix,3)=coord(ix,ic)
              sp=sp+xxx*xx(ix)
3302        dabc=dabc+xxx**2
            dabc=sqrt(dabc)
            cosa=sp/dab/dabc
            sina=sqrt(1.d0-cosa*cosa)
            cj=(dabc*dabc+hh*hh-rc*rc)/(2*dabc*hh*sina)
            if (cj .lt. 1.d0) ntrp2=ntrp2+2
            if (cj .gt. 1.d0 .or. cj .lt. -1.d0) go to 3390
            sj=sqrt(1.d0-cj*cj)
c  create ring vectors
            do 3305 ix=1,3
3305        tvx(ix)=(xta(ix,3)-xd(ix))-cosa*dabc*xx(ix)/dab
            dist=sqrt(tvx(1)**2+tvx(2)**2+tvx(3)**2)
            do 3306 ix=1,3
3306        tvx(ix)=hh*tvx(ix)/dist
            tvy(1)=(xx(2)*tvx(3)-xx(3)*tvx(2))/dab
            tvy(2)=(xx(3)*tvx(1)-xx(1)*tvx(3))/dab
            tvy(3)=(xx(1)*tvx(2)-xx(2)*tvx(1))/dab
            do 3380 l=-1,1,2
              il=ntrp+1
              do 3310 ix=1,3
3310          trp(ix)=xd(ix)+cj*tvx(ix)+sj*tvy(ix)*l
              do 3320 ik=ilipa+1,ilipa+nipa(ia)
                k=lipa(ik)
                if(k.eq.ib .or. k.eq.ic) go to 3320
                dabck=0.d0
                do 3315 ix=1,3
3315            dabck=dabck+(trp(ix)-coord(ix,k))**2
                dabck=sqrt(dabck)
                if (dabck .lt. srad(k)) go to 3380
3320          continue
              ntrp=ntrp+1
              spx=0.d0
              spy=0.d0
              do 3322 ix=1,3
                spx=spx+rvx(ix)*(trp(ix)-xd(ix))
                spy=spy+rvy(ix)*(trp(ix)-xd(ix))
3322          continue
              phi=acos(spx/hh**2)
              if(spy.lt.0.d0) phi=-phi
              phiset(il)=phi
              sp=0.d0
              do 3324 ix=1,3
3324          sp=sp+(-spy*rvx(ix)+spx*rvy(ix))
     &               *(trp(ix)-xta(ix,3))
              iset(ntrp)=1
              if(sp.lt.0.d0) iset(ntrp)=-1
c if the triple point is new the corresponding surface patches are added
c
c midi use only triple points which fulfil the following checks
              dbc=0.d0
              dac=0.d0
              do  ix=1,3
               dbc=dbc+(coord(ix,ic)-coord(ix,ib))**2
               dac=dac+(coord(ix,ic)-coord(ix,ia))**2
              enddo
              dbc = sqrt(dbc)
              dac = sqrt(dac)
              if(srad(ia)+srad(ib)-rsolv .lt. dab) then
               go to 3380
              endif
              if(srad(ic)+srad(ib)-rsolv .lt. dbc) then
               go to 3380
              endif
              if(srad(ia)+srad(ic)-rsolv .lt. dac) then
               go to 3380
              endif
c end midi
              if(ic.le.ib) go to 3380
              do 3325 ix=1,3
                svx(ix)=xta(ix,2)-xta(ix,1)
                svy(ix)=xta(ix,3)-xta(ix,1)
3325          em(ix)=0.d0
              do 3330 iiia=1,3
                dist=0.d0
                do 3326 ix=1,3
                  xxx=xta(ix,iiia)-trp(ix)
                  dist=dist+xxx*xxx
3326            ee(ix,iiia)=xxx
                dist=sqrt(dist)
                do 3328 ix=1,3
                  xxx=ee(ix,iiia)/dist
                  em(ix)=em(ix)+xxx
3328            ee(ix,iiia)=xxx
3330          continue
              dist=0.d0
              spn=0.d0
              do 3334 iiia=1,3
                dist=dist+em(iiia)**2
                iiib=mod(iiia,3)+1
                iiic=6-iiia-iiib
                xxx=svx(iiib)*svy(iiic)-svx(iiic)*svy(iiib)
                spn=spn+xxx*(xta(iiia,1)-trp(iiia))
                yy(iiia)=xxx
                sp=0.d0
                do 3332 ix=1,3
3332            sp=sp+ee(ix,iiib)*ee(ix,iiic)
                cc(iiia)=sp
3334          ss(iiia)=sqrt(1.d0-sp*sp)
              sar=-pi
              dist=sqrt(dist)
              do 3338 iiia=1,3
                em(iiia)=em(iiia)/dist
                iiib=mod(iiia,3)+1
                iiic=6-iiia-iiib
3338          sar=sar+
     &        acos((cc(iiia)-cc(iiib)*cc(iiic))/ss(iiib)/ss(iiic))
              sar=sar*rsolv**2/3
c if segment is less than 1% of a normal segment, drop it
              if (sar.lt. .5d0/nspa) go to 3380
              do 3370 iiia=1,3
                dist=0.d0
                spa=0.d0
                do 3345 ix=1,3
                  xxx=1.4d0*em(ix)+ee(ix,iiia)
                  dist=dist+xxx*xxx
                  spa=spa+yy(ix)*xxx
3345            cc(ix)=xxx
                dist=1/sqrt(dist)
                fact=rsolv*dist
c if the segment center lies below the plane of the three atoms drop it
                if(spa*fact/spn .gt. 1) then
                  go to 3370
                end if
                nps=nps+1
                iii=ia
                if(iiia.eq.2) iii=ib
                if(iiia.eq.3) iii=ic
                iatsp(nps)=iii
                ar(nps)=sar
                spnn=0.d0
                do 3348 ix=1,3
                  cosurf(ix,nps)=-cc(ix)*dist
                  xsp(ix,nps)=trp(ix)+fact*cc(ix)
                  spnn=spnn+xsp(ix,nps)*cosurf(ix,nps)
3348            continue
                area=area+sar
                volume=volume+sar*spnn
3370          continue
3380        continue
3390      continue
c sort the set of triple points on the ring
          if(mod(ntrp,2).ne.0) then
           write(icolum,'(a)') 'Problem: odd ntrp in consts.f'
           stop
          endif
          if(ntrp.gt.18) then
           write(icolum,'(a)') 'Problem: ntrp  too big in consts.f'
           stop
          endif
          if(ntrp+ntrp2.eq.0) then
            phiset(1)=0
            phiset(2)=2*pi
            iset(1)=1
            iset(2)=-1
            ntrp=2
          end if


3400      ic=0
          do 3410 l=2,ntrp
          if (phiset(l).lt.phiset(l-1)) then
            phi=phiset(l)
            iii=iset(l)
            phiset(l)=phiset(l-1)
            iset(l)=iset(l-1)
            phiset(l-1)=phi
            iset(l-1)=iii
            ic=ic+1
          end if
3410      continue
          if(ic.gt.0) go to 3400
          if(iset(1) .eq. -1) then
            phiset(1)=phiset(1)+2*pi
            go to 3400
          end if
c now for each continuous section of the ring triangles are created
          sumphi=0.d0
          ips0=nps
          do 3600 l=2,ntrp,2
            k=l-1
            phiu=phiset(k)
            phio=phiset(l)
            nsa=(phio-phiu)/2/pi*nsring
            nsa=max(nsa+1,2)
            sumphi=sumphi+phio-phiu
            dp=(phio-phiu)/(nsa-1)
            do 3550 ich=1,2
            do 3550 ja=ich,nsa,2
              jb=max(ja-1,1)
              jc=min(ja+1,nsa)
              phi=phiu+(ja-1)*dp
              cphi=cos(phi)
              sphi=sin(phi)
              do 3545 ix=1,3
                ca=xd(ix)+(cphi*rvx(ix)+sphi*rvy(ix))*fz(ich)
                ca=ca+(xta(ix,ich)-ca)*yx(ich)
3545          xja(ix)=ca+(xta(ix,3-ich)-xta(ix,ich))*ddd*
     &                (1.d0-fz(ich))
              phi=phiu+(jb-1)*dp
              cphi=cos(phi)
              sphi=sin(phi)
              do 3546 ix=1,3
                ca=xd(ix)+cphi*rvx(ix)+sphi*rvy(ix)
3546          xjb(ix)=ca+(xta(ix,3-ich)-ca)*yx(3-ich)
              phi=phiu+(jc-1)*dp
              cphi=cos(phi)
              sphi=sin(phi)
              do 3547 ix=1,3
                ca=xd(ix)+cphi*rvx(ix)+sphi*rvy(ix)
3547          xjc(ix)=ca+(xta(ix,3-ich)-ca)*yx(3-ich)
              sp=0.d0
              d1=0.d0
              d2=0.d0
              do ix=1,3
                xx1=xjc(ix)-xjb(ix)
                xx2=xja(ix)-xjb(ix)
                d1=d1+xx1*xx1
                d2=d2+xx2*xx2
                sp=sp+xx1*xx2
              enddo
              dl=sqrt(d2-sp*sp/d1)
              sar=(jc-jb)*dp*hh*(1.d0-yx(3-ich))*dl/2
c if segment is less than 1% of a normal segment, drop it
              if (sar.lt. .5d0/nspa) go to 3550
              nps=nps+1
              nsab=nsab+1
              spn=0.d0
              dist=0.d0
              iatsp(nps)=ib
              if(ich.eq.2) iatsp(nps)=ia
              iat=iatsp(nps)
              do 3548 ix=1,3
                xsp(ix,nps)=(xja(ix)*0.5d0+xjb(ix)+xjc(ix))/2.5d0
                i2=mod(ix,3)+1
                i3=mod(i2,3)+1
                cosurf(ix,nps)=(xjc(i2)-xjb(i2))*(xja(i3)-xjb(i3))
     &                        -(xja(i2)-xjb(i2))*(xjc(i3)-xjb(i3))
                dist=dist+cosurf(ix,nps)**2
                spn=spn+cosurf(ix,nps)*(xsp(ix,nps)-coord(ix,iat))
3548          continue
              dist=1.d0/sqrt(dist)
              if(spn .lt. 0.d0) dist=-dist
              spn=0.d0
              do 3549 ix=1,3
                cosurf(ix,nps)=cosurf(ix,nps)*dist
3549          spn=spn+cosurf(ix,nps)*xsp(ix,nps)
              ar(nps)=sar
              volume=volume+ar(nps)*spn
              area=area+ar(nps)
3550        continue
3600      continue
          if (sumphi.gt.1.d-10) then
c           nip counts intersections between atoms
c           ia, ib denote intersecting atoms
c           nsab is the number of gridpoints on the intersection ring
c             of atoms ia, ib
c           ips0 is the last gridpoint at atom ia before the
c             intersection points
            nip=nip+1
            isude(1,nip)=ia
            isude(2,nip)=ib
            isude(3,nip)=nsab
            isude(4,nip)=ips0
            call ansude(ra-rsolv,rb-rsolv,dab,rsolv,
     &                aa,ab,aar,abr,aad,abd)
            sumphi=sumphi/(2*pi)
            sude(1,nip)=aar*sumphi
            sude(2,nip)=abr*sumphi
            sude(3,nip)=aad*sumphi
            sude(4,nip)=abd*sumphi
          endif
3700    continue
3800  ilipa=ilipa+nipa(ia)
      volume=volume/3

c-----------save sude-stuff on file sude----------------------------

      call writsude(nip,sude,isude,icossu)

c--------------------------------------------------------------------
c
3333  continue
c--------------------------------------------------------------------
c Introducing Symmetry
c compress xsp with respect to the irrep
c--------------------------------------------------------------------
c skip this part if C1

      if(isym .eq. 0) goto 447

      do ips=1, nps
         redseg(ips)=.false.
      enddo
      npspherred = 0
      npsred = 0

c check segments

      do ips=1, nps
         call irreducible(isym,xsp(1,ips),xsp(2,ips),
     &                    xsp(3,ips),valid)
         if(valid) then
            redseg(ips)=.true.
            if(ips.le.npspher) npspherred = npspherred+1
            npsred =npsred+1
         endif
      enddo

c compress xsp, nar, nsetf, ar and iatsp

      do ips=1,npsred
         if(.not.redseg(ips)) then
            do jps=ips+1,nps
               if(redseg(jps)) then
                  do ix=1,3
                     xsp(ix,ips) = xsp(ix,jps)
                     cosurf(ix,ips) = cosurf(ix,jps)
                  enddo

                  nar(ips)=nar(jps)
                  nsetf(ips)=nsetf(jps)
                  ar(ips)=ar(jps)
                  iatsp(ips)=iatsp(jps)

                  redseg(jps) = .false.
                  goto 445
               endif
            enddo
         endif
445      continue
      enddo
      nps=npsred
      npspher = npspherred

447   continue
c----------------------------------------------------------------------
c size for outersphere is npspher
c size for innershpere is nps
c replace nps by npspher
      do 449 ips=1,npspher
          jps=nps+ips
          i=iatsp(ips)
          iatsp(jps)=i
          ri=srad(i)+(routf-1)*rsolv
          nar(jps)=nar(ips)
          nsetf(jps)=nsetf(ips)
          do 448 ix=1,3
c             cosurf(ix,jps)=cosurf(ix,ips)
              xsp(ix,jps)=coord(ix,i)+
     $                    (xsp(ix,ips)-coord(ix,i))*ri/(srad(i)-rsolv)
448       continue
          ar(jps)=ar(ips)*(ri/(srad(i)-rsolv))**2
449   continue

      if (nps .gt. maxnps) then
        write (icolum,11) nps,maxnps
   11   format ('nps .gt. maxnps in <cosurf>, nps=',i5,', maxnps=',i5)
        stop
      endif
      npsd= nps + npspher

c      write(icolum,'(a)') ' COSMO surface:'
c      write(icolum,'(a,i5)') '   number of segments:',nps
c      write(icolum,'(a,f14.6)') '   area (bohr**2):  ',area
c      write(icolum,'(a,f14.6)') '   volume (bohr**3):',volume

cmididebug-------------------------------------------------------------
c
c      open(ideb,file='debug',status='unknown')
c
c      write segment information
c
c        write(ideb,'(a)') '# Segment information'
c        write(ideb,'(a)') '#  n   atom                position
c     &  (X, Y, Z)[bohr]    area [bohr**2]'
c        do ix=1,nps+npspher
c              write(ideb,'(i5,i5,4f15.9)') ix,iatsp(ix),(xsp(jx,ix),
c     &        jx=1,3),ar(ix)
c        enddo
c
c     write basisgridpoints
c        write(ideb,'(a)') '# Basis grid points'
c        write(ideb,'(a)') '# segment      position   (X, Y, Z) [bohr]'
c        do ix=1,nps+npspher
c          if(ix .le. npspher .or. ix .gt. nps) then
c            i=iatsp(ix)
c            ri=srad(i)-rsolv
c            if(ix .gt. nps) ri=ri+routf*rsolv
c            nsetfi=nsetf(ix)
c            nari=nar(ix)
c            do k=nsetfi,nsetfi+nari-1
c              j1=nset(k)
c              do iy=1,3
c                xx(iy)=dirvec(iy,j1)*ri
c              end do
c              x1=xx(1)*tm(1,1,i)+xx(2)*tm(2,1,i)+xx(3)*tm(3,1,i)
c     &           +coord(1,i)
c              x2=xx(1)*tm(1,2,i)+xx(2)*tm(2,2,i)+xx(3)*tm(3,2,i)
c     &           +coord(2,i)
c              x3=xx(1)*tm(1,3,i)+xx(2)*tm(2,3,i)+xx(3)*tm(3,3,i)
c     &           +coord(3,i)
c              write(ideb,'(i5,3f15.9)') ix, x1, x2, x3
c            enddo
c          endif
c        enddo
c
c        close(ideb)
c
c----------------------------------------------------------------------
c set A-1

      fdiag=1.05d0*sqrt(nppa+0.d0)
c
c ---------------------------------------------------------------------
c     the A matrix is now ready to be set
c ---------------------------------------------------------------------
cmididebug
c      write(icolum,'(a)') '=======START A MATRIX SETUP=========='
c      write(icolum,'(a,f14.7)')'fdiag: ',fdiag

cmididebug kill ipump
c      ipump=0

      do 450 ips=1,npsd
cmididebug
c      write(icolum,'(a,i3)') 'ips: ',ips
c skip torus
        if(ips.gt.npspher.and.ips.le.nps) then
          aa=fdiagr/sqrt(ar(ips))
        else

          i=iatsp(ips)
          ri=srad(i)-rsolv

c         if (ips .gt. nps) ri=ri+rsolv-0.05d0*rsolv
          if (ips .gt. nps) ri=ri+routf*rsolv
          nari=nar(ips)
c         write(icolum,*) ' segment ',ips,' # points ',nari
          nsetfi=nsetf(ips)
          aa=0.d0
          do 350 k=nsetfi,nsetfi+nari-1
            j1=nset(k)
            aa=aa+fdiag
            x1=dirvec(1,j1)
            x2=dirvec(2,j1)
            x3=dirvec(3,j1)
            do 350 l=nsetfi,k-1
              j2=nset(l)
              aa=aa+2.d0/sqrt((x1-dirvec(1,j2))**2+
     1           (x2-dirvec(2,j2))**2+(x3-dirvec(3,j2))**2)
cmididebug
c              write(icolum,'(a,f14.7)')'basgrd dist: ', sqrt((x1-
c     &      dirvec(1,j2))**2+(x2-dirvec(2,j2))**2+(x3-dirvec(3,j2))**2)
c              write(icolum,'(a,f14.7)')'inner loop aa: ',aa
c              write(icolum,'(a,3f14.7)')'dirvec 1: ',x1,x2,x3
c              write(icolum,'(a,3f14.7)')'dirvec 1: ',dirvec(1,j2),
c     &        dirvec(2,j2),dirvec(3,j2)

  350     continue
          aa=aa/ri/nari/nari
        endif

c        if (ips .le. nps) then
c          ia1=ips*(ips+1)/2
c          a1mat(ia1)=aa
c        endif
c        a23mat(ips)=aa

        do 360 ix=1,3
          xi(ix)=coord(ix,i)
  360   xa(ix)=xsp(ix,ips)


c        do 440 jps=ips+1,npsd
c we include ips to the set of jps to get all replicates
c

        do 440 jps=ips,npsd
cmididebug
c        write(icolum,'(a,i3)') 'jps: ',jps

c data for original (not replicated) jps
c
          narj=nar(jps)
          nsetfj=nsetf(jps)
          j=iatsp(jps)
          dist=0.d0
          do 370 ix=1,3
            xj(ix)=coord(ix,j)-xi(ix)
  370     dist=dist+(xsp(ix,jps)-xa(ix))**2

c get all replicates

          call replicate(isym,xsp(1,jps),xsp(2,jps),xsp(3,jps),
     &                   jnp,srepx,srepy,srepz)
c get all replicates of the atom j which belongs to jps
          call replicate(isym,coord(1,j),coord(2,j),coord(3,j),
     &                     jnpa,arepx,arepy,arepz)
c
cmididebug
c          write(icolum,'(a,i3)')'#of replicated jps: ',jnp
c          write(icolum,'(a,i3)')'#of replicated atoms (j): ',jnpa

          joff = 1
          aijacc = 0.d0
          if(jps .eq. ips) then
            joff = 2
            aijacc = aa
          endif
cmididebug
c         write(icolum,'(a,f14.7)')'aijacc: ',aijacc
c
c jpsr > 1 -> replicated point
c
          do 435 jpsr=joff, jnp


c set values for replicated points
c
            if(jpsr .gt. 1) then
cmiddebug
c            if(ips .eq. 1 .and. jps .le. nps) then
c              ipump = ipump+1
c       write(icolum,'(a,i5,i5,i5)')'ips jps ipump: ',ips, jps, ipump
c              if(jpsr .gt.1) then
c                xsp(1,nps+jps)=arepx(jpsr)
c                xsp(2,nps+jps)=arepy(jpsr)
c                xsp(3,nps+jps)=arepz(jpsr)
c              endif
c             endif



              dist=0.d0
                xj(1)=arepx(jpsr)-xi(1)
                xj(2)=arepy(jpsr)-xi(2)
                xj(3)=arepz(jpsr)-xi(3)
                dist=dist+(srepx(jpsr)-xa(1))**2
                dist=dist+(srepy(jpsr)-xa(2))**2
                dist=dist+(srepz(jpsr)-xa(3))**2
            endif
cmididebug
c           write(icolum,'(a,i5,a,f14.7)') 'jpsr: ',jpsr,' dist: ',dist
c           write(icolum,'(a,3f14.7)') 'point jpsr: ',srepx(jpsr),
c     &               srepy(jpsr),srepz(jpsr)
c            write(icolum,'(a,3f14.7)')'atom of jpsr',arepx(jpsr),
c     &               arepy(jpsr),arepz(jpsr)
c           write(icolum,'(a,i5,a,i5)')'nari: ',nari,' narj: ',narj
c
c ips and jps are located on  the inner or outer spheres
c
            if (dist .lt. disex2 .and.
     &        (ips.le.npspher.or.ips.gt.nps) .and.
     &        (jps.le.npspher.or.jps.gt.nps)) then
              rj=srad(j)-rsolv
c             if(jps .gt. nps) rj=rj+rsolv-0.05d0*rsolv
              if(jps .gt. nps) rj=rj+routf*rsolv
cmididebug
c              write(icolum,'(a,f14.7)')'both on sphere rj: ',rj

              aij=0.d0
              do 430 k=nsetfi,nsetfi+nari-1
                j1=nset(k)
                do 380 ix=1,3
  380             xx(ix)=dirvec(ix,j1)*ri
c
c if ips and jps are located at the same atom, the replicated
c segments of jps connot be located on the same atom than ips
c
                if ((i .ne. j) .or. (jpsr .gt. 1)) then
                  x1=xx(1)*tm(1,1,i)+xx(2)*tm(2,1,i)+xx(3)*tm(3,1,i)
     &               -xj(1)
                  x2=xx(1)*tm(1,2,i)+xx(2)*tm(2,2,i)+xx(3)*tm(3,2,i)
     &               -xj(2)
                  x3=xx(1)*tm(1,3,i)+xx(2)*tm(2,3,i)+xx(3)*tm(3,3,i)
     &               -xj(3)

                  do 400 l=nsetfj,nsetfj+narj-1
                    j2=nset(l)
                    do 390 ix=1,3
  390                 xx(ix)=dirvec(ix,j2)*rj
                    y1=xx(1)*tm(1,1,j)+xx(2)*tm(2,1,j)+xx(3)*tm(3,1,j)
                    y2=xx(1)*tm(1,2,j)+xx(2)*tm(2,2,j)+xx(3)*tm(3,2,j)
                    y3=xx(1)*tm(1,3,j)+xx(2)*tm(2,3,j)+xx(3)*tm(3,3,j)
c
c replicate the basisgrd. point if needed here
c
                    if(jpsr .gt. 1) then
                      call replicate(isym,y1,y2,y3,
     &                       jnpb,brepx,brepy,brepz)
                      y1=brepx(jpsr)
                      y2=brepy(jpsr)
                      y3=brepz(jpsr)
                    endif

                    y1=y1-x1
                    y2=y2-x2
                    y3=y3-x3

                    aij=aij+1.d0/sqrt(y1*y1+y2*y2+y3*y3)
cmididebug
  400             continue

                else

c               only the original jps survives (jpsr=1)

  410             do 420 l=nsetfj,nsetfj+narj-1
                    j2=nset(l)
                    aa=(dirvec(1,j2)*rj-xx(1))**2+(dirvec(2,j2)*rj
     &                 -xx(2))**2+(dirvec(3,j2)*rj-xx(3))**2
                    aij = aij + 1.0d0/sqrt(aa)
cmididebug
c           write(icolum,'(a,f14.7)')'same atom basgrddist: ',sqrt(aa)
  420             continue
                end if
  430         continue
              aij=aij/nari/narj
cmididebug
c              write(icolum,'(a,f14.7)')'aij: ',aij
c
c ips is located on the inner spheres  amd jps is a trp or a ring segmen
c
            else if (dist .lt. disex2 .and.
     &        (ips.le.npspher) .and.
     &        (jps.gt.npspher.and.jps.le.nps)) then
              aij=0.d0
cmididebug
c              write(icolum,'(a)')'ips sphere, jps ring/trp'

c calc. radius of basis segment rk sqrt(area/pi)
c area of basis segmnet: ri**2*pi*4/nppa

              rk = sqrt(4.d0*ri**2/nppa)
              do k=nsetfi,nsetfi+nari-1
                j1=nset(k)
                do ix=1,3
                  xx(ix)=dirvec(ix,j1)*ri
                end do
                x1=xx(1)*tm(1,1,i)+xx(2)*tm(2,1,i)+xx(3)*tm(3,1,i)+xi(1)
                x2=xx(1)*tm(1,2,i)+xx(2)*tm(2,2,i)+xx(3)*tm(3,2,i)+xi(2)
                x3=xx(1)*tm(1,3,i)+xx(2)*tm(2,3,i)+xx(3)*tm(3,3,i)+xi(3)

c if needed use coordinates of the replicated segmnet
                if(jpsr .gt. 1) then
                  y1=srepx(jpsr)-x1
                  y2=srepy(jpsr)-x2
                  y3=srepz(jpsr)-x3
                else
                  y1=xsp(1,jps)-x1
                  y2=xsp(2,jps)-x2
                  y3=xsp(3,jps)-x3
                endif
c use the same damping function as below to handle to close trp/ring
c basis grid point distances
                bdist = sqrt(y1*y1+y2*y2+y3*y3)
                rskj=(rk+sqrt(ar(jps)/pi))/2.d0
                if (bdist.gt.rskj) then
                   aij=aij+1.d0/bdist
                else
cmididebug
c                write(icolum,'(a)')'Use dumping'
                   ak00=fdiagr/sqpi/rskj
                   aij=aij+ak00+(bdist/rskj)*(1.d0/rskj-ak00)
                endif
              end do
              aij=aij/nari
cmididebug
c              write(icolum,'(a,f14.7)')'aij: ',aij
            else
c ak midi  aug. 2001
c now the center appr. is used, with damping for close contacts
c For all pairs with a distance < 0.5 sum of segment radius a damping
c function is used.
c (0.5 seems to be a good approximation)

              if (dist .ge. disex2) then
                 aij=1.d0/sqrt(dist)
cmididebug
c              write(icolum,'(a,f14.7)')'other case >disex2; aij: ',aij
              else
cmididebug
c                 write(icolum,'(a)')'other case <disex2'
                 rsij=(sqrt(ar(ips)/pi)+sqrt(ar(jps)/pi))/2.d0
                 dist=sqrt(dist)
                 if (dist.gt.rsij) then
                   aij=1.d0/dist
                 else
cmididebug
c                    write(icolum,'(a)')'Use damping'
c                   write(icolum,*) 'Use damping (drel): ',ips,' ',jps,
c       & '(',dist/(rsij*2.d0),')'
                   a00=fdiagr/sqpi/rsij
                   aij=a00+(dist/rsij)*(1.d0/rsij-a00)
                 end if
cmididebug
c                 write(icolum,'(a,f14.7)')'aij: ',aij
               end if
            end if
          aijacc = aijacc+aij

c         end loop over replicates
  435     continue
cmdididebug
c      write(icolum,'(a,i5,i5,a,f14.7)')'matrix elem.',ips, jps,' ',
c     &                                 aijacc
c set diagonal elements

          if(ips .eq. jps) then
            if (ips .le. nps) then
              ia1=ips*(ips+1)/2
              a1mat(ia1)= aijacc
            endif
            a23mat(ips)= aijacc
c          endif
          else

c set other elements
cmididebug: be careful (in this context jps should be jps+1, because ips
            if (ips .le. nps .and. jps .le. nps) then
              ia1=ia1+jps-1
              a1mat(ia1)=aijacc
            end if
            a23mat(jps)=aijacc
          endif
  440   continue
        if (ips.le.nps) then
c         write row of full A2 matrix
          write (icosa2) (a23mat(jps),jps=nps+1,npsd)
        else
c         write row of triangular A3 matrix
          write (icosa3) (a23mat(jps),jps=ips,npsd)
        endif

  450 continue

c     write lower triangle of A1 matrix
      write (icosa) (a1mat(i),i=1,nps*(nps+1)/2)

cmididebug
c      write (icolum,'(a)')'============A1 MATRIX=================='
c      write (icolum,'(f15.7)') (a1mat(i),i=1,nps*(nps+1)/2)


cas   Cholesky decomposition of A1
      call coschol1(a1mat,nps,info)
      if (info.lt.0) then
c       go to the end and write some debug putput (cosurf)
c
        goto 500
      endif

  500 continue

c   nar is number of points per segment, nppa total number of points
c   points so each piece has size
c
c   overwrite xsp (1,*)

      do l=1,npsd
          cosurf(1,l) = xsp(1,l)
          cosurf(2,l) = xsp(2,l)
          cosurf(3,l) = xsp(3,l)
      enddo

c restore undisturbed coordinates
      k=1
      do i=1,natoms
        do j=1,3
          coord(j,i)=xyz(k)
          k=k+1
        enddo
      enddo

      write(icosco,'(a)') '# Information about the cavity'
      write(icosco,'(a)') '#  atomic coordinates'
      do i=1,natoms
          write(icosco,1000) (coord(j,i),j=1,3),mtype(i)
      enddo
c
c     write segment information
c
      write(icosco,'(a)') '# Segment information'
      write(icosco,'(a)') '# Number of segments (nps)'
      write(icosco,*) nps
      write(icosco,'(a)') '# he="normal seg."; x=triple p. or ring'
      write(icosco,'(a)') '#  n   atom                position
     &  (X, Y, Z)[bohr]      area [bohr**2]'
      do i=1,nps
         if(i .le. npspher) then
c
c             points on spheres
c
            write(icosco,1001) i,iatsp(i),(cosurf(j,i),j=1,3),
     &        ar(i),'he'
         else
c
c             ring and triple points
c
            write(icosco,1001) i,iatsp(i),(cosurf(j,i),j=1,3),
     &        ar(i),'x'
         endif
      enddo
cmididebug write outer cavity
      do i=nps+1,npsd
        write(icosco,1001) i,iatsp(i),(cosurf(j,i),j=1,3), ar(i),'be'
      enddo
cendmididebug
 1000 format(3(f20.14,2x),4x,a)
 1001 format(i5,i5,4f15.9,a5)


c
c-----------------------------------------------------------
c
c midi 14.8.2001
c If the amat error occured, we have to stop after
c writing the cosurf for debug purposes
c
      if (info.lt.0) then
         write(icolum,'(a)')'cosmo consts: a1mat not pos. def.'
         stop
      endif
      return
c
c-----------------------------------------------------------
      end
