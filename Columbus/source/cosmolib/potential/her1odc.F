!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C  /* Deck getodc */
      SUBROUTINE GETODC(ODC,JMAXA,JMAXB,JMAXT,JMAXD,JMAXM,DIFODC,KINODC,
     &                  ONECEN,EXPA,EXPB,IPRINT,SAAB13,EXPPI,WORK,LWORK,
     &                  CORPX,CORPY,CORPZ,DONUC1,DOMOM1,ORIGIN,INTTYP)
#include <implicit.h>
#include <priunit.h>
      LOGICAL DIFODC, KINODC, ONECEN, DONUC1, DOMOM1
      DIMENSION ODC(0:JMAXA,0:JMAXB,0:JMAXT,0:JMAXD,0:JMAXM,3),
     &          WORK(LWORK), ORIGIN(3)
C
      IF (DONUC1) THEN
         JMAXAP = JMAXA + JMAXD
         JMAXBP = JMAXB
      ELSE
         JMAXAP = JMAXA
         JMAXBP = JMAXB + JMAXD
      END IF
      IF (DOMOM1) THEN
         JMAXAP = JMAXAP + JMAXM
      ELSE
         JMAXBP = JMAXBP + JMAXM
      END IF
      ITEX = MAX(1,JMAXD + JMAXM)
      JMAXTP = JMAXT + ITEX
C
      IF ((INTTYP .EQ. 49) .OR. (INTTYP .EQ. 51)) THEN
         IF (DOMOM1) THEN
            JMAXBP = JMAXBP + 1
         ELSE
            JMAXAP = JMAXAP + 1
         END IF
         JMAXTP = JMAXTP +1
      ENDIF
C
      KODCUN = 1
      KLAST  = 1 + 3*(JMAXAP + 4)*(JMAXBP + 3)*(JMAXTP + 3)*(JMAXD + 1)
     &              *(JMAXM + 1)
      IF (KLAST .GT. LWORK) CALL STOPIT('GETODC',' ',KLAST,LWORK)
      CALL GETOD1(ODC,JMAXA,JMAXB,JMAXT,JMAXD,JMAXM,JMAXAP,JMAXBP,
     &            JMAXTP,WORK(KODCUN),DIFODC,KINODC,ONECEN,EXPA,EXPB,
     &            IPRINT,SAAB13,EXPPI,CORPX,CORPY,CORPZ,DONUC1,DOMOM1,
     &            ORIGIN,INTTYP)
      RETURN
      END
C  /* Deck getod1 */
      SUBROUTINE GETOD1(ODC,JMAXA,JMAXB,JMAXT,JMAXD,JMAXM,JMAXAP,JMAXBP,
     &                  JMAXTP,ODCUND,DIFODC,KINODC,ONECEN,EXPA,EXPB,
     &                  IPRINT,SAAB13,EXPPI,CORPX,CORPY,CORPZ,DONUC1,
     &                  DOMOM1,ORIGIN,INTTYP)
#include <implicit.h>
#include <priunit.h>
      LOGICAL DIFODC, KINODC, ONECEN, DONUC1, DOMOM1
      DIMENSION ODC(0:JMAXA,0:JMAXB,0:JMAXT,0:JMAXD,0:JMAXM,3),
     &      ODCUND(-2:JMAXAP+1,-1:JMAXBP+1,-2:JMAXTP,0:JMAXD,0:JMAXM,3)
C
C     Clear arrays
C
      CALL DZERO(ODC,3*(JMAXA  + 1)*(JMAXB + 1)*(JMAXT + 1)*(JMAXD + 1)
     &                *(JMAXM + 1))
      CALL DZERO(ODCUND,3*(JMAXAP + 4)*(JMAXBP + 3)*(JMAXTP+3)
     &                   *(JMAXD+1)*(JMAXM + 1))
C
C     Undifferentiated expansion coefficients
C
      CALL ONEODC(ODCUND,JMAXAP,JMAXBP,JMAXTP,JMAXD,JMAXM,SAAB13,EXPPI,
     &            CORPX,CORPY,CORPZ,IPRINT)
      CALL COPODC(ODCUND,ODC,JMAXAP,JMAXBP,JMAXA,JMAXB,JMAXT,JMAXTP,
     &            JMAXD,JMAXM,0,0)
      IF (IPRINT .GE. 10) THEN
c         CALL TITLER('Output from GETOD1','*',103)
         NROW = (JMAXA + 1)*(JMAXB + 1)
         NCOL = JMAXT + 1
         CALL AROUND('Undifferentiated ODC in GETOD1 - x component')
         CALL OUTPUT(ODC(0,0,0,0,0,1),1,NROW,1,NCOL,NROW,NCOL,1,LUPRI)
         CALL AROUND('Undifferentiated ODC in GETOD1 - y component')
         CALL OUTPUT(ODC(0,0,0,0,0,2),1,NROW,1,NCOL,NROW,NCOL,1,LUPRI)
         CALL AROUND('Undifferentiated ODC in GETOD1 - z component')
         CALL OUTPUT(ODC(0,0,0,0,0,3),1,NROW,1,NCOL,NROW,NCOL,1,LUPRI)
      END IF
C
C     Expansion coefficients for derivatives
C
      IF (KINODC) THEN
            CALL TODC(ODC,ODCUND,JMAXA,JMAXAP,JMAXBP,JMAXB,JMAXT,
     &                JMAXTP,JMAXD,JMAXM,EXPA)
      END IF
C
C     Expansion coeffiecients for moments or electric derivatives
C
      RETURN
      END
C  /* Deck copodc */
      SUBROUTINE COPODC(ODCUND,ODC,JMAXAP,JMAXBP,JMAXA,JMAXB,JMAXT,
     &                JMAXTP,JMAXD,JMAXM,ICOPYD,ICOPYM)
C
C     Copy expansion coefficients from ODCUND to ODC
C
#include <implicit.h>
      DIMENSION ODCUND(-2:JMAXAP+1,-1:JMAXBP+1,-2:JMAXTP,
     &                 0:JMAXD,0:JMAXM,3),
     &          ODC(0:JMAXA,0:JMAXB,0:JMAXT,0:JMAXD,0:JMAXM,3)
      DO 100 IC = 1, 3
         DO 100 IM = 0, ICOPYM
         DO 100 ID = 0, ICOPYD
            DO 100 IT = 0, JMAXT
               DO 100 IB = 0, JMAXB
               DO 100 IA = 0, JMAXA
                  ODC(IA,IB,IT,ID,IM,IC) = ODCUND(IA,IB,IT,ID,IM,IC)
  100 CONTINUE
      RETURN
      END
C  /* Deck oneodc */
      SUBROUTINE ONEODC(ODCUND,JMAXAP,JMAXBP,JMAXTP,JMAXD,JMAXM,FAC,
     &                  EXPPI,CORPX,CORPY,CORPZ,IPRINT)
C
C     TUH 91
C
#include <implicit.h>
#include <priunit.h>
      PARAMETER (D2 = 2.D0)
      INTEGER T, A, B, AB
      DIMENSION ODCUND(-2:JMAXAP+1,-1:JMAXBP+1,-2:JMAXTP,
     &                 0:JMAXD,0:JMAXM,3)
#include <onecom.h>
#include <sdpre.h>
C
      PAX = CORPX - CORAX
      PAY = CORPY - CORAY
      PAZ = CORPZ - CORAZ
      PBX = CORPX - CORBX
      PBY = CORPY - CORBY
      PBZ = CORPZ - CORBZ
      EXPPIH = EXPPI/D2
      DO 100 A = 0, JMAXAP
         IF (A .EQ. 0) THEN
            ODCUND(0,0,0,0,0,1) = FAC
            ODCUND(0,0,0,0,0,2) = FAC
            ODCUND(0,0,0,0,0,3) = FAC
         ELSE
            DO 200 T = 0, A
               ODCUND(A,0,T,0,0,1) = EXPPIH*ODCUND(A-1,0,T-1,0,0,1)
     *                            + PAX*ODCUND(A-1,0,T  ,0,0,1)
     *                     + SDPRE(T+1)*ODCUND(A-1,0,T+1,0,0,1)
               ODCUND(A,0,T,0,0,2) = EXPPIH*ODCUND(A-1,0,T-1,0,0,2)
     *                            + PAY*ODCUND(A-1,0,T  ,0,0,2)
     *                     + SDPRE(T+1)*ODCUND(A-1,0,T+1,0,0,2)
               ODCUND(A,0,T,0,0,3) = EXPPIH*ODCUND(A-1,0,T-1,0,0,3)
     *                            + PAZ*ODCUND(A-1,0,T  ,0,0,3)
     *                     + SDPRE(T+1)*ODCUND(A-1,0,T+1,0,0,3)
  200       CONTINUE
         END IF
         DO 300 B = 1, JMAXBP
            AB = A + B
C
            DO 400 T = 0, AB
               ODCUND(A,B,T,0,0,1) = EXPPIH*ODCUND(A,B-1,T-1,0,0,1)
     &                            + PBX*ODCUND(A,B-1,T  ,0,0,1)
     &                     + SDPRE(T+1)*ODCUND(A,B-1,T+1,0,0,1)
               ODCUND(A,B,T,0,0,2) = EXPPIH*ODCUND(A,B-1,T-1,0,0,2)
     &                            + PBY*ODCUND(A,B-1,T  ,0,0,2)
     &                     + SDPRE(T+1)*ODCUND(A,B-1,T+1,0,0,2)
               ODCUND(A,B,T,0,0,3) = EXPPIH*ODCUND(A,B-1,T-1,0,0,3)
     &                            + PBZ*ODCUND(A,B-1,T  ,0,0,3)
     &                     + SDPRE(T+1)*ODCUND(A,B-1,T+1,0,0,3)
  400          CONTINUE
  300    CONTINUE
  100 CONTINUE
      IF (IPRINT .GE. 20) THEN
c         CALL TITLER('Output from ONEODC','*',103)
         NROW = (JMAXAP + 4)*(JMAXBP + 3)
         NCOL = JMAXTP + 3
C
C     NOTICE: Prints all ODCUND, including dummy elements.
C
         CALL AROUND('ODCUND in ONEODC - x component')
         CALL OUTPUT(ODCUND(-2,0,-2,0,0,1),1,NROW,1,NCOL,NROW,NCOL,
     &               1,LUPRI)
         CALL AROUND('ODCUND in ONEODC - y component')
         CALL OUTPUT(ODCUND(-2,0,-2,0,0,2),1,NROW,1,NCOL,NROW,NCOL,
     &               1,LUPRI)
         CALL AROUND('ODCUND in ONEODC - z component')
         CALL OUTPUT(ODCUND(-2,0,-2,0,0,3),1,NROW,1,NCOL,NROW,NCOL,
     &               1,LUPRI)
      END IF
      RETURN
      END
C  /* Deck dodca */
C  /* Deck todc */
      SUBROUTINE TODC(ODC,ODCUND,JMAXA,JMAXAP,JMAXBP,JMAXB,JMAXT,
     &                JMAXTP,JMAXD,JMAXM,EXPA)
C
C     TUH
C
#include <implicit.h>
#include <priunit.h>
      INTEGER A, B, T
      DIMENSION ODCUND(-2:JMAXAP+1,-1:JMAXBP+1,-2:JMAXTP,0:JMAXD,
     &                 0:JMAXM,3),
     &          ODC(0:JMAXA,0:JMAXB,0:JMAXT,0:JMAXD,0:JMAXM,3)
#include <sdpre.h>
      TEXPA1 = EXPA + EXPA
      TEXPA2 = TEXPA1*TEXPA1
      IF (JMAXD .LT. 2) THEN
c         WRITE (LUPRI,'(//,1X,A,I2,A)')
c     &          ' JMAXD = ',JMAXD,' too small in TODC,',
c     &          ' program cannot proceed.'
         CALL QUIT('JMAXD too small in TODC')
      END IF
      DO 100 A = 0, JMAXA
         FAC21A = TEXPA1*SDPRE(2*A + 1)
         IF (A .LT. 2) THEN
            DO 200 B = 0, JMAXB
               ODC(A,B,0,2,0,1) = TEXPA2*ODCUND(A+2,B,0,0,0,1)
     &                          - FAC21A*ODCUND(A  ,B,0,0,0,1)
               ODC(A,B,0,2,0,2) = TEXPA2*ODCUND(A+2,B,0,0,0,2)
     &                          - FAC21A*ODCUND(A  ,B,0,0,0,2)
               ODC(A,B,0,2,0,3) = TEXPA2*ODCUND(A+2,B,0,0,0,3)
     &                          - FAC21A*ODCUND(A  ,B,0,0,0,3)
  200       CONTINUE
         ELSE
            FAC20A = SDPRE(A*(A - 1))
            DO 300 B = 0, JMAXB
               ODC(A,B,0,2,0,1) = TEXPA2*ODCUND(A+2,B,0,0,0,1)
     &                        - FAC21A*ODCUND(A  ,B,0,0,0,1)
     &                        + FAC20A*ODCUND(A-2,B,0,0,0,1)
               ODC(A,B,0,2,0,2) = TEXPA2*ODCUND(A+2,B,0,0,0,2)
     &                        - FAC21A*ODCUND(A  ,B,0,0,0,2)
     &                        + FAC20A*ODCUND(A-2,B,0,0,0,2)
               ODC(A,B,0,2,0,3) = TEXPA2*ODCUND(A+2,B,0,0,0,3)
     &                        - FAC21A*ODCUND(A  ,B,0,0,0,3)
     &                        + FAC20A*ODCUND(A-2,B,0,0,0,3)
  300       CONTINUE
         END IF
  100 CONTINUE
      RETURN
      END
C
