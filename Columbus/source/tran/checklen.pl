#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


   open FIN,"<potentialmod.F90";
   $lcnt=0;
   $lastline=<FIN>;
   chop $lastline;
   $lastline=~s/^[cC]/!/;
   while (<FIN> )

   { #@str=split(//,$_); $lcnt++;
     # if ($#str > 72 ) { print "$lcnt, $#str: $_"; } 
     if (/^     \S/) { print "$lastline  & \n"; 
                       s/^     \S/     &/; }
              else   { print "$lastline \n";}
     $lastline=$_; chop $lastline;
     $lastline=~s/^[cC]/!/;
   }
 
