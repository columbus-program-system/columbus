#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


  open FIN,"<allparams" || die ("could not open allparams");
  while (<FIN>) 
   { s/^ *PARAMETER *//;
     chop;
     $saveit=$_;
     s/=.*$//;
     s/[( ]//g;
     $pname=$_;
     $saveit=~s/^.*=//;
     $saveit=~s/[) ]//g;
     $phash{$pname}[0]=$saveit; 
     $_=$saveit;
     if (/^[0-9.]/) { $phash{$pname}[1]='essential';} 
          else      { $phash{$pname}[1]='derived';} 
      $phash{$pname}[2]++;
   }


  close FIN;
  open FPAR,">newparam.inc";
  @essential=();
  foreach $f ( keys %phash ) 
   { printf " parameter %-20s   %-20s   %-10s  # %3d \n", $f,$phash{$f}[0],$phash{$f}[1],$phash{$f}[2];
     printf FPAR "         INTEGER, PARAMETER, PRIVATE :: $f ( $phash{$f}[0] ) \n"; 
     if ( grep (/essential/,$phash{$f}[1])) { push @essential, $f;}
   }
   close FPAR;

  print "Essential parameters:\n";
  foreach $i (@essential) 
     { print "  .... checking  $i \n";
       foreach $fortran (<*.f>)
       {open FOR,"<$fortran";
        while (<FOR>) 
         { if ( !/(parameter|integer|real\*8)/i  && /\b$i\b/ ) { $pcnt{$i}++; print "$fortran $_ ";}}
        close FOR;
        }
    }

  print "SUMMARY ..\n";
  @neverused=();
  foreach $i (@essential)
  { print "****************** essential parameter $i  found $pcnt{$i} times ************************\n";
    if ( ! $pcnt{$i} ) { push @neverused, $i;}
   #system ("grep -i $i *.f ");
   } 
 print "NEVER USED ..\n";
 print join(',',@neverused), "\n";
