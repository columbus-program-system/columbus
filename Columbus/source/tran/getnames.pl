#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


  foreach $i  (@ARGV) 
  { $fin=$i;
    open FIN,"<$fin" || die ("could not open :$fin:\n");
    @validlines=();
    while (<FIN>)
    { if (/COMMON/) { push @validlines,$_;} }

    print "extracted the following lines\n";
    print @validlines;

    $commonname=$validlines[0];
    chop $commonname;
    $commonname=~/^.*COMMON \/(.*)\//;
    $commonname=$1;
    $commonname=~s/ //g;
    $commonname=lc($commonname);

    $variables="";
    foreach $lin (@validlines)
      { chop $lin; $lin=~s/COMMON *\/.*\///g;
        $variables = $variables . $lin ;}
     $variables=~s/,/ /g;
     $variables=~s/ +/ /g;
     $variables=~s/^ *//;
     @variables=split(/\s+/,$variables);
     foreach $lin (@variables) { $foundvars{$lin}=0; print " VARIABLE  $lin  ==> $commonname%$lin \n";}
    close FIN;

     foreach $fortran (<*.f>) 
       { # search for $commonname.inc
        $status=system(" grep $commonname.inc $fortran > tmp "); 
        if (! $status) {print "found $commonname in $fortran \n";
                      open FIN,"<$fortran";
                      open FOUT,">$fortran.new";
                      while (<FIN>) { $fline=$_; foreach $x (@variables) { #print " $x:$fline"; 
                                                                           if  ( grep (/\b$x\b/i, $fline) ) { $fline=~s/$x/$commonname%$x/gi;
                                                                                                          print $fline;
                                                                                                          $foundvars{$x}++; }}
                      print FOUT $fline; 
                      }
                      close FOUT;
                      close FIN;
                      unlink ("$fortran");
                      system("mv $fortran.new $fortran");
                     }
       }

      $usedvars=" ";
      $unusedvars=" ";
      foreach $x (keys %foundvars )
       { if ($foundvars{$x}) { print " VARIABLE $x was used $foundvars{$x} times\n";
                               $usedvars = "$usedvars, $x";}
                    else       {$unusedvars= "$unusedvars, $x";}
         }
      $usedvars=~s/^ *, *//;
      $unusedvars=~s/^ *, *//;
      print "USED:\n $usedvars\nUNUSED:\n $unusedvars\n"; 

    


  }
 
  exit;

#     COMMON / DORPS  /   DOREPS,      DOCOOR,      NDCORD,      DCORD
#     COMMON / DORPS  /   DCORGD,      DOPERT

