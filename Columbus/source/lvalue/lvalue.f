!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program lvalue
c
c*************************************************************
c
c Author: Michal Dallos, Uni Wien, 19.Dec. 2001
c
c  Description:
c  This program reads the different <state1|L|state2> matrix elements
c  and diagonalizes the matrix.
c
c
c  input files:  matel
c  output file:  lvaluels
c
c  Description of the input parameters:
c  new version tm, fzj, feburary 2, 2012 
c
c*************************************************************
c
       implicit none
c  ##  parameter & common block section
c
      integer maxsym,nmotx
      parameter (maxsym=8, nmotx=510)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      character*8 program,version
      parameter (program='LVALUE')
      parameter (version='1.0')
c
      real*8 two,zero,one
      parameter (two=2.0d+00,zero=0.0d+00,one=1.0d+00)
c
      integer mdim
      parameter (mdim=100)
c
c  ##  integer section
c
      integer cpt(5)
      integer drt1(mdim),drt2(mdim),datafl
      integer info,i,ind1,ind2,ind3,idir,icount,inpfl
      integer jsym,j
      integer kdim
      integer lwork
      integer map(mdim,mdim),maprev(mdim),ndim
      integer nlist,liwork
      integer state1(mdim),state2(mdim)
      integer ntrans,tidx(4,100)
      real*8  tlval(3,100)
c
c  ##  real*8 section
c
      real*8 core(1000000)
      real*8 matrix(5*mdim*mdim)
      real*8 number,norm(3)
      real*8 rwork(2*mdim)
      real*8 temp,l2
      real*8 x
      real*8 y
      real*8 z
c
c  ##  character section
c
      character*80 flname,ctmp
      character*2 chrx(3)
      character*8 cmaprev(mdim)
c
c  ##  complex section
c
      complex cmatrix(mdim*mdim),ceig(mdim),cvec(mdim*mdim),
     &  cwork(2*mdim),cdum
c
      data chrx/'Lx','Ly','Lz'/
      data nlist/6/
      data inpfl/5/
      data datafl /50/
c
c
c------------------------------------------------------------
c
      open(unit=nlist,file="lvaluels",form="formatted")
c
      call headwr(nlist,program,version)
c
      callwzero(3*mdim*mdim,matrix,1)
c
      open(unit=inpfl,file="matel",status="old")
      ntrans=0
c  skip comment lines
 25   read(inpfl,'(a80)') ctmp
      ntrans=ntrans+1
      if (ctmp(1:1).ne.'#') 
     .    call bummer('incorrect comment line',ntrans,2) 
      read(inpfl,*,end=26) tidx(1:4,ntrans),tlval(1:3,ntrans)
      goto 25 
 26   continue
      write(nlist,*) 'Read ',ntrans,' transitions:'
      do i=1,ntrans
       write(nlist,27)i, tidx(1:4,ntrans),tlval(1:3,ntrans)
 27    format('#',i2,3x,i2,'.',i2,'->',i2,'.',i2,' lxyz=',
     .        3f8.4)
       call addstate(tidx(1,ntrans),tidx(2,ntrans),kdim,map,maprev,mdim)
       call addstate(tidx(3,ntrans),tidx(4,ntrans),kdim,map,maprev,mdim)
      enddo 
     

      write(nlist,*)'Number of states in calculation:',kdim
      write(nlist,*)
      write(nlist,*)'List of states:'
      write(nlist,*)'State   DRT   Index'
      do i=1,kdim
      write(nlist,'(2x,3(i2,5x))')maprev(i)/1000, mod(maprev(i),1000), i 
      write(cmaprev(i),'(i3,a,i3)') 
     .    maprev(i)/1000, '.',mod(maprev(i),1000) 
      enddo 

c
c     tidx(1:4,1:ntrans) = indices (nroot1,drt1,nroot2,drt2) of transitions
c     tlval(1:3,1:ntrans) = lx,ly,lz values of transitions
c     map(nroot,drt) = state_ordering_index
c     maprev(state_ordering_index)= root*1000+drt 

      call wzero(3,norm,1)
c     offsets for lx,ly,lz matrices in the given space 
      cpt(1)=0
      cpt(2)=cpt(1)+kdim*kdim
      cpt(3)=cpt(2)+kdim*kdim
c     l2 values
      cpt(4)=cpt(3)+kdim*kdim
      cpt(5)=cpt(4)+kdim*kdim
        do i=1,ntrans
        ind1=map(tidx(1,ntrans),tidx(2,ntrans))
        ind2=map(tidx(3,ntrans),tidx(4,ntrans))
        l2=tlval(1,ntrans)**2
        l2=l2+tlval(2,ntrans)**2
        l2=l2+tlval(3,ntrans)**2
        if (ind1.gt.ind2) then 
        matrix(cpt(4)+ind1*(ind1-1)+ind2)=l2
        else
        matrix(cpt(4)+ind2*(ind2-1)+ind1)=l2
        endif

        matrix(cpt(1)+kdim*(ind1-1)+ind2)=tlval(1,ntrans)
        matrix(cpt(1)+kdim*(ind2-1)+ind1)=-tlval(1,ntrans)
        norm(1)= norm(1)+ abs(tlval(1,ntrans))
        matrix(cpt(2)+kdim*(ind1-1)+ind2)=tlval(2,ntrans)
        matrix(cpt(2)+kdim*(ind2-1)+ind1)=-tlval(2,ntrans)
        norm(2)= norm(2)+ abs(tlval(2,ntrans))
        matrix(cpt(3)+kdim*(ind1-1)+ind2)=tlval(3,ntrans)
        matrix(cpt(3)+kdim*(ind2-1)+ind1)=-tlval(3,ntrans)
        norm(3)= norm(3)+ abs(tlval(3,ntrans))
        enddo ! end of: do i=1,ndim
c
      call prblkt('Lx-matrix (imaginary part)',matrix(cpt(1)+1),
     & kdim,kdim,kdim,cmaprev,cmaprev,1,nlist)
      call prblkt('Ly-matrix (imaginary part)',matrix(cpt(2)+1),
     & kdim,kdim,kdim,cmaprev,cmaprev,1,nlist)
      call prblkt('Lz-matrix (imaginary part)',matrix(cpt(3)+1),
     & kdim,kdim,kdim,cmaprev,cmaprev,1,nlist)
      call prblkt('L2-matrix ',matrix(cpt(4)+1),
     & kdim,kdim,kdim,cmaprev,cmaprev,1,nlist)
c

!   evd: LWORK=1+6*N+2*N*N
        lwork = 1+6*kdim+2*kdim*kdim
        liwork = 2+6*kdim 
        if (liwork+lwork.gt.1000000) 
     .       CALL bummer('insufficient space=',liwork+lwork,2)
        info=0
        CALL dsyevd_wr('V','U',kdim,matrix(cpt(4)+1),
     .       kdim,matrix(cpt(5)+1),core(1),lwork,core(lwork+1),
     .       liwork,info)
        write(nlist,*) 'eigenvalues and eigenvectors of L**2 '
        CALL prvblk(matrix(cpt(4)+1),matrix(cpt(5)+1),kdim,kdim,kdim,
     .       0,0,cmaprev,cmaprev,"l2",3,nlist)

        do idir=1,3
          do i=1,kdim*kdim
c         cmatrix(i)=cmplx(matrix(cpt(idir)+i))
          temp=matrix(cpt(idir)+i)
          cmatrix(i)=cmplx(0,temp)
          enddo ! end of: do i=1,kdim*kdim
        lwork=2*kdim
         if (norm(idir).le.1.0d-10) then
          call wzero(mdim,ceig,1)
          call wzero(mdim*mdim,cvec,1)
          icount=0
            do i=1,kdim
            cvec(icount+i)=one
            icount=icount+kdim
            enddo
         else
          call cgeev('n','v',kdim,cmatrix,kdim,ceig,cdum,1,cvec,kdim,
     &    cwork,lwork,rwork,info)
          if (info.ne.0) stop'error'
         endif
c
        write(nlist,*)
        write(nlist,*)'------------------------------------------'
        icount = 0
          do i=1,kdim
          write(nlist,'(i2,2F12.6,a1)')i,ceig(i),'i'
          write(nlist,'(6(2F10.5,a2))')(cvec(icount+j),'i|',j=1,kdim)
          icount = icount + kdim
          enddo ! end of: do i=1,kdim
        enddo ! end of: do idir=1,3
c
      call bummer('normal termination',0,3)
      stop 'end of lvalue'
      end
c
c*************************************************************
c

       subroutine addstate(root,drt,kdim,map,mdim,maprev)
       implicit none
       integer root,drt,kdim,mdim,map(mdim,mdim),maprev(mdim)
       integer i,j
       if (map(root,drt).le.0) then
           kdim=kdim+1
           if (mdim .lt. kdim) call bummer
     &        ('Too many states (increase dimension in program) =',
     .         kdim, 2)
           map(root,drt)=kdim
           maprev(kdim)=root*1000+drt
       endif
       return
       end


