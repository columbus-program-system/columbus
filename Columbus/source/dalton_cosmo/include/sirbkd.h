C
C     Define block data modules needed for sirius as external
C     to be sure they are included.
C
      EXTERNAL  SBDORB, SBDTAP, BDGETD
C
C     -----------------------------------------------------------------
C
C     The following COMDECKS are used for backup of sirius variables
C
C     /INPBAK/ is a copy of /INFINP/
C
