#if defined (VAR_TESTIBM)
      PARAMETER (MXSHEL =  15, MXPRIM =  60, MXCORB =  30,
#else
#if defined (VAR_SIRBIG)
      PARAMETER (MXSHEL = 400, MXPRIM = 2000, MXCORB = 1200,
#else
      PARAMETER (MXSHEL = 200, MXPRIM = 800, MXCORB = 400,
#endif
#endif
     *           MXORBT = MXCORB*(MXCORB + 1)/2)
