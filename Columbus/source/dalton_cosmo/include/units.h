C
C     XF* conversion factor from a.u. to   *
C     XT* conversion factor to   a.u. from *
C
C     See CODATA 86, Europhysics News 18 (5) May 1987, p.65
C
      PARAMETER ( XFAMU  = 1822.887 D0, XTKAYS = 2.1947463068D5,
     &            XTKMML =  974.864 D0, XTANG  = 0.52917706  D0,
     &            XKJMOL = 2625.5000D0, XKCMOL = XKJMOL/4.184D0,
     &            XFSEC  = 2.4189 D-17, XAJOUL = 4.359828D0)
