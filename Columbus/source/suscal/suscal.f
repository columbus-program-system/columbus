!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program suscal
      implicit real*8(a-h,o-z)
c
c***scale3*******fortran77*version*for*ibm*3090*************************
c                                                                      *
c     program scale3 for least-squares fitting of force constants      *
c                                                                      *
c                 the original program was written by:                 *
c                 dr. gabor pongor                                     *
c                 laboratory of theoretical chemistry                  *
c                 eotvos lorand university                             *
c                 h-1088 budapest                                      *
c                 muzeum krt. 6-8.                                     *
c                 hungary/europe                                       *
c                                                                      *
c***********************************************************************
c
c     adapted for recalculation of dipole moment derivatives
c     according to pulay's method (m.kofranek, sept.1987)
c     ref: pulay et al., jacs 101,10,2550 (may 9,1979)
c
c***********************************************************************
c     version for ibm 3090 august 1989 / m. kofranek                   *
c***********************************************************************
c     phonondispersion    october 1989 / m. kofranek                   *
c***********************************************************************
c
c                             input data
c
c.... ntype is the number of the different types of computations.
c.... nmax is the max. dimension of a species.
c.... nm is the max. number of the species, calc. simultaneously.
c.... nd is the max. number of the normal scaling factors (fixed=nd,
c....    vari=nd).
c.... nd1 is the max. number of the extra scaling factors (fixed=nd1,
c....    vari=nd1).
c.... nfreq is the max. number of the sum of the numbers of the species'
c....    frequencies, calc. simultaneously.
c.... nextra is the max. number of the extra scaled species, calc.
c....    simultaneously.
c.... neel is the max. number of the extra scaled f-matrix elements in a
c....    species.
c.... ib file = input
c.... ik file = output
c.... dimension of the linear array begin is ntype.
c.... dimension of the linear arrays sv and sf is nd.
c.... dimension of the linear arrays sef and sev is nd1.
c.... dimension of the linear arrays osv and sv1 is nd + nd1.
c.... dimension of the two-dimensional arays istef and istev are
c....    3,nd1.
c.... dimensions of the two-dimensional arrays istf and istv are
c....    3, nd.
c.... dimension of the linear arrays ixl, ixm, dipx, dipy and dipz is
c....    nmax.
c.... dimensions of the two-dimensional array itext are 18, nm.
c.... dimension of the linear arrays ndi and jlow is nm.
c.... dimension of the linear array sigma is nsigm.
c.... dimension of the linear arrays ex, mut, eig, ss, we, adj and vod
c....    is nfreq.
c.... dimension of the linear array tj is nt.
c.... dimension of the linear array h is nh.
c.... dimension of the linear array q is nq.
c.... dimension of the linear array iex is nextra.
c.... dimension of the two-dimensional arrays iepa and mue are neel,
c....    nextra.
c
c
c     error codes
c     ier  jer
c      1    j   the number of frequencies for the j-th molecule/species
c               is greater than nmax
c      2    -   total number of frequencies is greater than nfreq
c      3    j   error in specification of the j-th scale factor
c      4    -   gmat or gpre card not in its proper place
c      5    -   fmat or fpre card not in its proper place
c
c***********************************************************************
c
c     # 20-feb-02 some obsolete hollerith junk removed. -rls
c
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      parameter(nsigm=(nd+nd1)*(nd+nd1+1)/2,kmax=1000)
      integer yy,my,dy,hy,py,sy,hd
      logical mis,twol,test,dipol
      dimension sigma(nsigm),vod(nfreq),sv(nd),
     1 ixl(nmax),ixm(nmax),osv(nd+nd1),sv1(nd+nd1),sev(nd1)
      dimension sstak(nmax*nmax)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      common/egy/ q(nq1),h(nh)
      common/har/ tj(nt)
      common/texts/ itext(18,nm),istf(3,nd),istv(3,nd),
     & istef(3,nd1),istev(3,nd1)
      common/extra/ iex(nextra),iepa(neel,nextra),mue(neel,nextra),
     & sef(nd1)
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      common /coord/ xsol(ncorm,2*nmax),ysol(ncorm,2*nmax),
     & zsol(ncorm,2*nmax),xist(ncorm,2*nmax),yist(ncorm,2*nmax),
     & zist(ncorm,2*nmax)
      common /bmat/ am(3*ncorm,3*ncorm)
      common/dip/ dipx(nmax,0:nism),dipy(nmax,0:nism),dipz(nmax,0:nism)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common/bem/ adj(nfreq)
      common/tset/ test,mis,dipol
      common/factor/ fac,hfac2
      common/matrix/ fstart(nh,nm),fscal(nh,nm),gmcf(nh,nm)
      common /phonon/ zvect,phirot,neighn,kval
      dimension nis(nm)
      character*40 filnam
      character*4 a(nd)
      character*4 star,      en
      parameter(star='****', en='stop')
      character*1 xxxf,   xxxv
      parameter(xxxf='f', xxxv='v')
      character*2 xxxef,    xxxev
      parameter(xxxef='ef', xxxev='ev')
      character*4 begin(ntype)
      data begin(1)/'g   '/, begin(2)/'gf  '/, begin(3)/'opt '/
      ib=5
      ik=6
      open(ib,file='suscin',form='formatted',status='unknown')
      open(ik,file='suscalls',form='formatted',status='unknown')
      fac=1.302794d0
      hfac2=half*(fac*fac)
   10 read(ib,11) a
   11 format(20a4)
      if(a(1).eq.en) goto 20
      do 12 i=1,ntype
         if(a(1).eq.begin(i)) write(ik,13)
   12 continue
   13 format(' input data'/)
      write(ik,14) a
   14 format(1x,20a4)
      goto 10
   20 continue
      write(ik,14) a
      rewind ib
   30 continue
      do 1000 i=1,nd
         osv(i)=zero
         sv1(i)=zero
 1000 continue
      do 1100 i=1,nmax
         ixl(i)=0
         ixm(i)=0
 1100 continue
      do 1200 i=1,nsigm
         sigma(i)=zero
 1200 continue
      do 1300 i=1,nfreq
         adj(i)=zero
         vod(i)=zero
 1300 continue
      ier=0
      jer=0
      call inp(
     &  mv,     sv,   mev,   sev,
     &  iter,   err,  bpar,  step,
     &  ity,    mis,  test,  dipol,
     &  ier,    jer,  nis,   ip)
      if(.not.mis) goto 32
      write(ik,31) ier,jer
   31 format(//' error in input  ier =',i2,' jer =',i2/)
      goto 30
   32 continue
      call fgin (mis,ier,jer,nis)
      if(.not.mis) goto 36
      if(neighn.ne.0.and.mv.eq.0.and.mev.eq.0) then
         iq=0
         if(mf.ne.0) then
            iq=1
            do 377 i=1,mf
               sf(i)=sqrt(sf(i))
377         continue
         endif
         do 5100 nmole=1,nmo
            call scaf(mv,sv,mev,sev,mis)
            if(iq.eq.1) then
               write(ik,125)
               il=1
               m=ndi(1)
               mm=m*(m+1)/2
               do 430 i=1,m
                  iu=il+i-1
                  write(ik,135) (fscal(j,nmole),j=il,iu)
                  if(ip.ne.0) write(ip,140) (fscal(j,nmole),j=il,iu)
                  il=iu+1
430            continue
               call phofmr(fscal(mm+1,nmole),m,neighn,0,ik,ip)
            endif
            call phongf(iq,ip,nis)
5100     continue
         goto 30
      endif
      write(ik,31) ier,jer
      goto 30
   36 continue
      if(ity.ne.1) goto 40
      m=ndi(1)
      call sdiag2(m,m,q,eig,q)
      write(ik,33)
   33 format(//' eigenvalues and eigenvectors of g'/)
      il=1
      m1=m-1
      do 34 i=1,m
         iu=il+m1
         write(ik,35) eig(i),(q(j),j=il,iu)
         il=iu+1
   34 continue
   35 format(1x,1p,e16.5,0p,4x,5f11.5,/,(21x,5f11.5))
      s=one
      do 43 i=1,m
         s=s*eig(i)
   43 continue
      write(ik,46) s
   46 format(//' determinant of g=',1pe15.5)
      goto 30
   40 continue
      if(mf.eq.0) goto 310
      do 300 i=1,mf
         sf(i)=sqrt(sf(i))
  300 continue
  310 continue
      mvt=mv+mev
      if((ity.ne.2).and.(mvt.gt.0)) goto 55
      if(mf.le.0) goto 100
      do 2600 nmole=1,nmo
         m=ndi(nmole)
         mm=(m*(m+1))/2
         il=jlow(nmole)
         call scaf(mv,sv,mev,sev,mis)
         if(.not.mis) goto 49
         write(ik,48)
48       format(//' error in the definition of the scaling factors')
         goto 30
49       continue
2600  continue
      goto 100
   55 continue
c.... newton-raphson method
      kount=0
      res=zero
   60 continue
      do 400 i=1,mv
      sv(i)=sqrt(sv(i))
  400 continue
      kount=kount+1
c.... fill jacobi matrix (j) in array tj
      call f1(mv,sv,mev,sev)
      if(mis) goto 30
c.... fill (j+)wj matrix in array h
      m=nsumf
      lkk=0
      iend=m*(mvt-1)+1
      do 62 l=1,iend,m
      il1=l-1
      do 62 kk=1,iend,m
      lkk=lkk+1
      ikk1=kk-1
      s=zero
      do 61 i=1,m
      il=il1+i
      ikk=ikk1+i
      s=s+tj(il)*we(i)*tj(ikk)
   61 continue
      h(lkk)=s
   62 continue
      write(ik,69) kount
   69 format(/' *',i3,'*'/)
      mvt1=mvt+1
      iend=mvt*mvt
      write(ik,600)
  600 format(' the diagonal elements of (j+)wj matrix'/)
      write(ik,410) (h(i),i=1,iend,mvt1)
      d=one
      do 620 i=1,iend,mvt1
      d=d*h(i)
      h(i)=h(i)+bpar
  620 continue
      write(ik,2930) d
 2930 format(/' the product of the diagonal elements of (j+)wj',
     & ' matrix =',1pe15.8/)
c.... let ((j+)wj)-1 be in array h
      call osinv(h,mvt,d,tenm8,ixl,ixm)
      write(ik,63) d
   63 format(' the determinant of (j+)wj =',1pe15.8/)
      if(d) 64,100,64
   64 continue
      call aprod(eig,1,nsumf,ten3)
      call aprod(ex,1,nsumf,ten3)
      do 1800 nmole=1,nmo
c     write(ik,1810) (itext(i,nmole),i=1,18)
 1810 format(//,1x,17a4,a4,/)
      call freq
 1800 continue
      s=zero
      wsum=zero
      do 1880 i=1,nsumf
      del=eig(i)-ex(i)
      s=s+we(i)*(del*del)
      wsum=wsum+we(i)
 1880 continue
      res=s/wsum
      dmean=sqrt(res)
      write(ik,1840) dmean
 1840 format(/' total mean deviation (cm-1)=',f6.1/)
c.... fill gradient vector (d) in array ss
      call aprod(eig,1,nsumf,tenm3)
      call aprod(ex,1,nsumf,tenm3)
      l=0
      iend=m*(mvt-1)+1
      do 66 kk=1,iend,m
      l=l+1
      ikk1=kk-1
      s=zero
      do 65 i=1,m
      ikk=ikk1+i
      s=s+we(i)*(eig(i)-ex(i))*tj(ikk)
   65 continue
      ss(l)=s
   66 continue
c.... fill delta(x) in array eig *** generate old(x) and new(x)
      ddif=zero
      iend=mvt*(mvt-1)+1
      j=0
      do 68 k=1,iend,mvt
      j=j+1
      kl1=k-1
      s=zero
      do 67 l=1,mvt
      kl=kl1+l
      s=s-h(kl)*ss(l)
   67 continue
      if(j.gt.mv) goto 550
      osv(j)=sv(j)*sv(j)
      sv1(j)=(sv(j)+s)*(sv(j)+s)
      goto 560
  550 jmv=j-mv
      osv(j)=sev(jmv)
      sv1(j)=sev(jmv)+s
  560 eig(j)=sv1(j)-osv(j)
      if(abs(eig(j)).gt.ddif) ddif=abs(eig(j))
   68 continue
      fmult=one
      if(ddif.gt.step) fmult=step/ddif
      do 70 i=1,mvt
      eig(i)=fmult*eig(i)
      if(i.gt.mv) goto 570
      sv(i)=osv(i)+eig(i)
      goto 70
  570 imv=i-mv
      sev(imv)=osv(i)+eig(i)
   70 continue
      write(ik,660)
  660 format(/' miscellaneous'/)
      write(ik,410) (osv(i),i=1,mvt)
      write(ik,410) (ss(i),i=1,mvt)
      write(ik,410) (eig(i),i=1,mvt)
      write(ik,410) (sv(i),i=1,mv),(sev(i),i=1,mev)
  410 format(1x,1p,5e12.4,3x,5e12.4)
      if((kount.ge.iter).or.(ddif.le.err)) goto 75
      goto 60
   75 continue
      do 420 i=1,mv
      sv(i)=sqrt(sv(i))
  420 continue
      k=0
      do 2700 i=1,mvt
      ji1=(i-1)*mvt
      do 2700 j=1,i
      ji=ji1+j
      k=k+1
      sigma(k)=h(ji)
 2700 continue
      do 2000 nmole=1,nmo
      m=ndi(nmole)
      mm=(m*(m+1))/2
      call scaf(mv,sv,mev,sev,mis)
      if(.not.mis) goto 76
      write(ik,48)
      goto 30
   76 continue
 2000 continue
  100 continue
      if(mv.eq.0) goto 650
      do 640 i=1,mv
      sv(i)=sv(i)*sv(i)
  640 continue
  650 continue
      if(mf.eq.0) goto 510
      do 500 i=1,mf
      sf(i)=sf(i)*sf(i)
  500 continue
  510 continue
      write(ik,110)
  110 format(//' ***results***'/)
      iq=1
      if((mv.le.0).and.(mf.le.0)) iq=0
      if(iq.eq.0) goto 123
      write(ik,113)
  113 format(//' fixed and/or optimized scaling factors'/,
     & ' type/no.',6x,'name',9x,'value',4x,'nmol',8x,
     & 'connecting coordinates'/)
      if((mf.le.0).and.(mv.le.0)) goto 4000
      twol=.false.
      iftc=-1
      lim=mf
  114 continue
      if(lim.eq.0) goto 118
      if(twol) iftc=+1
      do 117 i=1,lim
      numf=0
      do 115 j=1,nsumf
      if((iftc*mut(j)).ne.i) goto 115
      numf=numf+1
      ss(numf) = (j)
  115 continue
      if(numf.eq.0) goto 120
      if(.not.twol) write(ik,116) xxxf,i,(istf(j,i),j=1,3),sf(i)
      if(twol)      write(ik,116) xxxv,i,(istv(j,i),j=1,3),sv(i)
  116 format(3x,a1,3x,i2,4x,2a4,a2,4x,f7.4)
      nyom=1
      nmole1=1
      j=0
    1 k=0
    2 j=j+1
      if(j.gt.numf) goto 8
      ics=ss(j)+tenm1
      do 3 nunvs=1,nmo
      ics=ics-ndi(nunvs)
      if(ics.gt.0) goto 3
      if((k.ne.0).and.(nunvs.ne.nunvs1)) goto 5
      k=k+1
      if(k.gt.10) goto 4
      ixm(k)=ics+ndi(nunvs)
      nunvs1=nunvs
      goto 2
    3 continue
      goto 120
    4 k=k-1
    5 j=j-1
    8 continue
      if(nyom.eq.1) write(ik,6) nunvs1,(ixm(jj),jj=1,k)
      if(nyom.ne.1) write(ik,7) nunvs1,(ixm(jj),jj=1,k)
    6 format(/38x,i2,5x,10(4x,i2))
    7 format(39x,i2,5x,10(4x,i2))
      if(j.gt.numf) goto 117
      nyom=2
      goto 1
  117 continue
  118 continue
      write(ik,119) (star,i=1,8)
  119 format(1x,8a4)
      if(twol.or.mv.eq.0) goto 4000
      twol=.true.
      lim=mv
      goto 114
 4000 if(mev.le.0) goto 4041
      twol=.false.
      iftc=+1
      lim=mev
 4002 continue
      do 4040 i=1,lim
      if(.not.twol) write(ik,4003) xxxev,i,(istev(j,i),j=1,3),sev(i)
      if(twol) write(ik,4003) xxxef,i,(istef(j,i),j=1,3),sef(i)
 4003 format(3x,a2,2x,i2,4x,2a4,a2,4x,f7.4)
      nyom=1
      do 4030 nmole=1,nextra
      ics=iex(nmole)
      if(ics.eq.0) goto 4040
      k=0
      do 4010 j=1,ics
      if(iftc*mue(j,nmole).ne.i) goto 4010
      k=k+1
      if(k.gt.neel) goto 120
      ij=iepa(j,nmole)
c.... inverse idia (in the following 3 lines)
      ipa=(sqrt(eight*float(ij)+one)-one)/two+alone
      ixl(k)=ipa
      ixm(k)=ij-ipa*(ipa-1)/2
 4010 continue
      if(k.eq.0) goto 4030
      il=1
 4020 iu=il+4
      if(k.lt.iu) iu=k
      if(nyom.eq.1) write(ik,4025) nmole,(ixl(jj),ixm(jj),jj=il,iu)
 4025 format(/38x,i2,4x,5(5x,'(',i2,',',i2,')'))
      if(nyom.eq.2) write(ik,4026) nmole,(ixl(jj),ixm(jj),jj=il,iu)
 4026 format(38x,i2,4x,5(5x,'(',i2,',',i2,')'))
      if(iu.eq.k) goto 4030
      nyom=2
      il=iu+1
      goto 4020
 4030 continue
 4040 continue
      if(twol) goto 122
 4041 if(mvt.gt.0) write(ik,119) (star,i=1,8)
      if(mef.le.0) goto 122
      twol=.true.
      iftc=-1
      lim=mef
      goto 4002
  120 continue
      write(ik,121)
  121 format(' error in factors'/)
      goto 30
  122 continue
      if((mvt.le.0).or.(nsumf.le.mvt)) goto 123
c.... calculation of the standard errors and correlation coefficients.
c.... v1 is the variance of an observation of unit weight.
      v1=(tenm6*res)/dfloat(nsumf-mvt)
      do 2802 i=1,nsumf
      s=zero
      do 2801 j=1,mvt
      ij1=(j-1)*nsumf
      do 2801 l=1,mvt
      il=(l-1)*nsumf+i
      ij=ij1+i
      jl=idia(j,l)
      s=s+tj(ij)*sigma(jl)*tj(il)
 2801 continue
      vod(i)=ten3*sqrt(v1*s)
 2802 continue
      do 2830 i=1,mvt
      ii=(i*(i+1))/2
      osv(i)=sigma(ii)
      if(i.eq.1) goto 2820
      i1=i-1
      ij=ii1
      do 2810 j=1,i1
      ij=ij+1
      sigma(ij)=sigma(ij)/sqrt(sigma(ii)*osv(j))
 2810 continue
 2820 continue
      ii1=ii
      sigma(ii)=sqrt(v1*sigma(ii))
 2830 continue
      write(ik,2840)
 2840 format(' err-corr matrix'/)
      il=1
      do 2850 i=1,mvt
      iu=il+i-1
      write(ik,135) (sigma(j),j=il,iu)
      il=iu+1
 2850 continue
  123 continue
      do 2100 nmole=1,nmo
      m=ndi(nmole)
      mm=(m*(m+1))/2
      ill=jlow(nmole)
      write(ik,2200) (star,i=1,18)
 2200 format(/,1x,18a4,/)
      if(iq.eq.0) goto 145
      write(ik,125)
  125 format(/' final f matrix'/)
      il=1
      do 130 i=1,m
      iu=il+i-1
      write(ik,135) (fscal(j,nmole),j=il,iu)
      if(ip.ne.0) write(ip,140) (fscal(j,nmole),j=il,iu)
      il=iu+1
  130 continue
  135 format(/,(1x,5f12.6,3x,5f12.6))
  140 format(8f10.6)
      if(neighn.ne.0) call phofmr(fscal(mm+1,nmole),m,neighn,0,ik,ip)
 2300 format(20a4)
  145 continue
      if(neighn.eq.0) then
       do 146 i=1,mm
        if(iq.eq.0) h(i)=fstart(i,nmole)
        if(iq.eq.1) h(i)=fscal(i,nmole)
  146  continue
       k=0
       do 77 i=1,m
        ji1=(i-1)*m
        do 77 j=1,i
         ij=(j-1)*m+i
         ji=ji1+j
         k=k+1
         q(ij)=h(k)
         q(ji)=q(ij)
   77  continue
      else
       co=1.d0
       si=0.d0
       m1=mm+1
       if(iq.eq.0) then
        do 81 i=0,neighn
        call kuelma(m,m1,co,si,fstart(1,nmole),q,i)
   81   continue
       else
        do 82 i=0,neighn
        call kuelma(m,m1,co,si,fscal(1,nmole),q,i)
   82   continue
       endif
       write(ik,126)
  126  format(/' final f matrix at k=0'/)
       do 129 i=1,m
        ij=m*(i-1)
        do 128 j=1,i-1
  128    q(j+ij)=q(m*(j-1)+i)
        write(ik,135) (q(j+ij),j=1,i)
        if(ip.ne.0) write(ip,140) (q(j+ij),j=1,i)
  129  continue
      endif
      do 79 i=1,mm
       h(i)=gmcf(i,nmole)
   79 continue
      call gfeig(m,m,q,h,eig,ill)
      call aprod(eig,ill,ill-1+m,ten3)
      call aprod(ex,ill,ill-1+m,ten3)
      write(ik,3100)
 3100 format(//)
      call freq
      write(ik,180)
  180 format(//' frequencies and l transpose'/)
      call inpri(q,eig,m,0,ik,0,one,ill)
      mk=m*m
      do 181 j=1,m
      do 181 i=1,m
      ji=(i-1)*m+j
      ii=(j-1)*m+i
      sstak(ji)=q(ii)
  181 continue
      if(.not.dipol) goto 186
      write(ik,183)
  183 format(//,12x,'dipole moment derivatives',10x,
     & 'square and absolute intensity'//,18x,'d/(a*amu**.5)',
     & 16x,'d**2/(amu*a**2)',4x,'cm/mmol',5x,'km/mol',6x,'cm**2/mol',
     & 2x,'cm**-2*atm**-1 (273.15k)',/)
       do 185 j=1,m
      ij=(j-1)*m
      s1=zero
      s2=zero
      s3=zero
      do 188 i=1,m
      ij=ij+1
      s1=s1+q(ij)*dipx(i,nmole-1)
      s2=s2+q(ij)*dipy(i,nmole-1)
      s3=s3+q(ij)*dipz(i,nmole-1)
  188 continue
      s4=s1**2+s2**2+s3**2
      s5=s4*4225.47d0
      s6=s5*.01d0
      s7=0.d0
      if(eig(j).ne.0.d0) s7=s5*1.d3/eig(j)
      s8=s5*12.19d0/273.15d0
      write(ik,190) j,s1,s2,s3,s4,s5,s6,s7,s8
  190 format(1x,i3,3x,3f12.5,3x,f13.6,4f13.2)
  185 continue
  186 continue
      if(neighn.ne.0) then
        call phongf(iq,ip,nis)
        goto 2100
      endif
      tol=tenm8
      call osinv(q,m,d,tol,ixl,ixm)
      write(ik,206) d
  206 format(//' frequencies and l-1 matrix (det(l)=',1pe12.5,')'/)
      if(d) 207,30,207
  207 continue
      do 208 i=1,m
      ji1=(i-1)*m
      do 208 j=1,i
      ji=ji1+j
      ij=(j-1)*m+i
      s=q(ij)
      q(ij)=q(ji)
      q(ji)=s
  208 continue
      call inpri(q,eig,m,0,ik,0,one,ill)
      write(ik,240)
  240 format(//' frequencies and the m matrix (ted)'/)
      do 245 j=1,m
      do 245 i=1,m
      ji=(i-1)*m+j
      ii=(j-1)*m+i
      q(ji)=q(ji)*sstak(ii)
  245 continue
      call inpri(q,eig,m,0,ik,0,one,ill)
        if(neighn.eq.0.and.ity.eq.2) then
         call fceig(iq,ik)
         call moldenout(ik)
        endif
 2100 continue
      if((ity.ne.3).or.(mvt.le.0).or.(nsumf.le.mvt)) goto 2915
      write(ik,2900)
 2900 format(//' the calculated frequencies and their standard',
     & ' errors'/,2x,'no.',3x,'nmol',3x,'nfreq',3x,'calc(cm-1)',
     & 3x,'st. error(cm-1)'/)
      i=0
      do 2912 nmole=1,nmo
      m=ndi(nmole)
      l=0
      do 2911 j=1,m
      l=l+1
      i=i+1
      write(ik,2913) i,nmole,l,eig(i),vod(i)
 2911 continue
 2912 continue
 2913 format(1x,i3,4x,i3,5x,i3,2x,f10.2,4x,f10.2)
 2915 continue
      goto 30
      end
c
c***********************************************************************
c
      block data
      implicit real*8(a-h,o-z)
c
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      data zero/0.0d0/,half/0.5d0/,one/1.0d0/,two/2.0d0/,
     1   ten3/1.0d3/,tenm1/1.0d-1/,tenm3/1.0d-3/,tenm6/1.0d-6/,
     2   tenm8/1.0d-8/,alone/0.999999d0/,sixt/16.0d0/,eight/8.0d0/,
     3   psix/0.06250d0/,ten2/1.0d2/,tenm2/1.0d-2/
      end
c
c**********************************************************************
c
      subroutine fgin (lab,ier,jer,nis)
c
c     # 20-feb-02 some obsolete hollerith junk removed. -rls
c
      implicit real*8(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      logical lab,tes,lola,dipol,dical
      dimension nis(nm),xistp(ncorm)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common /coord/ xsol(ncorm,2*nmax),ysol(ncorm,2*nmax),
     & zsol(ncorm,2*nmax),xist(ncorm,2*nmax),yist(ncorm,2*nmax),
     & zist(ncorm,2*nmax)
      common /bmat/ am(3*ncorm,3*ncorm)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      common/egy/ q(nq1),h(nh)
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common/matrix/ fstart(nh,nm),fscal(nh,nm),gmcf(nh,nm)
      common /phonon/ zvect,phirot,neighn,kval
c
      character*4 szo, word(10), a(5)
c
      real*8     xggeo
c
      real*8    zero,     one,     two
      parameter(zero=0d0, one=1d0, two=2d0)
c
      data word/'gmat', 'fmat', 'calc', 'gpre', 'fpre',
     & 'cmat', 'x   ', 'y   ', 'z   ', 'bmat' /
c
      ix  = 5
      ik  = 6
      icm = 0
      clx = zero
      cly = zero
      clz = zero
      szo = ' '
      do 400 nmole=1,nmo
      nmole1=nmole-1
      m=ndi(nmole)
      mm=(m*(m+1))/2
      if(nis(nmole).eq.0) goto 102
      nis0=nmole
      read(ix,11) szo,a(1),xggeo,(a(i),i=2,5)
   11 format(a4,6x,a4,f6.0,4(a4,6x))
      if(szo.eq.word(10)) then
        backspace ix
        read(ix,21)xnatm,xkmas,xlmas,xkangs,xptr
   21   format(10x,7f10.6)
        natm=xnatm+.1
        kmas=xkmas+.1
        lmas=xlmas+.1
        kangs=xkangs+.1
        iptr=xptr+.1
        nistp=nis(nis0)-1
      if (nistp.ne.0) then
        do 94 im=1,nistp
        read(ix,96)(xistp(jm),jm=1,natm)
        do 94 jm=1,ndi(nmole)
        nxstp(jm,im)=xistp(jm)+.1
   96   format(8f10.6)
   94   continue
      endif
        call auslen (.false.,natm,0,0,nistp,kmas,kangs,lmas,ix,ik,iptr)
        read(ix,11) szo,a(1),xggeo,(a(i),i=2,5)
      endif
      if((szo.eq.word(1)).or.(szo.eq.word(4))) goto 310
      ier=4
      jer=0
      goto 115
  310 continue
  102 write (ik,530)
  530 format (21x,'nuclear coordinates (a) and masses',/)
      do 103 i=1,natm
  103 write (ik,550) i,xa(i),ya(i),za(i),xmas(i,nmole-nis0)
  550 format (5x,i2,3x,3f12.7,3x,f12.6)
      write(ik,101)
  101 format(/' g matrix'/)
      if(szo.eq.word(4)) goto 315
      if(a(1).eq.word(3)) then
      iggeo=xggeo+.1
      if(a(2).eq.word(6)) icm=1
      if(a(3).eq.word(7)) clx= one
      if(a(4).eq.word(8)) cly= one
      if(a(5).eq.word(9)) clz= one
      call gmtcal(m,nmole-nis0,iggeo,ix,ik,icm,clx,cly,clz)
      call inpri(q,ss,m,0,ik,0,.5d0,1)
      goto 330
      endif
      call inpri(q,ss,m,ix,ik,0,.5d0,1)
      goto 330
  315 continue
      if(m.ne.ndi(nmole1)) goto 115
      do 325 i=1,m
      ji1=(i-1)*m
      ll=i-1
      do 320 j=1,ll
      ji=ji1+j
      ij=(j-1)*m+i
      q(ij)=q(ji)
  320 continue
      ii=ji1+i
      q(ii)=eig(i)
  325 continue
      call inpri(q,ss,m,0,ik,0,.5d0,1)
  330 continue
cmd    ity not defined !!!!!
cmd    if(ity.eq.1) goto 120
cmd
      if(a(1).eq.word(3).and.nis(nmole).eq.0) goto 235
      read(ix,11) szo
      if((szo.eq.word(2)).or.(szo.eq.word(5))) goto 332
      ier=5
      jer=0
      goto 115
  332 continue
      write(ik,107)
  107 format(//' f matrix'/)
      if(szo.eq.word(5)) goto 335
      il=1
      do 110 i=1,m
      iu=il+i-1
      read(ix,111) (h(j),j=il,iu)
      write(ik,112) (h(j),j=il,iu)
      il=iu+1
  110 continue
  111 format(8f10.5)
  112 format((1x,5f12.6,3x,5f12.6))
      if(neighn.ne.0) call phofmr(fstart(mm+1,nmole),m,neighn,ix,ik,ip)
      goto 340
  335 continue
      if(m.ne.ndi(nmole1)) goto 115
  235 continue
      do 1 i=1,mm
    1 h(i)=fstart(i,nmole1)
      if(neighn.ne.0) then
      do 334 i=1,m*m*neighn
      fstart(i+mm,nmole)=fstart(i+mm,nmole1)
  334 continue
      endif
      il=1
      do 336 i=1,m
      iu=il+i-1
      if(nis(nmole).ne.0) write(ik,112) (h(j),j=il,iu)
      il=iu+1
  336 continue
  340 continue
      do 2 i=1,mm
    2 fstart(i,nmole)=h(i)
      do 341 i=1,m
      ji1=(i-1)*m
      ij=ji1+i
      do 341 j=i,m
      ji=ji1+j
      q(ij)=q(ji)
      ij=ij+m
  341 continue
      call chold1(q,eig,m,m,d,kit,lola)
      if(.not.lola) goto 345
      write(ik,342)
  342 format(/' g is not positive definite'/)
      if(neighn.ne.0) then
        lab=.true.
        goto 400
      endif
      goto 115
  345 d=d*(two**kit)
      write(ik,346) d
  346 format(/' determinant of g=',1pe15.5/)
      do 347 i=1,m
      ii=(i-1)*m+i
      s=q(ii)
      q(ii) = one / eig(i)
      eig(i)=s
  347 continue
      k=0
      do 350 i=1,m
      do 350 j=1,i
      ij=(j-1)*m+i
      k=k+1
      h(k)=q(ij)
  350 continue
      do 300 i=1,mm
      gmcf(i,nmole)=h(i)
  300 continue
  400 continue
      ier=0
      jer=0
      goto 120
  115 continue
      lab=.true.
  120 continue
      return
      end
c
c*********************************************************************
c
      subroutine phofmr(a,ni,n,inp,iout,ip)
      implicit real*8 (a-h,o-z)
      dimension a(*)
      ia=0
      do 3 jj=1,n
      write(iout,5) jj
    5 format(/'  f(',i1,')-matrix')
      do 4 i=1,ni
      if(inp.ne.0) read(inp,10)(a(ia+j),j=1,ni)
      write(iout,112)(a(ia+j),j=1,ni)
      if(ip.ne.0) write(ip,140) (a(ia+j),j=1,ni)
  112 format(/,(1x,5f12.6,3x,5f12.6))
  140 format(8f10.6)
    4 ia=ia+ni
    3 continue
   10 format(8f10.6)
      return
      end
c
c*************************************************************
c
      subroutine inp(
     &  mv,    sv,    mev,    sev,
     &  iter,  err,   bpar,   step,
     &  ity,   lab,   tes,    dipol,
     &  ier,   jer,   nis,    ip)
c
c     # 20-feb-02 some obsolete hollerith junk removed. -rls
c
      implicit real*8(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10,kmax=1000)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      character*4 szo, a(20), word(20), en
      logical lab,tes,lola,dipol,dical
      dimension sv(1),xistp(ncorm),nis(nm),nish(nm),xnis(nm)
      dimension sev(1)
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      common/egy/ q(nq1),h(nh)
      common/har/ tj(nt)
      common/texts/ itext(18,nm),istf(3,nd),istv(3,nd),
     & istef(3,nd1),istev(3,nd1)
      common/dip/ dipx(nmax,0:nism),dipy(nmax,0:nism),dipz(nmax,0:nism)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common/extra/ iex(nextra),iepa(neel,nextra),mue(neel,nextra),
     & sef(nd1)
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
c
      real*8    a45
      parameter(a45=45d0)
c
      character*4 star
      parameter(star='****')
c
      common /phonon/ zvect,phirot,neighn,kval
      data word/'g   ', 'gf  ', 'opt ', 'dim ', 'iter',
     & 'err ',  'fix ', 'var ', 'exp ', 'punc', 'nmol',
     & 'weig',  'damp', 'step', 'fext', 'dipm', 'vext',
     & 'bmat',  'calc', 'poly'/
      data en/'stop'/
      ix=5
      ik=6
      ip=0
      do 990 i=1,nd1
      sev(i)=zero
      sef(i)=zero
  990 continue
      do 1000 i=1,nd
      sv(i)=one
      sf(i)=one
 1000 continue
      do 1010 i=1,3
      do 1010 j=1,nd1
      istef(i,j)=0
      istev(i,j)=0
 1010 continue
      do 1050 i=1,3
      do 1050 j=1,nd
      istf(i,j)=0
      istv(i,j)=0
 1050 continue
      do 1100 i=1,nmax
      dipx(i,0)=zero
      dipy(i,0)=zero
      dipz(i,0)=zero
 1100 continue
      do 1110 i=1,18
      do 1110 j=1,nm
      itext(i,j)=0
 1110 continue
      do 1120 i=1,nm
      nis(i)=0
      nish(i)=0
      ndi(i)=0
      jlow(i)=0
 1120 continue
      do 1150 i=1,nfreq
      ex(i)=zero
      we(i)=one
      mut(i)=0
      eig(i)=zero
      ss(i)=zero
 1150 continue
      do 1200 i=1,nt
      tj(i)=zero
 1200 continue
      do 1300 i=1,nh
      h(i)=zero
 1300 continue
      do 1400 i=1,nq1
      q(i)=zero
 1400 continue
      do 1500 i=1,nextra
      iex(i)=0
      do 1500 j=1,neel
      iepa(j,i)=0
      mue(j,i)=0
 1500 continue
      qex=one
      iter=15
      err=tenm3
      nextr1=(neel/7+1)*nextra
      nmo=1
      jmo=1
      m=0
      mv=0
      mf=0
      mev=0
      mef=0
      natm=0
      bpar=zero
      step=tenm2
      nsumf=0
      nditer=0
      nistp=0
      neighn=0
      nish(1)=1
      lab=.false.
      tes=.false.
      lola=.false.
      dipol=.false.
      dical=.false.
   10 read(ix,11) szo,(a(i),i=1,18)
   11 format(a4,6x,17a4,a2)
      if(szo.eq.en) goto 130
      do 12 ity=1,ntype
      if(szo.eq.word(ity)) goto 13
   12 continue
      goto 10
   13 continue
      write(ik,14) (star,i=1,30),word(ity),(a(i),i=1,18),(star,i=1,30)
   14 format(/,1x,30a4,/,1x,a4,'*',3x,17a4,a2,6x,
     & '*programmed by g. pongor, 1978'/,1x,30a4)
   15 read(ix,11) szo
      backspace ix
      ntype1=ntype+1
      do 17 i=ntype1,20
      if(szo.eq.word(i)) goto 18
   17 continue
      goto 100
   18 i=i-ntype
      goto(20,25,35,40,40,50,60,70,80,85,90,40,97,40,101,20,102),i
   20 read(ix,21) (ss(j),j=1,nmo)
   21 format(10x,7f10.5,/,(8f10.5))
      if(nmo.eq.7) backspace ix
      jar=1
      j1=0
      do 22 j=1,nmo
      do 22 i=1,nish(j)
      j1=j1+1
      if(i.eq.1) nis(j1)=nish(j)
      jlow(j1)=jar
      ndi(j1)=ss(j)+tenm1
      jer=j
      ier=j1
      if((ndi(j1).le.0).or.(ndi(j1).gt.nmax)) goto 115
      jar=jar+ndi(j1)
   22 continue
      nmo=j1
      nsumf=jlow(nmo)-1+ndi(nmo)
      jer=0
      ier=2
      if((nsumf.le.0).or.(nsumf.gt.nfreq)) goto 115
      goto 15
   25 read(ix,21) xit
      iter=xit+tenm1
      goto 15
   35 read(ix,21) err
      goto 15
   40 continue
      if((nsumf.le.0).or.(nsumf.gt.nfreq)) goto 115
      read(ix,46) fval,charta,k,kk,kkk
   46 format(10x,2f10.5,2a4,a2)
      icha=charta+tenm1
      jer=mf+mv+mef+mev+1
      ier=3
      if((i.eq.12).or.(i.eq.14)) goto 500
      if((icha.le.0).or.(icha.gt.nsumf)) goto 115
      if(i.eq.5) goto 41
      mf=mf+1
      istf(1,mf)=k
      istf(2,mf)=kk
      istf(3,mf)=kkk
      mfv=mf
      sf(mf)=fval
      jel=-1
      goto 42
   41 continue
      mv=mv+1
      istv(1,mv)=k
      istv(2,mv)=kk
      istv(3,mv)=kkk
      mfv=mv
      sv(mv)=fval
      jel=+1
   42 continue
      do 45 j=1,icha
      read(ix,111) xknmo,(ss(jj),jj=1,7)
  111 format(8f10.5)
      knmo=xknmo+tenm1
      if(knmo-nmo) 43,43,115
   43 continue
      knmo1=1
      if(knmo.gt.1) then
      do 47 i=1,knmo-1
   47 knmo1=knmo1+nish(i)
      endif
      do 44 jj=1,7
      knco=ss(jj)+tenm1
      if((knco.lt.0).or.(knco.gt.nmax)) goto 115
      if(knco.eq.0) goto 45
      do 44 i=1,nish(knmo)
      l=jlow(knmo1+i-1)-1+knco
      if((l.le.0).or.(l.gt.nsumf)) goto 115
      mut(l)=jel*mfv
   44 continue
   45 continue
      goto 15
  500 ier=7
      if((icha.le.0).or.(icha.gt.nextr1)) goto 115
      if(i.eq.14) goto 501
      mef=mef+1
      istef(1,mef)=k
      istef(2,mef)=kk
      istef(3,mef)=kkk
      sef(mef)=fval
      mfv=mef
      jel=-1
      goto 502
  501 mev=mev+1
      istev(1,mev)=k
      istev(2,mev)=kk
      istev(3,mev)=kkk
      sev(mev)=fval
      mfv=mev
      jel=+1
  502 ier=6
      do 560 j=1,icha
      read(ix,111) xknmo,(ss(jj),jj=1,7)
      knmo=xknmo+tenm1
      jer=knmo
      if(knmo-nmo) 510,510,115
  510 continue
      knmo1=1
      if(knmo.gt.1) then
      do 547 i=1,knmo-1
  547 knmo1=knmo1+nish(i)
      endif
      msz=ndi(knmo1)
      if(neighn.ne.0) msz=msz*(2*neighn+1)
      mij=idia(msz,msz)
      do 550 jj=1,7
      ass=ss(jj)
      ipa=idint(ass)
      if(ipa.eq.0) goto 560
      jpa=idint(ten2*(ass-float(ipa))+tenm1)
      ij=idia(ipa,jpa)
      if(ij.ge.mij) goto 115
      do 550 i=1,nish(knmo)
      iex(knmo1+i-1)=iex(knmo1+i-1)+1
      jjj=iex(knmo1+i-1)
      if(jjj.gt.neel) goto 115
      iepa(jjj,knmo1+i-1)=ij
      mue(jjj,knmo1+i-1)=jel*mfv
  550 continue
  560 continue
      goto 15
   50 continue
      read(ix,21) uszo
      read(ix,220) (ex(j),ss(j),j=1,nsumf)
  220 format(2f10.5)
      do 400 j=1,nsumf
      ex(j)=ex(j)/ten3
      if(ss(j).ge.zero) goto 410
      we(j)=zero
      goto 400
  410 continue
      if(ss(j).gt.zero) goto 420
      if(ex(j).eq.zero) then
        we(j)=zero
      else
        we(j)=(one/ex(j))**qex
      endif
      goto 400
  420 continue
      we(j)=ss(j)*((one/ex(j))**qex)
  400 continue
      goto 15
   60 read(ix,21) pun
      ip=pun+tenm1
      if(ip.eq.0) ip=7
      write(ik,'(a,i3)')' final f matrix will be punched on unit ',ip
      open(ip,form='formatted',status='unknown')
      goto 15
   70 continue
      read(ix,21) xnmo
      nmo=xnmo+tenm1
      jmo=nmo
      if((nmo.le.0).or.(nmo.gt.nm)) goto 115
      backspace ix
      read(ix,21) xndum,(xnis(i),i=1,nmo)
      if(nmo.eq.6) backspace ix
      do 72 i=1,nmo
      nish(i)=xnis(i)+tenm1
      if(nish(i).eq.0) nish(i)=1
   72 continue
      goto 15
   80 read(ix,21) qex
      goto 15
   85 read(ix,21) bpar
      goto 15
   90 read(ix,21) step
      goto 15
   97 continue
      if(jmo.ne.1) then
        ier=6
        jer=0
        goto 115
      endif
      dipol=.true.
      read(ix,16)(a(ndip),ndip=1,2),znd,zndsp,zan,xndum,xnatm
   16 format(a4,6x,a4,6x,6f10.6)
      if(a(2).eq.word(19)) then
        dical=.true.
        nditer=znd+.1
        ndisp=zndsp+.1
        nangs=zan+.1
        natm=xnatm+.1
        read(ix,71)xmu(0),ymu(0),zmu(0)
   71   format(40x,3f10.6)
        do 99 im=1,nditer
        read(ix,19)(xxin(jm,im),ccin(jm,im),jm=1,4),xmu(im),ymu(im),
     &   zmu(im)
        do 99 jm=1,4
        if(nangs.lt.im) ccin(jm,im)=ccin(jm,im)*0.529177d0
   99   continue
   19   format(4(f4.0,f6.4),3f10.6)
       goto 15
      endif
c     do 98 nmole=1,nmo
c     m=ndi(nmole)
      m=ndi(1)
      read(ix,230) (dipx(j,0),dipy(j,0),dipz(j,0),j=1,m)
   98 continue
  230 format(3f10.5)
      goto 15
  101 if(jmo.ne.1) goto 100
      read(ix,21)xnatm,xkmas,xlmas,xkangs,xptr
      natm=xnatm+.1
      kmas=xkmas+.1
      lmas=xlmas+.1
      kangs=xkangs+.1
      iptr=xptr+.1
      nistp=nis(1)-1
      if (nistp.ne.0) then
        do 94 im=1,nistp
        read(ix,96)(xistp(jm),jm=1,natm)
        do 94 jm=1,natm
        nxstp(jm,im)=xistp(jm)+.1
   96   format(8f10.6)
   94   continue
      endif
      call auslen (dical,natm,ndisp,nditer,nistp,kmas,kangs,lmas,ix,ik,
     & iptr)
      goto 15
  102 continue
cmk   if(nmo.ne.1) then
cmk     ier=6
cmk     jer=0
cmk     goto 115
cmk   endif
      read(ix,21) xneigh,xkval,zvect,phirot
      phirot = phirot * atan(one) / a45
      neighn=xneigh+.1
      kval=xkval+.1
      ndfmat=ndi(1)*(ndi(1)+1)/2+ndi(1)*ndi(1)*neighn
      if(ndfmat.gt.nh) goto 115
      if(kval.gt.kmax) goto 115
      dical=.false.
      ndisp=0
      goto 15
  100 continue
      ier=0
      jer=0
      goto 120
  115 continue
      lab=.true.
  120 continue
      return
  130 continue
      close(ix)
      close(ik)
      call bummer('normal termination',0,3)
      stop 'end of suscal'
      end
c
c*******************************************************
c
      subroutine aprod(a,ilow,iupp,s)
      implicit real*8(a-h,o-z)
      dimension a(1)
      do 100 i=ilow,iupp
      a(i)=s*a(i)
  100 continue
      return
      end
      subroutine freq
      implicit real*8(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      logical imze
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      common/egy/ q(nq1),h(nh)
      common/har/ tj(nt)
      common/bem/ adj(nfreq)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common/texts/ itext(18,nm),istf(3,nd),istv(3,nd),
     & istef(3,nd1),istev(3,nd1)
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      character*4 star
      parameter(star='****')
c
      imze=.false.
      ik=6
      m=ndi(nmole)
      il=jlow(nmole)
      iu=il-1+m
      write(ik,150)
  150 format(' frequencies'/,2x,'no.  exp(cm-1)',4x,
     & 'calc(cm-1)',3x,'calc-exp',4x,'100*(calc-exp)/exp',6x,'weight'/)
      s=zero
      wsum=zero
      eigsum=0.0
      do 155 i=il,iu
      adj(i)=zero
      ss(i)=zero
      del=eig(i)-ex(i)
cmd
      if (eig(i).ge.0.0d+00) eigsum=eigsum+eig(i)
cmd   eigsum=eigsum+eig(i)
      s=s+we(i)*(del*del)
      wsum=wsum+we(i)
      if(ex(i).eq.zero) goto 155
      adj(i)=del
      ss(i)=ten2*del/ex(i)
  155 continue
      dmean=sqrt(s/wsum)
      l=0
      do 170 i=il,iu
      l=l+1
      write(ik,160) l,ex(i),eig(i),adj(i),ss(i),we(i)
      if(eig(i).gt.zero) goto 170
      write(ik,175)
      imze=.true.
  170 continue
  160 format(1x,i3,1x,f10.2,3x,f10.2,1x,f10.2,11x,f7.2,8x,1pe13.6)
  175 format(/78x,'attention* this nu is zero or imaginary')
      write(ik,165) dmean
      eigsum = eigsum * half
      write(ik,166) eigsum
  165 format(5x,'mean deviation (cm-1)=',f6.1/)
  166 format(5x,'zero point energy (cm-1)=',f10.2/)
      if(.not.imze) goto 500
      write(ik,100) (star,i=1,11)
  100 format(1x,11a4)
      write(ik,200) (star,i=1,2)
  200 format(1x,a4,36x,a4)
      write(ik,300) (star,i=1,2)
  300 format(1x,a4,'  remember, at least one frequency  ',a4)
      write(ik,400) (star,i=1,2)
  400 format(1x,a4,8x,'is zero or imaginary',8x,a4)
      write(ik,200) (star,i=1,2)
      write(ik,100) (star,i=1,11)
  500 continue
      return
      end
      subroutine inpri(a,x,n,ibe,iki,ifi,vezer,ka)
      implicit real*8(a-h,o-z)
c.... (a)     matrix for i/o
c.... (x)     vector for o
c.... (n)     their actual dimension
c.... (ibe)   input file (read)
c.... (iki)   output file (write)
c.... (ifi)   dump file unit number. when its value .ne. 0  and vezer
c....         .eq. 1.0 , a dump of matrix (a) will be generated on file
c....         ifi.
c.... (vezer) when its value .eq. 1.0 , write vector (x) and matrix (a)
c....         simultaneously. in all other cases read and write the low
c....         triangle of a symmetric matrix (a).
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      dimension a(n,n),x(n)
      if(vezer.eq.one) goto 40
      do 10 i=1,n
      if(ibe.ne.0) read(ibe,20) (a(i,j),j=1,i)
      write(iki,30) (a(i,j),j=1,i)
   10 continue
   20 format(8f10.5)
   30 format((1x,5f12.6,3x,5f12.6))
      return
   40 continue
      if(ifi.ne.0) rewind ifi
      l=ka-1
      do 50 i=1,n
      l=l+1
      write(iki,60) i,x(l),(a(j,i),j=1,n)
      if(ifi.ne.0) write(ifi) (a(i,j),j=1,n)
   50 continue
   60 format(1x,i3,2x,f10.2,3x,5f11.5,2x,5f11.5,
     & /,(19x,5f11.5,2x,5f11.5))
      if(ifi.ne.0) endfile ifi
      return
      end
      subroutine osinv( a,n,d,tol,l,m)
      implicit real*8(a-h,o-z)
c     parameters:  a - input matrix , destroyed in computation and repla
c                      by resultant inverse (must be a general matrix)
c                  n - order of matrix a
c                  d - resultant determinant
c            l and m - work vectors of lenght n
c                tol - if pivot element is less than this parameter the
c                      matrix is taken for singular (usually = 1.0d-8)
c     a determinant of zero indicates that the matrix is singular
c
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      dimension a(1),l(1),m(1)
      d=one
      nk=-n
      do 80 k=1,n
      nk=nk+n
      l(k)=k
      m(k)=k
      kk=nk+k
      biga=a(kk)
      do 20 j=k,n
      iz=n*(j-1)
      do 20 i=k,n
      ij=iz+i
c  10 follows
      if (abs(biga)-abs(a(ij))) 15,20,20
   15 biga=a(ij)
      l(k)=i
      m(k)=j
   20 continue
      j=l(k)
      if(j-k) 35,35,25
   25 ki=k-n
      do 30 i=1,n
      ki=ki+n
      holo=-a(ki)
      ji=ki-k+j
      a(ki)=a(ji)
   30 a(ji)=holo
   35 i=m(k)
      if (i-k) 45,45,38
   38 jp=n*(i-1)
      do 40 j=1,n
      jk=nk+j
      ji=jp+j
      holo=-a(jk)
      a(jk)=a(ji)
   40 a(ji)=holo
   45 if (abs(biga)-tol) 46,48,48
   46 d=zero
      return
   48 do 55 i=1,n
      if (i-k) 50,55,50
   50 ik=nk+i
      a(ik)=a(ik)/(-biga)
   55 continue
      do 65 i=1,n
      ik=nk+i
      ij=i-n
      do 65 j=1,n
      ij=ij+n
      if (i-k) 60,65,60
   60 if (j-k) 62,65,62
   62 kj=ij-i+k
      a(ij)=a(ik)*a(kj)+a(ij)
   65 continue
      kj=k-n
      do 75 j=1,n
      kj=kj+n
      if (j-k) 70,75,70
   70 a(kj)=a(kj)/biga
   75 continue
      d=d*biga
      a(kk)=one/biga
   80 continue
      k=n
  100 k=k-1
      if (k) 150,150,105
  105 i=l(k)
      if (i-k) 120,120,108
  108 jq=n*(k-1)
      jr=n*(i-1)
      do 110 j=1,n
      jk=jq+j
      holo=a(jk)
      ji=jr+j
      a(jk)=-a(ji)
  110 a(ji)=holo
  120 j=m(k)
      if (j-k) 100,100,125
  125 ki=k-n
      do 130 i=1,n
      ki=ki+n
      holo=a(ki)
      ji=ki+j-k
      a(ki)=-a(ji)
  130 a(ji)=holo
      goto 100
  150 return
      end
      subroutine sdiag2 (m,n,a,d,x)
      implicit real*8(a-h,o-z)
c
c      computation of all eigenvalues and eigenvectors of a real
c      symmetric matrix by the method of qr transformations.
c      if the euclidean norm of the rows varies   s t r o n g l y
c      most accurate results may be obtained by permuting rows and
c      columns to give an arrangement with increasing norms of rows.
c
c      two machine constants must be adjusted appropriately,
c      eps = minimum of all x such that 1+x is greater than 1 on the
c      e     computer,
c      tol = inf / eps  with inf = minimum of all positive x represen-
c            table within the computer.
c      a dimension statement e(160) may also be changed appropriately.
c
c      input
c
c      (m)   not larger than 160,  corresponding value of the actual
c            dimension statement a(m,m), d(m), x(m,m),
c      (n)   not larger than (m), order of the matrix,
c      (a)   the matrix to be diagonalized, its lower triangle has to
c            be given as  ((a(i,j), j=1,i), i=1,n),
c
c      output
c
c      (d)   components d(1), ..., d(n) hold the computed eigenvalues
c            in ascending sequence. the remaining components of (d) are
c            unchanged,
c      (x)   the computed eigenvector corresponding to the j-th eigen-
c            value is stored as column (x(i,j), i=1,n). the eigenvectors
c            are normalized and orthogonal to working accuracy. the
c            remaining entries of (x) are unchanged.
c
c      array (a) is unaltered. however, the actual parameters
c      corresponding to (a) and (x)  may be identical, ''overwriting''
c      the eigenvectors on (a).
c
c      leibniz-rechenzentrum, munich 1965
c
c
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      dimension   a(m,m), d(m), x(m,m)
      dimension   e(160)
c
c     correct adjustment for ibm 370
c
      eps=2.0d-10
      tol=1.0d-65
c
      if(n.eq.1) go to 400
      do 10 i=1,n
      do 10 j=1,i
   10 x(i,j)=a(i,j)
c
c     householder's reduction
c     simulation of loop do 150 i=n,2,(-1)
c
      do 150 ni=2,n
      ii=n+2-ni
c     fake loop for recursive address calculation
      do 150 i=ii,ii
      l=i-2
      h=zero
      g=x(i,i-1)
      if(l) 140,140,20
   20 do 30 k=1,l
   30 h=h+x(i,k)**2
      s=h+g*g
      if(s.ge.tol) go to 50
   40 h=zero
      go to 140
   50 if(h) 140,140,60
   60 l=l+1
      f=g
      g=sqrt(s)
      if(f) 75,75,70
   70 g=-g
   75 h=s-f*g
      x(i,i-1)=f-g
      f=zero
c
      do 110 j=1,l
      x(j,i)=x(i,j)/h
      s=zero
      do 80 k=1,j
   80 s=s+x(j,k)*x(i,k)
      j1=j+1
      if(j1.gt.l) go to 100
      do 90 k=j1,l
   90 s=s+x(k,j)*x(i,k)
  100 e(j)=s/h
  110 f=f+s*x(j,i)
c
      f=f/(h+h)
c
      do 120 j=1,l
  120 e(j)=e(j)-f*x(i,j)
c
      do 130 j=1,l
      f=x(i,j)
      s=e(j)
      do 130 k=1,j
  130 x(j,k)=x(j,k)-f*e(k)-x(i,k)*s
c
  140 d(i)=h
  150 e(i-1)=g
c
c     accumulation of transformation matrices
c
  160 d(1)=x(1,1)
      x(1,1)=one
      do 220 i=2,n
      l=i-1
      if(d(i)) 200,200,170
  170 do 190 j=1,l
      s=zero
      do 180 k=1,l
  180 s=s+x(i,k)*x(k,j)
      do 190 k=1,l
  190 x(k,j)=x(k,j)-s*x(k,i)
  200 d(i)=x(i,i)
      x(i,i)=one
  210 do 220 j=1,l
      x(i,j)=zero
  220 x(j,i)=zero
c
c     diagonalization of the tridiagonal matrix
c
      b=zero
      f=zero
      e(n)=zero
c
      do 340 l=1,n
      h=eps*(abs(d(l))+abs(e(l)))
      if (h.gt.b) b=h
c
c     test for splitting
c
      do 240 j=l,n
      if (abs(e(j)).le.b) goto 250
  240 continue
c
c     test for convergence
c
  250 if(j.eq.l) go to 340
c
c     shift from upper 2*2 minor
c
  260 p=(d(l+1)-d(l))*half/e(l)
      r=sqrt(p*p+one)
      if(p) 270,280,280
  270 p=p-r
      go to 290
  280 p=p+r
  290 h=d(l)-e(l)/p
      do 300 i=l,n
  300 d(i)=d(i)-h
      f=f+h
c
c     qr transformation
c
      p=d(j)
      c=one
      s=zero
c
c     simulation of loop do 330 i=j-1,l,(-1)
c
      j1=j-1
      do 330 ni=l,j1
      ii=l+j1-ni
c     fake loop for recursive address calculation
      do 330 i=ii,ii
      g=c*e(i)
      h=c*p
c
c     protection against underflow of exponents
c
      if (abs(p).lt.abs(e(i))) goto 310
      c=e(i)/p
      r=sqrt(c*c+one)
      e(i+1)=s*p*r
      s=c/r
      c=one/r
      go to 320
  310 c=p/e(i)
      r=sqrt(c*c+one)
      e(i+1)=s*e(i)*r
      s=one/r
      c=c/r
  320 p=c*d(i)-s*g
      d(i+1)=h+s*(c*g+s*d(i))
      do 330 k=1,n
      h=x(k,i+1)
      x(k,i+1)=x(k,i)*s+h*c
  330 x(k,i)=x(k,i)*c-h*s
c
      e(l)=s*p
      d(l)=c*p
      if (abs(e(l)).gt.b) go to 260
c
c     convergence
c
  340 d(l)=d(l)+f
c
c     ordering of eigenvalues
c
      ni=n-1
  350 do 380i=1,ni
      k=i
      p=d(i)
      j1=i+1
      do 360j=j1,n
      if(d(j).ge.p) goto 360
      k=j
      p=d(j)
  360 continue
      if (k.eq.i) goto 380
      d(k) =d(i)
      d(i)=p
      do 370 j=1,n
      p=x(j,i)
      x(j,i)=x(j,k)
  370 x(j,k)=p
  380 continue
  390 go to 410
c
c     special treatment of case n = 1
c
  400 d(1)=a(1,1)
      x(1,1)=one
  410 return
      end
      subroutine chold1(a,p,n,m,d1,d2,lab)
      implicit real*8(a-h,o-z)
c.... this is a fortran version of the algol procedure choldet1 of
c.... wilkinson-reinsch* book titled linear algebra. the total dimension
c.... of array a is (n,n), the actual one is (m,m).  lab is a logical
c.... variable. its value will be .true. when a is not positive definit.
      integer d2
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      logical lab
      dimension a(1),p(1)
      lab=.false.
      d1=one
      d2=0
      do 70 i=1,m
      ii=i-1
      ji1=ii*n
      ij=ji1+i
      do 70 j=i,m
      jk=j
      x=a(ij)
      ij=ij+n
      ji=ji1+j
      if(ii.eq.0) goto 10
      ik=i
      do 5 k=1,ii
      x=x-a(jk)*a(ik)
      jk=jk+n
      ik=ik+n
    5 continue
   10 continue
      if(j.ne.i) goto 60
      d1=d1*x
      if(x.ne.zero) goto 20
      d2=0
      goto 80
   20 continue
      if(abs(d1).lt.one) goto 30
      d1=d1*psix
      d2=d2+4
      goto 20
   30 continue
      if(abs(d1).ge.psix) goto 40
      d1=d1*sixt
      d2=d2-4
      goto 30
   40 continue
      if(x.ge.zero) goto 50
      goto 80
   50 continue
      p(i)=one/sqrt(x)
      goto 70
   60 continue
      a(ji)=x*p(i)
   70 continue
      goto 90
   80 continue
      lab=.true.
   90 continue
      return
      end
      function idia(k,l)
      implicit real*8(a-h,o-z)
      if(k.ge.l) idia=(k*(k-1))/2+l
      if(l.gt.k) idia=(l*(l-1))/2+k
      return
      end
      subroutine f1(n,arg,ne,arge)
      implicit real*8(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      logical test,mis,dipol
      dimension arg(1),arge(1)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      common/egy/ q(nq1),h(nh)
      common/har/ tj(nt)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common/tset/ test,mis,dipol
      common/factor/ fac,hfac2
      common/matrix/ fstart(nh,nm),fscal(nh,nm),gmcf(nh,nm)
      common/extra/ iex(nextra),iepa(neel,nextra),mue(neel,nextra),
     & sef(nd1)
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      common /phonon/ zvect,phirot,neighn,kval
      ik=6
      mis=.false.
      do 200 nmole=1,nmo
      m=ndi(nmole)
      mm=(m*(m+1))/2
      iez=iex(nmole)
      il=jlow(nmole)
      iu=il-1+m
      call scaf(n,arg,ne,arge,mis)
      if(.not.mis) goto 80
      write(ik,10)
   10 format(//' error in the definition of the scaling factors')
      goto 150
   80 continue
      do 3 i=1,mm
       h(i)=gmcf(i,nmole)
    3 continue
      if(neighn.eq.0) then
       k=0
       do 90 i=1,m
        ji1=(i-1)*m
        do 90 j=1,i
         ij=(j-1)*m+i
         ji=ji1+j
         k=k+1
         q(ij)=fscal(k,nmole)
         q(ji)=q(ij)
   90  continue
      else
       co = one
       si = zero
       m1=mm+1
       do 82 i=0,neighn
       call kuelma(m,m1,co,si,fscal(1,nmole),q,i)
   82  continue
       do 129 i=1,m
        ij=m*(i-1)
        do 128 j=1,i-1
  128    q(j+ij)=q(m*(j-1)+i)
  129  continue
      endif
      call gfeig(m,m,q,h,eig,il)
      konst=il-1
      nt1=n+ne
      do 140 k=1,nt1
      iik=(k-1)*nsumf+konst
      do 4 i=1,mm
      h(i)=fscal(i,nmole)
    4 continue
      ij=0
      if(arg(k) .ne. zero) then
       stemp=one/arg(k)
      else
       stemp=zero
      endif
      ke=k-n
      if(ke .gt. 0 .and. arge(ke) .ne. zero) then
       setemp=one/arge(ke)
      else
       setemp=zero
      endif
      do 111 i=il,iu
      s1=zero
      if(mut(i).eq.k) s1=stemp
      do 110 j=il,i
      ij=ij+1
      s2=zero
      if(mut(j).eq.k) s2=stemp
      if(iez.eq.0) then
c....   no extra scaling
        h(ij)=(s1+s2)*h(ij)
        if(neighn.ne.0) call phodif(h(ij),fscal(m1,nmole),
     &          s1+s2,i,j,neighn,m)
      else
        do 100 ie=1,iez
          if(iepa(ie,nmole).eq.ij) goto 102
  100   continue
c....   normal matrix element
        if(k.le.n) then
c....     normal factor
          h(ij)=(s1+s2)*h(ij)
          if(neighn.ne.0) call phodif(h(ij),fscal(m1,nmole),
     &          s1+s2,i,j,neighn,m)
        else
c....     extra factor
          h(ij)=zero
        endif
        goto 110
  102   continue
c....   extra matrix element
        if(k.le.n) then
c....     normal factor
          h(ij)=zero
        else
c....     extra factor
          if(mue(ie,nmole).eq.ke) then
            h(ij)=setemp*h(ij)
            if(neighn.ne.0) call phodif(h(ij),fscal(m1,nmole),
     &          setemp,i,j,neighn,m)
          else
            h(ij)=zero
          endif
        endif
      endif
  110 continue
  111 continue
      do 130 ii=1,m
      kkii1=(ii-1)*m
      s=zero
      do 120 kk=1,m
      kkii=kkii1+kk
      do 120 l=1,m
      lii=kkii1+l
      kkl=idia(kk,l)
      s=s+q(kkii)*h(kkl)*q(lii)
  120 continue
c.... dlambda(i)/dx(k) in s
      iik=iik+1
      ii1=ii+konst
      tj(iik)=(hfac2/eig(ii1))*s
  130 continue
  140 continue
  200 continue
  150 continue
      return
      end
      subroutine phodif(x,a,s,i,j,neighn,m)
      implicit real*8(a-h,o-z)
      dimension a(*)
      n1=0
      mm=m*m
      do 1 n=1,neighn
      k1=n1+(i-1)*m+j
      k2=n1+(j-1)*m+i
      x=x+s*(a(k1)+a(k2))
    1 n1=n1+mm
      return
      end
      subroutine gfeig(n1,n,f,a,d,ka)
      implicit real*8(a-h,o-z)
c.... force constants in array f, cholesky-lower-triangular-matrix in
c.... array a. the total dimension of f (in the calling routine) is
c.... (n1,n1), the actual one is (n,n). the cholesky-matrix has only its
c.... lower triangle in array a of one dimension, row by row. the
c.... cholesky-matrix is the cholesky-decomposition-matrix of wilson*s
c.... g matrix. the eigenvectors of the (gf) matrix is overwritten on
c.... array f on return. the frequencies will be in array d on return,
c.... in (1000*cm-1) units.
      parameter(nfreq=300)
      dimension f(1),a(1),d(1)
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      common/bem/ dd(nfreq)
      common/factor/ fac,hfac2
      do 20 i=1,n
      do 20 j=1,n
      kj=(j*(j+1))/2
      ik=(j-1)*n1+i
      ij=ik
      s=zero
      do 10 k=j,n
      s=s+f(ik)*a(kj)
      ik=ik+n1
      kj=kj+k
   10 continue
      f(ij)=s
   20 continue
      do 40 j=1,n
      kj1=(j-1)*n1
      do 40 i=j,n
      ij=kj1+i
      ki=(i*(i+1))/2
      s=zero
      do 30 k=i,n
      kj=kj1+k
      s=s+a(ki)*f(kj)
      ki=ki+k
   30 continue
      f(ij)=s
   40 continue
c     call scri(n,f,n,n)
      call sdiag2(n1,n,f,dd,f)
      k=0
      kam=ka-1+n
      do 45 j=ka,kam
      k=k+1
      if(abs(dd(k)).eq.zero) goto 41
      elj=sign(one,dd(k))
      d(j)=(elj*fac)*sqrt(elj*dd(k))
      goto 45
   41 d(j)=zero
   45 continue
      do 60 j=1,n
      kj1=(j-1)*n1
      do 60 is=1,n
      i=n+1-is
      ij=kj1+i
      ik1=(i*(i-1))/2
      s=zero
      do 50 k=1,i
      kj=kj1+k
      ik=ik1+k
      s=s+a(ik)*f(kj)
   50 continue
      f(ij)=s
   60 continue
      return
      end
      subroutine scaf(n,w,n1,w1,label)
      implicit real*8(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
c.... w is the linear array of variable scaling factors. its actual
c.... dimension is n. label is a logical variable. its value will be
c.... .true. when an error exists in array mut. the force constants
c.... are in linear array h (lower triangle, row by row), and they will
c.... be scaled.
      logical label
      dimension w(1),w1(1)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      common/egy/ q(nq1),h(nh)
      common/har/ tj(nt)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common/extra/ iex(nextra),iepa(neel,nextra),mue(neel,nextra),
     & sef(nd1)
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     1   one,two,eight,sixt,ten2,ten3
      common/matrix/ fstart(nh,nm),fscal(nh,nm),gmcf(nh,nm)
      common /phonon/ zvect,phirot,neighn,kval
      m=ndi(nmole)
      mm=m*(m+1)/2
      il=jlow(nmole)
      iu=il-1+m
      label=.false.
      do 30 i=il,iu
      ii=iabs(mut(i))
      if(mut(i)) 10,50,20
   10 continue
c.... fix factor
      if(ii.gt.mf) goto 50
      ll=i-il+1
      ss(ll)=sf(ii)
      goto 30
   20 continue
c.... variable factor
      if(ii.gt.n) goto 50
      ll=i-il+1
      ss(ll)=w(ii)
   30 continue
      ij=0
      do 40 i=1,m
      do 40 j=1,i
      ij=ij+1
      fscal(ij,nmole)=(ss(i)*ss(j))*fstart(ij,nmole)
   40 continue
      if(neighn.ne.0)
     &   call phosca(fscal(mm+1,nmole),fstart(mm+1,nmole),m,neighn)
      mab=iex(nmole)
      if(mab.eq.0) goto 60
c.... extra scaling
      do 100 k=1,mab
      ij=iepa(k,nmole)
c.... inverse idia (in following 2 lines)
      i=(sqrt(eight*float(ij)+one)-one)/two+alone
      j=ij-i*(i-1)/2
      is=mue(k,nmole)
      if(is.le.0) goto 90
c.... extra vari
      if(is.gt.n1) goto 50
      sfv=w1(is)
      goto 95
c.... extra fix
   90 if(is.eq.0) goto 50
      iss=-is
      if(iss.gt.mef) goto 50
      sfv=sef(iss)
   95 continue
      if(neighn.eq.0) then
        if(i.ne.j)
     &  fscal(ij,nmole)=fscal(ij,nmole)/(ss(i)*ss(j))*sfv
      else
       necall=0
       if(i.gt.m) then
         i=mod(i,m)
         if(j.gt.m) then
           necall=neighn
           j=mod(j,m)
           ij=idia(i,j)
           if(i.ne.j)
     &      fscal(ij,nmole)=fscal(ij,nmole)/(ss(i)*ss(j))*sfv
         endif
         call phosce(fscal(mm+1,nmole),i,j,sfv,m,necall)
       else
         if(j.gt.m) then
           j=mod(j,m)
           call phosce(fscal(mm+1,nmole),i,j,sfv,m,necall)
         else
           if(i.ne.j)
     &      fscal(ij,nmole)=fscal(ij,nmole)/(ss(i)*ss(j))*sfv
         endif
       endif
      endif
  100 continue
      goto 60
   50 continue
      label=.true.
   60 continue
      return
      end
      subroutine phosca(scal,start,m,neighn)
      implicit real*8(a-h,o-z)
      parameter(nfreq=300,nd=20)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      dimension scal(*),start(*)
      ij=0
      do 1 n=1,neighn
      do 1 i=1,m
      do 1 j=1,m
      ij=ij+1
      scal(ij)=(ss(i)*ss(j))*start(ij)
    1 continue
      return
      end
      subroutine phosce(scal,i,j,sfv,m,necall)
      implicit real*8(a-h,o-z)
      parameter(nfreq=300,nd=20)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
      dimension scal(m,*)
      neighn=necall
      if(neighn.eq.0) neighn=1
      do 1 n=1,neighn
      k=i+(n-1)*m
      l=j+(n-1)*m
      scal(j,k)=scal(j,k)*sfv/(ss(i)*ss(j))
      if(i.ne.j.and.necall.ne.0) scal(i,l)=scal(i,l)*sfv/(ss(i)*ss(j))
    1 continue
      return
      end
      subroutine gmtcal(nq,nis,iggeo,inp,iout,icm,clx,cly,clz)
      implicit real*8(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common /coord/ xsol(ncorm,2*nmax),ysol(ncorm,2*nmax),
     & zsol(ncorm,2*nmax),xist(ncorm,2*nmax),yist(ncorm,2*nmax),
     & zist(ncorm,2*nmax)
      common /bmat/ am(3*ncorm,3*ncorm)
      common/egy/ q(nq1),h(nh)
      common/dimen/ idumm(2*nm+4),natm,mef
      common /phonon/ zvect,phirot,neighn,kval
      dimension qhelp(nmax,nmax)
      dimension ve(2*ncorm,3),mv(ncorm),iia(ncorm,8),
     & iat1(2*ncorm),iat2(2*ncorm),r(2*ncorm)
      dimension xm(3*ncorm), gs(3*ncorm), cs(3*ncorm)
c
      real*8    zero,     half,       one
      parameter(zero=0d0, half=0.5d0, one=1d0)
c
      mn=ncorm
      mp=2*ncorm
      mnn=3*ncorm
c     mn-- max.number of atoms, mp max.number of bonds
c     if you want to change dimensions, consider the following:
c     you change mn,mp and mnn and the above dimensions according to:
c     dimensions: xa,ya,za,sn,mv (mn), iia(mn,8)
c     ve(mp,3), iat1,iat2,r(mp)
c     am(mnn,mnn), xm,gs(mnn)
      do 200 i=1,natm
  200 iat1(i)=sn(i)+0.1
      if (iggeo.ne.1) goto 201
      call geopar(xa,iat1,natm,iout,mn)
  201 nek=3*natm/(2*neighn+1)
      nek2=nek-2
      do 502 i=1,natm
      i0=(i-1)*3+1
      i1=i0+1
      i2=i0+2
      xmi=1.d0/xmas(i,nis)
      xm(i0)=xmi
      xm(i1)=xmi
      xm(i2)=xmi
  502 continue
      if(icm.ne.1) goto 861
      if(clx .lt. half .and. cly .lt. half .and. clz .lt. half) clz=one
      do 503 i=1,nek
  503 xm(i)=1./xm(i)
      if(clx .lt. half) goto 5061
  507 format(/' coriolis c(alfa) matrix,alfa=  ',a4,//)
      do 506 i=1,nq
      do 504 k=1,nq
  504 cs(k) = zero
      do 505 j=1,nq
      do 505 k=1,nek,3
      k1=k+1
      k2=k+2
      cs(j)=cs(j)+xm(k)*(am(k1,i)*am(k2,j)-am(k2,i)*am(k1,j))
  505 continue
      write(iout,1936) (cs(j),j=1,nq)
  506 continue
 5061 continue
      if(cly .lt. half) goto 5161
      do 516 i=1,nq
      do 514 k=1,nq
  514 cs(k) = zero
      do 515 j=1,nq
      do 515 k=1,nek,3
      k2=k+2
      cs(j)=cs(j)+xm(k)*(am(k2,i)*am(k,j)-am(k,i)*am(k2,j))
  515 continue
      write(iout,1936) (cs(j),j=1,nq)
  516 continue
 5161 continue
      if(clz .lt. half) goto 5261
      do 526 i=1,nq
      do 524 k=1,nq
  524 cs(k) = zero
      do 525 j=1,nq
      do 525 k=1,nek,3
      k1=k+1
      cs(j)=cs(j)+xm(k)*(am(k,i)*am(k1,j)-am(k1,i)*am(k,j))
  525 continue
      write(iout,1936) (cs(j),j=1,nq)
  526 continue
 5261 continue
 1936 format(/,(1x,5f12.6,3x,5f12.6))
  861 continue
      if(neighn.ne.0) then
      do 1801 j=1,nek
      do 1801 k=1,nq
 1801 qhelp(j,k) = zero
      do 1800 j=1,nek
      do 1800 k=1,nq
      do 1800 i=1,2*neighn+1
      jn=(i-1)*nek+j
      qhelp(j,k)=qhelp(j,k)+am(jn,k)
 1800 continue
      do 1834 i=1,nq
      do 1831 k=1,nq
 1831 gs(k) = zero
      do 1832 k=1,nek
      do 1832 j=i,nq
      gs(j)=gs(j)+xm(k)*qhelp(k,i)*qhelp(k,j)
 1832 continue
      do 1833  j=i,nq
      ii=nq*(i-1)+j
 1833 q(ii)=gs(j)
 1834 continue
c  testmk-----
      ii=nq*nq
      q(ii)=q(ii)+1.d-8
      ii=ii-nq-1
      q(ii)=q(ii)+1.d-8
c       ------
      else
      do 1934 i=1,nq
      do 1931 k=1,nq
 1931 gs(k) = zero
      do 1932 k=1,nek
      do 1932 j=i,nq
      gs(j)=gs(j)+xm(k)*am(k,i)*am(k,j)
 1932 continue
      do 1933  j=i,nq
      ii=nq*(i-1)+j
 1933 q(ii)=gs(j)
 1934 continue
      endif
      return
      end
      subroutine geopar(x,ian,ind,ik,idim)
      implicit real*8(a-h,o-z)
      parameter(ncorm=80)
      dimension x(idim,3),xx(3),ian(1)
c.... idim is the corresponding dimension statement in the calling routi
c.... it may also read as x(idim),y(idim),z(idim)
c
      real*8    zero
      parameter(zero=0d0)
      real*8    degrad
      parameter(degrad=57.2957795130823208767981548141d0)
c
      dimension radius(19)
      dimension li(ncorm),lj(ncorm),rr(ncorm),ip(6),u(3),v(3),w(3),z(3)
      data radius(1),radius(2),radius(3),radius(4),radius(5),radius(6),
     1 radius(7),radius(8),radius(9),radius(10) /0.4d0,0.3d0,1.25d0,
     2 0.97d0,.82d0,.75d0,.69d0,.66d0,.62d0,.4d0/
      data radius(11),radius(12),radius(13),radius(14),radius(15),
     1 radius(16),radius(17),radius(18),radius(19)
     2 /1.7d0,1.6d0,1.2d0,1.05d0,1.d0,1.d0,1.d0,1.d0,1.3d0/
c.... ind=number of atoms, ian(i)=atomic numbers, ik=output file
c.... uses the routines normal, arcos
      write(ik,1001)
 1001 format(//' geometry parameters'//)
      nek=ind*3
      nq=0
      do 100 i=1,ind
      do 100 j=1,i
      if(i.eq.j) go to 100
      ii=ian(i)
      jj=ian(j)
      if(ii.gt.19) ii=19
      if(jj.gt.19) jj=19
      r=sqrt((x(i,1)-x(j,1))**2+(x(i,2)-x(j,2))**2+(x(i,3)-x(j,3))**2)
      if(r.gt.(1.25*(radius(ii)+radius(jj)))) go to 100
      ni=nq*nek-ind
      nq=nq+1
       li(nq)=i
      lj(nq)=j
      rr(nq)=r
  100 continue
c     calculation of geometry parameters
      write(ik,1300)
 1300 format(/' bond lengths'/)
      do 301 m=1,nq
      write(ik,1301)li(m),lj(m),rr(m)
 1301 format(1x,2i5,f12.6)
  301 continue
       write(ik,1400)
 1400 format(/' bond angles'/)
      do 302 m=1,nq
      do 302 k=1,m
      if(m.eq.k) goto 302
      im=li(m)
      jm=lj(m)
      ki=li(k)
      jk=lj(k)
      j=0
      if(im.eq.ki) j=1
      if(im.eq.jk)j=2
      if(jm.eq.ki) j=3
      if(jm.eq.jk)j=4
      if(j.eq.0)goto 302
      j1=im
      j2=jm
      j3=ki
       goto(310,320,330,340),j
  310 j3=jk
  320 goto 350
  330 j3=jk
  340 j1=jm
      j2=im
  350 s=zero
      do 360 l=1,3
      s=s+(x(j2,l)-x(j1,l))*(x(j3,l)-x(j1,l))
  360 continue
       s=s/(rr(m)*rr(k))
      s=darcos(s)
      s1 = s * degrad
      write(ik,1401) j2,j1,j3,s1,s
 1401 format(1x,3i5,f12.4,2x,f12.6)
  302 continue
      write(ik,1500)
 1500 format(/' dihedral angles'/)
      do 400 m=1,nq
      do 400 n=1,nq
      do 400 k=1,n
       if(m.eq.n.or.m.eq.k.or.n.eq.k)goto 400
      j1=li(m)
      j2=lj(m)
      j3=0
      j5=0
      j6=0
      j4=0
      if(li(n).eq.j1) j3=lj(n)
      if(lj(n).eq.j1) j3=li(n)
      if(li(n).eq.j2) j4=lj(n)
      if(lj(n).eq.j2) j4=li(n)
      if(j3.ne.0 .and. li(k).eq.j1) j5=lj(k)
      if(j3.ne.0 .and. lj(k).eq.j1) j5=li(k)
c.... j2 out of  j3-j5-j1 (centre) plane
      if(j4.ne.0 .and. li(k).eq.j2) j6=lj(k)
      if(j4.ne.0 .and. lj(k).eq.j2) j6=li(k)
      if(j5.ne.0 .or. j6.ne.0) goto 500
c.... j1 out of j4-j6-j2 (centre) planr
      if(li(k).eq.j1) j3=lj(k)
      if(lj(k).eq.j1) j3=li(k)
      if(li(k).eq.j2) j4=lj(k)
      if(lj(k).eq.j2) j4=li(k)
      if(j3.eq.0 .or. j4.eq.0) goto 500
      if(j3.eq.j4) goto 500
c      j3-j1-j2-j4 torsion
      do 410 l=1,3
      u(l)=x(j3,l)-x(j1,l)
       v(l)=x(j2,l)-x(j1,l)
      w(l)=x(j2,l)-x(j4,l)
  410 continue
      call normal(u,v,z)
      call normal(v,w,u)
      s=zero
      do 401 l=1,3
  401 s=s+z(l)*u(l)
      s1 = darcos(s) * degrad
      call normal(u,v,w)
      s2=zero
      do 403 l=1,3
  403 s2=s2+w(l)*z(l)
      if(s2 .lt. zero) s1=-s1
      s = s1 / degrad
      write(ik,1501)j3,j1,j2,j4,s1,s
 1501 format(5x,'torsion',2x,4i5,f12.4,2x,f12.6)
  500 if(j6.eq.0) goto 510
      j5=j6
      j3=j4
      jj=j2
      j2=j1
      j1=jj
  510 if(j5.eq.0) goto 400
      do 520 l=1,3
      u(l)=x(j5,l)-x(j1,l)
      v(l)=x(j2,l)-x(j1,l)
      w(l)=x(j3,l)-x(j1,l)
  520 continue
      call normal(u,w,z)
      s=zero
      do 530 l=1,3
  530 s=v(l)*z(l)+s
      s=s/rr(m)
      s1 = darcos(s) * degrad
      s1 = 90.0 - s1
      s  = s1 / degrad
      write(ik,540) j2,j3,j5,j1,s1,s
  540 format(1x,i5,1x,'out of',3i5,' plane',f12.4,2x,f12.6)
  400 continue
      return
      end
      subroutine norm(u)
      implicit real*8(a-h,o-z)
      real*8    one
      parameter(one=1d0)
      dimension u(3)
      x = one / sqrt(scalar(u,u))
      do 1 i = 1, 3
         u(i) = u(i) * x
    1 continue
      return
      end
      real*8 function s2(x)
      implicit real*8(a-h,o-z)
      real*8    one
      parameter(one=1d0)
      s2 = sqrt(one - x*x)
      return
      end
      real*8 function scalar(u,v)
      implicit real*8(a-h,o-z)
      real*8    zero
      parameter(zero=0d0)
      dimension u(3),v(3)
      scalar=zero
      do 1 i=1,3
         scalar = scalar + u(i) * v(i)
    1 continue
      return
      end
      subroutine normal(u,v,w)
      implicit real*8(a-h,o-z)
      dimension u(3),v(3),w(3)
c99999...  w wird ein senkrecht auf die ebene(u,v) stehender einheitsvek
c      tor
      w(1)=u(2)*v(3)-u(3)*v(2)
      w(2)=u(3)*v(1)-u(1)*v(3)
      w(3)=u(1)*v(2)-u(2)*v(1)
      call norm(w)
      return
      end
      real*8 function darcos(x)
      implicit real*8(a-h,o-z)
      real*8    zero,     one,     two,     small
      parameter(zero=0d0, one=1d0, two=2d0, small=1d-11)
      real*8     pi
      parameter( pi=3.14159265358979323846264338328d0 )
      parameter(pis2 = pi / two)
      if(x .ge. one) goto 100
      if(x .le. -one) goto 200
      x1 = sqrt(one - x**2)
      if(abs(x) .lt. small) goto 300
      s = atan(x1 / x)
      if(x .lt. zero) s = s + pi
      darcos = s
      return
100   darcos = zero
      return
200   darcos = pi
      return
300   darcos = pis2
      return
      end
      subroutine auslen (dical,na,ndisp,nditer,nistp,ks,ka,l1,inp,iout,
     & iptr)
      implicit real*8 (a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      common /coord/ xsol(ncorm,2*nmax),ysol(ncorm,2*nmax),
     & zsol(ncorm,2*nmax),xist(ncorm,2*nmax),yist(ncorm,2*nmax),
     & zist(ncorm,2*nmax)
      common /bmat/ am(3*ncorm,3*ncorm)
      common/dip/ dipx(nmax,0:nism),dipy(nmax,0:nism),dipz(nmax,0:nism)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      dimension l(7)
      dimension din(3),dineu(3,2*nmax)
      logical dical
      l(1)=1
      l(2)=ks
      l(3)=ndisp
      l(4)=l1
      l(5)=iptr-1
      l(6)=0
      if(iptr.gt.0) l(6)=1
      l(7)=ka
      call bmcal(na,ndisp,nistp,0,l,inp,iout)
      l(1)=0
      l(6)=0
      if(.not.dical) goto 5
      do 2 nis=0,nistp
      call bmcal(na,ndisp,nistp,nis,l,inp,iout)
      do 1 k=1,ndisp
      if(nis.eq.0) call schift(1,k,nis)
      call schift(2,k,nis)
      call umat1(k,iout,din)
      do 1 lk=1,3
      dineu(lk,k)=din(lk)
   1  continue
      call dipder(nis,nditer,ndisp,iout,dineu)
   2  continue
      call bmcal(na,0,0,0,l,inp,iout)
   5  return
      end
      subroutine bmcal(na,ndisp,nistp,nis,lopt,inp,iout)
      implicit real*8 (a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      dimension lopt(7)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      common /coord/ xsol(ncorm,2*nmax),ysol(ncorm,2*nmax),
     & zsol(ncorm,2*nmax),xist(ncorm,2*nmax),yist(ncorm,2*nmax),
     & zist(ncorm,2*nmax)
      common /bmat/ b(3*ncorm,3*ncorm)
      common/dip/ dipx(nmax,0:nism),dipy(nmax,0:nism),dipz(nmax,0:nism)
      common /phonon/ zvect,phirot,neighn,kval
      integer p,q
      integer*4 irt
      dimension wk(16)
      dimension ww(2)
      equivalence(b,ww)
      logical writ
      dimension xin(4),cin(4)
      dimension c(nq1)
      dimension f(3*ncorm),fi(3*ncorm-6),cc(3*ncorm),xm(3*ncorm),
     & xy(3*ncorm),qq1(3*ncorm),qq(3*ncorm-6)
      dimension l(3*ncorm-6), m(3*ncorm-6), ki(3*ncorm-6), ia(ncorm)
      isgn=0
      ang=1.889726342d0
      ajoule=0.22936758d0
      zero=0.0d+00
      one=1.0d+00
      ffak=ang/ajoule
      ix=inp
      ncmax=3*ncorm
      nqmax=ncmax
      nfix=0
      thre=0.001d0
      irep=0
      iconv=0
      xmiter = one
      xmaxit = one
c
c       dimension of the b matrix must conform to b(ncmax,nqmax)
c
      natp=na
   44 continue
      if (na.gt.0 .and. na.le.ncorm) goto 50
      write(iout,520) na
      stop
   50 continue
      n=na
      if (lopt(1).ne.1) go to 110
      na=0
      do 71 j=1,natp
      na=na+1
      i3=na*3
      read(inp,930)atname(na),xa(na),ya(na),za(na),xmdum
         call mascal(xmdum,nistp,lopt(2),lopt(4),na,nia)
         ia(na)=nia
         sn(na)=nia
         if (lopt(7).eq.1) go to 60
         xa(na)=xa(na)/ang
         ya(na)=ya(na)/ang
         za(na)=za(na)/ang
   60    continue
         xm(i3) = one / xmss(na)
         xm(i3-1) = xm(i3)
         xm(i3-2) = xm(i3)
   71 continue
      if(neighn.ne.0) call phocor(na,nistp)
  110 nek=3*na
      if(lopt(1).eq.0) then
       do 6 i=1,natp
       ii=3*i
       xm(ii) = one / xmas(i,nis)
       xm(ii-1)=xm(ii)
       xm(ii-2)=xm(ii)
    6  continue
      endif
      ndi=lopt(3)
      writ=.false.
      if (lopt(6).eq.1) writ=.true.
      ii=0
  325 ii=ii+1
      call machb (nek,b,ncmax,nqmax,xa,ya,za,qq,n,ix,iout,nq,writ,.false
     1.,ncard)
      writ=.false.
      if(lopt(6).ne.1 .or. ii.gt.1) goto 151
      write (iout,610)
      do 150 i=1,nq
         write (iout,620) (b(j,i),j=1,nek)
  150 continue
  151 continue
      if(ndisp.eq.0) goto 482
      i1=0
      do 170 i=1,nq
      do 170 j=1,nq
         i1=i1+1
         s=zero
         do 160 k=1,nek
  160    s=s+b(k,i)*b(k,j)*xm(k)
  170 c(i1)=s
      tol=1.0d-8
      call osinv (c,nq,d,tol,l,m)
      if(lopt(5).eq.2) then
      write (iout,630) d
      write (iout,730)
      write (iout,740) (qq(j),j=1,nq)
      endif
         do 330 j=1,4
            xin(j)=xxin(j,ii)
            cin(j)=ccin(j,ii)
  330    continue
        if(lopt(5).eq.2) write (iout,750) (xin(j),cin(j),j=1,4)
         do 340 j=1,nq
            cc(j)=zero
  340    continue
         do 350 j=1,4
            j1=xin(j)+0.1
            if (j1.le.0) go to 350
            cc(j1)=cin(j)
  350    continue
         do 360 j=1,nq
            qq1(j)=cc(j)
  360    continue
      go to 362
  361 do 363 i=1,ncard
      backspace inp
  363 continue
      call machb(nek,b,ncmax,nqmax,xa,ya,za,qq,n,ix,iout,nq,.false.,
     1 .false.,ncard)
      irep=0
c     do 713 i=1,nq
c        write (iout,620) (b(j,i),j=1,nek)
c 713 continue
  366 i1=0
      do 364 i=1,nq
      do 364 j=1,nq
      i1=i1+1
      s=zero
      do 365 k=1,nek
  365 s=s+b(k,i)*b(k,j)*xm(k)
  364 c(i1)=s
      call osinv(c,nq,d,tol,l,m)
      if(irep.gt.0) go to 380
  362 continue
         i3=0
         do 370 i=1,n
            xy(i)=xa(i)
            j=i+n
            xy(j)=ya(i)
            j=j+n
            xy(j)=za(i)
  370    continue
      if(iconv.eq.0) go to 380
  371 miter=miter+1
      if(miter.gt.maxit)goto 459
      xmiter=miter
      irep=0
      do 372 i=1,nq
  372 qq1(i)=cc(i)/xmaxit
  380    continue
         irep=irep+1
         ij=0
         do 400 i=1,nq
            s = zero
            do 390 j=1,nq
               ij=ij+1
               s=s+c(ij)*qq1(j)
  390       continue
            fi(i)=s
  400    continue
         do 420 i=1,nek
            s = zero
            do 410 j=1,nq
               s=s+b(i,j)*fi(j)
  410       continue
            qq1(i)=s*xm(i)
  420    continue
         i3=0
         do 430 i=1,n
            i3=i3+3
            xy(i)=xy(i)+qq1(i3-2)
            j=i+n
            xy(j)=xy(j)+qq1(i3-1)
            j=j+n
            xy(j)=xy(j)+qq1(i3)
  430    continue
         i2=2*n+1
         do 440 i=1,ncard
            backspace inp
  440    continue
         call machb (nek,b,ncmax,nqmax,xy(1),xy(n+1),xy(i2),qq1,n,ix,iou
     1    t,nq,.false.,.false.,ncard)
c     do 714 i=1,nq
c        write (iout,620) (b(j,i),j=1,nek)
c 714 continue
         if(lopt(5).eq.2) then
         write (iout,760) irep
         write (iout,740) (qq1(i),i=1,nq)
         endif
         qmax = zero
         do 450 i=1,nq
      qq1(i)=qq(i)+xmiter/xmaxit*cc(i)-qq1(i)
            if (qmax.lt.abs(qq1(i))) qmax=abs(qq1(i))
  450    continue
      if(irep.lt.9.and.qmax.gt.0.5d-6) go to 366
      if(qmax.gt.0.5d-6) write(iout,801)
      if(iconv.gt.0 .and. qmax.le.0.5d-6) go to 371
      if(iconv.gt.0 .and. qmax.gt.0.5d-6) stop
      if(qmax.le.0.5d-6) go to 459
      iconv=1
      maxit=5
      xmaxit=maxit
      miter=0
      go to 361
  459 if(lopt(5).eq.2) write(iout,770)
  460    continue
         i3=0
      ij=0
         do 470 i=1,n
            i3=i3+3
            ym = one / xm(i3)
            if (ym.lt.6.5) iat=(ym+1.2)/2.
            if (ym.ge.6.5) iat=(ym+0.2)/2.0
            xiat=ia(i)
            if (ia(i).eq.0) xiat=iat
            j=i+n
            j1=j+n
            x=xy(i)-xa(i)
            y=xy(j)-ya(i)
            z=xy(j1)-za(i)
      if(abs(x).gt.thre) ij=1
      if(abs(y).gt.thre) ij=1
      if(abs(z).gt.thre) ij=1
         if(lopt(5).eq.2)
     &      write (iout,790) i,xiat,x,y,z,xy(i),xy(j),xy(j1),ym
         if (lopt(1).eq.1) then
          xist(i,ii)=xy(i)
          yist(i,ii)=xy(j)
          zist(i,ii)=xy(j1)
         else
          xsol(i,ii)=xy(i)
          ysol(i,ii)=xy(j)
          zsol(i,ii)=xy(j1)
         endif
  470    continue
  480 continue
      irep=0
      miter=0
      xmaxit = one
      xmiter = one
      iconv=0
      if(lopt(5).gt.0) write(iout,772)
      jj=0
      do 490 i=1,natp
      jj=jj+1
      jj1=jj+n
      jj2=jj1+n
      x=xy(jj)*ang
      y=xy(jj1)*ang
      z=xy(jj2)*ang
      if(lopt(5).gt.0) write(iout,792)jj,xiat,x,y,z
  490 continue
         do 491 i=1,ncard
            backspace inp
  491    continue
      if(ii.lt.ndisp)go to 325
  482 continue
      return
c
  520 format (' too many nuclei',i5)
  610 format (' b matrix'/)
  620 format (/,(1x,4(3f10.6,3x)))
  630 format(/5x,'determinant  = ',1pe16.5)
  772 format(/' cartesian coordinates in a.u.'/)
  730 format (/' original internal coordinates'/)
  740 format (2x,5f12.7,3x,5f12.7)
  750 format (/' internal displacement',4(f4.0,f12.6))
  760 format (' step=',i4)
  770 format (/' cartesian displacements and new cartesian',
     & ' coordinates'/)
  780 format(' displ. ',4(f3.0,',',f6.4),3x,3(a4,a6))
  790 format(1x,i3,'   n',f3.0,3f12.7,6x,3f12.7,6x,f12.6)
  792 format(1x,i3,'   n',f3.0,3f12.7)
  800 format(' n=',a4,4x,5f10.6)
  801 format(/' *****caution, within 9 steps no convergence*****'/)
  930 format(a4,4f10.6)
c
      end
      subroutine phocor(na,nistp)
      implicit real*8 (a-h,o-z)
      parameter(ncorm=80,nism=20)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common /phonon/ zvect,phirot,neighn,kval
      do 1 i=1,neighn
      iseg=na*(2*i-1)
      co=cos(phirot*dble(i))
      si=sin(phirot*dble(i))
      do 2 j=1,na
      ipls=j+iseg
      imin=ipls+na
      xa(ipls)=xa(j)*co+ya(j)*si
      ya(ipls)=ya(j)*co-xa(j)*si
      za(ipls)=za(j)+dble(i)*zvect
      xa(imin)=xa(j)*co-ya(j)*si
      ya(imin)=ya(j)*co+xa(j)*si
      za(imin)=za(j)-dble(i)*zvect
      xmss(ipls*3)=xmss(j*3)
      xmss(ipls*3-1)=xmss(j*3)
      xmss(ipls*3-2)=xmss(j*3)
      xmss(imin*3)=xmss(j*3)
      xmss(imin*3-1)=xmss(j*3)
      xmss(imin*3-2)=xmss(j*3)
      do 3 k=0,nistp
      xmas(ipls,k)=xmas(j,k)
   3  xmas(imin,k)=xmas(j,k)
   2  continue
   1  continue
      na=na*(2*neighn+1)
      return
      end
      subroutine machb (nek,bmat,ncmax,nqmax,xa,ya,za,qq,n,inp,iout,nq,w
     1ri,qonly,ncard)
c
c     # 20-feb-02 some obsolete hollerith junk removed. -rls
c
      implicit real*8 (a-h,o-z)
      logical wri,qonly
c
c     nq ist die zahl der inneren koordinaten
c     nek ist  3* die zahl der kerne fuer die krafte berechnet werden
c     .... bmat is the transpose b matrix
c     datenangabe fuer jede koordinate eine karte typ und die atome
c     typ kann sein stretch,bend,out,torsion,linear1,linear2 links ge-
c     buendelt zu schreiben. bei stretch 2, bei bend 3, bei den anderen
c     4 atome in format f10.3, die nummern der atome
c
      real*8 scale,scaleold,scalex
      dimension qq(1)
      character*4 typ, tipus(6)
      character*1 we, wort(3)
      dimension a(4), ia(4), u(3), v(3), w(3), z(3), x(3), uu(3), vv(3),
     1 ww(3), zz(3), uv(12)
      equivalence (ka,ia(1)), (kb,ia(2)), (kc,ia(3)), (kd,ia(4))
      equivalence (uv(1),uu(1)), (uv(4),vv(1)), (uv(7),ww(1)), (uv(10),z
     1z(1))
      dimension bmat(ncmax,nqmax), xa(1), ya(1), za(1)
c
      real*8    zero,     one,     two
      parameter(zero=0d0, one=1d0, two=2d0)
      real*8     pi
      parameter( pi=3.14159265358979323846264338328d0 )
      real*8     twopi
      parameter( twopi = two * pi )
      real*8     pis2
      parameter( pis2 = pi / two )
c
      data tipus/'stre','bend','out ','tors','lin1','lin2'/
      data wort/'k',' ','f'/
      data anull/1.0d0/
      ncard=0
      o = one
      nab=nek
      nab=nab/3.0+0.1
      i=0
      c1 = zero
      matf=0
      scaleold = zero
      scalex = one
      if (wri) write (iout,330)
   10 read (inp,340) we
      backspace inp
      do 20 k=1,3
         if (we.eq.wort(k)) go to 50
   20 continue
   30 continue
cmd   if (c1 .ne. 0 )c1=sqrt(1.0/c1)/anull
      if (c1 .ne. zero ) c1 = sqrt(one / c1) * scalex
      qq(i)=qq(i)*c1
      if (qonly) go to 310
      do 40 k=1,nek
         bmat(k,i)=bmat(k,i)*c1
   40 continue
      go to 310
   50 continue
      ncard=ncard+1
      read (inp,350) we,scale,c,typ,a
      if (scale .eq. zero) scale = one
      if (c .eq. zero) c = one
      if (we.eq.wort(2)) c1=c1+c**2
      if (we.ne.wort(1)) go to 100
         if (we.eq.wort(1)) then
          scaleold=scalex
          scalex=scale
         endif ! end of: if (we.eq.wort(1)) then
      if (i.eq.0) go to 80
      if (wri) write (iout,360)
cmd   c1=sqrt(1.0d0/c1)/anull
      c1 = sqrt(one / c1) * scaleold
      if (qonly) go to 70
      do 60 k=1,nek
         bmat(k,i)=bmat(k,i)*c1
   60 continue
   70 qq(i)=qq(i)*c1
   80 i=i+1
      qq(i) = zero
      c1=c**2
      if (qonly) go to 100
      do 90 j=1,nek
         bmat(j,i) = zero
   90 continue
  100 if (we.ne.wort(3)) go to 110
      matf=1
      go to 30
  110 do 120 k=1,4
         ia(k)=a(k)+0.1
  120 continue
      do 130 k=1,6
         if (typ.eq.tipus(k)) go to 140
  130 continue
      error=7
      write (iout,370) i
      go to 320
  140 if (wri) write (iout,380) i,typ,ia,c
      if (ka.lt.1.or.ka.gt.nab.or.kb.lt.1.or.kb.gt.nab) go to 300
      if (k.gt.1.and.(kc.lt.1.or.kc.gt.nab)) go to 300
      if (k.gt.2.and.(kd.lt.1.or.kd.gt.nab)) go to 300
      go to (150,160,180,210,230,250), k
c
c     ..... stretch
c
  150 call vektor (uu,r1,ka,kb,xa,ya,za)
      uu(1)=uu(1)*anull
      uu(2)=uu(2)*anull
      uu(3)=uu(3)*anull
      vv(1)=-uu(1)
      vv(2)=-uu(2)
      vv(3)=-uu(3)
      ia(3)=0
      ia(4)=0
      qq(i)=qq(i)+r1*c
      go to 270
c
c     .....bending
c
  160 call vektor (u,r1,ka,kc,xa,ya,za)
      call vektor (v,r2,kb,kc,xa,ya,za)
      co=scalar(u,v)
      si=s2(co)
      do 170 l=1,3
         uu(l)=(co*u(l)-v(l))/(si*r1)
         vv(l)=(co*v(l)-u(l))/(si*r2)
         ww(l)=-uu(l)-vv(l)
  170 continue
      ia(4)=0
      qq(i)=qq(i)+c*darcos(co)
      go to 270
c
c     .....out of plane
c
  180 call vektor (u,r1,ka,kd,xa,ya,za)
      call vektor (v,r2,kb,kd,xa,ya,za)
      call vektor (w,r3,kc,kd,xa,ya,za)
      call normal (v,w,z)
      steta=scalar(u,z)
      cteta=s2(steta)
      cfi1=scalar(v,w)
      sfi1=s2(cfi1)
      cfi2=scalar(w,u)
      cfi3=scalar(v,u)
      den=cteta*sfi1**2
      st2=(cfi1*cfi2-cfi3)/(r2*den)
      st3=(cfi1*cfi3-cfi2)/(r3*den)
      do 190 l=1,3
         vv(l)=z(l)*st2
         ww(l)=z(l)*st3
  190 continue
      call normal (z,u,x)
      call normal (u,x,z)
      do 200 l=1,3
         uu(l)=z(l)/r1
         zz(l)=-uu(l)-vv(l)-ww(l)
  200 continue
      cx=-c
      if (steta .lt. zero) cx=c
      qq(i)=qq(i)-cx*darcos(cteta)
      go to 270
c
c     ..... torsion
c
  210 call vektor (u,r1,ka,kb,xa,ya,za)
      call vektor (v,r2,kc,kb,xa,ya,za)
      call vektor (w,r3,kc,kd,xa,ya,za)
      call normal (u,v,z)
      call normal (w,v,x)
      co=scalar(u,v)
      co2=scalar(v,w)
      si=s2(co)
      si2=s2(co2)
      do 220 l=1,3
         uu(l)=z(l)/(r1*si)
         zz(l)=x(l)/(r3*si2)
         vv(l)=(r1 * co / r2 - one) * uu(l) - r3 * co2 /r2 * zz(l)
         ww(l)=-uu(l)-vv(l)-zz(l)
  220 continue
      co=scalar(z,x)
      u(1)=z(2)*x(3)-z(3)*x(2)
      u(2)=z(3)*x(1)-z(1)*x(3)
      u(3)=z(1)*x(2)-z(2)*x(1)
      co2=scalar(u,v)
      s=darcos(-co)
      if (co2 .lt. zero) s = -s
      if (s .gt. pis2) s = s - twopi
      qq(i)=qq(i)-c*s
c
c     .... remember that the range of this coordinate is -pi/2 to 3*pi/2
c     .... in order to shift the discontinuity off the planar position
c
      go to 270
c
c     ......linear complanar bending
c
  230 call vektor (u,r1,ka,kc,xa,ya,za)
      call vektor (v,r2,kd,kc,xa,ya,za)
      call vektor (x,r2,kb,kc,xa,ya,za)
      co=scalar(v,u)
      co2=scalar(x,v)
      qq(i) = qq(i) + c * ( pi - darcos(co) - darcos(co2) )
      call normal (v,u,w)
      call normal (u,w,z)
      call normal (x,v,w)
      call normal (w,x,u)
c
c     ..... coordinate positiv if atom a moves towards atom d
c
      do 240 l=1,3
         uu(l)=z(l)/r1
         vv(l)=u(l)/r2
         ww(l)=-uu(l)-vv(l)
  240 continue
      ia(4)=0
      go to 270
c
c     .....linear perpendicular bending

c
  250 call vektor (u,r1,ka,kc,xa,ya,za)
      call vektor (v,r2,kd,kc,xa,ya,za)
      call vektor (z,r2,kb,kc,xa,ya,za)
      call normal (v,u,w)
      call normal (z,v,x)
      do 260 l=1,3
         uu(l)=w(l)/r1
         vv(l)=x(l)/r2
         ww(l)=-uu(l)-vv(l)
  260 continue
      ia(4)=0
      co=scalar(u,w)
      co2=scalar(z,w)
      qq(i) = qq(i) + c * ( pi - darcos(co) - darcos(co2) )
  270 if (qonly) go to 10
      do 290 j=1,4
         m=ia(j)
         if (m.le.0) go to 290
         m=m-1
         j1=3*(j-1)
         do 280 l=1,3
            m1=3*m+l
            l1=j1+l
            bmat(m1,i)=uv(l1)*c+bmat(m1,i)
  280    continue
  290 continue
      go to 10
  300 error=6
      write (iout,390) i
  310 nq=i
  320 return
c
  330 format (/' definition of internal coordinates'/)
cmd  340 format (a4)
cmd  350 format (a4,6x,f10.4,a4,6x,5f10.4)
  340 format (a1)
  350 format (a1,f9.5,f10.4,a4,6x,4f10.4)
  360 format (1x)
  370 format (/' undefined int.coordinate type at no. ',i3/1x,40('*'))
  380 format (1x,i3,'.',a4,4x,4i10,f12.5)
  390 format (/' atoms erronously defined,coordinate no. ',i3/1x,
     & 40('*'))
c
      end
      subroutine vektor(u,r,i,j,xa,ya,za)
      implicit real*8(a-h,o-z)
      dimension u(3),xa(1),ya(1),za(1)
c       bildet den normierten entfernungsvektor vom kern j nach kern i
c        und die entfernung r
      u(1)=xa(i)-xa(j)
      u(2)=ya(i)-ya(j)
      u(3)=za(i)-za(j)
      r= sqrt(scalar(u,u))
c     write(6,1)xa(i),ya(i),za(i),xa(j),ya(j),za(j)
c     write(6,1)(u(k),k=1,3),r
c  1  format(6f10.6)
      call norm(u)
      return
      end
      subroutine mascal (x,n,l1,l2,na,nm1)
      implicit double precision(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
c
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      dimension amass(171),atmwt1(75),atmwt2(75),atmwt3(21)
      equivalence (amass(1),atmwt1(1)), (amass(76),atmwt2(1)),
     1            (amass(151),atmwt3(1))
      integer   atmno0(53)
      data atmno0/   1,   4,   6,   7,   9,  10,  12,  14,  17,  18,
     1    21,  22,  25,  26,  29,  30,  34,  38,  39,  41,  46,  49,
     2    52,  54,  57,  59,  62,  63,  68,  70,  75,  80,  82,  87,
     3    89,  95,  97, 101, 102, 103, 108, 114, 121, 123, 127, 130,
     4   138, 141, 149, 152, 161, 162, 169/
      data atmwt1/
     1     1.007825,   2.014102,   3.016030,   4.002603,   6.015123,
     2     7.016005,   9.012183,  10.01294,   11.00931,   12.00000,
     3    13.00336,   14.00307,   15.00011,   15.99492,   16.99913,
     4    17.99916,   18.99840,   19.99244,   20.99385,   21.99138,
     5    22.98977,   23.98504,   24.98584,   25.98259,   26.98154,
     6    27.97693,   28.97650,   29.97377,   30.97376,   31.97207,
     7    32.97146,   33.96787,   35.96708,   34.96885,   36.96590,
     8    35.96755,   37.96273,   39.96238,   38.96371,   40.96183,
     9    39.96259,   41.95863,   42.95878,   43.95549,   45.95369,
     a    44.95592,   45.95263,   46.95177,   47.94795,   48.94787,
     b    49.94478,   50.94396,   49.94605,   51.94051,   52.94065,
     c    53.93888,   54.93805,   53.93961,   55.93493,   56.93539,
     d    57.93327,   58.93319,   57.93534,   59.93078,   60.93105,
     e    61.92834,   63.92796,   62.92959,   64.92779,   63.92914,
     f    65.92604,   66.92713,   67.92485,   69.92533,   68.92558/
      data atmwt2/
     1    70.92470,   69.92425,   71.92208,   72.92346,   73.92118,
     2    75.92140,   74.92160,   73.92248,   75.91921,   76.91991,
     3    77.91731,   79.91653,   81.91671,   78.91833,   80.91629,
     4    77.92040,   79.91638,   81.91348,   82.91413,   83.91151,
     5    85.91062,   84.91180,   83.91343,   85.90928,   86.90889,
     6    87.90563,   88.90587,   89.90471,   90.90564,   91.90504,
     7    93.90632,   95.90829,   92.90638,   91.90681,   93.90509,
     8    94.90584,   95.90467,   96.90602,   97.90541,   99.90748,
     9    95.90760,   97.90529,   98.90594,   99.90422,  100.9056,
     a   101.9043,   103.9054,   102.9055,   101.9056,   103.9040,
     b   104.9051,   105.9035,   107.9039,   109.9052,   106.9051,
     c   108.9048,   105.9065,   107.9042,   109.9030,   110.9042,
     d   111.9028,   112.9044,   113.9034,   115.9048,   112.9041,
     e   114.9039,   111.9048,   113.9028,   114.9034,   115.9017,
     f   116.9030,   117.9016,   118.9033,   119.9022,   121.9035 /
      data atmwt3/
     1   123.9053,   120.9038,   122.9042,   119.9040,   121.9031,
     2   122.9043,   123.9028,   124.9044,   125.9033,   127.9045,
     3   129.9062,   126.9045,   123.9061,   125.9043,   127.9035,
     4   128.9048,   129.9035,   130.9051,   131.9042,   133.9054,
     5   135.9072 /
      nm1=x+.1
      if(nm1.eq.0) then
        if(x .eq. zero) x = 1000.d0
        xmss(na)=x
        do 5 i=0,n
   5      xmas(na,i)=x
        return
      endif
      if(l2.eq.0) then
      dxmin=100.d0
      do 2 k=1,171
      dx=abs(x-amass(k))
      if(dx.lt.dxmin) then
      nm1=k
      dxmin=dx
      endif
    2 continue
      xmss(na)=x
      xmas(na,0)=x
      if(dxmin.gt.0.001)
     &write(6,4) na,x,amass(nm1)
    4 format(' atomic mass for atom ',i4,' not in data base ',/,
     & ' program uses ',f10.6,' instead of ',f10.6)
      else
      nm1=atmno0(nm1)
      xmss(na)=amass(nm1)
      xmas(na,0)=amass(nm1)
      endif
    8 if(l1 .eq. 1) xmss(na) = one
      do 1 i=1,n
      ict=nxstp(na,i)+nm1
      xmas(na,i)=amass(ict)
    1 continue
      do 6 i=1,53
      if(nm1.eq.atmno0(i)) goto 7
    6 continue
    7 nm1=i
      return
      end
      subroutine schift(isteu,kl,nis)
      implicit double precision(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common /coord/ xsol(ncorm,2*nmax),ysol(ncorm,2*nmax),
     & zsol(ncorm,2*nmax),xist(ncorm,2*nmax),yist(ncorm,2*nmax),
     & zist(ncorm,2*nmax)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
c
      real*8    zero
      parameter(zero=0d0)
c
      xs    = zero
      ys    = zero
      zs    = zero
      xmass = zero
      do 10 k=1,natm
      if(sn(k).eq.0.) goto 10
      goto(2,3) isteu
  2   xs=xs+xist(k,kl)*xmas(k,nis)
      ys=ys+yist(k,kl)*xmas(k,nis)
      zs=zs+zist(k,kl)*xmas(k,nis)
      goto 9
  3   xs=xs+xsol(k,kl)*xmas(k,nis)
      ys=ys+ysol(k,kl)*xmas(k,nis)
      zs=zs+zsol(k,kl)*xmas(k,nis)
  9   xmass=xmass+xmas(k,nis)
 10   continue
      xs=xs/xmass
      ys=ys/xmass
      zs=zs/xmass
c     write(6,7)xs,ys,zs,xmass
      do 20 k=1,natm
      goto(12,13) isteu
 12   xist(k,kl)=xist(k,kl)-xs
      yist(k,kl)=yist(k,kl)-ys
      zist(k,kl)=zist(k,kl)-zs
c     write(6,7)xist(k,kl),yist(k,kl),zist(k,kl)
      goto 20
 13   xsol(k,kl)=xsol(k,kl)-xs
      ysol(k,kl)=ysol(k,kl)-ys
      zsol(k,kl)=zsol(k,kl)-zs
c     write(6,7)xsol(k,kl),ysol(k,kl),zsol(k,kl)
 20   continue
      return
  7   format(4f14.8)
      end
      subroutine umat1(kl,iout,din)
c
c   u-matrix : rotational correction to the theoretical dipol moment
c
      implicit real*8 (a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      dimension sj(3*nmax,3),sjp(3,3*nmax),sjpp(3,3*nmax),
     & dd(3,3),x(3*nmax),vk(3),sk(3,3),
     & kk(3*nmax),km(3*nmax),di(3),din(3)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      common /coord/ xsol(ncorm,2*nmax),ysol(ncorm,2*nmax),
     & zsol(ncorm,2*nmax),xist(ncorm,2*nmax),yist(ncorm,2*nmax),
     & zist(ncorm,2*nmax)
      common/dip/ dipx(nmax,0:nism),dipy(nmax,0:nism),dipz(nmax,0:nism)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
c
      real*8    zero,     one,     two
      parameter(zero=0d0, one=1d0, two=2d0)
c
      tol=1.d-8
c     thre1=1.d-8
      im=0
      do 10 j=1,natm
      if(sn(j) .eq. zero) goto 10
      im=im+3
      x(im-2)=xsol(j,kl)-xist(j,kl)
      x(im-1)=ysol(j,kl)-yist(j,kl)
      x(im)=zsol(j,kl)-zist(j,kl)
c     write(6,120)x(im-2),x(im-1),x(im)
      sj(im-2,1)=ysol(j,kl)+yist(j,kl)
      sj(im-2,2)=zsol(j,kl)+zist(j,kl)
      sj(im-2,3)= zero
      sj(im-1,1)=-xsol(j,kl)-xist(j,kl)
      sj(im-1,2)= zero
      sj(im-1,3)=zsol(j,kl)+zist(j,kl)
      sj(im,1)= zero
      sj(im,2)=-xsol(j,kl)-xist(j,kl)
      sj(im,3)=-ysol(j,kl)-yist(j,kl)
  10  continue
c     call scri(n,sj,im,3)
      do 20 i=1,im
      do 20 j=1,3
      sjp(j,i)=sj(i,j)
      sjpp(j,i)= zero
  20  continue
c     call scri(3,sjp,3,im)
      do 50 i=1,3
      do 45 ii=1,3
  45  dd(i,ii)= zero
      do 50 k=1,im
      do 50 j=1,3
  50  dd(i,j)=dd(i,j)+sj(k,i)*sjp(j,k)
c     call scri(3,dd,3,3)
      call osinv (dd,3,d,tol,kk,km)
c     call scri(3,dd,3,3)
      do 60 i=1,3
      do 60 k=1,3
      do 60 j=1,im
  60  sjpp(i,j)=sjpp(i,j)+sjp(k,j)*dd(i,k)
c     call scri(3,sjpp,3,im)
      do 65 i=1,3
      din(i)= zero
  65  vk(i)= zero
      do 70 i=1,3
      do 70 k=1,im
c     if(abs(x(k)).le.thre1) x(k)= zero
      vk(i)=vk(i)+sjpp(i,k)*x(k)
c     write(6,120)(vk(m),m=1,3)
  70  continue
      sk(1,1)= one
      sk(2,2)= one
      sk(3,3)= one
      sk(1,2)=-vk(1)
      sk(1,3)=-vk(2)
      sk(2,1)=vk(1)
      sk(2,3)=-vk(3)
      sk(3,1)=vk(2)
      sk(3,2)=vk(3)
      call osinv (sk,3,d,tol,kk,km)
c     call scri(3,sk,3,3)
c     write(iout,'(a20)')' u - matrix '
      do 30 i=1,3
      do 40 j=1,3
      sk(i,j) = two * sk(i,j)
      if(i .eq. j) sk(i,j) = sk(i,j) - one
  40  continue
c     write(iout,120)(sk(i,j),j=1,3)
  30  continue
      di(1)=xmu(kl)
      di(2)=ymu(kl)
      di(3)=zmu(kl)
      do 80 i=1,3
      do 80 j=1,3
  80  din(j)=din(j)+di(i)*sk(j,i)
c     write(iout,'(a20)')' alte dipolmomente '
c     write(iout,120)(di(i),i=1,3)
c     write(iout,'(a20)')' neue dipolmomente '
c     write(iout,120)(din(i),i=1,3)
 100  format(i4)
 110  format(a3,2i3,f3.0)
 120  format(3f14.8)
      return
      end
c
c----------------------------------------------------------------------
c
      subroutine dipder(nis,nditer,ndisp,iout,dineu)
      implicit double precision(a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nd1=10,neel=200,nextra=10)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax,nt=(nd+nd1)*nfreq)
      common/dipol/ xxin(4,2*nmax),ccin(4,2*nmax),
     & xmu(0:2*nmax),ymu(0:2*nmax),zmu(0:2*nmax),nxstp(ncorm,nism)
      common/dip/ dipx(nmax,0:nism),dipy(nmax,0:nism),dipz(nmax,0:nism)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      dimension dineu(3,2*nmax),d(3,50),dipbf(3,50),ausbf(50),
     & bx(3),ic(3)
c
      real*8    zero
      parameter(zero=0d0)
      integer dim
c
      write(iout,120)
      dim=ndi(1)
      do 1 i=1,dim
      k=1
      do 2 j=1,nditer
       if(xxin(1,j) .lt. zero) then
         nx=-xxin(1,j)+.1
         if(nx.ne.i) goto 2
         kx=ccin(1,j)+.1
         ic(1)=ccin(2,j)+.1
         ic(2)=ccin(3,j)+.1
         ic(3)=ccin(4,j)+.1
          bx(ic(1))=dipx(kx,nis)
          bx(ic(2))=dipy(kx,nis)
          bx(ic(3))=dipz(kx,nis)
          dipx(nx,nis)=xxin(2,j)*bx(1)
          dipy(nx,nis)=xxin(3,j)*bx(2)
          dipz(nx,nis)=xxin(4,j)*bx(3)
         goto 8
       endif
      nx=xxin(1,j)+.1
      if(nx.ne.i) goto 2
       if(j.gt.ndisp) then
       ausbf(k)=ccin(1,j)
       dipbf(1,k)=xmu(j)
       dipbf(2,k)=ymu(j)
       dipbf(3,k)=zmu(j)
       goto 9
      endif
      ausbf(k)=ccin(1,j)
      do 4 l=1,3
      dipbf(l,k)=dineu(l,j)
    4 continue
    9 k=k+1
      do 3 l=2,4
      if(xxin(l,j).gt.0.1) then
        write(iout,100) j
        k=k-1
      endif
    3 continue
    2 continue
      k=k-1
      if(k.eq.0) then
        write(iout,110) i
        dipx(i,nis) = zero
        dipy(i,nis) = zero
        dipz(i,nis) = zero
      elseif(k.eq.1) then
        dipx(i,nis)=(dipbf(1,1)-xmu(0))/ausbf(1)
        dipy(i,nis)=(dipbf(2,1)-ymu(0))/ausbf(1)
        dipz(i,nis)=(dipbf(3,1)-zmu(0))/ausbf(1)
      else
        lct=1
        do 5 ll=2,k
        do 5 mm=1,ll-1
        do 6 nn=1,3
        d(nn,lct)=(dipbf(nn,ll)-dipbf(nn,mm))/(ausbf(ll)-ausbf(mm))
        d(nn,lct+1) = zero
    6   continue
        lct=lct+1
    5   continue
        do 7 kl=1,lct-1
        do 7 nn=1,3
        d(nn,lct)=d(nn,lct)+d(nn,kl)
    7   continue
        dipx(i,nis)=d(1,lct)/(lct-1)
        dipy(i,nis)=d(2,lct)/(lct-1)
        dipz(i,nis)=d(3,lct)/(lct-1)
      endif
    8 write(iout,130) i,nis,dipx(i,nis),dipy(i,nis),dipz(i,nis)
    1 continue
      return
  100 format(' point ',i4,
     & ' not used for calculation of dipole derivative')
  110 format(' int. coordinate ',i4,' has no dipol moment',/
     &,' zero assumed for derivative ')
  120 format(/,' int.coord., isotopnr., dipole derivatives ',/)
  130 format(2x,i4,8x,i4,4x,3f10.6)
      end
      subroutine scri(ndim,a,naz,nas)
      implicit real*8 (a-h,o-z)
c     version scrih
c     ausdrucken der matrix a, dimension (naz,nas)
c----------------------------------------------------------------------
c
c
      dimension a(ndim,2), k(6)
c
    1 format (/)
   4  format(6x,6(i2,'.column ')/)
    5 format(i6,6f10.3)
      m = nas/6
      nasr = nas - 6*m
      k(6) = 0
      if (m.eq.0) goto 100
      do 2 i=1,m
      do 3 j=1,6
    3 k(j) = j + 6*(i-1)
      l = k(1)
      ll = k(6)
      write (6,1)
      write (6,4) (k(j),j=1,6)
      do 6 ii =1,naz
    6 write (6,5) ii,(a(ii,j),j=l,ll)
    2 continue
      if (nasr.eq.0) goto 200
  100 do 7 j=1,nasr
    7 k(j) = k(6) + j
      l = k(1)
      ll = k(nasr)
      write (6,1)
      write (6,4) (k(j),j=1,nasr)
      write (6,1)
      do 8 ii=1,naz
    8 write (6,5) ii,(a(ii,j),j=l,ll)
  200 return
      end
      subroutine kuelma(n,n1,c,s,a1,aij,ni)
      implicit real*8 (a-h,o-z)
      dimension a1(*),aij(n,2)
c
      real*8    zero
      parameter(zero=0d0)
c
      idx(i1,i2)=n2+i1*n+i2
      if(ni.eq.0) then
      j=0
      do 20 i=1,n
      do 20 k=1,i
      j=j+1
      aij(i,k)=a1(j)
      if(k.eq.i) go to 20
      aij(k,i) = zero
   20 continue
      else
      n2=n1-1-n*(1+n*(1-ni))
   10 do 30 i=1,n
   30 aij(i,i)=aij(i,i)+2*c*a1(idx(i,i))
      do 40 i=2,n
      j=i-1
      do 40 k=1,j
      aij(i,k)=aij(i,k)+c*(a1(idx(i,k))+a1(idx(k,i)))
      aij(k,i)=aij(k,i)+s*(a1(idx(i,k))-a1(idx(k,i)))
   40 continue
      endif
      return
      end
      subroutine phongf(iq,ip,nis)
      implicit real*8 (a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax)
      parameter(kmax=1000)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     & za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common/egy/ q(nq1),h(nh)
      common /bmat/ am(3*ncorm,3*ncorm)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common/matrix/ fstart(nh,nm),fscal(nh,nm),gmcf(nh,nm)
      common /phonon/ zvect,phirot,neighn,kval
      dimension br(nq1),bi(nq1),f(nq1),fx(nq1),vr(nq1),vi(nq1)
      dimension pdisp(nmax*kmax),rm(nq1)
      dimension nis(nm)
c
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
c
      eps=1.0d-09
      prma=1.0d-12
      spn=1.0d-78
      pi=4.0d0*atan(one)
      fakt=1302.9d0
      nnr=1024
      if(nis(nmole).ne.0) nis0=nmole
      ni=ndi(nmole)
      m1=ni*(ni+1)/2+1
      nk=3*natm/(2*neighn+1)
      nkk=ni*nk
      nk2=nk*nk
      ni2=ni*ni
      call wzero( rm, nk2, 1)
      call rmass(rm,nk,xmas(1,nmole-nis0))
      if(nmole.eq.nis0) call brot(am,3*ncorm,neighn,nk,ni,phirot)
cmk   if(phirot.ne. zero) then
c     do 150 i=1,ni
c        write (6,620) (am(j,i),j=1,3*natm)
c 150 continue
c 620 format (/,(1x,4(3f10.6,3x)))
cmk   endif
      iadc=0
      wertk = zero
      if(kval.ne.1) wertin=pi/dble(kval-1)
      do 100 kk=1,kval
         call wzero( br, nkk, 1 )
         call wzero( bi, nkk, 1 )
         call wzero( f,  nk2, 1 )
         do 110 j=0,neighn
            co=cos(wertk*dble(j))
            si=sin(wertk*dble(j))
            call bvonk(3*ncorm,ni,nk,co,si,am,br,bi,j)
            if(iq.eq.0)
     &       call kuelma(ni,m1,co,si,fstart(1,nmole),f,j)
            if(iq.eq.1)
     &       call kuelma(ni,m1,co,si,fscal(1,nmole),f,j)
110      continue
         if((kk.eq.1 .or. kk.eq.kval) .and. ip.ne.0) then
            write(ip,*)
            do 529 i=1,nk
               write(ip,140) (br(j+(i-1)*ni),j=1,ni)
529         continue
            write(ip,*)
            do 531 i=1,nk
               write(ip,140) (bi(j+(i-1)*ni),j=1,ni)
531         continue
            write(ip,*)
            do 129 i=1,ni
               write(ip,140) (f(i+(j-1)*ni),j=1,i)
140            format(8f10.6)
129         continue
         endif
         if(kk.eq.kval) call wzero( bi, nkk, 1 )
         call uthm(ni,nk,br,bi,f,fx)
         call wzero( vr, nk2, 1)
         call uthm(nk,nk,rm,vr,fx,f)
         call ceig(nk,nk,6,eps,prma,spn,f,eig,vr,vi)
         if(kk.eq.1) write(6,77)
77       format(/' cartesian displacements at k=0')
         if(kk.eq.(kval/2+1).and.mod(kval,2).eq.1) then
            write(6,477)
477         format(/' cartesian displacements at k=pi/2')
            call scri(nk,vr,nk,nk)
            call scri(nk,vi,nk,nk)
         endif
         if(kk.eq.kval) write(6,377)
377      format(/' cartesian displacements at k=pi')
         if(kk.eq.1 .or. kk.eq.kval) call scri(nk,vr,nk,nk)
         do 130 j=1,nk
            aa=eig(j)
            aa = max(aa, zero)
130         pdisp(iadc+j)=fakt*sqrt(aa)
            iadc=iadc+nk
            wertk=wertk+wertin
100      continue
         write(6,200)
200      format(/,' phonon dispersion')
         call scri(nk,pdisp,nk,kval)
         if(ip.ne.0) then
            open(8,file='pdisp',form='formatted',status='unknown')
            write(8,'(2i5)') nk,kval
            i1=0
            do 210 j=1,kval
               write(8,'(4d20.13)')(pdisp(i1+i),i=1,nk)
210            i1=i1+nk
               close(8)
            endif
c  testmk-----
c     stop
c     do 500 i=1,ni
c     ii=(i-1)*ni+i
c     q(ii)=q(ii)+1.d-6
c 500 continue
c     call scri(ni,q,ni,ni)
c     return
c    ------
            end
      subroutine rmass(a,nk,am)
      implicit real*8 (a-h,o-z)
      dimension a(*),am(*)
      real*8    one
      parameter(one=1d0)
      n=1
      do 30 i=1,nk
      idx=(i-1)/3+1
      a(n) = one / sqrt(am(idx))
   30 n=n+nk+1
      return
      end
      subroutine bvonk(n,ni,nk,c,s,a1,ar,ai,nz)
      implicit real*8 (a-h,o-z)
      dimension a1(n,*),ar(*),ai(*)
      na=1
      if(nz.eq.0) then
      do 20 j=1,nk
      do 20 i=1,ni
      ar(na)=a1(j,i)
   20 na=na+1
      else
      isf=nk*(2*nz-1)
      do 30 j=isf+1,isf+nk
      do 30 i=1,ni
        ar(na)=ar(na)+c*(a1(j,i)+a1(j+nk,i))
        ai(na)=ai(na)+s*(a1(j,i)-a1(j+nk,i))
   30 na=na+1
      endif
      return
      end
      subroutine brot(b,ndim,n,nk,ni,phi)
      implicit real*8 (a-h,o-z)
      dimension b(ndim,*)
      do 1 j=1,n
      co=cos(phi*dble(j))
      si=-sin(phi*dble(j))
      is=(2*j-1)*nk
       do 2 i=1,ni
       do 2 k=1,nk,3
        bx1=b(is+k,i)
        bx2=b(is+k+nk,i)
        b(is+k,i)=co*b(is+k,i)+si*b(is+k+1,i)
        b(is+k+1,i)=co*b(is+k+1,i)-si*bx1
        b(is+k+nk,i)=co*b(is+k+nk,i)-si*b(is+k+nk+1,i)
        b(is+k+nk+1,i)=co*b(is+k+nk+1,i)+si*bx2
   2   continue
   1  continue
      return
      end
      subroutine uthm (nm,n,ur,ui,h,htr)
      implicit real*8 (a-h,o-z)
      parameter(nmax=100)
c      transformation h' =(u+) h u
      dimension ur(nm,*),ui(nm,*),h(nm,*),htr(n,*)
      dimension xr(nmax),xi(nmax)
      real*8    zero
      parameter(zero=0d0)
      do 250 j=1,n
      do 185 m=1,nm
      xr(m) = zero
      xi(m) = zero
  185 continue
      do 200 i=1,nm
      do 190 k=1,nm
      if(k.eq.i) go to 20
      if(k.gt.i) go to 40
      xr(i)=xr(i)+h(i,k)*ur(k,j)+h(k,i)*ui(k,j)
      xi(i)=xi(i)+h(i,k)*ui(k,j)-h(k,i)*ur(k,j)
      go to 190
   20 xr(i)=xr(i)+h(i,k)*ur(k,j)
      xi(i)=xi(i)+h(i,k)*ui(k,j)
      go to 190
   40 xr(i)=xr(i)+h(k,i)*ur(k,j)-h(i,k)*ui(k,j)
      xi(i)=xi(i)+h(k,i)*ui(k,j)+h(i,k)*ur(k,j)
  190 continue
  200 continue
      do 300 l=1,n
      fr = zero
      fi = zero
      if(l.lt.j) go to 170
      do 290 i=1,nm
      fr=fr+ur(i,l)*xr(i)+ui(i,l)*xi(i)
  290 continue
      htr(l,j)=fr
      go to 300
  170 do 292 i=1,nm
      fi =fi+ur(i,l)*xi(i)-ui(i,l)*xr(i)
  292 continue
      htr(l,j)=fi
  300 continue
  250 continue
      return
      end
c     # this routine should be eliminated and replaced with lapack call.
      subroutine ceig (nm,n,nam,eps,prma,spn,a,d,zr,zi)
      implicit real*8 (a-h,o-z)
c
c     eigenvalues and eigenvectors of a hermitian complex matrix h
c     merkel muenchen 73
c
c     real functions dsqrr and dabb  ( route and absolute of a
c     real number )  are used.
c     input:
c
c     nm        corresponding value of the actual dimension statement
c     n         oder of the matrix
c     nam       number of the standard output medium
c     eps       iteration stops, if some subdiagonal element in
c               the tridiagonalform is not greater then eps*norm
c               of the corresponding leading principal minor,
c     prma      precision of the machine,
c     spn       smallest positive number ,
c     a(nm,nm)  collects real and imaginary parts of the matrix h
c               a(i,k)=re(h(i,k)) for i>=k
c               a(i,k)=im(h(i,k)) for i<k.
c
c     output:
c
c     d(nm)     gives the eigenvalues in ascending sequence,
c     zr(nm,nm) the columns of the matrix zr represent the
c               real part of the complex eigenvectors,
c     zi(nm,nm) represents the corresponding imag. part
c
      dimension a(nm,nm),zr(nm,nm),zi(nm,nm),d(nm)
      dimension e(100),e2(100)
c
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
c
      if ( n .gt. 100 ) then
c        # local array e(:) and e2(:) exceeded.
         call bummer('ceig: n>100, n=', n, 2)
      endif
      if (n.le.nm) goto 10
    1 format (' ceig: incorrect dimension',2i5)
      write(nam,1) n,nm
      call bummer( 'ceig: n-nm=', (n-nm), 2)
   10 do 30 i=1,nm
      do 20 j=1,nm
   20 zr(j,i) = zero
   30 zr(i,i) = one
      call ctred (nm,n,spn,prma,a,d,e,e2)
      call tql2 (nm,n,eps,d,e,zr)
      call ctrbak (nm,n,spn,a,zr,zi)
      return
      end
c     # this routine should be eliminated.
      subroutine tql2 (nm,n,eps,d,e,x)
      implicit real*8 (a-h,o-z)
      dimension d(nm), e(nm), x(nm,nm)
      if (n .eq. 1) goto 400
      do 230 i = 2,n
  230    e(i-1) = e(i)
      b=.0d0
      r=.0d0
      f=.0d0
      e(n)=.0d0
      do 340 l=1,n
      h=eps*(abs (d(l))+abs (e(l)))
      if (h.gt.b) b=h
      do 240 j=l,n
      if (abs (e(j)).le.b) goto 250
  240 continue
  250 if(j.eq.l) go to 340
  260 g=d(l)
      p=(d(l+1)-g)*.5d0/e(l)
      if(p) 270,280,280
  270 p=p-r
      go to 290
  280 p=p+r
  290 d(l)=e(l)/p
      h=g-d(l)
      k=l+1
      do 300 i=k,n
  300 d(i)=d(i)-h
      f=f+h
      p=d(j)
      c=1.d0
      s=.0d0
      j1=j-1
      do 330 ni=l,j1
      ii=l+j1-ni
      do 330 i=ii,ii
      g=c*e(i)
      h=c*p
      if (abs (p).lt.abs (e(i))) goto 310
      c=e(i)/p
      c1=c*c+1.d0
      r=sqrt(c1)
      e(i+1)=s*p*r
      s=c/r
      c=1.0d0/r
      go to 320
  310 c=p/e(i)
      c1=c*c+1.d0
      r=sqrt(c1)
      e(i+1)=s*e(i)*r
      s=1.d0/r
      c=c/r
  320 p=c*d(i)-s*g
      d(i+1)=h+s*(c*g+s*d(i))
      do 330 k=1,n
      h=x(k,i+1)
      x(k,i+1)=x(k,i)*s+h*c
  330 x(k,i)=x(k,i)*c-h*s
      e(l)=s*p
      d(l)=c*p
      if (abs (e(l)).gt.b) go to 260
  340 d(l)=d(l)+f
      ni=n-1
  350 do 380i=1,ni
      k=i
      p=d(i)
      j1=i+1
      do 360j=j1,n
      if(d(j).ge.p) go to 360
      k=j
      p=d(j)
  360 continue
      if (k.eq.i) goto 380
      d(k) =d(i)
      d(i)=p
      do 370 j=1,n
      p=x(j,i)
      x(j,i)=x(j,k)
  370 x(j,k)=p
  380 continue
  400 return
      end
      subroutine ctred (nm,n,spn,prma,a,d,e,e2)
      implicit real*8 (a-h,o-z)
      dimension a(nm,nm),d(nm),e(nm),e2(nm)
c
      real*8    zero,     one,     two,     half
      parameter(zero=0d0, one=1d0, two=2d0, half=5d-1)
c
      tol=spn/prma
      ii1=n+1
      do 130 ii=1,n
      ii1=ii1-1
      do 130 i=ii1,ii1
      h= zero
      d(i)=a(i,i)
      if (i.eq.1) goto 30
      l=i-1
      m=l-1
      if (m.eq.0) goto 15
      do 10 k=1,m
   10 h=h+a(i,k)*a(i,k)+a(k,i)*a(k,i)
   15 f=a(l,i)*a(l,i)+a(i,l)*a(i,l)
      sr=sqrt(f)
      if (h.gt.spn) goto 20
      e(i)=-sr
      e2(i)=f
      goto 120
   20 h=h+f
      if (h.ge.tol) goto 40
   30 e(i)= zero
      e2(i)= zero
      h = zero
      goto 120
   40 si=sqrt(h)
      e(i)=si
      e2(i)=h
      h=sr*si+h
      if (sr.gt.spn) goto 50
      a(i,l)=si
      a(l,i)= zero
      goto 60
   50 f=(sr+si)/sr
      a(i,l)=a(i,l)*f
      a(l,i)=a(l,i)*f
   60 f = zero
      do 90 j=1,l
      sr= zero
      si= zero
      m=j-1
      if (m.eq.0) goto 75
      do 70 k=1,m
      sr=a(i,k)*a(j,k)+a(k,i)*a(k,j)+sr
      si=a(k,i)*a(j,k)-a(i,k)*a(k,j)+si
   70 continue
   75 sr=a(i,j)*a(j,j)+sr
      si=a(j,i)*a(j,j)+si
      j1=j+1
      if (j1.gt.l) goto 85
      do 80 k=j1,l
      sr=a(k,j)*a(i,k)-a(j,k)*a(k,i)+sr
      si=a(k,j)*a(k,i)+a(j,k)*a(i,k)+si
   80 continue
   85 d(j)=sr
      e(j)=si
      f=f+sr*a(i,j)+si*a(j,i)
   90 continue
      f = half * f / h
      do 110 j=1,l
      gr=(d(j)-f*a(i,j))/h
      d(j)=gr
      gi=(e(j)-f*a(j,i))/h
      e(j)=gi
      sr=a(i,j)
      si=a(j,i)
      m=j-1
      if (m.eq.0) goto 105
      do 100 k=1,m
      a(j,k)=a(j,k)-d(k)*sr-gr*a(i,k)-e(k)*si-gi*a(k,i)
      a(k,j)=a(k,j)+d(k)*si+gi*a(i,k)-e(k)*sr-gr*a(k,i)
  100 continue
  105 a(j,j) = a(j,j) - (gr * sr + gi * si) * two
  110 continue
  120 a(i,i)=h
  130 continue
      return
      end
      subroutine ctrbak (nm,n,spn,a,zr,zi)
      implicit real*8 (a-h,o-z)
      dimension a(nm,nm),zr(nm,nm),zi(nm,nm)
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
      do 70 i=1,n
      l=i-1
      if (i.eq.1) goto 10
      sr=sqrt(a(i,l)*a(i,l)+a(l,i)*a(l,i))
      if (sr.lt.spn) goto 10
      ur=-a(i,l)/sr
      ui=a(l,i)/sr
      sr=ur*vr-ui*vi
      vi=ur*vi+ui*vr
      vr=sr
      goto 20
   10 vr=one
      vi=zero
   20 do 30 j=1,n
      zi(i,j)=vi*zr(i,j)
      zr(i,j)=vr*zr(i,j)
   30 continue
      if (i.eq.1) goto 70
      h=a(i,i)
      if (h.lt.spn) goto 70
      do 60 j=1,n
      sr=zero
      si=zero
      do 40 k=1,l
      sr=a(i,k)*zr(k,j)+a(k,i)*zi(k,j)+sr
      si=a(i,k)*zi(k,j)-a(k,i)*zr(k,j)+si
   40 continue
      sr=sr/h
      si=si/h
      do 50 k=1,l
      zr(k,j)=zr(k,j)-sr*a(i,k)+si*a(k,i)
      zi(k,j)=zi(k,j)-si*a(i,k)-sr*a(k,i)
   50 continue
   60 continue
   70 continue
      return
      end
c
c**************************************************************
c
      subroutine fceig(iq,ik)
      implicit real*8 (a-h,o-z)
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax)
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      common/matrix/ fstart(nh,nm),fscal(nh,nm),gmcf(nh,nm)
      common/egy/ q(nq1),h(nh)
      common /bmat/ am(3*ncorm,3*ncorm)
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef
      common /phonon/ zvect,phirot,neighn,kval
      real*8 vr
      common /cartfreq/ vr(nq1)
      dimension br(nq1),bi(nq1),fx(nq1),rm(nq1),f(nq1),vi(nq1)
c
      character*6 ind1(nfreq)
      character*4 ind2(nfreq)
c
      real*8    one
      parameter(one=1d0)
c
      do i=1,nfreq
      write(ind2(i),'(i4)')i
      enddo
c
      eps=1.0d-09
      prma=1.0d-12
      spn=1.0d-78
      pi=4.0d0*atan(one)
      fakt=1302.9d0
      ni=ndi(nmole)
      m1=ni*(ni+1)/2+1
      nk=3*natm/(2*neighn+1)
      nkk=ni*nk
      nk2=nk*nk
      ni2=ni*ni
c
      icount = 1
      do i=1,nk,3
      write(ind1(i),'(a4," x")')atname(icount)
      write(ind1(i+1),'(a4," y")')atname(icount)
      write(ind1(i+2),'(a4," z")')atname(icount)
      icount = icount + 1
      enddo
      call wzero( rm, nk2, 1 )
      call wzero( f,  ni2, 1 )
      call wzero( bi, nkk, 1 )
      call wzero( vr, nk2, 1 )
      in=1
      if=1
      do 1 i=1,ni
      do 2 j=1,i
      ig=(j-1)*ni+i
      if(iq.eq.0) then
       f(ig)=fstart(if,nmole)
      else
       f(ig)=fscal(if,nmole)
      endif
   2  if=if+1
   1  continue
      do 3 j=1,nk
      do 3 i=1,ni
      br(in)=am(j,i)
   3  in=in+1
c     call scri(ni,f,ni,ni)
      call rmass(rm,nk,xmas(1,nmole-1))
      call uthm(ni,nk,br,bi,f,fx)
      call uthm(nk,nk,rm,vr,fx,q)
      call ceig(nk,nk,ik,eps,prma,spn,q,eig,vr,vi)
c
      do i=1,nk
         temp=fakt*sqrt(abs(eig(i)))
         eig(i)= sign(temp,eig(i))
      enddo
      call prvbkc_new("cartesian frequencies",vr,eig,nk,nk,nk,ind1,
     & ind2,"freq",1,ik)
cmd
c     write(ik,200)
c 200 format(/,' frequencies',/)
c     do 130 j=1,nk
c     eig(j)=dmax1(eig(j),0.0d0)
c     eig(j)=fakt*sqrt(eig(j))
c 130 write(ik,'(f10.2)') eig(j)
cmd
      return
      end
c
c***********************************************************************
c
      subroutine moldenout(ik)
       implicit none
c  ##  parameter & common block section
c
      real*8 au2ang
      parameter (au2ang=0.529177249d+00)
c
      integer nmax, ncorm, nism, nfreq, nm, nd, ntype
      parameter(nmax=100,ncorm=80,nism=20,nfreq=300,nm=25,nd=20,ntype=3)
c
      integer nh, nq1
      parameter(nh=nmax*(nmax+1)/2,nq1=nmax*nmax)
c
      real*8 ex, sf, eig, ss, we
      integer mut
      common /work/ ex(nfreq),sf(nd),mut(nfreq),eig(nfreq),
     & ss(nfreq),we(nfreq)
c
      real*8 xmas, xa, ya, za, xmss, sn
      character*4 atname
      common /xmas/ xmas(ncorm,0:nism),   xa(ncorm),    ya(ncorm),
     &              za(ncorm),            xmss(ncorm),  sn(ncorm)
      common /cxmas/ atname(ncorm)
c
      real*8 q, h
      common/egy/ q(nq1),h(nh)
c
      integer ndi, nmo, nmole, jlow, mf, nsumf, natm, mef
      common/dimen/ ndi(nm),nmo,nmole,jlow(nm),mf,nsumf,natm,mef

      integer neighn,kval
      real*8 zvect,phirot
      common /phonon/ zvect,phirot,neighn,kval
c
      real*8 zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     &       one,two,eight,sixt,ten2,ten3
      common/number/ zero,tenm8,tenm6,tenm3,tenm2,psix,tenm1,half,alone,
     &   one,two,eight,sixt,ten2,ten3
c
      real*8 vr
      common /cartfreq/ vr(nq1)
c
c  ##  integer section
c
      integer ik,i,icount
      integer j
      integer nk
c
c  ##  real*8 section
c
      real*8 fact
c
c-----------------------------------------------------------------
c
      write(ik,'(/60(1h#)/)')
      write(ik,*)'-- > start of molden input'
      write(ik,*)'[Molden Format]'
c
      nk=3*natm/(2*neighn+1)
      write(ik,*)'[FREQ]'
      write(ik,'(f10.2)')(eig(i),i=1,nk)
c
      fact=one/au2ang
      write(ik,*)'[FR-COORD]'
      do i=1,natm
      write(ik,'(a4,3f13.6)')atname(i),xa(i)*fact,ya(i)*fact,
     &                       za(i)*fact
      enddo
c
      write(ik,*)'[FR-NORM-COORD]'
      icount = 1
      do i=1,nk
      write(ik,*)'vibration  ',i
         do j=1,natm
         fact=one/(sqrt(xmss(j)*au2ang))
         write(ik,'(3f12.5)')vr(icount)*fact,vr(icount+1)*fact,
     &    vr(icount+2)*fact
         icount=icount+3
         enddo ! end of do j=1,natom
      enddo ! end of: do i=1,nk
c
      write(ik,*)'--> end of molden input'
c
      return
      end
c
c***********************************************************************
c
      subroutine prvbkc_new(
     & title, z, v, ldz, nr, nc, labr, labc, labv, ifmt, nlist )
c
c  print a rectangular matrix and a corresponding vector with labels.
c
c  this version prints 8 columns at a time with one of three formats.
c  parameter ncol and the appropriate formats should be modified to
c  print a different number of columns or to use different formats.
c
c  input:
c  title    = optional title.  only printed if (title.ne.' ').
c  z(*,*)   = matrix to be printed.
c  v(*)     = vector to be printed. v(i) corresponds to z(*,i).
c  ldz      = effective leading dimension of z(*).
c  nr       = number of rows to print.
c  nc       = column dimension.
c  labr(*)  = character row labels.
c  labc(*)  = character column labels.
c  labv     = character vector label.
c  ifmt     = format type (1:f, 2:e, 3:g).
c  nlist    = output unit number.
c
c  18-oct-90 prvblk() modified. title,labr(*),labc(*) change. -rls
c  format statement assignment version 5-jun-87 (rls).
c  ron shepard 17-may-84.
c
       implicit none
      integer    ncol
      parameter (ncol=8)

      character*18 fstring(6)
      data fstring /'(1x,a8,8f10.5)    ',  
     .                   '(1x,a8,1p,8e15.6) ',
     .                   '(1x,a8,1p,8g15.6) ',
     .                   '(/1x,a8,8f10.2)   ',
     .                   '(/1x,a8,1p,8e15.6)',
     .                   '(/1x,a8,1p,8g15.6)'/
10    format(/8x,8(1x,a8,1x))
1     format(1x,a8,8f10.5)
2     format(1x,a8,1p,8e15.6)
3     format(1x,a8,1p,8g15.6)
11    format(/1x,a8,8f10.2)
12    format(/1x,a8,1p,8e15.6)
13    format(/1x,a8,1p,8g15.6)
20    format(/10x,a)
c
      integer ldz, nr, nc, ifmt, nlist
      character*(*) title, labr(*), labc(*), labv
      real*8 z(ldz,nc), v(nc)
c
      integer fmtz, fmtv, jt, jlast, jstrt, j, i
      real*8     zero
      parameter (zero=0d0)
c
      if(ifmt.le.1)then
         fmtz=1
         fmtv=4
      elseif(ifmt.eq.2)then
         fmtz=2
         fmtv=5
      else
         fmtz=3
         fmtv=6
      endif
c
      if ( title .ne. ' ' ) write(nlist,20) title
c
      jlast = 0
      do 400 jstrt = 1, nc, ncol
         jlast = min( nc, jlast+ncol )
c
         write(nlist,10) ( labc(j), j = jstrt, jlast )
         write(nlist,fstring(fmtv)) labv, ( v(j), j = jstrt, jlast)
         write(nlist,*)
c
         do 300 i = 1, nr
c
c           # print the row if a nonzero element is found.
c
            do 100 j = jstrt, jlast
               if ( z(i,j) .ne. zero ) then
                    write(nlist,fstring(fmtz)) 
     .                labr(i), ( z(i,jt), jt=jstrt,jlast)
                  go to 101
               endif
100         continue
101         continue
300      continue
400   continue
c
      return
      end

