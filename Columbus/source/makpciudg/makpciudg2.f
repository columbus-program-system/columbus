!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      block data no2

         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,   NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C

c
c     # standard irrep multiplication table.
c
c
c     # buffer and record sizes for output files:
c     # maxbuf = record length for 3-, 4-external integrals.
c     # intmxo = 0-, 1-, and 2-external sequential file record length.
c     #          this is also used for the integral records of the
c     #          diagonal ints file.

*@if (defined sun && defined osu) || (defined cray && defined osu)
*       data maxbuf/32767/, maxbl3/32767/, maxbl4/32767/, intmxo/765/
*@elif defined  ibmcms
*      data maxbuf/30000/, maxbl3/30000/, maxbl4/30000/, intmxo/4095/
*@elif defined  ibmmvs
*      data maxbuf/2383/, maxbl3/2383/, maxbl4/2383/, intmxo/1070/
*@else
       data maxbuf/32767/, maxbl3/60000/, maxbl4/60000/, intmxo/4095/
*@endif


      end
      subroutine drvsort( core, lencor,szdg,szof,sz3w,sz3x,sz4w,sz4x,
     .                     nof1e)
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
C
      INTEGER             ATEBYT,      FORBYT
C
C     Parameter variables
C
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)



      integer maxsym
      parameter (maxsym=8)
C
C     Argument variables
C
      INTEGER             LENCOR,      SZ3W,        SZ3X,        SZ4W
      INTEGER             SZ4X,        SZDG,        SZOF
C
      REAL*8              CORE(LENCOR)
C
C     Local variables
C
      INTEGER             I,           ICD(50),     ITOTAL
      INTEGER             NDG0X
      INTEGER             NDG1E,       NDG2X,       NDG4X
      INTEGER             NLIST
      INTEGER             NOF1E,       NREC3W,      NREC3X
      INTEGER             NREC4W,      NREC4X,      NRECDG,      NRECOF
      INTEGER             NUM3X(2)
      INTEGER             NUM4X(2),    NUMX(8)
C
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,   NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C     Common variables
C
      INTEGER             NIOT,        NROW
C
      COMMON / DRTINFO/   NROW,        NIOT
C
      integer srtopt,ldamin,ldamax,ldainc,
     .            prnopt,molcas
      real*8  thresh

      namelist /input/
     & srtopt, ldamin, ldamax, ldainc,
     & maxbuf, maxbl3, maxbl4, intmxo,
     & prnopt, molcas, thresh


        open(unit=7,file='cisrtin',status='old')
        read(7,input,end=3,err=4)
 3      close(unit=7)
        goto 5
 4      continue
        call bummer_interact(' namelist input error in ciudgin',0,2)
 5      continue


C
c
c
      icd(1) = 1
      nlist = 20
c
      nnlev = (nlevel * (nlevel + 1)) / 2
      icd(1) = 1
      icd(2) = icd(1) + forbyt( nsym*nnlev )
      icd(3) = icd(2) + forbyt(nnlev)
c
      itotal = icd(3) + forbyt(nmomx)
      if ( itotal .gt. lencor ) then
         write(nlist,6090)'indxdg',(icd(i),i=1,8),itotal
         call bummer_interact('cisrt: indxdg() (itotal-lencor)=',
     &    (itotal-lencor),faterr)
      endif
c

      call indxdg(nsym,levsym,ndg0x,ndg2x,ndg4x,core(icd(3)))
      call indxof(core(icd(1)), core(icd(2)), numx,
     .             core(icd(3)) )
c
      call ptofdg( nsym, niot,num3x,num4x)

      ndg1e=0
      do i=1,nsym
         ndg1e=ndg1e+nmpsy(i)
      enddo

      nof1e=0
      do i=1,nsym
          nof1e=nof1e+nepsy(i)*(nepsy(i)+1)/2
      enddo

      write(nlist,6070) ndg1e,ndg0x,ndg2x,ndg4x
      write(nlist,6075) (numx(i),i=1,8),nof1e
      write(nlist,6080) (num3x(i),i=1,2),(num4x(i),i=1,2)
6070  format(' Orig.  diagonal integrals:  1electron:',i10/
     &       '                             0ext.    :',i10/
     &       '                             2ext.    :',i10/
     &     '                             4ext.    :',i10//)
6075  format(' Orig. off-diag. integrals:  4ext.    :',i10/
     &       '                             3ext.    :',i10/
     &       '                             2ext.    :',i10/
     &       '                             1ext.    :',i10/
     &       '                             0ext.    :',i10/
     &       '                             2ext. SO :',i10/
     &       '                             1ext. SO :',i10/
     &       '                             0ext. SO :',i10/
     &     '                             1electron:',i10//)
6080  format(' Sorted integrals            3ext.  w :',i10, ' x :',i10/
     &   '                             4ext.  w :',i10, ' x :',i10//)

      write(22,*) 
     .'ndg4ext ndg2ext ndg0ext                                '
      write(22,6071) ndg4x,ndg2x,ndg0x
      write(22,*) 
     .'n4ext n3ext n2ext n1ext n0ext n2extso n1extso n0extso  '
      write(22,6071) (numx(i),i=1,8)
6071  format(8(i10,1x))

      nrecdg=0
      nrecdg=nrecdg+ (ndg1e-1)/intmxo +1
      nrecdg=nrecdg+ (ndg0x-1)/intmxo +1
      nrecdg=nrecdg+ (ndg2x-1)/intmxo +1
      nrecdg=nrecdg+ (ndg4x-1)/intmxo +1

c     write(nlist,6105) nrecdg,intmxo,nrecdg*intmxo
c6105  format('diagint file: ',i6,' records  of ',i6,' WP each',
c     .  '=> ',i10,' WP total')

       nrecof=0
       nrecof=nrecof+ (nof1e-1)/intmxo+1
       do i=3,5
          nrecof=nrecof+ (numx(i)-1)/intmxo+1
       enddo
       if (spnorb) then
       do i=6,8
          nrecof=nrecof+ (numx(i)-1)/intmxo+1
       enddo
       endif

c     write(nlist,6106) nrecof,intmxo,nrecof*intmxo
c6106  format('ofdgint file: ',i6,' records  of ',i6,' WP each',
c     .  '=> ',i10,' WP total')


       nrec3w=(num3x(1)-1)/maxbuf+1
       nrec3x=(num3x(2)-1)/maxbuf+1
       nrec4w=(num4x(1)-1)/maxbuf+1
       nrec4x=(num4x(2)-1)/maxbuf+1

c     write(nlist,6107) 'fil3w',nrec3w,maxbuf,nrec3w*maxbuf
c     write(nlist,6107) 'fil3x',nrec3x,maxbuf,nrec3x*maxbuf
c     write(nlist,6107) 'fil4w',nrec4w,maxbuf,nrec4w*maxbuf
c     write(nlist,6107) 'fil4x',nrec4x,maxbuf,nrec4x*maxbuf
c6107  format(a5,'   file: ',i6,' records  of ',i6,' WP each',
c     .  '=> ',i10,' WP total')

      szdg=nrecdg*intmxo
      szof=nrecof*intmxo
      sz3w=nrec3w*maxbuf
      sz3x=nrec3x*maxbuf
      sz4w=nrec4w*maxbuf
      sz4x=nrec4x*maxbuf
c
      return
c
6090  format(' program stopping due to space allocation error.'/
     & 1x,a,(1x,10i8))
      end
      subroutine ptofdg( nsym, niot, num3x,num4x)
c
c  this subroutine controls the writing of the off-diagonal
c  sorted integral files.
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Argument variables
C
      INTEGER  NIOT,       NSYM
      INTEGER             NUM3X(2),    NUM4X(2)
C
C     Local variables
C
C

C
C
c     # incnt = offset for the first integral in the current segment.
c     #         iblkst.
c     # num = address of the last integral in the current segment.
c
      call prep4e( nsym, num4x)
c
c
      call prep3e( nsym, niot, num3x )

c
      return
      end
      subroutine prep4e( nmsym, num4x)
c
c  determine the block structure of the 4-external integrals and write
c  the blocks to the appropriate file.  the integrals are first read
c  into a segment buffer in triple-sort order into the array pints(*).
c  linear combinations of these integrals are formed and placed into
c  the array wxints(*).  the combinations are then moved into the
c  buffers bufw(*) and bufx(*) and written out as they are filled.
c  each combination block consists of all possible i and j indices
c  consistent with the parameters lsym, ksym, lstart, lfinal,
c  kstart, and kfinal.  these parameters are written at the beginning
c  of the block into two working precision words.  the array wxints(*)
c  must hold at least one set of ij indices and should hold several so
c  as to minimize the two-word overhead and the subsequent processing
c  overhead in the diagonalization program.
c  blocks are not split between buffers.  the expected wasted space in
c  each buffer is therefore half of the average block size.  see the
c  put*34() routines for more details of the da buffer processing.
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      integer             maxsym
      parameter           (maxsym = 8)
      integer             nmomx
      parameter           (nmomx = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             NMSYM,       NUM4X(2)
C
C     Local variables
C
      INTEGER             CNTW12,      CNTW3,       CNTWX,       CNTX12
      INTEGER             CNTX3,       CW12OD,      CW3OD,       CWXOD
      INTEGER             CX12OD,      CX3OD,       FINBL,       I
      INTEGER             IBLOCK,      ICONTW,      ICONTX,      ICTODW
      INTEGER             ICTODX,      IOFFS,       IS,          ISYM
      INTEGER             JS,          JSYM,        K1,          KLS
      INTEGER             KS,          KSYM,        L1,          LS
      INTEGER             LSYM,        MX4INT,      NI,          NIJ
      INTEGER             NJ,          NK,          NL
      INTEGER             SMABCD,      SYM12(8),    SYM21(8),    SYMCHK
C

C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,   NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C

C
c
c     # ioffs = number of words in each block to reserve for block
c     #         parameters.
c     # mx4int = maximum number of w-type or x-type integral
c     #          combinations that can be placed in wxints(*).
c
      ioffs=2
      mx4int=maxbl4/2-ioffs
      num4x(1)=0
      num4x(2)=0
c
      do 1 i=1,8
         sym12(i)=i
         sym21(i)=i
1     continue
      finbl=0
      icontw=0
      icontx=0
      iblock=0
      ictodw=0
      ictodx=0
      cntx12=0
      cntw3=0
      cntw12=0
      cntx3=0
      cntwx=0
      cw3od=0
      cw12od=0
      cx12od=0
      cx3od=0
      cwxod=0
c
      do 10 lsym=1,nmsym
c
         nl=nepsy(lsym)
         if(nl.eq.0)go to 10
c
         do 20 l1=1,nl
c
            smabcd=0
c
            do 30 ksym=1,(lsym-1)
c
               nk=nepsy(ksym)
               if(nk.eq.0)go to 30
c
c
c              # isym < jsym < ksym < lsym
c
               if(ksym.eq.1)go to 30
               ks=sym12(ksym)
               ls=sym12(lsym)
               kls=mult(ks,ls)
c
               do 40 k1=1,nk
c
                  symchk=0
c
                  do 50 jsym=1,(ksym-1)
c
                     nj=nepsy(jsym)
                     if(nj.eq.0)go to 50
                     js=sym12(jsym)
                     is=mult(js,kls)
                     isym=sym21(is)
                     if(isym.ge.jsym)go to 50
                     ni=nepsy(isym)
                     if(ni.eq.0)go to 50
c
                     symchk=1
                     smabcd=1
                     nij=ni*nj
                     cntwx=cntwx+nij
                     icontw=icontw+3*nij
                     icontx=icontx+3*nij
50                continue
c
                  if(symchk.eq.0)go to 30
                  if(finbl.ne.0)then
                     finbl=0
                  endif
                  if(icontw-mx4int)70,80,90
70                continue
                  if(icontx-mx4int)75,80,90
75                continue
c
c                 # both w-block and x-block fit into the internal
c                 # buffer. save the valid block parameters.
c
                  ictodw=icontw
                  ictodx=icontx
                  cwxod=cntwx
                  go to 40
c
c                 # both w-block and x-block fit.  one block (w-blocks
c                 # are larger than x-blocks) fits exactly so go ahead
c                 # and form combinations and move to da buffers.
c
80                iblock=iblock+1
c
c
                  cntwx=0
                  ictodw=0
                  ictodx=0
c                 numout=numout+icontw+icontx
                  num4x(1)=num4x(1)+icontw
                  num4x(2)=num4x(2)+icontx
                  icontw=0
                  icontx=0
                  finbl=1
                  cwxod=-999999
                  go to 40
c
c                 # at least one block is too large to fit into the
c                 # remaining internal buffer space.  form combinations
c                 # for the last valid block parameters and move to da
c                 # buffers.
c
90                continue
c
                  if(cwxod.eq.0)then
                     call bummer_interact('prep4e: cwxod=',cwxod,faterr)
                  endif
c
                  iblock=iblock+1
c
c
c                 numout=numout+ictodw+ictodx
                  num4x(1)=num4x(1)+ictodw
                  num4x(2)=num4x(2)+ictodx
                  cntwx=cntwx-cwxod
                  cwxod=cntwx
                  ictodw=icontw-ictodw
                  ictodx=icontx-ictodx
                  icontw=ictodw
                  icontx=ictodx
c
c                 # .end of k1 loop.
40             continue
c
               if(ictodw.eq.0.and.ictodx.eq.0)go to 45
c
c              # process the last block for this ksym.
c
               iblock=iblock+1
c
c
c              numout=numout+ictodw+ictodx
                  num4x(1)=num4x(1)+ictodw
                  num4x(2)=num4x(2)+ictodx
45             continue
               cntwx=0
               icontw=0
               icontx=0
               ictodw=0
               ictodx=0
               cwxod=-999999
               finbl=1
c
c              # .end of ksym loop.
30          continue
c
c           # ksym =lsym case (isym = jsym).
c
            ksym=lsym
c
            do 150 k1=1,l1
c
               symchk=0
c
c              # isym = jsym < ksym = lsym
c
               do 160 jsym=1,(ksym-1)
c
                  nj=nepsy(jsym)
                  if(nj.eq.0)go to 160
                  symchk=1
                  cntw3=cntw3+nj*(nj+1)/2
                  if(k1.ne.l1)then
                     cntw12=cntw12+nj*nj
                     cntx12=cntx12+nj*nj
                     cntx3=cntx3+nj*(nj-1)/2
                  else
                     cntw12=cntw12+nj*(nj-1)/2
                     cntx12=cntx12+nj*(nj-1)/2
                  endif
160            continue
c
c              # isym = jsym = ksym = lsym
c
               if(k1.gt.1)then
                  symchk=1
                  if(k1.ne.l1)then
c                    # (k1-1)**2+k1-1
                     cntw12=cntw12+k1*(k1-1)
c                    # (k1-1)**2
                     cntx12=cntx12+(k1-1)*(k1-1)
c                    # k1*(k1-1)/2+k1-1
                     cntw3=cntw3+k1*(k1+1)/2-1
c                    # (k1-1)*(k1-2)/2+k1-1
                     cntx3=cntx3+(k1-1)*k1/2
                  else
c                    # k1*(k1-1)/2+k1-1
                     cntw3=cntw3+k1*(k1+1)/2-1
                     cntw12=cntw12+k1*(k1-1)/2
                     if(k1.gt.2)cntx12=cntx12+(k1-1)*(k1-2)/2
                  endif
c
               endif
c
               icontw=cntw3+cntw12
               icontx=cntx3+cntx12
               if(symchk.eq.0)go to 150
               if(finbl.ne.0)then
                  finbl=0
               endif
               if(icontw-mx4int)190,200,210
190            continue
               if(icontx-mx4int)195,200,210
195            continue
c
c              # both w-block and x-block fit into the internal buffer.
c              # save the valid block parameters.
c
               ictodw=icontw
               ictodx=icontx
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               go to 150
c
c              # both w-block and x-block fit.  one block (w-blocks are
c              # larger than x-blocks) fits exactly so go ahead and
c              # form combinations and move to da buffers.
c
200            iblock=iblock+1
c
c
c              numout=numout+icontw+icontx
                  num4x(1)=num4x(1)+icontw
                  num4x(2)=num4x(2)+icontx
               finbl=1
               icontw=0
               icontx=0
               ictodw=0
               ictodx=0
               cntw12=0
               cntw3=0
               cntx12=0
               cntx3=0
               cw12od=-999999
               cw3od=-999999
               cx12od=-999999
               cx3od=-999999
               go to 150
c
c              # at least one block is too large to fit into the
c              # remaining internal buffer space.  form combinations
c              # for the last valid block parameters and move to
c              # output buffers.
c
210            iblock=iblock+1
c
               if(cw3od.eq.0)then
                  call bummer_interact('prep4e: cw3od=',cw3od,faterr)
               endif
c
c
c
c              numout=numout+ictodw+ictodx
                  num4x(1)=num4x(1)+ictodw
                  num4x(2)=num4x(2)+ictodx
               ictodw=icontw-ictodw
               ictodx=icontx-ictodx
               cntw12=cntw12-cw12od
               cntw3=cntw3-cw3od
               cntx12=cntx12-cx12od
               cntx3=cntx3-cx3od
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               icontw=0
               icontx=0
c
c              # .end of k1 loop.
150         continue
c
            if(smabcd.eq.0)go to 20
            if(ictodw.eq.0.and.ictodx.eq.0)go to 20
            iblock=iblock+1
c
c
c           numout=numout+ictodw+ictodx
                  num4x(1)=num4x(1)+ictodw
                  num4x(2)=num4x(2)+ictodx
            finbl=1
            icontw=0
            icontx=0
            ictodw=0
            ictodx=0
            cntw12=0
            cntw3=0
            cntx12=0
            cntx3=0
            cw12od=-999999
            cw3od=-999999
            cx12od=-999999
            cx3od=-999999
            cwxod=-999999
c
c           # end of l1 loop.
20       continue
c
         if(ictodw.eq.0.and.ictodx.eq.0)go to 130
c
c        # process the last block for this lsym.
c
         iblock=iblock+1
c
c
c        numout=numout+ictodw+ictodx
                  num4x(1)=num4x(1)+ictodw
                  num4x(2)=num4x(2)+ictodx
130      continue
         finbl=1
         icontw=0
         icontx=0
         ictodw=0
         ictodx=0
         cntw12=0
         cntw3=0
         cntx12=0
         cntx3=0
         cw12od=-999999
         cw3od=-999999
         cx12od=-999999
         cx3od=-999999
         cwxod=-999999
c
c        # end of lsym loop.
10    continue
c
c     num4x=numout
c
      return
      end
      subroutine prep3e( nmsym, niot, num3x   )
c
c  determine the block structure of the 3-external integrals and write
c  the blocks to the appropriate file.  the integrals are first read
c  into a segment buffer in triple-sort order into the array pints(*).
c  linear combinations of these integrals are formed and placed into
c  the array wxints(*).  the combinations are then moved into the
c  buffers bufw(*) and bufx(*) and written out as they are filled.
c  each combination block consists of all possible i and j indices
c  consistent with the parameters ksym, kstart, and kfinal.  these
c  parameters are written at the beginning of the block into two
c  working precision words.  the array wxints(*) must hold at least one
c  set of ij indices and should hold several so as to minimize the two-
c  word overhead and the subsequent processing overhead in the
c  diagonalization program.
c  blocks are not split between buffers.  the expected wasted space in
c  each buffer is therefore half of the average block size.  see the
c  put*34() routines for more details of the da buffer processing.
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
       integer maxsym,nmomx
       parameter (maxsym=8,nmomx=1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             NIOT,    NMSYM
      INTEGER             NUM3X(2)
C
C     Local variables
C
      INTEGER             CNTW12,      CNTW3,       CNTWX,       CNTX12
      INTEGER             CNTX3,       CW12OD,      CW3OD,       CWXOD
      INTEGER             CX12OD,      CX3OD,       FINBL,       I
      INTEGER             IBLOCK,      ICONTW,      ICONTX,      ICTODW
      INTEGER             ICTODX,      IOFFS,       IS,          ISYM
      INTEGER             JS,          JSYM,        K1,          KLS
      INTEGER             KS,          KSYM,        L1,          LS
      INTEGER             LSYM,        MX3INT,      NI
      INTEGER             NIJ,         NJ,          NK,          NL
      INTEGER             NUMOUT,      SYM12(8),    SYM21(8),    SYMCHK
	      INTEGER              k1strt,kfinod
C

C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,   NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C



C
c
c  ioffs = number of words in each block to reserve for block
c          parameters.
c  mx3int = maximum number of w-type or x-type integral combinations
c           that can be placed in wxints(*).
c
c     open(unit=22,file='prep3e.dat',form='formatted')
      write(22,*) 
     .'iblock,l1,ksym,k1s,k1f                                 '
      ioffs  = 2
      mx3int = maxbl3/2-ioffs
      num3x(1)=0
      num3x(2)=0
c
      iblock = 0
c
      do 1 i = 1, 8
         sym12(i) = i
         sym21(i) = i
1     continue
      k1strt=1
      finbl=0
      numout=0
      icontw=0
      icontx=0
      iblock=0
      ictodw=0
      ictodx=0
      cntx12=0
      cntw3=0
      cntw12=0
      cntx3=0
      cntwx=0
      cw3od=0
      cw12od=0
      cx12od=0
      cx3od=0
      cwxod=0
c
      do 10 l1=1,niot
c
c        ls=levsym(l1)
         ls=levsym(l1+lowinl-1)
         lsym=sym21(ls)
c
         do 20 ksym=1,nmsym
c
            nk=nepsy(ksym)
            if(nk.eq.0)go to 20
            ks=sym12(ksym)
            if(ksym.eq.lsym)go to 110
            if(ksym.eq.1)go to 20
            kls=mult(ks,ls)
c
            do 30 k1=1,nk
c
               symchk=0
c
               do 40 jsym=1,(ksym-1)
c
                  nj=nepsy(jsym)
                  if(nj.eq.0)go to 40
                  js=sym12(jsym)
                  is=mult(js,kls)
                  isym=sym21(is)
                  if(isym.ge.jsym)go to 40
c
c                 # isym < jsym < ksym
c
                  ni=nepsy(isym)
                  if(ni.eq.0)go to 40
                  nij=ni*nj
                  cntwx=cntwx+nij
                  icontw=icontw+3*nij
                  icontx=icontx+3*nij
                  symchk=1
c
c                 # end of jsym loop.
40             continue
c
               if(ksym.le.lsym)go to 50
c
c              # isym = lsym , jsym = ksym
c
               nl=nepsy(lsym)
               if(nl.eq.0)go to 50
               symchk=1
c
c              # 2*k1*nl + (k1-1)*nl
c
               icontw=icontw+nl*(3*k1-1)
c
c              # k1*nl + 2*(k1-1)*nl
c
               icontx=icontx+nl*(3*k1-2)
c
50             continue
               if(symchk.eq.0)go to 20
               if(finbl.ne.0)then
                  finbl=0
                  k1strt=k1
               endif
               if(icontw-mx3int) 60,70,80
60             continue
               if(icontx-mx3int) 65,70,80
65             continue
c
c              # both w-block and x-block fit into the internal buffer.
c              # save the valid block parameters.
c
               kfinod=k1
               ictodw=icontw
               ictodx=icontx
               cwxod=cntwx
               go to 30
c
c              # both w-block and x-block fit.  one block (w-blocks
c              # are larger than x-blocks) fits exactly so go ahead
c              # and form combinations and move to the output buffers.
c
70             continue
               iblock=iblock+1
               write(22,9010) iblock,l1,ksym,k1strt,k1
 9010          format(5i6)
c
c
c
               cntwx=0
               ictodw=0
               ictodx=0
               numout=numout+icontw+icontx
               num3x(1)=num3x(1)+icontw
               num3x(2)=num3x(2)+icontx
               icontw=0
               icontx=0
               cntwx=0
               finbl=1
               kfinod=-999999
               go to 30
c
c              # at least one block is too large to fit into the
c              # remaining internal buffer space.  form combinations
c              # for the last valid block parameters and move to the
c              # output buffers.
c
80             iblock=iblock+1
               write(22,9010) iblock,l1,ksym,k1strt,kfinod
               kfinod=k1
c
c
c
               numout=numout+ictodw+ictodx
               num3x(1)=num3x(1)+ictodw
               num3x(2)=num3x(2)+ictodx
               ictodw=icontw-ictodw
               ictodx=icontx-ictodx
               icontw=ictodw
               icontx=ictodx
               cntwx=cntwx-cwxod
               cwxod=cntwx
               k1strt=k1
c
c              # end of k1 loop.
30          continue
c
            if(ictodw.eq.0.and.ictodx.eq.0)go to 36
c
c           # process the last block for this ksym.
c
            iblock=iblock+1
             write(22,9010) iblock,l1,ksym,k1strt,nk

c
c
c
            numout=numout+ictodw+ictodx
               num3x(1)=num3x(1)+ictodw
               num3x(2)=num3x(2)+ictodx
c
36          continue
            icontw=0
            icontx=0
            ictodw=0
            ictodx=0
            cntwx=0
            cwxod=0
            finbl=1
            go to 20
c
110         continue
c
c           # ksym = lsym cases:
c
            do 120 k1=1,nk
c
               do 140 jsym=1,(ksym-1)
c
c                 # isym = jsym < ksym = lsym
c
                  nj=nepsy(jsym)
                  if(nj.eq.0)go to 140
                  cntw12=cntw12+nj*nj
                  cntx12=cntx12+nj*nj
                  cntw3=cntw3+nj*(nj+1)/2
                  cntx3=cntx3+nj*(nj-1)/2
140            continue
c
c              # isym = jsym = ksym = lsym
c
               cntw3=cntw3+k1*(k1+1)/2-1
               cntx3=cntx3+k1*(k1-1)/2
               cntw12=cntw12+(k1-1)*k1
               cntx12=cntx12+(k1-1)*(k1-1)
               icontw=cntw12+cntw3
               icontx=cntx12+cntx3
               if(finbl.ne.0)then
                  finbl=0
                  k1strt=k1
               endif
               if(icontw-mx3int) 160,170,180
160            continue
               if(icontx-mx3int) 161,170,180
161            continue
c
c              # both w-block and x-block fit into the internal buffer.
c              # save the valid block parameters.
c
               kfinod=k1
               ictodw=icontw
               ictodx=icontx
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               go to 120
c
c              # both w-block and x-block fit.  one block (w-blocks are
c              # larger than x-blocks) bits exactly so go ahead and
c              # form combinations and move to output buffers.
c
170            iblock=iblock+1
               write(22,9010) iblock,l1,ksym,k1strt,k1

c
c
c
               numout=numout+icontw+icontx
               num3x(1)=num3x(1)+ictodw
               num3x(2)=num3x(2)+ictodx
               finbl=1
               icontw=0
               icontx=0
               ictodw=0
               ictodx=0
               cntw12=0
               cntw3=0
               cntx12=0
               cntx3=0
               cw12od=-999999
               cw3od=-999999
               cx12od=-999999
               cx3od=-999999
               kfinod=-99999
               go to 120
c
c              # at least one block is too large to fit into the
c              # remaining internal buffer space.  form combinations
c              # for the last valid block parameters and move to the
c              # output buffers.
c
180            continue
               if(cw3od.le.0)then
                  call bummer_interact('prep3e: cw3od=',cw3od,faterr)
               endif
c
               iblock=iblock+1
               write(22,9010) iblock,l1,ksym,k1strt,kfinod

c
c
c
               numout=numout+ictodw+ictodx
               num3x(1)=num3x(1)+ictodw
               num3x(2)=num3x(2)+ictodx
               ictodw=icontw-ictodw
               ictodx=icontx-ictodx
               cntw12=cntw12-cw12od
               cntw3=cntw3-cw3od
               cntx12=cntx12-cx12od
               cntx3=cntx3-cx3od
               cw12od=cntw12
               cw3od=cntw3
               cx12od=cntx12
               cx3od=cntx3
               kfinod=k1
               k1strt=k1
               icontw=0
               icontx=0
c
c              # end of k1 loop.
120         continue
c
            if(ictodw.eq.0 .and. ictodx.eq.0) go to 20
c
c           # process the last block for this ksym.
c
            iblock=iblock+1
            write(22,9010) iblock,l1,ksym,k1strt,nk

c
c
c
            numout=numout+ictodw+ictodx
               num3x(1)=num3x(1)+ictodw
               num3x(2)=num3x(2)+ictodx
            finbl=1
            icontw=0
            icontx=0
            ictodw=0
            ictodx=0
            cntw12=0
            cntw3=0
            cntx12=0
            cntx3=0
            cw12od=-999999
            cw3od=-999999
            cx12od=-999999
            cx3od=-999999
            kfinod=-999999
c
c           # end of ksym loop.
20       continue
c
c        # end of l1 loop.
10    continue

c      close(22)
c
      if(ictodw.ne.0 .or. ictodx.ne.0)then
         call bummer_interact('prep3e: ictodw, ictodx error',0,faterr)
      endif
c     write(20,*) 'numout=',numout
c
c     num3x=numout
c
      return
      end
      subroutine indxdg(
     & nsym,   levsym, num0x,num2x,num4x,iim1)
c
c  this subroutine sets up the addressing arrays for the
c  integrals that contribute to the diagonal elements
c  of the hamiltonian.
c
         IMPLICIT NONE
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,   NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
C
C     Argument variables
C
      INTEGER             IIM1(NMOMX), LEVSYM(NLEVEL),           NSYM
      INTEGER             NUM0X,       NUM2X,       NUM4X
C
C     Local variables
C
      INTEGER             HSTUOC,      I,           IMAX,        IMIN
      INTEGER             IORB1,       IPOS,        ISTSYM(8),   ISYM
      INTEGER             ITP,         ITSYM,       J,           JMAX
      INTEGER             JMIN,        JSTRT,       JSYM,        NUM420X
      INTEGER             NUM42X
C
c
      do 10 i = 1, nmomx
         iim1(i) = (i * (i - 1)) / 2
10    continue

      ipos=1
      hstuoc=lowinl-1
c
c     # case 1 : j and i are both external orbitals
c
      do 50 itp=1,nsym
         istsym(itp)=1
         nepsy(itp)=0
50    continue
      itsym=1
      do 150 iorb1=1,hstuoc
         if(levsym(iorb1).eq.itsym) go to 100
         itsym=levsym(iorb1)
         istsym(itsym)=iorb1
100      nepsy(itsym)=nepsy(itsym)+1
150   continue
c
c
      do 400 jsym=1,nsym
         if(nepsy(jsym).eq.0) go to 400
         jmin=istsym(jsym)
         jmax=istsym(jsym)+nepsy(jsym)-1
         do 350 isym=1,jsym
            if(nepsy(isym).eq.0) go to 350
            imin=istsym(isym)
            imax=istsym(isym)+nepsy(isym)-1
            do 300 j=jmin,jmax
               jstrt=iim1(j)
               if(isym.eq.jsym) imax=j
               do 250 i=imin,imax
            if (jstrt+i.eq.iim1(hstuoc)+hstuoc) num4x=ipos+1
                  ipos=ipos+2
250            continue
300         continue
350      continue
400   continue
c
c     # case 2 : j is internal; i is external
c
      do 500 j = lowinl, nlevel
         jstrt = iim1(j)
          do 450 i = 1, hstuoc
            if (jstrt+i.eq.iim1(nlevel)+hstuoc) num42x=ipos+1
            ipos=ipos+2
450      continue
500   continue
c
c     # case 3 : j and i are both internal
c
      do 600 j = lowinl, nlevel
         jstrt = iim1(j)
         do 550 i = lowinl, j
            if (jstrt+i .eq. iim1(nlevel)+nlevel) num420x=ipos+1
            ipos = ipos + 2
550      continue
600   continue
c
c     # calculate number of each type of integrals
c
      num2x=num42x-num4x
      num0x=num420x-num42x
c
      return
      end
      subroutine indxof(
     & ijkl,   ijsym,  numx,iim1  )
c
c  this subroutine sets up the addressing arrays for the
c  integrals that contribute to the off-diagonal elements
c  of the hamiltonian.
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      integer  maxsym
      parameter (maxsym=8)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)

C
C     Argument variables
C
      INTEGER             IIM1(NMOMX), IJKL(*),     IJSYM(*)
      INTEGER             NUMX(8)
C
C     Local variables
C
      INTEGER             HSTUOC,      I,           ICNT,        IDIF
      INTEGER             IMAX,        IPOS,        ISYM,        J
      INTEGER             JSTRT,       JSYM,        K,           KKM
      INTEGER             L,           LKSYM
      INTEGER             LMDA(8,8),   LMDB(8,8),   LSTRT,       LXYZ
      INTEGER             NMBJ,        NMBPR(8),    NNDX,        NNDXA
      INTEGER             OFFSKL,      STRT
      INTEGER             STRTIJ(8),   SYM, savelk
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,  NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW

      INTEGER        MX4X,MX3X,MX2X
      COMMON /C9 / MX4X, MX3X, MX2X
C
      integer offsklsave
C
c
      hstuoc = lowinl - 1
c
c     # calculate divisions of ijkl
c     # *** this should be changed to a 2-d array. ***
c
      icnt=0
      do 100 i=1,nsym
         strtij(i) = icnt
         icnt=icnt+nnlev
100   continue
      call izero_wr( 8, numx, 1)

c
c   strtij (i) enthaelt die offsets fuer die
c   Blockung   nlevel*(nlevel+1)/2   x irreps
c
c     # setup ijsym
c
      nndx = 1
      do 250 j = 1, nlevel
         jsym = levsym(j)
         do 200 i=1,j
            isym = levsym(i)
            ijsym(nndx) = mult(jsym,isym)
            nndx = nndx + 1
200      continue
250   continue
c     write(*,201)(levsym(j),j=1,nlevel)
c     write(*,202)(ijsym(j),j=1,nndx-1)
c201   format('levsym:',20i3)
c202   format('ijsym:',20i3)
c
c    ijsym (i) enthaelt symmetrie des Produkts
c    von orbital level i * orbital level j,
c    lower triang.

c
c     # calculate ijkl for all irreps
c
      do 8100 sym = 1, nsym
         ipos = 1
         strt = strtij(sym)
c
c        # case 1: i is external; j is external or internal
c
         do 840 j = 1, nlevel
            imax = min(j,hstuoc)
            if (j .eq. lowinl) ipos = 1
c
c   iim1(j) = j*(j-1)/2
c
            jstrt=iim1(j)
            do 830 i=1,imax
               nndx=jstrt+i
c
c    ijkl(offset(sym) + offset(j) + i) und enthaelt die groesse
c                                      der Integralbloecke
c        (2 Indices fest, 2 frei)
c
c    fuer jeden symmetrieblock wird von 1 gezaehlt
c    innerhalb des symblocks fuer externes von 1 , internes j von 1
c
               ijkl(strt+nndx)=ipos
               if(ijsym(nndx).eq.sym) ipos=ipos+3
830         continue
840      continue
c
c        # case 2: i and j are internal
c
         ipos=1
         do 860 j=lowinl,nlevel
            jstrt=iim1(j)
            do 850 i=lowinl,j
               nndx=jstrt+i
               ijkl(strt+nndx)=ipos
               if(ijsym(nndx).eq.sym) ipos=ipos+3
850         continue
860      continue
8100  continue
c
c         ijkl[1:nsym,1::nnlev]
c
c
      offskl=0
      offsklsave=0
c
c     # calculate lk432x for 4 external
c
      mx4x=0
      nndx=0
      do 8130 l=1,hstuoc
         do 8120 k=1,l
            nndx=nndx+1
            if(k.eq.1) go to 8120
c
c     idif= ijkl[lsym*nsym,k]
c
            idif=ijkl(strtij(ijsym(nndx))+iim1(k+1))-1
            mx4x=max(mx4x,idif)
            offskl=offskl+idif
8120     continue
8130  continue

        numx(1)=offskl
        offsklsave=offskl
c
c     # calculate lk432x for 3 external
c
       mx3x=0
      do 8150 l = lowinl, nlevel
         lstrt=iim1(l)
         do 8140 k=1,hstuoc
            nndx=lstrt+k
c           if (nndx.eq.iim1(lowinl)+1) numx(1)=offskl
            if(k.eq.1) go to 8140
            idif=ijkl(strtij(ijsym(nndx))+iim1(k+1))-1
            mx3x=max(mx3x,idif)
            offskl=offskl+idif
8140     continue
8150  continue

        numx(2)=offskl-offsklsave
        offsklsave=offskl
c
c     # calculate lk432x for 2 external
c
      mx2x=0
      do 8192 l = lowinl, nlevel
         lstrt=iim1(l)
         do 8190 k=lowinl,l
            nndx=lstrt+k
c           if (nndx.eq.iim1(lowinl)+lowinl) numx(2)=offskl
            savelk=offskl
            lksym=ijsym(nndx)
            do 8172 j=1,hstuoc
               jstrt=iim1(j)
               do 8170 i=1,j
                  nndxa=jstrt+i
                  if(ijsym(nndxa).eq.lksym) offskl=offskl+3
8170           continue
8172        continue
          mx2x=max(mx2x,offskl-savelk)
8190     continue
8192  continue
        numx(3)=offskl-offsklsave
        offsklsave=offskl
c
c     # calculate lk1ex for 1 external
c
      do 8250 l = lowinl, nlevel
         lstrt=iim1(l)
         do 8240 k=lowinl,l
c           if (l1strt+k-hstuoc.eq.1) numx(3)=offskl+1
            lksym=ijsym(lstrt+k)
            do 8230 j=lowinl,k
               jstrt=iim1(j)
               do 8220 i=1,hstuoc
                  nndxa=jstrt+i
                  if(ijsym(nndxa).eq.lksym) offskl=offskl+3
8220           continue
8230        continue
8240     continue
8250  continue
        numx(4)=offskl-offsklsave
        offsklsave=offskl
c
c     # calculate lk0ex for all internal indices
c
      do 8300 l = lowinl, nlevel
         lstrt=iim1(l)
         do 8290 k=lowinl,l
c           if (l1strt+k-hstuoc.eq.1) numx(4)=offskl+1
            if(k.eq.lowinl) go to 8290
            nndx=lstrt+k
            kkm=strtij(ijsym(nndx))+iim1(k)+k
            offskl=offskl+ijkl(kkm)-1
8290     continue
8300  continue
        numx(5)=offskl-offsklsave
        offsklsave=offskl
c
*@ifdef spinorbit
c
      if(.not.spnorb)goto 8301
c
c     # calculate the number of 0 external spin orbit integrals
c
c
      do 6000 j = lowinl, nlevel
         jsym = levsym(j)
         do 6100 i = lowinl, j-1
            isym = levsym(i)
            lksym = mult(isym, jsym)
            do 6200 lxyz = 1, 3
               if( lxyzir(lxyz) .eq. lksym )then
                   numx(6) = numx(6) + 1
               endif
6200        continue
6100     continue
6000  continue
c
c     # calculate the number of 1 external spin orbit integrals
c
      do 5000 j = lowinl, nlevel
         jsym = levsym(j)
         do 5100 lxyz = 1, 3
            isym = mult(jsym, lxyzir(lxyz))
            if( nepsy(isym) .eq.0 )goto 5100
            numx(7) = numx(7) + nepsy(isym)
5100     continue
5000  continue
c
c     # lmdb(i,j) lmda(i,j) symmetry pairs defined as
c     # lmd(i)=lmdb(i,j)*lmda(i,j), j=nmdpr(i)
c     # lmdb(i,j).ge.lmda(i,j), lmdb(i,j).lt.lmdb(i,j+1)
c     # nmbpr(i) number of pairs
c
      do 9300 i = 1, nsym
         nmbj=0
         do 9301 j = 1, nsym
            if(nepsy(j) .eq. 0)goto 9301
            lksym = mult(i, j)
            if(nepsy(lksym) .eq. 0) goto 9301
            if(lksym .gt. j)goto 9301
            nmbj = nmbj + 1
            lmdb(i,nmbj) = j
            lmda(i,nmbj) = lksym
9301     continue
         nmbpr(i) = nmbj
9300  continue
c
c     # calculate the number of 2 external spin orbit integrals
c
      do 7000 lxyz = 1, 3
         lksym = lxyzir(lxyz)
         if(nmbpr(lksym) .eq. 0)goto 7000
         do 7001 k = 1, nmbpr(lksym)
            isym = lmda(lksym, k)
            jsym = lmdb(lksym, k)
            if(isym .eq. jsym)then
               numx(8) = numx(8) + nepsy(isym) * (nepsy(isym) + 1) / 2
            else
               numx(8) = numx(8) + nepsy(isym) * nepsy(jsym)
            endif
7001     continue
7000  continue
8301  continue
*@endif
c
      return
      end
