!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      PROGRAM MAKPCIUDG


         IMPLICIT NONE
      include "../colib/getlcl.h" 

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            FSTRLEN
C
      INTEGER             FSTRLEN
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG =200)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
        integer lcored
C
C     Local variables
C
      CHARACTER*200       COLUMBUS
      CHARACTER*70        FILESTR(4)
      CHARACTER*20        HOSTNAME
      CHARACTER*70        MACH,        MACHINES(10)
      CHARACTER*200       USERID,      WORKDIR
C
      INTEGER             FIX,         GROUPSGA(6), GROUPSLD(6)
      INTEGER             GROUPSVD(6), I,           ICHOICE
      INTEGER             ICMPR(4),    IERR,        IFIRST
      INTEGER             IGROUPSGA,   IGROUPSLD,   IGROUPSVD,   LCORE
      INTEGER             MACHNO,      MAXMEM
      INTEGER             NWST,        TOTNC0
      INTEGER             TOTNC1,      TOTNC2,      TOTNC3,      TOTNC4
      INTEGER             VLEN, indsym,limvec,top,conft,forbyt
      integer   ineed,memneed,indxcpy,bl,strtdrt,isum
      integer   procfunc,minsub,memsub
      integer   maxlen,rl,fmode,icase,atebyt,maxsub
      integer   mem, smptype,ldisk

c     real*8 a(2)
c
C
C     Common variables
C
      INTEGER             MCORE(5),    MGA(5),      MVD(5)
      INTEGER             NVCIMX(5),   VALID(5)
C
      COMMON / PCHOICE/   MCORE,       MGA,         MVD,         NVCIMX
      COMMON / PCHOICE/   VALID
C
C     Common variables
C
      INTEGER             DESMINSUB
C
      COMMON / SUBSPACE/  DESMINSUB
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,       TOTSPC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c     KBL3,       unused ????
c     KBL4,       unused ????
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c      MAXBL3,    unused ????
c   MAXBL4,       unused ????
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c      MXKBL3,    unused ????
c     MXKBL4,     unused ????
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c       N0XTLP    unused ????
c      N1EXT,     number of one-external off-diagonal integrals
c      N1XTLP,    unused ????
c     N2EXT       number of two-external off-diagonal integrals
c       N2XTLP,   unused ????
c     N3EXT,      number of three-external off-diagonal integrals
c       N3XTLP,i  unused ????
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMBLK3     unused ????
c      NMBLK4     unused ????
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)

C     Common variables
C
      INTEGER             C2EX0EX,     C3EX1EX,     CDG4EX
      INTEGER             MAXSEG
      INTEGER             NSEG1X(4) ,NSEGD(4), NTYPE
      INTEGER             NPROC

      COMMON / INDATA / C2EX0EX,     C3EX1EX,     CDG4EX, MAXSEG,
     &                   NSEG1X ,NSEGD, NTYPE, NPROC
C
C     Common variables
C
      INTEGER             IVWALK(4),   NREFS,       NROW
      INTEGER             WCSF,        XBAR(4),     XCSF,        YCSF
      INTEGER             ZCSF
C
      COMMON / CIDRTINFO/ ZCSF,        YCSF,        XCSF,        WCSF
      COMMON / CIDRTINFO/ IVWALK,      NROW,        XBAR
      COMMON / CIDRTINFO/ NREFS
C
C     Common variables
C
      INTEGER             CIST(MXNSEG,7),           DRTIDX(MXNSEG,7)
      INTEGER             DRTLEN(MXNSEG,7),         DRTST(MXNSEG,7)
      INTEGER             IPTHST(MXNSEG,7),         LENCI,       NSEG(7)
      INTEGER             SEGCI(MXNSEG,7),          SEGEL(MXNSEG,7)
      INTEGER             SEGSCR(MXNSEG,7),         SEGTYP(MXNSEG,7)
C
      COMMON / CIVCT  /   LENCI,       NSEG,        SEGEL,       CIST
      COMMON / CIVCT  /   SEGCI,       SEGSCR,      SEGTYP,      IPTHST
      COMMON / CIVCT  /   DRTST,       DRTLEN,      DRTIDX
C
      character*2 chtemp
c
C     Common variables
C
      INTEGER             CIDRTFL,     DIAGINT,     FIL3W,       FIL3X
      INTEGER             FIL4W,       FIL4X,       OFDG
C
      COMMON / FILESIZES/ DIAGINT,     OFDG,        FIL3W,       FIL3X
      COMMON / FILESIZES/ FIL4W,       FIL4X,       CIDRTFL
c
      integer nroot,froot
      common/ciroot/nroot,froot
c
      real*8 scale1,scale2
      common /scale/ scale1,scale2
C
      integer itemp
c
      integer retcode,maxbuf,intmxo
*@if (defined sun && defined osu) ||  (defined cray && defined osu)
*       data maxbuf/32767/, intmxo/765/
*@elif defined  ibmcms
*      data maxbuf/30000/, intmxo/4095/
*@elif defined  ibmmvs
*      data maxbuf/2383/,  intmxo/1070/
*@else
       data maxbuf/32767/,intmxo/4095/
*@endif

C
C    END OF VARIABLE DECLARATIONS FOR subroutine ciinput

c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
c
c     # bummer_interact error types.
c
c     # "call pbginf" should be the first executable statement.  this
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.
c
c
c
c     # use standard f90 memory allocation.
c
      integer mem1
      parameter ( lcored = 5 000 000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('argos: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore) )
      ifirst = 1
      mem1   = 0
c
c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
c
c
c open listing file

      OPEN (UNIT=20,FILE='makpciudgls',status='unknown')
c     WRITE (20,40)
 41   format ('All file sizes and core memory sizes are given',
     .        'in working precision units'/
     .        '(1 working precision unit = 8 bytes)')

c open configuration file
      open (unit=30,FILE='config.parallel',status='unknown')

c open keystroke file

      open (unit=31,FILE='makpciudgky',status='unknown')

c
c     call readinput
c#############################################################
c  read cidrtfl file and calculate integral file sizes

      WRITE (20,29)
      OPEN (UNIT=11,FILE='cidrtfl',status='unknown')
cc
c    # rddrtao returns
c    # core(1)  reference limvec
c    # core(indsym) internal walk symmetry
c    # core(limvec) index vector
c

      CALL RDDRTAO (20,11,a(ifirst),rl(lcore),indsym,limvec,top)
      CLOSE (11)
29    FORMAT (20('----')/'Reading cidrtfl ... ')
c###############################################################

c
c   von Interesse: dg, ofdg,3w,3x,4w,4x
c
      CALL GETFILESIZES (a(ifirst+top),lcore-top)

      call bummer('normal termination',0,3)
      stop 
      END
      SUBROUTINE GETFILESIZES (core,mxcore)
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
      integer maxsym,nmomx
      parameter (maxsym=8,nmomx=1023)
C
C
C     Argument variables
C
      INTEGER             MXCORE,      T0,          T1,          T2
      INTEGER             T3,          T4
C
      REAL*8              CORE(*)
C
C     Local variables
C
C
      INTEGER             NEXT
      INTEGER             NEXTERN(8),  NMOT
      INTEGER             NUM1E,limvec,indsym
      integer     isum
      external    isum
C
C     Common variables
C
      INTEGER             IVWALK(4),   NREFS,       NROW
      INTEGER             WCSF,        XBAR(4),     XCSF,        YCSF
      INTEGER             ZCSF
C
      COMMON / CIDRTINFO/ ZCSF,        YCSF,        XCSF,        WCSF
      COMMON / CIDRTINFO/ IVWALK,      NROW,        XBAR
      COMMON / CIDRTINFO/ NREFS
C
C     Common variables
C
      INTEGER             CIDRTFL,     DIAGINT,     FIL3W,       FIL3X
      INTEGER             FIL4W,       FIL4X,       OFDG
C
      COMMON / FILESIZES/ DIAGINT,     OFDG,        FIL3W,       FIL3X
      COMMON / FILESIZES/ FIL4W,       FIL4X,       CIDRTFL
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,   NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
      INTEGER        MX4X,MX3X,MX2X,I
      COMMON /C9 / MX4X, MX3X, MX2X

       character*12 mnemonic(24)
       integer ntot,iliste(24),tasks(24)
        real*8 loops(24),looptime(24),tmult(24),zero
       data iliste /1,2,3,4,8,9,11,13,14,21,22,23,24,25,26,27,28,
     .      29,31,32,45,46,47,75/
       data zero /0.0d0/
       data mnemonic / 'allint zz   ','allint yy   ','allint xx*  ',
     .                 'allint ww*  ','allint xx+  ','allint ww+  ',
     .                 'one-ext yz ','one-ext yx ','one-ext yw ',
     .                 'two-ext yy ','two-ext xx*','two-ext ww*',
     .                 'two-ext xz ','two-ext wz ','two-ext wx*',
     .                 'two-ext wx+','two-ext xx+','two-ext ww+',
     .                 'thr-ext yx ','thr-ext yw ','4exdg024 y ',
     .                 '4exdg024 x ','4exdg024 w ','dg-024ext z'/


C
C    END OF VARIABLE DECLARATIONS FOR subroutine getfilesizes

c mnbas = maximum number of external basis functions per irrep
      open(unit=22,file='prep3e.dat',form='formatted')
      call drvsort(core,mxcore,diagint,ofdg,fil3w,fil3x,fil4w,fil4x,
     .      num1e)
 65   format(6i14)
 66   format(8i8)
      write(22,*) 
     .'intmxo mx4x mx3x mx2x maxbuf                           '
      write(22,65) intmxo,mx4x,mx3x,mx2x, maxbuf
      write(22,*) 
     .'number of external orbitals                            '
      write(22,66) (nbas(i),i=1,8)     
      write(22,*) 
     .'diagint ofdg fil3x fil3w fil4w fil4x (DP)             '
      write(22,65) DIAGINT,OFDG,FIL3X,FIL3W,FIL4W,FIL4X
      close(22)
c
c    create fake tmodells file 
c
      ntot=0 
      next=0
      do i=1,nsym
       ntot=ntot+nmpsy(i)  
       next=next+nbas(i)  
      enddo

      open (unit=22,file='tmodells.fake',form='formatted')
      write(22,901) 
      write(22,905) ' total number of MOs           :',next 
      write(22,905) ' total number of internal MOs  :',ntot-next 
      write(22,905) ' frozen core orbitals          :', -1 
      write(22,905) ' frozen virtual orbitals       :', -1 
      write(22,905) ' number of rows in DRT         :', nrow 
      write(22,905) ' number of irreducible repr.   :',nsym
      write(22,905) ' number of external orbitals   :',
     .      (nbas(i),i=1,nsym) 
      write(22,905) ' symmetry of state             :', -1
      write(22,905) ' number of walks (zyxw)        :', 
     .      (xbar(i),i=1,4)
      write(22,905) ' number of valid walks (zyxw)  :', 
     .             (ivwalk(i),i=1,4)
      write(22,906) ' number of csfs   zyxw         :', 
     .      zcsf,ycsf,xcsf,wcsf,zcsf+ycsf+xcsf+wcsf 
      write(22,902) 
901   format(36('=')'CIDRT INFO ARRAY',47('='))
902   format(99('='))
      write(22,903)
903   format(36('=')'FILE SIZES ',52('='))
904   format(a,':',i11,' Bytes (',f10.2,' MB)')
905   format(a,8(i10,1x))
906   format(a,4(i10,1x),' total=',i10)
      write(22,*) 'Filesizes:'
      write(22,904) 'fil4x',fil4x*8,dble(fil4x/(128*1024))
      write(22,904) 'fil3x',fil3x*8,dble(fil3x/(128*1024))
      write(22,904) 'fil4w',fil4w*8,dble(fil4w/(128*1024))
      write(22,904) 'fil3w',fil3w*8,dble(fil3w/(128*1024))
      write(22,904) 'ofdgint',ofdg*8,dble(ofdg/(128*1024))
      write(22,904) 'diagint',diagint*8,dble(diagint/(128*1024))
      write(22,902) 
      write(22,910) 
910   format(36('='),'CIUDGLS INFO ',49('='))
911   format(a,4i6)
      write(22,911) ' maximum subspace dimension :'
      write(22,911) ' number of nodes            :'
      write(22,911) ' actually used segm (0ext)  :'
      write(22,911) ' actually used segm (1ext)  :'
      write(22,911) ' actually used segm (2ext)  :'
      write(22,911) ' actually used segm (2extwx):'
      write(22,911) ' actually used segm (3ext)  :'
      write(22,911) ' actually used segm (4ext)  :'
      write(22,902)
      write(22,912)
      write(22,913)
      write(22,914)
      write(22,*) '       '
912   format(' -----------  integral related GA communication ',
     . 'time & volume (summed over all nodes) ------------')
913   format('    iter       3-external read                ',
     .'  4-external read ')
914   format('            time         volume      pertask        time',
     . '        volume      pertask    (average transfer rate)')

      write(22,920)
      write(22,921)
      write(22,922)
      write(22,923)
920   format(35('-'),' total times per task type ',60('-'))
921   format('task type                        tmult    tcomm(vw)',
     . '   MFLOPS       GFLOP     loops           loop time',          
     . '        loop      ntasks ')
922   format('  code         mnemonic           [s]        [s]',
     . '      exloop       total    [million]            [s]',
     . '          GFLOP               ') 
923   format(30('----'))
924   format(i6,9x,a,2x,f8.2,2x,4(f8.2,4x),f8.2,2x,
     .      '( 00.0 %)      0.000   ',i4)

c     loop times are accessible from cimksegls 
c     #loops  3ext < 2ext ~ 0ext < 1-ext 
c     for boot strapping assume
c     
       do i=1,23 
         tmult(i)=100.0
         loops(i)=20
         looptime(i)=10
         tasks(i)=1
       enddo
         tmult(24)=5.0
         tasks(24)=1

       do i=1,20
        write(22,924) iliste(i),mnemonic(i), tmult(i),
     .            zero, zero,zero,loops(i),looptime(i),
     .            tasks(i)
       enddo
     
       do i=21,24
        write(22,924) iliste(i),mnemonic(i), tmult(i),
     .            zero,zero,zero,zero, zero, tasks(i)
       enddo
      write(22,923)
      close(22)

c     WRITE (*,*) 'Integral filesizes in working precision units'
c     WRITE (*,60) DIAGINT,OFDG,FIL3X,FIL3W,FIL4W,FIL4X
      write (20,61)
 61   format(20('----'))
      WRITE (20,*) 'Integral filesizes in working precision units'
      WRITE (20,60) DIAGINT,OFDG,FIL3X,FIL3W,FIL4W,FIL4X
      write (20,61)
      WRITE (20,*) 'Integral filesizes in bytes'    
      WRITE (20,60) DIAGINT*8,OFDG*8,FIL3X*8,FIL3W*8,FIL4W*8,FIL4X*8
      write (20,61)


c     T4=2*MAXBUF+MNBAS*MNBAS+MAX(NUM1E+INTMXO,MNBAS*MNBAS*2+MNBAS)
c     T3=2*MAXBUF+NEXT*NEXT
c     T2=MX2X+INTMXO+16/3*MX2X+3*MNBAS*MNBAS
c     T1=3*MNBAS+INTMXO+MNBAS+2*MNBAS*MNBAS
c     T0=INTMXO

      RETURN
C   
c30    FORMAT (20('----')/'Reading cidrtfl ... ')
60    FORMAT (2x,'diagint    ',I12,5X,'ofdg     ',I12,5X,
     +'fil3x   ',I12,5X,/ 
     + 2x,'fil3w      ',I12,5X,'fil4w    ',I12,5X,'fil4x   ',I12,5X,/)
      END 

      SUBROUTINE RDDRTAO (NLIST,NDRT,buf,mxbuf,indsym,limvec,top)
C
c  read some of the ci drt file info for the integral transformation..
C
c  input:
c  nlist = list file unit number.
c  ndrt = drt unit number.
c  buf(*) = scratch buffer space of at least size = max(nsym,lidrt).
C
c  output:
c  nsym = number of symmetry blocks
c  nbft = number of basis functions
c  nmpsy = number of mos per symmetry block
c  nextern   = number of external mos per symmetry block
C
C
C====>Begin Module RDDRT      File colop.f        All variables
C
C     Parameter variables
C
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      integer             maxsym
      parameter            (maxsym=8)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMx = 128)

C
C     Argument variables
C
      INTEGER             BUF(*),      MXBUF,       NDRT
      INTEGER             NEXTERN(8),  NLIST,       NMOT
      integer             limvec,indsym,ref,ibuf,top
C
C     Local variables
C
      CHARACTER*80        TITLE
C
      INTEGER             I,           IERR,        J,           LENBUF
      INTEGER             LOWINT
      INTEGER             NCSFSM(8*4), NCSFT, NX(0:1)
      INTEGER             NHDINT,      NIOT, NFVT
      INTEGER             NVALWT
      INTEGER             NVIWSM(8*4), NVWXY,       NWALK
      INTEGER             VRSION, K,nl, icsf,stwlk,ewlk
      integer              calcthrxt,forbyt
      external  calcthrxt ,forbyt
C
C
      REAL                FATERR
C
C     Common variables
C
      INTEGER             FREEZE(NMOMX),            MAPOUT(NMOMX)
      INTEGER             N2BSKP(8),   NBMSKP(8),   NBSKP(8)
      INTEGER             NFCPSY(8),   NFRZCT,      NFRZVT
      INTEGER             NMOPSY(8),   NMSKP(8),    NNBSKP(8)
      INTEGER             NNMSKP(8),   NORBT,       NSYMMO
C
      LOGICAL             INTERN(NMOMX)
C
      COMMON / CSYMB2 /   NNBSKP,      NBSKP,       NFCPSY,      FREEZE
      COMMON / CSYMB2 /   NSYMMO,      NMOPSY,      MAPOUT,      NORBT
      COMMON / CSYMB2 /   NFRZCT,      NFRZVT,      N2BSKP,      NMSKP
      COMMON / CSYMB2 /   NNMSKP,      NBMSKP,      INTERN
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      INTEGER             INTMXO,      LOWINL,      MAXBL3
      INTEGER             MAXBL4,      MAXBUF,      NEPSY(8),    NLEVEL
      INTEGER             NNLEV,   NMPSY(8) , NINTERN
C
      COMMON / CI     /   NLEVEL,      NNLEV,       LOWINL
      COMMON / CI     /   NEPSY,       MAXBL3
      COMMON / CI     /   MAXBL4,      MAXBUF,      INTMXO
      COMMON / CI     /   NMPSY,     NINTERN
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,       TOTSPC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c     KBL3,       unused ????
c     KBL4,       unused ????
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c      MAXBL3,    unused ????
c   MAXBL4,       unused ????
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c      MXKBL3,    unused ????
c     MXKBL4,     unused ????
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c       N0XTLP    unused ????
c      N1EXT,     number of one-external off-diagonal integrals
c      N1XTLP,    unused ????
c     N2EXT       number of two-external off-diagonal integrals
c       N2XTLP,   unused ????
c     N3EXT,      number of three-external off-diagonal integrals
c       N3XTLP,i  unused ????
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMBLK3     unused ????
c      NMBLK4     unused ????
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)

C     Common variables
C
      INTEGER             NIOTX,       NROWX
C
      COMMON / DRTINFO/   NROWX,       NIOTX
C
C     Common variables
C
      INTEGER             IVWALK(4),   NREFS,       NROW
      INTEGER             WCSF,        XBAR(4),     XCSF,        YCSF
      INTEGER             ZCSF
C
      COMMON / CIDRTINFO/ ZCSF,        YCSF,        XCSF,        WCSF
      COMMON / CIDRTINFO/ IVWALK,      NROW,        XBAR
      COMMON / CIDRTINFO/ NREFS
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             BORD(NIMOMX),             BSYM(NIMOMX)
      INTEGER             CASE(0:NROWMX),           L(0:3,NROWMX,3)
      INTEGER             MU(NIMOMX),  NJ(0:NIMOMX)
      INTEGER             NJSKP(0:NIMOMX),          ROW(0:NROWMX)
      INTEGER             XBARCDRT(NROWMX,3),       Y(0:3,NROWMX,3)
      INTEGER             YB(0:NROWMX),             YK(0:NROWMX)
C
      COMMON / DRT    /   B,           XBARCDRT,    L,           Y
      COMMON / DRT    /   YB,          YK,          CASE,        ROW
      COMMON / DRT    /   BORD,        BSYM,        ARCSYM,      NJ
      COMMON / DRT    /   NJSKP,       MU
C
      integer limvecinfo(3)
      integer icd(10),numv1


C
C    END OF VARIABLE DECLARATIONS FOR subroutine rddrtao


c    nnbskip(i)
c    nbskip(8)
c    nfcpsy(i) = number of frozen orbitals per sym block
c    freeze(i) = 0 for frozen core, -1 for frozen virt.
c                otherwise 1 .. norbt being the new index of each
c                of the remaining integrals
c    nbpsy(i)  = number of basis functions per sym block
c    nmpsy(i)  = number of output orbitals per sym block
c    nsymmo    = number of output symmetry blocks
c    nmopsy    = number of orbitals in each symmetry block of the
c                output file
c    mapout      mo-to-outputmo mapping vector
c    norbt     = number of orbitals excluding frozen virt and frozen cor
c    spezielle offset-arrays: gesetzt in readinfofl
c  intern(1 .. nbft) = true fuer intern  false fuer extern
C====>End Module   RDDRT      File colop.f
c     # title card...
      REWIND NDRT
      READ (NDRT,'(a)',IOSTAT=IERR) TITLE
      IF (IERR.NE.0) CALL BUMMER ('rdhead: title ierr=',IERR,FATERR)
*@ifdef spinorbit
      read(ndrt,'(2L4,4I4)',iostat=ierr)spnorb, spnodd, lxyzir,hmult
      if(ierr.ne.0)call bummer_interact('rddrt: ierr=',ierr,faterr)
*@endif



ct    WRITE (NLIST,120) TITLE
C
c     # number of integers...
C
      if (mxbuf.lt.16)
     .  call bummer_interact('rddrtao: not enough core memory',mxbuf,2)

      CALL RDDBL (NDRT,3,BUF,3)
C
      VRSION=BUF(1)
      LENBUF=BUF(2)
      NHDINT=BUF(3)
C
      IF (VRSION.NE.6) THEN
      call bummer('NEED DRT VERSION 6',vrsion,2)
      END IF
C
c     # header info...
C
      CALL RDDBL (NDRT,LENBUF,BUF,NHDINT)
C
      NMOT=BUF(1)
      NIOT=BUF(2)
      nintern = niot
      NFCT=BUF(3)
      NFVT=BUF(4)
      NROW=BUF(5)
      NSYM=BUF(6)
      SSYM=BUF(7)
      XBAR(1)=BUF(8)
      XBAR(2)=BUF(9)
      XBAR(3)=BUF(10)
      XBAR(4)=BUF(11)
      NVALZ=BUF(12)
      NVALY=BUF(13)
      NVALX=BUF(14)
      NVALW=BUF(15)
      NCSFT=BUF(16)
c     ZCSF=xbar(1)
c     YCSF=xbar(2)
c     XCSF=xbar(3)
c     WCSF=xbar(4)
      NVWXY=NVALW+NVALx+NVALy
c     NVALZ=XBAR(1)
      NLEVEL=NIOT+1
      niotx = niot
      nrowx = nrow
      pthz = xbar(1)
      pthy = xbar(2)
      pthx = xbar(3)
      pthw = xbar(4)
      nmsym = nsym
      intorb = niot
      if(spnodd)intorb=niot-1
C
      NWALK=XBAR(1)+XBAR(2)+XBAR(3)+XBAR(4)
      nintpt=nwalk
      NVALWT= nvalz+nvaly+nvalx+nvalw
      NVALWK= NVALWT
      IVWALK(1)=NVALZ
      IVWALK(2)=NVALY
      IVWALK(3)=NVALX
      IVWALK(4)=NVALW

c
c
      ref=1
*@ifdef int64
      limvec=ref+forbyt(pthz)
      indsym=limvec+forbyt(nwalk)
      ibuf=indsym+forbyt(nvalwt)
*@else
*      limvec=ref+forbyt(pthz)*2
*      indsym=limvec+forbyt(nwalk)*2
*      ibuf=indsym+forbyt(nvalwt)*2
*@endif
      if (mxbuf.lt.ibuf+nrowmx)
     .  call bummer_interact('rddrtao mxbuf.lt.ibuf+nrowmx',ibuf,2)
C
ct    WRITE (NLIST,130) NMOT,NIOT,NFCT,NFVT,NROW,NSYM,SSYM,LENBUF
ct    WRITE (NLIST,140) 'nwalk,xbar',NWALK,XBAR
ct    WRITE (NLIST,140) 'nvalwt,nvalw',NVALWT,NVALW
ct    WRITE (NLIST,140) 'ncsft',NCSFT

C
c     # slabel(*)...
      CALL RDDBL (NDRT,LENBUF,BUF(ibuf),-NSYM)
C
c     # map(*)...
      CALL RDDBL (NDRT,LENBUF,FREEZE,NMOT)
C
c     # mu(*)...
      CALL RDDBL (NDRT,LENBUF,MU,NIOT)
ct    WRITE (NLIST,150) 'mu(*)=',(MU(I),I=1,NIOT)

      DO 10 I=1,NIOT
           IF (MU(I).LT.0.OR.MU(I).GT.2) THEN
                WRITE (NLIST,*) 'error: mu(*)=0,1,2 only in this version
     +'
                CALL BUMMER ('rddrt:',0,FATERR)
           END IF
10    CONTINUE
C
c     # nmpsy(*)...
      CALL RDDBL (NDRT,LENBUF,NMPSY,NSYM)

      nl=0
      do i=1,nmot
        if (freeze(i).gt.0) then
          nl=nl+1
          buf(ibuf+nl)= freeze(i)
        endif
      enddo
c
c  in cidrt is nmot the total numberof orbitals
c  in cisrt the total number of non frozen orbs
c
c     lowinl = nmot-niot+1
      lowinl = nl-niot+1

        k=0
        nl=0
        do i=1,nsym
          do j = 1,nmpsy(i)
            k=k+1
            if (freeze(k).gt.0) then
             nl=nl+1
             levsym(buf(ibuf+nl))=i
            endif
          enddo
        enddo

c       write(0,1151) (levsym(i),i=1,k)
1151    format('def levsym:',20i3)
c
c       levsym enthaelt nur gueltige Eintraege!
c
c       do i=lowinl,k
c         levsym(i-lowinl+1) = levsym(i)
c       enddo

c       write(0,1152) (levsym(i),i=1,k)
1152    format('shifted levsym:',20i3)

C
c     # nexo(*)...
      CALL RDDBL (NDRT,LENBUF,NEXTERN,NSYM)

      call icopy_wr(nsym,nextern,1,nbas,1)

      MNBAS=0
      DO 20 I=1,NSYM
           MNBAS=MAX(MNBAS,NEXTERN(I))
20    CONTINUE
C     # nviwsm
      CALL RDDBL (NDRT,LENBUF,NVIWSM,4*NSYM)
C     #ncsfsm
      CALL RDDBL (NDRT,LENBUF,NCSFSM,4*NSYM)
           ICSF=0
      DO 60 I=1,4
           DO 50 J=1,NSYM
                ICSF=ICSF+NCSFSM(I+4*(J-1))
50         CONTINUE
           if (i.eq.1) zcsf=icsf
           if (i.eq.2) ycsf=icsf-zcsf
           if (i.eq.3) xcsf=icsf-zcsf-ycsf
           if (i.eq.4) wcsf=icsf-zcsf-ycsf-xcsf
60    CONTINUE
       if (icsf.ne.ncsft) then
        write(*,*) 'icsf,ncsft=',icsf,ncsft
        call bummer_interact
     &   ('rddrto - inconsistent read of DRT file',2,0)
       endif
c     WRITE (NLIST,*) 'csfs grouped by internal walks symmetry'
c     WRITE (NLIST,170) 'zwalks ','ywalks  ','wwalks  ','xwalks  '
c     DO 70 I=1,NSYM
c          WRITE (NLIST,160) I,(NCSFSM(J+(I-1)*4),J=1,4)
c70    CONTINUE
      write(nlist,400)
400   format(20x,
     .  '-----z----- -----y----- -----x----- -----w----- ---total---'/)
      write(nlist,401) zcsf,ycsf,xcsf,wcsf,ncsft
401   format(16x,'CSFs',5(i10,2x))
      write(nlist,402) (xbar(j),j=1,4),nwalk
402   format(6x,'internal walks',5(i10,2x))
      write(nlist,403) nvalz,nvaly,nvalx,nvalw,nvalwt
403   format('valid internal walks',5(i10,2x))
C
C
c     # modrt(*)...
      CALL RDDBL (NDRT,NIMOMX,BORD,NIOT)
C
c     # syml(*)...
      CALL RDDBL (NDRT,NIMOMX,BSYM,NIOT)

c     
c   ## calculate the arcsym values
c
      do i = 1, niot
      arcsym(0,i-1) = 1 
      arcsym(1,i-1) = bsym(i)
      arcsym(2,i-1) = bsym(i)
      arcsym(3,i-1) = 1
      enddo

C
c     # nj(*)...
      CALL RDDBL (NDRT,NIMOMX,NJ,NLEVEL)
C
c     # njskp(*)...
      CALL RDDBL (NDRT,NIMOMX,NJSKP,NLEVEL)
C
c     # a(*)...
      CALL RDDBL (NDRT,LENBUF,BUF(ibuf),NROW)

      neli = 2*buf(ibuf+nrow-1)

C
c     # b(*)...
      CALL RDDBL (NDRT,NROWMX,B,NROW)
C
c     # l(*,*,*)...
      DO 80 I=1,3
           CALL RDDBL (NDRT,lenbuf,l(0,1,i),4*NROW)
80    CONTINUE
C
c     # y(*,*,*)...
      DO 90 I=1,3
           CALL RDDBL (NDRT,LENBUF,y(0,1,i),4*NROW)
90    CONTINUE
C
c     # xbar(*,*)...
      DO 100 I=1,3
           CALL RDDBL (NDRT,LENBUF,xbarcdrt(1,i),NROW)
100   CONTINUE
C
      call inisp

c     # limvec(*)
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed limvec)
      call rddbl( ndrt, lenbuf, buf(limvec), limvecinfo(2) )
      call uncmprlimvec(buf(limvec),nwalk,limvecinfo(1),limvecinfo(2),
     .   limvecinfo(3))

      call skpx01(nwalk,buf(limvec),0,numv1)
      icd(1)=ibuf+lenbuf
      icd(2)=icd(1)+niot+1
      icd(3)=icd(2)+niot+1
      icd(4)=icd(3)+niot+1
      icd(5)=icd(4)+niot+1
      icd(6)=icd(5)+nsym*4
      icd(7)=icd(6)+nsym*4
      if (icd(7).gt.mxbuf) call bummer('insufficient memory',icd(7),2)

      call symwlk(niot,nsym,nrow,nrowmx,
     .    nwalk,l,y,buf(icd(1)),
     .    buf(icd(2)),buf(indsym),buf(icd(3)),arcsym,
     .    buf(icd(4)),xbarcdrt,ssym,mult,
     .    buf(limvec),nexw,ncsft,
     .    buf(icd(5)),buf(icd(6)),spnorb,b,
     .    spnir,multmx,spnodd,numv1)

      j = 0
      do i = 1, nwalk
         if ( buf(limvec+i-1) .eq. 0 ) then
            j = j + 1
            buf(limvec+i-1) = j
         else
            buf(limvec+i-1) = -buf(limvec+i-1)
         endif
      enddo
C
c     # csym(*)  for valid y,x,w walks...
c     CALL RDDBL (NDRT,LENBUF,buf(indsym+nvalz),NVWXY)
c      do i=1,nvalz
c        buf(indsym+i-1)=1
c      enddo
C
c     # 0/1 ref(*)...
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed ref)
      call rddbl( ndrt, lenbuf, buf(ref), limvecinfo(2) )
      call uncmprlimvec(buf(ref),PTHZ,limvecinfo(1),limvecinfo(2),
     .   limvecinfo(3))
      NREFS=0
      DO  I=1,PTHZ
           IF (buf(ref+i-1).EQ.1) NREFS=NREFS+1
      enddo
405   format(6x,'reference CSFs',50x,(i8,2x))
      WRITE (NLIST,405) NREFS
      write (nlist,406)
406   format(20('----')/)

c
c
c     maxlp3  = maximum number of loops per l value  needed for the
c               three external formula tape
c

c
      if (spnodd) niot=niot-1

c     call iwritex('limvecrddrtao4',buf(limvec),350)

c
c     set nlevel to the appropriate value
c     for the sort program which is
c     NOT niot+1
c     but nmot - nfc -nfv
c

      limvec=ref+forbyt(pthz)
      indsym=limvec+forbyt(nwalk)
      top=indsym+forbyt(nvalz+nvwxy)
c     write(*,*) 'limvec,indsym,top=',limvec,indsym,top

      nlevel=nl

C
c     write(*,*) 'cidrtinfo'
c     write(*,*) (icsf(i),i=1,4),mnbas
c     write(*,*) (ivcsf(i),i=1,4),nrow
c     write(*,*) (xbar(i),i=1,4),irefs

      RETURN
C
c120   FORMAT (/' drt header information:'/1X,A)
c130   FORMAT (' nmot  =',I6,' niot  =',I6,' nfct  =',I6,' nfvt  =',I6/
c     + 'nrow  =',I6,' nsym  =',I6,' ssym  =',I6,' lenbuf=',I6)
c140   FORMAT (1X,A,':',T15,5I10)
c150   FORMAT (1X,A,(T12,20I3))
c160   FORMAT ('irrep ',I2,4I10)
c170   FORMAT ('      ',2X,4A8,2x)
c180   FORMAT ('sum ',4X,4I10)
c190   FORMAT ('=====>',2X,32X,I10,A)
c200   FORMAT (5('---------'))
      END
c

       function maxlen(cc,nsegs,icase)
        implicit none 
         integer maxlen
        character*2 cc
       integer nsegs(4),big,i,ist,iend,icase
       integer mxnseg
       parameter (MXNSEG=200)
C
      INTEGER             CIST(MXNSEG,7),           DRTIDX(MXNSEG,7)
      INTEGER             DRTLEN(MXNSEG,7),         DRTST(MXNSEG,7)
      INTEGER             IPTHST(MXNSEG,7),         LENCI,       NSEG(7)
      INTEGER             SEGCI(MXNSEG,7),          SEGEL(MXNSEG,7)
      INTEGER             SEGSCR(MXNSEG,7),         SEGTYP(MXNSEG,7)
C
      COMMON / CIVCT  /   LENCI,       NSEG,        SEGEL,       CIST
      COMMON / CIVCT  /   SEGCI,       SEGSCR,      SEGTYP,      IPTHST
      COMMON / CIVCT  /   DRTST,       DRTLEN,      DRTIDX

C
      if (cc(1:1).eq.'z') then
          ist=1
          iend=nsegs(1)
      elseif (cc(1:1).eq.'y') then
          ist=nsegs(1)+1
          iend=nsegs(1)+nsegs(2)
      elseif (cc(1:2).eq.'xw') then
          ist=nsegs(1)+nsegs(2)+1
          iend=nsegs(1)+nsegs(2)+nsegs(3)+nsegs(4)
      elseif (cc(1:1).eq.'x') then
          ist=nsegs(1)+nsegs(2)+1
          iend=nsegs(1)+nsegs(2)+nsegs(3)
      elseif (cc(1:1).eq.'w') then
          ist=nsegs(1)+nsegs(2)+nsegs(3)+1
          iend=nsegs(1)+nsegs(2)+nsegs(3)+nsegs(4)
      endif
      big=0
      do i=ist,iend
        if (big.lt.segci(i,icase)) big=segci(i,icase)
      enddo
      maxlen=big
      return
      end
c*******************************************************************
c
      subroutine bummer_interact( text, ierr, errtyp )
c
c  process a program error.
c  interactive version, program waits for confirmation by user
c
c  input:
c  text  = character string to be printed.
c  ierr  = internal program error to be printed.
c  errtyp = 0 for warning.  traceback may be generated. execution
c             continues.
c         = 1 for nonfatal error.  traceback may be generated.
c             execution is stopped. jcl condition code is set to allow
c             subsequent program steps to continue if possible.
c         = 2 for fatal error.  traceback may be generated.
c             execution is stopped. jcl condition code is set to abort
c             subsequent program steps if possible.
c         = 3 only writing the bummer_interact file (normal termination)
c
c  entry ibummr must be called prior to bummer_interact() to set the out
c  unit and to perform any additional initialization.
c
c  version log:
c  13-oct-94 remove 'call exit' for ibm rs6000. (ahhc)
c  24-apr-92 %val() blocks added for ibm rs6000. -rls
c  11-sep-91 parerr() calls added. -rjh
c  13-mar-91 posix code added. -rls
c  01-mar-89 write(stderr,*) and call exit() for  machines (rls).
c  05-jul-88 unicos version (rls).
c  03-nov-87 ibm version (dcc).
c  10-sep-87 written by ron shepard.
c
      implicit integer(a-z)
c
      character*(*) text
      integer iunit, ierr, errtyp
c
c     # bummer_interact error types.
c     wrnerr = warning message
c     nfterr = non fatal error
c     faterr = fatal error
c     termerr = writes "normally terminated" onto file bummer_interact
      integer   wrnerr,  nfterr,  faterr, termerr
      parameter(wrnerr=0,nfterr=1,faterr=2, termerr=3)
c
      integer f77err
      integer nlist,        stderr
      save    nlist,        stderr
      data    nlist / 6 /,  stderr / 0 /
c
c*******************************************************************
      if ( errtyp .eq. wrnerr ) then
c*******************************************************************
c
      write(*,*)
      write(*,*)' Error occurred, press return to continue!'
      write(*,*)text
      read(*,*)
c
c        # print a warning message and continue execution.
c
         write(nlist,6010) 'bummer_interact (warning):', text, ierr
*@ifdef posix
*         write(stderr,6010) 'bummer_interact (warning):', text, ierr
*@elif defined  unicos
*         write(stderr,6010) 'bummer_interact (warning):', text, ierr
*C        call tracebk( nlist )
*@elif defined  unix
         write(stderr,6010) 'bummer_interact (warning):', text, ierr
*@endif
         return
c
c*******************************************************************
      elseif ( errtyp .eq. nfterr ) then
c*******************************************************************
c
      write(*,*)
      write(*,*)' Error occurred, press return to exit!'
      write(*,*)text
      read(*,*)
c
c        # print a warning message, stop execution.
c

         write(nlist,6010) 'bummer_interact (nonfatal):', text, ierr
*@ifdef posix
*         write(stderr,6010) 'bummer_interact (nonfatal):', text, ierr
*C        call parerr( ierr )
*         call f77exit( 0 )
*@elif defined  unicos
*         write(stderr,6010) 'bummer_interact (nonfatal):', text, ierr
*         call tracebk( nlist )
*C        call parerr( ierr )
*         stop 'program error'
*@elif defined  (hp) || defined ( rs6000) || defined ( unix) 
         write(stderr,6010) 'bummer_interact (nonfatal):', text, ierr
c        call parerr( ierr )
         stop 'program error'
*@elif defined  vax
*         call sys$exit(%val(42))
*@elif defined  ibm
*         stop 901
*@else
*C        call parerr( ierr )
*         stop 'program error'
*@endif
c
c**********************************************************************
      elseif ( errtyp .eq. faterr ) then
c**********************************************************************
c
      write(*,*)
      write(*,*)' Error occurred, press return to exit!'
      write(*,*)text
      read(*,*)
c
c        # print an error message, stop execution, and abort job
c        # sequence.
c
         write(nlist,6010) 'bummer_interact (fatal):', text, ierr
         open(unit=99,file='bummer_interact',status='unknown')
         write(99,*) 'fatal error encountered'
         close(99)

*@ifdef posix
*         write(stderr,6010) 'bummer_interact (fatal):', text, ierr
*C        call parerr( ierr )
*         call f77exit( 1 )
*@elif defined  unicos
*         write(stderr,6010) 'bummer_interact (fatal):', text, ierr
*C        call parerr( ierr )
*C        # abort() generates tracebacks automatically.
*         call abort
*@elif defined  (hp) || defined ( rs6000)
*         write(stderr,6010) 'bummer_interact (fatal):', text, ierr
*C        call parerr( ierr )
*C        # abort() generates tracebacks automatically.
*         stop 'program error'
*@elif defined  unix
         write(stderr,6010) 'bummer_interact (fatal):', text, ierr
C        call parerr( ierr )
         call flushstdout
         call exit( 1 )
         stop 'bummer_interact'
*@elif defined  vax
*         call sys$exit( %val(44) )
*@elif defined  ibm
*         stop 902
*@else
*         stop 'program error'
*@endif
c
c*******************************************************************
      elseif (errtyp .eq. termerr) then
c*******************************************************************
          return
c*******************************************************************
      else
c*******************************************************************
c
c        # unknown error level.  treat as a fatal error.
c
      write(*,*)
      write(*,*)' Error occurred, press return to exit!'
      write(*,*)text
      read(*,*)
c
         write(nlist,6020) 'bummer_interact (unknown): errtyp=',
     &    errtyp, text, ierr

*@ifdef posix
*         write(stderr,6020) 'bummer_interact (unknown): errtyp=',
*     &    errtyp, text, ierr
*C        call parerr( ierr )
*         call f77exit( 1 )
*@elif defined  (hp) || defined ( rs6000) || defined (unix)
         write(stderr,6020) 'bummer_interact (unknown): errtyp=',
     &    errtyp, text, ierr
c        call parerr( ierr )
c        # abort() generates tracebacks automatically.
         stop
*@elif defined  unicos
*         write(stderr,6020) 'bummer_interact (unknown): errtyp=',
*     &    errtyp, text, ierr
*C        call parerr( ierr )
*C        # abort() generates tracebacks automatically.
*         call abort
*@elif defined  vax
*         call sys$exit( %val(44) )
*@elif defined  ibm
*         stop 903
*@else
*         stop 'program error'
*@endif
c*******************************************************************
      endif
c*******************************************************************
c
c     # this statement is not executed, it is included
c     # just to avoid compiler warnings. -rls
      stop 'bummer_interact error'
c
c     # initialization...
c
      entry ibummr_interactive( iunit )
c
c     # save the listing unit for use later.
c
      nlist = iunit
c
*@ifdef posix
*C     # set stderr to the correct value.
*      call f77const( 'stderr', stderr, f77err )
*      if ( f77err .ne. 0 ) then
*         write(*,6010) 'ibummr f77const() error=', f77err
*         call f77exit( 1 )
*      endif
*@endif
c
      return
6010  format(1x,a,a,i10)
6020  format(1x,a,i10,a,i10)
      end


      subroutine symwlk( 
     . niot,   nsym,   nrow,   nrowmx, 
     . nwalk,  l,      y,      row, 
     . step,   csym,   wsym,   arcsym, 
     . ytot,   xbar,   ssym, 
     . mult,   limvec, nexw,   ncsft, 
     . nviwsm, ncsfsm, spnorb, b, 
     . spnir,  multmx, spnodd , numv1  )
c
c  this routine calculates the symmetry of all the valid internal
c  walks and computes csf counts.
c
c  output:
c  csym(*) = mult( internal_walk_symmetry, ssym ) for
c            the valid z,y,x,w-walks.
c  ncsft = total number of csfs for this orbital basis and drt.
c  nviwsm(1:4,1:nsym) = number of valid internal walks of each symmetry
c                       through each vertex.
c  ncsfsm(1:4,1:nsym) = number of csfs passing through each vertex
c                       indexed by the internal symmetry.
c
c  07-jun-89  skip-vector based walk generation. rls
c
      implicit none 
c====>Begin Module SYMWLK                 File cidrt2.f                 
c---->Makedcls Options: All variables                                   
c
c     Parameter variables
c
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMx = 128)
c
c     Argument variables
c
c
      INTEGER             ARCSYM(0:3,0:NIMOMX),  B(*),        CSYM(*)
      INTEGER             L(0:3,NROWMX,3),          LIMVEC(*)
      INTEGER             MULT(8,8),   MULTMX,      NCSFSM(4,NSYM)
      INTEGER             NCSFT,       NEXW(8,4),   NIOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVIWSM(4,NSYM)
      INTEGER             NWALK,       ROW(0:NIOT), SPNIR(MULTMX,MULTMX)
      INTEGER             SSYM,        STEP(0:NIOT)
      INTEGER             WSYM(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3),          YTOT(0:NIOT)


c
      LOGICAL             SPNODD,      SPNORB
c
c     Local variables
c
      CHARACTER           WXYZ(4)
c
      INTEGER             CLEV,        I,           IMUL,        ISSYM
      INTEGER             ISTEP,       ISYM,        MUL,         NLEV
      INTEGER             NUMV1,       NVALWT,      ROWC,        ROWN
      INTEGER             VER,         VER1,        VERS(4)
c
c====>End Module   SYMWLK                 File cidrt2.f                 
      data vers/1,2,3,3/
      data wxyz/'z','y','x','w'/
c
c
c     # limvec(*) in skip-vector form.
c
      nvalwt = 0
c
      call izero_wr( 4*nsym, nviwsm, 1 )
c
c     # loop over z, y, x, and w vertices.
c
      do 200 ver = 1, 4
         ver1 = vers(ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
         wsym(0) = 1
c
         do 50 i = 0, niot
            step(i) = 4
50       continue
c
100      continue
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)go to 180
            clev = clev-1
            goto 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         row(nlev) = rown
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
         if( limvec( ytot(nlev)+1 ) .ge. xbar(rown,ver1) )goto 100
         wsym(nlev) = mult( arcsym(istep,clev), wsym(clev) )
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         endif
c        # a complete valid walk has been generated.
         mul = 1
         if(spnorb)mul = b(rown) + 1
         do 179 imul = 1, mul
               isym  = mult(wsym(nlev), spnir(imul, mul))
               issym = mult(isym,ssym)
               if ( nexw(issym,ver) .ne. 0 ) then
                  nvalwt = nvalwt+1
                  csym(nvalwt) = issym
                  nviwsm(ver,isym) = nviwsm(ver,isym) + 1
               endif
179      continue
c
         go to 100
c
180      continue
c        # finished with walk generation for this vertex.
200   continue
c
c     # check the valid upper walk count...
      if(numv1.ne.nvalwt)
     .  call bummer('symwlk: numv1-nvalwt=', numv1-nvalwt,2)
c
      return
c
6100  format(1x,a,':',(8i8))
      end


      subroutine inisp
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      integer     nmomx
       parameter (nmomx=1023)
C
C     Local variables
C
      INTEGER             I,           IMULT,       J,           JKSYM
      INTEGER             JSYM,        KSYM,        MS,          NJK
      INTEGER             NUMJ
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
c
c     # calculate the number of external walks of each symmetry
c     # for each vertex.
c
      call izero_wr(4*8,nexw,1)
c
c     # z-walks (1 effective walk of symmetry 1)...
      nexw(1,1) = 1
c
c     # y-walks...
      do 420 i = 1,8
         nexw(i,2) = nbas(i)
420   continue
c
c     # w,x -walks...
      do 440 jsym = 1,8
         numj = nbas(jsym)
         nexw(1,3) = nexw(1,3)+(numj*(numj-1))/2
         nexw(1,4) = nexw(1,4)+(numj*(numj+1))/2
         do 430 ksym = 1,(jsym-1)
            jksym = mult(ksym,jsym)
            njk = numj*nbas(ksym)
            nexw(jksym,3) = nexw(jksym,3)+njk
            nexw(jksym,4) = nexw(jksym,4)+njk
430      continue
440   continue


c
c     # initialize the spin function irrep and multp arrays
c
c
      nmul = 0
      do 5 j = 1, hmult, 2
         nmul = nmul +1
         multp(nmul) = j
5     continue

      do 10 i = 1, multmx
         do 11 j = 1, multmx
         spnir(i, j) = 0
11       continue
10    continue
c
c     # spin function irreps(integer spin)
c
c
c     # spin = 0, 2, 4, ...(even); multiplicity = 1, 5, 9, ...
c
c     write(*, *)' hmult', hmult
      do 6 imult = 1, multmx, 4
         spnir(1, imult) = 1
         do 7 ms = 2, imult, 4
            spnir(ms,   imult) = lxyzir(1)
            spnir(ms+1, imult) = lxyzir(2)
            spnir(ms+2, imult) = lxyzir(3)
            spnir(ms+3, imult) = 1
7        continue
6     continue

c
c     # spin = 1, 3, 5, ...(odd) ; multiplicity = 3, 7, 11, ...
c
      do 8 imult = 3, multmx, 4
         spnir(1, imult) = lxyzir(3)
         spnir(2, imult) = lxyzir(2)
         spnir(3, imult) = lxyzir(1)
         do 9 ms = 4, imult, 4
            spnir(ms,   imult) = 1
            spnir(ms+1, imult) = lxyzir(3)
            spnir(ms+2, imult) = lxyzir(2)
            spnir(ms+3, imult) = lxyzir(1)
9        continue
8     continue
c     write(*,*)'lxyzir', lxyzir
c     write(*,*)'spnir'
c     write(*,"(19i3)")spnir
      return
      end

      block data 

C
C     Common variables
C
C     Parameter variables
      integer maxsym,nmomx
      parameter (maxsym=8,nmomx=1023)

      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym


      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/

      end
