!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine drvciudg(lcore,nntype,maxbuf,retcode)
c
c  organization is a bit unconventional, however, just like the CI
c  also pass indsym
c  lcore means here: maximum available memory on target platform !
c
c

         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
      EXTERNAL            MXSCR,       RL
C
      INTEGER             ATEBYT,      FORBYT
      INTEGER             MXSCR,       RL
C
C     Parameter variables
C
      INTEGER             TMIN0
      PARAMETER           (TMIN0 = 0)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
C
C     Argument variables
C
      INTEGER             LCORE,       NNTYPE
C
C     Local variables
C
      INTEGER             I,           ICLOCK,      ICOUNT
      INTEGER             LVLPRT
      INTEGER             MAXNEX,      MXORB3
      INTEGER             NI
      INTEGER             TAPE6,       TOTHRF,      TOTNC0
      INTEGER             TOTNC1,      TOTNC2,      TOTNC3,      TOTNC4
      INTEGER             TOTNCD,      TOTNEC,      TOTREF,      TOTSTC
      INTEGER             TOTVEC
      integer procfunc
C
*@ifdef big
*      integer     nmomx
*      parameter ( nmomx=1023 )
*@else
*      integer     nmomx
*      parameter ( nmomx=1023 )
*@endif
        integer retcode,scr1xl,scr4xl,scr3xl,maxbuf

C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,       TOTSPC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c     KBL3,       unused ????
c     KBL4,       unused ????
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c      MAXBL3,    unused ????
c   MAXBL4,       unused ????
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c      MXKBL3,    unused ????
c     MXKBL4,     unused ????
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c       N0XTLP    unused ????
c      N1EXT,     number of one-external off-diagonal integrals
c      N1XTLP,    unused ????
c     N2EXT       number of two-external off-diagonal integrals
c       N2XTLP,   unused ????
c     N3EXT,      number of three-external off-diagonal integrals
c       N3XTLP,i  unused ????
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMBLK3     unused ????
c      NMBLK4     unused ????
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)

C     Common variables
C
      INTEGER             CIST(MXNSEG,7),           DRTIDX(MXNSEG,7)
      INTEGER             DRTLEN(MXNSEG,7),         DRTST(MXNSEG,7)
      INTEGER             IPTHST(MXNSEG,7),         LENCI,       NSEG(7)
      INTEGER             SEGCI(MXNSEG,7),          SEGEL(MXNSEG,7)
      INTEGER             SEGSCR(MXNSEG,7),         SEGTYP(MXNSEG,7)
C
      COMMON / CIVCT  /   LENCI,       NSEG,        SEGEL,       CIST
      COMMON / CIVCT  /   SEGCI,       SEGSCR,      SEGTYP,      IPTHST
      COMMON / CIVCT  /   DRTST,       DRTLEN,      DRTIDX
C
C     Common variables
C
      INTEGER             ICNFST(MXNSEG,7),         ISGCNF(MXNSEG,7)
C
      COMMON / CNFSPC /   ICNFST,      ISGCNF
C
C     Common variables
C
      INTEGER             C2EX0EX,     C3EX1EX,     CDG4EX
      INTEGER             MAXSEG
      INTEGER             NSEG1X(4) ,NSEGD(4), NTYPE
      INTEGER             NPROC

      COMMON / INDATA / C2EX0EX,     C3EX1EX,     CDG4EX, MAXSEG,
     &                   NSEG1X ,NSEGD, NTYPE, NPROC
C
       logical notinit

      tape6=20
      lvlprt=3

c     call readinput
      notinit=.false.
      do i=1,4
       if (nsegd(i).eq.-1 .or. nseg1x(i).eq.-1) notinit=.true.
      enddo
      if (.not.notinit) then
      write(*,*) '*** NOTE:using ciudgin segmentation:',nsegd,'/',nseg1x
      else
c
c     nseg defaults
c
      ntype=nntype
      maxseg = MXNSEG
      do i=1,4
         nsegd(i)=procfunc(i,nproc,1)
         nseg1x(i)=procfunc(i,nproc,2)
      enddo
      cdg4ex=0
      c3ex1ex=0
      c2ex0ex=0
      write(*,*) 'using default segmentation:',nsegd,'/',nseg1x
      endif

      call timer(' ', tmin0, iclock, tape6 )
c
      totspc = lcore
c
c     # determine blocking and space requirements for the different
c     # sections of the program
c     # count one electron integrals and external orbitals.
c
      next=0
      nmonel=0
      icount=1
      do 310 i=1, nmsym
        blst1e(i)=icount
        ni=nbas(i)
        next=next+ni
        icount=icount+ni*(ni+1)/2
310   continue
      nmonel = icount - 1
c
c     # set some record values.
c
c     # space required for four external case.
      call smprep

c
c     # space requried for four external case.
c

      scr4xl = mxscr( mnbas, 'ci4x' )
c
      totnc4 = 2*atebyt(maxbuf) + atebyt(mnbas*mnbas) +
     +        max(atebyt(scr4xl),atebyt(nmonel+bufszi))
c
c     # space requried for three external case.
c
      scr3xl = mxscr( next, 'ci3x' )
c
      totnc3=atebyt(bufszl+maxlp3) + atebyt(maxbuf) + atebyt(scr3xl)
     .       + 5*forbyt(maxlp3)
c
      mxbl23 = maxbl2/3
c
c     # the call to mxscr will total the mnbas dependent requirements
c     # which have become machine independent. (another bad turn)
c
      scrlen = mxscr ( mnbas, 'ci2x' )
c
      totnc2 = atebyt( maxbl2+bufszi ) + atebyt( 2*mxbl23 ) +
     +    atebyt( bufszl ) +  2*atebyt( mnbas*mnbas ) +
     +    6*atebyt( mxbl23 ) + 3*atebyt( scrlen )
c
c     # one external.
c
c     # mxorb = blocksize for the two electron integrals(one external)
c
      mxorb = mnbas
      mxorb3 = mxorb * 3
c
c     # 1-ex routines do not need scratch as written. this call is
c     # to preserve the structure for allocating scratch space in
c     # the future
c
      scr1xl = mxscr ( mxorb, 'ci1x' )
c
      totnc1  =  atebyt( mxorb3 + bufszi ) + atebyt( mxorb ) +
     +       atebyt( bufszl ) + 2*atebyt(mxorb*mxorb) +
     +       atebyt( scr1xl )
c
c     # all internal
c
      totnc0  =  atebyt( bufszi ) + atebyt( bufszl )
c
c     # space requried for diagonal case.
c
      totncd =atebyt(next)+atebyt(mxbld)+atebyt(bufszl)+atebyt(intorb)+
     +        atebyt(next) + atebyt(nmomx)
c
c     # max210 is the starting location of the core memory which may
c     # be used in the last two steps, namely csfout and davidc.
c
c
      totref = forbyt ( nvalz )
      tothrf = forbyt(nvalz)
c
c
c     # report the space needed.
c     loc1 .. reflst(nvalz)  ..  totref
c     for AQCC only: loc5  .. forbyt(nvalz)  ..  totref
c
      maxnex = max(totnc4,totnc3,totnc2,totnc1,totnc0,totncd)
      totstc = totref
      if (ntype.eq.3) totstc = totstc + tothrf
      totnec = totstc + maxnex
      totvec = totspc - totnec

c
      if ( lvlprt .ge. 1 ) then
        write(tape6, * )
        write(tape6, * ) '< n-ex core usage >'
        write(tape6,*)   '    routines:'
        write(tape6, 1010 ) ' fourex ', totnc4
        write(tape6, 1010 ) ' threx  ', totnc3
        write(tape6, 1010 ) ' twoex  ', totnc2
        write(tape6, 1010 ) ' onex   ', totnc1
        write(tape6, 1010 ) ' allin  ', totnc0
        write(tape6, 1010 ) ' diagon ', totncd
        write(tape6, '(3x,8x,4x,a7)' ) '======='
        write(tape6, 1010 ) 'maximum ', maxnex
        write(tape6, *  )
        write(tape6, * ) ' __ static summary __ '
        write(tape6, 1010 ) 'reflst  ', totref
        write(tape6, 1010 ) 'hrfspc  ', tothrf
        write(tape6, '(3x,8x,4x,a7)' ) '-------'
        write(tape6, 1010 ) 'static->', totstc
        write(tape6, *  )
        write(tape6, * ) ' __ core required  __ '
        write(tape6, 1010 ) 'totstc  ', totstc
        write(tape6, 1010 ) 'max n-ex', maxnex
        write(tape6, '(3x,8x,4x,a7)' ) '-------'
        write(tape6, 1010 ) 'totnec->', totnec
        write(tape6, *  )
        write(tape6, * ) ' __ core available __ '
        write(tape6, 1010 ) 'totspc  ', totspc
        write(tape6, 1010 ) 'totnec -', totnec
        write(tape6, '(3x,8x,4x,a7)' ) '-------'
        write(tape6, 1010 ) 'totvec->', totvec
      endif
1010  format (3x,a8,4x,i12)
c
c     # stop if there is not enough core.
c
      if( totnec .gt. totspc ) then
         retcode=-1
         return
      endif
c
c
c     # calculate spcci for prepd (for segmentation)
c     # spcci is the maximum amount available for the
c     # CI vector
c     # in prepd this must also cover the amount
c     # of space needed for index,indsym and conft
c
      spcci = rl(totvec)
c
c     # core(*)-->
c
      retcode=0
      return
      end
      subroutine prepdnew
     .  ( index, indsym, conft, heap,maxfree )
c
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT,      ISUM,        RL
C
      INTEGER             ATEBYT,      FORBYT,      ISUM,        RL
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 20)
      INTEGER             MXENER
      PARAMETER           (MXENER = 40)
      INTEGER             MAXMAP
      PARAMETER           (MAXMAP = 20)
      INTEGER             MAXBFT
      PARAMETER           (MAXBFT = 1023)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             CONFT(*),    HEAP(*),     INDEX(*)
      INTEGER             INDSYM(*),   MAXFREE
C
C
C     Local variables
C
      CHARACTER           AMAP(4)
      CHARACTER*80        TITLE(MXTITL)
C
      INTEGER             BL,          CONFW,       CONFX,       CONFY
      INTEGER             CONFZ
      INTEGER             HPFREE
      INTEGER             HPTOP,       I,           IBLK,        ICNTW
      INTEGER             ICNTX
      INTEGER             INDXLEN,     IPTH
      INTEGER             ISYM,        IVAL,        JBF
      INTEGER             JSYM
      INTEGER             MXSEG
      INTEGER             NI,          NIJ
      INTEGER             NMBJ
      INTEGER             NY
      INTEGER             STRTDRT,     STRTVC
      INTEGER             STRTZYXW,    XMAP(MAXBFT,MAXMAP)
       integer            ierror
C
      REAL*8              ENERGY(MXENER),           RTOL(NVMAX)
      REAL*8              RTOLBK(NVMAX),            RTOLCI(NVMAX)
C
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,       TOTSPC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c     KBL3,       unused ????
c     KBL4,       unused ????
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c      MAXBL3,    unused ????
c   MAXBL4,       unused ????
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c      MXKBL3,    unused ????
c     MXKBL4,     unused ????
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c       N0XTLP    unused ????
c      N1EXT,     number of one-external off-diagonal integrals
c      N1XTLP,    unused ????
c     N2EXT       number of two-external off-diagonal integrals
c       N2XTLP,   unused ????
c     N3EXT,      number of three-external off-diagonal integrals
c       N3XTLP,i  unused ????
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMBLK3     unused ????
c      NMBLK4     unused ????
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)

C     Common variables
C
      INTEGER             C2EX0EX,     C3EX1EX,     CDG4EX
      INTEGER             MAXSEG
      INTEGER             NSEG1X(4) ,NSEGD(4), NTYPE
      INTEGER             NPROC

      COMMON / INDATA / C2EX0EX,     C3EX1EX,     CDG4EX, MAXSEG,
     &                   NSEG1X ,NSEGD, NTYPE, NPROC
C
C     Common variables
C
      INTEGER             CIST(MXNSEG,7),           DRTIDX(MXNSEG,7)
      INTEGER             DRTLEN(MXNSEG,7),         DRTST(MXNSEG,7)
      INTEGER             IPTHST(MXNSEG,7),         LENCI,       NSEG(7)
      INTEGER             SEGCI(MXNSEG,7),          SEGEL(MXNSEG,7)
      INTEGER             SEGSCR(MXNSEG,7),         SEGTYP(MXNSEG,7)
C
      COMMON / CIVCT  /   LENCI,       NSEG,        SEGEL,       CIST
      COMMON / CIVCT  /   SEGCI,       SEGSCR,      SEGTYP,      IPTHST
      COMMON / CIVCT  /   DRTST,       DRTLEN,      DRTIDX
C
C     Common variables
C
      INTEGER             ICNFST(MXNSEG,7),         ISGCNF(MXNSEG,7)
C
      COMMON / CNFSPC /   ICNFST,      ISGCNF
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C     Common variables
C
      INTEGER             IVWALK(4),   NREFS,       NROW
      INTEGER             WCSF,        XBAR(4),     XCSF,        YCSF
      INTEGER             ZCSF
C
      COMMON / CIDRTINFO/ ZCSF,        YCSF,        XCSF,        WCSF
      COMMON / CIDRTINFO/ IVWALK,      NROW,        XBAR
      COMMON / CIDRTINFO/ NREFS
C
C
C
cvp
c     # calculate external walks through x and w
c
      do 100 iblk = 1, nmsym
         icntx = 0
         icntw = 0
         nmbj = nmbpr(iblk)
         if (nmbj .gt. 0 ) then
            do 200 jbf = 1, nmbj
               jsym = lmdb(iblk,jbf)
               isym = lmda(iblk,jbf)
               if ( isym .ne. jsym ) then
                  nij = nbas(isym)*nbas(jsym)
                  icntx = icntx + nij
                  icntw = icntw + nij
               else
                  ni = nbas(isym)
                  icntx = icntx + ((ni-1)*ni)/2
                  icntw = icntw + ((ni+1)*ni)/2
               endif
200         continue
         endif
         cnfx(iblk) = icntx
         cnfw(iblk) = icntw
100   continue
c
c     write(20,6001)
c6001  format(/' number of external paths / symmetry')
c     write(20,6002)'x',(cnfx(i),i=1,nmsym)
c     write(20,6002)'w',(cnfw(i),i=1,nmsym)
c     write(20,*) 'internal paths:',pthz,pthy,pthx,pthw
c     write(20,*) 'external basis functions:',nbas
c6002  format(1x,'vertex',a2,8i8)
c
c     # get z-walk information from index vector
c
      ival=0
      confz = 0
      do 110 ipth = 1, pthz
         if (index(ipth).gt.0) then
            ival=ival+1
            conft(ival) = confz
            confz = confz + 1
         endif
110   continue
c
c     # get y-walk information from index vector
c
      confy = 0
      do 120 ipth = (pthz + 1), (pthz + pthy)
         if (index(ipth).gt.0) then
            ival=ival+1
            isym = sym21(indsym(ival))
            ny = nbas(isym)
            if (ny.le.0) then
               conft(ival) = -1
               index(ipth) = -1
               call bummer_interact
     &          ('prepd: zero externals for path no.',
     &          ipth, 0 )
c              write(*,*) 'nbas(',isym,')=',nbas(isym)
            else
               conft(ival) = confy + confz
               confy = confy + ny
            endif
         endif
120   continue

c
      confx = 0
      do 130 ipth = (pthy + pthz + 1) , (pthz + pthy + pthx)
         if ( index(ipth) .gt. 0 ) then
            ival = ival+1
            isym  = sym21( indsym(ival) )
            confx = confx + cnfx(isym)
         endif
130   continue

      confw = 0
      do 131 ipth = (pthy+pthz+pthx+1) , (pthz+pthy+pthx+pthw)
         if ( index(ipth) .gt. 0 ) then
            ival=ival+1
            isym  = sym21( indsym(ival) )
            confw = confw + cnfw(isym)
         endif
131   continue
      ierror=0
      if (confz.ne.zcsf) then
        ierror=1
        call bummer_interact
     &   ('prepd: wrong z configuration count',confz,0)
      endif
      if (confy.ne.ycsf) then
        ierror=1
        call bummer_interact
     &   ('prepd: wrong y configuration count',confy,0)
      endif
      if (confx.ne.xcsf) then
       ierror=1
       call bummer_interact
     &  ('prepd: wrong x configuration count',confx,0)
      endif
      if (confw.ne.wcsf) then
        ierror=1
        call bummer_interact
     &   ('prepd: wrong w configuration count',confw,0)
      endif
c      write(*,*) '######',confz,confy,confx,confw
c      call iwritex('index',index,pthz+pthy+pthx+pthw)
c      call iwritex('indsym',indsym,nvalz+nvaly+nvalw+nvalx)
c      call iwritex('conft',indsym,nvalz+nvaly+nvalw+nvalx)
      if (ierror.eq.1) stop 1111
      return
c7000  format(' program-defined maxseg (',i3,') will be exceeded',
c     + 'following nseg = ',i3)
      end
      subroutine segment
     .  ( index, indsym, conft, heap,maxfree,
     .    nsegt, slice,confz,confy,confx,confw,
     .   segtitle,mxseg,strtdrt,nout)

         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
C
      INTEGER             ATEBYT,      FORBYT
C
C     Parameter variables
C
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             ZTYPE
      PARAMETER           (ZTYPE = 1)
      INTEGER             YTYPE
      PARAMETER           (YTYPE = 2)
      INTEGER             XTYPE
      PARAMETER           (XTYPE = 3)
      INTEGER             WTYPE
      PARAMETER           (WTYPE = 4)
C
C     Argument variables
C
      CHARACTER*(*)       SEGTITLE
C
      INTEGER             CONFT(*),    CONFW,       CONFX,       CONFY
      INTEGER             CONFZ,       HEAP(*),     INDEX(*)
      INTEGER             INDSYM(*),   MAXFREE,     MXSEG
      INTEGER             NSEGT(*),    SLICE,       STRTDRT
      integer             nout
C
C     Local variables
C
      INTEGER             BUFSIZ,      CISTRT,      CMPSYM
      INTEGER             ENDZYXW
      INTEGER             I,           IMAP(4),     INDEX1,      INDXLEN
      INTEGER             IPTH,        ISEG,        ISGTYPE,     LCONFW
      INTEGER             LCONFX,      LCONFY,      LCONFZ
      INTEGER             MAXCSF,      NUMCNF,      NUMCSF
      INTEGER             NUMDMAT,     NUMPTH
      INTEGER             NWALKNEW,    NWALKS,      PTHCSF,      SCRCSF
      INTEGER             SEGNUMBER,   SGTYPE,      STCONF
      INTEGER             STRTVC,      STRTZYXW,    WRNERR
      integer  csf(4)
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,       TOTSPC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c     KBL3,       unused ????
c     KBL4,       unused ????
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c      MAXBL3,    unused ????
c   MAXBL4,       unused ????
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c      MXKBL3,    unused ????
c     MXKBL4,     unused ????
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c       N0XTLP    unused ????
c      N1EXT,     number of one-external off-diagonal integrals
c      N1XTLP,    unused ????
c     N2EXT       number of two-external off-diagonal integrals
c       N2XTLP,   unused ????
c     N3EXT,      number of three-external off-diagonal integrals
c       N3XTLP,i  unused ????
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMBLK3     unused ????
c      NMBLK4     unused ????
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)

C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C     Common variables
C
      INTEGER             CIST(MXNSEG,7),           DRTIDX(MXNSEG,7)
      INTEGER             DRTLEN(MXNSEG,7),         DRTST(MXNSEG,7)
      INTEGER             IPTHST(MXNSEG,7),         LENCI,       NSEG(7)
      INTEGER             SEGCI(MXNSEG,7),          SEGEL(MXNSEG,7)
      INTEGER             SEGSCR(MXNSEG,7),         SEGTYP(MXNSEG,7)
C
      COMMON / CIVCT  /   LENCI,       NSEG,        SEGEL,       CIST
      COMMON / CIVCT  /   SEGCI,       SEGSCR,      SEGTYP,      IPTHST
      COMMON / CIVCT  /   DRTST,       DRTLEN,      DRTIDX
C
C     Common variables
C
      INTEGER             ICNFST(MXNSEG,7),         ISGCNF(MXNSEG,7)
C
      COMMON / CNFSPC /   ICNFST,      ISGCNF
C

      data imap/1,2,3,4/
c
c this subroutine sets up the segmentation of the ci vector
c array index 1 (z), 2 (y), 3 (x), 4 (w)

c
c      print out
c

c      call iwritex('index',index,pthz+pthy+pthx+pthw)
c      call iwritex('indsym',indsym,nvalz+nvaly+nvalw+nvalx)


       csf(1)=confz
       csf(2)=confy
       csf(3)=confx
       csf(4)=confw

c
c     # initialize segmentation procedure
c
c
c     consider 1v+1w and 2v+2w case separately

c    # segment order  z y x w
c    # index order    z y x w
c
      iseg = 0
      strtvc=1
      segnumber=0
c
c  zero out arrays, so that we can restrict
c  segmentation to particular segment types
c
      call izero_wr(mxnseg,segel(1,slice),1)
      call izero_wr(mxnseg, cist(1,slice),1)
      call izero_wr(mxnseg,icnfst(1,slice),1)
      call izero_wr(mxnseg,isgcnf(1,slice),1)
      call izero_wr(mxnseg,segtyp(1,slice),1)
      call izero_wr(mxnseg,segscr(1,slice),1)
      call izero_wr(mxnseg,ipthst(1,slice),1)

      do 1000 isgtype = 1,4

         if (csf(isgtype).le.0) goto 1000

       sgtype=imap(isgtype)

      if ( sgtype .eq. ztype ) then
c
c        # initialize for z walks
c
         strtzyxw = 1
         endzyxw  = strtzyxw + pthz -1
         cistrt   = 0
         stconf   = 0
         lconfz   = 0
         maxcsf   = confz/nsegt(1) + 1
      elseif (sgtype .eq. ytype ) then
c
c        # initialize for y walks
c
         strtzyxw = pthz+1
         endzyxw  = strtzyxw + pthy -1
         cistrt   = confz
         stconf   = nvalz
         lconfy   = 0
         maxcsf   = confy/nsegt(2) + 1

      elseif ( sgtype .eq. wtype ) then
c
c        # initialize for w walks
c
         strtzyxw = pthz + pthy + pthx + 1
         endzyxw  = strtzyxw + pthw - 1
         cistrt = confz+confy + confx
         stconf = nvalz + nvaly + nvalx
         lconfw   = 0
         maxcsf   = confw/nsegt(4) + 1
c
      elseif ( sgtype .eq. xtype ) then
c
c        # initialize for x walks
c
         strtzyxw = pthz + pthy + 1
         endzyxw  = strtzyxw + pthx - 1
         cistrt = confz+confy
         stconf = nvalz + nvaly
         lconfx   = 0
         maxcsf   = confx/nsegt(3) + 1
      else
       call bummer_interact('segment: invalid sgtype=',sgtype,2)
      endif
c
c     # initialize segment type independent counts
c
      index1 = strtzyxw
      numpth = 0
      numcnf = 0
      numcsf = 0
      numdmat = 0

c
      do 140 ipth=strtzyxw,endzyxw
c
c        # get complimentary symmetry and determine number or external
c        # csfs for this path
c
         if ( index(ipth) .lt. 0 ) then
            numpth = numpth + 1
         else
            cmpsym = sym21(indsym(stconf+numcnf+1))
            if (sgtype.eq.wtype) then
               pthcsf = cnfw(cmpsym)
c              scrcsf = dmatw(cmpsym)
               scrcsf = 0
               lconfw = lconfw + pthcsf
            elseif (sgtype.eq.xtype) then
               pthcsf = cnfx(cmpsym)
c              scrcsf = dmatx(cmpsym)
               scrcsf = 0
               lconfx = lconfx + pthcsf
            elseif (sgtype.eq.ytype) then
               pthcsf = nbas(cmpsym)
               scrcsf = 0
               lconfy = lconfy + pthcsf
            elseif (sgtype.eq.ztype) then
               pthcsf = 1
               scrcsf = 0
               lconfz = lconfz + pthcsf
            endif
            if (pthcsf.le.0) then
               conft(stconf+numcnf+1) = -1
               index(ipth)        = -1
               call bummer_interact
     &          ('segment: zero externals for path no.',
     &          ipth, 0 )
c              write(*,*) 'pthcsf,cmpsym,sgtype=',pthcsf,cmpsym,sgtype
c              write(*,*) 'indsym=',indsym(stconf+numcnf+1),
c    .          stconf,numcnf
c
c              # account for bad path in index vector
c
               numpth = numpth + 1
            else
               conft(stconf+numcnf+1) = numcsf + cistrt
               if ( numcsf.ge.maxcsf
     .                       .and. index(ipth).eq.2 ) then
c
c                 # assign values for the segment up to ipth-1
c
                  if ( iseg .eq. mxseg ) then
                     write (0,7000) mxseg,iseg
                     call bummer_interact('segment: mxseg=', mxseg, 2 )
                  endif
                  iseg = iseg + 1
                  segel(iseg,slice) = numpth
                  cist (iseg,slice) = cistrt
                  icnfst(iseg,slice)= stconf
                  isgcnf(iseg,slice)= numcnf
                  segci (iseg,slice)= numcsf
                  segtyp(iseg,slice)= sgtype
                  segscr(iseg,slice)= numdmat
                  ipthst(iseg,slice)= index1
c
c                 # write out index vector for this segment
c
                  call pruneseg(sgtype,iseg,index(index1),numpth,
     &                      index1,heap,maxfree,conft(stconf+1),
     .                      indsym(stconf+1),numcnf,
     .                      nwalknew,bufsiz,indxlen)
                  segel(iseg,slice) = nwalknew
                  drtlen(iseg,slice) = bufsiz
                  drtidx(iseg,slice) = indxlen
                  drtst(iseg,slice) = strtdrt
c
c                 # set-up values for the potential next segment
c                 # beginning with ipth
c
                  strtdrt= strtdrt + bufsiz
                  strtvc = strtvc + ((numcsf-1)/4096+ 1)
                  index1 = index1 + numpth
                  numpth = 1
                  cistrt = cistrt + numcsf
                  numcsf = pthcsf
                  stconf = stconf + numcnf
                  numcnf = 1
ctm
                  numdmat=scrcsf
ctm
               else
c
c                 # add this path to the segment
c
                  numpth = numpth + 1
                  numcnf = numcnf + 1
                  numcsf = numcsf + pthcsf
ctm
              numdmat = numdmat + scrcsf
ctm
               endif
            endif
         endif
140   continue
c
c
      if ( numpth .gt. 0 ) then
         if ( iseg .eq. mxseg ) then
            write (0,7000) mxseg,iseg
            call bummer_interact
     &       ('segment: (iseg-mxseg)=', (iseg-mxseg), 2 )
         endif
         iseg = iseg + 1
         segel(iseg,slice) = numpth
         cist (iseg,slice) = cistrt
         icnfst(iseg,slice)= stconf
         isgcnf(iseg,slice)= numcnf
         segci(iseg,slice) = numcsf
         segtyp(iseg,slice)= sgtype
         segscr(iseg,slice) = numdmat
         ipthst(iseg,slice)= index1
c
c        # write out index vector for this segment
         call pruneseg(sgtype,iseg,index(index1),numpth,
     &                      index1,heap,maxfree,conft(stconf+1),
     .                      indsym(stconf+1),numcnf,
     .               nwalknew,bufsiz,indxlen)
          segel(iseg,slice) = nwalknew
          drtlen(iseg,slice) =bufsiz
          drtidx(iseg,slice) = indxlen
          drtst(iseg,slice) = strtdrt
         strtdrt = strtdrt + bufsiz
         strtvc = strtvc + ((numcsf-1)/4096+ 1)
      endif

       nsegt(isgtype)=iseg-segnumber
       segnumber=iseg

1000  continue
c
c     # validate number of determined walks
c
c   modified to check type-specific (z,y,x,w)
c
      nwalks = 0
      do 150 i = 1, iseg
         nwalks = nwalks + isgcnf(i,slice)
150   continue
c
c     # write a segmentation summary in tabular form
c
c just debug stuff
c     write(nout,6101) 'segmentation summary for type ',
c    .  segtitle
c     write(nout,6102)
c     write(nout,6105) 'seg.','no. of','no. of','starting',
c    + 'internal','starting'
c    + , 'starting'
c     write(nout,6105) 'no.','internal','ci','csf',
c    + 'walks','walk', 'DRT'
c     write(nout,6105) ' ','paths','elements','number',
c    + '/seg.','number','record'
c     write(nout,6102)
c     do 160 i = 1, iseg
c        write (nout,6107) amap(segtyp(i,slice)),
c    .    i,segel(i,slice),segci(i,slice),
c    +    cist(i,slice),isgcnf(i,slice),
c    .    icnfst(i,slice), drtst(i,slice)
c        write(nout,6102)
c 160   continue
c6101  format(///t20,2a)
c6102  format(1x,79('-'))
c6105  format(1x,a4,t8,6(a8,'|'))
c6107  format(1x,a2,i2,t8,6(i8,'|'))


      if ( nwalks .ne. nvalwk ) then
         write(0,7001) nwalks,nvalwk
         call bummer_interact
     &    ('segment: (nwalks-nvalwk)=',(nwalks-nvalwk), 2)
      endif
cvp
c7201  format(///t20,a)
c7202  format(1x,79('-'))
c7205  format(1x,a4,t8,8(a8,'|'))
c7207  format(1x,a2,i2,t8,8(i8,'|'))
cvp
c
c     # end summary
c
c      write(*,6202)(index(i),i=1,nintpt)
c      write(*,6204)(conft(i),i=1,nvalz+nvaly+nvalx+nvalw)
c      write(*,6205)(indsym(i),i=1,nvalz+nvaly+nvalx+nvalw)
c6202  format(' index vector'/(1x,20i6))
c6204  format(' conft vector'/(1x,20i6))
c6205  format(' indsymvector'/(1x,20i6))
      if (lconfz.ne.confz)
     . call bummer_interact('inconsitent confz',lconfz,2)
      if (lconfy.ne.confy)
     . call bummer_interact('inconsitent confy',lconfy,2)
      if (lconfw.ne.confw)
     . call bummer_interact('inconsitent confw',lconfw,2)
      if (lconfx.ne.confx)
     . call bummer_interact('inconsitent confx',lconfx,2)

      dimci = confz + confy + confx + confw
      lenci = dimci
      confyz=confz+confy
c
      return
7000  format(' program-defined mxseg (',i3,') will be exceeded',
     + 'following iseg = ',i3)
7001  format(' error in segment :  nwalks',i8,' nvalwk',i8,' stop')
      end

      function mxscr( size, progcd )
c
c  this function returns the amount of scratch space necessary for
c  the program section specifed by the character variable progcd.
c  size = an integer parameter used to determine workspace.
c
c  19-dec-90 machine-dependent allocations removed. case independent
c            string comparisons used. -rls
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            STREQ
C
      LOGICAL             STREQ
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*(*)       PROGCD
C
      INTEGER             SIZE,mxscr
C

C
c
      mxscr = 0
c
      if ( streq( progcd, 'ci4x' ) )then
c
c-*mdc*if cray + (ibm * vector) + alliant + titan + (sun * argonne)
c        # size = dimension of the largest external orbital block.
         mxscr = size * size + size + size * size
c-*mdc*else
c
c non-matrix call machines need no extra
c
          mxscr = 1
c
c-*mdc*endif
      elseif ( streq( progcd, 'ci3x' ) ) then
c
c-*mdc*if cray alliant titan or (sun .and. argonne)
c        # size = dimension of the external orbital space.
         mxscr = size * size
c-*mdc*else
c
c non-matrix call machines need no extra
c
          mxscr = 1
c-*mdc*endif
      elseif ( streq( progcd, 'ci2x' ) ) then
c        # size = largest number of orbitals in any single irrep
         mxscr = size*size
      elseif ( streq( progcd, 'ci1x' ) ) then
c        # size = dimension of the largest external orbital block.
         mxscr = 1
      else
         call bummer_interact('mxscr: unknown string',0,faterr)
      endif
      return
      end

      integer function calcthrxt(index)

c
c  calculate the maximum number of loops
c  contributing to the 1-internal formula tape.
c
c  code   loop          description
c  ----   -----        ---------------
c
c   0  -- new level ---
c   1  -- new record --
c   4  -- new vertex --
c
c   2       12c         (xy,wy) new relative loop value.
c   3       12c         (xy,wy) same loop value as before.
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            LOOP3CLC
C
      INTEGER             LOOP3CLC
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
C
      REAL*8              ONE
      PARAMETER           (ONE = 1.0D+00)
C
C     Argument variables
C
      INTEGER             INDEX(*)
C
C     Local variables
C
      INTEGER             MAXW1,       NUMC(2),     NUMW1(2)
C
C
C     Common variables
C
      INTEGER             BTAIL,       CLEV,        ILEV,        IMO
      INTEGER             JKLSYM,      JLEV,        JMO
      INTEGER             JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER             KMO,         KTAIL,       LLEV,        LMO
      INTEGER             LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
C     Common variables
C
      INTEGER             HEADV(MAXBF),             HLTV(MAXBF), IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             XKTV(MAXBF), YBTV(MAXBF), YKTV(MAXBF)
C
      REAL*8              VALV(MAXBF,3)
C
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XKTV
      COMMON / SPECIAL/   XBTV,        YBTV,        YKTV,        HLTV
      COMMON / SPECIAL/   IBF
C
C     Common variables
C
      INTEGER             CODE(4),     HILEV,       MO(0:4),     NINTL
      INTEGER             NIOT,        NMO,         NUMV
C
      LOGICAL             QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT,        NUMV,        HILEV
      COMMON / CLI    /   CODE,        MO,          NMO,         NINTL
C
C     Common variables
C
      INTEGER             ST1(8),      ST10(6),     ST11(4)
      INTEGER             ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER             ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER             ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON / CST    /   ST1,         ST2,         ST3,         ST4
      COMMON / CST    /   ST4P,        ST5,         ST6,         ST7
      COMMON / CST    /   ST8,         ST10,        ST11D,       ST11
      COMMON / CST    /   ST12,        ST13,        ST14
C
C     Common variables
C
      INTEGER             DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER             DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER             DB13(4,0:2), DB14(2,0:2,2)
      INTEGER             DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER             DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER             DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER             DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON / CDB    /   DB1,         DB2,         DB3,         DB4
      COMMON / CDB    /   DB4P,        DB5,         DB6,         DB7
      COMMON / CDB    /   DB8,         DB10,        DB11,        DB12
      COMMON / CDB    /   DB13,        DB14
C
C     Common variables
C
      INTEGER             VTPT
C
      REAL*8              OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON / CVTRAN /   VTRAN,       VCOEFF,      OLDVAL,      VTPT
C

C
c
      call intab
      call istack
c
      nintl = 1
      mo(0) = 0
      qdiag = .false.
c
      numv    = 1
      nmo     = 1
      code(1) = 3
      code(2) = 2
      maxw1 = 0
      lmo = 1
100   continue
         numw1(1) = 0
         numw1(2) = 0
         numc(1)=0
         numc(2)=0
         mo(1) = lmo
c
         ver = 3
90       continue ! ver = 3, 4
            oldval = one
c
           numw1(ver-2)=numw1(ver-2)+
     .           loop3clc( ver, 2, st12, db12(1,0,3), 4, 2,index)

c
          numc(ver-2)=numc(ver-2)+1
          ver = ver + 1
          if (ver .le. 4 ) go to 90
c
         numc(ver-2)=numc(ver-2)+1
         maxw1 = max(maxw1,numw1(1)+numc(1),numw1(2)+numc(2))
c
      lmo = lmo + 1
      if (lmo .le. niot) go to 100
c
cmd   call dump(1)
      icodev(ibf) = 0
      jumpinposition = 0
c
c6020  format(' maxw1=',i8)
      calcthrxt=maxw1
      return
c6100  format(' int1:  lmo = ',i4)
      end
      subroutine inisp
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      integer     nmomx
       parameter (nmomx=1023)
C
C     Local variables
C
      INTEGER             I,           IMULT,       J,           JKSYM
      INTEGER             JSYM,        KSYM,        MS,          NJK
      INTEGER             NUMJ
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
c
c     # calculate the number of external walks of each symmetry
c     # for each vertex.
c
      call izero_wr(4*8,nexw,1)
c
c     # z-walks (1 effective walk of symmetry 1)...
      nexw(1,1) = 1
c
c     # y-walks...
      do 420 i = 1,8
         nexw(i,2) = nbas(i)
420   continue
c
c     # w,x -walks...
      do 440 jsym = 1,8
         numj = nbas(jsym)
         nexw(1,3) = nexw(1,3)+(numj*(numj-1))/2
         nexw(1,4) = nexw(1,4)+(numj*(numj+1))/2
         do 430 ksym = 1,(jsym-1)
            jksym = mult(ksym,jsym)
            njk = numj*nbas(ksym)
            nexw(jksym,3) = nexw(jksym,3)+njk
            nexw(jksym,4) = nexw(jksym,4)+njk
430      continue
440   continue


c
c     # initialize the spin function irrep and multp arrays
c
c
      nmul = 0
      do 5 j = 1, hmult, 2
         nmul = nmul +1
         multp(nmul) = j
5     continue

      do 10 i = 1, multmx
         do 11 j = 1, multmx
         spnir(i, j) = 0
11       continue
10    continue
c
c     # spin function irreps(integer spin)
c
c
c     # spin = 0, 2, 4, ...(even); multiplicity = 1, 5, 9, ...
c
c     write(*, *)' hmult', hmult
      do 6 imult = 1, multmx, 4
         spnir(1, imult) = 1
         do 7 ms = 2, imult, 4
            spnir(ms,   imult) = lxyzir(1)
            spnir(ms+1, imult) = lxyzir(2)
            spnir(ms+2, imult) = lxyzir(3)
            spnir(ms+3, imult) = 1
7        continue
6     continue

c
c     # spin = 1, 3, 5, ...(odd) ; multiplicity = 3, 7, 11, ...
c
      do 8 imult = 3, multmx, 4
         spnir(1, imult) = lxyzir(3)
         spnir(2, imult) = lxyzir(2)
         spnir(3, imult) = lxyzir(1)
         do 9 ms = 4, imult, 4
            spnir(ms,   imult) = 1
            spnir(ms+1, imult) = lxyzir(3)
            spnir(ms+2, imult) = lxyzir(2)
            spnir(ms+3, imult) = lxyzir(1)
9        continue
8     continue
c     write(*,*)'lxyzir', lxyzir
c     write(*,*)'spnir'
c     write(*,"(19i3)")spnir
      return
      end
        subroutine limcnvrt(nwalk,limvec,
     &   y,xbar,l,b,arcsym,segtype,fpth)
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            FORBYT
C
      INTEGER             FORBYT
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      integer             nmomx
      parameter (nmomx=1023)
C
C     Argument variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX),   FPTH
      INTEGER             L(0:3,NROWMX),            LIMVEC(*),   NWALK
      INTEGER             SEGTYPE,     XBAR(NROWMX)
      INTEGER             Y(0:3,NROWMX)
C
C     Local variables
C
      INTEGER             CLEV,        I,           IMUL,        IOFF
      INTEGER             ISSYM,       ISTEP,       ISYM,        IWALK
      INTEGER             IWALKN,      J,           JSKIP,       LPTH
      INTEGER             MUL,         NLEV,        ROW(0:NIMOMX)
      INTEGER             ROWC,        ROWN,        SEGMARKS
      INTEGER             STEP(0:NIMOMX),           WSYM(0:NIMOMX)
      INTEGER             YTOT(0:NIMOMX)
C
c
c   input:  niot  number of internal orbitals
c           nwalk number of internal walks
c           limvec() full index vector segment
c           nrow  number of rows in drt
c           y,xbar,l,b,arcsym: drt arrays
c           segtype z:0 y:1 x: 2 w: 3
c           fpth  first walk in index segment (absolute)
c
c   output limvec
c      valid segmentation breaks marked by 2, other valid walks by 1
c      invalids are negative, length unchanged
c
c
c     modify initial index vector:
c
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      INTEGER             NIOT,        NROW
C
      COMMON / DRTINFO/   NROW,        NIOT
C


        lpth=fpth+nwalk-1
        do i=0,niot
          step(i)=4
        enddo

        clev = 0
        row(0) = segtype
        ytot(0) =0
        wsym(0) =1
        segmarks=0
        iwalkn=0

c
c       # begin walk generation ...
c

100     continue
c       # decrement the step at the current level
        nlev = clev+1
        istep = step(clev)-1
        if (istep.lt.0) then
c         # decrement the current level
          step(clev) =4
          if (clev.eq.0) goto 180
          clev=clev-1
          goto 100
        endif
        step(clev)=istep
        rowc = row(clev)
        rown = l(istep,rowc)
c       # valid row in the old drt?
        if (rown.eq.0) goto 100
        ytot(nlev) = ytot(clev)+y(istep,rowc)
         if ((ytot(nlev)+xbar(rown)).lt.fpth .or.
     .       (ytot(nlev)+1).gt.lpth) goto 100
          ioff= ytot(nlev)+1
c
c       # invalid walk
        if (-limvec(ioff).ge.xbar(rown)) goto 100
c       # cross check
        wsym(nlev)=mult(wsym(clev),arcsym(istep,clev))
        row(nlev) =rown
        if (nlev.lt.niot) then
c
c        # decrement to the next level
c
          clev=nlev
          goto 100
        else
           mul = 1
           if (spnorb) mul = b(rown) +1
           iwalk = ytot(nlev)+1
c
c        # complete walk has been generated
c

           do imul = 1, mul
              isym=mult(wsym(nlev),spnir(imul,mul))
              issym=mult(isym,ssym)
              if (nexw(issym,segtype).ne.0) then
                 if (imul.eq.1) then
                    limvec(iwalk)=2
                    segmarks=segmarks+1
                 else
                    limvec(iwalk)=1
                 endif
                 iwalkn=iwalkn+1
c            write(*,6020) iwalkn,iwalk,(step(i),i=0,(niot-1))
c6020     format(1x,2i8,(t20,50i1))
              endif
               iwalk=iwalk+1
            enddo
            goto 100
          endif
180       continue
c        if ( segmarks.ne.iwalk)
c    .    write(20,*) 'limcnvrt: found',iwalkn,' valid internal walks',
c    .   'out of ',iwalk-fpth,' walks (skipping trailing invalids)'
c         write(20,*) ' ... adding ',segmarks,' segmentation marks',
c    .    ' segtype=',segtype
          return
          end

        INTEGER FUNCTION ISUM(I,IA)

         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Argument variables
C
      INTEGER             I,           IA(*)
C
C     Local variables
C
      INTEGER             J,           SUM
C

       sum=0
       do j=1,i
        sum=sum+ia(j)
       enddo
       isum=sum
       return
       end
      subroutine pruneseg(sgtype,iseg,index,nwalko,fwalk,
     .  heap,maxfree,conft,indsym,nvwalk,nwalknew,
     .  bufsiz,indxlen)
c
c     input:  sgtype  (z(1),y(2),x(3),w(4))
c             iseg    segment number
c             index   index vector for this segment
c             nwalko  number of internal walks
c             fwalk   absolute number of first internal walk
c             heap    work space
c             maxfree free work space
c             conft   array mapping valid iwalk to csf number
c                     for this segment
c             indsym  array giving the symmetry of a valid iwalk
c             nvwalk  number of valid iwalks
c    output:  nwalknew new number of internal walks for this
c                      segment
c             indxlen  length of packed index vector
c             bufsiz   amount of work space used as write buffer
c                       in integer*4
c             heap(1:bufsiz) DRT info to be written to file
c
c    the calling routine must write the segment specific data
c    to a file
c

         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      integer       nwalko
      INTEGER             BUFSIZ,      CONFT(*),    FWALK,       HEAP(*)
      INTEGER             INDEX(NWALKO),            INDSYM(*),   INDXLEN
      INTEGER             ISEG,        MAXFREE,     NVWALK
      INTEGER             NWALKNEW,    SGTYPE
C
C     Local variables
C
      INTEGER             DRTMAP(4),   I,           IDRT,        INVALID
      INTEGER             J,           LNEW(0:3,NROWMX),         NWALKX
      INTEGER             PRINTLEVEL,  XBARNEW(NROWMX)
      INTEGER             YNEW(0:3,NROWMX)
C
c
c     # Arguments:  sgtype  (1=z,2=y,3=x,4=w)
c                   iseg    segment number
c                   index   index vector for this segment
c                   nwalko  number of walks in this segment
c                   fwalk   offset of the index
c                   niot    number of internal orbitals
c


C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             BORD(NIMOMX),             BSYM(NIMOMX)
      INTEGER             CASE(0:NROWMX),           L(0:3,NROWMX,3)
      INTEGER             MU(NIMOMX),  NJ(0:NIMOMX)
      INTEGER             NJSKP(0:NIMOMX),          ROW(0:NROWMX)
      INTEGER             XBARCDRT(NROWMX,3),       Y(0:3,NROWMX,3)
      INTEGER             YB(0:NROWMX),             YK(0:NROWMX)
C
      COMMON / DRT    /   B,           XBARCDRT,    L,           Y
      COMMON / DRT    /   YB,          YK,          CASE,        ROW
      COMMON / DRT    /   BORD,        BSYM,        ARCSYM,      NJ
      COMMON / DRT    /   NJSKP,       MU
C
C     Common variables
C
      INTEGER             NIOT,        NROW
C
      COMMON / DRTINFO/   NROW,        NIOT
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C

C
      data drtmap /1,2,3,3/

c
c     # select proper DRT from 3-DRT common
c
      call inisp
      idrt =drtmap(sgtype)
c
c     # prune l(*,*,idrt) according to the index vector segment
c
      call lpruneseg(6,nwalko,index,y(0,1,idrt),
     .               l(0,1,idrt),xbarcdrt(1,idrt),lnew,spnorb,b,spnodd,
     .               fwalk,sgtype)


c
c     # from the lnew chaining indices create xbar and y
c
        call chainseg( niot, nj,  njskp,  lnew,
     &                 ynew,xbarnew, nrow,
     &                 nwalkx,spnorb, multp, nmul,spnodd,
     &                 nwalko)


c
c     # from limvec,y,xbar,l (old)
c     # and  ynew,xbarnew,lnew generate the new
c     # index vector - if possible we should do
c     # this inplace as the new index vector won't
c     # be larger than the old one in any case
c     # heap(1..nwalknew) llimnew
c

        call limnew (niot,nwalko,index,nrow,
     &               ynew,lnew,y(0,1,idrt),xbarcdrt(1,idrt),
     &               l(0,1,idrt),b,arcsym,
     &               sgtype,fwalk,xbarnew,heap,nwalknew,
     &               nvwalk,invalid,maxfree,indxlen)


c
c      # return lnew,ynew,xbarnew,llimnew via the heap
c      # to the calling subroutine which will write it
c      # via diracc to the direct access file
c
        bufsiz=(4+1+4)*nrow+indxlen+nvwalk*2+1


        return
        end
      subroutine istack
c
c  initialize necessary stack information for loop construction.
c
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
C
C     Local variables
C
      INTEGER             I
C
C     Common variables
C
      INTEGER             BROW(0:NIMOMX),           EXTBK(0:NIMOMX)
      INTEGER             KROW(0:NIMOMX),           RANGE(0:NIMOMX)
      INTEGER             YBRA(0:NIMOMX),           YKET(0:NIMOMX)
C
      LOGICAL             QEQ(0:NIMOMX)
C
      REAL*8              V(3,0:NIMOMX)
C
      COMMON / CSTACK /   V,           RANGE,       EXTBK,       QEQ
      COMMON / CSTACK /   BROW,        KROW,        YBRA,        YKET
C
C

      do 100 i = 0, nimomx
         extbk(i) = 0
         v(1,i)   = zero
         v(2,i)   = zero
         v(3,i)   = zero
         ybra(i)    = 0
         yket(i)    = 0
100   continue
c
      return
      end

      integer function loop3clc( btail, ktail, stype, db0, nran, ir0,
     .  index)
c
c  construct loops and accumulate products of segment values.
c
c  input:
c  btail=    bra tail of the loops to be constructed.
c  ktail=    ket tail of the loops to be constructed.
c  stype(*)= segment extension type of the loops.
c  db0(*)=   delb offsets for the vn(*) segment values.
c  nran=     number of ranges for this loop template.
c  ir0=      range offset for this loop.
c  /cpt/     segment value pointer tables.
c  /cex/     segment type extension tables.
c  /ctab/    segment value tables.
c  /cdrt/    drt information.
c  /cdrt2/   vectors spanning number of walks.
c  /cstack/  loop construction stack arrays.
c  /csym/    symmetry information.
c  /cli/     loop information.
c
c  modified for mu-dependent segments 9-nov-86 (-rls).
c  this version written 07-sep-84 by ron shepard.
c
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NST
      PARAMETER           (NST = 40)
      INTEGER             NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
      INTEGER             MAXSYM
      PARAMETER           (MAXsYM = 8)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NWLKMX
      PARAMETER           (NWLKMX = 2**20-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      integer             nmomx
      parameter (nmomx=1023)

C
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER             NRAN,BTAIL,       DB0(NRAN,0:2,*)
      INTEGER             INDEX(*),    IR0,         KTAIL
      INTEGER             STYPE(NRAN)
C
C     Local variables
C
      INTEGER             BBRA,        BCASE,       BKET,        BROWC
      INTEGER             BROWN,       BVER,        DELB,        EXT
      INTEGER             I,           IGONV,       IL,          IMU
      INTEGER             INDXX,       IR,          KCASE,       KROWC
      INTEGER             KROWN,       KSEG,        KVER,        NEXR
      INTEGER             NLEV,        NLOOPLOC,    NUMBEROFLOOPS
      INTEGER             R,           STR,         WTLPHDB,     WTLPHDK
C
      LOGICAL             QBCHK(-4:4)
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             BORD(NIMOMX),             BSYM(NIMOMX)
      INTEGER             CASE(0:NROWMX),           L(0:3,NROWMX,3)
      INTEGER             MU(NIMOMX),  NJ(0:NIMOMX)
      INTEGER             NJSKP(0:NIMOMX),          ROW(0:NROWMX)
      INTEGER             XBARCDRT(NROWMX,3),       Y(0:3,NROWMX,3)
      INTEGER             YB(0:NROWMX),             YK(0:NROWMX)
C
      COMMON / DRT    /   B,           XBARCDRT,    L,           Y
      COMMON / DRT    /   YB,          YK,          CASE,        ROW
      COMMON / DRT    /   BORD,        BSYM,        ARCSYM,      NJ
      COMMON / DRT    /   NJSKP,       MU
C
C     Common variables
C
      INTEGER             BCASEX(6,-2:3),           KCASEX(6,-2:3)
      INTEGER             NEX(-2:3)
C
      COMMON / CEX    /   NEX,         BCASEX,      KCASEX
C
C     Common variables
C
      INTEGER             CODE(4),     HILEV,       MO(0:4),     NINTL
      INTEGER             NIOT,        NMO,         NUMV
C
      LOGICAL             QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT,        NUMV,        HILEV
      COMMON / CLI    /   CODE,        MO,          NMO,         NINTL
C
C     Common variables
C
      INTEGER             PT(0:3,0:3,NST5)
C
      COMMON / CPT    /   PT
C
C     Common variables
C
      INTEGER             BROW(0:NIMOMX),           EXTBK(0:NIMOMX)
      INTEGER             KROW(0:NIMOMX),           RANGE(0:NIMOMX)
      INTEGER             YBRA(0:NIMOMX),           YKET(0:NIMOMX)
C
      LOGICAL             QEQ(0:NIMOMX)
C
      REAL*8              V(3,0:NIMOMX)
C
      COMMON / CSTACK /   V,           RANGE,       EXTBK,       QEQ
      COMMON / CSTACK /   BROW,        KROW,        YBRA,        YKET
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C     Common variables
C
      REAL*8              TAB(NTAB,0:MAXB)
C
      COMMON / CTAB   /   TAB
C
C     Common variables
C
      INTEGER             CLEV,        ILEV,        IMO,         JKLSYM
      INTEGER             JLEV,        JMO,         JUMPINPOSITION
      INTEGER             KLEV,        KLSYM,       KMO,         LLEV
      INTEGER             LMO,         LSYM,        TAILB,       TAILK
      INTEGER             VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  TAILB,       TAILK,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
C


c     # dummy:
c     # local:
      data qbchk/.true.,.true.,.false.,.false.,.false.,
     +  .false.,.false.,.true.,.true./

      bver = min( btail, 3 )
      kver = min( ktail, 3 )
c
c     # assign a range (ir1 to nran) for each level of the
c     # current loop type.
c
      hilev = mo(nmo)
c
      ir = ir0
      do 120 i = 1, nmo
         ir = ir + 1
         do 110 il = mo(i-1), (mo(i)-2)
            range(il) = ir
110      continue
         ir = ir+1
         range(mo(i)-1) = ir
120   continue
c
c     # initialize stack arrays for loop construction.
c
      clev    = 0
      ybra(0)   = 0
      yket(0)   = 0
      brow(0) = btail
      krow(0) = ktail
      qeq(0)  = btail.eq.ktail
      v(1,0)  = one
      v(2,0)  = one
      v(3,0)  = one

      nlooploc=0
c
c     # one, two, or three segment value products are accumulated.
c
      if(numv.eq.1)then
         assign 1310 to igonv
      else if(numv.eq.2)then
         assign 1320 to igonv
      else
         assign 1330 to igonv
      endif
c
c     # begin loop construction:
c
      goto 1100
c
1000  continue
c
c     # decrement the current level, and check if done.
c
      extbk(clev) = 0
      clev = clev-1
      if(clev.lt.0) then
       loop3clc=nlooploc
       return
       endif
c
1100  continue
c
c     # new level:
c
      if(clev.eq.hilev)then
c
c        qdiag=t :allow both diagonal and off-diagonal loops.
c        qdiag=f :allow only off-diagonal loops.
c
         if ( qdiag .or. (.not. qeq(hilev)) ) then
            nlooploc=nlooploc+1
         endif
         go to 1000
      endif
c
      nlev = clev+1
      r = range(clev)
      str = stype(r)
      nexr = nex(str)
1200  continue
c
c     # new extension:
c
      ext = extbk(clev)+1
      if(ext.gt.nexr)go to 1000
      extbk(clev) = ext
      bcase = bcasex(ext,str)
      kcase = kcasex(ext,str)
c
c     # check for extension validity.
c
c     # canonical walk check:
c
      if ( qeq(clev) .and. (bcase .lt. kcase) ) go to 1200
      qeq(nlev) = qeq(clev).and.(bcase.eq.kcase)
c
c     # individual segment check:
c
      browc = brow(clev)
      brown = l(bcase,browc,bver)
      if(brown.eq.0)go to 1200
      krowc = krow(clev)
      krown = l(kcase,krowc,kver)
      if(krown.eq.0)go to 1200
      brow(nlev) = brown
      krow(nlev) = krown
c
c     # check b values of the bra and ket walks.
c
      bbra = b(brown)
      bket = b(krown)
      delb = bket-bbra
c
c     # the following check is equivalent to:
c     # if ( abs(delb) .gt. 2 ) go to 1200
c
      if ( qbchk(delb) ) go to 1200
c
      ybra(nlev) = ybra(clev)+y(bcase,browc,bver)
      wtlphdb = xbarcdrt(brown,bver)

      yket(nlev) = yket(clev)+y(kcase,krowc,kver)
      wtlphdk = xbarcdrt(krown,kver)
c
c check for invalid walks
c
c #   bra invalid
c
      if (-index(ybra(nlev)+1).ge. wtlphdb) goto 1200
c
c #   ket invalid
c
      if (-index(yket(nlev)+1).ge. wtlphdk) goto 1200

ctm ************************************************************

c     # accumulate the appropriate number of products:
c
      imu = mu(nlev)
      go to igonv
     + ,(1310, 1320, 1330)
c
1310  continue
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      if ( abs(v(1,nlev)) .le. small ) go to 1200
      go to 1340
c
1320  continue
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small      )go to 1200
      go to 1340
c
1330  continue
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      v(3,nlev) = v(3,clev)*tab(pt(bcase,kcase,db0(r,imu,3)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small .and.
     & abs(v(3,nlev)) .le. small      )go to 1200
c
1340  continue
c
c     # passed all the tests, this is a
c     # valid segment extension, increment to the next level:
c
      clev = nlev
      go to 1100
c
      end
      subroutine lpruneseg(
     & nlist, nwalk,  limvec,
     & y, l,      xbar,  lnew,  spnorb,
     & b,      spnodd, fpth,segtype)

c
c  prune l(*,*,*) according to limvec(*) so that it results in
c  the smallest drt and the most compact indexing scheme.
c
c  input:
c  niot = total number of internal orbitals.
c  nwalk = total number of walks using the current indexing scheme.
c  limvec(1:nwalk) = 0/1 representation (current indexing scheme).
c  y(*),xbar(*),l(*) = current drt chaining indices.
c  ytot(*),step(*),yntot(*),row(*) = temp walk generation arrays.
c  fpth  = absolute number of first path on index
c  segtype (z,y,x,w=1,2,3,4)
c  output:
c  lnew(*) = pruned chaining array.
c
c  31-jul-89 ref(*) checks added for z-walks. -rls
c  05-jun-89 written by ron shepard.
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      integer            nwalk
      INTEGER             B(*),        FPTH,        L(0:3,NROWMX)
      INTEGER             LIMVEC(NWALK),            LNEW(0:3,NROWMX)
      INTEGER             NLIST,  SEGTYPE
      INTEGER             XBAR(NROWMX),             Y(0:3,NROWMX)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             CLEV,        I,           II,          IMUL
      INTEGER             IOFF,        IROW,        ISTEP,       IWALK
      INTEGER             JJ,          LPTH,        LSCR(0:3,NROWMX)
      INTEGER             MUL,         NLEV,        NPRUNE,      NREF
      INTEGER             NUMV1,       NVALWT,      ROW(0:NIMOMX)
      INTEGER             ROWC,        ROWN,        STACK(0:NIMOMX,3)
      INTEGER             STEP(0:NIMOMX),           VER,         VER1
      INTEGER             YNTOT(0:NIMOMX),          YTOT(0:NIMOMX)
C
C     Common variables
C
      INTEGER             NIOT,        NROW
C
      COMMON / DRTINFO/   NROW,        NIOT
C

c
      rown = 0
      numv1 = 0
      rowc = 0
c
c     # initialize lnew(*)...
      call izero_wr(4*nrowmx,lnew,1)
      call izero_wr(4*nrowmx,lscr,1)


c
c     fpth = first path in index vector
c     lpth = last path in index vector
c
      lpth = fpth + nwalk-1
c     write(*,*) 'lprune: fpth,lpth,nwalk=',fpth,lpth,nwalk

      nvalwt = 0
      ver = segtype
c     # cidrt indx01   limvec:  0 0 1 1 0 0 1
c     # cidrt skpx              2 1 0 0 2 1 0
c     # CI                     -2-1 1 2-2-1 3

c
c        # begin walk generation for this vertex...
c
         clev = 0
         row(0) = ver
         ytot(0) = 0
c        call izero_wr(niot,row,1)
c        call izero_wr(niot,ytot,1)
c
         do 20 i = 0,niot
            step(i) = 4
20       continue
c
100      continue
c        # decrement the step at the current level.
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)goto 200
            clev = clev-1
            go to 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc)
         if(rown.eq.0)go to 100
         ytot(nlev) = ytot(clev)+y(istep,rowc)
c
c        # current path
c        # g�ltiger Bereich: ytot(nlev)+1 .. ytot(nlev)+xbar
c
         if ((ytot(nlev)+xbar(rown)).lt.fpth .or.
     .       (ytot(nlev)+1).gt.lpth) goto 100
          ioff= ytot(nlev)+1-fpth+1
         if(ver.eq.1)then
c           # z-walk; check both limvec(*) and ref(*).
            if( (-limvec(ioff) .ge. xbar(rown) )) goto 100
c           if( ( limvec(ytot(nlev)+1) .ge. xbar(rown) )
c    &       .and. ( ref(ytot(nlev)+1) .ge. xbar(rown) ))go to 100
         else
c           # w,x, or y walk.  just check limvec(*).
            if(-limvec(ioff) .ge. xbar(rown) )go to 100
         endif
c        # this step is used by at least one valid or reference walk.
         lnew(istep,rowc) = rown
         row(nlev) = rown
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         else
            mul = 1
            iwalk = ytot(nlev) + 1 -fpth +1
            if( spnorb )mul = b(rown) + 1
            do 179 imul = 1, mul
               if(limvec(iwalk) .gt. 0 )then
c         write(*,1000) iwalk,(step(i),i=0,niot-1)
1000    format('valid iwalk=',i4,'step:',20i2)
c                 # complete valid walk has been generated.
                  nvalwt = nvalwt+1
               endif
               iwalk = iwalk + 1
179         continue
c
c    # invalidate step
            go to 100
         endif
c
200   continue
c
c     # compute the number of pruned arcs.
         nprune = 0
         do 220 irow = 1,nrow
            do 210 istep = 0,3
               if(lnew(istep,irow).ne.l(istep,irow)) then
                nprune = nprune+1
c               write(*,*) 'deleted arc (istep,nrow)=',istep,irow,
c    .   'l=',l(istep,irow),'lnew=',lnew(istep,irow)
               endif
210         continue
220      continue
c      write(nlist,'(/1x,a,i8,a,i8,1x,a,i8)')
c    & 'lprune: l(*,*,*) pruned with nwalk=',nwalk,' nvalwt=',nvalwt
c    &    ,'nprune=', nprune
c
      return
      end
      subroutine chainseg( niot,   nj,     njskp,  l,
     & y,      xbar,   nrow,
     & nwalk,  spnorb, multp,
     & nmul,   spnodd ,seglen)
c
c  calculate the chaining information for the drt.
c  reverse lexical ordering is used.
c  pass only l,xbar,y, for this specific drt!
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
C
C     Argument variables
C
      INTEGER             NMUL,L(0:3,NROWMX),     MULTP(NMUL), NIOT
      INTEGER             NJ(0:NIMOMX),             NJSKP(0:NIMOMX)
      INTEGER             NROW,        NWALK,       SEGLEN
      INTEGER             XBAR(NROWMX),             Y(0:3,NROWMX)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             DRTTYP,      I,           ILEV,        IMUL
      INTEGER             IR1,         IR2,         IROW,        ISTEP
      INTEGER             LT,          NLEV,        VER,         VERS(4)
C

c
c        # initialize the top row.
c
         if(spnorb)then
c        call bummer_interact('spnorb missing',0,2)
            do 1001 imul = 1, nj(niot)
               irow = njskp(niot)+imul
               xbar(irow) = multp(imul)
               y(0,irow)  = 0
               y(1,irow)  = 0
               y(2,irow)  = 0
               y(3,irow)  = 0
1001        continue
         else
            irow = njskp(niot)+1
            xbar(irow) = 1
            y(0,irow)  = 0
            y(1,irow)  = 0
            y(2,irow)  = 0
            y(3,irow)  = 0
         endif
c
c        # loop over all the lower rows.
c
         do 600 nlev = niot, 1, -1
            ilev = nlev-1
c
            ir1 = njskp(ilev)+1
            ir2 = njskp(ilev)+nj(ilev)
            do 400 irow = ir1, ir2
c
c              # y(3,*,*) = 0  for all rows; included only for indexing.
c
               y(3,irow) = 0
c
c              # loop over the remaining step numbers.
c
               do 200 istep = 2, 0, -1
                  lt = l(istep+1,irow)
                  if ( lt .ne. 0 ) then
                     y(istep,irow) = y(istep+1,irow)
     &                + xbar(lt)
                  else
                     y(istep,irow) = y(istep+1,irow)
                  endif
200            continue
c
               lt = l(0,irow)
               if(lt.ne.0)then
                  xbar(irow) = y(0,irow)+xbar(lt)
               else
                  xbar(irow) = y(0,irow)
               endif
c
400         continue
600      continue
1000  continue

c     write(*,*)'chain: coded walks ',xbar(1)+xbar(2)+xbar(3)+xbar(4)
      return
      end
        subroutine limnew(niot,nwalko,limvec,nrow,
     &   ynew,lnew,y,xbar,l,b,arcsym,
     &   segtype,fpth,xbarnew,limn,nwalknew,nvalw,invalid,mxfre,
     &   indxlen)
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     External functions
C
      EXTERNAL            FORBYT
C
      INTEGER             FORBYT
C
C     Parameter variables
C
      integer             nmomx
      parameter (nmomx=1023)

      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX),   FPTH
      INTEGER             INDXLEN,     INVALID,     L(0:3,NROWMX)
      INTEGER             LIMN(*),     LIMVEC(*),   LNEW(0:3,NROWMX)
      INTEGER             MXFRE,       NIOT,        NROW,        NVALW
      INTEGER             NWALKNEW,    NWALKO,      SEGTYPE
      INTEGER             XBAR(NROWMX),             XBARNEW(NROWMX)
      INTEGER             Y(0:3,NROWMX),            YNEW(0:3,NROWMX)
C
C     Local variables
C
      INTEGER             CLEV,        CURVAL,      I,           IMUL
      INTEGER             IOFF,        ISSYM,       ISTEP,       ISYM
      INTEGER             IWALK,       IWALKN,      J,           JSKIP
      INTEGER             LPTH,        MUL,         NLEV,        NVWALK
      INTEGER             NWALK,       NXTVAL,      ROW(0:NIMOMX)
      INTEGER             ROWC,        ROWN,        SKIP
      INTEGER             STEP(0:NIMOMX),           WSYM(0:NIMOMX)
      INTEGER             YNTOT(0:NIMOMX),          YTOT(0:NIMOMX)
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C

        j=0
        do i=1,nwalko
          if (limvec(i).gt.0) j=j+1
        enddo
        if (forbyt(j).gt.mxfre)
     .   call bummer_interact('overflow of limn buffer',j,2)
        call izero_wr(j,limn,1)


c       write(*,'(1x,a)')
c    .  'limnew: beginning the valid upper walk index recomputation...'

        lpth=fpth+nwalko-1
        do i=0,niot
          step(i)=4
        enddo

        iwalkn=0
        nvalw = 0
        clev = 0
        row(0) = segtype
        ytot(0) =0
        yntot(0) =0
        wsym(0) =1

c
c       # begin walk generation ...
c

100     continue
c       # decrement the step at the current level
        nlev = clev+1
        istep = step(clev)-1
        if (istep.lt.0) then
c         # decrement the current level
          step(clev) =4
          if (clev.eq.0) goto 180
          clev=clev-1
          goto 100
        endif
        step(clev)=istep
        rowc = row(clev)
        rown = l(istep,rowc)
c       # valid row in the old drt?
        if (rown.eq.0) goto 100
        ytot(nlev) = ytot(clev)+y(istep,rowc)
c
c       # here we start getting into the offset trouble
c       # yold still counts the full segment
c       # ynew only relative within the current segment
c       # so how to achieve consistency by using offsets???
         if ((ytot(nlev)+xbar(rown)).lt.fpth .or.
     .       (ytot(nlev)+1).gt.lpth) goto 100
          ioff= ytot(nlev)+1-fpth+1
c
c       # invalid walk
        if (-limvec(ioff).ge.xbar(rown)) goto 100
c       # cross check
        if (lnew(istep,rowc).eq.0)
     .    call bummer_interact('limnew: error rowc=',rowc,2)
        wsym(nlev)=mult(wsym(clev),arcsym(istep,clev))
        row(nlev) =rown
        yntot(nlev) = yntot(clev)+ynew(istep,rowc)
        if (nlev.lt.niot) then
c
c        # decrement to the next level
c
          clev=nlev
          goto 100
        else
           mul = 1
           if (spnorb) mul = b(rown) +1
           iwalk = yntot(nlev)+1
c
c       #   if we have found the first valid walk number of
c       #   this segment in the NEW drt, we could offset
c       #   the arc weights at level zero by this amount
c       #   to force starting the index vector at this border
c       #   and avoid a number of zero entries at the beginning
c
c        # complete walk has been generated
c

           do imul = 1, mul
              isym=mult(wsym(nlev),spnir(imul,mul))
              issym=mult(isym,ssym)
              if (nexw(issym,segtype).ne.0) then
                 nvalw=nvalw+1
                 iwalkn=iwalkn+1
c # limn contains the internal walk number
                 limn(iwalkn)=iwalk
c             # possibly lim(iwalkn)=iwalk
c            write(*,6020) iwalkn,iwalk,(step(i),i=0,(niot-1))
6020     format(1x,2i8,(t20,50i1))
              endif
               iwalk=iwalk+1
            enddo
            goto 100
          endif
180       continue
c           write(*,*) 'limnew:',nvalw,' valid walks'
c
c        # at this point contains limnew(1:iwalkn)=iwalkno (newdrt)
c        # now we know
c        #    1. the first 1..limn(1)-1 walks (newdrt) are invalid
c        #    2. limn(iwalkn) is the last valid walk
c        #    3. indexvector length: limn(iwalkn)-limn(1)+1

c        #   check whether new index vector length is shorter than
c        #   the old one
            nwalknew = limn(iwalkn)-limn(1)+1
            if (nwalko.lt.nwalknew)
     .        call bummer_interact('index vector length increases ... ',
     .        nwalknew-nwalko,2)

            invalid = limn(1)-1
            ioff = limn(1)-1
          j=0
          skip=1
           curval=limn(1)-ioff
           do i=2,iwalkn
             nxtval=limn(i)-ioff
             if (curval+1.eq.nxtval) then
                skip=skip+1
             else
                j=j+2
                skip=1
             endif
           curval=nxtval
           enddo
          j=j+2
          indxlen=j

            return
           end

      block data  no
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             NIOMX
      PARAMETER           (NIOMX = 128)
      INTEGER             MAXXW
      PARAMETER           (MAXXW = 5000)
      INTEGER             MXLOOP
      PARAMETER           (MXLOOP = 21000)
      INTEGER             LPLENG
      PARAMETER           (LPLENG = NIOMX*MAXXW)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
C
C     Local variables
C
C
*@ifdef debug
*      integer         itest
*      common /ctestx/ itest(40)
*@endif
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C
      data symtab/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      data sym12/1,2,3,4,5,6,7,8/
      end

      block data nummerzwo
c
c  pointer tables for segment construction and evaluation.
c
c  mu-dependent entries added 7-nov-86 (rls).
c  this version written 7-sep-84 by ron shepard.
c
c
c     # s32=sqrt(3./2.) s34=sqrt(3./4.)
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER             MAXSYM
      parameter          (maxsym=8)
      integer             nmomx
      parameter (nmomx=1023)
      INTEGER             NST
      PARAMETER           (NST = 40)
      INTEGER             NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER             W00
      PARAMETER           (W00 = 3)
      INTEGER             RT0
      PARAMETER           (RT0 = 8)
      INTEGER             LB0
      PARAMETER           (LB0 = 13)
      INTEGER             RB0
      PARAMETER           (RB0 = 18)
      INTEGER             LT0
      PARAMETER           (LT0 = 23)
      INTEGER             R0
      PARAMETER           (R0 = 28)
      INTEGER             L0
      PARAMETER           (L0 = 33)
      INTEGER             WRT00
      PARAMETER           (WRT00 = 38)
      INTEGER             WRB00
      PARAMETER           (WRB00 = 43)
      INTEGER             WW00
      PARAMETER           (WW00 = 48)
      INTEGER             WR00
      PARAMETER           (WR00 = 53)
      INTEGER             RTRT00
      PARAMETER           (RTRT00 = 58)
      INTEGER             RBRB00
      PARAMETER           (RBRB00 = 63)
      INTEGER             RTRB00
      PARAMETER           (RTRB00 = 68)
      INTEGER             RRT00
      PARAMETER           (RRT00 = 73)
      INTEGER             RRT10
      PARAMETER           (RRT10 = 78)
      INTEGER             RRB00
      PARAMETER           (RRB00 = 83)
      INTEGER             RRB10
      PARAMETER           (RRB10 = 88)
      INTEGER             RR00
      PARAMETER           (RR00 = 93)
      INTEGER             RR10
      PARAMETER           (RR10 = 98)
      INTEGER             RTLB0
      PARAMETER           (RTLB0 = 103)
      INTEGER             RTLT10
      PARAMETER           (RTLT10 = 108)
      INTEGER             RBLB10
      PARAMETER           (RBLB10 = 113)
      INTEGER             RTL10
      PARAMETER           (RTL10 = 118)
      INTEGER             RLB10
      PARAMETER           (RLB10 = 123)
      INTEGER             RLT10
      PARAMETER           (RLT10 = 128)
      INTEGER             RL00
      PARAMETER           (RL00 = 133)
      INTEGER             RL10
      PARAMETER           (RL10 = 138)
      INTEGER             W10
      PARAMETER           (W10 = 143)
      INTEGER             W20
      PARAMETER           (W20 = 148)
      INTEGER             WW10
      PARAMETER           (WW10 = 153)
      INTEGER             WW20
      PARAMETER           (WW20 = 158)
      INTEGER             WRT10
      PARAMETER           (WRT10 = 163)
      INTEGER             WRT20
      PARAMETER           (WRT20 = 168)
      INTEGER             WRB10
      PARAMETER           (WRB10 = 173)
      INTEGER             WRB20
      PARAMETER           (WRB20 = 178)
      INTEGER             WR10
      PARAMETER           (WR10 = 183)
      INTEGER             WR20
      PARAMETER           (WR20 = 188)
      INTEGER             RTRB10
      PARAMETER           (RTRB10 = 193)
      INTEGER             RTRB20
      PARAMETER           (RTRB20 = 198)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              HALF
      PARAMETER           (HALF = 5D-1)
      REAL*8              HALFM
      PARAMETER           (HALFM = -5D-1)
      REAL*8              ONEM
      PARAMETER           (ONEM = -1D0)
      REAL*8              S32
      PARAMETER
     x(S32 = 1.2247448713915890490986420373529458130097385D0)
      REAL*8              S34
      PARAMETER
     x(S34 = .86602540378443864676372317075293616107654866D0)
C
      integer             l(0:3,0:3,-2:2),          lb(0:3,0:3,-2:2)
      integer             lt(0:3,0:3,-2:2),         r(0:3,0:3,-2:2)
      integer             rb(0:3,0:3,-2:2),         rblb1(0:3,0:3,-2:2)
      integer             rbrb0(0:3,0:3,-2:2),      rl0(0:3,0:3,-2:2)
      integer             rl1(0:3,0:3,-2:2),        rlb1(0:3,0:3,-2:2)
      integer             rlt1(0:3,0:3,-2:2),       rr0(0:3,0:3,-2:2)
      integer             rr1(0:3,0:3,-2:2),        rrb0(0:3,0:3,-2:2)
      integer             rrb1(0:3,0:3,-2:2),       rrt0(0:3,0:3,-2:2)
      integer             rrt1(0:3,0:3,-2:2),       rt(0:3,0:3,-2:2)
      integer             rtl1(0:3,0:3,-2:2),       rtlb(0:3,0:3,-2:2)
      integer             rtlt1(0:3,0:3,-2:2),      rtrb0(0:3,0:3,-2:2)
      integer             rtrb1(0:3,0:3,-2:2),      rtrb2(0:3,0:3,-2:2)
      integer             rtrt0(0:3,0:3,-2:2),      w0(0:3,0:3,-2:2)
      integer             w1(0:3,0:3,-2:2),         w2(0:3,0:3,-2:2)
      integer             wr0(0:3,0:3,-2:2),        wr1(0:3,0:3,-2:2)
      integer             wr2(0:3,0:3,-2:2),        wrb0(0:3,0:3,-2:2)
      integer             wrb1(0:3,0:3,-2:2),       wrb2(0:3,0:3,-2:2)
      integer             wrt0(0:3,0:3,-2:2),       wrt1(0:3,0:3,-2:2)
      integer             wrt2(0:3,0:3,-2:2),       ww0(0:3,0:3,-2:2)
      integer             ww1(0:3,0:3,-2:2),        ww2(0:3,0:3,-2:2)

C     Common variables
C
      INTEGER             DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER             DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER             DB13(4,0:2), DB14(2,0:2,2)
      INTEGER             DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER             DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER             DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER             DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON / CDB    /   DB1,         DB2,         DB3,         DB4
      COMMON / CDB    /   DB4P,        DB5,         DB6,         DB7
      COMMON / CDB    /   DB8,         DB10,        DB11,        DB12
      COMMON / CDB    /   DB13,        DB14
C
C     Common variables
C
      INTEGER             BCASEX(6,-2:3),           KCASEX(6,-2:3)
      INTEGER             NEX(-2:3)
C
      COMMON / CEX    /   NEX,         BCASEX,      KCASEX
C
C     Common variables
C
      INTEGER             PT(0:3,0:3,NST5)
C
      COMMON / CPT    /   PT
C
C     Common variables
C
      INTEGER             ST1(8),      ST10(6),     ST11(4)
      INTEGER             ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER             ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER             ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON / CST    /   ST1,         ST2,         ST3,         ST4
      COMMON / CST    /   ST4P,        ST5,         ST6,         ST7
      COMMON / CST    /   ST8,         ST10,        ST11D,       ST11
      COMMON / CST    /   ST12,        ST13,        ST14
C
C     Common variables
C
      INTEGER             MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
      integer            levsym (nmomx)
C
      COMMON / CSYM   /   MULT,        NSYM,        SSYM, levsym
C
C
C     Common variables
C
      INTEGER             VTPT
C
      REAL*8              OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON / CVTRAN /   VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
      equivalence (w0(0,0,0),pt(0,0,w00))
c
      equivalence (rt(0,0,0),pt(0,0,rt0))
c
      equivalence (lb(0,0,0),pt(0,0,lb0))
c
      equivalence (rb(0,0,0),pt(0,0,rb0))
c
      equivalence (lt(0,0,0),pt(0,0,lt0))
c
      equivalence (r(0,0,0),pt(0,0,r0))
c
      equivalence (l(0,0,0),pt(0,0,l0))
c
      equivalence (wrt0(0,0,0),pt(0,0,wrt00))
c
      equivalence (wrb0(0,0,0),pt(0,0,wrb00))
c
      equivalence (ww0(0,0,0),pt(0,0,ww00))
c
      equivalence (wr0(0,0,0),pt(0,0,wr00))
c
      equivalence (rtrt0(0,0,0),pt(0,0,rtrt00))
c
      equivalence (rbrb0(0,0,0),pt(0,0,rbrb00))
c
      equivalence (rtrb0(0,0,0),pt(0,0,rtrb00))
c
      equivalence (rrt0(0,0,0),pt(0,0,rrt00))
c
      equivalence (rrt1(0,0,0),pt(0,0,rrt10))
c
      equivalence (rrb0(0,0,0),pt(0,0,rrb00))
c
      equivalence (rrb1(0,0,0),pt(0,0,rrb10))
c
      equivalence (rr0(0,0,0),pt(0,0,rr00))
c
      equivalence (rr1(0,0,0),pt(0,0,rr10))
c
      equivalence (rtlb(0,0,0),pt(0,0,rtlb0))
c
      equivalence (rtlt1(0,0,0),pt(0,0,rtlt10))
c
      equivalence (rblb1(0,0,0),pt(0,0,rblb10))
c
      equivalence (rtl1(0,0,0),pt(0,0,rtl10))
c
      equivalence (rlb1(0,0,0),pt(0,0,rlb10))
c
      equivalence (rlt1(0,0,0),pt(0,0,rlt10))
c
      equivalence (rl0(0,0,0),pt(0,0,rl00))
c
      equivalence (rl1(0,0,0),pt(0,0,rl10))
c
      equivalence (w1(0,0,0),pt(0,0,w10))
c
      equivalence (w2(0,0,0),pt(0,0,w20))
c
      equivalence (ww1(0,0,0),pt(0,0,ww10))
c
      equivalence (ww2(0,0,0),pt(0,0,ww20))
c
      equivalence (wrt1(0,0,0),pt(0,0,wrt10))
c
      equivalence (wrt2(0,0,0),pt(0,0,wrt20))
c
      equivalence (wrb1(0,0,0),pt(0,0,wrb10))
c
      equivalence (wrb2(0,0,0),pt(0,0,wrb20))
c
      equivalence (wr1(0,0,0),pt(0,0,wr10))
c
      equivalence (wr2(0,0,0),pt(0,0,wr20))
c
      equivalence (rtrb1(0,0,0),pt(0,0,rtrb10))
c
      equivalence (rtrb2(0,0,0),pt(0,0,rtrb20))
c
c     # for each loop type, assign the segment extension type pointers
c     # and the delb offsets for each range.
c
c     # loop type 1:
      data  st1, db1
     & /  3,     1,     0,    -1,     0,     1,     0,    -1,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 2:
      data  st2, db2
     & /  3,     1,     0,    -1,     0,    -1,     0,     1,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 3:
      data  st3, db3
     & /  3,     1,     0,     1,     0,    -1,     0,    -1,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0/
c
c     # loop type 4:
      data  st4, db4
     & /  3,     0,     0,     1,     0,    -1,
     & rl00,   w00,  rl00,   rb0,    r0,   rt0,
     & rl00,   w10,  rl00,   rb0,    r0,   rt0,
     & rl00,   w20,  rl00,   rb0,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 4':
      data st4p, db4p
     & /  3,      0,     0,    -1,     0,     1,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 5:
      data  st5, db5
     & /  3,      2,     0,    -1,     0,    -1,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0/
c
c     # loop type 6:
      data  st6, db6
     & /  3,     1,     0,     0,     0,    -1,
     & rl00,   rb0,    r0,  wr00,    r0,   rt0,
     & rl00,   rb0,    r0,  wr10,    r0,   rt0,
     & rl00,   rb0,    r0,  wr20,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb00,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb10,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb20,    r0,   rt0/
c
c     # loop type 7:
      data  st7, db7
     & /  3,     1,     0,    -2,     0,     1,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0/
c
c     # loop type 8:
      data  st8, db8
     & /  3,     1,     0,    -1,     0,      0,
     & rl00,   rb0,    r0,   rt0,  rl00,    w00,
     & rl00,   rb0,    r0,   rt0,  rl00,    w10,
     & rl00,   rb0,    r0,   rt0,  rl00,    w20,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10/
c
c     # loop type 10:
      data st10, db10
     & /  3,     1,     0,     1,     0,     -2,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00/
c
c     # loop type 11d, 11:
      data st11d, st11, db11
     & /  3,      3,      3,      3,
     &    3,      0,      0,      0,
     & rl00,    w00,   rl00,    w00,
     & rl00,    w10,   rl00,    w10,
     & rl00,    w20,   rl00,    w20,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10/
c
c     # loop type 12:
      data st12, db12
     & /  3,     1,     0,    -1,
     & rl00, wrb00,    r0,   rt0,
     & rl00, wrb10,    r0,   rt0,
     & rl00, wrb20,    r0,   rt0,
     & rl00,   rb0,    r0, wrt00,
     & rl00,   rb0,    r0, wrt10,
     & rl00,   rb0,    r0, wrt20,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0/
c
c     # loop type 13:
      data st13, db13
     & /  3,      2,     0,     -2,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00/
c
c     # loop type 14:
      data st14, db14
     & /  3,     3,
     & rl00,  ww00,
     & rl00,  ww10,
     & rl00,  ww20,
     & rl00,   w00,
     & rl00,   w10,
     & rl00,   w20/
c
c     # segment values are calculated as:
c     #
c     #        tab( type(bcase,kcase,delb), bket)
c     #
c     # where the entries in tab(*,*) are defined in the table
c     # initialization routine.  type(*,*,*) is equivalenced to the
c     # appropriate  location of pt(*,*,*) and pt(*,*,*) is actually
c     # referenced with the corresponding offset added to delb.  in
c     # this version, this offset is determined from the range offset
c     # and from the reference occupation, imu, of the orbital.
c     #
c     # e.g. tab( pt(bcase,kcase,db0(r,imu)+delb), bket)
c
c**********************************************************
c     # w segment value pointers (assuming mu(*)=0):
      data w0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top segment value pointers:
      data rt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2 ,1 ,1 ,1,  2, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-bottom segment value pointers:
      data lb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  2, 1, 1, 1,  1,20, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1,  1, 1, 1, 1,  1, 1,18, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom segment value pointers:
      data rb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 2, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-top segment value pointers:
      data lt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 2, 1,  1, 1, 1,18,  1, 1, 1,20,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r segment value pointers:
      data r/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l segment value pointers:
      data l/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,33, 1, 1,  1,10, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3,11, 1,  1, 1,33, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=0):
      data wrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=0):
      data wrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=0):
      data ww0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=0):
      data wr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-top, x=0 segment value pointers:
      data rtrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  6, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-r-bottom,x=0  segment value pointers:
      data rbrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 6,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=0):
      data rtrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 2, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 2, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=0 segment value pointers:
      data rrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1, 54, 1, 1, 1,  1, 7, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 44, 1, 1, 1,  1, 1, 1, 1,  1, 1, 7, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=1 segment value pointers:
      data rrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 55, 1, 1, 1,  1,53,24, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 43, 1, 1, 1,  2, 1, 1, 1,  1,14,45, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=0 segment value pointers:
      data rrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,48,49, 1,  1, 1, 1, 7,  1, 1, 1, 7,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=1 segment value pointers:
      data rrb1/
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1,23,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,50,48, 1,  1, 1, 1,46,  1, 1, 1,52,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1,17,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=0 segment value pointers:
      data rr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=1 segment value pointers:
      data rr1/
     & 2, 1, 1, 1,  1, 2, 1, 1,  1,30,41, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,38,27, 1,  1,29,40, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,36,26, 1,  1, 1, 2, 1,  1, 1, 1, 2/
c**********************************************************
c     # r-top-l-bottom segment value pointers:
      data rtlb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 20, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 18, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l-top, x=1 segment value pointers:
      data rtlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,47, 2, 1,  1, 2,51, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-l-bottom, x=1 segment value pointers:
      data rblb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1,34, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,45, 1, 1,  1, 1,53, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1,31, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l, x=1 segment value pointers:
      data rtl1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 51, 1, 1, 1,  1,48,21, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 47, 1, 1, 1,  2, 1, 1, 1,  1,19,50, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-l-bottom, x=1 segment value pointers:
      data rlb1/
     & 1, 1, 1, 1,  1, 1, 1, 1, 34, 1, 1, 1,  1,25, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 45, 1, 1, 1, 53, 1, 1, 1,  1,50,48, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 31, 1, 1, 1,  1, 1, 1, 1,  1, 1,15, 1/
c**********************************************************
c     # r-l-top, x=1 segment value pointers:
      data rlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,53, 2, 1,  1, 1, 1,23,  1, 1, 1,56,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2,45, 1,  1, 1, 1,42,  1, 1, 1,17,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=0 segment value pointers:
      data rl0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=1 segment value pointers:
      data rl1/
     & 2, 1, 1, 1,  1,35, 1, 1,  1,13,35, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,37,28, 1,  1,28,39, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,32, 9, 1,  1, 1,32, 1,  1, 1, 1, 2/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=1):
      data w1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=2):
      data w2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=1):
      data ww1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=2):
      data ww2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 4, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=1):
      data wrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 58, 1, 1, 1, 58, 1, 1, 1,  1,66,67, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=2):
      data wrt2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  3, 1, 1, 1,  3, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=1):
      data wrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,58, 1, 1,  1, 1, 1, 1,  1, 1, 1,68,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1,58, 1,  1, 1, 1,65,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=2):
      data wrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=1):
      data wr1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=2):
      data wr2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 2, 1, 1,  1,60,35, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1,32,59, 1,  1, 1, 2, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottomsegment value pointers (assuming mu(*)=1):
      data rtrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,57, 1, 1,  1,64,70, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,69,63, 1,  1, 1,57, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=2):
      data rtrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1,62,34, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31,61, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c
c     # segment extension tables:
c     # casex(*,i) for i=-2 to 2 are extensions for occupation changes
c     #                     of i.
c     #                i=3 is a special case for diagonal extensions.
c
      data nex/ 1, 4, 6, 4, 1, 4/
*@ifdef reference
*C    # debug versions to agree with ancient ft program. -rls
*     data bcasex/
*     +   0, 0, 0, 0, 0, 0,
*     +   0, 0, 2, 1, 0, 0,
*     +   0, 1, 1, 2, 2, 3,
*     +   1, 2, 3, 3, 0, 0,
*     +   3, 0, 0, 0, 0, 0,
*     +   0, 1, 2, 3, 0, 0/
*     data kcasex/
*     +   3, 0, 0, 0, 0, 0,
*     +   1, 2, 3, 3, 0, 0,
*     +   0, 1, 2, 1, 2, 3,
*     +   0, 0, 1, 2, 0, 0,
*     +   0, 0, 0, 0, 0, 0,
*     +   0, 1, 2, 3, 0, 0/
*@else
      data bcasex/
     & 0, 0, 0, 0, 0, 0,
     & 2, 1, 0, 0, 0, 0,
     & 3, 2, 2, 1, 1, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
      data kcasex/
     & 3, 0, 0, 0, 0, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 2, 1, 2, 1, 0,
     & 2, 1, 0, 0, 0, 0,
     & 0, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
*@endif
c
c     irrep multiplication table.
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c     loop-value to ft-value transformation matrices.
c
      data vtran /
     & one,   zero,  zero,   one,
     & one,   zero,  halfm,  one,
     & one,   onem,  one,    one,
     & half,  zero,  zero,   one,
     & one,   zero,  halfm,  s32 /
c
      data vcoeff /
     & one,   half,  s32,    s34 /

      end



      subroutine smprep
c*****
c     computation of symmetry tables
c*****
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
C
C     Local variables
C
      INTEGER             I,           ICNT1,       ICNT2,       IJS
      INTEGER             IJSYM,       IS,          ISYM,        IX
      INTEGER             J,           JS,          JSYM,        NI
      INTEGER             NMBJ
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c
c   SYMTAB(*,*)   product of irreps table
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,       TOTSPC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c     KBL3,       unused ????
c     KBL4,       unused ????
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c      MAXBL3,    unused ????
c   MAXBL4,       unused ????
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c      MXKBL3,    unused ????
c     MXKBL4,     unused ????
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c       N0XTLP    unused ????
c      N1EXT,     number of one-external off-diagonal integrals
c      N1XTLP,    unused ????
c     N2EXT       number of two-external off-diagonal integrals
c       N2XTLP,   unused ????
c     N3EXT,      number of three-external off-diagonal integrals
c       N3XTLP,i  unused ????
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMBLK3     unused ????
c      NMBLK4     unused ????
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)

c
      do 10 i=1,nmsym
         ix=sym12(i)
         sym21(ix)=i
10    continue
      lamda1=0
      do 15 i=1,nmsym
         if(sym12(i).ne.1)go to 15
         lamda1=i
         go to 17
15    continue
17    continue
      do 22 i=1,nmsym
         nmbpr(i)=0
         do 20 j=1,nmsym
            lmda(i,j)=0
            lmdb(i,j)=0
20       continue
22    continue
c
c     lmdb(i,j),lmda(i,j)   symmetry pairs defined as
c     lmd(i)=lmdb(i,j)*lmda(i,j) , j=1,nmbpr(i)
c     lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1)
c     nmbpr(i)   number of pairs
c
      do 200 i=1,nmsym
         is=sym12(i)
         nmbj=0
         do 210 j=1,nmsym
            if(nbas(j).eq.0)go to 210
            js=sym12(j)
            ijs=symtab(is,js)
            ijsym=sym21(ijs)
            if(nbas(ijsym).eq.0)go to 210
            if(ijsym.gt.j)go to 210
            nmbj=nmbj+1
            lmdb(i,nmbj)=j
            lmda(i,nmbj)=ijsym
210      continue
         nmbpr(i)=nmbj
200   continue
c
      return
      end



      subroutine intab
c
c  initialize tables for segment value calculations.
c
c  for details of segment evaluation and the auxiliary functions,
c  see:
c        i. shavitt, in "the unitary group for the evaluation of
c        electronic energy matrix elements", j. hinze, ed.
c        (springer-verlag, berlin, 1981) p. 51. and i. shavitt,
c        "new methods in computational quantum chemistry and
c        their application on modern super-computers" (annual
c        report to the national aeronautics and space administration),
c        battelle columbus laboratories, june 1979.
c
c  mu-dependent segment evaluation entries added 7-nov-86 (rls).
c  this version written 01-aug-84 by ron shepard.
c
c
         IMPLICIT NONE

C---->Makedcls Options: All variables
C
C     Statement functions
C
      REAL*8              RX,          XA,          XB,          XC
      REAL*8              XD,          XF,          XR
C
C     Parameter variables
C
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
      REAL*8              HALF
      PARAMETER           (HALF = .5D0)
C
C     Local variables
C
      INTEGER             B,           I,           P,           Q
      INTEGER             R,           S
C
      REAL*8              SQRT2,       T
C
C     Common variables
C
      REAL*8              TAB(NTAB,0:MAXB)
C
      COMMON / CTAB   /   TAB
C
C
c**********************************************************************
c  table entry definitions:
c   1:0             2:1             3:-1            4:2
c   5:-2            6:sqrt2         7:-t            8:1/b
c   9:-sqrt2/b     10:1/(b+1)      11:-1/(b+1)     12:-1/(b+2)
c  13:-sqrt2/(b+2) 14:a(-1,0)      15:-a(-1,0)     16:a(1,0)
c  17:-a(1,0)      18:a(0,1)       19:-a(0,1)      20:a(2,1)
c  21:-a(2,1)      22:a(1,2)       23:-a(1,2)      24:a(3,2)
c  25:-a(3,2)      26:b(-1,0)      27:b(0,1)       28:-b(0,2)
c  29:b(1,2)       30:b(2,3)       31:c(0)         32:-c(0)
c  33:c(1)         34:c(2)         35:-c(2)        36:d(-1)
c  37:d(0)         38:-d(0)        39:d(1)         40:-d(1)
c  41:d(2)         42:ta(-1,0)     43:-ta(-1,0)    44:ta(1,0)
c  45:ta(2,0)      46:-ta(2,0)     47:-ta(-1,1)    48:ta(0,1)
c  49:ta(2,1)      50:-ta(2,1)     51:ta(3,1)      52:ta(0,2)
c  53:-ta(0,2)     54:ta(1,2)      55:ta(3,2)      56:-ta(3,2)
c         added for mu-dependent segment evaluation
c  57:1/2          58:-1/2         59:-1/b         60:1/(b+2)
c  61:(b+1)/b      62:(b+1)/(b+2)  63:(2b+1)/2b    64:(2b+3)/(2b+4)
c  65:1/2*a(1,0)   66:1/2*a(0,1)   67:1/2*a(2,1)   68:1/2*a(1,2)
c  69:1/2*c(0)     70:1/2*c(2)
c**********************************************************************
      rx(p)         = p
      xa(p,q,b)     = sqrt(rx(b+p) / rx(b+q))
      xb(p,q,b)     = sqrt(two / rx((b+p) * (b+q)))
      xc(p,b)       = sqrt(rx((b+p-1) * (b+p+1))) / rx(b+p)
      xd(p,b)       = sqrt(rx((b+p-1) * (b+p+2)) / rx((b+p) * (b+p+1)))
      xr(p,b)       = one / rx(b+p)
      xf(p,q,r,s,b) = rx(p*b+q) / rx(r*b+s)
c
      sqrt2 = sqrt(two)
      t     = one / sqrt2
c
      do 110 b = 0, maxb
         do 100 i = 1, ntab
            tab(i,b) = zero
100      continue
110   continue
c
      do 200 b = 0, maxb
         tab( 1,b) = zero
         tab( 2,b) = +one
         tab( 3,b) = -one
         tab( 4,b) = +two
         tab( 5,b) = -two
         tab( 6,b) = +sqrt2
         tab( 7,b) = -t
         tab(10,b) = +xr(1,b)
         tab(11,b) = -xr(1,b)
         tab(12,b) = -xr(2,b)
         tab(13,b) = -sqrt2*xr(2,b)
         tab(18,b) = +xa(0,1,b)
         tab(19,b) = -xa(0,1,b)
         tab(20,b) = +xa(2,1,b)
         tab(21,b) = -xa(2,1,b)
         tab(22,b) = +xa(1,2,b)
         tab(23,b) = -xa(1,2,b)
         tab(24,b) = +xa(3,2,b)
         tab(25,b) = -xa(3,2,b)
         tab(29,b) = +xb(1,2,b)
         tab(30,b) = +xb(2,3,b)
         tab(34,b) = +xc(2,b)
         tab(35,b) = -xc(2,b)
         tab(39,b) = +xd(1,b)
         tab(40,b) = -xd(1,b)
         tab(41,b) = +xd(2,b)
         tab(48,b) = +t * xa(0,1,b)
         tab(49,b) = +t * xa(2,1,b)
         tab(50,b) = -t * xa(2,1,b)
         tab(51,b) = +t * xa(3,1,b)
         tab(52,b) = +t * xa(0,2,b)
         tab(53,b) = -t * xa(0,2,b)
         tab(54,b) = +t * xa(1,2,b)
         tab(55,b) = +t * xa(3,2,b)
         tab(56,b) = -t * xa(3,2,b)
         tab(57,b) = +half
         tab(58,b) = -half
         tab(60,b) = +xr(2,b)
         tab(62,b) = +xf(1,1,1,2,b)
         tab(64,b) = +xf(2,3,2,4,b)
         tab(66,b) = +half * xa(0,1,b)
         tab(67,b) = +half * xa(2,1,b)
         tab(68,b) = +half * xa(1,2,b)
         tab(70,b) = +half * xc(2,b)
200   continue
c
      do 300 b = 1, maxb
         tab( 8,b) = +xr(0,b)
         tab( 9,b) = -sqrt2 * xr(0,b)
         tab(14,b) = +xa(-1,0,b)
         tab(15,b) = -xa(-1,0,b)
         tab(16,b) = +xa(1,0,b)
         tab(17,b) = -xa(1,0,b)
         tab(27,b) = +xb(0,1,b)
         tab(28,b) = -xb(0,2,b)
         tab(31,b) = +xc(0,b)
         tab(32,b) = -xc(0,b)
         tab(33,b) = +xc(1,b)
         tab(37,b) = +xd(0,b)
         tab(38,b) = -xd(0,b)
         tab(42,b) = +t * xa(-1,0,b)
         tab(43,b) = -t * xa(-1,0,b)
         tab(44,b) = +t * xa(1,0,b)
         tab(45,b) = +t * xa(2,0,b)
         tab(46,b) = -t * xa(2,0,b)
         tab(47,b) = -t * xa(-1,1,b)
         tab(59,b) = -xr(0,b)
         tab(61,b) = +xf(1,1,1,0,b)
         tab(63,b) = +xf(2,1,2,0,b)
         tab(65,b) = +half * xa(1,0,b)
         tab(69,b) = +half * xc(0,b)
300   continue
c
      do 400 b = 2, maxb
         tab(26,b) = +xb(-1,0,b)
         tab(36,b) = +xd(-1,b)
400   continue
c
      return
      end

      integer function procfunc(segtype,nproc,pairtype)
c
c     THIS RETURNS THE DEFAULT SEGMENTATION
c      MACHINE DEPENDENT !!!!!
c
c
       implicit none 
       integer segtype,nproc,pairtype
c     segtype: 1=z 2=y 3=x 4=w
c     nproc: number of processors
c     pairtype: 1 1v+1w
c               2 2v+2w
c

c
      real*8 scale1,scale2
      common /scale/ scale1,scale2
c
      if (segtype.eq.1) then
        procfunc=1
        return
      endif
      if (segtype.eq.2) then
        procfunc=1
        return
      endif
      if (segtype.eq.3.or.segtype.eq.4) then
c compute number of segments procfunc=nproc*scale1*0.5
c factor of 0.5 because x and w segments are treated separately
c J.Comp.Chem. 18,430(1997), Eq. 2.
        if (pairtype.eq.1) procfunc=int(dble(nproc)*
     &   scale1*0.5d+00+0.5d+00)
c compute number of segments:
c     procfunc=(-0.5+sqrt(0.25+2*scale2*nproc))*0.5
c J.Comp.Chem. 18,430(1997), Eq. 3.
        if (pairtype.eq.2) procfunc=int(
     &   (-0.5d+00 + sqrt(0.25+2.0d+00*scale2*dble(nproc)))*0.5d+00+
     &   0.5d+00)
c
        procfunc = max(procfunc,1)
c
        return
      endif
      call bummer_interact('invalid segtype in procfunc',segtype,2)
      return
      end


c     integer function improve( .... )

c
c
c      a) increase x and w segments up  by equal amounts
c
c         b) if length of a x or w segment is smaller than the y
c            segment increase the number of y segments by 1
c         c) if length of a x or w segment is smaller than the z
c            segment increase the number of z segments by 1
c
c      until maximum number of x,w,y,z segments is found or memory fits
c
c
c

        subroutine readinput
         implicit none
      integer nvmax
      parameter (nvmax=40)
C
C     Common variables
C
      INTEGER         CSFPRN,      DAVCOR,      FRCSUB
      INTEGER         FTCALC,      IBKTHV,      IBKTV,       ICITHV
      INTEGER         ICITV,       IORTLS,      ISTRT,       IVMODE
      INTEGER         NBKITR,      NCOUPLE,     NITER
      INTEGER         NO0EX,       NO1EX,       NO2EX,       NO3EX
      INTEGER         NO4EX,       NOLDHV,      NOLDV,       NRFITR
      INTEGER         NUNITV,      NVBKMN
      INTEGER         NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER         NVRFMX,      RTMODE,      VOUT
      INTEGER         NSEG0X(4),   NSEG2X(4)
      INTEGER         NSEG3X(4),   NSEG4X(4),   NODIAG
      integer         fileloc(7)
      integer             finalv,finalw
      integer  mode,ninitv,lvlprt,uciopt,nciitr,gset
      integer  skipso,aodrv,ncorel,iroot,iden,itran
C
      REAL*8            CTOL,G,      ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8            LRTSHIFT,RTOL(NVMAX),RTOLBK(NVMAX),RTOLCI(NVMAX)
c
      integer nroot,froot
      common/ciroot/nroot,froot
c

         integer i
         logical status

      INTEGER             C2EX0EX,     C3EX1EX,     CDG4EX
      INTEGER             MAXSEG
      INTEGER             NSEG1X(4) ,NSEGWX(4), NTYPE
      INTEGER             NPROC, molcas,NSEGD(4)
      INTEGER             nnseg0x(20),nnseg1x(20),nnseg2x(20),
     .  nnseg3x(20),nnseg4x(20),nnsegwx(20)

      COMMON / INDATA / C2EX0EX,     C3EX1EX,     CDG4EX, MAXSEG,
     &                   NSEG1X ,NSEGD, NTYPE, NPROC


      NAMELIST / INPUT  / LVLPRT,      NROOT,       NOLDV,       NOLDHV
     x,                   NUNITV,      NBKITR,      NITER,       DAVCOR
     x,                   CSFPRN,      ETOLBK,      ETOL,        CTOL
     x,                   UCIOPT,      IVMODE,      ISTRT,       VOUT
     x,                   IORTLS
     x,                   NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
     x,                   ICITV,       ICITHV,      FRCSUB,      NCIITR
     x,                   RTOLBK,      RTOL,        RTOLCI,      MODE
     x,                   NINITV,      NVBKMN,      NVCIMN,      MAXSEG
     x,                   NTYPE, GSET, G
     x,                   AODRV,       NRFITR,      NCOREL
     x,                   NVRFMX,      NVRFMN
     x,                   IDEN
     x,                   ITRAN,       IROOT,       FROOT,       RTMODE
     x,                   LRTSHIFT,    NCOUPLE,     SKIPSO
     x,                   nseg0x,nseg1x,nseg2x,nseg3x,nseg4x,nsegwx
     x,                   no0ex,no1ex,no2ex,no3ex,no4ex ,nodiag ,
     .                    molcas,cdg4ex,c3ex1ex,c2ex0ex,fileloc
     .                  , finalv,finalw,ftcalc, 
     .                    nnseg0x,nnseg1x,nnseg2x,nnseg3x,nnseg4x,
     .                    nnsegwx,fileloc

c
c  set defaults
c
        do i=1,4
          nsegd(i)=-1
          nseg0x(i)=-1
          nseg1x(i)=-1
          nseg2x(i)=-1
          nseg3x(i)=-1
          nseg4x(i)=-1
        enddo
        nroot=0
        froot=0
        inquire(file='ciudgin',exist=status)
        if (.not. status) goto 4
        open(unit=15,file='ciudgin')
        read(15,input,end=3,err=3)
 3      close(unit=15)
        do i=1,4
         nsegd(i)=max(nsegd(i),nseg4x(i))
         nseg1x(i)=max(nseg1x(i),nseg2x(i),nseg3x(i),nseg0x(i))
        enddo
        return
 4      continue
        call bummer_interact(' namelist input error in ciudgin',0,2)
        end

