!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       subroutine sif_energy(name,value,n,energy,ietype)
       implicit none
       integer i,itypea,itypeb,ierr,ietype(*), n
       character*(*) name
       real*8 value,energy(*)

       do i=len_trim(name)+1,8
           name(i:i)=' '
       enddo 

       call siftyp(itypea,itypeb,name,ierr)
       if (ierr.ne.0) then 
            write (6,*) 'name:',name,'#'
            call bummer('invalid name',ierr,2)
       endif 

       n=n+1
       energy(n)=value
       ietype(n)=itypea*1024+itypeb
       write(6,*) name,' >', itypea,itypeb,ietype(n)
       return
       end 
 
