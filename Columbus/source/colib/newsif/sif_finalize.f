!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine sif_finalize(fhdle)
      implicit none
      integer fhdle
      include 'sif_data.h'
c
c     purpose: finalize an open SIFS file
c              clean status%
c 

      close (unit=status%unitno(1,fhdle))
      if (status%info(1,fhdle).eq.2)
     .    close (unit=status%unitno(2,fhdle))
      
      status%unitno(1:2,fhdle)=-1
      status%nbft(fhdle)=-1
      status%nsym(fhdle)=-1
      status%nbpsy(1:8,fhdle)=-1
      status%info(1:7,fhdle)=-1
      return
      end

