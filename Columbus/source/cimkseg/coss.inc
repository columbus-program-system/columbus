!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             LCAVITY,     NN(3,MAXAT), NPPA,        NSPA
      INTEGER             NSPH
C
      REAL*8              AMPRAN,      AREA,        DISEX,       DISEX2
      REAL*8              EPS,         FEPSI,       FN2,         PHSRAN
      REAL*8              REFIND,      ROUTF,       RSOLV
      REAL*8              SRAD(MAXAT), TM(3,3,MAXAT),            VOLUME
C
      COMMON / COSS   /   EPS,         FEPSI,       DISEX,       DISEX2
      COMMON / COSS   /   RSOLV,       ROUTF,       NSPA,        NSPH
      COMMON / COSS   /   NPPA,        AREA,        REFIND,      FN2
      COMMON / COSS   /   LCAVITY,     SRAD,        TM,          NN
      COMMON / COSS   /   VOLUME,      PHSRAN,      AMPRAN
C
