!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      Subroutine pruneseg(sgtype,iseg,index,nwalko,fwalk,
     .  heap,maxfree,conft,indsym,nvwalk,nwalknew,
     .  bufsiz,indxlen)
c
c     input:  sgtype  (z(1),y(2),x(3),w(4))
c             iseg    segment number
c             index   index vector for this segment
c             nwalko  number of internal walks
c             fwalk   absolute number of first internal walk
c             heap    work space
c             maxfree free work space
c             conft   array mapping valid iwalk to csf number
c                     for this segment
c             indsym  array giving the symmetry of a valid iwalk
c             nvwalk  number of valid iwalks
c    output:  nwalknew new number of internal walks for this
c                      segment
c             indxlen  length of packed index vector
c             bufsiz   amount of work space used as write buffer
c                       in integer*4
c             heap(1:bufsiz) DRT info to be written to file
c
c    the calling routine must write the segment specific data
c    to a file
c

      implicit none
c
c     # Arguments:  sgtype  (1=z,2=y,3=x,4=w)
c                   iseg    segment number
c                   index   index vector for this segment
c                   nwalko  number of walks in this segment
c                   fwalk   offset of the index
c                   niot    number of internal orbitals
c

C====>Begin Module PRUNESEG               File pruneseg.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             BUFSIZ,      CONFT(*),    FWALK,       HEAP(*)
      INTEGER             INDEX(NWALKO),            INDSYM(*),   INDXLEN
      INTEGER             ISEG,        MAXFREE,     NVWALK
      INTEGER             NWALKNEW,    NWALKO,      SGTYPE
C
C     Local variables
C
      INTEGER             DRTMAP(4),   I,           IDRT,        INVALID
      INTEGER             J,           LNEW(0:3,NROWMX),         NWALKX
      INTEGER             PRINTLEVEL,  XBARNEW(NROWMX)
      INTEGER             YNEW(0:3,NROWMX)
C
      INCLUDE 'cciopt.inc'
C
C     Equivalenced common variables
C
      INTEGER             LVLPRT
C
      INCLUDE 'drt.inc'
      INCLUDE 'drtinfo.inc'
      INCLUDE 'solxyz.inc'
C====>End Module   PRUNESEG               File pruneseg.f               
      equivalence (lvlprt,uciopt(1))

C
      data drtmap /1,2,3,3/

c     bufsiz=(4+1+4)*nrow+indxlen+nvwalk*2+1
c
c     # select proper DRT from 3-DRT common
c
      call inisp
      idrt =drtmap(sgtype)
101   format(4('===='),a,i4,a,i4,a,4('===='))
      if (lvlprt.gt.4) write(6,101)
     .  ' pruneseg on segment ',iseg,'(',idrt,')'

c
c     # prune l(*,*,idrt) according to the index vector segment
c
      if (lvlprt.gt.4) then
      write(6,*) 'original DRT'
      call printstep(fwalk,sgtype,l(0,1,idrt),nrow,niot,
     .  y(0,1,idrt),0)
       call prcdrt(niot,nrow,nrowmx,nj,njskp,
     &             l(0,1,idrt),y(0,1,idrt),xbarcdrt(1,idrt))
      endif

      call lpruneseg(6,nwalko,index,y(0,1,idrt),
     .               l(0,1,idrt),xbarcdrt(1,idrt),lnew,spnorb,b,spnodd,
     .               fwalk,sgtype)


c
c     # from the lnew chaining indices create xbar and y
c
        call chainseg( niot, nj,  njskp,  lnew,
     &                 ynew,xbarnew, nrow,
     &                 nwalkx,spnorb, multp, nmul,spnodd,
     &                 nwalko)




      if (lvlprt.gt.4) then
        write(6,*) 'ynew(',idrt,'):'
        do i=0,3
         write(6,100) (ynew(i,j),j=1,nrow)
        enddo
100    format(20i3)
        write(6,*) 'xbarnew(',idrt,'):'
        write(6,100) (xbarnew(j),j=1,nrow)
      endif

c
c     # from limvec,y,xbar,l (old)
c     # and  ynew,xbarnew,lnew generate the new
c     # index vector - if possible we should do
c     # this inplace as the new index vector won't
c     # be larger than the old one in any case
c     # heap(1..nwalknew) llimnew
c

        call limnew (niot,nwalko,index,nrow,
     &               ynew,lnew,y(0,1,idrt),xbarcdrt(1,idrt),
     &               l(0,1,idrt),b,arcsym,
     &               sgtype,fwalk,xbarnew,heap,nwalknew,
     &               nvwalk,invalid,maxfree,indxlen)

      if (lvlprt.gt.4) then
      write(6,*) 'reduced DRT,invalid=',invalid
      call printstep(fwalk,sgtype,lnew,nrow,niot,ynew,invalid)
      endif
       if (lvlprt.gt.4) then
       call prcdrt(niot,nrow,nrowmx,nj,njskp,
     &             lnew,ynew,xbarnew)
       endif

c
c      # return lnew,ynew,xbarnew,llimnew via the heap
c      # to the calling subroutine which will write it
c      # via diracc to the direct access file
c
        call packbuffer(heap,maxfree,lnew,ynew,xbarnew,nrow,
     .                  nrowmx, index,indxlen,conft,
     .                  indsym, nvwalk,bufsiz,invalid)


        return
        end

        subroutine packbuffer( buffer,maxfree,l,y,xbar,nrow,nrowmx,
     .   index,indxlen,conft,indsym,nvwalk,bufsiz,invalid)
        implicit none
         integer buffer(*),maxfree,nvwalk,indsym(*),conft(*)
        integer nrow,nrowmx,indxlen,index(*)
        integer  l(0:3,nrowmx),xbar(nrowmx),y(0:3,nrowmx)
        integer bufsiz,off,iskip,i
        integer forbyt,invalid
        external forbyt

c
c #     calculate buffer size
c
        bufsiz=(4+1+4)*nrow+indxlen+nvwalk*2+1
        if (forbyt(bufsiz).gt.maxfree)
     .   call bummer('insufficient space in packbuffer',0,2)
c
c #     1. l  2. y  3. xbar 4. index 5. conft 6. indsym
c
        off=1
        call icopy_wr(4*nrow,l,1,buffer(off),1)
        off=off+4*nrow
        call icopy_wr(4*nrow,y,1,buffer(off),1)
        off=off+4*nrow
        call icopy_wr(nrow,xbar,1,buffer(off),1)
        off=off+nrow
        call icopy_wr(indxlen,index,1,buffer(off),1)
        off=off+indxlen
        call icopy_wr(nvwalk,conft,1,buffer(off),1)
c
c       # shift conft array to segment boundary
c       # i.e. conft(i) = conft(i)-conft(1)+1
c
        iskip=buffer(off)
        buffer(off)=0
        do i=off+1,off+nvwalk-1
         buffer(i)=buffer(i)-iskip
        enddo
c
        off=off+nvwalk
        call icopy_wr(nvwalk,indsym,1,buffer(off),1)
        off=off+nvwalk
        buffer(off)=invalid
        return
        end



        subroutine unpackbuffer( mode,buffer,l,y,xbar,
     .   index,indxlen,conft,indsym,nvwalk,bufsiz,invalid)
        implicit none
        integer nrowmx
        parameter (nrowmx=1023)

        integer buffer(*),nvwalk,indsym(*),conft(*)
        integer indxlen,index(*)
        integer  l(0:3,nrowmx),xbar(nrowmx),y(0:3,nrowmx)
        integer bufsiz,off,iskip,invalid
        integer  mode,ioff,i,j,val,curval
c
c #     1. l  2. y  3. xbar 4. index 5. conft 6. indsym
c
c       mode 0: just read indexvector and drt
c       mode 1: also indsym and conft
      integer nrow,niot
      COMMON /DRTINFO/nrow,niot
c
c
c
c  actual number of rows and internal orbitals
c  in (all) DRTs
c
c


        off=1
        call izero_wr(4*nrowmx,l,1)
        call icopy_wr(4*nrow,buffer(off),1,l,1)
        off=off+4*nrow
        call izero_wr(4*nrowmx,y,1)
        call icopy_wr(4*nrow,buffer(off),1,y,1)
        off=off+4*nrow
        call izero_wr(nrowmx,xbar,1)
        call icopy_wr(nrow,buffer(off),1,xbar,1)
        off=off+nrow
c
c       decode index vector
c
        ioff=0
        curval=0
        do i=1,indxlen
          val=buffer(off)
          if (val.lt.0) then
            do j=val,-1
             ioff=ioff+1
             index(ioff)=j
            enddo
          elseif (val.gt.0) then
            do j=1,val
            curval=curval+1
            ioff=ioff+1
            index(ioff)=curval
            enddo
         elseif (val.eq.0 .and. i.ne.indxlen) then
           call bummer('index unpacking error',0,2)
         endif
         off=off+1
        enddo
       if (mode.ne.0 ) then
        call icopy_wr(nvwalk,buffer(off),1,conft,1)
        off=off+nvwalk
        call icopy_wr(nvwalk,buffer(off),1,indsym,1)
        off=off+nvwalk
       else
         off=off+nvwalk+nvwalk
       endif
        invalid=buffer(off)
        return
        end



        subroutine printstep(fpth,sgtype,l,nrow,niot,y,invalid)
        implicit none
       integer nrowmx
      parameter (nrowmx = 1023)
      integer nimomx
      parameter (nimomx =128)


        integer istep,mul,nrow
        integer niot,rown,rowc,lpth,fpth,sgtype,clev,nlev
        integer row(0:nimomx),step(0:nimomx),ytot(0:nimomx)
        integer iwalk,l(0:3,nrowmx),i,y(0:3,nrowmx),invalid

        rown = 0
        rowc =0
c       lpth = fpth +nwalk-1

        clev =0
        row(0) = sgtype
        ytot(0) = 0
        do i=0,niot
          step(i) = 4
        enddo

100     continue
        nlev = clev +1
        istep = step(clev)-1
        if (istep.lt.0) then
            step(clev) =4
            if (clev.eq.0) goto 200
            clev = clev -1
            goto 100
        endif
        step(clev)=istep
        rowc = row(clev)
        rown = l(istep,rowc)
        if (rown.eq.0 ) goto 100
        ytot(nlev) = ytot(clev)+y(istep,rowc)
        row(nlev) = rown
        if (nlev.lt.niot) then
           clev = nlev
           goto 100
        else
          mul = 1
          iwalk = ytot(nlev)+1
          write(6,1000) iwalk-invalid,(step(i),i=0,niot-1)
1000    format('iwalk=',i4,'step:',20i2)
          goto 100
        endif
200     continue

        return
        end


        subroutine limnew(niot,nwalko,limvec,nrow,
     &   ynew,lnew,y,xbar,l,b,arcsym,
     &   segtype,fpth,xbarnew,limn,nwalknew,nvalw,invalid,mxfre,
     &   indxlen)
        implicit none

C====>Begin Module LIMNEW                 File limnew.f                 
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            FORBYT
C
      INTEGER             FORBYT
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX),   FPTH
      INTEGER             INDXLEN,     INVALID,     L(0:3,NROWMX)
      INTEGER             LIMN(*),     LIMVEC(*),   LNEW(0:3,NROWMX)
      INTEGER             MXFRE,       NIOT,        NROW,        NVALW
      INTEGER             NWALKNEW,    NWALKO,      SEGTYPE
      INTEGER             XBAR(NROWMX),             XBARNEW(NROWMX)
      INTEGER             Y(0:3,NROWMX),            YNEW(0:3,NROWMX)
C
C     Local variables
C
      INTEGER             CLEV,        CURVAL,      I,           IMUL
      INTEGER             IOFF,        ISSYM,       ISTEP,       ISYM
      INTEGER             IWALK,       IWALKN,      J,           JSKIP
      INTEGER             LPTH,        MUL,         NLEV,        NVWALK
      INTEGER             NWALK,       NXTVAL,      ROW(0:NIMOMX)
      INTEGER             ROWC,        ROWN,        SKIP
      INTEGER             STEP(0:NIMOMX),           WSYM(0:NIMOMX)
      INTEGER             YNTOT(0:NIMOMX),          YTOT(0:NIMOMX)
C
      INCLUDE 'cciopt.inc'
C
C     Equivalenced common variables
C
      INTEGER             LVLPRT
C
      INCLUDE 'csym.inc'
      INCLUDE 'solxyz.inc'
      equivalence (lvlprt,uciopt(1))
C====>End Module   LIMNEW                 File limnew.f                 

        j=0
        do i=1,nwalko
          if (limvec(i).gt.0) j=j+1
        enddo
        if (forbyt(j).gt.mxfre)
     .   call bummer('overflow of limn buffer',j,2)
        call izero_wr(j,limn,1)


c       write(6'(1x,a)')
c    .  'limnew: beginning the valid upper walk index recomputation...'

        lpth=fpth+nwalko-1
        do i=0,niot
          step(i)=4
        enddo

        iwalkn=0
        nvalw = 0
        clev = 0
        row(0) = segtype
        ytot(0) =0
        yntot(0) =0
        wsym(0) =1

c
c       # begin walk generation ...
c

100     continue
c       # decrement the step at the current level
        nlev = clev+1
        istep = step(clev)-1
        if (istep.lt.0) then
c         # decrement the current level
          step(clev) =4
          if (clev.eq.0) goto 180
          clev=clev-1
          goto 100
        endif
        step(clev)=istep
        rowc = row(clev)
        rown = l(istep,rowc)
c       # valid row in the old drt?
        if (rown.eq.0) goto 100
        ytot(nlev) = ytot(clev)+y(istep,rowc)
c
c       # here we start getting into the offset trouble
c       # yold still counts the full segment
c       # ynew only relative within the current segment
c       # so how to achieve consistency by using offsets???
         if ((ytot(nlev)+xbar(rown)).lt.fpth .or.
     .       (ytot(nlev)+1).gt.lpth) goto 100
          ioff= ytot(nlev)+1-fpth+1
c
c       # invalid walk
        if (-limvec(ioff).ge.xbar(rown)) goto 100
c       # cross check
        if (lnew(istep,rowc).eq.0)
     .    call bummer('limnew: error rowc=',rowc,2)
        wsym(nlev)=mult(wsym(clev),arcsym(istep,clev))
        row(nlev) =rown
        yntot(nlev) = yntot(clev)+ynew(istep,rowc)
        if (nlev.lt.niot) then
c
c        # decrement to the next level
c
          clev=nlev
          goto 100
        else
           mul = 1
           if (spnorb) mul = b(rown) +1
           iwalk = yntot(nlev)+1
c
c       #   if we have found the first valid walk number of
c       #   this segment in the NEW drt, we could offset
c       #   the arc weights at level zero by this amount
c       #   to force starting the index vector at this border
c       #   and avoid a number of zero entries at the beginning
c
c        # complete walk has been generated
c

           do imul = 1, mul
              isym=mult(wsym(nlev),spnir(imul,mul))
              issym=mult(isym,ssym)
              if (nexw(issym,segtype).ne.0) then
                 nvalw=nvalw+1
                 iwalkn=iwalkn+1
c # limn contains the internal walk number
                 limn(iwalkn)=iwalk
c             # possibly lim(iwalkn)=iwalk
c            write(66020) iwalkn,iwalk,(step(i),i=0,(niot-1))
6020     format(1x,2i8,(t20,50i1))
              endif
               iwalk=iwalk+1
            enddo
            goto 100
          endif
180       continue
c           write(6*) 'limnew:',nvalw,' valid walks'
c
c        # at this point contains limnew(1:iwalkn)=iwalkno (newdrt)
c        # now we know
c        #    1. the first 1..limn(1)-1 walks (newdrt) are invalid
c        #    2. limn(iwalkn) is the last valid walk
c        #    3. indexvector length: limn(iwalkn)-limn(1)+1

c        #   check whether new index vector length is shorter than
c        #   the old one
            nwalknew = limn(iwalkn)-limn(1)+1
c
c        # convert to 0 0 0 1 2 0 0 3 4 in limvec
       if (lvlprt.gt.4) then
         write(6,*) 'limn array=',iwalkn
         write(6,6200) (limn(i),i=1,iwalkn)
         write(6,*) 'limvec array=',nwalko
         write(6,6200) (limvec(i),i=1,nwalko)
6200     format(20i3)
       endif

            if (nwalko.lt.nwalknew)
     .        call bummer('index vector length increases ... ',
     .        nwalknew-nwalko,0)

         if (forbyt(nwalknew+1).gt.mxfre) then
           call bummer('insufficient space in pruneseg',nwalknew+1,2)
         endif
           call izero_wr(nwalknew,limvec,1)

c        #  save a segment specific index vector
c        #  running from limn(1) to limn(iwalkn)
c        #  with internal walk numbers counting from
c        #  1 through iwalkn
c        #  correspondingly the order of valid walks
c        #  has not changed and we save a index specific
c        #  copy of indsym(*) to the file
c        #  and in the same manner also a copy of conft
c        #  in the latter case we could also incorporate
c        #  the segment shift of configuration numbers and
c        #  hence, remove the offset manipulation and
c        #  checking
c
c        #  use length encoding for limvec right now
c        #  negative number: n consecutive invalid walks
c        #  positive number: n consecutive valid walks
c        #  0 : end of limvec
c
            invalid = limn(1)-1
            ioff = limn(1)-1
c        j points to the current position in limvec
c        skip number of consecutive valid walks
c        curval last number of valid walk
c        nxtval next number of valid walk

          j=0
          skip=1
           curval=limn(1)-ioff
           do i=2,iwalkn
             nxtval=limn(i)-ioff
             if (curval+1.eq.nxtval) then
                skip=skip+1
             else
                j=j+1
                limvec(j)=skip
                j=j+1
                limvec(j)=-(nxtval-curval-1)
                skip=1
             endif
           curval=nxtval
           enddo
          j=j+1
          limvec(j)=skip
          j=j+1
          limvec(j)=0
          indxlen=j

c        write(6*) 'runlen limvec=',indxlen
c        write(66200) (limvec(i),i=1,indxlen)

c        write(66200) (limvec(i),i=1,nwalknew)

c
c  the correspondingly changes to the y arrays is
c  carried out in wloop
c

            return
           end


c




      subroutine chainseg(
     & niot,   nj,     njskp,  l,
     & y,      xbar,   nrow,
     & nwalk,  spnorb, multp,
     & nmul,   spnodd ,seglen)
c
c  calculate the chaining information for the drt.
c  reverse lexical ordering is used.
c  pass only l,xbar,y, for this specific drt!
c
      implicit logical(a-z)
      integer nrowmx
      parameter (nrowmx = 1023)
      integer nimomx
      parameter (nimomx =128)

      integer niot,nrow, nwalk, drttyp
      integer vers(4)
      integer nj(0:nimomx), njskp(0:nimomx)
      integer l(0:3,nrowmx), y(0:3,nrowmx), xbar(nrowmx)
      integer seglen
      logical spnorb, spnodd
      integer nmul, imul
      integer multp(nmul)
      integer i, ver, irow, nlev, ilev, ir1, ir2, istep, lt

c
c        # initialize the top row.
c
         if(spnorb)then
c        call bummer('spnorb missing',0,2)
            do 1001 imul = 1, nj(niot)
               irow = njskp(niot)+imul
               xbar(irow) = multp(imul)
               y(0,irow)  = 0
               y(1,irow)  = 0
               y(2,irow)  = 0
               y(3,irow)  = 0
1001        continue
         else
            irow = njskp(niot)+1
            xbar(irow) = 1
            y(0,irow)  = 0
            y(1,irow)  = 0
            y(2,irow)  = 0
            y(3,irow)  = 0
         endif
c
c        # loop over all the lower rows.
c
         do 600 nlev = niot, 1, -1
            ilev = nlev-1
c
            ir1 = njskp(ilev)+1
            ir2 = njskp(ilev)+nj(ilev)
            do 400 irow = ir1, ir2
c
c              # y(3,*,*) = 0  for all rows; included only for indexing.
c
               y(3,irow) = 0
c
c              # loop over the remaining step numbers.
c
               do 200 istep = 2, 0, -1
                  lt = l(istep+1,irow)
                  if ( lt .ne. 0 ) then
                     y(istep,irow) = y(istep+1,irow)
     &                + xbar(lt)
                  else
                     y(istep,irow) = y(istep+1,irow)
                  endif
200            continue
c
               lt = l(0,irow)
               if(lt.ne.0)then
                  xbar(irow) = y(0,irow)+xbar(lt)
               else
                  xbar(irow) = y(0,irow)
               endif
c
400         continue
600      continue
1000  continue

c     write(6*)'chain: coded walks ',xbar(1)+xbar(2)+xbar(3)+xbar(4)
      return
      end




      subroutine lpruneseg(
     & nlist, nwalk,  limvec,
     & y, l,      xbar,  lnew,  spnorb,
     & b,      spnodd, fpth,segtype)

c
c  prune l(*,*,*) according to limvec(*) so that it results in
c  the smallest drt and the most compact indexing scheme.
c
c  input:
c  niot = total number of internal orbitals.
c  nwalk = total number of walks using the current indexing scheme.
c  limvec(1:nwalk) = 0/1 representation (current indexing scheme).
c  y(*),xbar(*),l(*) = current drt chaining indices.
c  ytot(*),step(*),yntot(*),row(*) = temp walk generation arrays.
c  fpth  = absolute number of first path on index
c  segtype (z,y,x,w=1,2,3,4)
c  output:
c  lnew(*) = pruned chaining array.
c
c  31-jul-89 ref(*) checks added for z-walks. -rls
c  05-jun-89 written by ron shepard.
c
      implicit none
       integer nrow,niot
      COMMON /DRTINFO/nrow,niot
c
c
c
c  actual number of rows and internal orbitals
c  in (all) DRTs
c
c

      integer nrowmx
      parameter (nrowmx = 1023)
      integer nimomx
      parameter (nimomx =128)


      integer nlist,nwalk
      integer limvec(nwalk)
      integer xbar(nrowmx), y(0:3,nrowmx), l(0:3,nrowmx)
      integer ytot(0:nimomx), yntot(0:nimomx), step(0:nimomx),
     .    row(0:nimomx)
      integer lnew(0:3,nrowmx)
      integer lpth ,fpth,segtype,ioff
      integer stack(0:nimomx,3)
*@ifdef spinorbit
      logical spnorb, spnodd
      integer b(*)
      integer imul, mul, iwalk,ii,jj
*@endif
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer  numv1,  nref,   nvalwt, ver,    ver1,   clev,   i,
     & j,nlev,   istep,  rowc,   rown,   nprune, irow
c
      integer    toskp,   to01
      parameter( toskp=0, to01=1 )


c
      rown = 0
      numv1 = 0
      rowc = 0
c
c     # initialize lnew(*)...
c     call izero_wr(4*nrowmx,lnew(0,1),1)
       do i=1,nrowmx
        do j=0,3
          lnew(j,i)=0
        enddo
       enddo

c pgf: the previous call does not initialize lnew properly
c     write(6,*) 'lprunenew: limvec(*) '
c     write(6,5000) (limvec(i),i=1,nwalk)
5000  format(20i4)


c
c     fpth = first path in index vector
c     lpth = last path in index vector
c
      lpth = fpth + nwalk-1
c     write(6*) 'lprune: fpth,lpth,nwalk=',fpth,lpth,nwalk

      nvalwt = 0
      ver = segtype
c     # cidrt indx01   limvec:  0 0 1 1 0 0 1
c     # cidrt skpx              2 1 0 0 2 1 0
c     # CI                     -2-1 1 2-2-1 3
c     # with marks             -2-1 2 2-2-1 2   2= on SOCI boundary
c                                               1= with SOCI multiplet
c
c
c        # begin walk generation for this vertex...
c
         clev = 0
         row(0) = ver
         ytot(0) = 0
c        call izero_wr(niot,row,1)
c        call izero_wr(niot,ytot,1)
c
         do 20 i = 0,niot
            step(i) = 4
20       continue
c
100      continue
c        # decrement the step at the current level.
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)goto 200
            clev = clev-1
            go to 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc)
         if(rown.eq.0)go to 100
         ytot(nlev) = ytot(clev)+y(istep,rowc)
c
c        # current path
c        # g�ltiger Bereich: ytot(nlev)+1 .. ytot(nlev)+xbar
c
         if ((ytot(nlev)+xbar(rown)).lt.fpth .or.
     .       (ytot(nlev)+1).gt.lpth) goto 100
          ioff= ytot(nlev)+1-fpth+1
         if(ver.eq.1)then
c           # z-walk; check both limvec(*) and ref(*).
            if( (-limvec(ioff) .ge. xbar(rown) )) goto 100
c           if( ( limvec(ytot(nlev)+1) .ge. xbar(rown) )
c    &       .and. ( ref(ytot(nlev)+1) .ge. xbar(rown) ))go to 100
         else
c           # w,x, or y walk.  just check limvec(*).
            if(-limvec(ioff) .ge. xbar(rown) )go to 100
         endif
c        # this step is used by at least one valid or reference walk.
         lnew(istep,rowc) = rown
         row(nlev) = rown
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         else
            mul = 1
            iwalk = ytot(nlev) + 1 -fpth +1
            if( spnorb )mul = b(rown) + 1
            do 179 imul = 1, mul
               if(limvec(iwalk) .gt. 0 )then
c         write(6,1000) iwalk,(step(i),i=0,niot-1)
1000    format('valid iwalk=',i4,'step:',20i2)
c                 # complete valid walk has been generated.
                  nvalwt = nvalwt+1
               endif
               iwalk = iwalk + 1
179         continue
c
c    # invalidate step
            go to 100
         endif
c
200   continue
c
c     # compute the number of pruned arcs.
         nprune = 0
         do 220 irow = 1,nrow
            do 210 istep = 0,3
               if (lnew(istep,irow).ne.l(istep,irow)) then
               if (l(istep,irow).eq.0) then
                write(6,*) 'ADDED arc (istep,nrow)=',istep,irow,
     .   'l=',l(istep,irow),'lnew=',lnew(istep,irow)
               call bummer('lprunenew: ADDING(!) arc',0,2)
                else
                if (lnew(istep,irow).eq.0) nprune = nprune+1
               endif
               endif
210         continue
220      continue
       write(nlist,'(/1x,a,i8,a,i8,1x,a,i8)')
     & 'lprune: l(*,*,*) pruned with nwalk=',nwalk,' nvalwt=',nvalwt
     &    ,'nprune=', nprune
c
      return
      end


c*deck inisp
      subroutine inisp
      implicit none

C====>Begin Module INISP                  File inisp.f                  
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Local variables
C
      INTEGER             I,           IMULT,       J,           JKSYM
      INTEGER             JSYM,        KSYM,        MS,          NJK
      INTEGER             NUMJ
C
      INCLUDE 'csym.inc'
      INCLUDE 'data.inc'
      INCLUDE 'solxyz.inc'
C====>End Module   INISP                  File inisp.f                  
c
c     # calculate the number of external walks of each symmetry
c     # for each vertex.
c
      call izero_wr(4*8,nexw,1)
c
c     # z-walks (1 effective walk of symmetry 1)...
      nexw(1,1) = 1
c
c     # y-walks...
      do 420 i = 1,8
         nexw(i,2) = nbas(i)
420   continue
c
c     # w,x -walks...
      do 440 jsym = 1,8
         numj = nbas(jsym)
         nexw(1,3) = nexw(1,3)+(numj*(numj-1))/2
         nexw(1,4) = nexw(1,4)+(numj*(numj+1))/2
         do 430 ksym = 1,(jsym-1)
            jksym = mult(ksym,jsym)
            njk = numj*nbas(ksym)
            nexw(jksym,3) = nexw(jksym,3)+njk
            nexw(jksym,4) = nexw(jksym,4)+njk
430      continue
440   continue


c
c     # initialize the spin function irrep and multp arrays
c
c
      nmul = 0
      do 5 j = 1, hmult, 2
         nmul = nmul +1
         multp(nmul) = j
5     continue

      do 10 i = 1, multmx
         do 11 j = 1, multmx
         spnir(i, j) = 0
11       continue
10    continue
c
c     # spin function irreps(integer spin)
c
c
c     # spin = 0, 2, 4, ...(even); multiplicity = 1, 5, 9, ...
c
c     write(6 *)' hmult', hmult
      do 6 imult = 1, multmx, 4
         spnir(1, imult) = 1
         do 7 ms = 2, imult, 4
            spnir(ms,   imult) = lxyzir(1)
            spnir(ms+1, imult) = lxyzir(2)
            spnir(ms+2, imult) = lxyzir(3)
            spnir(ms+3, imult) = 1
7        continue
6     continue

c
c     # spin = 1, 3, 5, ...(odd) ; multiplicity = 3, 7, 11, ...
c
      do 8 imult = 3, multmx, 4
         spnir(1, imult) = lxyzir(3)
         spnir(2, imult) = lxyzir(2)
         spnir(3, imult) = lxyzir(1)
         do 9 ms = 4, imult, 4
            spnir(ms,   imult) = 1
            spnir(ms+1, imult) = lxyzir(3)
            spnir(ms+2, imult) = lxyzir(2)
            spnir(ms+3, imult) = lxyzir(1)
9        continue
8     continue
c     write(6*)'lxyzir', lxyzir
c     write(6*)'spnir'
c     write(6"(19i3)")spnir
      return
      end


      subroutine prcdrt(
     & niot,    nrow,   nrowmx,
     & nj,      njskp,
     & l,       y,       xbar   )
c
c  print drt information from levels levprt(1) to levprt(2).
c
c  24-nov-90 chrprn added. -rls
c
      implicit logical(a-z)
c
      integer niot, nrow, nrowmx
      integer nj(0:niot),njskp(0:niot)
      integer l(0:3,nrowmx),xbar(nrow),y(0:3,nrow)
c
      integer l1, l2, ilev, ir1, ir2, irow, i

c
c     # restrict array references to a meaningful range.
c
      l1 = 0
      l2 = niot-1
c
c
      write(6,6010)l1,l2
c
      do 300 ilev = l2, l1, -1
          ir1 = njskp(ilev)+1
          ir2 = njskp(ilev)+nj(ilev)
          do 200 irow = ir1,ir2
              write(6,6030)irow,ilev,
     &          (l(i,irow),i=0,3),
     &          (y(i,irow),i=0,2),xbar(irow)
200       continue
          write(6,6050)
300   continue
c
      return
6010  format(' level',i3,' through level',i3,' of the drt:'//
     &  1x,'row',' lev',
     &  '  l0','  l1','  l2','  l3',
     &  '     y0 ','     y1 ','     y2 ','   xbar '/)
6030  format(1x,i3,i3,4i4,4i8)
6050  format(1x,36('.')/)
6060  format(a)
      end


        subroutine limcnvrt(nwalk,limvec,
     &   y,xbar,l,b,arcsym,segtype,fpth)
        implicit none
c
c   input:  niot  number of internal orbitals
c           nwalk number of internal walks
c           limvec() full index vector segment
c           nrow  number of rows in drt
c           y,xbar,l,b,arcsym: drt arrays
c           segtype z:0 y:1 x: 2 w: 3
c           fpth  first walk in index segment (absolute)
c
c   output limvec
c      valid segmentation breaks marked by 2, other valid walks by 1
c      invalids are negative, length unchanged
c

C====>Begin Module LIMCNVRT               File limcnvrt.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            FORBYT
C
      INTEGER             FORBYT
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX),   FPTH
      INTEGER             L(0:3,NROWMX),            LIMVEC(*),   NWALK
      INTEGER             SEGTYPE,     XBAR(NROWMX)
      INTEGER             Y(0:3,NROWMX)
C
C     Local variables
C
      INTEGER             CLEV,        I,           IMUL,        IOFF
      INTEGER             ISSYM,       ISTEP,       ISYM,        IWALK
      INTEGER             IWALKN,      J,           JSKIP,       LPTH
      INTEGER             MUL,         NLEV,        ROW(0:NIMOMX)
      INTEGER             ROWC,        ROWN,        SEGMARKS
      INTEGER             STEP(0:NIMOMX),           WSYM(0:NIMOMX)
      INTEGER             YTOT(0:NIMOMX)
C
      INCLUDE 'csym.inc'
      INCLUDE 'drtinfo.inc'
      INCLUDE 'gapointer.inc'
      INCLUDE 'solxyz.inc'
C====>End Module   LIMCNVRT               File limcnvrt.f               
        lpth=fpth+nwalk-1
        do i=0,niot
          step(i)=4
        enddo

        clev = 0
        row(0) = segtype
        ytot(0) =0
        wsym(0) =1
        segmarks=0
        iwalkn=0

c
c       # begin walk generation ...
c

100     continue
c       # decrement the step at the current level
        nlev = clev+1
        istep = step(clev)-1
        if (istep.lt.0) then
c         # decrement the current level
          step(clev) =4
          if (clev.eq.0) goto 180
          clev=clev-1
          goto 100
        endif
        step(clev)=istep
        rowc = row(clev)
        rown = l(istep,rowc)
c       # valid row in the old drt?
        if (rown.eq.0) goto 100
        ytot(nlev) = ytot(clev)+y(istep,rowc)
         if ((ytot(nlev)+xbar(rown)).lt.fpth .or.
     .       (ytot(nlev)+1).gt.lpth) goto 100
          ioff= ytot(nlev)+1
c
c       # invalid walk
        if (-limvec(ioff).ge.xbar(rown)) goto 100
c       # cross check
        wsym(nlev)=mult(wsym(clev),arcsym(istep,clev))
        row(nlev) =rown
        if (nlev.lt.niot) then
c
c        # decrement to the next level
c
          clev=nlev
          goto 100
        else
           mul = 1
           if (spnorb) mul = b(rown) +1
           iwalk = ytot(nlev)+1
c
c        # complete walk has been generated
c

           do imul = 1, mul
              isym=mult(wsym(nlev),spnir(imul,mul))
              issym=mult(isym,ssym)
              if (nexw(issym,segtype).ne.0) then
                 if (imul.eq.1) then
                    limvec(iwalk)=2
                    segmarks=segmarks+1
                 else
                    limvec(iwalk)=1
                 endif
                 iwalkn=iwalkn+1
c            write(66020) iwalkn,iwalk,(step(i),i=0,(niot-1))
6020     format(1x,2i8,(t20,50i1))
              endif
               iwalk=iwalk+1
            enddo
            goto 100
          endif
180       continue
         if (me.eq.0 .and. segmarks.ne.iwalk)
     .    write(6,*) 'limcnvrt: found',iwalkn,' valid internal walks',
     .   'out of ',iwalk-fpth,' walks (skipping trailing invalids)'
          write(6,*) ' ... adding ',segmarks,' segmentation marks',
     .    ' segtype=',segtype
          return
          end
