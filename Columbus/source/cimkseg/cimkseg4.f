!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine prep1
c
         IMPLICIT NONE

C====>Begin Module PREP1                  File prep1.f                  
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
C
C     Local variables
C
      INTEGER             I,           TASKID
C
      INCLUDE 'cciopt.inc'
C
C     Equivalenced common variables
C
      INTEGER             LVLPRT
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             TAPE6
C
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
C
C     Common variables
C
      INTEGER             BLXT(MAXSYM),             N2OFFR(MAXSYM)
      INTEGER             N2OFFS(MAXSYM),           N2ORBR,      N2ORBT
      INTEGER             NOFFR(MAXSYM),            NOFFS(MAXSYM)
      INTEGER             NSGES(MAXSYM)
C
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR,      N2ORBR
C
C====>End Module   PREP1                  File prep1.f                  
C
      equivalence (tape6,nunits(1))
      equivalence (lvlprt,uciopt(1))

C HPM  subroutine_start
c       call _f_hpm_start_(14,4790, "pciudg4.f","prep1")
C HPM
      mxorb = nbas(1)
      blxt(1) = 0
      do 10 i = 2, nmsym
         blxt(i) = blxt(i-1) + nbas(i-1)
         mxorb = max( mxorb, nbas(i) )
10    continue
c
      if ( lvlprt .ge. 2 ) write(tape6,30000) mxorb
c
C HPM   return
c       call _f_hpm_stop_(14,4802)
C HPM
      return
30000 format(' mxorb=',i4)
      end
      subroutine setref(
     & ref,    reflst, nzwalk, nvalz,
     & limvec, nlist,  lvlprt )
c
c  this routine combines the 0/1 ref(*) vector and the z-walk limvec(*)
c  vector and computes reflst(*).
c
c  input:
c  ref(1:nzwalk) = 0/1 representation of the reference list.
c  nzwalk = total number of z-walks in the DRT.
c  nvalz  = number of valid z-walks in the DRT.
c  limvec(1:nzwalk) = -1/icsf representation of the valid z-walks.
c  nlist = output listing file.
c  lvlprt = print flag.
c
c  output:
c  reflst(1:nvalz) = -1     for valid nonreference walks.
c                  = iref   for valid reference walks.
c  i.e. for zsegment: reflst(icsf)=iref or -1

c
c  10-dec-90  boolst(*) removed, ref(*) added. -rls
c  03-nov-89  nlist, lvlprt added to the argument list.
c             numrem added. -rls
c
         IMPLICIT NONE

C====>Begin Module SETREF                 File setref.f                 
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             LIMVEC(*),   LVLPRT,      NLIST,       NVALZ
      INTEGER             NZWALK,      REF(*),      REFLST(*)
C
C     Local variables
C
      INTEGER             I,           IPTR,        IRFCNT,      NUMREM
      INTEGER             TASKID
C
      INCLUDE 'gapointer.inc'
C====>End Module   SETREF                 File setref.f                 
      numrem = 0
      iptr   = 0
      irfcnt = 0
c     # loop over all zpaths in the limvec vector.
      do 100 i = 1, nzwalk
         if (limvec(i).gt.0) then
c           # valid walk.
            iptr = iptr + 1
            if (iptr .gt. nvalz .and. me.eq.0 ) then
               call bummer('setref: nvalz exceeded',iptr,faterr)
            endif
c           # check if it is a reference as indicated by the drt
            if ( ref(i) .eq. 1 ) then
               irfcnt       = irfcnt + 1
               reflst(iptr) = irfcnt
            else
               reflst(iptr) = -1
            endif
         else
c           # invalid walk.
            if ( ref(i) .eq. 1 ) then
               numrem = numrem + 1
               if ( lvlprt .ge. 2 .and.me.eq.0 ) then
c                 # this was made optional to eliminate several pages of
c                 # output from listing files. -rls
                  write(nlist,'(1x,a,i8,a,i8)') 'zpath ', i,
     +             ' marked invalid by limvec as reference', (irfcnt+1)
               endif
            endif
         endif
100   continue
      if (me.eq.0) then
      write(nlist,6010) irfcnt, numrem, (irfcnt+numrem)
      if (iptr .ne. nvalz) then
         call bummer('setref: nvalz not met',iptr,faterr)
      endif
c     write(6,*) 'reflst array in setref:'
c     call iwritex(reflst,nvalz)
      endif


c
C HPM   return
c       call _f_hpm_stop_(15,4928)
C HPM
      return
c
6010  format(/' setref:',t10,i8,' references kept,'
     & /t10,i8,' references were marked as invalid, out of'
     & /t10,i8,' total.')
      end
      subroutine smprep
c*****
c     computation of symmetry tables
c*****
         IMPLICIT NONE

C====>Begin Module SMPREP                 File smprep.f                 
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
C
C     Local variables
C
      INTEGER             I,           ICNT1,       ICNT2,       IJS
      INTEGER             IJSYM,       IS,          ISYM,        IX
      INTEGER             J,           JS,          JSYM,        NI
      INTEGER             NMBJ,        TASKID
C
      INCLUDE 'cciopt.inc'
C
C     Equivalenced common variables
C
      INTEGER             LVLPRT
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             TAPE6
C
      INCLUDE 'csym.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
C====>End Module   SMPREP                 File smprep.f                 
      equivalence (tape6,nunits(1))
      equivalence (lvlprt,uciopt(1))
c
C HPM  subroutine_start
c       call _f_hpm_start_(16,5164, "pciudg4.f","smprep")
C HPM
      do 10 i=1,nmsym
         ix=sym12(i)
         sym21(ix)=i
10    continue
      if(lvlprt.eq.2)
     + write(tape6,30030)(nbas(i),i=1,nmsym)
      if(lvlprt.eq.2)
     + write(tape6,30030)(sym12(i),i=1,nmsym)
      if(lvlprt.eq.2)
     + write(tape6,30030)(sym21(i),i=1,nmsym)
      lamda1=0
      do 15 i=1,nmsym
         if(sym12(i).ne.1)go to 15
         lamda1=i
         go to 17
15    continue
17    continue
      if(lvlprt.eq.2)
     + write(tape6,30050)lamda1
30050 format(' lamda1',i4)
      do 22 i=1,nmsym
         nmbpr(i)=0
         do 20 j=1,nmsym
            lmda(i,j)=0
            lmdb(i,j)=0
            strtbx(i,j)=0
            strtbw(i,j)=0
20       continue
22    continue
c
c     lmdb(i,j),lmda(i,j)   symmetry pairs defined as
c     lmd(i)=lmdb(i,j)*lmda(i,j) , j=1,nmbpr(i)
c     lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1)
c     nmbpr(i)   number of pairs
c
      do 200 i=1,nmsym
         is=sym12(i)
         nmbj=0
         do 210 j=1,nmsym
            if(nbas(j).eq.0)go to 210
            js=sym12(j)
            ijs=mult(is,js)
            ijsym=sym21(ijs)
            if(nbas(ijsym).eq.0)go to 210
            if(ijsym.gt.j)go to 210
            nmbj=nmbj+1
            lmdb(i,nmbj)=j
            lmda(i,nmbj)=ijsym
210      continue
         nmbpr(i)=nmbj
200   continue
      if(lvlprt.eq.2)
     + write(tape6,30010)
30010 format(' lmda,lmdb')
      do 40000 i=1,nmsym
         if(lvlprt.eq.2) write(tape6,30000)(lmdb(i,j),lmda(i,j),j=1,
     +    nmsym)
30000    format(1x,8(2i4,2x))
40000 continue
      if(lvlprt.eq.9)
     + write(tape6,30030)(nmbpr(i),i=1,nmsym)
c
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c
      do 300 i=1,nmsym
         nmbj=nmbpr(i)
         if(nmbj.eq.0)go to 300
         icnt1=0
         icnt2=0
         do 310 j=1,nmbj
            jsym=lmdb(i,j)
            isym=lmda(i,j)
            if(isym.eq.jsym)go to 320
            strtbw(jsym,i)=icnt1
            strtbx(jsym,i)=icnt1
            icnt1=icnt1+nbas(isym)*nbas(jsym)
            go to 310
320         continue
            ni=nbas(isym)
            strtbw(isym,i)=icnt1
            strtbx(isym,i)=icnt2
            icnt1=icnt1+ni*(ni+1)/2
            icnt2=icnt2+(ni-1)*ni/2
310      continue
300   continue
      if(lvlprt.eq.9)
     + write(tape6,30020)
30020 format(' strtbx')
      do 40010 i=1,nmsym
         if(lvlprt.eq.2)
     +    write(tape6,30030)(strtbx(i,j),j=1,nmsym)
40010 continue
30030 format(1x,20i6)
      if(lvlprt.eq.2)
     + write(tape6,30040)
30040 format(' strtbw')
      do 40020 i=1,nmsym
         if(lvlprt.eq.2)
     +    write(tape6,30030)(strtbw(i,j),j=1,nmsym)
40020 continue
c
C HPM   return
c       call _f_hpm_stop_(16,5270)
C HPM
      return
      end
      subroutine tapin( core, maxbuf, reflst, limvec, indsym,me )
c
c  29-jan-92 added momap common block -eas
c  10-dec-90 /csife/ added. nucrep, refen removed. reflst,
c            limvec, and indsym pointers returned. -rls
c  03-nov-89 ntitle added back to sort info file, filsti. -rls
c
         IMPLICIT NONE

C====>Begin Module TAPIN                  File tapin.f                  
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            ATEBYT,      CALCTHRXT,   FORBYT
      EXTERNAL            SIFSCE
C
      INTEGER             ATEBYT,      CALCTHRXT,   FORBYT
C
      REAL*8              SIFSCE
C
C     Parameter variables
C
      INTEGER             NWLKMX
      PARAMETER           (NWLKMX = 2**20-1)
      INTEGER             NTITMX
      PARAMETER           (NTITMX = 30)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NSIFMX
      PARAMETER           (NSIFMX = 30)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             READC
      PARAMETER           (READC = 1)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             INDSYM,      LIMVEC,      MAXBUF,      ME
      INTEGER             REFLST
C
      REAL*8              CORE(*)
C
C     Local variables
C
      CHARACTER*80        DRTTIT
C
      INTEGER             EWLK,        I,           IAB,         IBUF
      INTEGER             IERR,        INT,         INTBL,       J
      INTEGER             LENBUF,      MODE,        NCIITR
      INTEGER             NCSFV(4),    NFVT,        NINITV,      NROW
      INTEGER             NTITXX,      NVWALK(4),   NVWXY,       NWALK
      INTEGER             REF,         STWLK,       TASKID,      TOTX
      INTEGER             VRSION,      XBAR(4)
C
      REAL*8              RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8              RTOLCI(NVMAX)
C
      INCLUDE 'cciopt.inc'
C
C     Equivalenced common variables
C
      INTEGER             IDEN,        LVLPRT
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILDRT,      FILIND,      FILLPI,      FILSTI
      INTEGER             NSLIST,      TAPE6
C
      INCLUDE 'cli.inc'
      INCLUDE 'cntitl.inc'
      INCLUDE 'csife.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'ctitle.inc'
      INCLUDE 'data.inc'
      INCLUDE 'drt.inc'
      INCLUDE 'indata.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'momap.inc'
      INCLUDE 'rep.inc'
      INCLUDE 'slabel.inc'
      INCLUDE 'soinf.inc'
      INCLUDE 'solxyz.inc'
C====>End Module   TAPIN                  File tapin.f                  
      equivalence (lvlprt, uciopt(1))
      equivalence (iden, uciopt(2))
      equivalence (nunits(1),tape6)
      equivalence (nunits(3),nslist)
      equivalence (nunits(8),filind)
      equivalence (nunits(13),fillpi)
      equivalence (nunits(10),fildrt)
      equivalence (nunits(39),filsti)


c     # repnuc = nuclear_repulsion + frozen_core + repartitioning energy
C     Common variables
c
c     # sifsce() = computes the total core energy from a list of
c     #            partial contributions.
c
c  maxbl2  --  maximum number of two electron integrals to be held
c              in core for 2 external case
c
c  maxbuf  --  size of integral buffers for fourex and threx
c
c  minbl4  --  minimum nuber of two electron integrals to be held
c              in core for 4 external case
c
c  minbl3  --  minimum nuber of two electron integrals to be held
c              in core for 3 external case
c
c  n#ext   --  number of off diagonal two electron integrals
c              #  varies from 0 to 4 external
c
c  nd#ext  --  number of diagonal two electron integrals
c              #  =  0, 2, or 4 external
c
c
c     # read the header information from the ft info file.
c
c     # read the first several records of cidrtfl to get the spin-orbit
c     # status
      rewind fildrt
C HPM  subroutine_start
c       call _f_hpm_start_(17,5877, "pciudg4.f","tapin")
C HPM
      read(fildrt,'(a)',iostat=ierr)drttit
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
      read(fildrt,*,iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
ctm
      if (spnorb) then
        iden=0
        call bummer(' calculation: setting iden=0',0,0)
      endif
c
c  nvalwk: number of valid internal paths
c
c  nvalw,nvalx,nvaly,nvalz : number of valid paths through w,x,y, and z
c
c
c     # read the prep3e.dat file.
c
c"iblock,l1,ksym,k1s,k1f"
c    1     1     1     1    16
c    2     1     2     1     9
c       ............             
c"ndg4ext ndg2ext ndg0ext"
c   1482     456      42
c"n4ext n3ext n2ext n1ext n0ext n2extso n1extso n0extso"
c  79122   47394   13251    2040     138       0       0       0
c"intmxo mx4x mx3x mx2x maxbuf"
c     4095       705       705       708     32767
c"number of external orbitals"
c     16       9       4       9
c" diagint ofdg fil3x fil3w fil4w fil4x (DP)"
c    16380     28665     65534     65534     98301     65534

      open(unit=71,file='prep3e.dat',form='formatted')
199   read (71,'(a50)',err=200,end=201) drttit 
      if (drttit(2:8).eq.'ndg4ext') then
          read(71,*) nd4ext,nd2ext,nd0ext
      endif
      if (drttit(2:6).eq.'n4ext') then
          read(71,*) n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int
      endif
      if (drttit(2:7).eq.'number') then
          read(71,*) (nbas(i),i=1,8)
      endif
      if (drttit(2:7).eq.'intmxo') then
         read(71,*) bufszi,minbl4,minbl3,maxbl2,maxbuf
      endif 
200   goto 199 
201   close(71)     
c
c     # read the final record containing i/o parameters.
c
      int   = max( nd4ext, nd2ext, nd0ext )
      intbl = (int - 1) / bufszi + 1
      mxbld = intbl * bufszi
c
c     # read the header information from the drt file.
c
      ntitle = ntitle + 1
ctm?? ibuf not yet set, hope that's right
      ibuf=1

c
      call rdhdrt(
     & title(ntitle),  nmot,       niot,   nfct,
     & nfvt,           nrow,       nsym,   ssym,
     & xbar,           nvwalk,     ncsfv,  lenbuf,
     & nwalk,          nimomx,     nmomx,  nrowmx,
     & nwlkmx,         core(ibuf), tape6,  fildrt,
     & ftcalc,         spnorb,     spnodd, lxyzir,hmult )
c
      pthz=xbar(1)
      pthy=xbar(2)
      pthx=xbar(3)
      pthw=xbar(4)
      nvalz=nvwalk(1)
      nvaly=nvwalk(2)
      nvalx=nvwalk(3)
      nvalw=nvwalk(4)
      nintpt=pthz+pthy+pthx+pthw
      nvalwk=nvalz+nvaly+nvalx+nvalw
      nmsym=nsym
*@ifdef spinorbit
       intorb=niot
       if(spnodd)intorb=niot-1
*@endif
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c
c     # mnbas = maximum number of external basis functions in any block.
c
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      mnbas = nbas(1)
      do 20 i = 2, nmsym
         mnbas = max( mnbas, nbas(i) )
20    continue

      write(tape6,130) nvalwk, nvalz, nvaly, nvalx, nvalw
  130 format(' total number of valid internal walks:',i8/
     +' nvalz,nvaly,nvalx,nvalw = ',4i8)
      write(tape6,20030) filind, bufszi, mxbld, nd4ext, nd2ext,
     & nd0ext, n4ext, n3ext, n2ext, n1ext, n0ext,
     & n2int, n1int, n0int,
     & minbl4, minbl3, maxbl2, maxbuf
20030 format(/' cisrt info file parameters:'/
     & ' file number',i4,' blocksize ',i6/' mxbld ',i6/
     & ' nd4ext,nd2ext,nd0ext',3i6/
     & ' n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int',8i9/
     & ' minbl4,minbl3,maxbl2',3i6/
     & ' maxbuf',i6)
      write(tape6,20060) (nbas(i),i=1,nmsym)
c
20060 format(' number of external orbitals per symmetry block:',8i4)

c
c
c     # core(*)--> 1:reflst(1:nvalz), 2:limvec(1:nintpt),
c     #            3:indsym(1:nvalwk), 4:ibuf
      reflst  = 1
      limvec  = reflst + forbyt( nvalz )
      indsym  = limvec + forbyt( nintpt )
      ibuf    = indsym + forbyt( nvalwk )
c
c     # allow for some scratch space.
      totx = ibuf   + forbyt( 30 )
      if( totx . gt. totspc ) then
c        # 30 integers is more than enough to read the header, but we
c        # should have more space than this anyway. -rls
         call bummer('tapin: rdhdrt, (totx-totspc)=',(totx-totspc),
     &    faterr)
      endif
c
      write(tape6,110) nmsym, intorb
  110 format(' nmsym',i4,' number of internal orbitals',i4)
      write(tape6, 6100)'formula file title:', (title(i),i=1,ntitle)
c     write(nslist,6100)'formula file title:', (title(i),i=1,ntitle)
      write(tape6,20050) fillpi, bufszl, pthz, pthy, pthx, pthw,
     & nintpt, maxlp3,  n2lp,   n1lp,   n0lp
20050 format(' file nmb.',i4,' block size',i6/
     +' pthz,pthy,pthx,pthw:',4i6,' total internal walks:',i8
     +/' maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp',8i6)
c
c     # eliminate duplicate titles.
      call unique( ntitle, title )
c
      ref   = ibuf + forbyt(lenbuf)
      iab   = ref  + forbyt( pthz )
      totx  = iab  + forbyt( nrow )
      if( totx . gt. totspc ) then
         call bummer('tapin: rddrt, (totx-totspc)=',(totx-totspc),
     &    faterr)
      endif
      call inisp
c
c     # read the drt arrays.
c
      nvwxy = nvwalk(2) + nvwalk(3) + nvwalk(4)
c
      call rddrt(
     & nmot,       niot,       nsym,       nrowmx,
     & nrow,       nwalk,      lenbuf,     pthz,
     & nvwxy,      map,        bord,       bsym,       core(iab),
     & b,          l,          y,          core(ref),
     & labels,     core(ibuf), fildrt,
     & xbarcdrt,   mu,         nj,         njskp,
     & core(limvec),core(indsym),arcsym,ssym,mult)

c
      do i=1,intorb
      orbsym(i) = bsym(i)
      enddo
c
c     maxlp3  = maximum number of loops per l value  needed for the
c               three external formula tape
c

      maxlp3=calcthrxt(core(limvec))


c     set loop buffer size to 0 if calculating formula tape on the
c     fly

      if (ftcalc.eq.1) bufszl=0
      write(tape6,120)(orbsym(i),i=1,intorb)
  120 format(' orbsym(*)=',40i2)

c
c
c     # compute the reflst(*) reference csf mapping vector.
c
      call setref(
     & core(ref),    core(reflst), pthz,  nvalz,
     & core(limvec), tape6,        lvlprt )
c
c     # all done with the cidrt file.
      close(unit=fildrt)
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c    mark possible segmentation breaks in index vector
c
c    # z walks
      stwlk=1
      ewlk=xbar(1)
      call limcnvrt(xbar(1),core(limvec),
     &   y(0,1,1),xbarcdrt(1,1),l(0,1,1),b,arcsym,1,1)

c    # y walks
      stwlk=ewlk+1
      ewlk=ewlk+xbar(2)
      call limcnvrt(xbar(2),core(limvec),
     &   y(0,1,2),xbarcdrt(1,2),l(0,1,2),b,arcsym,2,stwlk)

c    # x walks
      stwlk=ewlk+1
      ewlk=ewlk+xbar(3)
      call limcnvrt(xbar(3),core(limvec),
     &   y(0,1,3),xbarcdrt(1,3),l(0,1,3),b,arcsym,3,stwlk)

c    # w walks
      stwlk=ewlk+1
      ewlk=ewlk+xbar(4)
      call limcnvrt(xbar(4),core(limvec),
     .  y(0,1,3),xbarcdrt(1,3),l(0,1,3),b,arcsym,4,stwlk)
c
c
      if (spnodd) niot=niot-1
C HPM   return
c       call _f_hpm_stop_(17,6122)
C HPM
      return
6100  format(/1x,a/(1x,a))
      end
      subroutine upwkst(
     & hilev, niot,
     & rwlphd, yblp,   yklp, clev )
c
c  initialize upper walk variables in /walksv/ and /drt/.
c
         IMPLICIT NONE

C====>Begin Module UPWKST                 File upwkst.f                 
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
C
C     Argument variables
C
      INTEGER             CLEV,        HILEV,       NIOT,        RWLPHD
      INTEGER             YBLP,        YKLP
C
C     Local variables
C
      INTEGER             I,           TASKID
C
      INCLUDE 'drt.inc'
      INCLUDE 'walksv.inc'
C====>End Module   UPWKST                 File upwkst.f                 
c
c some save space used during the generation of upper walk
c extensions
c

      save   /walksv/
c
C HPM  subroutine_start
c       call _f_hpm_start_(18,6205, "pciudg4.f","upwkst")
C HPM
      do 10 i = hilev, niot
         case(i) = 4
10    continue
      clev      = hilev
      yk(clev)  = yklp
      yb(clev)  = yblp
      row(clev) = rwlphd
c
      clevc   = clev
      hilevc  = hilev
      niotc   = niot
c
C HPM   return
c       call _f_hpm_stop_(18,6220)
C HPM
      return
      end
      subroutine wcode_dir(ibf,code,icodev,iposition,break2)
c
         IMPLICIT NONE

C====>Begin Module WCODE_DIR              File wcode_dir.f              
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
C
C     Argument variables
C
      INTEGER             CODE,        IBF,         ICODEV(*)
      INTEGER             IPOSITION
C
      LOGICAL             BREAK2
C
C     Local variables
C
      INTEGER             TASKID
C
      INCLUDE 'lcontrol.inc'
C====>End Module   WCODE_DIR              File wcode_dir.f              
      break2 = .false.
      ibf = ibf + 1
c
      if (ibf.eq.maxbf-1) then
c
      jumpinposition = iposition
c    #  code end of record
      icodev(ibf) = 1
      break2 = .true.
c
      else
c
c    #  if enough space add next loop type code
      icodev(ibf) = code
c
      endif
c
c HPM   return
c       call _f_hpm_stop_(19,6288)
C HPM
      return
      end
      subroutine multnxnew(
     & reflst, conftbra, indsymbra,
     & indxbra,  cibra,    sgmabra,
     & conftket, indsymket,indxket,ciket, sgmaket,
     & sglen,  sgnci,  sgcist, sgcsf,
     & sgncsf, sgpath, sginfo, times,
     & iskip,  heap,   totfre, sgscr,
     & ntask,task  )

c
c     bra >= ket
c
c  this routine will compute the products of yz, and
c x/w segments with each other and themselves. which products
c are actually computed is controlled by a parameter list, sginfo,
c which holds segment types and desired products. (see below)
c
c     reflst(*) int list of refs (tagged z walks; -1 = non ref)
c     indsym(*) symmetry of valid csf(i)
c     conft(*) map of valid csf to expansion location
c     heap(*) free space left for dynamic allocation
c     totfre    amount of free heap space left
c segmentation variables
c      sgmayz : sigma vector for yz paths
c       indx1  : index vector for segment 1
c       indx2  : index vector for segment 2
c       sgma1  : sigma vector for segment 1
c       sgma2  : sigma vector for segment 2
c       ci1    : ci vector segment 1
c       ci2    : ci vector for segment 2
c       sglen  : number of internal paths for this segment
c       sgnci  : length of ci expansion for given segment
c       sgcist : begining ci element for each segment
c       sgcsf  : configuration sf start number for each segment
c       sgncsf : number of configurations covered in segment
c       sgpath : starting path number for this segment
c       sginfo : segment types and products to compute
c control variables
c       times  : cpu event for each nex routine
c       iskip  : controls type of product to be formed
c
c
         IMPLICIT NONE

C====>Begin Module MULTNXNEW              File multnxnew.f              
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
C
      INTEGER             ATEBYT,      FORBYT
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             SG1TYP
      PARAMETER           (SG1TYP = 1)
      INTEGER             SG2TYP
      PARAMETER           (SG2TYP = 2)
      INTEGER             YZBYYZ
      PARAMETER           (YZBYYZ = 3)
      INTEGER             YZBYS1
      PARAMETER           (YZBYS1 = 4)
      INTEGER             YZBYS2
      PARAMETER           (YZBYS2 = 5)
      INTEGER             S1BYS1
      PARAMETER           (S1BYS1 = 6)
      INTEGER             S1BYS2
      PARAMETER           (S1BYS2 = 7)
      INTEGER             S2BYS2
      PARAMETER           (S2BYS2 = 8)
      INTEGER             WSEG
      PARAMETER           (WSEG = 2)
      INTEGER             XSEG
      PARAMETER           (XSEG = 1)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             TIME0X
      PARAMETER           (TIME0X = 0)
      INTEGER             TIME1X
      PARAMETER           (TIME1X = 1)
      INTEGER             TIME2X
      PARAMETER           (TIME2X = 2)
      INTEGER             TIME3X
      PARAMETER           (TIME3X = 3)
      INTEGER             TIME4X
      PARAMETER           (TIME4X = 4)
      INTEGER             TIMESO0X
      PARAMETER           (TIMESO0X = 5)
      INTEGER             TIMESO1X
      PARAMETER           (TIMESO1X = 6)
      INTEGER             TIMESO2X
      PARAMETER           (TIMESO2X = 7)
      INTEGER             TIMED
      PARAMETER           (TIMED = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             TMINIT
      PARAMETER           (TMINIT = 1)
      INTEGER             TMPRT
      PARAMETER           (TMPRT = 2)
      INTEGER             TMREIN
      PARAMETER           (TMREIN = 3)
      INTEGER             TMCLR
      PARAMETER           (TMCLR = 4)
      INTEGER             TMSUSP
      PARAMETER           (TMSUSP = 5)
      INTEGER             TMRESM
      PARAMETER           (TMRESM = 6)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXINFO
      PARAMETER           (MXINFO = 20)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             SEGBRA
      PARAMETER           (SEGBRA = 1)
      INTEGER             SEGKET
      PARAMETER           (SEGKET = 2)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*),  ISKIP,       NTASK
      INTEGER             REFLST(*),   SGCIST(*),   SGCSF(*)
      INTEGER             SGINFO(8),   SGLEN(*),    SGNCI(*)
      INTEGER             SGNCSF(*),   SGPATH(*),   SGSCR(*),    TASK(*)
      INTEGER             TIMES(0:8),  TOTFRE
C
      REAL*8              CIBRA(*),    CIKET(*),    HEAP(*)
      REAL*8              SGMABRA(*),  SGMAKET(*)
C
C     Local variables
C
      CHARACTER*20        DATESTR,     TIMESTR
C
      INTEGER             ADRF,        CISTLOC(2),  DO1EX,       FIRST0
      INTEGER             FIRST1,      FIRST4,      I
      INTEGER             IPTH1LOC(2), ISCRL,       ISGEXC,      ITASK
      INTEGER             ITMP,        IWX,         KK,          LOC05
      INTEGER             LOC06,       LOC0MX,      LOC15,       LOC16
      INTEGER             LOC17,       LOC18,       LOC19,       LOC1MX
      INTEGER             LOC210,      LOC211,      LOC212,      LOC213
      INTEGER             LOC25,       LOC26,       LOC27,       LOC28
      INTEGER             LOC29,       LOC2MX,      LOC36,       LOC37
      INTEGER             LOC38,       LOC3BF,      LOC3D1,      LOC3D2
      INTEGER             LOC3IB,      LOC3ICOMB,   LOC3K1,      LOC3K2
      INTEGER             LOC3LA,      LOC3MX,      LOC3SCRATCH, LOC3SO2
      INTEGER             LOC3SOGOUT,  LOC3SOIJKL,  LOC3SPECIALBUF
      INTEGER             LOC3TT,      LOC3UVWX,    LOC3VA,      LOC45
      INTEGER             LOC45A,      LOC46,       LOC47,       LOC4MX
      INTEGER             LOC51,       LOC52,       LOC55,       LOC56
      INTEGER             LOC57,       LOC58,       LOC59,       LOC5MX
      INTEGER             LOC61,       LOC62,       LOC63,       LOC64
      INTEGER             LOC65,       LOC66,       LOC77,       LSCR
      INTEGER             LSCT,        MODE,        MSEG,        MXORB3
      INTEGER             NCIITR,      NINITV,      NSEG1,       NSEG34
      INTEGER             NSEGXZ,      SEGSMLOC(2), SEGTYPE(2),  TASKID
      INTEGER             TASKINT,     TASKTYP,     YSEG
C
      LOGICAL             CLCXZ,       LSTSEG,      NO(5),       NOWW
      LOGICAL             NOWX,        NOWZ,        NOXX,        NOXZ
      LOGICAL             NOYW,        NOYX,        NOYY,        NOYZ
      LOGICAL             S234X
C
      REAL*8              RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8              RTOLCI(NVMAX)
C
      INCLUDE 'aoinfo.inc'
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             TAPE6
C
      INCLUDE 'data.inc'
      INCLUDE 'f4x.inc'
      INCLUDE 'indata.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'tilda.inc'
      INCLUDE 'timao.inc'
      INCLUDE 'tmstat.inc'
      INCLUDE 'vppass.inc'
      character*12 descr(110)
      common /description/ descr
C====>End Module   MULTNXNEW              File multnxnew.f              
      equivalence (tape6,nunits(1))


*@ifdef hpm
*        call _f_hpm_start_(22,8224, "pciudg4.f","multnxnew")
*@endif
      no(5)=.false.
      no(4)=.false.
      no(3)=.false.
      no(2)=.false.
      no(1)=.false.
      noxx=.false.
      noyy=.false.
      noww=.false.
      nowz=.false.
      noxz=.false.
      nowx=.false.
      noyw =.false.
      noyx =.false.
      noyz =.false.
      if (no4ex.ge.1) no(5)=.true.
      if (no3ex.ge.1) no(4)=.true.
      if (no2ex.ge.1) no(3)=.true.
       itmp=no2ex
      if (mod(no2ex,2).gt.0) noyy=.true.
         no2ex=no2ex/10
      if (mod(no2ex,2).gt.0) noxx=.true.
         no2ex=no2ex/10
      if (mod(no2ex,2).gt.0) noww=.true.
         no2ex=no2ex/10
      if (mod(no2ex,2).gt.0) nowz=.true.
         no2ex=no2ex/10
      if (mod(no2ex,2).gt.0) noxz=.true.
         no2ex=no2ex/10
      if (mod(no2ex,2).gt.0) nowx=.true.
         no2ex=itmp
      itmp=no1ex
      if (mod(no1ex,2).gt.0) noyw=.true.
         no1ex=no1ex/10
      if (mod(no1ex,2).gt.0) noyx=.true.
         no1ex=no1ex/10
      if (mod(no1ex,2).gt.0) noyz=.true.
      no1ex=itmp
      if (no0ex.ge.1) no(1)=.true.
      if (no1ex.ge.1) no(2)=.true.
c     write(6,*) 'multnxnew: 01234ex:',(no(itmp),itmp=1,5),
c    . 'wx,xz,wz,ww,xx,yy:', nowx,noxz,nowz,noww,noxx,noyy
c    . ,'yz,yx,yw:',noyz,noyx,noyw


c
c
c     # the space allocation for heap is computed
c     # in terms of real*8 words
c
c     # set first position for temporary buffer space
c
      adrf = 1
c     #
c     # nseg34 = number of segments passed to threx and forex.
c     # mseg   = number of segments passed to all other routines.
c     # nseg1  = 0,1,2 the number of non-yz segments used in onex().
c     # do1ex  > 0 if onex() is needed.
c     #
c     # determine type of calls
c
c     # yz segment dependencies
c
c     # determine multiplication type: href, bk or full
c     #   iskip = -1  : href - call allin only
c     #   iskip = 0   : bk  - call twoex,onex and allin
c     #   iskip = 1   : full - call all n-ex routines
c
      if (iskip.eq.2) call bummer('multnxnew, invalid iskip=',iskip,2)

      do 8000 itask=1,ntask
      if (task(itask).eq.-1) goto 8000
      taskint=task(itask)/10
      tasktyp=task(itask)-taskint*10
      call date_and_time(datestr,timestr)

      write(6,375) descr(task(itask)),timestr(1:2),timestr(3:4),
     . timestr(5:6)
 375   format('multnx: starting ',a,' at ',a2,':',a2,':',a2)

c     if (no(taskint+1) .and. taskint.le.4) goto 8000
c     if (nodiag.eq.1 .and. taskint.gt.4) goto 8000


c # taskint = 0 (0ex) 1 (1ex) 2 (2ex) 3 (3ex) 4 (4ex) 5(diag4ex)
c #           6 (diag2ex) 7 (diag0ex)
c #           8 (so2int)  9 (so1int) 10 (so0int)
c
       goto (111,222,333,444,555,666,777,888,910,920,930) (taskint+1)

c**********************************************************************
c      diagonal 4 external
c      y (52) x (53) w (54)
c**********************************************************************
c
c      heap(loc61)  intbuf     atebyt(mxbld)
c      heap(loc62)  oneldg     atebyt(next+intorb)
c      heap(loc63)  lpbuf      atebyt(bufszl)
c      heap(loc64)  transf     atebyt(next)
c      heap(loc65)  pseudo     atebyt(cilen)
c
C
 666      if (nodiag.eq.1) goto 8000
          goto 8000

c**********************************************************************
c      diagonal 2 external
c      y (62) x (63) w (64)
c**********************************************************************
c
c      heap(loc61)  intbuf     atebyt(mxbld)
c      heap(loc62)  oneldg     atebyt(next+intorb)
c      heap(loc63)  lpbuf      atebyt(bufszl)
c      heap(loc64)  transf     atebyt(next)
c      heap(loc65)  pseudo     atebyt(cilen)
c
 777      if (nodiag.eq.1) goto 8000
          goto 8000

c**********************************************************************
c      diagonal 0 external
c      z (71)  y (72) x (73) w (74)
c**********************************************************************
c
c      heap(loc61)  intbuf     atebyt(mxbld)
c      heap(loc62)  oneldg     atebyt(next+intorb)
c      heap(loc63)  lpbuf      atebyt(bufszl)
c      heap(loc64)  transf     atebyt(next)
c      heap(loc65)  pseudo     atebyt(cilen)
c
 888      if (nodiag.eq.1) goto 8000
          goto 8000

c
c***********************************************************************
c
c     four external
c     z (41) y (42) x (43) w (44)
c***********************************************************************
c
c     # call fourex only if needed
c
555    continue
         if (no(5)) goto 8000
        goto 8000
c
c***********************************************************************
c
c     three external
c     yx 31  yw 32
c***********************************************************************
c
c     # call threex only if needed
c
444     continue
c        no(4)=.true.
        if (no(4)) goto 8000
c
c      nseg34 wird nicht mehr benoetigt!
c
c        call timer(' ', tmresm, times(time3x), tape6 )
cvp
c      call timer(' ', tminit, ttran, tape6)
c      call timer(' ', tmsusp, ttran, tape6)
c      call timer(' ', tminit, tiscn, tape6)
c      call timer(' ', tmsusp, tiscn, tape6)
cvp
c
c        # maxbuf = blocksize for the two electron
c        #          integrals (three external)
c        # maxlp3 = blocksize for the formula tape three external
c        # bufszl = buffersize for formula tape
c
c        # heap map:
ctm      #   heap( loc36 )  lpbuf  (5*forbyt(maxlp3) + bufszl ) real*8
c        #   heap( loc37 )  rdbuf  ( maxbuf, 2 )        real*8
c        #   heap( loc38 )  scr3x  ( scr3xl )           real*8
c
cvp
         loc36 = adrf
         loc37 = loc36 + 5*forbyt(maxlp3)+atebyt( maxlp3 + bufszl )
         loc38 = loc37 + atebyt( 1 * maxbuf )
         loc3mx = loc38 + atebyt( scr3xl ) - 1
c
         if ( loc3mx .gt. totfre ) then
            write(tape6,20020)totfre,loc3mx
            write(tape6, 20010 ) loc36,loc37,loc38,loc3mx
            call bummer('multnx: (totfre-loc3mx)=',
     &       (totfre-loc3mx), faterr )
         endif
cvp
c

       if (aodrv.lt.1) then

         call threx(
     &    indsymbra, conftbra,indxbra,  cibra,
     &    sgmabra, indsymket,conftket,indxket, ciket, sgmaket,
     &    heap(loc36), heap(loc37),sglen,sgcist,sgpath,
     &    sginfo(segbra)-2, heap(loc38) )
         endif

      goto 8000
c
c********************************************************************
c
c     two external
c     yy 21  xx 22 ww 23 xz 24 wz 25 wx 26
c********************************************************************
c
 333  continue
c     call timer(' ', tmresm, times(time2x), tape6 )
c
c     # maxbl2 = blocksize for the two electron integrals(two external)
c     # mnbas  = largest number of orbitals in any symmetry block
c     # mxbl23 = size of transformed linear combinations of
c     #          integrals (mxbl23=maxbl2/3)
c     # bufszl = buffersize for formula tape
c     # bufszi = buffersize for reading the two electron integrals
c
c     # heap map:
c     #   heap( loc25 )   intbuf ( maxbl2 + bufszi )   real*8
c     #   heap( loc26 )   transf ( mxbl23*2 )          real*8
c     #   heap( loc27 )   lpbuf  ( bufszl )            real*8
c     #   heap( lscr  )   lscr   ( mnbas * mnbas )     real*8
c     #   heap( lscr  )   lsct   ( mnbas * mnbas )     real*8
c     #   heap( loc28 )   scrj   ( 2 * mxbl23    )     real*8
c     #   heap( loc29 )   scrk   ( 2 * mxbl23    )     real*8
c     #   heap( loc210)   scrkt  ( 2 * mxbl23    )     real*8
c     #   heap( loc211)   scr1   ( mnbas * mnbas )     real*8
c     #   heap( loc212)   scr2   ( mnbas * mnbas )     real*8
c     #   heap( loc213)   scr3   ( mnbas * mnbas )     real*8
c
      loc25  = adrf
      loc26  = loc25 + atebyt( maxbl2 + bufszi )
      loc27  = loc26 + atebyt( mxbl23*2 )
      lscr   = loc27 + atebyt( bufszl )
      lsct   = lscr + atebyt( mnbas * mnbas )
      loc28  = lsct + atebyt( mnbas * mnbas )
      loc29  = loc28 + atebyt( mxbl23*2 )
      loc210 = loc29 + atebyt( mxbl23*2 )
      loc211 = loc210 + atebyt( mxbl23*2 )
      loc212 = loc211 + atebyt( mnbas * mnbas )
      loc213 = loc212 + atebyt( mnbas * mnbas )
      loc2mx = loc213 + atebyt( mnbas * mnbas ) - 1
c
      if ( loc2mx .gt. totfre ) then
         write(tape6,20040)totfre,loc2mx
         write(tape6, 20010 ) loc25,loc26,loc27,lscr,loc28,loc29,
     &    loc210,loc2mx
         call bummer('multnx: (totfre-loc2mx)= ',
     &    (totfre-loc2mx), faterr )
      endif
c

       if (tasktyp.eq.1) then
c
c ===============================  YY ================================
c
        if (noyy) goto 2499

      call twoex_yy(indsymbra,conftbra,indxbra, cibra,sgmabra,
     .  heap(loc25),heap(loc26), heap(loc27), heap(loc28),heap(loc29),
     .  heap(loc210),
     .  indsymket, conftket, indxket, ciket,sgmaket,
     .  sglen,sgcist)

       endif

        if (tasktyp.eq.4) then

c
c ===============================  XZ ==============================
c
        if (noxz) goto 2499

            call twoex_xz(conftbra,indxbra, cibra,sgmabra,
     .      heap(loc25),
     .      heap(loc26), heap(loc27), heap(loc28),   heap(loc29),
     .      heap(loc210),conftket,indxket,ciket,
     .      sgmaket, sglen)
      endif


        if (tasktyp.eq.5) then

c
c ===============================  WZ ==============================
c

        if (nowz) goto 2499
c        write(6,*) 'vor twoex_wz sgma1'
c        call writex(sgma1,100)
c        call writex(ci1,100)
c        write(6,*) 'vor twoex_wz sgma2'
c        call writex(sgma2,100)
c        call writex(ci2,100)

c   auch twoex_wz umdrehen 1.Segment X 2. Segment Z!

            call twoex_wz(conftbra,indxbra,cibra,sgmabra, heap(loc25),
     .      heap(loc26), heap(loc27), heap(loc28),   heap(loc29),
     .      heap(loc210), conftket,indxket,ciket,
     .      sgmaket,sglen)


c        write(6,*) 'nach twoex_wz sgma1'
c        call writex(sgma1,100)
c        write(6,*) 'nach twoex_wz sgma2'
c        call writex(sgma2,100)

      endif


         if (tasktyp.eq.6) then
c
c ===============================  WX   ==============================
c

        if (nowx) goto 2499
          call twoex_wx(indsymbra,conftbra,indsymket,conftket,
     .       heap(loc25),
     .       heap(loc26), heap(loc27), heap(loc28),   heap(loc29),
     .       heap(loc210) ,  heap(loc211), heap(loc212),   heap(loc213),
     .       indxbra, cibra,   sgmabra,  indxket, ciket,  sgmaket,
     .       sglen)

      endif


         if (tasktyp.eq.3 ) then
c
c ===============================  WW =============================
c
        if (noww) goto 2499

            call twoex_ww(indsymbra,conftbra,indsymket,conftket,
     .       heap(loc25),
     .       heap(loc26), heap(loc27), heap(loc28),   heap(loc29),
     .       heap(loc210),  heap(loc211), heap(loc212),heap(loc213),
     .       indxbra, cibra,   sgmabra,  indxket, ciket,  sgmaket,
     .       sglen,sgcist)

        endif

         if (tasktyp.eq.2 ) then
c
c ===============================  XX =============================
c
        if (noxx) goto 2499

              call twoex_xx(indsymbra, conftbra,
     .         indsymket,conftket, heap(loc25),
     .         heap(loc26), heap(loc27), heap(loc28),   heap(loc29),
     .         heap(loc210),heap(loc211),heap(loc212),heap(loc213),
     .         indxbra, cibra, sgmabra,  indxket, ciket,  sgmaket,
     .         sglen,sgcist)
        endif



2499  dotild = .false.
c
c     call timer(' ', tmsusp, times(time2x), tape6 )
      goto 8000
c
2500   continue
c***********************************************************************
c
c     one external
c     yz 11 yx 13 yw 14
c
c***********************************************************************
c
c222  call timer(' ', tmresm, times(time1x), tape6 )
 222  continue
c
c     # mxorb  = blocksize for the two electron integrals (one external)
c     # bufszl = buffersize for formula tape
c     # bufszi = buffersize for reading the two electron integrals
c
c     # heap map:
c     #   heap( loc15 )   intbuf ( mxorb3 + bufszi )   real*8
c     #   heap( loc16 )   transf ( mxorb )             real*8
c     #   heap( loc17 )   lpbuf  ( bufszl )            real*8
c     #   heap( loc18 )   scr1   ( mxorb * mxorb )     real*8
c     #   heap( loc19 )   scr2   ( mxorb * mxorb )     real*8
c
      mxorb3 = mxorb * 3
      loc15  = adrf
      loc16  = loc15 + atebyt( mxorb3 + bufszi )
      loc17  = loc16 + atebyt( mxorb )
      loc18  = loc17 + atebyt( bufszl )
      loc19  = loc18 + atebyt( mxorb * mxorb )
      loc1mx = loc19 + atebyt( mxorb * mxorb ) - 1
c
      if ( loc1mx .gt. totfre ) then
         write(tape6,20050)totfre,loc1mx
         write(tape6,20010) loc15,loc16,loc17,loc1mx
         call bummer('mult: (loc1mx-totfre)=',(loc1mx-totfre),
     &    faterr)
      endif
c
c
c   just yz contributions
c
c
c    now two different y and zsegments, so check onex_sp
c
c   indx1,indx2 umgedreht
      if (task(itask).eq.11 .and. noyz) goto 2995
      if (task(itask).eq.13 .and. noyx) goto 2995
      if (task(itask).eq.14 .and. noyw) goto 2995


       call onex_sp( indsymbra, conftbra,indxbra, cibra,
     &               sgmabra,indsymket,conftket,indxket,
     .               ciket,    sgmaket,
     &               heap(loc15),heap(loc16),heap(loc17),
     &               heap(loc18),heap(loc19),
     &               sglen, sgcist,
     &               tasktyp , sginfo )

c2995  call timer(' ', tmsusp, times(time1x), tape6 )
 2995 continue
c
c
c     # skip to here for an all internal calculation such
c     # as href(*) computation.
c
c
      goto 8000
3000  continue
c********************************************************************
c
c     all internal
c
c********************************************************************
c

c111   call timer(' ', tmresm, times(time0x), tape6 )
 111   continue
c
c     # bufszl = buffersize for formula tape
c     # bufszi = buffersize for reading the two electron integrals
c
c     # heap map:
c     #   heap( loc05 )     intbuf ( bufszi )    real*8
c     #   heap( loc06 )     lpbuf  ( bufszl )    real*8
c
      loc05  = adrf
      loc06  = loc05 + atebyt( bufszi )
      loc0mx = loc06 + atebyt( bufszl ) - 1
c
      if ( loc0mx .gt. totfre ) then
         write(tape6,20060)totfre,loc0mx
         write(tape6,20010)loc05,loc06,loc0mx,
     &    loc06,loc0mx
         call bummer('multnx: (totfre-loc0mx)=',
     &    (totfre-loc0mx), faterr )
      endif

c
c      calculate zz, yy contribution
c
c      fuer xx' or ww' combinations
c      allin_sp umdrehen

       call allin_sp
     .   (reflst, indsymbra, conftbra,indxbra,cibra,sgmabra,
     .    indsymket,conftket,indxket, ciket,  sgmaket,
     .    heap(loc05), heap(loc06),sglen,
     .    cibra,tasktyp,sginfo,sgcist )


c
c     call timer(' ', tmsusp, times(time0x), tape6 )
      goto 8000

c
910   continue
c
c********************************************************************
c
c     all internal so
c
c     81 0ex z
c     82 0ex y
c     83 0ex x
c     84 0ex w
c********************************************************************
c
c     call timer(' ', tmresm, times(timeso0x), tape6 )
c
c     # bufszl = buffersize for formula tape
c     # bufszi = buffersize for reading the two electron integrals
c
c     # heap map:
c     #   heap( loc51 )     intbuf ( bufszi )    real*8
c     #   heap( loc52 )     lpbuf  ( bufszl )    real*8
c
      loc51  = adrf
      loc52  = loc51 + atebyt( bufszi )
      loc0mx = loc52 + atebyt( bufszl ) - 1
c
      if ( loc0mx .gt. totfre ) then
         write(tape6,20060)totfre,loc0mx
         write(tape6,20010)loc05,loc06,loc0mx,
     &    loc06,loc0mx
         call bummer('multnx: (totfre-loc0mx)=',
     &    (totfre-loc0mx), faterr )
      endif
c
      call so2int(  reflst,indsymbra,indsymket,conftbra,
     . conftket,cibra,ciket,sgmabra,sgmaket,indxbra,indxket,
     & heap(loc51), heap(loc52),  sglen,sgcist, cibra,tasktyp  )

c     call timer(' ', tmsusp, times(timeso0x), tape6 )
      goto 8000
c
c**********************************************************************
c
c     one external so
c
c     91 1ex zy
c     93 1ex xy
c     94 1ex wy
c**********************************************************************
c
920   continue
c     call timer(' ', tmresm, times(timeso1x), tape6 )
c
c     # mxorb  = blocksize for the two electron integrals (one external
c     # bufszl = buffersize for formula tape
c     # bufszi = buffersize for reading the two electron integrals
c
c     # heap map:
c     #   heap( loc55 )   intbuf ( mxorb3 + bufszi )   real*8
c     #   heap( loc56 )   transf ( mxorb )             real*8
c     #   heap( loc57 )   lpbuf  ( bufszl )            real*8
c     #   heap( loc58 )   scr1   ( mxorb * mxorb )     real*8
c     #   heap( loc59 )   scr2   ( mxorb * mxorb )     real*8
c
      mxorb3 = mxorb * 3
      loc55  = adrf
      loc56  = loc55 + atebyt( mxorb3 + bufszi )
      loc57  = loc56 + atebyt( mxorb )
      loc58  = loc57 + atebyt( bufszl )
      loc59  = loc58 + atebyt( mxorb * mxorb )
      loc5mx = loc59 + atebyt( mxorb * mxorb ) - 1
c
      if ( loc5mx .gt. totfre ) then
         write(tape6,20050)totfre,loc1mx
         write(tape6,20010) loc15,loc16,loc17,loc1mx
         call bummer('mult: (loc1mx-totfre)=',(loc1mx-totfre),
     &    faterr)
      endif
c
         call so1int(  indsymbra,indsymket,conftbra,conftket,
     .    cibra,ciket,sgmabra,sgmaket,indxbra,indxket,
     &    heap(loc55), heap(loc56), heap(loc57), heap(loc58),
     &    sglen,       sgcist,   tasktyp )

c     call timer(' ', tmsusp, times(timeso1x), tape6 )
      goto 8000
c
c
930   continue
c********************************************************************
c
c     two external so
c     101 yy
c     102 xx
c     103 ww
c     104 wx
c
c********************************************************************
c
c     call timer(' ', tmresm, times(timeso2x), tape6 )
c
c     # maxbl2 = blocksize for the two electron integrals(two external)
c     # mnbas  = largest number of orbitals in any symmetry block
c     # mxbl23 = size of transformed linear combinations of
c     #          integrals (mxbl23=maxbl2/3)
c     # bufszl = buffersize for formula tape
c     # bufszi = buffersize for reading the two electron integrals
c
c     # heap map:
c     #   heap( loc25 )   intbuf ( maxbl2 + bufszi )   real*8
c     #   heap( loc26 )   transf ( mxbl23*2 )          real*8
c     #   heap( loc27 )   lpbuf  ( bufszl )            real*8
c     #   heap( lscr  )   lscr   ( mnbas * mnbas )     real*8
c     #   heap( lscr  )   lsct   ( mnbas * mnbas )     real*8
c     #   heap( loc28 )   scrj   ( 2 * mxbl23    )     real*8
c     #   heap( loc29 )   scrk   ( 2 * mxbl23    )     real*8
c     #   heap( loc210)   scrkt  ( 2 * mxbl23    )     real*8
c     #   heap( loc211)   scr1   ( mnbas * mnbas )     real*8
c     #   heap( loc212)   scr2   ( mnbas * mnbas )     real*8
c     #   heap( loc213)   scr3   ( mnbas * mnbas )     real*8
c
      loc25  = adrf
      loc26  = loc25 + atebyt( maxbl2 + bufszi )
      loc27  = loc26 + atebyt( mxbl23*2 )
      lscr   = loc27 + atebyt( bufszl )
      lsct   = lscr + atebyt( mnbas * mnbas )
      loc28  = lsct + atebyt( mnbas * mnbas )
      loc29  = loc28 + atebyt( mxbl23*2*3 )
      loc210 = loc29
      loc211 = loc210
      loc212 = loc211 + atebyt( mnbas * mnbas )
      loc213 = loc212 + atebyt( mnbas * mnbas )
      loc2mx = loc213 + atebyt( mnbas * mnbas ) - 1
c
      if ( loc2mx .gt. totfre ) then
         write(tape6,20040)totfre,loc2mx
         write(tape6, 20010 ) loc25,loc26,loc27,lscr,loc28,loc29,
     &    loc210,loc2mx
         call bummer('multnx: (totfre-loc2mx)= ',
     &    (totfre-loc2mx), faterr )
      endif
c
      call so0int( conftbra,indsymbra,indxbra,cibra,sgmabra,
     . conftket,indsymket,indxket,ciket,sgmaket,
     & heap(loc25),  heap(loc27),
     & heap(loc28),  heap(loc211), heap(loc212), heap(loc213),
     & sglen, sgcist, tasktyp )
c
c     call timer(' ', tmsusp, times(timeso2x), tape6 )
2001  continue

 8000  continue
c
c
*@ifdef hpm
*        call _f_hpm_stop_(22,8975)
*@endif
      return
c
20000 format(' not enough heap storage for four external case'/
     & ' available ',i10,'  wanted ',i10)
20010 format(1x,10i10)
20020 format(' not enough heap storage for three external'/
     & ' available',i10,'  wanted',i10)
20040 format(' not enough heap storage for two external case'/
     & ' available',i10,' wanted',i10)
20050 format(' not enough heap storage for one external case'/
     & ' available',i10,' wanted',i10)
20060 format(' not enough space for all internal case'/
     & ' available',i10,' wanted',i10)
      end
      subroutine upwlknew( ket, bra, walk, mul,mxket,mxbra,jump )
c
c   # calculate new valid bra and ket combination corresponding
c   # to an upper walk on DIFFERENT DRTs
c   # walk = .true. if no valid upper walk can be constructed
c   # mul  = current multiplicity
c   # mxket = maximum int path ket within this segment
c   # mxbra = maximum int path bra within this segment
c   # jump  = true : skip this walk but allow to continue
c   #                with walk construction

c   # jump must be handled internally !!
c
c

C====>Begin Module UPWLKNEW               File upwlknew.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             BRA,         KET,         MUL,         MXBRA
      INTEGER             MXKET
C
      LOGICAL             JUMP,        WALK
C
C     Local variables
C
      INTEGER             BROWN,       BVER,        CLEV,        HILEV
      INTEGER             ICASE,       KROWN,       KVER,        NIOT
      INTEGER             NLEV,        ROWC,        TASKID
C
      INCLUDE 'drt.inc'
      INCLUDE 'drtbra.inc'
      INCLUDE 'drtket.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'walksv.inc'
C====>End Module   UPWLKNEW               File upwlknew.f               
      save   /walksv/

      if (mul.gt.1) then
            mul=mul-1
            ket=ket+1
            bra=bra+1
            jump = (bra.le.0 .or. ket.le.0)
            walk = (bra.gt.mxbra .or. ket.gt.mxket)
C HPM   return
c       call _f_hpm_stop_(28,10467)
C HPM
           return
      endif
      rowc  = 0
      krown = 0
      brown = 0
c
c     # reassign saved common block values.
      niot  = niotc
      clev  = clevc
      hilev = hilevc
c
      walk = .true.
      if ( hilev .eq. niot ) then
         brown = row( clev )
         go to 500
      endif
      go to 100
c
c     # decrement the current level and check for exit.
c
90    continue
      case(clev) = 4
      if ( clev .eq. hilev ) go to 1000
      clev = clev - 1
c
c     # next case at the current level.
c
100   continue
      nlev = clev + 1
      icase = case(clev) - 1
      if ( icase .lt. 0 ) go to 90
      case(clev) = icase
      rowc = row(clev)
      brown = lbra(icase,rowc)
      if ( brown .eq. 0 ) go to 100
      krown = lket(icase,rowc)
      if ( krown .eq. 0 ) go to 100
      if ( brown .ne. krown ) then
         call bummer('upwalk: (brown-krown)=',(brown-krown),faterr)
      endif
      row(nlev) = krown
      yb(nlev) = yb(clev) + yybra(icase,rowc)
      yk(nlev) = yk(clev) + yyket(icase,rowc)
      if ( nlev .eq. niot ) go to 500
      clev = nlev
      go to 100
500   continue
      ket  = yk(niot)+1
      bra  = yb(niot)+1
      mul = 1
      if(spnorb)mul = b(brown) + 1
c
c     # check whether bra and ket are within the valid range
c     # of 1 to mxbra and 1 to mxket
c
      jump = (bra.le.0 .or. ket.le.0)
      walk = (bra.gt.mxbra .or. ket.gt.mxket)
c
c     # resave scalar parameters.
1000  continue
      clevc  = clev
      hilevc = hilev
      niotc  = niot
c
C HPM   return
c       call _f_hpm_stop_(28,10534)
C HPM
      return
      end
      subroutine prepdnew
     .  ( index, indsym, conft, lpbuf, mxbllp, heap,maxfree )
c
c this subroutine sets up the segmentation of the ci vector
c namelist input currently enforces the number of segments
c nsegd(*)   number of Segments diagonal case
c nseg0x(*)  number of Segments all-internal cas
c nseg1x(*)  number of Segments one-external cas
c nseg2x(*)  number of Segments two-external cas
c nseg3x(*)  number of Segments three-exeternal case
c nseg4x(*)  number of Segments all-external case
c array index 1 (z), 2 (y), 3 (x), 4 (w)
c at the moment we take nsegd for ALL types !
c
         IMPLICIT NONE

C====>Begin Module PREPDNEW               File prepdnew.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT,      ISUM,        RL
C
      INTEGER             ATEBYT,      FORBYT,      ISUM,        RL
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             ZTYPE
      PARAMETER           (ZTYPE = 1)
      INTEGER             YTYPE
      PARAMETER           (YTYPE = 2)
      INTEGER             XTYPE
      PARAMETER           (XTYPE = 3)
      INTEGER             WTYPE
      PARAMETER           (WTYPE = 4)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXINFO
      PARAMETER           (MXINFO = 20)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 20)
      INTEGER             MXENER
      PARAMETER           (MXENER = 40)
      INTEGER             MAXMAP
      PARAMETER           (MAXMAP = 20)
      INTEGER             MAXBFT
      PARAMETER           (MAXBFT = 1023)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             CONFT(*),    HEAP(*),     INDEX(*)
      INTEGER             INDSYM(*),   MAXFREE,     MXBLLP
C
      REAL*8              LPBUF(*)
C
C     Local variables
C
      CHARACTER           AMAP(4)
      CHARACTER*8         BFNLAB(MAXBFT)
      CHARACTER*4         SLABEL(MAXSYM)
      CHARACTER*80        TITLE(MXTITL)
C
      INTEGER             BL,          BUFSIZ,      CISTRT,      CMPSYM
      INTEGER             CONFW,       CONFX,       CONFY,       CONFZ
      INTEGER             DICNTW,      DICNTX,      DRTFIL,      ENDDRT
      INTEGER             ENDIND,      ENDZYXW,     FIN,         FRESPC
      INTEGER             GA_NNODES,   GA_NODEID,   GA_READ_INC, HPFREE
      INTEGER             HPTOP,       I,           IBLK,        ICNTW
      INTEGER             ICNTX,       IERR,        IETYPE(MXENER)
      INTEGER             IMAP(4),     IMTYPE(MAXMAP),           INDEX1
      INTEGER             INDXLEN,     IPTH,        ISEG,        ISGTYPE
      INTEGER             ISYM,        ITMP,        IVAL,        IVAL2
      INTEGER             IVOUT,       JBF,         JSYM,        LCONFW
      INTEGER             LCONFX,      LCONFY,      LCONFZ,      LVLPRT
      INTEGER             MAXCSF,      MAXSIZ,      MDTOB,       MITOB
      INTEGER             MODE,        MT_BYTE,     MT_DBL,      MT_DCPL
      INTEGER             MT_INT,      MT_LOG,      MT_REAL,     MT_SCPL
      INTEGER             MXSEG,       NBFT,        NCIITR,      NENRGY
      INTEGER             NI,          NIJ,         NINFO,       NINITV
      INTEGER             NMAP,        NMBJ,        NSYM,        NTITLE
      INTEGER             NUMCNF,      NUMCSF,      NUMDMAT,     NUMPTH
      INTEGER             NWALKNEW,    NWALKS,      NY,          PTHCSF
      INTEGER             SCRCSF,      SEGSPC,      SGTYPE,      STCONF
      INTEGER             STRIND,      STRTDRT,     STRTVC
      INTEGER             STRTZYXW,    TASKID,      XMAP(MAXBFT,MAXMAP)
C
      LOGICAL             GA_CREATE,   GA_CREATE_MUTEXES
      LOGICAL             GA_DESTROY_MUTEXES
C
      REAL*8              ENERGY(MXENER),           RTOL(NVMAX)
      REAL*8              RTOLBK(NVMAX),            RTOLCI(NVMAX)
C
      INCLUDE 'aoinfo.inc'
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             SHOUT,       TAPE6
C
      INCLUDE 'civct.inc'
      INCLUDE 'cnfspc.inc'
      INCLUDE 'data.inc'
      INCLUDE 'indata.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'tmstat.inc'
      INCLUDE 'vppass.inc'
C====>End Module   PREPDNEW               File prepdnew.f               
      equivalence (tape6,nunits(1))
      equivalence (shout,nunits(3))
C
      data imap/1,2,3,4/
      data amap/'Z','Y','X','W'/

cvp
c
c
c
c     nseg number of segments of the ci vector
c        order of the segments z and y paths, w , x paths
c        the x paths start with a new segment
c     segel(nseg) number of internal paths for each segment
c     cist(nseg) starting number of the first ci vector element of
c                each segment
c     indxst(nseg) number of the record at which a given segment of
c                  the index vector starts
c     segci(nseg) number of ci elements for each segment
c
c     segscr(nseg) size of the d1,d2, kd1 and kd2 arrays in the aodriven
c
c     segtype(nseg) segment type (1=Z, 2=Y, 3=X, 4=W)
c
c     ipthst (nseg) first internal path
c

C HPM  subroutine_start
c       call _f_hpm_start_(29,11215, "pciudg4.f","prepdnew")
C HPM
      if (aodrv .eq. 1) then
       call bummer('ao-driven mode not supported',0,2)
      endif
ctmend
ctm additionally need some space for density calculation
ctm see prepdd (totspc)
ctm
      ntot=isum(maxsym,nbpsy,1)
c
c     # calculate external walks through x and w
c
      write(6,*) 'prepdnew: nmbpr(*)',nmbpr
      do 100 iblk = 1, nmsym
         icntx = 0
         icntw = 0
         nmbj = nmbpr(iblk)
         if (nmbj .gt. 0 ) then
            do 200 jbf = 1, nmbj
               jsym = lmdb(iblk,jbf)
               isym = lmda(iblk,jbf)
               if ( isym .ne. jsym ) then
                  nij = nbas(isym)*nbas(jsym)
                  icntx = icntx + nij
                  icntw = icntw + nij
               else
                  ni = nbas(isym)
                  icntx = icntx + ((ni-1)*ni)/2
                  icntw = icntw + ((ni+1)*ni)/2
               endif
200         continue
         endif
         cnfx(iblk) = icntx
         cnfw(iblk) = icntw
100   continue
c
      write(tape6,6001)
6001  format(/' number of external paths / symmetry')
      write(tape6,6002)'x',(cnfx(i),i=1,nmsym)
      write(tape6,6002)'w',(cnfw(i),i=1,nmsym)
6002  format(1x,'vertex',a2,8i8)

c
c    compute the CI vector length corresponding
c    to each bucket
c        nmb1=(nmb1a-1)/isize_zz+1
c        nmb2=(nmb2a-1)/isize_yy+1
c


      isize_zz=max(100,(nvalz-1)/150+1)
      isize_yy=max(100,(nvaly-1)/150+1)
      isize_xx=max(100,(nvalx-1)/150+1)
      isize_ww=max(100,(nvalw-1)/150+1)



c
c     # get z-walk information from index vector
c
      ival=0
      confz = 0
      do 110 ipth = 1, pthz
         if (index(ipth).gt.0) then
            ival=ival+1
            conft(ival) = confz
            confz = confz + 1
            itmp=(ival-1)/isize_zz+1
            clen_zz(itmp)=clen_zz(itmp)+1
         endif
110   continue
c
c     # get y-walk information from index vector
c
      confy = 0
      ival2=0
      do 120 ipth = (pthz + 1), (pthz + pthy)
         if (index(ipth).gt.0) then
            ival=ival+1
            ival2=ival2+1
            isym = sym21(indsym(ival))
            ny = nbas(isym)
            if (ny.le.0) then
               conft(ival) = -1
               index(ipth) = -1
               call bummer('prepd: zero externals for path no.',
     &          ipth, wrnerr )
            else
               conft(ival) = confy + confz
               confy = confy + ny
               itmp=(ival2-1)/isize_yy+1
               clen_yy(itmp)=clen_yy(itmp)+ny
            endif
         endif
120   continue

c
c     # pre-walk x portion of index vector to count x csfs
c     # this is used later to start the x segments in the right place
c
      confx = 0
      ival2=0
      do 130 ipth = (pthy + pthz + 1) , (pthz + pthy + pthx)
         if ( index(ipth) .gt. 0 ) then
            ival = ival+1
            ival2 = ival2+1
            isym  = sym21( indsym(ival) )
            confx = confx + cnfx(isym)
            itmp=(ival2-1)/isize_xx+1
            clen_xx(itmp)=clen_xx(itmp)+cnfx(isym)
         endif
130   continue

c
c     # pre-walk w portion of index vector to count w csfs
c
      confw = 0
      ival2=0
      do 131 ipth = (pthy+pthz+pthx+1) , (pthz+pthy+pthx+pthw)
         if ( index(ipth) .gt. 0 ) then
            ival=ival+1
            ival2=ival2+1
            isym  = sym21( indsym(ival) )
            confw = confw + cnfw(isym)
            itmp=(ival2-1)/isize_ww+1
            clen_ww(itmp)=clen_ww(itmp)+cnfw(isym)
         endif
131   continue

c
c   # calculate segments for each task type
c
c   # eventuell den Inhalt von civct fuer jeden
c   # segmenttyp auf den DRTFIL schreiben
c
c   # for the multiple segmentation make a backup copy
c   # of index in heap(1)
c
c   # two-external case
c       number of wx pairs = nw * nx
c                 zx       = nx * nz
c                 zw       = nw * nz
c                 yy       = (ny+1)*ny/2
c                 ww       = (nw+1)*nw/2
c                 xx       = (nx+1)*nx/2
c
c      defaults:  nz = 1
c                 ny = np
c                 nx,nw = sqrt(2*ppr*np+0.25)-0.5
c                 ppr = number of pairs per node = 4
c                 np  = number of nodes
c

      indxlen=pthz+pthy+pthx+pthw
      hpfree=maxfree -indxlen
      hptop = indxlen+1
*@ifndef int64
*c
*c     take care of proper alignment of heap(hptop) on
*c     real*8 boundary
*c
*      if (mod(hptop,2).eq.0) then
*         hptop=hptop+1
*         hpfree=hpfree-1
*      endif
*@endif

      if (hpfree.lt.1)
     .   call bummer('prepd: insufficient memory',hpfree,2)
      call icopy_wr(indxlen,index,1,heap,1)
c
c   # diagonal elements
c
      bl=1
      strtdrt=1
      call segment(index,indsym,conft,heap(hptop),hpfree,
     .      nsegd,bl,confz,confy,confx,confw,
     .      'diagonal',maxseg,strtdrt)
      nseg(bl)=nsegd(1)+nsegd(2)+nsegd(3)+nsegd(4)
*@ifndef parallel
c     if (nseg(bl).le.4) then
c     for sequential version use just one segmentation
c
c     use in-core method, so no need for further
c     segmentation
c
      if (spnorb) then
       bl=7
      else
       bl=6
      endif
      do i=2,bl
        nseg(i)=nseg(1)
        call icopy_wr(nseg(1),segci(1,1),1,segci(1,i),1)
        call icopy_wr(nseg(1),cist(1,1),1,cist(1,i),1)
        call icopy_wr(nseg(1),segel(1,1),1,segel(1,i),1)
        call icopy_wr(nseg(1),segscr(1,1),1,segscr(1,i),1)
        call icopy_wr(nseg(1),segtyp(1,1),1,segtyp(1,i),1)
        call icopy_wr(nseg(1),ipthst(1,1),1,ipthst(1,i),1)
        call icopy_wr(nseg(1),drtst(1,1),1,drtst(1,i),1)
        call icopy_wr(nseg(1),drtlen(1,1),1,drtlen(1,i),1)
        call icopy_wr(nseg(1),drtidx(1,1),1,drtidx(1,i),1)
        call icopy_wr(nseg(1),icnfst(1,1),1,icnfst(1,i),1)
        call icopy_wr(nseg(1),isgcnf(1,1),1,isgcnf(1,i),1)
      enddo

      call icopy_wr(4,nsegd,1,nseg0x,1)
      call icopy_wr(4,nsegd,1,nseg1x,1)
      call icopy_wr(4,nsegd,1,nseg2x,1)
      call icopy_wr(4,nsegd,1,nseg3x,1)
      call icopy_wr(4,nsegd,1,nseg4x,1)
      if (spnorb) then
         call icopy_wr(4,nsegd,1,nsegso,1)
      endif
c     endif
      goto 960
*@endif

c
c   # all-internal elements
c
      call icopy_wr(indxlen,heap,1,index,1)
      bl=2
      call segment(index,indsym,conft,heap(hptop),hpfree,
     .      nseg0x,bl, confz,confy,confx,confw,
     .      'all-internal',maxseg,strtdrt)
      nseg(bl)=nseg0x(1)+nseg0x(2)+nseg0x(3)+nseg0x(4)

c
c   # one-external elements
c
      call icopy_wr(indxlen,heap,1,index,1)
      bl=3
      call segment(index,indsym,conft,heap(hptop),hpfree,
     .      nseg1x,bl,confz,confy,confx,confw,
     .      'one-external',maxseg,strtdrt)
      nseg(bl)=nseg1x(1)+nseg1x(2)+nseg1x(3)+nseg1x(4)

c
c   # two-external elements
c
      call icopy_wr(indxlen,heap,1,index,1)
      bl=4
      call segment(index,indsym,conft,heap(hptop),hpfree,
     .      nseg2x,bl,confz,confy,confx,confw,
     .      'two-external',maxseg,strtdrt)
      nseg(bl)=nseg2x(1)+nseg2x(2)+nseg2x(3)+nseg2x(4)

c
c   # three-external elements
c
      call icopy_wr(indxlen,heap,1,index,1)
      bl=5
      call segment(index,indsym,conft,heap(hptop),hpfree,
     .      nseg3x,bl,confz,confy,confx,confw,
     .      'three-external',maxseg,strtdrt)
       nseg(bl)=nseg3x(1)+nseg3x(2)+nseg3x(3)+nseg3x(4)

c
c   # four-external elements
c
      call icopy_wr(indxlen,heap,1,index,1)
      bl=6
      call segment(index,indsym,conft,heap(hptop),hpfree,
     .      nseg4x,bl,confz,confy,confx,confw,
     .     'four-external',maxseg,strtdrt)
      nseg(bl)=nseg4x(1)+nseg4x(2)+nseg4x(3)+nseg4x(4)

c
c   # so elements
c
      if (spnorb) then
      call icopy_wr(indxlen,heap,1,index,1)
      bl=7
      call segment(index,indsym,conft,heap(hptop),hpfree,
     .      nsegso,bl,confz,confy,confx,confw,
     .     'spin-orbit',maxseg,strtdrt)
      nseg(bl)=nsegso(1)+nsegso(2)+nsegso(3)+nsegso(4)
      else
       nseg(7)=0
       call izero_wr(4,nsegso,1)
      endif
6202  format(' index vector'/(1x,20i6))
960   continue                            

950   continue
C HPM   return
c       call _f_hpm_stop_(29,11557)
C HPM
        write(0,*) 'prepdnew: nsym,nmsym=',nsym,nmsym
        open(unit=4,file='SEGANALYSE.HDR',
     .                  status='unknown',form='formatted')
        write(4,1002) nvalz,nvaly,nvalx,nvalw
1002    format('NVALZYXW=',4i10)
        write(4,1003) nmsym
1003    format('NSYM=',i4)
        write(4,1004) (nbas(i),i=1,nmsym)
1004    format('NEXTERN=',8i6)
        write(4,1005) lenci
1005    format('CI-DIMENSION=',i12)
        write(4,1006) isize_zz,isize_yy,isize_xx,isize_ww
1006    format('BATCHSIZE_ZYXW=',4i10)
        write(4,1007) 'CSFBATCH_Z='
        write(4,1001) (clen_zz(i),i=1,(nvalz-1)/isize_zz +1)
        write(4,1007) 'CSFBATCH_Y='
        write(4,1001) (clen_yy(i),i=1,(nvaly-1)/isize_yy +1)
        write(4,1007) 'CSFBATCH_X='
        write(4,1001) (clen_xx(i),i=1,(nvalx-1)/isize_xx +1)
        write(4,1007) 'CSFBATCH_W='
        write(4,1001) (clen_ww(i),i=1,(nvalw-1)/isize_ww +1)
1001    format(5I10)
1007    format(a11)
        close (4) 
      return
7000  format(' program-defined maxseg (',i3,') will be exceeded',
     + 'following nseg = ',i3)
7001  format(' error in prepd :  nwalks',i8,' nvalwk',i8,' stop')
      end
      subroutine segment
     .  ( index, indsym, conft, heap,maxfree,
     .    nsegt, slice,confz,confy,confx,confw,
     .   segtitle,mxseg,strtdrt)

       implicit none

C====>Begin Module SEGMENT                File segment.f                
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
C
      INTEGER             ATEBYT,      FORBYT
C
C     Parameter variables
C
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             ZTYPE
      PARAMETER           (ZTYPE = 1)
      INTEGER             YTYPE
      PARAMETER           (YTYPE = 2)
      INTEGER             XTYPE
      PARAMETER           (XTYPE = 3)
      INTEGER             WTYPE
      PARAMETER           (WTYPE = 4)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             FLOFDG
      PARAMETER           (FLOFDG = 2)
      INTEGER             FLDIAG
      PARAMETER           (FLDIAG = 1)
      INTEGER             FL3X
      PARAMETER           (FL3X = 3)
      INTEGER             FL3W
      PARAMETER           (FL3W = 4)
      INTEGER             FL4X
      PARAMETER           (FL4X = 5)
      INTEGER             FL4W
      PARAMETER           (FL4W = 6)
      INTEGER             FLDRT
      PARAMETER           (FLDRT = 7)
      INTEGER             LDSK
      PARAMETER           (LDSK = 0)
      INTEGER             VDK
      PARAMETER           (VDK = 1)
      INTEGER             GLAR
      PARAMETER           (GLAR = 2)
      INTEGER             DONE
      PARAMETER           (DONE = 3)
      INTEGER             DTWO
      PARAMETER           (DTWO = 4)
      INTEGER             DFOR
      PARAMETER           (DFOR = 5)
C
C     Argument variables
C
      CHARACTER*(*)       SEGTITLE
C
      INTEGER             CONFT(*),    CONFW,       CONFX,       CONFY
      INTEGER             CONFZ,       HEAP(*),     INDEX(*)
      INTEGER             INDSYM(*),   MAXFREE,     MXSEG
      INTEGER             NSEGT(*),    SLICE,       STRTDRT
C
C     Local variables
C
      CHARACTER           AMAP(4)
C
      INTEGER             BUFSIZ,      CISTRT,      CMPSYM,      ENDDRT
      INTEGER             ENDZYXW,     FRESPC,      I,           IMAP(4)
      INTEGER             IMAXSEGT,    INDEX1,      INDXLEN,     IPTH
      INTEGER             ISEG,        ISGTYPE,     LCONFW,      LCONFX
      INTEGER             LCONFY,      LCONFZ,      LVLPRT,      MAXCNF
      INTEGER             MAXCSF,      MAXSIZ,      MODE,        NCIITR
      INTEGER             NINITV,      NUMCNF,      NUMCSF,      NUMDMAT
      INTEGER             NUMPTH,      NWALKNEW,    NWALKS,      PTHCSF
      INTEGER             SCRCSF,      SEGNUMBER,   SEGSPC,      SGTYPE
      INTEGER             STCONF,      STRTVC,      STRTZYXW,    TASKID
      INTEGER             WRNERR
C
      REAL*8              RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8              RTOLCI(NVMAX)
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             DRTFIL,      FLINDX,      NLIST,       NVFILE
C
      INCLUDE 'civct.inc'
      INCLUDE 'cnfspc.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'drtsize.inc'
      INCLUDE 'indata.inc'
      INCLUDE 'inf.inc'
C====>End Module   SEGMENT                File segment.f                
      equivalence (nlist,nunits(1))

      data imap/1,2,3,4/
      data amap/'Z','Y','X','W'/

      equivalence (flindx,nunits(14))
      equivalence (drtfil,nunits(38))
      equivalence (nvfile,nunits(7))

c
c this subroutine sets up the segmentation of the ci vector
c array index 1 (z), 2 (y), 3 (x), 4 (w)


c
c     # initialize segmentation procedure
c
C HPM  subroutine_start
c       call _f_hpm_start_(30,11969, "pciudg4.f","segment")
C HPM
      frespc = forbyt(spcci)
      if (fileloc(fldrt).eq.glar) strtdrt=1
c
      maxsiz = (frespc) / 2
c    # segment order  z y x w
c    # index order    z y x w
c
      iseg = 0
      strtvc=1
      segnumber=0
      imaxsegt=4
      if (llenci.gt.0) imaxsegt=1

      do 1000 isgtype = 1,imaxsegt

       sgtype=imap(isgtype)

      if ( sgtype .eq. ztype ) then
c
c        # initialize for z walks
c
         strtzyxw = 1
C HPM   subroutine_end
c       call _f_hpm_stop_(30,11994)
C HPM
         endzyxw  = strtzyxw + pthz -1
         cistrt   = 0
         stconf   = 0
         lconfz   = 0
         maxcsf   = confz/nsegt(1) + 1
         maxcnf   = nvalz/nsegt(1) + 1
      elseif (sgtype .eq. ytype ) then
c
c        # initialize for y walks
c
         strtzyxw = pthz+1
C HPM   subroutine_end
c       call _f_hpm_stop_(30,12008)
C HPM
         endzyxw  = strtzyxw + pthy -1
         cistrt   = confz
         stconf   = nvalz
         lconfy   = 0
         maxcsf   = confy/nsegt(2) + 1
         maxcnf   = nvaly/nsegt(2)+1

      elseif ( sgtype .eq. wtype ) then
c
c        # initialize for w walks
c
         strtzyxw = pthz + pthy + pthx + 1
C HPM   subroutine_end
c       call _f_hpm_stop_(30,12023)
C HPM
         endzyxw  = strtzyxw + pthw - 1
         cistrt = confz+confy + confx
         stconf = nvalz + nvaly + nvalx
         lconfw   = 0
         maxcsf   = confw/nsegt(4) + 1
         maxcnf   = nvalw/nsegt(4)+1
c
      elseif ( sgtype .eq. xtype ) then
c
c        # initialize for x walks
c
         strtzyxw = pthz + pthy + 1
C HPM   subroutine_end
c       call _f_hpm_stop_(30,12038)
C HPM
         endzyxw  = strtzyxw + pthx - 1
         cistrt = confz+confy
         stconf = nvalz + nvaly
         lconfx   = 0
         maxcsf   = confx/nsegt(3) + 1
         maxcnf   = nvalx/nsegt(3)+1
      else
         call bummer('prepd: invalid sgtype=',sgtype,2)
      endif
c
c     # initialize segment type independent counts
c
      index1 = strtzyxw
      numpth = 0
      numcnf = 0
      numcsf = 0
      numdmat = 0
c
      do 140 ipth=strtzyxw,endzyxw
c
c        # get complimentary symmetry and determine number or external
c        # csfs for this path
c
         if ( index(ipth) .lt. 0 ) then
            numpth = numpth + 1
         else
            cmpsym = sym21(indsym(stconf+numcnf+1))
            if (sgtype.eq.wtype) then
               pthcsf = cnfw(cmpsym)
c              scrcsf = dmatw(cmpsym)
               scrcsf = 0
               lconfw = lconfw + pthcsf
            elseif (sgtype.eq.xtype) then
               pthcsf = cnfx(cmpsym)
c              scrcsf = dmatx(cmpsym)
               scrcsf = 0
               lconfx = lconfx + pthcsf
            elseif (sgtype.eq.ytype) then
               pthcsf = nbas(cmpsym)
               scrcsf = 0
               lconfy = lconfy + pthcsf
            elseif (sgtype.eq.ztype) then
               pthcsf = 1
               scrcsf = 0
               lconfz = lconfz + pthcsf
            endif
            if (pthcsf.le.0) then
               conft(stconf+numcnf+1) = -1
               index(ipth)        = -1
               call bummer('prepd: zero externals for path no.',
     &          ipth, wrnerr )
c
c              # account for bad path in index vector
c
               numpth = numpth + 1
            else
               conft(stconf+numcnf+1) = numcsf + cistrt
c              segspc = 2*atebyt(numcsf + pthcsf) + forbyt(numpth+1)
               segspc = 2*atebyt(pthcsf) + forbyt(numpth+1)
               if (segspc.gt.maxsiz .or. numcnf.ge.maxcnf
     .                       .and. index(ipth).eq.2 ) then
c
c                 # assign values for the segment up to ipth-1
c
                  if ( iseg .eq. mxseg ) then
                     write (*,7000) mxseg,iseg
                     call bummer('prepd: mxseg=', mxseg, 2 )
                  endif
                  iseg = iseg + 1
                  segel(iseg,slice) = numpth
                  cist (iseg,slice) = cistrt
                  icnfst(iseg,slice)= stconf
                  isgcnf(iseg,slice)= numcnf
                  segci (iseg,slice)= numcsf
                  segtyp(iseg,slice)= sgtype
                  segscr(iseg,slice)= numdmat
                  ipthst(iseg,slice)= index1
c
c                 # write out index vector for this segment
c
                  call pruneseg(sgtype,iseg,index(index1),numpth,
     &                      index1,heap,maxfree,conft(stconf+1),
     .                      indsym(stconf+1),numcnf,
     .                      nwalknew,bufsiz,indxlen)
                  segel(iseg,slice) = nwalknew
                  drtlen(iseg,slice) = forbyt(bufsiz)
                  drtidx(iseg,slice) = indxlen
                  drtst(iseg,slice) =  strtdrt
                  if((strtdrt+forbyt(bufsiz)-1).gt.
     .            forbyt(drtsiz).or.slice.gt.7) then
                    write(nlist,*)'slice,strtdrt+forbyt(bufsiz)-1,',
     .                'forbyt(drtsiz)',slice,strtdrt+forbyt(bufsiz)-1,
     .            forbyt(drtsiz)
                    call bummer('segment: drt size error' , 0,2)
                  endif
                  If (fileloc(fldrt).eq.glar) then
                  call putvecp(drtfil,slice,heap,
     .                     drtst(iseg,slice),drtlen(iseg,slice),0)
                  strtdrt= strtdrt + forbyt(bufsiz)
                  elseif(fileloc(fldrt).eq.ldsk) then
                  call diraccnew(drtfil,2,heap,drtlen(iseg,slice),
     .                   dalen(drtfil),drtst(iseg,slice),strtdrt)
                  strtdrt=strtdrt+1
                  else
                   call bummer('segment: invalid fileloc(fldrt)',0,2)
                  endif

c
c                 # set-up values for the potential next segment
c                 # beginning with ipth
c
                  strtvc = strtvc + ((numcsf-1)/dalen(nvfile)+ 1)
                  index1 = index1 + numpth
                  numpth = 1
                  cistrt = cistrt + numcsf
                  numcsf = pthcsf
                  stconf = stconf + numcnf
                  numcnf = 1
ctm
                  numdmat=scrcsf
ctm
               else
c
c                 # add this path to the segment
c
                  numpth = numpth + 1
                  numcnf = numcnf + 1
                  numcsf = numcsf + pthcsf
ctm
              numdmat = numdmat + scrcsf
ctm
               endif
            endif
         endif
140   continue
c
c
      if ( numpth .gt. 0 ) then
         if ( iseg .eq. mxseg ) then
            write (*,7000) mxseg,iseg
            call bummer('prepd: (iseg-mxseg)=', (iseg-mxseg), 2 )
         endif
         iseg = iseg + 1
         segel(iseg,slice) = numpth
         cist (iseg,slice) = cistrt
         icnfst(iseg,slice)= stconf
         isgcnf(iseg,slice)= numcnf
         segci(iseg,slice) = numcsf
         segtyp(iseg,slice)= sgtype
         segscr(iseg,slice) = numdmat
         ipthst(iseg,slice)= index1
c
c        # write out index vector for this segment
         call pruneseg(sgtype,iseg,index(index1),numpth,
     &                      index1,heap,maxfree,conft(stconf+1),
     .                      indsym(stconf+1),numcnf,
     .               nwalknew,bufsiz,indxlen)
          segel(iseg,slice) = nwalknew
          drtlen(iseg,slice) =forbyt(bufsiz)
          drtidx(iseg,slice) = indxlen
          drtst(iseg,slice) = strtdrt
                  if((strtdrt+forbyt(bufsiz)-1).gt.
     .            forbyt(drtsiz).or.slice.gt.7) then
                    write(nlist,*)'slice,strtdrt+forbyt(bufsiz)-1,',
     .                'forbyt(drtsiz)',slice,strtdrt+forbyt(bufsiz)-1,
     .                forbyt(drtsiz)
                    call bummer('segment: drt size error' , 0,2)
                  endif
          if (fileloc(fldrt).eq.glar) then
          call putvecp(drtfil,slice,heap,
     .             drtst(iseg,slice),drtlen(iseg,slice),0)
           strtdrt = strtdrt + forbyt(bufsiz)
           elseif(fileloc(fldrt).eq.ldsk) then
           call diraccnew(drtfil,2,heap,drtlen(iseg,slice),
     .             dalen(drtfil),drtst(iseg,slice),strtdrt)
           strtdrt=strtdrt+1
           else
            call bummer('segment: invalid fileloc(fldrt)',0,2)
           endif
         strtvc = strtvc + ((numcsf-1)/dalen(nvfile)+ 1)
      endif

       nsegt(isgtype)=iseg-segnumber
       segnumber=iseg

1000  continue
c
c     # validate number of determined walks
      nwalks = 0
      do 150 i = 1, iseg
         nwalks = nwalks + isgcnf(i,slice)
150   continue
      if (llenci.gt.0) then
      if ( nwalks .ne. nvalz ) then
         write(6,7001) nwalks,nvalz
         call bummer('prepd: (nwalks-nvalz)=',(nwalks-nvalz), 2)
      endif
      else
      if ( nwalks .ne. nvalwk ) then
         write(6,7001) nwalks,nvalwk
         call bummer('prepd: (nwalks-nvalwk)=',(nwalks-nvalwk), 2)
      endif
      endif
c
c     # write a segmentation summary in tabular form
c
      write(6,6101) 'segmentation summary for type ',
     .  segtitle
      write(6,6102)
      write(6,6105) 'seg.','no. of','no. of','starting',
     + 'internal','starting'
     + , 'starting'
      write(6,6105) 'no.','internal','ci','csf',
     + 'walks','walk', 'DRT'
      write(6,6105) ' ','paths','elements','number',
     + '/seg.','number','record'
      write(6,6102)
      do 160 i = 1, iseg
         write (6,6107) amap(segtyp(i,slice)),
     .    i,segel(i,slice),segci(i,slice),
     +    cist(i,slice),isgcnf(i,slice),
     .    icnfst(i,slice), drtst(i,slice)
         write(6,6102)
  160   continue
c
6101  format(///t20,2a)
6102  format(1x,79('-'))
6105  format(1x,a4,t8,6(a10,'|'))
6107  format(1x,a2,i2,t8,6(i10,'|'))


cvp
7201  format(///t20,a)
7202  format(1x,79('-'))
7205  format(1x,a4,t8,8(a10,'|'))
7207  format(1x,a2,i2,t8,8(i10,'|'))
cvp
c
c     # end summary
c
c      write(6,6202)(index(i),i=1,nintpt)
c      write(6,6204)(conft(i),i=1,nvalz+nvaly+nvalx+nvalw)
c      write(6,6205)(indsym(i),i=1,nvalz+nvaly+nvalx+nvalw)
6202  format(' index vector'/(1x,20i6))
6204  format(' conft vector'/(1x,20i6))
6205  format(' indsymvector'/(1x,20i6))
      if (lconfz.ne.confz)
     . call bummer('inconsitent confz',lconfz,2)
       if (llenci.gt.0 ) goto 7301
      if (lconfy.ne.confy)
     . call bummer('inconsitent confy',lconfy,2)
      if (lconfw.ne.confw)
     . call bummer('inconsitent confw',lconfw,2)
      if (lconfx.ne.confx)
     . call bummer('inconsitent confx',lconfx,2)

7301  dimci = confz + confy + confx + confw
      lenci = dimci
      confyz=confz+confy
c
c     write(*,6203) dimci
6203  format(/'dimension of the ci-matrix ->>>',i10/)
C HPM   return
c       call _f_hpm_stop_(30,12302)
C HPM
      return
7000  format(' program-defined mxseg (',i3,') will be exceeded',
     + 'following iseg = ',i3)
7001  format(' error in prepd :  nwalks',i8,' nvalwk',i8,' stop')
      end
      subroutine multcore(
     & nvfile, vrecd,  nhvfil, hvrecd,
     & reflst, heap,   totfre )
c
c  compute a hamiltonian matrix-vector product.
c
c   aodriven is implemented here. vp- 18-apr-95
c
c iskip.ge. 1     normal calculation
c       eq. 0     'bk' calculation
c       eq.-1     calculation only for z walks
c       eq.-2     construct h matrix on common jpathz,
c                   including diagonal elements!
c       eq.-3     only reflst(*) elements
c       eq.-4     return diagonal elements in heap(1..)
c    #  bra >= ket !
c
c     nvfile : unit number with ci vector
c     vrecd : current logical ci vector
c     nhvfil : unit number for sigma vector
c     hvrecd : current logical sigma vector (if producing sig vector)
c     reflst(*) int list of refs (tagged z walks; -1 = non ref)
c     indsym(*) symmetry of valid csf(i)
c     conft(*) map of valid csf to expansion location
c     heap(*) free space left for dynamic allocation
c     totfre    amount of free heap space left
c
         IMPLICIT NONE

C====>Begin Module MULTCORE               File multcore.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
C
      INTEGER             ATEBYT,      FORBYT
C
C     Parameter variables
C
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             TIME0X
      PARAMETER           (TIME0X = 0)
      INTEGER             TIME1X
      PARAMETER           (TIME1X = 1)
      INTEGER             TIME2X
      PARAMETER           (TIME2X = 2)
      INTEGER             TIME3X
      PARAMETER           (TIME3X = 3)
      INTEGER             TIME4X
      PARAMETER           (TIME4X = 4)
      INTEGER             TIMESO0X
      PARAMETER           (TIMESO0X = 5)
      INTEGER             TIMESO1X
      PARAMETER           (TIMESO1X = 6)
      INTEGER             TIMESO2X
      PARAMETER           (TIMESO2X = 7)
      INTEGER             TIMED
      PARAMETER           (TIMED = 8)
      INTEGER             TIMEFTD
      PARAMETER           (TIMEFTD = 9)
      INTEGER             TIMEFT0
      PARAMETER           (TIMEFT0 = 10)
      INTEGER             TIMEFT1
      PARAMETER           (TIMEFT1 = 11)
      INTEGER             TIMEFT2
      PARAMETER           (TIMEFT2 = 12)
      INTEGER             TIMEFT3
      PARAMETER           (TIMEFT3 = 13)
      INTEGER             XSEG
      PARAMETER           (XSEG = 1)
      INTEGER             WSEG
      PARAMETER           (WSEG = 2)
      INTEGER             SG1TYP
      PARAMETER           (SG1TYP = 1)
      INTEGER             SG2TYP
      PARAMETER           (SG2TYP = 2)
      INTEGER             YZBYYZ
      PARAMETER           (YZBYYZ = 3)
      INTEGER             YZBYS1
      PARAMETER           (YZBYS1 = 4)
      INTEGER             YZBYS2
      PARAMETER           (YZBYS2 = 5)
      INTEGER             S1BYS1
      PARAMETER           (S1BYS1 = 6)
      INTEGER             S1BYS2
      PARAMETER           (S1BYS2 = 7)
      INTEGER             S2BYS2
      PARAMETER           (S2BYS2 = 8)
      INTEGER             TMINIT
      PARAMETER           (TMINIT = 1)
      INTEGER             TMPRT
      PARAMETER           (TMPRT = 2)
      INTEGER             TMREIN
      PARAMETER           (TMREIN = 3)
      INTEGER             TMCLR
      PARAMETER           (TMCLR = 4)
      INTEGER             TMSUSP
      PARAMETER           (TMSUSP = 5)
      INTEGER             TMRESM
      PARAMETER           (TMRESM = 6)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      INTEGER             MAXTASK
      PARAMETER           (MAXTASK = 16384)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
C
C     Argument variables
C
      INTEGER             HVRECD,      NHVFIL,      NVFILE
      INTEGER             REFLST(*),   TOTFRE,      VRECD
C
      REAL*8              HEAP(*)
C
C     Local variables
C
      CHARACTER*20        DATESTR
      CHARACTER           SEGCHAR(4)
      CHARACTER*20        TIMESTR
C
      INTEGER             ADR1,        ADR2,        ADR3,        ADR3A
      INTEGER             ADR3B,       ADR4,        ADR5,        ADR6
      INTEGER             ADR6A,       ADR6B,       ADRBUF
      INTEGER             ADRCI(4),    ADRCIB,      ADRCIK,      ADRF
      INTEGER             ADRSG(4),    ADRSGB,      ADRSGK
      INTEGER             ADRSTART,    COMBTASK(10),             FILDIA
      INTEGER             FIN,         FIRSTX,      I,           ICNT
      INTEGER             II,          III,         IMULTC,      ISEG
      INTEGER             ITASK,       KSG1,        KSG2,        LOC1
      INTEGER             LOC2,        NCOMBTASK,   NSEGMX
      INTEGER             SEGPATH(MXNSEG),          SGCIST(2)
      INTEGER             SGCSF(2),    SGINFO(9),   SGLEN(2)
      INTEGER             SGNCI(2),    SGNCSF(2),   SGPATH(2),   SGPTH1
      INTEGER             SGPTH2,      SGSCR(2),    SL1,         SL2
      INTEGER             TASKID,      VALIDTASK,   X
C
      REAL*8              DDOT_WR
C
      INCLUDE 'allint_stat.inc'
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             DRTFIL,      FILSGM,      FLINDX,      FOCKDG
      INTEGER             PSEUDG,      TAPE6
C
      INCLUDE 'civct.inc'
      INCLUDE 'cnfspc.inc'
      INCLUDE 'counter.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'debug.inc'
      INCLUDE 'drtbra.inc'
      INCLUDE 'drtket.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'onex_stat.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'tasklst.inc'
      INCLUDE 'threex_stat.inc'
      INCLUDE 'tilda.inc'
      INCLUDE 'timerlst.inc'
      INCLUDE 'twoex_stat.inc'
      INCLUDE 'vppass.inc'
C====>End Module   MULTCORE               File multcore.f               

      equivalence (tape6,nunits(1))
      equivalence (filsgm,nunits(4))
      equivalence (flindx,nunits(14))
      equivalence (pseudg,nunits(5))
      equivalence (fockdg,nunits(17))
      equivalence (drtfil,nunits(38))
c
cvp sgscr was added to the end
c
c     sglen : length of index vector for each segment
c     sgnci : length of ci space for each segment
c     sgcist: beginning ci element for  each segment
c     sgcsf : beginning csf for each segment
c     sgncsf: number of csfs for each segment
c     sgpath: path number of internal paths for each segment
c
c
c     # sg1typ : type of segment1 (1=x,2=w,0=none)
c     # sg2typ : type of segment2 (1=x,2=w,0=none)
c
cvp
      call izero_wr(21,times,1)
      call i8zero(150*150,cnt3x_yw,1)
      call i8zero(150*150,cnt3x_yx,1)
      call i8zero(150*150,cnt2x_yy,1)
      call i8zero(150*150,cnt2x_ww,1)
      call i8zero(150*150,cnt2x_xx,1)
      call i8zero(150*150,cnt2x_wx,1)
      call i8zero(150*150,cnt2x_wz,1)
      call i8zero(150*150,cnt2x_xz,1)
      call i8zero(150*150,cnt1x_yz,1)
      call i8zero(150*150,cnt1x_yx,1)
      call i8zero(150*150,cnt1x_yw,1)
      call i8zero(150*150,cnt0x_zz,1)
      call i8zero(150*150,cnt0x_yy,1)
      call i8zero(150*150,cnt0x_xx,1)
      call i8zero(150*150,cnt0x_ww,1)
c
c # initialize check counters
c
       dg4x=0
       dg2x=0
       dg0x=0
       zz0x=0
       yy0x=0
       xx0x=0
       ww0x=0
       yz1x=0
       yx1x=0
       yw1x=0
       yy2ex=0
       xx2x=0
       ww2x=0
       xz2x=0
       wz2x=0
       wx2x=0
       yx3x=0
       yw3x=0
       zz0xso=0
       yy0xso=0
       xx0xso=0
       ww0xso=0
       yz1xso=0
       yx1xso=0
       yw1xso=0
       yy2xso=0
       xx2xso=0
       wx2xso=0
       xx2xso2=0
C
cvp
      adr6   = 0
cvp
      dotild = .true.
c
c     # set some variables for commons.
c
c     # default values for segment a and b characteristics
c
      do 700 iseg = 1, 2
         sglen(iseg)  = 0
         sgnci(iseg)  = 0
         sgcist(iseg) = 0
         sgcsf(iseg)  = 0
         sgncsf(iseg) = 0
         sgscr(iseg)  = 0
         sgpath(iseg) = 0
700   continue
      do 710 i = 1, 9
         sginfo(i) = 0
710   continue

       adrstart=1
       do i=1,4
        adrci(1)=adrstart
        adrsg(1)=adrstart
       enddo
       adr1  = adrstart 

            do 1000 itask=1,ntask

          call ptimer('tmult','evin',itask)
          call ptimer('tloop','evin',itask)
          call ptimer('tloop','susp',itask)
          call ptimer('tint','evin',itask)
          call ptimer('tint','susp',itask)
          call ptimer('tmnx','evin',itask)
          call ptimer('tmnx','susp',itask)

c
c               remove all tasks incompatible with iskip modes
c               check SO contributions as well
c
            if (iskip.eq.-1) then
c               # iskip = -1  allin + diag(z) only
                x=task(itask)
               if (.not.(x.eq.1 .or. x.eq.75 .or. x.eq.81 ))
     .             goto 1000
c              if (task(itask).gt.10 .or. task(itask).lt.50) goto 1000
             endif
            if (iskip.eq.0) then
c               # iskip = 0  diag allin(zz),onex (yz),twoex (wx,xz) only
c                            so-0ext(zz) so-1ext(zy)
              x=task(itask)
              if (.not.(x.eq.1 .or. x.eq.11 .or. x.eq.24 .or. x.eq.25
     .          .or. (x.ge.45 .and. x.le.81) .or. x.eq.91 )) goto 1000
            endif
            if (iskip.eq.-2 .or. iskip.eq.-3 ) then
              if ((task(itask).ne.1).and.(task(itask).ne.75)
     .          .and. (task(itask).ne.81) ) goto 1000
            endif

             if (iskip.eq.-4) then
c               # iskip = 0  diag only (bk)
                 if (.not.(task(itask).ge.45 .and. task(itask).lt.80))
     .             goto 1000
              endif

             if(.not.spnorb .or. skipso) then
                 if (task(itask).gt.80) goto 1000
              else
               call sotbl
             endif



               currtsk=itask
c              write(6,*) 'multcore: itask=',itask,task(itask)


               ksg1 = kseg(itask)
               sl1  = slice(itask)

               sglen(2)  = segel(ksg1,sl1)
               sgnci(2)  = segci(ksg1,sl1)
               sgcist(2) = cist(ksg1,sl1)
               sgcsf(2)  = icnfst(ksg1,sl1)
               sgncsf(2) = isgcnf(ksg1,sl1)
               sgscr(2)  = segscr(ksg1,sl1)
               sgpath(2) = ipthst(ksg1,sl1)-1
               sginfo(sg2typ)=segtyp(ksg1,sl1)

               if (iskip.eq.-2) then
c
c  # count the number of references
c
               icnt=0
               do i=1,nvalz
                if (reflst(i).gt.0) icnt=icnt+1
               enddo
c
c  # special case: building complete href matrix
c    in space of CI and Sigma vectors; however

c    if 2*cidim .lt. nref*(nref+1)/2 then memory
c    references are overwritten !
c    adr1 is now modified accordingly
c
c
               adr1 = max(adr1,atebyt(icnt*(icnt+1)/2)+1)
c
               adr2 = adr1+forbyt(sglen(2))
               adr3 = adr2+forbyt(sgncsf(2))
               adrcik = 1
               adrsgk = 0
               adr4  = adr3+forbyt(sgncsf(2))
               adrf = adr4
             else
               adr2 = adr1 + forbyt(sglen(2))
               adr3 = adr2+forbyt(sgncsf(2))
               adr4 = adr3+forbyt(sgncsf(2))
               adrcik =adrci(ksg1)
               adrsgk =adrsg(ksg1)
               adrf = adr4
             endif


               if (iskip.eq.-4) then
c
c  # return diagonal elements on sgma1, positioned at heap(1)
c
               adr2 = adr1+forbyt(sglen(2))
               adr3 = adr2+forbyt(sgncsf(2))
               adrcik = adrci(ksg1)
               adrsgk = 1
               adr4  = adr3+forbyt(sgncsf(2))
               adrf = adr4
               endif

               adrbuf = adrf + forbyt(drtlen(ksg1,sl1))
               if (adrbuf.gt. totfre)
     .      call bummer('insufficient space in multcore(1)',adrbuf,2)

         call getvecp(drtfil,sl1,heap(adrf),
     .                drtst(ksg1,sl1),drtlen(ksg1,sl1))

c
         call unpackbuffer(1,heap(adrf),lket,yyket,xbarket,
     .   heap(adr1),drtidx(ksg1,sl1),heap(adr2),heap(adr3),sgncsf(2),
     .   drtlen(ksg1,sl1),invldket)


              ksg2=bseg(itask)
              sl2 =slice(itask)
c
                  sglen(1) = segel(ksg2,sl2)
                  sgnci(1) = segci(ksg2,sl2)
                  sgcist(1)= cist(ksg2,sl2)
                  sgcsf(1) = icnfst(ksg2,sl2)
                  sgncsf(1)= isgcnf(ksg2,sl2)
                  sgpath(1)= ipthst(ksg2,sl2)-1
                  sgscr(1) = segscr(ksg2,sl2)
                  sginfo(sg1typ)=segtyp(ksg2,sl2)

c
c             Tasktypes
c
c             d4ex    y (52)  x (53)   w (54)
c             d2ex    y (62)  x (63)   w (64)
c             d0ex    z (71)  y (72)   x (73)  w (74)
c             d024ex  z (75)  y (76)   x (77)  w (78)
c
c             4ex     z (41)  y( 42)   x (43)  w (44)
c             3ex     yx (31) yw(32)
c             2ex     yy (21) xx(22)  ww (23)  xz (24)  wz (25)  wx (26)
c             1ex     yz (11) yx (13)  yw (14)
c             0ex     zz (1)  yy (2)   xx (3)  ww (4)
c
c             so0ex   z (81)  y (82)   x (83)   w (84)
c             so1ex   zy(91)  yx (93)  yw(94)
c             so2ex   yy(101) xx (102) wx (103)
c
c  combinations:
c            d024ex4ex  y (45) x (46) w (47)
c            1ex3ex     yx(15) yw (16)
c            2ex0ex     yy(5)  xx (6)  ww(7)
c


c
c   #  diagonal case
c


       if (ksg2.eq.ksg1) then

                  adr4=adr1
                  adr5=adr2
                  adr6=adr3
                  adrcib=adrcik
                  adrsgb=adrsgk
c
c  # make sure that lket,yyket and xbarket are properly initialized
c  # (better copy the commons DRTBRA to DRTKET)
c
         call unpackbuffer(1,heap(adrf),lbra,yybra,xbarbra,
     .   heap(adr4),drtidx(ksg1,sl1),heap(adr5),heap(adr6),sgncsf(1),
     .   drtlen(ksg1,sl1),invldbra)

c
          else
c  # offdiagonal case
                if (iskip.eq.-2) then
                  adr4 = adrf
                  adr5=adr4+forbyt(sglen(1))
                  adr6=adr5+forbyt(sgncsf(1))
                  adrf = adr6 + forbyt(sgncsf(1))
                  adrcib=1
                  adrsgb=adrsg(ksg2)
                else
                  adr4 = adrf
                  adr5=adr4+forbyt(sglen(1))
                  adr6=adr5+forbyt(sgncsf(1))
                  adrf = adr6 + forbyt(sgncsf(1))
                  adrcib=adrci(ksg2)
                  adrsgb=adrsg(ksg2)
                endif

c
c                 # get index and ci and sigma vectors if needed
c
                adrbuf = adrf + forbyt(drtlen(ksg2,sl2))
             if (adrbuf.gt. totfre)
     .      call bummer('insufficient space in multnew(2)',adrbuf,2)
         call getvecp(drtfil,sl2,heap(adrf),
     .                drtst(ksg2,sl2),drtlen(ksg2,sl2))
c
         call unpackbuffer(1,heap(adrf),lbra,yybra,xbarbra,
     .   heap(adr4),drtidx(ksg2,sl2),heap(adr5),heap(adr6),sgncsf(1),
     .   drtlen(ksg2,sl2),invldbra)


         endif

              iket =ksg1
              ibra =ksg2

         loopcnt(currtsk)=0
       if (task(itask).eq.75) then
c
c         diag: dg0ext(z)
c
         combtask(1)=71
         ncombtask=1
       elseif (task(itask).ge.76 .and. task(itask).le.78 ) then
c
c          diag: dg0ext+dg2ext+dg4ext
c
         combtask(1)=task(itask)-4
         combtask(2)=task(itask)-14
         combtask(3)=task(itask)-24
         ncombtask=3
        elseif (task(itask).ge.5 .and. task(itask).le.7) then
c
c          0ex+2ex yy,xx,ww
c
c
         combtask(1)=21+task(itask)-5
         combtask(2)=2+task(itask)-5
         ncombtask=2
        elseif(task(itask).ge.45 .and. task(itask).le.47) then
c
c         dgex+4ex yy,xx,ww
c
c
         combtask(1)=72+task(itask)-45
         combtask(2)=62+task(itask)-45
         combtask(3)=52+task(itask)-45
         combtask(4)=42+task(itask)-45
         ncombtask=4

        elseif(task(itask).eq.15 .or. task(itask).eq.16) then

c
cc        1ex3ex yx,yw
c
         combtask(1)=31+task(itask)-15
         combtask(2)=13+task(itask)-15
         ncombtask=2
        else
         combtask(1)=task(itask)
         ncombtask=1
        endif

          do i=1,ncombtask
            if (iskip.eq.-1 .and. combtask(i).gt.4
     .             .and.combtask(i).ne.71 ) combtask(i)=-1
            if (iskip.eq.-4 .and. combtask(i).lt.50) combtask(i)=-1
            if (iskip.eq.0 .and. combtask(i).ne.1
     .         .and. combtask(i).ne.11 .and. combtask(i).ne.24
     .         .and. combtask(i).ne.25 .and.(.not.(combtask(i).gt.50
     .         .and. combtask(i).le.81)) .and. combtask(i).ne.91   )
     .          combtask(i)=-1
c         write(6,*) 'multcore: combtask',i,combtask(i)
          enddo



                call ptimer('tmnx','resm',itask)
                  call multnxnew(
     &             reflst,     heap(adr5), heap(adr6),
     &             heap(adr4),  heap(adrcib),
     &             heap(adrsgb),  heap(adr2), heap(adr3),
     &             heap(adr1), heap(adrcik),  heap(adrsgk),
     &             sglen,      sgnci,      sgcist,      sgcsf,
     &             sgncsf,     sgpath,     sginfo,      times,
     &             iskip,      heap(adrf), (totfre-adrf), sgscr,
     &             ncombtask, combtask )
                call ptimer('tmnx','susp',itask)
c
            call ptimer('tmult','stop',itask)
1000        continue
            call ptimer('twait','resm',1)

c
c      #  write sigma vectors to file
c
1005  continue
      write(tape6,8001)  DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X,
     . YZ1X,YX1X,YW1X,YY2eX,WW2X,XX2X,XZ2X,WZ2X,WX2X,YX3X,YW3X
8001  format(//25('====')
     .     /'Diagonal     counts:  0x:',i10,' 2x:',i10,' 4x:',i10,/
     .       'All internal counts: zz :',i10,' yy:',i10,' xx:',i10,
     .        ' ww:',i10,/
     .       'One-external counts: yz :',i10,' yx:',i10,' yw:',i10,/
     .       'Two-external counts: yy :',i10,' ww:',i10,' xx:',i10,
     .        ' xz:',i10,' wz:',i10,' wx:',i10,/
     .       'Three-ext.   counts: yx :',i10,' yw:',i10)
      write(tape6,8002) ZZ0XSO,YY0XSO,XX0XSO,WW0XSO,
     .       YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
8002  format(/'SO-0ex       counts: zz :',i10,' yy:',i10,' xx:',i10,
     .     ' ww:',i8,/
     .         'SO-1ex       counts: yz :',i10,' yx:',i10,' yw:',i10,/
     .         'SO-2ex       counts: yy :',i10,' xx:',i10,' wx:',i10
     .     /25('====')//)
        write(tape6,*) 'xx2xso2=',xx2xso2
        write(tape6,8003)
        write(tape6,8004) (i,loopcnt(i)-1,i=1,ntask)
8003  format(//'LOOPCOUNT per task:')
8004  format(4('task #',i4,':',i10,4x))
      do i=1,ntask
        if (loopcnt(i).gt.0) loopcnt(i)=1
      enddo

      call ptimer('twait','stop',1)

        open(unit=4,file='SEGANALYSE',status='unknown',form='formatted')
        write(4,1012) nvalz,nvaly,nvalx,nvalw
1012    format('NVALZYXW=',4i10)
        write(4,1013) nmsym
1013    format('NSYM=',i4)
        write(4,1014) (nbas(i),i=1,nmsym)
1014    format('NEXTERN=',8i6)
        write(4,1015) lenci
1015    format('CI-DIMENSION=',i12)
        write(4,1016) isize_zz,isize_yy,isize_xx,isize_ww
1016    format('BATCHSIZE_ZYXW=',4i10)
        write(4,1017) 'CSFBATCH_Z='
        write(4,1001) (clen_zz(i),i=1,(nvalz-1)/isize_zz +1)
        write(4,1017) 'CSFBATCH_Y='
        write(4,1001) (clen_yy(i),i=1,(nvaly-1)/isize_yy +1)
        write(4,1017) 'CSFBATCH_X='
        write(4,1001) (clen_xx(i),i=1,(nvalx-1)/isize_xx +1)
        write(4,1017) 'CSFBATCH_W='
        write(4,1001) (clen_ww(i),i=1,(nvalw-1)/isize_ww +1)
1001    format(5I10)
1017    format(a11)

        call printstat(cnt2x_yy,'2x_yy',nvaly,nvaly,isize_yy,isize_yy)
        call printstat(cnt2x_xx,'2x_xx',nvalx,nvalx,isize_xx,isize_xx)
        call printstat(cnt2x_ww,'2x_ww',nvalw,nvalw,isize_ww,isize_ww)
        call printstat(cnt2x_wx,'2x_wx',nvalw,nvalx,isize_ww,isize_xx)
        call printstat(cnt2x_wz,'2x_wz',nvalw,nvalz,isize_ww,isize_zz)
        call printstat(cnt2x_xz,'2x_xz',nvalx,nvalz,isize_xx,isize_zz)

        call printstat(cnt1x_yz,'1x_yz',nvaly,nvalz,isize_yy,isize_zz)
        call printstat(cnt1x_yw,'1x_yw',nvaly,nvalw,isize_yy,isize_ww)
        call printstat(cnt1x_yx,'1x_yx',nvaly,nvalx,isize_yy,isize_xx)

        call printstat(cnt0x_zz,'0x_zz',nvalz,nvalz,isize_zz,isize_zz)
        call printstat(cnt0x_yy,'0x_yy',nvaly,nvaly,isize_yy,isize_yy)
        call printstat(cnt0x_xx,'0x_xx',nvalx,nvalx,isize_xx,isize_xx)
        call printstat(cnt0x_ww,'0x_ww',nvalw,nvalw,isize_ww,isize_ww)

        call printstat(cnt3x_yx,'3x_yx',nvaly,nvalx,isize_yy,isize_xx)
        call printstat(cnt3x_yw,'3x_yw',nvaly,nvalw,isize_yy,isize_ww)

        close(4)
      return
      end

       subroutine printstat(cnt2x,typ,nvali,nvalj,ibatch1,ibatch2)
       integer*8 sum,cnt2x(150,150)
       character*(*) typ
        integer i,j,k,nvali,nvalj,ibatch1,ibatch2

        sum=0
        write(4,*) '**** BEGIN ANALYSIS FOR ',
     .           typ,' ****'
        do i=1,(nvali-1)/ibatch1+1
           do j=1,(nvalj-1)/ibatch2+1
             if (cnt2x(i,j).gt.0) then 
       write(4,100) 
     . (i-1)*ibatch1+1,i*ibatch1,(j-1)*ibatch2,j*ibatch2,cnt2x(i,j)
             sum=sum+cnt2x(i,j)
             endif
           enddo
         enddo
 100    format('[',i12,'..',i12,'],[',i12,'..',i12,']=',i12)
        write(4,*) 'SUM OF ALL ', typ, 'contributions:',sum
        write(4,*) '**** END ANALYSIS FOR ', typ,'****'
        return
        end






        subroutine i8zero(idim,iarr,inc)
        implicit none
        integer i,idim,inc
        integer*8 iarr(idim)
        if (inc.ne.1) call bummer('i8zero, invalid inc',0,2)
        do i=1,idim
           iarr(i)=0
        enddo
        return
        end

        
