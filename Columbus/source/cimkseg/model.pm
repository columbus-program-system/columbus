package  model;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw( encode_zerotask createciudgin partition_data refine_tasks comp_buckets comp_boundaries print_tasks search_emptypatches rd_seganalyse_hdr rd_seganalyse_datablock print_2D );   # exported per default
#       @EXPORT_OK =qw();   # exported on request 
#
#==========================================================
#
   sub rd_seganalyse_hdr {
   my ($nsym,$nval_zyxw,$nextern, $nsize_zyxw, $csfbatch_z,$csfbatch_y,$csfbatch_x,$csfbatch_w,$statseg);
   my ($i,$tmpz,@tmpz);
   ($nsym,$nval_zyxw,$nextern, $nsize_zyxw, $csfbatch_z,$csfbatch_y,$csfbatch_x,$csfbatch_w,$statseg) = @_;
   
   open FIN, "<SEGANALYSE";
        $_=<FIN>; if ( ! /NVALZYXW/) {die ("inconsistent SEGANALYSE file \n");}
        chop; s/^.*= *//; @$nval_zyxw=split(/\s+/,$_);
        $_=<FIN>; if ( ! /NSYM/) {die ("inconsistent SEGANALYSE file \n");}
        chop; s/^.*= *//; @tmpz=split(/\s+/,$_); $nsym=shift @tmpz;

         $_=<FIN>; if ( ! /NEXTERN/) {die ("inconsistent SEGANALYSE file \n");}
         chop; s/^.*= *//; @$nextern=split(/\s+/,$_);
         $_=<FIN>;

         $_=<FIN>; if ( ! /BATCHSIZE_ZYXW/) {die ("inconsistent SEGANALYSE file \n");}
         chop; s/^.*= *//; @$nsize_zyxw=split(/\s+/,$_);
 
         $_=<FIN>; if ( ! /CSFBATCH_Z/) {die ("inconsistent SEGANALYSE file Z: $_\n");}
            $tmpz=" ";
            while (<FIN>)  { if ( /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
            $tmpz =~ s/^ *//;
            $tmpz =~ s/ *$//;
            @$csfbatch_z = split(/\s+/,$tmpz);
            unshift @$csfbatch_z, -1;

        if ( ! /CSFBATCH_Y/) {die ("inconsistent SEGANALYSE file Y:$_ \n");}
            $tmpz=" ";
            while (<FIN>)  { if (  /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
            $tmpz =~ s/^ *//;
            $tmpz =~ s/ *$//;
            @$csfbatch_y = split(/\s+/,$tmpz);
            unshift @$csfbatch_y, -1;

        if ( ! /CSFBATCH_X/) {die ("inconsistent SEGANALYSE file X:$_ \n");}
            $tmpz=" ";
            while (<FIN>)  { if (  /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
            $tmpz =~ s/^ *//;
            $tmpz =~ s/ *$//;
            @$csfbatch_x = split(/\s+/,$tmpz);
            unshift @$csfbatch_x, -1;

        if ( ! /CSFBATCH_W/) {die ("inconsistent SEGANALYSE file W:$_ \n");}
           $tmpz=" ";
           while (<FIN>)  { if ( /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
           $tmpz =~ s/^ *//;
           $tmpz =~ s/ *$//;
           @$csfbatch_w = split(/\s+/,$tmpz);
           unshift @$csfbatch_w, -1;
       
       close FIN;

       for ($i=0; $i<4; $i++) { $$statseg[$i]=int(($$nval_zyxw[$i]-1)/$$nsize_zyxw[$i])+1;}
       start_tracker("rd_seganalyse_hdr");
       printf "number of irred. repres.      : %10d \n", $nsym; 
       printf "nmb. external orb./irrep     : ";
       for ($i=0;$i<$nsym;$i++) {printf " %10d",$$nextern[$i];} print "\n"; 
       printf "number of valid internal walks: %10d %10d %10d %10d \n",@$nval_zyxw;
       printf "batch sizes                   : %10d %10d %10d %10d \n",@$nsize_zyxw;
       printf "typical CSF batch size        : %10d %10d %10d %10d \n",$$csfbatch_z[1],$$csfbatch_y[1],$$csfbatch_x[1],$$csfbatch_w[1];
       printf "number of statistic segments  : %10d %10d %10d %10d \n",@$statseg;
       end_tracker("rd_seganalyse_hdr");

#
#       Der erste Wert des csfbatch arrays [0] = -1
#

       return 0 ;
  }

########################################################################################################################################

sub rd_seganalyse_datablock { 
 my($segtype,$pairtype,$array,$ibatch,$jbatch, $iseg, $jseg)=@_;
 my ($tmp,$s,$p,$ilo,$ihi,$jlo,$jhi,$val,$i,$j,$foundentry);

#
#  IN:  $segtype = 0x,1x,2x,3x,4x,dg
#       $pairtype = zz,yy,xx,ww,...
#       $ibatch,$jbatch = batchsize i,j
#       $iseg,$jseg = maximum number of segments i ,j
#  OUT: $array[i][j] :  #loops for batch (i,j)


open FIN,"<SEGANALYSE";
$foundentry=0;
while (<FIN>)
  {  if (/BEGIN ANALYSIS FOR/) { chop;s/^.*FOR//; $tmp=$_;
                              s/^ *//; s/ .*$//; ($s,$p)=split(/_/,$_);
                           if ($s eq $segtype && $p eq $pairtype)
                               { $foundentry=1; 
                                while (<FIN>)
                                { if (/SUM OF/) {last;}
                                  chop; s/[\[\.,\]=]/ /g; s/^ *//;
                                  ($ilo,$ihi,$jlo,$jhi,$val)=split(/\s+/,$_);
                                  $i=int($ihi/$ibatch+0.5); $j=int($jhi/$jbatch+0.5);
                                  if ($i > $iseg) {die("rd_seganalyse_dataablock: invalid i stat segment index $i vs. $iseg\n");}
                                  if ($j > $jseg) {die("rd_seganalyse_dataablock: invalid j stat segment index $j vs. $jseg\n");}
                                  $$array[$i][$j]=$val;
                               #print "P( $i,$j ) = $val\n";
                                }
                              }
                           next;}
  }
 close FIN;

 if (! $foundentry ) {die("rd_seganalyse_dataablock: cannot find entry $segtype $pairtype\n");}

 $zero=0;
 open TEMP,">$segtype.$pairtype.scilab";
  for ($j=1; $j <= $jseg; $j++) { for ($i=1; $i<= $iseg; $i++) { if (! $$array[$i][$j]) {$zero++;}
                                                                 printf TEMP "%10d  ",$$array[$i][$j]  ;} print TEMP "\n"; }
 print TEMP "\n";
 close TEMP;

 print "SCILAB written: $segtype.$pairtype  #segments=$jseg,$iseg   zeroes=";
 printf "%10d ( %6.2f \% ) \n", $zero,$zero*100/($jseg*$iseg);

 return ;
}

#############################################################################################################################################

sub search_emptypatches {

  my ($iseg,$jseg,$datablock,$skipsize,$skipx,$skipy,$assoc,$patch,$xpatch)=@_;

# find empty patches
# input  $iseg,$jseg: number of segments
#        @datablock [i][j]  number of contributions for seg i j combination 
#        $skipsize = all empty patches below $skipsize segment pairs are marked by -1 contributions
#        $skipy    = all empty patches with y dimension below $skipy are marked by -1 contributions
#        $skipx    = all empty patches with x dimension below $skipx are marked by -1 contributions
#
# output @assoc[i][j] = $clusterid
#        @patch[cnt][0..3] = coordinates of patch cnt with xup,yup,xlo,ylo (0..3)
#        $npatch    0 number of patches found
#        @datablock  empty patches to be ignored (due to their size) are set to -1 (instead of 0) 
#    
#
@$assoc=();
@$patch=();
$npatch=0;

 my ($izero,$jzero,$val);
 my ($ok,$notok,$i,$j,$xup,$yup,$xlo,$ylo,$npatch);

  while (1)
  {
  ($izero,$jzero)=getzerotask($datablock,$assoc,$iseg,$jseg);
  if (!( $izero + $jzero)) {last;}  # no more empty patches 
  ($xup,$yup,$xlo,$ylo)=makecluster($datablock,$assoc,$izero,$jzero,$iseg,$jseg);

  if (  ($xlo-$xup+1)*($ylo-$yup+1) <= $skipsize  || ($xlo-$xup+1) <= $skipx || ($ylo-$yup+1) <= $skipy )
   # skipping task
   { for ($i=$xup; $i<=$xlo; $i++) { for ($j=$yup; $j<=$ylo; $j++) { $$datablock[$i][$j]=-1;}}
   }
   else
     # updating $assoc ,$patch array
    { $npatch++;
      for ($i=$xup; $i<=$xlo; $i++) { for ($j=$yup; $j<=$ylo; $j++) { $$assoc[$i][$j]=$npatch;}}
      $$patch[$npatch][0]=$xup;
      $$patch[$npatch][1]=$yup;
      $$patch[$npatch][2]=$xlo;
      $$patch[$npatch][3]=$ylo;
#     printf "patch %5d = %5d %5d : %5d %5d \n", $npatch,$xup,$yup,$xlo,$ylo;
    }
  }


#  check assoc vs. datablock

   $ok=0; $notok=0;
   for ($i=1; $i <=$iseg; $i++) { for ($j=1; $j<=$jseg; $j++) {
      if ($$datablock[$i][$j] > 0 ) 
            {  if (! $$assoc[$i][$j]) { $ok++;} else {$notok++;}}
   }}
   if ($notok) { die ( "search_emptypatches: assoc-datablock test failed: ok/notok $ok / $notok\n");}
   print "search_emptypatches: $npatch generated\n";
   $$xpatch=$npatch;

 return;
}
###############################################################################################################################################



  sub getzerotask
{ my ($datablock,$assoc,$iseg,$jseg)= @_;
  my ($i,$j,$ii,$jj,$iii,$jjj,$found,$sum);
  $found=0;
  for ($j=1; $j <= $jseg; $j++)
       { for ($i=1; $i<= $iseg; $i++)
            { if (( ! $$datablock[$i][$j]) && (! $$assoc[$i][$j])) { $found="$i:$j"; last; }}
        if ($found) {last;} 
       }
 if (! $found) { return (0,0);}
 ($ii,$jj)=split(/:/,$found);
 # try to shift the zero into the center of some zeropatch
 # find last zero in this line
  $j=$ii;
  $sum=0;
  while ($j <=$iseg) { $sum=$sum+$$datablock[$j][$jj]+$$assoc[$j][$jj]; if ($sum) {last;} $j++;}
  $iii=int(($j-$ii)/2)+$ii;

  $j=$jj;
  $sum=0;
  while ($j <=$jseg) { $sum=$sum+$$datablock[$iii][$j]+$$assoc[$iii][$j]; if ($sum) {last;} $j++;}
  $jjj=int(($j-$jj)/2)+$jj;
  return($iii,$jjj);
  }



################################################################################################################################################



  sub makecluster
 { my($datablock,$assoc,$izero,$jzero,$iseg,$jseg)=@_;
   my ($i,$j,$found,$xup,$yup,$xlo,$ylo,$cycle);
   # start from i,j and extend to -x +x -y +y
   $xup=$izero; $xlo=$izero; $yup=$jzero; $ylo=$jzero;
#  print "makecluster: $izero,$jzero .... $xup:$xlo $yup:$ylo\n";
   $found=1;
   $cycle=0;
   while ($found)
   { $found=0;
   if ($xup-1 > 0 ) {  # check xup-1,yup:ylo
                       $sum=0;
                       for ($j=$yup; $j<=$ylo; $j++) { $sum=$sum+$$datablock[$xup-1][$j]+$$assoc[$xup-1][$j];}
                       if ( ! $sum) { $found=1;$xup=$xup-1; }
                    }
   if ($xlo+1 <= $iseg  ) {  # check xlo+1,yup:ylo
                       $sum=0;
                       for ($j=$yup; $j<=$ylo; $j++) { $sum=$sum+$$datablock[$xlo+1][$j]+$$assoc[$xlo+1][$j];}
                       if ( ! $sum) { $found=1;$xlo=$xlo+1; }
                    }
   if ($yup-1 > 0 ) {  # check yup-1,xup:xlo
                       $sum=0;
                       for ($i=$xup; $i<=$xlo; $i++) { $sum=$sum+$$datablock[$i][$yup-1]+$$assoc[$i][$yup-1];}
                       if ( ! $sum) { $found=1;$yup=$yup-1; }
                    }
   if ($ylo+1 <= $jseg   ) {  # check ylo+1,yup:ylo
                       $sum=0;
                       for ($i=$xup; $i<=$xlo; $i++) { $sum=$sum+$$datablock[$i][$ylo+1]+$$assoc[$i][$ylo+1];}
                       if ( ! $sum) {$found=1; $ylo=$ylo+1; }
                    }
#  $cycle++; print "cycle number $cycle : range (xy)    $xup,$yup  $xlo,$ylo \n";
   }
   return ($xup,$yup,$xlo,$ylo);
  }

####################################################################################################################################################

  sub comp_boundaries {
#
#  input $patch    patch array
#        $npatch   number of patches
#        $iseg,$jseg : max segments
#  output
#        @xb  : initial x boundaries
#        @yb  : initial y boundaries
#

  my ($npatch,$patch,$iseg,$jseg,$xb,$yb)=@_;
  my ($i,@xbound,@ybound);

# print "comp_boundaries: $npatch,$iseg,$jseg\n";
 for ($i=1; $i<=$npatch; $i++)
   { $xbound[$$patch[$i][0]]=1;
     $xbound[$$patch[$i][2]]=1;
     $ybound[$$patch[$i][1]]=1;
     $ybound[$$patch[$i][3]]=1;
      }

 @$xb=();
 for ($i=1; $i<=$iseg; $i++) { if ($xbound[$i]) {push @$xb,$i;}}
 @yb=();
 for ($i=1; $i<=$jseg; $i++) { if ($ybound[$i]) {push @$yb,$i;}}
 if ($xb[0]>1) { unshift @$xb,1;}
 push @$xb, $iseg+1;
 if ($$yb[0]>1) { unshift @$yb,1;}
 push @$yb, $jseg+1;

 print "comp_boundaries: generated $#$xb x-boundaries and $#$yb y boundaries\n";
 return;
}

###################################################################################################################################################

sub comp_buckets{
 my ($name,$datablock,$xb,$yb,$iseg,$jseg,$csfbatch_i,$csfbatch_j,$wantedtasks,$zeroarray)=@_;
#
#  input $datablock : to evaluate total costs , also corrects the -1
#        @xb,@yb    : x and y boundaries
#        $iseg,$jseg : max i, jsegs
#        @csfbatch_i,_j : number of csfs for segment i,j
#        $wantedtasks:  number of desired tasks - for avtask computation
#
 my ($bsum,%buckets,$checksum,$VOLUME,$totcost,$i,$j);
%buckets=();
$zerotasks=0;
$checksum=0;
$VOLUME=0;
$totcost=0;
for ($i=1; $i<=$iseg; $i++) { for ($j=1; $j<=$jseg; $j++) {
   if ($$datablock[$i][$j] < 0) {$$datablock[$i][$j]=0;} $totcost+=$$datablock[$i][$j]; }}
$avtask=$totcost/($wantedtasks);
$checksum+= mk_buckets_q($xb,$yb,$avtask,\%buckets,$datablock,$name,$csfbatch_i,$csfbatch_j,\$VOLUME,\$zerotasks,$zeroarray);
$bsum=0;
foreach $i  (sort msort keys(%buckets)) { $bsum+= $buckets{$i}; printf "BUCKETS: size= %8.5f  number: %5d \n", 2**($i),$buckets{$i};}
printf "total number of non-zero  tasks= %6d $bsum   zerotasks= %6d VOLUME=%8.3f GB \n",$bsum,$zerotasks,$VOLUME;

 return;
}


#####################################################################################################################################################

sub mk_buckets_q
{ my($borderi, $borderj,$avtask,$buckets,$array,$txt,$icsf,$jcsf,$XVOLUME,$zerotasks,$zeroarray)=@_;
  my ($i,$j,$jj,$checksum,$b,$VOLUME,$zero_task);
  $checksum=0;
  $zero_task=0;
  $prtlevel=0;
 for ($i=0; $i<$#$borderi; $i++)
  {
    for ($j=0; $j<$#$borderj; $j++)
    {
       $val=patchij($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$array);
      $checksum+=$val;
      $b=$val/$avtask;
      if ( ! $b) {$zero_task++; my $tmp =sprintf "%-15s %5d %5d :  %5d %5d  => %5d %5d ",
                                          $txt,$$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$i+1,$j+1;
                  push @$zeroarray,$tmp ;  next;}
       $VOLUME+=volij($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$icsf,$jcsf);
      $LARGEST_TASK= $b > $LARGEST_TASK ? $b : $LARGEST_TASK;
      if ($prtlevel > 1) { printf "$txt: [  %3d:%3d,%3d:%3d ] cost %8.3f\n", $$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1 , $b;}
        $b=int(log($b)/log(2));$$buckets{"$b"}=$$buckets{"$b"}+1;
  }}
 $$XVOLUME=$VOLUME;
 $$zerotasks+=$zero_task;
   return $checksum;
}

########################################################################################################################################################



 sub patchij
 { my ($ilo,$ihi,$jlo,$jhi,$array)=@_;
   my ($b,$i,$j,$val,$jj);
   $val=0;
   for ($i=$ilo;$i<=$ihi;$i++)
    {
      for ($j=$jlo; $j<=$jhi; $j++)
     { $val+=$$array[$i][$j];}}
    { return $val;}

   return $val;
}




 sub volij
 { my ($ilo,$ihi,$jlo,$jhi,$icsf,$jcsf)=@_;
   my ($b,$i,$j,$vol,$jj);
#
#  a task is associated  v1 (ilo:ihi)  v2(jlo:jhi)
#
   $vol=0;
   for ($i=$ilo;$i<=$ihi;$i++) { $vol+= $$icsf[$i]*8e-9;}
    { for ($i=$jlo;$i<=$jhi;$i++) { $vol+= $$jcsf[$i]*8e-9;}}
   $vol=$vol+$vol;
    return $vol;
}


####################################################################################################################################################
sub refine_tasks {

my ($xb,$yb,$datablock,$iseg,$jseg,$wantedtasks,$wantxseg,$wantyseg,$redx,$redy)=@_;
my ($i,$j,$totcost,$avcost);
my ($linesy,$linesx,$lastlinex,$lastliney);

$totcost=0;
for ($i=1; $i<=$iseg; $i++) { for ($j=1; $j<=$jseg; $j++) {
   if ($$datablock[$i][$j] < 0) {$$datablock[$i][$j]=0;} $totcost+=$$datablock[$i][$j]; }}
$avtask=$totcost/($wantedtasks);
$lastlinex=1001; $lastliney=1002;

while (1)
{
  if ($redy > 1) { $linesy=combine($yb,$xb,$datablock,$wantyseg,$redy,0,$totcost,$avtask);}
  if ($redx > 1) { $linesx=combine($xb,$yb,$datablock,$wantxseg,$redx,1,$totcost,$avtask);}
  if ($linesy <= $wantyseg && $linesx <=$wantxseg) {last;}
  if ($lastlinex == $linesx && $lastliney == $linesy) {last;}
  $lastlinex=$linesx; $lastliney=$linesy;
}
 print "refine_tasks: retaining $#$xb X segments  ",join(':',@$xb),"\n";
 print "refine_tasks: retaining $#$yb Y segments  ",join(':',@$yb),"\n";

 return
}

###################################################################################################################################################


 sub combine
#
#  input:  $border border to be optimized
#          $other  border to remain constant
#          $array  datablock with cost estimates
#          $maxlines  maximum number of segments in $border
#          $red      $reduction factor per cycle
#          $xdir   border to be optimized xdir (=1) or ydir (=0) 
#          $totcost     : total cost estimate         
#          $avcost      : cost per task limit
#

{ my ($border,$other,$array,$maxlines,$red,$xdir,$totcost,$avcost)=@_;
  my (@size,$l,$ll,@vval,@val,$sum,$i,$j,$lcost,@bordertmp,$curr);
  my ($iutmp,$iltmp,@bmap);
  @val=();
  @vval=();
  $ll=0;



  if ($xdir)
   {
  for ($i=0; $i<$#$border; $i++)
    { $val[$i] = patchij(1,75,$$border[$i],$$border[$i+1]-1,$array);
      for ($j=0; $j<$#$other; $j++)
         {$vval[$i][$j]=patchij($$border[$i],$$border[$i+1]-1,$$other[$j],$$other[$j+1]-1,$array);}
      $ll=$ll+1;
      }
#     print "total cost for lines", $$border[$i]," to ", $$border[$i+1]-1," = $val[$i]\n";}
   }
  else
   {
  for ($i=0; $i<$#$border; $i++) 
    { $val[$i] = patchij($$border[$i],$$border[$i+1]-1,1,75,$array);
      for ($j=0; $j<$#$other; $j++)
         {$vval[$i][$j]=patchij($$other[$j],$$other[$j+1]-1,$$border[$i],$$border[$i+1]-1,$array);}
      $ll=$ll+1;
      }
#     print "total cost for lines", $$border[$i]," to ", $$border[$i+1]-1," = $val[$i]\n";}
   }
  if ($ll <= $maxlines) {return $ll;}
  $l=int($ll/$red);
  if ($l < $maxlines) { $l=$maxlines;}
# print "reducing number of tasks from $ll to ",$l," maxlines=$maxlines\n";
  $lcost=$totcost/$l;
  @bordertmp=();
  $curr=0; 
# Prozess des Zusammenfuegens sollte nicht unbedingt vom Rand starten
# get smallest non assoc block, add neigbouring blocks until exceeding
# mark assoc blocks  and continue
     for ($i=0; $i<$#$border; $i++)
        {  $curr+=$val[$i];
           $maxblock=0;
           for ($j=0; $j<$#$other; $j++) { if ($vval[$i][$j] > $maxblock) { $maxblock=$vval[$i][$j];}}
           if ($curr>$lcost  || $maxblock > $avcost) { push @bordertmp, $$border[$i]; $curr=$val[$i];}
        }
  if ($bordertmp[0]!=1) { unshift @bordertmp,1;}
   push @bordertmp, 76 ; 
    {#print "bordertmp=",join(':',@bordertmp),"\n";
     #print "retaining $#bordertmp  X=$xdir segments\n";
      $l=$#bordertmp;
      @$border= @bordertmp ; } 
  return $l;
}  
   

####################################################################################################################################################
sub partition_data {

my ( $inttype,$pairtype,$isegtype,$jsegtype,$nsize_zyxw,$statseg,$name,
     $skipsize,$skipx,$skipy,$csfbatches,$tasks,$wantxseg,$wantyseg,$redx,$redy,
     $fixbd,$fixx,$zeroarray )=@_;

#
#  input   inttype = integral type  (0x,1x,2x,3x,4x,dg)
#          pairtype= yx,yw,...
#          $isegtype,jsegtype: 0,1,2,3 for z,y,x,w segments
#          nsize_zyxw: number of internal walks per statistic segment 
#          statseg   : number of statistic segments
#          fixbd     : fixed boundary values
#          fixx      :  0 none
#                       1 yseg
#                       2 xseg
#          zeroarray : list of zerotasks
#          
# output  returns boundaries as  (\@xb,\@yb);

 my (@datablock,$square,@assoc,@patch,$npatch,@xb,@yb);
 $square = ! ( $pairtype eq "zz" || $pairtype eq "yy" || $pairtype eq "xx" || $pairtype eq "ww");

 @datablock=();
 
 rd_seganalyse_datablock($inttype,$pairtype,\@datablock,
                         $$nsize_zyxw[$isegtype],$$nsize_zyxw[$jsegtype],
                         $$statseg[$isegtype],$$statseg[$jsegtype]);
 
 if ($prtlevel > 1) { print_2D($name . "data_array",\@datablock,$$statseg[$isegtype],$statseg[$jsegtype],$square);}

#search_emptypatches ( $iseg,$jseg,$datablock,$skipsize,$skipx,$skipy,$assoc,$patch,$npatch)
# input  $iseg,$jseg: number of segments
#        @datablock [i][j]  number of contributions for seg i j combination
#        $skipsize = all empty patches below $skipsize segment pairs are marked by -1 contributions
#        $skipy    = all empty patches with y dimension below $skipy are marked by -1 contributions
#        $skipx    = all empty patches with x dimension below $skipx are marked by -1 contributions
# 
# output @assoc[i][j] = $clusterid
#        @patch[cnt][0..3] = coordinates of patch cnt with xup,yup,xlo,ylo (0..3)
#        $npatch    0 number of patches found
#        @datablock  empty patches to be ignored (due to their size) are set to -1 (instead of 0)
#
 
 search_emptypatches ($$statseg[$isegtype],$$statseg[$jsegtype],\@datablock,$skipsize,$skipx,$skipy,
                       \@assoc,\@patch,\$npatch);

if ($prtlevel > 2 ) {print_2D($name . "assoc array",\@assoc,
                              $$statseg[$isegtype],$$statseg[$jsegtype],$square);}


print_tasks($name . "EMPTY_PATCHES",$$statseg[$isegtype],$$statseg[$isegtype],\@assoc);

comp_boundaries($npatch,\@patch,$$statseg[$isegtype],$$statseg[$jsegtype],\@xb,\@yb);

       if ($fixx == 1 ) { @yb = @$fixbd;}
       if ($fixx == 2 ) { @xb = @$fixbd;}

@tmp=();
comp_buckets($name,\@datablock,\@xb,\@yb,$$statseg[$isegtype],
             $$statseg[$jsegtype],$$csfbatches[$isegtype],$$csfbatches[$jsegtype],$tasks,\@tmp);
            


refine_tasks(\@xb,\@yb,\@datablock,$$statseg[$isegtype],$$statseg[$jsegtype],
             $tasks,$wantxseg,$wantyseg,$redx,$redy);

comp_buckets($name,\@datablock,\@xb,\@yb,$$statseg[$isegtype],
             $$statseg[$jsegtype],$$csfbatches[$isegtype],$$csfbatches[$jsegtype],$tasks,$zeroarray);


return (\@xb,\@yb);


}




####################################################################################################################################################



sub print_2D
{ my($header,$array,$ncols,$nrows,$full)=@_;
 my ($ncol,$irow,$icol,$ic,$ir,$outstring,$upc,$upr,$full);
 $ncol=10;

  print "                       ",uc($header)," ( $nrows * $ncols )\n";

  for ($irow=1;  $irow < $nrows; $irow+=$ncol )
   { for ($icol=1; $icol < $ncols; $icol+=$ncol )
      { # print patch [($irow-1)*$ncol+1:min($irow*$ncol,$nrows), ($icol-1)*$ncol:min($icol*$ncol,$ncols)]
        if (( ! $full ) && $icol>$irow ) {next;}
        $outstring= "  ";
        $upc=$icol+$ncol-1 > $ncols ? $ncols : $icol+$ncol-1;
        $upr=$irow+$ncol-1 > $nrows ? $nrows : $irow+$ncol-1;
        for ($ic=$icol; $ic<=$upc; $ic++) { $outstring .= sprintf "%8d ",$ic;}
        print "\n $outstring\n    -------------------------------------------------------------------------------------------\n";
        for ($ir=$irow; $ir<=$upr; $ir++)
         { $outstring= sprintf "%3d |", $ir;
           for ($ic=$icol;$ic<=$upc ; $ic++)
            { $outstring .= sprintf "%8d ",$$array[$ir][$ic];}
            print "$outstring \n";
         }
      }
   }
  return;
  }


 sub print_tasks
 {my ($header,$iseg,$jseg,$assoc)=@_;
  my ($zero,$i,$j,$line,@achr);
  @achr=('A' .. 'Z','a' .. 'z');
  $zero=0;
  for($j=1; $j <=$jseg; $j++)
  { $line=' ';
    for ($i=1;$i<=$iseg; $i++) { if ( $$assoc[$i][$j] ) {$zero++; $x=$achr[(($$assoc[$i][$j]-1) % $#achr)]; } else {$x=' ';}
                                                                $line=$line . $x;}
    print "$line\n";
  }
  print "Zero tasks  retained: ",$zero*100/($iseg*$jseg),"%\n";
  return;
 }






##############################################################################################################################
      
       sub start_tracker {
       my ($name); $name=shift @_;
       print "...................................", " $name .........................................\n";
       return; }

       sub end_tracker {
       my ($name); $name=shift @_;
       print "-----------------------------------", " $name -----------------------------------------\n";
       return; }


 sub msort
 {
   if ($a<$b) { return -1; }
   else {return  1;}
 }


###############################################################################################################################


sub createciudgin
{
 my ($borders_zz,$borders_yy,$borders_xx,$borders_ww,$nsize_zyxw,$nval_zyxw)=@_;
 my  (@nseg,@nnseg,$noffset,$tmp);

 @nnseg=();
  shift @$borders_zz;
  push @nnseg,1;
  $nseg[0]=0;
  $nseg[0]++;
  pop @$borders_zz;
  while ($tmp = shift @$borders_zz ) {if ($tmp*$$nsize_zyxw[0]<$$nval_zyxw[0]) { $nseg[0]++; push @nnseg,$tmp*$$nsize_zyxw[0];}}

  shift @$borders_yy;
  $noffset=$$nval_zyxw[0];
  push @nnseg, $noffset+1;
  $nseg[1]=0;
  $nseg[1]++;
  pop @$borders_yy;
  while ($tmp = shift @$borders_yy ) {if ($tmp*$$nsize_zyxw[1]<$$nval_zyxw[1])
                                      {$nseg[1]++; push @nnseg,$tmp*$$nsize_zyxw[1]+$noffset;}}

  shift @$borders_xx;
  $noffset+=$$nval_zyxw[1];
  push @nnseg, $noffset+1;
  $nseg[2]=0;
  $nseg[2]++;
  pop @$borders_xx;
  while ($tmp = shift @$borders_xx ) {if ($tmp*$$nsize_zyxw[2]<$$nval_zyxw[2])
                                      {$nseg[2]++;push @nnseg,$tmp*$$nsize_zyxw[2]+$noffset;}}

  shift @$borders_ww;
  $noffset+=$$nval_zyxw[2];
  push @nnseg, $noffset+1;
  $nseg[3]=0;
  $nseg[3]++;
   pop @$borders_ww;
  while ($tmp = shift @$borders_ww ) { if ($tmp*$$nsize_zyxw[2]<$$nval_zyxw[2])
                                       {$nseg[3]++; push @nnseg,$tmp*$$nsize_zyxw[3]+$noffset;}}

  $noffset+=$$nval_zyxw[3];
  push @nnseg,$noffset+1;
  return (\@nseg,\@nnseg);
 return;
}


sub encode_zerotask{
 my ($zero_task)=@_;


print " -----------------------------------  ZERO Tasks --------------------------------------------------------------\n";

{ my ($i,$tmpc,$tasktype,$tmp1,$tmp2,$tmp3,$tmp4,$bra,$ket,$tmp5,@tmpci);
 $tmpc=""; @tmpci=();
for ($i=0; $i<=$#$zero_task; $i++) {#printf " %4d  %-60s \n", $i, $$zero_task[$i];
                                  push @tmpci,$tmpc;
                                   $tmpc=$$zero_task[$i]; $tmpc=~s/^ *//; $tmpc=~s/=>//; $tmpc=~s/://;
                                   ($tasktype,$tmp1,$tmp2,$tmp3,$tmp4,$bra,$ket,$tmp5)=split(/\s+/,$tmpc);
                                   if ($tasktype eq "YY_2x") {$tmpc = ($ket+($bra+21000)*1000);next;}
                                   if ($tasktype eq "XX_2x") {$tmpc = ($ket+($bra+22000)*1000);next;}
                                   if ($tasktype eq "WW_2x") {$tmpc = ($ket+($bra+23000)*1000);next;}
                                   if ($tasktype eq "XZ_2x") {$tmpc = ($ket+($bra+24000)*1000);next;}
                                   if ($tasktype eq "WZ_2x") {$tmpc = ($ket+($bra+25000)*1000);next;}
                                   if ($tasktype eq "WX_2x") {$tmpc = ($ket+($bra+26000)*1000);next;}
                                   if ($tasktype eq "YZ_1x") {$tmpc = ($ket+($bra+11000)*1000);next;}
                                   if ($tasktype eq "YX_1x") {$tmpc = ($bra+($ket+12000)*1000);next;}
                                   if ($tasktype eq "YW_1x") {$tmpc = ($bra+($ket+13000)*1000);next;}
                                   if ($tasktype eq "YX_3x") {$tmpc = ($bra+($ket+31000)*1000);next;}
                                   if ($tasktype eq "YW_3x") {$tmpc = ($bra+($ket+32000)*1000);next;}
                                   if ($tasktype eq "ZZ_0x") {$tmpc = ($ket+($bra+1000)*1000);next;}
                                   if ($tasktype eq "YY_0x") {$tmpc = ($ket+($bra+2000)*1000);next;}
                                   if ($tasktype eq "XX_0x") {$tmpc = ($ket+($bra+3000)*1000);next;}
                                   if ($tasktype eq "WW_0x") {$tmpc = ($ket+($bra+4000)*1000);next;}
                                   die ("invalid tasktype $tasktype\n");
                                   }
 push @tmpci,$tmpc;
 shift @tmpci;
 sort msort @tmpci;
  $tmpc=" zerotasks=" . join(',',@tmpci) . "\n";
#push @ciudgin,$tmpc;
 print $tmpc;
}
print " --------------------------------------------------------------------------------------------------------------\n";

 return;
}




############################################################################################################################################


