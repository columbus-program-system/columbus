!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             IWX1,        MAXBUF,      MINWX,       NMBWX
      INTEGER             SCR1XL,      SCR3XL,      SCR4XL,      WXS
      INTEGER             WXSYM
C
      REAL*8              SQRT2
C
      COMMON / F4X    /   SQRT2,       NMBWX,       WXS,         WXSYM
      COMMON / F4X    /   MINWX,       IWX1,        MAXBUF,      SCR4XL
      COMMON / F4X    /   SCR3XL,      SCR1XL
C
