!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
C
      COMMON / DATA   /   SYM12,       SYM21,       NBAS,        LMDA
      COMMON / DATA   /   LMDB,        NMBPR,       STRTBW,      STRTBX
      COMMON / DATA   /   BLST1E,      NMSYM,       MNBAS,       CNFX
      COMMON / DATA   /   CNFW,        SCRLEN
C
