!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             HMULT,       LXYZIR(3),   MULTP(NMULMX)
      INTEGER             NEXW(8,4),   NMUL,        SPNIR(MULTMX,MULTMX)
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON / SOLXYZ /   SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON / SOLXYZ /   SPNIR,       MULTP,       NMUL,        HMULT
      COMMON / SOLXYZ /   NEXW
C
