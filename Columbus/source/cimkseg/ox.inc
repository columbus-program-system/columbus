!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             CISTRT(2),   CODE,        HILEV,       INDI1
      INTEGER             ISYM,        MLP,         MLPPR,       NI
      INTEGER             NIOT,        RWLPHD,      SEGSUM(2),   WTLPHD
C
      REAL*8              SQRT2,       VALUE2
C
      COMMON / OX     /   SQRT2,       WTLPHD,      MLP,         MLPPR
      COMMON / OX     /   SEGSUM,      CISTRT,      ISYM,        NI
      COMMON / OX     /   CODE,        INDI1,       RWLPHD,      HILEV
      COMMON / OX     /   NIOT,        VALUE2
C
