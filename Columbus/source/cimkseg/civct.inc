!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             CIST(MXNSEG,7),           DRTIDX(MXNSEG,7)
      INTEGER             DRTLEN(MXNSEG,7),         DRTST(MXNSEG,7)
      INTEGER             IPTHST(MXNSEG,7),         LENCI,       NSEG(7)
      INTEGER             SEGCI(MXNSEG,7),          SEGEL(MXNSEG,7)
      INTEGER             SEGSCR(MXNSEG,7),         SEGTYP(MXNSEG,7)
C
      COMMON / CIVCT  /   LENCI,       NSEG,        SEGEL,       CIST
      COMMON / CIVCT  /   SEGCI,       SEGSCR,      SEGTYP,      IPTHST
      COMMON / CIVCT  /   DRTST,       DRTLEN,      DRTIDX
C
