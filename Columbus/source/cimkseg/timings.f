!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       program timings

       integer i,nextern(8),nrange(3)
       namelist /input/ nextern,nrange
       real*4 rand 
       real*8 testmm,testmv,mm(50),mv(50),dax(50)
       integer icont,ndim(50)


       do i=1,8
        nextern(i)=-1
       enddo
        nrange(1)=-1
        nrange(2)=-1
        nrange(3)=-1


       open (unit=7,file='timings.in')
       read(7,input)
       close (7)

       icont=0
       do i=nrange(1),nrange(2),nrange(3)
        icont=icont+1
        mm(icont)=testmm(i)
        mv(icont)=testmv(i)
        dax(icont)=testvx(i)
        ndim(icont)=i
       enddo

       do i=1,icont
       write(6,100) ndim(i),
     .     mm(i),mv(i),mv(i)/mm(i),dax(i),dax(i)/mv(i)
 100   format('N=',i8,3x,'mm=',f8.4,3x,'mv=',f8.4,3x,'mv/mm=',
     .  f8.4,3x,'daxpy=',f8.4,3x,'daxpy/mv=',f8.4,3x,'GFLOPS')
       enddo

 
       

  

       stop
       end
 

       subroutine dzero(n,a)
       integer i,n
       real*8 a(*)
       do i=1,n
        a(i)=0.0d0
       enddo
       return
       end

       subroutine dfill(n,a)
       real*4 rand
       integer i,n
       real*8 a(*)
       do i=1,n
         a(i)=dble(rand())
       enddo
       return
       end




       real*8 function testmm(n)
       real*8 a1(n,n),a2(n,n),a3(n,n)
       real*4 reftime,ref
       integer nmax
       call dzero(n*n,a3)
       call dfill(n*n,a1)
       call dfill(n*n,a2)
       ref=0.0
       nmax=10
       if (n.lt.200) nmax=20 
       if (n.lt.100) nmax=50 
       if (n.lt.50) nmax=200 
       reftime=comptime(ref)
       do i=1,nmax
        call dgemm('N','N',n,n,n,1.0d0,a1,n,a2,n,0.0d0,a3,n)
        call dgemm('N','N',n,n,n,1.0d0,a2,n,a3,n,0.0d0,a1,n)
      enddo 
       ref=comptime(reftime)

c      write(6,*) '20 dgemm(N=',n,' take ',ref,' seconds.'
c      write(6,100) 'N=',n,ref/(2*nmax),'seconds/dgemm (',
c    .        ((dble(n)/1024.0d0)**3)/dble(ref/(2*nmax)),' GFLOPS'
100    format(a,i8,3x,f12.4,3x,a,f12.3,3x,a)
       testmm=((dble(n)/1024.0d0)**3)/dble(ref/(2*nmax))
       return
       end


       real*8 function testmv(n)
       real*8 a1(n,n),v3(n),v1(n),v2(n)
       real*4 reftime,ref
       call dzero(n,v2)
       call dfill(n,v1)
       call dfill(n,v3)
       call dfill(n*n,a1)
       ref=0.0
       nmax=10
       if (n.lt.200) nmax=2000 
       if (n.lt.100) nmax=5000
       if (n.lt.50) nmax=20000
       reftime=comptime(ref)
       do i=1,nmax
        call dgemv('N',n,n,1.0d0,a1,n,v1,1,0.0d0,v2,1)
        call dgemv('N',n,n,1.0d0,a1,n,v3,1,0.0d0,v2,1)
      enddo
       ref=comptime(reftime)

c      write(6,*) '1000 dgemv(N=',n,' take ',ref,' seconds.'
c      write(6,100) 'N=',n,ref/(2*nmax),'seconds/dgemv (',
c    .        ((dble(n)/1024.0d0)**2)/dble(ref/(2*nmax)*1024d0),'GFLOPS'
100    format(a,i8,3x,f12.4,3x,a,f12.3,3x,a)
       testmv=((dble(n)/1024.0d0)**2)/dble(ref/(2*nmax)*1024d0)
       return
       end


       real*8 function testvx(n)
       real*8 alpha,v1(n),v2(n)
       real*4 reftime,ref
       call dzero(n,v2)
       call dfill(n,v1)
       ref=0.0
       nmax=10
       if (n.lt.200) nmax=2000
       if (n.lt.100) nmax=5000
       if (n.lt.50) nmax=20000
       reftime=comptime(ref)
       do i=1,nmax
        call daxpy(N,2.5d0,v1,1,v2,1)
        call daxpy(N,0.4d0,v2,1,v1,1)
      enddo
       ref=comptime(reftime)

c      write(6,*) '1000 dgemv(N=',n,' take ',ref,' seconds.'
c      write(6,100) 'N=',n,ref/(2*nmax),'seconds/dgemv (',
c    .        ((dble(n)/1024.0d0)**2)/dble(ref/(2*nmax)*1024d0),'GFLOPS'
100    format(a,i8,3x,f12.4,3x,a,f12.3,3x,a)
       testvx=(dble(n*2*nmax)/(1024.0d0)**3)
       return
       end


      

        























       real*4 function comptime(reftime)
       implicit none
       real*4 buffer(2),reftime
       real*4  etime 
       comptime = etime(buffer)-reftime
      RETURN
      END


       

       
