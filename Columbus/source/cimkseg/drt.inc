!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             BORD(NIMOMX),             BSYM(NIMOMX)
      INTEGER             CASE(0:NROWMX),           L(0:3,NROWMX,3)
      INTEGER             MU(NIMOMX),  NJ(0:NIMOMX)
      INTEGER             NJSKP(0:NIMOMX),          ROW(0:NROWMX)
      INTEGER             XBARCDRT(NROWMX,3),       Y(0:3,NROWMX,3)
      INTEGER             YB(0:NROWMX),             YK(0:NROWMX)
C
      COMMON / DRT    /   B,           XBARCDRT,    L,           Y
      COMMON / DRT    /   YB,          YK,          CASE,        ROW
      COMMON / DRT    /   BORD,        BSYM,        ARCSYM,      NJ
      COMMON / DRT    /   NJSKP,       MU
C
