!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*@if (defined(ibm) || defined ( vector)) || defined (essl)
*@define ibmvecessl
*@endif
       integer function calcthrxt(index)
c
c  calculate the maximum number of loops
c  contributing to the 1-internal formula tape.
c
c  code   loop          description
c  ----   -----        ---------------
c
c   0  -- new level ---
c   1  -- new record --
c   4  -- new vertex --
c
c   2       12c         (xy,wy) new relative loop value.
c   3       12c         (xy,wy) same loop value as before.
c
         IMPLICIT NONE

C====>Begin Module CALCTHRXT              File calcthrxt.f              
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            LOOP3CLC
C
      INTEGER             LOOP3CLC
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
C
      REAL*8              ONE
      PARAMETER           (ONE = 1.0D+00)
C
C     Argument variables
C
      INTEGER             INDEX(*)
C
C     Local variables
C
      INTEGER             MAXW1,       NUMC(2),     NUMW1(2),    TASKID
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cst.inc'
      INCLUDE 'cvtran.inc'
      INCLUDE 'lcontrol.inc'
      INCLUDE 'special.inc'
C====>End Module   CALCTHRXT              File calcthrxt.f              
      call intab
      call istack
999   continue
c
      nintl = 1
      mo(0) = 0
      qdiag = .false.
c
      numv    = 1
      nmo     = 1
      code(1) = 3
      code(2) = 2
      maxw1 = 0
      lmo = 1
100   continue
         numw1(1) = 0
         numw1(2) = 0
         numc(1)=0
         numc(2)=0
         mo(1) = lmo
c
         ver = 3
90       continue ! ver = 3, 4
            oldval = one
c
1           numw1(ver-2)=numw1(ver-2)+
     .           loop3clc( ver, 2, st12, db12(1,0,3), 4, 2,index)

c
          numc(ver-2)=numc(ver-2)+1
          ver = ver + 1
          if (ver .le. 4 ) go to 90
c
         numc(ver-2)=numc(ver-2)+1
         maxw1 = max(maxw1,numw1(1)+numc(1),numw1(2)+numc(2))
c
      lmo = lmo + 1
      if (lmo .le. niot) go to 100
c
cmd   call dump(1)
      icodev(ibf) = 0
      jumpinposition = 0
c
6020  format(' maxw1=',i8)
      calcthrxt=maxw1
C HPM   return
c       call _f_hpm_stop_(63,195)
C HPM
      return
6100  format(' int1:  lmo = ',i4)
      end
       subroutine copyvalues(
     .    licodev,lheadv,lxbtv,
     .    lybtv,lyktv,lval,
     .    copiedvalues,validvalues)

         IMPLICIT NONE

C====>Begin Module COPYVALUES             File copyvalues.f             
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
C
C     Argument variables
C
      INTEGER             COPIEDVALUES,             LHEADV(*)
      INTEGER             LICODEV(*),  LXBTV(*),    LYBTV(*)
      INTEGER             LYKTV(*),    VALIDVALUES
C
      REAL*8              LVAL(*)
C
C     Local variables
C
      INTEGER             I,           TASKID
C
      INCLUDE 'special.inc'
C====>End Module   COPYVALUES             File copyvalues.f             
         call icopy_wr(validvalues,icodev,1,licodev(copiedvalues+1),1)
         call icopy_wr(validvalues,headv,1,lheadv(copiedvalues+1),1)
         call icopy_wr(validvalues,xbtv,1,lxbtv(copiedvalues+1),1)
         call icopy_wr(validvalues,ybtv,1,lybtv(copiedvalues+1),1)
         call icopy_wr(validvalues,yktv,1,lyktv(copiedvalues+1),1)

         do i=1,validvalues
           lval(copiedvalues+i)=valv(1,i)
         enddo
         copiedvalues=copiedvalues+validvalues

*@ifdef debug
*          write(6,*) 'range occupied= 1..',copiedvalues
*@endif
C HPM   return
c       call _f_hpm_stop_(64,266)
C HPM
         return
         end
         integer function getinteger(buf,i)
         IMPLICIT NONE
C HPM  include
C HPM   include 'f_hpm.h'
        INTEGER taskID
C HPM

C
C     Argument variables
C
      INTEGER             BUF(*),      I
C


         getinteger=buf(i)
         return
         end
      integer function loop3clc( bt, kt, stype, db0, nran, ir0,
     .  index)

c
c  construct loops and accumulate products of segment values.
c
c  input:
c  bt=    bra tail of the loops to be constructed.
c  kt=    ket tail of the loops to be constructed.
c  stype(*)= segment extension type of the loops.
c  db0(*)=   delb offsets for the vn(*) segment values.
c  nran=     number of ranges for this loop template.
c  ir0=      range offset for this loop.
c  /cpt/     segment value pointer tables.
c  /cex/     segment type extension tables.
c  /ctab/    segment value tables.
c  /cdrt/    drt information.
c  /cdrt2/   vectors spanning number of walks.
c  /cstack/  loop construction stack arrays.
c  /csym/    symmetry information.
c  /cli/     loop information.
c
c  modified for mu-dependent segments 9-nov-86 (-rls).
c  this version written 07-sep-84 by ron shepard.
c
c
         IMPLICIT NONE

C====>Begin Module LOOP3CLC               File loop3clc.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NST
      PARAMETER           (NST = 40)
      INTEGER             NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NWLKMX
      PARAMETER           (NWLKMX = 2**20-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
C
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER             BT,          DB0(NRAN,0:2,*)
      INTEGER             INDEX(*),    IR0,         KT,          NRAN
      INTEGER             STYPE(NRAN)
C
C     Local variables
C
      INTEGER             BBRA,        BCASE,       BKET,        BROWC
      INTEGER             BROWN,       BVER,        DELB,        EXT
      INTEGER             I,           IGONV,       IL,          IMU
      INTEGER             INDXX,       IR,          KCASE,       KROWC
      INTEGER             KROWN,       KSEG,        KVER,        NEXR
      INTEGER             NLEV,        NLOOPLOC,    NUMBEROFLOOPS
      INTEGER             R,           STR,         TASKID,      WTLPHDB
      INTEGER             WTLPHDK
C
      LOGICAL             QBCHK(-4:4)
C
      INCLUDE 'cex.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cpt.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'ctab.inc'
      INCLUDE 'drt.inc'
      INCLUDE 'lcontrol.inc'
C====>End Module   LOOP3CLC               File loop3clc.f               
C
c  formula tape construction

c     # dummy:
c     # local:
      data qbchk/.true.,.true.,.false.,.false.,.false.,
     +  .false.,.false.,.true.,.true./

      bver = min( bt, 3 )
      kver = min( kt, 3 )
c
c     # assign a range (ir1 to nran) for each level of the
c     # current loop type.
c
      hilev = mo(nmo)
c
      ir = ir0
      do 120 i = 1, nmo
         ir = ir + 1
         do 110 il = mo(i-1), (mo(i)-2)
            range(il) = ir
110      continue
         ir = ir+1
         range(mo(i)-1) = ir
120   continue
c
c     # initialize stack arrays for loop construction.
c
      clev    = 0
      ybra(0)   = 0
      yket(0)   = 0
      brow(0) = bt
      krow(0) = kt
      qeq(0)  = bt.eq.kt
      v(1,0)  = one
      v(2,0)  = one
      v(3,0)  = one

      nlooploc=0
c
c     # one, two, or three segment value products are accumulated.
c     # begin loop construction:
c
      goto 1100
c
1000  continue
c
c     # decrement the current level, and check if done.
c
      extbk(clev) = 0
      clev = clev-1
      if(clev.lt.0) then
       loop3clc=nlooploc
C HPM   return
c       call _f_hpm_stop_(66,544)
C HPM
       return
       endif
c
1100  continue
c
c     # new level:
c
      if(clev.eq.hilev)then
c
c        qdiag=t :allow both diagonal and off-diagonal loops.
c        qdiag=f :allow only off-diagonal loops.
c
         if ( qdiag .or. (.not. qeq(hilev)) ) then
            nlooploc=nlooploc+1
         endif
         go to 1000
      endif
c
      nlev = clev+1
      r = range(clev)
      str = stype(r)
      nexr = nex(str)
1200  continue
c
c     # new extension:
c
      ext = extbk(clev)+1
      if(ext.gt.nexr)go to 1000
      extbk(clev) = ext
      bcase = bcasex(ext,str)
      kcase = kcasex(ext,str)
c
c     # check for extension validity.
c
c     # canonical walk check:
c
      if ( qeq(clev) .and. (bcase .lt. kcase) ) go to 1200
      qeq(nlev) = qeq(clev).and.(bcase.eq.kcase)
c
c     # individual segment check:
c
      browc = brow(clev)
      brown = l(bcase,browc,bver)
      if(brown.eq.0)go to 1200
      krowc = krow(clev)
      krown = l(kcase,krowc,kver)
      if(krown.eq.0)go to 1200
      brow(nlev) = brown
      krow(nlev) = krown
c
c     # check b values of the bra and ket walks.
c
      bbra = b(brown)
      bket = b(krown)
      delb = bket-bbra
c
c     # the following check is equivalent to:
c     # if ( abs(delb) .gt. 2 ) go to 1200
c
      if ( qbchk(delb) ) go to 1200
c
      ybra(nlev) = ybra(clev)+y(bcase,browc,bver)
      wtlphdb = xbarcdrt(brown,bver)

      yket(nlev) = yket(clev)+y(kcase,krowc,kver)
      wtlphdk = xbarcdrt(krown,kver)
c
c check for invalid walks
c
c #   bra invalid
c
      if (-index(ybra(nlev)+1).ge. wtlphdb) goto 1200
c
c #   ket invalid
c
      if (-index(yket(nlev)+1).ge. wtlphdk) goto 1200

ctm ************************************************************

c     # accumulate the appropriate number of products:
c
      imu = mu(nlev)
      if (numv.eq.1) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      if ( abs(v(1,nlev)) .le. small ) go to 1200
      elseif (numv.eq.2) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small      )go to 1200
      else 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      v(3,nlev) = v(3,clev)*tab(pt(bcase,kcase,db0(r,imu,3)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small .and.
     & abs(v(3,nlev)) .le. small      )go to 1200
      endif
c
c     # passed all the tests, this is a
c     # valid segment extension, increment to the next level:
c
      clev = nlev
      go to 1100
c
      end
      subroutine thresg(
     & indsymbra,conftbra,indxbra,cibra,sgmabra,
     & indsymket,conftket,indxket,ciket,sgmaket,
     & intbuf, iblnr,  iblanz, iblstt,
     & scr3x  )

c
         IMPLICIT NONE

C====>Begin Module THRESG                 File thresg.f                 
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            DDOT_WR
C
      REAL*8              DDOT_WR
C
C     Parameter variables
C
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
      REAL*8              ONE
      PARAMETER           (ONE = 1.0D0)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), IBLANZ,      IBLNR
      INTEGER             IBLSTT(*),   INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              SCR3X(*),    SGMABRA(*),  SGMAKET(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        HSTR12,      HSTRT
      INTEGER             HSTRT1,      HSTRT3,      IBL,         IBLKST
      INTEGER             IK1,         ISYM,        JSYM,        K1
      INTEGER             K1F,         K1J,         K1S,         KET
      INTEGER             KLS,         KLSYM,       KLYS,        KLYSYM
      INTEGER             KS,          KSYM,        M1LP2,       MLP1Z
      INTEGER             MLP2Z,       MUL,         NI,          NINTKL
      INTEGER             NJ,          NMB1,        NMB1A,       NMB1P
      INTEGER             NMB1X,       NMB2,        NMB2A,       NMB2X
      INTEGER             NMBINT,      NUMK,        NWLK,        STRTWX
      INTEGER             STW1,        STW121,      STW2,        STW3
      INTEGER             STWX1,       STWX12,      STX1,        STX121
      INTEGER             STX2,        STX3,        TASKID,      YS
      INTEGER             YSYM
C
      LOGICAL             SKIP,        WALK
C
      INCLUDE 'blint3.inc'
      INCLUDE 'cnndx.inc'
      INCLUDE 'counter.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'data.inc'
      INCLUDE 'f4x.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'threex_stat.inc'
      INCLUDE 'thrx.inc'
C====>End Module   THRESG                 File thresg.f                 
      bra  = 0
      ket  = 0
      walk = .false.
      k1j  = 0
c
      do 100 ibl = iblnr, iblanz
         ksym=ksymx(ibl)
         k1s=k1sx(ibl)
         k1f=k1fx(ibl)
         stw121=stw12x(ibl)
         stw1=stw1x(ibl)
         stw2=stw2x(ibl)
         stw3=stw3x(ibl)
         stx121=stx12x(ibl)
         stx1=stx1x(ibl)
         stx2=stx2x(ibl)
         stx3=stx3x(ibl)
         nintkl=ninklx(ibl)
         iblkst=iblstt(ibl)-1
c         write(6,32000) ibl,iblnr,iblanz,ksym,k1s,k1f,stw121,stw1,
c     &    stw2,stw3,stx121,stx1,stx2,stx3,nintkl,iblkst
c
         if(spnodd)niot = niot + 1
         mul = 0
         call upwkst(hilev,niot,rwlphd,mlpb,mlpk,clev)
c         write(6,32010)hilev,niot,rwlphd,mlp2,mlp1,clev
1900     continue
         call upwlknew(ket,bra,walk,mul,segsum(2),segsum(1),skip)
         if(walk)then
            if(spnodd)niot = niot - 1
            go to 100
         else
            if (skip) goto 1910
         endif
c
         nmb1a=indxket(ket)
         if(nmb1a.lt.0)go to 1910
         nmb1=conftket(nmb1a)
         ys=indsymket(nmb1a)
         ysym=sym21(ys)
c         write(6,31010) nmb1a,nmb1,ysym
         if(ysym.gt.ksym)go to 1910
c
c        # check if the ci vector segment is available.
c
c         write(6,31000)m1lp2,segsum
         nmb2a=indxbra(bra)
         if(nmb2a.lt.0)go to 1910
         nmb2=conftbra(nmb2a)
         wxs=indsymbra(nmb2a)
         wxsym=sym21(wxs)
         nmb1p=nmb1+1
c         write(6,30210)mlp1,ket,nmb1,ysym,ys,mlp2,bra,nmb2,wxsym,wxs
         ks=sym12(ksym)
         if(ksym.eq.lsym)go to 2300
         if(ksym.eq.1)go to 1920
         if(ysym.gt.ksym)go to 1920
         kls=mult(ks,ls)
         klsym=sym21(kls)
         if(ysym.eq.ksym)go to 2150
         klys=mult(ys,kls)
         klysym=sym21(klys)
c          write(6,30220)ksym,ks,k11,k12,klsym,kls,klysym,klys
         if(klysym-ysym)1930,1940,1950
1940     write(0,20030)
         call bummer('thresg: klysym=ysym=',ysym,faterr)
1930     continue
         if(ysym.eq.lsym)go to 1920
c
c        # lmd(i).lt.lmd(j).lt.lmd(k)
c        # 1a,b wy,xy
c
         isym=klysym
         ni=nbas(isym)
         if(ni.eq.0)go to 1920
         jsym=ysym
         nj=nbas(jsym)
         if(nj.eq.0)go to 1920
         if(ntrypr.lt.3)go to 1960
         strtwx=nmb2+strtbw(ksym,wxsym)+(k1s-1)*ni
         hstrt=stw1+strtbw(jsym,klsym)
         yw3x=yw3x+1

c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_ww+1
         cnt3x_yw(nmb1x,nmb2x)=cnt3x_yw(nmb1x,nmb2x)+1


         go to 1970
1960     strtwx=nmb2+strtbx(ksym,wxsym)+(k1s-1)*ni
         hstrt=stx1+strtbx(jsym,klsym)
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_xx+1
         cnt3x_yx(nmb1x,nmb2x)=cnt3x_yx(nmb1x,nmb2x)+1
         yx3x=yx3x+1
1970     continue
1980     continue
         go to 1920
c
c        # 2a,b wy,xy
c
1950     continue
         if(klysym-ksym)2010,2020,1920
c
c        # lmd(i).lt.lmd(j).lt.lmd(k)
c
2010     isym=ysym
         ni=nbas(isym)
         if(ni.eq.0)go to 1920
         jsym=klysym
         nj=nbas(jsym)
         if(nj.eq.0)go to 1920
         if(ntrypr.lt.3)go to 2030
         strtwx=nmb2+strtbw(ksym,wxsym)+(k1s-1)*nj
         hstrt=stw2+strtbw(jsym,klsym)
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_ww+1
         cnt3x_yw(nmb1x,nmb2x)=cnt3x_yw(nmb1x,nmb2x)+1

         yw3x=yw3x+1
         go to 2040
2030     strtwx=nmb2+strtbx(ksym,wxsym)+(k1s-1)*nj
         hstrt=stx2+strtbx(jsym,klsym)
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_xx+1
         cnt3x_yx(nmb1x,nmb2x)=cnt3x_yx(nmb1x,nmb2x)+1
         yx3x=yx3x+1
2040     continue
c          write(6,50010)strtwx,k1s,k1f
c
c
         go to 1920
c
c        # lmd(j)=lmd(k) 2a,b wy,xy
c
2020     continue
         isym=ysym
         ni=nbas(isym)
         if(ni.eq.0)go to 1920
         if(ntrypr.lt.3)go to 2080
         strtwx=nmb2+strtbw(ksym,wxsym)+(k1s-1)*k1s/2
         hstrt=stw2+nintkl+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_ww+1
         cnt3x_yw(nmb1x,nmb2x)=cnt3x_yw(nmb1x,nmb2x)+1

         yw3x=yw3x+1
         go to 2090
2080     strtwx=nmb2+strtbx(ksym,wxsym)+(k1s-1)*(k1s-2)/2
         hstrt=stx2+nintkl+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_xx+1
         cnt3x_yx(nmb1x,nmb2x)=cnt3x_yx(nmb1x,nmb2x)+1

         yx3x=yx3x+1
2090     continue
c
         go to 1920
c
c        # lmd(y)=lmd(k)
c
2150     continue
c
c        # 3a,b wy,xy
c
         if(ntrypr.lt.3)go to 2170
         strtwx=nmb2
         hstrt=stw3+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_ww+1
         cnt3x_yw(nmb1x,nmb2x)=cnt3x_yw(nmb1x,nmb2x)+1

         yw3x=yw3x+1
         go to 2180
2170     strtwx=nmb2
         hstrt=stx3+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_xx+1
         cnt3x_yx(nmb1x,nmb2x)=cnt3x_yx(nmb1x,nmb2x)+1

         yx3x=yx3x+1
2180     continue
c          write(6,50030)strtwx
c
c
c        # 3a,b  6a,b
c
c          write(6,30360)strtwx,hstrt
c
c        # lmd(j)=lmd(k), lmd(i)=lmd(l)  1a,b
c
         if(lsym.gt.ksym)go to 1920
         ni=nbas(lsym)
         if(ni.eq.0)go to 1920
         if(ntrypr.lt.3)go to 2175
         stwx1=nmb2+strtbw(ksym,wxsym)
         hstrt=stw1+nintkl+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_ww+1
         cnt3x_yw(nmb1x,nmb2x)=cnt3x_yw(nmb1x,nmb2x)+1

         yw3x=yw3x+1
         go to 2185
2175     stwx1=nmb2+strtbx(ksym,wxsym)
         hstrt=stx1+nintkl+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_xx+1
         cnt3x_yx(nmb1x,nmb2x)=cnt3x_yx(nmb1x,nmb2x)+1

         yx3x=yx3x+1
2185     continue
         stwx1=stwx1+(k1s-1)*ni
         go to 1920
c
c        # lmd(k)=lmd(l),lmd(i)=lmd(j)
c
2300     continue
         if(ysym.gt.ksym)go to 1920
         if(ysym.eq.ksym)go to 2400
c
c        # lmd(y).lt.lmd(k),lmd(y)=lmd(j)
c        # 1a,b,4a,b 2a,b
c
         isym=ysym
         nj=nbas(isym)
         if(nj.eq.0)go to 1920
c---           njm=nj-1
         if(ntrypr.lt.3)go to 2310
         stwx1=nmb2+strtbw(ksym,wxsym)+(k1s-1)*nj
         hstrt1=stw121+intbl2(ysym)
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_ww+1
         cnt3x_yw(nmb1x,nmb2x)=cnt3x_yw(nmb1x,nmb2x)+1
         yw3x=yw3x+1
         go to 2315
2310     continue
         hstrt1=stx121+intbl2(ysym)
         stwx1=nmb2+strtbx(ksym,wxsym)+(k1s-1)*nj
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_xx+1
         cnt3x_yx(nmb1x,nmb2x)=cnt3x_yx(nmb1x,nmb2x)+1

         yx3x=yx3x+1
2315     continue
         nmb1p=nmb1
         go to 1920
c
c        # lmd(y)=lmd(k)
c
2400     continue
         if(ntrypr.lt.3)go to 2420
         stwx1=nmb2+strtbw(ksym,wxsym)
         strtwx=nmb2
         hstrt3=stw3+iblkst
         hstr12=stw121+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_ww+1
         cnt3x_yw(nmb1x,nmb2x)=cnt3x_yw(nmb1x,nmb2x)+1

         yw3x=yw3x +1
         go to 2430
2420     continue
         stwx1=nmb2+strtbx(ksym,wxsym)
         strtwx=nmb2
         hstrt3=stx3+iblkst
         hstr12=stx121+iblkst
c        nmb1 ysegment
         nmb1x=(nmb1a-1)/isize_yy+1
c        nmb2 w segment
         nmb2x=(nmb2a-1)/isize_xx+1
         cnt3x_yx(nmb1x,nmb2x)=cnt3x_yx(nmb1x,nmb2x)+1

         yx3x=yx3x +1
2430     continue
c         write(6,30470) stwx1,strtwx,hstrt3,hstr12
c
1920     continue
c
c        # end of do loop over upper walks
c
c        # go to next upper walk
1910     continue
         if(mul.gt.1 .or. hilev.lt.niot)go to 1900
         if(spnodd)niot = niot - 1
c
c        # end of loop over integral blocks
c
100   continue
      return
32000 format(' thresg ',20i6)
32010 format(' upwkst',30i4)
31010 format(' nmb1a',20i6)
31000 format(' thr',20i6)
30210 format('threx 1910',20i6)
30220 format(' 1920',20i6)
20030 format(' stop 1940 threx')
30240 format(' 1970a',20i6)
50000 format(' 1970',20i6)
30250 format(' 1990',20i6)
50010 format(' 2040',20i6)
30270 format(' 2040a',20i6)
30280 format(' 2060',20i6)
50020 format(' 2090',20i6)
30300 format(' 2090a',20i6)
30310 format(' 2110',20i6)
50030 format(' 2180',20i6)
30360 format(' 2200',20i6)
50040 format(' 2185',20i6)
30390 format(' 2185',20i6)
30400 format(' 2205',20i6)
50050 format(' 2300',2i6)
30460 format(' 2340',20i6)
30470 format(' 2430',20i6)
30481 format(' 2450',20i6)
30480 format(' 2510',20i6)
      end
      subroutine threx(
     & indsymbra, conftbra,  indxbra, cibra,
     & sgmabra, indsymket,conftket,indxket, ciket,sgmaket,
     & lpbuf,rdbuf,  segsm,  cist,   ipth1,
     & iwx,    scr3x )

c
c
c
c
c*****
c     three external case
c
c     completely rewritten on 09/07/99 (tm)
c
c
c      nmbseg gibt an welches x,w Segment zu waehlen ist
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indexy   index vector for y walks
c     ciy,sigmay  ci and sigma vector for y
c     index1   index vector for (xw)segment 1
c     ci1      ci vector for (xw)segment 1
c     sigm1    sigma vector for (xw) segment 1
c     lpbuf    buffer for formula tape
c     rdbuf(maxbuf) buffer for two electron integrals
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     ipth1(2) start number for internal paths for segments 1 and 2
c     iwx=1    x walks
c        =2    w walks
c     scr3x(*)  scrach array for matrix multip. (only cray)
c
         IMPLICIT NONE
c
c==================================================================
c scheme:  loop over all internal l values
c
c              read all loops belonging to this l value from
c              file / calculate them, respectively
c
c                  read one record of integrals consisting of
c                  n blocks of (l,krange,all ij) integrals
c
c                  determine the number of blocks with valid l
c
c                  contract integrals
c          endloop
c
c===================================================================

C====>Begin Module THREX                  File threx.f                  
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT,      GETINTEGER,  RL
C
      INTEGER             ATEBYT,      FORBYT,      GETINTEGER,  RL
C
C     Parameter variables
C
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
C
      REAL*8              ONE
      PARAMETER           (ONE = 1.0D0)
C
C     Argument variables
C
      INTEGER             CIST(2),     CONFTBRA(*), CONFTKET(*)
      INTEGER             INDSYMBRA(*),             INDSYMKET(*)
      INTEGER             INDXBRA(*),  INDXKET(*),  IPTH1(2),    IWX
      INTEGER             SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    LPBUF(*)
      REAL*8              RDBUF(MAXBUF),            SCR3X(*)
      REAL*8              SGMABRA(*),  SGMAKET(*)
C
C     Local variables
C
      INTEGER             ADDHEADV,    ADDICODEV,   ADDTOP,      ADDVAL
      INTEGER             ADDXBTV,     ADDYBTV,     ADDYKTV,     BLANZ
      INTEGER             CODE,        CODEP,       I,           IBLANZ
      INTEGER             IBLNR,       IBLST,       IBLSTT(MXNSEG)
      INTEGER             IBLSYM,      IFILE,       II,          ILSTP
      INTEGER             INTBLK(MXNSEG),           INTSTT,      IOFFS
      INTEGER             IOFFST,      JJ,          L1
      INTEGER             L1BL(MXNSEG),             LANZ,        LOOP8
      INTEGER             MLP1M,       MLP1Z,       MLP2M,       MLP2Z
      INTEGER             NEWVAL,      RESTBL(2),   SKPINT
      INTEGER             STARTPOS,    TASKID,      TOTINTEGRALS
      INTEGER             TOTLOOPS,    UNPACK(2),   VALIDLOOPS
      INTEGER             VALIDVALUES , IBLOCK
C
      REAL*8              VAL,         VALREL
C
C
C     Common variables
C
      INTEGER             INTBL2(MAXSYM),           INTBLL(MAXSYM)
      INTEGER             INTBLM,      K1F(MXNSEG), K1S(MXNSEG)
      INTEGER             KSYM(MXNSEG),             NINTKL(MXNSEG)
      INTEGER             STW1(MXNSEG),             STW121(MXNSEG)
      INTEGER             STW2(MXNSEG),             STW3(MXNSEG)
      INTEGER             STX1(MXNSEG),             STX121(MXNSEG)
      INTEGER             STX2(MXNSEG),             STX3(MXNSEG)
C
      COMMON / BLINT3 /   INTBLM,      KSYM,        K1S,         K1F
      COMMON / BLINT3 /   STW121,      STW1,        STW2,        STW3
      COMMON / BLINT3 /   STX121,      STX1,        STX2,        STX3
      COMMON / BLINT3 /   NINTKL,      INTBL2,      INTBLL
C
      INCLUDE 'cciopt.inc'
C
C     Equivalenced common variables
C
      INTEGER             LVLPRT
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILLP,       IFIL3W,      IFIL3X
C
      INCLUDE 'cnndx.inc'
      INCLUDE 'data.inc'
      INCLUDE 'f4x.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'thrx.inc'
C====>End Module   THREX                  File threx.f                  
      equivalence (fillp,nunits(12))
      equivalence (ifil3w,nunits(33))
      equivalence (ifil3x,nunits(34))
      equivalence (lvlprt,uciopt(1))



c  x type or y type integrals
c  iwx=1 (x) iwx=2 (w)
c
      if ( iwx .eq. 2 ) then
         ifile = ifil3w
      else
         ifile = ifil3x
      endif
      open (unit=71,file='prep3e.dat',form='formatted')
      read(71,*)

c
c   set default values
c
      code   = 0
      ilstp  = 0
c   blanz  determines the number of decoded integral subblocks
      blanz  = 0
c   lanz   are the number of decoded integral subblocks belonging
c          to the current l value
      lanz   = 0
c   iblnr   is the first integral subblock considered
      iblnr  = 1
      iblst  = 0

      restbl(1) = 0
      totloops=0
      totintegrals=0
      startpos=1
c
c   calculate various offsets
c
      ii = 0
      jj = 0
      do 100 i = 1, nmsym
         intbl2(i) = 0
         intbll(i) = 0
         if (nbas(i).eq.0) go to 100
         intbl2(i) = ii
         intbll(i) = jj
         ii = ii + nbas(i)**2
         jj = jj + nndx(nbas(i))
100   continue
      niot   = intorb

      ioffs  = 2
      ioffst = 2

        addheadv=bufszl+1
        addicodev = addheadv+forbyt(maxlp3)
        addxbtv   = addicodev +forbyt(maxlp3)
        addybtv   = addxbtv   +forbyt(maxlp3)
        addyktv   = addybtv   +forbyt(maxlp3)
        addval    = addyktv   +forbyt(maxlp3)
        addtop    = addval + atebyt(maxlp3)

*@ifdef debug
*        write(6,*) 'bufszl,maxlp3=',bufszl,maxlp3
*        write(6,*) 'infothrex: addresses: ',
*     .  addheadv, addicodev, addxbtv, addybtv,addyktv,addval, addtop
*@endif

c
c     # loop over internal orbitals
c
      do 300 l1 = 1, intorb
c
         hilev  = l1
         skpint = 0
         ls     = orbsym(l1)
         lsym   = sym21(ls)

c     return the loops for lvalue l1 in common /special/
c     lpbuf is needed only for ftcalc=0,
c     if validvalues .lt. 0  maxbf is insufficient.
c     (to be extended for these cases)

        call ptimer('tloop','resm',1)
        call threxloops(l1,indxbra,indxket,ipth1,segsm,
     .                 lpbuf,validvalues,bufszl,
     .                 iwx,maxlp3 )
        call ptimer('tloop','susp',1)

        if (validvalues.lt.0)
     .   call bummer('insufficient loop space',-validvalues,2)
        if (validvalues.gt.maxlp3) then
         write(6,*) 'maxlp3=',maxlp3,' validvalues=',validvalues
         call bummer('invalid maxlp3 or validvalues for l=',l1,2)
        endif



c     assumption: valid loops from (1..validvalues+1)
c                 code values 0 (new l ) and 1 (new record) do not exist
c
c
         skpint=1
         totloops=totloops+validvalues


c        start the integral section
315      continue

c
c   asynchronous read and write currently disabled
c   we just use rdbuf(1) for reading the integrals
c
         iblnr=1
         blanz=1
         read(71,*,end=2000,err=2000) 
     .      iblock,l1bl(blanz),ksym(blanz),k1s(blanz),k1f(blanz)
c        write(0,*) 'reading ',iblock
         iblsym=1
         intblk(blanz)=1
         stx121(blanz)=1
         stx3(blanz)=1
         stx1(blanz)=1
         stx2(blanz)=1
         stw121(blanz)=1
         stw3(blanz)=1
         stw1(blanz)=1
         stw2(blanz)=1

c
c        find out how many of these blanz blocks belong to the
c        current lvalue


         if (l1bl(1).gt.l1) goto 300

         iblanz=1

c
c        initialize counter

         loop8  = 1

c        start with the x case

         ntrypr = 2
c
c        # end of read
c

c        if there are no loop contributions to the current
c        segment pair, just skip the contraction step
c        also fixes the problem of having no loop-values at all
c        for a given l; if we have encountered the end of the
c        decoded integral blocks make sure that the next decoded
c        block set does not contain contributions to the current
c        l value

         if (validvalues.eq.0) then
            if (iblanz.eq.blanz) goto 315
             go to 300
         endif

         if ( skpint .eq. 0 ) go to 315
c
c        # treatment of internal loops
c
         valrel = one
         newval = 0
c
902      continue

         if (loop8.gt.validvalues) goto 315
c
c        # structure of the formula tape three external
c        # code,wtlphd,mlp2,mlp1
c        # mlpk     loop weight of ket (y)
c        # mlpb     loop weight of bra (x,w)
c        # wtlphd   weight of the loop head
c        # ntrypr   =2 xy
c        #          =3 wy
c        # code     0 end of loop values for a given l
c        # code     1 new record
c        # code     2 new relative loop value
c        # code     3 the same relative loop value as before
c        # code     4 change xy-wy
c

         code=getinteger(lpbuf(addicodev),loop8)
         rwlphd=getinteger(lpbuf(addheadv),loop8)
         wtlphd=getinteger(lpbuf(addxbtv),loop8)
         mlpb  =getinteger(lpbuf(addybtv),loop8)
         mlpk  =getinteger(lpbuf(addyktv),loop8)

*@ifdef debug
*         write(6,*) 'code,rwlphd,wtlphd,mlpb,mlpk,loop8=',
*     .     code,rwlphd,wtlphd,mlpb,mlpk,loop8,
*     .     'val=',lpbuf(addval+loop8-1)
*C        write(6,*) 'addval+loop8-1=',addval+loop8-1
*@endif


         codep = code + 1
         go to (904,906,905,1900,950),codep

c        # code = 0  error since code stripped away
904      call bummer('threx: code=',0,faterr)

c        # code = 1, error since code stripped away
c
906      continue
         call bummer('threx: code=',1,faterr)
c
c        # code = 2, multiply by new relative loop value
c
905      continue
         val = lpbuf(addval+loop8-1)
         loop8 = loop8 + 1
         if ( (iwx + 1) .eq. ntrypr ) then
            valrel = valrel * val
            newval = 1
         endif
         go to 907
c
907      continue
         val = valrel
         valrel = one
         newval = 0
c
c        # code = 3, construction of external loops
c
1900     continue
c
c        # check whether ci vector segment is available
c
         if ( code .eq. 3 ) loop8 = loop8 + 1
            segsum(2) = segsm(2)
            cistrt(2) = cist(2)
c
c   # check for y segment
c

           segsum(1) = segsm(1)
           cistrt(1) = cist(1)

            if ( newval .gt. 0 ) then
               val = valrel
               valrel = one
               newval = 0
            endif
c
               call thresg(
     &          indsymbra,conftbra,indxbra,cibra,sgmabra,
     &          indsymket,conftket,indxket,ciket,sgmaket,
     &          rdbuf(1), 1,  1, iblstt,
     &          scr3x  )
1915        continue
         go to 902
c
c        # code = 4
c
950      continue
         ntrypr = 3
         loop8  = loop8 + 1
         go to 902
c
c        # end of do loop l1
c
c
300   continue
c
2000  continue
       close(71)
c
      if (lvlprt.ge.5)
     & write(6,*) 'threx: total number of loops=',totloops
c
C HPM   return
c       call _f_hpm_stop_(68,2217)
C HPM
      return
      end
      subroutine threx_tm(initial,indxbra,indxket,ipth1,segsm,
     &  l1,iwx)
c
c  construct the 1-internal formula tape.
c  rewritten 07/09/99 (tm)
c
c  code   loop          description
c  ----   -----        ---------------
c
c   0  -- new level ---
c   1  -- new record --
c   4  -- new vertex --
c
c   2       12c         (xy,wy) new relative loop value.
c   3       12c         (xy,wy) same loop value as before.
c
         IMPLICIT NONE

C====>Begin Module THREX_TM               File threx_tm.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXTASK
      PARAMETER           (MAXTASK = 16384)
C
      REAL*8              ONE
      PARAMETER           (ONE = 1.0D+00)
C
C     Argument variables
C
      INTEGER             INDXBRA(*),  INDXKET(*),  IPTH1(2),    IWX
      INTEGER             L1,          SEGSM(2)
C
      LOGICAL             INITIAL
C
C     Local variables
C
      INTEGER             IPATH(3),    SEGSMM(3),   SEGTYPE(3),  TASKID
C
      LOGICAL             BREAK2
C
      REAL*8              FTBUF(LBFT)
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cftb.inc'
      INCLUDE 'cibi.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cst.inc'
      INCLUDE 'cvtran.inc'
      INCLUDE 'lcontrol.inc'
      INCLUDE 'special.inc'
      INCLUDE 'tasklst.inc'
      INCLUDE 'timerlst.inc'
C====>End Module   THREX_TM               File threx_tm.f               
      ibf = 0

      if (initial) jumpinposition=0
      go to  (990,1) (jumpinposition+1)
c
990   continue
c
      nintl = 1
      maxw1 = 0
      mo(0) = 0
      nloop = 0
      qdiag = .false.
c
      numv    = 1
      nmo     = 1
      code(1) = 3
      code(2) = 2
      lmo = l1
         numw1 = 0
         mo(1) = lmo
c
        if (iwx.eq.2) then
c    #      put code 4
            call wcode_dir(ibf,4,icodev,2,break2)
            if (break2) call bummer('should never occur!',0,2)
               numw1 = numw1 + 1
             headv(ibf) =0
             xbtv(ibf)  =0
             ybtv(ibf)  =0
             yktv(ibf)  =0
             ver=4
         else
             ver=3
         endif


            oldval = one
1         continue
          call loopcalc( ver, 2, st12, db12(1,0,3), 4, 2,
     &    indxbra, indxket,segsm)


            if (break) then
            jumpinposition=1
            goto 999
            endif
c
      loopcnt(currtsk)=nloop
6010  format(' three-external ft construction complete.',
     + i12,' loops constructed for l=',i3)
6100  format(' int1:  lmo = ',i4)
c999   call timer( 'threxft',5,times(13),6)
C HPM   return
c       call _f_hpm_stop_(69,2478)
C HPM
999   return
      end
       subroutine threxloops(l1,indxbra,indxket,ipth1,segsm,
     .                 lpbuf,validvalues,bufszl,iwx,
     .                 mxlp3)
c
c     use lpbuf as buffer for diracc as well as for the code values
c     etc ...
c
c     lpbuf(1..bufszl)  : reserved for diracc
c     lpbuf(bufszl+1 .. bufszl+maxlp3) headv
c     lpbuf(bufszl+mxlp3+1 .. bufszl+2*maxlp3) icodev
c     lpbuf(bufszl+2*mxlp3+1 .. bufszl+3*maxlp3) xbtv
c     lpbuf(bufszl+3*mxlp3+1 .. bufszl+4*maxlp3) ybtv
c     lpbuf(bufszl+4*mxlp3+1 .. bufszl+5*maxlp3) yktv
c     lpbuf(bufszl+5*mxlp3+1 .. bufszl+7*maxlp3) values real*8

c
c


         IMPLICIT NONE

C====>Begin Module THREXLOOPS             File threxloops.f             
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT,      RL
C
      INTEGER             ATEBYT,      FORBYT,      RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
C
C     Argument variables
C
      INTEGER             BUFSZL,      INDXBRA(*),  INDXKET(*)
      INTEGER             IPTH1(2),    IWX,         L1,          MXLP3
      INTEGER             SEGSM(2),    VALIDVALUES
C
      REAL*8              LPBUF(*)
C
C     Local variables
C
      INTEGER             ADDHEADV,    ADDICODEV,   ADDTOP,      ADDVAL
      INTEGER             ADDXBTV,     ADDYBTV,     ADDYKTV,     CODE
      INTEGER             COPIEDVALUES,             FIN,         II
      INTEGER             LPST,        MLP1,        MLP2,        RWLPHD
      INTEGER             TASKID,      WTLPHD
C
      LOGICAL             INIT,        SKIP
C
      REAL*8              WORD8
C
      INCLUDE 'special.inc'
C====>End Module   THREXLOOPS             File threxloops.f             
C
c
c  internal stuff for loop calculation
c
c

C
        save ii,lpst,addheadv, addicodev,addxbtv, addybtv, addyktv
        save addval,addtop

c      initialization

C HPM  subroutine_start
c       call _f_hpm_start_(70,2576, "pciudg7.f","threxloops")
C HPM
       if (l1.eq.1) then
        ii=0
        lpst=1
        addheadv=bufszl+1
        addicodev = addheadv+forbyt(mxlp3)
        addxbtv   = addicodev +forbyt(mxlp3)
        addybtv   = addxbtv   +forbyt(mxlp3)
        addyktv   = addybtv   +forbyt(mxlp3)
        addval    = addyktv   +forbyt(mxlp3)
        addtop    = addval + atebyt(mxlp3)
c       write(6,*) 'info: addresses: ',
c    .  addheadv, addicodev, addxbtv, addybtv,addyktv,addval, addtop
       endif



         init=.true.
         validvalues=0
         copiedvalues=0
 80      continue
         call threx_tm(init,indxbra,indxket,ipth1,segsm,l1,iwx)

        validvalues=ibf
c       just a formal check

         if (icodev(validvalues).eq.1) then
         call copyvalues(
     .    lpbuf(addicodev),lpbuf(addheadv),lpbuf(addxbtv),
     .    lpbuf(addybtv),lpbuf(addyktv),lpbuf(addval),
     .    copiedvalues,validvalues-1)
          validvalues=0
          init=.false.
          goto 80
         endif
         goto 9999

9999   continue
         call copyvalues(
     .    lpbuf(addicodev),lpbuf(addheadv),lpbuf(addxbtv),
     .    lpbuf(addybtv),lpbuf(addyktv),lpbuf(addval),
     .    copiedvalues,validvalues)
          validvalues=copiedvalues

C HPM   return
c       call _f_hpm_stop_(70,2622)
C HPM
      return
      end
      subroutine loopcalc( btail, ktail, stype, db0, nran, ir0,
     .  indxbra,indxket,segsm)
c
c     btail = type of bracket segment
c     ktail = type of ket segment
c
c
c  construct loops and accumulate products of segment values.
c
c  input:
c  btail=    bra tail of the loops to be constructed.
c  ktail=    ket tail of the loops to be constructed.
c  stype(*)= segment extension type of the loops.
c  db0(*)=   delb offsets for the vn(*) segment values.
c  nran=     number of ranges for this loop template.
c  ir0=      range offset for this loop.
c  /cpt/     segment value pointer tables.
c  /cex/     segment type extension tables.
c  /ctab/    segment value tables.
c  /cdrt/    drt information.
c  /cdrt2/   vectors spanning number of walks.
c  /cstack/  loop construction stack arrays.
c  /cli/     loop information.
c
c  modified for mu-dependent segments 9-nov-86 (-rls).
c  this version written 07-sep-84 by ron shepard.
c
c
         IMPLICIT NONE

C====>Begin Module LOOPCALC               File loopcalc.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NST
      PARAMETER           (NST = 40)
      INTEGER             NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NWLKMX
      PARAMETER           (NWLKMX = 2**20-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER             BTAIL,       DB0(NRAN,0:2,*)
      INTEGER             INDXBRA(*),  INDXKET(*),  IR0,         KTAIL
      INTEGER             NRAN,        SEGSM(2),    STYPE(NRAN)
C
C     Local variables
C
      INTEGER             BBRA,        BCASE,       BKET,        BRASEG
      INTEGER             BROWC,       BROWN,       BVER,        DELB
      INTEGER             EXT,         I,           IGONV,       IL
      INTEGER             IMU,         INDXX,       IPATH(2),    IR
      INTEGER             ISEGXW,      ISEGYZ,      ITYPE,       J
      INTEGER             K,           KCASE,       KETSEG,      KROWC
      INTEGER             KROWN,       KSEG,        KVER,        NEXR
      INTEGER             NLEV,        NMBSEG,      NUMBEROFLOOPS
      INTEGER             OFFSETB,     OFFSETK,     R
      INTEGER             SEGTYPE(2),  STR,         TASKID,      WTLPHDB
      INTEGER             WTLPHDK
C
      LOGICAL             QBCHK(-4:4), REV,         VALID(2,2)
C
      INCLUDE 'cex.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cpt.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'ctab.inc'
      INCLUDE 'drt.inc'
      INCLUDE 'drtbra.inc'
      INCLUDE 'drtket.inc'
C
C     Common variables
C
      INTEGER             CLEV,        ILEV,        IMO,         JKLSYM
      INTEGER             JLEV,        JMO,         JUMPINPOSITION
      INTEGER             KLEV,        KLSYM,       KMO,         LLEV
      INTEGER             LMO,         LSYM,        TAILB,       TAILK
      INTEGER             VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  TAILB,       TAILK,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
      INCLUDE 'solxyz.inc'
C====>End Module   LOOPCALC               File loopcalc.f               

      data qbchk/.true.,.true.,.false.,.false.,.false.,
     +  .false.,.false.,.true.,.true./
c
c     # assign a range (ir1 to nran) for each level of the
c     # current loop type.
c
C HPM  subroutine_start
c       call _f_hpm_start_(71,2892, "pciudg7.f","loopcalc")
C HPM
      rev = btail.le.ktail
      if (rev) then
       braseg=2
       ketseg=1
      else
       braseg=1
       ketseg=2
      endif

      hilev = mo(nmo)
*@ifdef spinorbit
      if(spnorb .and. nintl .eq. 7)hilev = niot
*@endif
c
      ir = ir0
      do 120 i = 1, nmo
         ir = ir + 1
         do 110 il = mo(i-1), (mo(i)-2)
            range(il) = ir
110      continue
         ir = ir+1
         range(mo(i)-1) = ir
120   continue
c
c     # initialize stack arrays for loop construction.
c
      if (.not. break) then
      clev    = 0
      ybra(0)   = 0
      yket(0)   = 0
      brow(0) = btail
      krow(0) = ktail
      qeq(0)  = btail.eq.ktail
      v(1,0)  = one
      v(2,0)  = one
      v(3,0)  = one
      endif
c
c     # one, two, or three segment value products are accumulated.
c
c
c     # begin loop construction:
c
      if (.not. break) go to 1100
c     goto 1100
c
1000  continue
c
c     # decrement the current level, and check if done.
c
      extbk(clev) = 0
      clev = clev-1
      if(clev.lt.0) then
       break=.false.
C HPM   return
c       call _f_hpm_stop_(71,2956)
C HPM
       return
       endif
c
1100  continue
c
c     # new level:
c
      if(clev.eq.hilev)then
c
c        qdiag=t :allow both diagonal and off-diagonal loops.
c        qdiag=f :allow only off-diagonal loops.
c
         if ( qdiag .or. (.not. qeq(hilev)) ) then
            call wloop(rev)
            if (break)  Then
C HPM   return
c       call _f_hpm_stop_(71,2973)
C HPM
      RETURN
      END IF
         endif
         go to 1000
      endif
c
      nlev = clev+1
      r = range(clev)
      str = stype(r)
      nexr = nex(str)
1200  continue
c
c     # new extension:
c
      ext = extbk(clev)+1
      if(ext.gt.nexr)go to 1000
      extbk(clev) = ext
      bcase = bcasex(ext,str)
      kcase = kcasex(ext,str)
c
c     # check for extension validity.
c
c     # canonical walk check:
c
      if ( qeq(clev) .and. (bcase .lt. kcase) ) go to 1200
      qeq(nlev) = qeq(clev).and.(bcase.eq.kcase)
c
c     # individual segment check:
c
      browc = brow(clev)
      krowc = krow(clev)
      if (rev) then
        brown = lket(bcase,browc)
        krown = lbra(kcase,krowc)
      else
        brown = lbra(bcase,browc)
        krown = lket(kcase,krowc)
      endif
      if(brown.eq.0)go to 1200
      if(krown.eq.0)go to 1200
      brow(nlev) = brown
      krow(nlev) = krown
c
c     # check b values of the bra and ket walks.
c
      bbra = b(brown)
      bket = b(krown)
      delb = bket-bbra
c
c     # the following check is equivalent to:
c     # if ( abs(delb) .gt. 2 ) go to 1200
c
      if ( qbchk(delb) ) go to 1200
ctm ************************************************************+
c
c 4    diag case  <index1|index1>
c
c 8    threx 1. Segment
c               <index1|indxyz>
c               <indxyz|index1>
c
c 9    twoex 2 Segmente: xz, wz, yy,  wx
c               <index1|index2>
c               <index2|index1>
c 10   twoex 2 Segmente: xx,ww all combinations
c
c      valid(1,ksg)  bra within ksg
c      valid(2,ksg)  ket within ksg
c      ksg=2,3 = x,w  ksg=1 = yz
c
ctm ************************************************************

c
c     # bra-walk extension check:
c       valid range: ybra(nlev)+1 to ybra(nlev)+wtlphdb
c
      if (rev) then
      ybra(nlev) = ybra(clev)+yyket(bcase,browc)
      wtlphdb = xbarket(brown)
      offsetb = invldket
      else
      ybra(nlev) = ybra(clev)+yybra(bcase,browc)
      wtlphdb = xbarbra(brown)
      offsetb= invldbra
      endif

c     # invalid bra ?

        if  ( (ybra(nlev)+wtlphdb).le.offsetb .or.
     .        ybra(nlev).ge.segsm(braseg)+offsetb) goto 1200
c
c     # ket-walk extension check:
c      valid range: yket(nlev)+1 to yket(nlev)+wtlphdk
c
      if (rev) then
      yket(nlev) = yket(clev)+yybra(kcase,krowc)
      wtlphdk = xbarbra(krown)
      offsetk= invldbra
      else
      yket(nlev) = yket(clev)+yyket(kcase,krowc)
      wtlphdk = xbarket(krown)
      offsetk= invldket
      endif

        if  ( (yket(nlev)+wtlphdk).le.offsetk .or.
     .        yket(nlev).ge.segsm(ketseg)+offsetk) goto 1200

c
c check for invalid walks
c
c #   bra invalid
c
       indxx = ybra(nlev)+1-offsetb
       if (indxx.lt.1) then
          wtlphdb = wtlphdb +indxx -1
          indxx=1
       endif
       if (rev) then
         if (-indxket(indxx).ge.wtlphdb) goto 1200
       else
         if (-indxbra(indxx).ge.wtlphdb) goto 1200
       endif

c
c #   ket invalid
c


       indxx = yket(nlev)+1-offsetk
       if (indxx.lt.1) then
          wtlphdk = wtlphdk +indxx -1
          indxx=1
       endif
       if (rev) then
       if (-indxbra(indxx).ge.wtlphdk) goto 1200
       else
       if (-indxket(indxx).ge.wtlphdk) goto 1200
       endif

ctm ************************************************************

c     # accumulate the appropriate number of products:
c
      imu = mu(nlev)
      if (numv.eq.1) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      if ( abs(v(1,nlev)) .le. small ) go to 1200
      elseif  (numv.eq.2) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small      )go to 1200
      else
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      v(3,nlev) = v(3,clev)*tab(pt(bcase,kcase,db0(r,imu,3)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small .and.
     & abs(v(3,nlev)) .le. small      )go to 1200
      endif 
c
c     # passed all the tests, this is a
c     # valid segment extension, increment to the next level:
c
      clev = nlev
      go to 1100
c
      end
      subroutine wloop(rev)
c
c  append loop information to ft buffer.
c  rev: true <- loopcalcrev
c       false <- loopcalc
c
c
c  24-nov-90 l(*), y(*), and xbar(*) moved back to /cdrt/. -rls
c  09-apr-90 rwlphd->head change. rwhdmx eliminated. -rls
c  modification of packing 2-12-87 (p. szalay)


C====>Begin Module WLOOP                  File wloop.f                  
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NWLKMX
      PARAMETER           (NWLKMX = 2**20-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
C
C     Argument variables
C
      LOGICAL             REV
C
C     Local variables
C
      INTEGER             BBRA,        BKET,        DELB,        HEAD
      INTEGER             HEADK,       I,           ICD4(0:3),   ICODE
      INTEGER             IT3,         IV3(3,4),    NV,          NV3(4)
      INTEGER             NW,          TASKID,      XBT,         YBT
      INTEGER             YKT
C
      REAL*8              DWORD(4),    FTBUF(LBFT), RELVAL,      VAL(3)
C
      INCLUDE 'cex.inc'
      INCLUDE 'cftb.inc'
      INCLUDE 'cibi.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'cvtran.inc'
      INCLUDE 'drt.inc'
      INCLUDE 'drtbra.inc'
      INCLUDE 'drtket.inc'
      INCLUDE 'lcontrol.inc'
      INCLUDE 'special.inc'
C====>End Module   WLOOP                  File wloop.f                  

      equivalence ( val(1), dword(2) )
c
      data icd4/1,2,2,3/
      data nv3/1,2,2,3/
      data iv3/3,2,1, 2,3,2, 1,3,2, 1,2,3/
c
c     # the new packing method uses the head, not the weight. -rls
C HPM  subroutine_start
c       call _f_hpm_start_(72,3404, "pciudg7.f","wloop")
C HPM
      head = brow(hilev)
      headk= krow(hilev)
      ybt  = ybra(hilev)
      ykt  = yket(hilev)
ctm
      val(1)=0.0d0
      val(2)=0.0d0
      val(3)=0.0d0
ctm
      if (rev) then
        xbt  = xbarket(head)
        if ( xbt .eq. 0 .or. xbarbra(headk) .eq. 0 ) then
          write(6,6020)xbt,xbarbra(headk),hilev,head
          call bummer('wloop: skipping invalid loop, xbt=0',0,0)
          break=.false.
C HPM   return
c       call _f_hpm_stop_(72,3417)
C HPM
          return
        endif
      else
        xbt  = xbarbra(head)
        if ( xbt .eq. 0 .or. xbarket(headk) .eq. 0 ) then
          write(6,6020)xbt,xbarket(headk),hilev,head
          call bummer('wloop: skipping invalid loop, xbt=0',0,0)
          break=.false.
C HPM   return
c       call _f_hpm_stop_(72,3428)
C HPM
          return
        endif
      endif
c
*@ifdef spinorbit
      go to (1100,1200,1300,1400,1500,1600,1700), nintl
*@else
*      go to (1100,1200,1300,1400,1500,1600), nintl
*@endif
1100  continue
c
c     # 1-internal contributions:
c
      relval = v(1,hilev)/oldval
      if ( abs(relval-one) .gt. small ) then
c
c        # new relative loop value.
c
         icode = code(2)
         nv = 1
         val(1) = relval
         oldval = v(1,hilev)
      else
c
c        # same relative loop value.
c
         icode = code(1)
         nv = 0
      endif
      numw1 = numw1+nv+1
      go to 2000
c
1200  continue
c
c     # 2-internal contributions:
c
      if ( numv .eq. 1 ) then
         nv = 1
c        val(1) = vcoeff(vtpt)*v(1,hilev)
         val(1)=1.0d0
         icode = code(1)
         go to 2000
      endif
c
      val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
c     val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
      val(2) =1.0d0 
      if ( val(1) .eq. zero ) then
         nv = 1
         val(1) = val(2)
         icode = code(2)
      else
         nv = 2
         icode = code(1)
      endif
      go to 2000
c
1300  continue
c
c     # 3-internal contributions:
c
      if(numv.eq.1)then
         nv = 1
c        val(1) = vcoeff(vtpt)*v(1,hilev)
         val(1) = 1.0d0
         icode = code(1)
      else
         val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
c        val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
         val(2) =1.0d0
         if((val(1).eq.zero) .and. (code(2).ne.0))then
            nv = 1
            val(1) = val(2)
            icode = code(2)
         else
            nv = 2
            icode = code(1)
         endif
      endif
      go to 2000
c
1400  continue
c
c     # 4-internal contributions:
c
      go to (1410,1420,1430),numv
1410  continue
c
c     # one-value loops:
c
      nv = 1
c     val(1) = vcoeff(vtpt)*v(1,hilev)
      val(1) =1.0d0
      icode = code(1)
      go to 2000
c
1420  continue
c
c     # two-value loops:
c
      val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
c     val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
      val(2) = 1.0d0
      if ( (val(1) .eq. zero) .and. (code(2) .ne. 0) ) then
         nv = 1
         val(1) = val(2)
         icode = code(2)
      else
         nv = 2
         icode = code(1)
      endif
      go to 2000
c
1430  continue
c
c     # three-value loops:
c
      it3 = 1
      if(v(2,hilev).ne.zero)it3 = it3+1
      if(v(1,hilev).ne.zero)it3 = it3+2
c
c     # it3=1  12c   .ne.0
c     # it3=2  12bc  .ne.0
c     # it3=3  12ac  .ne.0
c     # it3=4  12abc .ne.0
c
      nv = nv3(it3)
c     val(1) = v( iv3(1,it3), hilev)
c     val(2) = v( iv3(2,it3), hilev)
c     val(3) = v( iv3(3,it3), hilev)
      val(1) = 1.0d0
      val(2) = 1.0d0
      val(3) = 1.0d0
      icode = code(it3)
      go to 2000
c
1500  continue
c
c     # two internal diagonal contributions:
c
      nv = numv
      if ( nv .eq. 1 ) then
c        val(1) = vcoeff(vtpt)*v(1,hilev)
         val(1) =1.0d0
      else
c        val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
c        val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
          val(1)=1.0d0
          val(2)=1.0d0
      endif
      icode = code(1)
      go to 2000
c
1600  continue
c
c     # four internal diagonal:
c     # see routine diag for code definitions.
c
c
c  speed up loop calculation by not computing the actual loop values
c
      nv = numv
      if(nv.eq.1)then
c        val(1) = vcoeff(vtpt)*v(1,hilev)
         val(1) =1.0d0
         icode = code(icd4(bcasex(extbk(hilev-1),3)))
      else
c        val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
c        val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
         val(1) =1.0d0
         val(2) =1.0d0
         icode = code(1)
      endif
      goto 2000
c
*@ifdef spinorbit
 1700 continue
c
      nv = numv
      bbra = b(head)
      bket = b(headk)
      delb = bbra - bket
      if(abs(delb) .eq. 2 .or. delb .eq. 0 .and. bket .ne. 0) then
         val(1) = vcoeff(vtpt)*v(1,hilev)
         icode = code(1)
         goto 2000
      else
C HPM   return
c       call _f_hpm_stop_(72,3600)
C HPM
         return
      endif
*@endif
c
2000  continue
c
c     # process loop.
c
      nloop = nloop + 1

c
c     # with the segment specific DRTs we have
c     # the problem with leading invalid walks coded
c     # in the DRT but not present in the index
c     # vector, in order to guarantee a new index
c     # vector which is never longer than the
c     # original one
c     # HENCE, yybra,ybra,xbarbra refer to the DRT
c     #  but   yktv,ybtv,xbtv,xktv are later used
c     #  with  respect to the index vector
c
c       DRT:   -2 -1 1 2 3 -2 -1 4 5 6 -2 -1
c     Index:         1 2 3 -2 -1 4 5 6
c
c     ybtv,yktv -> mlp,mlppr must be reduced by the
c                  missing leading walks
c      Die fr�here Annahme war gewesen, dass durch
c      schlichtes Modifizieren von xbar(1-4) und
c      yk(level=0) der entsprechende Shift bei den
c      Counting Indices zu erreichen ist. Das ist
c      aber nicht der Fall!!! Daher ist der Shift
c      jetzt explicit an diese Stelle eingefuegt
c      worden. Solange explizit Abfragen der Art
c       if (mlp+wtlphd.gt.0)
c      vorhanden sind funktioniert alles, solange
c      bei allen weiteren Anwendungen der in
c      yktv,ybtv steckende Offset nicht verloren
c      gehen kann
c
c
c

      ibf=ibf+1
      headv(ibf) = brow(hilev)
      hltv(ibf)  = hilev
      icodev(ibf)=icode
      if (rev) then
      yktv(ibf)  = ybra(hilev)-invldket
      ybtv(ibf)  = yket(hilev)-invldbra
      xbtv(ibf)  = xbarket(head)
      xktv(ibf)  = xbarbra(headk)
      else
      ybtv(ibf)  = ybra(hilev)-invldbra
      yktv(ibf)  = yket(hilev)-invldket
      xbtv(ibf)  = xbarbra(head)
      xktv(ibf)  = xbarket(headk)
      endif

      if(icode.eq.4 .and.xbtv(ibf).eq.1 .and.xktv(ibf).eq.3
     &     .and. ybtv(ibf).eq. 5276 .and. yktv(ibf).eq.6621)then
        call bummer('SO code: 5276 6621',0,2)
      endif

c     do i=1,nv
c        valv(ibf,i)=val(i)
c     enddo
c     do i=nv+1,3
c      valv(ibf,i)=0.0d0
c     enddo
        valv(1,ibf)=val(1)
        valv(2,ibf)=val(2)
        valv(3,ibf)=val(3)

c      write(6,6010)icode,xbt,ybt,ykt,head,(val(i),i=1,nv)
       break=(ibf.eq.maxbf-1)
       if (break) then
c     if (ibf.eq.maxbf-1) then
c      ibf=ibf+1
       ibf=maxbf
       icodev(ibf)=1
c      break=.true.
c      write(6,6010)icode,xbt,ybt,ykt,head,(val(i),i=1,nv)
c     else
c      break =.false.
      endif

C HPM   return
c       call _f_hpm_stop_(72,3682)
C HPM
      return
6010  format(' wloop: code,xbt,ybt,ykt,head,val=',
     + i2,4i6,3f10.6)
6020  format(' test in wloop, xbt=0',10i4)
      end
