!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       integer function cntref(list,length)
c
c list is an integer array of reference tags
c length is the length of that array
c written by eric stahlberg 8-15-89
c

C
C     Argument variables
C
      INTEGER         LENGTH,      LIST(LENGTH)
C
C     Local variables
C
      INTEGER         COUNTR,      I
C


      countr = 0
      do 100 i = 1, length
         if ( list(i) .gt. 0 ) countr = countr + 1
100   continue
      cntref = countr
c
      return
      end

      integer function mxscr( size, progcd )
c
c  this function returns the amount of scratch space necessary for
c  the program section specifed by the character variable progcd.
c  size = an integer parameter used to determine workspace.
c
c  19-dec-90 machine-dependent allocations removed. case independent
c            string comparisons used. -rls
c
         IMPLICIT NONE

C
C     External functions
C
      EXTERNAL            STREQ
C
      LOGICAL             STREQ
C
C     Parameter variables
C
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*(*)       PROGCD
C
      INTEGER             SIZE
C

C
C     External functions
C
C
C
C     Parameter variables
C
C
C     Argument variables
C
C
C
c
C     Common variables
C
C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS
C
C     Equivalenced common variables
C
      INTEGER             TAPE6
C
C
C     Equivalenced common variables
C
C
      equivalence (tape6,nunits(1))
c
c
c
c     # bummer error types.
c
      mxscr = 0
c
      if ( streq( progcd, 'ci4x' ) )then
c
c-*mdc*if cray + (ibm * vector) + alliant + titan + (sun * argonne)
c        # size = dimension of the largest external orbital block.
         mxscr = size * size + size + size * size
c-*mdc*else
c
c non-matrix call machines need no extra
c
          mxscr = 1
c
c-*mdc*endif
      elseif ( streq( progcd, 'ci3x' ) ) then
c
c-*mdc*if cray alliant titan or (sun .and. argonne)
c        # size = dimension of the external orbital space.
         mxscr = size * size
c-*mdc*else
c
c non-matrix call machines need no extra
c
          mxscr = 1
c-*mdc*endif
      elseif ( streq( progcd, 'ci2x' ) ) then
c        # size = largest number of orbitals in any single irrep
         mxscr = size*size
      elseif ( streq( progcd, 'ci1x' ) ) then
c        # size = dimension of the largest external orbital block.
         mxscr = 1
      else
         write(tape6, * ) 'unknown program code in mxscr -- ', progcd
         call bummer('mxscr: unknown string',0,faterr)
      endif
      return
      end
      subroutine rddrt(
     & nmot,   niot,   nsym,   nrowmx,
     & nrow,   nwalk,  lenbuf, nzwalk,
     & nvwxy,  map,    modrt,  syml,   a,
     & b,      l,      y,      ref,
     & slabel, buf,    ndrt,
     & xbar,   mu,     nj,     njskp,
     & limvec, csym,arcsym,ssym,mult)
c
c  read some of the drt arrays from the drt file.
c
c  title and dimension information have already been read.
c  the file is assumed to be positioned correctly for the
c  next records.
c  vectors are buffered in records of length lenbuf.
c
c  some of these arrays have already been read from the ft files,
c  and are skipped over here.
c
c  10-dec-90 version=3 drt changes. -rls
c
         IMPLICIT NONE


C
C     Parameter variables
C
      INTEGER         NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER         NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER         NSYM,NROW,LENBUF,NROWMX
      CHARACTER*4         SLABEL(NSYM)
C
      INTEGER         A(NROW),     B(NROW),     BUF(LENBUF), CSYM(*)
      INTEGER         L(0:3,NROWMX,3)
      INTEGER         LIMVEC(*),   MAP(*),      MODRT(*),    MU(*)
      INTEGER         NDRT,        NIOT,        NJ(0:NIMOMX)
      INTEGER         NJSKP(0:NIMOMX),          NMOT
      INTEGER         NVWXY,       NWALK
      INTEGER         NZWALK,      REF(NZWALK), SYML(NIOT)
      INTEGER         XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
      INTEGER       ARCSYM(0:3,0:NIMOMX)
      integer       ssym,mult(maxsym,maxsym)
C
C     Local variables
C
      INTEGER         I,           ICOUNT,      ISYM,        J
      INTEGER         NLEVEL,      NX(0:1)
      INTEGER         LIMVECINFO(3), numv1,ncsft,icd(10)
C
C     Common variables
C
      INTEGER         BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER         INTORB,      LAMDA1
      INTEGER         MAXBL2,      MAXLP3
      INTEGER         MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER         MXORB,       N0EXT
      INTEGER         N1EXT,       N2EXT
      INTEGER         N3EXT,       N4EXT
      INTEGER         ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER         NEXT,        NFCT,        NINTPT
      INTEGER         NMONEL,      NVALW,       NVALWK
      INTEGER         NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER         PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER         SPCCI,       TOTSPC
C
      COMMON /INF    /  PTHZ,        PTHY,        PTHX,        PTHW
      COMMON /INF    /  NINTPT,      DIMCI
      COMMON /INF    /  LAMDA1,      BUFSZI
      COMMON /INF    /  NMONEL,      INTORB
      COMMON /INF    /  MAXLP3,      BUFSZL
      COMMON /INF    /  MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON /INF    /  NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON /INF    /  ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON /INF    /  N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON /INF    /  SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON /INF    /  NVALZ,       NVALY,       NVALX,       NVALW
      COMMON /INF    /  NFCT,        NELI
C
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c      N1EXT,     number of one-external off-diagonal integrals
c     N2EXT       number of two-external off-diagonal integrals
c     N3EXT,      number of three-external off-diagonal integrals
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)


C     Common variables
C
      INTEGER         MODRT2(NIMOMX),           NAPSY(MAXSYM)
      INTEGER         NBPSY(MAXSYM),            NMPSY(MAXSYM)
C
      COMMON /ORBINF /  NMPSY,       NBPSY,       NAPSY,       MODRT2
       integer nmulmx,multmx
       parameter (nmulmx=9,multmx=19)
       integer, allocatable :: ccsym_local(:)

       include 'solxyz.inc'
C
c
c
c   nbpsy(*)  number of basis functions per irrep
c   nmpsy(*)  number of molecular orbitals per irrep
c   napsy(*)  nmpsy(*) -number of external basis functions per irrep
c   modrt2(*) mapping of mo to drtlevel excluding frozen core/frozen vir
c
c  nbas muss in cidrtfl ausgelesen und gesetzt werden statt aus cisrtif
c  zu werden!!!
c

C
c
      nlevel = niot + 1
C
c     # slabel(*)...
      call rddbl( ndrt, lenbuf, buf, nsym )
      call wtlabel(slabel,nsym,'(a4)',buf)
c     do 10 i = 1, nsym
c        write( slabel(i), '(a4)' ) buf(i)
c10    continue
c
c     # map(*)...
      call rddbl( ndrt, lenbuf, map,  nmot )
c
c     # mu(*)...
      call rddbl( ndrt, lenbuf, mu, niot )
c
c     # nbpsy(*)...Nr. of basis fuctions per irrep
      call rddbl( ndrt, lenbuf, nbpsy, nsym )
c
c     # nmpsy(*)...Nr. of MOs pre irrep
      icount = 1
         do isym=1,nsym
         nmpsy(isym) = nbpsy(isym)
          do i=1,nbpsy(isym)
c..  no FC
             if (map(icount).eq.-1) nmpsy(isym) = nmpsy(isym)-1
c..  no FV
             if (map(icount).eq. 0) nmpsy(isym) = nmpsy(isym)-1
            icount = icount + 1
          enddo ! i=1,nbpsy(isym)
         enddo ! i=1,nsym
c
c     # nexo(*)...
      call rddbl( ndrt, lenbuf, buf, -nsym )
c
c     # nviwsm(*)...
      call rddbl( ndrt, lenbuf, buf, -4*nsym )
c
c     # ncsfsm(*)...
      call rddbl( ndrt, lenbuf, buf, -4*nsym )
c
c     # modrt(*)...
      call rddbl( ndrt, lenbuf, modrt, niot )
c
      do i=1,niot
      icount = 0
         do j=1,modrt(i)
c..  no FC
        if (map(j).eq.-1) icount = icount + 1
c..  no FV
        if (map(j).eq. 0) icount = icount + 1
       enddo ! j=1,modrt(i)
      modrt2(i) = modrt(i) - icount
      enddo ! i=1,niot
c
c     # syml(*)...
      call rddbl( ndrt, lenbuf, syml, niot )

!
!   ## calculate the arcsym values
!
        DO i = 1, niot 
          arcsym(0,i-1) = 1
          arcsym(1,i-1) = syml(i)
          arcsym(2,i-1) = syml(i)
          arcsym(3,i-1) = 1
        END DO
!

c
c     # nj(*)...
      call rddbl( ndrt, lenbuf, nj, nlevel )
c
c     # njskp(*)...
      call rddbl( ndrt, lenbuf, njskp, nlevel )
c
c     # a(*)...
      call rddbl( ndrt, lenbuf, a, nrow )
c
c     # neli = number of internal electrons = 2*a(nrow)+b(nrow)
      neli = 2*a(nrow)
c
c     # b(*)...
      call rddbl( ndrt, lenbuf, b, nrow )
      neli = neli + b(nrow)
c
c     # l(*,*,*)...
      do 20 i = 1, 3
         call rddbl( ndrt, lenbuf, l(0,1,i), 4*nrow )
20    continue
c
c     # y(*,*,*)...
      do 30 i = 1, 3
         call rddbl( ndrt, lenbuf, y(0,1,i), 4*nrow )
30    continue
c
c     # xbar(*,*)...
      do 40 i = 1, 3
         call rddbl( ndrt, lenbuf, xbar(1,i) , nrow )
40    continue
c     # limvec(*)
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed limvec)
      call rddbl( ndrt, lenbuf, limvec, limvecinfo(2) )
      call uncmprlimvec(limvec,nwalk,limvecinfo(1),limvecinfo(2),
     .  limvecinfo(3))


       numv1 = 0
       call skpx01( nwalk, limvec, 0, numv1 )

       icd(1)=1              ! row
       icd(2)=icd(1)+niot+1  ! step
       icd(3)=icd(2)+niot+1  ! wsym
       icd(4)=icd(3)+niot+1  ! ytot
       icd(5)=icd(4)+niot+1  ! nviwsm
       icd(6)=icd(5)+nsym*4  ! ncsfsm
       icd(7)=icd(6)+nsym*4
       if (icd(7).gt.lenbuf) call bummer('insufficient memory',icd(7),2)

        call  symwlk( 
     . niot,   nsym,   nrow,   nrowmx, 
     . nwalk,  l,  y, buf(icd(1)),
     . buf(icd(2)),csym,buf(icd(3)),arcsym, 
     . buf(icd(4)),   xbar,   ssym, 
     . mult,limvec, nexw,   ncsft, 
     . buf(icd(5)),buf(icd(6)), spnorb, b, 
     . spnir,  multmx, spnodd ,numv1 )




c     # 0/1 limvec(*) for all the walks...
c     # in the diagonalization program.
c     # input: limvec(*)=  0  0  3  2  1  0  0  2  1  0
c     # output:limvec(*)=  1  2 -3 -2 -1  3  4 -2 -1  5
c
      j = 0
      do i = 1, nwalk
         if ( limvec(i) .eq. 0 ) then
            j = j + 1
            limvec(i) = j
         else
            limvec(i) = -limvec(i)
         endif
      enddo
c
*@ifdef debugcsym
*!
*!     # csym(*)  for valid y,x,w walks...
*        allocate (ccsym_local(nvwxy))
*        CALL rddbl(ndrt,lenbuf,ccsym_local,nvwxy)
*        write(0,*) 'checking for consistent csym(*)  ...'
*        do i=1,nvwxy
*          if (ccsym_local(i).ne.csym(nvalz+i)) then
*            write(0,*) i,ccsym_local(i),csym(nvalz+i)
*          endif
*        enddo
*        write(0,*) '... done'
*        deallocate(ccsym_local)
*         stop 9999
*@else
c       CALL rddbl(ndrt,lenbuf,buf,-nvwxy)
*@endif
c
c     # 0/1 ref(*)...
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed ref)
      call rddbl( ndrt, lenbuf, ref, limvecinfo(2) )
      call uncmprlimvec(ref,nzwalk,limvecinfo(1),limvecinfo(2),
     .  limvecinfo(3))
c     call rddbl( ndrt, lenbuf, ref, nzwalk )
c

      return
      end
      subroutine rdhdrt(
     & title,  nmot,   niot,   nfct,
     & nfvt,   nrow,   nsym,   ssym,
     & xbar,   nvalw,  ncsft,  lenbuf,
     & nwalk,  niomx,  nmomx,  nrowmx,
     & nwlkmx, buf,    nlist,  ndrt ,
     & ftcalc, spnorb, spnodd, lxyzir,hmult )

c
c  read the header info from the drt file.
c
         IMPLICIT NONE


C
C     Parameter variables
C
      INTEGER         WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER         NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*80        TITLE
C
      INTEGER         BUF(*),      FTCALC,      LENBUF
      INTEGER         LXYZIR(3),   NCSFT,       NDRT,        NFCT
      INTEGER         NFVT,        NIOMX,       NIOT,        NLIST
      INTEGER         NMOMX,       NMOT,        NROW,        NROWMX
      INTEGER         NSYM,        NVALW(4),    NWALK,       NWLKMX
      INTEGER         SSYM,        XBAR(4),hmult
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER         IERR,        NHDINT,      NVALWT,      VRSION
C

      integer nrowx,niotx
      COMMON /DRTINFO/ nrowx,niotx

       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c

      rewind ndrt
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
       if (me.eq.0) write(nlist,6010)title
*@ifdef spinorbit
      read(ndrt,*,iostat=ierr)spnorb, spnodd, lxyzir,hmult
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if (me.eq.0) write(6,*)"spnorb, spnodd, lxyzir,hmult",
     .   spnorb, spnodd, lxyzir,hmult
*@endif
c
c     # number of integer drt parameters, buffer length, and version.
c
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 6 ) then
         call bummer
     .    ('DRT version 6 required (index vector compression)',vrsion,2)
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning: rdhdrt: drt version .ne. 3 *** vrsion=',vrsion
*      endif
*@endif
c
c     # header info...
c
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      nmot     = buf(1)
      niot     = buf(2)
      nfct     = buf(3)
      nfvt     = buf(4)
      nrow     = buf(5)
      nsym     = buf(6)
      ssym     = buf(7)
      xbar(1)  = buf(8)
      xbar(2)  = buf(9)
      xbar(3)  = buf(10)
      xbar(4)  = buf(11)
      nvalw(1) = buf(12)
      nvalw(2) = buf(13)
      nvalw(3) = buf(14)
      nvalw(4) = buf(15)
      ncsft    = buf(16)
      niotx = niot
      nrowx = nrow

c
      nwalk  = xbar(1)  + xbar(2)  + xbar(3)  + xbar(4)
      nvalwt = nvalw(1) + nvalw(2) + nvalw(3) + nvalw(4)
c
      if (me.eq.0) then
      write(nlist,6020)nmot,niot,nfct,nfvt,nrow,nsym,ssym,lenbuf
      write(nlist,6100)'nwalk,xbar',nwalk,xbar
      write(nlist,6100)'nvalwt,nvalw',nvalwt,nvalw
      write(nlist,6100)'ncsft',ncsft
c
      if(niot.gt.niomx)then
         write(nlist,6900)'niot,niomx',niot,niomx
         call bummer('rdhead: niomx=',niomx,faterr)
      endif
      if(nmot.gt.nmomx)then
         write(nlist,6900)'nmot,nmomx',nmot,nmomx
         call bummer('rdhead: nmomx=',nmomx,faterr)
      endif
      if(nrow.gt.nrowmx)then
         write(nlist,6900)'nrow,nrowmx',nrow,nrowmx
         call bummer('rdhead: nrowmx=',nrowmx,faterr)
      endif
      if((nwalk.gt.nwlkmx).and.(ftcalc.eq.0))then
         write(nlist,6900)'nwalk,nwlkmx',nwalk,nwlkmx
         call bummer('rdhead: nwlkmx=',nwlkmx,faterr)
      endif
      endif
c
      return
c
6010  format(/' drt header information:'/1x,a)
6020  format(
     & ' nmot  =',i6,' niot  =',i6,' nfct  =',i6,' nfvt  =',i6/
     & ' nrow  =',i6,' nsym  =',i6,' ssym  =',i6,' lenbuf=',i6)
6100  format(1x,a,':',t15,5i9)
6900  format(/' error: ',a,3i10)
      end

      subroutine diraccnew(
     & ifile,  rdwr,   a,      number,
     & nrec,   recst,  recend  )
c
c  # since no dense index vector file needs to
c  # be written any longer, there is no need
c  # to use an a(*) of type integer
c    (tm)
c
c
         IMPLICIT NONE
C
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER         RL
C
C     Parameter variables
C
      INTEGER         WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER         NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
      INTEGER         IREAD
      PARAMETER           (IREAD = 1)
      INTEGER         IWRITE
      PARAMETER           (IWRITE = 2)
      integer mxunit
      parameter(mxunit=98)
C
C     Argument variables
C
      INTEGER         IFILE,       NREC,        NUMBER
      INTEGER         RDWR,        RECEND,      RECST, FIN
      REAL*8            A(*)
C
C     Local variables
C
      INTEGER         I,  SUM
      integer fadr,ncp
c
c     since it is also used for the distribution of
c     fil4w etc.
c
      real*8 buffer(300000)


       integer  isflvdsk(mxunit),voffset,vsize,top
       integer  filestart(mxunit)

        double precision, pointer :: vdsk(:)
        COMMON /VPOINTER/ vdsk,isflvdsk,voffset,vsize,filestart,top(2)



c
c emulating virtual disk
c vdsk(voffset:voffset+vsize)  virtual disk space
c isflvdsk(*)                  is unit virtual disk or global array or l
c filestart(*)                 offset of the first element of file on vd
c top                          first free element of vdisk
c
c

c
c    rdwr=1  read
c     rdwr=2  write
c     a  array to be read or written
c     number length of the array a
c     nrec  record length (in real*8 words)
c     recst  number of record at which read or write starts
c     recend  number of record at which read or write terminates
c
c
      recend = (number - 1) / (nrec) + recst
      if (nrec.gt.300000 .and. isflvdsk(ifile).eq.0)
     .  call bummer('diraccnew: Internal buffer too small',nrec,2)


c
c  # dies setzt voraus, dass a in diracc als
c  # real*8 array gesetzt ist.
c  # dies sollte man vielleicht generell aendern, so
c  # dass direct in DP units geschrieben wird (forbyt,atebyt)
c  # nutzen!
c
c
      call ptimer('tint','resm',1)
c
c     file is on virtual disk
c
      if (isflvdsk(ifile).eq.1) then
c       includes the offset
        fadr=filestart(ifile)+(recst-1)*nrec
        if (rdwr.eq.iread) then
            call dcopy_wr (number,vdsk(fadr),1,a,1)
        elseif(rdwr.eq.iwrite) then
            call dcopy_wr (number,a,1,vdsk(fadr),1)
        else
         call bummer('invalid rdwr mode in diracc',rdwr,2)
       endif
       call ptimer('tint','susp',1)
       return

c     file is on global array
c
      elseif (isflvdsk(ifile).eq.2) then
        fadr=(recst-1)*nrec+1
        if (rdwr.eq.iread) then
           call getvecp(ifile,1,a,fadr,number)
        elseif (rdwr.eq.iwrite) then
           call putvecp(ifile,1,a,fadr,number,0)
        else
         call bummer('invalid rdwr mode in diracc',rdwr,2)
        endif
       call ptimer('tint','susp',1)
      else

c
c     file is on local disk
c
      if ( rdwr .eq. iread ) then
         sum = 1
         do 10 i = recst, recend
            call readda( ifile, i, buffer, nrec )
            ncp=min(nrec,number-sum+1)
            call dcopy_wr(ncp,buffer,1,a(sum),1)
            sum = sum + nrec
10       continue
      elseif ( rdwr .eq. iwrite ) then
         sum = 1
         do 110 i = recst, recend
            ncp=min(nrec,number-sum+1)
            call dcopy_wr(ncp,a(sum),1,buffer,1)
            call writda( ifile, i,buffer, nrec )
            sum = sum + nrec
110      continue
      else
         call bummer('diracc: unknown rdwr=', rdwr, faterr )
      endif
      endif
c
      return
      end


c deck ufvout
c *** this routine is incremental ***
      subroutine ufvoutnew(
     & unit,   reclen, ivec,   ciuvfl,
     & lenci,  ninfo,  info,   nenrgy,
     & ietype, energy, ntitle, title,
     & heap,   totfre, loglen  )

ctm
c     lenci : number of elements to be written to sequential file
c     loglen: logical length of ci vector
ctm
c
c  this subroutine writes the specified CI vector
c  in a standard sequential unformatted vector format.
c
c  author: eric stahlberg
c  date  : 10-oct-90
c  version: 2.0
c
      implicit logical(a-z)

      integer unit, ivec, lenci, totfre, ciuvfl, reclen
      integer loglen
      real*8 heap(*)
      integer ntitle, nenrgy, ninfo
      character*80 title(*)
      real*8 info(*), energy(*)
      integer ietype(*)
c
c     # local variables.
c
      integer i, blksiz, recamt, icsf
      integer versn
c
c     # curver is the current version number for this writing routine.
c
      integer    curver
      parameter (curver = 1)

c
      blksiz = min( totfre, lenci, reclen )
      versn = curver
c
c     # file header.
      write(ciuvfl) versn, blksiz, lenci
c     # general information.
      write(ciuvfl ) ninfo, nenrgy, ntitle
      write(ciuvfl) (info(i),i=1,ninfo)
      write(ciuvfl) (title(i),i=1,ntitle)
      write(ciuvfl) (ietype(i),i=1,nenrgy),(energy(i),i=1,nenrgy)
c     # list of coefficients.
      do 10 icsf = 1, lenci, blksiz
         recamt = min (blksiz, (lenci - icsf + 1))
         call getvecp( unit, ivec, heap, icsf, recamt )
          write(6,*) 'ufvoutnew: ... writing ',' recamt=',recamt
         call seqwbf(ciuvfl,heap,recamt)
10    continue
c
      return
      end
c
c***********************************************************
c
      subroutine  wtlabel(slabel,nsym,fmt,buf)
      implicit none
      integer nsym
      integer i
      character*4 slabel(nsym),buf(nsym)
       character*(*) fmt
      do 10 i = 1, nsym
         write( slabel(i), '(a4)' ) buf(i)
10    continue
      return
      end


      subroutine symwlk( 
     . niot,   nsym,   nrow,   nrowmx, 
     . nwalk,  l,      y,      row, 
     . step,   csym,   wsym,   arcsym, 
     . ytot,   xbar,   ssym, 
     . mult,   limvec, nexw,   ncsft, 
     . nviwsm, ncsfsm, spnorb, b, 
     . spnir,  multmx, spnodd , numv1  )
!
!  this routine calculates the symmetry of all the valid internal
!  walks and computes csf counts.
!
!  output:
!  csym(*) = mult( internal_walk_symmetry, ssym ) for
!            the valid z,y,x,w-walks.
!  ncsft = total number of csfs for this orbital basis and drt.
!  nviwsm(1:4,1:nsym) = number of valid internal walks of each symmetry
!                       through each vertex.
!  ncsfsm(1:4,1:nsym) = number of csfs passing through each vertex
!                       indexed by the internal symmetry.
!
!  07-jun-89  skip-vector based walk generation. rls
!
      implicit none
!====>Begin Module SYMWLK                 File cidrt2.f
!---->Makedcls Options: All variables
!     Parameter variables
!
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
      INTEGER         NIMOMX
      PARAMETER           (NIMOMX = 128)
!
!     Argument variables
!
!
      INTEGER             ARCSYM(0:3,0:NIMOMX),       B(*),   CSYM(*)
      INTEGER             L(0:3,NROWMX,3),          LIMVEC(*)
      INTEGER             MULT(8,8),   MULTMX,      NCSFSM(4,NSYM)
      INTEGER             NCSFT,       NEXW(8,4),   NIOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVIWSM(4,NSYM)
      INTEGER             NWALK,       ROW(0:NIOT), SPNIR(MULTMX,MULTMX)
      INTEGER             SSYM,        STEP(0:NIOT)
      INTEGER             WSYM(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3),          YTOT(0:NIOT)
!
      LOGICAL             SPNODD,      SPNORB
!
!     Local variables
!
      CHARACTER           WXYZ(4)
!
      INTEGER             CLEV,        I,           IMUL,        ISSYM
      INTEGER             ISTEP,       ISYM,        MUL,         NLEV
      INTEGER             NUMV1,       NVALWT,      ROWC,        ROWN
      INTEGER             VER,         VER1,        VERS(4)
!
!====>End Module   SYMWLK                 File cidrt2.f
      data vers/1,2,3,3/
      data wxyz/'z','y','x','w'/
!
!
!     # limvec(*) in skip-vector form.
!
      nvalwt = 0
!
      call izero_wr( 4*nsym, nviwsm, 1 )
!
!     # loop over z, y, x, and w vertices.
!
      do 200 ver = 1, 4
         ver1 = vers(ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
         wsym(0) = 1
!
         do 50 i = 0, niot
            step(i) = 4
50       continue
!
100      continue
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
!           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)go to 180
            clev = clev-1
            goto 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         row(nlev) = rown
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
         if( limvec( ytot(nlev)+1 ) .ge. xbar(rown,ver1) )goto 100
         wsym(nlev) = mult( arcsym(istep,clev), wsym(clev) )
         if(nlev.lt.niot)then
!           # increment to the next level.
            clev = nlev
            go to 100
         endif
!        # a complete valid walk has been generated.
         mul = 1
         if(spnorb)mul = b(rown) + 1
         do 179 imul = 1, mul
               isym  = mult(wsym(nlev), spnir(imul, mul))
               issym = mult(isym,ssym)
               if ( nexw(issym,ver) .ne. 0 ) then
                  nvalwt = nvalwt+1
                  csym(nvalwt) = issym
                  nviwsm(ver,isym) = nviwsm(ver,isym) + 1
               endif
179      continue
!
         go to 100
!
180      continue
!        # finished with walk generation for this vertex.
200   continue
!
!     # check the valid upper walk count...
      if(numv1.ne.nvalwt)
     . call bummer('symwlk: numv1-nvalwt=', numv1-nvalwt,2)
!
      return
!
6100  format(1x,a,':',(8i8))
      end

