!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*@if defined (cray) || defined (t3e64)
*@define allcrays
*@endif
*@if defined (ibm) || defined (vector)
*@defined vectoribm
*@endif
      subroutine wx(
     &      indsymbra,conftbra,indxbra,cibra,sgmabra,
     &      indsymket,conftket,indxket,ciket,sgmaket,
     & trans,  scr1,   scr2,   scr3 ,segsm )
c
c     two external w'-x
c
         IMPLICIT NONE

C====>Begin Module WX                     File wx.f                     
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    SCR1(*),     SCR2(*)
      REAL*8              SCR3(*),     SGMABRA(*),  SGMAKET(*)
      REAL*8              TRANS(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         M1
      INTEGER             M1LP,        M1LPR,       MUL,         NMB1
      INTEGER             NMB1A,       NMB2,        NMB2A,       TASKID
      INTEGER             WS,          WSYM,        XS,          XSYM
C
      LOGICAL             SKIP,        WALK
C
      INCLUDE 'counter.inc'
      INCLUDE 'data.inc'
      INCLUDE 'debug.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'twoext.inc'
      INCLUDE 'twoex_stat.inc'
C====>End Module   WX                     File wx.f                     
      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0
      if(spnodd)niot = niot + 1
      call upwkst(hilev,  niot, rwlphd, mlppr,  mlp, clev  )
c
260   continue
      call upwlknew( ket, bra, walk, mul,segsm(2),segsm(1),skip )
      if ( walk )then
         if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(47,1034)
C HPM
         return
      else
         if (skip) goto 2410
      endif

c
         nmb2a=indxbra(bra)
         if(nmb2a.lt.0)go to 2410
         nmb1a=indxket(ket)
         if(nmb1a.lt.0)go to 2410
         nmb1=(nmb1a-1)/isize_xx+1
         nmb2=(nmb2a-1)/isize_ww+1
         if (ksym.ne.lsym) then
          cnt2x_wx(nmb2,nmb1)=cnt2x_wx(nmb2,nmb1)+2
         else
          cnt2x_wx(nmb2,nmb1)=cnt2x_wx(nmb2,nmb1)+1
         endif

         wx2x=wx2x+1
c
2410  continue
      if(mul.gt.1 .or. hilev.lt.niot)go to 260
      if(spnodd)niot = niot - 1
      return
      end

      subroutine xxww(
     &    indsymbra, conftbra, indsymket,conftket,
     &    indxbra,indxket,cibra,sgmabra,ciket,sgmaket,
     & trans,  scr1,   scr2,   scr3,
     & strtab, iwx,segsm,cist ,cntr )

c
c     two external x'-x and w'-w
c
         IMPLICIT NONE

C====>Begin Module XXWW                   File xxww.f                   
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             CIST(2),     CNTR,        CONFTBRA(*)
      INTEGER             CONFTKET(*), INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*),  IWX,         SEGSM(2)
      INTEGER             STRTAB(*)
C
      REAL*8              CIBRA(*),    CIKET(*),    SCR1(*),     SCR2(*)
      REAL*8              SCR3(*),     SGMABRA(*),  SGMAKET(*)
      REAL*8              TRANS(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         M1
      INTEGER             M1LP,        M1LPR,       MUL,         NMB1
      INTEGER             NMB1A,       NMB2,        NMB2A,       TASKID
      INTEGER             XS,          XSPR,        XSYM,        XSYMPR
C
      LOGICAL             BRNEKT,      SKIP,        WALK
C
      INCLUDE 'data.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'twoext.inc'
      INCLUDE 'twoex_stat.inc'
C====>End Module   XXWW                   File xxww.f                   
      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0
      if(spnodd)niot = niot + 1
      call upwkst(hilev, niot, rwlphd, mlppr,  mlp,clev  )
c
260   continue
      call upwlknew( ket, bra, walk, mul,segsm(2),segsm(1),skip )
      if ( walk )then
         if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(49,1660)
C HPM
         return
      else
         if (skip) goto 1210
      endif
c
         nmb1a=indxbra(bra)
         if(nmb1a.lt.0)go to 1210
         nmb2a=indxket(ket)
         if(nmb2a.lt.0)go to 1210
         cntr=cntr+1
        if (iwx.eq.1) then
         nmb1=(nmb1a-1)/isize_xx+1
         nmb2=(nmb2a-1)/isize_xx+1
         if (ksym.ne.lsym) then
          cnt2x_xx(nmb1,nmb2)=cnt2x_xx(nmb1,nmb2)+2
         else
          cnt2x_xx(nmb1,nmb2)=cnt2x_xx(nmb1,nmb2)+1
         endif
          else
         nmb1=(nmb1a-1)/isize_ww+1
         nmb2=(nmb2a-1)/isize_ww+1
         if (ksym.ne.lsym) then
          cnt2x_ww(nmb1,nmb2)=cnt2x_ww(nmb1,nmb2)+2
         else
          cnt2x_ww(nmb1,nmb2)=cnt2x_ww(nmb1,nmb2)+1
         endif
        endif
c
1210  continue
      if(mul.gt.1 .or. hilev.lt.niot)go to 260
      if(spnodd)niot = niot - 1

C HPM   return
c       call _f_hpm_stop_(49,1698)
C HPM
      return
      end

      subroutine zx(
     &         conftbra,indxbra,cibra,sgmabra,
     &         conftket,indxket,ciket,sgmaket,
     &         transf,segsm,cntr,isx)

c     zx and zw cases
c
c     loop over upper walks
c
         IMPLICIT NONE

C====>Begin Module ZX                     File zx.f                     
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            DDOT_WR
C
      REAL*8              DDOT_WR
C
C     Parameter variables
C
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             CNTR,        CONFTBRA(*), CONFTKET(*)
      INTEGER             INDXBRA(*),  INDXKET(*),  ISX
      INTEGER             SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    SGMABRA(*)
      REAL*8              SGMAKET(*),  TRANSF(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         M1LP
      INTEGER             M1LPR,       MUL,         NMB1,        NMB1A
      INTEGER             NMB2,        NMB2A,       TASKID
C
      LOGICAL             SKIP,        WALK
C
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'twoext.inc'
      INCLUDE 'twoex_stat.inc'
C====>End Module   ZX                     File zx.f                     
      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0
c
      if(spnodd)niot = niot + 1
      call upwkst(hilev, niot,rwlphd, mlppr,  mlp, clev  )
c
260   continue
      call upwlknew( ket, bra, walk, mul,segsm(2),segsm(1),skip)
      if ( walk )then
         if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(51,1955)
C HPM
         return
      else
         if (skip) goto 240
      endif
      nmb2a=indxbra(bra)
      if(nmb2a.lt.0)go to 240
      nmb1a=indxket(ket)
      if(nmb1a.lt.0)go to 240
      cntr=cntr+1
         if (isx.eq.1) then
         nmb1=(nmb1a-1)/isize_zz+1
         nmb2=(nmb2a-1)/isize_xx+1
         cnt2x_xz(nmb2,nmb1)=cnt2x_xz(nmb2,nmb1)+1
         else
         nmb1=(nmb1a-1)/isize_zz+1
         nmb2=(nmb2a-1)/isize_ww+1
         cnt2x_wz(nmb2,nmb1)=cnt2x_wz(nmb2,nmb1)+1
         endif

240   continue
      if(mul.gt.1 .or. hilev.lt.niot)go to 260
      if(spnodd)niot = niot - 1
c
C HPM   return
c       call _f_hpm_stop_(51,1981)
C HPM
      return
3030  format(' zx',20i6)
      end

      subroutine skipkl
     . ( icodev,loop8_cal,lpbuf,indxbra,indxket,
     .   segsm,itask)

         IMPLICIT NONE

C====>Begin Module SKIPKL                 File skipkl.f                 
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             IREAD
      PARAMETER           (IREAD = 1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
C
C     Argument variables
C
      INTEGER             ICODEV(*),   INDXBRA(*),  INDXKET(*),  ITASK
      INTEGER             LOOP8_CAL,   SEGSM(*)
C
      REAL*8              LPBUF(*)
C
C     Local variables
C
      INTEGER             CODE,        IDUM,        LPST,        NMBSEG
      INTEGER             NUMVAL(2:12),             PTHYZ,       RBUFL
      INTEGER             TASKID
C
C====>End Module   SKIPKL                 File skipkl.f                 
C

c     # numval(code) contains the number of loop values for code.
c     #      code=  2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
      data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /


c
c              # no integrals to process. skip over the formulas.
c              # do until (code = 0) ...
               write(6,*)'### nmij .eq. 0 ###'
280            continue
c                 # word8 -> lpbuf(loop8) fixed 06-may-91. eas/rls
                   code=icodev(loop8_cal)
                  loop8_cal = loop8_cal + 1
c
                  if ( code .eq. 0 ) then
c                    # exit.
                     goto 282
                  elseif ( code .eq. 1 ) then
c                    # get the new formula buffer.
                      call twoex_ft(.false.,indxbra,indxket,
     &                 segsm,itask)
                     loop8_cal = 1
                  endif
               goto 280
282            continue
C HPM   return
c       call _f_hpm_stop_(53,2203)
C HPM
               return
             end
      subroutine twoex_ft(initial,indxbra,indxket,segsm,
     &  tsk)

c
c  construct the 2-internal formula tape.
c
c  code  loop type     description
c  ----  ---------     --------------
c
c   0  -- new level --
c   1  -- new record --
c
c   2       3b          (xz)
c   3       3a          (wz)
c           10          (wz)
c   4       1ab         (yy)
c           8ab         (yy +diag)
c   5       1b          (yy)
c           8b          (yy +diag)
c   6       2b'         (yy)
c   7       1ab         (xx)
c           8ab         (xx +diag)
c   8       1b          (xx)
c           8b          (xx +diag)
c   9       2b'         (xx)
c  10       1b          (wx)
c           8b          (wx)
c  11       2b'         (wx)
c  12       1a          (ww)
c           8a          (ww +diag)
c

c
c    task 1  xz
c         2  wz
c         3  yy
c         4  xx
c         5  wx
c         6  ww
c
c
c


         IMPLICIT NONE

C====>Begin Module TWOEX_FT               File twoex_ft.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXTASK
      PARAMETER           (MAXTASK = 16384)
C
C     Argument variables
C
      INTEGER             INDXBRA(*),  INDXKET(*),  SEGSM(2),    TSK
C
      LOGICAL             INITIAL
C
C     Local variables
C
      CHARACTER*2         TYPE(6)
C
      INTEGER             IPATH(3),    IPTHYZ,      SEGSMM(3)
      INTEGER             SEGTYPE(3),  TASKID
C
      LOGICAL             BREAK2
C
      REAL*8              FTBUF(LBFT)
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             NLIST
C
      INCLUDE 'cftb.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cprt.inc'
      INCLUDE 'cst.inc'
      INCLUDE 'cvtran.inc'
      INCLUDE 'lcontrol.inc'
      INCLUDE 'special.inc'
      INCLUDE 'tasklst.inc'
      INCLUDE 'timerlst.inc'
C====>End Module   TWOEX_FT               File twoex_ft.f               
C
      equivalence (nlist,nunits(1))

       data type /'XZ','WZ','YY','XX','WX','WW'/

C HPM  subroutine_start
c       call _f_hpm_start_(54,2487, "pciudg6.f","twoex_ft")
C HPM
      call ptimer('tloop','resm',1)
c     call timer("twoex_ft",6,times(12),6)

      ibf = 0

      if (initial) jumpinposition=0
      go to  (990,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
     .        20,21,22,23,24,25,26,27)
     & (jumpinposition+1)
c
 10   call bummer('twoex_ft: invalid jumpinposition 10 ',0,2)
990   continue

c
      nintl = 2
      mo(0) = 0
      nloop = 0
      qdiag = .true.

      goto ( 199,209,219,229,239,249) (tsk)

      call bummer ('twoex_ft: invalid task',tsk,2)

c
c #######################      XZ task     ############################
c
199   lmo = 1
200   continue ! lmo = 1, niot
c
         kmo = 1
         if (kmo .gt. (lmo-1)) go to 101
100      continue! kmo = 1, (lmo-1)
            if(qprt.ge.5)write(nlist,6100)lmo,kmo
c***********************************************************************
c           i<l loop types:
c***********************************************************************
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
c           # xz loop 3b:
c
            numv = 1
            vtpt = 1
            code(1) = 2
1           call loopcalc( 3, 1, st3, db3(1,0,2), 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=1
            goto 999
            endif

c
c    #      put code 0
16          call wcode_dir(ibf,0,icodev,16,break2)
            if (break2) goto 999
c
         kmo = kmo + 1
         if (kmo .le. (lmo-1)) go to 100
101      continue
c***********************************************************************
c        i=l loop types:
c***********************************************************************
         if(qprt.ge.5)write(nlist,6100)lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c    #   put code 0
17       call wcode_dir(ibf,0,icodev,17,break2)
         if (break2) goto 999

c
      lmo = lmo + 1
      if (lmo .le. niot) go to 200
c
      ibf=ibf+1
      icodev(ibf) = 1
      jumpinposition = 0
c
c     if (nlist.gt.0) write(nlist,6010) nloop, 'XZ'
      loopcnt(currtsk)=nloop
6010  format(' two-external ft construction complete.',
     + i12,a,' loops constructed.')
      goto 999
6100  format(' int2:  lmo,kmo,ibf = ',2i4,2x,i8)


c
c    ########################  WZ TASK #################################
c
c
209   lmo = 1
210   continue ! lmo = 1, niot
c
         kmo = 1
         if (kmo .gt. (lmo-1)) go to 111
110      continue! kmo = 1, (lmo-1)
            if(qprt.ge.5)write(nlist,6100)lmo,kmo
c***********************************************************************
c           i<l loop types:
c***********************************************************************
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo

c
c           # wz loop 3a:
c
            numv = 1
            vtpt = 1
            code(1) = 3
2           call loopcalc( 4, 1, st3, db3, 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=2
            goto 999
            endif
c
c    #      put code 0
18          call wcode_dir(ibf,0,icodev,18,break2)
            if (break2) goto 999
c
         kmo = kmo + 1
         if (kmo .le. (lmo-1)) go to 110
111      continue
c***********************************************************************
c        i=l loop types:
c***********************************************************************
         if(qprt.ge.5)write(nlist,6100)lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c        # wz loop 10:
c
         numv = 1
         vtpt = 1
         code(1) = 3
11       call loopcalc( 4, 1, st10, db10, 6, 4,
     &       indxbra, indxket, segsm)
         if (break) then
         jumpinposition=11
         goto 999
         endif
c
c    #   put code 0
19       call wcode_dir(ibf,0,icodev,19,break2)
         if (break2) goto 999

c
      lmo = lmo + 1
      if (lmo .le. niot) go to 210
c
      ibf=ibf+1
      icodev(ibf) = 1
      jumpinposition = 0
c
c     if (nlist.gt.0) write(nlist,6010) nloop, 'WZ'
      loopcnt(currtsk)=nloop
      goto 999

c
c    ########################  YY TASK #################################
c
c
219   lmo = 1
220   continue ! lmo = 1, niot
c
         kmo = 1
         if (kmo .gt. (lmo-1)) go to 121
120      continue! kmo = 1, (lmo-1)
            if(qprt.ge.5)write(nlist,6100)lmo,kmo
c***********************************************************************
c           i<l loop types:
c***********************************************************************
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
c           # yy loop 1ab:
c
            numv = 2
            vtpt = 5
            code(1) = 4
            code(2) = 5
3           call loopcalc( 2, 2, st1, db1, 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=3
            goto 999
            endif
c
c           # yy loop 2b':
c
            numv = 1
            vtpt = 3
            code(1) = 6
4           call loopcalc( 2, 2, st2, db2(1,0,2), 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=4
            goto 999
            endif
c
c    #      put code 0
20          call wcode_dir(ibf,0,icodev,20,break2)
            if (break2) goto 999
c
         kmo = kmo + 1
         if (kmo .le. (lmo-1)) go to 120
121      continue
c***********************************************************************
c        i=l loop types:
c***********************************************************************
         if(qprt.ge.5)write(nlist,6100)lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c        # yy loop 8ab:
c
         numv = 2
         vtpt = 5
         code(1) = 4
         code(2) = 5
12       call loopcalc( 2, 2, st8, db8, 6, 4,
     &       indxbra, indxket,segsm)
         if (break) then
         jumpinposition=12
         goto 999
         endif
c
c    #   put code 0
21       call wcode_dir(ibf,0,icodev,21,break2)
         if (break2) goto 999

c
      lmo = lmo + 1
      if (lmo .le. niot) go to 220
c
      ibf=ibf+1
      icodev(ibf) = 1
      jumpinposition = 0
c
c     if (nlist.gt.0) write(nlist,6010) nloop, 'YY'
      loopcnt(currtsk)=nloop
      goto 999

c
c    ########################  XX TASK #################################
c
c

229   lmo = 1
230   continue ! lmo = 1, niot
c
         kmo = 1
         if (kmo .gt. (lmo-1)) go to 131
130      continue! kmo = 1, (lmo-1)
            if(qprt.ge.5)write(nlist,6100)lmo,kmo
c***********************************************************************
c           i<l loop types:
c***********************************************************************
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
c           # xx loop 1ab:
c
            numv = 2
            vtpt = 2
            code(1) = 7
            code(2) = 8
5           call loopcalc( 3, 3, st1, db1, 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=5
            goto 999
            endif
c
c           # xx loop 2b':
c
            numv = 1
            vtpt = 1
            code(1) = 9
6           call loopcalc( 3, 3, st2, db2(1,0,2), 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=6
            goto 999
            endif
c
c    #      put code 0
22          call wcode_dir(ibf,0,icodev,22,break2)
            if (break2) goto 999
c
         kmo = kmo + 1
         if (kmo .le. (lmo-1)) go to 130
131      continue
c***********************************************************************
c        i=l loop types:
c***********************************************************************
         if(qprt.ge.5)write(nlist,6100)lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c        # xx loop 8ab:
c
         numv = 2
         vtpt = 2
         code(1) = 7
         code(2) = 8
13       call loopcalc( 3, 3, st8, db8, 6, 4,
     &       indxbra, indxket,segsm)
         if (break) then
         jumpinposition=13
         goto 999
         endif
c
c    #   put code 0
23      call wcode_dir(ibf,0,icodev,23,break2)
         if (break2) goto 999

c
      lmo = lmo + 1
      if (lmo .le. niot) go to 230
c
      ibf=ibf+1
      icodev(ibf) = 1
      jumpinposition = 0
c
      loopcnt(currtsk)=nloop
c      if (nlist.gt.0) write(nlist,6010) nloop, 'XX'
      goto 999

c
c    ########################  WX TASK #################################
c
c
239   lmo = 1
240   continue ! lmo = 1, niot
c
         kmo = 1
         if (kmo .gt. (lmo-1)) go to 141
140      continue! kmo = 1, (lmo-1)
            if(qprt.ge.5)write(nlist,6100)lmo,kmo
c***********************************************************************
c           i<l loop types:
c***********************************************************************
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
c           # wx loop 1b:
c
            numv = 1
            vtpt = 4
            code(1) = 10
7           call loopcalc( 4, 3, st1, db1(1,0,2), 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=7
            goto 999
            endif
c
c           # wx loop 2b':
c
            numv = 1
            vtpt = 4
            code(1) = 11
8           call loopcalc( 4, 3, st2, db2(1,0,2), 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=8
            goto 999
            endif
c
c    #      put code 0
24          call wcode_dir(ibf,0,icodev,24,break2)
            if (break2) goto 999
c
         kmo = kmo + 1
         if (kmo .le. (lmo-1)) go to 140
141      continue
c***********************************************************************
c        i=l loop types:
c***********************************************************************
         if(qprt.ge.5)write(nlist,6100)lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c        # wx loop 8b:
c
         numv = 1
         vtpt = 4
         code(1) = 10
14       call loopcalc( 4, 3, st8, db8(1,0,2), 6, 4,
     &       indxbra, indxket,segsm)
         if (break) then
         jumpinposition=14
         goto 999
         endif
c
c    #   put code 0
25       call wcode_dir(ibf,0,icodev,25,break2)
         if (break2) goto 999

c
      lmo = lmo + 1
      if (lmo .le. niot) go to 240
c
      ibf=ibf+1
      icodev(ibf) = 1
      jumpinposition = 0
c
      loopcnt(currtsk)=nloop
c     if (nlist.gt.0) write(nlist,6010) nloop, 'WX'
      goto 999

c
c    ########################  WW TASK #################################
c
c
249   lmo = 1
250   continue ! lmo = 1, niot
c
         kmo = 1
         if (kmo .gt. (lmo-1)) go to 151
150      continue! kmo = 1, (lmo-1)
            if(qprt.ge.5)write(nlist,6100)lmo,kmo
c***********************************************************************
c           i<l loop types:
c***********************************************************************
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
c           # ww loop 1a:
c
            numv = 1
            vtpt = 1
            code(1) = 12
9           call loopcalc( 4, 4, st1, db1, 8, 4,
     &       indxbra, indxket,segsm)
            if (break) then
            jumpinposition=9
            goto 999
            endif
c
c    #      put code 0
26          call wcode_dir(ibf,0,icodev,26,break2)
            if (break2) goto 999
c
         kmo = kmo + 1
         if (kmo .le. (lmo-1)) go to 150
151      continue
c***********************************************************************
c        i=l loop types:
c***********************************************************************
         if(qprt.ge.5)write(nlist,6100)lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c        # ww loop 8a:
c
         numv = 1
         vtpt = 1
         code(1) = 12
15       call loopcalc( 4, 4, st8, db8, 6, 4,
     &       indxbra, indxket,segsm)
         if (break) then
         jumpinposition=15
         goto 999
         endif
c
c    #   put code 0
27       call wcode_dir(ibf,0,icodev,27,break2)
         if (break2) goto 999

c
      lmo = lmo + 1
      if (lmo .le. niot) go to 250
c
      ibf=ibf+1
      icodev(ibf) = 1
      jumpinposition = 0
c
c     if (nlist.gt.0) write(nlist,6010) nloop, 'WW'
      loopcnt(currtsk)=nloop


c  ##############


c999   call timer("twoex_ft",5,times(12),6)
 999   call ptimer('tloop','susp',1)

c
C HPM   return
c       call _f_hpm_stop_(54,2986)
C HPM
      return
      end
      subroutine twoex_xz(
     & conftbra,indxbra, cibra,sgmabra,
     & intbuf, transf, lpbuf,
     & scrj,   scrk,   scrkt,
     & conftket,indxket,ciket,sgmaket,
     & segsm)


C====>Begin Module TWOEX_XZ               File twoex_xz.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER             RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             IREAD
      PARAMETER           (IREAD = 1)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), INDXBRA(*)
      INTEGER             INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              LPBUF(*),    SCRJ(*),     SCRK(*)
      REAL*8              SCRKT(*),    SGMABRA(*),  SGMAKET(*)
      REAL*8              TRANSF(*)
C
C     Local variables
C
      INTEGER             BLSTRA(9),   CODE,        FIN,         IBLOCK
      INTEGER             ICONT1,      ICONT2,      IND1,        IND2
      INTEGER             INTBLK,      INTST,       ISIGN,       ISYM
      INTEGER             IWX,         JSYM,        K1,          KLS
      INTEGER             KLSYM,       KS,          KSG,         L1
      INTEGER             LINCMB,      LOOP8,       LOOP8_CAL,   LPST
      INTEGER             LS,          MAXBL,       MLP1,        MLPPR1
      INTEGER             MLPPRT,      NI,          NI2,         NIJ
      INTEGER             NMAX,        NMAX1,       NMBINT,      NMBM
      INTEGER             NMBUF,       NMIJ,        NMINT1
      INTEGER             NUMVAL(2:12),             RBUFI,       RBUFL
      INTEGER             START,       TOTINT,      WTLPHD
C
      LOGICAL             LDIAG,       LOGTR,       LSTSEG,      LSYMKL
C
      REAL*8              FACT,        SQRT2,       SQRT22,      VALUE
      REAL*8              VALUE1,      VALUE2,      WORD8
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'counter.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'special.inc'
      INCLUDE 'twoext.inc'
C====>End Module   TWOEX_XZ               File twoex_xz.f               
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))

C
      data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /
c    ##  initialise the elements from common/lcontrol/
c
C HPM  subroutine_start
c       call _f_hpm_start_(55,3405, "pciudg6.f","twoex_xz")
C HPM
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c     write(6,*) '================== twoex_xz ======================='
c     write(6,*) 'ipth1,segsm,cist=',ipth1,segsm,cist
c     write(6,*) 'indxyz,indx1'
c     call iwritex(indxyz,10)
c     call iwritex(index1,10)

c
      loop8_cal = 0
      sqrt2  = sqrt(two)
      sqrt22 = sqrt2 / two
      niot   = intorb
      iblock = 0
      ind2   = 0
      intblk = 0
      totint = 0
      maxbl  = maxbl2 + bufszi
      mlppr1 = 0
      mlp1   = 0
      isign  = 0
      iwx    = 0
      fact   = zero
      value  = zero
      value1 = zero
      value2 = zero
      word8  = zero
      ldiag  = .false.
      logtr  = .false.
      lsymkl = .false.
      rbufl  = rl( bufszl )
      rbufi  = ( bufszi )
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c
      intst = bl4xt + 1
c
c     # read the first record of the formula tape.
c
c

       call twoex_ft(.true.,indxbra,indxket,segsm,1)
c
      do 100 l1 = 1, intorb
         ls    = orbsym(l1)
         lsym  = sym21(ls)
         hilev = l1
         do 110 k1=1,l1
            ks    = orbsym(k1)
            ksym  = sym21(ks)
            kls   = mult(ks,ls)
            klsym = sym21(kls)
c           write(6,3333)l1,k1,lsym,ls,ksym,ks,klsym,kls
c
            lsymkl = ksym .ne. lsym
c
c           # determination of the block structure of the two
c           # electron integrals for fixed k1 and l1.
c
            nmij   = nmbpr(klsym)
c
            if ( nmij .eq. 0 ) then
               call skipkl
     . ( icodev,loop8_cal+1,lpbuf,indxbra,indxket,segsm,1)

               goto 110
            endif

180         continue
            loop8_cal=loop8_cal+1
c
c           # process the formula tape
c
c           # word8 contains code,wtlphd,mlppr,mlp
c           #
c           # mlppr    weight of the bra part of the loop
c           # mlp      weight of the ket part of the loop
c
c
c           # code   0  new set of (k,l)
c           #        1  end of record
c           #        2  xz
c           #        3  wz
c           #        4  yy (1a,b)
c           #        5  yy (1b)
c           #        6  yy(1b')
c           #        7  xx (1a,b)
c           #        8  xx (1b)
c           #        9  xx (1b')
c           #        10 wx (1b)
c           #        11 wx (1b')
c           #        12 ww (1a,b)
c
             code=icodev(loop8_cal)
             rwlphd=headv(loop8_cal)
             mlppr= ybtv(loop8_cal)
             mlp= yktv(loop8_cal)
c      write(6,*) 'rwlphd,wtlphd,mlppr,mlp=',rwlphd,wtlphd,mlppr,mlp
c      write(6,*) 'value=',valv(1,loop8_cal)

c
            if ( code .eq. 0 ) then
c  ### code 0
c              # new level.
               goto 110
            elseif ( code .eq. 1 ) then
c  ### code 1
c              # read the next buffer of formulas.
c
             call twoex_ft(.false.,indxbra,indxket,segsm,1)
               loop8_cal = 0
            elseif ( code .eq. 2 ) then
c  ### code 2
c
c              # zx loop.
c
                value = valv(1,loop8_cal)
               lincmb = 0
c
c                 # check whether segment of ci vector is available
c
                  isign  = -1
                  icont1 = 1
                  icont2 = 2
c
c                 # form linear combinations.
c
                  if ( lincmb .eq. 0 ) then
                     lincmb = 1
                  endif
c
c                 # update the sigma vector.
c
                     call zx(
     &                conftbra,indxbra,cibra,sgmabra,
     &                conftket,indxket,ciket,sgmaket,
     &                transf,segsm,xz2x,1)
210            continue
            else
              call bummer('twoex: unknown code=', code, faterr )
            endif
c
c           # loop back for the next formula contribution.
            goto 180
c
c           # end of do loop k1
110      continue
c        # end of do loop l1
100   continue
c
      return
3333  format(' 110',20i6)
20000 format(' insufficient space for two electron integrals in twoex'
     & /' space wanted ',i6,'  space available ',i6)
20070 format(' twoext. nmb. of int. calculated',i6,' from sort',i6)
80800 format(' new loop value; iseg1,iseg2,isgflg',10i5)
      end
      subroutine twoex_wz(
     &  conftbra,indxbra,cibra,sgmabra,
     & intbuf, transf, lpbuf,
     & scrj,   scrk,   scrkt,
     & conftket,indxket,ciket,sgmaket,
     & segsm)
c*****
c  two external case
c
c  05-jun-91 twoseg() call added. -rls
c  06-may-91 infinite loop bug at 270 fixed. -eas.
c  15-apr-90 by e. stahlberg to allow 1 or 0 x,w segments
c  new version february 1988, hans lischka, university of vienna
c  23-dec-98 direct FT calculation implemented (Michal Dallos)
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     intbuf   buffer for two electron integrals
c     transf   lin. comb. of integrals
c     lpbuf    buffer for formula tape
c     scfj,scrk,scrkt j and k matrices
c     scr1,scr2,scr3 scratch matrices
c     index1   index vector for segment 1
c     ci1      ci vector for segment 1
c     sigm1    sigma vector for segment 1
c     ipth1(2) start number for internal paths
c     segsm(2) number of internal walks
c     cist(2)   starting number for the ci vector
c
         IMPLICIT NONE

C====>Begin Module TWOEX_WZ               File twoex_wz.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER             RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             IREAD
      PARAMETER           (IREAD = 1)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), INDXBRA(*)
      INTEGER             INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              LPBUF(*),    SCRJ(*),     SCRK(*)
      REAL*8              SCRKT(*),    SGMABRA(*),  SGMAKET(*)
      REAL*8              TRANSF(*)
C
C     Local variables
C
      INTEGER             BLSTRA(9),   CODE,        DIFF,        FIN
      INTEGER             I,           IBLOCK,      ICONT1,      ICONT2
      INTEGER             IDUM,        IJ,          IND1,        IND2
      INTEGER             INTBLK,      INTST,       ISGEXC,      ISIGN
      INTEGER             ISYM,        IWX,         JSYM,        K1
      INTEGER             KLS,         KLSYM,       KS,          KSG
      INTEGER             L1,          LINCMB,      LOOP8
      INTEGER             LOOP8_CAL,   LPST,        LS,          MAXBL
      INTEGER             MLP1,        MLPPR1,      MLPPRT,      NI
      INTEGER             NI2,         NIJ,         NMAX,        NMAX1
      INTEGER             NMBINT,      NMBM,        NMBSEG,      NMBUF
      INTEGER             NMIJ,        NMINT1,      NMSGXZ
      INTEGER             NUMVAL(2:12),             RBUFI,       RBUFL
      INTEGER             START,       TASKID,      TOTINT,      WTLPHD
C
      LOGICAL             CLCXZ,       LDIAG,       LOGTR,       LSTSEG
      LOGICAL             LSYMKL
C
      REAL*8              FACT,        SQRT2,       SQRT22,      VALUE
      REAL*8              VALUE1,      VALUE2,      WORD8
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'counter.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'special.inc'
      INCLUDE 'twoext.inc'
C====>End Module   TWOEX_WZ               File twoex_wz.f               
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))

C
      data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /
c    ##  initialise the elements from common/lcontrol/
c
c     write(6,*) '================== twoex_wz ======================='
c     write(6,*) 'ipth1,segsm,cist=',ipth1,segsm,cist
c     write(6,*) 'indxyz,indx1'
c     call iwritex(indxyz,10)
c     call iwritex(index1,10)

C HPM  subroutine_start
c       call _f_hpm_start_(56,4015, "pciudg6.f","twoex_wz")
C HPM
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)

c
      loop8_cal = 0
      sqrt2  = sqrt(two)
      sqrt22 = sqrt2 / two
      niot   = intorb
      iblock = 0
      ind2   = 0
      intblk = 0
      totint = 0
      maxbl  = maxbl2 + bufszi
      mlppr1 = 0
      mlp1   = 0
      isign  = 0
      iwx    = 0
      fact   = zero
      value  = zero
      value1 = zero
      value2 = zero
      word8  = zero
      ldiag  = .false.
      logtr  = .false.
      lsymkl = .false.
      rbufl  = rl( bufszl )
      rbufi  = ( bufszi )
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c
      intst = bl4xt + 1
c
c     # read the first record of the formula tape.
c
c
       call twoex_ft(.true.,indxbra,indxket,segsm,2)
c
      do 100 l1 = 1, intorb
         ls    = orbsym(l1)
         lsym  = sym21(ls)
         hilev = l1
         do 110 k1=1,l1
            ks    = orbsym(k1)
            ksym  = sym21(ks)
            kls   = mult(ks,ls)
            klsym = sym21(kls)
c           write(6,3333)l1,k1,lsym,ls,ksym,ks,klsym,kls
c
            lsymkl = ksym .ne. lsym
c
c           # determination of the block structure of the two
c           # electron integrals for fixed k1 and l1.
c
            nmij   = nmbpr(klsym)
c
            if ( nmij .eq. 0 ) then
               call skipkl
     . ( icodev,loop8_cal+1,lpbuf,indxbra,indxket,segsm,2)

               goto 110
            endif
c
180         continue
            loop8_cal=loop8_cal+1
c
c           # process the formula tape
c
c           # word8 contains code,wtlphd,mlppr,mlp
c           #
c           # mlppr    weight of the bra part of the loop
c           # mlp      weight of the ket part of the loop
c
c
c           # code   0  new set of (k,l)
c           #        1  end of record
c           #        2  xz
c           #        3  wz
c           #        4  yy (1a,b)
c           #        5  yy (1b)
c           #        6  yy(1b')
c           #        7  xx (1a,b)
c           #        8  xx (1b)
c           #        9  xx (1b')
c           #        10 wx (1b)
c           #        11 wx (1b')
c           #        12 ww (1a,b)
c
             code=icodev(loop8_cal)
             rwlphd=headv(loop8_cal)
             mlppr= ybtv(loop8_cal)
             mlp= yktv(loop8_cal)


c
c           write(6,80800) iseg1,iseg2,isgflg,code
c
            if ( code .eq. 0 ) then
c  ### code 0
c              # new level.
               goto 110
            elseif ( code .eq. 1 ) then
c  ### code 1
c              # read the next buffer of formulas.
c
             call twoex_ft(.false.,indxbra,indxket,segsm,2)
               loop8_cal = 0
210            continue
            elseif ( code .eq. 3 ) then
c  ### code 3
c              # zw
c
               isign = 1
                value = valv(1,loop8_cal)
               lincmb = 0
c
c                 # check whether segment of ci vector is available.
c
                  icont1 = 1
                  icont2 = 2
c
c                 # form linear combinations.
c
                  if ( lincmb .eq. 0 ) then
                     lincmb = 1
                  endif
c
c                 # update the sigma vector.
c
                     call zx(
     &                conftbra,indxbra,cibra,sgmabra,
     &                conftket,indxket,ciket,sgmaket,
     &                transf,segsm,wz2x,0)
310            continue
            else
              call bummer('twoex: unknown code=', code, faterr )
            endif
c
c           # loop back for the next formula contribution.
            goto 180
c
c           # end of do loop k1
110      continue
c        # end of do loop l1
100   continue
      return
3333  format(' 110',20i6)
20000 format(' insufficient space for two electron integrals in twoex'
     & /' space wanted ',i6,'  space available ',i6)
20070 format(' twoext. nmb. of int. calculated',i6,' from sort',i6)
80800 format(' new loop value; iseg1,iseg2,isgflg',10i5)
      end
      subroutine twoex_wx(
     & indsymbra,conftbra,indsymket,conftket,
     & intbuf, transf, lpbuf,
     & scrj,   scrk,   scrkt,  scr1, scr2,   scr3,
     & indxbra, cibra,   sgmabra,  indxket, ciket,  sgmaket,
     & segsm)

c*****
c  two external case
c
c  05-jun-91 twoseg() call added. -rls
c  06-may-91 infinite loop bug at 270 fixed. -eas.
c  15-apr-90 by e. stahlberg to allow 1 or 0 x,w segments
c  new version february 1988, hans lischka, university of vienna
c  23-dec-98 direct FT calculation implemented (Michal Dallos)
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     intbuf   buffer for two electron integrals
c     transf   lin. comb. of integrals
c     lpbuf    buffer for formula tape
c     scfj,scrk,scrkt j and k matrices
c     scr1,scr2,scr3 scratch matrices
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     sigm1    sigma vector for segment 1
c     sigm2    sigma vector for segment 2
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     nmbseg    number of segments
c     isegxc=0  take combination between all segments
c           =1  take combination between all segments excluding
c               yy loops (diference between segment numbers is one)
c           =2  take combination between different segments only
c               excluding yy loops (difference between segment
c               numbers is larger than one)
c     lstseg=.true.  last segment (ksg2=nseg) has to be taken into
c                    account
c     clcxz=.true.   compute xz and wz cases
c     nmsgxz         number of segments for xz and wz cases
c
         IMPLICIT NONE

C====>Begin Module TWOEX_WX               File twoex_wx.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER             RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             IREAD
      PARAMETER           (IREAD = 1)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128 )
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 200)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              LPBUF(*),    SCR1(*),     SCR2(*),     SCR3(*)
      REAL*8              SCRJ(*),     SCRK(*),     SCRKT(*)
      REAL*8              SGMABRA(*),  SGMAKET(*),  TRANSF(*)
C
C     Local variables
C
      INTEGER             BLSTRA(9),   CIST(2),     CODE,        DIFF
      INTEGER             FIN,         I,           IBLOCK,      ICONT1
      INTEGER             ICONT2,      IDUM,        IJ,          IND1
      INTEGER             IND2,        INTBLK,      INTST
      INTEGER             IPTH1(2),    ISGEXC,      ISIGN,       ISYM
      INTEGER             IWX,         JSYM,        K1,          KLS
      INTEGER             KLSYM,       KS,          KSG,         L1
      INTEGER             LINCMB,      LOOP8,       LOOP8_CAL,   LPST
      INTEGER             LS,          MAXBL,       MLP1,        MLPPR1
      INTEGER             MLPPRT,      NI,          NI2,         NIJ
      INTEGER             NMAX,        NMAX1,       NMBINT,      NMBM
      INTEGER             NMBSEG,      NMBUF,       NMIJ,        NMINT1
      INTEGER             NMSGXZ,      NUMVAL(2:12),             RBUFI
      INTEGER             RBUFL,       START,       TASKID,      TOTINT
      INTEGER             WTLPHD
C
      LOGICAL             CLCXZ,       LDIAG,       LOGTR,       LSTSEG
      LOGICAL             LSYMKL
C
      REAL*8              FACT,        SQRT2,       SQRT22,      VALUE
      REAL*8              VALUE1,      VALUE2,      WORD8
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'debug.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'special.inc'
      INCLUDE 'twoext.inc'
C====>End Module   TWOEX_WX               File twoex_wx.f               
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))

      data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /


c
c    ##  initialise the elements from common/lcontrol/
c
C HPM  subroutine_start
c       call _f_hpm_start_(57,4604, "pciudg6.f","twoex_wx")
C HPM
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c     write(6,*) '================== twoex_wx ======================='
c     write(6,*) 'ipath=',ipthst(ibra),ipthst(iket),ibra,iket

c     write(6,*) 'ipth1,segsm,cist=',ipth1,segsm,cist
c     write(6,*) 'index1,index2'
c     call iwritex(index1,10)
c     call iwritex(index2,10)

c
      loop8_cal = 0
      sqrt2  = sqrt(two)
      sqrt22 = sqrt2 / two
      niot   = intorb
      iblock = 0
      ind2   = 0
      intblk = 0
      totint = 0
      maxbl  = maxbl2 + bufszi
      mlppr1 = 0
      mlp1   = 0
      isign  = 0
      iwx    = 0
      fact   = zero
      value  = zero
      value1 = zero
      value2 = zero
      word8  = zero
      ldiag  = .false.
      logtr  = .false.
      lsymkl = .false.
      rbufl  = rl( bufszl )
      rbufi  = ( bufszi )
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c
      intst = bl4xt + 1
c
c     # read the first record of the formula tape.
c
c
       call twoex_ft(.true.,indxbra,indxket,segsm,5)
c
      do 100 l1 = 1, intorb
         ls    = orbsym(l1)
         lsym  = sym21(ls)
         hilev = l1
         do 110 k1=1,l1
            ks    = orbsym(k1)
            ksym  = sym21(ks)
            kls   = mult(ks,ls)
            klsym = sym21(kls)
c           write(6,3333)l1,k1,lsym,ls,ksym,ks,klsym,kls
c
            lsymkl = ksym .ne. lsym
c
c           # determination of the block structure of the two
c           # electron integrals for fixed k1 and l1.
c
            nmij   = nmbpr(klsym)
c
            if ( nmij .eq. 0 ) then
               call skipkl
     . ( icodev,loop8_cal+1,lpbuf,indxbra,indxket,segsm,5)

               goto 110
            endif
c
180         continue
            loop8_cal=loop8_cal+1
c
c           # process the formula tape
c
c           # word8 contains code,wtlphd,mlppr,mlp
c           #
c           # mlppr    weight of the bra part of the loop
c           # mlp      weight of the ket part of the loop
c
c
c           # code   0  new set of (k,l)
c           #        1  end of record
c           #        2  xz
c           #        3  wz
c           #        4  yy (1a,b)
c           #        5  yy (1b)
c           #        6  yy(1b')
c           #        7  xx (1a,b)
c           #        8  xx (1b)
c           #        9  xx (1b')
c           #        10 wx (1b)
c           #        11 wx (1b')
c           #        12 ww (1a,b)
c
             code=icodev(loop8_cal)
             rwlphd=headv(loop8_cal)
             mlppr= ybtv(loop8_cal)
             mlp= yktv(loop8_cal)
c
c           write(6,80800) iseg1,iseg2,isgflg,code
c           write(6,80801) loop8_cal,code,l1,k1
80801  format('##=',i8,'...',3i4)
c
            if ( code .eq. 0 ) then
c  ### code 0
c              # new level.
               goto 110
            elseif ( code .eq. 1 ) then
c  ### code 1
c              # read the next buffer of formulas.
c
             call twoex_ft(.false.,indxbra,indxket,segsm,5)
               loop8_cal = 0
            elseif ( (code .eq. 10) .or. (code .eq. 11) ) then
c  ### code 10 & 11
c
c              # xw paths

               value2 = valv(1,loop8_cal)
               value1 = zero

               ldiag = .true.
                  call wx(
     &             indsymbra,conftbra,indxbra,cibra,sgmabra,
     &             indsymket,conftket,indxket,ciket,sgmaket,
     &             transf, scr1,   scr2,   scr3,segsm  )


          else
            call bummer('twoex: unknown code=', code, faterr )
          endif
c
c           # loop back for the next formula contribution.
            goto 180
c
c           # end of do loop k1
110      continue
c        # end of do loop l1
100   continue
c
      return
3333  format(' 110',20i6)
20000 format(' insufficient space for two electron integrals in twoex'
     & /' space wanted ',i6,'  space available ',i6)
20070 format(' twoext. nmb. of int. calculated',i6,' from sort',i6)
80800 format(' new loop value; iseg1,iseg2,isgflg',10i5)
      end
      subroutine twoex_xx(
     & indsymbra, conftbra,indsymket,conftket,
     & intbuf, transf, lpbuf,
     & scrj,   scrk,   scrkt,  scr1,
     & scr2,   scr3,   indxbra, cibra,
     & sgmabra,  indxket, ciket,  sgmaket,
     & segsm,  cist)

c*****
c  two external case
c
c  05-jun-91 twoseg() call added. -rls
c  06-may-91 infinite loop bug at 270 fixed. -eas.
c  15-apr-90 by e. stahlberg to allow 1 or 0 x,w segments
c  new version february 1988, hans lischka, university of vienna
c  23-dec-98 direct FT calculation implemented (Michal Dallos)
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     intbuf   buffer for two electron integrals
c     transf   lin. comb. of integrals
c     lpbuf    buffer for formula tape
c     scfj,scrk,scrkt j and k matrices
c     scr1,scr2,scr3 scratch matrices
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     sigm1    sigma vector for segment 1
c     sigm2    sigma vector for segment 2
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     nmbseg    number of segments
c     isegxc=0  take combination between all segments
c           =1  take combination between all segments excluding
c               yy loops (diference between segment numbers is one)
c           =2  take combination between different segments only
c               excluding yy loops (difference between segment
c               numbers is larger than one)
c     lstseg=.true.  last segment (ksg2=nseg) has to be taken into
c                    account
c     clcxz=.true.   compute xz and wz cases
c     nmsgxz         number of segments for xz and wz cases
c
         IMPLICIT NONE

C====>Begin Module TWOEX_XX               File twoex_xx.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER             RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             IREAD
      PARAMETER           (IREAD = 1)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
C
C     Argument variables
C
      INTEGER             CIST(2),     CONFTBRA(*), CONFTKET(*)
      INTEGER             INDSYMBRA(*),             INDSYMKET(*)
      INTEGER             INDXBRA(*),  INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              LPBUF(*),    SCR1(*),     SCR2(*),     SCR3(*)
      REAL*8              SCRJ(*),     SCRK(*),     SCRKT(*)
      REAL*8              SGMABRA(*),  SGMAKET(*),  TRANSF(*)
C
C     Local variables
C
      INTEGER             BLSTRA(9),   CODE,        DIFF,        FIN
      INTEGER             I,           IBLOCK,      ICONT1,      ICONT2
      INTEGER             IDUM,        IJ,          IND1,        IND2
      INTEGER             INTBLK,      INTST,       IPTH1(2),    ISGEXC
      INTEGER             ISIGN,       ISYM,        IWX,         JSYM
      INTEGER             K1,          KLS,         KLSYM,       KS
      INTEGER             KSG,         L1,          LINCMB,      LOOP8
      INTEGER             LOOP8_CAL,   LPST,        LS,          MAXBL
      INTEGER             MLP1,        MLPPR1,      MLPPRT,      NI
      INTEGER             NI2,         NIJ,         NMAX,        NMAX1
      INTEGER             NMBINT,      NMBM,        NMBSEG,      NMBUF
      INTEGER             NMIJ,        NMINT1,      NMSGXZ
      INTEGER             NUMVAL(2:12),             RBUFI,       RBUFL
      INTEGER             START,       TASKID,      TOTINT,      WTLPHD
C
      LOGICAL             CLCXZ,       LDIAG,       LOGTR,       LSTSEG
      LOGICAL             LSYMKL
C
      REAL*8              FACT,        SQRT2,       SQRT22,      VALUE
      REAL*8              VALUE1,      VALUE2,      WORD8
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'counter.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'special.inc'
      INCLUDE 'twoext.inc'
C====>End Module   TWOEX_XX               File twoex_xx.f               
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))

C
      data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /
C
c    ##  initialise the elements from common/lcontrol/
c
C HPM  subroutine_start
c       call _f_hpm_start_(58,5228, "pciudg6.f","twoex_xx")
C HPM
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c     write(6,*) '================== twoex_xx ======================='
c     write(6,*) 'ipth1,segsm,cist=',ipth1,segsm,cist
c     write(6,*) 'index1,index2'
c     call iwritex(index1,10)
c     call iwritex(index2,10)

c
      loop8_cal = 0
      sqrt2  = sqrt(two)
      sqrt22 = sqrt2 / two
      niot   = intorb
      iblock = 0
      ind2   = 0
      intblk = 0
      totint = 0
      maxbl  = maxbl2 + bufszi
      mlppr1 = 0
      mlp1   = 0
      isign  = 0
      iwx    = 0
      fact   = zero
      value  = zero
      value1 = zero
      value2 = zero
      word8  = zero
      ldiag  = .false.
      logtr  = .false.
      lsymkl = .false.
      rbufl  = rl( bufszl )
      rbufi  = ( bufszi )
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c
      intst = bl4xt + 1
c
c     # read the first record of the formula tape.
c
c
       call twoex_ft(.true.,indxbra,indxket,segsm,4)
c
      do 100 l1 = 1, intorb
         ls    = orbsym(l1)
         lsym  = sym21(ls)
         hilev = l1
         do 110 k1=1,l1
            ks    = orbsym(k1)
            ksym  = sym21(ks)
            kls   = mult(ks,ls)
            klsym = sym21(kls)
c           write(6,3333)l1,k1,lsym,ls,ksym,ks,klsym,kls
c
            lsymkl = ksym .ne. lsym
c
c           # determination of the block structure of the two
c           # electron integrals for fixed k1 and l1.
c
            nmij   = nmbpr(klsym)
c
            if ( nmij .eq. 0 ) then
               call skipkl
     . ( icodev,loop8_cal+1,lpbuf,indxbra,indxket, segsm,4)
               goto 110
            endif
c
180         continue
            loop8_cal=loop8_cal+1
c
c           # process the formula tape
c
c           # word8 contains code,wtlphd,mlppr,mlp
c           #
c           # mlppr    weight of the bra part of the loop
c           # mlp      weight of the ket part of the loop
c
c
c           # code   0  new set of (k,l)
c           #        1  end of record
c           #        2  xz
c           #        3  wz
c           #        4  yy (1a,b)
c           #        5  yy (1b)
c           #        6  yy(1b')
c           #        7  xx (1a,b)
c           #        8  xx (1b)
c           #        9  xx (1b')
c           #        10 wx (1b)
c           #        11 wx (1b')
c           #        12 ww (1a,b)
c
             code=icodev(loop8_cal)
             rwlphd=headv(loop8_cal)
             mlppr= ybtv(loop8_cal)
             mlp= yktv(loop8_cal)

c
c           write(6,80800) iseg1,iseg2,isgflg,code
c
            if ( code .eq. 0 ) then
c  ### code 0
c              # new level.
               goto 110
            elseif ( code .eq. 1 ) then
c  ### code 1
c              # read the next buffer of formulas.
c
             call twoex_ft(.false.,indxbra,indxket,segsm,4)
               loop8_cal = 0
            elseif ( code .eq. 7 ) then
c  ### code 7
c              # 1a,b,4a,b 8a,b
               ldiag  = k1 .ne. l1
               logtr  = .true.
                value1 = valv(1,loop8_cal)
                value2 = valv(2,loop8_cal)
           call xxww(
     &        indsymbra, conftbra, indsymket,conftket,
     &        indxbra,indxket,cibra,sgmabra,ciket,sgmaket,
     &        transf, scr1,   scr2,   scr3,
     &        strtbx, 1, segsm,cist,xx2x  )

            elseif ( code .eq. 8 ) then
c  ### code 8
c              # 1b-4b,8b,11b
               ldiag  = .not.(mlp .eq. mlppr .and. cist(1).eq.cist(2))
               logtr  = .true.
               value2  = valv(1,loop8_cal)
           call xxww(
     &        indsymbra, conftbra, indsymket,conftket,
     &        indxbra,indxket,cibra,sgmabra,ciket,sgmaket,
     &        transf, scr1,   scr2,   scr3,
     &        strtbx, 1, segsm,cist,xx2x  )

            elseif ( code .eq. 9 ) then
c  ### code 9
c              # 1b-4b,8b,11b
               ldiag  = .not.(mlp .eq. mlppr .and. cist(1).eq.cist(2))
               logtr  = .false.
                value2  = valv(1,loop8_cal)
           call xxww(
     &        indsymbra, conftbra, indsymket,conftket,
     &        indxbra,indxket,cibra,sgmabra,ciket,sgmaket,
     &        transf, scr1,   scr2,   scr3,
     &        strtbx,1, segsm,cist,xx2x  )
            else
               call bummer('twoex: unknown code=', code, faterr )
            endif

c
c           # loop back for the next formula contribution.
            goto 180
c
c           # end of do loop k1
110      continue
c        # end of do loop l1
100   continue
c
      return
3333  format(' 110',20i6)
20000 format(' insufficient space for two electron integrals in twoex'
     & /' space wanted ',i6,'  space available ',i6)
20070 format(' twoext. nmb. of int. calculated',i6,' from sort',i6)
80800 format(' new loop value; iseg1,iseg2,isgflg',10i5)
      end
      subroutine twoex_ww(
     & indsymbra,conftbra,indsymket,conftket,
     & intbuf, transf, lpbuf,
     & scrj,   scrk,   scrkt,  scr1,
     & scr2,   scr3,
     & indxbra, cibra,   sgmabra,  indxket, ciket,  sgmaket,
     & segsm,  cist )

c*****
c  two external case
c
c  05-jun-91 twoseg() call added. -rls
c  06-may-91 infinite loop bug at 270 fixed. -eas.
c  15-apr-90 by e. stahlberg to allow 1 or 0 x,w segments
c  new version february 1988, hans lischka, university of vienna
c  23-dec-98 direct FT calculation implemented (Michal Dallos)
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     intbuf   buffer for two electron integrals
c     transf   lin. comb. of integrals
c     lpbuf    buffer for formula tape
c     scfj,scrk,scrkt j and k matrices
c     scr1,scr2,scr3 scratch matrices
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     sigm1    sigma vector for segment 1
c     sigm2    sigma vector for segment 2
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     nmbseg    number of segments
c     isegxc=0  take combination between all segments
c           =1  take combination between all segments excluding
c               yy loops (diference between segment numbers is one)
c           =2  take combination between different segments only
c               excluding yy loops (difference between segment
c               numbers is larger than one)
c     lstseg=.true.  last segment (ksg2=nseg) has to be taken into
c                    account
c     clcxz=.true.   compute xz and wz cases
c     nmsgxz         number of segments for xz and wz cases
c
         IMPLICIT NONE

C====>Begin Module TWOEX_WW               File twoex_ww.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER             RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             IREAD
      PARAMETER           (IREAD = 1)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
C
C     Argument variables
C
      INTEGER             CIST(2),     CONFTBRA(*), CONFTKET(*)
      INTEGER             INDSYMBRA(*),             INDSYMKET(*)
      INTEGER             INDXBRA(*),  INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              LPBUF(*),    SCR1(*),     SCR2(*),     SCR3(*)
      REAL*8              SCRJ(*),     SCRK(*),     SCRKT(*)
      REAL*8              SGMABRA(*),  SGMAKET(*),  TRANSF(*)
C
C     Local variables
C
      INTEGER             BLSTRA(9),   CODE,        DIFF,        FIN
      INTEGER             I,           IBLOCK,      ICONT1,      ICONT2
      INTEGER             IDUM,        IJ,          IND1,        IND2
      INTEGER             INTBLK,      INTST,       IPTH1(2),    ISGEXC
      INTEGER             ISIGN,       ISYM,        IWX,         JSYM
      INTEGER             K1,          KLS,         KLSYM,       KS
      INTEGER             KSG,         L1,          LINCMB,      LOOP8
      INTEGER             LOOP8_CAL,   LPST,        LS,          MAXBL
      INTEGER             MLP1,        MLPPR1,      MLPPRT,      NI
      INTEGER             NI2,         NIJ,         NMAX,        NMAX1
      INTEGER             NMBINT,      NMBM,        NMBSEG,      NMBUF
      INTEGER             NMIJ,        NMINT1,      NMSGXZ
      INTEGER             NUMVAL(2:12),             RBUFI,       RBUFL
      INTEGER             START,       TASKID,      TOTINT,      WTLPHD
C
      LOGICAL             CLCXZ,       LDIAG,       LOGTR,       LSTSEG
      LOGICAL             LSYMKL
C
      REAL*8              FACT,        SQRT2,       SQRT22,      VALUE
      REAL*8              VALUE1,      VALUE2,      WORD8
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'counter.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'special.inc'
      INCLUDE 'twoext.inc'
C====>End Module   TWOEX_WW               File twoex_ww.f               
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))
C
      data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /
c
c    ##  initialise the elements from common/lcontrol/
c
C HPM  subroutine_start
c       call _f_hpm_start_(59,5866, "pciudg6.f","twoex_ww")
C HPM
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c     write(6,*) '================== twoex_ww ======================='
c     write(6,*) 'ipth1,segsm,cist=',ipth1,segsm,cist
c     write(6,*) 'index1,index2'
c     call iwritex(index1,10)
c     call iwritex(index2,10)

c
      loop8_cal = 0
      sqrt2  = sqrt(two)
      sqrt22 = sqrt2 / two
      niot   = intorb
      iblock = 0
      ind2   = 0
      intblk = 0
      totint = 0
      maxbl  = maxbl2 + bufszi
      mlppr1 = 0
      mlp1   = 0
      isign  = 0
      iwx    = 0
      fact   = zero
      value  = zero
      value1 = zero
      value2 = zero
      word8  = zero
      ldiag  = .false.
      logtr  = .false.
      lsymkl = .false.
      rbufl  = rl( bufszl )
      rbufi  = ( bufszi )
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c
      intst = bl4xt +  1
c
c     # read the first record of the formula tape.
c
c
       call twoex_ft(.true.,indxbra,indxket,segsm,6)
c
      do 100 l1 = 1, intorb
         ls    = orbsym(l1)
         lsym  = sym21(ls)
         hilev = l1
         do 110 k1=1,l1
            ks    = orbsym(k1)
            ksym  = sym21(ks)
            kls   = mult(ks,ls)
            klsym = sym21(kls)
c           write(6,3333)l1,k1,lsym,ls,ksym,ks,klsym,kls
c
            lsymkl = ksym .ne. lsym
c
c           # determination of the block structure of the two
c           # electron integrals for fixed k1 and l1.
c
            nmij   = nmbpr(klsym)
c
            if ( nmij .eq. 0 ) then
               call skipkl
     . ( icodev,loop8_cal+1,lpbuf,indxbra,indxket,segsm,6)
               goto 110
            endif
c
c
180         continue
            loop8_cal=loop8_cal+1
c
c           # process the formula tape
c
c           # word8 contains code,wtlphd,mlppr,mlp
c           #
c           # mlppr    weight of the bra part of the loop
c           # mlp      weight of the ket part of the loop
c
c
c           # code   0  new set of (k,l)
c           #        1  end of record
c           #        2  xz
c           #        3  wz
c           #        4  yy (1a,b)
c           #        5  yy (1b)
c           #        6  yy(1b')
c           #        7  xx (1a,b)
c           #        8  xx (1b)
c           #        9  xx (1b')
c           #        10 wx (1b)
c           #        11 wx (1b')
c           #        12 ww (1a,b)
c
             code=icodev(loop8_cal)
             rwlphd=headv(loop8_cal)
             mlppr= ybtv(loop8_cal)
             mlp= yktv(loop8_cal)
c
c           write(6,80800) iseg1,iseg2,isgflg,code
c
            if ( code .eq. 0 ) then
c  ### code 0
c              # new level.
               goto 110
            elseif ( code .eq. 1 ) then
c  ### code 1
c              # read the next buffer of formulas.
c
             call twoex_ft(.false.,indxbra,indxket,segsm,6)
               loop8_cal = 0
            elseif ( code .eq. 12 ) then
c  ### code 12
c              # ww case
               ldiag  = k1 .ne. l1
               value1 = valv(1,loop8_cal)
               value2 = -value1 * half
               logtr  = .true.
           call xxww(
     &        indsymbra, conftbra, indsymket,conftket,
     &        indxbra,indxket,cibra,sgmabra,ciket,sgmaket,
     &        transf, scr1,   scr2,   scr3,
     &        strtbw, 2, segsm,cist,ww2x  )

            else
               call bummer('twoex: unknown code=', code, faterr )
            endif

c
c           # loop back for the next formula contribution.
            goto 180
c
c           # end of do loop k1
110      continue
c        # end of do loop l1
100   continue
c
      return
3333  format(' 110',20i6)
20000 format(' insufficient space for two electron integrals in twoex'
     & /' space wanted ',i6,'  space available ',i6)
20070 format(' twoext. nmb. of int. calculated',i6,' from sort',i6)
80800 format(' new loop value; iseg1,iseg2,isgflg',10i5)
      end
      subroutine twoex_yy(
     & indsymbra,conftbra,indxbra, cibra,sgmabra,
     & intbuf, transf, lpbuf,
     & scrj,   scrk,   scrkt,
     & indsymket, conftket, indxket, ciket,sgmaket,
     & segsm,  cist)

c*****
c
c     the possibility of several y segments
c     must be included later
c  two external case
c
c  05-jun-91 twoseg() call added. -rls
c  06-may-91 infinite loop bug at 270 fixed. -eas.
c  15-apr-90 by e. stahlberg to allow 1 or 0 x,w segments
c  new version february 1988, hans lischka, university of vienna
c  23-dec-98 direct FT calculation implemented (Michal Dallos)
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     intbuf   buffer for two electron integrals
c     transf   lin. comb. of integrals
c     lpbuf    buffer for formula tape
c     scfj,scrk,scrkt j and k matrices
c     scr1,scr2,scr3 scratch matrices
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     sigm1    sigma vector for segment 1
c     sigm2    sigma vector for segment 2
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     nmbseg    number of segments
c     isegxc=0  take combination between all segments
c           =1  take combination between all segments excluding
c               yy loops (diference between segment numbers is one)
c           =2  take combination between different segments only
c               excluding yy loops (difference between segment
c               numbers is larger than one)
c     lstseg=.true.  last segment (ksg2=nseg) has to be taken into
c                    account
c     clcxz=.true.   compute xz and wz cases
c     nmsgxz         number of segments for xz and wz cases
c
         IMPLICIT NONE

C====>Begin Module TWOEX_YY               File twoex_yy.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER             RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             IREAD
      PARAMETER           (IREAD = 1)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
C
C     Argument variables
C
      INTEGER             CIST(2),     CONFTBRA(*), CONFTKET(*)
      INTEGER             INDSYMBRA(*),             INDSYMKET(*)
      INTEGER             INDXBRA(*),  INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              LPBUF(*),    SCRJ(*),     SCRK(*)
      REAL*8              SCRKT(*),    SGMABRA(*),  SGMAKET(*)
      REAL*8              TRANSF(*)
C
C     Local variables
C
      INTEGER             BLSTRA(9),   CODE,        DIFF,        FIN
      INTEGER             I,           IBLOCK,      ICONT1,      ICONT2
      INTEGER             IDUM,        IJ,          IND1,        IND2
      INTEGER             INTBLK,      INTST,       ISGEXC,      ISIGN
      INTEGER             ISYM,        IWX,         JSYM,        K1
      INTEGER             KLS,         KLSYM,       KS,          KSG
      INTEGER             L1,          LINCMB,      LOOP8
      INTEGER             LOOP8_CAL,   LPST,        LS,          MAXBL
      INTEGER             MLP1,        MLPPR1,      MLPPRT,      NI
      INTEGER             NI2,         NIJ,         NMAX,        NMAX1
      INTEGER             NMBINT,      NMBM,        NMBSEG,      NMBUF
      INTEGER             NMIJ,        NMINT1,      NMSGXZ
      INTEGER             NUMVAL(2:12),             RBUFI,       RBUFL
      INTEGER             START,       TASKID,      TOTINT,      WTLPHD
C
      LOGICAL             CLCXZ,       LDIAG,       LOGTR,       LSTSEG
      LOGICAL             LSYMKL
C
      REAL*8              FACT,        SQRT2,       SQRT22,      VALUE
      REAL*8              VALUE1,      VALUE2,      WORD8
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'special.inc'
      INCLUDE 'twoext.inc'
C====>End Module   TWOEX_YY               File twoex_yy.f               
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))

C
      data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /
c
c     write(6,*) '================== twoex_yy ======================='
c     write(6,*) 'ipth1,segsm,cist=',ipth1,segsm,cist
c     write(6,*) 'index1,index2'
c     call iwritex(index1,10)
c     call iwritex(index2,10)

C HPM  subroutine_start
c       call _f_hpm_start_(60,6434, "pciudg6.f","twoex_yy")
C HPM
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c
      loop8_cal = 0
      sqrt2  = sqrt(two)
      sqrt22 = sqrt2 / two
      niot   = intorb
      iblock = 0
      ind2   = 0
      intblk = 0
      totint = 0
      maxbl  = maxbl2 + bufszi
      mlppr1 = 0
      mlp1   = 0
      isign  = 0
      iwx    = 0
      fact   = zero
      value  = zero
      value1 = zero
      value2 = zero
      word8  = zero
      ldiag  = .false.
      logtr  = .false.
      lsymkl = .false.
      rbufl  = rl( bufszl )
      rbufi  = ( bufszi )
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c
      intst = bl4xt +  1
c
c     # read the first record of the formula tape.
c
c
       call twoex_ft(.true.,indxbra,indxket,segsm,3)
c
      do 100 l1 = 1, intorb
         ls    = orbsym(l1)
         lsym  = sym21(ls)
         hilev = l1
         do 110 k1=1,l1
            ks    = orbsym(k1)
            ksym  = sym21(ks)
            kls   = mult(ks,ls)
            klsym = sym21(kls)
c           write(6,3333)l1,k1,lsym,ls,ksym,ks,klsym,kls
c
            lsymkl = ksym .ne. lsym
c
c           # determination of the block structure of the two
c           # electron integrals for fixed k1 and l1.
c
            nmij   = nmbpr(klsym)
            if ( nmij .eq. 0 ) then
               call skipkl
     . ( icodev,loop8_cal+1,lpbuf,indxbra,indxket,
     .   segsm,3)

               goto 110
            endif
180         continue
            loop8_cal=loop8_cal+1
c
c           # process the formula tape
c
c           # word8 contains code,wtlphd,mlppr,mlp
c           #
c           # mlppr    weight of the bra part of the loop
c           # mlp      weight of the ket part of the loop
c
c
c           # code   0  new set of (k,l)
c           #        1  end of record
c           #        2  xz
c           #        3  wz
c           #        4  yy (1a,b)
c           #        5  yy (1b)
c           #        6  yy(1b')
c           #        7  xx (1a,b)
c           #        8  xx (1b)
c           #        9  xx (1b')
c           #        10 wx (1b)
c           #        11 wx (1b')
c           #        12 ww (1a,b)
c
             code=icodev(loop8_cal)
             rwlphd=headv(loop8_cal)
             mlppr= ybtv(loop8_cal)
             mlp= yktv(loop8_cal)
c      write(6,*) 'YY: code,rwlphd,wtlphd,mlppr,mlp',
c    .  code,rwlphd,wtlphd,mlppr,mlp
c
            if ( code .eq. 0 ) then
c  ### code 0
c              # new level.
               goto 110
            elseif ( code .eq. 1 ) then
c  ### code 1
c              # read the next buffer of formulas.
c
             call twoex_ft(.false.,indxbra,indxket,segsm,3)
               loop8_cal = 0
            elseif ( code .eq. 4 ) then
c  ### code 4
               ldiag  = k1 .ne. l1
                value1 = valv(1,loop8_cal)
                value2 = valv(2,loop8_cal)
               logtr = .true.
c
c              # update the sigma vector.
c
               call yy(
     &          indsymbra, conftbra,  indxbra, cibra,sgmabra,
     &          indsymket, conftket,  indxket, ciket,sgmaket,
     &          transf, blstra,segsm,cist  )
            elseif ( code .eq. 5 ) then
c  ### code 5
               ldiag  = .not.(mlp .eq. mlppr .and. cist(1).eq.cist(2))
               logtr  = .true.
                value  = valv(1,loop8_cal)
               value2 = value
               value1 = zero
c
c              # update the sigma vector.
c
               call yy(
     &          indsymbra, conftbra,  indxbra, cibra,sgmabra,
     &          indsymket, conftket,  indxket, ciket,sgmaket,
     &          transf, blstra,segsm,cist  )
            elseif ( code .eq. 6 ) then
c  ### code 6
               ldiag  = .not.(mlp .eq. mlppr .and. cist(1).eq.cist(2))
               logtr  = .false.
                value  = valv(1,loop8_cal)
               value2 = value
               value1 = zero
c
c              # update the sigma vector.
c
               call yy(
     &          indsymbra, conftbra,  indxbra, cibra,sgmabra,
     &          indsymket, conftket,  indxket, ciket,sgmaket,
     &          transf, blstra,segsm,cist  )
            else
               call bummer('twoex: unknown code=', code, faterr )
            endif

c
c           # loop back for the next formula contribution.
            goto 180
c
c           # end of do loop k1
110      continue
c        # end of do loop l1
100   continue
c
      return
3333  format(' 110',20i6)
20000 format(' insufficient space for two electron integrals in twoex'
     & /' space wanted ',i6,'  space available ',i6)
20070 format(' twoext. nmb. of int. calculated',i6,' from sort',i6)
80800 format(' new loop value; iseg1,iseg2,isgflg',10i5)
      end
      subroutine yy(
     &          indsymbra, conftbra,  indxbra, cibra,sgmabra,
     &          indsymket, conftket,  indxket, ciket,sgmaket,
     &          transf, blstra,segsm,cist  )
c
         IMPLICIT NONE
C====>Begin Module YY                     File yy.f                     
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             BLSTRA(9),   CIST(2),     CONFTBRA(*)
      INTEGER             CONFTKET(*), INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*),  SEGSM(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    SGMABRA(*)
      REAL*8              SGMAKET(*),  TRANSF(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         M1
      INTEGER             M1LP,        M1LPR,       MUL,         NMB1
      INTEGER             NMB1A,       NMB2,        NMB2A,       TASKID
      INTEGER             YS,          YSPR,        YSYM,        YSYMPR
C
      LOGICAL             BRNEKT,      SKIP,        WALK
C
      INCLUDE 'counter.inc'
      INCLUDE 'data.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'twoext.inc'
      INCLUDE 'twoex_stat.inc'
C====>End Module   YY                     File yy.f                     
      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0
      if(spnodd)niot = niot + 1
      call upwkst(hilev, niot,rwlphd, mlppr,  mlp, clev  )
c
260   continue
      call upwlknew( ket, bra, walk, mul,segsm(2),segsm(1),skip )
      if ( walk )then
         if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(61,6839)
C HPM
         return
      else
         if (skip) goto 610
      endif

         nmb1a=indxbra(bra)

         if(nmb1a.lt.0)go to 610
         nmb2a=indxket(ket)
         if(nmb2a.lt.0)go to 610
        

c        call addtostat(nmb1a,nmb2a,cnt2x_yy,'2x_yy')

         nmb1=(nmb1a-1)/isize_yy+1
         nmb2=(nmb2a-1)/isize_yy+1
         cnt2x_yy(nmb1,nmb2)=cnt2x_yy(nmb1,nmb2)+1

         yy2ex=yy2ex+1
         


610   continue
      if(mul.gt.1 .or. hilev.lt.niot)go to 260
      if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(61,6866)
C HPM
      return
30000 format(' yy',20i4)
      end



