!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       real*8 function tcgtime()
        implicit none 
*@if defined (f95)
      real*8 cpu
      intrinsic cpu_time, system_clock
      call cpu_time( cpu )
      tcgtime=cpu
*@elif defined(linux) || defined(sun) 
*       real*4 tarray(2),etime
*       real*8 fwtime
*       external fwtime,etime
*       tcgtime = (etime(tarray))
*@elif defined(aix)
*       integer mclock
*       external mclock
*       tcgtime = dfloat(mclock())*0.01d0
*@elif defined  t3e64
*      real*8   timef
*      external timef
*      call secondr(tcgtime)
*C     tcgtime=timef()
*@endif

        return
        end

       subroutine ga_terminate
        implicit none
c       write(6,*) 'ga_terminate'
        return
        end

       subroutine ga_initialize

       implicit none 
c      write(6,*) 'entering dummy ga_initialize'
       return
       end

       integer function ga_nodeid()
       implicit none 
       ga_nodeid=0
       return
       end

       integer function ga_nnodes()
       implicit none 
       ga_nnodes=1
       return
       end

       subroutine ga_brdcst(itype,array,len,node)
       implicit none
        integer itype,len,node,array(*)

c      write(6,*) 'broadcasting ',itype,len,node
       return
       end

       subroutine ga_sync
       implicit none
c      write(6,*) 'dummy ga_sync'
       return
       end

       subroutine ga_set_memory_limit(size)
       implicit none
        integer size
c      write(6,*) 'ga_limit:',size
       return
       end

       logical function ga_create(itype,dim1,dim2,name,ch1,ch2,hdle)
       implicit none        
       integer gnxttsk
       common /ganxttask/ gnxttsk
       integer v,w,hd,fourw,fourx,thrw,thrx,drtfil,dg,ofdg,nxttask
       parameter (v=11,w=10,hd=4,fourw=31,fourx=32,thrw=33,thrx=34,
     .            drtfil=38,dg=12,ofdg=13,nxttask=90)

       integer itype,dim1,dim2,ch1,ch2,hdle
       character*(*) name
       logical compabb
c      write(6,*) 'creating ga ',name
       if (compabb(name,'civect',6)) hdle=v
       if (compabb(name,'sigvec',6)) hdle=w
       if (compabb(name,'hdg',3)) hdle=hd
       if (compabb(name,'ex3w',4)) hdle=thrw
       if (compabb(name,'ex3x',4)) hdle=thrx
       if (compabb(name,'ex4w',4)) hdle=fourw
       if (compabb(name,'ex4x',4)) hdle=fourx
       if (compabb(name,'diag',4)) hdle=dg
       if (compabb(name,'ofdg',4)) hdle=ofdg
       if (compabb(name,'drt',3)) hdle=drtfil
       if (compabb(name,'nxttask',7)) then
            hdle=nxttask
            gnxttsk=0
       endif
       ga_create=.true.
       return
       end

       integer function  ga_read_inc(hdle,ilo,ihi,inc)
       implicit none
       integer hdle,ilo,ihi,inc
       integer gnxttsk
       common /ganxttask/ gnxttsk
       integer v,w,hd,fourw,fourx,thrw,thrx,drtfil,dg,ofdg,nxttask
       parameter (v=11,w=10,hd=4,fourw=31,fourx=32,thrw=33,thrx=34,
     .            drtfil=38,dg=12,ofdg=13,nxttask=90)
       if (hdle.eq.nxttask) then
         ga_read_inc=gnxttsk
         gnxttsk=gnxttsk+1
       endif
       return
       end

       subroutine ga_error(string,itype)
       implicit none
        character*(*) string
       integer itype
       call bummer(string,itype,2)
       return
       end

       logical function ga_create_mutexes(ino)
       implicit none
        integer ino

c      write(6,*) 'create ',ino,'mutexes'
       ga_create_mutexes=.true.
       return
       end

       logical function ga_destroy_mutexes()
       implicit none
c      write(6,*) 'destroying mutexes'
       ga_destroy_mutexes=.true.
       return
       end

       subroutine ga_print_distribution(hdle)
       implicit none
        integer hdle
c      write(6,*) 'print distribution for ',hdle
       return
       end

       subroutine ga_distribution(hdle,node,ilo,ihi,jlo,jhi)
       implicit none
         integer mxnseg,nvmax
        parameter (mxnseg=200,nvmax=40)
C     Common variables
C
      INTEGER         LENCI, NSEG(7)
      INTEGER         SEGCI(MXNSEG,7),CIST(MXNSEG,7)
      INTEGER         SEGEL(MXNSEG,7),            SEGSCR(MXNSEG,7)
      INTEGER         SEGTYP(MXNSEG,7),       IPTHST(MXNSEG,7)
      INTEGER         DRTST(MXNSEG,7), DRTLEN(MXNSEG,7)
      INTEGER         DRTIDX(MXNSEG,7)
C
      COMMON /CIVCT  /  LENCI,       NSEG,  SEGEL, CIST
      COMMON /CIVCT  /  SEGCI,       SEGSCR,      SEGTYP, IPTHST
      COMMON /CIVCT /   DRTST, DRTLEN, DRTIDX
C
c
c   LENCI                   length of CI vector
c NSEG(I)                   total number of segments in segmentation sc
c   SEGCI(i,j)         number of CI elements in segment j of segment. s
c   CIST(MXNSEG,7)     offset of first CI element corresponding to segm
c    SEGEL(MXNSEG,7),  number of internal paths in segment j of seg sch
c    SEGSCR(MXNSEG,7)   AO-driven stuff
c    SEGTYP(MXNSEG,7), segment type of segment j, seg scheme i
c       IPTHST(MXNSEG,7) offset of first internal path in index segment
c                        belonging to segment j of seg. scheme i
c    DRTST(MXNSEG,7),    offset of first element of DRT information on
c                        belonging to segment j of seg scheme i
c    DRTLEN(MXNSEG,7)    number of elements of DRT info on file drtfil
c    DRTIDX(MXNSEG,7)    length of index vector segment for segment j s
c
C     Common variables
C
      INTEGER         CSFPRN,      DAVCOR,      FRCSUB,      FROOT
      INTEGER         FTCALC,      IBKTHV,      IBKTV,       ICITHV
      INTEGER         ICITV,       IORTLS,      ISTRT,       IVMODE
      INTEGER         MAXSEG,      NBKITR,      NCOUPLE,     NITER
      INTEGER         NO0EX,       NO1EX,       NO2EX,       NO3EX
      INTEGER         NO4EX,       NOLDHV,      NOLDV,       NRFITR
      INTEGER         NROOT,       NTYPE,       NUNITV,      NVBKMN
      INTEGER         NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER         NVRFMX,      RTMODE,      VOUT
      INTEGER         NSEGD(4),    NSEG0X(4),   NSEG1X(4),NSEG2X(4)
      INTEGER         NSEG3X(4),   NSEG4X(4),   NSEGSO(4),NODIAG
      integer             cdg4ex,c3ex1ex,c2ex0ex,fileloc(10)
      integer             finalv,finalw
C
      REAL*8            CTOL,        ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8            LRTSHIFT
C
      COMMON /INDATA /  LRTSHIFT,    ETOLBK,      ETOL,        CTOL
      COMMON /INDATA /  NROOT,       NOLDV,       NOLDHV,      NUNITV
      COMMON /INDATA /  NBKITR,      NITER,       DAVCOR,      CSFPRN
      COMMON /INDATA /  IVMODE,      ISTRT,       VOUT,        IORTLS
      COMMON /INDATA /  NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
      COMMON /INDATA /  ICITV,       ICITHV,      FRCSUB,      NVBKMN
      COMMON /INDATA /  NVCIMN,      MAXSEG,      NTYPE,       NRFITR
      COMMON /INDATA /  NVRFMX,      NVRFMN,      FROOT,       FTCALC
      COMMON /INDATA /  RTMODE,      NCOUPLE,     NO0EX,       NO1EX
      COMMON /INDATA /  NO2EX,       NO3EX,       NO4EX,       NODIAG
      COMMON /INDATA /  NSEGD, NSEG0X, NSEG1X, NSEG2X, NSEG3X, NSEG4X
      COMMON /INDATA /  NSEGSO,cdg4ex,c3ex1ex,c2ex0ex,fileloc
      COMMON /INDATA /  FINALV, FINALW
C
C     Equivalenced common variables
C
      INTEGER         MODE,        NCIITR,      NINITV
C
      REAL*8            RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8            RTOLCI(NVMAX)
C

c LRTSHIFT,      aqcc/lrt shift  (falls <0 )
c ETOLBK,        etol-Grenzwert fuer BK iteration
c  ETOL,         etol-Grenzwert fuer CI iteration
c       CTOL     ctol-Grenzwert fuer CSF Analyse
c NROOT,         Zahl der zu berechnenden Wurzeln
c    NOLDV,      Zahl der vorhandenen CI-Vektoren auf civfl
c       NOLDHV,  Zahl der vorhandenen Sigma-Vektoren auf cihvfl
c     NUNITV     Zahl der zu generierenden Startvektoren
c NBKITR,        max. Zahl der BK Iterationen
c    NITER,      max. Zahl der CI Iterationen
c       DAVCOR,  Davidson-Korrektur berechnen
c     CSFPRN     CI-Vektor analysieren
c IVMODE,        Startvektorgenerierung
c     ISTRT,     Startvektorgenerierung bei ivmode=0
c      VOUT,     ????
c       IORTLS   defaults for ICITV,ICITHV
c NVBKMX,        maximale Subspace dimension in BK-Iteratione
c    IBKTV,      kontrolliert transformation der BK V-Vektoren
c       IBKTHV,  kontrolliert transformation der BK Sigma-Vektoren
c     NVCIMX     maximale Subspace dimension in CI-Iteration
c ICITV,         kontrolliert transformation der CI V-Vektoren
c      ICITHV,   kontrolliert transformation der CI Sigma-Vektoren
c     FRCSUB,    kontrolliert Neuberechnung der Subspace-Darstellung
c      NVBKMN    min. Gr��e des BK subspaces
c NVCIMN,        min. Gr��e des CI subspaces
c     MAXSEG,    max. Zahl von CI Segmenten
c     NTYPE,     Rechnungstyp
c       NRFITR   max. Zahl der CI Iterationen bei iterat. Referenzraumd
c NVRFMX,        max. Gr��e es Subspaces bei iterat. Referenzraumdiagon
c    NVRFMN,     min. Gr��e des Subspaces bei iterat. Referenzraumdiago
c    FROOT,      Root to follow
c       FTCALC   Berechnung des Formulatapes on the fly
c RTMODE,        obsolet
c    NCOUPLE,    Iteration in der der Lrtshift zum ersten Mal anzuwende
c     NO0EX,     keine Berechnung der 0externen Beitraege
c     NO1EX      keine Berechnung der 1externen Beitr�ge
c NO2EX,         keine Berechnung der 2externen Beitr�ge
c     NO3EX,     keine Berechnung der 3externen Beitr�ge
c     NO4EX,     keine Berechnung der 4externen Beitr�ge
c      NODIAG    keine Berechnung der diagonalen Beitr�ge
c NSEGD,         Segmentierungschema f�r diagonale Beitr�ge
c NSEG0X,        Segmentierungsschema fuer all-interne Beitr�ge
c NSEG1X,        ... 1externe ....
cNSEG2X,         ... 2externe ....
cNSEG3X,         ... 3externe ....
c NSEG4X         ... 4externe ....
c NSEGSO,        ...  ...
ccdg4ex,         Behandle diagonalfall und 4-externen Fall gemeinsam
cc3ex1ex,        Behandle 3-externen Fall und 1-externen Fall gemeinsam
cc2ex0ex,        Behandle 2-externen Fall und 0-externen Fall gemeinsam
cfileloc         Filetyp f�r   diagint,ofdgint,fil3w,fil3x,fil4w,fil4x
c                  mit 1 = vdisk  2= ga  3=distrib. (one-proc-sys) 4=di
cfinalv          final v vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk
cfinalw          final w vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk

       integer hdle,node,ilo,ihi,jlo,jhi

         ilo=1
         ihi=lenci
         jlo=1
         jhi=nvcimx

        if (node.ne.0)
     .    call bummer('invalid call to ga_distribution',node,2)
        return
        end

        subroutine ga_igop (itype,array,len,op)
        implicit none
        integer itype,array(*),len
        character*1 op
c       write(6,*) 'ga_igop',itype
        return
        end


        subroutine ga_dgop (itype,array,len,op)
        implicit none
         integer itype,array(*),len
        character*1 op
c       write(6,*) 'ga_dgop',itype
        return
        end

        subroutine ga_lock (mutex)
        implicit none
        integer mutex
c       write(6,*) 'ga_lock ',mutex
        return
        end

        subroutine ga_unlock (mutex)
        implicit none
        integer mutex
c       write(6,*) 'ga_unlock ',mutex
        return
        end

        subroutine ga_fence
        implicit none
c       write(6,*) 'ga_fence'
        return
        end

        subroutine ga_init_fence
        implicit none
c       write(6,*) 'ga_init_fence'
        return
        end

        subroutine ga_get(hdle,start,end,recst,recend,vector,ix)
        implicit none
         integer hdle,start,end,recst,recend,ix
        integer mxnseg,mxunit
        parameter (mxnseg=200,mxunit=99)
C     Common variables
C
      INTEGER         LENCI, NSEG(7)
      INTEGER         SEGCI(MXNSEG,7),CIST(MXNSEG,7)
      INTEGER         SEGEL(MXNSEG,7),            SEGSCR(MXNSEG,7)
      INTEGER         SEGTYP(MXNSEG,7),       IPTHST(MXNSEG,7)
      INTEGER         DRTST(MXNSEG,7), DRTLEN(MXNSEG,7)
      INTEGER         DRTIDX(MXNSEG,7)
C
      COMMON /CIVCT  /  LENCI,       NSEG,  SEGEL, CIST
      COMMON /CIVCT  /  SEGCI,       SEGSCR,      SEGTYP, IPTHST
      COMMON /CIVCT /   DRTST, DRTLEN, DRTIDX
C
c
c   LENCI                   length of CI vector
c NSEG(I)                   total number of segments in segmentation sc
c   SEGCI(i,j)         number of CI elements in segment j of segment. s
c   CIST(MXNSEG,7)     offset of first CI element corresponding to segm
c    SEGEL(MXNSEG,7),  number of internal paths in segment j of seg sch
c    SEGSCR(MXNSEG,7)   AO-driven stuff
c    SEGTYP(MXNSEG,7), segment type of segment j, seg scheme i
c       IPTHST(MXNSEG,7) offset of first internal path in index segment
c                        belonging to segment j of seg. scheme i
c    DRTST(MXNSEG,7),    offset of first element of DRT information on
c                        belonging to segment j of seg scheme i
c    DRTLEN(MXNSEG,7)    number of elements of DRT info on file drtfil
c    DRTIDX(MXNSEG,7)    length of index vector segment for segment j s
c
C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

        real*8 vector(*)
        integer v,w,hd,drtfil
          integer*8 offset
c     integer cosmofile
        parameter (v=11,w=10,hd=4,drtfil=38)
c
         if (hdle.eq.v) then
           offset=(recst-1)*lenci+start
           call getputvec('civfl',vector,end-start+1,offset,'openrd')
         elseif (hdle.eq.w) then
           offset=(recst-1)*lenci+start
         call getputvec('cihvfl',vector,end-start+1,offset,'openrd')
         elseif (hdle.eq.hd) then
           offset=start
        call getputvec('cihdiag',vector,end-start+1,offset,'openrd')
        else
          call bummer( 'invalid call to ga_get',hdle,2)
        endif
        return   
        end

        subroutine ga_put(hdle,start,end,recst,recend,vector,ix)
        implicit none
        integer mxnseg,mxunit
        parameter (mxnseg=200,mxunit=99)
C     Common variables
C
      INTEGER         LENCI, NSEG(7)
      INTEGER         SEGCI(MXNSEG,7),CIST(MXNSEG,7)
      INTEGER         SEGEL(MXNSEG,7),            SEGSCR(MXNSEG,7)
      INTEGER         SEGTYP(MXNSEG,7),       IPTHST(MXNSEG,7)
      INTEGER         DRTST(MXNSEG,7), DRTLEN(MXNSEG,7)
      INTEGER         DRTIDX(MXNSEG,7)
C
      COMMON /CIVCT  /  LENCI,       NSEG,  SEGEL, CIST
      COMMON /CIVCT  /  SEGCI,       SEGSCR,      SEGTYP, IPTHST
      COMMON /CIVCT /   DRTST, DRTLEN, DRTIDX
C
c
c   LENCI                   length of CI vector
c NSEG(I)                   total number of segments in segmentation sc
c   SEGCI(i,j)         number of CI elements in segment j of segment. s
c   CIST(MXNSEG,7)     offset of first CI element corresponding to segm
c    SEGEL(MXNSEG,7),  number of internal paths in segment j of seg sch
c    SEGSCR(MXNSEG,7)   AO-driven stuff
c    SEGTYP(MXNSEG,7), segment type of segment j, seg scheme i
c       IPTHST(MXNSEG,7) offset of first internal path in index segment
c                        belonging to segment j of seg. scheme i
c    DRTST(MXNSEG,7),    offset of first element of DRT information on
c                        belonging to segment j of seg scheme i
c    DRTLEN(MXNSEG,7)    number of elements of DRT info on file drtfil
c    DRTIDX(MXNSEG,7)    length of index vector segment for segment j s
c
C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

        integer mode
*@ifdef rs6000
*        parameter (mode=2)
*@else
        parameter (mode=0)
*@endif
        integer hdle,start,end,recst,recend,ix
        integer vector(*)
        integer*8 offset
        integer v,w,hd,drtfil,nxttsk
        parameter (v=11,w=10,hd=4,drtfil=38,nxttsk=90)
        integer gnxttsk
        common /ganxttask/ gnxttsk
c
         if (hdle.eq.v) then
           offset=(recst-1)*lenci+start
           call getputvec('civfl',vector,end-start+1,offset,'openwt')
         elseif (hdle.eq.w) then
           offset=(recst-1)*lenci+start
         call getputvec('cihvfl',vector,end-start+1,offset,'openwt')
         elseif (hdle.eq.hd) then
           offset=start
         call getputvec('cihdiag',vector,end-start+1,offset,'openwt')
        elseif (hdle.eq.nxttsk) then
           gnxttsk=vector(1)
        else
          call bummer( 'invalid call to ga_put',hdle,2)
        endif
        return
        end


        subroutine ga_acc(hdle,start,end,recst,recend,vector,ix,one)
        implicit none
        integer hdle,start,end,recst,recend
        real*8 ix,one,vector(*)

        call bummer('invalid call to ga_acc',hdle,2)
        return
        end


        subroutine ga_fill_patch (hdle,ilo,ihi,jlo,jhi,val)
        implicit  none
        integer mxnseg,mxunit,reclen
        parameter (mxnseg=200,mxunit=99,reclen=32768)
C     Common variables
C
      INTEGER         LENCI, NSEG(7)
      INTEGER         SEGCI(MXNSEG,7),CIST(MXNSEG,7)
      INTEGER         SEGEL(MXNSEG,7),            SEGSCR(MXNSEG,7)
      INTEGER         SEGTYP(MXNSEG,7),       IPTHST(MXNSEG,7)
      INTEGER         DRTST(MXNSEG,7), DRTLEN(MXNSEG,7)
      INTEGER         DRTIDX(MXNSEG,7)
C
      COMMON /CIVCT  /  LENCI,       NSEG,  SEGEL, CIST
      COMMON /CIVCT  /  SEGCI,       SEGSCR,      SEGTYP, IPTHST
      COMMON /CIVCT /   DRTST, DRTLEN, DRTIDX
C
c
c   LENCI                   length of CI vector
c NSEG(I)                   total number of segments in segmentation sc
c   SEGCI(i,j)         number of CI elements in segment j of segment. s
c   CIST(MXNSEG,7)     offset of first CI element corresponding to segm
c    SEGEL(MXNSEG,7),  number of internal paths in segment j of seg sch
c    SEGSCR(MXNSEG,7)   AO-driven stuff
c    SEGTYP(MXNSEG,7), segment type of segment j, seg scheme i
c       IPTHST(MXNSEG,7) offset of first internal path in index segment
c                        belonging to segment j of seg. scheme i
c    DRTST(MXNSEG,7),    offset of first element of DRT information on
c                        belonging to segment j of seg scheme i
c    DRTLEN(MXNSEG,7)    number of elements of DRT info on file drtfil
c    DRTIDX(MXNSEG,7)    length of index vector segment for segment j s
c
C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i
        integer mode
*@ifdef rs6000
*        parameter (mode=2)
*@else
        parameter (mode=0)
*@endif

        real*8 buffer(reclen)
        integer recst,ipos
        integer v,w,hd,drtfil,nxttsk
        parameter (v=11,w=10,hd=4,drtfil=38,nxttsk=90)
        integer hdle,ilo,ihi,jlo,jhi
        character*10 fname
        integer ihdle,ixlen
         integer*8 offset
        real*8 val
        if (dalen(hdle).ne.reclen)
     .    call bummer('invalid reclen in ga_fill_patch',dalen(hdle),2)
        if (abs(val).gt.1.d-10)
     .   call bummer('invalid call to ga_fill_patch',hdle,2)
        call wzero(reclen,buffer,1)
        if (hdle.eq.v .or. hdle.eq.w .or. hdle.eq.hd ) then
          if (hdle.eq.v) then
                 ihdle=2
                 fname='civfl'
                 ixlen=5
          elseif (hdle.eq.w) then
                   ihdle=3
                   fname='cihvfl'
                 ixlen=6
          else
                   ihdle=1
                   fname='cihdiag'
                 ixlen=7
          endif

          do recst=jlo,jhi
           do ipos = ilo,ihi,reclen
            if (ihdle.eq.2)  then
               offset=ipos
            else
               offset=ipos+(recst-1)*int(lenci,8)
            endif
            call getputvec(fname(1:ixlen),buffer,min(ihi-ipos+1,reclen),
     .                     offset,'openwt')
           enddo
          enddo
        else
          call bummer( 'invalid call to ga_fillpatch',hdle,2)
        endif
*
        return
        end


        subroutine ga_zero(hdle)
         implicit none
         integer hdle
         call bummer('invalid call to ga_zero',hdle,2)
         return
         end


        subroutine ga_access(a,b,c,d,e)
        return
        end

        subroutine ga_release(a,b,c,d,e)
        return
        end

        integer function mitob(len)
        implicit none
        integer len
*@ifdef int64
        mitob=len*8
*@else
*        mitob=len*4
*@endif
        return
        end

        integer function mdtob(len)
        implicit none
        integer len
        mdtob=len*8
        return
        end


