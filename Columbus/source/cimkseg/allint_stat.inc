!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER*8           CNT0X_WW(150,150),        CNT0X_XX(150,150)
      INTEGER*8           CNT0X_YY(150,150),        CNT0X_ZZ(150,150)
C
      COMMON / ALLINT_STAT/            CNT0X_ZZ,    CNT0X_YY
      COMMON / ALLINT_STAT/            CNT0X_WW,    CNT0X_XX
C
