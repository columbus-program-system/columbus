#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


#print @ARGV,"=$#ARGV \n";

#defaults

 $prtlevel=0;
 $ncpu=0;
 $fraction=0;
 $maxtask=0;
 $tmodel=0;
 $maxmem=100000000;

#
#./optimize.pl  -ncpu 64 -maxtask 0.7 -maxmem 16000000 -tmodel tmodells -print 1
#

 while ($_=shift @ARGV)
  {  if (/-help/) { die "usage: optimize.pl -print <number> -ncpu <number> -maxtask  <number> -fraction <number> -tmodel <filename> \n";}
     if (/-print/) { $prtlevel=shift @ARGV;next;}
     if (/-ncpu/) {$ncpu=shift @ARGV; next;}
     if (/-maxtask/) {$maxtask=shift @ARGV; next;}
     if (/-fraction/) {$fraction=shift @ARGV; next;}
     if (/-maxmem/) { $maxmem=shift @ARGV; next;}
     if (/-tmodel/) {$tmodel=shift @ARGV; next;}
      die "usage: optimize.pl -print <number> -ncpu <number> -maxtask  <number> -fraction <number> -tmodel <filename> \n";
  }

if ( ! ($ncpu && $maxtask) ) { die ("specify number of cpus and maxtask fraction e.g. -ncpu 16 -maxtask 0.5 \n");}

 $tottask=int($ncpu/$maxtask)+1;

$LARGEST_TASK=0;
$zero_task=0;
@zero_task=();


#
#  read SEGANALYSE
#

open FIN, "<SEGANALYSE";
 $_=<FIN>; if ( ! /NVALZYXW/) {die ("inconsistent SEGANALYSE file \n");}
           chop; s/^.*= *//; @nval_zyxw=split(/\s+/,$_);

 $_=<FIN>; if ( ! /NEXTERN/) {die ("inconsistent SEGANALYSE file \n");}
            chop; s/^.*= *//; @nextern=split(/\s+/,$_);

 $_=<FIN>;

 $_=<FIN>; if ( ! /BATCHSIZE_ZYXW/) {die ("inconsistent SEGANALYSE file \n");}
            chop; s/^.*= *//; @nsize_zyxw=split(/\s+/,$_);

 $_=<FIN>; if ( ! /CSFBATCH_Z/) {die ("inconsistent SEGANALYSE file Z: $_\n");}
     $tmpz=" "; 
     while (<FIN>)  { if ( /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;} 
     $tmpz =~ s/^ *//; 
     $tmpz =~ s/ *$//; 
     @csfbatch_z = split(/\s+/,$tmpz);
     unshift @csfbatch_z, -1;

 if ( ! /CSFBATCH_Y/) {die ("inconsistent SEGANALYSE file Y:$_ \n");}
     $tmpz=" ";
     while (<FIN>)  { if (  /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
     $tmpz =~ s/^ *//;
     $tmpz =~ s/ *$//;
     @csfbatch_y = split(/\s+/,$tmpz);
     unshift @csfbatch_y, -1;

 if ( ! /CSFBATCH_X/) {die ("inconsistent SEGANALYSE file X:$_ \n");}
     $tmpz=" ";
     while (<FIN>)  { if (  /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
     $tmpz =~ s/^ *//;
     $tmpz =~ s/ *$//;
     @csfbatch_x = split(/\s+/,$tmpz);
     unshift @csfbatch_x, -1;

 if ( ! /CSFBATCH_W/) {die ("inconsistent SEGANALYSE file W:$_ \n");}
     $tmpz=" ";
     while (<FIN>)  { if ( /[A-Za-z]/ ) {last;} chop;  $tmpz = $tmpz . $_;}
     $tmpz =~ s/^ *//;
     $tmpz =~ s/ *$//;
     @csfbatch_w = split(/\s+/,$tmpz);
     unshift @csfbatch_w, -1;




close FIN;
print "number of valid internal walks: ",join(' ',@nval_zyxw),"\n";
print "number of external orbitals   : ",join(' ',@nextern),"\n";
print "batch sizes                   : ",join(' ',@nsize_zyxw),"\n";

if ($prtlevel > 1 ) {
print "CSFBATCH_Z:",join(' ',@csfbatch_z),"\n";
print "CSFBATCH_Y:",join(' ',@csfbatch_y),"\n";
print "CSFBATCH_X:",join(' ',@csfbatch_x),"\n";
print "CSFBATCH_W:",join(' ',@csfbatch_w),"\n";
 }


#
#  cimkseg.x  decomposes z,y,w,x walks into at most 75 segments each

 @zz_0x=();
 @yy_0x=();
 @xx_0x=();
 @ww_0x=();
($zseg,$tmpj)=getdata("0x","zz",\@zz_0x,$nsize_zyxw[0],$nsize_zyxw[0]);
($yseg,$tmpj)=getdata("0x","yy",\@yy_0x,$nsize_zyxw[1],$nsize_zyxw[1]);
($xseg,$tmpj)=getdata("0x","xx",\@xx_0x,$nsize_zyxw[2],$nsize_zyxw[2]);
($wseg,$tmpj)=getdata("0x","ww",\@ww_0x,$nsize_zyxw[3],$nsize_zyxw[3]);

print "segments: $zseg,$yseg,$xseg, $wseg \n";

 if ($prtlevel > 1) { printdata("all-internal zz ",\@zz_0x,$zseg,$zseg,0);
                     printdata("all-internal yy ",\@yy_0x,$yseg,$yseg,0);
                     printdata("all-internal xx ",\@xx_0x,$xseg,$xseg,0);
                     printdata("all-internal ww ",\@ww_0x,$wseg,$wseg,0);}
 
 @yz_1x=();
 @yw_1x=();
 @yx_1x=();
($tmpi,$tmpj)=getdata("1x","yz",\@yz_1x,$nsize_zyxw[0],$nsize_zyxw[1]);
 if ($tmpi != $zseg || $tmpj != $yseg ) {die(" 1xyz $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("1x","yw",\@yw_1x,$nsize_zyxw[1],$nsize_zyxw[3]);
 if ($tmpi != $wseg || $tmpj != $yseg ) {die(" 1xwy $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("1x","yx",\@yx_1x,$nsize_zyxw[1],$nsize_zyxw[2]);
 if ($tmpi != $xseg || $tmpj != $yseg ) {die(" 1xxy $tmpi,$tmpj\n");}

if ($prtlevel > 1) { printdata("one-external yz ",\@yz_1x,$zseg,$yseg,1);
                     printdata("one-external yw ",\@yw_1x,$yseg,$wseg,1);
                     printdata("one-external yx ",\@yx_1x,$yseg,$wseg,1);}

 @yy_2x=();
 @xx_2x=();
 @ww_2x=();
 @wx_2x=();
 @wz_2x=();
 @xz_2x=();

($tmpi,$tmpj)=getdata("2x","yy",\@yy_2x,$nsize_zyxw[1],$nsize_zyxw[1]);
if ($tmpi != $yseg || $tmpj != $yseg ) {die(" 2xyy $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("2x","xx",\@xx_2x,$nsize_zyxw[2],$nsize_zyxw[2]);
if ($tmpi != $xseg || $tmpj != $xseg ) {die(" 2xxx $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("2x","ww",\@ww_2x,$nsize_zyxw[3],$nsize_zyxw[3]);
if ($tmpi != $wseg || $tmpj != $wseg ) {die(" 2xww $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("2x","wx",\@wx_2x,$nsize_zyxw[3],$nsize_zyxw[2]);
if ($tmpi != $xseg || $tmpj != $wseg ) {die(" 2xwx $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("2x","wz",\@wz_2x,$nsize_zyxw[3],$nsize_zyxw[0]);
if ($tmpi != $zseg || $tmpj != $wseg ) {die(" 2xzw $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("2x","xz",\@xz_2x,$nsize_zyxw[2],$nsize_zyxw[0]);
if ($tmpi != $zseg || $tmpj != $xseg ) {die(" 2xzx $tmpi,$tmpj\n");}

 if ($prtlevel > 1) { printdata("two-internalzz ",\@zz_2x,$zseg,$zseg,0);
                     printdata("two-internal yy ",\@yy_2x,$yseg,$yseg,0);
                     printdata("two-internal xx ",\@xx_2x,$xseg,$xseg,0);
                     printdata("two-internal ww ",\@ww_2x,$wseg,$wseg,0);
                     printdata("two-internal wx ",\@wx_2x,$wseg,$xseg,1);
                     printdata("two-internal wz ",\@wz_2x,$wseg,$zseg,1);
                     printdata("two-internal xz ",\@xw_2x,$xseg,$zseg,1);}
 

@yw_3x=();
@yx_3x=();

($tmpi,$tmpj)=getdata("3x","yx",\@yx_3x,$nsize_zyxw[1],$nsize_zyxw[2]);
if ($tmpi != $yseg || $tmpj != $xseg ) {die(" 3xyx $tmpi,$tmpj\n");}

($tmpi,$tmpj)=getdata("3x","yw",\@yw_3x,$nsize_zyxw[1],$nsize_zyxw[3]);
if ($tmpi != $yseg || $tmpj != $wseg ) {die(" 3xyw $tmpi,$tmpj\n");}

 if ($prtlevel > 1) { printdata("three-internal yx ",\@yx_3x,$yseg,$xseg,1);
                     printdata("three-internal yw ",\@yw_3x,$yseg,$wseg,1);}


#
#  use $nseg1 = compseg($nvalzyxw()) to compute number of segments
#  use $nsize = compsize($nvalzyxw(1)) to compute segment batch size
#
################## END OF READING SEGANALYSE ############################

#
# scale data
#

#
#   2-ext:    yy  matrix*vector              O(N**2)   
#             xx  matrix*matrix              O(N**3)
#             ww  matrix*matrix              O(N**3)
#             wx  matrix*matrix              O(N**3)
#             wz  daxpy                      O(N)
#             xz  daxpy                      O(N)
#
#  1-ext      yz  daxpy                      O(N)
#             yw  matrix*vector              O(N**2)
#             yx  matrix*vector              O(N**2)
#
#  0-ext     
#             zz  scalar                     O(1)
#             yy  daxpy                      O(N)
#             xx  daxpy                      O(N)
#             ww  daxpy                      O(N)
#
#
#   MFLOPS   matrix*matrix = 4* to 10* matrix-vector
#            matrix*vector = 4* to 10* daxpy
#            daxpy         = 4* to 10* scalar 
#
#
#    cnts -> time  :   cnt*weight factor 
#                    
#    weight-factor for matrix-matrix product = 1
#                  for matrix-vector product = 10/(sum Nextern)
#                  for daxpy                 = 100/(sum Nextern**2)
#                  for scalar                = 1000/(sum Nextern**3) 
#
############################################################################


#
# adjust machine dependent scaling factors for actual costs
# approx. cpu_seconds for class type/cost numbers for task type
#

@vol0x=(); # zz,yy,xx,ww
@vol1x=(); # yz yx yw
@vol2x=(); # xz,wz,yy,xx,ww,wx 
@vol3x=(); # yx yw
@vol4x=(); # 

@tcost=computescaling($tmodel);

$totcost=$tcost[0]+$tcost[1]+$tcost[2]+$tcost[3];


$avcost=$totcost/$tottask;


#scaling factors need to be derived 

@ciudgin=();

%buckets=();
$ciseg0x= allinternal($avcost,$tcost[0]);
$ciseg1x= one_external($avcost,$tcost[1]);
$ciseg2x= two_external($avcost,$tcost[2]);
$ciseg3x= three_external($avcost,$tcost[3]);

$cimax=0;
for ($i=1; $i<=4; $i++) { 
$cimax= $cimax < $ciseg0x[$i] ? $ciseg0x[$i]: $cimax;
$cimax= $cimax < $ciseg1x[$i] ? $ciseg1x[$i]: $cimax;
$cimax= $cimax < $ciseg2x[$i] ? $ciseg2x[$i]: $cimax;
$cimax= $cimax < $ciseg3x[$i] ? $ciseg3x[$i]: $cimax;
$cimax= $cimax < $ciseg4x[$i] ? $ciseg4x[$i]: $cimax;
}
#
# use tmult times for forext to determine segmentation
#
$ciseg4x= four_external(1,1,int(0.1*$ncpu),int(0.1*$ncpu));


print "---------------------------------------final results------------------------------------------------------\n";

print "largest task=$LARGEST_TASK\n";
$bsum=0;
foreach $i  (sort msort keys(%buckets)) { $bsum+= $buckets{$i}; printf "BUCKETS: size= %8.5f  number: %5d \n", 2**($i),$buckets{$i};}
print "total number of tasks=",$bsum," plus ",2+int(0.1*$ncpu)*2," four-external and diagonal tasks.\n";
print "largest task approximately ",int($LARGEST_TASK*$maxtask*100)," % of the total time per CI iteration on $ncpu nodes \n";

print "maximum CI segment sizes:\n";
print "all-internal: ",join(' ',@$ciseg0x),"\n";
print "one-external: ",join(' ',@$ciseg1x),"\n";
print "two-external: ",join(' ',@$ciseg2x),"\n";
print "three-external: ",join(' ',@$ciseg3x),"\n";
print "four-external: ",join(' ',@$ciseg4x),"\n";
print "maximum CI segment in total= $cimax \n";

print "------------------------------------------------------------------------------------------------------------------------------\n";
printf "Volume 0-external :  %9.3f %9.3f %9.3f %9.3f                     (zz,yy,xx,ww)       total= %9.3f \n", $vol0x[0],$vol0x[1],$vol0x[2],$vol0x[3],
   $vol0x[0]+$vol0x[1]+$vol0x[2]+$vol0x[3];
printf "Volume 1-external :  %9.3f %9.3f %9.3f                               (yz,yx,yw)          total= %9.3f \n", $vol1x[0],$vol1x[1],$vol1x[2],
   $vol1x[1]+$vol1x[1]+$vol1x[2];
printf "Volume 2-external :  %9.3f %9.3f %9.3f %9.3f %9.3f %9.3f (xz,wz,yy,xx,ww,wx) total= %9.3f \n", $vol2x[0],$vol2x[1],$vol2x[2],$vol2x[3],$vol2x[4],$vol2x[5],
   $vol2x[0]+$vol2x[1]+$vol2x[2]+$vol2x[3]+$vol2x[4]+$vol2x[5];
printf "Volume 3-external :  %9.3f %9.3f                                         (yx,yw)             total= %9.3f \n", $vol3x[0],$vol3x[1], $vol3x[1]+$vol3x[1];
printf "Volume 4-ext+diag :  %9.3f %9.3f %9.3f %9.3f                     (z,y,x,w)           total= %9.3f \n", $vol4x[0],$vol4x[1],$vol4x[2],$vol4x[3],
   $vol4x[0]+$vol4x[1]+$vol4x[2]+$vol4x[3];
print "------------------------------------------------------------------------------------------------------------------------------\n";
printf " total Volume (sigma & CI vector transfer)                                                               %12.3f GB \n", 
 $vol0x[0]+$vol0x[1]+$vol0x[2]+$vol0x[3]+$vol1x[1]+$vol1x[1]+$vol1x[2]+$vol2x[0]+$vol2x[1]+$vol2x[2]+$vol2x[3]+$vol2x[4]+$vol2x[5]+
 $vol3x[1]+$vol3x[1]+$vol4x[0]+$vol4x[1]+$vol4x[2]+$vol4x[3] ;


#
# kodieren wir  als namelist :  zerotasks (i) = tasktype . brasegment . ketsegment  
#


if ($zero_task) 
 {

print " -----------------------------------  ZERO Tasks --------------------------------------------------------------\n";

{ my ($tmpc,$tasktype,$tmp1,$tmp2,$tmp3,$tmp4,$bra,$ket,$tmp5,@tmpci);
 $tmpc=""; @tmpci=();
for ($i=0; $i<=$#zero_task; $i++) {printf " %4d  %-60s \n", $i, $zero_task[$i]; 
                                  push @tmpci,$tmpc; 
                                   $tmpc=$zero_task[$i]; $tmpc=~s/^ *//; $tmpc=~s/=>//; $tmpc=~s/://; 
                                   ($tasktype,$tmp1,$tmp2,$tmp3,$tmp4,$bra,$ket,$tmp5)=split(/\s+/,$tmpc);
                                   if ($tasktype eq "YY_2x") {$tmpc = ($ket+($bra+21000)*1000);next;} 
                                   if ($tasktype eq "XX_2x") {$tmpc = ($ket+($bra+22000)*1000);next;} 
                                   if ($tasktype eq "WW_2x") {$tmpc = ($ket+($bra+23000)*1000);next;} 
                                   if ($tasktype eq "XZ_2x") {$tmpc = ($ket+($bra+24000)*1000);next;} 
                                   if ($tasktype eq "WZ_2x") {$tmpc = ($ket+($bra+25000)*1000);next;} 
                                   if ($tasktype eq "WX_2x") {$tmpc = ($ket+($bra+26000)*1000);next;} 
                                   if ($tasktype eq "YZ_1x") {$tmpc = ($ket+($bra+11000)*1000);next;} 
                                   if ($tasktype eq "YX_1x") {$tmpc = ($bra+($ket+12000)*1000);next;}
                                   if ($tasktype eq "YW_1x") {$tmpc = ($bra+($ket+13000)*1000);next;}
                                   if ($tasktype eq "YX_3x") {$tmpc = ($bra+($ket+31000)*1000);next;}
                                   if ($tasktype eq "YW_3x") {$tmpc = ($bra+($ket+32000)*1000);next;}
                                   if ($tasktype eq "ZZ_0x") {$tmpc = ($ket+($bra+1000)*1000);next;}
                                   if ($tasktype eq "YY_0x") {$tmpc = ($ket+($bra+2000)*1000);next;}
                                   if ($tasktype eq "XX_0x") {$tmpc = ($ket+($bra+3000)*1000);next;}
                                   if ($tasktype eq "WW_0x") {$tmpc = ($ket+($bra+4000)*1000);next;}
                                   }
 push @tmpci,$tmpc;
 shift @tmpci; 
 sort msort @tmpci;
  $tmpc=" zerotasks=" . join(',',@tmpci) . "\n";
 push @ciudgin,$tmpc; 
}
print " --------------------------------------------------------------------------------------------------------------\n";
}
print "number of zero_tasks=$zero_task\n";

print "*******************************************************\n";
print "add to ciudgin: \n ";

 while ( $_= shift @ciudgin )
{ chop; my @tmp=split(',',$_);
  my $ttmp=shift @tmp;
  for ($i=0; $i<=$#tmp; $i++) 
   { if (length($ttmp)>80) {print " $ttmp\n"; $ttmp=" ";}
               else {$ttmp.= ',' . $tmp[$i];}
   }
  if ( grep(/\S/,$ttmp) ){ print " $ttmp\n";} 
}
  

print "*******************************************************\n";

exit;

 sub msort 
 { 
   if ($a<$b) { return -1; }
   else {return  1;}
 }


 sub patchij_lt
 { my ($ilo,$ihi,$jlo,$jhi,$array)=@_;
   my ($b,$i,$j,$val,$jj);
   $val=0;
   for ($i=$ilo;$i<=$ihi;$i++)
    { if ($jhi>$i) { $jj=$i;} else {$jj=$jhi;}
      for ($j=$jlo; $j<=$jj; $j++)
     { $val+=$$array[$i][$j];}}
    { return $val;}

   return $val;
} 


 sub volij_lt
 { my ($ilo,$ihi,$jlo,$jhi,$csf)=@_;
   my ($b,$i,$j,$vol,$jj);
#
#  a task is associated  v1 (ilo:ihi)  v2(jlo:jhi) 
#
   $vol=0;
   for ($i=$ilo;$i<=$ihi;$i++) { $vol+= $$csf[$i]*8e-9;}
   if ($jlo != $ilo || $jhi != $ihi ) 
    { for ($i=$jlo;$i<=$jhi;$i++) { $vol+= $$csf[$i]*8e-9;}}

   $vol=$vol+$vol;
    return $vol;
}

 sub volij
 { my ($ilo,$ihi,$jlo,$jhi,$icsf,$jcsf)=@_;
   my ($b,$i,$j,$vol,$jj);
#
#  a task is associated  v1 (ilo:ihi)  v2(jlo:jhi)
#
   $vol=0;
   for ($i=$ilo;$i<=$ihi;$i++) { $vol+= $$icsf[$i]*8e-9;}
    { for ($i=$jlo;$i<=$jhi;$i++) { $vol+= $$jcsf[$i]*8e-9;}}
   $vol=$vol+$vol;
    return $vol;
}




 sub patchij
 { my ($ilo,$ihi,$jlo,$jhi,$array)=@_;
   my ($b,$i,$j,$val,$jj);
   $val=0;
   for ($i=$ilo;$i<=$ihi;$i++)
    { 
      for ($j=$jlo; $j<=$jhi; $j++)
     { $val+=$$array[$i][$j];}}
    { return $val;}

   return $val;
}



sub patchsize_lt
 { my ($start,$end,$array,$csf)=@_;
    my($i,$j,$memory);
    my $val=0;
    $memory=0;
    for ($i=$start; $i<=$end; $i++)
      { $memory+= $$csf[$i]; {for ($j=$start; $j<=$i; $j++) { $val+=$$array[$i][$j];
                                       }} }
    return ($memory,$val);}

 

sub getdata
{ my($segtype,$pairtype,$array,$ibatch,$jbatch)=@_;
 my ($tmp,$s,$p,$ilo,$ihi,$jlo,$jhi,$val,$i,$j);  

#
#  IN:  $segtype = 0x,1x,2x,3x,4x,dg
#       $pairtype = zz,yy,xx,ww,...
#       $ibatch,$jbatch = batchsize i,j
#  OUT: $array[i][j] :  #loops for batch (i,j)
#  RETURN: ($maxi,$maxj) : number of batches for column and rows
#


open FIN,"<SEGANALYSE";
while (<FIN>)
  {  if (/BEGIN ANALYSIS FOR/) { chop;s/^.*FOR//; $tmp=$_; 
                              s/^ *//; s/ .*$//; ($s,$p)=split(/_/,$_);
                           if ($s eq $segtype && $p eq $pairtype)
                               {print "reading  $s,$p  .....\n";
                                $maxi=0; $maxj=0;
                                while (<FIN>)
                                { if (/SUM OF/) {last;}
                                  chop; s/[\[\.,\]=]/ /g; s/^ *//;
                                  ($ilo,$ihi,$jlo,$jhi,$val)=split(/\s+/,$_);
                                  $i=int($ihi/$ibatch+0.5); $j=int($jhi/$jbatch+0.5);
                                  $$array[$i][$j]=$val;
                                  if ($i > $maxi) { $maxi=$i;}
                                  if ($j > $maxj) { $maxj=$j;}
                               #print "P( $i,$j ) = $val\n";
                                }
                              }
                           next;}
  }
 close FIN;

 $zero=0;
 open TEMP,">$segtype.$pairtype.scilab";
  for ($j=1; $j <= $maxj; $j++) { for ($i=1; $i<= $maxi; $i++) { if (! $$array[$i][$j]) {$zero++;} 
                                                                 printf TEMP "%10d  ",$$array[$i][$j]  ;} print TEMP "\n"; }
 print TEMP "\n";
 close TEMP;
  
 print "SCILAB: $segtype.$pairtype  maxij=$maxj,$maxi   zeroes=$zero (",$zero*100/($maxj*$maxi)," %)\n";

 return ($maxi,$maxj);
}


sub printdata
{ my($header,$array,$ncols,$nrows,$full)=@_;
 my ($ncol,$irow,$icol,$ic,$ir,$outstring,$upc,$upr,$full);  
 $ncol=10;

  print "                       ",uc($header)," ( $nrows * $ncols )\n";
 
  for ($irow=1;  $irow < $nrows; $irow+=$ncol )
   { for ($icol=1; $icol < $ncols; $icol+=$ncol )
      { # print patch [($irow-1)*$ncol+1:min($irow*$ncol,$nrows), ($icol-1)*$ncol:min($icol*$ncol,$ncols)]
        if (( ! $full ) && $icol>$irow ) {next;}
        $outstring= "  ";
        $upc=$icol+$ncol-1 > $ncols ? $ncols : $icol+$ncol-1;
        $upr=$irow+$ncol-1 > $nrows ? $nrows : $irow+$ncol-1;
        for ($ic=$icol; $ic<=$upc; $ic++) { $outstring .= sprintf "%8d ",$ic;}
        print "\n $outstring\n    -------------------------------------------------------------------------------------------\n";
        for ($ir=$irow; $ir<=$upr; $ir++)
         { $outstring= sprintf "%3d |", $ir;
           for ($ic=$icol;$ic<=$upc ; $ic++)
            { $outstring .= sprintf "%8d ",$$array[$ir][$ic];}
            print "$outstring \n";
         }
      }
   }
  return;
  }

 
 



 sub summe 
 { my ($maxi,$maxj,$array,$quad)=@_;
   my ($i,$j,$jj);
  my $sum=0;
  my $lt = ! $quad; 
 for ($i=1; $i<=$maxi; $i++)
  { if ($lt) { $jj=$i;} else {$jj=$maxj;}
    for ($j=1; $j<=$jj; $j++) { $sum+=$$array[$i][$j];}
  }
  return $sum;
} 



sub comp_borders
{ my ($maxi,$maxj,$avtask,$array,$csf)=@_;
  my @border=();
  my $lt = ($maxi == $maxj);
  my ($start,$value,$i,$memory);
 $start=1;
 push @border,1;
 $i=0;
  while ( $i <$maxi )
 {  $i++; if ($lt) {($memory,$value)=patchsize_lt($start,$i,$array,$csf);}
             else  {die("patchsize only for lt");}
#    print "value,memory,maxmem=$value,$memory,$maxmem\n"; 

    if ($value > $avtask*(1+$fraction) || $memory > $maxmem ) {
                            if ($i > $start+1) {push @border, $i; $start=$i;} else {push @border, $start+1; $start++;}
                            $i--; $i--;
                            }
 }
#
#  eliminate queer tasks
#
   my $tmp=pop @border;
   if ($tmp < $maxi) { push @border,$tmp;}
   push @border,$maxi+1;
   return @border;
}


 sub dscal
 { my ($val,$array)=@_;
   my $i;
   for ($i=0;$i<=$#$array;$i++) { $$array[$i]=$val*($$array[$i]-1)+1;}
   return;
  }

 sub ddscal
 { my ($val,$array,$dim1,$dim2)=@_;
   my ($i,$j);
   for ($i=1;$i<=$dim1;$i++) {
         for ($j=1; $j<=$dim2;$j++) { $$array[$i][$j]*=$val}}
   return;
  }

 

sub make_buckets_lt
{ my($border,$avtask,$buckets,$array,$txt,$csf)=@_;
  my ($i,$j,$jj,$checksum,$b);
  $checksum=0;
 for ($i=0; $i<$#$border; $i++)
  { for ($j=0; $j<=$i; $j++)
    { 
      $val=patchij_lt($$border[$i],$$border[$i+1]-1,$$border[$j],$$border[$j+1]-1,$array);
      $checksum+=$val;
      $b=$val/$avtask;
      if ( ! $b) {$zero_task++; my $tmp =sprintf "%-15s %5d %5d :  %5d %5d  => %5d %5d ", $txt,$$border[$i],$$border[$i+1]-1,$$border[$j],$$border[$j+1]-1, $i+1,$j+1; 
                  push @zero_task,$tmp ; next;}
      $VOLUME+=volij_lt($$border[$i],$$border[$i+1]-1,$$border[$j],$$border[$j+1]-1,$csf);
      $LARGEST_TASK= $b > $LARGEST_TASK ? $b : $LARGEST_TASK; 
      if ($prtlevel > 1) { printf "$txt: [  %3d:%3d,%3d:%3d ] %8.3f \n", $$border[$i],$$border[$i+1]-1,$$border[$j],$$border[$j+1]-1 , $b, $VOLUME ;}
      $b=int(log($b)/log(2));$$buckets{"$b"}=$$buckets{"$b"}+1;
   }}
   return $checksum;
}

sub make_buckets_q
{ my($borderi, $borderj,$avtask,$buckets,$array,$txt,$icsf,$jcsf)=@_;
  my ($i,$j,$jj,$checksum,$b);
  $checksum=0;
 for ($i=0; $i<$#$borderi; $i++)
  { 
    for ($j=0; $j<$#$borderj; $j++)
    {
       $val=patchij($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$array);
      $checksum+=$val;
      $b=$val/$avtask;
      if ( ! $b) {$zero_task++; my $tmp =sprintf "%-15s %5d %5d :  %5d %5d  => %5d %5d ", $txt,$$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$i+1,$j+1; 
                  push @zero_task,$tmp ; next;}
       $VOLUME+=volij($$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1,$icsf,$jcsf);
      $LARGEST_TASK= $b > $LARGEST_TASK ? $b : $LARGEST_TASK; 
      if ($prtlevel > 1) { printf "$txt: [  %3d:%3d,%3d:%3d ] %8.3f \n", $$borderi[$i],$$borderi[$i+1]-1,$$borderj[$j],$$borderj[$j+1]-1 , $b,$VOLUME ;}
        $b=int(log($b)/log(2));$$buckets{"$b"}=$$buckets{"$b"}+1;
  }}
   return $checksum;
}




 

   sub compseg {
   my ($nval1)=@_;
   my ($minseg,$maxwalk,$iseg1,$iseg2,$isize);
       
   $maxwalk = $nval1;
   $minseg=int(($maxwalk-1)/75)+1;
   $isize = $minseg > 100 ? $minseg : 100;
   $iseg1=int(($nval1-1)/$isize)+1;
   return $iseg1;
}
   

   sub compsize {
   my ($nval1)=@_;
   my ($minseg,$maxwalk,$iseg1,$iseg2,$isize);

   $maxwalk = $nval1; 
   $minseg=int(($maxwalk-1)/75)+1;
   $isize = $minseg > 100 ? $minseg : 100;
   return $isize;
}



    sub allinternal {
 my ($avtask,$sum) =@_;

print "sum of all 0external tasks=$sum   minimum number of tasks=$tottask \n";
print "average task size: $avtask,", ($sum/$tottask)," \n";


 # generate bounderies that may be used to define tasks
 # such that no task takes more than the average task size and
 # has at least the minimum size of a task.
 # possibly subject to refinement
# gibt die startgrenzen an, 1. Wert 1, letzter Wert 51

 @borders_zz = comp_borders($zseg,$zseg,$avtask,\@zz_0x,\@csfbatch_z);
 @borders_yy = comp_borders($yseg,$yseg,$avtask,\@yy_0x,\@csfbatch_y);
 @borders_xx = comp_borders($xseg,$xseg,$avtask,\@xx_0x,\@csfbatch_x);
 @borders_ww = comp_borders($wseg,$wseg,$avtask,\@ww_0x,\@csfbatch_w);

 if ($prtlevel > 3) {
   print  "elementary starttasks zz =",join(':',@borders_zz), "\n";
   print  "elementary starttasks yy=",join(':',@borders_yy), "\n";
   print  "elementary starttasks xx=",join(':',@borders_xx), "\n";
   print  "elementary starttasks ww=",join(':',@borders_ww), "\n";
  }


#
# compute the distribution of tasks
# and print it
#
 
# %buckets=();

$checksum=0;
$VOLUME=0;
$checksum+= make_buckets_lt(\@borders_zz,$avtask,\%buckets,\@zz_0x,"ZZ_0x",\@csfbatch_z);
$vol0x[0]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_lt(\@borders_yy,$avtask,\%buckets,\@yy_0x,"YY_0x",\@csfbatch_y);
$vol0x[1]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_lt(\@borders_xx,$avtask,\%buckets,\@xx_0x,"XX_0x",\@csfbatch_x);
$vol0x[2]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_lt(\@borders_ww,$avtask,\%buckets,\@ww_0x,"WW_0x",\@csfbatch_w);
$vol0x[3]=$VOLUME; $VOLUME=0;

print "VOLUME (GB) all-internal (zz,yy,xx,ww): ",join(' ',@vol0x)," \n"; 

 $ciseg0x[1] = comp_maxciseg(\@borders_zz,\@csfbatch_z);
 $ciseg0x[2] = comp_maxciseg(\@borders_yy,\@csfbatch_y);
 $ciseg0x[3] = comp_maxciseg(\@borders_xx,\@csfbatch_x);
 $ciseg0x[4] = comp_maxciseg(\@borders_ww,\@csfbatch_w);

($nseg0x,$nnseg0x)=createciudgin(\@borders_zz,\@borders_yy,\@borders_xx,\@borders_ww);
 push @ciudgin,"nseg0x=" . join(',',@$nseg0x) . "\n";
 push @ciudgin,"nnseg0=" . join(',',@$nnseg0x) . "\n";

 

  return \@ciseg0x;
}


sub comp_borderq
{ my ($maxi,$maxj,$avtask,$array,$row,$fborders,$csf)=@_;
  my @rborder=();
  my @cborder=();
  my ($start,$value,$i);


#
#  if ($row) :  the matrix is row-wise dissected
#               $minval indicates dissection on cols
#  if (! $row) :  the matrix is col-wise dissected
#               $minval indicates dissection on rows
#


if ($row) 
    { # first pass: dissect rows 
      # compute borders for col dissection
#       push @cborder,1;
#       $jtmp=1;
#       for ($i=1; $i< $minval; $i++ ) { $jtmp+= int( ($maxj-1)/$minval); push @cborder,$jtmp;}
#       push @cborder,$maxj;

      $start=1;
      push @rborder,1;
      $i=0;
      @value=(); $memory=0;
      while ( $i <$maxi )
      {  $i++; 
          # print "evaluating row $i,$maxi,$maxj\n";
          $toolarge=0;
          $memory+=$$csf[$i];
         for ($j=1; $j<=$#$fborders; $j++ ) { $value[$j]+=patch_row($i,$$fborders[$j-1],$$fborders[$j],$array);
                                          if ($value[$j] > $avtask*(1+$fraction) || $memory > $maxmem ) {$toolarge=1;}}
    if ($toolarge ) { if ($i > $start+1) { push @rborder, $i; $start=$i;} else {push @rborder, $start+1; $start++;}
                            $i=$start-1; @value=();$memory=0 ;
                            }
        }

    my $tmp=pop @rborder;
   if ($tmp < $maxi) { push @rborder,$tmp;}
    push @rborder,$maxi+1;
#    print "row borders: ", $#rborder, "sections",  join(':',@rborder), "\n";
#    print "enforced col borders: ", $#$fborders, "sections",  join(':',@$fborders), "\n";
    return (\@rborder,\@fborder);
   }

 else

    { # first pass: dissect cols
      # compute borders for row dissection
#       push @rborder,1;
#       $jtmp=1;
#       for ($i=1; $i< $minval; $i++ ) { $jtmp+= int( ($maxi-1)/$minval); push @rborder,$jtmp;}
#       push @rborder,$maxi;

      $start=1;
      push @cborder,1;
      $i=0;
      @value=();$memory=0; 
      while ( $i <$maxj )
      {  $i++;
          # print "evaluating row $i,$maxi,$maxj\n";
          $toolarge=0;
         $memory+=$$csf[$i];
         for ($j=1; $j<=$#$fborders; $j++ ) { $value[$j]+=patch_col($i,$$fborders[$j-1],$$fborders[$j],$array);
                                          if ($value[$j] > $avtask*(1+$fraction)  || $memory > $maxmem ) {$toolarge=1;}}
    if ($toolarge ) { if ($i > $start+1) { push @cborder, $i; $start=$i;} else {push @cborder, $start+1; $start++;}
                            $i=$start-1; @value=(); $memory=0;
                            }
        }
    my $tmp=pop @cborder;
   if ($tmp < $maxj) { push @cborder,$tmp;}
    push @cborder,$maxi+1;
#    print "col borders: ", $#cborder, "sections",  join(':',@cborder), "\n";
#    print "enforced row borders: ", $#$fborders, "sections",  join(':',@$fborders), "\n";
    return (\@cborder,\@fborder);
   }
return;
}


sub patch_row 
{ my ($i,$mincol,$maxcol,$array) =@_;
  my $val=0;
  my $ii;
  for ($ii=$mincol-1; $ii<$maxcol; $ii++) {  $val+=$$array[$i][$ii];}
  return $val;
}


 
sub patch_col
{ my ($i,$minrow,$maxrow,$array) =@_;
  my $val=0;
  my $ii;

  for ($ii=$minrow; $ii<=$maxrow; $ii++) { $val+=$$array[$ii][$i];}
  return $val;
}

sub patch_mem
{ my ($i,$mincol,$maxcol,$csf) =@_;
  my $val=0;
  my $ii;
  for ($ii=$mincol-1; $ii<$maxcol; $ii++) {  $val+=$$csf[$ii];}
  return $val;
}


  
 sub one_external
 {
 my ($avtask,$sum) =@_;
 
################## SEGMENTATION OF THE ONE-EXTERNAL CASE #################

print "sum of all 1external tasks=$sum   minimum number of tasks=$tottask \n";
print "average task size: $avtask,", ($sum/$tottask)," \n";


 # generate bounderies that may be used to define tasks
 # such that no task takes more than the average task size and
 # has at least the minimum size of a task.
 # possibly subject to refinement 
# gibt die startgrenzen an, 1. Wert 1, letzter Wert 51


 # step 1: choose a segmentation such that $zseg=n, $yseg=1

 $ytmp=1; 
 while ( 1 ) 
 { 
  @borders_yy=();
  push @borders_yy,1;
  $cnt=0;
  $mem=0;
  for ($i=1; $i<$yseg; $i++)
   { $cnt++;
     $mem+=$csfbatch_y[$i];
     if ($cnt > int($yseg/$ytmp) || $mem > $maxmem )
       { $cnt=0; $mem=0; push @borders_yy,$i;}
   }
  if ($cnt) { push @borders_yy,$yseg+1;}

   ($borders_zz,$tmp)=comp_borderq($zseg,$yseg,$avtask,\@yz_1x,1,\@borders_yy,\@csfbatch_z);
   ($borders_xx,$tmp)=comp_borderq($yseg,$xseg,$avtask,\@yx_1x,0,\@borders_yy,\@csfbatch_x);
   ($borders_ww,$tmp)=comp_borderq($yseg,$xseg,$avtask,\@yw_1x,0,\@borders_yy,\@csfbatch_w);

 if ($#$borders_ww > 4*$#borders_yy || $#$borders_xx > 4*$#borders_yy || $#$borders_zz > 4*$#borders_yy ) 
  { $ytmp++;  print "increasing one-ext yseg to $ytmp:   $#$borders_zz $#borders_yy, $#$borders_xx $#$borders_ww\n"; next;}
   last;
 }

 


 if ($prtlevel > 1) {
   print  "elementary starttasks 1ex z =",join(':',@$borders_zz), "\n";
   print  "elementary starttasks 1ex y =",join(':',@borders_yy), "\n";
   print  "elementary starttasks 1ex x =",join(':',@$borders_xx), "\n";
   print  "elementary starttasks 1ex w =",join(':',@$borders_ww), "\n";
  }


#
# compute the distribution of tasks
# and print it
#


$checksum=0;
$VOLUME=0;
$checksum+= make_buckets_q($borders_zz,\@borders_yy,$avtask,\%buckets,\@yz_1x,"YZ_1x",\@csfbatch_z,\@csfbatch_y);
$vol1x[0]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_q(\@borders_yy,$borders_xx,$avtask,\%buckets,\@yx_1x,"YX_1x",\@csfbatch_y,\@csfbatch_x);
$vol1x[1]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_q(\@borders_yy,$borders_ww,$avtask,\%buckets,\@yw_1x,"YW_1x",\@csfbatch_y,\@csfbatch_w);
$vol1x[2]=$VOLUME; $VOLUME=0;

print "VOLUME (GB) one-external yz,yx,yw:",join(' ',@vol1x)," \n";


 $ciseg1x[1] = comp_maxciseg($borders_zz,\@csfbatch_z);
 $ciseg1x[2] = comp_maxciseg(\@borders_yy,\@csfbatch_y);
 $ciseg1x[3] = comp_maxciseg($borders_xx,\@csfbatch_x);
 $ciseg1x[4] = comp_maxciseg($borders_ww,\@csfbatch_w);


($nseg1x,$nnseg1x)=createciudgin($borders_zz,\@borders_yy,$borders_xx,$borders_ww);

 push @ciudgin,"nseg1x=" . join(',',@$nseg1x) . "\n";
 push @ciudgin,"nnseg1=" . join(',',@$nnseg1x) . "\n";
 return \@ciseg1x;
}

  sub two_external
 {

 my ($avtask,$sum) =@_;

print "sum of all 2external tasks=$sum   minimum number of tasks=$tottask \n";
print "average task size: $avtask,", ($sum/$tottask)," \n";


 # generate bounderies that may be used to define tasks
 # such that no task takes more than the average task size and
 # has at least the minimum size of a task.
 # possibly subject to refinement

 # choose segmentation just as for the all-internal case
 # for yy,xx,ww

 @borders_yy = comp_borders($yseg,$yseg,$avtask,\@yy_2x,\@csfbatch_y);
 @borders_xx = comp_borders($xseg,$xseg,$avtask,\@xx_2x,\@csfbatch_x);
 @borders_ww = comp_borders($wseg,$wseg,$avtask,\@ww_2x,\@csfbatch_w);

 # determine z from wz or xz

 ($borders_zz,$tmp)=comp_borderq($wseg,$zseg,$avtask,\@wz_2x,0,\@borders_ww,\@csfbatch_z);
 ($borders_zz2,$tmp)=comp_borderq($wseg,$zseg,$avtask,\@wz_2x,0,\@borders_xx,\@csfbatch_z);
 if ($#$borders_zz2 > $#$borders_zz) 
  {($borders_zz,$tmp)=comp_borderq($wseg,$zseg,$avtask,\@wz_2x,0,\@borders_xx,\@csfbatch_z);}


 # determine wx partitioning

# if ($#$borders_ww > $#$borders_xx) 
# { ($borders_xx_wx,$tmp)=comp_borderq($xseg,$wseg,$avtask,\@wx_2x,1,\@borders_ww);
#    $borders_ww_wx=\@borders_ww;}
# else
# { ($borders_ww_wx,$tmp)=comp_borderq($xseg,$wseg,$avtask,\@wx_2x,0,\@borders_xx);
#   $borders_xx_wx=\@borders_xx;}


if ($#$borders_xx < $#$borders_ww ) 
 {
 $xtmp=1; 
 while ( 1 ) 
 { 
    @borders_xx_wx=(); push @borders_xx_wx,1;
   for ($i=1; $i<= $xtmp-1; $i++) {push @borders_xx_wx, int(($xseg-1)/$xtmp)*$i; }
   push @borders_xx_wx,$xseg+1;
   ($borders_ww_wx,$tmp)=comp_borderq($xseg,$wseg,$avtask,\@wx_2x,0,\@borders_xx_wx,\@csfbatch_w);
 if ($#$borders_ww_wx > 3*$#borders_xx_wx  )
  { $xtmp++;  print "increasing two-ext wx xseg to $xtmp\n"; next;}
   last;
 }
  $borders_xx_wx=\@borders_xx_wx;
 }
else
 {
 $wtmp=1;
 while ( 1 )
 { 
    @borders_ww_wx=(); push @borders_ww_wx,1;
   for ($i=1; $i<= $wtmp-1; $i++) {push @borders_ww_wx, int(($wseg-1)/$wtmp)*$i; }
   push @borders_ww_wx,$wseg+1;
   ($borders_xx_wx,$tmp)=comp_borderq($xseg,$wseg,$avtask,\@wx_2x,1,\@borders_ww_wx,\@csfbatch_x);
 if ($#$borders_xx_wx > 3*$#borders_ww_wx  )
  { $wtmp++;  print "increasing two-ext wx wseg to $wtmp\n"; next;}
   last;
 }
  $borders_ww_wx=\@borders_ww_wx;
 }





 if ($prtlevel > 1) {
   print  "elementary starttasks 2ex z =",join(':',@$borders_zz), "\n";
   print  "elementary starttasks 2ex y =",join(':',@borders_yy), "\n";
   print  "elementary starttasks 2ex x =",join(':',@borders_xx), "\n";
   print  "elementary starttasks 2ex w =",join(':',@borders_ww), "\n";
   print  "elementary starttasks 2exwx w =",join(':',@$borders_ww_wx), "\n";
   print  "elementary starttasks 2exwx x =",join(':',@$borders_xx_wx), "\n";
  }




#
# compute the distribution of tasks
# and print it
# 


$checksum=0;
$VOLUME=0;
$checksum+= make_buckets_q(\@borders_ww,$borders_zz,$avtask,\%buckets,\@wz_2x,"WZ_2x",\@csfbatch_w,\@csfbatch_z);
$vol2x[0]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_q(\@borders_xx,$borders_zz,$avtask,\%buckets,\@xz_2x,"XZ_2x",\@csfbatch_x,\@csfbatch_z);
$vol2x[1]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_lt(\@borders_yy,$avtask,\%buckets,\@yy_2x,"YY_2x",\@csfbatch_y);
$vol2x[2]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_lt(\@borders_xx,$avtask,\%buckets,\@xx_2x,"XX_2x",\@csfbatch_x);
$vol2x[3]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_lt(\@borders_ww,$avtask,\%buckets,\@ww_2x,"WW_2x",\@csfbatch_w);
$vol2x[4]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_q($borders_ww_wx,$borders_xx_wx,$avtask,\%buckets,\@wx_2x,"WX_2x",\@csfbatch_w,\@csfbatch_x);
$vol2x[5]=$VOLUME; $VOLUME=0;

print "VOLUME (GB) two-external wz,xz,yy,xx,ww,wx:",join(' ',@vol2x)," \n";


 $ciseg2x[1] = comp_maxciseg($borders_zz,\@csfbatch_z);
 $ciseg2x[2] = comp_maxciseg(\@borders_yy,\@csfbatch_y);
 $ciseg2x[3] = comp_maxciseg(\@borders_xx,\@csfbatch_x);
 $ciseg2x[4] = comp_maxciseg(\@borders_ww,\@csfbatch_w);
 $ciseg2x[5] = comp_maxciseg($borders_xx_wx,\@csfbatch_x);
 $ciseg2x[6] = comp_maxciseg($borders_ww_wx,\@csfbatch_w);


($nseg2x,$nnseg2x)=createciudgin($borders_zz,\@borders_yy,\@borders_xx,\@borders_ww);
($nseg2xwx,$nnseg2xwx)=createciudgin($borders_zz,\@borders_yy,$borders_xx_wx,$borders_ww_wx);

 push @ciudgin,"nseg2x=" . join(',',@$nseg2x) . "\n";
 push @ciudgin,"nnseg2=" . join(',',@$nnseg2x) . "\n";
 push @ciudgin,"nsegwx=" . join(',',@$nseg2xwx) . "\n";
 push @ciudgin,"nnsegwx=" . join(',',@$nnseg2xwx) . "\n";

 return \@ciseg2x;
}



  sub three_external
 {
 my ($avtask,$sum) =@_;


print "sum of all 3external tasks=$sum   minimum number of tasks=$tottask \n";
print "average task size: $avtask,", ($sum/$tottask)," \n";


 # generate bounderies that may be used to define tasks
 # such that no task takes more than the average task size and
 # has at least the minimum size of a task.
 # possibly subject to refinement

 # choose segmentation just as for the all-internal case
 # choose yseg=1

 $borders_zz[0]=1;
 $borders_zz[1]=$zseg+1;
 $ytmp=1;
 while ( 1 )
 { @borders_yy=();
   push @borders_yy,1;
   for ($i=1; $i<= $ytmp-1; $i++) {push @borders_yy, int(($yseg-1)/$ytmp)*$i; }
   push @borders_yy,$yseg+1;
($borders_ww,$tmp)=comp_borderq($yseg,$wseg,$avtask,\@yw_3x,0,\@borders_yy,\@csfbatch_w);
($borders_xx,$tmp)=comp_borderq($yseg,$xseg,$avtask,\@yx_3x,0,\@borders_yy,\@csfbatch_x);
 if ($#$borders_ww > 4*$#borders_yy || $#$borders_xx > 4*$#borders_yy  )
  { $ytmp++;  print "increasing three-ext yseg to $ytmp\n"; next;}
   last;
 }





 if ($prtlevel > 1) {
   print  "elementary starttasks 3ex y =",join(':',@borders_yy), "\n";
   print  "elementary starttasks 3ex x =",join(':',@$borders_xx), "\n";
   print  "elementary starttasks 3ex w =",join(':',@$borders_ww), "\n";
  }




#
# compute the distribution of tasks
# and print it
# 


$checksum=0;
$VOLUME=0;
$checksum+= make_buckets_q(\@borders_yy,$borders_xx,$avtask,\%buckets,\@yx_3x,"YX_3x",\@csfbatch_y,\@csfbatch_x);
$vol3x[0]=$VOLUME; $VOLUME=0;
$checksum+= make_buckets_q(\@borders_yy,$borders_ww,$avtask,\%buckets,\@yw_3x,"YW_3x",\@csfbatch_y,\@csfbatch_w);
$vol3x[1]=$VOLUME; $VOLUME=0;

print "VOLUME (GB) three-external yx,yw =",join(' ',@vol3x),"\n";


 $ciseg3x[2] = comp_maxciseg(\@borders_yy,\@csfbatch_y);
 $ciseg3x[3] = comp_maxciseg($borders_xx,\@csfbatch_x);
 $ciseg3x[4] = comp_maxciseg($borders_ww,\@csfbatch_w);



($nseg3x,$nnseg3x)=createciudgin(\@borders_zz,\@borders_yy,$borders_xx,$borders_ww);

 push @ciudgin,"nseg3x=" . join(',',@$nseg3x) . "\n";
 push @ciudgin,"nnseg3=" . join(',',@$nnseg3x) . "\n";

 return \@ciseg3x;
}


  sub four_external
 {
 my ($nsegz,$nsegy,$nsegx,$nsegw) =@_;


 # generate bounderies that may be used to define tasks
 # such that no task takes more than the average task size and
 # has at least the minimum size of a task.
 # possibly subject to refinement

  @borders_zz=(); 
  push @borders_zz,1;
  $cnt=0;
  $mem=0;
  for ($i=1; $i<$zseg; $i++) 
   { $cnt++; 
     $mem+=$csfbatch_z[$i];
     if ($cnt > int($zseg/$nsegz) || $mem > $maxmem ) 
       { $cnt=0; $mem=0; push @borders_zz,$i;}
   }
  if  ($cnt) { push @borders_zz,$zseg+1;}

   for ($i=0; $i<$#borders_zz; $i++)
   { $vol4x[0]+=volij_lt($borders_zz[$i],$borders_zz[$i+1]-1,$borders_zz[$i],$borders_zz[$i+1]-1,\@csfbatch_z);}

  @borders_yy=(); 
  push @borders_yy,1;
  $cnt=0;
  $mem=0;
  for ($i=1; $i<$yseg; $i++) 
   { $cnt++;
     $mem+=$csfbatch_y[$i];
     if ($cnt > int($yseg/$nsegy) || $mem > $maxmem )
       { $cnt=0; $mem=0; push @borders_yy,$i;}
   }
  if ($cnt) { push @borders_yy,$yseg+1;}
   for ($i=0; $i<$#borders_yy; $i++)
   { $vol4x[1]+=volij_lt($borders_yy[$i],$borders_yy[$i+1]-1,$borders_yy[$i],$borders_yy[$i+1]-1,\@csfbatch_y);}

  @borders_xx=(); 
  push @borders_xx,1;
  $cnt=0;
  $mem=0;
  for ($i=1; $i<$xseg; $i++) 
   { $cnt++;
     $mem+=$csfbatch_x[$i];
     if ($cnt > int($xseg/$nsegx) || $mem > $maxmem )
       { $cnt=0; $mem=0; push @borders_xx,$i;}
   }
  if ($cnt) { push @borders_xx,$xseg+1;}
   for ($i=0; $i<$#borders_xx; $i++)
   { $vol4x[2]+=volij_lt($borders_xx[$i],$borders_xx[$i+1]-1,$borders_xx[$i],$borders_xx[$i+1]-1,\@csfbatch_x);}

      
  @borders_ww=(); 
  push @borders_ww,1;
  $cnt=0;
  $mem=0;
  for ($i=1; $i<$wseg; $i++) 
   { $cnt++;
     $mem+=$csfbatch_w[$i];
     if ($cnt > int($wseg/$nsegw) || $mem > $maxmem )
       { $cnt=0; $mem=0; push @borders_ww,$i;}
   }
  if ($cnt) { push @borders_ww,$wseg+1;}

   for ($i=0; $i<$#borders_ww; $i++)
   { $vol4x[3]+=volij_lt($borders_ww[$i],$borders_ww[$i+1]-1,$borders_ww[$i],$borders_ww[$i+1]-1,\@csfbatch_w);}


 $ciseg4x[1] = comp_maxciseg(\@borders_zz,\@csfbatch_z);
 $ciseg4x[2] = comp_maxciseg(\@borders_yy,\@csfbatch_y);
 $ciseg4x[3] = comp_maxciseg(\@borders_xx,\@csfbatch_x);
 $ciseg4x[4] = comp_maxciseg(\@borders_ww,\@csfbatch_w);

($nseg4x,$nnseg4x)=createciudgin(\@borders_zz,\@borders_yy,\@borders_xx,\@borders_ww);
 
#print "add to ciudgin .... \n";
#print "nseg4x=",join(',',@$nseg4x), "\n";
#print "nnseg4=",join(',',@$nnseg4x), "\n";
#print "nsegd=",join(',',@$nseg4x), "\n";
#print "nnsegdg=",join(',',@$nnseg4x), "\n";
#print "CDG4EX=1 \n";
 push @ciudgin,"nseg4x=" . join(',',@$nseg4x) . "\n";
 push @ciudgin,"nnseg4=" . join(',',@$nnseg4x) . "\n";
 push @ciudgin,"CDG4EX=1\n";

 return \@ciseg4x;
}




  sub computescaling
{
my ($tmodel)=@_;
my ($globfac,$scale_0xzz,$scale_0xyy,$scale_0xxx,$scale_0xww);
my ($sum,$scale_1xyz,$scale_1xyw,$scale_1xyz);
my ($scale_2xyy, $scale_2xxx,$scale_2xww,$scale_2xwx,$scale_2xwz,$scale_2xxz);
my ($scale_3xyx,$scale_3xyw);


#
#   all-internal
#
# experimentell fuer ethylene dz case
#
#          zz  yy   xx    ww 
#        1.2 1.7   7.5   7.5
# jump:  2.4  2.6  6.3   4.8
#

$globfac=0.001;
@sum=();

if ($tmodel) 
 { print "computing scaling factors from tmodel output\n";
   open FTMOD,"<$tmodel";
   while (<FTMOD>) { if (/total times per task type/) {last;}}
   $_=<FTMOD>; $_=<FTMOD>; 
   @tasktimes=();
   while (<FTMOD>)
   { if (/-------/) {last;}
     s/^ *//; chop; s/ *$//; 
     ($type,$mn1,$mn2,$time,$x1,$x2,$x3,$x4) = split (/\s+/,$_);
     $tasktimes[$type]=$time;
   } 
    close FTMOD;

   open FSEG,"<SEGANALYSE";
   @contrib=();
   while (<FSEG>) { if (/SUM OF ALL/) { $tmp=$_; $tmp=~s/^.*SUM OF ALL //; chop $tmp;  
                                        $tmp=~s/.*: *//; $tmp=~s/ *$//;
                                       if (/0x_zz/) {$contrib[1]=$tmp;} 
                                       if (/0x_yy/) {$contrib[2]=$tmp;} 
                                       if (/0x_xx/) {$contrib[3]=$tmp;} 
                                       if (/0x_ww/) {$contrib[4]=$tmp;} 
                                       if (/1x_yz/) {$contrib[11]=$tmp;} 
                                       if (/1x_yx/) {$contrib[13]=$tmp;} 
                                       if (/1x_yw/) {$contrib[14]=$tmp;} 
                                       if (/2x_yy/) {$contrib[21]=$tmp;} 
                                       if (/2x_xx/) {$contrib[22]=$tmp;} 
                                       if (/2x_ww/) {$contrib[23]=$tmp;} 
                                       if (/2x_xz/) {$contrib[24]=$tmp;} 
                                       if (/2x_wz/) {$contrib[25]=$tmp;} 
                                       if (/2x_wx/) {$contrib[26]=$tmp;} 
                                       if (/3x_yx/) {$contrib[31]=$tmp;} 
                                       if (/3x_yw/) {$contrib[32]=$tmp;} 
                                      }
                  }
    close FSEG;

    if ( $prtlevel > 1) { for ($i=1; $i<100; $i++) { if ($tasktimes[$i]) {print "Type: $i tasktimes $tasktimes[$i] sum $contrib[$i]\n";}}}
    $scale_0xzz=$tasktimes[1]*$globfac*1.e6/$contrib[1];
    $scale_0xyy=$tasktimes[2]*$globfac*1.e6/$contrib[2];
    $scale_0xxx=$tasktimes[3]*$globfac*1.e6/$contrib[3];
    $scale_0xww=$tasktimes[4]*$globfac*1.e6/$contrib[4];
    $scale_1xyz=$tasktimes[11]*$globfac*1.e6/$contrib[11];
    $scale_1xyx=$tasktimes[13]*$globfac*1.e6/$contrib[13];
    $scale_1xyw=$tasktimes[14]*$globfac*1.e6/$contrib[14];
    $scale_2xyy=$tasktimes[21]*$globfac*1.e6/$contrib[21];
    $scale_2xxx=$tasktimes[22]*$globfac*1.e6/$contrib[22];
    $scale_2xww=$tasktimes[23]*$globfac*1.e6/$contrib[23];
    $scale_2xxz=$tasktimes[24]*$globfac*1.e6/$contrib[24];
    $scale_2xwz=$tasktimes[25]*$globfac*1.e6/$contrib[25];
    $scale_2xwx=$tasktimes[26]*$globfac*1.e6/$contrib[26];
    $scale_3xyx=$tasktimes[31]*$globfac*1.e6/$contrib[31];
    $scale_3xyw=$tasktimes[32]*$globfac*1.e6/$contrib[32];

    if ($prtlevel > 0 ) 
    { print "computed relative scale factors :\n";
      print " 0x zz: ",$scale_0xzz/$globfac,"\n"; 
      print " 0x yy: ",$scale_0xyy/$globfac,"\n"; 
      print " 0x xx: ",$scale_0xxx/$globfac,"\n"; 
      print " 0x ww: ",$scale_0xww/$globfac,"\n"; 
      print " 1x yz: ",$scale_1xyz/$globfac,"\n"; 
      print " 1x yx: ",$scale_1xyx/$globfac,"\n"; 
      print " 1x yw: ",$scale_1xyw/$globfac,"\n"; 
      print " 2x yy: ",$scale_2xyy/$globfac,"\n"; 
      print " 2x xx: ",$scale_2xxx/$globfac,"\n"; 
      print " 2x ww: ",$scale_2xww/$globfac,"\n"; 
      print " 2x xz: ",$scale_2xxz/$globfac,"\n"; 
      print " 2x wz: ",$scale_2xwz/$globfac,"\n"; 
      print " 2x wx: ",$scale_2xwx/$globfac,"\n"; 
      print " 3x yx: ",$scale_3xyx/$globfac,"\n"; 
      print " 3x yw: ",$scale_3xyw/$globfac,"\n"; 
    }
  }

  else
  {
    $scale_0xzz=2.4 *$globfac;
    $scale_0xyy=2.6*$globfac;
    $scale_0xxx=6.3*$globfac;
    $scale_0xww=4.8*$globfac;
    $scale_1xyz=2.3*$globfac;
    $scale_1xyx=5.3*$globfac;
    $scale_1xyw=4.8*$globfac;
 
    $scale_2xyy=4.5*$globfac;
    $scale_2xxx=30*$globfac;
    $scale_2xww=30*$globfac;
    $scale_2xwx=30*$globfac; 
    $scale_2xwz=3.4*$globfac;
    $scale_2xxz=3.4*$globfac;
    $scale_3xyx=25*$globfac;
    $scale_3xyw=24*$globfac;
  }
  
 ddscal($scale_0xzz,\@zz_0x,$zseg,$zseg);
 ddscal($scale_0xyy,\@yy_0x,$yseg,$yseg);
 ddscal($scale_0xxx,\@xx_0x,$xseg,$xseg);
 ddscal($scale_0xww,\@ww_0x,$wseg,$wseg);
  

$sum=0;
$sum+=summe($zseg,$zseg,\@zz_0x,0);
$sum+=summe($yseg,$yseg,\@yy_0x,0);
$sum+=summe($xseg,$xseg,\@xx_0x,0);
$sum+=summe($wseg,$wseg,\@ww_0x,0); 

$sum[0]=$sum;

#
# one-external
# experimentell fuer ethylene dz case
# 
#  yz  (yw,yx) :  1.6  5.6
# jump 2.3 5.3 4.8 



 ddscal($scale_1xyz,\@yz_1x,$zseg,$yseg);
 ddscal($scale_1xyw,\@yw_1x,$yseg,$wseg);
 ddscal($scale_1xyx,\@yx_1x,$yseg,$xseg);
 

$sum=0;
$sum+=summe($zseg,$yseg,\@yz_1x,1);
$sum+=summe($yseg,$wseg,\@yw_1x,1);
$sum+=summe($yseg,$xseg,\@yx_1x,1);

$sum[1]=$sum;

#
# two-external
# experimentell fuer ethylene dz case
#
#  yy  (xx,ww,wx) (wz,xz):   4.3  45 4.4
# jump                       4.5  30 3.4
#



 ddscal($scale_2xyy,\@yy_2x,$yseg,$yseg);
 ddscal($scale_2xww,\@ww_2x,$wseg,$wseg);
 ddscal($scale_2xxx,\@xx_2x,$xseg,$xseg);
 ddscal($scale_2xwx,\@wx_2x,$wseg,$xseg);
 ddscal($scale_2xwz,\@wz_2x,$wseg,$zseg);
 ddscal($scale_2xxz,\@xz_2x,$xseg,$zseg);
  
  
$sum=0;
$sum+=summe($yseg,$yseg,\@yy_2x,0);
$sum+=summe($wseg,$wseg,\@ww_2x,0);
$sum+=summe($xseg,$xseg,\@xx_2x,0);
$sum+=summe($wseg,$xseg,\@wx_2x,1);
$sum+=summe($wseg,$zseg,\@wz_2x,1); 
$sum+=summe($xseg,$zseg,\@xz_2x,1);

$sum[2]=$sum;


#
# three-external
# experimentell fuer ethylene dz case
#
#  yx yw:  1 1
# jump   25  24
#



 ddscal($scale_3xyx,\@yx_3x,$yseg,$xseg);
 ddscal($scale_3xyw,\@yw_3x,$yseg,$wseg);


$sum=0;
$sum+=summe($yseg,$xseg,\@yx_3x,1);
$sum+=summe($yseg,$wseg,\@yw_3x,1);

$sum[3]=$sum;

 return @sum;
}


#($nseg,$nnseg)=createciudgin(\@borders_zz,\@borders_yy,\@borders_xx,\@borders_ww);

sub createciudgin
{
 my ($borders_zz,$borders_yy,$borders_xx,$borders_ww)=@_;
 my  (@nseg,@nnseg,$noffset);

 @nnseg=();
  shift @$borders_zz;
  push @nnseg,1;
  $nseg[0]=0;
  $nseg[0]++;
  pop @$borders_zz;
  while ($tmp = shift @$borders_zz ) {if ($tmp*$nsize_zyxw[0]<$nval_zyxw[0]) { $nseg[0]++; push @nnseg,$tmp*$nsize_zyxw[0];}}

  shift @$borders_yy;
  $noffset=$nval_zyxw[0];
  push @nnseg, $noffset+1;
  $nseg[1]=0;
  $nseg[1]++;
  pop @$borders_yy;
  while ($tmp = shift @$borders_yy ) {if ($tmp*$nsize_zyxw[1]<$nval_zyxw[1])
                                      {$nseg[1]++; push @nnseg,$tmp*$nsize_zyxw[1]+$noffset;}}

  shift @$borders_xx;
  $noffset+=$nval_zyxw[1];
  push @nnseg, $noffset+1;
  $nseg[2]=0;
  $nseg[2]++;
  pop @$borders_xx;
  while ($tmp = shift @$borders_xx ) {if ($tmp*$nsize_zyxw[2]<$nval_zyxw[2]) 
                                      {$nseg[2]++;push @nnseg,$tmp*$nsize_zyxw[2]+$noffset;}}

  shift @$borders_ww;
  $noffset+=$nval_zyxw[2];
  push @nnseg, $noffset+1;
  $nseg[3]=0;
  $nseg[3]++;
   pop @$borders_ww;
  while ($tmp = shift @$borders_ww ) { if ($tmp*$nsize_zyxw[2]<$nval_zyxw[2]) 
                                       {$nseg[3]++; push @nnseg,$tmp*$nsize_zyxw[3]+$noffset;}}

  $noffset+=$nval_zyxw[3];
  push @nnseg,$noffset+1;
  return (\@nseg,\@nnseg);
 return;
}

sub comp_maxciseg
{
 my ($borders,$csfbatch)=@_;
  my $maxsize=0;
#  print "comp:borders=",join(':',@$borders),"\n";
  for ($i=1; $i<=$#$borders;$i++)
  { $size=0; 
    for ($j=$$borders[$i-1]; $j<$$borders[$i]; $j++) { $size += $$csfbatch[$j];}
     if ($size > $maxsize) { $maxsize=$size;}
  }
  return $maxsize;
}






