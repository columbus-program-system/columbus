!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER*8           CNTSO0_WW(150,150),        CNTSO0_XX(150,150)
      INTEGER*8           CNTSO0_YY(150,150),        CNTSO0_ZZ(150,150)
      INTEGER*8           CNTSO1_YZ(150,150),        CNTSO1_YX(150,150)
      INTEGER*8           CNTSO1_YW(150,150),        CNTSO2_YY(150,150)
      INTEGER*8           CNTSO2_XX(150,150),        CNTSO2_WW(150,150)
      INTEGER*8           CNTSO2_WX(150,150)
C
      COMMON / SOINT_STAT/ CNTSO0_ZZ, CNTSO0_YY, CNTSO0_WW, CNTSO0_XX
      COMMON / SOINT_STAT/ CNTSO1_YZ, CNTSO1_YX, CNTSO1_YW
      COMMON / SOINT_STAT/ CNTSO2_YY, CNTSO2_XX, CNTSO2_WX
C
