!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             CLEN_WW(150),             CLEN_XX(150)
      INTEGER             CLEN_YY(150),             CLEN_ZZ(150)
      INTEGER             ISIZE_WW,    ISIZE_XX,    ISIZE_YY
      INTEGER             ISIZE_ZZ
C
      COMMON / SIZES_STAT/             ISIZE_ZZ,    ISIZE_YY
      COMMON / SIZES_STAT/             ISIZE_WW,    ISIZE_XX,    CLEN_ZZ
      COMMON / SIZES_STAT/             CLEN_YY,     CLEN_WW,     CLEN_XX
C
