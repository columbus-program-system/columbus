!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      block data  no
         IMPLICIT NONE


C
C     Parameter variables
C
      INTEGER         MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         NIOMX
      PARAMETER           (NIOMX = 128)
      INTEGER         MAXXW
      PARAMETER           (MAXXW = 5000)
      INTEGER         MXLOOP
      PARAMETER           (MXLOOP = 21000)
      INTEGER         LPLENG
      PARAMETER           (LPLENG = NIOMX*MAXXW)
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
C
C     Local variables
C
C     Equivalenced common variables
C
      INTEGER         AOFIL2,      AOFILE,      CIREFV,      CIUVFL
      INTEGER         FIACPF,      FILDIA,      FILDRT,      FILEV
      INTEGER         FILEW,       FILIND,      FILINT,      FILLP
      INTEGER         FILLPD,      FILLPI,      FILSC4,      FILSC5
      INTEGER         FILSTI,      FILTOT,      FILTPQ,      FLINDX
      INTEGER         FVFILE,      IFIL3W,      IFIL3X,      IFIL4W
      INTEGER         IFIL4X,      INUFV,       MCREST,      MOCOEF
      INTEGER         NSLIST,      OUTUFV,      PRTAP,       PSEUDG
      INTEGER         TAPE5,       TAPE6,       DRTFIL,      filevres
C

C
*@ifdef debug
*      integer         itest
*      common /ctestx/ itest(40)
*@endif
c da file record parameters
C     Common variables
C
      INTEGER         INDXLN,      VECRLN
C
      COMMON /DAPARM /  VECRLN,      INDXLN
C
c   vecrln    record length of vector files
c   indxln    record length of index files
c*mdc*if direct
c     include 'param.h'
c*mdc*endif
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c       46      filevres
c

c
c

C     Common variables
C
      CHARACTER*60        FNAME(NFILMX)
C
      COMMON /CFNAME /  FNAME
C
c      fname(1)    ciudgls
c            2     ciudgin
c            3     ciudgsm
c            4     cihdiag
c            5     cihpseu
c            6     cihvfl
c            7     civfl
c            8     diagint
c            9     ofdgint
c           10     cidrtfl
c           11     ciftdfl
c           12     ciftofl
c           13     ciftifl
c           14     ciflind
c           15     civin
c           16     civout
c           17     fockdg
c           18     d1fl
c           20     cifvfl
c           21     ciscr4
c           22     ciftotd
c           23     flacpfd
c           26     ciscr5
c           27     restart
c           28     civout
c           29     cirefv
c           31     fil4w
c           32     fil4x
c           33     fil3w
c           34     fil3x
c           36     aoints
c           37     aoints2
c           38     drtfil
c           39     cisrtif
c           42     mocoef
c           45     nocoef_ci
c           46     filevres
c

C     Common variables
C
      INTEGER*2           LPINDX(NIOMX,MAXXW),      YPATH(MXLOOP)
C
      COMMON /INDXLP /  LPINDX,      YPATH
C
c
c  AO-driven stuff
c
C     Common variables
C
      REAL*8            LOOPS(MXLOOP)
C
      COMMON /LOOPVL /  LOOPS
C
c
c  AO-driven stuff
c
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

C     Common variables
C
      INTEGER         AODRV
C
      COMMON /VPPASS /  AODRV
C
c
c  AODRV: AO-driven procedure
c
C
      equivalence (tape6,nunits(1))
      equivalence (tape5,nunits(2))
      equivalence (nslist,nunits(3))
      equivalence (fildia,nunits(4))
      equivalence (pseudg,nunits(5))
      equivalence (filew,nunits(6))
      equivalence (filev,nunits(7))
      equivalence (filind,nunits(8))
      equivalence (filint,nunits(9))
      equivalence (fildrt,nunits(10))
      equivalence (fillpd,nunits(11))
      equivalence (fillp,nunits(12))
      equivalence (fillpi,nunits(13))
      equivalence (flindx,nunits(14))
      equivalence (inufv ,nunits(15))
      equivalence (outufv,nunits(16))
      equivalence (fvfile,nunits(20))
      equivalence (filsc4,nunits(21))
      equivalence (nunits(22),filtot)
      equivalence (nunits(23),fiacpf)
      equivalence (filsc5,nunits(26))
      equivalence (nunits(27),mcrest)
      equivalence (nunits(28),ciuvfl)
      equivalence (cirefv,nunits(29))
      equivalence (ifil4w,nunits(31))
      equivalence (ifil4x,nunits(32))
      equivalence (ifil3w,nunits(33))
      equivalence (ifil3x,nunits(34))
      equivalence (filtpq, nunits(35))
      equivalence (aofile, nunits(36))
      equivalence (aofil2, nunits(37))
      equivalence (drtfil, nunits(38))
      equivalence (filsti,nunits(39))
      equivalence (nunits(42),mocoef)
      equivalence (nunits(45),prtap )
      equivalence (nunits(46),filevres)
c
c michal2{
c
ct      integer cosmofile
ct      equivalence (nunits(54),cosmofile)
c
c michal2}
C
C
      data loops/mxloop*0.d0/
      data lpindx/lpleng*0/
      data ypath/mxloop*0/
c
      data sym12/1,2,3,4,5,6,7,8/
*@if defined(debug) && defined(argonne)
*      data itest/40*0/
*@endif

c     # tape6 = standard listing file.
      data tape6/6/, fname(1)/'cimksegls'/
c
c     # tape5 = user input file.
      data tape5/5/, fname(2)/'ciudgin'/
c
c     # nslist = summary listing file.
      data nslist/7/, fname(3)/'cimksegsm'/
c
c     # fildia = diagonal h elements.
      data fildia/4/, fname(4)/'cihdiag'/
      data pseudg/3/, fname(5)/'cihpseu'/
c
c     # filew = (h*v) file.
      data filew/10/, fname(6)/'cihvfl'/
c
c     # filev = v file.
      data filev/11/, fname(7)/'civfl'/
c
c     # filind = diagonal integral file.
      data filind/12/, fname(8)/'diagint'/
c
c     # filint = off-diagonal 0-,1-, 2-external integrals.
      data filint/13/, fname(9)/'ofdgint'/
c
c     # fildrt = drt file.
      data fildrt/17/, fname(10)/'cidrtfl'/
c
c     # fillpd = diagonal formula file.
      data fillpd /20/, fname(11)/'ciftdfl'/
c
c     # fillp = off-diagonal formula file.
      data fillp/21/, fname(12)/'ciftofl'/
c
c     # fillpi = formula indexing file.
      data fillpi/24/, fname(13)/'ciftifl'/
c
c     # flindx = index file.
      data flindx/8/, fname(14)/'ciflind'/
c
c     # standard unformatted sequential vector files.
c
      data inufv/15/,  fname(15)/'civin'/
      data outufv/16/, fname(16)/'civout'/
c
*@ifdef mrpt
*C    # fock matrix diagonal file (for mrpt only)
*      data nunits(17)/19/, fname(17)/'fockdg'/
*@endif
c    # mcscf D1 density file (containing fock matrix for MRPT)
c     data nunits(18)/18/, fname(18)/'d1fl'/
c
cmd
c
c     # fvfile = formatted vector file.
      data fvfile/53/, fname(20)/'cifvfl'/
c
      data filsc4/25/, fname(21)/'ciscr4'/
      data filsc5/26/, fname(26)/'ciscr5'/
ctk scratch files for acpf density
      data filtot/41/, fname(22)/'ciftotd'/
      data fiacpf/42/, fname(23)/'flacpfd'/
c
c  mcrest = mcscf restart file.
      data mcrest/47/, fname(27)/'restart'/
c
c  ciuvfl = sequential ci vector, flprop replacement
      data ciuvfl/48/, fname(28)/'civout'/

c
c     # cirefv = ci reference vector for density program
      data cirefv/49/, fname(29)/'cirefv'/
c
c     # ifil4w = plus combination of  4-external integrals.
      data ifil4w/31/, fname(31)/'fil4w'/
c
c     # ifil4x = minus combination of 4-external integral.
      data ifil4x/32/, fname(32)/'fil4x'/
c
c     # ifil3w = plus combination of  3-external integrals.
      data ifil3w/33/, fname(33)/'fil3w'/
c
c     # ifil3x = minus combination of 3-external integral.
      data ifil3x/34/, fname(34)/'fil3x'/
cvp   aofile = ao integral file (sif format)
c     aofil2 = 2e ao file, if fsplit=2 ... see sif docu
      data aofile/36/, fname(36)/'aoints'/
      data aofil2/37/, fname(37)/'aoints2'/
      data drtfil/38/, fname(38)/'drtfil'/
c
c     # filsti = cisrt info file
      data filsti/50/,fname(39)/'cisrtif'/
c
c vorsicht!
c     # last fname is used for various filename manipulations.
c     data nunits(46)/0/, fname(46)/' '/
c
c  mocoef = mo coefficient file.
      data mocoef/50/, fname(42)/'mocoef'/
c  nocoef_ci . no coefficients and occupation numbers
      data prtap/52/, fname(45)/'nocoef_ci'/
c  civfl_restart  - CI vector restart file
      data filevres/53/, fname(46)/'civfl_restart'/
c
c michal2{
c
ct    data cosmofile/54/,fname(54)/'civcosmo'/
c
c michal2}
c

c
c
c     # da record lengths
c     # record lengths for ci created direct acc. files
c     #
c     # indxln : da record length for index file
c     # vecrln : da record length for vector files
c
c     # should be tuned for your machine....
c
*@if defined(vax) || (defined (ibm) && defined( vector))
*C      # vax maximum da record length is 4095
*       data indxln/4095/, vecrln/4095/
*@else
       data indxln/32768/, vecrln/32768/
*@endif
      end
      subroutine opn1st
c
c  open the required info and listing files
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
C
C     Equivalenced common variables
C
      INTEGER             FILDRT,      FILLPI,      FILSTI,      NSLIST
C

C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS
C     Common variables
C
      CHARACTER*60        FNAME(NFILMX)
C
      COMMON / CFNAME /   FNAME
C
C
      equivalence (nslist,nunits(3))
      equivalence (fildrt,nunits(10))
      equivalence (filsti,nunits(39))
c
      open( unit = fildrt, file = fname(10), status = 'unknown' )
      open( unit = nunits(1), file = fname(1), status = 'unknown')
      open( unit = nslist, file= fname(3), status = 'unknown')

      return
      end
c
c**************************************************************+
c
      subroutine driver( core, lcore, mem1, ifirst)

         IMPLICIT NONE
*@ifdef big
*      integer     nmomx
*      parameter ( nmomx=1023 )
*@else
      integer     nmomx
      parameter ( nmomx=1023 )
*@endif
       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex


        integer mitob
        integer mdtob
C
c      file locations
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w,drtfil
c      value:   0 local disk  1: virtual disk  2: global array
c               3 distribute one proc sys
c               4 distribute two proc sys
c               5 distribute four proc sys
c      default:      1    1    2     2     2      2
c
c      flofdg=1
c      fldiag=2
c      fl3x=3
c      fl3w=4
c      fl4x=5
c      fl4w=6
c      fldrt=7
c      ldsk=0
c      vdsk=1
c      glar=2
c      done=3
c      dtwo=4
c      dfor=5
       integer flofdg,fldiag,fl3x,fl3w,fl4x,fl4w,fldrt
       integer ldsk,vdk,glar,done,dtwo,dfor
       parameter (flofdg=2,fldiag=1,fl3x=3,fl3w=4,fl4x=5,fl4w=6)
       parameter (fldrt=7,ldsk=0,vdk=1,glar=2,done=3,dtwo=4,dfor=5)


C
C     External functions
C
      EXTERNAL            ATEBYT,      CNTREF,      FORBYT,      MXSCR
      EXTERNAL            RL
C
      INTEGER         ATEBYT,      CNTREF,      FORBYT,      MXSCR
      INTEGER         RL
C
C     Parameter variables
C
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER         TMIN0
      PARAMETER           (TMIN0 = 0)
      INTEGER         TMINIT
      PARAMETER           (TMINIT = 1)
      INTEGER         TMPRT
      PARAMETER           (TMPRT = 2)
      INTEGER         TMREIN
      PARAMETER           (TMREIN = 3)
      INTEGER         TMCLR
      PARAMETER           (TMCLR = 4)
      INTEGER         TMSUSP
      PARAMETER           (TMSUSP = 5)
      INTEGER         TMRESM
      PARAMETER           (TMRESM = 6)
      INTEGER         WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER         NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         NTITMX
      PARAMETER           (NTITMX = 30)
      INTEGER         NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER         NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER         MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER         MXNSEG
      PARAMETER           (MXNSEG = 200)
      INTEGER         IRBUFF
      PARAMETER           (IRBUFF = 8*4096)
      INTEGER         NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER         NSIFMX
      PARAMETER           (NSIFMX = 30)
      integer             mxproc
      parameter           (mxproc = 511)
C
      REAL*8            TWO
      PARAMETER           (TWO = 2.0D0)
C
C     Argument variables
C
      INTEGER         IFIRST,      LCORE,       MEM1
C
      REAL*8            CORE(LCORE)
C
C     Local variables
C
      INTEGER         BLONE,       CONFT
      INTEGER         HRFSPC,      I,           ICLOCK,      ICOUNT
      INTEGER         ICSFW,       ICSFX,       ICSFXW,      INDSYM
      INTEGER         IPTHW,       IPTHX,       IPTHXW,      ISFREE
      INTEGER         ISLAST,      LIMVEC,      LOC1,LOC2
      INTEGER         LOC5,        LOC6
      INTEGER         LOC7,        LOC8
      INTEGER         LOCMX, LOCH
      INTEGER         LOCV,        LPBUF
      INTEGER         LPEND,       MAX210,      MAXNEX,      MXORB3
      INTEGER         MXPTH1,      MXPTH2,      MXSEG1,      MXSEG2
      INTEGER         MXSGYZ,      NI,          NUMREF,      REFLST
      INTEGER         TOTAL,       TOTHRF
      INTEGER         TOTNC0,      TOTNC1
      INTEGER         TOTNC2,      TOTNC3,      TOTNC4,      TOTNCD
      INTEGER         TOTNEC,      TOTREF,      TOTSTC,      TOTVEC
      INTEGER         drtbuffer
      integer   nzy, ii,jj,kk, ihi,ilo,logrec,drtfil
      integer   ilop(mxproc),ihip(mxproc),jlop(mxproc),jhip(mxproc)

C
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c
C
      CHARACTER*60        FNAME(NFILMX)
      COMMON /CFNAME /  FNAME
C
c      fname(1)    ciudgls
c            2     ciudgin
c            3     ciudgsm
c            4     cihdiag
c            5     cihpseu
c            6     cihvfl
c            7     civfl
c            8     diagint
c            9     ofdgint
c           10     cidrtfl
c           11     ciftdfl
c           12     ciftofl
c           13     ciftifl
c           14     ciflind
c           15     civin
c           16     civout
c           17     fockdg
c           18     d1fl
c           20     cifvfl
c           21     ciscr4
c           22     ciftotd
c           23     flacpfd
c           26     ciscr5
c           27     restart
c           28     civout
c           29     cirefv
c           31     fil4w
c           32     fil4x
c           33     fil3w
c           34     fil3x
c           36     aoints
c           37     aoints2
c           38     drtfil
c           39     cisrtif
c           42     mocoef
c           45     nocoef_ci
c
C
      INTEGER         CSFPRN,      DAVCOR,      FRCSUB,      FROOT
      INTEGER         FTCALC,      IBKTHV,      IBKTV,       ICITHV
      INTEGER         ICITV,       IORTLS,      ISTRT,       IVMODE
      INTEGER         MAXSEG,      NBKITR,      NCOUPLE,     NITER
      INTEGER         NO0EX,       NO1EX,       NO2EX,       NO3EX
      INTEGER         NO4EX,       NOLDHV,      NOLDV,       NRFITR
      INTEGER         NROOT,       NTYPE,       NUNITV,      NVBKMN
      INTEGER         NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER         NVRFMX,      RTMODE,      VOUT
      INTEGER         NSEGD(4),    NSEG0X(4),   NSEG1X(4),NSEG2X(4)
      INTEGER         NSEG3X(4),   NSEG4X(4),   NSEGSO(4),NODIAG
      integer             cdg4ex,c3ex1ex,c2ex0ex,fileloc(10)
      integer             finalv,finalw
      REAL*8            CTOL,        ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8            LRTSHIFT
      COMMON /INDATA /  LRTSHIFT,    ETOLBK,      ETOL,        CTOL
      COMMON /INDATA /  NROOT,       NOLDV,       NOLDHV,      NUNITV
      COMMON /INDATA /  NBKITR,      NITER,       DAVCOR,      CSFPRN
      COMMON /INDATA /  IVMODE,      ISTRT,       VOUT,        IORTLS
      COMMON /INDATA /  NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
      COMMON /INDATA /  ICITV,       ICITHV,      FRCSUB,      NVBKMN
      COMMON /INDATA /  NVCIMN,      MAXSEG,      NTYPE,       NRFITR
      COMMON /INDATA /  NVRFMX,      NVRFMN,      FROOT,       FTCALC
      COMMON /INDATA /  RTMODE,      NCOUPLE,     NO0EX,       NO1EX
      COMMON /INDATA /  NO2EX,       NO3EX,       NO4EX,       NODIAG
      COMMON /INDATA /  NSEGD, NSEG0X, NSEG1X, NSEG2X, NSEG3X, NSEG4X
      COMMON /INDATA /  NSEGSO,cdg4ex,c3ex1ex,c2ex0ex,fileloc
      COMMON /INDATA /  FINALV, FINALW
C
C     Equivalenced common variables
C
      INTEGER         MODE,        NCIITR,      NINITV
C
      REAL*8            RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8            RTOLCI(NVMAX)
C

c LRTSHIFT,      aqcc/lrt shift  (falls <0 )
c ETOLBK,        etol-Grenzwert fuer BK iteration
c  ETOL,         etol-Grenzwert fuer CI iteration
c       CTOL     ctol-Grenzwert fuer CSF Analyse
c NROOT,         Zahl der zu berechnenden Wurzeln
c    NOLDV,      Zahl der vorhandenen CI-Vektoren auf civfl
c       NOLDHV,  Zahl der vorhandenen Sigma-Vektoren auf cihvfl
c     NUNITV     Zahl der zu generierenden Startvektoren
c NBKITR,        max. Zahl der BK Iterationen
c    NITER,      max. Zahl der CI Iterationen
c       DAVCOR,  Davidson-Korrektur berechnen
c     CSFPRN     CI-Vektor analysieren
c IVMODE,        Startvektorgenerierung
c     ISTRT,     Startvektorgenerierung bei ivmode=0
c      VOUT,     ????
c       IORTLS   defaults for ICITV,ICITHV
c NVBKMX,        maximale Subspace dimension in BK-Iteratione
c    IBKTV,      kontrolliert transformation der BK V-Vektoren
c       IBKTHV,  kontrolliert transformation der BK Sigma-Vektoren
c     NVCIMX     maximale Subspace dimension in CI-Iteration
c ICITV,         kontrolliert transformation der CI V-Vektoren
c      ICITHV,   kontrolliert transformation der CI Sigma-Vektoren
c     FRCSUB,    kontrolliert Neuberechnung der Subspace-Darstellung
c      NVBKMN    min. Groesse des BK subspaces
c NVCIMN,        min. Groesse des CI subspaces
c     MAXSEG,    max. Zahl von CI Segmenten
c     NTYPE,     Rechnungstyp
c       NRFITR   max. Zahl der CI Iterationen bei iterat. Referenzraumdi
c NVRFMX,        max. Groesse es Subspaces bei iterat. Referenzraumdiagona
c    NVRFMN,     min. Groesse des Subspaces bei iterat. Referenzraumdiagon
c    FROOT,      Root to follow
c       FTCALC   Berechnung des Formulatapes on the fly
c RTMODE,        obsolet
c    NCOUPLE,    Iteration in der der Lrtshift zum ersten Mal anzuwenden
c     NO0EX,     keine Berechnung der 0externen Beitraege
c     NO1EX      keine Berechnung der 1externen Beitraege
c NO2EX,         keine Berechnung der 2externen Beitraege
c     NO3EX,     keine Berechnung der 3externen Beitraege
c     NO4EX,     keine Berechnung der 4externen Beitraege
c      NODIAG    keine Berechnung der diagonalen Beitraege
c NSEGD,         Segmentierungschema fuer diagonale Beitraege
c NSEG0X,        Segmentierungsschema fuer all-interne Beitraege
c NSEG1X,        ... 1externe ....
cNSEG2X,         ... 2externe ....
cNSEG3X,         ... 3externe ....
c NSEG4X         ... 4externe ....
c NSEGSO,        ...  ...
ccdg4ex,         Behandle diagonalfall und 4-externen Fall gemeinsam
cc3ex1ex,        Behandle 3-externen Fall und 1-externen Fall gemeinsam
cc2ex0ex,        Behandle 2-externen Fall und 0-externen Fall gemeinsam
cfileloc         Filetyp fuer   diagint,ofdgint,fil3w,fil3x,fil4w,fil4x
c                  mit 1 = vdisk  2= ga  3=distrib. (one-proc-sys) 4=dis
cfinalv          final v vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk
cfinalw          final w vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk

C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
      LOGICAL             IFREF
      COMMON /DENMODE/  IFREF

c
c michal2{
c
c variable lastcall introduced for cosmo calculation
c this variable controls whether the civout vector will be
c read or not
       logical lastcall
       integer cosmocalc
       common/cosmo2/cosmocalc
c
c michal2}
c
C
c  a control variable in density part
c  should be eliminated !!
C     Common variables
C
      INTEGER         NNDX(NMOMX)
      COMMON /CNNDX  /  NNDX
c
c  nndx(i) = i*(i-1)/2
c
      INTEGER         IWX1
      INTEGER         MAXBUF,      MINWX,       NMBWX
      INTEGER         SCR1XL,      SCR3XL,      SCR4XL,      WXS
      INTEGER         WXSYM
      REAL*8            SQRT2
      COMMON /F4X    /  SQRT2,       NMBWX,       WXS,         WXSYM
      COMMON /F4X    /  MINWX
      COMMON /F4X    /  IWX1,        MAXBUF,      SCR4XL
      COMMON /F4X    /  SCR3XL,      SCR1XL
C
c SQRT2
c NMBWX       internally  number of w or x configurations (conft)
c WXS         internally  symmetry of the internal walk
c WXSYM       internally  symmetry of the external walk
c MINWX       internally  symmetry of first valid symmetry pair
c IWX1         segment type 1=x 2=w
c MAXBUF       buffer/record size of fil* files
c SCR4XL       length of scratch array scr4x
c SCR3XL       length of scratch array scr3x
c SCR1XL       length of scratch array scr1x --- unused ???
C     Common variables
C
      INTEGER         BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER         INTORB,      LAMDA1
      INTEGER         MAXBL2,      MAXLP3
      INTEGER         MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER         MXORB,       N0EXT
      INTEGER         N1EXT,       N2EXT
      INTEGER         N3EXT,       N4EXT
      INTEGER         ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER         NEXT,        NFCT,        NINTPT
      INTEGER         NMONEL,      NVALW,       NVALWK
      INTEGER         NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER         PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER         SPCCI,       TOTSPC
C
      COMMON /INF    /  PTHZ,        PTHY,        PTHX,        PTHW
      COMMON /INF    /  NINTPT,      DIMCI
      COMMON /INF    /  LAMDA1,      BUFSZI
      COMMON /INF    /  NMONEL,      INTORB
      COMMON /INF    /  MAXLP3,      BUFSZL
      COMMON /INF    /  MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON /INF    /  NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON /INF    /  ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON /INF    /  N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON /INF    /  SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON /INF    /  NVALZ,       NVALY,       NVALX,       NVALW
      COMMON /INF    /  NFCT,        NELI
C
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c      N1EXT,     number of one-external off-diagonal integrals
c     N2EXT       number of two-external off-diagonal integrals
c     N3EXT,      number of three-external off-diagonal integrals
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)


*@ifdef spinorbit
C     Common variables
C
      INTEGER         N0INT,       N0LP,        N1INT,       N1LP
      INTEGER         N2INT,       N2LP
C
      COMMON /SOINF  /  N2INT,       N1INT,       N0INT,       N2LP
      COMMON /SOINF  /  N1LP,        N0LP
C
c
c  n2int   two-internal so ints
c  n1int   one-internal so ints
c  n0int   all-external so ints
c  n2lp    number of two-internal loops
c  n1lp    number of one-internal loops
c  n0lp    number of zero-internal loops
c
*@endif
C     Common variables
C
      INTEGER         BL0XT,       BL1XT,       BL2XT, BL4XT
      INTEGER         FILV,        FILW,        ISKIP
      INTEGER         NZPTH
C
      COMMON /INF3   /  ISKIP,       NZPTH  , BL4XT
      COMMON /INF3   /  BL2XT,       BL1XT,       BL0XT,       FILV
      COMMON /INF3   /  FILW
C
c BL0XT,          number of blocks of all-internal integrals
c    BL1XT,       number of blocks of one-external integrals
c    BL2XT,       number of blocks of two-external integrals
c   FILV,         unit number of v-vector file
c      FILW,      unit number of w vector file
c      ISKIP      controls operation of mult
c NZPTH           number of valid z walks ( possibly mixed-up with nvalz

*@ifdef spinorbit
C     Common variables
C
      INTEGER         BL0INT,      BL1INT,      BL2INT
C
      COMMON /SOINF3 /  BL2INT,      BL1INT,      BL0INT
C
c
c start records for
c bl0int  all-external so
c bl1int  one-external so
c bl2int  two-external so
c

*@endif
C
      INTEGER         INDXLN,      VECRLN
      COMMON /DAPARM /  VECRLN,      INDXLN
C
c   vecrln    record length of vector files
c   indxln    record length of index files
C     Common variables
C
      INTEGER         LENCI, NSEG(7)
      INTEGER         SEGCI(MXNSEG,7),CIST(MXNSEG,7)
      INTEGER         SEGEL(MXNSEG,7),            SEGSCR(MXNSEG,7)
      INTEGER         SEGTYP(MXNSEG,7),       IPTHST(MXNSEG,7)
      INTEGER         DRTST(MXNSEG,7), DRTLEN(MXNSEG,7)
      INTEGER         DRTIDX(MXNSEG,7)
C
      COMMON /CIVCT  /  LENCI,       NSEG,  SEGEL, CIST
      COMMON /CIVCT  /  SEGCI,       SEGSCR,      SEGTYP, IPTHST
      COMMON /CIVCT /   DRTST, DRTLEN, DRTIDX
C
c
      integer maxmap,mxsize
      parameter (maxmap=20,mxsize = 20)
c
c   LENCI                   length of CI vector
c NSEG(I)                   total number of segments in segmentation sch
c   SEGCI(i,j)         number of CI elements in segment j of segment. sc
c   CIST(MXNSEG,7)     offset of first CI element corresponding to segme
c    SEGEL(MXNSEG,7),  number of internal paths in segment j of seg sche
c    SEGSCR(MXNSEG,7)   AO-driven stuff
c    SEGTYP(MXNSEG,7), segment type of segment j, seg scheme i
c       IPTHST(MXNSEG,7) offset of first internal path in index segment
c                        belonging to segment j of seg. scheme i
c    DRTST(MXNSEG,7),    offset of first element of DRT information on f
c                        belonging to segment j of seg scheme i
c    DRTLEN(MXNSEG,7)    number of elements of DRT info on file drtfil f
c    DRTIDX(MXNSEG,7)    length of index vector segment for segment j se
c
      INTEGER         UCIOPT(NCIOPT)
      COMMON /CCIOPT /  UCIOPT
C
c  some options which should be obsolete
c
c
c   uciopt(1)       lvlprt
c   uciopt(2)       iden
c   uciopt(3)       unused
c   uciopt(4)       itran
c   uciopt(5)       iroot
c
      INTEGER         AODRV
      COMMON /VPPASS /  AODRV
c
      integer             iprpmx
      parameter           (iprpmx = 130816)
      real*8              den1(iprpmx),den1ref(iprpmx),no(nmomx)
      common / den1p  /   den1,den1ref,no
c
c  AODRV: AO-driven procedure
c
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c

c
c     # nvmax = maximum subspace dimension allowed in the
c     #         iterative diagonalisation procedure.
c     #         (see also routine diagon and common eci)
c     #
c     # e(*)      = final energies.
c     # resid(*)  = final residual norms.
c     # deltae(*) = energy change from the last iteration.
c     # apxde(*)  = approximate energy lowerings for the next iteration.
C
      integer tape6,tape5,nslist,fildia,pseudg,filew,filev,filind,
     .        filint,fildrt,fillpd,fillp,fillpi,flindx,fockdg,fvfile,
     .        filsc4,filsc5,cirefv,ifil4w,ifil4x,ifil3w,ifil3x,prtap
      equivalence (tape6,nunits(1))
      equivalence (tape5,nunits(2))
      equivalence (nslist,nunits(3))
      equivalence (fildia,nunits(4))
      equivalence (pseudg,nunits(5))
      equivalence (filew,nunits(6))
      equivalence (filev,nunits(7))
      equivalence (filind,nunits(8))
      equivalence (filint,nunits(9))
      equivalence (fildrt,nunits(10))
      equivalence (fillpd,nunits(11))
      equivalence (fillp,nunits(12))
      equivalence (fillpi,nunits(13))
      equivalence (flindx,nunits(14))
      equivalence (fockdg,nunits(17))
      equivalence (fvfile,nunits(20))
      equivalence (filsc4,nunits(21))
      equivalence (filsc5,nunits(26))
      equivalence (cirefv,nunits(29))
      equivalence (ifil4w,nunits(31))
      equivalence (ifil4x,nunits(32))
      equivalence (ifil3w,nunits(33))
      equivalence (ifil3x,nunits(34))
      equivalence (drtfil, nunits(38))
      equivalence (prtap,nunits(45))
      integer lvlprt
      equivalence (lvlprt,uciopt(1))
c
      integer             blxt(8),     n2offr(8),   n2offs(8),   n2orbt
      integer             noffr(8),    noffs(8),    nsges(8)
      common / offs   /   n2orbt,      blxt,        nsges,       noffs
      common / offs   /   n2offs,      noffr,       n2offr
c
c  ##  integer section
c
      integer dum1(maxsym),dum2(maxsym),dum
      integer filerr
      integer imtype,isym,icnt1
      integer loc4,loc9,loc10,loc11
      integer map(nmomx,maxmap)
      integer nmap,nbft,nsym,nbpsy_mx,nbpsy(maxsym),nnbf,nd_at,nges,ntit
      integer syserr

       integer drtsiz,llenci
       common /drtsize/drtsiz,llenci
c
c  ##  character section
c
      character*3 mtype(maxmap)
      character*80 fmt
      character*8 bfnlab(nmomx)
      character*4 slabelao(nmomx)
      character*80 tit(mxsize)
      character*20 fmocoef
      character*4 lab(maxsym)



      integer*8 cnt2x_yy,cnt2x_ww, cnt2x_xx, cnt2x_wx, cnt2x_xz,cnt2x_wz
      common/twoex_stat/cnt2x_yy(150,150),cnt2x_ww(150,150),
     .                cnt2x_xx(150,150)
     .       ,        cnt2x_wx(150,150),cnt2x_xz(150,150),
     .                cnt2x_wz(150,150)


      integer*8 cnt1x_yz,cnt1x_yx, cnt1x_yw
      common/onex_stat/cnt1x_yz(150,150),cnt1x_yx(150,150),
     .     cnt1x_yw(150,150)


      integer*8 cnt0x_zz,cnt0x_yy, cnt0x_ww,cnt0x_xx
      common/allint_stat/cnt0x_zz(150,150),cnt0x_yy(150,150),
     .             cnt0x_ww(150,150)
     .     ,       cnt0x_xx(150,150)

      integer      isize_zz,isize_yy,isize_ww,isize_xx
      integer      clen_zz(150),clen_yy(150),clen_ww(150),
     .      clen_xx(150)
      common /sizes_stat/ isize_zz,isize_yy,isize_ww,isize_xx,
     .             clen_zz,clen_yy,clen_ww,clen_xx
c


      integer*8 cnt3x_yx,cnt3x_yw
      common/threex_stat/cnt3x_yx(150,150),cnt3x_yw(150,150)








c
c-----------------------------------------------------------------------
c
c
c     # repnuc = nuclear_repulsion + frozen_core + repartitioning energy
c
c     # irbuff .ge. max(bufszi, bufszl, vecrln)  must be satisfied.
c
c     # mxscr()  = scratch space needed for matrix-multiply routines.
c
c     # cntref() is used to count references.
c
c     # if running in parallel mode, only process 0 writes to
c     # sequential files
c
      me = ga_nodeid()
      if (me.ne.0) tape6=0

c     call timer(' ', tmin0, iclock, tape6 )
c
c     # some initializations.
c
      sqrt2 = sqrt(two)
      indsym = 0
      limvec = 0
      hrfspc = 0
c
      do 10 i = 1, nmomx
          nndx(i) = (i * (i - 1)) / 2
10    continue
c
c     # perform any necessary filename translations.
c
c     # still useful feature ?????
      call trnfln( 40, fname )
c
c
c     open the listing files.
c
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      if (me.eq.0) then
c     # read the user input  and open the input file.
c
      call datain
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      endif
      if (me.ne.0) nunits(1)=0
c     # open the necessary info containing files
c     # cidrtfl, cisrtif : in principle sufficient for node 0
c     # passing all info to all nodes
c     # however, this requires severe cleanup of program
c
      if (me.eq.0) then
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c
      write(tape6,6020) lcore, mem1, ifirst
6020  format(/' workspace allocation information:',
     + ' lcore=',i10,' mem1=',i10,' ifirst=',i10)

      endif
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

c
c     # read info from integral, drt, and ft files.
c     # again sufficient for node 0 to read these files
c     # and again as usual cleanup necessary
c
      totspc = lcore
      limvec = 0
      indsym = 0
c
c     # core(*)--> 1:reflst(1:nvalz), 2:limvec(1:nintpt),
c     #            3:indsym(1:nvalwk)
      if (me.eq.0)
     .  call tapin ( core(1), maxbuf, reflst, limvec, indsym,me )
      call ga_brdcst(14203,maxbuf,mitob(1),0)
      call ga_brdcst(14233,reflst,mitob(1),0)
      call ga_brdcst(14234,indsym,mitob(1),0)
      call ga_brdcst(14229,core(reflst),mitob(nvalz),0)
      call ga_sync()

      numref = cntref( core(reflst), nvalz )

c
c     # open files common to both acpf and ci passing da record lengths.
c
      call opncom(bufszi,vecrln,indxln,maxbuf,bufszl)

c
c     # prepare symmetry arrays.
c
      call smprep
c
c     # determine blocking and space requirements for the different
c     # sections of the program
c     # count one electron integrals and external orbitals.
c
      next=0
      nmonel=0
      icount=1
      do 310 i=1, nmsym
        blst1e(i)=icount
        ni=nbas(i)
        next=next+ni
        icount=icount+ni*(ni+1)/2
310   continue
      nmonel = icount - 1
c
c     # set some record values.
c
      bl4xt = 0
      blone = (nmonel-1) / bufszi + 1
      bl4xt = bl4xt + blone
      if(nmonel.eq.0)bl4xt=0
      nzpth = nvalz
      pthyz = pthy + pthz
      bl2xt = (n2ext-1) / bufszi + 1
      if(n2ext.eq.0)bl2xt=0
      bl1xt = (n1ext-1) / bufszi + 1
      if(n1ext.eq.0)bl1xt=0
      bl0xt = (n0ext-1) / bufszi + 1
      if(n0ext.eq.0)bl0xt=0
      bl2int = (n2int-1) / bufszi + 1
      if(n2int.eq.0)bl2int=0
      bl1int = (n1int-1) / bufszi + 1
      if(n1int.eq.0)bl1int=0
      bl0int = (n0int-1) / bufszi + 1
      if(n0int.eq.0)bl0int=0
      if(lvlprt.ge.2.and.me.eq.0)
     .  write(tape6,20090)bl4xt,bl2xt,bl1xt,bl0xt
     +     ,bl2int,bl1int,bl0int
20090 format(' nmb.of records 4-ext',i6,
     +     /,' nmb.of records 3-ext',i6,
     +     /,' nmb.of records 2-ext',i6,
     +     /,' nmb.of records 1-ext',i6,
     +     /,' nmb.of records 0-ext',i6,
     +     /,' nmb.of records 2-int',i6,
     +     /,' nmb.of records 1-int',i6,
     +     /,' nmb.of records 0-int',i6)
c
c     # space required for four external case.

      call intab
c
c
c    # copy diagint and ofdgint to virtual disk
c

      call initvdsk(fileloc(fldiag),fileloc(flofdg),me,
     .              core(indsym+forbyt(nvalwk)),4095)

c
c    # copy fil3w,fil3x,fil4w,fil4x to global arrays
c    # currently these are sequential files, hence,
c    # process 0 reads the data, only
c
       call initga34x(fileloc(fl4w),fileloc(fl4x),fileloc(fl3w),
     .                fileloc(fl3x),
     .                 core(indsym+forbyt(nvalwk)),maxbuf,10)
c
c     # space requried for four external case.
c

c     # space requried for three external case.
c
      scr3xl = mxscr( next, 'ci3x' )
c
      if (aodrv.lt.1) then
      totnc3=atebyt(bufszl+maxlp3) + atebyt(maxbuf) + atebyt(scr3xl)
     .       + 5*forbyt(maxlp3)
      endif
c
      mxbl23 = maxbl2/3
c
c     # space requried for two external case
c
c     # two-external memory requirements have been altered
c     # so as to make mxscr less usable (boo!)
c
c     # the call to mxscr will total the mnbas dependent requirements
c     # which have become machine independent. (another bad turn)
c
      scrlen = mxscr ( mnbas, 'ci2x' )
c
      totnc2 = atebyt( maxbl2+bufszi ) + atebyt( 2*mxbl23 ) +
     +    atebyt( bufszl ) +  2*atebyt( mnbas*mnbas ) +
     +    6*atebyt( mxbl23 ) + 3*atebyt( scrlen )
c
c     # one external.
c
c     # mxorb = blocksize for the two electron integrals(one external)
c
      call prep1
      mxorb3 = mxorb * 3
c
c     # 1-ex routines do not need scratch as written. this call is
c     # to preserve the structure for allocating scratch space in
c     # the future
c
      scr1xl = mxscr ( mxorb, 'ci1x' )
c
      totnc1  =  atebyt( mxorb3 + bufszi ) + atebyt( mxorb ) +
     +       atebyt( bufszl ) + 2*atebyt(mxorb*mxorb) +
     +       atebyt( scr1xl )
c
c     # all internal
c
      totnc0  =  atebyt( bufszi ) + atebyt( bufszl )
c
c     # space requried for diagonal case.
c
      totncd =atebyt(next)+atebyt(mxbld)+atebyt(bufszl)+atebyt(intorb)+
     +        atebyt(next) + atebyt(nmomx)
c
c     # max210 is the starting location of the core memory which may
c     # be used in the last two steps, namely csfout and davidc.
c
      max210 = max( totnc2, totnc1, totnc0 )
c
      totref = forbyt ( nvalz )
      tothrf = forbyt(nvalz)
c
c
c     # report the space needed.
c     loc1 .. reflst(nvalz)  ..  totref
c     for AQCC only: loc5  .. forbyt(nvalz)  ..  totref
c
      maxnex = max(totnc3,totnc2,totnc1,totnc0,totncd)
      totstc = totref
      if (ntype.eq.3) totstc = totstc + tothrf
      totnec = totstc + maxnex
      totvec = totspc - totnec

c
      if ( lvlprt .ge. 1 .and. me.eq.0 ) then
        write(tape6, * )
        write(tape6, * ) '< n-ex core usage >'
        write(tape6,*)   '    routines:'
        write(tape6, 1010 ) ' threx  ', totnc3
        write(tape6, 1010 ) ' twoex  ', totnc2
        write(tape6, 1010 ) ' onex   ', totnc1
        write(tape6, 1010 ) ' allin  ', totnc0
        write(tape6, 1010 ) ' diagon ', totncd
        write(tape6, '(3x,8x,4x,a7)' ) '======='
        write(tape6, 1010 ) 'maximum ', maxnex
        write(tape6, *  )
        write(tape6, * ) ' __ static summary __ '
        write(tape6, 1010 ) 'reflst  ', totref
        write(tape6, 1010 ) 'hrfspc  ', tothrf
        write(tape6, '(3x,8x,4x,a7)' ) '-------'
        write(tape6, 1010 ) 'static->', totstc
        write(tape6, *  )
        write(tape6, * ) ' __ core required  __ '
        write(tape6, 1010 ) 'totstc  ', totstc
        write(tape6, 1010 ) 'max n-ex', maxnex
        write(tape6, '(3x,8x,4x,a7)' ) '-------'
        write(tape6, 1010 ) 'totnec->', totnec
        write(tape6, *  )
        write(tape6, * ) ' __ core available __ '
        write(tape6, 1010 ) 'totspc  ', totspc
        write(tape6, 1010 ) 'totnec -', totnec
        write(tape6, '(3x,8x,4x,a7)' ) '-------'
        write(tape6, 1010 ) 'totvec->', totvec
      endif
1010  format (3x,a8,4x,i7)
c
c     # stop if there is not enough core.
c
      if( totnec .gt. totspc ) then
        call bummer('dirci: pre-segmentation space error, '
     &    //'(totnec-totspc)=',(totnec-totspc), faterr )
      endif
c
c
c     # calculate spcci for prepd (for segmentation)
c     # spcci is the maximum amount available for the
c     # CI vector
c     # in prepd this must also cover the amount
c     # of space needed for index,indsym and conft
c
      spcci = rl(totvec)
c
c     # core(*)-->
c
c     #  1:   reflst(1:nvalz)       integer*4
c     #  2:   limvec(1:nintpt)      integer*4
c     #  3:   indsym(1:nvalwk)      integer*4
c     #  4:   conft(1:nvalwk)       integer*4
c

      conft = indsym + forbyt( nvalwk )
c
c     # free amount totspc-conft-forbyt(nvalwk))
c     # currently for segmentation we need a copy of
c     # the index vector
c

       drtbuffer=conft+forbyt(nvalwk)
       if (totspc-drtbuffer-forbyt(nintpt).lt.0)
     .  call bummer('insufficient space for prepdnew',
     .  totspc-drtbuffer-forbyt(nintpt),2)

c
c     # set up the segmentation (7 concurrent schemes)
c
      if (me.eq.0)
     . call prepdnew( core(limvec), core(indsym), core(conft),
     .     core(lpbuf), maxlp3*3+bufszl, core(drtbuffer),
     .     totspc-drtbuffer )


c
c    # from now on we only need
c    #  core 1: reflst(1..nvalz)
c
c     # set up the tasklist
c     # note, later we have to eliminate all tasks with
c     # a loop count of 0 !
c
      if (me.eq.0) call gentasklist


c     # call inivec to transfer segmentation information to word
c     # addressable i/o routines.
c
cseq  call inivec( cist, nseg ) ?????
c
      mxseg1 = 0
      mxseg2 = 0
      mxpth1 = 0
      mxpth2 = 0
      ipthx  = 0
      icsfx  = 0
      ipthw  = 0
      icsfw  = 0
      do 42 i = 1, nseg(1)
        if (segtyp(i,1).eq.4) then
c           # count w configurations
        ipthw = ipthw + segel(i,1)
        icsfw = icsfw + segci(i,1)
         elseif(segtyp(i,1).eq.3) then
c           # count x configurations
         ipthx = ipthx + segel(i,1)
         icsfx = icsfx + segci(i,1)
         endif
         if (segci(i,1) .gt. mxseg2 ) then
         if (segci(i,1) .gt. mxseg1 ) then
            mxseg2 = mxseg1
            mxpth2 = mxpth1
            mxseg1 = segci(i,1)
            mxpth1 = segel(i,1)
         else
            mxseg2 = segci(i,1)
            mxpth2 = segel(i,1)
         endif
         endif
42    continue
      ipthxw = ipthx + ipthw
      icsfxw = icsfx + icsfw
c
      total = totstc + maxnex +
     + 2 * atebyt(mxseg1) + forbyt(mxpth1) +
     + 2 * atebyt(mxseg2) + forbyt(mxpth2)
c
c     # core:
c
c     # loc1 -reflst(nvalz)         integer*4
c     # loc5 - intbuf(mxbld)        real*8
c     # loc6 - oneldg(next+intorb)  real*8
c     # loc7 - lpbuf(bufszl)        real*8
c     # loc8 - transf(next)         real*8
c
c     # space allocations determined in terms of real*8 words
c     #
c     # mxbld:   blocksize for the two electron integrals (diagonal)
c     # bufszi:  buffer size of the two electron integral file
c     # bufszl:  buffer size of the formula tape file
c     # nvalwk:  number of valid internal paths
c     # next:    number of orbitals in the external space
c     # intorb:  number of orbitals in the internal space
c     # pthyz:   number of y and z walks
c     # confyz:  number of y and z valid walks
cmd
      loc1  = 1
      loc2  = loc1 + forbyt(nvalz)
      loc5  = loc2
      loc6  = loc5  + atebyt( mxbld )
      loc7  = loc6  + atebyt( next + intorb )
      loc8  = loc7  + atebyt( bufszl )
      locmx = loc8  + atebyt( next )
ctmp
c
c     # calculate the diagonal h-matrix elements and save on file.
c     # should be eliminated (-> ivmode=0, cinewv)
c     # eliminate index-vector file ???
c

c

c     call timer('ciudg cumulative time', tmprt, iclock, tape6 )
c
c     # core:
c     #
c     # loc1 - reflst(nvalz)        integer*4
c     # ntype = 3  requires additional space
c     # loc5 - aacsf(nvalz)         integer*4
c
      if (ntype.eq.3) then
       loch  = loc5  + forbyt(nvalz)
      else
       loch = loc5
      endif
c
      locv = loch
c     # last address of static allocated information.
      islast = locv
c     # amount of free space left to allocate.
      isfree = totspc - islast
c     # number of zy configurations
c
c     #cleanup dmain !!!
c

      nzy = 0
      do ii = 1,2
      kk = 0
       do jj=1,nsegd(ii)
         kk=kk+1
         nzy=nzy+segci(kk,ii)
       enddo
      enddo
c
        call dmain(
     &    dimci,       nroot,        filev,        filew,
     &    fildia,      noldv,
     &    noldhv,      nunitv,       nbkitr,       niter,
     &    etolbk,      etol,         nvbkmx,       nvbkmn,
     &    nvcimx,      nvcimn,       ibktv,        ibkthv,
     &    icitv,       icithv,       frcsub,       core,
     &    ivmode,      vout,         nzy ,     istrt,
     &    iortls,      core(reflst),
     &    core(loc5),   core(islast), isfree,
     &    nrfitr,      nvrfmx, nvrfmn,  nvalz,ntype, froot ,
     &    rtmode,      pthyz,
     &    confyz,      mxbld,        next,         intorb,
     &    bufszl,      bufszi, lrtshift, ncouple)

c
c
      call izero_wr(mxproc,ilop,1)
      call izero_wr(mxproc,jlop,1)
      call izero_wr(mxproc,ihip,1)
      call izero_wr(mxproc,jhip,1)


      return
      end
c
c***********************************************************************
c
      subroutine datain
c
c  read user input.
c
c  28-jun-91 .le. bug fixed in loop 100. -pz/rls
c  23-may-90 made list-directed input read all input options -eas
c
         IMPLICIT NONE

c      file locations
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w,drtfil
c      value:   0 local disk  1: virtual disk  2: global array
c               3 distribute one proc sys
c               4 distribute two proc sys
c               5 distribute four proc sys
c      default:      1    1    2     2     2      2
c
c      flofdg=1
c      fldiag=2
c      fl3x=3
c      fl3w=4
c      fl4x=5
c      fl4w=6
c      fldrt=7
c      ldsk=0
c      vdsk=1
c      glar=2
c      done=3
c      dtwo=4
c      dfor=5
       integer flofdg,fldiag,fl3x,fl3w,fl4x,fl4w,fldrt
       integer ldsk,vdk,glar,done,dtwo,dfor
       parameter (flofdg=2,fldiag=1,fl3x=3,fl3w=4,fl4x=5,fl4w=6)
       parameter (fldrt=7,ldsk=0,vdk=1,glar=2,done=3,dtwo=4,dfor=5)


C
C     Parameter variables
C
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER         WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER         NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
      INTEGER         NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER         MAXORB
      PARAMETER           (MAXORB = 1023)
      INTEGER         ISFLN
      PARAMETER           (ISFLN = 4)
      INTEGER         NGVAL
      PARAMETER           (NGVAL = 5)
      INTEGER         MXAVST
      PARAMETER           (mxavst = 50)

C
C     Local variables
C
      INTEGER         I,           IERR,        IFILE,       IST
      INTEGER         J,           SGLPRC, GSET
      logical   stest
C
      REAL*8            NORM,        XXX, G(3)
C
C     Equivalenced common variables
C
      INTEGER         NIN,         NLIST,       NSLIST
C

C
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

C     Common variables
C
      CHARACTER*60        FNAME(NFILMX)
C
      COMMON /CFNAME /  FNAME
C
c      fname(1)    ciudgls
c            2     ciudgin
c            3     ciudgsm
c            4     cihdiag
c            5     cihpseu
c            6     cihvfl
c            7     civfl
c            8     diagint
c            9     ofdgint
c           10     cidrtfl
c           11     ciftdfl
c           12     ciftofl
c           13     ciftifl
c           14     ciflind
c           15     civin
c           16     civout
c           17     fockdg
c           18     d1fl
c           20     cifvfl
c           21     ciscr4
c           22     ciftotd
c           23     flacpfd
c           26     ciscr5
c           27     restart
c           28     civout
c           29     cirefv
c           31     fil4w
c           32     fil4x
c           33     fil3w
c           34     fil3x
c           36     aoints
c           37     aoints2
c           38     drtfil
c           39     cisrtif
c           42     mocoef
c           45     nocoef_ci
c

C     Common variables
C
      INTEGER         UCIOPT(NCIOPT)
C
      COMMON /CCIOPT /  UCIOPT
C
c  some options which should be obsolete
c
c
c   uciopt(1)       lvlprt
c   uciopt(2)       iden
c   uciopt(3)       unused
c   uciopt(4)       itran
c   uciopt(5)       iroot

C     Common variables
C
      INTEGER         METHODACPF,  NCOREL
C
      REAL*8            A4DEN(NVMAX),             EREF(NVMAX), GVALUE
C
      COMMON /ACPFINFO/ NCOREL,      METHODACPF,  GVALUE,      EREF
      COMMON /ACPFINFO/ A4DEN
C
C     Common variables
C
      INTEGER         CSFPRN,      DAVCOR,      FRCSUB,      FROOT
      INTEGER         FTCALC,      IBKTHV,      IBKTV,       ICITHV
      INTEGER         ICITV,       IORTLS,      ISTRT,       IVMODE
      INTEGER         MAXSEG,      NBKITR,      NCOUPLE,     NITER
      INTEGER         NO0EX,       NO1EX,       NO2EX,       NO3EX
      INTEGER         NO4EX,       NOLDHV,      NOLDV,       NRFITR
      INTEGER         NROOT,       NTYPE,       NUNITV,      NVBKMN
      INTEGER         NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER         NVRFMX,      RTMODE,      VOUT
      INTEGER         NSEGD(4),    NSEG0X(4),   NSEG1X(4),NSEG2X(4)
      INTEGER         NSEG3X(4),   NSEG4X(4),   NSEGSO(4),NODIAG
      integer             cdg4ex,c3ex1ex,c2ex0ex,fileloc(10)
      integer             finalv,finalw,llenci
C
      REAL*8            CTOL,        ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8            LRTSHIFT
C
      COMMON /INDATA /  LRTSHIFT,    ETOLBK,      ETOL,        CTOL
      COMMON /INDATA /  NROOT,       NOLDV,       NOLDHV,      NUNITV
      COMMON /INDATA /  NBKITR,      NITER,       DAVCOR,      CSFPRN
      COMMON /INDATA /  IVMODE,      ISTRT,       VOUT,        IORTLS
      COMMON /INDATA /  NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
      COMMON /INDATA /  ICITV,       ICITHV,      FRCSUB,      NVBKMN
      COMMON /INDATA /  NVCIMN,      MAXSEG,      NTYPE,       NRFITR
      COMMON /INDATA /  NVRFMX,      NVRFMN,      FROOT,       FTCALC
      COMMON /INDATA /  RTMODE,      NCOUPLE,     NO0EX,       NO1EX
      COMMON /INDATA /  NO2EX,       NO3EX,       NO4EX,       NODIAG
      COMMON /INDATA /  NSEGD, NSEG0X, NSEG1X, NSEG2X, NSEG3X, NSEG4X
      COMMON /INDATA /  NSEGSO,cdg4ex,c3ex1ex,c2ex0ex,fileloc
      COMMON /INDATA /  FINALV, FINALW
C
C     Equivalenced common variables
C
      INTEGER         MODE,        NCIITR,      NINITV
C
      REAL*8            RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8            RTOLCI(NVMAX)
C

c LRTSHIFT,      aqcc/lrt shift  (falls <0 )
c ETOLBK,        etol-Grenzwert fuer BK iteration
c  ETOL,         etol-Grenzwert fuer CI iteration
c       CTOL     ctol-Grenzwert fuer CSF Analyse
c NROOT,         Zahl der zu berechnenden Wurzeln
c    NOLDV,      Zahl der vorhandenen CI-Vektoren auf civfl
c       NOLDHV,  Zahl der vorhandenen Sigma-Vektoren auf cihvfl
c     NUNITV     Zahl der zu generierenden Startvektoren
c NBKITR,        max. Zahl der BK Iterationen
c    NITER,      max. Zahl der CI Iterationen
c       DAVCOR,  Davidson-Korrektur berechnen
c     CSFPRN     CI-Vektor analysieren
c IVMODE,        Startvektorgenerierung
c     ISTRT,     Startvektorgenerierung bei ivmode=0
c      VOUT,     ????
c       IORTLS   defaults for ICITV,ICITHV
c NVBKMX,        maximale Subspace dimension in BK-Iteratione
c    IBKTV,      kontrolliert transformation der BK V-Vektoren
c       IBKTHV,  kontrolliert transformation der BK Sigma-Vektoren
c     NVCIMX     maximale Subspace dimension in CI-Iteration
c ICITV,         kontrolliert transformation der CI V-Vektoren
c      ICITHV,   kontrolliert transformation der CI Sigma-Vektoren
c     FRCSUB,    kontrolliert Neuberechnung der Subspace-Darstellung
c      NVBKMN    min. Groesse des BK subspaces
c NVCIMN,        min. Groesse des CI subspaces
c     MAXSEG,    max. Zahl von CI Segmenten
c     NTYPE,     Rechnungstyp
c       NRFITR   max. Zahl der CI Iterationen bei iterat. Referenzraumdi
c NVRFMX,        max. Groesze des Subspaces bei iterat. Referenzraumdiagona
c    NVRFMN,     min. Groesze des Subspaces bei iterat. Referenzraumdiagon
c    FROOT,      Root to follow
c       FTCALC   Berechnung des Formulatapes on the fly
c RTMODE,        obsolet
c    NCOUPLE,    Iteration in der der Lrtshift zum ersten Mal anzuwenden
c     NO0EX,     keine Berechnung der 0externen Beitraege
c     NO1EX      keine Berechnung der 1externen Beitraege
c NO2EX,         keine Berechnung der 2externen Beitraege
c     NO3EX,     keine Berechnung der 3externen Beitraege
c     NO4EX,     keine Berechnung der 4externen Beitraege
c      NODIAG    keine Berechnung der diagonalen Beitraege
c NSEGD,         Segmentierungschema fuer diagonale Beitraege
c NSEG0X,        Segmentierungsschema fuer all-interne Beitraege
c NSEG1X,        ... 1externe ....
cNSEG2X,         ... 2externe ....
cNSEG3X,         ... 3externe ....
c NSEG4X         ... 4externe ....
c NSEGSO,        ...  ...
ccdg4ex,         Behandle diagonalfall und 4-externen Fall gemeinsam
cc3ex1ex,        Behandle 3-externen Fall und 1-externen Fall gemeinsam
cc2ex0ex,        Behandle 2-externen Fall und 0-externen Fall gemeinsam
cfileloc         Filetyp fuer   diagint,ofdgint,fil3w,fil3x,fil4w,fil4x
c                  mit 1 = vdisk  2= ga  3=distrib. (one-proc-sys) 4=dis
cfinalv          final v vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk
cfinalw          final w vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk

      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB    CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
c
*@ifdef mrpt
*C     Common variables
*C
*      INTEGER         PTGREF,      PTH0TYPE,    PTITER,      PTLEVL
*      INTEGER         PTLINSOL,    PTMETH,      PTMTST,      PTNAVST
*      INTEGER         PTNVMX,      PTROOT
*      REAL*8            PTSHIFT(MXAVST),          PTTOL
*      REAL*8            PTWAVST(MXAVST)
*      COMMON /MRPTX  /  PTGREF,      PTLEVL,      PTMETH,      PTITER
*      COMMON /MRPTX  /  PTTOL,       PTROOT,      PTNAVST,     PTNVMX
*      COMMON /MRPTX  /  PTLINSOL,    PTSHIFT,     PTWAVST,     PTMTST
*      COMMON /MRPTX  /  PTH0TYPE
*C
*C
*C  variables which control MRPT stuff
*C
*@endif
C     Common variables
C
      INTEGER         AODRV
C
      COMMON /VPPASS /  AODRV
C
c
c  AODRV: AO-driven procedure
c

C
C
      integer lvlprt,iden,itran,iroot
      character*60 fnamex
      equivalence (nlist,nunits(1))
      equivalence (nin,nunits(2))
      equivalence (nslist,nunits(3))
      equivalence (fname(nfilmx),fnamex)
      equivalence (lvlprt,uciopt(1))
      equivalence (iden,uciopt(2))
      equivalence (itran,uciopt(4))
      equivalence (iroot,uciopt(5))
      equivalence (etolbk,rtolbk), (etol,rtol,rtolci)
      equivalence (niter, nciitr)
      equivalence (ivmode, mode)
      equivalence (nunitv, ninitv)
      integer prtap
      equivalence (nunits(45),prtap )
c
      integer mindim,nsegwx(4),directhd
c
cvp/ctm
c     # variable "aodrv" is added to the namelist for determine which
c     # CI method will be used
c     # eg.
c     # aodriv = 0     for conventional CI
c     #        = 1     for ao-driven random integeral based.
c     #                                   V. Parasuk 14/3/95
c     #        = 2     for double direct code
c     #                                   T. Mueller 10/7/97

      NAMELIST / INPUT  / LVLPRT,      NROOT,       NOLDV,       NOLDHV
     x,                   NUNITV,      NBKITR,      NITER,       DAVCOR
     x,                   CSFPRN,      ETOLBK,      ETOL,        CTOL
     x,                   UCIOPT,      IVMODE,      ISTRT,       VOUT
     x,                   IORTLS
     x,                   NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
     x,                   ICITV,       ICITHV,      FRCSUB,      NCIITR
     x,                   RTOLBK,      RTOL,        RTOLCI,      MODE
     x,                   NINITV,      NVBKMN,      NVCIMN,      MAXSEG
     x,                   NTYPE, GSET, G
     x,                   AODRV,       NRFITR,      NCOREL
     x,                   NVRFMX,      NVRFMN
     x,                   IDEN
     x,                   ITRAN,       IROOT,       FROOT,       RTMODE
     x,                   LRTSHIFT,    NCOUPLE,     SKIPSO
     x,                   nsegd,nseg0x,nseg1x,nseg2x,nseg3x,nseg4x
     x,     nsegwx,       no0ex,no1ex,no2ex,no3ex,no4ex ,nodiag ,
     .                    nsegso,cdg4ex,c3ex1ex,c2ex0ex,fileloc,molcas
c michal2{
c
     .                  , finalv,finalw,ftcalc,llenci,cosmocalc,directhd

c
c declaration of cosmocalc
      integer cosmocalc,molcas
c common block for cosmocalc
      common/cosmo2/cosmocalc
c michal2}
c
*@ifdef mrpt
*     x,                   PTGREF,      PTLEVL,      PTMETH,      PTITER
*     x,                   PTTOL
*     x,                   PTNAVST,     PTNVMX,      PTLINSOL
*     x,                   PTSHIFT,     PTWAVST,     PTMTST,PTHOTYPE
*@endif
C
cvp

c
      ierr = 0
c
ctm start
c  ciudgin and ciudgls are opened in driver now
c     open the input and listing files.
c
*@ifdef ibm
*C  use preconnected nlist.
*C ibm mvs won't allow open of unit 6
*@else
c     open( unit = nlist, file = fname(1), status = 'unknown')
*@endif
      open( unit = nin, file = fname(2), status = 'unknown')
ctm end
c
c     read and echo standard input until eof is reached.
c
      write(nlist,6010)
6010  format(' echo of the input for program ciudg:')
      call echoin( nin, nlist, ierr )
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind nin
c
      do 15 i = 1,nciopt
         uciopt(i) = 0
15    continue
c     # set namelist default values.
c
ctmstart
c
c michal2{
c
c set default value for cosmocalc
       cosmocalc = 0
c
c michal2}
c
c      set default method ci
       methodacpf = 0
       ncorel = 0
       iden= 1
       itran=0
       iroot = 1
       do i=1,4
         nsegd(i)=1
         nseg0x(i)=1
         nseg1x(i)=1
         nseg2x(i)=1
         nseg3x(i)=1
         nseg4x(i)=1
         nsegso(i)=1
       enddo
      no0ex=0
      no1ex=0
      no2ex=0
      no3ex=0
      no4ex=0
      nodiag=0
      ntype = 0
      lvlprt = 0
      nroot = 1
      noldv = 0
      noldhv = 0
      nunitv = 1
      nbkitr = -1
      nrfitr = 30
      niter = 20
      nlist = nlist
      davcor = 1
      csfprn = 1
      ctol = 0.01d+00
      ivmode = 0
      istrt = 0
      vout = 0
      iortls = 0
c     nvbkmx = -1
      nvbkmx = 16
      nvbkmn = -1
      nvcimx = 16
      nvcimn = -1
      nvrfmx = 16
      nvrfmn = -1
      ibktv = -1
      ibkthv = -1
      icitv = -1
      icithv = -1
      frcsub = 0
      froot = 0
*@ifdef spinorbit
      skipso = .false.
*@endif
      rtmode = 0
      lrtshift =1.0d0
c
c     ncouple indicates the iteration when the AQCC shift
c     is applied for the first time
c     in the program it contains the number of iterations
c     to be carried out without AQCC shift plus one,
c     i.e. it is counted down.
c
      ncouple = 1

c   # compute FT information
      ftcalc=1
*@ifdef mrpt
*C    MRPT related
*      pth0type=0
*      ptlevl = 2
*      ptgref = 1
*      ptmeth = 0
*      ptiter = 40
*      pttol = 1.0d-05
*      ptnvmx = 16
*      ptlinsol = 0
*      ptnavst = 1
*      ptmtst = 1
*C  initialize  ptwavst, ptshift
*      do i=1,mxavst
*      ptshift(i) = 0.0d+00
*      ptwavst(i) = 0.0d+00
*      enddo
*C     avstat defaults ....
*      ptwavst(1) = 1.0d+00
*@endif
c
c
cvp added aodrv
      aodrv = 0
cvp
      do 10 i = 1, nvmax
         etolbk(i) = 1.d-4
         etol(i)   = 1.d-4
10    continue
c
c     # read namelist input and keep processing if eof is reached.
c
*@ifdef crayctss
*C  setup for standard namelist.
*      call ddiopon('ibm')
*@endif
c
*@ifdef nonamelist
*@elif defined  nml_required
*      read( nin, nml=input, end=3 )
*@else
      read( nin, input, end=3 )
*@endif
c
c     # read filenames starting with iunit(3)...
c
*@if defined(unix) || defined ( argonne) || (defined (sun) && defined (os4))
c      # argonne codes use environment variables.
c      # sunos4 fortran io bug doesn't allow filenames to be read. -rls
*@else
*100   continue
*         read (nin,*,end=3)ifile, fnamex
*         if(ifile.ge.3 .and. ifile.le.(nfilmx-1) .and. fnamex.ne.' ')
*     +    fname(ifile) = fnamex
*      go to 100
*@endif
c
3     continue
c
c
c      override input
       fileloc(fldiag)=ldsk
       fileloc(flofdg)=ldsk
       fileloc(fl3x)=ldsk
       fileloc(fl3w)=ldsk
       fileloc(fl4x)=ldsk
       fileloc(fl4w)=ldsk
       fileloc(fldrt)=ldsk
        do i=1,4
         nsegd(i)=nseg4x(i)    
        enddo
       cdg4ex=1
       c3ex1ex=0
       c2ex0ex=0
       finalv=0
       finalw=0
c     # set to 4 to force in core calculations
      maxseg = 4
       do i=1,4
         nsegd(i)=1
         nseg1x(i)=1
         nseg2x(i)=1
         nseg3x(i)=1
         nseg4x(i)=1
         nsegso(i)=1
       enddo

      if (lrtshift.gt.-1.d-8 ) then
         lrtshift=1.0d0
      else
       write(6,*) 'LRT calculation: diagonal shift=',lrtshift
      endif

      if( (ftcalc.ne.1) )
     & call bummer('invalid ftcalc<>1 .. setting to 1',ftcalc,0)
       ftcalc=1
c
      if (ncouple.gt.1) then
         if ( ntype.eq.0)
     &    call bummer('setting ncouple meaningless for CI',ncouple,0)
         if ( ntype.eq.3)
     &  call bummer('start applying AQCC shift at iteration',ncouple,0)
      endif



      if ( noldv .ne. 0 ) then
c        # use all old v vectors requested
         ninitv = noldv
c        # and read from v file
         mode   = 1
      endif
c
c     # check ivmode to determine nbkitr default
c
      if (nbkitr.lt.0) then
         if ( (ivmode .eq. 1) .or. (ivmode .eq. 3) ) then
            nbkitr = 1
         else
            nbkitr = 0
         endif
      endif
c
       nunitv=max(nunitv,nroot)

       if (froot.ne.0) then
         if (nroot.lt.froot) nroot=froot
         if (nunitv.lt.froot) nunitv=froot
         if (vout.ne.0) vout=froot
         if (rtmode.eq.0) then
          write(nlist,*) 'froot operation modus:',
     .      ' follow reference vector.'
         elseif (rtmode.eq.1) then
          write(nlist,*) 'froot operation modus:',
     .      ' follow ci vector of previous ci iteration.'
         else
         call bummer('invalid rtmode, rtmode=',rtmode,2)
         endif
ctm redundant
         if (davcor.ne.0 .and. davcor.le.10) davcor=froot
         if (csfprn.ne.0 .and. csfprn.le.10) csfprn=froot
       endif
       if ((froot.eq.0).and.(nroot.gt.1).and.(ntype.eq.3)) then
        if (lrtshift.gt.-1.0d-8)
     .  call bummer('ntype=3, froot=0, nolrtshift, nroot>1:',nroot,2)
       endif
       if ((froot.eq.0).and.(ntype.eq.3) .and. (lrtshift.gt.-1.0d-8))
     .  vout=1


*@ifdef mrpt
*C
*C   ##  MRPT calculation
*C
*        if (ntype.eq.4) then
*C
*C     state averaging
*C
*C     normalize the weights
*      norm = 0.0d+00
*      do i= 1,ptnavst
*       norm = norm + ptwavst(i)
*      enddo
*      do i= 1,ptnavst
*       ptwavst(i) = ptwavst(i)/norm
*      enddo
*C
*C     write(nlist,'(//,1x,3(1h*),2x,
*C    &  "State averaging for reference density:",
*C    & 2x,3(1h*)//)')
*C
*C     write(nlist,
*C    & '(3x,''no.of aver. states'',3x,''weights'')')
*C     write(nlist,6057)(ptnavst,ptwavst(i),i=1, ptnavst)
*C6057  format(3x,i3,13x,8(f5.3,1x))
*C
*C    ##  check the validity of input parameter for MRPT
*C
*           if ( (ptlevl.lt.2) .or. (ptlevl.gt.3) )
*     &      call bummer('invalid ptlevl=',ptlevl,faterr)
*           if (ptiter.lt.0)
*     &      call bummer('invalid ptiter =',ptiter,faterr)
*           if (ptnvmx.le.0)
*     &      call bummer('invalid ptnvmx=',ptnvmx,faterr)
*           if (ptnvmx.gt.nvmax) then
*            ptnvmx = nvmax
*            call bummer('ptnvmx changed to nvmax=',nvmax,wrnerr)
*           endif
*           if( (pth0type.lt.0) .or. (pth0type.gt.1) )
*     &      call bummer('invalid pth0type=',pth0type,faterr)
*           if( (ptgref.lt.0) .or. (ptgref.gt.1) )
*     &      call bummer('invalid ptgref =',ptgref,faterr)
*           if( (ptlinsol.lt.0) .or. (ptlinsol.gt.1) )
*     &      call bummer('invalid ptlinsol=',ptlinsol,faterr)
*           if( (ptmeth.lt.0) .or. (ptmeth.gt.1) )
*     &      call bummer('invalid ptmeth =',ptmeth,faterr)
*           if ( (ptnavst.le.0) .or. (ptnavst.gt.mxavst))
*     &      call bummer('invalid ptnavst=',ptnavst,faterr)
*C
*C    ##  chack for valid combinations of input parameters for MRPT
*C
*           if (ptgref.eq.1) then
*              ivmode = 8
*              if (nrfitr.lt.2) nrfitr = 20
*              ptroot = max(froot,1)
*              nroot = max(ptroot,nroot,ptnavst)
*              nunitv = max(nunitv,nroot)
*           elseif (ptgref.eq.0) then
*        ivmode = 3
*              ptroot = max(froot,1)
*              nroot = max(ptroot,nroot,ptnavst)
*              nunitv = max(nunitv,nroot)
*           endif
*C
*           if ((ptmeth.eq.0).and.(ptnavst.gt.1)) then
*              write(nlist,
*     &         '(2x,''For ptnavst ='',i2,'' ptmeth has to be: 1 !'')')
*     &         ptnavst
*              call bummer('invalid ptmeth =',ptmeth,faterr)
*           endif
*        endif
*@endif

c   # set nvcimn and nvbkmn to appropriate values.
      nvcimn = max( nroot, min( nvcimn, nvcimx-1 ))
      if ( nvbkmn .lt. 0 ) then
         nvbkmn = nvcimn
      else
         nvbkmn = max( nroot, min( nvbkmn, nvbkmx-1 ))
      endif
      if ( nvrfmn .lt. 0 ) then
         nvrfmn = nvcimn
      else
         nvrfmn = max( nroot, min( nvrfmn, nvrfmx-1 ))
      endif
c
      mindim = min(nvbkmx,nvcimx,nvrfmx)
      nvbkmx = mindim
      nvcimx = mindim
      nvrfmx = mindim
c
c     # check for acpf calculation and set some values for that calc'n
c
c
c
c
c     # the value of 3 is used for the
c     # linearized versions of cepa0, acpf, aqcc and
c     #  aqcc-v incorporated into the ci code
c     #  using the standard input parameter
c     #  methodacpf=-1  userdefined gvalue = g(3)
c     #  methodacpf=1   cepa0
c     #  methodacpf=2   acpf
c     #  methodacpf=3 aqcc
c     #  methodacpf=4   aqcc-v

        if (ivmode .eq. 4) then
         nbkitr = 0
         call bummer('1:changed keyword: nbkitr=',0,WRNERR)
        endif
c
        if ( (ntype .eq. 3) ) then
        methodacpf = gset
        if (methodacpf.eq.-1) gvalue=g(3)
        davcor = 0
        nbkitr = 0
         call bummer('2:changed keyword: nbkitr=',0,WRNERR)
         call bummer('changed keyword: davcor=',0,WRNERR)
c
        if (noldv.eq.0) then
        if ( ((methodacpf.gt.4).or.(methodacpf.lt.1)) .and.
     .    (methodacpf.ne.-1))
     .  call bummer('invalid method for ntype=3, gset=',gset,2)
C     # force to generate start vector via diagonalization in reference
        if((ivmode.ne.3).and.(ivmode.ne.8).and.(ivmode.ne.4)) then
           if (ivmode.le.5) then
           call bummer('ivmode is changed to 3. it was',ivmode,wrnerr)
           ivmode = 3
           else
           call bummer('ivmode is changed to 8. it was',ivmode,wrnerr)
           ivmode = 8
           endif
        endif
        davcor = 0
        endif
        endif

         if ( ntype .gt. 4 ) then
         call bummer('invalid ntype specified',ntype,2)
         endif

        if ((ivmode.eq.8).or.(ivmode.eq.9)) then
          if (nrfitr.le.0)
     &  call bummer ('nrfitr.le.0 and ivmode=',ivmode,faterr)
        endif
ctmend
      if ( niter .le. 0 ) then
        davcor = 0
      endif
c
      write(nlist,6030)nrfitr, nvrfmx,nvrfmn ,
     &    lvlprt, nroot, noldv, noldhv,
     & nunitv, ntype, nbkitr, niter,ivmode,vout,istrt,iortls,
     & nvbkmx,ibktv,ibkthv,frcsub,nvcimx,icitv,icithv,maxseg,
     & iden, itran, froot,rtmode,
     & ftcalc, lrtshift, ncouple,skipso,ncorel,
     & csfprn,ctol,davcor
*@ifdef mrpt
*      if (ntype.eq.4)
*     & write(nlist,6031)
*     & pth0type,
*     & ptshift,ptmtst, ptiter,ptnvmx,
*     & ptmeth,ptgref,ptlinsol,pttol,ptlevl,ptnavst,
*     & (i,ptwavst(i),i=1,ptnavst)
*@endif
      write(nlist,6035)
     & (i, etolbk(i), etol(i), i=1, nroot)
 6030 format(/' ** list of control variables **'/
     +    ' nrfitr = ',i4,5x,' nvrfmx = ',i4,
     + 5x,' nvrfmn = ',i4 /
     +    ' lvlprt = ',i4,5x,' nroot  = ',i4,
     + 5x,' noldv  = ',i4,5x,' noldhv = ',i4/
     +    ' nunitv = ',i4,5x,' ntype  = ',i4,
     + 5x,' nbkitr = ',i4,5x,' niter  = ',i4/
     +    ' ivmode = ',i4,5x,' vout   = ',i4,
     + 5x,' istrt  = ',i4,5x,' iortls = ',i4/
     +    ' nvbkmx = ',i4,5x,' ibktv  = ',i4,
     + 5x,' ibkthv = ',i4,5x,' frcsub = ',i4/
     +    ' nvcimx = ',i4,5x,' icitv  = ',i4,
     + 5x,' icithv = ',i4,5x,' maxseg = ',i4,/
     +    ' iden   = ',i4,5x,' itran  = ',i4,
     + 5x,' froot  = ',i4,5x,' rtmode = ',i4,/
     +    ' ftcalc = ',i4,5x,' lrtshift=',f6.4,
     + 3x,' ncouple= ',i4,5x,' skipso  =',l4,/
     +    ' ncorel = ',i4,5x,' csfprn  =',i4,
     + 5x,' ctol   = ',1pe8.2,2x, 'davcor  =',i4/)

 6031 format( ' pth0type=',i4,
     + 5x,' ptshift= ',f6.4,2x,' ptmtst =',i4/
     +    ' ptiter=  ',i4,5x,' ptnvmx = ',i4,
     + 5x,' ptmeth = ',i4,5x,' ptgref = ',i4,/
     +    ' ptlinsol=',i3,5x,' pttol  = ',f10.8,
     + 3x,' ptlevl = ',i4,5x,' ptnavst= ',i4,//
     & ' ref.d1 averaging (ptwavst - for mrpt only):'/
     & '  state     weight'/
     & ' -------   --------'/(2x,i2,3x,f10.6))
 6035 format(/,
     & ' convergence tolerances of bk and full diagonalization steps'/
     + ' root #       rtolbk        rtol'/
     + ' ------      --------      ------'/(1x,i4,4x,1p,2e13.3) )
c
      if(aodrv .eq. 1) then
        write(nlist,*) ' '
        write(nlist,*) 'This is the AO-driven CI calculation USING'
        write(nlist,*) '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
      write(nlist,*) '###################'
      write(nlist,*) '# RANDOM INTEGRAL #'
      write(nlist,*) '###################  written by V.Parasuk'
      elseif (aodrv.eq.2) then
        write(nlist,*) ' '
        write(nlist,*) 'This is the AO-driven CI calculation USING'
        write(nlist,*) '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
      write(nlist,*) '###################'
      write(nlist,*) '# DOUBLE DIRECT   #'
      write(nlist,*) '###################  written by T. Mueller'
      endif
ctmstart

       if (cdg4ex.ne.0) then
         stest=.true.
         do i=1,4
           stest=stest.and.nsegd(i).eq.nseg4x(i)
         enddo
         if (.not.stest)
     .   call bummer('nsegd.ne.nseg4x and cdg4ex=1',0,2)
       endif

       if (c3ex1ex.ne.0) then
         stest=.true.
         do i=1,4
           stest=stest.and.nseg3x(i).eq.nseg1x(i)
         enddo
         if (.not.stest)
     .   call bummer('nseg3x.ne.nseg1x and c3ex1ex=1',0,2)
       endif

       if (c2ex0ex.ne.0) then
         stest=.true.
         do i=1,4
           stest=stest.and.nseg2x(i).eq.nseg0x(i)
         enddo
         if (.not.stest)
     .   call bummer('nseg2x.ne.nseg0x and c2ex0ex=1',0,2)
       endif

       do i=1,7
         if (fileloc(i).ne.0)
     .    call bummer(' fileloc <>0',i,2)
       enddo



ctm end

      write(nlist,*)
      write(nlist,*)'units and filenames:'
c
      do 110 i = 1, (nfilmx - 1)
         if ( nunits(i) .eq. 0 ) goto 110
         write(nlist,6050) i, nunits(i), fname(i)
110   continue
6050  format(1x,i4,': (',i2,')',4x,a)
6051  format(1x,i4,': (',i2,')',4x,a,t35,a)
c
      write(nlist,6020)
      close(unit=nin)

      return
6020  format(1x,72('-'))
      end

      subroutine opncom(intrln,vecrln,indxln,nexrln,ffrln)
c
c open files common to ci and acpf
c
         IMPLICIT NONE
C

C
C     Parameter variables
C
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER         ISFLN
      PARAMETER           (ISFLN = 4)
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
C
C     Argument variables
C
      INTEGER         FFRLN,       INDXLN,      INTRLN,      NEXRLN
      INTEGER         VECRLN
C
C
C     Equivalenced common variables
C
      INTEGER         AOFILE,      CIREFV,      FILDIA,      FILEV
      INTEGER         FILEW,       FILIND,      FILINT,      FILLP
      INTEGER         FILLPD,      FILSC4,      FILSC5,      FLINDX
      INTEGER         FOCKDG,      IFIL3W,      IFIL3X,      IFIL4W
      INTEGER         IFIL4X,      PSEUDG,      DRTFIL

C
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

C     Common variables
C
      CHARACTER*60        FNAME(NFILMX)
C
      COMMON /CFNAME /  FNAME
C
c      fname(1)    ciudgls
c            2     ciudgin
c            3     ciudgsm
c            4     cihdiag
c            5     cihpseu
c            6     cihvfl
c            7     civfl
c            8     diagint
c            9     ofdgint
c           10     cidrtfl
c           11     ciftdfl
c           12     ciftofl
c           13     ciftifl
c           14     ciflind
c           15     civin
c           16     civout
c           17     fockdg
c           18     d1fl
c           20     cifvfl
c           21     ciscr4
c           22     ciftotd
c           23     flacpfd
c           26     ciscr5
c           27     restart
c           28     civout
c           29     cirefv
c           31     fil4w
c           32     fil4x
c           33     fil3w
c           34     fil3x
c           36     aoints
c           37     aoints2
c           38     drtfil
c           39     cisrtif
c           42     mocoef
c           45     nocoef_ci
c

C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

C     Common variables
C
      INTEGER         AODRV
C
      COMMON /VPPASS /  AODRV
C
c
c  AODRV: AO-driven procedure
c
C     Common variables
C
      INTEGER         CSFPRN,      DAVCOR,      FRCSUB,      FROOT
      INTEGER         FTCALC,      IBKTHV,      IBKTV,       ICITHV
      INTEGER         ICITV,       IORTLS,      ISTRT,       IVMODE
      INTEGER         MAXSEG,      NBKITR,      NCOUPLE,     NITER
      INTEGER         NO0EX,       NO1EX,       NO2EX,       NO3EX
      INTEGER         NO4EX,       NOLDHV,      NOLDV,       NRFITR
      INTEGER         NROOT,       NTYPE,       NUNITV,      NVBKMN
      INTEGER         NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER         NVRFMX,      RTMODE,      VOUT
      INTEGER         NSEGD(4),    NSEG0X(4),   NSEG1X(4),NSEG2X(4)
      INTEGER         NSEG3X(4),   NSEG4X(4),   NSEGSO(4),NODIAG
      integer             cdg4ex,c3ex1ex,c2ex0ex,fileloc(10)
      integer             finalv,finalw
C
      REAL*8            CTOL,        ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8            LRTSHIFT
C
      COMMON /INDATA /  LRTSHIFT,    ETOLBK,      ETOL,        CTOL
      COMMON /INDATA /  NROOT,       NOLDV,       NOLDHV,      NUNITV
      COMMON /INDATA /  NBKITR,      NITER,       DAVCOR,      CSFPRN
      COMMON /INDATA /  IVMODE,      ISTRT,       VOUT,        IORTLS
      COMMON /INDATA /  NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
      COMMON /INDATA /  ICITV,       ICITHV,      FRCSUB,      NVBKMN
      COMMON /INDATA /  NVCIMN,      MAXSEG,      NTYPE,       NRFITR
      COMMON /INDATA /  NVRFMX,      NVRFMN,      FROOT,       FTCALC
      COMMON /INDATA /  RTMODE,      NCOUPLE,     NO0EX,       NO1EX
      COMMON /INDATA /  NO2EX,       NO3EX,       NO4EX,       NODIAG
      COMMON /INDATA /  NSEGD, NSEG0X, NSEG1X, NSEG2X, NSEG3X, NSEG4X
      COMMON /INDATA /  NSEGSO,cdg4ex,c3ex1ex,c2ex0ex,fileloc
      COMMON /INDATA /  FINALV, FINALW
C
C     Equivalenced common variables
C
      INTEGER         MODE,        NCIITR,      NINITV
C
      REAL*8            RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8            RTOLCI(NVMAX)
C

c LRTSHIFT,      aqcc/lrt shift  (falls <0 )
c ETOLBK,        etol-Grenzwert fuer BK iteration
c  ETOL,         etol-Grenzwert fuer CI iteration
c       CTOL     ctol-Grenzwert fuer CSF Analyse
c NROOT,         Zahl der zu berechnenden Wurzeln
c    NOLDV,      Zahl der vorhandenen CI-Vektoren auf civfl
c       NOLDHV,  Zahl der vorhandenen Sigma-Vektoren auf cihvfl
c     NUNITV     Zahl der zu generierenden Startvektoren
c NBKITR,        max. Zahl der BK Iterationen
c    NITER,      max. Zahl der CI Iterationen
c       DAVCOR,  Davidson-Korrektur berechnen
c     CSFPRN     CI-Vektor analysieren
c IVMODE,        Startvektorgenerierung
c     ISTRT,     Startvektorgenerierung bei ivmode=0
c      VOUT,     ????
c       IORTLS   defaults for ICITV,ICITHV
c NVBKMX,        maximale Subspace dimension in BK-Iteratione
c    IBKTV,      kontrolliert transformation der BK V-Vektoren
c       IBKTHV,  kontrolliert transformation der BK Sigma-Vektoren
c     NVCIMX     maximale Subspace dimension in CI-Iteration
c ICITV,         kontrolliert transformation der CI V-Vektoren
c      ICITHV,   kontrolliert transformation der CI Sigma-Vektoren
c     FRCSUB,    kontrolliert Neuberechnung der Subspace-Darstellung
c      NVBKMN    min. Groesze des BK subspaces
c NVCIMN,        min. Groesze des CI subspaces
c     MAXSEG,    max. Zahl von CI Segmenten
c     NTYPE,     Rechnungstyp
c       NRFITR   max. Zahl der CI Iterationen bei iterat. Referenzraumdi
c NVRFMX,        max. Groesze es Subspaces bei iterat. Referenzraumdiagona
c    NVRFMN,     min. Groesze des Subspaces bei iterat. Referenzraumdiagon
c    FROOT,      Root to follow
c       FTCALC   Berechnung des Formulatapes on the fly
c RTMODE,        obsolet
c    NCOUPLE,    Iteration in der der Lrtshift zum ersten Mal anzuwenden
c     NO0EX,     keine Berechnung der 0externen Beitraege
c     NO1EX      keine Berechnung der 1externen Beitraege
c NO2EX,         keine Berechnung der 2externen Beitraege
c     NO3EX,     keine Berechnung der 3externen Beitraege
c     NO4EX,     keine Berechnung der 4externen Beitraege
c      NODIAG    keine Berechnung der diagonalen Beitraege
c NSEGD,         Segmentierungschema fuer diagonale Beitraege
c NSEG0X,        Segmentierungsschema fuer all-interne Beitraege
c NSEG1X,        ... 1externe ....
cNSEG2X,         ... 2externe ....
cNSEG3X,         ... 3externe ....
c NSEG4X         ... 4externe ....
c NSEGSO,        ...  ...
ccdg4ex,         Behandle diagonalfall und 4-externen Fall gemeinsam
cc3ex1ex,        Behandle 3-externen Fall und 1-externen Fall gemeinsam
cc2ex0ex,        Behandle 2-externen Fall und 0-externen Fall gemeinsam
cfileloc         Filetyp fuer   diagint,ofdgint,fil3w,fil3x,fil4w,fil4x
c                  mit 1 = vdisk  2= ga  3=distrib. (one-proc-sys) 4=dis
cfinalv          final v vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk
cfinalw          final w vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk


      equivalence (fildia,nunits(4))
      equivalence (pseudg,nunits(5))
      equivalence (filew,nunits(6))
      equivalence (filev,nunits(7))
      equivalence (filind,nunits(8))
      equivalence (filint,nunits(9))
      equivalence (fillpd,nunits(11))
      equivalence (fillp,nunits(12))
      equivalence (flindx,nunits(14))
      equivalence (fockdg,nunits(17))
      equivalence (filsc4,nunits(21))
      equivalence (filsc5,nunits(26))
      equivalence (cirefv,nunits(29))
      equivalence (ifil4w,nunits(31))
      equivalence (ifil4x,nunits(32))
      equivalence (ifil3w,nunits(33))
      equivalence (ifil3x,nunits(34))
      equivalence (aofile, nunits(36))
      equivalence (drtfil, nunits(38))
c
c michal2{
c
c       integer      cosmofile
c       equivalence (cosmofile, nunits(54))
c
        integer cosmocalc
        common/cosmo2/cosmocalc
c
c michal2}
c
C
c
c
c     # diagonal integral file
      dalen(filind) = intrln
      dalen(filev) = vecrln
      dalen(filew) = vecrln
      dalen(fildia) = vecrln
c     # off-diagonal integral file
      dalen(filint) = intrln

c     # index file
      dalen(flindx) = indxln
      call openda(flindx,fname(14),dalen(flindx),'keep','random')


c     # drtfil file
      dalen(drtfil) = indxln
      call openda(drtfil,fname(38),indxln,'keep','random')

c
c
c     # open ifil4x, ifil4w, ifil3x and ifil3w using record length
c     # maxbuf from tapin and the appropriate filename.
c     # asynchronous i/o not actually implemented but calls retained
c     # to make implementation in the future easier
c
      return
      end
      subroutine gentasklist
c
c  compute a hamiltonian matrix-vector product.
c
c   aodriven is implemented here. vp- 18-apr-95
c
c iskip.ge. 1     normal calculation
c       eq. 0     'bk' calculation
c       eq.-1     calculation only for z walks
c       eq.-2     construct h matrix on common jpathz,
c                   including diagonal elements!
c       eq.-3     only reflst(*) elements
c       eq.-4     return diagonal elements in heap(1..)
cmd
c       eq. 2     calculate only 1-e contributions to Hv
cmd
c    #  bra >= ket !
c
         IMPLICIT NONE

      integer loc1,adr1,iseg1,iseg2,st1
      integer itask,isegm,isgt,iseg,i
      character*1 segchar(4)
      integer nvmax
      parameter (nvmax=40)
      integer mxnseg
      parameter (MXNSEG = 200)
      character*2  segtype2(7)
      character*12 descr(110)
       common /description/ descr


      INTEGER MAXTASK
      parameter (maxtask=16384)
      INTEGER BSEG(MAXTASK), KSEG(MAXTASK),
     .        TASK(MAXTASK),NTASK,
     .        SLICE(MAXTASK),LOOPCNT(MAXTASK),
     .        CURRTSK
      REAL*8 TASKTIMES(MAXTASK),MFLOP(MAXTASK)
      COMMON/TASKLST/MFLOP,
     .      TASKTIMES,BSEG,KSEG,TASK,SLICE,
     .      LOOPCNT,NTASK,CURRTSK


c
c tasklist
c BSEG      bra segment
c KSEG      ket segment
c TASK      task type
c NTASK     total number of task
c SLICE     segmentation type
c loopcnt   loopcnt for debugging
c currtsk   current task
c tasktimes time spent in task
c
c
       INTEGER   TIMES
       CHARACTER*30 TDESCR
       COMMON /TIMERLST/   TIMES(0:20), TDESCR(0:20)
c
c  timer for standard ci

c

C     Common variables
C
      INTEGER         CSFPRN,      DAVCOR,      FRCSUB,      FROOT
      INTEGER         FTCALC,      IBKTHV,      IBKTV,       ICITHV
      INTEGER         ICITV,       IORTLS,      ISTRT,       IVMODE
      INTEGER         MAXSEG,      NBKITR,      NCOUPLE,     NITER
      INTEGER         NO0EX,       NO1EX,       NO2EX,       NO3EX
      INTEGER         NO4EX,       NOLDHV,      NOLDV,       NRFITR
      INTEGER         NROOT,       NTYPE,       NUNITV,      NVBKMN
      INTEGER         NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER         NVRFMX,      RTMODE,      VOUT
      INTEGER         NSEGD(4),    NSEG0X(4),   NSEG1X(4),NSEG2X(4)
      INTEGER         NSEG3X(4),   NSEG4X(4),   NSEGSO(4),NODIAG
      integer             cdg4ex,c3ex1ex,c2ex0ex,fileloc(10)
      integer             finalv,finalw
C
      REAL*8            CTOL,        ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8            LRTSHIFT
C
      COMMON /INDATA /  LRTSHIFT,    ETOLBK,      ETOL,        CTOL
      COMMON /INDATA /  NROOT,       NOLDV,       NOLDHV,      NUNITV
      COMMON /INDATA /  NBKITR,      NITER,       DAVCOR,      CSFPRN
      COMMON /INDATA /  IVMODE,      ISTRT,       VOUT,        IORTLS
      COMMON /INDATA /  NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
      COMMON /INDATA /  ICITV,       ICITHV,      FRCSUB,      NVBKMN
      COMMON /INDATA /  NVCIMN,      MAXSEG,      NTYPE,       NRFITR
      COMMON /INDATA /  NVRFMX,      NVRFMN,      FROOT,       FTCALC
      COMMON /INDATA /  RTMODE,      NCOUPLE,     NO0EX,       NO1EX
      COMMON /INDATA /  NO2EX,       NO3EX,       NO4EX,       NODIAG
      COMMON /INDATA /  NSEGD, NSEG0X, NSEG1X, NSEG2X, NSEG3X, NSEG4X
      COMMON /INDATA /  NSEGSO,cdg4ex,c3ex1ex,c2ex0ex,fileloc
      COMMON /INDATA /  FINALV, FINALW
C
C     Equivalenced common variables
C
      INTEGER         MODE,        NCIITR,      NINITV
C
      REAL*8            RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8            RTOLCI(NVMAX)
C

c LRTSHIFT,      aqcc/lrt shift  (falls <0 )
c ETOLBK,        etol-Grenzwert fuer BK iteration
c  ETOL,         etol-Grenzwert fuer CI iteration
c       CTOL     ctol-Grenzwert fuer CSF Analyse
c NROOT,         Zahl der zu berechnenden Wurzeln
c    NOLDV,      Zahl der vorhandenen CI-Vektoren auf civfl
c       NOLDHV,  Zahl der vorhandenen Sigma-Vektoren auf cihvfl
c     NUNITV     Zahl der zu generierenden Startvektoren
c NBKITR,        max. Zahl der BK Iterationen
c    NITER,      max. Zahl der CI Iterationen
c       DAVCOR,  Davidson-Korrektur berechnen
c     CSFPRN     CI-Vektor analysieren
c IVMODE,        Startvektorgenerierung
c     ISTRT,     Startvektorgenerierung bei ivmode=0
c      VOUT,     ????
c       IORTLS   defaults for ICITV,ICITHV
c NVBKMX,        maximale Subspace dimension in BK-Iteratione
c    IBKTV,      kontrolliert transformation der BK V-Vektoren
c       IBKTHV,  kontrolliert transformation der BK Sigma-Vektoren
c     NVCIMX     maximale Subspace dimension in CI-Iteration
c ICITV,         kontrolliert transformation der CI V-Vektoren
c      ICITHV,   kontrolliert transformation der CI Sigma-Vektoren
c     FRCSUB,    kontrolliert Neuberechnung der Subspace-Darstellung
c      NVBKMN    min. Groesze des BK subspaces
c NVCIMN,        min. Groesze des CI subspaces
c     MAXSEG,    max. Zahl von CI Segmenten
c     NTYPE,     Rechnungstyp
c       NRFITR   max. Zahl der CI Iterationen bei iterat. Referenzraumdi
c NVRFMX,        max. Groesze es Subspaces bei iterat. Referenzraumdiagona
c    NVRFMN,     min. Groesze des Subspaces bei iterat. Referenzraumdiagon
c    FROOT,      Root to follow
c       FTCALC   Berechnung des Formulatapes on the fly
c RTMODE,        obsolet
c    NCOUPLE,    Iteration in der der Lrtshift zum ersten Mal anzuwenden
c     NO0EX,     keine Berechnung der 0externen Beitraege
c     NO1EX      keine Berechnung der 1externen Beitraege
c NO2EX,         keine Berechnung der 2externen Beitraege
c     NO3EX,     keine Berechnung der 3externen Beitraege
c     NO4EX,     keine Berechnung der 4externen Beitraege
c      NODIAG    keine Berechnung der diagonalen Beitraege
c NSEGD,         Segmentierungschema fuer diagonale Beitraege
c NSEG0X,        Segmentierungsschema fuer all-interne Beitraege
c NSEG1X,        ... 1externe ....
cNSEG2X,         ... 2externe ....
cNSEG3X,         ... 3externe ....
c NSEG4X         ... 4externe ....
c NSEGSO,        ...  ...
ccdg4ex,         Behandle diagonalfall und 4-externen Fall gemeinsam
cc3ex1ex,        Behandle 3-externen Fall und 1-externen Fall gemeinsam
cc2ex0ex,        Behandle 2-externen Fall und 0-externen Fall gemeinsam
cfileloc         Filetyp fuer   diagint,ofdgint,fil3w,fil3x,fil4w,fil4x
c                  mit 1 = vdisk  2= ga  3=distrib. (one-proc-sys) 4=dis
cfinalv          final v vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk
cfinalw          final w vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk

      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB    CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
c
C     Common variables
C
      INTEGER         LENCI, NSEG(7)
      INTEGER         SEGCI(MXNSEG,7),CIST(MXNSEG,7)
      INTEGER         SEGEL(MXNSEG,7),            SEGSCR(MXNSEG,7)
      INTEGER         SEGTYP(MXNSEG,7),       IPTHST(MXNSEG,7)
      INTEGER         DRTST(MXNSEG,7), DRTLEN(MXNSEG,7)
      INTEGER         DRTIDX(MXNSEG,7)
C
      COMMON /CIVCT  /  LENCI,       NSEG,  SEGEL, CIST
      COMMON /CIVCT  /  SEGCI,       SEGSCR,      SEGTYP, IPTHST
      COMMON /CIVCT /   DRTST, DRTLEN, DRTIDX
C
c
c   LENCI                   length of CI vector
c NSEG(I)                   total number of segments in segmentation sch
c   SEGCI(i,j)         number of CI elements in segment j of segment. sc
c   CIST(MXNSEG,7)     offset of first CI element corresponding to segme
c    SEGEL(MXNSEG,7),  number of internal paths in segment j of seg sche
c    SEGSCR(MXNSEG,7)   AO-driven stuff
c    SEGTYP(MXNSEG,7), segment type of segment j, seg scheme i
c       IPTHST(MXNSEG,7) offset of first internal path in index segment
c                        belonging to segment j of seg. scheme i
c    DRTST(MXNSEG,7),    offset of first element of DRT information on f
c                        belonging to segment j of seg scheme i
c    DRTLEN(MXNSEG,7)    number of elements of DRT info on file drtfil f
c    DRTIDX(MXNSEG,7)    length of index vector segment for segment j se
c
C     Common variables
C
      INTEGER         ICNFST(MXNSEG,7), ISGCNF(MXNSEG,7)
C
      COMMON /CNFSPC /  ICNFST,      ISGCNF
C
c
c  icnfst(i,j)  number of the first valid internal walk for segment i sc
c  isgcnf(i,j)  number of valid internal walks for segment i scheme j
c


      data segtype2 /'DG','OX','1X','2X','3X','4X','SO'/
      data segchar /'Z','Y','X','W'/
      call izero_wr(21,times,1)
      call wzero(maxtask,tasktimes,1)
      do i=1,110
         descr(i)='            '
      enddo
       descr(1)=  'allint zz   '
       descr(2)=  'allint yy   '
       descr(3)=  'allint xx   '
       descr(4)=  'allint ww   '

       descr(5)=  '0ex2ex yy   '
       descr(6)=  '0ex2ex xx   '
       descr(7)=  '0ex2ex ww   '

       descr(11)=  'one-ext yz  '
       descr(13)=  'one-ext yx  '
       descr(14)=  'one-ext yw  '

       descr(15)=  '1ex3ex  yx  '
       descr(16)=  '1ex3ex  yw  '

       descr(21)=  'two-ext yy  '
       descr(22)=  'two-ext xx  '
       descr(23)=  'two-ext ww  '
       descr(24)=  'two-ext xz  '
       descr(25)=  'two-ext wz  '
       descr(26)=  'two-ext wx  '
       descr(31)=  'thr-ext yx  '
       descr(32)=  'thr-ext yw  '
       descr(41)=  'four-ext z  '
       descr(42)=  'four-ext y  '
       descr(43)=  'four-ext x  '
       descr(44)=  'four-ext w  '

       descr(45)=  '4exdg024 y  '
       descr(46)=  '4exdg024 x  '
       descr(47)=  '4exdg024 w  '

       descr(52)=  'dg-4ext y   '
       descr(53)=  'dg-4ext x   '
       descr(54)=  'dg-4ext w   '
       descr(62)=  'dg-2ext y   '
       descr(63)=  'dg-2ext x   '
       descr(64)=  'dg-2ext w   '
       descr(71)=  'dg-0ext z   '
       descr(72)=  'dg-0ext y   '
       descr(73)=  'dg-0ext x   '
       descr(74)=  'dg-0ext w   '

       descr(75)=  'dg-024ext z '
       descr(76)=  'dg-024ext y '
       descr(77)=  'dg-024ext x '
       descr(78)=  'dg-024ext w '

       descr(81)=  'so-0ext zz  '
       descr(82)=  'so-0ext yy  '
       descr(83)=  'so-0ext xx  '
       descr(84)=  'so-0ext ww  '
       descr(91)=  'so-1ext zy  '
       descr(93)=  'so-1ext yx  '
       descr(94)=  'so-1ext yw  '
       descr(101)= 'so-2ext yy  '
       descr(102)= 'so-2ext xx  '
       descr(103)= 'so-2ext wx  '
c
      loc1  =  1
      adr1 = loc1
         ntask=0

c        # diagonal elements: dg4x  y(52) x(53) w(54)
c                             dg2x  y(62) x(63) w(64)
c                             dg0x  z(71) y(72) x(73) w(74)
c          combined tasks     dg024x  z(75) y(76) x(77) w(78)
c
c
c        # 2-external yy(21) ww(22) xx(23) xz (24) wz (25) wx(26)
c
         if (c2ex0ex.eq.0) then
         st1=0
         do isgt=2,4
         st1=st1+nseg2x(isgt-1)
         do iseg1 = 1, nseg2x(isgt)
           do iseg2 = iseg1, nseg2x(isgt)
             ntask=ntask+1
             bseg(ntask)=st1+iseg2
             kseg(ntask)=st1+iseg1
             task(ntask)=20+isgt-1
             slice(ntask)=4
           enddo
         enddo
         enddo
         endif

         st1=nseg2x(1)
         do isgt=3,4
          st1=st1+nseg2x(isgt-1)
          do iseg1 = 1, nseg2x(isgt)
            do iseg2=1,nseg2x(1)
            ntask=ntask+1
            bseg(ntask)=st1+iseg1
            kseg(ntask)=iseg2
            task(ntask)=21+isgt
             slice(ntask)=4
            enddo
          enddo
         enddo

         st1=nseg2x(1)+nseg2x(2)
          do iseg1=1,nseg2x(3)
           do iseg2 = 1,nseg2x(4)
           ntask=ntask+1
           bseg(ntask)=iseg2+st1+nseg2x(3)
           kseg(ntask)=iseg1+st1
           task(ntask)=26
             slice(ntask)=4
           enddo
          enddo




cc
c       # 1-external: yz (11) yx (13) yw (14)
c
         st1=0
         do     isgt=1,4
           if (isgt.eq.2) goto 201
           if (isgt.gt.2 .and. c3ex1ex.eq.1) goto 201
           do iseg1=1,nseg1x(2)
             do iseg2=1,nseg1x(isgt)
             ntask=ntask+1
             task(ntask)=10+isgt
             if (isgt.eq.1) then
             kseg(ntask)=st1+iseg2
             bseg(ntask)=nseg1x(1)+iseg1
             slice(ntask)=3
             else
             bseg(ntask)=st1+iseg2
             kseg(ntask)=nseg1x(1)+iseg1
             slice(ntask)=3
             endif
             enddo
           enddo
 201     st1=st1+nseg1x(isgt)
         enddo


c
c        # 3-external:  yx (31) yw(32)
c
         st1=nseg3x(1)+nseg3x(2)

         do iseg1 = 1,nseg3x(3)
           do iseg2 = 1,nseg3x(2)
            ntask = ntask+1
            if (c3ex1ex.eq.0) then
            task(ntask) = 31
            else
            task(ntask) = 15
            endif
            bseg(ntask) = st1+iseg1
            kseg(ntask) = nseg3x(1)+iseg2
             slice(ntask)=5
           enddo
         enddo
         st1=nseg3x(1)+nseg3x(2)+nseg3x(3)
         do iseg1 = 1,nseg3x(4)
           do iseg2 = 1,nseg3x(2)
            ntask = ntask+1
            if (c3ex1ex.eq.0) then
            task(ntask) = 32
            else
            task(ntask) = 16
            endif
            bseg(ntask) = st1+iseg1
            kseg(ntask) = nseg3x(1)+iseg2
            slice(ntask)=5
           enddo
         enddo

c
c       # 0-external zz (1) yy(2) xx(3) ww(4)
c
          st1=0
          do isgt=1,4
            do iseg1 = 1, nseg0x(isgt)
              do iseg2 = iseg1,nseg0x(isgt)
              ntask=ntask+1
              if (c2ex0ex.eq.0 .or. isgt.eq.1) then
              task(ntask)=isgt
              else
              task(ntask)=isgt+3
              endif
              bseg(ntask)=iseg2+st1
              kseg(ntask)=iseg1+st1
             slice(ntask)=2
              enddo
            enddo
            st1=st1+nseg0x(isgt)
           enddo





c
c  generate combined tasks for diag, only
c
         isegm=0
         do isgt =1, 4
          do 200 iseg=1,nsegd(isgt)
             isegm=isegm+1
             ntask=ntask+1
             if (cdg4ex.eq.0 .or. isgt.eq.1) then
              task(ntask)=74+isgt
             else
              task(ntask)=43+isgt
             endif
             bseg(ntask)=isegm
             kseg(ntask)=isegm
             slice(ntask)=1
 200      continue
         enddo

        if (cdg4ex.eq.0) then

c        # 4-external :   y(42) x(43) w(44)
c
         isegm=nseg4x(1)
         do isgt =2, 4
          do iseg=1,nseg4x(isgt)
             ntask=ntask+1
             isegm=isegm+1
             task(ntask)=40+isgt
             bseg(ntask)=isegm
             kseg(ntask)=isegm
             slice(ntask)=6
          enddo
         enddo
        endif

        if (spnorb) then
c
c       # so-case so0x zz (81) yy (82) xx (83) ww (84)
c                 so1x yz (91) yx (93) yw (94)
c                 so2x yy (101) xx (102) wx (103)

          st1=-nsegso(1)
          do isgt=1,4
            st1=st1+nsegso(isgt)
            do iseg1 = 1, nsegso(isgt)
              do 202 iseg2 = iseg1,nsegso(isgt)
              ntask=ntask+1
              task(ntask)=isgt+80
              bseg(ntask)=iseg2+st1
              kseg(ntask)=iseg1+st1
                slice(ntask)=7
              if (isgt.eq.1) goto 202
              if (isgt.eq.2) then
                ntask=ntask+1
                task(ntask)=101
                bseg(ntask)=iseg2+st1
                kseg(ntask)=iseg1+st1
                slice(ntask)=7
              endif
              if (isgt.eq.3) then
                ntask=ntask+1
                task(ntask)=102
                bseg(ntask)=iseg2+st1
                kseg(ntask)=iseg1+st1
                slice(ntask)=7
              endif
 202          continue
            enddo
           enddo

         st1=-nsegso(1)
         do 203 isgt=1,4
           st1=st1+nsegso(isgt)
           if (isgt.eq.2) goto 203
           do iseg1=1,nsegso(2)
             do iseg2=1,nsegso(isgt)
             ntask=ntask+1
             task(ntask)=90+isgt
             if (isgt.eq.1) then
             kseg(ntask)=st1+iseg2
             bseg(ntask)=nsegso(1)+iseg1
                slice(ntask)=7
             else
             bseg(ntask)=st1+iseg2
             kseg(ntask)=nsegso(1)+iseg1
                slice(ntask)=7
             endif
             enddo
           enddo
 203     continue

         st1=nsegso(1)+nsegso(2)
          do iseg1=1,nsegso(3)
           do iseg2 = 1,nsegso(4)
           ntask=ntask+1
           bseg(ntask)=iseg2+st1+nsegso(3)
           kseg(ntask)=iseg1+st1
                slice(ntask)=7
           task(ntask)=103
           enddo
          enddo

        endif ! SOCI
       if (ntask.gt.maxtask)
     .  call bummer('tasklist: maxtasks exceeded, ntask=',ntask,2)
       write(6,1001)
         do itask=1,ntask
          loopcnt(itask)=1
          write(6,1000) itask,bseg(itask),kseg(itask),
     .        task(itask),descr(task(itask)),segtype2(slice(itask)),
     .  segtyp(bseg(itask),slice(itask)),
     .  segtyp(kseg(itask),slice(itask)),
     .  segel(bseg(itask),slice(itask)),
     .  segel(kseg(itask),slice(itask)),
     .  segci(bseg(itask),slice(itask)),
     .  segci(kseg(itask),slice(itask)),
     .  isgcnf(bseg(itask),slice(itask)),
     .  isgcnf(kseg(itask),slice(itask))
       enddo
       write(6,1002)
1001   format(20x,'TASKLIST'/ 25('----')/
     .  'TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE',
     .  '    SEGEL              SEGCI          VWALKS' /
     .  25('----'))
1002   format(25('----'))
1000   format(i6,i3,1x,i3,1x,i5,6x,a12,1x,a2,1x,i2,i2,1x,i7,1x,i7,1x,
     .   i10,1x,i10,1x,i7,1x,i7)

       return
       end



        subroutine initvdsk(dgmode,odgmode,me,buffer,ilen)

       implicit none
       integer i,mxunit,nfilmx
       parameter (mxunit=99,nfilmx=55)
       integer filind,nrec,irec,fadr,bufsz,restofrec
       integer totsize,fin,dgmode,odgmode
       integer me,ilen,vproc,iirec,iiblock,inrec
       real*8 buffer(ilen)
       integer rl,fadr1,dgrec,odgrec
       external rl
       integer ibls,isiz(7),iblock,filint
       integer mdtob,mitob
       external mdtob,mitob
       character*9 ityp(7)


       real*8   vdsk(2)
       integer  isflvdsk(mxunit),voffset,vsize,top
       integer  filestart(mxunit)
*@ifdef pointer
*@ifdef bit64
*@ifndef sp4
*         integer*8 mem2
*@endif
*@else
*        integer*4 mem2
*@endif
*       pointer (mem2,vdsk)
*       COMMON /VPOINTER/ mem2,isflvdsk,voffset,vsize,filestart,top
*@else
       COMMON /VPOINTER/ vdsk,isflvdsk,voffset,vsize,filestart,top
*@endif

C     Common variables
C
      INTEGER         BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER         INTORB,      LAMDA1
      INTEGER         MAXBL2,      MAXLP3
      INTEGER         MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER         MXORB,       N0EXT
      INTEGER         N1EXT,       N2EXT
      INTEGER         N3EXT,       N4EXT
      INTEGER         ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER         NEXT,        NFCT,        NINTPT
      INTEGER         NMONEL,      NVALW,       NVALWK
      INTEGER         NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER         PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER         SPCCI,       TOTSPC
C
      COMMON /INF    /  PTHZ,        PTHY,        PTHX,        PTHW
      COMMON /INF    /  NINTPT,      DIMCI
      COMMON /INF    /  LAMDA1,      BUFSZI
      COMMON /INF    /  NMONEL,      INTORB
      COMMON /INF    /  MAXLP3,      BUFSZL
      COMMON /INF    /  MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON /INF    /  NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON /INF    /  ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON /INF    /  N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON /INF    /  SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON /INF    /  NVALZ,       NVALY,       NVALX,       NVALW
      COMMON /INF    /  NFCT,        NELI
C
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c      N1EXT,     number of one-external off-diagonal integrals
c     N2EXT       number of two-external off-diagonal integrals
c     N3EXT,      number of three-external off-diagonal integrals
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)


C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

C     Common variables
C
      INTEGER         N0INT,       N0LP,        N1INT,       N1LP
      INTEGER         N2INT,       N2LP
C
      COMMON /SOINF  /  N2INT,       N1INT,       N0INT,       N2LP
      COMMON /SOINF  /  N1LP,        N0LP

C     Common variables
C
      CHARACTER*60        FNAME(NFILMX)
C
      COMMON /CFNAME /  FNAME

C

C
c
c  n2int   two-internal so ints
c  n1int   one-internal so ints
c  n0int   all-external so ints
c  n2lp    number of two-internal loops
c  n1lp    number of one-internal loops
c  n0lp    number of zero-internal loops
c
C
       integer vadr,gadr


       filind=nunits(8)
       bufsz = dalen(filind)
       isflvdsk(filind)=0
       vadr=-1
       gadr=-1
c      call openda(filind,fname(8),dalen(filind),'keep','random')
       filint=nunits(9)
       bufsz = dalen(filint)
c      call openda(filint,fname(9),dalen(filint),'keep','random')
       return
       end

         subroutine initga34x(mode4x,mode4w,mode3x,mode3w,
     .                        buffer,maxbuf,nbuf)
         implicit none
          integer nfilmx,nvmax
         parameter (nfilmx=55,nvmax=40)
         integer mode4x,mode4w,mode3x,mode3w

       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex

        integer mitob
        integer mdtob
C
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c
C     Common variables
C
      CHARACTER*60        FNAME(NFILMX)
C
      COMMON /CFNAME /  FNAME
C

c
c


C
         integer maxbuf,unpack(2),ilstp,nbuf
         real*8 buffer(maxbuf,nbuf)
         integer ifil4w,ifil4x,ifil3x,ifil3w
         integer icnt,fsize,icnt2,i,j
         external fsize

c      file locations
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w
c      value:   0 local disk  1: virtual disk  2: global array
c               3 distribute to local disk (except 0)
c      default:      1    1    2     2     2      2

      equivalence (ifil4w,nunits(31))
      equivalence (ifil4x,nunits(32))
      equivalence (ifil3w,nunits(33))
      equivalence (ifil3x,nunits(34))

c     call openda(ifil3x,fname(34),maxbuf,'keep','random')
c     call openda(ifil3w,fname(33),maxbuf,'keep','random')
c     call openda(ifil4x,fname(32),maxbuf,'keep','random')
c     call openda(ifil4w,fname(31),maxbuf,'keep','random')
      return
      end
       subroutine inittask
       implicit none 
       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex

        integer mitob
        integer mdtob
C
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c

       logical status

         if (me.eq.0 ) then
          call ga_put(nxttask,1,1,1,1,1,1)
         endif
        call ga_sync()
c      if (me.eq.0) write(6,*) 'counter reinitialized'
       return
       end
       subroutine getvecp(unit,logrec,vector,start,length)
       implicit none
        integer unit,logrec,start,length
       real*8 vector(*)
       integer nvmax,mxunit
       parameter (nvmax=40,mxunit=99)

        integer mitob
        integer mdtob
C
c      file locations
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w,drtfil
c      value:   0 local disk  1: virtual disk  2: global array
c               3 distribute one proc sys
c               4 distribute two proc sys
c               5 distribute four proc sys
c      default:      1    1    2     2     2      2
c
c      flofdg=1
c      fldiag=2
c      fl3x=3
c      fl3w=4
c      fl4x=5
c      fl4w=6
c      fldrt=7
c      ldsk=0
c      vdsk=1
c      glar=2
c      done=3
c      dtwo=4
c      dfor=5
       integer flofdg,fldiag,fl3x,fl3w,fl4x,fl4w,fldrt
       integer ldsk,vdk,glar,done,dtwo,dfor
       parameter (flofdg=2,fldiag=1,fl3x=3,fl3w=4,fl4x=5,fl4w=6)
       parameter (fldrt=7,ldsk=0,vdk=1,glar=2,done=3,dtwo=4,dfor=5)

C     Common variables
C
      INTEGER         CSFPRN,      DAVCOR,      FRCSUB,      FROOT
      INTEGER         FTCALC,      IBKTHV,      IBKTV,       ICITHV
      INTEGER         ICITV,       IORTLS,      ISTRT,       IVMODE
      INTEGER         MAXSEG,      NBKITR,      NCOUPLE,     NITER
      INTEGER         NO0EX,       NO1EX,       NO2EX,       NO3EX
      INTEGER         NO4EX,       NOLDHV,      NOLDV,       NRFITR
      INTEGER         NROOT,       NTYPE,       NUNITV,      NVBKMN
      INTEGER         NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER         NVRFMX,      RTMODE,      VOUT
      INTEGER         NSEGD(4),    NSEG0X(4),   NSEG1X(4),NSEG2X(4)
      INTEGER         NSEG3X(4),   NSEG4X(4),   NSEGSO(4),NODIAG
      integer             cdg4ex,c3ex1ex,c2ex0ex,fileloc(10)
      integer             finalv,finalw
C
      REAL*8            CTOL,        ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8            LRTSHIFT
C
      COMMON /INDATA /  LRTSHIFT,    ETOLBK,      ETOL,        CTOL
      COMMON /INDATA /  NROOT,       NOLDV,       NOLDHV,      NUNITV
      COMMON /INDATA /  NBKITR,      NITER,       DAVCOR,      CSFPRN
      COMMON /INDATA /  IVMODE,      ISTRT,       VOUT,        IORTLS
      COMMON /INDATA /  NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
      COMMON /INDATA /  ICITV,       ICITHV,      FRCSUB,      NVBKMN
      COMMON /INDATA /  NVCIMN,      MAXSEG,      NTYPE,       NRFITR
      COMMON /INDATA /  NVRFMX,      NVRFMN,      FROOT,       FTCALC
      COMMON /INDATA /  RTMODE,      NCOUPLE,     NO0EX,       NO1EX
      COMMON /INDATA /  NO2EX,       NO3EX,       NO4EX,       NODIAG
      COMMON /INDATA /  NSEGD, NSEG0X, NSEG1X, NSEG2X, NSEG3X, NSEG4X
      COMMON /INDATA /  NSEGSO,cdg4ex,c3ex1ex,c2ex0ex,fileloc
      COMMON /INDATA /  FINALV, FINALW
C
C     Equivalenced common variables
C
      INTEGER         MODE,        NCIITR,      NINITV
C
      REAL*8            RTOL(NVMAX), RTOLBK(NVMAX)
      REAL*8            RTOLCI(NVMAX)
C

c LRTSHIFT,      aqcc/lrt shift  (falls <0 )
c ETOLBK,        etol-Grenzwert fuer BK iteration
c  ETOL,         etol-Grenzwert fuer CI iteration
c       CTOL     ctol-Grenzwert fuer CSF Analyse
c NROOT,         Zahl der zu berechnenden Wurzeln
c    NOLDV,      Zahl der vorhandenen CI-Vektoren auf civfl
c       NOLDHV,  Zahl der vorhandenen Sigma-Vektoren auf cihvfl
c     NUNITV     Zahl der zu generierenden Startvektoren
c NBKITR,        max. Zahl der BK Iterationen
c    NITER,      max. Zahl der CI Iterationen
c       DAVCOR,  Davidson-Korrektur berechnen
c     CSFPRN     CI-Vektor analysieren
c IVMODE,        Startvektorgenerierung
c     ISTRT,     Startvektorgenerierung bei ivmode=0
c      VOUT,     ????
c       IORTLS   defaults for ICITV,ICITHV
c NVBKMX,        maximale Subspace dimension in BK-Iteratione
c    IBKTV,      kontrolliert transformation der BK V-Vektoren
c       IBKTHV,  kontrolliert transformation der BK Sigma-Vektoren
c     NVCIMX     maximale Subspace dimension in CI-Iteration
c ICITV,         kontrolliert transformation der CI V-Vektoren
c      ICITHV,   kontrolliert transformation der CI Sigma-Vektoren
c     FRCSUB,    kontrolliert Neuberechnung der Subspace-Darstellung
c      NVBKMN    min. Groesze des BK subspaces
c NVCIMN,        min. Groesze des CI subspaces
c     MAXSEG,    max. Zahl von CI Segmenten
c     NTYPE,     Rechnungstyp
c       NRFITR   max. Zahl der CI Iterationen bei iterat. Referenzraumdi
c NVRFMX,        max. Groesze es Subspaces bei iterat. Referenzraumdiagona
c    NVRFMN,     min. Groesze des Subspaces bei iterat. Referenzraumdiagon
c    FROOT,      Root to follow
c       FTCALC   Berechnung des Formulatapes on the fly
c RTMODE,        obsolet
c    NCOUPLE,    Iteration in der der Lrtshift zum ersten Mal anzuwenden
c     NO0EX,     keine Berechnung der 0externen Beitraege
c     NO1EX      keine Berechnung der 1externen Beitraege
c NO2EX,         keine Berechnung der 2externen Beitraege
c     NO3EX,     keine Berechnung der 3externen Beitraege
c     NO4EX,     keine Berechnung der 4externen Beitraege
c      NODIAG    keine Berechnung der diagonalen Beitraege
c NSEGD,         Segmentierungschema fuer diagonale Beitraege
c NSEG0X,        Segmentierungsschema fuer all-interne Beitraege
c NSEG1X,        ... 1externe ....
cNSEG2X,         ... 2externe ....
cNSEG3X,         ... 3externe ....
c NSEG4X         ... 4externe ....
c NSEGSO,        ...  ...
ccdg4ex,         Behandle diagonalfall und 4-externen Fall gemeinsam
cc3ex1ex,        Behandle 3-externen Fall und 1-externen Fall gemeinsam
cc2ex0ex,        Behandle 2-externen Fall und 0-externen Fall gemeinsam
cfileloc         Filetyp fuer   diagint,ofdgint,fil3w,fil3x,fil4w,fil4x
c                  mit 1 = vdisk  2= ga  3=distrib. (one-proc-sys) 4=dis
cfinalv          final v vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk
cfinalw          final w vectors  0=complete on node 0
c                                 1=distributed one-proc systems
c                                 2=distributed two-proc systems
c                                -1= don't write vector to disk

       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c
C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i


c      file locations
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w
c      value:   0 local disk  1: virtual disk  2: global array
c      default:      1    1    2     2     2      2
c      for non-ga access, length must equal record length!
       logical compabb
       external compabb

       character*(6) marker
       integer hdle,recend,irec
       real*8 tcgtime,tmp
       external tcgtime
       integer v,w,hd,fourw,fourx,thrw,thrx,drtfil,dg,ofdg
        integer cosmofile
       parameter (v=11,w=10,hd=4,fourw=31,fourx=32,thrw=33,thrx=34,
     .            drtfil=38,dg=12,ofdg=13)

       marker='xxxx'
       if (unit.eq.fourw .and. fileloc(fl4w).ne.glar) marker='none'
       if (unit.eq.fourx .and. fileloc(fl4x).ne.glar) marker='none'
       if (unit.eq.thrw  .and. fileloc(fl3w).ne.glar) marker='none'
       if (unit.eq.thrx  .and. fileloc(fl3x).ne.glar) marker='none'
       if (unit.eq.ofdg  .and. fileloc(flofdg).ne.glar) marker='none'
       if (unit.eq.dg    .and. fileloc(fldiag).ne.glar) marker='none'
       if (unit.eq.drtfil.and. fileloc(fldrt).ne.glar) marker='none'

      if (.not. compabb(marker,'none',4)) then
      call ptimer('tsync','resm',me+1)
      tmp=tcgtime()
      call ga_init_fence()
      endif

c      write(6,*) 'entering getvecp: unit=',unit,'s-l',start,length
      if (unit.eq.v) then
       call ga_get(v_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='vrd'
       hdle=v_hdle
      elseif (unit.eq.w) then
       call ga_get(w_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='wrd'
       hdle=w_hdle
      elseif (unit.eq.hd) then
       call ga_get(hd_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='hdrd'
       hdle=hd_hdle
      elseif (unit.eq.fourw) then
       if (fileloc(fl4w).eq.glar) then
       call ga_get(ex4w_hdle,start,start+length-1,1,1,vector,1)
       marker='4exrd'
       hdle=ex4w_hdle
       else
       irec=(start+length)/length
       call diraccnew(unit,1,vector,length,length,irec,recend)
       marker='none'
       endif
      elseif (unit.eq.fourx) then
       if (fileloc(fl4x).eq.glar) then
       call ga_get(ex4x_hdle,start,start+length-1,1,1,vector,1)
       marker='4exrd'
       hdle=ex4x_hdle
       else
       irec=(start+length)/length
       call diraccnew(unit,1,vector,length,length,irec,recend)
       marker='none'
       endif
      elseif (unit.eq.thrw ) then
       if (fileloc(fl3w).eq.glar) then
       call ga_get(ex3w_hdle,start,start+length-1,1,1,vector,1)
       marker='3exrd'
       hdle=ex3w_hdle
       else
       irec=(start+length)/length
       call diraccnew(unit,1,vector,length,length,irec,recend)
       marker='none'
       endif
      elseif (unit.eq.thrx ) then
       if (fileloc(fl3x).eq.glar) then
       call ga_get(ex3x_hdle,start,start+length-1,1,1,vector,1)
       marker='3exrd'
       hdle=ex3x_hdle
       else
       irec=(start+length)/length
       call diraccnew(unit,1,vector,length,length,irec,recend)
       marker='none'
       endif
      elseif (unit.eq.drtfil ) then
       if (fileloc(fldrt).eq.glar) then
       call ga_get(drt_hdle,start,start+length-1,
     .             logrec,logrec,vector,1)
       marker='drtrd'
       hdle=drt_hdle
       else
        irec=start
        call diraccnew(unit,1,vector,length,dalen(drtfil),irec,recend)
        marker='none'
       endif
      elseif (unit.eq.dg ) then
        if (fileloc(fldiag).eq.glar) then
       call ga_get(dg_hdle,start,start+length-1,
     .             logrec,logrec,vector,1)
       marker='dgrd'
       hdle=dg_hdle
       else
       irec=(start+length)/length
       call diraccnew(unit,1,vector,length,length,irec,recend)
       marker='none'
       endif
      elseif (unit.eq.ofdg ) then
       if (fileloc(flofdg).eq.glar) then
       call ga_get(ofdg_hdle,start,start+length-1,
     .             logrec,logrec,vector,1)
       marker='ofdgrd'
       hdle=ofdg_hdle
       else
       irec=(start+length)/length
       call diraccnew(unit,1,vector,length,length,irec,recend)
       marker='none'
       endif
c
c michal2{
c
c      elseif (unit.eq.cosmofile) then
c       call diraccnew(unit,1,vector,length,length,irec,recend)
c       marker='none'
c      call ga_get(cosmofile,start,start+length-1,logrec,logrec,
c    & vector,1)
c      marker='vrd'
c      hdle=cosmofile
c       endif
c
c michal2}
c
      else
       call bummer('invalid unitno in getvecp',unit,2)
      endif


      if (.not. compabb(marker,'none',4)) then
      call ga_fence()
      call ptimer('tsync','susp',me+1)
      tmp=tcgtime()-tmp
      call pfstat(marker,'resm',start,start+length-1,tmp,hdle)
      endif

      return
      end
       subroutine putvecp(unit,logrec,vector,start,length,acc)
       implicit none
        integer unit,logrec,start,length,acc
       real*8 vector(*)
       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex

        integer mitob
        integer mdtob
C
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
c
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c

       logical compabb
       external compabb
       character*(6) marker
       integer hdle
       real*8 tcgtime,tmp
       external tcgtime

       real*8 one
       integer v,w,hd,fourw,fourx,thrw,thrx,drtfil,dg,ofdg
c
c
       parameter (v=11,w=10,hd=4,fourw=31,fourx=32,thrw=33,thrx=34,
     .            drtfil=38,dg=12,ofdg=13,one=1.0d0)

       call ptimer('tsync','resm',me+1)
       tmp=tcgtime()

      call ga_init_fence()
      if (acc.eq.1) then
      if (unit.eq.v) then
       call ga_acc(v_hdle,start,start+length-1,logrec,logrec,vector,1,
     .     one)
       marker='vwt'
       hdle=v_hdle
      elseif (unit.eq.w) then
       call ga_acc(w_hdle,start,start+length-1,logrec,logrec,vector,1,
     .     one)
       marker='wwt'
       hdle=w_hdle
      elseif (unit.eq.hd) then
       call ga_acc(hd_hdle,start,start+length-1,logrec,logrec,vector,1,
     .     one)
       marker='hdwt'
       hdle=hd_hdle
c
c michal2{
c
c      elseif (unit.eq.cosmofile) then
c      call ga_acc(cosmofile,start,start+length-1,logrec,logrec,
c    & vector,1,one)
c      marker='vwt'
c      hdle=cosmofile
c
c michal2}
c
      else
       call bummer('invalid unitno in putvecp1',unit,2)
      endif
      elseif (acc.eq.0) then
      if (unit.eq.v) then
       call ga_put(v_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='vwt'
       hdle=v_hdle
      elseif (unit.eq.w) then
       call ga_put(w_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='wwt'
       hdle=w_hdle
      elseif (unit.eq.hd) then
      call ga_put(hd_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='hdwt'
       hdle=hd_hdle
      elseif (unit.eq.drtfil) then
      call ga_put(drt_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='drtwt'
       hdle=drt_hdle
      elseif (unit.eq.dg ) then
      call ga_put(dg_hdle,start,start+length-1,logrec,logrec,vector,1)
       marker='dgwt'
       hdle=dg_hdle
      elseif (unit.eq.ofdg ) then
      call ga_put(ofdg_hdle,start,start+length-1,logrec,logrec,
     .     vector,1)
       marker='ofdgwt'
       hdle=ofdg_hdle
      elseif (unit.eq.fourw ) then
      call ga_put(ex4w_hdle,start,start+length-1,logrec,logrec,
     .   vector,1)
       marker='4exwt'
       hdle=ex4w_hdle
      elseif (unit.eq.fourx ) then
      call ga_put(ex4x_hdle,start,start+length-1,logrec,logrec,
     .   vector,1)
       marker='4exwt'
       hdle=ex4x_hdle
      elseif (unit.eq.thrx ) then
      call ga_put(ex3x_hdle,start,start+length-1,logrec,logrec,
     .   vector,1)
       marker='3exwt'
       hdle=ex3x_hdle
      elseif (unit.eq.thrw ) then
      call ga_put(ex3w_hdle,start,start+length-1,logrec,logrec,
     .   vector,1)
       marker='3exwt'
       hdle=ex3w_hdle
c
c michal2{
c
c     elseif (unit.eq.cosmofile ) then
c     call ga_put(cosmofile,start,start+length-1,logrec,logrec,
c    .   vector,1)
c      marker='vwt'
c      hdle=cosmofile
c
c michal2}
c
      else
       call bummer('invalid unitno in putvecp2',unit,2)
      endif
      else
       call bummer('invalid acc mode in putvecp',acc,2)
      endif
      call ga_fence()
      call ptimer('tsync','susp',me+1)
      tmp=tcgtime()-tmp
      call pfstat(marker,'resm',start,start+length-1,tmp,hdle)
      return
      end
       subroutine ptimer(str,smode,itask)

       implicit none
        character*(*) str,smode
       integer mode,itask

       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex


        integer mitob
        integer mdtob
C
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint

      INTEGER MAXTASK
      parameter (maxtask=16384)
      INTEGER BSEG(MAXTASK), KSEG(MAXTASK),
     .        TASK(MAXTASK),NTASK,
     .        SLICE(MAXTASK),LOOPCNT(MAXTASK),
     .        CURRTSK
      REAL*8 TASKTIMES(MAXTASK),MFLOP(MAXTASK)
      COMMON/TASKLST/MFLOP,
     .      TASKTIMES,BSEG,KSEG,TASK,SLICE,
     .      LOOPCNT,NTASK,CURRTSK


c
c tasklist
c BSEG      bra segment
c KSEG      ket segment
c TASK      task type
c NTASK     total number of task
c SLICE     segmentation type
c loopcnt   loopcnt for debugging
c currtsk   current task
c tasktimes time spent in task
c
c

       integer maxtypes
       parameter (maxtypes=11)
       real*8 zero
       parameter (zero=0.0d0)
       real*8 tsktimes(maxtask,maxtypes)
       real*8 starttimes(maxtypes)
       integer starttsk(maxtypes),susp(maxtypes)
       integer tsktoproc(maxtask)
       common /PTIME/ tsktimes,starttimes,starttsk,susp
       common /PTIME/ tsktoproc

        real*8 sumwait,sumtcpu,sumint,sumloop,sumwall,summnx

        real*8 tcgtime
        external tcgtime
        logical compabb
        external compabb
        integer event,i,j,ev

c============================================================
c  # str            characterizes the event
c        tmult      total cpu time for task spent in mult
c        tloop      cpu time for loop construction
c        tint       cpu time for integral processing
c        twait      cpu time spent waiting for sync.
c        ths        time spent in formhs
c        tcin       time spent in cinewv
c        teig       time spent in eig
c        tvect      time spent in vectrn
c        ttotal     total time per iteration
c        tsync      total ga-access times
c        tmnx       total cpu time for task spent in multnx
c
c  # mode = 'init'       initialize  everything
c         = 'evin'       initialize  event and start timing
c         = 'susp'       suspend  event timing
c         = 'resm'       resume   event timing
c         = 'stop'       stop event timing
c         = 'prme'       print timings for this process
c         = 'prnt'       print timings for all processes
c         = 'copy'       copy total tasktimings to tasklist
c
c  # itask          task number
c============================================================

        event=-1

        if (compabb(str,'tmult',5)) event=1
        if (compabb(str,'tloop',5)) event=2
        if (compabb(str,'tint',4)) event=3
        if (compabb(str,'twait',5)) event=4
        if (compabb(str,'ths',3)) event=5
        if (compabb(str,'tcin',4)) event=6
        if (compabb(str,'teig',4)) event=7
        if (compabb(str,'tvect',4)) event=8
        if (compabb(str,'tsync',4)) event=9
        if (compabb(str,'ttotal',6)) event=10
        if (compabb(str,'tmnx',3)) event=11

        mode =999
        if (compabb(smode,'init',4)) mode =0
        if (compabb(smode,'evin',4)) mode =1
        if (compabb(smode,'susp',4)) mode =2
        if (compabb(smode,'resm',4)) mode =3
        if (compabb(smode,'stop',4)) mode =4
        if (compabb(smode,'prme',4)) mode =5
        if (compabb(smode,'prnt',4)) mode =6
        if (compabb(smode,'copy',4)) mode =7


        if (event.eq.-1 .or. mode.gt.7) then
         write(0,*) 'event,mode=',event,mode,'#',str,'#',smode
         call bummer('invalid event',0,2)
        endif

        if (mode.eq.0) then
          call wzero(maxtask*maxtypes,tsktimes,1)
          call wzero(maxtypes,starttimes,1)
          call izero_wr(maxtypes,susp,1)
        endif

        if (mode.eq.1) then
         tsktimes(itask,event)=zero
         starttimes(event) = tcgtime()
         starttsk(event)=itask
         susp(event)=0
         if (event.eq.10) then
            call izero_wr(maxtask,tsktoproc,1)
c           write(0,*) 'node=',me,' initializing tsktoproc'
         endif
        elseif (mode.eq.2) then
         i=starttsk(event)
         susp(event)=1
         tsktimes(i,event)=
     .      tsktimes(i,event)+tcgtime()-starttimes(event)
c        if (event.eq.1) tsktoproc(i)=me
        elseif (mode.eq.3) then
         i=starttsk(event)
         susp(event)=0
         starttimes(event) = tcgtime()
         if (event.eq.3) then
             tsktoproc(i)=me
c           write(0,*) 'node=',me,' setting i,node=',i,me
         endif
        elseif (mode.eq.4) then
          i=starttsk(event)
          if(susp(event).eq.0)
     .   tsktimes(i,event)=
     .      tsktimes(i,event)+tcgtime()-starttimes(event)
        elseif (mode.eq.5) then
         write(0,100) me
         do i=1,ntask
           write(0,101) (tsktimes(i,ev),ev=1,maxtypes)
         enddo
         write(0,102)
        elseif (mode.eq.6) then
         call ga_dgop(31500,tsktimes,maxtypes*maxtask,'+')
         call ga_igop(31501,tsktoproc,maxtask,'+')
         call ga_sync()
         if (me.eq.0) then
         write(6,200)
         do i=1,ntask
           write(6,201) i,task(i),tsktoproc(i),
     .          (tsktimes(i,ev),ev=1,3),tsktimes(i,11),
     .          mflop(i),dble(loopcnt(i))*1.d-6
         enddo
         write(6,202)
 215    format(4('===='),' TIMING STATISTICS FOR JOB     ',4('====')/
     &           'time for subspace matrix construction',f10.6/
     &          'time for cinew                       ',f10.6/
     &          'time for eigenvalue solver           ',f10.6/
     &          'time for vector access               ',f10.6)
         write(6,215) (tsktimes(1,ev),ev=5,8)
         write(6,202)
         endif
          sumtcpu=zero
          sumwait=zero
          sumint=zero
          sumloop=zero
          summnx=zero
          sumwall=zero
         do i=1,ntask
           tasktimes(i)=tsktimes(i,1)
           sumtcpu=tsktimes(i,1)+sumtcpu
           sumwait=tsktimes(i,4)+sumwait
           sumint=tsktimes(i,3)+sumint
           sumloop=tsktimes(i,2)+sumloop
           summnx=tsktimes(i,11)+summnx
           sumwall=tsktimes(i,10)+sumwall
         enddo
         write(6,203) sumtcpu,summnx,sumint,sumloop,sumwait,sumwall
        endif
100     format(4('===='),' TIMING STATISTICS FOR NODE ',i2,4('====')/
     .    'task# type  tmult  tloop   tint ',
     .    ' twait    ths   tcin   teig  tvect  tga   ttotal ')
101     format( i3,i6,10(f7.2))
102     format(16('===='))

200     format(6('===='),' TIMING STATISTICS PER TASK    ',6('====')/
     .    'task# type node  tmult  tloop   tint  tmnx   ',
     .    ' MFLIPS     mloop ')
201     format( i5,i5,i5,4(f7.2),1x,f10.0,2x,f8.4)
202     format(16('===='))
203     format(4('===='),' TIMING STATISTICS FOR JOB     ',4('====')/
     .      'time spent in mult:              ',f13.4,'s '/
     .      'time spent in multnx:            ' ,f13.4,'s '/
     .      'integral transfer time:          ',f13.4,'s '/
     .      'time spent for loop construction:',f13.4,'s '/
     .      'syncronization time in mult:     ',f13.4,'s '/
     .      'total time per CI iteration:     ',f13.4,'s '/
     .   ' ** parallelization degree for mult section:',f6.4)

204     format(4('===='),' TIMING STATISTICS PER NODE    ',4('====')/
     .    'node# twait    ths   tcin   teig  tvect  tga   ttotal ')
205     format( i3,7(f7.2))
        return
        end
      subroutine pfstat(str,smode,nlo,nhi,dt,hdle)

      implicit none
      character*(*) str,smode
      integer nhi,nlo,hdle
      real*8 dt

       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex


        integer mitob
        integer mdtob
C
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c

      integer maxtyp
      parameter(maxtyp=16)
      real*8 tvolume,time, rates(maxtyp,2)
      real*8 totalvol(2),totalrates(2),tottime(2)
      common /FILESTAT/ tvolume(maxtyp,2),time(maxtyp,2)

      integer locality,i,j,event,mode,ihi,ilo,jhi,jlo
      real*8  fac
      logical compabb
      external compabb
      character*10 strg(maxtyp)
      data strg /'v  (read) ',
     .           'v  (write)',
     .           'w  (read) ','w  (write)',
     .           'dg (read) ','dg (write)',
     .           'drt(read) ','drt(write)',
     .           '3x (read) ','3x (write)',
     .           '4x (read) ','4x (write)',
     .           'dg (read) ','dg (write)',
     .           'ofdg(rd)  ','ofdg (wt) '/


c============================================================
c  # str           characterizes the event
c         vrd      reading from vector file
c         vwt      writing to vector file
c         wrd      reading from sigma file
c         wwt      writing to sigma file
c         hdrd     reading diagonal elements
c         hdwt     writing diagonal elements
c         drtrd    reading drtfil
c         drtwt    writing drtfil
c         3xrd     reading 3external
c         3xwt     writing 3external
c         4xrd     reading 4external
c         4xwt     writing 4external
c         dgwt     writing diagonal integral file
c         dgrd     reading diagonal integral file
c         ofdgwt   writing offdiagonal integral file
c         ofdgrd   reading offdiagonal integral file
c
c  # mode = 'init'       initialize  everything
c         = 'evin'       initialize  counter and add first transfer
c         = 'resm'       add to counter
c         = 'prnt'       print transfer rates
c
c  # ilo,ihi : ga range
c  # dt      : time elapsed for transfer operation
c============================================================
        event=-1

        if (compabb(str,'vrd',3)) event=1
        if (compabb(str,'vwt',3)) event=2
        if (compabb(str,'wrd',3)) event=3
        if (compabb(str,'wwt',3)) event=4
        if (compabb(str,'hdrd',4)) event=5
        if (compabb(str,'hdwt',4)) event=6
        if (compabb(str,'drtrd',5)) event=7
        if (compabb(str,'drtwt',5)) event=8
        if (compabb(str,'3exrd',5)) event=9
        if (compabb(str,'3exwt',5)) event=10
        if (compabb(str,'4exrd',5)) event=11
        if (compabb(str,'4exwt',5)) event=12
        if (compabb(str,'dgrd',4)) event=13
        if (compabb(str,'dgwt',4)) event=14
        if (compabb(str,'ofdgrd',6)) event=15
        if (compabb(str,'ofdgwt',6)) event=16


        mode =999
        if (compabb(smode,'init',4)) mode =0
        if (compabb(smode,'evin',4)) mode =1
        if (compabb(smode,'resm',4)) mode =2
        if (compabb(smode,'prnt',4)) mode =3

        if (event.eq.-1 .or. mode.gt.3) then
         write(0,*) 'event,mode=',event,mode,'#',str,'#',smode
         call bummer('invalid pfstat event',0,2)
        endif

        if (mode.eq.0) then
           call wzero(maxtyp*2,tvolume,1)
           call wzero(maxtyp*2,time,1)
           return
        elseif(mode.eq.1 .or. mode.eq.2) then
c
c  # determine whether ga transfer is completely local or not
c
           call ga_distribution(hdle,me,ilo,ihi,jlo,jhi)
           if (nlo.ge.ilo .and. nlo.le.ihi
     .              .and. nhi.ge.ilo .and. nhi.le.ihi) then

              locality=1
           else
              locality=2
           endif
           if (event.eq.7 .or. event.eq.8) then
              fac=4.d0
           else
              fac=8.0d0
           endif
        endif

        if (mode.eq.1) then
          tvolume(event,locality)= (nhi-nlo+1)*fac
          time(event,locality)   = dt
        elseif (mode.eq.2) then
          tvolume(event,locality)=
     .            tvolume(event,locality)+(nhi-nlo+1)*fac
          time(event,locality)   = time(event,locality)+dt
        elseif (mode.eq.3) then
          call ga_dgop(33201,tvolume,maxtyp*2,'+')
          call ga_dgop(33202,time,maxtyp*2,'+')
          call ga_sync()
          call wzero(2,totalvol,1)
          call wzero(2,totalrates,1)
          call wzero(2*maxtyp,rates,1)
          call wzero(2,tottime,1)
         do j=1,2
          do i=1,maxtyp
           tvolume(i,j)=tvolume(i,j)/1048576.0d0
           totalvol(j)=totalvol(j)+tvolume(i,j)
           tottime(j) =tottime(j)+time(i,j)
           if (time(i,j).gt.1.d-3)
     .     rates(i,j) = tvolume(i,j)/(time(i,j))
          enddo
           if (tottime(j).gt.1.d-3)
     .     totalrates(j)=totalvol(j)/(tottime(j))
         enddo
         if (me.eq.0) then
           write(6,200)
           do i=1,maxtyp
             write(6,201) strg(i),tvolume(i,1),time(i,1),rates(i,1)
           enddo
           write(6,201) '  total:  ',totalvol(1),tottime(1),
     .                     totalrates(1)
          write(6,203)
          write(6,204)
           do i=1,maxtyp
             write(6,201) strg(i),tvolume(i,2),time(i,2),rates(i,2)
           enddo
           write(6,201) '  total:  ',totalvol(2),tottime(2),
     .                     totalrates(2)
          write(6,203)
         endif
 200    format(/5('===='),' LOCAL GA TRANSFER ',5('====')/
     .   4x,'   type           volume        time          rate   '/
     .   4x,'                   [MB]         [s]          [MB/s] ')
 201    format(4x,a10,2x,f12.2,2x,f10.2,6x,f9.3)
 203    format(14('====')/)
 204    format(/5('===='),' NON-LOCAL GA TRANSFER ',4('====')/
     .   4x,'   type           volume        time          rate   '/
     .   4x,'                   [MB]         [s]          [MB/s] ')

        endif
        return
        end

      integer function nexttask()
      implicit none
c
       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex

        integer mitob
        integer mdtob
C
      INTEGER MAXTASK
      parameter (maxtask=16384)
      INTEGER BSEG(MAXTASK), KSEG(MAXTASK),
     .        TASK(MAXTASK),NTASK,
     .        SLICE(MAXTASK),LOOPCNT(MAXTASK),
     .        CURRTSK
      REAL*8 TASKTIMES(MAXTASK),MFLOP(MAXTASK)
      COMMON/TASKLST/MFLOP,
     .      TASKTIMES,BSEG,KSEG,TASK,SLICE,
     .      LOOPCNT,NTASK,CURRTSK


c
c tasklist
c BSEG      bra segment
c KSEG      ket segment
c TASK      task type
c NTASK     total number of task
c SLICE     segmentation type
c loopcnt   loopcnt for debugging
c currtsk   current task
c tasktimes time spent in task
c
c
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c
        integer tmp
        logical status
c
c      works only, if ntask is a multiple of processor count
c

c      status = ga_create_mutexes(1)
c      if (.not.status)
c    .   call ga_error('nexttask,ga_create_mutexes failed',1)
c        call ga_lock(0)
c        call ga_init_fence()
         tmp = ga_read_inc(nxttask,1,1,1)
c        call ga_fence()
c        call ga_unlock(0)
c      status = ga_destroy_mutexes()
c      if (.not.status)
c    .   call ga_error('nexttask,ga_destroy_mutexes failed',1)
       if (tmp.gt.ntask) then
             nexttask=-1
        else
             nexttask=tmp
       endif
       return
       end


      subroutine intab
c
c  initialize tables for segment value calculations.
c
c  for details of segment evaluation and the auxiliary functions,
c  see:
c        i. shavitt, in "the unitary group for the evaluation of
c        electronic energy matrix elements", j. hinze, ed.
c        (springer-verlag, berlin, 1981) p. 51. and i. shavitt,
c        "new methods in computational quantum chemistry and
c        their application on modern super-computers" (annual
c        report to the national aeronautics and space administration),
c        battelle columbus laboratories, june 1979.
c
c  mu-dependent segment evaluation entries added 7-nov-86 (rls).
c  this version written 01-aug-84 by ron shepard.
c
c
         IMPLICIT NONE

C
C     Statement functions
C
      REAL*8            RX,          XA,          XB,          XC
      REAL*8            XD,          XF,          XR
C
C     Parameter variables
C
      INTEGER         NTAB
      PARAMETER           (NTAB = 70)
      INTEGER         MAXB
      PARAMETER           (MAXB = 19)
C
      REAL*8            ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8            ONE
      PARAMETER           (ONE = 1D0)
      REAL*8            TWO
      PARAMETER           (TWO = 2D0)
      REAL*8            HALF
      PARAMETER           (HALF = .5D0)
C
C     Local variables
C
      INTEGER         B,           I,           P,           Q
      INTEGER         R,           S
C
      REAL*8            SQRT2,       T
C
C     Common variables
C
      REAL*8            TAB(NTAB,0:MAXB)
C
      COMMON /CTAB   /  TAB
C
c  formula tape construction
c**********************************************************************
c  table entry definitions:
c   1:0             2:1             3:-1            4:2
c   5:-2            6:sqrt2         7:-t            8:1/b
c   9:-sqrt2/b     10:1/(b+1)      11:-1/(b+1)     12:-1/(b+2)
c  13:-sqrt2/(b+2) 14:a(-1,0)      15:-a(-1,0)     16:a(1,0)
c  17:-a(1,0)      18:a(0,1)       19:-a(0,1)      20:a(2,1)
c  21:-a(2,1)      22:a(1,2)       23:-a(1,2)      24:a(3,2)
c  25:-a(3,2)      26:b(-1,0)      27:b(0,1)       28:-b(0,2)
c  29:b(1,2)       30:b(2,3)       31:c(0)         32:-c(0)
c  33:c(1)         34:c(2)         35:-c(2)        36:d(-1)
c  37:d(0)         38:-d(0)        39:d(1)         40:-d(1)
c  41:d(2)         42:ta(-1,0)     43:-ta(-1,0)    44:ta(1,0)
c  45:ta(2,0)      46:-ta(2,0)     47:-ta(-1,1)    48:ta(0,1)
c  49:ta(2,1)      50:-ta(2,1)     51:ta(3,1)      52:ta(0,2)
c  53:-ta(0,2)     54:ta(1,2)      55:ta(3,2)      56:-ta(3,2)
c         added for mu-dependent segment evaluation
c  57:1/2          58:-1/2         59:-1/b         60:1/(b+2)
c  61:(b+1)/b      62:(b+1)/(b+2)  63:(2b+1)/2b    64:(2b+3)/(2b+4)
c  65:1/2*a(1,0)   66:1/2*a(0,1)   67:1/2*a(2,1)   68:1/2*a(1,2)
c  69:1/2*c(0)     70:1/2*c(2)
c**********************************************************************
      rx(p)         = (p)
      xa(p,q,b)     = sqrt(rx(b+p) / rx(b+q))
      xb(p,q,b)     = sqrt(two / rx((b+p) * (b+q)))
      xc(p,b)       = sqrt(rx((b+p-1) * (b+p+1))) / rx(b+p)
      xd(p,b)       = sqrt(rx((b+p-1) * (b+p+2)) / rx((b+p) * (b+p+1)))
      xr(p,b)       = one / rx(b+p)
      xf(p,q,r,s,b) = rx(p*b+q) / rx(r*b+s)
c
      sqrt2 = sqrt(two)
      t     = one / sqrt2
c
      do 110 b = 0, maxb
         do 100 i = 1, ntab
            tab(i,b) = zero
100      continue
110   continue
c
      do 200 b = 0, maxb
         tab( 1,b) = zero
         tab( 2,b) = +one
         tab( 3,b) = -one
         tab( 4,b) = +two
         tab( 5,b) = -two
         tab( 6,b) = +sqrt2
         tab( 7,b) = -t
         tab(10,b) = +xr(1,b)
         tab(11,b) = -xr(1,b)
         tab(12,b) = -xr(2,b)
         tab(13,b) = -sqrt2*xr(2,b)
         tab(18,b) = +xa(0,1,b)
         tab(19,b) = -xa(0,1,b)
         tab(20,b) = +xa(2,1,b)
         tab(21,b) = -xa(2,1,b)
         tab(22,b) = +xa(1,2,b)
         tab(23,b) = -xa(1,2,b)
         tab(24,b) = +xa(3,2,b)
         tab(25,b) = -xa(3,2,b)
         tab(29,b) = +xb(1,2,b)
         tab(30,b) = +xb(2,3,b)
         tab(34,b) = +xc(2,b)
         tab(35,b) = -xc(2,b)
         tab(39,b) = +xd(1,b)
         tab(40,b) = -xd(1,b)
         tab(41,b) = +xd(2,b)
         tab(48,b) = +t * xa(0,1,b)
         tab(49,b) = +t * xa(2,1,b)
         tab(50,b) = -t * xa(2,1,b)
         tab(51,b) = +t * xa(3,1,b)
         tab(52,b) = +t * xa(0,2,b)
         tab(53,b) = -t * xa(0,2,b)
         tab(54,b) = +t * xa(1,2,b)
         tab(55,b) = +t * xa(3,2,b)
         tab(56,b) = -t * xa(3,2,b)
         tab(57,b) = +half
         tab(58,b) = -half
         tab(60,b) = +xr(2,b)
         tab(62,b) = +xf(1,1,1,2,b)
         tab(64,b) = +xf(2,3,2,4,b)
         tab(66,b) = +half * xa(0,1,b)
         tab(67,b) = +half * xa(2,1,b)
         tab(68,b) = +half * xa(1,2,b)
         tab(70,b) = +half * xc(2,b)
200   continue
c
      do 300 b = 1, maxb
         tab( 8,b) = +xr(0,b)
         tab( 9,b) = -sqrt2 * xr(0,b)
         tab(14,b) = +xa(-1,0,b)
         tab(15,b) = -xa(-1,0,b)
         tab(16,b) = +xa(1,0,b)
         tab(17,b) = -xa(1,0,b)
         tab(27,b) = +xb(0,1,b)
         tab(28,b) = -xb(0,2,b)
         tab(31,b) = +xc(0,b)
         tab(32,b) = -xc(0,b)
         tab(33,b) = +xc(1,b)
         tab(37,b) = +xd(0,b)
         tab(38,b) = -xd(0,b)
         tab(42,b) = +t * xa(-1,0,b)
         tab(43,b) = -t * xa(-1,0,b)
         tab(44,b) = +t * xa(1,0,b)
         tab(45,b) = +t * xa(2,0,b)
         tab(46,b) = -t * xa(2,0,b)
         tab(47,b) = -t * xa(-1,1,b)
         tab(59,b) = -xr(0,b)
         tab(61,b) = +xf(1,1,1,0,b)
         tab(63,b) = +xf(2,1,2,0,b)
         tab(65,b) = +half * xa(1,0,b)
         tab(69,b) = +half * xc(0,b)
300   continue
c
      do 400 b = 2, maxb
         tab(26,b) = +xb(-1,0,b)
         tab(36,b) = +xd(-1,b)
400   continue
c
      return
      end


      subroutine istack
c
c  initialize necessary stack information for loop construction.
c
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         NIMOMX
      PARAMETER           (NIMOMX = 128)
C
      REAL*8            ZERO
      PARAMETER           (ZERO = 0D0)
C
C     Local variables
C
      INTEGER         I
C
C     Common variables
C
      INTEGER         BROW(0:NIMOMX),           EXTBK(0:NIMOMX)
      INTEGER         KROW(0:NIMOMX),           RANGE(0:NIMOMX)
      INTEGER         YBRA(0:NIMOMX),           YKET(0:NIMOMX)
C
      LOGICAL             QEQ(0:NIMOMX)
C
      REAL*8            V(3,0:NIMOMX)
C
      COMMON /CSTACK /  V,           RANGE,       EXTBK,       QEQ
      COMMON /CSTACK /  BROW,        KROW,        YBRA,       YKET
C
c  stack for loop construction

      do 100 i = 0, nimomx
         extbk(i) = 0
         v(1,i)   = zero
         v(2,i)   = zero
         v(3,i)   = zero
         ybra(i)    = 0
         yket(i)    = 0
100   continue
c
      return
      end


      
       integer function isum (n,ia,inc)
       integer n,ia(n),inc  
       integer i
       isum=0
        do i=1,n
         isum=isum+ia(i)
       enddo
       return
       end

      block data nummerzwo
c
c  pointer tables for segment construction and evaluation.
c
c  mu-dependent entries added 7-nov-86 (rls).
c  this version written 7-sep-84 by ron shepard.
c
c
c     # s32=sqrt(3./2.) s34=sqrt(3./4.)
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      parameter          (maxsym=8)
      INTEGER         NST
      PARAMETER           (NST = 40)
      INTEGER         NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER         W00
      PARAMETER           (W00 = 3)
      INTEGER         RT0
      PARAMETER           (RT0 = 8)
      INTEGER         LB0
      PARAMETER           (LB0 = 13)
      INTEGER         RB0
      PARAMETER           (RB0 = 18)
      INTEGER         LT0
      PARAMETER           (LT0 = 23)
      INTEGER         R0
      PARAMETER           (R0 = 28)
      INTEGER         L0
      PARAMETER           (L0 = 33)
      INTEGER         WRT00
      PARAMETER           (WRT00 = 38)
      INTEGER         WRB00
      PARAMETER           (WRB00 = 43)
      INTEGER         WW00
      PARAMETER           (WW00 = 48)
      INTEGER         WR00
      PARAMETER           (WR00 = 53)
      INTEGER         RTRT00
      PARAMETER           (RTRT00 = 58)
      INTEGER         RBRB00
      PARAMETER           (RBRB00 = 63)
      INTEGER         RTRB00
      PARAMETER           (RTRB00 = 68)
      INTEGER         RRT00
      PARAMETER           (RRT00 = 73)
      INTEGER         RRT10
      PARAMETER           (RRT10 = 78)
      INTEGER         RRB00
      PARAMETER           (RRB00 = 83)
      INTEGER         RRB10
      PARAMETER           (RRB10 = 88)
      INTEGER         RR00
      PARAMETER           (RR00 = 93)
      INTEGER         RR10
      PARAMETER           (RR10 = 98)
      INTEGER         RTLB0
      PARAMETER           (RTLB0 = 103)
      INTEGER         RTLT10
      PARAMETER           (RTLT10 = 108)
      INTEGER         RBLB10
      PARAMETER           (RBLB10 = 113)
      INTEGER         RTL10
      PARAMETER           (RTL10 = 118)
      INTEGER         RLB10
      PARAMETER           (RLB10 = 123)
      INTEGER         RLT10
      PARAMETER           (RLT10 = 128)
      INTEGER         RL00
      PARAMETER           (RL00 = 133)
      INTEGER         RL10
      PARAMETER           (RL10 = 138)
      INTEGER         W10
      PARAMETER           (W10 = 143)
      INTEGER         W20
      PARAMETER           (W20 = 148)
      INTEGER         WW10
      PARAMETER           (WW10 = 153)
      INTEGER         WW20
      PARAMETER           (WW20 = 158)
      INTEGER         WRT10
      PARAMETER           (WRT10 = 163)
      INTEGER         WRT20
      PARAMETER           (WRT20 = 168)
      INTEGER         WRB10
      PARAMETER           (WRB10 = 173)
      INTEGER         WRB20
      PARAMETER           (WRB20 = 178)
      INTEGER         WR10
      PARAMETER           (WR10 = 183)
      INTEGER         WR20
      PARAMETER           (WR20 = 188)
      INTEGER         RTRB10
      PARAMETER           (RTRB10 = 193)
      INTEGER         RTRB20
      PARAMETER           (RTRB20 = 198)
C
      REAL*8            ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8            ONE
      PARAMETER           (ONE = 1D0)
      REAL*8            HALF
      PARAMETER           (HALF = 5D-1)
      REAL*8            HALFM
      PARAMETER           (HALFM = -5D-1)
      REAL*8            ONEM
      PARAMETER           (ONEM = -1D0)
      REAL*8            S32
      PARAMETER
     x(S32 = 1.2247448713915890490986420373529458130097385D0)
      REAL*8            S34
      PARAMETER
     x(S34 = .86602540378443864676372317075293616107654866D0)
C
      integer             l(0:3,0:3,-2:2),          lb(0:3,0:3,-2:2)
      integer             lt(0:3,0:3,-2:2),         r(0:3,0:3,-2:2)
      integer             rb(0:3,0:3,-2:2),         rblb1(0:3,0:3,-2:2)
      integer             rbrb0(0:3,0:3,-2:2),      rl0(0:3,0:3,-2:2)
      integer             rl1(0:3,0:3,-2:2),        rlb1(0:3,0:3,-2:2)
      integer             rlt1(0:3,0:3,-2:2),       rr0(0:3,0:3,-2:2)
      integer             rr1(0:3,0:3,-2:2),        rrb0(0:3,0:3,-2:2)
      integer             rrb1(0:3,0:3,-2:2),       rrt0(0:3,0:3,-2:2)
      integer             rrt1(0:3,0:3,-2:2),       rt(0:3,0:3,-2:2)
      integer             rtl1(0:3,0:3,-2:2),       rtlb(0:3,0:3,-2:2)
      integer             rtlt1(0:3,0:3,-2:2),      rtrb0(0:3,0:3,-2:2)
      integer             rtrb1(0:3,0:3,-2:2),      rtrb2(0:3,0:3,-2:2)
      integer             rtrt0(0:3,0:3,-2:2),      w0(0:3,0:3,-2:2)
      integer             w1(0:3,0:3,-2:2),         w2(0:3,0:3,-2:2)
      integer             wr0(0:3,0:3,-2:2),        wr1(0:3,0:3,-2:2)
      integer             wr2(0:3,0:3,-2:2),        wrb0(0:3,0:3,-2:2)
      integer             wrb1(0:3,0:3,-2:2),       wrb2(0:3,0:3,-2:2)
      integer             wrt0(0:3,0:3,-2:2),       wrt1(0:3,0:3,-2:2)
      integer             wrt2(0:3,0:3,-2:2),       ww0(0:3,0:3,-2:2)
      integer             ww1(0:3,0:3,-2:2),        ww2(0:3,0:3,-2:2)

C     Common variables
C
      INTEGER         DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER         DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER         DB13(4,0:2), DB14(2,0:2,2)
      INTEGER         DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER         DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER         DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER         DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON /CDB    /  DB1,         DB2,         DB3,         DB4
      COMMON /CDB    /  DB4P,        DB5,         DB6,         DB7
      COMMON /CDB    /  DB8,         DB10,        DB11,        DB12
      COMMON /CDB    /  DB13,        DB14
C
c  internal stuff for loop construction
C     Common variables
C
      INTEGER         BCASEX(6,-2:3),           KCASEX(6,-2:3)
      INTEGER         NEX(-2:3)
C
      COMMON /CEX    /  NEX,         BCASEX,      KCASEX
C
c  internal stuff for loop construction
C     Common variables
C
      INTEGER         PT(0:3,0:3,NST5)
C
      COMMON /CPT    /  PT
C
c  internal stuff for loop construction
C     Common variables
C
      INTEGER         ST1(8),      ST10(6),     ST11(4)
      INTEGER         ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER         ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER         ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON /CST    /  ST1,         ST2,         ST3,         ST4
      COMMON /CST    /  ST4P,        ST5,         ST6,         ST7
      COMMON /CST    /  ST8,         ST10,        ST11D,       ST11
      COMMON /CST    /  ST12,        ST13,        ST14
C
c  tables for formula tape construction
C     Common variables
C
      INTEGER         MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
C
      COMMON /CSYM   /  MULT,        NSYM,        SSYM
C
c mult   standard multiplication table
c nsym   number of irreps
c ssym   state symmetry
C     Common variables
C
      INTEGER         VTPT
C
      REAL*8            OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON /CVTRAN /  VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
c  for loop construction
      equivalence (w0(0,0,0),pt(0,0,w00))
c
      equivalence (rt(0,0,0),pt(0,0,rt0))
c
      equivalence (lb(0,0,0),pt(0,0,lb0))
c
      equivalence (rb(0,0,0),pt(0,0,rb0))
c
      equivalence (lt(0,0,0),pt(0,0,lt0))
c
      equivalence (r(0,0,0),pt(0,0,r0))
c
      equivalence (l(0,0,0),pt(0,0,l0))
c
      equivalence (wrt0(0,0,0),pt(0,0,wrt00))
c
      equivalence (wrb0(0,0,0),pt(0,0,wrb00))
c
      equivalence (ww0(0,0,0),pt(0,0,ww00))
c
      equivalence (wr0(0,0,0),pt(0,0,wr00))
c
      equivalence (rtrt0(0,0,0),pt(0,0,rtrt00))
c
      equivalence (rbrb0(0,0,0),pt(0,0,rbrb00))
c
      equivalence (rtrb0(0,0,0),pt(0,0,rtrb00))
c
      equivalence (rrt0(0,0,0),pt(0,0,rrt00))
c
      equivalence (rrt1(0,0,0),pt(0,0,rrt10))
c
      equivalence (rrb0(0,0,0),pt(0,0,rrb00))
c
      equivalence (rrb1(0,0,0),pt(0,0,rrb10))
c
      equivalence (rr0(0,0,0),pt(0,0,rr00))
c
      equivalence (rr1(0,0,0),pt(0,0,rr10))
c
      equivalence (rtlb(0,0,0),pt(0,0,rtlb0))
c
      equivalence (rtlt1(0,0,0),pt(0,0,rtlt10))
c
      equivalence (rblb1(0,0,0),pt(0,0,rblb10))
c
      equivalence (rtl1(0,0,0),pt(0,0,rtl10))
c
      equivalence (rlb1(0,0,0),pt(0,0,rlb10))
c
      equivalence (rlt1(0,0,0),pt(0,0,rlt10))
c
      equivalence (rl0(0,0,0),pt(0,0,rl00))
c
      equivalence (rl1(0,0,0),pt(0,0,rl10))
c
      equivalence (w1(0,0,0),pt(0,0,w10))
c
      equivalence (w2(0,0,0),pt(0,0,w20))
c
      equivalence (ww1(0,0,0),pt(0,0,ww10))
c
      equivalence (ww2(0,0,0),pt(0,0,ww20))
c
      equivalence (wrt1(0,0,0),pt(0,0,wrt10))
c
      equivalence (wrt2(0,0,0),pt(0,0,wrt20))
c
      equivalence (wrb1(0,0,0),pt(0,0,wrb10))
c
      equivalence (wrb2(0,0,0),pt(0,0,wrb20))
c
      equivalence (wr1(0,0,0),pt(0,0,wr10))
c
      equivalence (wr2(0,0,0),pt(0,0,wr20))
c
      equivalence (rtrb1(0,0,0),pt(0,0,rtrb10))
c
      equivalence (rtrb2(0,0,0),pt(0,0,rtrb20))
c
c     # for each loop type, assign the segment extension type pointers
c     # and the delb offsets for each range.
c
c     # loop type 1:
      data  st1, db1
     & /  3,     1,     0,    -1,     0,     1,     0,    -1,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 2:
      data  st2, db2
     & /  3,     1,     0,    -1,     0,    -1,     0,     1,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 3:
      data  st3, db3
     & /  3,     1,     0,     1,     0,    -1,     0,    -1,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0/
c
c     # loop type 4:
      data  st4, db4
     & /  3,     0,     0,     1,     0,    -1,
     & rl00,   w00,  rl00,   rb0,    r0,   rt0,
     & rl00,   w10,  rl00,   rb0,    r0,   rt0,
     & rl00,   w20,  rl00,   rb0,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 4':
      data st4p, db4p
     & /  3,      0,     0,    -1,     0,     1,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 5:
      data  st5, db5
     & /  3,      2,     0,    -1,     0,    -1,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0/
c
c     # loop type 6:
      data  st6, db6
     & /  3,     1,     0,     0,     0,    -1,
     & rl00,   rb0,    r0,  wr00,    r0,   rt0,
     & rl00,   rb0,    r0,  wr10,    r0,   rt0,
     & rl00,   rb0,    r0,  wr20,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb00,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb10,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb20,    r0,   rt0/
c
c     # loop type 7:
      data  st7, db7
     & /  3,     1,     0,    -2,     0,     1,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0/
c
c     # loop type 8:
      data  st8, db8
     & /  3,     1,     0,    -1,     0,      0,
     & rl00,   rb0,    r0,   rt0,  rl00,    w00,
     & rl00,   rb0,    r0,   rt0,  rl00,    w10,
     & rl00,   rb0,    r0,   rt0,  rl00,    w20,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10/
c
c     # loop type 10:
      data st10, db10
     & /  3,     1,     0,     1,     0,     -2,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00/
c
c     # loop type 11d, 11:
      data st11d, st11, db11
     & /  3,      3,      3,      3,
     &    3,      0,      0,      0,
     & rl00,    w00,   rl00,    w00,
     & rl00,    w10,   rl00,    w10,
     & rl00,    w20,   rl00,    w20,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10/
c
c     # loop type 12:
      data st12, db12
     & /  3,     1,     0,    -1,
     & rl00, wrb00,    r0,   rt0,
     & rl00, wrb10,    r0,   rt0,
     & rl00, wrb20,    r0,   rt0,
     & rl00,   rb0,    r0, wrt00,
     & rl00,   rb0,    r0, wrt10,
     & rl00,   rb0,    r0, wrt20,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0/
c
c     # loop type 13:
      data st13, db13
     & /  3,      2,     0,     -2,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00/
c
c     # loop type 14:
      data st14, db14
     & /  3,     3,
     & rl00,  ww00,
     & rl00,  ww10,
     & rl00,  ww20,
     & rl00,   w00,
     & rl00,   w10,
     & rl00,   w20/
c
c     # segment values are calculated as:
c     #
c     #        tab( type(bcase,kcase,delb), bket)
c     #
c     # where the entries in tab(*,*) are defined in the table
c     # initialization routine.  type(*,*,*) is equivalenced to the
c     # appropriate  location of pt(*,*,*) and pt(*,*,*) is actually
c     # referenced with the corresponding offset added to delb.  in
c     # this version, this offset is determined from the range offset
c     # and from the reference occupation, imu, of the orbital.
c     #
c     # e.g. tab( pt(bcase,kcase,db0(r,imu)+delb), bket)
c
c**********************************************************
c     # w segment value pointers (assuming mu(*)=0):
      data w0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top segment value pointers:
      data rt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2 ,1 ,1 ,1,  2, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-bottom segment value pointers:
      data lb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  2, 1, 1, 1,  1,20, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1,  1, 1, 1, 1,  1, 1,18, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom segment value pointers:
      data rb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 2, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-top segment value pointers:
      data lt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 2, 1,  1, 1, 1,18,  1, 1, 1,20,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r segment value pointers:
      data r/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l segment value pointers:
      data l/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,33, 1, 1,  1,10, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3,11, 1,  1, 1,33, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=0):
      data wrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=0):
      data wrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=0):
      data ww0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=0):
      data wr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-top, x=0 segment value pointers:
      data rtrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  6, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-r-bottom,x=0  segment value pointers:
      data rbrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 6,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=0):
      data rtrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 2, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 2, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=0 segment value pointers:
      data rrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1, 54, 1, 1, 1,  1, 7, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 44, 1, 1, 1,  1, 1, 1, 1,  1, 1, 7, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=1 segment value pointers:
      data rrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 55, 1, 1, 1,  1,53,24, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 43, 1, 1, 1,  2, 1, 1, 1,  1,14,45, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=0 segment value pointers:
      data rrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,48,49, 1,  1, 1, 1, 7,  1, 1, 1, 7,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=1 segment value pointers:
      data rrb1/
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1,23,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,50,48, 1,  1, 1, 1,46,  1, 1, 1,52,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1,17,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=0 segment value pointers:
      data rr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=1 segment value pointers:
      data rr1/
     & 2, 1, 1, 1,  1, 2, 1, 1,  1,30,41, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,38,27, 1,  1,29,40, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,36,26, 1,  1, 1, 2, 1,  1, 1, 1, 2/
c**********************************************************
c     # r-top-l-bottom segment value pointers:
      data rtlb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 20, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 18, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l-top, x=1 segment value pointers:
      data rtlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,47, 2, 1,  1, 2,51, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-l-bottom, x=1 segment value pointers:
      data rblb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1,34, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,45, 1, 1,  1, 1,53, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1,31, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l, x=1 segment value pointers:
      data rtl1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 51, 1, 1, 1,  1,48,21, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 47, 1, 1, 1,  2, 1, 1, 1,  1,19,50, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-l-bottom, x=1 segment value pointers:
      data rlb1/
     & 1, 1, 1, 1,  1, 1, 1, 1, 34, 1, 1, 1,  1,25, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 45, 1, 1, 1, 53, 1, 1, 1,  1,50,48, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 31, 1, 1, 1,  1, 1, 1, 1,  1, 1,15, 1/
c**********************************************************
c     # r-l-top, x=1 segment value pointers:
      data rlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,53, 2, 1,  1, 1, 1,23,  1, 1, 1,56,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2,45, 1,  1, 1, 1,42,  1, 1, 1,17,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=0 segment value pointers:
      data rl0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=1 segment value pointers:
      data rl1/
     & 2, 1, 1, 1,  1,35, 1, 1,  1,13,35, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,37,28, 1,  1,28,39, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,32, 9, 1,  1, 1,32, 1,  1, 1, 1, 2/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=1):
      data w1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=2):
      data w2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=1):
      data ww1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=2):
      data ww2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 4, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=1):
      data wrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 58, 1, 1, 1, 58, 1, 1, 1,  1,66,67, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=2):
      data wrt2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  3, 1, 1, 1,  3, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=1):
      data wrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,58, 1, 1,  1, 1, 1, 1,  1, 1, 1,68,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1,58, 1,  1, 1, 1,65,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=2):
      data wrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=1):
      data wr1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=2):
      data wr2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 2, 1, 1,  1,60,35, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1,32,59, 1,  1, 1, 2, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottomsegment value pointers (assuming mu(*)=1):
      data rtrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,57, 1, 1,  1,64,70, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,69,63, 1,  1, 1,57, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=2):
      data rtrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1,62,34, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31,61, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c
c     # segment extension tables:
c     # casex(*,i) for i=-2 to 2 are extensions for occupation changes
c     #                     of i.
c     #                i=3 is a special case for diagonal extensions.
c
      data nex/ 1, 4, 6, 4, 1, 4/
*@ifdef reference
*C    # debug versions to agree with ancient ft program. -rls
*     data bcasex/
*     +   0, 0, 0, 0, 0, 0,
*     +   0, 0, 2, 1, 0, 0,
*     +   0, 1, 1, 2, 2, 3,
*     +   1, 2, 3, 3, 0, 0,
*     +   3, 0, 0, 0, 0, 0,
*     +   0, 1, 2, 3, 0, 0/
*     data kcasex/
*     +   3, 0, 0, 0, 0, 0,
*     +   1, 2, 3, 3, 0, 0,
*     +   0, 1, 2, 1, 2, 3,
*     +   0, 0, 1, 2, 0, 0,
*     +   0, 0, 0, 0, 0, 0,
*     +   0, 1, 2, 3, 0, 0/
*@else
      data bcasex/
     & 0, 0, 0, 0, 0, 0,
     & 2, 1, 0, 0, 0, 0,
     & 3, 2, 2, 1, 1, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
      data kcasex/
     & 3, 0, 0, 0, 0, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 2, 1, 2, 1, 0,
     & 2, 1, 0, 0, 0, 0,
     & 0, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
*@endif
c
c     irrep multiplication table.
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c     loop-value to ft-value transformation matrices.
c
      data vtran /
     & one,   zero,  zero,   one,
     & one,   zero,  halfm,  one,
     & one,   onem,  one,    one,
     & half,  zero,  zero,   one,
     & one,   zero,  halfm,  s32 /
c
      data vcoeff /
     & one,   half,  s32,    s34 /

      end
