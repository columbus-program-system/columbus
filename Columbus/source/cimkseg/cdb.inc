!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER             DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER             DB13(4,0:2), DB14(2,0:2,2)
      INTEGER             DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER             DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER             DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER             DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON / CDB    /   DB1,         DB2,         DB3,         DB4
      COMMON / CDB    /   DB4P,        DB5,         DB6,         DB7
      COMMON / CDB    /   DB8,         DB10,        DB11,        DB12
      COMMON / CDB    /   DB13,        DB14
C
