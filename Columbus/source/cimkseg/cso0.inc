!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             CIST1,       CIST2,       CODE,        KLSYM
      INTEGER             MLP,         MLPPR,       NMIJ,        SGSM1
      INTEGER             SGSM2,       WTBRHD,      WTLPHD
C
      COMMON / CSO0   /   KLSYM,       NMIJ,        WTLPHD,      MLPPR
      COMMON / CSO0   /   MLP,         CODE,        SGSM1,       SGSM2
      COMMON / CSO0   /   CIST1,       CIST2,       WTBRHD
C
