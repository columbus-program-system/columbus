!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c
c
      subroutine diagon(
     & ncsf,   noldv,  noldhv, nroot,
     & niter,  nvxmax, nvxmin, frcsub,
     & lenrec, rtol,   nvfile, nhvfil,
     & ndiagf, core,   chrstr, chrtyp,
     & ivout,  itrnv,  itrnhv, iconv,
     & reflst,
     & heap,   totfre , aacsf,
     & ivmode ,rtflw,refflw,lrtshift,iprint,
     & ncouple )
ctm end
c
c  this routine finds eigenvectors and eigenvalues using the
c  davidson iterative method.
c
c  subroutine mult() performs the required matrix-vector products.
c  intermediate results are written to files nvfile and nhvfil.  input
c  expansion vectors are not required to be orthonormal, but they
c  should be linearly independent.  error limits, represented by
c  residual norms, are calculated for the lowest nroot eigenvalues.
c  the lowest eigenvalue "i" having an error greater than rtol(i) is
c  improved on the next iterative cycle until all nroot eigenvalues
c  are converged.
c
c  this routine also handles the ACPF and AQCC subspace method -
c  P. G. Szalay, 'Recent Advances in Coupled Cluster Theory,
c  ed. R.J. Bartlett, Word Scientific, 1997'
c
c  input:
c  ncsf = number of configuration state functions.
c  noldv = number of old v vectors on unit nvfile.
c  noldhv = number of old h*v vectors on unit nhvfil.
c  nroot = number of roots to calculate.
c  niter = maximum number of main iterations.
c  nvxmax = maximum number of expansion vectors to allow.
c  nvxmin = minimum number of vectors after subspace reduction.
c  frcsub = 0 don't recompute the subspace matrix representations from
c             the vectors after transformation.  compute them instead
c             by transforming the old subspace representations.
c         = 1 force subspace matrix recomputation.  this is useful when
c             many transformations must be performed to eliminate
c             cumulative roundoff error in the subspace representations.
c             this is rarely necessary, but is sometimes useful for
c             slowly convergent cases or for excited state calculations.
c         = 2 force recomputation of the matrix-vector products after
c             subspace transformation.  this is useful when roundoff
c             error is accumulated in the vector transformations.  this
c             is a relatively expensive option, and should essentially
c             never be required, but it is included for maximum
c             flexibility in extreme situations.
c  lenrec = record length in w.p. words of the direct-access files
c           nvfile, nhvfil, ndiagf, and filsc4.
c  rtol(*) = convergence tolerances on the residual norms.
c  nvfile, nhvfil, ndiagf = da files for vectors, products, and diagonal
c                           elements.
c  chrstr = either 'bk' or 'ci'.
c  ivout = which ci vector is to be writen out at convergence
c          onto the civec file.
c  itrnv,itrnhv = conditional output transformation codes for the
c          expansion vectors, v(*), and matrix-vector products, hv(*).
c              0: always transform the vectors on exit.
c              1: transform the vectors only if convergence is reached.
c              2: never transform the vectors.
c          note that some combinations don't make sense, but this
c          routine performs no consistency checking.
c  aacsf(*) = for active-active excitations set to 0
c  rtflw = root following flag
c        0 do not follow
c       >0 follows this root, it is assumed that h is diagonal in the
c           space of the available expansion vectors
c  refflw = reference vector to follow
c
c  heap(*) = memory which may be dynamically allocated.
c  totfre = amount of free memory remaining.
c
c  (the following arrays are not used directly by this routine but are
c   passed to mult() for use by other routines.)
c  core(*) = workspace passed to mult.
c  reflst(*) = integer list of tagged zpaths.
c
c  output:
c  noldv = number of v vectors written on unit nvfile.
c  noldhv = number of h*v vectors written on unit nhvfil.
c           if convergence is not reached, then noldv may be as
c           large as (noldhv+1).
c           note: on output, noldv and noldhv are consistent with the
c               itrnv and itrnhv parameters.
c  iconv = 0 for successful return.
c        = the index of the last unconverged vector otherwise.
c  a4den = factor for aqcc density
c
c  04-may-92 flushx() added. nroot->numv in write statement. -rls
c  16-feb-90 nvxmin added, cinewv() and cinewr() combined.  repnuc
c            contributions completely removed from e(*). -rls
c  18-oct-89 frcsub, itrnv, itrnhv added. segmented-vector based
c            subspace transformations, residual norm computations, and
c            subspace matrix constructions used.
c            this version also reintroduces several features that have
c            been deleted or disabled from previous versions. -rls
c  modified by p.g.szalay 20/4/87-segmented version with new subroutines
c  modified by rob eades 6/5/84 - chrstr included.
c  written by ron shepard 8/2/82
c
         IMPLICIT NONE

C
C     External functions
C
      EXTERNAL            ATEBYT, FORBYT
C
      INTEGER         ATEBYT, FORBYT
C
C     Parameter variables
C
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER         NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         NVRDIM
      PARAMETER           (NVRDIM = NVMAX)
      INTEGER         NNVDIM
      PARAMETER           (NNVDIM = (NVRDIM*(NVRDIM+1))/2)
      INTEGER         TMINIT
      PARAMETER           (TMINIT = 1)
      INTEGER         TMPRT
      PARAMETER           (TMPRT = 2)
      INTEGER         TMREIN
      PARAMETER           (TMREIN = 3)
      INTEGER         TMCLR
      PARAMETER           (TMCLR = 4)
      INTEGER         TMSUSP
      PARAMETER           (TMSUSP = 5)
      INTEGER         TMRESM
      PARAMETER           (TMRESM = 6)
      INTEGER         WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER         NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
C
      REAL*8            ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8            ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      CHARACTER*2         CHRSTR
      CHARACTER*8         CHRTYP
C
      INTEGER         AACSF(*),    FRCSUB,      ICONV
      INTEGER         IPRINT,      ITRNHV
      INTEGER         ITRNV,       IVMODE,      IVOUT,       LENREC
      INTEGER         NCOUPLE,     NCSF,        NDIAGF,      NHVFIL
      INTEGER         NITER,       NOLDHV,      NOLDV,       NROOT
      INTEGER         NVFILE,      NVXMAX,      NVXMIN
      INTEGER         REFLST(*),   RTFLW,       RTMODE,      TOTFRE
C
      REAL*8            CORE(*),     HEAP(TOTFRE)
      REAL*8            LRTSHIFT,    RTOL(*)
C
C     Local variables
C
      CHARACTER*8         CHRSTR2(0:4)
C
      INTEGER         I,           ICD(4),      IDVITM,      ITER
      INTEGER         ITLAST,      ITOTAL,      ITRAN,       ITRHV
      INTEGER         ITRITM,      ITRV,        NROOTI,      NSUBV
      INTEGER         NUME,        NUMH2,       NUMHV,       NUMV
      INTEGER         REFFLW
      integer             ilo,ihi,jlo,jhi
C
      REAL*8            BIGS,        EOLD(NVRDIM)
      REAL*8            H2(NNVDIM),  HT(NNVDIM),  HTSAV(NNVDIM)
      REAL*8            SCRA(NVRDIM*NVRDIM),      SCRB(NVRDIM*NVRDIM)
      REAL*8            SOVL(NNVDIM),             SOVL2(NNVDIM)
      REAL*8            U(NVRDIM*NVRDIM)

C
       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex


        integer mitob
        integer mdtob
C

C

c     # note: in this version, apxde(*) is only computed for the
c     #       last vector to converge.
c     #       resid(*) is computed accurately only for 1:nroot.
c     #       remaining elements may not be accurate [see cinewv()].
c
c     # e(*)      = final energies.
c     # resid(*)  = final residual norms.
c     # deltae(*) = energy change from the last iteration.
c     # apxde(*)  = approximate energy lowerings for the next iteration.
C     Common variables
C
C
c
c     # repnuc = nuclear_repulsion + frozen_core + repartitioning energy
C     Common variables
c     # acpf infos
c     # ncorel = number of correlated electrons
c     # method = 1 = cepa0 2=acpf 3=aqcc 4=aqccv
c     # gvalue = the (one!) required gvalue
c     # eref(nvmax) reference energies
C     Common variables
C
      REAL*8            APXDE(NVMAX),             DELTAE(NVMAX)
      REAL*8            E(NVMAX),    RESID(NVMAX)
C
      COMMON /ECI    /  E,           RESID,       DELTAE,      APXDE
C
c
c  contains the status of the current Davidson iteration
c
c  apxde (*)  estimate of the approximate energy lowering for next itera
c  deltae(*)  achieved energy lowering for current iteration
c  e(*)       current total energy
c  resid(*)   current residuum
c
c
C     Common variables
C
      REAL*8            REPNUC
C
      COMMON /REP    /  REPNUC
C
c
c  nuclear repulsion energy

C     Common variables
C
      INTEGER         BL0XT,       BL1XT,       BL2XT, BL4XT
      INTEGER         FILV,        FILW,        ISKIP
      INTEGER         NZPTH
C
      COMMON /INF3   /  ISKIP,       NZPTH  , BL4XT
      COMMON /INF3   /  BL2XT,       BL1XT,       BL0XT,       FILV
      COMMON /INF3   /  FILW
C
c BL0XT,          number of blocks of all-internal integrals
c    BL1XT,       number of blocks of one-external integrals
c    BL2XT,       number of blocks of two-external integrals
c   FILV,         unit number of v-vector file
c      FILW,      unit number of w vector file
c      ISKIP      controls operation of mult
c NZPTH           number of valid z walks ( possibly mixed-up with nvalz

C     Common variables
C
      INTEGER         BL0INT,      BL1INT,      BL2INT
C
      COMMON /SOINF3 /  BL2INT,      BL1INT,      BL0INT
C
c
c start records for
c bl0int  all-external so
c bl1int  one-external so
c bl2int  two-external so
c

C     Common variables
C
      INTEGER         METHOD,      NCOREL
C
      REAL*8            A4DEN(NVMAX),             EREF(NVMAX), GVALUE
C
      COMMON /ACPFINFO/ NCOREL,      METHOD,      GVALUE,      EREF
      COMMON /ACPFINFO/ A4DEN
C
c  NCOREL    number of correlated electrons
c  METHOD    CI(0),Cepa0(1),Acpf(2),Aqcc(3),aqccv(4)
c            AQCC/LRT (30)
c  GVALUE    g-value
c  EREF(i)   CI energy from reference space diagonalization for root i
c  A4DEN(i)  a4den factor for root i

c michal2{
c
c Silmar - common blocks added because of cosmo implementation
c
       integer maxorb,norb,maxdens,maxrep
       real*8 Vsolv,edielnew,sumq,sumq1,sumq2,bohr
       parameter(maxorb=1023,maxrep=8,bohr=0.529177249d0)
       common/solvent/Vsolv(maxorb*(maxorb+1)/2),
     . norb,maxdens
       real*8 Vnenuc,xfinal(maxrep),yfinal(maxrep),zfinal(maxrep)
       common/Vne3/Vnenuc
c
c Common block to solv the error with nsubv, for cosmo
c
c
        integer icosin,icosa2,icosa3,icosa,icosco,icossu,
     . icosout,icolum
c
      common /cosmovar/icosin, icosa2, icosa3, icosa, icosco,
     & icossu, icosout, icolum
c
        save /cosmovar/
c
c Silmar - variables to be saved from  the
c  previous iteration
c
       real*8 repnucold,edielold,elastold
       real*8 deltaediel,deltaelast
       real*8 enlast
       common/lasten/enlast
c
      integer   nimomx,maxsym
      parameter(nimomx= 128,maxsym=8)
c
      integer nmpsy,nbpsy,napsy,modrt2
      common /orbinf/nmpsy(maxsym),nbpsy(maxsym),napsy(maxsym),
     &               modrt2(nimomx)
         integer nsym
      integer cosmocalc
      common/cosmo2/cosmocalc
c common block for iref
      logical ifref
      common/denmode/ifref
c
      logical lastcall
c
      integer loc1a,loc1b,loc2,loc3,loc4
      integer loc5,locmx
C
      INTEGER         BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER         INTORB,      LAMDA1
      INTEGER         MAXBL2,      MAXLP3
      INTEGER         MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER         MXORB,       N0EXT
      INTEGER         N1EXT,       N2EXT
      INTEGER         N3EXT,       N4EXT
      INTEGER         ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER         NEXT,        NFCT,        NINTPT
      INTEGER         NMONEL,      NVALW,       NVALWK
      INTEGER         NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER         PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER         SPCCI,       TOTSPC
C
      COMMON /INF    /  PTHZ,        PTHY,        PTHX,        PTHW
      COMMON /INF    /  NINTPT,      DIMCI
      COMMON /INF    /  LAMDA1,      BUFSZI
      COMMON /INF    /  NMONEL,      INTORB
      COMMON /INF    /  MAXLP3,      BUFSZL
      COMMON /INF    /  MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON /INF    /  NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON /INF    /  ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON /INF    /  N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON /INF    /  SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON /INF    /  NVALZ,       NVALY,       NVALX,       NVALW
      COMMON /INF    /  NFCT,        NELI

c michal2}

C     Common variables
C
      INTEGER         UCIOPT(NCIOPT)
C
      COMMON /CCIOPT /  UCIOPT
C
c  some options which should be obsolete
c
c
c   uciopt(1)       lvlprt
c   uciopt(2)       iden
c   uciopt(3)       unused
c   uciopt(4)       itran
c   uciopt(5)       iroot

C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c michal2{
c
c
c Silmar - files, common block and variables  needed for cosmo-module
c
      integer maxat,natoms,maxnps
      parameter(maxat=350, maxnps = 2000)
      integer mtype(maxat)
      real*8 xyz(3,maxat),xyzpoints(3)
      real*8 cosurf(3,2*maxnps),phi(maxnps)
      real*8 qcos(maxnps),qcoszero(maxnps),symf
      real*8 qcosaux(maxnps),elastprime,qcosdalton(maxnps)
      real*8 ediel,elast,qq,phi0,qcossave(maxnps)
      real*8 a1mat(maxnps*(maxnps+1)/2),rtolcosmo
      real*8 a2mat(maxnps,maxnps),phizero(maxnps),dcos(3,maxat)
      real*8 phiground(maxnps)
      real*8 edielcorr,a3mat(maxnps*(maxnps+1)/2)
      character*26 atext,string
c
      real*8 ddot_wr
      external ddot_wr
c
      character*3 symgrp
      logical convcheck,cosmomode,start,freezing,valid
      integer nps,npspher,j,pp
      integer dbglvl,isymf,np
      integer rootcalc
      integer cosmocalc_save
c
       real*8 eps,fepsi,disex,disex2,rsolv,routf,area,refind,
     . fn2,srad,tm,volume,phsran,ampran
       integer nspa,nsph,nppa,lcavity,nn
c
      common /coss/ eps, fepsi, disex, disex2, rsolv, routf,
     &               nspa, nsph, nppa, area, refind, fn2,
     &               lcavity, srad(maxat), tm(3,3,maxat),
     &               nn(3,maxat), volume, phsran, ampran
c
c michal2}
c
c

C     Common variables
C
      INTEGER         MXOV(NVMAX), NREFVEC,rtflwrest,refflwrest
C
      REAL*8            SOVREF(NVMAX*NVMAX),      TCIREF(NVMAX*NVMAX)
C
      COMMON /LRT    /  SOVREF,      TCIREF,      MXOV,     NREFVEC,
     .                    rtflwrest,refflwrest
C
c
c stuff needed for rootfollowing
c
c  sovref       overlap of CI vectors with reference vectors
c  tciref       transformation matrix to track subsequent
c               subspace transformations
c  mxov         maximum overlap with reference number
c  nrefvec      number of reference vectors
c  rtflwrest    root to follow in case of restart
c  refflwrest   reference vector to follow in case of restart
c
c
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
      INTEGER MAXTASK
      parameter (maxtask=16384)
      INTEGER BSEG(MAXTASK), KSEG(MAXTASK),
     .        TASK(MAXTASK),NTASK,
     .        SLICE(MAXTASK),LOOPCNT(MAXTASK),
     .        CURRTSK
      REAL*8 TASKTIMES(MAXTASK),MFLOP(MAXTASK)
      COMMON/TASKLST/MFLOP,
     .      TASKTIMES,BSEG,KSEG,TASK,SLICE,
     .      LOOPCNT,NTASK,CURRTSK


c
c tasklist
c BSEG      bra segment
c KSEG      ket segment
c TASK      task type
c NTASK     total number of task
c SLICE     segmentation type
c loopcnt   loopcnt for debugging
c currtsk   current task
c tasktimes time spent in task
c
c
       integer nlist,nslist,filsgm,filsc4
       integer lvlprt

       logical cosmomodint




       data chrstr2/'mr-sdci ','cepa(0) ',
     .     'mracpf  ','mraqcc  ','mraqcc-v'/

      equivalence (lvlprt,uciopt(1))
      equivalence (nlist,nunits(1))
      equivalence (nslist,nunits(3))
      equivalence (filsgm,nunits(5))
      equivalence (filsc4,nunits(21))
c
c     # nvrdim = maximum subspace dimension allowed.
c     # ht(nvrdim*(nvrdim+1)/2) = reduced (h-repnuc) matrix.
c     # u(nvrdim*nvrdim) = eigenvectors of the ht(*) matrix.
c     # h2(nnvdim) = reduced (h-repnuc)**2 matrix.
c     # sovl(nndim) = reduced overlap matrix.
c     # e(nvrdim) = eigenvalues of the ht(*) matrix.
c     # scra(nvrdim*nvrdim) = scratch vector.
c     # scrb(nvrdim*nvrdim) = scratch vector.
c     # eold(nvrdim) = energies of the previous iteration.
c     # for ntype = 0  h2 contains the subspace matrix
c                     and sovl the reduced overlap matrix
c     # for ntype = 3 (method= 1..4)
c        h2 contains the subspace matrix plus diagonal shift
c        htsav contains the subspace matrix without shift
c        sovl contains the reduced overlap matrix
c        sovl2 is the overlap matrix excluding all contributions
c              due to active-active excitations
c
c     sovref(ref,ci) = overlap between ci eigen vectors
c                      and reference vector
c     tciref(ref,ci) = overlap matrix between v vectors and reference
C
c
c  if this is the final diagonalization call (chrstr='ci')
c  then the initial set of vectors are the vectors generated
c  by reference space diagonalization
c  (assumption: ivmode=3,8 nbkitr=0 !!)

c
c     # initialize some variables and arrays.
c     # nsubv = number of correct rows in the subspace matrices
c     #          ht(*), sovl(*).
c     # numh2 = number of correct rows in h2(*).
c     # numv = number of expansion vectors on nvfile.
c     # numhv = numver of matrix-vector products on nhvfil.
c
      itrv  = 0
      itrhv = 0
      nsubv = 0
      numh2 = 0
      numv  = noldv
      numhv = noldhv
      iconv = 1
      call wzero( nvrdim,        eold,   1 )
      call wzero( nvrdim*nvrdim, u,      1 )
      call wzero( nnvdim,        ht,     1 )
      call wzero( nnvdim,        h2,     1 )
      call wzero( nnvdim,        sovl,   1 )
      call wzero( nvrdim,        deltae, 1 )
      call wzero( nvrdim,        resid,  1 )
      call wzero( nvrdim,        apxde,  1 )
      call wset( noldv,  one,     u, (noldv+1) )


         itlast=iter
         cosmomodint=.false.
         call multcore(
     &     nvfile, (i),    nhvfil, (i),
     &       reflst, heap,   totfre  )
1180     continue
4010  continue
c
      return
      end
c
c --------------------------
c
      subroutine dmain(
     & ncsf,   nroot,  nvfile, nhvfil,
     & ndiagf,  noldv,
     & noldhv, nunitv, nbkitr, niter,
     & rtolbk, rtol,   nvbkmx, nvbkmn,
     & nvcimx, nvcimn, ibktv,  ibkthv,
     & icitv,  icithv, frcsub, core,
     & ivmode, ivout,  nzy,    istrt,
     & iortls, reflst,
     & aacsf,  heap,   totfre,
     & nrfitr, nvrfmx, nvrfmn, nvalz,
     & ntype , froot,  rtmode, pthyz,
     & confyz, mxbld,  next,   intorb,
     & bufszl, bufszi,lrtshift,ncouple   )
ctm end
c
c  this routine initializes trial vectors and controls the
c  various approximations used for generating the data sets
c  for the calls to subroutine diagon.
c
c  variable definitions:
c  ncsf = number of configuration state functions.
c  nroot = number of roots to calculate.
c  noldv = number of old v vectors on unit nvfile.
c  noldhv = number of old h*v products on unit nhvfil.
c  nunitv = number of unit vector columns to use for trial vectors.
c  nbkitr = number of bk type iterations to perform.
c  niter = number of main ci iterations.
c  rtolbk(*) = convergence tolerances on the bk iterative procedure.
c  rtol(*) = convergence tolerances on the eigenvalues.
c            rtolbk(*) and rtol(*) are residual norm tolerances.
c  nvbkmx = maximum number of bk expansion vectors.
c  nvbkmn = minimum number of bk vectors after subspace reduction.
c  nvcimx = maximum number of ci expansion vectors.
c  nvcimn = minimum number of ci vectors after subspace reduction.
c  ibktv, ibkthv,
c  icitv, icithv = conditional vector transformation flags for the
c                  bk and ci vectors.
c                <0 use default values. defaults depend on nbkitr and
c                   iortls.
c                =0 always transform.
c                =1 transform only upon convergence.
c                =2 never transform.
c  frcsub = 0 don't force subspace matrix recomputation after
c             subspace transformation.
c         = 1 force subspace matrix recomputation from the vectors
c             after every subspace transformation.
c         = 2 force recomputation of matrix-vector products after every
c             subspace transformation.
c  core(*) = workspace passed to mult.
c  ivmode = initial vector generation mode.
c          0  ci start vectors genereted internaly.
c          3  ci start vectors from diagonalization from href(*,*).
c  ivout = which ci vector is to be write out at convergence
c          onto civec file.
c  nzy = number of csfs that pass through the "z" and "y" vertices.
c  istrt = 0  start ci vector from the z walks.
c          1  start ci vector from the z and y walks.
c          2  start ci vector from the y walks.
c          this varible is important if trial vector genereted internaly
c  iortls = flag to modify default ci vector transformation codes.
c           0  always transform the ci vectors.
c           1  transform the ci vectors only on convergence.
c  reflst(*) = list of tagged zpaths for references (-1 = non ref)
c  aacsf(*)  = active-active excitations marked with "0" in this array
c  heap(*) = dynamic free space
c  totfre = amount of dynamic free space
c
c  nzcsf = number of csfs that pass through the "z" vertex.
c  iskip = 0  to perform bk-type iterations
c          1  to perform complete hamiltonian matrix-vector products.
c  froot = number of root to be followed
c  rtmode= root following modus 0: reference vector
c                               1: previous ci vector
c  lrtshift = if lrt calculation it is the diagonal shift
c  ncouple  = contains the number of iterations+1 without aqcc shift
c
c  files: nvfile = da expansion vector file.
c         nhvfil = da h*v file.
c         ndiagf = sequential file with diagonal h elements.
c         filsc4 = da file for the current trial vector and for
c                  the diagonal h elements.
c
c  17-jan-97 allow for ntype = 3  -tm
c  23-may-90 mcscf input dependencies stripped out -eas
c  16-feb-90 nvbkmn and nvcimn added. -rls
c  27-oct-89 minor changes due to diagon() modifications. nvbkmx,
c            nvcimx, ibktv, ibkthv, icitv, icithv, frcsub, added. -rls
c  modified by p.g.szalay 25/8/87- read start vector from mcscf program
c                               - construct start vector not only along
c                                 the z walks
c  modified by rob eades 6/5/84 - 'bk' and 'ci' strings included in
c                                 the calls to diag
c  written by ron shepard 8/6/82
c
         IMPLICIT NONE

C
C     External functions
C
      EXTERNAL            ATEBYT,      CNTREF,      FORBYT
C
      INTEGER         ATEBYT,      CNTREF,      FORBYT
C
C     Statement functions
C
      INTEGER         NNDX
C
C     Parameter variables
C
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER         MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         MXAVST
      PARAMETER           (mxavst = 50)
      INTEGER         NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER         NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER         NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
      INTEGER         IPRPMX
      PARAMETER           (IPRPMX = 130816)
C
C     Argument variables
C
      INTEGER         AACSF(*),    BUFSZI,      BUFSZL
      INTEGER         CONFYZ,      FRCSUB,      FROOT
      INTEGER         IBKTHV,      IBKTV,       ICITHV,      ICITV
      INTEGER         INTORB,      IORTLS
      INTEGER         ISTRT,       IVMODE,      IVOUT,       MXBLD
      INTEGER         NBKITR,      NCOUPLE,     NCSF,        NDIAGF
      INTEGER         NEXT,        NHVFIL,      NITER,       NOLDHV
      INTEGER         NOLDV,       NRFITR,      NROOT,       NTYPE
      INTEGER         NUNITV,      NVALZ,       NVBKMN,      NVBKMX
      INTEGER         NVCIMN,      NVCIMX,      NVFILE,      NVRFMN
      INTEGER         NVRFMX,      NZY,         PTHYZ
      INTEGER         REFLST(*),   RTMODE,      TOTFRE
C
      REAL*8            CORE(*),     HEAP(*)
      REAL*8            LRTSHIFT,    RTOL(NROOT), RTOLBK(NROOT)
C
C     Local variables
c
c michal2{
c
c lastcall introduced for cosmo calculation
c
      logical lastcall
c
c michal2{
C
      CHARACTER*8         CHRTYP
C
      INTEGER         FIRSTV,      GENV,        I,           IBKOUT
      INTEGER         ICONV,       IDUM,        IRFTHV,      IRFTV
      INTEGER         IROOT,       ISYM,        IVEC,        J
      INTEGER         LDA,         LOC1,        LOC2,        LOC3
      INTEGER         LOC4,        LOC5,        LOC6,        LOC7
      INTEGER         METHODSAVE,  MID,         NNBSKP(MAXSYM)
      INTEGER         NNOLDV,      NZCSF,       RTFLW,       XEREF
      INTEGER         XFROOT, locpthz
      integer             ilo,ihi,jlo,jhi,refflw
C
      REAL*8            BIGS
C
       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex

        integer mitob
        integer mdtob
C
C     Common variables
C
      INTEGER         BL0INT,      BL1INT,      BL2INT
C
      COMMON /SOINF3 /  BL2INT,      BL1INT,      BL0INT
C
c
c start records for
c bl0int  all-external so
c bl1int  one-external so
c bl2int  two-external so
c

C     Common variables
C
      INTEGER         BL0XT,       BL1XT,       BL2XT, BL4XT
      INTEGER         FILV,        FILW,        ISKIP
      INTEGER         NZPTH
C
      COMMON /INF3   /  ISKIP,       NZPTH  , BL4XT
      COMMON /INF3   /  BL2XT,       BL1XT,       BL0XT,       FILV
      COMMON /INF3   /  FILW
C
c BL0XT,          number of blocks of all-internal integrals
c    BL1XT,       number of blocks of one-external integrals
c    BL2XT,       number of blocks of two-external integrals
c   FILV,         unit number of v-vector file
c      FILW,      unit number of w vector file
c      ISKIP      controls operation of mult
c NZPTH           number of valid z walks ( possibly mixed-up with nvalz

C     Common variables
C
      REAL*8            APXDE(NVMAX),             DELTAE(NVMAX)
      REAL*8            E(NVMAX),    RESID(NVMAX)
C
      COMMON /ECI    /  E,           RESID,       DELTAE,      APXDE
C
c
c  contains the status of the current Davidson iteration
c
c  apxde (*)  estimate of the approximate energy lowering for next itera
c  deltae(*)  achieved energy lowering for current iteration
c  e(*)       current total energy
c  resid(*)   current residuum
c
c
C     Common variables
C
      LOGICAL             BKNOTILD,    DOTILD
C
      COMMON /TILDA  /  DOTILD,      BKNOTILD
C
c
c Tilda correction /AO-driven stuff only
c

C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

C     Common variables
C
      INTEGER         UCIOPT(NCIOPT)
C
      COMMON /CCIOPT /  UCIOPT
C
c  some options which should be obsolete
c
c
c   uciopt(1)       lvlprt
c   uciopt(2)       iden
c   uciopt(3)       unused
c   uciopt(4)       itran
c   uciopt(5)       iroot

C     Common variables
C
      LOGICAL             IFREF
C
      COMMON /DENMODE/  IFREF
C
c  a control variable in density part
c  should be eliminated !!
C     Common variables
C
      INTEGER         MODRT2(NIMOMX),           NAPSY(MAXSYM)
      INTEGER         NBPSY(MAXSYM),            NMPSY(MAXSYM)
C
      COMMON /ORBINF /  NMPSY,       NBPSY,       NAPSY,       MODRT2
C
c
c
c   nbpsy(*)  number of basis functions per irrep
c   nmpsy(*)  number of molecular orbitals per irrep
c   napsy(*)  nmpsy(*) -number of external basis functions per irrep
c   modrt2(*) mapping of mo to drtlevel excluding frozen core/frozen vir
c
c  nbas muss in cidrtfl ausgelesen und gesetzt werden statt aus cisrtif
c  zu werden!!!
c
C     Common variables
C
      INTEGER         MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
C
      COMMON /CSYM   /  MULT,        NSYM,        SSYM
C
c mult   standard multiplication table
c nsym   number of irreps
c ssym   state symmetry
C     Common variables
C
      INTEGER         METHOD,      NCOREL
C
      REAL*8            A4DEN(NVMAX),             EREF(NVMAX), GVALUE
C
      COMMON /ACPFINFO/ NCOREL,      METHOD,      GVALUE,      EREF
      COMMON /ACPFINFO/ A4DEN
C
c  NCOREL    number of correlated electrons
c  METHOD    CI(0),Cepa0(1),Acpf(2),Aqcc(3),aqccv(4)
c            AQCC/LRT (30)
c  GVALUE    g-value
c  EREF(i)   CI energy from reference space diagonalization for root i
c  A4DEN(i)  a4den factor for root i

C     Common variables
C
      INTEGER         MXOV(NVMAX), NREFVEC,rtflwrest,refflwrest
C
      REAL*8            SOVREF(NVMAX*NVMAX),      TCIREF(NVMAX*NVMAX)
C
      COMMON /LRT    /  SOVREF,      TCIREF,      MXOV,     NREFVEC,
     .                    rtflwrest,refflwrest
c
c   vecrln    record length of vector files
c   indxln    record length of index files
      integer indxln, vecrln
      common /daparm/  vecrln, indxln
C
      CHARACTER*60 FNAME(NFILMX)
      COMMON /CFNAME /  FNAME
c
c stuff needed for rootfollowing
c
c  sovref       overlap of CI vectors with reference vectors
c  tciref       transformation matrix to track subsequent
c               subspace transformations
c  mxov         maximum overlap with reference number
c  nrefvec      number of reference vectors
c  rtflwrest    root to follow in case of restart
c  refflwrest   reference vector to follow in case of restart
c
c
*@ifdef mrpt
*      INTEGER         PTGREF,      PTH0TYPE,    PTITER,      PTLEVL
*      INTEGER         PTLINSOL,    PTMETH,      PTMTST,      PTNAVST
*      INTEGER         PTNVMX,      PTROOT
*      REAL*8            PTSHIFT(MXAVST),          PTTOL
*      REAL*8            PTWAVST(MXAVST)
*      COMMON /MRPTX  /  PTGREF,      PTLEVL,      PTMETH,      PTITER
*      COMMON /MRPTX  /  PTTOL,       PTROOT,      PTNAVST,     PTNVMX
*      COMMON /MRPTX  /  PTLINSOL,    PTSHIFT,     PTWAVST,     PTMTST
*      COMMON /MRPTX  /  PTH0TYPE
*C
*C
*C  variables which control MRPT stuff
*C
*@endif
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c
      integer iwrite,nlist,filev,filind,hvfile,civout,d1fl
      integer lvlprt,filevres

      equivalence (iwrite,nunits(1))
      equivalence (nlist,nunits(3))
      equivalence (filev,nunits(7))
      equivalence (filind,nunits(8))
      equivalence (nunits(6),hvfile)
      equivalence (civout,nunits(16))
      equivalence (d1fl,nunits(18))
      equivalence (lvlprt,uciopt(1))
      equivalence (filevres,nunits(46))
c
c     # repnuc = nuclear_repulsion + frozen_core + repartitioning energy
C
C
c
c     # acpf infos
c     # ncorel = number of correlated electrons
c     # method = 1 = cepa0 2=acpf 3=aqcc 4=aqccv
c     # gvalue = the (one!) required gvalue
c     # eref(nvmax) reference energies
c     # a4den  = a4den value for density calculation
c
      integer nvec,blksiz,icsf,blkcsf
      logical yesno
c
      nndx(i) = (i * (i - 1)) / 2
c
c-----------------------------------------------------------------------
c
ctm
c     nzcsf = nvalz ????? instead of nzpth ?????
      nzcsf = nzpth


      genv  = 0
      call ga_sync()


c      we just need one regular call to diagon
c
c      ivmode =0
c      noldv = 0
c      noldhv = 0
       noldv=0
       noldhv=0
       ivmode=0
       niter=1
      if ( niter .gt. 0 ) then
c
c        # prepare for the general call to routine diag.
c
         if (me.eq.0 ) then
           write(iwrite,6040)
           write(nlist,6040)
         endif
6040     format(/1x,72('*')/
     &    ' beginning the ci iterative diagonalization procedure... '
     &    /1x,72('*'))
c
c        # iskip=1  flags routine mult to perform complete
c        #          hamiltonain matrix-vector products.
c
         iskip = 1
         icitv=0
         icithv=0
         bknotild=.false.
         call diagon(
     &    ncsf,          noldv,  noldhv, nroot,
     &    niter,         nvcimx, nvcimn, frcsub,
     &    dalen(nvfile), rtol,   nvfile, nhvfil,
     &    ndiagf,        core,   'ci', chrtyp,
     &    ivout,         icitv,  icithv, iconv,
     &    reflst,
     &    heap,          totfre, aacsf,
     &    ivmode ,rtflw,refflw,lrtshift,1,ncouple )
c
      endif ! ( niter .gt. 0 )
c
      return
      end
c
