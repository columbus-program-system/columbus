!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
    SUBROUTINE driversize(ia,lencor,me,szdg,szof,sz3w,sz3x,sz4w,sz4x,nof1e, &
        srtscr,nvalz)
      USE maksort
      INTEGER lencor,nvalz, me, ia(lencor)
      INTEGER szdg, szof, sz3w, sz3x, sz4w, sz4x, nof1e, srtscr

      IF (me==0) THEN
        CALL drvsort(ia,lencor,szdg,szof,sz3w,sz3x,sz4w,sz4x,nof1e,srtscr,nvalz)
      END IF
      open (unit=99,file='fsizes')
      WRITE (99,*) 'diagint ', szdg*8
      WRITE (99,*) 'ofdgint ', szof*8
      WRITE (99,*) 'fil3x   ', sz3x*8
      WRITE (99,*) 'fil3w   ', sz3w*8
      WRITE (99,*) 'fil4x   ', sz4x*8
      WRITE (99,*) 'fil4w   ', sz4w*8
      WRITE (99,*) 'srtscr  ', srtscr*8
      close(99)
      RETURN
    END SUBROUTINE driversize
