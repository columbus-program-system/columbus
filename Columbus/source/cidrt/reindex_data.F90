!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
        module reindex_data
        implicit none

      TYPE, PUBLIC :: pconsttyp
        REAL*8 zero
        REAL*8 half
        REAL*8 halfm
        REAL*8 one
        REAL*8 two
        INTEGER wrnerr
        INTEGER nfterr
        INTEGER faterr
        INTEGER tmin0
        INTEGER tminit
        INTEGER tmprt
        INTEGER tmrein
        INTEGER tmclr
        INTEGER tmsusp
        INTEGER tmresm
        INTEGER qprt
        INTEGER INTLEN
      END TYPE pconsttyp

#ifdef INT64
      TYPE (pconsttyp), PARAMETER :: pconst = pconsttyp(0.0D0,0.5D0,-0.5D0, &
        1.0D0,2.0D0,0,1,2,0,1,2,3,4,5,6,0,1)
#else
      TYPE (pconsttyp), PARAMETER :: pconst = pconsttyp(0.0D0,0.5D0,-0.5D0, &
        1.0D0,2.0D0,0,1,2,0,1,2,3,4,5,6,0,2)
#endif
      TYPE, PUBLIC :: csymtyp
        INTEGER mult(8,8)
      END TYPE csymtyp
       type(csymtyp) :: csym



      TYPE, PUBLIC :: pdimtyp
        INTEGER mxinfo
        INTEGER maxsym
        INTEGER nmomx
        INTEGER nrowmx
        INTEGER nimomx
        INTEGER iprpmx
        INTEGER maxtask
        INTEGER multmx
        INTEGER nmulmx
        INTEGER nfilmx
        INTEGER mxnseg
        INTEGER maxbf
        INTEGER nvmax
        INTEGER nwlkmx
        INTEGER nciopt
        INTEGER maxat
        INTEGER maxnps
        INTEGER ntitmx
        INTEGER nsifmx
        INTEGER nst, nst5, ntab, maxb
        INTEGER nengmx, mxintblk, maxs, maxnum, maxtyp, maxtypes
        INTEGER maxmap
        INTEGER nvmax2
        INTEGER nimomx2
        INTEGER maxdrt
      END TYPE pdimtyp

      TYPE (pdimtyp), PARAMETER :: pdim = pdimtyp( &
           10,  &       ! mxinfo
           8,   &       ! maxsym 
           1023, &       ! nmomx 
           1023,&       ! nrowmx
           128, &       ! nimomx 
           1023*(1023+1),&! iprpmx  
           16384,&      !  maxtask 
           19,&         ! multmx 
            9,&         ! nmulmx 
            99, &       ! nfilmx 
            300,&       ! mxnseg 
            35000,&     ! maxbf 
             40, &      ! nvmax 
            2**20-1,&   ! nwlkmax 
            10, &       ! nciopt 
            50, &       ! maxat 
            2000, &     ! maxnps
            30,   &     ! ntitmx 
            160,  &     ! nsifmx 
            40,   &     !  nst
            40*5, &     ! nst5
            70,   &     ! ntab 
            19,   &     ! maxb
            11,   &     ! nengmx 
            20,   &     ! mxintblk 
            11,   &     ! maxs 
            134,  &     ! maxnum 
            19,   &     ! maxtyp 
            18,   &     ! maxtypes 
            5,    &     ! maxmap
            1600, &     ! nvmax2
            8256, &     ! nimomx2
            32  )       ! maxdrt

        type, public :: drtinfotyp
          integer spnorb,spnodd,lxyzir(3),hmult,nmul
          integer nmot,niot,nfct,nrow,nsym,ssym,neli
          integer pthzyxw(4),nvalzyxw(4),ncsft,intorbsodd
          character*4 labels(pdim%nimomx)
          integer,dimension (pdim%nmomx) :: map,momap 
          integer,dimension (pdim%nimomx) ::  mu,bord,modrt2,bsym
          integer, dimension (pdim%maxsym) :: nbpsy,nmpsy,nepsy
          integer, dimension(pdim%maxsym,4) :: nviwsm,ncsfsm,nexw
          integer, dimension (pdim%nrowmx) :: a,b
          integer,dimension (pdim%nimomx+1) :: nj,njskp 
          integer  arcsym(0:3,0:pdim%nimomx)
          integer  y(0:3,pdim%nrowmx,1:3),l(0:3,pdim%nrowmx,1:3)
          integer  xbarcdrt(pdim%nrowmx,1:3)
          integer  spnir(pdim%multmx,pdim%multmx),multp(pdim%multmx)
        end type drtinfotyp


        type, public :: inpdatatyp
          CHARACTER*80  fn_civfl(pdim%maxdrt),fn_civflso(pdim%maxsym)
          CHARACTER*80  fn_cidrtfl(pdim%maxdrt),fn_cidrtflso(pdim%maxsym),fn_civoutso(pdim%maxsym)
          INTEGER       nroot(pdim%maxdrt),printlevel,nsym,lrtaqcc,rtirrep(pdim%maxsym),soirrep(pdim%maxsym)
          CHARACTER*4   slabel(pdim%maxsym)
          REAL*8        energy(pdim%multmx,pdim%maxsym,10)
        end type inpdatatyp
        type(inpdatatyp) :: inputdata

        type, public :: memorytyp
           integer     socnt(4,pdim%maxsym)
        end type memorytyp
        type (memorytyp) :: memory

        type, public :: sostattyp
            integer   ncsf(4,pdim%maxsym,19,pdim%maxsym) 
            integer   nvwalks(4,pdim%maxsym,19,pdim%maxsym) 
        end type sostattyp 
        type(sostattyp)   sostatnr,sostatnrdbl

       data csym%mult &
                    /1, 2, 3, 4, 5, 6, 7, 8, &
                     2, 1, 4, 3, 6, 5, 8, 7, &
                     3, 4, 1, 2, 7, 8, 5, 6, &
                     4, 3, 2, 1, 8, 7, 6, 5, &
                     5, 6, 7, 8, 1, 2, 3, 4, &
                     6, 5, 8, 7, 2, 1, 4, 3, &
                     7, 8, 5, 6, 3, 4, 1, 2, &
                     8, 7, 6, 5, 4, 3, 2, 1/




        end module reindex_data
