Pseudo Basis(1s,1p,1d):  Hay and Wadt, JCP 82, 270 (1985). ncore=68
   3
   3  1  1
        0.3314000D+00      -0.1107456D+01
        0.2314000D+00       0.1152236D+01
        0.5660000D-01       0.8217259D+00
   3  2  1
        0.4960000D+00      -0.5399860D-01
        0.8900000D-01       0.4325121D+00
        0.2800000D-01       0.6707919D+00
   3  3  1
        0.1116000D+01       0.3725292D+00
        0.4267000D+00       0.5014277D+00
        0.1378000D+00       0.3341287D+00
Pseudo Basis(2s,2p,2d):  Hay and Wadt, JCP 82, 270 (1985). ncore=68
   6
   2  1  1
        0.3314000D+00      -0.4423577D+01
        0.2314000D+00       0.4602443D+01
   1  1  1
        0.5660000D-01       0.1000000D+01
   2  2  1
        0.4960000D+00      -0.1311370D+00
        0.8900000D-01       0.1050367D+01
   1  2  1
        0.2800000D-01       0.1000000D+01
   2  3  1
        0.1116000D+01       0.4644937D+00
        0.4267000D+00       0.6252127D+00
   1  3  1
        0.1378000D+00       0.1000000D+01
Pseudo Basis(2s,2p,1d):  Hay and Wadt, JCP 82, 299 (1985). ncore=60
   5
   3  1  1
        0.2185000D+01      -0.1622373D+01
        0.1451000D+01       0.1993865D+01
        0.4585000D+00       0.4531166D+00
   5  1  1
        0.2185000D+01       0.6670413D+00
        0.1451000D+01      -0.8956845D+00
        0.4585000D+00      -0.5144151D+00
        0.2314000D+00       0.5295367D+00
        0.5660000D-01       0.8745771D+00
   3  2  1
        0.3358000D+01      -0.2318667D+00
        0.1271000D+01       0.7458683D+00
        0.4644000D+00       0.4632686D+00
   3  2  1
        0.4960000D+00      -0.5399860D-01
        0.8900000D-01       0.4325121D+00
        0.2800000D-01       0.6707919D+00
   3  3  1
        0.1116000D+01       0.3753542D+00
        0.4267000D+00       0.4969832D+00
        0.1378000D+00       0.3370171D+00
Pseudo Basis(3s,3p,2d):  Hay and Wadt, JCP 82, 299 (1985). ncore=60
   8
   3  1  1
        0.2185000D+01      -0.1622373D+01
        0.1451000D+01       0.1993865D+01
        0.4585000D+00       0.4531166D+00
   4  1  1
        0.2185000D+01       0.1545975D+01
        0.1451000D+01      -0.2075893D+01
        0.4585000D+00      -0.1192240D+01
        0.2314000D+00       0.1227286D+01
   1  1  1
        0.5660000D-01       0.1000000D+01
   3  2  1
        0.3358000D+01      -0.2318667D+00
        0.1271000D+01       0.7458683D+00
        0.4644000D+00       0.4632686D+00
   2  2  1
        0.4960000D+00      -0.1311370D+00
        0.8900000D-01       0.1050367D+01
   1  2  1
        0.2800000D-01       0.1000000D+01
   2  3  1
        0.1116000D+01       0.4689888D+00
        0.4267000D+00       0.6209591D+00
   1  3  1
        0.1378000D+00       0.1000000D+01
