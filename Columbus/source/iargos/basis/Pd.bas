Pseudo Basis(1s,1p,1d):  Hay and Wadt, JCP 82, 270 (1985). ncore=36
   3
   3  1  1
        0.4416000D+00      -0.3594574D+00
        0.1496000D+00       0.5167561D+00
        0.4360000D-01       0.7414499D+00
   3  2  1
        0.7368000D+00       0.3445780D-01
        0.8990000D-01       0.4397064D+00
        0.2620000D-01       0.6525627D+00
   4  3  1
        0.6091000D+01       0.4472930D-01
        0.1719000D+01       0.4425814D+00
        0.6056000D+00       0.5051035D+00
        0.1883000D+00       0.2450132D+00
Pseudo Basis(2s,2p,2d):  Hay and Wadt, JCP 82, 270 (1985). ncore=36
   6
   2  1  1
        0.4416000D+00      -0.1166042D+01
        0.1496000D+00       0.1676302D+01
   1  1  1
        0.4360000D-01       0.1000000D+01
   2  2  1
        0.7368000D+00       0.7632850D-01
        0.8990000D-01       0.9740065D+00
   1  2  1
        0.2620000D-01       0.1000000D+01
   3  3  1
        0.6091000D+01       0.5119570D-01
        0.1719000D+01       0.5065641D+00
        0.6056000D+00       0.5781248D+00
   1  3  1
        0.1883000D+00       0.1000000D+01
Pseudo Basis(2s,2p,1d):  Hay and Wadt, JCP 82, 299 (1985). ncore=28
   5
   3  1  1
        0.2787000D+01      -0.1610239D+01
        0.1965000D+01       0.1848984D+01
        0.6243000D+00       0.6037492D+00
   5  1  1
        0.2787000D+01       0.5271839D+00
        0.1965000D+01      -0.6533298D+00
        0.6243000D+00      -0.3332429D+00
        0.1496000D+00       0.3971289D+00
        0.4360000D-01       0.7836418D+00
   3  2  1
        0.5999000D+01      -0.1034910D+00
        0.1443000D+01       0.7456952D+00
        0.5264000D+00       0.3656494D+00
   3  2  1
        0.7368000D+00       0.3445780D-01
        0.8990000D-01       0.4397064D+00
        0.2620000D-01       0.6525627D+00
   4  3  1
        0.6091000D+01       0.3276000D-01
        0.1719000D+01       0.4529302D+00
        0.6056000D+00       0.4969642D+00
        0.1883000D+00       0.2502811D+00
Pseudo Basis(3s,3p,2d):  Hay and Wadt, JCP 82, 299 (1985). ncore=28
   8
   3  1  1
        0.2787000D+01      -0.1610239D+01
        0.1965000D+01       0.1848984D+01
        0.6243000D+00       0.6037492D+00
   4  1  1
        0.2787000D+01       0.1354078D+01
        0.1965000D+01      -0.1678085D+01
        0.6243000D+00      -0.8559381D+00
        0.1496000D+00       0.1020030D+01
   1  1  1
        0.4360000D-01       0.1000000D+01
   3  2  1
        0.5999000D+01      -0.1034910D+00
        0.1443000D+01       0.7456952D+00
        0.5264000D+00       0.3656494D+00
   2  2  1
        0.7368000D+00       0.7632850D-01
        0.8990000D-01       0.9740065D+00
   1  2  1
        0.2620000D-01       0.1000000D+01
   3  3  1
        0.6091000D+01       0.3761460D-01
        0.1719000D+01       0.5200479D+00
        0.6056000D+00       0.5706071D+00
   1  3  1
        0.1883000D+00       0.1000000D+01
