Nash-ECP Bk+2 basis; Pitzer group; compact cc-pvdz (4sd4p4f1g)/[3sd2p2f1g]
4
    4    3    3  / !No. primitives, PQN, No. contracted fns (Bk sd set)
        2.828000  -0.0994105  -0.0103482   0.0
        1.207000   0.7693014  -0.0224098   0.0
        0.488500   0.3630723   0.4985835   0.0
        0.155200   0.0074411   0.6382666   1.0
    4    2    2  / !No. primitives, PQN, No. contracted fns (Bk p set)
        9.955000  -0.0045752   0.0
        1.488000  -0.4597751   0.0
        0.837200   0.8609980   0.0
        0.295800   0.5586327   1.0
    4    4    2  / !No. primitives, PQN, No. contracted fns (Bk f set)
        5.578000   0.2122290   0.0
        2.380000   0.4666856   0.0
        0.959900   0.4101006   0.0
        0.343500   0.1895601   1.0
  1  5  1  / Bk g  (4sd4p4f1g)/[3sd2p2f1g] cc-g opt srb,rmp.
     2.387000    1.0
