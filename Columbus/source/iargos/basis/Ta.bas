Pseudo Basis(1s,1p,1d):  Hay and Wadt, JCP 82, 270 (1985). ncore=68
   3
   3  1  1
        0.3084000D+00      -0.6940372D+00
        0.1671000D+00       0.7824080D+00
        0.4820000D-01       0.7770293D+00
   3  2  1
        0.4360000D+00      -0.7373340D-01
        0.8400000D-01       0.5172072D+00
        0.2800000D-01       0.5910988D+00
   3  3  1
        0.8948000D+00       0.3884774D+00
        0.2989000D+00       0.5224911D+00
        0.9350000D-01       0.3190890D+00
Pseudo Basis(2s,2p,2d):  Hay and Wadt, JCP 82, 270 (1985). ncore=68
   6
   2  1  1
        0.3084000D+00      -0.2445236D+01
        0.1671000D+00       0.2756584D+01
   1  1  1
        0.4820000D-01       0.1000000D+01
   2  2  1
        0.4360000D+00      -0.1513014D+00
        0.8400000D-01       0.1061312D+01
   1  2  1
        0.2800000D-01       0.1000000D+01
   2  3  1
        0.8948000D+00       0.4746917D+00
        0.2989000D+00       0.6384469D+00
   1  3  1
        0.9350000D-01       0.1000000D+01
Pseudo Basis(2s,2p,1d):  Hay and Wadt, JCP 82, 299 (1985). ncore=60
   5
   3  1  1
        0.2044000D+01      -0.1310478D+01
        0.1267000D+01       0.1657992D+01
        0.4157000D+00       0.4848337D+00
   5  1  1
        0.2044000D+01       0.5226071D+00
        0.1267000D+01      -0.7313962D+00
        0.4157000D+00      -0.4640101D+00
        0.1671000D+00       0.5201181D+00
        0.4820000D-01       0.8109632D+00
   3  2  1
        0.2565000D+01      -0.2917238D+00
        0.1229000D+01       0.7570649D+00
        0.4244000D+00       0.5240081D+00
   3  2  1
        0.4360000D+00      -0.7373340D-01
        0.8400000D-01       0.5172072D+00
        0.2800000D-01       0.5910988D+00
   3  3  1
        0.8948000D+00       0.3859128D+00
        0.2989000D+00       0.5219817D+00
        0.9350000D-01       0.3226398D+00
Pseudo Basis(3s,3p,2d):  Hay and Wadt, JCP 82, 299 (1985). ncore=60
   8
   3  1  1
        0.2044000D+01      -0.1310478D+01
        0.1267000D+01       0.1657992D+01
        0.4157000D+00       0.4848337D+00
   4  1  1
        0.2044000D+01       0.1189731D+01
        0.1267000D+01      -0.1665046D+01
        0.4157000D+00      -0.1056334D+01
        0.1671000D+00       0.1184065D+01
   1  1  1
        0.4820000D-01       0.1000000D+01
   3  2  1
        0.2565000D+01      -0.2917238D+00
        0.1229000D+01       0.7570649D+00
        0.4244000D+00       0.5240081D+00
   2  2  1
        0.4360000D+00      -0.1513014D+00
        0.8400000D-01       0.1061312D+01
   1  2  1
        0.2800000D-01       0.1000000D+01
   2  3  1
        0.8948000D+00       0.4731072D+00
        0.2989000D+00       0.6399200D+00
   1  3  1
        0.9350000D-01       0.1000000D+01
