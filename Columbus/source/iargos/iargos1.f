!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c deck iargos
      program iargos
c   version log:
c   03-oct-01 compute arrays rather than use data statements.  eliminate
c             unused routines.  general clean up. -rmp
c   10-apr-98 some dalton corrections. -tm
c   22-mar-97 version including dalton gradient input. -hl
c          ?? obtained from Utah messkit.
c  cmdc info:
c  keyword   description
c  -------   -----------
c  f90       f90 write statements
c  $format   vax style format statements
c  posix     posix 1003.9 library support
c
      implicit real*8 (a-h,o-z)
      parameter (mxang=6,mxang1=(mxang*(mxang-1))/2,mxang2=mxang1+mxang,
     & mxang3=mxang-1,mxang4=mxang3+mxang,mxang5=(mxang2*(mxang+2))/3,
     & mxao=90,mxaov=60,mxatm=500,mxatmsym=12,mxconatm=25,mxbfcr=20,
     & mxcons=500,mxconset=50,mxcrs=12,mxgen=5,mxhiblk=40,mxhirep=20,
     & mxlogen=3,mxlorep=8,mxorder=120,mxprim=24,mxso=90,mxsoset=20)
c
      parameter (deg2rad=3.1415926535d0/180.0d0,sqtol=1.0d-12,
     & thr9=1.0d-9,thr15=1.0d-15)
c
      real*8 atmas(mxatm),blkshft(mxaov,mxaov),blshco(mxorder),
     & ccr(mxbfcr,0:mxang3,mxcrs),chg(mxatm),chi(mxhirep,mxorder),
     & cls(mxbfcr,mxang,mxcrs),ctrans(3,3,mxorder),
     & eta(mxconset,mxprim,mxcons),fconvert,gen(mxorder,mxgen),
     & hiso(mxaov,mxaov,mxsoset),p(3),poly1(mxang2),poly2(mxang2),q(3),
     & projao(mxaov,mxaov,mxhirep),soaox(mxaov,mxaov),
     & soaoy(mxaov,mxaov+1),soao1(mxaov,mxaov),soao1s(mxaov,mxaov),
     & sox(mxaov),trans(mxorder,mxorder+1),transao(mxaov,mxaov),
     & xyz(3,mxatm+1,mxatm),xyz1(3,mxatmsym),
     & zcr(mxbfcr,0:mxang3,mxcrs),zet(mxprim,mxcons),
     & zls(mxbfcr,mxang,mxcrs)
c
      integer faterr,iang(mxatm,mxconatm),iatmp(2,mxatm),ibxyz(3),
     & ichoice,icomp(mxang3,mxang2,mxang),icont(mxatm,mxang),
     & iconu(mxcons),icsu(mxsoset),ictu(mxsoset),ideg(mxhirep),
     & igcs(mxconatm,mxatm),igenp(mxlogen),igenq(mxlogen),igrp(mxatm),
     & ihatmbas(mxatm),ihiatm(mxatm),ihigcs(mxconatm,mxatm),
     & ihiord(mxaov,mxsoset),ihoff(mxhirep),ihsamep(mxatm),
     & ihsend(mxatm,mxorder,mxatm),iirep(mxso,mxsoset),ijk(3),
     & ilofhi(mxhiblk),iperm(mxang2,mxang),ipiv(mxaov),
     & iprd(3,mxang1,mxang3),iproj(mxang2,mxatmsym,mxatmsym,mxlorep),
     & irepofhi(mxhiblk),irepos(mxhiblk),isamep(mxatm,mxatm),
     & isend(mxatmsym,mxlorep,mxatm),isize(mxhirep),isizl(mxlorep),
     & islist(mxconatm),isocoef(mxao,mxso,mxsoset),isox(mxao),itemp,
     & itop,itsend(mxatm,mxorder),izcr(mxcrs),jgcsl(mxatm),lang(mxatm),
     & lco(mxang2,mxang4,mxang),lcru(mxcrs),lenc,
     & lgen(mxlorep+3,mxlogen),llsu(mxcrs),lmnp1(mxcons),
     & lmnv(3,mxang5),loatb(mxatm),loatmbas(mxatm),
     & ltrans(mxlorep+3,mxlorep),maxpri,mcons(mxconatm,mxatm),
     & mcrs(mxatm),multed(mxgen),nbfcr(0:mxang3,mxcrs),
     & nbfls(mxang,mxcrs),nc(mxatm),nchi(mxatm),nchi1(mxatm),
     & ncr(mxbfcr,0:mxang3,mxcrs),nct(mxatmsym),nf(mxatm),nfl(mxang),
     & nls(mxbfcr,mxang,mxcrs),nrcr(mxcons),nsamep(mxatm),
     & nsymtp(mxsoset),ntp(mxatm,mxatm),pcount(mxcons)
c  lco(i,j,l) is the coefficients of the ith monomial in the jth SO with
c  well-defined angular momentum l-1.
c     data (((lco(i,j,l),i=1,l*(l+1)/2),j=1,2*l-1),l=1,mxang)
c     pad with zeros for mxang=6
      data lco /
     & 1, 230*0,
     & 0,0,1, 18*0,
     & 1,0,0, 18*0,
     & 0,1,0, 186*0,
     & -1,-1,4,0,0,0, 15*0,
     & 0,0,0,0,1,0, 15*0,
     & 0,0,0,0,0,1, 15*0,
     & 1,-1,0,0,0,0, 15*0,
     & 0,0,0,1,0,0, 141*0,
     & 0,0,4,0,-9,0,-9,0,0,0, 11*0,
     & -1,0,0,0,0,-1,0,16,0,0, 11*0,
     & 0,-1,0,-1,0,0,0,0,16,0, 11*0,
     & 0,0,0,0,1,0,-1,0,0,0, 11*0,
     & 0,0,0,0,0,0,0,0,0,1, 11*0,
     & -1,0,0,0,0,9,0,0,0,0, 11*0,
     & 0,-1,0,9,0,0,0,0,0,0, 95*0,
     & -9,-9,-64,0,0,0,0,0,0,-36,576,576,0,0,0, 6*0,
     & 0,0,0,0,-9,0,0,16,0,0,0,0,0,-9,0, 6*0,
     & 0,0,0,0,0,0,-9,0,16,0,0,0,-9,0,0, 6*0,
     & 0,0,0,1,0,1,0,0,0,0,0,0,0,0,-36, 6*0,
     & 1,-1,0,0,0,0,0,0,0,0,-36,36,0,0,0, 6*0,
     & 0,0,0,0,-1,0,0,0,0,0,0,0,0,9,0, 6*0,
     & 0,0,0,0,0,0,-1,0,0,0,0,0,9,0,0, 6*0,
     & 1,1,0,0,0,0,0,0,0,-36,0,0,0,0,0, 6*0,
     & 0,0,0,1,0,-1,0,0,0,0,0,0,0,0,0, 48*0,
     & 0,0,64,0,225,0,225,0,0,0,0,0,0,-1600,-1600,0,0,0,900,0,0,
     & 1,0,0,0,0,1,0,64,0,4,-144,0,0,0,0,0,0,0,0,0,-144,
     & 0,1,0,1,0,0,0,0,64,0,0,4,-144,0,0,0,0,0,0,-144,0,
     & 0,0,0,0,-1,0,1,0,0,0,0,0,0,4,-4,0,0,0,0,0,0,
     & 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,4,0,0,0,
     & -1,0,0,0,0,9,0,0,0,4,64,0,0,0,0,0,0,0,0,0,-576,
     & 0,1,0,-9,0,0,0,0,0,0,0,-4,-64,0,0,0,0,0,0,576,0,
     & 0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,-36,0,0,
     & 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,-1,0,0,0,0,
     & 1,0,0,0,0,25,0,0,0,-100,0,0,0,0,0,0,0,0,0,0,0,
     & 0,1,0,25,0,0,0,0,0,0,0,-100,0,0,0,0,0,0,0,0,0/
c
      character*1 am(10),sphsymbol
c  if necessary to go beyond m orbitals, format for printing lmnv labels
c  will need to be expanded.
      data am /'s','p','d','f','g','h','i','k','l','m'/
      character*3 hilabl(mxhirep),lgrp(8),lolabl(mxlorep),loptgrp,
     & mtype(mxatm)
      data lgrp/'c1 ','cs ','c2 ','ci ','d2 ','c2v','c2h','d2h'/
      character*4 hiptgrp,ptgrp
c     character*5 poly(mxang2,mxang)
c     data ((poly(j,l),j=1,l*(l+1)/2),l=1,mxang) /'     ',
c    &  '    X','    Y','    Z',
c    &  '   X2','   Y2','   Z2','   XY','   XZ','   YZ',
c    &  '   X3','   Y3','   Z3','  X2Y','  X2Z',
c    &  '  Y2X','  Y2Z','  Z2X','  Z2Y','  XYZ',
c    &  '   X4','   Y4','   Z4','  X3Y','  X3Z',
c    &  '  Y3X','  Y3Z','  Z3X','  Z3Y',' X2Y2',
c    &  ' X2Z2',' Y2Z2',' X2YZ',' Y2XZ',' Z2XY'/
      character*8 option
      character*10 loctmp
      character*72 atitle, msg
      character*130 libdir, ptgrplib
c
      logical sphericalaos, reorder
c
      logical  compabb
      external compabb
c
      integer  fstrlen, ist
      external fstrlen
c
c     # bummer error type.
      parameter(faterr=2)
c
      if(mxang.gt.10) then
         write (6,*) 'need to extend the am(:) array'
         call bummer ('exiting ...',0,faterr)
      endif
c
c     # fill angular-momentum-related arrays
c
      call angarr(
     & icomp,  ijk,    iperm,  iprd,
     & lco,    lmnv,   mxang,  mxang1,
     & mxang2, mxang3, mxang4, mxang5)
c
      write (6,"('Program Iargos',/,'Argos and Dalton Input Program'
     & /,'version date 03-oct-01')")
      call headwr(6,'IARGOS','5.8   ')
      write (6,*)
      write (6,*)
      write (6,*) 'Defaults are in angle brackets <like this>.'
      write (6,*)
     & 'Alphabetic responses may be abbreviated to as few letters'
      write (6,*) '  as desired, as long as the choice is still unique.'
      write (6,*) 'Whenever "PROCEED" is offered as an option,'
      write (6,*) '  it means to continue to the next item.'
      write (6,*)
c
      open (unit=13,file='iargosky',status='unknown')
c
      iyn=0
      call getyn ('Would you like to do an interactive input?',iyn)
      if (iyn.eq.0) then
          open (unit=5, file='inpcol',  status='old', iostat=ist)
          if (ist .ne. 0) STOP 'Cannot open inpcol to read !!'
      endif
c
c     # use this unit temporarily to determine how many backspaces
c     # are required in order to write after an eof is found.
c
      call iskpend( 13, 1, ierr )
      if ( ierr .ne. 0 ) then
         call bummer( 'from iskpend(), ierr=', ierr, faterr )
      endif
c
      iyn=1
      call getyn ('Do you wish to use spherical basis functions?',iyn)
      if (iyn.eq.1) then
         sphericalaos=.true.
         write(6,*) 'INFO: Using spherical basis functions (l2,lz)'
      else
         sphericalaos=.false.
         write(6,*) 'INFO: Using cartesian basis functions'
      endif
c
      write(6,*) 'NOTE: use the DEFINE option only in conjunction ',
     & 'with the argos integral program!'
      write (6,*)
c
      iyn=1
      call getyn ('Use bohrs (a.u.) rather than Angstroms?',iyn)
      if (iyn.eq.0) then
         fconvert=1.889726134d0
         write(6,*) 'INFO: input of geometry data in Angstroms'
      else
         fconvert=1.0d0
         write(6,*) 'INFO: input of geometry data in bohrs'
      endif

      write (6,*)
      write (6,*) 'Writing input files for '
      write (6,*) 'DALTON (1)      ARGOS (2)    TURBOCOL (3)'
777   call getint ('enter your choice', ichoice_prg,'1')
      if (ichoice_prg.ne.1 .and. ichoice_prg.ne.2 .and.
     &  ichoice_prg.ne.3 ) goto 777
c
c     # determine the basis library and the point group data files.
c
      call setlib( libdir, ptgrplib )
      lenc = fstrlen( libdir )
      write(6,'(/a)') ' libdir   = ' // libdir(1:lenc)
      lenc = fstrlen( ptgrplib )
       if (ichoice_prg.eq.1) then
         ptgrplib(lenc+1:lenc+8)='.hermit'
       lenc = fstrlen( ptgrplib )
      endif
      write(6,'(a/)') ' ptgrplib = ' // ptgrplib(1:lenc)
c
c     # open and process the point group data file.
c
      open( unit=1, status='old', file=ptgrplib)
c
      write (6,*) 'INFO: for linear molecules, use a 12-fold axis'
      call getchr ('What is the point-group symmetry? ',hiptgrp,ptgrp)
c
      loctmp = 'UH:' // hiptgrp
      call locate (1,loctmp,ierr)
      if (ierr.eq.0) then       ! ptgrp is a hisym point group
         write (6,*) hiptgrp,' is a HIGH-SYMMETRY point group'
         read (1,'(a3)') loptgrp
         write (6,*) 'Its corresponding LOW-SYMMETRY point group is ',
     &    loptgrp
         call strupp (loptgrp)
         read (1,*) nhirep,ngen,nmsg
         read (1,'(20a3)') (hilabl(i),i=1,nhirep)
         read (1,*) (ideg(i),i=1,nhirep)
         read (1,*) ibxyz
         do imsg=1,nmsg
            read (1,'(a)') msg
            write (6,*) msg
         enddo
c        # generate hisym irrep transformation matrix offset array
         ioff=1
         do ihirep=1,nhirep
            ihoff(ihirep)=ioff
            ioff=ioff+ideg(ihirep)*ideg(ihirep)
         enddo
         issqd=ioff-1
c        # read the transformation matrices for the generators
         do igen=1,ngen
            do ihirep=1,nhirep
               call rdtrans (ideg(ihirep),1,gen(ihoff(ihirep),igen))
            enddo
         enddo
c        # generate hisym point group operations
         call genptgrp (
     &    gen,    ideg,   ihoff,   issqd,
     &    multed, mxgen,  mxhirep, mxorder,
     &    ngen,   nhirep, nop,     sqtol,
     &    trans)
c
         if (nop.ne.issqd) then
            write (6,*) 'The point group has the wrong order.'
            write (6,*) 'Contact the author of this software.'
            close (unit=1)
            call bummer ('exiting ...',0,faterr)
         endif
         write (6,*) 'The point group ',hiptgrp,' has ',nop,
     &    ' operations.'
         ihisym=1
c        # generate characters for first block of each irrep
         nhiblk=0
         do ihirep=1,nhirep
            do iop=1,nop
               chi(ihirep,iop)=trans(ihoff(ihirep),iop)
            enddo
            nhiblk=nhiblk+ideg(ihirep)
         enddo
c        # generate block shift operators using great orthogonality
c        # theorem (GOT)
         do iop=1,nop
            x=0
            do ihirep=1,nhirep
               jdeg=ideg(ihirep)
               ifact=nop/jdeg   ! normalization factor in GOT
               do iblk=1,jdeg
                  jblk=mod(iblk,jdeg)
                  x=x+trans(ihoff(ihirep)+(iblk-1)*jdeg+jblk,iop)/ifact
               enddo
            enddo
            blshco(iop)=x
         enddo
c        # generate irepofhi array (hisym block --> hisym irrep)
c        # and irepos array (hisym block --> position within irrep)
         ihiblk=0
         do ihirep=1,nhirep
            inblk=0
            do jhiblk=ihiblk+1,ihiblk+ideg(ihirep)
               irepofhi(jhiblk)=ihirep
               inblk=inblk+1
               irepos(jhiblk)=inblk
            enddo
            ihiblk=ihiblk+ideg(ihirep)
         enddo
c        # prepare cartesian transformation matrices
         do iop=1,nop
            do ixyz=1,3         ! loop over coordinates
               iblk=ibxyz(ixyz)
               ixrep=irepofhi(iblk)
               ipos=irepos(iblk)
               do jxyz=1,3      ! loop over coordinates
                  jblk=ibxyz(jxyz)
                  jxrep=irepofhi(jblk)
c                 # do coordinates ixyz and jxyz belong
c                 # to the same irrep?
                  if (jxrep .eq. ixrep) then ! yes
                     jdeg=ideg(ixrep)
                     jpos=irepos(jblk)
                     ctrans(jxyz,ixyz,iop)=
     &                trans(ihoff(ixrep)+(ipos-1)*jdeg+jpos-1,iop)
                  else          ! Nope. Use 0.
                     ctrans(jxyz,ixyz,iop)=0
                  endif
               enddo
            enddo
         enddo
         loctmp='UL:'//loptgrp
         call locate (1,loctmp,ierr)
         if (ierr.ne.0) then
            write (6,*) 'Losym point group ',loptgrp,' not found!'
            write (6,*) 'Contact the author of this software.'
            close (unit=1)
            call bummer ('exiting ...',0,faterr)
         endif
      else
         call wzero (9,ctrans,1) ! the identity is used as the hisym
         ctrans(1,1,1)=1        ! point group for later use in
         ctrans(2,2,1)=1        ! generating the atomic centers.
         ctrans(3,3,1)=1
         nop=1
         nhirep=1
         blshco(1)=1
         chi(1,1)=1
c        # at this point, the 4th character of hiptgrp should be a blank
         loptgrp=hiptgrp(1:3)
         ihisym=0
         loctmp = 'UL:' // loptgrp
         call locate (1,loctmp,ierr)
         if (ierr.ne.0) then
            write (6,*) 'Unknown point group.'
            close (unit=1)
            ptgrp=' '
            call bummer('unknown point group.',0,faterr)
         endif
         write (6,*) loptgrp,' is a LOW-SYMMETRY point group.'
      endif
      open(unit=38,file='infofl',status='unknown')
      call strlow(loptgrp)
      write(38,'(a3)') loptgrp

c     # At this point, hiptgrp is the hisym point group (if valid) and
c     # loptgrp is the losym point group.
c
c     # read losym point group information
c     # (regardless of whether or not there is also a hisym group)
c
      read (1,*) nirep,ngenl,nmsg
      read (1,'(20a3)') (lolabl(i),i=1,nirep)
      do imsg=1,nmsg
         read (1,'(a)') msg
         if (ihisym.eq.0) write (6,*) msg
      enddo
      do igenl=1,ngenl
         read (1,*) (lgen(j,igenl),j=1,3)
         read (1,*) (lgen(j,igenl),j=4,3+nirep)
      enddo
      close (unit=1)
c
c     # generate losym point group operations
c
      do j=1,nirep+3
         ltrans(j,1)=1
         if (ngenl.ge.1) then
            ltrans(j,2)=lgen(j,1)
            if (ngenl.ge.2) then
               ltrans(j,3)=lgen(j,2)
               ltrans(j,4)=lgen(j,2)*lgen(j,1)
               if (ngenl.ge.3) then
                  ltrans(j,5)=lgen(j,3)
                  ltrans(j,6)=lgen(j,3)*lgen(j,1)
                  ltrans(j,7)=lgen(j,3)*lgen(j,2)
                  ltrans(j,8)=lgen(j,3)*lgen(j,2)*lgen(j,1)
               endif
            endif
         endif
      enddo
c
c     # set all initial defaults (possibly to be overridden later by
c     # the contents of the incoming input deck)
c
      ns     = 0     ! # of losym kinds of atoms
      ncons  = 0     ! # of contraction sets
      ngcs   = 0     ! # of argos so sets
      nhigcs = 0     ! # of hisym so sets
      nshi   = 0     ! # of hisym kinds of atoms (=ns if not in hisym
      natm   = 0     ! # of atoms
      atitle = ' '   ! argos title
c
c     # get the atom information
c
200   continue
c
c     # define all LSOs and HSOs that are possible
c
      do ishi=1,nshi
         call fsamep (
     &    ihatmbas(ishi), ihsamep, ihsend, isamep,
     &    isend,          ishi,    mxatm,  mxatmsym,
     &    mxlorep,        mxorder, nc,     nchi(ishi),
     &    nchi1,          nhsamep, nirep,  nop,
     &    ns,             nsamep,  nshi)
         call defso (
     &    blkshft,          blshco,      chi,      ctrans,
     &    hiso,             icomp,       icsu,     ictu,
     &    ideg,             igcs,        ihatmbas, ihiatm,
     &    ihigcs,           ihiord,      ihisym,   ihsamep,
     &    ihsend(1,1,ishi), iirep,       ipiv,     iprd,
     &    iproj,            isamep,      isend,    ishi,
     &    isocoef,          lmnp1,       ltrans,   mcons,
     &    mxang,            mxang1,      mxang2,   mxang3,
     &    mxao,             mxaov,       mxatm,    mxatmsym,
     &    mxconatm,         mxcons,      mxhirep,  mxlorep,
     &    mxorder,          mxso,        mxsoset,  nc,
     &    nchi(ishi),       nchi1(ishi), nf,       ngcs,
     &    nhigcs,           nhirep,      nhsamep,  nirep,
     &    nop,              nsamep,      nsymtp,   poly1,
     &    poly2,            projao,      soaox,    soaoy,
     &    soao1,            soao1s,      sox,      sqtol,
     &    transao)
      enddo
c
c     # The main interactive routines begin here
c
201   continue
      write (6,*)
      jstat=1
      if (nshi.gt.0) then
         write (6,*)
     &    'The system currently consists of the following atoms:'
         do ishi=1,nshi
            istat=1
            msg='No basis functions'
            if (nf(ishi).le.0) goto 209
            istat=-2
            msg='Insufficient memory for SOs'
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
               do icon=1,nf(ishi)
                  if (igcs(icon,is).le.0) goto 209
               enddo
            enddo
            istat=-1
            msg='Basis set linearly dependent'
            do icon=1,nf(ishi)
               if (ihigcs(icon,ishi).lt.0) goto 209
            enddo
            istat=0
            if (ihisym.ne.0) then
               msg='Basis set incompatible with HISYM'
               do icon=1,nf(ishi)
                  if (ihigcs(icon,ishi).eq.0) goto 209
               enddo
            endif
            istat=1
            msg='Basis set OK'
209         continue
            write (6, "(i3,':  ',a3,'  nuclear chg =',f4.0,i5,
     &       ' atoms    ', a)")
     &       ishi,mtype(ishi),chg(ishi),nchi1(ishi),msg(1:35)
            jstat=min(jstat,istat)
         enddo
      else
         write (6,*) 'There are currently no atoms in the system.'
      endif
      if (jstat.le.-2) then
         write (6,*) 'You must reduce the basis so that the symmetry'
         write (6,*) 'orbitals will fit in memory before writing an'
         write (6,*) 'input deck.'
      elseif (jstat.eq.-1) then
         write (6,*) 'You must make the symmetry orbitals linearly'
         write (6,*) 'independent before writing an input deck.'
      endif
c
c     # present main menu
c
      write (6,*)
      write (6,*) 'Enter one of the following options:'
      if (nshi.lt.mxatm) then
         write (6,*) '  ADD     a set of symmetry-related atoms'
      endif
      if (nshi.gt.0) then
         write (6,*) '  CHANGE  the coordinates, atomic number,',
     &    ' and/or label of a set of atoms'
         write (6,*) '  BASIS:  Edit the basis on a set of atoms'
         write (6,*) '  PSEUDO: Edit the pseudopotential on a set of ',
     &    'atoms'
         write (6,*) '  DELETE  a set of atoms'
         write (6,*) '  VIEW    geometry information'
         write (6,*) '  CONVERT internal coordinates to cartesian ',
     &    'coordinates'
      endif
      if (jstat.ge.0) then
         write (6,*) '  PROCEED'
      endif
      write (6,*)   '  QUIT'
      write (6,*)
220   continue
      if (nshi.eq.0) then
         call getchr ('Which?',option,'ADD')
      elseif (jstat.ge.0) then
c        # changed from PROCEED to ADD 10-oct-01. -rls
         call getchr ('Which?',option,'ADD')
      else
         call getchr ('Which?',option,'BASIS')
      endif
      if (compabb(option,'ADD',1).and.nshi.lt.mxatm) then
         nshi=nshi+1
         chg(nshi)=(-1000)
         mtype(nshi)=' '
         call getatm (atmas(nshi),chg(nshi),ctrans,fconvert,
     &    loatmbas(ns+1),ltrans,mtype(nshi),mxatm,mxatmsym,mxlorep,
     &    nc(ns+1),nchi(nshi),nchi1(nshi),nirep,nop,sqtol,xyz(1,1,nshi),
     &    xyz1)
         ihatmbas(nshi)=ns
         do is=1,nchi(nshi)
            ihiatm(is+ns)=nshi
         enddo
         ns=ns+nchi(nshi)
         nf(nshi)=0
         ishi=nshi
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            call flperm (isend(1,1,is),ltrans,mxatmsym,mxlorep,nc(is),
     &       nirep,xyz(1,loatmbas(is)+1,ishi))
         enddo
         if (ihisym.ne.0) then
            call fhperm (ctrans,ihsend(1,1,ishi),mxatm,nchi1(ishi),nop,
     &       xyz(1,1,ishi))
         endif
         write(13,'(a6)') '&Basis'
         ichg=(chg(ishi)+0.1d0)
         write(13,'(i3)') ichg
c
c        # eingabe der Basissaetze
c
         call edtgcs (
     &    am,       chg,     eta,      hiso,
     &    iconu,    icsu,    ictu,     igcs,
     &    ihatmbas, ihiatm,  ihigcs,   ihiord,
     &    iirep,    ishi,    isocoef,  jgcsl,
     &    libdir,   lmnp1,   mcons,    mtype,
     &    mxang,    mxao,    mxaov,    mxatm,
     &    mxconatm, mxcons,  mxconset, mxprim,
     &    mxso,     mxsoset, nchi,     ncons,
     &    nf,       nfl,     ngcs,     nrcr,
     &    ns,       nshi,    nsymtp,   zet)
         write(13,'(a4)') '&End'
         write(13,'(a3)') '&SO'
c
         if (sphericalaos) then
            call defsph (
     &       blkshft, blshco,  chi,      ctrans,
     &       hiso,    icomp,   icsu,     ictu,
     &       ideg,    igcs,    ihatmbas, ihiatm,
     &       ihigcs,  ihiord,  ihisym,   ihsamep,
     &       ihsend,  iirep,   ipiv,     iprd,
     &       iproj,   isamep,  isend,    ishi,
     &       isocoef, lco,     lmnp1,    loatmbas,
     &       ltrans,  mcons,   mxang,    mxang1,
     &       mxang2,  mxang3,  mxang4,   mxao,
     &       mxaov,   mxatm,   mxatmsym, mxconatm,
     &       mxcons,  mxhirep, mxlorep,  mxorder,
     &       mxso,    mxsoset, nc,       nchi,
     &       nchi1,   nf,      ngcs,     nhigcs,
     &       nhirep,  nhsamep, nirep,    nop,
     &       ns,      nsamep,  nshi,     nsymtp,
     &       poly1,   poly2,   projao,   soaox,
     &       soaoy,   soao1,   soao1s,   sox,
     &       sqtol,   transao, xyz)
         endif
c
         call edtsoc (
     &    am,       blkshft,  blshco,  chi,
     &    ctrans,   hilabl,   hiptgrp, hiso,
     &    icomp,    icsu,     ictu,    ideg,
     &    igcs,     ihatmbas, ihiatm,  ihigcs,
     &    ihiord,   ihisym,   ihsamep, ihsend,
     &    iirep,    iperm,    ipiv,    iprd,
     &    iproj,    irepofhi, isamep,  isend,
     &    ishi,     isize,    islist,  isocoef,
     &    isox,     lco,      lmnp1,   lmnv,
     &    loatmbas, lolabl,   loptgrp, ltrans,
     &    mcons,    mtype,    mxang,   mxang1,
     &    mxang2,   mxang3,   mxang4,  mxang5,
     &    mxao,     mxaov,    mxatm,   mxatmsym,
     &    mxconatm, mxcons,   mxhiblk, mxhirep,
     &    mxlorep,  mxorder,  mxso,    mxsoset,
     &    nc,       nchi,     nchi1,   nf,
     &    ngcs,     nhigcs,   nhirep,  nhsamep,
     &    nirep,    nop,      ns,      nsamep,
     &    nshi,     nsymtp,   poly1,   poly2,
     &    projao,   soaox,    soaoy,   soao1,
     &    soao1s,   sox,      sqtol,   transao,
     &    xyz)
         write(13,'(a4)') '&End'
         goto 200
      elseif (compabb(option,'CHANGE',2).and.nshi.gt.0) then
         write (6,*)
221      call getint ('Which atom do you want to change? '//
     &    '<return to menu>',ishi,'$')
         if (ishi.eq.1000000) goto 201
         if (ishi.le.0.or.ishi.gt.nshi) goto 221
         nchix=nchi(ishi)
         nchi1x=nchi1(ishi)
         call getatm (
     &    atmas(ishi),   chg(ishi), ctrans,      fconvert,
     &    loatb,         ltrans,    mtype(ishi), mxatm,
     &    mxatmsym,      mxlorep,   nct,         nchi(ishi),
     &    nchi1(ishi),   nirep,     nop,         sqtol,
     &    xyz(1,1,ishi), xyz1)
         idnchi=nchi(ishi)-nchix ! idnchi =change in # of losym types of
         if (ishi.lt.nshi) then
c           # move all information which is indexed by losym atom
c           # type (is)
            if (idnchi.gt.0) then ! more losym types. Open space for new
               do is=ns,ihatmbas(ishi+1)+1,-1
                  loatmbas(is+idnchi)=loatmbas(is)
                  ihiatm(is+idnchi)=ihiatm(is)
                  nc(is+idnchi)=nc(is)
                  do icon=1,nf(ihiatm(is))
                     igcs(icon,is+idnchi)=igcs(icon,is)
                  enddo
                  do iopl=1,nirep
                     do iatm=1,nc(is)
                        isend(iatm,iopl,is+idnchi)=isend(iatm,iopl,is)
                     enddo
                  enddo
               enddo
            elseif (idnchi.lt.0) then ! fewer losym types.
               do is=ihatmbas(ishi+1)+1,ns
                  loatmbas(is+idnchi)=loatmbas(is)
                  ihiatm(is+idnchi)=ihiatm(is)
                  nc(is+idnchi)=nc(is)
                  do icon=1,nf(ihiatm(is))
                     igcs(icon,is+idnchi)=igcs(icon,is)
                  enddo
                  do iopl=1,nirep
                     do iatm=1,nc(is)
                        isend(iatm,iopl,is+idnchi)=isend(iatm,iopl,is)
                     enddo
                  enddo
               enddo
            endif
         endif
         do jshi=ishi+1,nshi
            ihatmbas(jshi)=ihatmbas(jshi)+idnchi
         enddo
         icperm=0 ! flag for change of atom permutation with new coordin
         if (nchi(ishi).ne.nchix) icperm=1
         if (nchi1(ishi).ne.nchi1x) icperm=1
c        # load new information on atom that was changed
         do is=1,nchi(ishi)
            loatmbas(is+ihatmbas(ishi))=loatb(is)
            if (nc(is+ihatmbas(ishi)).ne.nct(is)) icperm=1
            nc(is+ihatmbas(ishi))=nct(is)
            ihiatm(is+ihatmbas(ishi))=ishi
         enddo
         ns=ns+idnchi
c        # check change of atom permutations in hisym point group,
c        # if applicable
         if (ihisym.ne.0) then
            do iop=1,nop
               do iatm=1,nchi1(ishi)
                  itsend(iatm,iop)=ihsend(iatm,iop,ishi)
               enddo
            enddo
            call fhperm (ctrans,ihsend(1,1,ishi),mxatm,nchi1(ishi),nop,
     &       xyz(1,1,ishi))
            do iop=1,nop
               do iatm=1,nchi1(ishi)
                  if (itsend(iatm,iop) .ne. ihsend(iatm,iop,ishi))
     &             icperm=1
               enddo
            enddo
         endif
c        # check change of atom permutations in losym point group
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            do iopl=1,nirep
               do iatm=1,nc(is)
                  itsend(iatm,iopl)=isend(iatm,iopl,is)
               enddo
            enddo
            call flperm (isend(1,1,is),ltrans,mxatmsym,mxlorep,nc(is),
     &       nirep,xyz(1,loatmbas(is)+1,ishi))
            do iopl=1,nirep
               do iatm=1,nc(is)
                  if (itsend(iatm,iopl).ne.isend(iatm,iopl,is)) icperm=1
               enddo
            enddo
         enddo
         if (icperm.eq.0) goto 201
c        # The relationship of these atoms to the point group has
c        # changed. All LSO and HSO sets on these atoms become
c        # undefined.
         do icon=1,nf(ishi)     ! loop over contraction sets
            jhigcs=ihigcs(icon,ishi)
            ihigcs(icon,ishi)=0
            call rmhso (
     &       hiso,  ihigcs, ihiord,   jhigcs,
     &       mxaov, mxatm,  mxconatm, mxsoset,
     &       nf,    nhigcs, nshi,     nsymtp)
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
               jgcs=igcs(icon,is)
               igcs(icon,is)=0
               call rmlso (
     &          icsu,  ictu,     igcs, ihiatm,
     &          iirep, isocoef,  jgcs, mxao,
     &          mxatm, mxconatm, mxso, mxsoset,
     &          nf,    ngcs,     ns)
            enddo
         enddo
         write (6,*)
         write (6,*) 'The new coordinates of these atoms changes their'
         write (6,*) 'positions relative to the symmetry elements of'
         write (6,*) 'the molecule in such a way that the symmetry'
         write (6,*) 'orbitals must be redefined.'
         call edtsoc (
     &    am,       blkshft,  blshco,  chi,
     &    ctrans,   hilabl,   hiptgrp, hiso,
     &    icomp,    icsu,     ictu,    ideg,
     &    igcs,     ihatmbas, ihiatm,  ihigcs,
     &    ihiord,   ihisym,   ihsamep, ihsend,
     &    iirep,    iperm,    ipiv,    iprd,
     &    iproj,    irepofhi, isamep,  isend,
     &    ishi,     isize,    islist,  isocoef,
     &    isox,     lco,      lmnp1,   lmnv,
     &    loatmbas, lolabl,   loptgrp, ltrans,
     &    mcons,    mtype,    mxang,   mxang1,
     &    mxang2,   mxang3,   mxang4,  mxang5,
     &    mxao,     mxaov,    mxatm,   mxatmsym,
     &    mxconatm, mxcons,   mxhiblk, mxhirep,
     &    mxlorep,  mxorder,  mxso,    mxsoset,
     &    nc,       nchi,     nchi1,   nf,
     &    ngcs,     nhigcs,   nhirep,  nhsamep,
     &    nirep,    nop,      ns,      nsamep,
     &    nshi,     nsymtp,   poly1,   poly2,
     &    projao,   soaox,    soaoy,   soao1,
     &    soao1s,   sox,      sqtol,   transao,
     &    xyz)
         goto 200
      elseif (compabb(option,'BASIS',1).and.nshi.gt.0) then
         write (6,*)
222      call getint ('For which atom do you want to edit the basis? '//
     &    '<return to menu>',ishi,'$')
         if (ishi.eq.1000000) goto 201
         if (ishi.le.0.or.ishi.gt.nshi) goto 222
         write(13,'(a6)') '&Basis'
         ichg=(chg(ishi)+0.1d0)
         write(13,'(i3)') ichg
         call edtgcs (
     &    am,       chg,     eta,      hiso,
     &    iconu,    icsu,    ictu,     igcs,
     &    ihatmbas, ihiatm,  ihigcs,   ihiord,
     &    iirep,    ishi,    isocoef,  jgcsl,
     &    libdir,   lmnp1,   mcons,    mtype,
     &    mxang,    mxao,    mxaov,    mxatm,
     &    mxconatm, mxcons,  mxconset, mxprim,
     &    mxso,     mxsoset, nchi,     ncons,
     &    nf,       nfl,     ngcs,     nrcr,
     &    ns,       nshi,    nsymtp,   zet)
         write(13,'(a4)') '&End'
         write(13,'(a3)') '&SO'
         call edtsoc (
     &    am,       blkshft,  blshco,  chi,
     &    ctrans,   hilabl,   hiptgrp, hiso,
     &    icomp,    icsu,     ictu,    ideg,
     &    igcs,     ihatmbas, ihiatm,  ihigcs,
     &    ihiord,   ihisym,   ihsamep, ihsend,
     &    iirep,    iperm,    ipiv,    iprd,
     &    iproj,    irepofhi, isamep,  isend,
     &    ishi,     isize,    islist,  isocoef,
     &    isox,     lco,      lmnp1,   lmnv,
     &    loatmbas, lolabl,   loptgrp, ltrans,
     &    mcons,    mtype,    mxang,   mxang1,
     &    mxang2,   mxang3,   mxang4,  mxang5,
     &    mxao,     mxaov,    mxatm,   mxatmsym,
     &    mxconatm, mxcons,   mxhiblk, mxhirep,
     &    mxlorep,  mxorder,  mxso,    mxsoset,
     &    nc,       nchi,     nchi1,   nf,
     &    ngcs,     nhigcs,   nhirep,  nhsamep,
     &    nirep,    nop,      ns,      nsamep,
     &    nshi,     nsymtp,   poly1,   poly2,
     &    projao,   soaox,    soaoy,   soao1,
     &    soao1s,   sox,      sqtol,   transao,
     &    xyz)
         write(13,'(a4)') '&End'
         goto 200
      elseif (compabb(option,'PSEUDO',2).and.nshi.gt.0) then
         call edtecp (
     &    am,     ccr,   chg,    cls,
     &    ishi,   izcr,  lcru,   libdir,
     &    llsu,   mcrs,  mtype,  mxang,
     &    mxang3, mxatm, mxbfcr, mxcrs,
     &    nbfcr,  nbfls, ncr,    ncrs,
     &    nls,    nshi,  zcr,    zls)
         goto 201
      elseif (compabb(option,'VIEW',1).and.nshi.gt.0) then
         write (6,*)
         do ishi=1,nshi
            write (6,*) mtype(ishi),' atom(s):'
            do iatm=1,nchi1(ishi)
               write (6,'(i5,3f16.10)') iatm,(xyz(j,iatm,ishi),j=1,3)
            enddo
         enddo
         write (6,*) 'Internuclear distances:'
         do ishi=1,nshi
            do iatm=1,nchi1(ishi)
               do jshi=1,ishi
                  jatmx=nchi1(jshi)
                  if (ishi.eq.jshi) jatmx=iatm-1
                  do jatm=1,jatmx
                     dist = sqrt( sqdif(xyz(1,iatm,ishi),
     &                xyz(1,jatm,jshi),3) )
                     write (6,"(3x,a3,i3,'  to  ',a3,i3,' : ',f16.10)")
     &                mtype(jshi),jatm,mtype(ishi),iatm,dist
                  enddo
               enddo
            enddo
         enddo
         goto 201
      elseif (compabb(option,'DELETE',1).and.nshi.gt.0) then
         write (6,*)
223      call getint ('Which atom do you want to delete? '//
     &    '<return to menu>',ishi,'$')
         if (ishi.eq.1000000) goto 201
         if (ishi.le.0 .or. ishi.gt.nshi) goto 223
c        # Remove contraction sets, HSOs, and LSOs
         do icon=1,nf(ishi)
            jcon=mcons(icon,ishi)
            mcons(icon,ishi)=0
            call rmcon (
     &       eta,      iconu,  jcon,     lmnp1,
     &       mcons,    mxatm,  mxconatm, mxcons,
     &       mxconset, mxprim, ncons,    nf,
     &       nrcr,     nshi,   zet)
            jhigcs=ihigcs(icon,ishi)
            ihigcs(icon,ishi)=0
            call rmhso (
     &       hiso,  ihigcs, ihiord,   jhigcs,
     &       mxaov, mxatm,  mxconatm, mxsoset,
     &       nf,    nhigcs, nshi,     nsymtp)
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
               jgcs=igcs(icon,is)
               igcs(icon,is)=0
               call rmlso (
     &          icsu,  ictu,     igcs, ihiatm,
     &          iirep, isocoef,  jgcs, mxao,
     &          mxatm, mxconatm, mxso, mxsoset,
     &          nf,    ngcs,     ns)
            enddo
         enddo
c        # remove core potential, if any
         icrs=mcrs(ishi)
         mcrs(ishi)=0
         call rmecp (
     &    ccr,   cls,   icrs,   izcr,
     &    lcru,  llsu,  mxang,  mxang3,
     &    mcrs,  mxatm, mxbfcr, mxcrs,
     &    nbfcr, nbfls, ncr,    ncrs,
     &    nls,   nshi,  zcr,    zls)
         nchix=nchi(ishi)
c        # close spaces in information which is indexed by hisym atom
c        # type (ishi)
         nshi=nshi-1
         do jshi=ishi,nshi
            ihatmbas(jshi)=ihatmbas(jshi+1)-nchix
            nf(jshi)=nf(jshi+1)
            nchi(jshi)=nchi(jshi+1)
            nchi1(jshi)=nchi1(jshi+1)
            chg(jshi)=chg(jshi+1)
            mtype(jshi)=mtype(jshi+1)
            mcrs(jshi)=mcrs(jshi+1)
            do icon=1,nf(jshi)
               ihigcs(icon,jshi)=ihigcs(icon,jshi+1)
               mcons(icon,jshi)=mcons(icon,jshi+1)
            enddo
            do iop=1,nop
               do iatm=1,nchi1(jshi)
                  ihsend(iatm,iop,jshi)=ihsend(iatm,iop,jshi+1)
               enddo
            enddo
            do iatm=1,nchi1(jshi)
               do j=1,3
                  xyz(j,iatm,jshi)=xyz(j,iatm,jshi+1)
               enddo
            enddo
         enddo
c        # close spaces in information which is indexed by losym atom
c        # type (is)
         ns=ns-nchix
         do is=ihatmbas(ishi)+1,ns
            loatmbas(is)=loatmbas(is+nchix)
            ihiatm(is)=ihiatm(is+nchix)-1
            nc(is)=nc(is+nchix)
            do icon=1,nf(ihiatm(is))
               igcs(icon,is)=igcs(icon,is+nchix)
            enddo
            do iopl=1,nirep
               do iatm=1,nc(is)
                  isend(iatm,iopl,is)=isend(iatm,iopl,is+nchix)
               enddo
            enddo
         enddo
         goto 201
      elseif (compabb(option,'CONVERT',2)) then
c        # count up # of atoms, and make a list of hisym type and
c        # position within hisym type of each one (iatmp array)
         natm=0
         do ishi=1,nshi
            do iatm=1,nchi1(ishi)
               natm=natm+1
               iatmp(1,natm)=ishi
               iatmp(2,natm)=iatm
            enddo
         enddo
         if (natm.eq.1) then
            write (6,*)
            call getreal ('Enter distance from '//mtype(1)//' atom:',
     &       r,' ')
            call getreal ('Enter theta from '//mtype(1)//
     &       ' atom (relative to positive z direction):',
     &       th,' ')
            call getreal ('Enter phi (azimuthal angle) from ' //
     &       mtype(1) // ' atom (relative to xz plane):',
     &       ph,' ')
            th=th*deg2rad
            ph=ph*deg2rad
            write (6,*)
            write (6,"(' The new location is:'/'  x =',f16.10,
     &       '    y =',f16.10,'   z =',f16.10)")
     &       xyz(1,1,1)+r*sin(th)*cos(ph),
     &       xyz(2,1,1)+r*sin(th)*sin(ph),
     &       xyz(3,1,1)+r*cos(th)
         else
            write (6,*) 'The atoms are:'
            do iatm=1,natm
               ishi=iatmp(1,iatm)
               iatm1=iatmp(2,iatm)
               write (6,"(i4,':',2x,a3,3f16.10)") iatm,mtype(ishi),
     &          (xyz(j,iatm1,ishi),j=1,3)
            enddo
            write (6,*)
            if (natm.eq.2) then
               write (6,*) 'Which atom do you want calculate the new'
               call getint ('location relative to? (1/2)',ic,' ')
               if (ic.ne.1.and.ic.ne.2) goto 201
               ib=3-ic          ! the other atom
            else
               write (6,*)
     &          'You will specify the C-X distance, the B-C-X'
               write (6,*)
     &          'angle, and the A-B-C-X dihedral angle, where'
               write (6,*) 'A, B, and C are existing atoms and X is the'
               write (6,*) 'location you will compute.'
               write (6,*)
               write (6,*) 'Enter A, B, and C from the table above:'
               read (5,*) ia,ib,ic
               write(13,*) ia,ib,ic
               if (ia.le.0.or.ib.le.0.or.ic.le.0.or.
     &          ia.gt.natm.or.ib.gt.natm.or.ic.gt.natm) goto 201
               if (ia.eq.ib.or.ia.eq.ic.or.ib.eq.ic) goto 201
            endif
            p2=0
            q2=0
            pq=0
            do j=1,3
               if (natm.eq.2) then
                  p(j)=0        ! make it think there's 3 collinear atom
               else
                  p(j)=xyz(j,iatmp(2,ia),iatmp(1,ia))-
     &             xyz(j,iatmp(2,ib),iatmp(1,ib))
               endif
               q(j)=xyz(j,iatmp(2,ic),iatmp(1,ic))-
     &          xyz(j,iatmp(2,ib),iatmp(1,ib))
               p2=p2+p(j)**2
               q2=q2+q(j)**2
               pq=pq+p(j)*q(j)
            enddo
            q1=sqrt(q2)
            pq1=sqrt(p2*q2-pq*pq)
            if (natm.eq.2) then
               write (6,*) 'Enter the distance from this atom to the'
               call getreal ('calculated location:',r,' ')
               call getreal ('Enter the triatomic angle:',th,' ')
            else
               call getreal ('Enter the C-X distance:',r,' ')
               call getreal ('Enter the B-C-X angle:',th,' ')
            endif
            if (pq1*pq1.lt.sqtol) then ! collinear
               p(1)=0
               p(2)=0
               p(3)=1
               p2=1
               pq=q(3)
               pq1=sqrt(p2*q2-pq*pq)
               if (pq1*pq1.lt.sqtol) then ! vertical
                  p(1)=1
                  p(3)=0
                  pq=q(1)
                  pq1=sqrt(p2*q2-pq*pq)
                  write (6,*)
                  if (natm.eq.2) then
                     write (6,*) 'The two atoms are vertical.'
                  else
                     write (6,*) 'Atoms A, B, and C are vertical.'
                  endif
                  write (6,*)
                  write (6,*) 'Enter phi for the plane containing those'
                  write (6,*)
     &             'atoms and the calculated location (measured'
                  call getreal ('counterclockwise from the xz plane):',
     &             ph,' ')
               else
                  if (natm.eq.2) then
                     write (6,*)
     &                'Enter the angle between the plane of the'
                     write (6,*) 'three atoms and a vertical plane'
                     write (6,*) 'containing the bond between the two'
                     call getreal ('existing atoms:',ph,' ')
                  else
                     write (6,*)
                     write (6,*) 'Atoms A, B and C are collinear.'
                     write (6,*)
                     write (6,*)
     &                'Enter the angle between the B-C-X plane'
                     write (6,*)
     &                'and a vertical plane containing the B-C'
                     call getreal ('axis:',ph,' ')
                  endif
               endif
            else
               write (6,*) 'Enter the A-B-C-X dihedral angle (which is'
               write (6,*)
     &          'negative if the C-X bond appears clockwise of'
               write (6,*) 'the A-B bond when viewed along the B-C axis'
               call getreal ('with atom C closer to you):',ph,' ')
            endif
            th=th*deg2rad
            ph=ph*deg2rad
            r1=r*sin(th)*cos(ph)
            r2=r*sin(th)*sin(ph)
            r3=r*cos(th)
            write (6,"(' The new location is:'/'  x =',f16.10,
     &       '    y =',f16.10,'   z =',f16.10)")
     &       -q(1)*r3/q1+(q2*p(1)-pq*q(1))*r1/pq1/q1+
     &       (q(2)*p(3)-p(2)*q(3))*r2/pq1+
     &       xyz(1,iatmp(2,ic),iatmp(1,ic)),
     &       -q(2)*r3/q1+(q2*p(2)-pq*q(2))*r1/pq1/q1+
     &       (q(3)*p(1)-p(3)*q(1))*r2/pq1+
     &       xyz(2,iatmp(2,ic),iatmp(1,ic)),
     &       -q(3)*r3/q1+(q2*p(3)-pq*q(3))*r1/pq1/q1+
     &       (q(1)*p(2)-p(1)*q(2))*r2/pq1+
     &       xyz(3,iatmp(2,ic),iatmp(1,ic))
         endif
         goto 201
      elseif (compabb(option,'QUIT',1)) then
         write (6,*) 'Bye!'
         call bummer('normal termination',0,3)
         stop
      elseif (.not.compabb(option,'PROCEED',2).or.jstat.lt.0) then
         goto 220
      endif
c
      ihisym=abs(ihisym)        ! ihisym.lt.0 if hisym geometry
      if (jstat.eq.0) ihisym=-ihisym !           but not hisym basis
c
c     # count up total # of orbitals of each symmetry type
c
      if (ihisym.ne.0) then
         call izero_wr (nhirep,isize,1)
         call izero_wr (nhiblk,ilofhi,1)
      endif
      call izero_wr (isizl,nirep,1)
      do ishi=1,nshi            ! loop over hisym atom types
         do icon=1,nf(ishi)     ! loop over contraction sets on that ato
            jcon=mcons(icon,ishi)
            ncrx=nrcr(jcon)
            iam=lmnp1(jcon)
            jhigcs=ihigcs(icon,ishi)
c           # count orbitals in hisym irreps, if appropriate
            if (jhigcs.gt.0) then
               do ihso=1,nsymtp(jhigcs)
                  ihiblk=ihiord(ihso,jhigcs)
                  ihirep=irepofhi(ihiblk)
                  isize(ihirep)=isize(ihirep)+ncrx
               enddo
               ihsoff=0
            endif
c           # count orbitals in losym irreps
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
               jgcs=igcs(icon,is)
               ictue=iam*(iam+1)*nc(is)/2
               if (jgcs.gt.0) then
                  if (ictu(jgcs).ne.ictue) then
                     write (6,*)
     &                'There is a mismatch between the size of a'
                     write (6,*)
     &                'set of SO coefficients and the angular'
                     write (6,*)
     &                'momentum and number of atoms it comes from.'
                     write (6,*) 'Contact the author of this software!'
                     call bummer ('exiting ...',0,faterr)
                  endif
                  do ilso=1,icsu(jgcs) ! loop over LSOs
                     iloblk=iirep(ilso,jgcs)
                     isizl(iloblk)=isizl(iloblk)+ncrx
c                    # derive losym <--> hisym block correlation
                     if (jhigcs.gt.0) then
                        do ihso=1,nsymtp(jhigcs) ! loop over HSOs
                           if (hiso(ilso+ihsoff,ihso,jhigcs).ne.0) then
                              ihiblk=ihiord(ihso,jhigcs)
                              if (ilofhi(ihiblk).eq.0) then
                                 ilofhi(ihiblk)=iloblk
                              elseif (ilofhi(ihiblk).ne.iloblk) then
                                 write (6,*)
     &                            'A hisym block is ascribed to ',
     &                            'multiple losym blocks!!!'
                                 write (6,*) 'Contact the author ',
     &                            'of this software.'
                                 call bummer ('exiting ...',0,faterr)
                              endif
                           endif
                        enddo
                     endif
                  enddo
               endif
               ihsoff=ihsoff+icsu(jgcs)
            enddo
         enddo
      enddo
      if (ihisym.ne.0) then
         do ihirep=1,nhirep
            isize(ihirep)=isize(ihirep)/ideg(ihirep)
         enddo
      endif
c
c     # count symmetry types
c
      write (6,*)
      write (6,*)
      write (6,*)
     & 'The orbitals are organized by symmetry in the following way:'
      write (6,*)
      if (ihisym.gt.0) then
         write (6,"('     Irreps           number of',/,
     &    ' Hisym    Losym       orbitals')")
         do ihiblk=1,nhiblk
            ihirep=irepofhi(ihiblk)
            if (isize(ihirep).gt.0) write (6,'(2x,a3,6x,a3,i13)')
     &       hilabl(ihirep),lolabl(ilofhi(ihiblk)),isize(ihirep)
         enddo
      else
         write (6,"(' Argos ordering of irreps:',/)")
         write (6,"(' Irrep     # of orbitals')")
         do irep=1,nirep
            write (6,'(2x,a3,i15)') lolabl(irep),isizl(irep)
         enddo
      endif
c
      write (6,*)
      iyn=1
520   call getyn ('Is EVERYTHING now correct?',iyn)
      if (iyn.eq.0) goto 201
c
cmbr choose whether geom and daltaoin are reordered
      reorder=.true.
      iyn=1
      call getyn ('Reorder atoms for geometry optimization?',iyn)
      if (iyn.eq.0) then
        reorder=.false.
      endif
cmbr
      write(38,'(8i5)') (isizl(irep),irep=1,nirep)
      write(38,'(i3)') nirep
ctm
      do irep=1,nirep
         call strlow(lolabl(irep))
      enddo
      write(38,'(8(2x,a3))') (lolabl(irep),irep=1,nirep)
c
c     # calculate atom permutation generators for ARGOS
c
      ngenp=0
      igenq(1)=2
      igenq(2)=3
      igenq(3)=5
      do jgen=1,ngenl
         igen=igenq(jgen)
         do is=1,ns
            do iatm=1,nc(is)
               if (isend(iatm,igen,is).ne.iatm) goto 550
            enddo
         enddo
         goto 559
550      continue               ! this is not the identity
         do kgen=1,ngenp
            kgen1=igenp(kgen)
            do is=1,ns
               do iatm=1,nc(is)
                  if (isend(iatm,igen,is) .ne. isend(iatm,kgen1,is))
     &             goto 551
               enddo
            enddo
            goto 559
551         continue            ! this is not generator kgen
         enddo
         if (ngenp.ge.2) then
            kgen1=igenp(1)
            kgen2=igenp(2)
            do is=1,ns
               do iatm=1,nc(is)
                  if (isend(iatm,igen,is).ne.
     &             isend(isend(iatm,kgen2,is),kgen1,is)) goto 552
               enddo
            enddo
            goto 559
         endif
552      continue     ! this is not a product of previous generators
         ngenp=ngenp+1
         igenp(ngenp)=igenq(jgen)
559      continue
      enddo
      if (ncrs.gt.0) then
         do ishi=1,nshi
            if (mcrs(ishi).le.0) then ! Is there no pseudopotential?
               do icrs=1,ncrs   ! look for a dummy pseudopotential
                  if (lcru(icrs).eq.0.and.llsu(icrs).eq.0) goto 560
               enddo
               ncrs=ncrs+1      ! no dummy pseudopotential.  Add one.
               lcru(ncrs)=0
               llsu(ncrs)=0
               nbfcr(0,ncrs)=0
               izcr(ncrs)=0
               icrs=ncrs
560            continue
               mcrs(ishi)=icrs
            endif
         enddo
      else
         do ishi=1,nshi
            mcrs(ishi)=0
         enddo
      endif
c
      if (ichoice_prg.eq.2) then
c
c        # argos
         write (6,*)
         write(13,'(a6)') '&Title'
         call getitle ('Enter the title:',atitle)
         write(13,'(a4)') '&End'
         open(unit=9,file='argosin',status='unknown')
         write (9,'(a)') atitle
         write (9,'(15i3)') ngenp,ns,ngcs,ncons,ngcs,20,9,0,0,0,
     &    ncrs,0,0,0,0
         write (9,'(i3,8(i3,a3))') nirep,(1,lolabl(j),j=1,nirep)
         if (ngenl.le.1) then
            write (9,'(i3)') 0
         elseif (ngenl.eq.2) then
            write (9,'(i3)') 1
            write (9,'(3i3)') 2,3,4
         else
            write (9,'(i3)') 7
            write (9,'(3i3)') 2,3,4,2,5,6,2,7,8,3,5,7,3,6,8,4,5,8,4,6,7
         endif
c        # irreps der SO Kombinationen (symmetry-orbital irreps)
         do jgcs=1,ngcs
            write (9,'(15i5)') icsu(jgcs),(iirep(j,jgcs),j=1,icsu(jgcs))
         enddo
c        # SO-Linearkombinationen (symmetry orbitals)
         do jgcs=1,ngcs
            write (9,'(3i5)') icsu(jgcs),ictu(jgcs),jgcs
            do iso=1,icsu(jgcs)
               write (9,'(15i5)') (isocoef(k,iso,jgcs),k=1,ictu(jgcs))
            enddo
         enddo
c        # Kontrakionskoeffizienten (contraction coefficients)
         do icon=1,ncons
            write (9,'(3i5)') iconu(icon),lmnp1(icon),nrcr(icon)
            do j=1,iconu(icon)
               write (9,'(f15.7,(t21,4f12.7))')
     &          zet(j,icon),(eta(k,j,icon),k=1,nrcr(icon))
            enddo
         enddo
c        # Core potentiale und aehnliches Zeug?
         do icrs=1,ncrs
            write (9,'(2i5)') lcru(icrs),llsu(icrs)
            do l=0,lcru(icrs)
               write (9,'(i5)') nbfcr(l,icrs)
               do k=1,nbfcr(l,icrs)
                  write (9,'(i4,2f15.7)')
     &             ncr(k,l,icrs),zcr(k,l,icrs),ccr(k,l,icrs)
               enddo
            enddo
            do l=1,llsu(icrs)
               write (9,'(i5)') nbfls(l,icrs)
               do k=1,nbfls(l,icrs)
                  write (9,'(i4,2f16.8)')
     &             nls(k,l,icrs),zls(k,l,icrs),cls(k,l,icrs)
               enddo
            enddo
         enddo
c        # Ladungskorrektur (charge correction)
         if (ncrs.gt.0) then
            do ishi=1,nshi
               chg(ishi)=chg(ishi)-izcr(mcrs(ishi))
            enddo
         endif
         open(unit=48,file='geom',status='unknown')
         do is=1,ns
            ishi=ihiatm(is)
c           # Symbol ..
            write (9,'(a3,2i3,f3.0)') mtype(ishi),nf(ishi),nc(is),
     &       chg(ishi)
            do iatm=loatmbas(is)+1,loatmbas(is)+nc(is)
c              # Koordinate (coordinates)
               write (9,'(3f14.8)') (xyz(j,iatm,ishi),j=1,3)
               write(48,'(1x,a2,2x,f5.1,4f14.8)') mtype(ishi),chg(ishi),
     &          (xyz(j,iatm,ishi),j=1,3),atmas(ishi)
            enddo
c           # Generator
            if (nc(is).gt.1) then
               do jgenp=1,ngenp
                  write (9,'(26i3)')
     &             (isend(k,igenp(jgenp),is),k=1,nc(is))
               enddo
            endif
c           # Zuordnung CGTO, SO
            do iam=1,mxang
               do icon=1,nf(ishi)
                  jcon=mcons(icon,ishi)
c                 # igcs(Kontraktion auf einem Satz
c                 # symmetrieaequivalenter Atom, Satz)
                  if (lmnp1(jcon).eq.iam) write (9,'(2i3)') jcon,
     &             igcs(icon,is)
               enddo
            enddo
            if (ncrs.gt.0) write (9,'(i3)') mcrs(ishi)
         enddo
         close (unit=48)
         close (unit=9)
         write (6,*)
         write (6,*) 'the files geom, infofl, and argosin',
     &    ' have been written'
c
      else
c
c        # dalton or turbocol
         write(6,*) 'Writing geometry information on geom ... '
         open(unit=48,file='geom',status='unknown')
         open(unit=19,file='daltaoin',status='unknown')
         write(19,'(a)')'INTGRL'
         write(19,'(a)') atitle
         write(19,'(a)')' '
c        # general stuff
         i=0
         do 381 is=1,ns
            ishi=ihiatm(is)
           if (reorder) then
            do ii=1,i
               if (mtype(ishi).eq.mtype(ihiatm(ntp(ii,1)))) goto 381
            enddo
           endif
            i=i+1
            igrp(i)=1
            ntp(i,1)=is
           if (reorder) then
            do js=is+1,ns
               jshi=ihiatm(js)
               if (mtype(ishi).eq.mtype(jshi)) then
                  igrp(i)=igrp(i)+1
                  ntp(i,igrp(i))=js
               endif
            enddo
           endif
381      enddo
         ielm=i
         do i=1,ielm
            lang(i)=0
            do iam=1,mxang
               do icon=1,nf(ihiatm(ntp(i,1)))
                  jcon=mcons(icon,ihiatm(ntp(i,1)))
                  if(lmnp1(jcon).eq.iam) then
                     iang(i,icon)=jcon
                     lang(i)=max(lang(i),iam)
                  endif
               enddo
            enddo
ctm temporary solution to fix the error encountered
ctm if more than one basis set from the library is used per atom
ctm needs reordering of the contractions in ascending l-quantum number
            itop=1
            do iam=1,lang(i)
               icont(i,iam)=0
               do jj=1,nf(ihiatm(ntp(i,1)))
                  if (lmnp1(iang(i,jj)).eq.iam) then
                     icont(i,iam)=icont(i,iam)+1
                     itemp=iang(i,itop)
                     iang(i,itop)=iang(i,jj)
                     iang(i,jj)=itemp
                     itop=itop+1
                  endif
               enddo
            enddo
         enddo
ctm
         if (ichoice_prg.eq.1) then
c
c           # dalton
            open(unit=41,file='infofl.hermit',status='unknown')
            write(41,'(a3)') loptgrp
            do i=1,8
               if (loptgrp.eq.lgrp(i)) then
                  igr=i
                  go to 382
               endif
            enddo
382         continue
            if (sphericalaos) then
               sphsymbol='s'
            else
               sphsymbol='c'
            endif
            write(6,"('Dalton ordering of irreps:',/)")
            write(6,"( 'Irrep     # of orbitals')")
            go to (491,492,493,494,495,496,497,498),igr
c           # C1
491         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,0,
     &       ' ',' ',' ',thr15
            write(41,'(8i5)') (isizl(irep),irep=1,nirep)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') (lolabl(irep),irep=1,nirep)
            go to 499
c           # C2
492         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,1,
     &       ' Z ',' ',' ',thr15
            write(41,'(8i5)') (isizl(irep),irep=1,nirep)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') (lolabl(irep),irep=1,nirep)
            go to 499
c           # Cs
493         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,1,
     &       'XY ',' ',' ',thr15
            write(41,'(8i5)') (isizl(irep),irep=1,nirep)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') (lolabl(irep),irep=1,nirep)
            go to 499
c           # Ci
494         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,1,
     &       'XYZ',' ',' ',thr15
            write(41,'(8i5)') (isizl(irep),irep=1,nirep)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') (lolabl(irep),irep=1,nirep)
            go to 499
c           # C2v
495         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,2,
     &       'XY ','XZ ',' ',thr15
            write(41,'(8i5)') isizl(1),isizl(3),isizl(2),isizl(4)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') lolabl(1),lolabl(3),
     &       lolabl(2),lolabl(4)
            write(6,'(1x,a3,i15)')lolabl(1),isizl(1)
            write(6,'(1x,a3,i15)')lolabl(3),isizl(3)
            write(6,'(1x,a3,i15)')lolabl(2),isizl(2)
            write(6,'(1x,a3,i15)')lolabl(4),isizl(4)
            go to 499
c           # D2
496         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,2,
     &       'X  ',' Y ',' ',thr15
            write(41,'(8i5)') isizl(1),isizl(3),isizl(4),isizl(2)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') lolabl(1),lolabl(3),
     &       lolabl(4),lolabl(2)
            write(6,'(1x,a3,i15)')lolabl(1),isizl(1)
            write(6,'(1x,a3,i15)')lolabl(3),isizl(3)
            write(6,'(1x,a3,i15)')lolabl(4),isizl(4)
            write(6,'(1x,a3,i15)')lolabl(2),isizl(2)
            go to 499
c           # C2h
497         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,2,
     &       'XY ',' Z ',' ',thr15
            write(41,'(8i5)') isizl(1),isizl(4),isizl(3),isizl(2)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') lolabl(1),lolabl(4),
     &       lolabl(3),lolabl(2)
            write(6,'(1x,a3,i15)')lolabl(1),isizl(1)
            write(6,'(1x,a3,i15)')lolabl(4),isizl(4)
            write(6,'(1x,a3,i15)')lolabl(3),isizl(3)
            write(6,'(1x,a3,i15)')lolabl(2),isizl(2)
            go to 499
c           # D2h
498         write(19,'(a1,i4,i5,3a3,d10.2)')sphsymbol,ielm,3,
     &       ' X ',' Y ',' Z ',thr15
            write(41,'(8i5)') isizl(1),isizl(8),isizl(7),isizl(2),
     &       isizl(6),isizl(3),isizl(4),isizl(5)
            write(41,'(i3)') nirep
            write(41,'(8(2x,a3))') lolabl(1),lolabl(8),lolabl(7),
     &       lolabl(2),lolabl(6),lolabl(3),lolabl(4),lolabl(5)
            write(6,'(1x,a3,i15)')lolabl(1),isizl(1)
            write(6,'(1x,a3,i15)')lolabl(8),isizl(8)
            write(6,'(1x,a3,i15)')lolabl(7),isizl(7)
            write(6,'(1x,a3,i15)')lolabl(2),isizl(2)
            write(6,'(1x,a3,i15)')lolabl(6),isizl(6)
            write(6,'(1x,a3,i15)')lolabl(3),isizl(3)
            write(6,'(1x,a3,i15)')lolabl(4),isizl(4)
            write(6,'(1x,a3,i15)')lolabl(5),isizl(5)
499         continue
c           # maxpri = maximum number of primitives per l quantum number
c           #          and element
c           # loop over elements
            maxpri=0
            do i=1,ielm
               ishi=ihiatm(ntp(i,1))
               write(19,'(6x,f4.1,14i5)')chg(ishi),igrp(i),
     &          lang(i),(icont(i,iam),iam=1,lang(i))
               call izero_wr(mxcons,pcount,1)
c              # loop over individiual unique atoms belonging to this
c              # element
               do j=1,igrp(i)
                  write(19,'(a2,i2,3f20.15,7x,a1)')
     &             mtype(ishi),j,(xyz(jj,
     &             loatmbas(ntp(i,j))+1,ihiatm(ntp(i,j))),jj=1,3),'*'
c                 # loop over equivalent atoms
                  do iatm = (loatmbas(ntp(i,j))+1),
     &             (loatmbas(ntp(i,j))+nc(ntp(i,j)))
                     write(48,'(1x,a2,2x,f5.1,4f14.8)') mtype(ishi),
     &                chg(ishi),(xyz(jj,iatm,ihiatm(ntp(i,j))),jj=1,3),
     &                atmas(ishi)
                  enddo
               enddo
c              # loop over all contractions for element
               do icon=1,nf(ihiatm(ntp(i,1)))
                  jcon=iang(i,icon)
                  pcount(icon)=pcount(icon)+iconu(jcon)
                  write(19,'(a1,2i4)') 'H',iconu(jcon),nrcr(jcon)
                  do ij=1,iconu(jcon)
c                    # first line
                     write(19,'(f20.8,3f19.8)')zet(ij,jcon),
     &                (eta(k,ij,jcon),k=1,min(3,nrcr(jcon)))
c                    # further lines
                     if (nrcr(jcon).gt.3) write(19,'(20x,3f19.8)')
     &                (eta(k,ij,jcon),k=4,nrcr(jcon))
                  enddo
               enddo
               ij=0
               do iam=1,lang(i)
                  k=0
                  do j=1,icont(i,iam)
                     ij=ij+1
                     k=k+pcount(ij)
                  enddo
                  maxpri = max(maxpri,k)
               enddo
            enddo
            if(igr.le.4) then
               do irep=1,nirep
                  write(6,'(1x,a3,i15)')lolabl(irep),isizl(irep)
               enddo
            endif
            write(6,*)'writing daltcomm and daltaoin...'
            open(unit=29,file='daltcomm',status='unknown')
            write(29,'(a)')'**DALTONINPUT'
            write(29,'(a)')'.INTEGRALS'
            write(29,'(a)')'.PRINT'
            write(29,'(a)')'    2'
            write(29,'(a)')'*END OF'
            write(29,'(a)')'**INTEGRALS'
            write(29,'(a)')'.ANGMOM'
            write(29,'(a)')'.CARMOM'
            write(29,'(a)')'    4'
            write(29,'(a)')'.PRINT'
            write(29,'(a)')'    2'
            write(29,'(a)')'.NOSUP'
            write(29,'(a)')'*READIN'
            write(29,'(a)')'.MAXPRI'
            write(29,'(i5)') maxpri
            write(29,'(a)')'*END OF'
            write(29,'(a)')'*END OF INPUT'
            close (unit=29)
            close (unit=19)
            close (unit=41)
            write (6,*)
            write (6,*) 'the files geom, infofl.hermit, daltaoin,',
     &       ' and daltcomm have been written'
c
         else
c
c           # turbocol
            open(unit=18,file='control.basis',status='unknown')
            write(18,'(a)') '$title'
            write(18,'(a)') atitle
c         write(18,'(a)') '$coord'
c           # for turbocol all atoms must be listed, not only those that
c           # are symmetry distinct
            do i=1,ielm
               ishi=ihiatm(ntp(i,1))
               do j=1,igrp(i)
                  do iatm=loatmbas(ntp(i,j))+1,
     &             loatmbas(ntp(i,j))+nc(ntp(i,j))
                     write(48,'(1x,a2,2x,f5.1,4f14.8)') mtype(ishi),
     &                chg(ishi),(xyz(jj,iatm,ihiatm(ntp(i,j))),jj=1,3),
     &                atmas(ishi)
                  enddo
               enddo
            enddo
            write(18,'(a)') '$atoms'
            k=1
c           # assumes one basis set per element
            do i=1,ielm
               ishi=ihiatm(ntp(i,1))
               kk=k
               do j=1,igrp(i)
                  k=k+nc(ntp(i,j))
               enddo
               write(18,"(a,i5,' -',i3,' basis = setno',i2)")
     &          mtype(ishi),kk,k-1,i
            enddo
            write(18,'(a)') '$basis'
            write(18,'(a)') '*'
            do i=1,ielm
               write(18,"('setno',i2)") i
               write(18,'(a)') '*'
               do icon=1,nf(ihiatm(ntp(i,1)))
c                 # find out to which l quantum number the
c                 # contraction belongs
                  k=icon
                  do kk=1,lang(i)
                     k=k-icont(i,kk)
                     if (k.le.0) then
                        ltyp=kk
                        goto 5003
                     endif
                  enddo
5003              continue
                  jcon=iang(i,icon)
c                 # "converts" generalized contraction to segmented
                  do j=1,nrcr(jcon)
c                    # calculate the number of entries without zeros
                     k=0
                     do ij=1,iconu(jcon)
                        if (abs(eta(j,ij,jcon)).gt.thr9) k=k+1
                     enddo
c                    # write out basis sets as segmented contractions
                     write(18,'(i2,1x,a1)') k, am(ltyp)
                     do ij=1,iconu(jcon)
                        if (abs(eta(j,ij,jcon)).gt.thr9) then
                           write(18,'(f15.8,f18.8)') zet(ij,jcon),
     &                      eta(j,ij,jcon)
                        endif
                     enddo
                  enddo
               enddo
               write(18,'(a)') '*'
            enddo
            if (sphericalaos) then
               write(18,'(a)') '$pople AO'
            else
               write(18,'(a)') '$pople CAO'
            endif
            write(18,'(a9,2x,a6)') '$symmetry  ',loptgrp
            close (unit=18)
            write (6,*)
            write (6,*) 'the files geom, infofl, and control.basis',
     &       ' have been written'
c
         endif
c
      endif
      write (6,*)
      close (unit=48)
      close (unit=13)
      call bummer('normal termination',0,3)
      stop 'end of iargos'
      end
c deck setlib
      subroutine setlib(libdir, ptgrplib)
c
c     # determine the basis library and point group data files.
c
c  output:
c  libdir = directory with library of basis and ecps.
c  ptgrplib = file with point group symmetry info
c
c  10-oct-01 written by ron shepard.
c
       implicit none
c     # dummy:
      character*(*) libdir, ptgrplib
c
c     # local:
      integer lenc, ierr
      logical qexist
c
      character*130 ccol
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer  fstrlen
      external fstrlen
c
c     # first, check for environment variables.
c
      ierr     = 0
      libdir   = ' '
      ptgrplib = ' '
      ccol     = ' '
*@if defined (posix) || defined ( t3e64)
*c     # ignore return lengths and error values
*      call pxfgetenv( 'COL_BASISLIBDIR', 0, libdir,   lenc, ierr )
*      call pxfgetenv( 'COL_PTGRPLIB',    0, ptgrplib, lenc, ierr )
*      call pxfgetenv( 'COLUMBUS',        0, ccol,     lenc, ierr )
*@elif defined  unix
      call getenv( 'COL_BASISLIBDIR',   libdir )
      call getenv( 'COL_PTGRPLIB',      ptgrplib )
      call getenv( 'COLUMBUS',          ccol )
*@endif
c
      if (libdir .eq. ' ') then
         if ( ccol .eq. ' ') then
            write(6,*)
            write(6,*) 'neither COL_BASISLIBDIR nor COLUMBUS are set '
     &       // 'in the environment.'
            write(6,*) 'at a later time, you will be required to '
     &       // 'specify specific basis library files.'
         else
c           # COL_BASISLIBDIR was not set.
c           # set libdir to $COLUMBUS/source/iargos/basis/
c           # (change this later if the default basis directory moves.)
            lenc = fstrlen( ccol )
            libdir = ccol(1:lenc) //
     &       '/source/iargos/basis/'
         endif
      endif
c
c     # make sure libdir ends with a '/' character.
c
      lenc = fstrlen (libdir )
      if ( (lenc .gt. 0)              .and.
     &   (libdir(lenc:lenc) .ne. '/') .and.
     &   (lenc .lt. len(libdir) )           ) then
         libdir(lenc+1:lenc+1) = '/'
      endif
c
      if (ptgrplib .eq. ' ') then
         if ( ccol .eq. ' ') then
            write(6,*)
            write(6,*) 'neither COL_PTGRPLIB nor COLUMBUS are set '
     &       // 'in the environment.'
            write(6,*) 'you must specify a point group data file '
     &       // 'at this time.'
            call getitle( 'symmetry library file:', ptgrplib )
         else
c           # COL_PTGRPLIB was not set.
c           # set ptgrplib to $COLUMBUS/source/iargos/ptgrp
c           # (change this later if the default symmetry data moves.)
            lenc = fstrlen( ccol )
            ptgrplib = ccol(1:lenc) //
     &       '/source/iargos/ptgrp'
         endif
      endif
c
c     # check to make sure the symmetry data file exists.
c     # the contents of ptgrplib are not checked for validity,
c     # only the existence of the file.
c
100   continue
      inquire(file=ptgrplib, exist=qexist)
      lenc = fstrlen( ptgrplib )
      if ( .not. qexist .or. (lenc .eq. 0) ) then
         write(6,*)
         write(6,*) 'the symmetry file does not exist, ptgrplib='
         write(6,*) ptgrplib(1:lenc)
c
         write(6,*) 'you must specify a point group data file '
     &    // 'at this time.'
         ptgrplib = ' '
         call getitle( 'symmetry library file:', ptgrplib )
         goto 100
      endif
c
c     # note that the basis library directory is
c     # not checked for validity at this point.
c
      return
      end
c deck angarr
      subroutine angarr(
     & icomp,  ijk,    iperm,  iprd,
     & lco,    lmnv,   mxang,  mxang1,
     & mxang2, mxang3, mxang4, mxang5)
c
c     # fill angular-momentum-related arrays
c
      implicit real*8 (a-h,o-z)
      integer icomp(mxang3,mxang2,mxang),ijk(3),iperm(mxang2,mxang),
     & iprd(3,mxang1,mxang3),lco(mxang2,mxang4,mxang),lmnv(3,mxang5)
c
c  monomial products.  iprd(i,j,l) is the index of symbol i (1=x, 2=y,
c  3=z) times the jth monomial with degree l.  Example: y times xy is
c  xy^2, so iprd(2,4,2)=6, because xy is the 4th monomial of degree 2
c  and xy^2 is the 6th monomial of degree 3.
c     data (((iprd(i,j,l),i=1,3),j=1,l*(l+1)/2),l=1,mxang3)
c    & /1,2,3, 1,4,5,4,2,6,5,6,3,
c    &  1,4,5,6,2,7,8,9,3,4,6,10,5,10,8,10,7,9,
c    &  1,4,5,6,2,7,8,9,3,4,10,13,5,13,11,
c    &  10,6,14,14,7,12,11,15,8,15,12,9,13,14,15/
c  composition of monomials.  icomp(i,j,l) is the ith symbol (1=x, 2=y,
c  3=z) of the jth monomial of degree l.  Example: The first monomial of
c  degree 3 is x**3, so icomp(1,1,3)=icomp(2,1,3)=icomp(3,1,3)=1.
c     data (((icomp(i,j,l),i=1,l-1),j=1,l*(l+1)/2),l=2,mxang)
c    & /1,2,3, 1,1,2,2,3,3,1,2,1,3,2,3,
c    &  1,1,1,2,2,2,3,3,3,1,1,2,1,1,3,2,2,1,2,2,3,3,3,1,3,3,2,1,2,3,
c    &  1,1,1,1,2,2,2,2,3,3,3,3,1,1,1,2,1,1,1,3,
c    &  2,2,2,1,2,2,2,3,3,3,3,1,3,3,3,2,1,1,2,2,
c    &  1,1,3,3,2,2,3,3,1,1,2,3,2,2,1,3,3,3,1,2/
c
c  lco(i,j,l) is the coefficients of the ith monomial in the jth SO with
c  well-defined angular momentum l-1.
c     data (((lco(i,j,l),i=1,l*(l+1)/2),j=1,2*l-1),l=1,mxang)
c     pad with zeros for mxang=6
c     data lco /
c    &  1, 230*0,
c    &  0,0,1, 18*0,
c    &  1,0,0, 18*0,
c    &  0,1,0, 186*0,
c    &  -1,-1,4,0,0,0, 15*0,
c    &  0,0,0,0,1,0, 15*0,
c    &  0,0,0,0,0,1, 15*0,
c    &  1,-1,0,0,0,0, 15*0,
c    &  0,0,0,1,0,0, 141*0,
c    &  0,0,4,0,-9,0,-9,0,0,0, 11*0,
c    &  -1,0,0,0,0,-1,0,16,0,0, 11*0,
c    &  0,-1,0,-1,0,0,0,0,16,0, 11*0,
c    &  0,0,0,0,1,0,-1,0,0,0, 11*0,
c    &  0,0,0,0,0,0,0,0,0,1, 11*0,
c    &  -1,0,0,0,0,9,0,0,0,0, 11*0,
c    &  0,-1,0,9,0,0,0,0,0,0, 95*0,
c    &  -9,-9,-64,0,0,0,0,0,0,-36,576,576,0,0,0, 6*0,
c    &  0,0,0,0,-9,0,0,16,0,0,0,0,0,-9,0, 6*0,
c    &  0,0,0,0,0,0,-9,0,16,0,0,0,-9,0,0, 6*0,
c    &  0,0,0,1,0,1,0,0,0,0,0,0,0,0,-36, 6*0,
c    &  1,-1,0,0,0,0,0,0,0,0,-36,36,0,0,0, 6*0,
c    &  0,0,0,0,-1,0,0,0,0,0,0,0,0,9,0, 6*0,
c    &  0,0,0,0,0,0,-1,0,0,0,0,0,9,0,0, 6*0,
c    &  1,1,0,0,0,0,0,0,0,-36,0,0,0,0,0, 6*0,
c    &  0,0,0,1,0,-1,0,0,0,0,0,0,0,0,0, 48*0,
c    &  0,0,64,0,225,0,225,0,0,0,0,0,0,-1600,-1600,0,0,0,900,0,0,
c    &  1,0,0,0,0,1,0,64,0,4,-144,0,0,0,0,0,0,0,0,0,-144,
c    &  0,1,0,1,0,0,0,0,64,0,0,4,-144,0,0,0,0,0,0,-144,0,
c    &  0,0,0,0,-1,0,1,0,0,0,0,0,0,4,-4,0,0,0,0,0,0,
c    &  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,4,0,0,0,
c    &  -1,0,0,0,0,9,0,0,0,4,64,0,0,0,0,0,0,0,0,0,-576,
c    &  0,1,0,-9,0,0,0,0,0,0,0,-4,-64,0,0,0,0,0,0,576,0,
c    &  0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,-36,0,0,
c    &  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,-1,0,0,0,0,
c    &  1,0,0,0,0,25,0,0,0,-100,0,0,0,0,0,0,0,0,0,0,0,
c    &  0,1,0,25,0,0,0,0,0,0,0,-100,0,0,0,0,0,0,0,0,0/
c
c  iperm(j,l) is where the jth monomial of degree l-1 gets sent under a
c  x -> y -> z -> x permutation of the axes.
c     data ((iperm(j,l),j=1,l*(l+1)/2),l=1,mxang)
c    & /1,2,3,1,2,3,1,6,4,5,2,3,1,7,6,9,8,4,5,10,
c    &  2,3,1,7,6,9,8,4,5,12,10,11,14,15,13/
c
c     data ((poly(j,l),j=1,l*(l+1)/2),l=1,mxang) /'     ',
c    &  '    X','    Y','    Z',
c    &  '   X2','   Y2','   Z2','   XY','   XZ','   YZ',
c    &  '   X3','   Y3','   Z3','  X2Y','  X2Z',
c    &  '  Y2X','  Y2Z','  Z2X','  Z2Y','  XYZ',
c    &  '   X4','   Y4','   Z4','  X3Y','  X3Z',
c    &  '  Y3X','  Y3Z','  Z3X','  Z3Y',' X2Y2',
c    &  ' X2Z2',' Y2Z2',' X2YZ',' Y2XZ',' Z2XY'/
c
      call lmnvgn(mxang,lmnv)
c  monomial products.  iprd(i,j,l) is the index of symbol i (1=x, 2=y,
c  3=z) times the jth monomial with degree l.  Example: y times xy is
c  xy^2, so iprd(2,4,2)=6, because xy is the 4th monomial of degree 2
c  and xy^2 is the 6th monomial of degree 3.
      ndx=0
      ndxk=1
      do l=1,mxang3
         do j=1,(l*(l+1))/2
            do i=1,3
               i2=mod(i,3)+1
               i3=mod(i+1,3)+1
               do k=1,((l+1)*(l+2))/2
                  if(lmnv(i,ndx+j)+1.eq.lmnv(i,ndxk+k).and.
     &             lmnv(i2,ndx+j).eq.lmnv(i2,ndxk+k).and.
     &             lmnv(i3,ndx+j).eq.lmnv(i3,ndxk+k)) iprd(i,j,l)=k
               enddo
            enddo
         enddo
         ndx=ndxk
         ndxk=ndxk+((l+1)*(l+2))/2
      enddo
c  composition of monomials.  icomp(i,j,l) is the ith symbol (1=x, 2=y,
c  3=z) of the jth monomial of degree l.  Example: The first monomial of
c  degree 3 is x**3, so icomp(1,1,3)=icomp(2,1,3)=icomp(3,1,3)=1.
      ndx=1
      do l=2,mxang
         do j=1,(l*(l+1))/2
            ndx=ndx+1
            ijk(1)=1
            ijk(2)=2
            ijk(3)=3
            if(lmnv(ijk(1),ndx).lt.lmnv(ijk(2),ndx)) then
               isw=ijk(2)
               ijk(2)=ijk(1)
               ijk(1)=isw
            endif
            if(lmnv(ijk(2),ndx).lt.lmnv(ijk(3),ndx)) then
               isw=ijk(3)
               ijk(3)=ijk(2)
               ijk(2)=isw
            endif
            if(lmnv(ijk(1),ndx).lt.lmnv(ijk(2),ndx)) then
               isw=ijk(2)
               ijk(2)=ijk(1)
               ijk(1)=isw
            endif
            i=1
            do ixyz=1,3
               do k=1,lmnv(ijk(ixyz),ndx)
                  icomp(i,j,l)=ijk(ixyz)
                  i=i+1
               enddo
            enddo
         enddo
      enddo
c  iperm(j,l) is where the jth monomial of degree l-1 gets sent under a
c  x -> y -> z -> x permutation of the axes.
      ndx=0
      do l=1,mxang
         do j= 1,(l*(l+1))/2
            do k=1,(l*(l+1))/2
               if(lmnv(1,ndx+j).eq.lmnv(2,ndx+k).and.
     &          lmnv(2,ndx+j).eq.lmnv(3,ndx+k).and.
     &          lmnv(3,ndx+j).eq.lmnv(1,ndx+k)) iperm(j,l)=k
            enddo
         enddo
         ndx=ndx+(l*(l+1))/2
      enddo
c
c     do l=1,mxang3
c       write (6,'(40i2)') ((iprd(i,j,l),i=1,3),j=1,(l*(l+1))/2)
c     enddo
c     do l=2,mxang
c       write (6,'(40i2)') ((icomp(i,j,l),i=1,l-1),j=1,(l*(l+1))/2)
c     enddo
c     do l=1,mxang
c       write (6,'(40i2)') (iperm(j,l),j=1,(l*(l+1))/2)
c     enddo
c     if(mxang.gt.0) stop
c
      return
      end
c deck lmnvgn
      subroutine lmnvgn(lmn1u,lmnv)
c     # generate cartesian gaussian exponent array.
c     # lmnv(*,*) = exponents of the cartesian gaussian basis functions.
c     #             s  p  d   f   g   h   i
c     #       lmn = 0  1  2   3   4   5   6
c     #    numxyz = 1, 3, 6, 10, 15  21  28      =((lmn+1)*(lmn+2))/2
       implicit none 
       integer i, j, k, lmn, lmn1u, lmnv(3,*), ndx
c
      ndx = 0
      do 40 lmn = 0,lmn1u-1
         do 30 i = lmn,(lmn+2)/3,-1
            do 20 j = min(i,lmn-i),(lmn-i+1)/2,-1
               k = lmn - i -j
               if( i.eq.j .and. j.eq.k ) then
                  ndx = ndx + 1
                  lmnv(1,ndx) = i
                  lmnv(2,ndx) = i
                  lmnv(3,ndx) = i
               elseif( i.eq.j .and.j.gt.k ) then
                  ndx = ndx + 1
                  lmnv(1,ndx) = i
                  lmnv(2,ndx) = i
                  lmnv(3,ndx) = k
                  ndx = ndx + 1
                  lmnv(1,ndx) = i
                  lmnv(2,ndx) = k
                  lmnv(3,ndx) = i
                  ndx = ndx + 1
                  lmnv(1,ndx) = k
                  lmnv(2,ndx) = i
                  lmnv(3,ndx) = i
               elseif( i.gt.j .and. j.eq.k ) then
                  ndx = ndx + 1
                  lmnv(1,ndx) = i
                  lmnv(2,ndx) = j
                  lmnv(3,ndx) = j
                  ndx = ndx + 1
                  lmnv(1,ndx) = j
                  lmnv(2,ndx) = i
                  lmnv(3,ndx) = j
                  ndx = ndx + 1
                  lmnv(1,ndx) = j
                  lmnv(2,ndx) = j
                  lmnv(3,ndx) = i
               elseif( i.gt.j .and. j.gt.k ) then
                  ndx = ndx + 1
                  lmnv(1,ndx) = i
                  lmnv(2,ndx) = j
                  lmnv(3,ndx) = k
                  ndx = ndx + 1
                  lmnv(1,ndx) = i
                  lmnv(2,ndx) = k
                  lmnv(3,ndx) = j
                  ndx = ndx + 1
                  lmnv(1,ndx) = j
                  lmnv(2,ndx) = i
                  lmnv(3,ndx) = k
                  ndx = ndx + 1
                  lmnv(1,ndx) = k
                  lmnv(2,ndx) = i
                  lmnv(3,ndx) = j
                  ndx = ndx + 1
                  lmnv(1,ndx) = j
                  lmnv(2,ndx) = k
                  lmnv(3,ndx) = i
                  ndx = ndx + 1
                  lmnv(1,ndx) = k
                  lmnv(2,ndx) = j
                  lmnv(3,ndx) = i
               endif
20          enddo
30       enddo
40    enddo
      return
      end
