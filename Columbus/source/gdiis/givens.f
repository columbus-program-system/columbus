!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c
c*************************************************************
c
      subroutine givens (nx,nrootx,njx,a,b,root,vect,nvec)
      implicit real*8 (a-h,o-z)
c
c      qcpe program 62.3
c      eigenvalues and eigenvectors by the givens method.
c      by franklin prosser, indiana university.
c      september, 1967
c
c      *** argument nvec added may-1978 by ron shepard ***
c      *** parameters and other minor cleanup 11-july-88 ron shepard ***
c
c      calculates eigenvalues and eigenvectors of real symmetric matrix
c      stored in packed upper triangular form.
c
c      thanks are due to f. e. harris (stanford university) and h. h.
c      michels (united aircraft research laboratories) for excellent
c      work on numerical difficulties with earlier versions of this
c      program.
c
c      the arguments for the routine are...
c          nx     order of matrix
c          nrootx number of roots wanted.  the nrootx smallest (most
c                  negative) roots will be calculated.  if no vectors
c                  are wanted, make this number negative.
c          njx    row dimension of vect array.  see  vect  below.
c                  njx must be not less than nx.
c          a      matrix stored by columns in packed upper triangular
c                 form, i.e. occupying nx*(nx+1)/2 consecutive
c                 locations.
c          b      scratch array used by givens.  must be at least
c                  nx*5 cells.
c          root   array to hold the eigenvalues.  must be at least
c                 nrootx cells long.  the nrootx smallest roots are
c                  ordered largest first in this array.
c          vect   eigenvector array.  each column will hold an
c                  eigenvector for the corresponding root.  must be
c                  dimensioned with  njx  rows and at least  nrootx
c                  columns, unless no vectors
c                  are requested (negative nrootx).  in this latter
c                  case, the argument vect is just a dummy, and the
c                  storage is not used.
c                  the eigenvectors are normalized to unity.
c          nvec   number of vectors to calculate.
c
c      the arrays a and b are destroyed by the computation. the results
c      appear in root and vect.
c      for proper functioning of this routine, the result of a floating
c      point underflow should be a zero.
c
c      the original reference to the givens technique is in oak ridge
c      report number ornl 1574 (physics), by wallace givens.
c      the method as presented in this program consists of four steps,
c      all modifications of the original method...
c      first, the input matrix is reduced to tridiagonal form by the
c      householder technique (j. h. wilkinson, comp. j. 3, 23 (1960)).
c      the roots are then located by the sturm sequence method (j. m.
c      ortega (see reference below).  the vectors of the tridiagonal
c      form are then evaluated (j.h. wilkinson, comp. j. 1, 90 (1958)),
c      and last the tridiagonal vectors are rotated to vectors of the
c      original array (first reference).
c      vectors for degenerate (or near-degenerate) roots are forced
c      to be orthogonal, using a method suggested by b. garbow, argonne
c      national labs (private communication, 1964).  the gram-schmidt
c      process is used for the orthogonalization.
c
c      an excellent presentation of the givens technique is found in
c      j. m. ortega s article in  mathematics for digital computers,
c      volume 2, ed. by ralston and wilf, wiley (1967), page 94.
c
       dimension b(nx,*),a(*),root(*),vect(njx,*)
c
c ** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
c **   users please note...
c **   the following two parameters, eta and theta, should be adjusted
c **   by the user for his particular machine.
c **   eta is an indication of the precision of the floating point
c **   representation on the computer being used (roughly 10**(-m),
c **   where m is the number of decimals of precision ).
c **   theta is an indication of the range of numbers that can be
c **   expressed in the floating point representation (roughly the
c **   largest number).
c **   some recommended values follow.
c **   for ibm 7094, univac 1108, etc. (27-bit binary fraction, 8-bit
c **   binary exponent), eta=1.e-8, theta=1.e37.
c **   for control data 3600 (36-bit binary fraction, 11-bit binary
c **   exponent), eta=1.e-11, theta=1.e307.
c **   for control data 6600 (48-bit binary fraction, 11-bit binary
c **   exponent), eta=1.e-14, theta=1.e307.
c **   for ibm 360/50 and 360/65 real*8 (56-bit hexadecimal
c **   fraction, 7-bit hexadecimal exponent), eta=1.e-16, theta=1.e75.
c **   for telefunken tr440, eta=1.e-11, theta=1.e152.
c **
cdec20 data eta,theta /1.d-14,1.d+37/
c-       data eta,theta /1.d-12,1.d+35/
c
       parameter(eta=1d-12, theta=1d35)
       parameter(zero=0d0, half=5d-1, one=1d0, two=2d0, three=3d0,
     &  four=4d0, r4099=4099d0)
c ** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
c
       del1 = eta/(100)
       delta = eta**2*(100)
       small = eta**2/(100)
       delbig = theta*delta/(1000)
       theta1 = (1000)/theta
c      toler  is a factor used to determine if two roots are close
c      enough to be considered degenerate for purposes of orthogonali-
c      zing their vectors.  for the matrix normed to unity, if the
c      difference between two roots is less than toler, then
c      orthogonalization will occur.
       toler = eta*(100)
c
c      initial value for pseudorandom number generator... (2**23)-3
       rpower = (8388608)
       rpow1 = rpower*half
       rand1 = rpower - three
c
      n = nx
      nroot = abs(nrootx)
      if (nroot.eq.0) go to 1001
      if (n-1) 1001,1003,105
c
c     special section for n=1.
1003  root(1) = a(1)
      if (nvec.gt.0) vect(1,1) = one
      go to 1001
c
  105 if (n.gt.2) goto 106
c     special section for n=2
      if(a(2).ne.zero) goto 107
      root(1) = min(a(1),a(3))
      if (nrootx.lt.0) goto 108
      if(nvec.lt.1)go to 108
      vect(1,1) = one
      vect(2,1) = zero
  108 if (nroot.lt.2) goto 1001
      root(2) = max(a(1),a(3))
      if (nrootx.lt.0) goto 1001
      if(nvec.lt.2)go to 1001
      vect(2,2) = one
      vect(1,2) = zero
      goto 1001
  107 factor = half*sqrt((a(1)-a(3))**2+four*a(2)**2)
      temp  = half* (a(1)+a(3))
      root(1) = temp - factor
      if (nrootx.lt.0) goto 109
      if(nvec.lt.1)go to 109
      temp1 = one/a(2)
      vect(2,1) = (root(1)-a(1))*temp1
      anorm = one/sqrt(one+vect(2,1)**2)
      vect(2,1) = anorm*vect(2,1)
      vect(1,1) = anorm
  109 if (nroot.lt.2) goto 1001
      root(2) = temp + factor
      if (nrootx.lt.0) goto 1001
      if(nvec.lt.2)go to 1001
      vect(1,2) = (root(2)-a(3))*temp1
      anorm = one/sqrt(one+vect(1,2)**2)
      vect(1,2) = anorm*vect(1,2)
      vect(2,2) = anorm
      goto 1001
c
c     section for  n.gt.2 .
c     nsize    number of elements in the packed array
  106 nsize = (n*(n+1))/2
      nm1 = n-1
      nm2 = n-2
      do 20 j=1,5
      do 20 i=1,nx
   20 b(i,j)=zero
c
c     scale matrix to euclidean norm of 1.  scale factor is anorm.
      factor = zero
      do 70 i=1,nsize
      temp = abs(a(i))
70    factor = max(factor,temp)
      if (factor.ne.zero) go to 72
c     null matrix.  fix up roots and vectors, then exit.
      do 78 i=1,nroot
      if (nrootx.lt.0) go to 78
      if(nvec.le.0)go to 78
      do 77 j=1,n
77    vect(j,i) = zero
      vect(i,i) = one
78    root(i) = zero
      go to 1001
c
72    anorm = zero
      j = 1
      k = 1
86    temp = one/factor
      do 80 i=1,nsize
      if (i.ne.j) go to 81
      anorm = anorm + (a(i)*temp)**2*half
      k = k+1
      j = j+k
      go to 80
81    anorm = anorm + (a(i)*temp)**2
80    continue
83    anorm = sqrt(anorm*two)*factor
      temp1 = one/anorm
      do 91 i=1,nsize
91    a(i) = a(i)*temp1
      alimit = one
c
c      tridia section.
c      tridiagonalization of symmetric matrix
       id = 0
       ia = 1
c      if (nm2.eq.0) go to 201
       do 200  j=1,nm2
c      j       counts row  of a-matrix to be diagonalized
c      ia      start of non-codiagonal elements in the row
c      id      index of codiagonal element on row being codiagonalized.
       ia = ia+j+2
       id = id + j + 1
       jp2 = j+2
c      sum squares of non-codiagonal elements in row j
       ii = ia
       sum = zero
       do 100 i=jp2,n
       sum=sum+a(ii)**2
100    ii = ii + i
       temp = a(id)
       if (sum.gt.small) go to 110
c      no transformation necessary if all the non-codiagonal
c      elements are tiny.
120    b(j,1) = temp
       a(id) = zero
       go to 200
c      now complete the sum of off-diagonal squares
110    sum = sqrt(sum + temp**2)
c      new codiagonal element
       b(j,1) = -sign(sum,temp)
c      first non-zero element of this w-vector
       b(j+1,2) = sqrt((one + abs(temp)/sum)*half)
c      form rest of the w-vector elements
       temp = sign(half/(b(j+1,2)*sum),temp)
       ii = ia
       do 130 i=jp2,n
       b(i,2) = a(ii)*temp
130    ii = ii + i
c      form p-vector and scalar.  p-vector = a-matrix*w-vector.
c     scalar = w-vector*p-vector
       ak = zero
c      ic      location of next diagonal element
       ic = id + 1
       j1 = j + 1
       do 190  i=j1,n
       jj = ic
       temp = zero
       do 180  ii=j1,n
c      i       runs over the non-zero p-elements
c      ii      runs over elements of w-vector
       temp = temp + b(ii,2)*a(jj)
c      change incrementing mode at the diagonal elements.
       if (ii.lt.i) go to 210
140    jj = jj + ii
       go to 180
210    jj = jj + 1
180    continue
c      build up the k-scalar (ak)
       ak = ak + temp*b(i,2)
       b(i,1) = temp
c      move ic to top of next a-matrix  row
190    ic = ic + i
c      form the q-vector
       do 150  i=j1,n
150    b(i,1) = b(i,1) - ak*b(i,2)
c      transform the rest of the a-matrix
c      jj      start-1 of the rest of the a-matrix
       jj = id
c      move w-vector into the old a-matrix locations to save space
c      i       runs over the significant elements of the w-vector
       do 160  i=j1,n
       a(jj) = b(i,2)
       do 170  ii=j1,i
       jj = jj + 1
170    a(jj) = a(jj) - two*(b(i,1)*b(ii,2) + b(i,2)*b(ii,1))
160    jj = jj + j
200    continue
c      move last codiagonal element out into its proper place
201    continue
       b(nm1,1) = a(nsize-1)
       a(nsize-1) = zero
c
c     sturm section.
c     sturm sequence iteration to obtain roots of tridiagonal form.
c     move diagonal elements into second n elements of b-vector.
c     this is a more convenient indexing position.
c     also, put square of codiagonal elements in third n elements.
      jump=1
      do 320 j=1,n
      b(j,2)=a(jump)
      b(j,3) = b(j,1)**2
320   jump = jump+j+1
      do 310 i=1,nroot
310   root(i) = +alimit
      rootl = -alimit
c     isolate the roots.  the nroot lowest roots are found, lowest first
      do 330 i=1,nroot
c     find current  best  upper bound
      rootx = +alimit
      do 335 j=i,nroot
335   rootx = min(rootx,root(j))
      root(i) = rootx
c     get improved trial root
500   trial = (rootl + root(i))*half
      if (trial.eq.rootl.or.trial.eq.root(i)) go to 330
c     form sturm sequence ratios, using ortega s algorithm (modified).
c     nomtch is the number of roots less than the trial value.
350   continue
      nomtch = n
      j = 1
360   f0 = b(j,2) - trial
370   continue
      if (abs(f0).lt.theta1) go to 380
      if (f0.ge.zero) nomtch = nomtch - 1
      j = j + 1
      if (j.gt.n) go to 390
c     since matrix is normed to unity, magnitude of b(j,3) is less than
c     one, and since f0 is greater than theta1, overflow is not possible
c     at the division step.
      f0 = b(j,2) - trial - b(j-1,3)/f0
      go to 370
380   j = j + 2
      nomtch = nomtch - 1
      if (j.le.n) go to 360
390   continue
c     fix new bounds on roots
      if (nomtch.ge.i) go to 540
      rootl = trial
      go to 500
540   root(i) = trial
      nom = min(nroot,nomtch)
      root(nom) = trial
      go to 500
330   continue
c     reverse the order of the eigenvalues, since custom dictates
c     'largest first'.  this section may be removed if desired without
c     affecting the remainder of the routine.
c     nrt = nroot/2
c     do 10 i=1,nrt
c     save = root(i)
c     nmip1 = nroot - i + 1
c     root(i) = root(nmip1)
c  10 root(nmip1) = save
c
c     trivec section.
c     eigenvectors of codiagonal form
807   continue
c     quit now if no vectors were requested.
      if (nrootx.lt.0) go to 1002
      if(nvec.lt.1)go to 1002
c     initialize vector array.
      do 15 i=1,n
      do 15 j=1,nvec
15    vect(i,j) = one
      do 700 i=1,nvec
      aroot = root(i)
c     orthogonalize if roots are close.
      if (i.eq.1) go to 710
c     the absolute value in the next test is to assure that the trivec
c     section is independent of the order of the eigenvalues.
715   if (abs(root(i-1)-aroot).lt.toler) go to 720
710   ia = -1
720   ia = ia + 1
      elim1 = a(1) - aroot
      elim2 = b(1,1)
      jump = 1
      do 750  j=1,nm1
      jump = jump+j+1
c     get the correct pivot equation for this step.
      if (abs(elim1).le.abs(b(j,1))) goto 760
c     first (elim1) equation is the pivot this time.  case 1.
      b(j,2) = elim1
      b(j,3) = elim2
      b(j,4) = zero
      temp = b(j,1)/elim1
      elim1 = a(jump) - aroot - temp*elim2
      elim2 = b(j+1,1)
      go to 755
c     second equation is the pivot this time.  case 2.
760   b(j,2) = b(j,1)
      b(j,3) = a(jump) - aroot
      b(j,4) = b(j+1,1)
      temp = one
      if (abs(b(j,1)).gt.theta1) temp = elim1/b(j,1)
      elim1 = elim2 - temp*b(j,3)
      elim2 = -temp*b(j+1,1)
c     save factor for the second iteration.
755   b(j,5) = temp
750   continue
      b(n,2) = elim1
      b(n,3) = zero
      b(n,4) = zero
      b(nm1,4) = zero
      iter = 1
      if (ia.ne.0) go to 801
c     back substitute to get this vector.
790   l = n + 1
      do 780 j=1,n
      l = l - 1
786   continue
      lp1 = l+1
      lp2 = l+2
      elim1=vect(l,i)
      if (lp1.le.n) elim1=elim1-vect(lp1,i)*b(l,3)
      if (lp2.le.n) elim1=elim1-vect(lp2,i)*b(l,4)
c     if overflow is conceivable, scale the vector down.
c     this approach is used to avoid machine-dependent and system-
c     dependent calls to overflow routines.
      if (abs(elim1).gt.delbig) go to 782
      temp = b(l,2)
      if (abs(b(l,2)).lt.delta) temp = delta
      vect(l,i) = elim1/temp
      go to 780
c     vector is too big.  scale it down.
782   temp1 = one/delbig
      do 784 k=1,n
784   vect(k,i) = vect(k,i)*temp1
      go to 786
780   continue
      go to (820,800), iter
c     second iteration.  (both iterations for repeated-root vectors).
820   iter = iter + 1
890   elim1 = vect(1,i)
      do 830 j=1,nm1
      if (b(j,2).eq.b(j,1)) go to 840
c     case one.
850   vect(j,i) = elim1
      elim1 = vect(j+1,i) - elim1*b(j,5)
      go to 830
c     case two.
840   vect(j,i) = vect(j+1,i)
      elim1 = elim1 - vect(j+1,i)*temp
830   continue
      vect(n,i) = elim1
      go to 790
c     produce a random vector
801   continue
      temp1 = one/rpow1
      do 802 j=1,n
c     generate pseudorandom numbers with uniform distribution in (-1,1).
c     this random number scheme is of the form...
c     rand1 = dmod((2**12+3)*rand1,2**23)
c     it has a period of 2**21 numbers.
      rand1 = mod(r4099*rand1,rpower)
802   vect(j,i) = rand1*temp1 - one
      go to 790
c
c     orthogonalize this repeated-root vector to others with this root.
800   if (ia.eq.0) go to 885
      do 860 j1=1,ia
      k = i - j1
      temp = zero
      do 870 j=1,n
870   temp = temp + vect(j,i)*vect(j,k)
      do 880 j=1,n
880   vect(j,i) = vect(j,i) - temp*vect(j,k)
860   continue
885   go to (890,900), iter
c     normalize the vector
900   elim1 = zero
      do 904 j=1,n
904   elim1 = max(abs(vect(j,i)),elim1)
      temp=zero
      temp1 = one/elim1
      do 910 j=1,n
      elim2=vect(j,i)*temp1
      temp=temp+elim2**2
  910 continue
      temp=one/(sqrt(temp)*elim1)
      do 920 j=1,n
      vect(j,i) = vect(j,i)*temp
      if (abs(vect(j,i)).lt.del1) vect(j,i) = zero
920   continue
700   continue
c
c      simvec section.
c      rotate codiagonal vectors into vectors of original array
c      loop over all the transformation vectors
       if (nm2.eq.0) go to 1002
       jump = nsize - (n+1)
       im = nm1
       do 950  i=1,nm2
       j1 = jump
c      move a transformation vector out into better indexing position.
       do 955  j=im,n
       b(j,2) = a(j1)
955    j1 = j1 + j
c      modify all requested vectors.
       do 960  k=1,nvec
       temp = zero
c      form scalar product of transformation vector with eigenvector
       do 970  j=im,n
970    temp = temp + b(j,2)*vect(j,k)
       temp = temp + temp
       do 980  j=im,n
980    vect(j,k) = vect(j,k) - temp*b(j,2)
960    continue
       jump = jump - im
950    im = im - 1
1002   continue
c      restore roots to their proper size.
       do 95 i=1,nroot
95     root(i) = root(i)*anorm
1001   return
      end
