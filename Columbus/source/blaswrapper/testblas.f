!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       program testblas
       implicit none
       integer*4 n,i
       integer*8 n8,i8

       real*8 v(10),w(10)
       real*8 result, ddot

       do i=1,10 
         v(i)=i*1.0d0
         w(i)=1.0d0/i
        enddo

        write(6,'(10f6.2)') (v(i),i=1,10)
        write(6,'(10f6.2)') (w(i),i=1,10)
        n=10
        i=1
        result= ddot(n,v,i,w,i)
c       write(6,*) 'RESULT (integer*4)=',result
        if (abs(result-10.0d0).lt.1.d-10) then
           write(6,*) 'Blas library (ddot) supports integer*4'
        else
           write(6,*) 'Blas library (ddot) does not support integer*4'
        endif
        n8=10
        i8=1
        result= ddot(n8,v,i8,w,i8)
c       write(6,*) 'RESULT (integer*8)=',result
        if (abs(result-10.0d0).lt.1.d-10) then
           write(6,*) 'Blas library (ddot) supports integer*8'
        else
           write(6,*) 'Blas library (ddot) does not support integer*8'
        endif
        stop
        end

