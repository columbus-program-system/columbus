#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


sub shortenit{
 my ($stack,$fc,$ix,@tword,$x);

 # concatenate @stack
   $stack="";
   $ix = $stack[0]; if (grep(/^\*@/,$ix)|| grep(/^\#/,$ix)) { return;}
   foreach $ix (@stack) { chop $ix; 
                          if (/^[\*]/) {$ix=~s/^.//; $fc="*";}
                          elsif (/^[cC]/) {$ix=~s/^.//;
                                        $fc="c";}
                          else {$fc=" "; $ix=~s/^.//;}
                          $ix=~s/^.....//; $ix=~s/^\s*//; $ix=~s/ *$//; 
                          if (length($ix) != 66 ) { $ix=$ix . " ";} 
                           $stack=$stack . $ix;}
# print "concatenated line:\n $stack\n";
   @tword=split(/[,]/,$stack);
#  print "words:\n",join(':',@tword),"\n"; 
   @stack=();
   $stack="$fc     ";    
   $x = shift @tword;
   if (grep(/\S/,$x)) { $stack = $stack . $x; } 
    else { $x= shift @tword; $stack = $stack . $x;}
   while (1) 
    { $x=shift @tword;
      if (! $x) {last;}
#     print "length stack,x=", length($stack)," ", length($x),"\n";
      if ( length($stack) + length($x) < 70 ) {$stack=$stack ."," . $x;}
      else { $stack=$stack . "\n"; push @stack,$stack; $stack="$fc    + ," . $x ;}
    } 
   push @stack,$stack . "\n";
   return;
 }
    

$x='/scr/columbus.hlischka/Columbus/TOOLS/CPAN/filepp-1.3.0/modules/case';

@blas_cfunc = qw(cdotc cdotu);
@blas_dfunc = qw(dasum dcabs1 ddot dnrm2 dzasum dznrm2);
@blas_ifunc = qw(icamax idamax isamax izamax );
@blas_lfunc = qw(lsame);
@blas_sfunc = qw(sasum scasum scnrm2  sdot  snrm2);
@blas_zfunc = qw(zdotc zdotu);

@blas_csub =  qw(caxpy ccopy cgbmv cgemm cgemv cgerc cgeru chbmv chemm chemv cher2
                 cher2k cher cherk chpmv chpr2 chpr crotg cscal csscal cswap csymm
                 csyr2k csyrk ctbmv ctbsv ctpmv ctpsv ctrmm ctrmv ctrsm ctrsv);
@blas_dsub = qw(daxpy dcopy dgbmv dgemm dgemv dger drot drotg dsbmv dscal dspmv dspr2
                dspr dswap dsymm dsymv dsyr2 dsyr2k dsyr dsyrk dtbmv dtbsv dtpmv dtpsv
                dtrmm dtrmv dtrsm dtrsv);
@blas_ssub = qw(saxpy scopy sgbmv sgemm sgemv sger srot srotg ssbmv sscal sspmv sspr2
                sspr sswap ssymm ssymv ssyr2 ssyr2k ssyr ssyrk stbmv stbsv stpmv stpsv
                strmm strmv strsm strsv xerbla);
@blas_zsub = qw(zaxpy zcopy zdscal zgbmv zgemm zgemv zgerc zgeru zhbmv zhemm zhemv zher2
                zher2k zher zherk zhpmv zhpr2 zhpr zrotg zscal zswap zsymm zsyr2k zsyrk
                ztbmv ztbsv ztpmv ztpsv ztrmm ztrmv ztrsm ztrsv);

 $allfiles=join(' ',@ARGV);

 foreach $file (@ARGV) 
 { print "DOUBLE blasconversion for file $file \n";
   if (! -f $file ){ die "could not find $file\n";}
   %counts={};
   $keystring=" ";
   foreach $k (@blas_dsub) { $keystring=$keystring . " -D${k}=${k}_wr"; $counts{$k}=0; }
   print "keystring=\n $keystring\n";
   system("/scr/columbus.hlischka/Columbus/filepp $keystring -k -w -m $x/tolower.pm $file  > $file.$$; mv $file.$$ $file;");

   $keystring=" ";
   foreach $k (@blas_dfunc, @blas_ifunc, @blas_lfunc) { $keystring=$keystring . " -D${k}=${k}_wr"; $counts{$k}=0;}
   print "keystring=\n $keystring\n";
   system("/scr/columbus.hlischka/Columbus/filepp $keystring -k -w -m $x/tolower.pm $file  > $file.$$; mv $file.$$ $file;");

#
#  now check whether some lines are longer than 72 character
#

   open FIN,"<$file";
   open FOUT,">$file.out";
   $l=0;
   while (1) { $val=<FIN>; if (! $val ) {last;} $l++; 
        $_=$val; 
       if (/^ [\d ][\d ][\d ][\d ][\S]/ || /^[\*] [\d ][\d ][\d ][\d ][\S]/)
         { # this is a continuation line
             push @stack, $val; 
        }
       else {if ($toolong) {# print "original line \n@stack\n"; 
                               shortenit(); 
                            # print "replace line \n@stack\n"; 
                              }
                $toolong=0;
               print FOUT @stack; 
              @stack=(); push @stack,$val;}
    if (length($val) > 73) { print "line $l too long\n"; $toolong=1; 
   }}

   print FOUT @stack; 
   close FIN;
   close FOUT;
   @stack=();
   system("cp -f $file.out $file; rm -f $file.out ");

 }

  # counting

  system("grep _wr $allfiles > grepresult");

  open FIN,"<grepresult";
  @grepres = <FIN>;
  close FIN;
  foreach $key (keys %counts )
   { $cnt=grep(/\W${key}_wr\W/,@grepres);
     if ($cnt) {print " $cnt times $key\n";}}

  





