!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program plot_graph_density

   ! program to plot the graph density for gcf wave functions.

   ! 1 or 2 graph density filenames are read from stdin.
   ! if 1 filename is read, then the graph density for that file is plotted.
   ! if 2 files are given, then the density(2)-density(1) is plotted.
   ! the drt file is read from mcdrtfl.
   ! the output (currently gnuplot commands) is written to stdout.

   ! 27-may-2010 written by ron shepard.

   use precision_defs
   use mcdrt_mod
   use plot_graph_density_mod
   use gcf_mod, only: graph_density_type, allocate_graph_density

   implicit none

   integer, parameter :: ndrt=20, density1=22, density2=23
   integer, parameter :: gnuplot=6  ! it is assumed that this is preconnected to stdout.
   integer :: nfile, window, ierr

   type(graph_density_type) :: graph_density(2)

   type(mcdrt) :: drt

   character(132) :: fname

   ! open and read the drt file.
   fname = 'mcdrtfl'
   call trnfln( 1, fname )
   open(unit=ndrt, file=fname, status='old', form='formatted')

   call read_mcdrt( ndrt, drt )  ! allocate and read the drt arrays.

   close(unit=ndrt)

   call allocate_graph_density( drt%nrow, graph_density )

   ! read the info from the first file.

   
   ! read the info for the first density file.
   nfile = 1
   read(*,*) fname
   open(unit=density1,file=fname,form='unformatted')

   ! read the info for the second density file.
   read(*,*,iostat=ierr) fname
   if ( ierr == 0 ) then
      nfile = 2
      open(unit=density2,file=fname,form='unformatted')
   endif

   ! each file has one record for each state, plus an extra record for state-averaging if nstate>1.
   ! loop over these records.
   window = 0
   do
      read(density1,iostat=ierr) graph_density(1)%node, graph_density(1)%arc
      if ( ierr .ne. 0 ) goto 9999

      if ( nfile == 2 ) then  ! read the graph density from the optional second file.
         read(density2,iostat=ierr) graph_density(2)%node, graph_density(2)%arc
         if ( ierr .ne. 0 ) goto 9999

         ! replace the original density with the difference density.

         graph_density(1)%node = graph_density(2)%node - graph_density(1)%node
         graph_density(1)%arc  = graph_density(2)%arc  - graph_density(1)%arc
      endif

      ! write the gnuplot info to stdout (other options may be added in the future).
      call plot_graph_density_gnu( drt, graph_density(1), gnuplot, window)

      window = window + 1
enddo

9999 continue  ! input files have been exhausted.
end program plot_graph_density
