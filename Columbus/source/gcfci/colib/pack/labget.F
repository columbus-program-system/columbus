!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine labget(word,lab,ipos)
c
c  this routine gets the integer label 'lab' from the 'ipos' position
c  of the working precision word 'word'.
c
cevb  removed implicit ints
cevb      implicit integer(a-z)
      implicit none

cevb and F90/F95 instrinsic bitwise manipulations

#if defined(INT64)
      integer iword(2),lab,ipos
      integer word
      integer m32
      parameter(m32=2**32-1)
#  if defined(T3E64) || defined(CRAY)
c
      iword(1)=shiftr(word,32)
      iword(2)=and(word,m32)
      lab=iword(ipos)
#  elif defined(DECALPHA) || defined(SGIPOWER)
       iword(1)=ishft(word,-32)
       iword(2)=iand(word,m32)
       lab=iword(ipos)
#  elif defined(F90) || defined(F95)
       iword(1)=ishft(word,-32)
       iword(2)=iand(word,m32)
       lab=iword(ipos)
#  else 
       call bummer ('labget not implemented',0,2)
#  endif 
#else 
      integer iword(2),lab,ipos
      integer word(2)
      lab=word(ipos)
#endif 
      return
      end
