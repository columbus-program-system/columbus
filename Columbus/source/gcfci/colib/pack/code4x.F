!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine code4x(
     & buffer, strtnf, stn121, stn3,
     & nintkl, lsym,   ksym,   l1s,
     & l1f,    k1s,    k1f,    k1l1 )
c
c  pack the 4-external header info into two working precision words.
c
c  03-jul-01 modified to allow large basis sets (>170). -rls
c
      implicit none
c
c     # dummy:
      integer strtnf, stn121, stn3, nintkl, lsym, ksym,
     & l1s, l1f, k1s, k1f, k1l1
      integer i,j
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
#if defined(INT64)
       integer buffer(2)
       integer oder,links
#  if defined(F90) || defined(F95)
       oder(i,j)=or(i,j)
       links(i,j)=ishft(i,j)
#  elif defined(CRAY) || defined(T3E64)
       oder(i,j)=or(i,j)
       links(i,j)=shiftl(i,j)
#  elif defined(SGIPOWER) || defined(DECALPHA)
       oder(i,j)=ior(i,j)
       links(i,j)=ishft(i,j)
#  else 
       call bummer ('code4x not implemented',0,2)
#  endif 
      write(0,*) 'more than 255 basis functions not supported in 64bit'
      buffer(1)=oder(links(strtnf,48), oder(links(stn121,32),
     &          oder(links(stn3,  16), nintkl)))
      buffer(2)=oder(links(lsym,  56), oder(links(ksym,  48),
     &          oder(links(l1s,   40), oder(links(l1f,   32),
     &          oder(links(k1s,   24),
     &          oder(links(k1f,   16), k1l1))))))
#elif defined(OLDOLDSUN) || defined(FPS)
      integer iword(2)
      real*8 dword , buffer(2)
      equivalence (iword(1),dword)
      integer   lshift, or
      intrinsic lshift, or
      iword(1)=or(lshift(strtnf,16), stn121)
      iword(2)=or(lshift(stn3,  16), nintkl)
      buffer(1)=dword
      iword(1)=or(lshift(lsym,  24), or(lshift(ksym,16),
     &         or(lshift(l1s,    8), l1f)))
      iword(2)=or(lshift(k1s,   24), or(lshift(k1f, 16), k1l1))
      buffer(2)=dword
#elif defined(OBSOLETE)
c     # 32-bit integer machines with f90 bit operators.
c     # record length parameters are limited to 16-bit fields.
      integer iword(2)
      real*8 dword, buffer(2)
      equivalence (iword(1),dword)
      integer   ior, ishft
      intrinsic ior, ishft
      iword(1)  = ior(ishft(strtnf,16), stn121)
      iword(2)  = ior(ishft(stn3,  16), nintkl)
      buffer(1) = dword
      iword(1)  = ior(ishft(lsym,  24), ior(ishft(ksym,16),
     &            ior(ishft(l1s,    8), l1f)))
      iword(2)  = ior(ishft(k1s,   24), ior(ishft(k1f, 16), k1l1))
      buffer(2) = dword
#else 
c     # 32-bit integer machines with f90 bit operators.
c     # record length parameters are limited to 18-bit fields.
c
c     # dummy:
      real*8 buffer(3)
c
c     # local:
      integer iword(2)
      real*8 dword
      equivalence (iword(1),dword)
c
      integer ib1hi,  ib2hi,  ib3hi,  ib4hi
      integer ib1low, ib2low, ib3low, ib4low
c
      integer   maxsiz,             maxbfn,       mask2
      parameter(maxsiz = 2**18 - 1, maxbfn = 511, mask2=3)
c
      integer   ior, ishft, iand
      intrinsic ior, ishft, iand
c
      if ( strtnf .gt. maxsiz .or. strtnf .lt. 0 ) then
         call bummer('code4x: strtnf=', strtnf, faterr)
      elseif ( stn121 .gt. maxsiz .or. stn121 .lt. 0 ) then
         call bummer('code4x: stn121=', stn121, faterr)
      elseif ( stn3 .gt. maxsiz .or. stn3 .lt. 0 ) then
         call bummer('code4x: stn3=', stn3, faterr)
      elseif ( nintkl .gt. maxsiz .or. nintkl .lt. 0 ) then
         call bummer('code4x: nintkl=', nintkl, faterr)
      elseif ( lsym .gt. 8 .or. lsym .lt. 1 ) then
         call bummer('code4x: lsym=', lsym, faterr)
      elseif ( ksym .gt. 8 .or. ksym .lt. 1 ) then
         call bummer('code4x: ksym=', ksym, faterr)
      elseif ( l1s .gt. maxbfn .or. l1s .lt. 0 ) then
         call bummer('code4x: l1s=', l1s, faterr)
      elseif ( l1f .gt. maxbfn .or. l1f .lt. 0 ) then
         call bummer('code4x: l1f=', l1f, faterr)
      elseif ( k1s .gt. maxbfn .or. k1s .lt. 0 ) then
         call bummer('code4x: k1s=', k1s, faterr)
      elseif ( k1f .gt. maxbfn .or. k1f .lt. 0 ) then
         call bummer('code4x: k1f=', k1f, faterr)
      elseif ( k1l1 .gt. maxsiz .or. k1l1 .lt. 0 ) then
         call bummer('code4x: k1l1=', k1l1, faterr)
      endif
c
      ib1hi  = ishft(strtnf,-2)
      ib1low = iand(strtnf,mask2)
      ib2hi  = ishft(stn121,-2)
      ib2low = iand(stn121,mask2)
      ib3hi  = ishft(stn3,-2)
      ib3low = iand(stn3,mask2)
      ib4hi  = ishft(nintkl,-2)
      ib4low = iand(nintkl,mask2)
c
      iword(1)  = ior(ishft(ib1hi,16), ib2hi)
      iword(2)  = ior(ishft(ib3hi,16), ib4hi)
      buffer(1) = dword
cold
c     iword(1)  = ior(ishft(l1s,  24), ior(ishft(l1f,16),
c    &            ior(ishft(k1s,    8),          k1f)))
      iword(1)  = 0
      iword(2)  = ior(ishft(lsym-1,29), ior(ishft(ksym-1,26),
     &            ior(ishft(ib1low,24), ior(ishft(ib2low,22),
     &            ior(ishft(ib3low,20), ior(ishft(ib4low,18),
     &            k1l1))))))
      buffer(2) = dword
cnew
       iword(1)  = ior(ishft(l1s,  16), l1f)
       iword(2)  = ior(ishft(k1s,  16), k1f)
       buffer(3) = dword
c      write(0,*) 'code4x:',l1s,l1f,k1s,k1f
#endif 
      return
      end
