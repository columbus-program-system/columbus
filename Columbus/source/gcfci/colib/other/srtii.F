!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine srtii( n, a, indx )
c
c         indexed sort in increasing order
c
c  sort the elements a(*) by rearranging the index entries in indx(*)
c  into increasing order of the vector entries.
c  this routine uses the o(n*log(n)) heapsort method.
c
c  input:
c  n  = vector length of indx(*).
c  a(*) = vector elements.
c  indx(1:n) = initial order of the elements of a(*).
c
c  output:
c  a(*) = unchanged.
c  indx(1:n) = initial entries are rearranged such that
c              a(indx(i)).le.a(indx(i+1)) for i=1,(n-1).
c
c  written by ron shepard 23-july-87.
c  based on "numerical recipes, the art of scientific computing" by
c  w. h. press, b. p. plannery, s. a. teukolsky, and w. t. vetterling.
c
      implicit none
c
      integer n, indx(n)
      real*8 a(*)
c
      integer l, ir, indxt, i, j
      real*8 q
c
      l  = n / 2 + 1
      ir = n
10    continue
         if ( l .gt. 1 ) then
            l     = l - 1
            indxt = indx(l)
            q     = a(indxt)
         else
            indxt    = indx(ir)
            q        = a(indxt)
            indx(ir) = indx(1)
            ir       = ir - 1
            if ( ir .le. 1 ) then
               indx(1) = indxt
               return
            endif
         endif
         i = l
         j = l + l
20       if ( j .le. ir ) then
            if ( j .lt. ir ) then
               if ( a(indx(j)) .lt. a(indx(j+1)) ) j = j + 1
            endif
            if ( q .lt. a(indx(j)) ) then
               indx(i) = indx(j)
               i       = j
               j       = j + j
            else
               j = ir + 1
            endif
            go to 20
         endif
         indx(i) = indxt
      go to 10
c
      end
