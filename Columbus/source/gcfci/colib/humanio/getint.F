!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine getint (prompt,inum,default)
c     # reads an integer inum from the keyboard with prompt and default
c     # if default.eq.'$', 1000000 is returned on <cr>
c     # if default.eq.' ', there is no default.  reprompt on <cr>.
      character*(*) prompt, default
      character*16 string
9     if (default.eq.' ') then
10       continue
#if defined(F90)
         write (6,110,advance='no') prompt
110      format(1x,a)
#else 
         write (6,110) prompt
#  if defined(SFORMAT)
110      format (' ',a,' ',$)
#  else 
110      format (' ',a)
#  endif 
#endif 
         read (5,'(a)') string
         write(13,'(a)') string
         if (string.eq.' ') goto 10
      elseif (default.eq.'$') then
         write (6,110) prompt
         read (5,'(a)') string
         write(13,'(a)') string
         if (string.eq.' ') string='1000000'
      else
#if defined(F90)
         write (6,"(1x,a,' <',a,'> ')",advance='no') prompt, default
#else 
         write (6,100) prompt, default
#  if defined(SFORMAT)
100      format (' ',a,' <',a,'> ',$)
#  else 
100      format (' ',a,' <',a,'>')
#  endif 
#endif 
         read (5,'(a)') string
         write(13,'(a)') string
         if (string.eq.' ') string=default
      endif
      read (string,'(bn,i16)',err=9) inum
      return
      end
