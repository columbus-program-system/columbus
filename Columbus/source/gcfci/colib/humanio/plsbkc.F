!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine plsbkc(
     & gtitle, z, nblk, btitle, nrow, labr, ifmt, nlist )
c
c  print a lower-triangular subblocked matrix with row labels.
c
c  input:
c  gtitle    = general character title.
c  z(*)      = subblocked matrix to be printed.
c  nblk      = number of subblocks in the matrix z(*).
c  btitle(*) = specific character title of each subblock.
c  nrow(*)   = number of rows in each block.
c  labr(*)   = character*8 row labels.
c  ifmt      = format type (1:f, 2:e, 3:g).
c  nlist     = output unit number.
c
c  18-oct-90 plblks() modified. btitle(*),labr(*) change. -rls
c  ron shepard 17-may-84.
c
      implicit none
c
      integer nblk, ifmt, nlist
      character*(*) gtitle, btitle(1:nblk), labr(*)
      integer nrow(nblk)
      real*8 z(*)
c
      integer ipt, zpt, i, nr
c
      ipt = 1
      zpt = 1
      do 100 i = 1, nblk
         write(nlist,6010) gtitle, btitle(i), i
         nr = nrow(i)
         if ( nr .gt. 0 ) then
            call plblkc(' ', z(zpt), nr, labr(ipt), ifmt, nlist )
         endif
         ipt = ipt + nr
         zpt = zpt + ( nr * ( nr + 1 ) ) / 2
100   continue
      return
6010  format(/10x,a,1x,a,' block',i4)
      end
