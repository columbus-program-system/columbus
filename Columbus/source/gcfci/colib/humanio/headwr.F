!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine headwr(unit, program, version)

c  print out a banner with the program name and version information.
c
c  input:
c  unit    = output unit number.
c  program = the name of the calling program as a character string.
c  version = the version of the calling program as a character string.
c The program and version arguments will be output in a field of
c width 10, see CHARACTER_SIZE below.  Shorter actual arguments
c will be space filled; longer actual arguments will be truncated.

      implicit none

      integer unit
      character*(*) program, version

c Note CHARACTER_SIZE should be but is not used in the format statement.
      integer     CHARACTER_SIZE
      parameter ( CHARACTER_SIZE = 10 )
      character*(CHARACTER_SIZE) p, v, distribution

      p = program
      v = version
      distribution = '6.0 beta4'

c Note the 10's in the format statement are CHARACTER_SIZE.
      write(unit, 100) p, v, distribution
 100  format(//,
     .  5x, '********************************************'/
     .  5x, '**    PROGRAM:             ', 1x, a10, 4x, '**'/
     .  5x, '**    PROGRAM VERSION:     ', 1x, a10, 4x, '**'/
     .  5x, '**    DISTRIBUTION VERSION:', 1x, a10, 4x, '**'/
     .  5x, '********************************************'/)

      return
      end
