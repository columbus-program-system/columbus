!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine who2c( prognm, nlist )
c
c  print out a short banner containing support contact info.
c
c  input:
c  prognm = left-justified character string containing the
c           name of the calling program.
c  nlist  = output unit number for the listing file.
c
c  07-mar-2005 eliminated site specific emissions by srb.
c  14-feb-1991 written by rls/eas/rmp.
c
      implicit none

      character*(*) prognm
      integer nlist

      write (nlist, '(/a,a,a,/a,a,/a,/a)')
     .    ' This Version of Program ', prognm, ' is Supported by',
     .    ' the Columbus mailing list: ', ' columbus@osc.edu.',
     .    ' For subscription and other list information, see:',
     .    ' http://email.osc.edu/mailman/listinfo/columbus'

      return

      end
