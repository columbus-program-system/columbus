!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine expnds( n, a, b )
      implicit none
c
c     expand the symmetric, lower-triangular packed, matrix a(*)
c     into b(*,*).
c
      integer n
      real*8 a(*), b(n,n)
c
      integer i, j, ii
      real*8 xx
c     19-nov-89  carry-around scalar, ii, eliminated. -rls
c
      b(1,1) = a(1)
c
cdir$  ivdep
c$doit ivdep  #titan
cvd$   ivdep
*vocl loop,novrec
c
      do 20 i = 2, n
         ii = (i*(i-1))/2
c
cdir$    ivdep
c$doit   ivdep  #titan
cvd$     ivdep
*vocl loop,novrec
c
         do 10 j = 1, i
            xx     = a(ii+j)
            b(i,j) = xx
            b(j,i) = xx
10       continue
20    continue
c
      return
      end
