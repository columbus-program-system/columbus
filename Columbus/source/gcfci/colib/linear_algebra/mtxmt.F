!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine mtxmt(a,natr,b,nbtr,c,ncc)
c
c  to compute c = a(transpose) * b(transpose)
c
c  input:
c  a(*),b(*) = input matrices.
c  natr = number of rows in a(transpose)(*) and c(*).
c  nbtr = number of columns of a(transpose)(*) and
c         rows of b(transpose)(*).
c  ncc = number of columns of b(transpose)(*) and c(*).
c        all dimensions must be > 0.
c
c  output:
c  c(*) = a(transpose) * b(transpose) product matrix.
c
c  25-may-89 interface to ibm-essl dgemul(). -rls
c  written by ron shepard.
c
      implicit none
      integer natr, nbtr, ncc
      real*8 a(nbtr,natr), b(ncc,nbtr), c(natr,ncc)
c
      integer i, j, k
      real*8 aki, sum
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
c
      real*8   ddot
      external ddot
c
#if defined(FUJITSU)
c     # 14-may-92 fujitsu code of Ross Nobes and Roger Edberg
c     #           merged. -rls
      integer    maxvl
      parameter( maxvl = 512 )
      integer imax, istart, kextra
      real*8  b0, b1, b2, b3
      kextra = mod( nbtr, 4 )
c
      do 20 j = 1, ncc
         do 10 i = 1, natr
            c(i,j) = zero
10       continue
20    continue
c
         do 50 istart = 1, natr, maxvl
            imax = min( istart+maxvl-1, natr )
            do 30 k = 1, kextra
               do 30 j = 1, ncc
                  b0 = b(j,k)
*vocl loop,repeat(maxvl)
                  do 30 i = istart, imax
                     c(i,j) = c(i,j) + b0 * a(k,i)
30          continue
           do 40 k = kextra+1, nbtr, 4
              do 40 j = 1, ncc
                 b0 = b(j,k)
                 b1 = b(j,k+1)
                 b2 = b(j,k+2)
                 b3 = b(j,k+3)
*vocl loop,repeat(maxvl)
                 do 40 i = istart, imax
                    c(i,j) = c(i,j) + b0 * a(k,i)
     &                              + b1 * a(k+1,i)
     &                              + b2 * a(k+2,i)
     &                              + b3 * a(k+3,i)
40         continue
50       continue
c
#elif defined(CRAY)
c     # interface to the library mxma().
      call mxma(a,nbtr,1,  b,ncc,1,  c,1,natr,  natr,nbtr,ncc)
#elif defined(BLAS3)
c  25-may-89 interface to level-3 blas. -rls
      call dgemm('t','t',natr,ncc,nbtr,one,a,nbtr,b,ncc,zero,c,natr)
#elif (defined(IBM) && defined(VECTOR)) || defined(ESSL)
c  interface to the ibm-essl routime dgemul().
      call dgemul(a,nbtr,'t',  b,ncc,'t',  c,natr,  natr,nbtr,ncc)
#else 
c
c  all fortran version.
c  note: *** this code should not be modified ***
c
c  for this matrix operation, it is not possible to have sequential
c  memory accesses to all three matrices.  if this is important, then
c  this routine should partition the output matrix, and accumulate the
c  results for each partition into a sequentially accessed intermediate
c  array, dumping the results to c(*) as necessary.  note also that
c  c(transpose)=b*a.  the results could also be computed using mxm()
c  and explicitly transposed (if necessary) by the calling routine.
c  -rls
c
      do 20 j = 1, ncc
         do 10 i = 1, natr
            c(i,j) = zero
10       continue
20    continue
      do 50 i = 1, natr
         do 40 k = 1, nbtr
            aki = a(k,i)
            if ( aki .ne. zero ) then
               do 30 j = 1, ncc
                  c(i,j) = c(i,j) + aki * b(j,k)
30             continue
            endif
40       continue
50    continue
#endif 
      return
      end
