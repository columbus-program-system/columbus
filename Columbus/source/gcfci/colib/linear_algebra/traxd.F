!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      function traxd( n, a, d )
c
c  compute matrix_trace( a * d ).
c
c  input:
c  n = matrix dimension.
c  a(*) = symmetric matrix stored lower-triangular packed.
c  d(*) = symmetric matrix stored lower-triangular packed.
c
c  output:
c  result = tr( a * d )
c
c  08-oct-90 (columbus day) written by ron shepard.
c
      implicit integer(a-z)
c
      integer n
      real*8 traxd
      real*8 a(*), d(*)
c
      integer pq, p, q
      real*8 tempd, tempnd
c
      real*8     zero
      parameter( zero=0d0 )
c
c     # diagonal and nondiagonal terms are accumulated separately.
c
      tempd  = a(1) * d(1)
      tempnd = zero
      pq     = 1
      do 20 p = 2, n
         do 10 q = 1, (p-1)
            tempnd = tempnd + a( pq + q ) * d( pq + q )
10       continue
         pq = pq + p
         tempd = tempd + a(pq) * d(pq)
20    continue
c
      traxd = tempd + tempnd + tempnd
c
      return
      end
