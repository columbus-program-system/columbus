!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      function indot( len,   x, indexx,   y, indexy )
c
c  this function computes the dot product of 2 indexed vectors.
c
c     len       : length of in index vectors. (len>0)
c     x(*)      : x vector.
c     y(*)      : y vector.
c     indexx(*) : list of x locations.
c                 the elements of indexx(*) must be distinct.
c     indexy(*) : list of y locations.
c                 the elements of indexy(*) must be distinct.
c
c  30-nov-90 written by eric stahlberg.
c
      real*8 indot
      integer len, indexx(len), indexy(len)
      real*8 x(*), y(*)
c
      integer i
      real*8    zero,      dotpr
      parameter(zero=0.0d0)

c
      dotpr = zero
c
cdir$  ivdep
c$doit ivdep
cvd$   permutation (indexx, indexy)
*vocl loop,novrec
c
      do 10 i = 1, len
         dotpr = dotpr + x(indexx(i)) * y(indexy(i))
10    continue
c
      indot = dotpr
c
      return
      end
