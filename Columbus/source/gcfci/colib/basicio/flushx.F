!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine flushx( iunit )
c
c  flush any file buffers associated with fortran unit "iunit".
c
c  this routine is used primarily for flushing listing files
c  during iterative procedures.
c
c  06-may-92 written by ron shepard.
c
      implicit none
c
      integer iunit
c
#if defined(CRAY) || defined(T3E64) || defined(T3D)
c     # must include ierr to avoid aborts.
      integer ierr
      call flush( iunit, ierr )
#elif defined(RS6000) && (!defined(EXTNAME))
c     # must access the fortran library routine, with the underscore,
c     # not the C library function, without the underscore.
      call flush_( iunit )
#elif defined(SUN) || defined(SGIPOWER) || defined(RS6000) || defined(MACABSOFT)
      call flush( iunit )
#else 
c     # no-op call.
      continue
#endif 
c
      return
      end
