!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine moread(
     & unit,   flcod,  filerr, syserr,
     & mxtitl, ntitle, titles, afmt,
     & nsym,   nbfpsy, nmopsy, labels,
     & clen,   c  )
c
c subroutine: moread
c purpose   : universal interface read of mo coefficient file
c author    : eric stahlberg, osu chemistry (may 1989)
c
c version   : 2.1  (feb 1990)
c
c variable definitions
c
c *** in variables ***
c unit  : unit number of mo file
c flcod: file read operation code
c           10=read header only
c           20=read after header mocoef field
c           30=read after header energy field
c           40=read after header orbocc field
c           + block number to be read (0 means all blocks)
c         fopcod lt 10 implies all blocks and that fopcod
c mxtitl: maximum number of title cards which may be read
c clen  : available space in array c
c *** out variables ***
c filerr: mo file error number
c syserr: i/o system error - if filerr eq 1
c ntitle: number of title cards read
c titles: actual title cards
c afmt  : format with which values were read
c nsym  : number of irreps in point group
c nbfpsy: number of basis functions per mo of each irrep
c nmopsy: number of mos per irrep
c labels: symmetry labels for each irrep
c c     : coefficient array space
c
      implicit none
c
      integer unit, flcod, filerr, syserr, mxtitl, ntitle, nsym
      integer nbfpsy(*), nmopsy(*), clen
      real*8 c(*)
      character*80 titles(*)
      character*80 afmt
      character*4 labels(*)
c
c     # local variables.
c
      integer totspc, index, iversn, i, isym, imo, ifld
      integer symblk, fopcod
      logical ldio
      real*8 cdummy
      character*6 inname, field(4), fldfmt, flname, inline
c
c     # abelian point groups only.
c
      integer    maxsym
      parameter (maxsym=8)
c
c     # latest file version
c
      integer    lstver
      parameter (lstver=2)
c
c     # last valid fop code.
c
      integer    lstfop
      parameter (lstfop=4)
c
      real*8     zero
      parameter( zero=0d0 )
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # initialize some static constants.
c
      flname = 'mocoef'
      call allcap(flname)
c
      field(1) = 'header'
      field(2) = 'mocoef'
      field(3) = 'energy'
      field(4) = 'orbocc'
      do 10 ifld = 1, 4
         call allcap(field(ifld))
10    continue
      fldfmt  = ' (a6) '
c
c     # parse out symmetry block and real fopcod from fopcod.
c
      if (flcod.ge.10) then
         symblk = flcod - (flcod/10)*10
         fopcod = (flcod-symblk) / 10
      else
         fopcod = flcod
         symblk = 0
      endif
c
c     # check fopcod for valid value.
c
      if ((fopcod.gt.lstfop).or.(fopcod.lt.1)) then
         filerr=-5
         goto 1000
      endif
c
c     # check symblk for valid value.
c
      if ((symblk.gt.maxsym).or.(symblk.lt.0)) then
         filerr=-7
         goto 1000
      endif
c
c
c     # initialize state (assume success).
c
      filerr = 0
      syserr = 0
      ldio   = .false.
c
c     # address to begin filling retrieved data into c(:).
c
      index=0
c
c     # rewind file.
c
      rewind (unit)
c
c     # read initial line for parsing.
c
      read (unit,5001,iostat=syserr) iversn, inname
      if (syserr.ne.0) goto 1100
5001  format(i2,a6)
      call allcap(inname)
c
c     # (optional checks on file version number go here)
c
      if ( (iversn .gt. lstver)
     & .or. (inname .ne. flname) ) then
         filerr=-4
         goto 1000
      endif
c
c     # read field 1, header.
c
      read (unit,fmt=fldfmt,iostat=syserr) inline
      if (syserr.ne.0) goto 1100
      call allcap(inline)
      if (inline.ne.field(1)) then
         filerr=-14
         goto 1000
      endif
c
c     # read in title information.
c
      read (unit,*,iostat=syserr) ntitle
      if (syserr.ne.0) goto 1100
      if (ntitle.gt.mxtitl) then
         filerr=-2
         goto 1000
      endif
      do 100 i=1,ntitle
         read (unit,5002,iostat=syserr) titles(i)
100   continue
      if (syserr.ne.0) goto 1100
5002  format (a80)
c
c     # read in symmetry information (dimensions).
c
      read (unit,*,iostat=syserr) nsym
      if (syserr.ne.0) goto 1100
      if (symblk.gt.nsym) then
         filerr=-8
         goto 1000
      endif
      if ((nsym.gt.maxsym).or.(nsym.lt.1)) then
         filerr=-3
         goto 1000
      endif
c
c     # read in nbfpsy() and nmopsy().
c
      read (unit,*,iostat=syserr) (nbfpsy(i),i=1,nsym),
     + (nmopsy(i),i=1,nsym)
      if (syserr.ne.0) goto 1100
c
c     # check for adequate space.
c
      totspc = 0
      do 140 isym=1,nsym
c
c        # check to see if block is to be read.
c
         if (symblk.eq.0.or.isym.eq.symblk) then
            if (fopcod.le.2) then
               totspc=totspc+nmopsy(isym)*nbfpsy(isym)
            else
               totspc=totspc+nmopsy(isym)
            endif
         endif
140   continue
      if ((totspc.gt.clen).and.(fopcod.gt.1)) then
         filerr=-1
         goto 1000
      endif
c
c     # clear out c vector before reading data.
c
      do 141 i=1,clen
         c(i) = zero
141   continue
c
c     # read in label information
c
      read (unit,5003,iostat=syserr)(labels(i),i=1,nsym)
      if (syserr.ne.0) goto 1100
5003  format(8(a4,1x))
c
c     # test for symmetry and title read only and return if necessary.
c
      if (fopcod.eq.1) return
c
c     # loop until to get to next field
c     # drop out of loop if field is reached or eof detected
c
130   read (unit,fldfmt,end=137,iostat=syserr) inline
      if (syserr.ne.0) goto 1100
      call allcap(inline)
      if (inline.eq.field(fopcod)) goto 135
      goto 130
c
c     # end of file detected before field is matched.
c
137   continue
      filerr=-6
      goto 1000
c
c     # end of until loop.
135   continue
c
c     # field is matched, begin reading in c() at position index.
c
      read (unit,5004,iostat=syserr) afmt
5004  format(a80)
      if (syserr.ne.0) goto 1100
c
c     # parse format designation.
c
      if (afmt.eq.'(*)') then
         ldio=.true.
      else
         ldio=.false.
      endif
c
c     # read as coefficients only if fopcod eq 2.
c
      if (fopcod.eq.2) then
c
c        # read in coefficients
c
         do 110 isym=1,nsym
            do 120 imo=1,nmopsy(isym)
c
c              # read block if it is to be read.
c
               if (isym.eq.symblk.or.symblk.eq.0) then
                  if (ldio) then
                     read (unit,*,iostat=syserr)
     +                (c(index+i),i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD1'
                  else
                     read (unit,fmt=afmt,iostat=syserr)
     +                (c(index+i),i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD2'
                  endif
                  if (syserr.ne.0) goto 1100
                  index=index+nbfpsy(isym)
c                  write(*,*) 'MOREAD3'
               else
c
c                 # skip values which are not to be read.
c
                  if (ldio) then
                     read (unit,*,iostat=syserr)
     +                (cdummy,i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD4'
                  else
                     read (unit,fmt=afmt,iostat=syserr)
     +                (cdummy,i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD5'
                  endif
                  if (syserr.ne.0) goto 1100
c                  write(*,*) 'MOREAD6'
               endif
120         continue
110      continue
c
      elseif ((fopcod.eq.3).or.(fopcod.eq.4)) then
c
c        # read in other fields for fopcod 3,4.
c
         do 150 isym=1,nsym
            if (symblk.eq.0.or.symblk.eq.isym) then
c
c              # read if block is to be read.
c
               if (ldio) then
                  read (unit,*,iostat=syserr)
     +             (c(index+i),i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD7'
               else
                  read (unit,fmt=afmt,iostat=syserr)
     +             (c(index+i),i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD8'
               endif
               if (syserr.ne.0) goto 1100
               index=index+nmopsy(isym)
c                  write(*,*) 'MOREAD9'
            else
c
c              # skip values.
c
               if (ldio) then
                  read (unit,*,iostat=syserr)
     +             (cdummy,i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD10'
               else
                  read (unit,fmt=afmt,iostat=syserr)
     +             (cdummy,i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD11'
               endif
               if (syserr.ne.0) goto 1100
c                  write(*,*) 'MOREAD12'
            endif
150      continue
      endif
c
      return
c
c     # error handling portion.
c
c     # system errors.
1100  continue
      call bummer('moread: system file error',syserr,wrnerr)
      filerr=1
      return
c
c     # file errors.
1000  continue
      return
c
      end
