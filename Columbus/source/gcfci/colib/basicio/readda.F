!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine readda( unit, record, buffer, buflen )
c
c----------------------------------------------------------
c this is a primitive i/o routine to be used by all higher
c level routines requiring i/o of this type. machine i/o
c peculiarites are meant to reside in these routines only.
c----------------------------------------------------------
c
c routine name: readda
c version: 1.2                        date: 12/17/90
c author: eric stahlberg - ohio state university
c purpose: this routine will read a given vector from a
c          direct access file. the length of the vector is given
c          in minimum necessary real*8 words to access the entire
c          vector. io errors reported in encoded form: ioerr*100+unit
c parameters:
c     unit:   unit number of file to be read
c     record: record number to be read
c     buffer: buffer to read record into
c     buflen: number of real*8 words to read from record
c
      implicit integer(a-z)
c
ctm
c     mapunit(origunit,sequenceunits)
c     reqperunit (origunit)
      integer maxunits,maxsequnits
      parameter (maxunits=99,maxsequnits=4)
      integer mapunit(maxunits,maxsequnits)
      integer recperunit(maxunits),firstfreebl
      character*30 filename(maxunits)
      common /mapping/ mapunit,recperunit,firstfreebl,filename
ctm
      logical unitopen
      integer unit,record,buflen
      real*8 buffer ( buflen )
#if defined(IBMMVS)
c these variables help in the logical record to physical
c record reduction
c     index*:  loop counter
c     nphyrc:  number of complete physical records
c     ibmlen:  real*8 words in 1/2 track of 3380 device
c     phyrec:  physical record buffer
c     phyrcn:  physical record number for this logical record
      integer index,index2,nphyrc,ibmlen,phyrcn
      parameter (ibmlen=2932)
      real*8 phyrec (ibmlen)
#endif 
c
c common block for unit information
      integer         mxunit
      parameter       (mxunit=99)
c recnum retains next record to be accessed
c reclen retains record length in open
c seqwrt retains true if sequential write is to be enforced
      integer         recnum(mxunit)
      integer         reclen(mxunit)
      logical         seqwrt(mxunit)
      common  /esio88/   recnum, reclen, seqwrt
      save    /esio88/
c
c     # bummer error types
      integer wrnerr, nfterr, faterr
      parameter (wrnerr=0, nfterr=1, faterr=2)
c
      integer ioerr
      character*40 filnamn
c
c
c
c perform error checking
c
      if (unit.gt.mxunit.or.unit.le.0) goto 910
      if (buflen.ne.reclen(unit)) goto 920
      if (seqwrt(unit)) goto 930
#if defined(IBMMVS)
       if (reclen(unit).gt.ibmlen) then
c recreate da logical record from smaller physical records
c number of complete physical records in the logical record
         nphyrc=buflen/ibmlen
c point to first physical record for this logical record
         if (nphyrc*ibmlen.eq.buflen) then
            phyrcn=(record-1)*nphyrc+1
         else
            phyrcn=(record-1)*(nphyrc+1)+1
         endif
c read complete records
         do 10 index=1,nphyrc
c
c read the physical record
c
         read (unit, rec = phyrcn+index-1, iostat=ioerr, err=900 )
     +            phyrec
c
c copy physical record to buffer
c
            do 20 index2=1,ibmlen
 20           buffer((index-1)*ibmlen+index2)=phyrec(index2)
 10     continue
c
c check for remainder of record
c
         if (nphyrc*ibmlen.ne.buflen) then
            read (unit,rec=phyrcn+nphyrc,
     +         iostat=ioerr, err=900 ) phyrec
            do 30 index2=1,ibmlen
 30           buffer(buflen-ibmlen+index2)=phyrec(index2)
         endif
       else
         read (unit,rec=record) buffer
       endif
#elif defined(IBMCMS)
c ibmcms quirk: length of data xferred counted in 4 byte words
c
      len2=2*buflen
      ipos=(record-1)*len2+1
      call rwfil (unit, 1, ipos, buffer, len2, ierr)
      if (ierr.ne.0) then
        call bummer('readda: from rwfil, (ierr*100+unit)=',
     +  (ierr*100+unit),faterr)
      endif
#else 
ctm
c     map from unit to correct sequnit

      rectmp = (record-1)/recperunit(unit) +1
      if (rectmp.gt.maxsequnits)
     . call bummer('could not open more files',rectmp,2)
      unitnew= mapunit(unit,rectmp )
      inquire(unit=unitnew,opened=unitopen)
      if (.not. unitopen) then
c      must open old file
#  if defined(DECALPHA) || defined(SGIPOWER)
      lrecl=2*buflen
#  else 
       lrecl=8*buflen
#  endif 
       write(filnamn,'(a,a1,i1)')
     .  filename(unit)(1:fstrlen(filename(unit)))
     .      ,'_',rectmp
       if (firstfreebl.eq.99)
     .  call bummer('no more unit numbers available',firstfreebl,2)
       write(6,*) 'readda: opening file ',filnamn,' unit=',unitnew
       open(unit=unitnew,access='direct',form='unformatted',status=
     +  'unknown',file=filnamn,iostat=ioerr,recl=lrecl,err=900)
      endif
      recnew = record - (rectmp-1)*recperunit(unit)
      read (unitnew, rec = recnew ,iostat=ioerr, err=901) buffer
#endif 
c point to next record
 902  recnum(unit)=record+1
c
      return
 899  format(' current reclen=',i10/
     & ' opened reclen=',i10/' recnum=',i10)
c
c error conditions
 900  continue
      write(*,*) 'readda: opening file, unit ',unit, ' named ',
     & filename(unit)
      write(*,899) reclen(unit),buflen,record
      call bummer('readda: i/o error,  iostat =',
     & ioerr, faterr )
 901  continue
      write(*,*) 'readda: problem reading unit ',unit, ' named ',
     & filename(unit)
      write(*,899) reclen(unit),buflen,record
      call bummer('readda: i/o error,  iostat =',
     & ioerr, faterr )
 910  continue
      call bummer('readda: invalid unit number ',unit,faterr)
 920  continue
      write(*,899) reclen(unit),buflen,record
      call bummer('readda: buffer length gt record length ',unit,faterr)
 930  continue
      write(*,899) reclen(unit),buflen,record
      call bummer('readda: unit opened for seq. write',unit,faterr)
      end
