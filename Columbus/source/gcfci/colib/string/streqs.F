!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      logical function streqs( str1, str2 )
c
c  compare short string one to the initial characters of str two
c
      character*(*)     str1
      character*(*)     str2
c
c  --the strings are "equal" if the leading characters of the (possibly)
c    longer second string equal the shorter first string
c  --originally the two strings were treated equivalently.  while this
c    feels good, i needed the current behavior
c  --trailing blanks are ignored
c
      integer           strl
c
      external          streq
      logical           streq
      external          fstrlen
      integer           fstrlen
c
c
      strl = min( fstrlen(str1), len(str2) )
c
      streqs = streq( str1, str2(1:strl) )
c
      return
      end
