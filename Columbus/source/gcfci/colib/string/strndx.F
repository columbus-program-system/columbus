!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      integer function strndx ( number, target, list, inc )
      implicit integer  ( a-z )
c
c     function strndx returns the element number (index) in
c     array 'list' containing the value 'target' using streq (case
c     insenstive comparison) for equality check
c
      integer           number, inc
      character*(*)     list( inc, number ), target
c
      integer           index
c
      external          streq
      logical           streq
c
c
c     # assume target will not be found.
c
      strndx = 0
c
      do 100 index = 1, number
         if ( streq( list( 1, index ), target ) ) then
c
c           # target found.
c
            strndx = index
            return
         endif
 100  continue
c
c     # target not found
c
      return
      end
