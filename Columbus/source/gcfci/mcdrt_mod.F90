!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
module mcdrt_mod

   use symmetry_mod

   type b_vec_pt  ! set up for jagged array addressing
      integer              :: bmin, bmax
      integer, allocatable :: b(:)
   end type b_vec_pt

   type c_vec_pt  ! set up for jagged array addressing
      integer                     :: cmin, cmax
      type(b_vec_pt), allocatable :: c(:)
   endtype c_vec_pt

   type a_vec_pt  ! set up for jagged array addressing
      integer                     :: amin, amax
      type(b_vec_pt), allocatable :: a(:)
   endtype a_vec_pt

   type mc_drow  ! individual distinct row (a single node of a shavitt graph)
      integer :: a
      integer :: b
      integer :: c
      integer :: level
      integer :: k(0:3)           ! downward chaining indices
      integer :: l(0:3)           ! upward chaining indices
      integer :: y(nsym_max,0:3)  ! arc weights (bottom indexed)
      integer :: x(nsym_max)      ! lower walk count
      integer :: xbar(nsym_max)   ! upper walk count
      integer :: z(nsym_max)      ! lowest reverse-index walk
      logical :: qx(nsym_max)     ! logical lower walk flag
      logical :: qxbar(nsym_max)  ! logical upperwalk flag.
   end type mc_drow

   type mcdrt
      integer :: nsym    ! actual number of symmetry irreps
      integer :: nrow    ! actual number of distinct rows
      integer :: ndot    ! number of doubly occupied orbitals
      integer :: nact    ! number of active orbitals
      integer :: nlevel  ! number of levels in the drt
      integer :: nwalk   ! total number of walks
      integer :: ncsf    ! number of selected CSFs
      integer :: ssym    ! state symmetry for this drt
      integer :: smult   ! spin multiplicity for this drt
      integer :: nelt    ! total number of electrons
      integer :: nela    ! number of active electrons
      integer :: version ! external file format version
      integer :: lenbuf  ! external file buffer length
      integer :: nhdint  ! external file header length

      type(mc_drow), allocatable :: row(:)  ! this, and the following arrays, are really just allocatable

      integer, allocatable :: nj(:)          ! number of rows at each level.
      integer, allocatable :: njskp(:)       ! row offsets at each level.
      integer, allocatable :: doub(:)        ! doubly occupied orbitals.
      integer, allocatable :: symd(:)        ! doubly occupied orbital symmetry.
      integer, allocatable :: active(:)      ! active orbitals, symmetry-reduced index, indexed by level.
      integer, allocatable :: levsym(:)      ! active orbital symmetry, indexed by level.
      integer, allocatable :: limvec(:)      ! walk selection vector.
      integer, allocatable :: r(:)           ! reverse-to-forward indexing vector.
      character(len=4) :: slabel(0:nsym_max) ! symmetry labels.
      character(len=80) :: title             ! drt title.
      integer :: stepsym(0:3,1:nsym_max) = reshape( (/ &     ! step symmetry
           & 1,1,1,1, &   ! steps 0 and 3 are always totally symmetric.
           & 1,2,2,1, &   ! steps 1 and 2 have the symmetry of the corresponding orbital.
           & 1,3,3,1, &   ! this is a constant array, but parameters are not allowed in derived types.
           & 1,4,4,1, &
           & 1,5,5,1, &
           & 1,6,6,1, &
           & 1,7,7,1, &
           & 1,8,8,1  /), (/4,nsym_max/) )

      type(c_vec_pt), allocatable :: node_a(:)    ! referenced as node_a(a)%c(c)%b(b)

      type(a_vec_pt), allocatable :: node_lev(:)  ! referenced as node_lev(ilev)%a(a)%b(b)

   end type mcdrt

contains

   subroutine read_mcdrt( ndrt, drt )

      ! read an mcscf drt from an external file

      implicit none

      integer, intent(in) :: ndrt
      type(mcdrt) :: drt

      call read_mcdrt_header( ndrt, drt )  ! read the header info.

      call init_mult( drt%nsym )  ! set mult(:,:) for the remaining steps.

      call allocate_mcdrt( drt )  ! allocate all drt arrays on the external file.

      call read_mcdrt_data (ndrt, drt ) ! read/compute arrays from the external file.

      return

   end subroutine read_mcdrt

   subroutine read_mcdrt_header( ndrt, drt )

      ! read the header info for an mcscf drt from an external file.
      ! the file must be positioned correctly before this routine is called.

      use drt_util_mod
      use bummer_mod

      implicit none

      integer, intent(in) :: ndrt
      type(mcdrt), intent(inout) :: drt

      integer :: buf3(3)
      integer, allocatable :: buf(:)

      read(ndrt,'(a)') drt%title

      call read_biv( ndrt, 3, 3, buf3 )
      drt%version = buf3(1)
      drt%lenbuf  = buf3(2)
      drt%nhdint  = buf3(3)

      allocate( buf(drt%nhdint) )

      select case( drt%version )
      case(2)
         call read_biv( ndrt, drt%nhdint, drt%nhdint, buf )
         drt%ndot   = buf(1)
         drt%nact   = buf(2)
         drt%nsym   = buf(3)
         drt%nrow   = buf(4)
         drt%ssym   = buf(5)
         drt%smult  = buf(6)
         drt%nelt   = buf(7)
         drt%nela   = buf(8)
         drt%nwalk  = buf(9)
         drt%ncsf   = buf(10)
         drt%nlevel = drt%nact + 1
      case(3)
         call read_biv( ndrt, drt%nhdint, drt%nhdint, buf )
         drt%ndot   = buf(1)
         drt%nact   = buf(2)
         drt%nsym   = buf(3)
         drt%nrow   = buf(4)
         drt%ssym   = -1
         drt%smult  = buf(5)
         drt%nelt   = buf(6)
         drt%nela   = buf(7)
         drt%nwalk  = -1
         drt%ncsf   = -1
         drt%nlevel = drt%nact + 1
      case default
         call bummer( 'illegal mcdrt version=', drt%version, fatal_err)
      end select

      deallocate ( buf )

      return

   end subroutine read_mcdrt_header

   subroutine allocate_mcdrt( drt )

      ! allocate all of the arrays for an mcscf drt

      implicit none

      type(mcdrt), intent(inout) :: drt

      allocate( drt%row(drt%nrow) )
      allocate( drt%nj(0:drt%nact) )
      allocate( drt%njskp(0:drt%nact) )
      allocate( drt%doub(drt%ndot) )
      allocate( drt%symd(drt%ndot) )
      allocate( drt%active(0:drt%nact) )
      allocate( drt%levsym(0:drt%nact) )
      if ( drt%version .eq. 2 ) then
         allocate( drt%limvec(drt%nwalk) )
         allocate( drt%r(drt%nwalk) )
      endif

      return

   end subroutine allocate_mcdrt

   subroutine read_mcdrt_data( ndrt, drt )
      use drt_util_mod

      use bummer_mod
      implicit none

      integer, intent(in)        :: ndrt
      type(mcdrt), intent(inout) :: drt

      select case(drt%version)
      case(2)
         call read_mcdrt_data_2( ndrt, drt )
      case(3)
         call read_mcdrt_data_3( ndrt, drt )
      case default
         call bummer( 'unknown mcdrt version=', drt%version, fatal_err )
      end select

      return

   end subroutine read_mcdrt_data

   subroutine read_mcdrt_data_2( ndrt, drt )

      ! read the drt arrays for an mcscf drt from an external file.
      ! the external file should be positioned correctly before calling this subroutine.

      ! 26-mar-2006 qx(:) and qxbar(:) added. -rls

      use drt_util_mod

      implicit none

      integer, intent(in) :: ndrt
      type(mcdrt), intent(inout) :: drt

      integer :: isym, istep, irow, bpt, lt, nleft, nread
      integer :: a, b, c, a_head, ilev, amin, amax, bmin, bmax, cmin, cmax
      integer :: buf(drt%lenbuf)

      drt%slabel(0) = ' '
      call read_bcv( ndrt, drt%nsym, drt%nsym, drt%slabel(1:) )

      call read_biv( ndrt, drt%ndot, drt%ndot, drt%doub )

      call read_biv( ndrt, drt%ndot, drt%ndot, drt%symd )

      drt%active(0) = 0
      call read_biv( ndrt, drt%nact, drt%nact, drt%active(1:) )

      drt%levsym(0) = 1        ! levsym(:) is top-indexed
      call read_biv( ndrt, drt%nact, drt%nact, drt%levsym(1:) )

      call read_biv( ndrt, drt%nlevel, drt%nlevel, drt%nj )

      call read_biv( ndrt, drt%nlevel, drt%nlevel, drt%njskp )

      call read_biv( ndrt, drt%nrow, drt%nrow, drt%row(:)%a )

      call read_biv( ndrt, drt%nrow, drt%nrow, drt%row(:)%b )

      do ilev = 0, drt%nact
         do irow = (drt%njskp(ilev)+1), (drt%njskp(ilev)+drt%nj(ilev))  ! compute additional node information.
            drt%row(irow)%level = ilev
            a                   = drt%row(irow)%a
            c                   = ilev - a - drt%row(irow)%b
            drt%row(irow)%c     = c
         enddo
      enddo

      ! allocate the leading component of the node_lev array.

      allocate( drt%node_lev(0:drt%nact) )

      drt%node_lev(:)%amin = drt%nact
      drt%node_lev(:)%amax = -1

      do irow = 1, drt%nrow  ! determine amin and amax for each level.
         a    = drt%row(irow)%a
         ilev = drt%row(irow)%level
         drt%node_lev(ilev)%amin = min( drt%node_lev(ilev)%amin, a )
         drt%node_lev(ilev)%amax = max( drt%node_lev(ilev)%amax, a )
      enddo

      do ilev = 0, drt%nact
         amin = drt%node_lev(ilev)%amin
         amax = drt%node_lev(ilev)%amax
         allocate( drt%node_lev(ilev)%a(amin:amax) )  ! allocate jagged arrays.
         drt%node_lev(ilev)%a(:)%bmin = drt%nact + 1
         drt%node_lev(ilev)%a(:)%bmax = -1
      enddo

      do irow = 1, drt%nrow  ! determine bmin and bmax for each level and a value.
         a    = drt%row(irow)%a
         b    = drt%row(irow)%b
         ilev = drt%row(irow)%level
         drt%node_lev(ilev)%a(a)%bmin = min( drt%node_lev(ilev)%a(a)%bmin, b )
         drt%node_lev(ilev)%a(a)%bmax = max( drt%node_lev(ilev)%a(a)%bmax, b )
      enddo

      do ilev = 0, drt%nact
         do a = drt%node_lev(ilev)%amin, drt%node_lev(ilev)%amax
            bmin = drt%node_lev(ilev)%a(a)%bmin
            bmax = drt%node_lev(ilev)%a(a)%bmax
            allocate( drt%node_lev(ilev)%a(a)%b(bmin:bmax) )  ! allocate jagged arrays.
            drt%node_lev(ilev)%a(a)%b(:) = 0
         enddo
      enddo

      do irow = 1, drt%nrow  ! determine bmin and bmax for each level and a value.
         a    = drt%row(irow)%a
         b    = drt%row(irow)%b
         ilev = drt%row(irow)%level
         drt%node_lev(ilev)%a(a)%b(b) = irow
      enddo

      a_head = drt%row(drt%nrow)%a
      ! allocate the leading component of the node array.
      allocate( drt%node_a(0:a_head) )

      drt%node_a(:)%cmin = drt%row(drt%nrow)%c + 1
      drt%node_a(:)%cmax = -1

      do irow = 1, drt%nrow  ! determine cmin and cmax for each a value.
         a = drt%row(irow)%a
         c = drt%row(irow)%c
         drt%node_a(a)%cmin = min( drt%node_a(a)%cmin, c )
         drt%node_a(a)%cmax = max( drt%node_a(a)%cmax, c )
      enddo

      do a = 0, a_head
         cmin = drt%node_a(a)%cmin
         cmax = drt%node_a(a)%cmax
         allocate( drt%node_a(a)%c(cmin:cmax) )  ! allocate jagged arrays.
         drt%node_a(a)%c(:)%bmin = drt%nact + 1
         drt%node_a(a)%c(:)%bmax = -1
      enddo

      do irow = 1, drt%nrow  ! determine bmin and bmax for each (a,c) pair.
         a = drt%row(irow)%a
         b = drt%row(irow)%b
         c = drt%row(irow)%c
         drt%node_a(a)%c(c)%bmin = min( drt%node_a(a)%c(c)%bmin, b )
         drt%node_a(a)%c(c)%bmax = max( drt%node_a(a)%c(c)%bmax, b )
      enddo

      do a = 0, a_head
         do c = drt%node_a(a)%cmin, drt%node_a(a)%cmax
            bmin = drt%node_a(a)%c(c)%bmin
            bmax = drt%node_a(a)%c(c)%bmax
            allocate( drt%node_a(a)%c(c)%b(bmin:bmax) )  ! allocate jagged arrays.
            drt%node_a(a)%c(c)%b(:) = 0                  ! initialize the vector entries.
         enddo
      enddo

      ! set pointers to map from (a,c,b) to irow.
      ! null entries, if any, are indicated with zeros.
      do irow = 1, drt%nrow
         a = drt%row(irow)%a
         b = drt%row(irow)%b
         c = drt%row(irow)%c
         drt%node_a(a)%c(c)%b(b) = irow
      enddo

      ! row(:)%l(:)
      nleft = 4 * drt%nrow
      bpt = drt%lenbuf
      do irow = 1, drt%nrow
         do istep = 0, 3
            if ( bpt .eq. drt%lenbuf ) then
               nread = min( nleft, drt%lenbuf )
               call read_biv( ndrt, drt%lenbuf, nread, buf )
               bpt = 0
            endif
            bpt = bpt + 1
            drt%row(irow)%l(istep) = buf(bpt)
            nleft = nleft - 1
         enddo
      enddo

      ! compute row(:)%k(:) from row(:)%l(:)
      forall(irow=1:drt%nrow) drt%row(irow)%k = 0 ! (:)
      do irow = 1, drt%nrow
         do istep = 0, 3
            lt = drt%row(irow)%l(istep)
            if ( lt .ne. 0 ) drt%row(lt)%k(istep) = irow
         enddo
      enddo

      ! row(:)%y(:,:)
      nleft = 4 * drt%nsym * drt%nrow
      bpt = drt%lenbuf
      do irow = 1, drt%nrow
         do istep = 0, 3
            do isym = 1, drt%nsym
               if ( bpt .eq. drt%lenbuf ) then
                  nread = min( nleft, drt%lenbuf )
                  call read_biv( ndrt, drt%lenbuf, nread, buf )
                  bpt = 0
               endif
               bpt = bpt + 1
               drt%row(irow)%y(isym,istep) = buf(bpt)
               nleft = nleft - 1
            enddo
         enddo
      enddo

      ! row(:)%xbar(:) and row(:)%qxbar(:)
      nleft = drt%nsym * drt%nrow
      bpt = drt%lenbuf
      do irow = 1, drt%nrow
         do isym = 1, drt%nsym
            if ( bpt .eq. drt%lenbuf ) then
               nread = min( nleft, drt%lenbuf )
               call read_biv( ndrt, drt%lenbuf, nread, buf )
               bpt = 0
            endif
            bpt = bpt + 1
            drt%row(irow)%xbar(isym)  = buf(bpt)
            nleft = nleft - 1
         enddo
         drt%row(irow)%qxbar(:) = drt%row(irow)%xbar(:) .ne. 0  ! this assumes xbar(:) does not overflow.
      enddo

      ! row(:)%x(:)
      nleft = drt%nsym * drt%nrow
      bpt = drt%lenbuf
      do irow = 1, drt%nrow
         do isym = 1, drt%nsym
            if ( bpt .eq. drt%lenbuf ) then
               nread = min( nleft, drt%lenbuf )
               call read_biv( ndrt, drt%lenbuf, nread, buf )
               bpt = 0
            endif
            bpt = bpt + 1
            drt%row(irow)%x(isym) = buf(bpt)
            nleft = nleft - 1
         enddo
         drt%row(irow)%qx(:) = drt%row(irow)%x(:) .ne. 0  ! this assumes x(:) does not overflow.
      enddo

      ! row(:)%z(:)
      nleft = drt%nsym * drt%nrow
      bpt = drt%lenbuf
      do irow = 1, drt%nrow
         do isym = 1, drt%nsym
            if ( bpt .eq. drt%lenbuf ) then
               nread = min( nleft, drt%lenbuf )
               call read_biv( ndrt, drt%lenbuf, nread, buf )
               bpt = 0
            endif
            bpt = bpt + 1
            drt%row(irow)%z(isym) = buf(bpt)
            nleft = nleft - 1
         enddo
      enddo

      call read_biv( ndrt, drt%lenbuf, drt%nwalk, drt%limvec )

      call read_biv( ndrt, drt%lenbuf, drt%nwalk, drt%r )

      return
   end subroutine read_mcdrt_data_2

   subroutine read_mcdrt_data_3( ndrt, drt )

      ! read the drt arrays for an mcscf drt from an external file.
      ! the external file should be positioned correctly before calling this subroutine.

      ! 26-feb-2006 qx() and qxbar(:) computation added. -rls

      use drt_util_mod

      implicit none

      integer, intent(in) :: ndrt
      type(mcdrt), intent(inout) :: drt

      integer :: irow, jrow, ilev, a, b, c, istep, lt, ksym
      integer :: amin, amax, bmin, bmax, cmin, cmax, a_head
      integer :: l(0:3)
      character(20) :: cfmt

      drt%slabel(0) = ' '
      call read_bcv( ndrt, drt%nsym, drt%nsym, drt%slabel(1:) )

      call read_biv( ndrt, drt%ndot, drt%ndot, drt%doub )

      call read_biv( ndrt, drt%ndot, drt%ndot, drt%symd )

      drt%active(0) = 0
      call read_biv( ndrt, drt%nact, drt%nact, drt%active(1:) )

      drt%levsym(0) = 1        ! levsym(:) is top-indexed
      call read_biv( ndrt, drt%nact, drt%nact, drt%levsym(1:) )

      ! read the distinct row information one node at a time.

      read(ndrt,'(a)') cfmt

      drt%nj(:) = 0
      do irow = 1, drt%nrow
         read(ndrt,cfmt) jrow, ilev, a, b, l
         drt%row(jrow)%level = ilev
         drt%row(jrow)%a     = a
         drt%row(jrow)%b     = b
         drt%row(jrow)%l(:)  = l(:)
         drt%nj(ilev)        = drt%nj(ilev) + 1
      enddo

      drt%njskp(0) = 0
      do ilev = 1, drt%nact
         drt%njskp(ilev) = drt%njskp(ilev-1) + drt%nj(ilev-1)
      enddo

      drt%row(:)%c = drt%row(:)%level - drt%row(:)%a - drt%row(:)%b
         
      ! allocate the leading component of the node_lev array.

      allocate( drt%node_lev(0:drt%nact) )

      drt%node_lev(:)%amin = drt%nact
      drt%node_lev(:)%amax = -1

      do irow = 1, drt%nrow  ! determine amin and amax for each level.
         a    = drt%row(irow)%a
         ilev = drt%row(irow)%level
         drt%node_lev(ilev)%amin = min( drt%node_lev(ilev)%amin, a )
         drt%node_lev(ilev)%amax = max( drt%node_lev(ilev)%amax, a )
      enddo

      do ilev = 0, drt%nact
         amin = drt%node_lev(ilev)%amin
         amax = drt%node_lev(ilev)%amax
         allocate( drt%node_lev(ilev)%a(amin:amax) )  ! allocate jagged arrays.
         drt%node_lev(ilev)%a(:)%bmin = drt%nact + 1
         drt%node_lev(ilev)%a(:)%bmax = -1
      enddo

      do irow = 1, drt%nrow  ! determine bmin and bmax for each level and a value.
         a    = drt%row(irow)%a
         b    = drt%row(irow)%b
         ilev = drt%row(irow)%level
         drt%node_lev(ilev)%a(a)%bmin = min( drt%node_lev(ilev)%a(a)%bmin, b )
         drt%node_lev(ilev)%a(a)%bmax = max( drt%node_lev(ilev)%a(a)%bmax, b )
      enddo

      do ilev = 0, drt%nact
         do a = drt%node_lev(ilev)%amin, drt%node_lev(ilev)%amax
            bmin = drt%node_lev(ilev)%a(a)%bmin
            bmax = drt%node_lev(ilev)%a(a)%bmax
            allocate( drt%node_lev(ilev)%a(a)%b(bmin:bmax) )  ! allocate jagged arrays.
            drt%node_lev(ilev)%a(a)%b(:) = 0
         enddo
      enddo

      do irow = 1, drt%nrow  ! determine bmin and bmax for each level and a value.
         a    = drt%row(irow)%a
         b    = drt%row(irow)%b
         ilev = drt%row(irow)%level
         drt%node_lev(ilev)%a(a)%b(b) = irow
      enddo

      a_head = drt%row(drt%nrow)%a
      ! allocate the leading component of the node pointer array.
      allocate( drt%node_a(0:a_head) )

      drt%node_a(:)%cmin = drt%row(drt%nrow)%c + 1
      drt%node_a(:)%cmax = -1

      do irow = 1, drt%nrow  ! determine cmin and cmax for each a value.
         a = drt%row(irow)%a
         c = drt%row(irow)%c
         drt%node_a(a)%cmin = min( drt%node_a(a)%cmin, c )
         drt%node_a(a)%cmax = max( drt%node_a(a)%cmax, c )
      enddo

      do a = 0, a_head
         cmin = drt%node_a(a)%cmin
         cmax = drt%node_a(a)%cmax
         allocate( drt%node_a(a)%c(cmin:cmax) )  ! allocate jagged arrays.
         drt%node_a(a)%c(:)%bmin = drt%nact + 1
         drt%node_a(a)%c(:)%bmax = -1
      enddo

      do irow = 1, drt%nrow  ! determine bmin and bmax for each (a,c) pair.
         a = drt%row(irow)%a
         b = drt%row(irow)%b
         c = drt%row(irow)%c
         drt%node_a(a)%c(c)%bmin = min( drt%node_a(a)%c(c)%bmin, b )
         drt%node_a(a)%c(c)%bmax = max( drt%node_a(a)%c(c)%bmax, b )
      enddo

      do a = 0, a_head
         do c = drt%node_a(a)%cmin, drt%node_a(a)%cmax
            bmin = drt%node_a(a)%c(c)%bmin
            bmax = drt%node_a(a)%c(c)%bmax
            allocate( drt%node_a(a)%c(c)%b(bmin:bmax) )  ! allocate jagged arrays.
            drt%node_a(a)%c(c)%b(:) = 0                  ! initialize the vector entries.
         enddo
      enddo

      ! set pointers to map from (a,c,b) to irow.
      ! null entries, if any, are indicated with zeros.
      do irow = 1, drt%nrow
         a = drt%row(irow)%a
         b = drt%row(irow)%b
         c = drt%row(irow)%c
         drt%node_a(a)%c(c)%b(b) = irow
      enddo

      ! compute row(:)%k(:) from row(:)%l(:)
      forall (irow=1:drt%nrow) drt%row(irow)%k = 0  ! (:)
      do irow = 1, drt%nrow
         do istep = 0, 3
            lt = drt%row(irow)%l(istep)
            if ( lt .ne. 0 ) drt%row(lt)%k(istep) = irow
         enddo
      enddo

      ! compute row(:)%y(:,:), row(:)%xbar(:), and row(:)%qxbar(:)
      ! in this version, qxbar(:) is computed with logical operations to avoid
      ! integer overflow problems in the xbar(:) computation.
      drt%row(drt%nrow)%xbar(:)  = 0
      drt%row(drt%nrow)%xbar(1)  = 1  ! initialize the graph head
      drt%row(drt%nrow)%qxbar(:) = .false.
      drt%row(drt%nrow)%qxbar(1) = .true.
      do ilev = (drt%nact-1), 0, -1  ! loop over the levels from top to bottom
         do irow = (drt%njskp(ilev) + 1), (drt%njskp(ilev) + drt%nj(ilev))
            drt%row(irow)%xbar(:)  = 0
            drt%row(irow)%qxbar(:) = .false.
            do istep = 3, 0, -1
               drt%row(irow)%y(:,istep) = drt%row(irow)%xbar(:)
               jrow = drt%row(irow)%l(istep)
               if ( jrow .ne. 0 ) then
                  ksym = drt%stepsym(istep,drt%levsym(ilev+1))
                  drt%row(irow)%xbar(1:drt%nsym)  = drt%row(irow)%xbar(1:drt%nsym)  &
                       &                          + drt%row(jrow)%xbar(mult(:,ksym))
                  drt%row(irow)%qxbar(1:drt%nsym) = drt%row(irow)%qxbar(1:drt%nsym)  &
                       &                       .or. drt%row(jrow)%qxbar(mult(:,ksym))
               endif
            enddo
         enddo
      enddo

      ! compute row(:)%x(:), and row(:)%qx(:)
      ! in this version, qx(:) is computed with logical operations to avoid
      ! integer overflow problems in the x(:) computation.
      drt%row(1)%x(:)  = 0
      drt%row(1)%x(1)  = 1  ! initialize the graph tail.
      drt%row(1)%qx(:) = .false.
      drt%row(1)%qx(1) = .true.
      do ilev = 1, drt%nact  ! loop over the levels from bottom to top.
         do irow = (drt%njskp(ilev) + 1), (drt%njskp(ilev) + drt%nj(ilev))
            drt%row(irow)%x(:)  = 0
            drt%row(irow)%qx(:) = .false.
            do istep = 0, 3
               jrow = drt%row(irow)%k(istep)
               if ( jrow .ne. 0 ) then
                  ksym = drt%stepsym(istep,drt%levsym(ilev))
                  drt%row(irow)%x(1:drt%nsym)  = drt%row(irow)%x(1:drt%nsym)  &
                       &                       + drt%row(jrow)%x(mult(:,ksym))
                  drt%row(irow)%qx(1:drt%nsym) = drt%row(irow)%qx(1:drt%nsym)  &
                       &                    .or. drt%row(jrow)%qx(mult(:,ksym))
               endif
            enddo
         enddo
      enddo

      forall (irow=1:drt%nrow) drt%row(irow)%z  = 0  ! (:)  this is not used now, so just zero it out.
      
      return
   end subroutine read_mcdrt_data_3

   subroutine print_mcdrt( nlist, drt, llow, lhigh, print_node_a )

      ! print out the mcdrt arrays

      implicit none

      integer, intent(in) :: nlist  ! print output unit number
      type(mcdrt), intent(in) :: drt
      integer, intent(in), optional :: llow, lhigh  ! level range
      logical, intent(in), optional :: print_node_a

      integer :: l1, l2, ilev, ir1, ir2, irow, isym, a, c, amin, amax, cmin, cmax
      logical :: qp_node

      if ( present(print_node_a) ) then
         qp_node = print_node_a
      else
         qp_node = .true. !default
      endif

      if ( present(llow) ) then
         l1 = llow
      else
         l1 = 0  ! default
      endif

      if ( present(lhigh) ) then
         l2 = lhigh
      else
         l2 = drt%nact  ! default
      endif

      ! ensure 0 <= l1 <= l2 <= drt%nact
      ir1 = min(max(0,l1),drt%nact)
      ir2 = min(max(0,l2),drt%nact)
      l1  = min( ir1, ir2 )
      l2  = max( ir1, ir2 )

      write(nlist,'(a)') drt%title

      write(nlist,6010) l1, l2

      do ilev = l2, l1, -1
         ir1 = drt%njskp(ilev) + 1
         ir2 = drt%njskp(ilev) + drt%nj(ilev)

         do irow = ir1, ir2

            isym = 1
            write(nlist,6030) irow, ilev, drt%row(irow)%a, drt%row(irow)%b, drt%row(irow)%c,&
                 & drt%levsym(ilev), drt%slabel(drt%levsym(ilev)), drt%active(ilev),  &
                 & drt%row(irow)%l(:),  &
                 & isym, drt%row(irow)%xbar(isym), &
                 & drt%row(irow)%y(isym,0:2),  &
                 & drt%row(irow)%x(isym), drt%row(irow)%z(isym)

            do isym = 2, drt%nsym
               write(nlist,6040)  &
                    & isym, drt%row(irow)%xbar(isym), drt%row(irow)%y(isym,0:2), &
                    & drt%row(irow)%x(isym), drt%row(irow)%z(isym)
            enddo
         enddo
         write(nlist,6050)
      enddo

      if ( qp_node ) then  ! print the node_a(:,:) array
         write(nlist,6060)
         do a = 0, drt%row(drt%nrow)%a
            cmin = drt%node_a(a)%cmin
            cmax = drt%node_a(a)%cmax
            c    = cmin
            write(nlist,6070) a, c, &
                 & drt%node_a(a)%c(c)%bmin, drt%node_a(a)%c(c)%bmax, drt%node_a(a)%c(c)%b
            do c = (cmin+1), cmax
               write(nlist,6080) c, drt%node_a(a)%c(c)%bmin, drt%node_a(a)%c(c)%bmax, drt%node_a(a)%c(c)%b
            enddo
         enddo

         write(nlist,6090)
         do ilev = drt%nact, 0, -1
            amin = drt%node_lev(ilev)%amin
            amax = drt%node_lev(ilev)%amax
            a    = amin
            write(nlist,6100) ilev, a, &
                 & drt%node_lev(ilev)%a(a)%bmin, drt%node_lev(ilev)%a(a)%bmax, drt%node_lev(ilev)%a(a)%b(:)
            do a = (amin+1), amax
               write(nlist,6110) a, &
                 & drt%node_lev(ilev)%a(a)%bmin, drt%node_lev(ilev)%a(a)%bmax, drt%node_lev(ilev)%a(a)%b(:)
            enddo
         enddo
      endif

      return

6010  format(/'level',i3,' through level',i3,' of the drt:'//  &
           & 'row',' lev',' a b c',' syml',' lab',' rmo',   &
           &  '  l0 ',' l1 ',' l2 ',' l3 ',  &
           &  'isym',' xbar ', '  y0  ','  y1  ','  y2  ','  xp  ','   z')
6030  format(i3,i4,3i2,i4,a4,i4,i5,3i4,i4,6i6)
6040  format(42x,i4,6i6)
6050  format(42('-'))
6060  format(3x,'a   c  b-  b+    b(b-:b+)')
6070  format(4i4,99i5)
6080  format(4x,3i4,99i5)
6090  format(' lev   a  b-  b+    b(b-:b+)')
6100  format(4i4,99i5)
6110  format(4x,3i4,99i5)

   end subroutine print_mcdrt

end module mcdrt_mod

