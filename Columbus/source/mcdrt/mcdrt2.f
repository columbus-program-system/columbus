!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcdrt2.fcdrt2.f
cmcdrt part=2 of 2.  shared mcdrt/cidrt routines.
cversion=4.1 last modified: 24-nov-90
c deck inso
      subroutine inso( j, nmax, n, text, * )
c
c  read a symmetry-orbital integer array j(1:2,*) and return the
c  number of orbitals.
c  last orbital is followed by a zero symmetry entry.
c
       implicit none
      integer nmax, n
      character*(*) text
      integer j(2,nmax)
c
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, iret, ios, i, iwerr
c
      iwerr = 0
      iret = 0
c
      numerr = 0
100   continue
      call izero_wr(2*nmax,j,1)
      write(nout,6010)text
6010  format(1x,a,':')
c
      call qrdiv(nin,2*nmax,j,iret,ios)
c
      if(iret.eq.qok)then
c
c        # how many orbitals were input?
c
          n = 0
          do 200 i = 1,nmax
              if(j(1,i).le.0)then
                  call wrtivf(nkey,j,2*n,' ',' ',0,iwerr)
                  if(iwerr.ne.0)call bummer
     &             ('inso: from wrtivf, iwerr=',iwerr,faterr)
                  write(nkey,*)' /',text
                  return
              endif
              n = i
200       continue
          call bummer('inso: 0 entry not found. nmax=',nmax,faterr)
      elseif(iret.eq.qup)then
          write(nkey,*)' ^'
          return 1
      elseif(iret.eq.qend)then
          call bummer('inso: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
          write(nlist,*)'internal error, ios=',ios
          numerr = numerr+1
          if(numerr.gt.5)call bummer('inso: numerr=',numerr,faterr)
          go to 100
      elseif(iret.eq.qexerr)then
          write(nlist,*)'external error, iostat=',ios
          numerr = numerr+1
          if(numerr.gt.5)call bummer('inso: numerr=',numerr,faterr)
          go to 100
      else
          call bummer('inso: unknown code, iret=',iret,faterr)
      endif
c
      end
c deck wrtdbl
      subroutine wrtdbl( lenbuf, ia, n, cfmt, string )
c
c  write ia(1:n) to the drt file in blocks of length lenbuf.
c
c  29-nov-90 string added. -rls
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer lenbuf, n
      integer ia(*)
      character*(*) cfmt, string
c
      integer iflag, nleft, nwrite, bpt, iwerr
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      iwerr = 0
c
      iflag = 1
      nleft = n
      do 10 bpt = 1, n, lenbuf
          nwrite = min(nleft,lenbuf)
          call wrtivf(
     &     ndrt, ia(bpt), nwrite, cfmt, string, iflag, iwerr )
          if(iwerr.ne.0)call bummer('wrtdbl: from wrtivf, iwerr=',
     &      iwerr,faterr)
          nleft = nleft - nwrite
10    continue
      return
      end
c deck wrtivf
      subroutine wrtivf( unit, ia, lena, cfmt, string, flag, ierr )
c
c  write a formatted integer vector.  optionally determine the format
c  that gives the minimum number of 80 column records.  formats are
c  determined assuming no carriage control characters in column 1.
c
c  29-nov-90 string added. -rls
c  08-sep-90 ierr initialized for lena=0 cases. -rls
c  cfmt option added 23-sep-87. -rls
c  ierr added 21-sep-87. -rls
c  modified by ron shepard, 8/22/84.
c  written by ray bair, anl, 8/22/84.
c
c  input:
c  unit = fortran unit number.
c  ia(*)= integer array of numbers to output.
c  lena  = array length. may be zero.
c  cfmt = output format.
c         if cfmt=' ', then determine a suitable format.
c  flag = format flag.  only the last digit, mod(flag,10) is
c                       used in this version.
c       = *0  =>  do not write out the format.
c       = *n  =>  write out the format and string.
c
c  output:
c  ierr = 0 for normal return
c
       implicit none
      integer lena, ia(*), flag, unit, ierr
      character*(*) cfmt, string
c
      intrinsic len
c
      integer amina, amaxa, ifpt, i, imin, imax
      character*6 ifmt(2:21)
      data ifmt/'(40i2)','(26i3)','(20i4)','(16i5)','(13i6)','(11i7)',
     * '(10i8)','(8i9)','(8i10)','(7i11)','(6i12)','(6i13)','(5i14)',
     * '(5i15)','(5i16)','(4i17)','(4i18)','(4i19)','(4i20)','(3i26)'/
c
      ierr = 0
      if( cfmt .ne. ' ' )then
c        # optionally write out the format...
         if ( mod( flag, 10 ) .ne. 0 ) then
            if ( len(cfmt) .gt. 20 ) then
c              # drt formats must be no longer than 20 characters.
               ierr = -2
               return
            endif
            write(unit,6010,iostat=ierr,err=6) cfmt, string
         endif
c        # write the array using the dummy argument input format.
         if ( lena .gt. 0 ) then
            call wrtiv( unit, ia, lena, cfmt, ierr)
            if ( ierr .ne. 0 ) return
         endif
      else
c        # find the best format...
         if( lena .le. 0 ) then
            ifpt = 2
         else
            amina = 0
            amaxa = 0
c           # find largest and smallest element.
            do 1 i = 1, lena
               amina = min( ia(i), amina )
               amaxa = max( ia(i), amaxa )
1           continue
c           # find log(10)+2 of smallest element, allow for sign.
            imin = 2
            if( amina .eq. 0 ) go to 3
            i = 3
            amina = abs(amina)
            do 2 imin = i, 20
               if( amina .lt. 10 ) go to 3
               amina = amina / 10
2           continue
            imin = 21
c           # find log(10)+2 of largest element.
3           i = 2
            do 4 imax = i, 20
               if( amaxa.lt.10 ) go to 5
               amaxa = amaxa/10
4           continue
            imax = 21
5           ifpt = max( imin,imax )
         endif
c        # optionally write the format.
         if( mod( flag, 10 ) .ne. 0 ) then
            write(unit,6010,iostat=ierr,err=6) ifmt(ifpt), string
         endif
c        # write the array.
         if ( lena .gt. 0 ) then
            call wrtiv( unit, ia, lena, ifmt(ifpt), ierr )
            if ( ierr .ne. 0 ) return
         endif
      endif
c
6     return
6010  format(a,t21,a)
      end
c deck wrtiv
      subroutine wrtiv( iunit, ia, len, cfmt, ierr )
c
c  write an integer vector to unit iunit.
c
c  input:
c  iunit = fortran unit.
c  ia(*) = integer vector.
c  len   = vector length.  must be > 0.
c  cfmt  = character format.
c
c  output:
c  ierr  = 0 for normal return.
c
       implicit none
      integer iunit, len, ierr
      integer ia(len)
      character*(*) cfmt
c
      write(iunit,cfmt,iostat=ierr) ia
c
      return
      end
c deck inchst
      subroutine inchst( chst, prompt, dchst, * )
c
c  read a character string.
c  this routine cannot be used to return a blank string
c  unless the default string is blank -rls.
c
      implicit none 
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      character*(*) prompt,dchst,chst
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, iret, ios
c
      iret = 0
c
      numerr=0
100   chst=dchst
      write(nout,6010)prompt,dchst
6010  format(1x,a,', default=',a)
c
      call qrdc(nin,chst,iret,ios)
c
      if(iret.eq.qok)then
c        # replace blanks with default and return.
         if(chst.eq.' ') chst = dchst
         write(nkey,5010)chst
         return
      elseif(iret.eq.qup)then
         write(nkey,5010)' ^'
         return 1
      elseif(iret.eq.qend)then
         call bummer('inchst: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
         write(nlist,*)'internal error, ios=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('inchst: numerr=',numerr,faterr)
         go to 100
      elseif(iret.eq.qexerr)then
         write(nlist,*)'external error, iostat=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('inchst: numerr=',numerr,faterr)
         go to 100
      else
         call bummer('inchst: unknown code, iret=',iret,faterr)
      endif
c
5010  format(a)
      end
c deck ini
      subroutine ini( j, text, * )
c
c  read an integer variable j.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      character*(*) text
      integer j
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, iret, ios
      integer jv(1)
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
      iret = 0
c
      numerr=0
100   continue
      write(nout,6010)text,j
c
      jv(1)=j
      call qrdiv(nin,1,jv,iret,ios)
      j=jv(1)
c
      if(iret.eq.qok)then
         write(nkey,5020)j,' /',text
         return
      elseif(iret.eq.qup)then
         write(nkey,*)' ^'
         return 1
      elseif(iret.eq.qend)then
         call bummer('ini: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
         write(nlist,*)'internal error, ios=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('ini: numerr=',numerr,faterr)
         go to 100
      elseif(iret.eq.qexerr)then
         write(nlist,*)'external error, iostat=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('ini: numerr=',numerr,faterr)
         go to 100
      else
         call bummer('ini: unknown code, iret=',iret,faterr)
      endif
c
5020  format(i3,2a)
*@ifdef format
c     # use the nonstandard $ to suppress the carriage return.
6010  format(1x,a,' [',i3,']:',$)
*@else
*6010  format(1x,a,' [',i3,']:')
*@endif
      end
c deck inivp
      subroutine inivp( k, j, n, text, * )
c
c  print out integer array k(*) for a prompt, and
c  read an integer array j(*).
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer n
      character*(*) text
      integer k(n),j(n)
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, iret, ios, iwerr
c
      iwerr = 0
      iret = 0
c
      numerr=0
100   continue
      write(nout,6010)text,k
6010  format(1x,a,':'/(1x,20i3))
c
      call qrdiv(nin,n,j,iret,ios)
c
      if(iret.eq.qok)then
         call wrtivf(nkey,j,n,' ',' ',0,iwerr)
         if(iwerr.ne.0) then
            call bummer('inivp: from wrtivf, iwerr=',iwerr,faterr)
         endif
         return
      elseif(iret.eq.qup)then
         write(nkey,*)' ^'
         return 1
      elseif(iret.eq.qend)then
         call bummer('inivp: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
         write(nlist,*)'internal error, ios=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('inivp: numerr=',numerr,faterr)
         go to 100
      elseif(iret.eq.qexerr)then
         write(nlist,*)'external error, iostat=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('inivp: numerr=',numerr,faterr)
         go to 100
      else
         call bummer('inivp: unknown code, iret=',iret,faterr)
      endif
c
      end
c deck inyn
      subroutine inyn( yn, text, * )
c
c  read a character value of 'y' or 'n' and return an appropriate
c  code.  both upper and lower case may be input.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      character*(*) text
c
      integer yn
c
      integer   yes,  no
      parameter(yes=1,no=0)
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      character*7 ynprmp
c
      character*1 uppery,    uppern
      character*80 ynt
c
      integer numerr, iret, ios, i1
c
      integer  ifnch
      external ifnch
c
      iret = 0
c
      if(yn.eq.yes)then
         ynprmp='([y],n)'
      else
         yn=no
         ynprmp='(y,[n])'
      endif
c
      uppery = 'y'
      call allcap(uppery)
      uppern = 'n'
      call allcap(uppern)
c
      numerr=0
100   continue
      write(nout,6010)text,ynprmp
c
      call qrdc(nin,ynt,iret,ios)
c
      if(iret.eq.qok)then
         call allcap(ynt)
         i1=ifnch(ynt,' ')
         if(i1.eq.0)then
            write(nlist,*)'y or n required'
            go to 100
         elseif(ynt(i1:i1).eq.uppery)then
            yn=yes
            write(nkey,5010)uppery, ' /' ,text
            return
         elseif(ynt(i1:i1).eq.uppern)then
            yn=no
            write(nkey,5010)uppern, ' /' ,text
            return
         elseif(ynt(i1:i1).eq.'/')then
            if(yn.eq.yes)then
               write(nkey,5010)uppery, ' /' ,text
            else
               write(nkey,5010)uppern, ' /' ,text
            endif
            return
         else
            write(nlist,*)'y or n required'
            go to 100
         endif
      elseif(iret.eq.qup)then
         write(nkey,5010)' ^'
         return 1
      elseif(iret.eq.qend)then
         call bummer('inyn: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
         write(nlist,*)'internal error, ios=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('inyn: numerr=',numerr,faterr)
         go to 100
      elseif(iret.eq.qexerr)then
         write(nlist,*)'inyn: external error, iostat=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('inyn: numerr=',numerr,faterr)
         go to 100
      else
         call bummer('inyn: unknown code, iret=',iret,faterr)
      endif
5010  format(4a)
*@ifdef format
c     # use the nonstandard $ to suppress the carriage return.
6010  format(1x,2a,$)
*@else
*6010  format(1x,2a)
*@endif
      end
c deck qrdc
      subroutine qrdc( nunit, chst, iret, ios )
      character*(*) chst
c
c  read an undelelimited charater string, checking for up-arrows.
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
      read(nunit,'(a)',iostat=ios)chst
      if(ios.lt.0)then
         iret=qend
      elseif(ios.eq.0)then
         i = ifnch(chst,' ')
         if ( i.ne. 0 ) then
            if ( chst(i:i).eq.'^' ) then
               iret = qup
            else
               iret = qok
            endif
         else
            iret = qok
         endif
      elseif ( ios .gt. 0 ) then
         iret = qexerr
      endif
      return
      end
c deck iniv
      subroutine iniv( j, n, text, * )
c
c  read an integer array j(*).
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      character*(*) text
      integer n
      integer j(n)
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, iret, ios, iwerr
c
      iwerr = 0
      iret = 0
c
      numerr=0
100   continue
      write(nout,6010)text
6010  format(1x,a,':')
c
      call qrdiv( nin, n, j, iret, ios )
c
      if(iret.eq.qok)then
         call wrtivf(nkey,j,n,' ',' ',0,iwerr)
         if(iwerr.ne.0)
     &    call bummer('iniv: from wrtivf, iwerr=',iwerr,faterr)
         return
      elseif(iret.eq.qup)then
         write(nkey,*)' ^'
         return 1
      elseif(iret.eq.qend)then
         call bummer('iniv: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
         write(nlist,*)'internal error, ios=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('iniv: numerr=',numerr,faterr)
         go to 100
      elseif(iret.eq.qexerr)then
         write(nlist,*)'external error, iostat=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('iniv: numerr=',numerr,faterr)
         go to 100
      else
         call bummer('iniv: unknown code, iret=',iret,faterr)
      endif
c
      end
c deck qrdiv
      subroutine qrdiv( nunit, ivlen, iv, iret, ierr )
c
c  simulated list-directed read of an integer vector.
c
c  input:
c  nunit = fortran unit number.
c  ivlen = integer vector length.
c  iv(*) = integer vector.
c
c  output:
c  iv(*) = modified vector.
c  iret  = integer return code.  includes normal return, up-arrow,
c          internal input error, external input error, and
c          end-of-file.
c  ierr  = integer error code.  defined only with internal or
c          external errors.
c
c  written by ron shepard 1-oct-87.
c
       implicit none
      integer nunit, ivlen, iv(ivlen), iret, ierr
c
c     # output return codes...
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
      integer l1, l2, ivpt, l1n, ircode, starpt, repeat, ival, i1, i
      integer   linlen
      parameter(linlen=80)
      character*(linlen) linbuf
c
c     # internal return codes...
      integer   irok,   ireof,    ireor,    irup
      parameter(irok=0, ireof=-1, ireor=-2, irup=-3)
c
      integer  ichtoi
      external ichtoi
c
*@ifdef reference
*C
*C     # all-fortran code included for reference.
*C     # only external errors are detected. '^' is not detected.
*C
*      read(nunit,*,iostat=ierr)iv
*      if(ierr.lt.0)then
*          iret=qend
*      elseif(ierr.eq.0)then
*          iret=qok
*      elseif(ierr.gt.0)then
*          iret=qexerr
*      endif
*      return
*@endif
c
      ival = 0
      l2 = 0
      repeat = 0
      ircode = 0
c
      ivpt=0
      l1n=linlen+1
100   continue
      l1=l1n
      if(ivpt.ge.ivlen)then
         iret=qok
         return
      endif
      call gtoken(nunit,linbuf,linlen,l1,l2,l1n,ircode)
      if(ircode.eq.irok)then
c
c        # interpret token.
c        # l1 points to the first character of the token.
c        # l2 points to the last character of the token.
c        # l1n points to the first character after the token.
c        # token possibilities are: [ nnn [ * [ nnn ] ] ].
c
         if(l1.gt.l2)then
            ivpt=ivpt+1
            go to 100
         else
            starpt=index(linbuf(l1:l2),'*')
            if(starpt.eq.0)then
               ivpt=ivpt+1
               iv(ivpt)=ichtoi(linbuf(l1:l2),ierr)
               if(ierr.ne.0)then
                  iret=qinerr
                  return
               endif
               go to 100
            elseif(starpt.eq.1)then
               ierr=1
               iret=qinerr
               return
            else
               repeat=ichtoi(linbuf(l1:l1+starpt-2),ierr)
               if(ierr.ne.0)then
                  iret=qinerr
                  return
               elseif(repeat.le.0)then
                  ierr=1
                  iret=qinerr
                  return
               endif
               if(starpt.eq.l2-l1+1)then
                  ivpt=ivpt+repeat
                  go to 100
               else
                  ival=ichtoi(linbuf(l1+starpt:l2),ierr)
                  if(ierr.ne.0)then
                     iret=qinerr
                     return
                  endif
                  i1=ivpt+1
                  ivpt=min(ivpt+repeat,ivlen)
                  do 110 i=i1,ivpt
                     iv(i)=ival
110               continue
                  go to 100
               endif
            endif
         endif
      elseif(ircode.eq.ireof)then
         iret=qend
         return
      elseif(ircode.eq.ireor)then
         iret=qok
         return
      elseif(ircode.eq.irup)then
         iret=qup
         return
      else
         iret=qexerr
         ierr=ircode
         return
      endif
c
      end
c deck ichtoi
      integer function ichtoi( char, ierr )
c
c  convert packed character string with optional sign to
c  an integer value.
c
c  ierr = 0 for normal return.
c       = location of nondigit character for error.
c
c  this version does not use an internal read in order to return
c  a useful ierr.  -rls
c
       implicit none 
       character*(*) char
      integer ierr
      logical negate
c
      integer i1, i, j
      character*10 cdigts
      parameter(cdigts='0123456789')
c
      if ( char(1:1) .eq. '+' ) then
         i1 = 2
         negate = .false.
      elseif ( char(1:1) .eq. '-' ) then
         i1 = 2
         negate = .true.
      else
         i1 = 1
         negate = .false.
      endif
      ierr   = 0
      ichtoi = 0
      do 100 i = i1, len(char)
         j = index(cdigts,char(i:i))
         if ( j .eq. 0 ) then
            ierr = i
            return
         else
            ichtoi = ichtoi * 10 + (j-1)
         endif
100   continue
      if ( negate ) ichtoi = -ichtoi
      return
      end
c deck gtoken
      subroutine gtoken( nunit, linbuf, linlen, l1, l2, l1n, ircode )
c
c  get the next token from linbuf.  if linbuf is exhausted, read a
c  new buffer line from nunit.  tokens are delimited by spaces,
c  commas, end-of-lines, up-arrows, and slashes.
c
c  input:
c  nunit  = input unit.
c  linbuf = character line buffer.
c  linlen = buffer length.
c  l1     = initial position in linbuf at which to begin the search.
c
c  output:
c  linbuf = possibly modified.
c  l1     = first position of the token.
c  l2     = last position of the token.
c  l1n    = initial position of the next token.
c  ircode = return code.  codes include internal end-of-file,
c           end-of-record, special up-arrow,  normal returns and
c           external error codes.
c
c  written by ron shepard 1-oct-87.
c
       implicit none
      integer nunit, linlen, l1, l2, l1n, ircode
      character*(*) linbuf
c
      integer i, lx, ierr, match
      character*1 char
c
c     # internal return codes...
      integer   irok,   ireof,    ireor,    irup
      parameter(irok=0, ireof=-1, ireor=-2, irup=-3)
c
      integer  ifnch, ifchl
      external ifnch, ifchl
c
      ierr  = 0
      match = 0
c
100   continue
      if(l1.gt.linlen)then
         l1=1
         read(nunit,'(a)',iostat=ierr)linbuf
         if(ierr.lt.0)then
            ircode=ireof
            return
         elseif(ierr.gt.0)then
c           # return external error code.
            ircode=ierr
            return
         endif
      endif
c
c     # get the initial character...
      i=ifnch(linbuf(l1:),' ')
      if(i.eq.0)then
         l1=linlen+1
         go to 100
      endif
      l1=l1+i-1
c
c     # check for special characters...
      char=linbuf(l1:l1)
      if(char.eq.'/')then
         ircode=ireor
         return
      elseif(char.eq.'^')then
         ircode=irup
         return
      endif
c
c     # get the final character position...
      i = ifchl(linbuf(l1:),' ,/^',match)
      if(match.eq.0)then
         l2=linlen
         l1n=l2+1
      elseif(match.eq.1)then
         l2=l1+i-2
         lx=l2+ifnch(linbuf(l2+1:),' ')
         if(lx.eq.l2)then
            l1n=linlen+1
         elseif(linbuf(lx:lx).eq.',')then
            l1n=lx+1
         else
            l1n=lx
         endif
      elseif(match.eq.2)then
         l2=l1+i-2
         l1n=l2+2
      elseif(match.eq.3 .or. match.eq.4)then
         l2=l1+i-2
         l1n=l2+1
      endif
      ircode=irok
      return
      end
