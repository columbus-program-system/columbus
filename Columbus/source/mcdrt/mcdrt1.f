!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcdrt1.f
cmcdrt part=1 of 2.  mcscf drt routines.
cversion=4.1 last modified: 13-jun-96
c
c  things to do in this program:
c  * switch over to a menu driven input.
c
c deck mcdrt
      program mcdrt
c
c   mcscf drt file construction and csf expansion specification.
c   written by ron shepard.
c
c   13-jun-96: no minimum restriction to the number of active orbitals
c              (was 2 before) -md
c
c***********************************************************************
c
c   the following computer programs contain work performed
c   partially or completely by the argonne national laboratory
c   theoretical chemistry group under the auspices of the office
c   of basic energy sciences, division of chemical sciences,
c   u.s. department of energy, under contract w-31-109-eng-38.
c
c   these programs may not be (re)distributed without the
c   written consent of the argonne theoretical chemistry group.
c
c   since these programs are under development, correct results
c   are not guaranteed.
c
c***********************************************************************
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
c     # maximum number of distinct rows.
      integer   nrowmx
      parameter(nrowmx=2**14-1)
c
c     # maximum number of walks...
      integer   nwlkmx
      parameter(nwlkmx=2**20-1)
c
c     # maximum number of active orbitals...
      integer   nmomx
      parameter(nmomx=1023)
c
      integer a(nrowmx),b(nrowmx)
      integer k(0:3,nrowmx), l(0:3,nrowmx),
     & y(8,0:3,nrowmx), xbar(8,nrowmx),
     & xp(8,nrowmx), yp(8,0:3,nrowmx), z(8,nrowmx)
c
      integer modrt(0:nmomx), syml(0:nmomx)
      integer doub(nmomx), symd(nmomx)
      integer row(0:nmomx), step(0:nmomx), ytot(0:nmomx), yptot(0:nmomx)
      integer bmin(nmomx), bmax(nmomx), smask(nmomx)
      integer occmax(nmomx), occmin(nmomx), nj(0:nmomx), njskp(0:nmomx)
      integer arcsym(0:3,0:nmomx), wsym(0:nmomx)
c
c     # scratch vector for reading and converting symmetry orbitals...
      integer   nsomx
      parameter(nsomx=nmomx+1)
      integer symorb(2,nsomx)
c
      integer limvec(nwlkmx), r(nwlkmx)
c
c     # drt output buffer.
      integer   lenbuf
      parameter(lenbuf=1600)
      integer buf(lenbuf)
c
      integer   nremx
      parameter(nremx=100)
      integer remove(3,nremx),levprt(2)
c
      integer mult(8,8)
c
      character*80 title
      character*80 fname
      character*8 char8, cmult(0:8)
      character*4 inlab
      character*4 slabel(0:8)
c
      integer ichar, numend, i, smult, nelt, neld, nela, ssym, nsym,
     & nmot, nact, ndot, nrem, nwalk, ncsf, j, nrow
c
      integer   yes,  no
      parameter(yes=1,no=0)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      data cmult/
     & 'bad-tet','singlet','doublet','triplet','quartet',
     & 'quintet','sextet','septet','big-tet'/
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      nin   = 5
      nout  = 6
      nlist = 6
      ndrt  = 20
      nkey  = 21
c
c     # open nin and nlist...
c
*@ifdef crayctss
*      call link('unit5=tty,unit6=unit5//')
*@else
c  use preconnected units.
*@endif
c
c     # open the keystroke file...
c
      fname = 'mcdrtky'
      call trnfln( 1, fname )
*@ifdef vax
*C     # must use a nonstandard parameter to get the correct file type.
*      open(unit=nkey,file=fname,status='new',carriagecontrol='list')
*@else
      open(unit=nkey,file=fname,status='unknown')
*@endif
c
      call ibummr(nlist)
c
      numend=0
100   numend = numend + 1
      if ( numend .gt. 5 ) then
         call bummer('mcdrt: numend=', numend, faterr)
      endif
      write(nlist,6010)
6010  format(/' program "mcdrt 4.1 a3"',
     & //' distinct row table specification and csf',
     & /' selection for mcscf wavefunction optimization.',
     & //' programmed by: ron shepard'
     & //' version date: 17-oct-91'/)
c
c  version log:
c  17-oct-91 ntot increment changed in remarc(). -rls/g.gawboy
c  28-sep-91 some minor cleanup. -rls
c  03-jan-91 symmetry orbital input changes. -rls
c  08-sep-90 ierr initialized in wrtivf().
c  01-apr-89 sun, alliant, titan port and general cleanup. -rls
c  20-jul-88 unicos version (rls).
c  03-jun-85 start drt output in column 1 for cray compatibility (rls).
c  22-aug-84 variable format routine added (rls/rab).
c  10-jul-84 first version written based on "mdrt".  this version
c            incorporates spatial symmetry in the drt explicitly
c            in the arc weights similar to that described in
c            i. shavitt, chem. physics lett. 63, 421(1979). (rls).
c
      call who2c( 'mcdrt', nlist )
      call headwr(nlist,'MCDRT','5.5     ')
c
c     # set defaults.
c
      inquire(unit=nkey,name=fname)
      write(nlist,6060)'expanded keystroke file:',fname
c
      do 120 i = 1, nmomx
         modrt(i)  = 0
         syml(i)   = 0
         doub(i)   = 0
         symd(i)   = 0
         occmax(i) = 2*i
         occmin(i) = 2*i
         bmin(i)   = 0
         bmax(i)   = 0
         smask(i)  = 1111
120   continue
      smult = 0
      nelt  = 0
      neld  = 0
      nela  = 0
      ssym  = 0
      nsym  = 0
      nmot  = 0
      nact  = 0
      ndot  = 0
      nrem  = 0
      nwalk = 0
      ncsf  = 0
c
c     # start reading user input.
c
      write(nlist,*)
130   continue
      smult = 0
      call ini(smult,'input the spin multiplicity',*100)
      numend = 0
      char8 = cmult( max( 0, min( smult, 8 ) ) )
      write(nlist,6020) smult, char8
6020  format(' spin multiplicity:',i5,4x,a)
      if ( smult .le. 0 ) then
         write(nlist,*)'0 < smult'
         go to 130
      endif
c
140   continue
      nelt = 0
      call ini(nelt,'input the total number of electrons',*130)
      write(nlist,6100)'nelt',nelt
      if( mod( nelt, 2) .eq. mod( smult, 2) ) then
         write(nlist,6100)'inconsistent data, nelt,smult',nelt,smult
         go to 140
      elseif( nelt .le. 0 ) then
         write(nlist,*)'0 < nelt'
         go to 140
      elseif( nelt .lt. smult-1 ) then
         write(nlist,*)'smult-1 <= nelt'
         go to 140
      endif
c
142   continue
      nsym = 0
      call ini(nsym,'input the number of irreps (1-8)',*140)
      write(nlist,6100)'nsym',nsym
      if ( nsym .le. 0 .or. nsym .gt. 8 ) then
         write(nlist,*)'0 < nsym <= 8'
         go to 142
      endif
c
144   continue
      ichar = no
      call inyn(ichar,'enter symmetry labels:',*142)
      slabel(0) = ' '
      do 145 i = 1, nsym
         write (slabel(i),'(i4)') i
145   continue
      if ( ichar .eq. no ) then
         write(nlist,*) 'no symmetry labels entered: defaults taken'
      else
         write(nlist,'(1x,a,i2,a)') 'enter', nsym, ' labels (a4):'
         do 126 i = 1, nsym
            call inchst(inlab,'enter symmetry label',slabel(i),*144)
            slabel(i) = inlab
126      continue
      endif
c
150   continue
      ssym = 0
      call ini( ssym,
     & 'input the molecular spatial symmetry (irrep 1:nsym)',*144)
      write(nlist,6100)'spatial symmetry is irrep number',ssym
      if ( ssym .le. 0 .or. ssym .gt. nsym ) then
         write(nlist,*)'0 < ssym <= nsym'
         go to 150
      endif
c***********************************************************************
c  orbital input:
c***********************************************************************
152   continue
      write(nlist,*)
      call inso( symorb, nsomx, ndot,
     & 'input the list of doubly-occupied orbitals'//
     & ' (sym(i),rmo(i),i=1,ndot)', *150 )
      neld = 2 * ndot
      nela = nelt - neld
      write(nlist,6100) 'number of doubly-occupied orbitals', ndot
      write(nlist,6100) 'number of inactive electrons', neld
      write(nlist,6100) 'number of active electrons', nela
      if ( ndot .lt. 0 ) then
         write(nlist,*)'0 <= ndot'
         go to 152
      elseif ( nela .lt. smult-1 ) then
         write(nlist,*)'(smult-1) <= nela'
         go to 152
      endif
c
      if ( ndot .gt. 0 ) then
c        # check the inactive orbitals.
         do 158 i = 1, ndot
            symd(i) = symorb(1,i)
            doub(i) = symorb(2,i)
            if ( (symd(i) .le. 0) .or. (symd(i) .gt. nsym) ) then
               write(nlist,*)'1<=sym(i)<=nsym, i=', i
               goto 152
            elseif ( doub(i) .le. 0 ) then
               write(nlist,*)'0<rmo(i), i=',i
               goto 152
            endif
            do 156 j = 1, (i-1)
               if ( (symd(j) .eq. symd(i))
     &          .and. (doub(j) .eq. doub(i)) ) then
                  write(nlist,6100)
     &             'error in doubly-occupied orbital. i,j=', i,j
                  go to 152
               endif
156         continue
158      continue
         write(nlist,6200)'level(*)',  (i,               i=1,ndot)
         write(nlist,6200)'symd(*)',   (symd(i),         i=1,ndot)
         write(nlist,6201)'slabel(*)', (slabel(symd(i)), i=1,ndot)
         write(nlist,6200)'doub(*)',   (doub(i),         i=1,ndot)
      endif
c
c     # input the active orbital information.
c
160   continue
      write(nlist,*)
      call inso( symorb, nsomx, nact,
     & 'input the active orbitals (sym(i),rmo(i),i=1,nact)',*152)
      write(nlist,6100)'nact',nact
      if ( nact .lt. 2 ) then
c        # this restriction will eventually be removed. -rls
cmd      write(nlist,*)'2 <= nact for subsequent program steps'
c        go to 160
      elseif ( 2*nact .lt. nela ) then
         write(nlist,*)'nela <= (2*nact)'
         go to 160
      elseif ( nact .lt. smult-1 ) then
         write(nlist,*)'(smult-1) <= nact'
         go to 160
      endif
c
      modrt(0) = 0
      syml(0)  = 0
      do 176 i = 1, nact
         syml(i)  = symorb(1,i)
         modrt(i) = symorb(2,i)
         if ( syml(i) .le. 0 .or. (syml(i) .gt. nsym) ) then
            write(nlist,*)'1<=sym(i)<=nsym, i=', i
            goto 160
         elseif ( modrt(i) .le. 0 ) then
            write(nlist,*)'0 < modrt(i), i=', i
            goto 160
         endif
         do 172 j = 1, (i-1)
            if ( (syml(i) .eq. syml(j))
     &       .and. (modrt(i) .eq. modrt(j)) ) then
               write(nlist,6100)
     &          ' error in active orbital. i,j=', i,j
               go to 160
            endif
172      continue
         do 174 j = 1, ndot
            if( (syml(i) .eq. symd(j))
     &       .and. (modrt(i) .eq. doub(j)) ) then
               write(nlist,6100)
     &          ' error in active orbital i, inactive j=', i,j
               go to 160
            endif
174      continue
176   continue
c
      write(nlist,6200)'level(*)',  (i,               i=1,nact)
      write(nlist,6200)'syml(*)',   (syml(i),         i=1,nact)
      write(nlist,6201)'slabel(*)', (slabel(syml(i)), i=1,nact)
      write(nlist,6200)'modrt(*)',  (modrt(i),        i=1,nact)
c
      do 180 i = 1, nact
         occmin(i) = 0
         occmax(i) = nela
         bmin(i)   = 0
         bmax(i)   = nela
         smask(i)  = 1111
180   continue
      do 190 i = 1, nact
         arcsym(1,i-1) = syml(i)
         arcsym(2,i-1) = syml(i)
         arcsym(0,i-1) = 1
         arcsym(3,i-1) = 1
190   continue
c
200   continue
      call insvp( slabel(1), syml(1), modrt(1),
     & occmin, nact,
     & 'input the minimum cumulative occupation for each active level',
     & *160 )
c
240   continue
      call insvp( slabel(1), syml(1), modrt(1),
     & occmax, nact,
     & 'input the maximum cumulative occupation for each active level',
     & *200 )
c
      write(nlist,6201)'slabel(*)', (slabel(syml(i)), i=1,nact)
      write(nlist,6200)'modrt(*)',  (modrt(i),        i=1,nact)
      write(nlist,6200)'occmin(*)', (occmin(i),       i=1,nact)
      write(nlist,6200)'occmax(*)', (occmax(i),       i=1,nact)
c
260   continue
      call insvp( slabel(1), syml(1), modrt(1),
     & bmin, nact,
     & 'input the minimum b value for each active level',*240)
264   continue
      call insvp( slabel(1), syml(1), modrt(1),
     & bmax, nact,
     & 'input the maximum b value for each active level',*260)
c
      write(nlist,6201)'slabel(*)', (slabel(syml(i)), i=1,nact)
      write(nlist,6200)'modrt(*)',  (modrt(i),        i=1,nact)
      write(nlist,6200)'bmin(*)',   (bmin(i),         i=1,nact)
      write(nlist,6200)'bmax(*)',   (bmax(i),         i=1,nact)
c
269   continue
      call iniv(smask,nact,
     & 'input the step masks for each active level',*264)
      write(nlist,6040)(modrt(i),smask(i),i=1,nact)
6040  format(' modrt:smask='/(8(i4,':',i4.4)))
c
270   continue
      call ini(nrem,'input the number of vertices to be deleted',*269)
      write(nlist,6100)
     & 'number of vertices to be removed (a priori)',nrem
      if( nrem .lt. 0 ) then
         write(nlist,*)'0 <= nrem'
         go to 270
      elseif ( nrem .gt. nremx ) then
         write(nlist,*)'nremx=',nremx
         go to 270
      endif
      if ( nrem .ne. 0 ) then
280      continue
         call iniv(remove,3*nrem,
     &    'input the level, a, and b for the vertices to be removed',
     &    *270)
         write(nlist,6050)nrem,((remove(i,j),i=1,3),j=1,nrem)
      endif
6050  format(/' level,a, and b values of the',i3,
     & ' vertices marked for removal:', /(3i4) )
c
c     # construct the drt.
c
      call condrt(
     & a,      b,      k,      l,
     & nj,     njskp,  nact,   nela,
     & smult,  nrem,   remove, occmin,
     & occmax, bmin,   bmax,   smask,
     & nrowmx,   nrow  )
c
      write(nlist,6100)'number of rows in the drt',nrow
c
c     # manually remove unwanted arcs.
c
300   continue
      ichar=no
      call inyn(ichar,'are any arcs to be manually removed?',*160)
      if ( ichar .eq. yes ) then
         call remarc( a, b, k, l, nj, njskp, nact, nrow, *300 )
      endif
c
c     # calculate symmetry dependent chaining information.
c
      call chain(
     & nact,   nsym,   ssym,   nrow,
     & nj,     njskp,  k,      l,
     & mult,   y,      xbar,   yp,
     & xp,     step,   ytot,   yptot,
     & arcsym, wsym,   r,      z,
     & row,    nwalk,  nwlkmx  )
c
310   continue
      levprt(1)=0
      levprt(2)=nact
      call iniv(levprt,2,
     & 'input the range of drt levels to print (l1,l2)',*300)
      write(nlist,6200)'levprt(*)',levprt
      if(levprt(1).ge.0)then
         call prmdrt(
     &    levprt, nact,   nrow,   nsym,
     &    nj,     njskp,  slabel, modrt,
     &    syml,   a,      b,      l,
     &    xbar,   y,      xp,     z,
     &    8,      nlist )
      endif
c
c     # select valid csfs from the walks specified by the drt.
c
320   continue
      call select(
     & ssym,   nact,   nrow,   nela,
     & nwalk,  ncsf,   limvec, modrt,
     & step,   l,      ytot,   row,
     & wsym,   arcsym, xbar,   y,
     & mult,   *310  )
c
      write(nlist,*)' drt construction and csf selection complete.'
c
340   continue
      write(nlist,*)
      call inchst(title,'input a title card','mdrt2_title',*320)
      write(nlist,*)title
c
350   continue
c
c     # get the file name for drt file.
c
      call inchst(fname,'input a drt file name','mcdrtfl',*340)
      call trnfln( 1, fname )
*@ifdef vax
*C     # must use a nonstandard parameter to get the correct file type.
*      open(unit=ndrt,file=fname,status='new',carriagecontrol='list')
*@else
      open(unit=ndrt,file=fname,status='unknown')
*@endif
      inquire(unit=ndrt,name=fname)
      write(nlist,6060)'drt and indexing arrays written to file:',fname
c
      write(nlist,*)
      ichar = yes
      call inyn( ichar, 'write the drt file?', *350 )
c
      if ( ichar .eq. yes ) then
c
ctm
c         ichar = no
          ichar = yes
         call inyn( ichar, 'include step(*) vectors?', *350 )
c
c        # write drt header info onto the drt file.
c
         write(nlist,*)'drt file is being written...'
c
         call wrthd(
     &    ndrt,   title,  ndot,   nact,
     &    nsym,   nrow,   ssym,   smult,
     &    nelt,   nela,   nwalk,  ncsf,
     &    buf,    lenbuf  )
c
c        # use buf(*) to write the symmetry dependent drt
c        # arrays and csf vectors.
c
         call wrtdrt(
     &    ndot,   nact,   nsym,   nrow,
     &    nwalk,  lenbuf, buf,    slabel,
     &    doub,   symd,   modrt,  syml,
     &    nj,     njskp,  a,      b,
     &    l,      y,      xbar,   xp,
     &    z,      limvec, r,      step,
     &    ytot,   row,    wsym,   arcsym,
     &    mult,   ssym,   ncsf,   ichar  )
      else
         write(nlist,*)'drt file is not written.'
      endif
c
      close(unit=ndrt)
c
      call bummer('normal termination',0,3)
      stop 'end of mcdrt'
c
6060  format(1x,a)
6100  format(1x,a,': ',(10i6))
6200  format(1x,a,(t15,15i4))
6201  format(1x,a,(t15,15a4))
      end
c deck remarc
      subroutine remarc( a, b, k, l, nj, njskp, nact, nrow, * )
c
c  remove arcs manually.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer nact, nrow
      integer a(nrow), b(nrow), k(0:3,nrow), l(0:3,nrow)
      integer nj(0:nact), njskp(0:nact)
c
      integer ir1, ir2, row, lt, nrem, ntot
      integer itemp(4)
      integer ilev, arow, brow, istep
c
      write(nlist,6010)
c
      nrem = 0
      ntot = 0
100   continue
c
      itemp(1) = -1
      itemp(2) = 0
      itemp(3) = 0
      itemp(4) = 0
c
      call iniv( itemp, 4,
     & 'input the level, a, b, and step (end with < 0)', *9000 )
c
      ilev  = itemp(1)
      arow  = itemp(2)
      brow  = itemp(3)
      istep = itemp(4)
c
      if ( ilev .ge. 0 ) then
c
         ntot  = ntot + 1
c
         if ( (istep .lt. 0) .or. (istep .gt. 3) ) then
c           # invalid step.
            write(nlist,6100)'invalid step:', ilev, arow, brow, istep
            goto 100
         elseif ( ilev .le. nact ) then
c
c           # check to make sure the row exists.
c           # (ilev is the bottom of the arc)
c
            ir1 = njskp(ilev) + 1
            ir2 = njskp(ilev) + nj(ilev)
            do 400 row = ir1, ir2
               if ( (arow .eq. a(row)) .and. (brow .eq. b(row)) ) then
c                 # check step.
                  lt = l(istep,row)
                  if ( lt .eq. 0 ) then
                     write(nlist,6100)
     &                'arc not found', ilev, arow, brow, istep
                  else
                     l(istep,row) = 0
                     k(istep,lt)  = 0
                     nrem         = nrem + 1
                     write(nlist,6100)
     &                'arc removed', ilev, arow, brow, istep, nrem
                  endif
                  go to 100
               endif
400         continue
         endif
         write(nlist,6100)'vertex not found', ilev, arow, brow
         goto 100
      endif
c
      write(nlist,6020) nrem, ntot
      return
c
9000  return 1
c
6010  format(/' manual arc removal step:'/
     & /' input the level, a, b, and step values '
     & /' for the arcs to be removed (-1/ to end).'/)
6100  format(1x,a,': level=',i3,', a=',i3,', b=',i3:,
     & ', step=',i2:,', nrem=',i3)
6020  format(' remarc:',i4,' arcs removed out of',i4,' specified.')
      end
c deck vfail
      logical function vfail(
     & nact,   nel,    smult,  ai,
     & bi,     ilev,   occmin, occmax,
     & bmin,   bmax,   nrem,   remove  )
c
c  function vfail determines if a possible vertex is allowed.
c  .true.  indicates that the vertex is not allowed
c  .false. indicates that the vertex is acceptable
c
       implicit none
      integer nact, nel, smult, ai, bi, ilev, nrem
      integer occmin(*),occmax(*),remove(3,*),bmin(*),bmax(*)
c
      integer neleft, bleft, ibleft, ileft, irem, occ
c
      vfail=.true.
c
      if ( bi .lt. bmin(ilev) ) return
      if ( bi .gt. bmax(ilev) ) return
c
      occ = 2 * ai + bi
      if ( occ .lt. occmin(ilev) ) return
      if ( occ .gt. occmax(ilev) ) return
c
      neleft = nel - occ
      bleft = (smult-1) - bi
      ibleft = abs(bleft)
      if(neleft .lt. ibleft) return
      ileft = nact - ilev
      if ( (2 * ileft - neleft) .lt. ibleft ) return
c
      do 100 irem = 1, nrem
         if(     (ilev.eq.remove(1,irem))
     &    .and.  (  ai.eq.remove(2,irem))
     &    .and.  (  bi.eq.remove(3,irem))  )return
100   continue
c
      vfail = .false.
c
      return
      end
c deck chain
      subroutine chain(
     & nact,   nsym,   ssym,   nrow,
     & nj,     njskp,  k,      l,
     & mult,   y,      xbar,   yp,
     & xp,     step,   ytot,   yptot,
     & arcsym, wsym,   r,      z,
     & row,    nwalk,  nwlkmx  )
c
c  this subroutine calculates the forward and reverse weights of the
c  arcs defined by the k(*,*) and l(*,*) arrays.  the offset array
c  z(*,*) and the mapping array r(*) are calculated such that the
c  forward weight is mapped into the reverse weight.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer nact, nsym, ssym, nrow, nwalk, nwlkmx
      integer nj(0:nact), njskp(0:nact), k(0:3,nrow), l(0:3,nrow)
      integer y(8,0:3,nrow), xbar(8,nrow), z(8,nrow)
      integer yp(8,0:3,nrow), xp(8,nrow)
      integer yptot(0:nact), ytot(0:nact)
      integer row(0:nact), step(0:nact)
      integer arcsym(0:3,0:nact), wsym(0:nact)
      integer r(nwlkmx)
      integer mult(8,8)
c
      integer i, isym, j, ilev, lsym, ir1, ir2, irow, kt, lt, jsym,
     & clev, istep, rowc, rown, symc, symn, nlev, iw, tail, l2, l1, is,
     & head, symw, tsym, xt, hsym, xbh, yt, zt, ket
      integer ytotx(8,0:200)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # initialize drt arrays.
c
      do 24 i=1,nrow
         do 10 isym=1,nsym
            xbar(isym,i)=0
            xp(isym,i)=0
            z(isym,i)=0
10       continue
         do 22 j=0,3
            do 20 isym=1,nsym
               y(isym,j,i)=0
               yp(isym,j,i)=0
20          continue
22       continue
24    continue
c
c     # initialize level arrays.
c
      do 30 i=0,nact
         step(i)=4
30    continue
c
c     # calculate the yp(*) and xp(*) chaining indices for
c     # the forward weights.
c     # initialize the first distinct row at level 0.
c
      xp(ssym,1) = 1
c
c     # loop over higher rows
c
      do 2600 ilev = 1, nact
         lsym = arcsym(1,ilev-1)
c
         ir1 = njskp(ilev) + 1
         ir2 = njskp(ilev) + nj(ilev)
         do 2500 irow = ir1, ir2
c
c           # yp(isym,0,*) = 0  for all vertices and all symmetries.
c           # loop over the other step numbers.
c
            kt = k(0,irow)
            if ( kt .ne. 0 ) then
               do 2120 isym = 1, nsym
                  yp(isym,1,irow) = yp(isym,1,irow) + xp(isym,kt)
2120           continue
            endif
            do 2210 isym = 1, nsym
               yp(isym,2,irow) = yp(isym,1,irow)
2210        continue
            kt = k(1,irow)
            if ( kt .ne. 0 ) then
               do 2220 isym = 1, nsym
                  yp(isym,2,irow) = yp(isym,2,irow)
     &             +                xp(mult(isym,lsym),kt)
2220           continue
            endif
            do 2310 isym = 1, nsym
               yp(isym,3,irow) = yp(isym,2,irow)
2310        continue
            kt = k(2,irow)
            if ( kt .ne. 0 ) then
               do 2320 isym = 1, nsym
                  yp(isym,3,irow) = yp(isym,3,irow)
     &             +                xp(mult(isym,lsym),kt)
2320           continue
            endif
            do 2410 isym = 1, nsym
               xp(isym,irow) = yp(isym,3,irow)
2410        continue
            kt=k(3,irow)
            if ( kt .ne. 0 ) then
               do 2420 isym = 1, nsym
                  xp(isym,irow) = xp(isym,irow) + xp(isym,kt)
2420           continue
            endif
2500     continue
2600  continue
c
c     # calculate the reverse weights y(*,*) and the z(*) array.
c
c     # initialize the top row.
c
      xbar(1,nrow) = 1
c
c     # loop over all the lower rows.
c
      do 8000 ilev = (nact-1), 0, -1
         lsym = arcsym(1,ilev)
c
         ir1 = njskp(ilev) + 1
         ir2 = njskp(ilev) + nj(ilev)
         do 7000 irow = ir1, ir2
c
c           # y(isym,3,*)  are zero for all symmetries and all rows.
c
c           # loop over the remaining step numbers.
c
            do 4310 isym = 1, nsym
               y(isym,2,irow) = y(isym,3,irow)
4310        continue
c
            lt = l(3,irow)
            if ( lt .ne. 0 ) then
               do 4320 isym = 1, nsym
                  y(isym,2,irow) = y(isym,2,irow) + xbar(isym,lt)
4320           continue
            endif
            do 4210 isym = 1, nsym
               y(isym,1,irow) = y(isym,2,irow)
4210        continue
c
            lt=l(2,irow)
            if ( lt .ne. 0 ) then
               do 4220 isym = 1, nsym
                  y(isym,1,irow) = y(isym,1,irow)
     &             +               xbar(mult(isym,lsym),lt)
4220           continue
            endif
            do 4110 isym = 1, nsym
               y(isym,0,irow) = y(isym,1,irow)
4110        continue
c
            lt = l(1,irow)
            if ( lt .ne. 0 ) then
               do 4120 isym = 1, nsym
                  y(isym,0,irow) = y(isym,0,irow)
     &             +               xbar(mult(isym,lsym),lt)
4120           continue
            endif
            do 4010 isym = 1, nsym
               xbar(isym,irow) = y(isym,0,irow)
4010        continue
c
            lt = l(0,irow)
            if ( lt .ne. 0 ) then
               do 4020 isym = 1, nsym
                  xbar(isym,irow) = xbar(isym,irow) + xbar(isym,lt)
4020           continue
            endif
c
c           # find the first reverse case for irow.
c
            lt = l(0,irow)
            if ( lt .ne. 0 ) then
               do 6000 isym = 1, nsym
                  if ( xbar(isym,lt) .ne. 0 ) then
                     z(isym,irow) = z(isym,lt) + yp(isym,0,lt)
                  endif
6000           continue
            endif
c
            lt = l(1,irow)
            if ( lt .ne. 0 ) then
               do 6100 isym = 1, nsym
                  jsym = mult(isym,lsym)
                  if ( xbar(jsym,lt) .ne. 0 ) then
                     z(isym,irow) = z(jsym,lt) + yp(jsym,1,lt)
                  endif

6100           continue
            endif
c
            lt = l(2,irow)
            if ( lt .ne. 0 ) then
               do 6200 isym = 1, nsym
                  jsym = mult(isym,lsym)
                  if ( xbar(jsym,lt) .ne. 0 ) then
                     z(isym,irow) = z(jsym,lt) + yp(jsym,2,lt)
                  endif
6200           continue
            endif
c
            lt = l(3,irow)
            if ( lt .ne. 0 ) then
               do 6300 isym = 1, nsym
                  if( xbar(isym,lt) .ne. 0 ) then
                     z(isym,irow) = z(isym,lt) + yp(isym,3,lt)
                  endif
6300           continue
            endif
7000     continue
8000  continue
c
      nwalk = xbar(ssym,1)
      write(nlist,6060)nwalk
6060  format(/' nwalk=',i8)
      if ( nwalk .gt. nwlkmx ) then
         write(nlist,10010) nwalk, nwlkmx
         call bummer('chain: too many walks, nwalk=', nwalk, faterr )
      endif
c
c     # compute the r(*) mapping vector.  r(*) is used as:
c
c     #     iwalk = ytot(tsym,head) + r( z(tsym,tail)+j ) + i
c
c     # where i=1 to xbar(hsym,head) and  j=1 to xp(tsym,tail).
c
c     # ytot(tsym,head)= the sum of the arc weights of a partial
c     #                  walk from tail to head.
c     # tsym           = mult(lower-walk-symmetry,ssym)
c     # hsym           = mult(tsym,wsym) where wsym is the
c     #                  symmetry of the partial walk.
c
      do 9010 i = 1, nwalk
         r(i) = 0
9010  continue
c
      row(0)   = 1
      ytot(0)  = 0
      yptot(0) = 0
      clev     = 0
      wsym(0)  = ssym
      go to 9030
c
c     # decrement current level.
c
9020  continue
      step(clev) = 4
      if ( clev .eq. 0 ) go to 9040
      clev = clev - 1
c
c     # next step at the current level.
c
9030  continue
      istep = step(clev) - 1
      if ( istep .lt. 0 ) go to 9020
      step(clev) = istep
      rowc = row(clev)
      rown = l(istep,rowc)
      if ( rown .eq. 0 ) go to 9030
      symc = wsym(clev)
      symn = mult(arcsym(istep,clev),symc)
      if ( xbar(symn,rown) .eq. 0 ) go to 9030
c
      nlev = clev + 1
      row(nlev) = rown
      ytot(nlev) = ytot(clev)  + y(symc,istep,rowc)
      yptot(nlev)= yptot(clev) + yp(symn,istep,rown)
      wsym(nlev) = symn
      if ( nlev .eq. nact ) then
c        # new walk found.
         iw = yptot(nlev) + 1
         r(iw) = ytot(nlev)
         go to 9030
      endif
c
c     # increment to the next level.
c
      clev = nlev
      go to 9030
c
c     # walk generation complete.
9040  continue
c
c      write(nlist,6070)(r(i),i=1,nwalk)
c6070  format(/' r(*)='/(1x,10i6))
c
      if ( nact .ge. 0 ) return
c
c     # ...the preceeding branch is always taken.
c     # the following code is left here for documentation and
c     # as a possible debugging aid for subsequent programs (rls).
c
c     # the following code generates all the walk numbers sharing
c     # partial walks specified by the tail, level range, and
c     # step vector.
c
9910  write(nlist,6092)
6092  format(' input tail,l1,l2')
      read(nin,*) tail, l1, l2
      if(tail.le.0)go to 9999
      write(nlist,6094)
6094  format(' input step(*)')
      read(nin,*)(step(i),i=l1,l2)
c
c     # for this partial walk, accumulate segment weights.
c
      do 9920 is = 1, nsym
         ytotx(is,l1) = 0
9920  continue
      row(l1) = tail
      wsym(l1) = 1
      do 9940 clev = l1, l2
         nlev = clev + 1
         istep = step(clev)
         rowc = row(clev)
         rown = l(istep,rowc)
         if ( rown .eq. 0 ) go to 9910
         symc = wsym(clev)
         do 9930 is = 1, nsym
            jsym = mult(is,symc)
            ytotx(is,nlev) = ytotx(is,clev) + y(jsym,istep,rowc)
9930     continue
         wsym(nlev) = mult( arcsym(istep,clev), symc)
         row(nlev) = rown
9940  continue
c
      do 9950 clev = (l2+1), l1, -1
         write(nlist,6098) clev, (ytotx(isym,clev),isym=1,nsym)
9950  continue
6098  format(' clev=',i3,' ytotx(*,clev)=',8i6)
c
c     # loop over all walks in the drt that share the partial walk.
c
      head = row(l2+1)
      symw = wsym(l2+1)
      do 9980 tsym = 1, nsym
         xt = xp(tsym,tail)
         if ( xt .eq. 0 ) go to 9980
         hsym = mult(tsym,symw)
         xbh = xbar(hsym,head)
         if ( xbh .eq. 0 ) go to 9980
c
         yt = ytotx(tsym,l2+1)
         zt = z(tsym,tail)
         do 9970 j = 1, xt
            do 9960 i = 1, xbh
               ket = yt + r( zt +j) +i
               write(nlist,6096)ket
6096           format(' ket=',i8)
9960        continue
9970     continue
9980  continue
9990  continue
      go to 9910
9999  continue
c
      return
c
10010 format(/' chain: error. nwalk,nwlkmx=',2i10)
      end
c deck condrt
      subroutine condrt(
     & a,      b,      k,      l,
     & nj,     njskp,  nact,   nel,
     & smult,  nrem,   remove, occmin,
     & occmax, bmin,   bmax,   smask,
     & nrowmx,   nrow  )
c
c  subroutine condrt constructs the chaining indices for the distinct
c  row table subject to imposed restrictions.
c
c  28-sep-91 l(*,nrow)=0 added. -rls
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer nact, nel, smult, nrem, nrowmx, nrow
      logical vfail
      integer a(*),b(*),k(0:3,*),l(0:3,*)
      integer nj(0:nact),njskp(0:nact)
      integer smask(nact),occmin(nact),occmax(nact)
      integer bmin(nact),bmax(nact)
      integer remove(3,*)
c
      integer nlev, clev, ir1, ir2, irow, ac, bc, istep, anext, bnext,
     & irn1, irn2, irn
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer dela(0:3),   delb(0:3)
      data dela/0,0,1,1/,  delb/0,1,-1,0/
c
c     # initialize the first vertex at the zeroth level.
c
      a(1)     = 0
      b(1)     = 0
      nrow     = 1
      nj(0)    = 1
      njskp(0) = 0
c
c     # loop over all the remaining levels.
c
      do 600 nlev = 1, nact
         clev = nlev - 1
         if ( nj(clev) .eq. 0 ) then
            write(nlist,6020) nlev
            call bummer('condrt: nj(clev)=0, clev=', clev, faterr )
         endif
c
         njskp(nlev) = njskp(clev) + nj(clev)
         nj(nlev) = 0
c
c        # loop over the rows of the current level.
c
         ir1 = njskp(clev) + 1
         ir2 = njskp(clev) + nj(clev)
         do 500 irow = ir1, ir2
            l(0,irow) = 0
            l(1,irow) = 0
            l(2,irow) = 0
            l(3,irow) = 0
            ac        = a(irow)
            bc        = b(irow)
c
c           # loop over the four steps from the current vertex, irow.
c
            do 400 istep = 3, 0, -1
               if ( mod(smask(nlev)/10**(istep),10) .eq. 0 ) go to 400
               anext = ac + dela(istep)
               bnext = bc + delb(istep)
               if ( vfail(
     &          nact,   nel,    smult,  anext,
     &          bnext,  nlev,   occmin, occmax,
     &          bmin,   bmax,   nrem,   remove ) ) go to 400
c
c              # check to see if this new row already exists in the drt.
c
               if ( nj(nlev) .eq. 0 ) go to 200
c
               irn1 = njskp(nlev) + 1
               irn2 = njskp(nlev) + nj(nlev)
               do 100 irn = irn1, irn2
                  if ( anext .ne. a(irn) ) go to 100
                  if ( bnext .ne. b(irn) ) go to 100
c
c                 # this row has already been added.
c                 # adjust the chaining indices.
c
                  k(istep,irn)  = irow
                  l(istep,irow) = irn
                  go to 300
100            continue
200            continue
c
c              # this is a new row.  append this new row to the drt.
c
               nrow = nrow + 1
               if ( nrow .gt. nrowmx ) then
                  write(nlist,6010) nrow, nrowmx
                  call bummer('condrt: nrow=', nrow, faterr )
               endif
c
               k(0,nrow)     = 0
               k(1,nrow)     = 0
               k(2,nrow)     = 0
               k(3,nrow)     = 0
               nj(nlev)      = nj(nlev) + 1
               a(nrow)       = anext
               b(nrow)       = bnext
               k(istep,nrow) = irow
               l(istep,irow) = nrow
300            continue
400         continue
500      continue
600   continue
c
c     # set the l values for the top row.
c
      l(0,nrow)    = 0
      l(1,nrow)    = 0
      l(2,nrow)    = 0
      l(3,nrow)    = 0
c
      return
c
6010  format(/' condrt: nrow,nrowmx=',2i10)
6020  format(/' condrt: nj=0 at level',i3,
     & '. invalid drt restrictions.')
      end
c deck prtdrt
      subroutine prmdrt(
     & levprt, nact,   nrow,   nsym,
     & nj,     njskp,  slabel, modrt,
     & syml,   a,      b,      l,
     & xbar,   y,      xp,     z,
     & nsdim,  nlist )
c
c  print drt information from levels levprt(1) to levprt(2).
c
       implicit none
      integer nact, nrow, nsym, nsdim, nlist
      integer nj(0:nact),njskp(0:nact),modrt(0:nact),syml(0:nact)
      integer l(0:3,nrow),xbar(nsdim,nrow),y(nsdim,0:3,nrow)
      integer a(nrow),b(nrow)
      integer xp(nsdim,nrow),z(nsdim,nrow)
      integer levprt(2)
      character*4 slabel(0:nsym)
c
      integer l1, l2, ilev, ir1, ir2, irow, isym, i
c
c     # restrict array references to a meaningful range.
c
      l1 = min(levprt(1),levprt(2))
      l2 = max(levprt(1),levprt(2))
      l1 = min(max(l1,0),nact)
      l2 = min(max(l2,0),nact)
c
      write(nlist,6010) l1, l2
c
      do 300 ilev = l2, l1, -1
         ir1 = njskp(ilev) + 1
         ir2 = njskp(ilev) + nj(ilev)
c
         do 200 irow = ir1, ir2
c
            isym = 1
            write(nlist,6030) irow, ilev, a(irow), b(irow),
     &       syml(ilev), slabel(syml(ilev)), modrt(ilev),
     &       (l(i,irow),i=0,3),
     &       isym, xbar(isym,irow), (y(isym,i,irow),i=0,2),
     &       xp(isym,irow), z(isym,irow)
c
            do 100 isym = 2, nsym
               write(nlist,6040)
     &          isym, xbar(isym,irow), (y(isym,i,irow),i=0,2),
     &          xp(isym,irow), z(isym,irow)
100         continue
200      continue
         write(nlist,6050)
300   continue
c
      return
6010  format(/' level',i3,' through level',i3,' of the drt:'//
     &  1x,'row',' lev',' a',' b',' syml',' lab',' rmo',
     &  '  l0 ',' l1 ',' l2 ',' l3 ',
     &  'isym',' xbar ', '  y0  ','  y1  ','  y2  ','  xp  ','   z')
6030  format(/1x,i3,i4,i2,i2,i4,a4,i4,i5,3i4,i4,6i6)
6040  format(1x,40x,i4,6i6)
6050  format(1x,40('.'))
      end
c deck select
      subroutine select(
     & ssym,   nact,   nrow,   nela,
     & nwalk,  ncsf,   limvec, modrt,
     & step,   l,      ytot,   row,
     & wsym,   arcsym, xbar,   y,
     & mult,   *  )
c
c  control the various csf selection schemes available.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer ssym, nact, nrow, nela, nwalk, ncsf
      integer limvec(nwalk)
      integer l(0:3,nrow)
      integer ytot(0:nact),row(0:nact),step(0:nact),modrt(0:nact)
      integer wsym(0:nact),arcsym(0:3,0:nact)
      integer xbar(8,nrow),y(8,0:3,nrow)
      integer mult(8,8)
c
ctm   ndefmx maximum number of group definitions to be applied
      integer ngpmx,gldim,ndefmx
      parameter(ngpmx=50, gldim=1000, ndefmx=20 )
      integer         gpinfo,  glevel, ngroup, ndef
      common /cgroup/ gpinfo(3,ngpmx,ndefmx),glevel(gldim,ndefmx),
     .               ngroup(ndefmx), ndef
c
c
      integer   nrfmx,    refdim
      parameter(nrfmx=256,refdim=1000)
      integer       refocc,         exlev,        nref
      common /cref/ refocc(refdim), exlev(nrfmx), nref
c
      logical qgroup, qref, qindiv, qrejct
      integer ichar, i, clev, istep, rowc, rown, symc, symn, nlev,
     & iwalk, isym
      integer   yes,  no
      parameter(yes=1,no=0)
c
c     ngroup = 0
      call izero_wr(ndefmx,ngroup,1)

      nref   = 0
      symc   = 0
      qgroup = .false.
      qref   = .false.
      qindiv = .false.
      qrejct = .false.
      istep  = 0
      rowc   = 0
      rown   = 0
      symn   = 0
c
100   continue
      write(nlist,6010) nwalk
6010  format(/' initial csf selection step:'
     & /' total number of walks in the drt, nwalk=',i8)
c
      ichar = no
      call inyn( ichar, 'keep all of these walks?', *9000 )
c
      if ( ichar .eq. yes ) then
c
c        # keep all the walks in the drt.
c
         write(nlist,6020)
         ncsf = nwalk
         do 200 i = 1, nwalk
            limvec(i) = +1
200      continue
c
      else
c
c        # select a subset of the walks.
c
         write(nlist,*)
     &    'individual walks will be generated from the drt.'
300      continue
         ichar = no
         call inyn(ichar,
     &    'apply orbital-group occupation restrictions?',*100)
         qgroup = ichar .eq. yes
c
c        # get group information.
c
         if( qgroup ) call groupi(nact,modrt,*300)
         qgroup = qgroup .and. (ndef .gt. 0)
c
400      continue
         ichar = no
         call inyn( ichar,
     &    'apply reference occupation restrictions?',*300 )
         qref = ichar .eq. yes
c
c        # get reference information.
c
         if ( qref ) call refi(nact,modrt,nela,*400)
         qref = qref .and. (nref .gt. 0)
c
500      continue
         ichar = no
         call inyn( ichar, 'manually select individual walks?', *400 )
         qindiv = ichar .eq. yes
c
c        # initialize walks as invalid.  loop over all walks and
c        # determine the valid ones.  walks are determined in
c        # lexical order.
c
         ncsf = 0
         do 1010 i = 1, nwalk
            limvec(i) = 0
1010     continue
c
         do 1020 i = 0, nact
            step(i) = 4
1020     continue
c
c        # bottom level.
c
         row(0)  = 1
         ytot(0) = 0
         clev    = 0
         wsym(0) = ssym
         go to 1100
c
c        # decrement the current level.
c
1040     continue
         step(clev) = 4
         if ( clev .eq. 0 ) go to 1180
         clev = clev - 1
c
c        # next step at the current level.
c
1100     continue
         istep = step(clev) - 1
         if ( istep .lt. 0 ) go to 1040
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc)
         if(rown.eq.0)go to 1100
         symc = wsym(clev)
         symn = mult( arcsym(istep,clev), symc )
         if ( xbar(symn,rown) .eq. 0 ) go to 1100
c
         nlev = clev + 1
         row(nlev) = rown
         ytot(nlev) = ytot(clev) + y(symc,istep,rowc)
         wsym(nlev) = symn
         if ( nlev .eq. nact ) go to 1150
c
c        # increment to next level.
c
         clev = nlev
         go to 1100
c
c        # examine the current walk.
c
1150     continue
c
         iwalk = ytot(nact)+1
c
c        # check the current walk against the various selection modes.
c
         if ( qgroup ) then
            call group( step, nact, qrejct )
            if ( qrejct ) go to 1100
         endif
c
         if ( qref ) then
            call refchk( step, nact, qrejct )
            if ( qrejct ) go to 1100
         endif

         if ( qindiv ) then
            call indiv( iwalk, step, nact, qrejct, *500 )
            if ( qrejct ) go to 1100
         else
ctm
ctm       write(nkey,6011) (step(i),i=0,nact-1)
6011      format('Y /',50i1)
ctm

         endif
c
c        # walk passes all the tests.
c
         limvec(iwalk) = +1
         ncsf = ncsf + 1
         go to 1100
c
c        # exit from walk generation.
1180     continue
      endif
c
c     # initial selection complete.  begin step-vector csf selection.
c
      write(nlist,6040) ncsf, nwalk
6040  format(/' step-vector based csf selection complete.'
     & /1x,i8,' csfs selected from',i8,' total walks.'
     & //' beginning step-vector based csf selection.',
     & /' enter [step_vector/disposition] pairs:'/)
c
2010  continue
      step(0) = -1
      call iniv( step, nact,
     & 'enter the active orbital step vector, (-1/ to end)', *100 )
      if ( step(0) .lt. 0 ) go to 4000
      wsym(0) = ssym
      do 2040 nlev = 1, nact
         clev = nlev - 1
         istep = step(clev)
         if ( (istep .lt. 0) .or. (istep .gt. 3) ) then
            write(nlist,*)'0 <= step <= 3'
            go to 2010
         endif
         rowc = row(clev)
         rown = l(istep,rowc)
         if ( rown .eq. 0 ) then
            write(nlist,*)'not a valid walk'
            go to 2010
         endif
         symc = wsym(clev)
         symn = mult( arcsym(istep,clev), symc )
         if ( xbar(symn,rown) .eq. 0 ) then
            write(nlist,*)'not a valid walk'
            goto 2010
         endif
         row(nlev)  = rown
         ytot(nlev) = ytot(clev) + y(symc,istep,rowc)
         wsym(nlev) = mult( arcsym(istep,clev), symc )
2040  continue
      iwalk = ytot(nact) + 1
      isym = mult( wsym(nact), ssym )
      call indiv( iwalk, step, nact, qrejct, *2010 )
      if ( qrejct ) then
         if ( limvec(iwalk) .eq. 0 ) then
            write(nlist,6100)'walk already deleted',iwalk
         else
            limvec(iwalk) = 0
            ncsf          = ncsf - 1
            write(nlist,6100)'deleted: walk,isym,ncsf',iwalk,isym,ncsf
         endif
      else
         if ( limvec(iwalk) .eq. 0 ) then
            limvec(iwalk) = +1
            ncsf          = ncsf + 1
            write(nlist,6100)'added: walk,isym,ncsf', iwalk, isym, ncsf
         else
            write(nlist,6100)'walk already retained', iwalk
         endif
      endif
      go to 2010
c
4000  write(nlist,6050) ncsf, nwalk
6050  format(/' step-vector based csf selection complete.'
     & /1x,i8,' csfs selected from',i8,' total walks.'
     & //' beginning numerical walk selection:'
     & /' enter positive walk numbers to add walks,'
     & /' negative walk numbers to delete walks, and zero to end.'/)
4010  continue
      iwalk = 0
      call ini( iwalk, 'input walk number (0 to end)', *100 )
c
      if ( iwalk .lt. 0 ) then
c        # delete walk.
         iwalk = -iwalk
         if( iwalk .gt. nwalk ) then
            write(nlist,6100)'walk too big', iwalk
         elseif ( limvec(iwalk) .eq. 0 ) then
            write(nlist,6100)'walk already deleted', iwalk
         else
            call gstep( iwalk, ssym, nact, l, y, arcsym, mult, step )
            write(nlist,6200) (step(i),i=0,nact-1)
            limvec(iwalk) = 0
            ncsf = ncsf - 1
            write(nlist,6100)'deleted: iwalk,ncsf', iwalk, ncsf
         endif
         go to 4010
      elseif ( iwalk .gt. 0 ) then
c        # add walk.
         if ( iwalk .gt. nwalk ) then
            write(nlist,6100)'walk too big', iwalk
         elseif ( limvec(iwalk) .eq. 0 ) then
            call gstep( iwalk, ssym, nact, l, y, arcsym, mult, step )
            write(nlist,6200) (step(i),i=0,nact-1)
            limvec(iwalk) = +1
            ncsf = ncsf+1
            write(nlist,6100)'added: iwalk,ncsf', iwalk, ncsf
         else
            write(nlist,6100)'walk already retained', iwalk
         endif
         go to 4010
      endif
c
c     # finished with numerical walk number selection.
c
      write(nlist,6090) ncsf, nwalk
6090  format(/' final csf selection complete.'
     & /1x,i8,' csfs selected from',i8,' total walks.')
c
      return
c
9000  return 1
6020  format(/' all walks in the drt are initially retained.')
6100  format(1x,a,':',8i8)
6200  format(' step:',(1x,50i1))
      end
c deck gstep
      subroutine gstep( iwalk, ssym, nact, l, y, arcsym, mult, step )
c
c  generate the step vector for a given walk index.
c
c  10-jan-91 mcdrt version based on cidrt version. -rls
c  07-may-89 written by ron shepard.
c
       implicit none
      integer iwalk, ssym, nact
      integer l(0:3,*),y(8,0:3,*),arcsym(0:3,0:nact),mult(8,8)
      integer step(0:nact)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer ytot, rowc, nlev, clev, istept, istep, rown, ytemp, isym
c
      ytot  = iwalk - 1
      rowc  = 1
      isym  = ssym
      istep = 0
      rown  = 0
      ytemp = 0
      do 20 nlev = 1, nact
         clev = nlev - 1
         do 10 istept = 0, 3
            istep = istept
            rown = l(istep,rowc)
            if ( rown .ne. 0 ) then
               ytemp = ytot - y(isym,istep,rowc)
               if ( ytemp .ge. 0 ) go to 11
            endif
10       continue
c        # ...no valid exit from the loop.
         call bummer('gstep: ytemp=',ytemp,faterr)
11       continue
         ytot = ytemp
         rowc = rown
         step(clev) = istep
         isym = mult( arcsym(istep,clev), isym )
20    continue
      if ( ytot .ne. 0 ) call bummer('gstep: ytot=',ytot,faterr)
      return
      end
c deck groupi
      subroutine groupi( nact, modrt, * )
c
c  read in group information.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer nact
      integer modrt(0:nact)
c
      integer idef,  ngpmx,    gldim, ndefmx

ctm   ndefmx maximum number of group definitions to be applied

      parameter(ngpmx=50, gldim=1000, ndefmx=20 )
      integer         gpinfo,  glevel, ngroup, ndef
      common /cgroup/ gpinfo(3,ngpmx,ndefmx),glevel(gldim,ndefmx),
     .               ngroup(ndefmx), ndef
c
      integer nmpgmx, i, i2, ig, nmi, i1, i0, j
c
6100  format(1x,a,':',i8)
c
      write(nlist,6100)'maximum number of orbital group definitions'
     .  , ndefmx
      write(nlist,6100)'maximum number of orbital groups', ngpmx
c
 99   continue
      ndef =0
      call ini(ndef,'input the number orbital group definitions',
     .  *9000)
      if ( (ndef.lt.0) .or. (ndef.gt.ndefmx)) then
         write(nlist,*)'0 <= ndef <= ',ndefmx
         goto 99
      elseif (ndef.eq.0) then
         return
      endif
      do idef =1, ndef
100   continue
      write(nlist,6101) idef
6101  format(//10('<<'),' STARTING GROUP DEFINITION  ',i3,1x,
     .       10('>>')//)
      ngroup(idef) = 0
      call ini(ngroup(idef),
     .     'input the number of orbital groups',*9000)
      write(nlist,6100) 'ngroup', ngroup(idef)
      if ((ngroup(idef) .le. 0) .or. (ngroup(idef) .gt. ngpmx) ) then
         write(nlist,*)'0 <= ngroup <= ',ngpmx
         go to 100
      endif
      nmpgmx = min( nact, gldim/ngroup(idef) )
c
110   write(nlist,6100)'maximum number of orbitals in each group',nmpgmx
      write(nlist,6010)
6010  format(/' input the number of orbitals, the minimum occupation,'
     &  /' and the maximum occupation for each group.')
      call iniv( gpinfo(1,1,idef), 3*ngroup(idef),
     .     'nmopg,gmin,gmax', *100 )
      do 120 i = 1, ngroup(idef)
         if ( gpinfo(1,i,idef) .le. 0 ) go to 110
         if ( gpinfo(1,i,idef) .gt. min(nact,nmpgmx) ) go to 110
         if ( gpinfo(2,i,idef) .lt. 0 ) go to 110
         if ( gpinfo(3,i,idef) .lt. gpinfo(2,i,idef) ) go to 110
120   continue
c
      write(nlist,6050)
6050  format(1x)
c-      write(nlist,6060)'modrt(*)',(modrt(i),i=1,nact)
      write(nlist,6060)'level:',(i,i=1,nact)
6060  format(1x,a,(t15,20i3))
      i2 = 0
      do 160 ig=1,ngroup(idef)
         nmi = gpinfo(1,ig,idef)
         i1 = i2 + 1
         i2 = i2 + nmi
140      write(nlist,6030) nmi, ig
6030     format(/' input the levels of the',i3,' orbitals in group',i3)
         call iniv( glevel(i1,idef), nmi, 'glevel(*)', *110 )
         do 150 i = i1, i2
            if ( glevel(i,idef) .lt. 1 ) go to 140
            if ( glevel(i,idef) .gt. nact ) go to 140
150      continue
160   continue
c
      write(nlist,6070)
6070  format(/' group  nmopg  gmin  gmax  glevel(*)')
      i0 = 0
      do 200 ig = 1, ngroup(idef)
         nmi = gpinfo(1,ig,idef)
         write(nlist,6080)ig,
     .    (gpinfo(j,ig,idef),j=1,3),(glevel(i0+i,idef),i=1,nmi)
         i0 = i0 + nmi
200   continue
      enddo  ! idef
6080  format(1x,i4,i7,i6,i6,(t30,15i3))
      return
c
9000  return 1
c
      end
c deck group
      subroutine group( step, nact, qrejct )
c
c  determine if the csf specified in step(*) passes the
c  group occupation restrictions.
c
       implicit none
      integer nact
      integer step(nact)
      logical qrejct
c
ctm   ndefmx maximum number of group definitions to be applied

      integer ngpmx,gldim,ndefmx
      parameter(ngpmx=50, gldim=1000, ndefmx=20 )
      integer         gpinfo,  glevel, ngroup, ndef
      common /cgroup/ gpinfo(3,ngpmx,ndefmx),glevel(gldim,ndefmx),
     .               ngroup(ndefmx), ndef
c
      integer nok,idef,i2, ig, nmi, i1, neli, i
      integer stepoc(0:3)
      data stepoc/0,1,1,2/
c
c     pass over all ndef group definitions and reject only
c     those walks, that do NOT match ANY definition (.or.!)
      qrejct = .true.
      nok=0
      do 130 idef=1,ndef
      i2 = 0
      do 120 ig = 1, ngroup(idef)
         nmi = gpinfo(1,ig,idef)
         i1 = i2 + 1
         i2 = i2 + nmi
         neli = 0
         do 110 i = i1, i2
            neli = neli + stepoc(step(glevel(i,idef)))
110      continue
         if ( neli .lt. gpinfo(2,ig,idef) ) goto 130
         if ( neli .gt. gpinfo(3,ig,idef) ) goto 130
120   continue
         nok=nok+1
130   continue

        if (nok.gt.0) then
c
c     # csf is ok.
c
      qrejct = .false.
         endif
      return
      end
c deck refi
      subroutine refi( nact, modrt, nela, * )
c
c  read in reference occupation information.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer nact, nela
      integer modrt(0:nact)
c
      integer   nrfmx,    refdim
      parameter(nrfmx=256,refdim=1000)
      integer       refocc,         exlev,        nref
      common /cref/ refocc(refdim), exlev(nrfmx), nref
c
      integer refmax, i1, i2, iref, neli, i, i0
c
      neli = 0
      nref = 0
c
100   continue
      refmax = min( refdim/nact, nrfmx )
c
      write(nlist,6100)'maximum number of reference occupations',refmax
6100  format(/1x,a,':',i8)
c
      nref = 0
      call ini(nref,'input the number of reference occupations',*9000)
      write(nlist,6100)'number of reference occupations',nref
      if ( (nref .lt. 0) .or. (nref .gt. refmax) ) then
         write(nlist,*)'0 <= nref <= ',refmax
         go to 100
      elseif(nref.eq.0)then
         return
      endif
c
200   continue
      write(nlist,6100) 'nela', nela
      i2 = 0
      do 240 iref = 1, nref
         i1 = i2 + 1
         i2 = i2 + nact
210      write(nlist,6100)'reference occupation number',iref
         call iniv(refocc(i1),nact,
     &    'input the reference orbital occupation',*100)
         neli = 0
         do 220 i = i1, i2
            if ( refocc(i) .lt. 0 ) go to 210
            if ( refocc(i) .gt. 2 ) go to 210
            neli = neli + refocc(i)
220      continue
         if ( neli .ne. nela ) go to 210
240   continue
c
300   call iniv( exlev, nref,
     & 'input the excitation level for each reference',*200)
      do 310 iref = 1, nref
         if ( exlev(iref) .lt. 0 ) go to 300
310   continue
c
      i0 = 0
      do 320 iref = 1, nref
         write(nlist,6060) iref, exlev(iref), (refocc(i0+i),i=1,nact)
         i0 = i0 + nact
320   continue
6060  format(' iref=',i3,' exlev=',i3,' occ(*)=',(50i1))
c
      return
c
9000  return 1
c
      end
c deck refchk
      subroutine refchk( step, nact, qrejct )
c
c  compare csf to reference occupations.
c
       implicit none
      integer nact
      integer step(nact)
      logical qrejct
c
      integer   nrfmx,    refdim
      parameter(nrfmx=256,refdim=1000)
      integer       refocc,         exlev,        nref
      common /cref/ refocc(refdim), exlev(nrfmx), nref
c
      integer i0, iref, ex2, exr2, i
c
      integer diff(0:2,0:3)
      data diff/0,1,2, 1,0,1, 1,0,1, 2,1,0/
c
      qrejct = .false.
      i0 = 0
      do 120 iref = 1, nref
         ex2 = 0
         exr2 = 2 * exlev(iref)
         do 110 i = 1, nact
            ex2 = ex2 + diff( refocc(i0+i), step(i) )
110      continue
         i0 = i0 + nact
         if ( ex2 .le. exr2 ) return
120   continue
c
c     # maximum excitation level exceeded for all reference occupations.
c
      qrejct = .true.
c
      return
      end
c deck indiv
      subroutine indiv( iwalk, step, nact, qrejct, * )
c
c  individual csf selection.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer iwalk, nact
      integer step(nact)
      logical qrejct
c
      integer ichar
      integer   yes,  no
      parameter(yes=1,no=0)

      character*60 scr
c
      write(nlist,6010) iwalk, step
6010  format(' walk:',i8,' step(*)=',(50i1))
      write(scr,6011) step
6011  format(50i1,' keep? ')
      ichar = yes
      call inyn( ichar, scr, *9000 )
      qrejct = ichar .eq. no
c
      return
c
9000  return 1
c
      end
c deck wrthd
      subroutine wrthd(
     & ndrt,   title,  ndot,   nact,
     & nsym,   nrow,   ssym,   smult,
     & nelt,   nela,   nwalk,  ncsf,
     & buf,    lenbuf  )
c
c  write title and dimension information to drt file.
c
       implicit none
      integer ndrt, ndot, nact, nsym, nrow, ssym, smult,
     & nelt, nela, nwalk, ncsf, lenbuf
      integer buf(lenbuf)
      character*(*) title
c
      integer ninfo
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # vrsion should be incremented whenever the drt
c     # file format is changed.
      integer   vrsion
      parameter(vrsion=2)
c
c     # vrsion history:
c     #  2: 10-jan-90 expo version. header split in two records.
c     #  1: 10-jul-84 uexp2 symmetry-dependent chaining arrays.
c     #  0: 01-may-80 uexp index vector used to select symmetry walks.
c
      rewind ndrt
      write( ndrt, '(a)' ) title
c
      ninfo  = 10
c
      buf(1) = vrsion
      buf(2) = lenbuf
      buf(3) = ninfo
c
      call wrtdbl( 3, buf, 3, ' ', 'mcdrt' )
c
      buf(1)  = ndot
      buf(2)  = nact
      buf(3)  = nsym
      buf(4)  = nrow
      buf(5)  = ssym
      buf(6)  = smult
      buf(7)  = nelt
      buf(8)  = nela
      buf(9)  = nwalk
      buf(10) = ncsf
c
      call wrtdbl( lenbuf, buf, ninfo, ' ', 'info' )
c
      return
      end
c deck wrtdrt
      subroutine wrtdrt(
     & ndot,   nact,   nsym,   nrow,
     & nwalk,  lenbuf, buf,    slabel,
     & doub,   symd,   modrt,  syml,
     & nj,     njskp,  a,      b,
     & l,      y,      xbar,   xp,
     & z,      limvec, r,      step,
     & ytot,   row,    wsym,   arcsym,
     & mult,   ssym,   ncsft,  stpout   )
c
c  write the drt arrays to the drt file.
c  title and dimension information has already been written.
c  the file is assumed to be positioned correctly for the
c  next records.
c  symmetry dependent arrays and csf vectors are buffered in
c  records of length lenbuf.
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer ndot, nact, nsym, nrow, nwalk, lenbuf, ssym, ncsft, stpout
      integer buf(lenbuf)
      integer doub(*)
      integer symd(*)
      integer modrt(0:nact)
      integer syml(0:nact)
      integer nj(0:nact)
      integer njskp(0:nact)
      integer a(nrow)
      integer b(nrow)
      integer l(0:3,nrow)
      integer y(8,0:3,nrow)
      integer xbar(8,nrow)
      integer xp(8,nrow)
      integer z(8,nrow)
      integer limvec(nwalk)
      integer r(nwalk)
      integer step(0:nact), ytot(0:nact), row(0:nact), wsym(0:nact)
      integer arcsym(0:3,0:nact), mult(8,8)
      character*4 slabel(0:nsym)
c
c     # local:
      integer nlevel, bpt, irow, istep, isym, ncsf, i, clev, rowc, rown,
     & symc, symn, nlev
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    toskp,   to01
      parameter( toskp=0, to01=1 )
c
      integer    yes,   no
      parameter( yes=1, no=0 )
c
      ncsf = 0
c
      nlevel = nact + 1
c
c     # slabel(*)...
*@ifdef g95
*      write(ndrt,'(a)') '(8a4)'
*      write(ndrt,'(8a4)') (slabel(i),i=1,nsym)
*C   # use an internal read to localize the drt i/o in wrtdbl(). -rls
*@else
      do 10 i = 1, nsym
         read(slabel(i),'(a4)') buf(i)
10    continue
      call wrtdbl( lenbuf, buf, nsym, '(8a4)', 'slabel' )
*@endif
c
      if ( ndot .gt. 0 ) then
c        # doub(*)...
         call wrtdbl( lenbuf, doub, ndot, ' ', 'doub' )
c        # symd(*)...
         call wrtdbl( lenbuf, symd, ndot, ' ', 'symd' )
      endif
c
c     # modrt(*)...
      call wrtdbl( lenbuf, modrt(1), nact, ' ', 'modrt' )
c
c     # syml(*)...
      call wrtdbl( lenbuf, syml(1), nact,'(80i1)', 'syml' )
c
c     # nj(*)...
      call wrtdbl( lenbuf, nj, nlevel,' ', 'nj' )
c
c     # njskp(*)...
      call wrtdbl( lenbuf, njskp, nlevel,' ', 'njskp' )
c
c     # a(*)...
      call wrtdbl( lenbuf, a, nrow,' ', 'a' )
c
c     # b(*)...
      call wrtdbl( lenbuf, b, nrow,' ', 'b' )
c
c     # l(*,*)...
      call wrtdbl( lenbuf, l, 4*nrow,' ', 'l' )
c
c     # y(*,*,*)...
      bpt = 0
      do 130 irow = 1, nrow
         do 120 istep = 0, 3
            do 110 isym = 1, nsym
               if ( bpt .eq. lenbuf ) then
                  call wrtdbl( lenbuf, buf, bpt, ' ', 'y' )
                  bpt = 0
               endif
               bpt = bpt + 1
               buf(bpt) = y(isym,istep,irow)
110         continue
120      continue
130   continue
      call wrtdbl( lenbuf, buf, bpt, ' ', 'y' )
c
c     # xbar(*,*)...
      bpt = 0
      do 220 irow = 1, nrow
         do 210 isym = 1, nsym
            if ( bpt .eq. lenbuf ) then
               call wrtdbl( lenbuf, buf, bpt,' ', 'xbar' )
               bpt = 0
            endif
            bpt = bpt + 1
            buf(bpt) = xbar(isym,irow)
210      continue
220   continue
      call wrtdbl( lenbuf, buf, bpt, ' ', 'xbar' )
c
c     # xp(*,*)...
      bpt = 0
      do 320 irow = 1, nrow
         do 310 isym = 1, nsym
            if ( bpt .eq. lenbuf ) then
               call wrtdbl( lenbuf, buf, bpt,' ', 'xp' )
               bpt = 0
            endif
            bpt = bpt + 1
            buf(bpt) = xp(isym,irow)
310      continue
320   continue
      call wrtdbl( lenbuf, buf, bpt, ' ', 'xp' )
c
c     # z(*,*)...
      bpt = 0
      do 420 irow = 1, nrow
         do 410 isym = 1, nsym
            if ( bpt .eq. lenbuf ) then
               call wrtdbl( lenbuf, buf, bpt,' ', 'z' )
               bpt = 0
            endif
            bpt = bpt + 1
            buf(bpt) = z(isym,irow)
410      continue
420   continue
      call wrtdbl( lenbuf, buf, bpt,' ', 'z' )
c
c     # limvec(*)...
      call wrtdbl( lenbuf, limvec, nwalk, '(80i1)', 'limvec' )
c
c     # r(*)...
      call wrtdbl( lenbuf, r, nwalk, ' ', 'r' )
c
c     # check to see if step vectors should be included?
c
c     # this option will eventually be removed, and subsequent programs
c     # will be required to generate step-vectors directly from the
c     # drt arrays. -rls
c
      if ( stpout .eq. no ) return
       write(nlist,1205)
1205   format(//3x,'List of selected configurations (step vectors)'//)

c
c     # loop over all the valid walks and write out the step vector
c     # for each csf.  walks are generated in increasing order using
c     # a tree-search method.  partial walks contributing to deleted
c     # total walks are avoided by using skip vector information.
c
c     # change limvec to skip vector form.
c     # input: limvec(i)=0 for deleted walks.
c     #                 =1 for retained walks.
c     # output:limvec(i)=0 for retained walks.
c     #                 =n for deleted walks where n is the number of
c     #                    deleted walks to the next retained walk.
c
c     # e.g. input: 1 0 0 1 1 0 0 0 1 1 1 0 1 0 0 0 0 1...
c     #     output: 0 2 1 0 0 3 2 1 0 0 0 1 0 4 3 2 1 0...
c
      call skpx01( nwalk, limvec, toskp, ncsf )
c
      if ( ncsf .ne. ncsft ) then
         call bummer('wrtdrt: toskp, (ncsf-ncsft)=',
     &    (ncsf-ncsft), faterr )
      endif
c
c     # write out the format for step(*).
c
      write(ndrt,'(a)') '(80i1)'

      if ( nact.eq.0 ) then
       call bummer('there are no active orbitals defined',nact,0)
       ncsf=1
       goto 1180
      endif

c
c     # initialize step(*).
c
      do 1020 i=0,nact
         step(i)=4
1020  continue
c
c     # bottom level.
c
      ncsf    = 0
      row(0)  = 1
      ytot(0) = 0
      clev    = 0
      wsym(0) = ssym
      go to 1100
c
c     # decrement current level.
c
1040  continue
      step(clev) = 4
      if ( clev .eq. 0 ) go to 1180
      clev=clev-1
c
c     # next step.
c
1100  continue
      istep = step(clev) - 1
      if ( istep .lt. 0 ) go to 1040
      step(clev) = istep
      rowc = row(clev)
      rown = l(istep,rowc)
      if ( rown .eq. 0 ) go to 1100
      symc = wsym(clev)
      symn = mult(arcsym(istep,clev),symc)
      nlev = clev + 1
      ytot(nlev) = ytot(clev)+y(symc,istep,rowc)
      if ( limvec(ytot(nlev)+1) .ge. xbar(symn,rown) ) go to 1100
c
      row(nlev) = rown
      wsym(nlev) = symn
      if ( nlev .eq. nact ) go to 1150
c
c     # increment level.
c
      clev = nlev
      go to 1100
c
c     # write out current walk.
c
1150  continue
c
      ncsf = ncsf + 1
      write(ndrt,8010) ( step(i), i = 0, (nact-1) )
      write(nlist,1206) ncsf,(step(i), i = 0, (nact-1) )
1206  format(3x,'CSF#',i6,3x,30i2)
8010  format(80i1)
      go to 1100
c
c     # exit from walk generation.
1180  continue
c
      if ( ncsf .ne. ncsft ) then
         write(*,*) 'ncsf=',ncsf,'ncsft=',ncsft
         call bummer('wrtdrt: step(*), (ncsf-ncsft)=',
     &    (ncsf-ncsft), faterr )
      endif
c
c     # change limvec(*) back to 0/1 form.
c
      call skpx01( nwalk, limvec, to01, ncsf )
c
      if ( ncsf .ne. ncsft ) then
         call bummer('wrtdrt: to01, (ncsf-ncsft)=',
     &    (ncsf-ncsft), faterr )
      endif
c
      return
      end
c deck insvp
      subroutine insvp( chr, ichr, k, j, n, text, * )
c
c  print out the character array chr(ichr(*)), and the integer array
c  k(*) for a prompt, and read an integer array j(*).
c
c  e.g. chr(*) may be a vector of symmetry labels,
c       ichr(*) may be an integer vector of symmetry indices, and
c       k(*) may be an integer vector of reduced symmetry orbitals.
c
c  03-jan-90 inivp() modified to form insvp(). -rls
c
       implicit none
      integer         nin, nout, nlist, ndrt, nkey
      common /cfiles/ nin, nout, nlist, ndrt, nkey
c
      integer n
      character*(*) text
      integer ichr(n), k(n), j(n)
      character*(*) chr(n)
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer i, numerr, iret, ios, iwerr
c
      numerr=0
100   continue
      write(nout,6010) text
6010  format(1x,a,':')
      write(nout,6020) ( chr(ichr(i)), i = 1, n )
      write(nout,6030) k
6020  format(1x,20a4)
6030  format(1x,20i4)
c
      call qrdiv( nin, n, j, iret, ios )
c
      if(iret.eq.qok)then
         call wrtivf(nkey,j,n,' ',' ',0,iwerr)
         if(iwerr.ne.0) then
            call bummer('insvp: from wrtivf, iwerr=',iwerr,faterr)
         endif
         return
      elseif(iret.eq.qup)then
         write(nkey,*)' ^'
         return 1
      elseif(iret.eq.qend)then
         call bummer('insvp: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
         write(nlist,*)'internal error, ios=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('insvp: numerr=',numerr,faterr)
         go to 100
      elseif(iret.eq.qexerr)then
         write(nlist,*)'external error, iostat=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('insvp: numerr=',numerr,faterr)
         go to 100
      else
         call bummer('insvp: unknown code, iret=',iret,faterr)
      endif
c
      end
