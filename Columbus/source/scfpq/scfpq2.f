!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cscfpq2.f
cscfpq part=2 of 2.  scf and support routines.
cversion=5.6 last modified: 30-sep-2000
c
c*******************************************************************
c
c   this computer program contains work performed partially by the
c   argonne national laboratory theoretical chemistry group under
c   the auspices of the office of basic energy sciences,
c   division of chemical sciences, u.s. department of energy,
c   under contract w-31-109-eng-38.
c
c   these programs may not be (re)distributed without the
c   written consent of the argonne theoretical chemistry group.
c
c   since these programs are under development, correct results
c   are not guaranteed.
c
c*******************************************************************
c
c  scf and 2-configuration mcscf program ...many authors
c
c  version log:
c  09-nov-01 trnfln() added for aoints2 filename. -rls
c  30-sep-00 fix pulay-2-config. bug, clean-up. -rmp
c  20-apr-00 fix save, initialization bugs. -rmp
c  29-nov-99 scfvie, scfpq combined. dead code etc. removed. memory
c            allocation restored. level shifting modified. -rmp
c  09-sep-98 enable h,i,k,l functions, format improvements. -rmp
c  06-nov-97 enable g populations. -rmp
c  25-aug-97 revise output formats. -rmp
c  28-nov-96 bug fix in subr. vkx. -si
c  08-oct-96 closed-shell level shift code replaced, ahlrichs diis. -si
c  26-jun-96 fix allocation problems due to wrong use of wzero(). -si
c  24-may-96 r.a. type accelerations, non-supermatrix formalism. -si
c  15-nov-95 link to Vienna gradient program established. -si
c  04-may-92 merge eric stahlberg's MRPT changes. -rls
c  11-sep-91 parallel version incorporated into columbus. -rjh
c  07-jul-91 preliminary parallel version using tcgmsg. -rjh
c  25-may-91 alliant and stardent ports. minor cleanup. -rls
c  13-feb-90 stellar adaptation. (rmp)
c  19-jan-90 improve erduw, schmit; plblkt; corepq. (rmp)
c  19-oct-89 fix anc, ano arguments to gradient. (gsk)
c  11-oct-89 diis cleanup (mjmp); control parameter cleanup. (rmp)
c  09-aug-89 allocate arrays, simplify matrix algorithms, etc. (rmp)
c  09-oct-88 timer, izero, wzero, mdc, dimension control, etc. (rmp)
c  21-sep-88 sun adaptation. (eas)
c
c  things to do in this code:
c  * give filenames to all files and use trnfln().
c  * use atebyt() and forbyt() for runtime allocations.
c  * eliminate and/or localize nonportable integer*2 declarations.
c  * add support for title(*) and energy(*) SIFS arrays.
c
c  cmdc info:
c  keyword   description
c  -------   -----------
c  cray      cray code. includes "d" exponents in constants.
c  ibm       ibm (mainframe) code.
c  sgipower  silicon graphics power challenge code.
c  decalpha  dec alpha code.
c  unicos    cray unicos specific code.
c  pointer   pointer syntax.
c      generic  memory allocation using () interface.
c  parallel  tcgmsg parallel code.
c  int64     uses 64-bit integers.
c  T3E64
c
c deck driver
      subroutine driver( core, lencor, mem1, ifirst )
c
       implicit none 
       integer lencor, ifirst, mem1
      real*8 core(lencor)
*@ifdef int64
      integer i2a, i2b
*@else
*      integer*2 i2a, i2b
*@endif
      logical molcas_avail
      integer i, ia, ib, irt, i2rt
      dimension ia(8), i2a(8)
      real*8 a(2)
      equivalence (a,ia,i2a), (a(2),ib,i2b)
c
c      call wzero (lencor,core,1)
c
c     # compute ratios of working-precision word-length to
c     # integer word-lengths.
      do i=1,8
        ia(i) = i
      enddo
      irt = ib - 1
      do i=1,8
        i2a(i) = (i)
      enddo
      i2rt = i2b - 1
c
      call scfpqs(core,core,core,irt,i2rt,lencor,mem1,ifirst)
c
      return
      end
c deck scfpqs
      subroutine scfpqs(a,ia,i2a,irt,i2rt,iaup,mem1,ifirst)
c
      implicit real*8 (a-h,o-z)
      parameter ( nbfmx=1023, ntitmx = 20, nmapmx = 20, maxsym = 16 )
*@ifndef int64
*      integer*2 i2a
*@endif
      integer    mclau, mcia(nbfmx),itmp2(16),itmp(16)

      character*130 fname
      character*80 fmta, title(ntitmx), titles(ntitmx)
      character*8 bfnl(nbfmx)
c     character*24 rtitle
      real*8    rtitle(24)
      character*8 crtitle(24)
      equivalence(rtitle(1),crtitle(1))
      character*4 slabel(maxsym)
      integer*4  itmpcnt(2*nbfmx),nbt
      character*3 ityp(maxsym), mtype(nbfmx)
      logical frstim,tstsiz
      real*8  energy(10)
      integer info(6)
c                       , thet(50)
      integer ietype(10),imtype(nmapmx), inam(5)
      dimension a(iaup), ia(*), i2a(*)
c      (a, ia, i2a occupy the same space)
c
c  lp1mx  = largest l value (+1) possible in basis (5 for g fct.)
      parameter (lp1mx=9)
c
c  ilb1=no. single labels per floating-point word
c  ilb2=no. label pairs per floating-point word
c  idtr=length of integral buffer in floating-point words, not
c  counting the 2 integers
c
        logical molcas_avail
*@ifdef int64
      parameter (ilb1=1,ilb2=1,idtr=8190)
*@else
*      parameter (ilb1=4,ilb2=2,idtr=2880)
*@endif

       integer  ncorehole,nchindex,ioption
       common /coreholeinfo/ ncorehole,ioption, nchindex(2,20)

c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
      character*3 tmplabel(8)
      integer isymunit,typconv(0:6)
      logical lopen
      data typconv /1,2,4,6,9,12,16/

c
c  open output listing file
c
      nlist=6
*@ifdef ibm
*C  use preconnected unit 6
*@elif defined  parallel
*C     # everyone needs their own output file.
*      fname = 'scfls'
*      call trnfln( 1, fname )
*      call pfname( 1, fname )
*      open( nlist, status='unknown', file=fname )
*@else
      fname = 'scfls'
      call trnfln( 1, fname )
      open( nlist, status='unknown', file=fname )
*@endif
c
      call ibummr( nlist )
c
      call timer(' ', 0, jscf, nlist )
c
      frstim = .true.
      ifile1 = 4
      nin    = 3
      nout   = 2
      ifile2 = 8


*@ifdef corehole
*       call getcorehole_info
*@endif 



c
c  open input file
c
      ninput=5
*@ifdef parallel
*      fname = 'scfin'
*      call trnfln( 1, fname )
*      if ( nodeid() .ne. 0 ) call pfname( 1, fname )
*C     # make a copy of the input file. node0 -> everyone else.
*      call pfcopy( 99, 0, fname )
*      open( ninput, status='old', file=fname )
*@else
      fname = 'scfin'
      call trnfln( 1, fname )
      open( ninput, status='old', file=fname )
*@endif
c
      nmap = 2
   30 call input0(frstim,iaup,mem1,iall,iauto,idebug,idfix,ifirst,igrad,
     &  inflg,iprct,iprint,ipunch,irnd,isave,iscf,isto,itcon,itmax,
     &  itprep,i2con,ivect,ivo,micmx,ninput,nlist,title)
c     # determine whether pkints is needed ...
      if(irnd.ge.1) then
c
c       # open ifile1.  read from file 'aoints'.
c
      if (irnd.eq.2 .and. (.not. molcas_avail())) then
        call bummer('irnd=2 without molcas installation',0,2)
      endif
      if (irnd.eq.2) then
        call link_it()
        call getenvinit()
        call fioinit()
        call NameRun('RUNFILE')
        call get_dscalar('PotNuc',repnuc)
        call mcget_iscalar('nsym',mclau)
        lau=mclau
        call mcget_iarray('nbas',itmp,mclau)
        lapu=0
         do i=1,lau
         lapu=lapu+itmp(i)
         enddo
        ntitle=1
        title(1)='SEWARD INTEGRALS'
        nenrgy=1
        energy(nenrgy)=repnuc
        ietype(nenrgy)=0
        nmap=2
        jp     = 1
        js     = jp    + lau
        jnl    = js    + lapu
        nap    = jnl   + lapu
        jd     = nap   + lapu*nmap
  
        do i=1,lau
          ia(jp+i-1)=itmp(i) 
        enddo
        mclau=24
        call mcget_carray('irreps',tmplabel,mclau)
        do i=1,lau
          slabel(i)(2:4)=tmplabel(i)
          slabel(i)(1:1)=' '
        enddo
         mclau=8*lapu
c       write(6,*) 'nsym=',lau,'nbas=',itmp,'ntot=',lapu
        call mcget_carray('unique basis names',bfnl,mclau)
        mclau=lapu
c       write(6,*) 'nsym=',lau,'nbas=',itmp,'ntot=',lapu
c
c       we cannot obtain currently proper bfn-to-center and
c       orbital type map from the RUNFILE infos
c       instead use the $Project.SymInfo file (molcas.SymInfo)
c
         isymunit=-1
         do i=80,90
         inquire(unit=i,opened=lopen)
         if (.not.lopen) then
           isymunit=i
           exit
         endif
         enddo
         if (isymunit.eq.-1) then
          call bummer('could not find valid unitno (isymunit)',0,2)
         endif
         open(unit=isymunit,file='molcas.SymInfo')
         read(isymunit,*)
         read(isymunit,*)
         imtype(1)=3
         imtype(2)=4
         do i=1,lapu 
          read(isymunit,*,err=91,end=92) (itmp(j),j=1,4)
          ia(nap+i-1)=itmp(2)
          ia(nap+lapu+i-1) = typconv(itmp(3))
c #of funct, unique centre, L, M , # of sym.ad.functions , Phases
         enddo
         goto 93
 91      call bummer('molcas.SymInfo: error encountered',0,2)
 92      call bummer('molcas.SymInfo: eof encountered',0,2)
 93      continue

        do la=1,lau
          ityp(la)=slabel(la)(2:4)
        enddo
c
c       # initialize some addressing arrays. extract the bfn-to-center
c       # map vector and the bfn-to-bfntyp map vector from map(*,*).
c
        call getmap(bfnl,imtype,lapu,ia(nap),ia(jnl),ia(js),nlist,nmap)
c
c       # set ns = number of symmetry-distinct centers
c       # set mtype(i) = character center-label for each distinct center
        ns = 0
        do lap=1,lapu
          ns = max( ns, ia(js+lap-1) )
          mtype( ia(js+lap-1) ) = bfnl(lap)(1:2)//'_'
        enddo
c
        elseif (irnd.eq.3) then 
          write(6,*) 'READING one-electron integrals from AOONEINT ',
     .               'in Dalton 2.0 format'
c
c        take symmetry info from infofl
c
          open(unit=80,file='infofl',form='formatted')
            read(80,*)
            read(80,*)
            read(80,*) mclau
            read(80,97) (slabel(i)(1:4),i=1,mclau)
 97         format(8(1x,a4))
            rewind(80)
            read(80,*) 
            read(80,*) (itmp(i),i=1,mclau)
         close(80)
        ntitle=1
        title(1)='DALTON2 INTEGRALS'
c
c  for details see rdonel, wronel 
c

        CALL GPOPEN(LUONEL,'AOONEINT','OLD',' ','UNFORMATTED',IDUMMY,
     &            .FALSE.)
        READ(LUONEL) RTITLE,nsym,(itmp2(I),I = 1,mclau),repnuc
        if (iprint.ge.0) then 
        write(6,185) repnuc,nsym,(itmp2(i),i=1,nsym)
 185    format('DALTON2 INFO: repnuc=',f12.6,' nsym=',i4,
     .        ' nbpsy(*)=',8i4)
        endif 
        lau=nsym 
        lapu=0
        do i=1,lau
          if (itmp(i).ne.itmp2(i)) 
     .     call bummer('inconsistent AOONEINT and infofl file',0,2)
          lapu=lapu+itmp2(i)
        enddo
         CALL MOLLAB('SYMINPUT',LUONEL,6)
         READ (LUONEL) nbt,(itmpcnt(I),I=1,2*lapu)
         IF (NBT .NE. lapu) THEN
           call bummer('inconsistent data within SYMINPUT',0,2)
         END IF
         DO I=1,lapu 
c           write(6,186) itmpcnt(i),itmpcnt(i+lapu)
            write(bfnl(i),187) itmpcnt(i),itmpcnt(i+lapu)
            do j=1,8
               if (bfnl(i)(j:j).eq.' ') bfnl(i)(j:j)='_'
            enddo
  186       format('center:',a4,' type:',a4)
  187       format(a4,a4)
         enddo
        CALL GPCLOSE(LUONEL,'KEEP')
         if (nsym.ne.lau) call bummer('invalid dalton2 header',0,2)
         ierr=0
         do i=1,lau
           if (itmp(i).ne.itmp2(i)) ierr=ierr+1
         enddo
         if (ierr.gt.0) call bummer('invalid dalton2 basis info',0,2)


        nenrgy=1
        energy(nenrgy)=repnuc
        ietype(nenrgy)=0
        nmap=2
        jp     = 1
        js     = jp    + lau
        jnl    = js    + lapu
        nap    = jnl   + lapu
        jd     = nap   + lapu*nmap

        do i=1,lau
          ia(jp+i-1)=itmp(i)
        enddo
c       we obtain  proper bfn-to-center info only from  
c       SYMINFO file 
c
         isymunit=-1
         do i=80,90
         inquire(unit=i,opened=lopen)
         if (.not.lopen) then
           isymunit=i
           exit 
         endif
         enddo
         if (isymunit.eq.-1) then
          call bummer('could not find valid unitno (isymunit)',0,2)
         endif
         open(unit=isymunit,file='SYMINFO')
         read(isymunit,*)
         read(isymunit,*)
         imtype(1)=3
         imtype(2)=4
         do i=1,lapu
          read(isymunit,*,err=191,end=192) (itmp(j),j=1,4)
          ia(nap+i-1)=itmp(2)
          ia(nap+lapu+i-1) = typconv(itmp(3))
c #of funct, unique centre, L, M , # of sym.ad.functions , Phases
         enddo
         goto 193
 191      call bummer('dalton2 SYMINFO: error encountered',0,2)
 192      call bummer('dalton2 SYMINFO: eof encountered',0,2)
 193      continue
         close (isymunit)

        do la=1,lau
          ityp(la)=slabel(la)(2:4)
        enddo
c
c       # initialize some addressing arrays. extract the bfn-to-center
c       # map vector and the bfn-to-bfntyp map vector from map(*,*).
c
        call getmap(bfnl,imtype,lapu,ia(nap),ia(jnl),ia(js),nlist,nmap)
c
c       # set ns = number of symmetry-distinct centers
c       # set mtype(i) = character center-label for each distinct center
        ns = 0
        do lap=1,lapu
          ns = max( ns, ia(js+lap-1) )
          mtype( ia(js+lap-1) ) = bfnl(lap)(1:2)//'_'
        enddo
c


        else
        fname = 'aoints'
        call trnfln( 1, fname )
*@ifdef parallel
*        call pfname( 1, fname )
*@endif
        open (ifile1, file=fname, status='old', form='unformatted')
c
        call sifrh1(ifile1,ntitle,lau,lapu,ninfo,nenrgy,nmap,ierr)
        if (ierr.ne.0)
     $    call bummer('sifrh1 err:',ierr,faterr)
        if (ntitle.gt.ntitmx)
     $    call bummer('ntitle>ntitmx:',ntitle,faterr)
        if (ninfo.gt.6)
     $    call bummer('ninfo>5',ninfo,faterr)
        if (nenrgy.gt.10)
     $    call bummer('nenrgy>10',nenrgy,faterr)
c       allocate(bfnl(lapu))
c       allocate(mtype(lapu))
c       allocate(slabel(lau))
c       allocate(ityp(lau))
c
c       # read the second header record.
c
c       core: np,ms,mnl,map
c
        jp     = 1
        js     = jp    + lau
        jnl    = js    + lapu
        nap    = jnl   + lapu
        jd     = nap   + lapu*nmap
        call sifrh2(  ifile1,  ntitle,      lau,     lapu,    ninfo,
     $       nenrgy,    nmap,   title,   ia(jp),   slabel,     info,
     $         bfnl,  ietype,  energy,   imtype,  ia(nap),     ierr )
        if (ierr.ne.0)
     $    call bummer('sifrh2 err:',ierr,faterr)
        do la=1,lau
          ityp(la)=slabel(la)(2:4)
        enddo
c
c       # initialize some addressing arrays. extract the bfn-to-center
c       # map vector and the bfn-to-bfntyp map vector from map(*,*).
c
        call getmap(bfnl,imtype,lapu,ia(nap),ia(jnl),ia(js),nlist,nmap)
c
c       # set ns = number of symmetry-distinct centers
c       # set mtype(i) = character center-label for each distinct center
        ns = 0
        do lap=1,lapu
          ns = max( ns, ia(js+lap-1) )
cdalton2
c         mtype( ia(js+lap-1) ) = bfnl(lap)(4:5)//'_'
          mtype( ia(js+lap-1) ) = bfnl(lap)(1:3)
        enddo
       endif 
c
        repnuc = sifsce( nenrgy, energy, ietype )
      else
c
c       # open nin and nout.
c
*@ifdef parallel
*C       # processes have unique pkints and scratch files
*        fname = 'pkints'
*        call trnfln( 1, fname )
*        call pfname( 1, fname )
*        open( nin, status='old', form='unformatted', file=fname )
*        fname = 'pqints'
*        call trnfln( 1, fname )
*        call pfname( 1, fname )
*        open( nout, status='unknown', form='unformatted', file=fname )
*@else
        fname = 'pkints'
        call trnfln( 1, fname )
        open( nin, status='old', form='unformatted', file=fname )
        fname = 'pqints'
        call trnfln( 1, fname )
        open( nout, status='unknown', form='unformatted', file=fname )
*@endif
c       # read from file 'pkints'.
        read (nin) title(1), repnuc, lau, ns, lapu, nu
        jp     = 1
        js     = jp     + lau
        jnl    = js     + lapu
        jd     = jnl    + lapu + lapu*nmap
      endif
c
      ji     = jd     + lau
      jjl    = ji     + lau
      iauu=(jjl+irt-2)/irt
      if(iaup.lt.iauu) then
        write (nlist,*) 'input1: iaup too small', iauu
        call bummer('scfpqs: before input1() (iauu-iaup)=',
     &   (iauu-iaup), faterr )
      endif
c
c     core: np,ms,mnl,map,nd,ni
c
      call input1(bfnl,fmta,icmax,inam,inflg,iprct,irnd,isave,iscf,isto,
     &  ityp,ivo,lapu,lau,ia(jnl),mpu,ia(js),mtype,mu,mxpul,ia(jd),
     &  ia(ji),nin,ninput,nlist,ia(jp),ns,nu,repnuc,thrsh,title,titles)
      write(nlist,'(/a,i3)')
     1  'total number of SCF basis functions: ',lapu
      jju    = jjl    + lau
      jic    = jju    + lau
      jio    = jic    + lau
      jim1   = jio    + lau
      kiim1  = max(mpu,lau) + 1
      jan    = (jim1 + kiim1 + irt -2) / irt + 1
      janc   = jan    + lapu
      jano   = janc   + lapu
      jeig   = jano   + lapu
      kc     = jeig   + lapu
      jbn    = kc     + icmax
      jxti   = irt*(jbn+mpu-1)+1
      iauu=(jxti+mu+irt-2)/irt
      if(iaup.lt.iauu) then
        write (nlist,*) 'input2: iaup too small', iauu
        call bummer('scfpqs: before input2() (iauu-iaup)=',
     &    (iauu-iaup), faterr )
      endif
c
c     core: np,ms,mnl,map,nd,ni,ijl,iju,nic,nio,iim1,an,anc,ano,
c           eig,c,bn,ixti
c
      if(isave.le.1) call input2(a(jan),a(janc),a(jano),bfnl,a(jbn),
     1  a(kc),a(jeig),elec,fmta,iall,icmax,ia(jim1),ia(jjl),ia(jju),
     2  inflg,isave,ityp,ia(jxti),ivo,kiim1,lapu,lau,ia(jd),ia(ji),
     3  ia(jic),ninput,ia(jio),nlist,nmat,nost,ia(jp),nsm,title,titles)
c
      mnostp = (nost*nost+nost)/2
      jalpha = jbn
      jbeta  = jalpha + mnostp
      iauu   = jbeta  + mnostp - 1
      if(iaup.lt.iauu) then
        write (nlist,*) 'input3: iaup too small', iauu
        call bummer('scfpqs: before input3() (iauu-iaup)=',
     &    (iauu-iaup), faterr )
      endif
c
c     core: np,ms,mnl,map,nd,ni,ijl,iju,nic,nio,iim1,an,anc,ano,
c           eig,c,alpha,beta
c
      call input3(a(jalpha),a(jbeta),iprint,irnd,lau,nin,ninput,nlist,
     &  nost,ia(jp),title)
      idtrl=ilb1*idtr
      idtr1=(ilb1*idtr)/(2*ilb1+1)
      idtr2=(ilb1*idtr)/(ilb1+1)
      idtr3=(ilb2*idtr)/(2*ilb2+1)
      idtr4=(ilb2*idtr)/(ilb2+1)
      i1i1=idtr1/ilb1
      i1i2=i1i1+idtr1
      i2i1=idtr2/ilb1
      i3i1=idtr3/ilb2
      i3i2=i3i1+idtr3
      i4i1=idtr4/ilb2
      if(irt.eq.1) then
        lenbli=idtr3
        lenbl=idtr4
      else
        lenbli=0
        lenbl=0
      endif
c
c  allocate space for scf
c
      jorbsm = irt*iauu + 1
      if(irnd.eq.0) then
        lorbsm = 0
        liopen = 0
        lalph = 0
      else
        lorbsm = lapu
        liopen = lau
        lalph = lau*lau
      endif
      jopen  = jorbsm + lorbsm
      jmd    = jopen  + liopen
      jbskp  = jmd    + liopen
      jnbskp = jbskp  + liopen
      kalph  = (jnbskp + liopen + irt - 2)/irt + 1
      kbet   = kalph  + lalph
      kh     = kbet   + lalph
      ks     = kh     + nmat
      kt     = ks     + nmat
      kp1    = kt     + nmat    
      jx     = irt*(kp1-1)+1
      kp2    = kp1    + nmat
      kd1    = kp2    + nmat
      kd2    = kd1    + nmat
      iesh   = kd2    + nmat
      jrthtr = iesh   + lapu
      if(mxpul.ne.0) then
        lrthtr=icmax
        loldff=mxpul*nmat
        if(iprct.lt.-10) then
          lolde=loldff
          loldb=((mxpul+1)*(mxpul+2))/2
          lcnv=mpu*mpu
          lxsv=0
          lv0=0
        else
          lolde=0
          loldb=0
          lcnv=0
          lxsv=icmax
          lv0=icmax
        endif
      else
        lrthtr=0
        loldff=0
        lolde=0
        loldb=0
        lcnv=0
        lxsv=0
        lv0=0
      endif
      jolde  = jrthtr+lrthtr
      joldff = jolde +lolde
      joldb  = joldff+loldff
      jxsv   = joldb +loldb
      jv0    = jxsv  +lxsv
      jc1    = jv0   +lv0
      jc2    = jc1   +lau
      ksf11  = jc2   +lau
      ksf12  = ksf11 +nmat
      ksf21  = ksf12 +nmat
      ksf22  = ksf21 +nmat
      ksf1   = ksf22 +nmat
      ksf2   = ksf1  +nmat
      kfc    = ksf2  +nmat
      kfo    = kfc   +nmat
      jcinv  = kfo   +nmat
      jcnvc0 = jcinv +lcnv
      jep    = jcinv
      jsd    = jcnvc0+lcnv
      iauu2  = jsd+mpu-1
c
c     # place s, t, v, cor arrays in sequence for possible use in sifr1n
      kv     = kp1
      kcor   = kp2
      if(irnd.ge.1) then
c       # read in the stvc array.
        kstvc  = ks
        napin  = irt*(kcor + nmat - 1) + 1
        lntin  = napin  + lapu
        jlaoff = lntin  + (lau*lau+lau)/2
        call oneint(ifile1,ia(jim1),ia(jlaoff),info,iprint,ia(lntin),
     &    lapu,lau,ia(napin),nlist,ia(jbskp),nmat,ia(jnbskp),ia(jorbsm),
     &    ia(jp),nu,a(kstvc),ierr,irnd,a(iauu2))
        if ( ierr .ne. 0 ) then
          call bummer( 'from oneint(), ierr=', ierr, faterr )
        endif
      endif
      if(nu.gt.3.and.inam(4).eq.4) then
        kspi   = kd1
      else
        kspi   = kcor
      endif
      kspi1  = kspi  +idtr
c
c  find size of p and q supermatrices; check for adequate memory
c  for in-core processing.
c
      if(i2con.eq.2.or.irnd.ge.1) then
        kpp=iesh
        kqq=iesh
        jsmbg=jx
        jbgsm=jx
        tstsiz=.false.
      else
        icorep=(nmat*(nmat+1))/2
        icoreq=(nsm*(nsm+1))/2
        kpp   =iauu2 +1
        kqq   =kpp   +icorep
        iauu3 =kqq   +icoreq-1
        ktst=kspi1+lenbli+lenbli
        if(ktst.gt.kpp) then
          kspi  =iauu3 +1
          iauu3=iauu3+idtr
        endif
        kbl=i2rt*(kspi-1)+1
        jlbli =iauu3 +1
        llbli =jlbli +lenbli
        iauu3=llbli+lenbli-1
        jsmbg =irt*iauu3+1
        jbgsm =jsmbg +nsm
        iauu3=(jbgsm+nmat+irt-2)/irt
        tstsiz=iaup.ge.iauu3
        write (nlist,50) iauu3, iaup
   50     format('space needed for in-core processing is',i10/
     1           'space available is                    ',i10/)
      endif
c
*@ifdef parallel
*C     # only out-of-core case works in parallel. fix this later. -rls
*      tstsiz = .false.
*@endif
c
      if(tstsiz) then
c
c       (retain integrals in memory)
c       core: np,ms,mnl,map,nd,ni,ijl,iju,nic,nio,iim1,an,anc,ano,
c             eig,c,alpha,beta,h,s,t,v,cor,spi-lbl,ilbli,klbli,
c             ...,pp,qq,nsmbg,nbgsm
c         or
c             eig,c,alpha,beta,h,s,t,v,cor,
c             ...,pp,qq,spi-lbl,ilbli,klbli,nsmbg,nbgsm
c
        call corepq(a(jalpha),a(jbeta),idtrl,idtr3,ia(jim1),ia(jjl),
     1    ia(jju),inam,iprint,irnd,i3i1,i3i2,i2i1,a(kh),a(ks),a(kt),
     2    a(kv),a(kcor),a(kspi),i2a(kbl),ia(jlbli),ia(llbli),nin,nlist,
     3    nmat,nost,nsm,nu,ia(jsmbg),ia(jbgsm),icorep,icoreq,a(kpp),
     4    a(kqq))
      else
c       (store integrals on disk)
        kspi   = kspi1 -idtr
        kbl    = i2rt*(kspi-1)+1
        kspi2  = kspi1 +idtr
        kspi3  = kspi2 +idtr
        kspi4  = kspi3 +idtr
        kbl1   = i2rt*(kspi1-1)+1
        kbl2   = i2rt*(kspi2-1)+1
        kbl3   = i2rt*(kspi3-1)+1
        kbl4   = i2rt*(kspi4-1)+1
        jlbli  = kspi4 +idtr
        llbli  = jlbli +lenbli
        jlbl   = llbli +lenbli
        llbl   = jlbl  +lenbl
        iauu   = llbl+lenbl-1
        if(iaup.lt.iauu) then
          write (nlist,*) 'extlpq: iaup too small', iauu
          call bummer('scfpqs: before extlpq() (iauu-iaup)=',
     &      (iauu-iaup), faterr )
        endif
c
c       core: np,ms,mnl,map,nd,ni,ijl,iju,nic,nio,iim1,an,anc,ano,
c             eig,c,alpha,beta,h,s,t,v,cor,spi-lbl,spi1-lbl1,
c             spi2-lbl2,spi3-lbl3,spi4-lbl4,ilbli,klbli,ilbl,klbl
c
        call extlpq(a(jalpha),a(jbeta),idtr4,idtrl,idtr1,idtr3,idtr2,
     1    i4i1,ia(jim1),ia(jjl),ia(jju),inam,i1i1,i1i2,iprint,irnd,i3i1,
     2    i3i2,i2i1,nbfld,nin,nlist,nmat,nost,nout,nu,a(kh),a(ks),a(kt),
     3    a(kv),a(kcor),a(kspi),ia(jlbli),ia(llbli),ia(jlbl),ia(llbl),
     4    a(kspi1),a(kspi2),a(kspi3),a(kspi4),i2a(kbl),i2a(kbl1),
     5    i2a(kbl2),i2a(kbl3),i2a(kbl4))
        nsm=nmat
      endif
      if(irnd.eq.0) rewind nin
c
      if(inflg.lt.0) then
        khtmp =kp1
c
c       core: np,ms,mnl,map,nd,ni,ijl,iju,nic,nio,iim1,an,anc,ano,
c             eig,c,alpha,beta,h,s,htmp,p2,d1,d2,orthtr,c1,c2,
c             sf11,sf12,sf21,sf22,sf1,sf2,fc,fo,sd,pp,qq,nsmbg
c
        call guess(a(jan),bfnl,a(kc),a(jeig),a(kh),a(khtmp),ia(jim1),
     &    ityp,lau,ia(ji),nlist,ia(jp),a(ks),a(jsd),thrsh,title)
      endif
c
      kspi   = ksf1
      kbl    = i2rt*(kspi-1)+1
      iauu1  = kspi+idtr-1
      iauu   = max(iauu1,iauu2)
      if(iaup.lt.iauu) then
        write (nlist,*) 'scf: iaup too small', iauu
        call bummer('scfpqs: before scf() (iauu-iaup)=',
     &    (iauu-iaup), faterr )
      endif
c
c     core: np,ms,mnl,map,nd,ni,ijl,iju,nic,nio,iim1,an,anc,ano,
c           eig,c,alpha,beta,h,s,t,p1-nx,p2,d1,d2,orthtr,olde,oldfoc,
c           oldb,c1,c2,sf11,sf12,sf21,sf22,sf1,sf2,fc,fo,cinv-ep,
c           cinvc0,sd,pp,qq,nsmbg
c
      call scf(a(kalph),a(jalpha),a(jan),a(janc),a(jano),a(kbet),
     1  a(jbeta),bfnl,a(kc),a(jc1),a(jc2),a(jeig),elec,a(iesh),fmta,
     2  iall,iauto,icmax,idebug,idfix,idtr4,idtrl,idtr3,ifile1,i4i1,
     3  igrad,ia(jim1),ia(jju),inflg,info,i1i1,i1i2,ia(jopen),iprct,
     4  iprint,ipunch,irnd,iscf,itcon,i3i1,i3i2,itmax,itprep,i2con,i2i1,
     5  ityp,ivect,ivo,lapu,lau,ia(jmd),micmx,mxpul,nbfld,ia(jbskp),
     6  ia(jd),ia(ji),ia(jic),ninput,ia(jio),nlist,nmat,ia(jnbskp),
     7  ia(jorbsm),nost,nout,ia(jp),nsm,ia(jx),a(jrthtr),a(jolde),
     8  a(joldff),a(joldb),a(jxsv),a(jv0),slabel,title,a(kh),a(ks),
     9  a(kt),a(kp1),a(kp2),a(ksf1),a(ksf2),a(ksf11),a(ksf12),a(ksf21),
     a  a(ksf22),a(kd1),a(kd2),a(kfc),a(kfo),a(jcinv),a(jcnvc0),a(jep),
     b  a(jsd),a(kspi),i2a(kbl),a(kpp),a(kqq),ia(jsmbg),repnuc,
     c  thrsh,tstsiz)
c    b  a(jsd),thet,a(kspi),i2a(kbl),a(kpp),a(kqq),ia(jsmbg),repnuc,
      jgap   = kt
      jpgap  = jgap  +(lp1mx*ns)
      iauu   = jpgap+(lp1mx*ns)*mu-1
      if(iaup.lt.iauu) then
         write (nlist,*) 'mulpot: iaup too small', iauu
         call bummer('scfpqs: before mulpot() (iauu-iaup)=',
     &     (iauu-iaup), faterr )
      endif
c
c     core: np,ms,mnl,map,nd,ni,ijl,iju,nic,nio,iim1,an,anc,ano,
c           eig,c,alpha,beta,h,s,gap,pgap
c
      call mulpot(a(jan),a(kc),a(jgap),ia(jim1),ityp,lau,ia(jnl),ia(js),
     1  mtype,ia(ji),nlist,ia(jp),ns,a(jpgap),a(ks),title)
c
      call timer('scf required', 3, jscf, nlist)
c
c     # close file aoints
      close( ifile1 )
c
      frstim = .false.
c
      read (ninput,'(i4)') newst
      if ( newst .eq. 0 ) go to 30
c
c     # delete the pqints file.
      close(nout, status='delete')
c
      return
      end
c deck input0
      subroutine input0(frstim,lencor,mem1,iall,iauto,idebug,idfix,
     &  ifirst,igrad,inflg,iprct,iprint,ipunch,irnd,isave,iscf,isto,
     &  itcon,itmax,itprep,i2con,ivect,ivo,micmx,ninput,nlist,title)
c
c  read in and check scf input
c
      implicit real*8 (a-h,o-z)
      character*80 title
      integer ierr
      logical frstim
c
      dimension title(*)
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
      write (nlist,104)
  104 format(32x,'program "scfpq"'/
     1 28x,'columbus program system'/
     2 13x,'restricted hartree-fock scf and two-configuration mcscf'//
     3 19x,'programmed (in part) by russell m. pitzer'/
     4 28x,'version date: 30-sep-00')
c
      call who2c( 'SCFPQ', nlist )
      call headwr(nlist,'SCFPQ','5.5 ')
c
      if ( frstim ) then
c        # echo the entire input deck
         write(nlist,'(1x,a)') 'echo of the input file:'
         call echoin( ninput, nlist, ierr )
         if ( ierr .ne. 0 ) call bummer( 'from echoin(), ierr=',
     &    ierr, faterr)
         rewind ninput
      endif
c
      write(nlist,
     & '(/''workspace parameters: lcore='',i10,/'' mem1='',i11,'//
     & ''' ifirst='',i11)') lencor, mem1, ifirst
c
      irnd=0
      read (ninput,'(a)') title(3)
      write (nlist,'(/a)') title(3)
c
      read (ninput,'(21i4)')                iprct,iscf,i2con,inflg,
     &  ivect,ipunch,iprint,idebug,isave,itmax,isto,micmx,iauto,idfix,
     &  itprep,itcon,iall,ivo,igrad,idmat,irnd
      write (nlist,"(/'scf flags'/(20i4))") iprct,iscf,i2con,inflg,
     &  ivect,ipunch,iprint,idebug,isave,itmax,isto,micmx,iauto,idfix,
     &  itprep,itcon,iall,ivo,igrad,idmat,irnd
      if(itmax.le.0) itmax = 40
      if(isto.le.0) isto = 20
      irnd=max(irnd,idmat)
c
c  check for first time thru program (first state if more than one)
c
      if(frstim.and.isave.ne.0) then
        write (nlist,110)
  110     format(/'isave must be 0 for first state in run--check flags')
        call bummer('input1: illegal isave=', isave, faterr )
      endif
c
      return
      end
c deck getmap
      subroutine getmap (bfnl,imtype,lapu,map,mnl,ms,nlist,nmap)
c
c  extract mnl(*) and ms(*) from map(*,*).
c
c  25-may-91 written by russ pitzer.
c
      implicit logical (a-z)
c     # dummy:
      character*8 bfnl(*)
      integer imtype(*),lapu,map(lapu,*),mnl(lapu),ms(lapu),nlist,nmap
c     # local:
      integer mnltyp,mstyp
      parameter (mnltyp=4,mstyp=3)
      integer i,imnl,ims
c
      imnl=0
      ims=0
      do i=1,nmap
        if (imtype(i).eq.mnltyp) then
          imnl=i
        else if (imtype(i).eq.mstyp) then
          ims=i
        endif
      enddo
c
c     # extract the bfntyp(*) array from map(*,*).
      if (imnl.eq.0) then
c       # default: set everything to an s-type function.
        call iset (lapu,1,mnl,1)
      else
        call icopy_wr (lapu,map(1,imnl),1,mnl,1)
      endif
c
c     # extract the bfn-to-center array from map(*,*).
      if (ims.eq.0) then
c       # default: set everything to one center.
        call iset (lapu,1,ms,1)
      else
        call icopy_wr (lapu,map(1,ims),1,ms,1)
      endif
c
c     # write out the ms(*) and mnl(*) map vectors:
c
cthomas
      write(nlist,"('input orbital labels, i:bfnlab(i)=')")
      write(nlist,"(6(i4,':',a8))") (i,bfnl(i),i=1,lapu)
c
      write(nlist,"('bfn_to_center map(*), i:map(i)')")
      write(nlist,"(10(i4,':',i3))") (i,ms(i),i=1,lapu)
c
      write(nlist,"('bfn_to_orbital_type map(*), i:map(i)')")
      write(nlist,"(10(i4,':',i3))") (i,mnl(i),i=1,lapu)
c
      return
      end
c deck input1
      subroutine input1(bfnl,fmta,icmax,inam,inflg,iprct,irnd,isave,
     &  iscf,isto,ityp,ivo,lapu,lau,mnl,mpu,ms,mtype,mu,mxpul,nd,ni,nin,
     &  ninput,nlist,np,ns,nu,repnuc,thrsh,title,titles)
c
c  read in and check scf input
c
      implicit real*8 (a-h,o-z)
      character*130 fname
      character*80 fmta, title, titles
      character*8 bfnl
      character*3 ityp, mtype
      character*2 lblnl
c
      integer filerr, syserr, ntit
      character*4 labels(8)
c
      dimension bfnl(*),inam(*),ityp(*),mnl(*),ms(*),mtype(*),nd(*),
     &  ni(*),np(*),title(*),titles(*)
      dimension lxyzir(3),lblnl(25)
      parameter (a10=10.0d0)
      data lblnl /'1s','2p','3s','3d','4p','4f','5s','5d','5g',
     &  '6p','6f','6h','7s','7d','7g','7i','8p','8f','8h','8k',
     &  '9s','9d','9g','9i','9l'/
c
      integer   faterr
      parameter(faterr=2)
c
c  ******** definitions of variables ********
c
c     inam = pointer to the types of one-electron integrals
c     ityp = irrep labels
c     lau = number of irreps
c     lapu = total number of symmetry orbitals (so's)
c     lxyzir = irreps of angular momentum operators
c     mnl = pointer to the type of ao in an so (e.g. 1s,2p,3d)
c     ms = pointer to the label for the atom(s) the so is on
c     mtype = atom labels (e.g. hyd,oxy,mn,s)
c     nd = irrep degeneracies
c     ni = number of mo's for an irrep (in both input and output)
c     np = number of so's for an irrep
c     ns = number of symmetry-distinct types of atoms
c     nu = number of types of one-electron integrals (3,4, or 5)
c
c  ******************************************
c
c     if iprct < 0, mxpul = min(abs(iprct),8) = number of error
c                   matrices saved for diis extrapolation.
c
      if(irnd.eq.0) then
        read (nin) (nd(la), la=1,lau), (ityp(la), la=1,lau),
     1    (np(la), la=1,lau), (mtype(is), is=1,ns),
     2    (ms(lap), lap=1,lapu), (mnl(lap), lap=1,lapu),
     3    (inam(n), n=1,nu), lxyzir
        lap = 0
        do la = 1,lau
          do ip = 1,np(la)
            lap = lap + 1
            write (bfnl(lap), fmt='(i3,a3,a2)') lap, mtype(ms(lap)),
     &        lblnl(mnl(lap))
          enddo
        enddo
c
c  close nin in case coefficients are to be read from same tape.
c
        rewind nin
      else
c       # default nu, inam(*), and nd(*) values:
        nu=4
        do i=1,nu
          inam(i)=i
        enddo
        do i=1,lau
          nd(i) = 1
        enddo
      endif
c
      write(nlist,'(/a)') title(1)
      if(nu.gt.3.and.inam(4).eq.4) write (nlist,112)
  112   format(/'core potentials used')
      thrsh=a10**(-isto)
      write (nlist,
     & '(/''normalization threshold = 10**(-'',i2,'')''/''one- '//
     & 'and two-electron energy convergence criterion = 10**(-'','//
     2 'i2,'')'')') isto, iscf
      if(iprct.gt.0) then
        write (nlist,120) iprct
  120   format('iterations before extrapolation =',i3)
        mxpul=0
      else if(iprct.lt.0.and.iprct.gt.-10) then
        mxpul = -iprct
      else if(iprct.lt.0.and.iprct.gt.-20) then
        mxpul = -(iprct+10)
      else if(iprct.lt.0.and.iprct.gt.-30) then
        mxpul = -(iprct+20)
      else if(iprct.eq.0) then
        write (nlist,"('no diis/extrapolation used')")
      endif
c
c     # check that mxpul is less than or equal to 8
c
      if (mxpul.gt.8) then
        write (nlist,"('mxpul too large; mxpul set to 8')")
        mxpul = 8
      endif
csi      write (nlist,130) mxpul
csi  130 format('error matrices saved =',i3)

      if(inflg.gt.0) then
        if(inflg.ne.ninput) then
*@ifdef ibm
*C        # use the preconnected unit.
*         open(inflg)
*@else
          fname = 'mocoef'
          call trnfln( 1, fname )
          open(inflg,file=fname,form='formatted',status='unknown')
*@endif
        endif
        call moread(inflg,10,filerr,syserr,10,ntit,titles,fmta,lau,
     &    np,ni,labels,0,dummy)
      endif
      if(ivo.ne.0) write (nlist,150)
  150   format(/'improved virtual orbital calculation'//)
      if(isave.gt.1) then
        write (nlist,"(/'new state--same configuration')")
        return
      endif
      if(isave.lt.1) read (ninput,'(20i4)') (ni(la), la=1,lau)
      write (nlist,"(/'nuclear repulsion energy =',f20.13)") repnuc
      mpu=0
      mu=0
      icmax=0
      do la=1,lau
        ipu=np(la)
        mpu=max(mpu,ipu)
        mu=max(mu,ni(la))
        icmax=icmax+ipu*ipu
      enddo
      write(nlist,*)
      if (irnd.ge.1) then
        write(nlist,*)'fock matrix built from AO INTEGRALS'
        write(nlist,*)'                       ^^^^^^^^^^^^'
        if (irnd.eq.1) write(nlist,*) ' ao integrals in SIFS format '
        if (irnd.eq.2) write(nlist,*) ' ao integrals in MOLCAS format '
        write(nlist,*)'in-core processing switched off'
      else
        write(nlist,*)'fock matrix built from SUPERMATRIX'
        write(nlist,*)'                       ^^^^^^^^^^^'
      endif
      if(iprct.lt.0) then
        if(iprct.lt.-20) then
          write(nlist,
     &     '(/''DIIS SWITCHED ON (error vector is FDS-SDF)''/'//
     &     '''(turbomole subroutine)'')')
        else if(iprct.lt.-10) then
          write(nlist,"(/'DIIS SWITCHED ON (error vector is FDS-SDF)')")
        else
          write(nlist,"(/'DIIS SWITCHED ON (error vector is dF)')")
        endif
      endif
      if(iprct.ge.0) then
        if(iprct.eq.0) then
          write(nlist,"(/'DIIS/Extrapolation switched OFF')")
        else
          write(nlist,"(/'Extrapolation switched ON')")
        endif
      endif
      return
      end
c deck input2
      subroutine input2(an,anc,ano,bfnl,bn,c,eig,elec,fmta,iall,icmax,
     &  iim1,ijl,iju,inflg,isave,ityp,ixti,ivo,kiim1,lapu,lau,nd,ni,nic,
     &  ninput,nio,nlist,nmat,nost,np,nsm,title,titles)
c
c  read in and check scf input
c
      implicit real*8 (a-h,o-z)
      character*80 fmta, title, titles
      character*8 bfnl
      character*3 ityp
      dimension an(*),anc(*),ano(*),bfnl(*),bn(*),c(*),eig(*),iim1(*),
     1  ijl(*),iju(*),ityp(*),ixti(*),nd(*),ni(*),nic(*),nio(*),np(*),
     2  title(*),titles(*)
cvp ---moread
      integer filerr, syserr, ntit
      character*4 labels(8)
c
c  ******** definitions of variables ********
c
c     an = shell occupation numbers
c     anc = closed-shell occupation numbers
c     ano = open shell occupation numbers
c     c = mo coefficients
c     eig = orbital energies
c     ityp = irrep labels
c     lau = number of irreps
c     lapu = total number of symmetry orbitals (so's)
c     nd = irrep degeneracies
c     ni = number of mo's for an irrep (in both input and output)
c     nic = number of closed shells for an irrep
c     nio = number of open shells for an irrep
c     np = number of so's for an irrep
c     ns = number of symmetry-distinct types of atoms
c
c  ******************************************
c
      integer   faterr
      parameter(faterr=2)
c
      parameter (a0=0.0d0, a1=1.0d0)
c
c  set up iim1(i)=(i*(i-1))/2 array
c
      iim1(1) = 0
      do i=1,kiim1-1
        iim1(i+1)=iim1(i)+i
      enddo
c
      lap=0
      icmax=0
      nsm=0
      nmat=0
      nost=0
      elec=a0
      call izero_wr(lau,nic,1)
      call izero_wr(lau,nio,1)
      call wzero(lapu,an,1)
      call wzero(lapu,anc,1)
      call wzero(lapu,ano,1)
      do 310 la=1,lau
      ipu=np(la)
      if(ipu.eq.0) go to 310
c
      iu=ni(la)
      if(iu.eq.0) go to 260
c
c     # read occupation numbers
c
      open=a0
      full=(nd(la)+nd(la))
      read (ninput,'(40f2.0)') (an(lap+i), bn(i), i=1,iu)
      do 250 i=1,iu
        if(an(lap+i).ne.a0.and.bn(i).ne.a0) an(lap+i)=an(lap+i)/bn(i)
        if(an(lap+i).eq.a0) an(lap+i)=bn(i)
        if(full-an(lap+i)) 222,230,190
  190   if(an(lap+i)) 200,250,210
c
c       # ivo case
c
  200   if(ivo.eq.0) go to 222
        an(lap+i)=a0
        go to 220
c
c       # open-shell case
c
  210   if(an(lap+i).eq.open) go to 220
        if(open.ne.a0) go to 222
        open=an(lap+i)
  220   nio(la)=nio(la)+1
        ano(lap+i)=an(lap+i)
        go to 240
c
c       # incorrect occupation number
c
  222   write (nlist,224) i, ityp(la), an(lap+i)
  224     format(i4,a3,'occupation no. incorrect',f6.0)
        call bummer('input2: illegal occupation number, i=',i,faterr)
c
c       # closed-shell case
c
  230   nic(la)=nic(la)+1
        anc(lap+i)=an(lap+i)
c
  240   elec=elec+an(lap+i)
  250 enddo
c
      if(nio(la).ne.0) then
        nost=nost+1
        ijl(nost)=nmat+1
        nsm=nsm+iim1(ipu+1)
      endif
  260 nmat=nmat+iim1(ipu+1)
c
      if(nio(la).ne.0) iju(nost)=nmat
c
c     # obtain the mo coefficients
c
      icmxp=icmax
      icmax=icmax+ipu*ipu
      icx=icmxp
      if(iall.lt.0) iu=ipu
c
c  skip if inflg < 0 (program generates initial guess) or isave = 1
c  or iu = 0
c
      if(isave.ne.1.and.inflg.eq.0.and.iu.gt.0) then
c
c       # read in simplified initial guess (index of the symmetry
c       # orbital expected to have the most important coefficient
c       # in mo i)
c
        read (ninput,'(20i4)') (ixti(i), i=1,iu)
        call wzero((iu*ipu),c(icx+1),1)
        do i=1,iu
          c(icx+ixti(i))=a1
          icx=icx+ipu
        enddo
      endif
      lap=lap+ipu
  310 enddo
      if(inflg.gt.0) then
c
c       # read in mo coefficients from input file
c
        call moread(inflg,20,filerr,syserr,10,ntit,titles,fmta,lau,
     &       np,np,labels,icmax,c)
      endif
c
      if(inflg.gt.0.and.inflg.ne.ninput) close(inflg)
c
c  skip if inflg is negative--there are no input vectors to print
c
      if (inflg.ge.0) then
        write (nlist,"(/34x,13hinput vectors)")
        call cprint(an,bfnl,c,eig,0,ityp,lau,0,ni,nlist,np,title)
      endif
      return
      end
c deck input3
      subroutine input3(alpha,beta,iprint,irnd,lau,nin,ninput,nlist,
     &  nost,np,title)
c
c  read in and check scf input
c
      implicit real*8 (a-h,o-z)
      character*80 title
      dimension alpha(*), beta(*), np(*), title(*)
c     dimension map(21)
      dimension map(12)
c
c  ******** definitions of variables ********
c
c     alpha and beta = open-shell energy coefficients
c     lau = number of irreps
c     np = number of so's for an irrep
c
c  ******************************************
c
      if(nost.eq.0) go to 340
c
c     # set up alpha and beta values
c
      write (nlist,'(/26x,''open-shell energy coefficients''' //
     & '/15x,15hopen-shell pair,12x,5halpha,13x,4hbeta)')
      ijost=0
      do iost=1,nost
        do jost=1,iost
          ijost=ijost+1
          read (ninput,'(2(i6,1x,i3))') ian, iad, ibn, ibd
          if(iad.eq.0) iad=1
          if(ibd.eq.0) ibd=1
          fn=(ian)
          fd=(iad)
          alpha(ijost)=fn/fd
          fn=(ian*ibd-ibn*iad)
          fd=(iad*ibd)
          beta(ijost)=fn/fd
          write (nlist,328) iost, jost, ian, iad, ibn, ibd
  328     format(i21,',',i2,i20,'/',i3,i14,'/',i3)
        enddo
      enddo
  340 write (nlist,'(/a/a/)') title(1), title(3)
c
c     # reopen nin and space up to integrals.
c
      if(irnd.eq.0) then
        read (nin)
        read (nin)
      endif
c
      if(iprint.ne.0) then
        write (nlist,"(/'matrix map--lambda,p,q')")
        i=0
        call izero_wr(12,map,1)
        do la=1,lau
          do ip=1,np(la)
            do iq=1,ip
c             if(i.eq.21) then
              if(i.eq.12) then
                write (nlist,380) map
                i=0
              endif
              i=i+3
              map(i-2)=la
              map(i-1)=ip
              map(i)=iq
            enddo
          enddo
        enddo
        write (nlist,380) (map(j), j=1,i)
c 380     format(7(5x,2(i2,','),i2,5x))
  380     format(4(5x,2(i2,','),i2,5x))
      endif
      return
      end
c deck oneint
      subroutine oneint (aoints,iim1,ilaoff,info,iprint,kntin,lapu,lau,
     &  mapin,nlist,nbskp,nmat,nnbskp,norbsm,np,nu,stvc,ierr,irnd,work)
c
c  read in one-electron integrals; modified from scfarg
c
      implicit real*8 (a-h,o-z)
      integer aoints,iim1(*),ilaoff(*),info(*),kntin(*),mapin(*),
     &  nbskp(*),nnbskp(*),norbsm(*),np(*)
      real*8  stvc(4*nmat)
      real*8, allocatable :: buffer(:),values(:)
      integer, allocatable :: labels(:,:) 
      real*8  zero,work(*)
      integer irnd
c
      integer    itypea,   btypmx
      parameter( itypea=0, btypmx=6 )
      real*8     fcore(4)
      integer    btypes(0:btypmx)
      integer    last, lasta, lastb
      integer iirc
      integer nvalid,irc,icode,ione,iz
c
      character*2 nams, namt, namv, namc
      data nams, namt, namv, namc/ ' s',' t',' v',' c'/
c
c     # set the btypes(*) array:
c     #            0:s1, 1:t1, 2:v1, 3:vec, 4:vfc, 5:vref, 6:generic_h1
      data btypes/ 1,    2,    3,    4,     4,     4,      4          /
      data    zero/0.d0/

 2048 format(i19,a2,' integrals')
      nu=3
      lapu = 0
      nmat = 0
      do la = 1, lau
        nbskp(la) = lapu
        nnbskp(la) = nmat
        do lap=1,np(la)
          norbsm(lapu+lap)=la
        enddo
        lapu = lapu + np(la)
        nmat = nmat + iim1(np(la)+1)
      enddo
      do lap = 1, lapu
        mapin(lap) = lap
      enddo
c
      if (irnd.eq.2) then
      icode=6
      ione=1
      iz=0
      nu = 3
      call rdonexx(irc,icode,'Mltpl  0',ione,stvc,ione,nvalid,
     .  'ONEINT')
      if (irc.ne.0 .or. nvalid.gt.nmat) 
     . call bummer('reading overlap matrices failed',0,2)
      call rdonexx(irc,icode,'kinetic ',ione,stvc(nmat+1),ione,nvalid,
     .  'ONEINT')
      if (irc.ne.0 .or. nvalid.gt.nmat) 
     . call bummer('reading kinetic energy matrices failed',0,2)
      call rdonexx(irc,icode,'attract ',ione,stvc(2*nmat+1),ione,nvalid,
     . 'ONEINT')
      if (irc.ne.0 .or. nvalid.gt.nmat) 
     . call bummer('reading vne energy matrices failed',0,2)
      call rdonexx(irc,icode,'oneham  ',ione,stvc(3*nmat+1),ione,nvalid,
     .  'ONEINT')
      if (irc.ne.0 .or. nvalid.gt.nmat) 
     . call bummer('reading oneham     matrices failed',0,2)
      ierr=iz
      fcore(1)=0.0d0
      fcore(2)=0.0d0
      fcore(3)=0.0d0
      fcore(4)=0.0d0
c     assume no ecps
c     make it consistent  T+V=H even for DKH
      call daxpy_wr(nmat,-1.0d0,stvc(nmat+1),1,stvc(3*nmat+1),1)
      call dcopy_wr(nmat,stvc(3*nmat+1),1,stvc(2*nmat+1),1)
      call wzero(nmat,stvc(3*nmat+1),1) 
c     no effective core potentials
c
      elseif (irnd.eq.3) then
      nu = 3
 
      CALL GPOPEN(LUONEL,'AOONEINT','OLD',' ','UNFORMATTED',IDUMMY,
     &            .FALSE.)
      fcore(1)=0.0d0
      fcore(2)=0.0d0
      fcore(3)=0.0d0
      fcore(4)=0.0d0
      call getdalton1('OVERLAP ',LUONEL,stvc(1),nmat,
     .         norbsm,nbskp,nnbskp,lau)
      call getdalton1('KINETINT',LUONEL,stvc(nmat+1),nmat,
     .        norbsm,nbskp,nnbskp,lau)
      call getdalton1('ONEHAMIL',LUONEL,stvc(3*nmat+1),nmat,
     .        norbsm,nbskp,nnbskp,lau)
      CALL GPCLOSE(LUONEL,'KEEP')
      fcore(1)=0.0d0
      fcore(2)=0.0d0
      fcore(3)=0.0d0
      fcore(4)=0.0d0
c     assume no ecps
c     make it consistent  T+V=H even for DKH
      call daxpy_wr(nmat,-1.0d0,stvc(nmat+1),1,stvc(3*nmat+1),1)
      call dcopy_wr(nmat,stvc(3*nmat+1),1,stvc(2*nmat+1),1)
      call wzero(nmat,stvc(3*nmat+1),1)
c     no effective core potentials

      elseif (irnd.eq.1) then
      allocate(buffer(info(2)),values(info(3)),labels(4,info(3)))
      call wzero (info(2),buffer,1)
      call wzero (info(3),values,1)
      call wzero (4*nmat,stvc,1)
      call wzero (4,fcore,1)
      call izero_wr(4*info(3),labels,1)
c
c  arrays for contr3
c
      call sifr1n( aoints,   info, itypea, btypmx, btypes, buffer,
     &     values, labels,    lau,     np, ilaoff,  mapin,   nmat,
     &       stvc,  fcore, norbsm,  kntin,  lasta,  lastb,   last,
     &       nrec,   ierr )
c
c ierr.eq.-4 indicates that integrals for a value of btypes are not on
c the file aoints (usually core potential integrals)
      if (ierr.eq.-4) ierr=0
c
c     # check for nonzero effective core integrals:
c     # nu=3 for stv; nu=4 for stvc.
      do i = 1, nmat
        if ( stvc(3*nmat+i) .ne. zero ) go to 20
      enddo
      nu = 3
       deallocate(values,labels,buffer)
      else
       call bummer('invalid irnd ',irnd,2)
      endif
c
 20   if(fcore(1).ne.zero) write (nlist,
     &  '(/19h oneint:   score  =,e25.15)') fcore(1)
      if(fcore(2).ne.zero) write (nlist,
     &  '(/19h oneint:   tcore  =,e25.15)') fcore(2)
      if(fcore(3).ne.zero) write (nlist,
     &  '(/19h oneint:   vcore  =,e25.15)') fcore(3)
      if(fcore(4).ne.zero) write (nlist,
     &  '(/19h oneint:  vecore  =,e25.15)') fcore(4)
c
      write (nlist,2048) nmat, nams
      if(iprint.ge.1) write (nlist,'(4g18.8)')
     $ (stvc(i),i=1,nmat)
      write (nlist,2048) nmat, namt
      if(iprint.ge.2) write (nlist,'(4g18.8)')
     $ (stvc(i),i=nmat+1,2*nmat)
      write (nlist,2048) nmat, namv
      if(iprint.ge.2) write (nlist,'(4g18.8)')
     $ (stvc(i),i=2*nmat+1,3*nmat)
      write (nlist,2048) nmat, namc
      if(iprint.ge.2) write (nlist,'(4g18.8)')
     $ (stvc(i),i=3*nmat+1,4*nmat)
c check for positive semi-definite matrix
      ispt=1
      invalid=0
      do la = 1, lau
        call dcopy_wr(np(la)*(np(la)+1)/2,stvc(ispt),1,work,1)
        call erduw(work,work((np(la)*(np(la)+1))/2+1),np(la),1.d-6)
        ippt=0
        do ip=1,np(la)
          ippt=ippt+ip
         if (work(ippt).lt. 1.d-6) then
          write(6,'(a,i3,a,i3,a,f14.9)')
     .     'WARNING: eigenvalues (S) #',ip,' irrep#',la,' = ',work(ippt)
          invalid=invalid+1
         endif
        enddo
       ispt=ispt+(np(la)*(np(la)+1))/2
      enddo
      if (invalid.gt.0) then 
       call bummer('# eigenvalues(S).lt.-1.d-8',invalid,0)
      else
       write(6,*) 'check on positive semi-definiteness of S passed .'
      endif
      return
      end
c deck corepq
      subroutine corepq(alpha,beta,idtrl,idtr3,iim1,ijl,iju,inam,iprint,
     1  irnd,i3i1,i3i2,i2i1,h,s,t,v,cor,spi,lbl,ilbli,klbli,nin,nlist,
     2  nmat,nost,nsm,nu,nsmbg,nbgsm,icorep,icoreq,pp,qq)
c
c  read in p and k integrals; put p and q integrals into arrays.
c
      implicit real*8 (a-h,o-z)
      dimension alpha(*),beta(*),iim1(*),ijl(*),iju(*),inam(*),h(*),
     1  s(*),t(nmat),v(nmat),cor(nmat),pp(*),qq(*),nsmbg(*),nbgsm(*)
      dimension spi(*),lbl(idtrl),ilbli(*),klbli(*)
c     (spi and lbl occupy the same space)
c
*@ifdef int64
      integer rechts,und, lbl
*@if defined( t3e64 ) || defined ( cray )
*       rechts(i,j) = shiftr(i,j)
*       und(i,j)    =and(i,j)
*@else
        rechts(i,j) = ishft(i,-j)
        und(i,j)    = iand(i,j)
*@endif
*@else
*      integer*2 lbl
*@endif
c
      call wzero (icorep,pp,1)
      if (icoreq.gt.0) call wzero(icoreq,qq,1)
c
c  form arrays nsmbg and nbgsm
c
      nsm = 0
      do iost = 1,nost
        do nbg = ijl(iost),iju(iost)
          nsm = nsm + 1
          nbgsm(nbg) = nsm
          nsmbg(nsm) = nbg
        enddo
      enddo
c
      npp = 0
      nqq = 0
      ibfld = 0
c
c  read matrices s,t,v,cor and calculate h matrix
c
      call instvc(idtrl,inam,iprint,irnd,i2i1,nin,nlist,nmat,nu,h,s,t,v,
     &  cor,spi,lbl)
c
c  loop over batches of input electron repulsion integrals
c
   55 read (nin) iblk, nbuf, lbl
      ibfld=ibfld+1
*@ifdef int64
      do ibuf=1,nbuf
        ilbli(ibuf)=rechts(lbl(ibuf),32)
        klbli(ibuf)=und(lbl(ibuf),4294967295)
      enddo
      if(iprint.ge.4) write (nlist,2060) ibfld, nbuf, nbuf,
     1  (ilbli(i), klbli(i), spi(i+i3i1), spi(i+i3i2), i=1,nbuf)
*@else
*      if(iprint.ge.4) write (nlist,2060) ibfld, nbuf, nbuf, (lbl(i),
*     1  lbl(i+idtr3), spi(i+i3i1), spi(i+i3i2), i=1,nbuf)
*@endif
c
      do 90 ibuf = 1, nbuf
*@ifdef int64
        ij=ilbli(ibuf)
        kl=klbli(ibuf)
*@else
*        ij=lbl(ibuf)
*        kl=lbl(ibuf+idtr3)
*@endif
        if(ij.eq.kl) then
          if(nost.ne.0) then
            do 65 iost=1,nost
              if(ij.lt.ijl(iost)) go to 70
              if(ij.le.iju(iost)) then
c
c               # type 1 output integrals: diagonal (ij=kl) p and q
c
                ijq = nbgsm(ij)
                ijost = iim1(iost+1)
                nplcp = (ij*(ij+1)) / 2
                nplcq = (ijq*(ijq+1)) / 2
                pp(nplcp) = spi(ibuf + i3i1)
                qq(nplcq) = alpha(ijost) * spi(ibuf+i3i1) +
     1                      beta (ijost) * spi(ibuf+i3i2)
                npp = npp + 1
                nqq = nqq + 1
                go to 90
              endif
   65       enddo
   70       continue
          endif
c
c         # type 2 output integrals: diagonal (ij=kl) p only
c
          nplcp = (ij*(ij+1)) / 2
          pp(nplcp) = spi(ibuf +i3i1 )
          npp = npp + 1
        endif
        if(ij.ne.kl) then
          if(nost.ne.0) then
            do 80 iost=1,nost
              if(ij.lt.ijl(iost)) go to 85
              if(ij.le.iju(iost)) then
                do 75 jost=1,iost
                  if(kl.lt.ijl(jost)) go to 85
                  if(kl.le.iju(jost)) then
c
c                   # type 3 output integrals: offdiag (ij.ne.kl)
c                   # p and q
c
                    ijq = nbgsm(ij)
                    klq = nbgsm(kl)
                    nplcp = (ij*(ij-1)) / 2  + kl
                    nplcq = (ijq*(ijq-1)) / 2  + klq
                    ijost = iim1(iost) + jost
                    pp(nplcp) = spi(ibuf+i3i1)
                    qq(nplcq) = alpha(ijost)*spi(ibuf+i3i1) +
     1                          beta (ijost)*spi(ibuf+i3i2)
                    npp = npp + 1
                    nqq = nqq + 1
                    go to 90
                  endif
   75           enddo
              endif
   80       enddo
   85       continue
          endif
c
c         # type 4 output integrals: offdiag (ij.ne.kl) p only
c
          nplcp = (ij*(ij-1)) / 2  + kl
          pp(nplcp) = spi(ibuf+i3i1)
          npp = npp + 1
        endif
   90 enddo
      if(iblk.eq.0) go to 55
c
c  print integrals if iprint.ge.3
c
      if(iprint.ge.3) then
c
c       (print p)
c
        call plblkt('p supermatrix:',pp,nmat,'    ',3,nlist)
c
c       (print q)
c
        if (nsm.ne.0) then
          write (nlist,94) (nsmbg(i), i=1,nsm)
   94       format(/10x,'mapping of large q to small q:'/(10i8))
          call plblkt('condensed q supermatrix:',qq,nsm,'    ',3,nlist)
        endif
c
      endif
      write (nlist,"(i19,' p and',i8,' q integrals')") npp,nqq
      return
 2060 format(11hinput block,i5,i15,6h p and,i5,14h k/2 integrals/
     1  (7x,3(i6,i5,1p,2g14.6)))
      end
c deck extlpq
      subroutine extlpq(alpha,beta,idtr4,idtrl,idtr1,idtr3,idtr2,i4i1,
     1  iim1,ijl,iju,inam,i1i1,i1i2,iprint,irnd,i3i1,i3i2,i2i1,nbfld,
     2  nin,nlist,nmat,nost,nout,nu,h,s,t,v,cor,spi,ilbli,klbli,ilbl,
     3  klbl,sp1,sp2,sp3,sp4,lbl,lbl1,lbl2,lbl3,lbl4)
c
c  read in p and k integrals; write p and q integrals onto disk.
c
      implicit real*8 (a-h,o-z)
      dimension alpha(*),beta(*),iim1(*),ijl(*),iju(*),inam(*),h(*),
     1  s(*),t(nmat),v(nmat),cor(nmat),spi(*),sp1(*),sp2(*),sp3(*),
     2  sp4(*),lbl(idtrl),lbl1(idtrl),lbl2(idtrl),lbl3(idtrl),
     3  lbl4(idtrl),ilbli(*),klbli(*),ilbl(*),klbl(*)
c     (spi and lbl occupy the same space, as do sp1 and lbl1,
c       sp2 and lbl2, sp3 and lbl3, sp4 and lbl4)
c
*@ifdef int64
      integer rechts,und
      integer  ij,kl,lbl,lbl1,lbl2,lbl3,lbl4 , sp3, sp4
*@if defined( t3e64) || defined ( cray )
*       rechts(i,j) = shiftr(i,j)
*       und(i,j)    =and(i,j)
*@else
        rechts(i,j) = ishft(i,-j)
        und(i,j)    = iand(i,j)
*@endif
*@else
*      integer*2 ij, kl, lbl, lbl1, lbl2, lbl3, lbl4
*@endif
      if(irnd.eq.0) rewind ( nout )
      iblk1=1
      iblk2=2
      iblk3=3
      iblk4=4
      n1=0
      n2=0
      n3=0
      n4=0
      np=0
      nq=0
      ibfld=0
      nbfld=0
c
c  read matrices s,t,v,cor and calculate h matrix
c
      call instvc(idtrl,inam,iprint,irnd,i2i1,nin,nlist,nmat,nu,h,s,t,v,
     &  cor,spi,lbl)
c
      if( irnd.ge.1 ) return
c
c  loop over batches of input electron repulsion integrals
c
   34 read (nin) iblk, nbuf, lbl
      ibfld=ibfld+1
*@ifdef int64
      do ibuf=1,nbuf
        ilbli(ibuf)=rechts(lbl(ibuf),32)
        klbli(ibuf)=und(lbl(ibuf),4294967295)
      enddo
      if(iprint.ge.4) write (nlist,2060) ibfld, nbuf, nbuf,
     1  (ilbli(i), klbli(i), spi(i+i3i1), spi(i+i3i2), i=1,nbuf)
*@else
*      if(iprint.ge.4) write (nlist,2060) ibfld, nbuf, nbuf, (lbl(i),
*     1  lbl(i+idtr3), spi(i+i3i1), spi(i+i3i2), i=1,nbuf)
*@endif
      do 66 ibuf = 1, nbuf
*@ifdef int64
      ij=ilbli(ibuf)
      kl=klbli(ibuf)
*@else
*      ij=lbl(ibuf)
*      kl=lbl(ibuf+idtr3)
*@endif
      if(ij.ne.kl) go to 46
      if(nost.eq.0) go to 42
      do 40 iost=1,nost
        if(ij.lt.ijl(iost)) go to 42
        if(ij.gt.iju(iost)) go to 40
c
c       # type 1 output batches: 1 label, 2 integrals
c
        if(n1.eq.idtr1) then
          nbfld=nbfld+1
          write (nout) iblk1, n1, lbl1
          if(iprint.ge.3) write (nlist,2064) nbfld, n1, n1, (lbl1(i),
     1      sp1(i+i1i1), sp1(i+i1i2), i=1,n1)
          nq=nq+idtr1
          n1=0
        endif
        n1=n1+1
        lbl1(n1)=ij
        sp1(n1+i1i1)=spi(ibuf+i3i1)
        ijost=iim1(iost+1)
        sp1(n1+i1i2)=alpha(ijost)*spi(ibuf+i3i1)+beta(ijost)*
     1    spi(ibuf+i3i2)
        go to 66
   40 enddo
c
c     # type 2 output batches: 1 label, 1 integral
c
   42 if(n2.eq.idtr2) then
        nbfld=nbfld+1
        write (nout) iblk2, n2, lbl2
        if(iprint.ge.3) write (nlist,2072) nbfld, n2, (lbl2(i),
     1    sp2(i+i2i1), i=1,n2)
        np=np+idtr2
        n2=0
      endif
      n2=n2+1
      lbl2(n2)=ij
      sp2(n2+i2i1)=spi(ibuf+i3i1)
      go to 66
c
   46 if(nost.eq.0) go to 58
      do 56 iost=1,nost
      if(ij.lt.ijl(iost)) go to 58
      if(ij.gt.iju(iost)) go to 56
      do 54 jost=1,iost
      if(kl.lt.ijl(jost)) go to 58
      if(kl.gt.iju(jost)) go to 54
c
c     # type 3 output batches: 2 labels, 2 integrals
c
      if(n3.eq.idtr3) then
        nbfld=nbfld+1
        write (nout) iblk3, n3, lbl3
        if(iprint.ge.3) then
*@ifdef int64
          do i=1,n3
            ilbl(i)=rechts(sp3(i),32)
            klbl(i)=und(sp3(i),4294967295)
          enddo
          write (nlist,2080) nbfld, n3, n3, (ilbl(i), klbl(i),
*@else
*          write (nlist,2080) nbfld, n3, n3, (lbl3(i), lbl3(i+idtr3),
*@endif
     1      sp3(i+i3i1), sp3(i+i3i2), i=1,n3)
        endif
        nq=nq+idtr3
        n3=0
      endif
      n3=n3+1
      lbl3(n3)=lbl(ibuf)
*@ifndef int64
*      lbl3(n3+idtr3)=lbl(ibuf+idtr3)
*@endif
      sp3(n3+i3i1)=spi(ibuf+i3i1)
      ijost=iim1(iost)+jost
      sp3(n3+i3i2)=alpha(ijost)*spi(ibuf+i3i1)+beta(ijost)*
     1  spi(ibuf+i3i2)
      go to 66
c
   54 enddo
   56 enddo
c
c     # type 4 output batches: 2 labels, 1 integral
c
   58 if(n4.eq.idtr4) then
        nbfld=nbfld+1
        write (nout) iblk4, n4, lbl4
        if(iprint.ge.3) then
*@ifdef int64
          do i=1,n4
            ilbl(i)=rechts(sp4(i),32)
            klbl(i)=und(sp4(i),4294967295)
          enddo
          write (nlist,2088) nbfld,n4,(ilbl(i),klbl(i),
*@else
*          write (nlist,2088) nbfld,n4,(lbl4(i),lbl4(i+idtr4),
*@endif
     1      sp4(i+i4i1), i=1,n4)
        endif
        np=np+idtr4
        n4=0
      endif
      n4=n4+1
      lbl4(n4)=lbl(ibuf)
*@ifndef int64
*      lbl4(n4+idtr4)=lbl(ibuf+idtr3)
*@endif
      sp4(n4+i4i1)=spi(ibuf+i3i1)
   66 enddo
c
      if(iblk.eq.0) go to 34
c
c  write out last batches of integrals
c
      if(n1.ne.0) then
        nbfld=nbfld+1
        write (nout) iblk1, n1, lbl1
        if(iprint.ge.3) write (nlist,2064) nbfld, n1, n1, (lbl1(i),
     1    sp1(i+i1i1), sp1(i+i1i2), i=1,n1)
      endif
      if(n2.ne.0) then
        nbfld=nbfld+1
        write (nout) iblk2, n2, lbl2
        if(iprint.ge.3) write (nlist,2072) nbfld, n2, (lbl2(i),
     1    sp2(i+i2i1), i=1,n2)
      endif
      if(n3.ne.0) then
        nbfld=nbfld+1
        write (nout) iblk3, n3, lbl3
        if(iprint.ge.3) then
*@ifdef int64
          do i=1,n3
            ilbl(i)=rechts(sp3(i),32)
            klbl(i)=und(sp3(i),4294967295)
          enddo
          write (nlist,2080) nbfld, n3, n3, (ilbl(i), klbl(i),
*@else
*          write (nlist,2080) nbfld, n3, n3, (lbl3(i), lbl3(i+idtr3),
*@endif
     1      sp3(i+i3i1), sp3(i+i3i2), i=1,n3)
        endif
      endif
      if(n4.ne.0) then
        nbfld=nbfld+1
        write (nout) iblk4, n4, lbl4
        if(iprint.ge.3) then
*@ifdef int64
          do i=1,n4
            ilbl(i)=rechts(sp4(i),32)
            klbl(i)=und(sp4(i),4294967295)
          enddo
          write (nlist,2088) nbfld, n4, (ilbl(i), klbl(i),
*@else
*          write (nlist,2088) nbfld, n4, (lbl4(i), lbl4(i+idtr4),
*@endif
     1      sp4(i+i4i1), i=1,n4)
        endif
      endif
c
      nq=nq+n1+n3
      np=np+nq+n2+n4
      write (nlist,2096) np, nq, nbfld
      rewind (nout)
      return
c
 2060 format(/11hinput block,i5,i15,6h p and,i5,14h k/2 integrals/
     1  (7x,3(i6,i5,1p,2g14.6)))
 2064 format(/12houtput block,i5,i15,6h p and,i5,12h q integrals/
     1  4(i5,1p,2g14.6))
 2072 format(/'output block',i5,i15,' p integrals'/(5x,6(i6,1p,g14.6)))
 2080 format(/12houtput block,i5,i15,6h p and,i5,12h q integrals/
     1  (7x,3(i6,i5,1p,2g14.6)))
 2088 format(/12houtput block,i5,i15,12h p integrals/(3x,5(i6,i5,
     1  1p,g14.6)))
 2096 format(i19,6h p and,i8,15h q integrals in,i6,7h blocks)
      end
c deck instvc
      subroutine instvc(idtrl,inam,iprint,irnd,i2i1,nin,nlist,nmat,nu,h,
     &  s,t,v,cor,spi,lbl)
c
c  read in one-electron integrals and place them in arrays
c
      implicit real*8 (a-h,o-z)
*@ifdef  int64
       integer lbl
*@else
*      integer*2  lbl
*@endif
      character*2 nams, namt, namv, namc, namh
      dimension inam(*),h(*),s(*),t(*),v(*),cor(*),spi(*),lbl(idtrl)
c     (spi and lbl occupy the same space)
      data nams, namt, namv, namc, namh/ ' s',' t',' v',' c',' h'/
c
      if(irnd.ge.1) go to 26
c
c  read in overlap integrals
c
      call wzero(nmat,s,1)
   12 read (nin) iblk, nbuf, lbl
      do ibuf=1,nbuf
        s(lbl(ibuf))=spi(ibuf+i2i1)
      enddo
      if(iblk.eq.0) go to 12
      write (nlist,2048) nmat, nams
      if(iprint.ge.1) write (nlist,'(4g18.8)') (s(i), i=1,nmat)
c
c  read in kinetic energy integrals
c
      call wzero(nmat,t,1)
   16 read (nin) iblk, nbuf, lbl
      do ibuf=1,nbuf
        t(lbl(ibuf))=spi(ibuf+i2i1)
      enddo
      if(iblk.eq.0) go to 16
      write (nlist,2048) nmat, namt
      if(iprint.ge.2) write (nlist,'(4g18.8)') (t(i), i=1,nmat)
c
c  read in nuclear attraction integrals
c
      call wzero(nmat,v,1)
   20 read (nin) iblk, nbuf, lbl
      do ibuf=1,nbuf
        v(lbl(ibuf))=spi(ibuf+i2i1)
      enddo
      if(iblk.eq.0) go to 20
      write (nlist,2048) nmat, namv
      if(iprint.ge.2) write (nlist,'(4g18.8)') (v(i), i=1,nmat)
c
c  read in core potential integrals, if present
c
      if(nu.eq.3.or.inam(4).ne.4) go to 30
c
      call wzero(nmat,cor,1)
   24 read (nin) iblk, nbuf, lbl
      do ibuf=1,nbuf
        cor(lbl(ibuf))=spi(ibuf+i2i1)
      enddo
      if(iblk.eq.0) go to 24
      write (nlist,2048) nmat, namc
      if(iprint.ge.2) write (nlist,'(4g18.8)') (cor(i), i=1,nmat)
c
   26 do i=1,nmat
        v(i)=v(i)+cor(i)
      enddo
c
c  form h integrals
c
   30 do i=1,nmat
        h(i)=t(i)+v(i)
      enddo
      write (nlist,2048) nmat, namh
      if(iprint.ge.1) write (nlist,'(4g18.8)') (h(i), i=1,nmat)
      return
 2048 format(i19,a2,' integrals')
      end
c deck guess
      subroutine guess(an,bfnl,c,eig,h,htmp,iim1,ityp,lau,ni,nlist,np,s,
     &  sd,thrsh,title)
c
c  form diagonal guess
c
      implicit real*8 (a-h,o-z)
      character*80 title
      character*8 bfnl
      character*3 ityp
      parameter (a1m6=1.0d-6, a1=1.0d0)
      dimension an(*),bfnl(*),c(*),eig(*),h(*),htmp(*),iim1(*),ityp(*),
     &  ni(*),np(*),s(*),sd(*),title(*)
c
      lapi = 0
      ispt = 1
      icpt = 1
      do 20 la=1,lau
        ipu = np(la)
        if ( ipu .eq. 0 ) go to 20
c  fill unit matrix
        call wzero((ipu*ipu),c(lapi+1),1)
        do ip=1,ipu
          c((lapi-ipu)+(ipu+1)*ip)=a1
        enddo
        lapi=lapi+(ipu*ipu)
   20 enddo
c
c  orthogonalize guess
c
      call schmit(c,iim1,1,lau,ni,nlist,np,s,thrsh,sd)
c
c  for each symmetry...
c
      ihpt = 0
      icpt = 0
      iept = 0
c
      do 40 la= 1, lau
        ipu = np( la )
        if ( ipu .eq. 0 ) go to 40
c
c       # transform h matrix to orthogonal basis
c
        call tritrn(ipu,htmp(ihpt+1),h(ihpt+1),c(icpt+1),sd)
c
c       # diagonalize transformed h matrix
c
        call erduw(htmp(ihpt+1),c(icpt+1),ipu,a1m6)
c
c       # pick out eigenvalues from diagonal of h matrix
c
        iz = 0
        do i= 1, ipu
          iz = iz + i
          eig( iept+i ) = htmp( ihpt+iz )
        enddo
c
c       # order eigenvalues and eigenvectors
c
        call bubble ( c( icpt+1 ), eig( iept+1 ), ipu, ipu )
c
        ihpt = ihpt + iim1(ipu+1)
        icpt = icpt + ipu*ipu
        iept = iept + ipu
c
   40 enddo
c
c  print all vectors and energies
c
      write (nlist,
     & '(/11x,''starting vectors from diagonalization of '//
     & 'one-electron terms'')')
      call cprint(an,bfnl,c,eig,1,ityp,lau,1,ni,nlist,np,title)
c
c  compact coefficients to retain occupied orbitals only
c
      ipt = 0
      jpt = 0
c
      do la= 1, lau
        iu = ni( la )
        ipu = np( la )
c
        do i= 1, iu
c  (for Cray vector compilers)
cdir$ ivdep
          do j= 1, ipu
            ipt = ipt + 1
            jpt = jpt + 1
            c( ipt ) = c( jpt )
          enddo
        enddo
c
        jpt = jpt + ( ipu-iu )*ipu
      enddo
c
      return
      end
c deck scf
      subroutine scf(alph,alpha,an,anc,ano,bet,beta,bfnl,c,c1,c2,eig,
     1  elec,esh,fmta,iall,iauto,icmax,idebug,idfix,idtr4,idtrl,idtr3,
     2  ifile1,i4i1,igrad,iim1,iju,inflg,info,i1i1,i1i2,iopen,iprct,
     3  iprint,ipunch,irnd,iscf,itcon,i3i1,i3i2,itmax,itprep,i2con,i2i1,
     4  ityp,ivect,ivo,lapu,lau,lmd,micmx,mxpul,nbfld,nbskp,nd,ni,nic,
     5  ninput,nio,nlist,nmat,nnbskp,norbsm,nost,nout,np,nsm,nx,orthtr,
     6  olde,oldfoc,oldb,xsv,v0,slabel,title,h,s,t,p1,p2,sf1,sf2,sf11,
     7  sf12,sf21,sf22,d1,d2,fc,fo,cinv,cinvc0,ep,sd,spi,lbl,pp,qq,
     8  nsmbg,repnuc,thrsh,tstsiz)
c    7  sf12,sf21,sf22,d1,d2,fc,fo,cinv,cinvc0,ep,sd,thet,spi,lbl,pp,qq,
c
c  initialize and run scf iterations
c
      implicit real*8 (a-h,o-z)
*@ifndef int64
*      integer*2 lbl
*@endif
c
c     # this is to allow conversion of data between different machines.
c include 'msgtypesf.h'
c $Header: /home/harrison/c/ipcv3.1/RCS/msgtypesf.h,
c          v 1.2 90/10/19 18:30:29 harrison Exp Locker: harrison $
c
c  This defines bit masks that can be OR'ed with user types (1-32767)
c  to indicate the nature of the data to the message passing system
c
*@ifdef parallel
*      integer    MSGDBL,       MSGINT,        MSGCHR
*@ifdef ipsc
*      PARAMETER (MSGDBL=0,     MSGINT=0,      MSGCHR=0)
*@else
*      PARAMETER (MSGDBL=65536, MSGINT=131072, MSGCHR=262144)
*@endif
*@endif
c end of include
c
      character*130 fname
      character*80 fmta, title
      character*8 bfnl
      character*4 slabel
      character*3 ityp
      logical tstsiz
c
      character*80 derfmt
      integer filerr, syserr
      integer idump
      integer me
c
      integer   faterr
      parameter(faterr=2)
c
*@ifdef parallel
*      integer  nodeid
*      external nodeid
*@endif
       character*20 fordint
       logical     lexists
       integer     iordint
c
      dimension alph(lau,*),alpha(*),an(*),anc(*),ano(*),bet(lau,*),
     1  beta(*),bfnl(*),c(*),c1(*),c2(*),eig(*),esh(*),iim1(*),iju(*),
     2  info(6),iopen(*),ityp(*),lmd(*),nbskp(*),nd(*),ni(*),nic(*),
     3  nio(*),nnbskp(*),norbsm(*),np(*),nx(*),orthtr(*),olde(*),
     4  oldfoc(*),oldb(*),xsv(*),v0(*),slabel(*),title(*),h(nmat),s(*),
     5  t(nmat),p1(nmat),p2(nmat),sf1(nmat),sf2(nmat),sf11(nmat),
     6  sf12(nmat),sf21(nmat),sf22(nmat),d1(nmat),d2(nmat),fc(*),fo(*),
     7  cinv(*),cinvc0(*),ep(*),sd(*),spi(*),lbl(*),pp(*),qq(*),
     8  nsmbg(*)
c    7  cinv(*),cinvc0(*),ep(*),sd(*),thet(*),spi(*),lbl(*),pp(*),qq(*),
      dimension a(3),b(3),valofd(100),iofdg(100),jofdg(100)
      parameter (a0=0.0d0, a1m15=1.0d-15, a1m6=1.0d-6, a1m3=1.0d-3,
     &  apt1=0.1d0, apt2=0.2d0, a1s4=0.25d0, a1s2=0.5d0, a1=1.0d0,
     &  a2=2.0d0, a2pt2=2.2d0, a10=10.0d0, a1000=1000.0d0)
c

      data derfmt/'(1p3d25.15)'/
c
c     # idump = flag for writing mos to mocoef in each iteration
c     #         0 dump, 1 nodump
      data idump /0/
c
c     # me is always zero unless parallel and not node zero.
      data me /0/
c
*@ifdef parallel
*      me = nodeid()
*C     # for the moment avoid hassles associated with these
*C     # files.  fix this later. -rls
*      itcon  = 0
*      itprep = 0
*@endif
cVP
      call wzero (nmat,d1,1)
      call wzero (nmat,d2,1)
      icount=0
cVP end
c
c  set mxitr, eps, 2-config. values
c
      mxitr=itmax-1
      if (i2con.eq.2) then
        iscft=1
        eps=apt1
        eps1=eps
        epsscf=a10**(-iscf)
        epsci1=epsscf*a1000
        ivects=ivect
        ivect=-1
        lai1=0
        lai=0
        do la=1,lau
          do ip=1,np(la)
            lai=lai+1
            if(ano(lai).ne.a0) then
              if(lai1.eq.0) then
                lai1=lai
                la1=la
              else
                lai2=lai
                la2=la
                go to 140
              endif
            endif
          enddo
        enddo
      endif
      eps=a10**(-iscf)
      eps1=eps*a1000
      epsscf=eps
      pconv=a1
c
c  set next, occupation no. ratios
c
  140 continue
      next=0
      nextm=0
      niter=0
      lai=0
      do la=1,lau
        c1(la)=a0
        c2(la)=a0
        do ip=1,np(la)
          lai=lai+1
          if(anc(lai).ne.a0) c1(la)=anc(lai)
          if(ano(lai).ne.a0) c2(la)=ano(lai)
        enddo
        if(i2con.eq.2) then
          if(la.eq.la1) c11=c1(la)
          if(la.eq.la2) c12=c1(la)
        endif
        den=c1(la)-c2(la)
        if(den.ne.a0) then
          c1(la)=c1(la)/den
          c2(la)=c2(la)/den
        endif
      enddo
c
c  orthogonalize occupied mo's; construct virtual mo's
c
      call schmit(c,iim1,0,lau,ni,nlist,np,s,thrsh,sd)
      if(idebug.ne.0) call cprint(an,bfnl,c,eig,0,ityp,lau,0,ni,nlist,
     &  np,title)
      lapi=1
      do 210 la=1,lau
        ipu=np(la)
        if(ipu.eq.0) go to 210
        call izero_wr(ipu,nx,1)
        iu=ni(la)
        do i=1,iu
          cmax=a0
          do ip=1,ipu
            absc=abs(c(lapi))
            if(cmax.lt.absc.and.nx(ip).eq.0) then
              cmax=absc
              ipmax=ip
            endif
            lapi=lapi+1
          enddo
          nx(ipmax)=1
        enddo
        if(iu.eq.ipu) go to 210
        do ip=1,ipu
          if(nx(ip).ne.1) then
            call wzero (ipu,c(lapi),1)
            c(lapi+ip-1)=a1
            lapi=lapi+ipu
          endif
        enddo
  210 enddo
      if(idebug.ne.0) call cprint(an,bfnl,c,eig,1,ityp,lau,0,ni,nlist,
     &  np,title)
      call schmit(c,iim1,1,lau,ni,nlist,np,s,thrsh,sd)
      if(idebug.ne.0) call cprint(an,bfnl,c,eig,1,ityp,lau,0,ni,nlist,
     &  np,title)
      if(idump.eq.0) write (nlist,
     &  "(/'mo coefficients will be saved after each iteration',/)")
c
c  initialize diis quantities for FDS-SDF as error vector
c
      if(iprct.lt.-10.and.iprct.gt.-20) then
        if(mxpul.ne.0) then
          ndim = 1
          ndel = 0
          iread = 0
          do i=1,icmax
            orthtr(i)=c(i)
          enddo
        endif
      endif
c
c  initialize items for cycling
c
      keycif=0
      e1=a0
      e2=a0
      e1p=a0
      e2p=a0
      e1m2=a0
      e2m2=a0
      elast=a0
      de=a0
      dep=a0
      deavg=a0
      inew=0
      de1=a0
      keyf=0
c
      call wzero (lai,esh,1)
      read (ninput,'(4f10.0,3i5,f10.0)') ttr,tmr,tnr,eshst,
     &  msh,nsp,nosh,thrsta
      if (thrsta.eq.a0) thrsta=a1s2
      if (msh.ne.0) then
c       # general closed-shell orbital level shift,
c       # general level shift,
        if(nsp.eq.0) then
          lai=0
          do la=1,lau
            full=(nd(la)+nd(la))
            do ip=1,np(la)
              lai=lai+1
c             if (anc(lai).eq.full) esh(lai)=eshst
              esh(lai)=eshst*an(lai)/full
            enddo
          enddo
          write (nlist,
     &      "(/'general level shift  =  ',f10.5,/)") eshst
c    &      "(/'general closed-shell shift  =  ',f10.5,/)") eshst
        else
c         # user defined individual orbital level shift
          read (ninput,'(10(i3,f7.5))') (i1,esh(i1),ii=1,nsp)
          write (nlist,"(/'individual orbital shift values:')")
          lai=0
          do la=1,lau
            do ip=1,np(la)
              lai=lai+1
              if (esh(lai).ne.a0) write (nlist,
     &          "('esh(',i3,') = ',f10.5)") lai, esh(lai)
            enddo
          enddo
        endif
      endif
      read (ninput,'(a)') fmta
      if(itcon.ne.0) then
        fname = 'dampcon'
        call trnfln( 1, fname )
        open(itcon,form='unformatted',status='old',file=fname)
        read (itcon) e1,e2,elast,de,de1,keyf,deavg,inew,dep,itmaxo,
     1    sf11,sf12,sf21,sf22,dampsv
        close(itcon)
        if(idfix.ne.0) dampsv=ttr
        write (nlist,"(/'iterations continued from earlier run')")
        mxitr=itmaxo+mxitr
        next=next+itmaxo
      endif
      write (nlist,
     & '(''iteration'',7x,''energy'',11x,''one-electron energy'','//
     & '7x,''two-electron energy'')')
      write (nlist,"(9x,'ttr',10x,'tmr',11x,'tnr'/)")
c
      if ( i2con .ne. 2 ) go to 340
c
c  begin 2-config. cycle
c
  280 anc(lai2)=a2
      call dmat(anc,c,d1,iim1,lau,ni,nmat,np)
      anc(lai2)=a0
      anc(lai1)=a2
      call dmat(anc,c,d2,iim1,lau,ni,nmat,np)
      do i = 1, nmat
        amax=d1(i)
        term=d2(i)
        d1(i)=amax+term
        d2(i)=amax-term
      enddo
      if ( tstsiz ) then
c       # (use supermatrices in core)
        call corect(d1,d2,nmat,nost,nsm,p1,p2,pp,qq)
      else
        if (irnd.eq.0) then
c         # (use supermatrices on disk)
          call extlct(alpha,idtr4,idtrl,idtr3,i4i1,iju,i1i1,i1i2,i3i1,
     &      i3i2,0,i2i1,nbfld,nmat,nost,nout,d1,d2,p1,p2,spi,lbl)
        else
c         # (use ao integrals on disk)
          icount=icount+1
          if (icount.eq.1) then
            ijost=0
            do iost=1,nost
              do jost=1,iost
                ijost=ijost+1
                a(ijost)=alpha(ijost)
                b(ijost)=beta(ijost)
              enddo
            enddo
          endif
          if (irnd.eq.1) then
c  sifs
          call contr3(a,alph,b,bet,d1,d2,p1,p2,iim1,ifile1,iju,info,
     &      iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,0)
          elseif (irnd.eq.2) then
c  molcas
c  in case of parallel molcas we have to call contr3b several times
c  first time initializing p,q

           call wzero (nmat,p1,1)
           call wzero (nmat,p2,1)
          do iordint=0,9
          if (iordint.eq.0) then  
             fordint='ORDINT'
          else
            write(fordint,'(a,i1)') 'ORDINT',iordint
          endif 
          inquire(file=fordint,exist=lexists)
          if (lexists) then  
          call contr3b(a,alph,b,bet,d1,d2,p1,p2,iim1,ifile1,iju,info,
     &      iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,0,
     .      fordint)
          endif 
          enddo 
           call dscal_wr( nmat, 0.25d0, p1, 1 )
           call dscal_wr(nmat,0.5d0 ,p2,1)
          elseif (irnd.eq.3) then
c  dalton2
          call contr3c(a,alph,b,bet,d1,d2,p1,p2,iim1,ifile1,iju,info,
     &      iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,0)
          else
           call bummer('invalid irnd mode ',irnd,2)
          endif 
        endif
      endif
*@ifdef parallel
*C     # global add of the two-electron contributions.
*      call dgop( 1+MSGDBL, p1, nmat, '+' )
*      call dgop( 2+MSGDBL, p2, nmat, '+' )
*@endif
c
      eci1=a0
      eci2=a0
      eci3=a0
      do i=1,nmat
        eci1=eci1+d2(i)*h(i)
        eci2=eci2+d2(i)*p1(i)
        eci3=eci3+d2(i)*p2(i)
      enddo
      term=a1s2*(eci1+a1s2*eci2)
      hoff=-a1s4*eci3
      amax=sign(sqrt(term*term+hoff*hoff),term)
      cn=sqrt((amax+term)/(amax+amax))
      sn=hoff/(cn*(amax+amax))
ccc
c     write (6,*) 'eci1,eci2,eci3,term,hoff,amax,cn,sn'
c     write (6,'(4g18.8)') eci1,eci2,eci3,term,hoff,amax,cn,sn
c     if(i2con.eq.2) stop
ccc
      if(term.lt.a0) then
        ci1=sn
        ci2=cn
      else
        ci1=cn
        ci2=-sn
      endif
      write (nlist,"(10x,'ci coefficients',8x,2g18.8)") ci1,ci2
      anc(lai1)=a0
      ano(lai1)=a2*ci1*ci1
      an(lai1)=ano(lai1)
      ano(lai2)=a2*ci2*ci2
      an(lai2)=ano(lai2)
      den=c11-ano(lai1)
      c1(la1)=c11/den
      c2(la1)=ano(lai1)/den
      den=c12-ano(lai2)
      c1(la2)=c12/den
      c2(la2)=ano(lai2)/den
      alpha(1)=a1-a2/ano(lai1)
c     # alpha(2)=1;  alpha(2) used for beta(2) corect/extlct
      alpha(2)=-a1/(ci1*ci2)
      alpha(3)=a1-a2/ano(lai2)
c
c  begin scf cycle
c
  340 continue
      if(ipunch.ne.0.and.idump.eq.0) then
c       # only process 0 write the mocoef file
        if (me.eq.0) then
          open(ipunch,file='mocoef',form='formatted',status='unknown')
c         if (fmta.ne.' ') derfmt=fmta
          write(title(2),'(a)') 'mo coefficients generated by scfpq '
          call mowrit(ipunch,10,filerr,syserr,2,title,derfmt,
     .      lau,np,np, slabel,c)
c         call mowrit(ipunch,10,filerr,syserr,2,slabel,derfmt,
c    .      lau,np,np, title,c)
c     # write mo coefficients
          call mowrit(ipunch,20,filerr,syserr,2,title,derfmt,lau,np,np,
     +      slabel,c)
c     # write occupation numbers
          call mowrit(ipunch,40,filerr,syserr,2,title,derfmt,lau,np,np,
     +      slabel,an)
c     # write mo eigen energies
          call mowrit(ipunch,30,filerr,syserr,2,title,derfmt,lau,np,np,
     +      slabel,eig)
          close(ipunch)
        endif
      endif
      call dmat(anc,c,d1,iim1,lau,ni,nmat,np)
      if(idebug.ne.0) write (nlist,
     & '(/''closed-shell density''/(4g18.8))') d1
      if ( nost .ge. 1 ) then
        call dmat(ano,c,d2,iim1,lau,ni,nmat,np)
        if(idebug.ne.0) write (nlist,920) d2
        do i=1,nmat
          d1(i)=d1(i)+d2(i)
        enddo
        if(idebug.ne.0) write (nlist,890) d1
      endif
      if ( tstsiz ) then
c       # (use supermatrices in core; compact d2)
cdir$ ivdep
        do i=1,nsm
          d2(i)=d2(nsmbg(i))
        enddo
        call corect(d1,d2,nmat,nost,nsm,p1,p2,pp,qq)
      else
        if (irnd.eq.0) then
c         # (use supermatrices on disk)
          call extlct(alpha,idtr4,idtrl,idtr3,i4i1,iju,i1i1,i1i2,i3i1,
     &      i3i2,i2con,i2i1,nbfld,nmat,nost,nout,d1,d2,p1,p2,spi,lbl)
        elseif (irnd.eq.1) then
c         # (use sifs ao integrals on disk)
          call contr3(alpha,alph,beta,bet,d1,d2,p1,p2,iim1,ifile1,iju,
     &      info,iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,
     &      i2con)
        elseif (irnd.eq.2) then
c         #  use seward ao integrals on disk
c  molcas
c  in case of parallel molcas we have to call contr3b several times
c  first time initializing p,q

           call wzero (nmat,p1,1)
           call wzero (nmat,p2,1)

          do iordint=0,9
          if (iordint.eq.0) then  
             fordint='ORDINT'
          else
            write(fordint,'(a,i1)') 'ORDINT',iordint
          endif 
          inquire(file=fordint,exist=lexists)
          if (lexists) then
          call contr3b(alpha,alph,beta,bet,d1,d2,p1,p2,iim1,ifile1,iju,
     &      info,iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,
     &      i2con,fordint)
          endif
          enddo
           call dscal_wr( nmat, 0.25d0, p1, 1 )
           call dscal_wr(nmat,0.5d0 ,p2,1)
          elseif (irnd.eq.3) then
c  dalton2
          call contr3c(a,alph,b,bet,d1,d2,p1,p2,iim1,ifile1,iju,info,
     &      iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,0)
          else
           call bummer('invalid irnd mode ',irnd,2)
          endif
      endif
c
*@ifdef parallel
*C     # global add of the two-electron contributions.
*      call dgop( 3+MSGDBL, p1, nmat, '+' )
*      if ( nost .ge. 1 ) call dgop( 4+MSGDBL, p2, nmat, '+' )
*@endif
c
      e1m2=e1p
      e2m2=e2p
      e1p=e1
      e2p=e2
      elastp=elast
      de1p=de1
      e1=a0
      e2=a0
      do i=1,nmat
        e1=e1+d1(i)*h(i)
        e2=e2+d1(i)*p1(i)
      enddo
      if(nost.gt.0) then
        do i=1,nsm
          e2=e2-d2(i)*p2(i)
        enddo
      endif
c
c  if in core, expand p2
c
      if(nsm.ne.nmat) then
        ism=nsm
cdir$ ivdep
        do i=nmat,1,-1
          if(i.eq.nsmbg(ism)) then
            p2(i)=p2(ism)
            ism=ism-1
          else
            p2(i)=a0
          endif
        enddo
      endif
      e2=a1s2*e2
      elast=e1+e2+repnuc
      deltae=elast-elastp
      if(e2.eq.a0) then
        de1=eps
      else
        de1=(e2-e2p)/e2
      endif
      if(inew.eq.1) then
        if(abs(deltae).le.eps) then
          deavg=(abs(de1)+abs(de1p)+apt2*deavg)/a2pt2
          de=de1
          dep=de1p
          go to 350
        endif
        inew=0
      endif
      if(next.le.1) then
        deavg=abs(deltae)
      else
        dep=de
        de=deltae
        deavg=(abs(de)+abs(dep)+apt2*deavg)/a2pt2
        if(deavg.lt.a1m6) inew=1
      endif
  350 continue
      next=next+1
      write (nlist,'(i5,3g25.14)') next,elast,e1,e2
      write (nlist,'(4x,3g13.4)') ttr,tmr,tnr
      if(e1.eq.a0) then
        e1eps=eps
      else
        e1eps=abs((e1-e1p)/e1)
      endif
      if(e2.eq.a0) then
        e2eps=eps
      else
        e2eps=abs((e2-e2p)/e2)
        de2=abs(e2p-e2)
      endif
      epsm=max(e1eps,e2eps)
      epslon=max(min(a1m6,epsm),a1m15)
csi   # old scfpq criterion deleted
c     if(epsm.le.eps) then
c       key=1
c     else
c       key=0
c     endif
c     if(ivo.eq.0) then
c       keycnv=keyf*key
c     else
c       keycnv=1
c     endif
csi   # new criterion:
      if(abs(elastp-elast).lt.eps.and.abs(e1-e1p).lt.eps1) then
        key=1
      else
        key=0
      endif
      if(ivo.eq.0) then
        keycnv=keyf*key
      else
        keycnv=1
      endif
csi   # end of change
      if(keycnv.eq.1) iauto=1
c
c     # damp first iteration if core guess.
c     # to suppress this assure ttr-tmr.le.0.1
      dampn=a1
      if(next.eq.1.and.(ttr-tmr).gt.apt1.and.inflg.lt.0)
     1  dampn=a1/(a1+ttr)
c
c  vary damping factor?
c
      if((idfix.ne.1.and.next.gt.1).and.(idfix.ne.2.and.next.gt.2)) then
        if(ttr.lt.a0) then
          ttr=ttr+tnr
        else
          e1osz=(e1m2-e1p)*(e1p-e1)
          e2osz=(e2m2-e2p)*(e2p-e2)
          if((elastp-elast).gt.a0)        ttr=ttr-tnr
          if(e1osz.gt.a0.and.e2osz.gt.a0) ttr=ttr-tnr
          if((elast-elastp).gt.a0)        ttr=ttr+tnr
          ttr=max(tmr,ttr)
        endif
        if(abs(ttr).le.(tmr+a1m3)) idfix=2
      endif
c
c  extrapolate p, q matrices?
c
      if(keycnv.ne.1.and.iprct.gt.0) then
        ifext=0
c       call extrl(damp,dampsv,ifext,iprct,nmat,next,p1,sf12,sf22)
        call extrl(ttr,dampsv,ifext,iprct,nmat,next,p1,sf12,sf22)
c       if(nost.ne.0) call extrl(damp,dampsv,ifext,iprct,nmat,next,p2,
        if(nost.ne.0) call extrl(ttr,dampsv,ifext,iprct,nmat,next,p2,
     &    sf11,sf21)
        if(ifext.eq.1) write (nlist,370)
  370     format(10x,10('* '),'extrapolate p matrix',10(' *'))
      endif
c
c  or would you rather like to do diis (dF)?
c
      if(de2.lt.thrsta) nextm=1
      if(iprct.lt.0.and.iprct.gt.-10) then
c       # calculate inverse matrix of c: c**(-1)=transpose(s*c)=:xsv
        if(niter.eq.0.and.nextm.eq.1) call vksvn(s,c,xsv,lau,np,v0)
      endif
c
c  form fock matrices (over so's)
c
      do i = 1,nmat
        sf2(i) = h(i) + p1(i)*dampn
      enddo
      if(nost.gt.0) then
        do i = 1,nmat
          sf1(i) = sf2(i) - p2(i)*dampn
        enddo
      endif
c
      if(idebug.ne.0) then
        write (nlist,880) sf2
        if(nost.gt.0) write (nlist,910) sf1
      endif
c
c  transform to mo basis and assemble (single) fock matrix
c
      laij=0
      lai=0
      lapi=0
      do 470 la=1,lau
      ipu=np(la)
      if(ipu.eq.0) go to 470
      mic=max(1,micmx)
      if(nio(la).eq.0.or.nic(la).eq.0.or.keycnv.eq.1) mic=1
      do 460 micro=1,mic
      call tritrn(ipu,fc(laij+1),sf2(laij+1),c(lapi+1),sd)
      if(nio(la).eq.0.and.ivo.eq.0) go to 460
      call tritrn(ipu,fo(laij+1),sf1(laij+1),c(lapi+1),sd)
      ij=0
      if(ivo.eq.0) go to 400
c
c  make up fock matrix for ivo case
c
      do 390 i=1,ipu
        do 380 j=1,i
          ij=ij+1
          if(an(lai+i).ne.a0.or.an(lai+j).ne.a0) then
            if(i.ne.j) then
c             # virtual-occupied, off-diagonal occupied-occupied
              fc(laij+ij)=a0
            else
c             # diagonal open; diagonal closed already in place
              if(ano(lai+i).ne.a0) fc(laij+ij)=fo(laij+ij)
            endif
          else
c           # virtual-virtual
            fc(laij+ij)=fo(laij+ij)
          endif
  380   enddo
  390 enddo
      go to 460
c
  400 if((keycnv.ne.1).and.(next.le.mxitr)) go to 430
c
c  make up fock matrix (last cycle) for canonical orbital energies
c
      do 420 i=1,ipu
        do 410 j=1,i
          ij=ij+1
c         # closed-closed, virtual-closed already in place
c         if(anc(lai+i).ne.a0.or.anc(lai+j).ne.a0) then
c           # open-closed
c           if(ano(lai+i).ne.a0.or.ano(lai+j).ne.a0)
c    1        fc(laij+ij)=c1(la)*fc(laij+ij)-c2(la)*fo(laij+ij)
c         else
c           # open-open, virtual-open, virtual-virtual
c           fc(laij+ij)=fo(laij+ij)
c         endif
c         # fock matrix will be constructed a la scfarg
          if(ano(lai+i)+ano(lai+j).eq.a0) go to 410
          if(anc(lai+i)+anc(lai+j).eq.a0) fc(laij+ij)=fo(laij+ij)
          if(anc(lai+i)+anc(lai+j).eq.a0) go to 410
          fc(laij+ij)=c1(la)*fc(laij+ij)-c2(la)*fo(laij+ij)
  410   enddo
  420 enddo
      go to 460
c
c  make up fock matrix for generally best convergence
c
  430 do 450 i=1,ipu
        do 440 j=1,i
          ij=ij+1
csic       # fock matrix will be constructed a la scfarg
csic       (virtual-closed already in place)
csi        if((an(lai+i).eq.a0.or.an(lai+j).eq.a0).and.
csi       1   (an(lai+i).ne.a0.or.an(lai+j).ne.a0)) then
csic         (virtual-open)
csi          if(anc(lai+i).eq.a0.and.anc(lai+j).eq.a0)
csi       1  fc(laij+ij)=fo(laij+ij)
csi        else
csic         (occupied-occupied, virtual-virtual)
csi          fc(laij+ij)=c1(la)*fc(laij+ij)-c2(la)*fo(laij+ij)
csi        endif
          if(ano(lai+i)+ano(lai+j).eq.a0) go to 440
          if(anc(lai+i)+anc(lai+j).eq.a0) fc(laij+ij)=fo(laij+ij)
          if(anc(lai+i)+anc(lai+j).eq.a0) go to 440
          fc(laij+ij)=c1(la)*fc(laij+ij)-c2(la)*fo(laij+ij)
  440   enddo
  450 enddo
c
  460 enddo
      lai=lai+ipu
      laij=laij+iim1(ipu+1)
      lapi=lapi+ipu*ipu
  470 enddo
c
c  diis extrapolation (FDS-SDF)?
c
      if(mxpul.ne.0.and.keycnv.ne.1.and.iprct.lt.-10) then
        call diisa(an,fc,fo,s,c,orthtr,olde,oldfoc,oldb,np,cinv,cinvc0,
     &    ep,sd,apt1*abs(elast),epslon,iread,lau,mxpul,ndim,ndel,nlist,
     &    nmat)
      endif
c
c  level shift and diagonalization
c
      laij = 0
      lai = 0
      lapi = 0
      do 540 la=1,lau
      ipu=np(la)
      if(ipu.eq.0) go to 530
      iu=ni(la)
csi      if(iu.eq.0.and.keycnv.ne.1.and.next.le.mxitr) then
      if(iu.eq.0.and.keycnv.ne.1.and.next.le.1) then
        call wzero (ipu,eig(lai+1),1)
        go to 520
      endif
      mic=max(1,micmx)
      if(nio(la).eq.0.or.nic(la).eq.0.or.keycnv.eq.1) mic=1
      do 510 micro=1,mic
csi   # apply new accelerations
      xscr=a1
c     # apply user defined orbital level shifting
      if(msh.ne.0) then
        ii=laij
        do i=1,ipu
          ii=ii+i
          fc(ii)=fc(ii)-esh(lai+i)
          eig(lai+i)=eig(lai+i)-esh(lai+i)
        enddo
      endif
c     # damp diagonal fock matrix elements by ttr*old f
      if(next.le.1.or.idfix.eq.1) go to 490
      xscr=xscr*a1/(a1+ttr)
      ii=laij
      do i=1,ipu
        ii=ii+i
        if(keycnv.ne.1) then
          fc(ii)=fc(ii)+ttr*eig(lai+i)
        else
          fc(ii)=fc(ii)*(a1+ttr)
        endif
      enddo
c     # automatic virtual orbital level shift
      if(nosh.ne.0) go to 500
      nnv=nic(la)+nio(la)
      if(nnv.eq.0.or.i2con.eq.2) go to 500
      if(keycnv.ne.1) then
        diama=fc(laij+1)
        ii=laij
        do i=1,nnv
          ii=ii+i
          if(fc(ii).gt.diama) diama=fc(ii)
        enddo
        diami=diama+a1000
        iii=ii
        do i=nnv+1,ipu
          ii=ii+i
          if(fc(ii).lt.diami) diami=fc(ii)
        enddo
        ddia=diami-diama
        if(ddia.gt.apt1) go to 500
        ddia=apt1-ddia
        ii=iii
        do i=nnv+1,ipu
          ii=ii+i
          fc(ii)=fc(ii)+ddia
        enddo
        write (nlist,
     &   '(''irrep '',a,'': energies of virtual orbitals'','//
     &   ''' shifted by'',f10.5)') ityp(la), ddia
        go to 500
      endif
  490 continue
c     # multiply diagonal fock matrix elements by 2.0
      if(inflg.gt.0) then
        xscr=xscr*a1s2
        ii=laij
        do i=1,ipu
          ii=ii+i
          fc(ii)=fc(ii)*a2
        enddo
      endif
c
      


  500 continue

*@ifdef corehole
*      call apply_corehole(fc(laij+1),la,np(la))
*@endif 
      call erduw(fc(laij+1),c(lapi+1),ipu,a1m15)
c
      ii=laij
      do i=1,ipu
        ii=ii+i
        fc(ii)=fc(ii)*xscr
      enddo
      ii=laij
      do i=1,ipu
        ii=ii+i
        eig(lai+i)=fc(ii)
      enddo
  510 enddo
c
c  order mo's by orbital energy?
c
csi      if(nextm.eq.1) iauto=0
      if(iauto.ne.0) call bubble(c(lapi+1),eig(lai+1),ipu,ipu)
      if(msh.ne.0) then
        do i=1,ipu
          eig(lai+i)=eig(lai+i)+esh(lai+i)
        enddo
      endif
csi   # apply new accelerations
csic
csic  order mo's by orbital energy?
csic
  520 lai=lai+ipu
      laij=laij+iim1(ipu+1)
  530 lapi=lapi+ipu*ipu
  540 enddo
c
      if(keycnv.eq.1) go to 600
      keyf=key
      if(next.gt.mxitr) go to 610
      if(idebug.ne.0) call cprint(an,bfnl,c,eig,0,ityp,lau,next,ni,
     &  nlist,np,title)
      call schmit(c,iim1,1,lau,ni,nlist,np,s,thrsh,sd)
      if(ivect.gt.0) call cprint(an,bfnl,c,eig,0,ityp,lau,next,ni,nlist,
     &  np,title)
      if(nextm.eq.0.or.iprct.ge.0.or.iprct.lt.-10) go to 340
csi   # diis (dF)
      call wzero (icmax,orthtr,1)
      call vkx(lau,np,orthtr,c,xsv)
      call tfao(np,eig,orthtr,fc,lau,nic,nio)
      if(next.ge.2) then
        call pulay(fc,oldfoc,nmat,a1m6,lau,nic,nio,np,c,v0,
     &             esh,msh,niter,ttr,tmr,tnr,pconv,mxpul)
      endif
csi   # diis (dF) end
      go to 340
c
c  end of scf cycle
c
  600 if(i2con.ne.2) go to 620
      if(ivo.ne.0) go to 610
      if(abs(elastp-elast).lt.epsscf.and.abs(e1-e1p).lt.epsci1) then
        keyci=1
      else
        keyci=0
      endif
      if(keyci.eq.1.and.keycif.eq.1) then
        if(iscft.ge.iscf) go to 610
        iscft=iscft+1
        eps=a10**(-iscft)
        if(iscft.gt.4) then
          eps1=eps*a1000
        else
          eps1=eps
        endif
      endif
      keycif=keyci
      call schmit(c,iim1,1,lau,ni,nlist,np,s,thrsh,sd)
      if(next.gt.mxitr) go to 750
      if(nextm.eq.0.or.keycif.eq.1.or.iprct.ge.0.or.iprct.lt.-10)
     1  go to 280
      call wzero (icmax,orthtr,1)
      call vkx(lau,np,orthtr,c,xsv)
      call tfao(np,eig,orthtr,fc,lau,nic,nio)
      if(next.ge.2)
     1  call pulay(fc,oldfoc,nmat,a1m6,lau,nic,nio,np,c,v0,
     2             esh,msh,niter,ttr,tmr,tnr,pconv,mxpul)
      go to 280
c
c  end of 2-config. cycle
c
  610 ivect=ivects
  620 if(keycnv.eq.1) write (nlist,"(/'calculation has *converged*')")
      if(iall.eq.0) go to 760
c
      if(igrad.eq.0) go to 770
c
c     # generate input needed for scf gradient, including fock
c     # matrices needed for open-shell cases
c
c     # if calculation has not converged, skip gradient output
cSI   if(keycnv.ne.1) go to 770
      l0=0
      l00=0
      nofdg=0
      kk=0
      isu=0
      do 670 la=1,lau
        ipu=np(la)
        if (nio(la).eq.0) go to 660
        call tritrn (ipu,fc(kk+1),sf2(kk+1),c(isu+1),sd)
        if(nio(la).ne.0) call tritrn(ipu,fo(kk+1),sf1(kk+1),c(isu+1),sd)
        ij=kk
        do 650 i=1,ipu
          do 640 j=1,i
            ij=ij+1
            iii=l0+i
            iij=l0+j
            iii0=l00+i
            iij0=l00+j
            if (anc(iii).ne.a0.and.ano(iij).ne.a0) go to 630
            iii=l0+j
            iij=l0+i
            iii0=l00+j
            iij0=l00+i
            if (anc(iii).ne.a0.and.ano(iij).ne.a0) go to 630
            go to 640
  630       nofdg=nofdg+1
c
            valofd(nofdg)=fc(ij)+a1s2*ano(iij)*fo(ij)
c
            iofdg(nofdg)=iii0
            jofdg(nofdg)=iij0
  640     enddo
  650   enddo
  660   kk=kk+iim1(ipu+1)
        isu=isu+ipu*ipu
        l0=l0+ipu
        l00=l00+nic(la)+nio(la)
  670 enddo
      do la=1,lau
        if (nio(la).ge.2) then
          nochg=1
          go to 690
        endif
      enddo
  690 continue
      call outgrd(alpha,beta,ci1,ci2,fmta,igrad,iofdg,i2con,jofdg,lau,
     &  nic,nio,nlist,nochg,nofdg,nost,np,valofd)
      go to 770
c
c     # end of scf gradient input section
c
  750 write (nlist,860)
      if(i2con.eq.2) ivect=ivects
  760 iorder=0
  770 if(ipunch.ne.0) then
        if (me.eq.0) then
          open(ipunch,file='mocoef',form='formatted',status='unknown')
          if (fmta.ne.' ') derfmt=fmta
          call mowrit(ipunch,10,filerr,syserr,2,title,derfmt,lau,np,np,
     +      slabel,c)
c     # write mo coefficients
          call mowrit(ipunch,20,filerr,syserr,2,title,derfmt,lau,np,np,
     +      slabel,c)
c     # write occupation numbers
          call mowrit(ipunch,40,filerr,syserr,2,title,derfmt,lau,np,np,
     +      slabel,an)
c     # write orbital energies
          call mowrit(ipunch,30,filerr,syserr,2,title,derfmt,lau,np,np,
     +      slabel,eig)
          close(ipunch)
        endif
        if(iorder.eq.1) write (nlist,*)
     1    'vectors ordered by orbital energy within each irrep'
        if (irnd.eq.2) then
        open (unit=ipunch,file='mocoef_lumorb')
           call wrtmof_molcas(ipunch, 
     .       lau ,np ,np,c,'mos','mocoef_scfpq')
           call wrtmof_molcas(ipunch,
     .       lau,np,np,an,'occ','mocoef_scfpq')
        close (unit=ipunch) 
        endif 
      endif
      if(itprep.ne.0) then
        fname = 'dampcon'
        call trnfln( 1, fname )
        open(itprep,form='unformatted',status='unknown',file=fname)
        write (itprep) e1,e2,elast,de,de1,keyf,deavg,inew,dep,next,
     1    sf11,sf12,sf21,sf22,dampsv
        close(itprep)
        write (nlist,800) itprep
  800     format(/'iteration continuation data written on unit',i3)
      endif
c     if(ivect.ge.0) then
csi       # will be printed later anyway
csi       call cprint(an,bfnl,c,eig,0,ityp,lau,next,ni,nlist,np,title)
c     endif
c
      ovlp=a0
      ekin=a0
      do i=1,nmat
        ovlp=ovlp+d1(i)*s(i)
c       ekin=ekin+d1(i)*h(i)
        ekin=ekin+d1(i)*t(i)
      enddo
      if(elec.ne.a0) ovlp=ovlp/elec
      epot=elast-ekin
      virial=epot/elast
      write (nlist,850) elast,ekin,epot,virial,ovlp
  850   format(/5x,'total energy =',f22.10/5x,'kinetic energy =',
     1    f20.10/5x,'potential energy =',f18.10/5x,
     2    'virial theorem =',f20.10/5x,'wavefunction norm =',f17.10)
      if(keycnv.ne.1) write (nlist,860)
      if(ivect.ge.0) call cprint(an,bfnl,c,eig,1,ityp,lau,next,ni,nlist,
     &  np,title)
      if(keycnv.ne.1) write (nlist,860)
  860 format(/'calculation has not converged')
      if(iprint.ne.0) then
        write (nlist,"(/'p-matrix'/(4g18.8))") p1
        write (nlist,880) sf2
  880     format(/'closed-shell fock matrix'/(4g18.8))
        write (nlist,890) d1
  890     format(/8x,'total density'/(4g18.8))
        if(nost.ne.0) then
          write (nlist,"(/'q-matrix'/(4g18.8))") p2
          write (nlist,910) (sf1(i),i=1,nmat)
  910       format(/'  open-shell fock matrix'/(4g18.8))
          write (nlist,920) (d2(i), i=1,nsm)
  920       format(/'  open-shell density'/(4g18.8))
        endif
      endif
      return
      end
c deck dmat
      subroutine dmat(ane,c,d,iim1,lau,ni,nmat,np)
c
c  form closed- and open-shell density matrices
c
      implicit real*8 (a-h,o-z)
      parameter (a0=0.0d0)
      dimension ane(*), c(*), d(*), iim1(*), ni(*), np(*)
c
      call wzero(nmat,d,1)
      lai=0
      lapi=0
      lapq=0
      do 60 la=1,lau
        ipu=np(la)
        if(ipu.eq.0) go to 60
        lapqs=lapq
        iu=ni(la)
        do i=1,iu
          if(ane(lai+i).eq.a0) go to 30
          lapq=lapqs
          do ip=1,ipu
            do iq=1,ip-1
              lapq=lapq+1
              d(lapq)=d(lapq)+((ane(lai+i)*c(lapi+ip))+(ane(lai+i)*
     &          c(lapi+ip)))*c(lapi+iq)
            enddo
            lapq=lapq+1
            d(lapq)=d(lapq)+(ane(lai+i)*c(lapi+ip))*c(lapi+ip)
          enddo
   30     lapi=lapi+ipu
        enddo
        lai=lai+ipu
        lapi=lapi+(ipu-iu)*ipu
        if(lapq.eq.lapqs) lapq=lapqs+iim1(ipu+1)
   60 enddo
      return
      end
c deck corect
      subroutine corect(dt,do,nmat,nost,nsm,p,q,pp,qq)
c
c  form p and q matrices from in-core p and q supermatrices.  does
c  not presently work for 2-configuration cases.
c
      implicit real*8 (a-h,o-z)
c
      dimension dt(*), do(*), p(*), q(*), pp(*), qq(*)
c
c  if open shell(s), form q matrix
c
      if(nost.ne.0) then
        call wzero(nsm,q,1)
        nplcq = 0
        do ij = 1,nsm
          do kl = 1,ij-1
            q(ij) = q(ij) + qq(nplcq+kl) * do(kl)
          enddo
          do kl = 1,ij
            q(kl) = q(kl) + qq(nplcq+kl) * do(ij)
          enddo
          nplcq = nplcq + ij
        enddo
      endif
c
c  form p matrix
c
      call wzero(nmat,p,1)
      nplcp = 0
      do ij = 1,nmat
        do kl = 1,ij-1
          p(ij) = p(ij) + pp(nplcp+kl)*dt(kl)
        enddo
        do kl = 1,ij
          p(kl) = p(kl) + pp(nplcp+kl)*dt(ij)
        enddo
        nplcp = nplcp + ij
      enddo
c
      return
      end
c deck extlct
      subroutine extlct(alpha,idtr4,idtrl,idtr3,i4i1,iju,i1i1,i1i2,i3i1,
     &  i3i2,i2con,i2i1,nbfld,nmat,nost,nout,dt,do,p,q,spi,lbl)
c
c  form p and q matrices from out-of-core p and q supermatrices
c
      implicit real*8 (a-h,o-z)
      dimension dt(*),do(*),p(*),q(*),spi(*),lbl(idtrl),alpha(*),iju(*)
c     (spi and lbl occupy the same space)
c
*@ifdef int64
      integer rechts,und
       integer lbl,spi
      parameter (m32=2**32-1)
*@if defined( t3e64 ) || defined ( cray )
*       rechts(i,j) = shiftr(i,j)
*       und(i,j)    =and(i,j)
*@else
        rechts(i,j) = ishft(i,j)
        und(i,j)    = iand(i,j)
*@endif
*@else
*      integer*2 lbl
*@endif
c
      call wzero(nmat,p,1)
      if(nost.ne.0) call wzero(nmat,q,1)
      do 96 ibfld=1,nbfld
      read (nout) iblk, nbuf, lbl
      go to (16,40,56,80), iblk
c
c  two integrals; one label
c
   16 if(i2con.eq.2) then
        do ibuf=1,nbuf
          ij=lbl(ibuf)
          if(ij.le.iju(1)) then
            q(ij)=q(ij)+alpha(1)*spi(ibuf+i1i1)*do(ij)
          else if(ij.le.iju(2)) then
            q(ij)=q(ij)+alpha(3)*spi(ibuf+i1i1)*do(ij)
          else
            q(ij)=q(ij)+spi(ibuf+i1i2)*do(ij)
          endif
          p(ij)=p(ij)+spi(ibuf+i1i1)*dt(ij)
        enddo
      else
        do ibuf=1,nbuf
          ij=lbl(ibuf)
          p(ij)=p(ij)+spi(ibuf+i1i1)*dt(ij)
          q(ij)=q(ij)+spi(ibuf+i1i2)*do(ij)
        enddo
      endif
      go to 96
c
c  one integral; one label
c
   40 do ibuf=1,nbuf
        ij=lbl(ibuf)
        p(ij)=p(ij)+spi(ibuf+i2i1)*dt(ij)
      enddo
      go to 96
c
c  two integrals; two labels
c
   56 if(i2con.eq.2) then
        do 64 ibuf=1,nbuf
*@ifdef int64
        ij=rechts(spi(ibuf),32)
        kl=und(spi(ibuf),m32)
*@else
*        ij=lbl(ibuf)
*        kl=lbl(ibuf+idtr3)
*@endif
        if(ij.le.iju(1)) then
          q(ij)=q(ij)+alpha(1)*spi(ibuf+i3i1)*do(kl)
          q(kl)=q(kl)+alpha(1)*spi(ibuf+i3i1)*do(ij)
        else if(ij.le.iju(2)) then
          if(kl.le.iju(1)) then
            q(ij)=q(ij)+(spi(ibuf+i3i1)+alpha(2)*spi(ibuf+i3i2))*do(kl)
            q(kl)=q(kl)+(spi(ibuf+i3i1)+alpha(2)*spi(ibuf+i3i2))*do(ij)
          else
            q(ij)=q(ij)+alpha(3)*spi(ibuf+i3i1)*do(kl)
            q(kl)=q(kl)+alpha(3)*spi(ibuf+i3i1)*do(ij)
          endif
        else
          q(ij)=q(ij)+spi(ibuf+i3i2)*do(kl)
          q(kl)=q(kl)+spi(ibuf+i3i2)*do(ij)
        endif
        p(ij)=p(ij)+spi(ibuf+i3i1)*dt(kl)
        p(kl)=p(kl)+spi(ibuf+i3i1)*dt(ij)
   64   enddo
      else
        do ibuf=1,nbuf
*@ifdef int64
          ij=rechts(spi(ibuf),32)
          kl=und(spi(ibuf),m32)
*@else
*          ij=lbl(ibuf)
*          kl=lbl(ibuf+idtr3)
*@endif
          p(ij)=p(ij)+spi(ibuf+i3i1)*dt(kl)
          p(kl)=p(kl)+spi(ibuf+i3i1)*dt(ij)
          q(ij)=q(ij)+spi(ibuf+i3i2)*do(kl)
          q(kl)=q(kl)+spi(ibuf+i3i2)*do(ij)
        enddo
      endif
      go to 96
c
c  one integral; two labels
c
   80 do ibuf=1,nbuf
*@ifdef int64
        ij=rechts(spi(ibuf),32)
        kl=und(spi(ibuf),m32)
*@else
*        ij=lbl(ibuf)
*        kl=lbl(ibuf+idtr4)
*@endif
        p(ij)=p(ij)+spi(ibuf+i4i1)*dt(kl)
        p(kl)=p(kl)+spi(ibuf+i4i1)*dt(ij)
      enddo
c
   96 enddo
      rewind nout
      return
      end
c deck contr3
      subroutine contr3(alpha,a,beta,b,dt,do,p1,p2,nndx,ifile1,iju,info,
     &  iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,i2con)
      implicit real*8 (a-h,o-z)
c
c  Compute elements of fock matrix which is the P and Q matrix (see R.M.
c  Pitzer's O.S.U.-T.C.G. report No. 101) directly from AO integrals,
c  rather than the supermatrix of AO integrals. When this routine is
c  called, the routines contr2 and twoint will be skipped.
c
c  Vudhichai Parasuk, U. Vienna, on leave from Chulalongkorn U, Bangkok
c
c  For closed- and open-shell RHF and 2-configuration MCSCF calculations
c
      parameter (nbuf = 4096)
c     # integral continuation parameter (cf. sifsanl.doc, sifrd2)
      parameter (msame = 0)
c
      real*8 alpha(*),a(lau,*),beta(*),b(lau,*),dt(*),do(*),p1(*),p2(*)
      integer nndx(*),ifile1,iju(*),info(6),iopen(*),lapu,lau,lmd(*),
     &  nbskp(*),nmat,nnbskp(*),norbsm(*),nost,np(*),i2con
c
      real*8  val, fac
      integer ibitv(nbuf),ierr,ita,itb,ifmt,last,numi
      integer ifile,iost
      integer pq1,rs1,pr1,qs1,ps1,qr1,p,q,r,s,psym,qsym,rsym,ssym,x
      real*8, allocatable :: valin(:),bufin(:)
      integer, allocatable :: labin(:,:)
c
      real*8 quar, half, one, thrhf, two, three, four
      parameter (quar = 0.25d0, half = 0.5d0, one = 1.0d0,
     &  thrhf = 1.5d0, two = 2.0d0, three = 3.0d0, four = 4.0d0)
      real*8, allocatable :: buffer(:)
      integer mult(8,8)
      integer qmax,smax,imatrd,ibufrd,ioff
      integer ptst,qtst,rtst,stst
       integer  mcpsym,mcqsym,mcrsym,mcssym,irc,iflag,nmatl,nnbuf,
     .    nmatrd,i1,i2,ntot
c
      logical ordopened
      data ordopened /.false./
      character*130 fname
      data mult /
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
      save mult,ordopened
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
c     # initialize p1 (P matrix) and p2 (Q matrix) to zero
c
      allocate(valin(info(5)),bufin(info(4)),labin(4,info(5)))
      call wzero (nmat,p1,1)
      call wzero (nmat,p2,1)
c
c     # adjust diagonal elements of dt and do to appropriate values
c
      do lap = 1, lapu
        isym = norbsm(lap)
        is = lap - nbskp(isym)
        ii = nndx(is) + is + nnbskp(isym)
c       write(6,*) 'DTL:',lap,isym,is,ii
        dt(ii) = dt(ii)*two
        do(ii) = do(ii)*two
      enddo
c
c     # reindex alpha and beta by symmetry of irreps
c
      call izero_wr(lau,iopen,1)
      do 30 iost = 1, nost
        x = 0
        do la = 1, lau
          x = x + nndx(np(la)+1)
          if (iju(iost).eq.x) then
            lmd(iost) = la
            iopen(la) = 1
            go to 30
          endif
        enddo
  30  enddo
      do iost = 1, nost
        ii = nndx(iost)
        do jost = 1, iost
          a(lmd(iost),lmd(jost)) = alpha(ii+jost)
          a(lmd(jost),lmd(iost)) = alpha(ii+jost)
          b(lmd(iost),lmd(jost)) = beta(ii+jost)
          b(lmd(jost),lmd(iost)) = beta(ii+jost)
        enddo
      enddo
c
c     # open the file of electron-repulsion integrals.
c     # sifo2f() returns with the file positioned before the
c     # first record of integrals.
c
      fname = 'aoints2'
      call trnfln( 1, fname )
      call sifo2f( ifile1, 8, fname, info, ifile, ierr)
      if ( ierr .ne. 0 ) then
        call bummer('from sifo2f(), ierr=', ierr, faterr )
      endif
c
c     # read in buffers of two-electron integrals
c
      last = 0
      ita = 0
      itb = 0
      ntot = 0
c
   50 continue
      call sifrd2(ifile,info,4,1,bufin,numi,last,ita,itb,
     &     ibvtyp,valin,labin,ibitv,ierr)
      ntot=ntot+numi
c
c     # loop over integrals
c
      do iint = 1, numi
        p = labin(1,iint)
        q = labin(2,iint)
        r = labin(3,iint)
        s = labin(4,iint)
        val = valin(iint)
c
        if (p.lt.q) then
          itemp = p
          p = q
          q = itemp
        endif
        if (r.lt.s) then
          itemp = r
          r = s
          s = itemp
        endif
        if (p.lt.r.or.(p.eq.r.and.q.lt.s)) then
          itemp = p
          p = r
          r = itemp
          itemp = q
          q = s
          s = itemp
        endif
c
        psym = norbsm(p)
        qsym = norbsm(q)
        rsym = norbsm(r)
        ssym = norbsm(s)
c
        p = p - nbskp(psym)
        q = q - nbskp(qsym)
        r = r - nbskp(rsym)
        s = s - nbskp(ssym)
c
c       # determine the symmetry block type.  note that itype=4
c       # integrals do not contribute to the fock matrix.
c
        if (psym.eq.qsym) then
          if (psym.eq.rsym) then
            itype = 1
          else
            itype = 2
          endif
        elseif(psym.eq.rsym) then
          itype = 3
        else
          itype = 4
        endif

c
c       # test for two-configuration mcscf calculation
c
        if (i2con.ne.2) then
c
c       # test for closed- or open-shell calculation
c
        if (nost.eq.0) then
c
c       # closed-shell calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
          else
            if ( q .gt. r ) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
            endif
          elseif ( q .eq. s ) then
c           # q = s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
            endif
          endif
        endif
c       write(6,154) ddot(nmat,p1,1,p1,1)
154     format('ddot(p1)=',f15.8)
        else
c
c       # open-shell calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = (a(psym,psym) + b(psym,psym))*half
                  p2(pq1) = p2(pq1) + do(pq1)*fac*val
                endif
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = (a(psym,psym) - b(psym,psym))*half
                  p2(pr1) = p2(pr1) - do(pr1)*fac*val
                endif
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = (a(psym,psym) + b(psym,psym))
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = (a(psym,psym) + b(psym,psym))*half
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                endif
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = two*a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = (a(psym,psym) - b(psym,psym))*half
                  p2(pr1) = p2(pr1) - do(qs1)*fac*val
                  p2(qs1) = p2(qs1) - do(pr1)*fac*val
                endif
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
              if (iopen(psym).ne.0) then
                fac = (a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = (a(psym,psym) + b(psym,psym))
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
              endif
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = (three*a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = (three*a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
            if (iopen(psym).ne.0) then
              fac = (three*a(psym,psym) + b(psym,psym))*half
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
            if (iopen(psym).ne.0) then
              fac = (three*a(psym,psym) + b(psym,psym))*half
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
          else
            if ( q .gt. r ) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
            if (iopen(psym).ne.0) then
              fac = two*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = a(psym,rsym)
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two*a(psym,rsym)
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = a(psym,rsym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two*a(psym,rsym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p  =  r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          elseif ( q .eq. s ) then
c           # q = s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          endif
        endif
        endif
        else
c
c       # two-configuration mcscf calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)*half
                  p2(pq1) = p2(pq1) + do(pq1)*fac*val
                endif
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = a(psym,psym)*half
                  p2(pr1) = p2(pr1) - do(pr1)*fac*val
                endif
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)*half
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                endif
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = two*a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = a(psym,psym)*half
                  p2(pr1) = p2(pr1) - do(qs1)*fac*val
                  p2(qs1) = p2(qs1) - do(pr1)*fac*val
                endif
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
              endif
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = thrhf*a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = thrhf*a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                fac = a(psym,psym)*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
            if (iopen(psym).ne.0) then
              fac = thrhf*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              fac = a(psym,psym)
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
            if (iopen(psym).ne.0) then
              fac = thrhf*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              fac = a(psym,psym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
          else
            if ( q .gt. r) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
            if (iopen(psym).ne.0) then
              fac = two*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = one
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = one
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p  =  r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          elseif ( q .eq. s ) then
c           # q  =  s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          endif
        endif
        endif
c
c     # get another buffer?
c
c----------------------sifs loops ----------------------
      enddo
      if ( last .eq. msame ) go to 50
c
c     # close 2-electron integral file
c
      call sifc2f( ifile, info, ierr )
      if ( ierr .ne. 0 ) then
        call bummer('from sifc2f(), ierr=', ierr, faterr )
      endif
      write(6,*) 'total 2e-integrals processed:',ntot
c---------------end of branching -----------------------
c
c     # compute p1(i,j) = quar * fock(i,j)
c     # compute p2(i,j) = half * fock(i,j)
c
      call dscal_wr( nmat, quar, p1, 1 )
c     if (nost.ne.0) call dscal_wr(nmat,half,p2,1)
      call dscal_wr(nmat,half,p2,1)
c
c     # readjust diagonal elements of dt and do back
c
      do lap = 1, lapu
        isym = norbsm(lap)
        is = lap - nbskp(isym)
        ii = nndx(is) + is + nnbskp(isym)
        dt(ii) = dt(ii)*half
c       if (nost.ne.0) do(ii) = do(ii)*half
        do(ii) = do(ii)*half
      enddo
       deallocate(labin,valin,bufin)
c
      return
      end
c deck extrl
      subroutine extrl(dampq,dampsv,ifext,iprct,nmat,next,sf,sf1,sf2)
c
c  extrapolate fock matrices
c
      implicit real*8 (a-h,o-z)
      parameter (a0=0.0d0, a1m8=1.0d-8, a1s2=0.5d0, a1=1.0d0)
      dimension sf(*), sf1(*), sf2(*)
c
      damp = dampq
      if(next.eq.1) go to 48
      if((iprct.eq.0).or.(next.lt.3).or.
     &   ((damp.eq.a0).and.(dampsv.ne.a0))) go to 16
      if (mod(next+1,iprct) .le. 1)  damp = dampsv
      if(mod(next,iprct).eq.0) go to 24
   16 do i=1,nmat
        sf2(i)=sf1(i)
        sf(i)=(sf(i)+damp*sf2(i))/(a1+damp)
        sf1(i)=sf(i)
      enddo
      dampsv = damp
      dampq = damp
      return
c
   24 ifext=1
c     cutoff=a1s2
c     if(cutoff.ge.a1) cutoff=0.99d0
c     if(cutoff.le.a0) cutoff=a0
      do i=1,nmat
        sf(i)=(sf(i)+damp*sf1(i))/(a1+damp)
        diff=sf(i)-sf1(i)
        diff12=sf1(i)-sf2(i)
        if(diff12.ne.a0) then
          eps=diff/diff12
c         if(eps.gt.cutoff) then
          if(eps.gt.a1s2) then
c           sf(i)=sf(i)+diff12*cutoff**2/(a1-cutoff)
            sf(i)=sf(i)+diff12*a1s2
          elseif(eps.lt.-a1) then
            sf(i)=(sf(i)+damp*sf1(i))/(a1+damp)
          elseif(abs((diff-diff12)*diff).ge.a1m8*sf(i)*sf(i)) then
            sf(i)=(sf(i)*sf2(i)-sf1(i)*sf1(i))/(diff-diff12)
          endif
        endif
        sf2(i)=sf1(i)
        sf1(i)=sf(i)
      enddo
      dampsv = dampq
      return
c
   48 do i=1,nmat
        sf1(i)=sf(i)
      enddo
      dampsv = damp
      return
      end
c deck schmit
      subroutine schmit(c,iim1,iswtch,lau,ni,nlist,np,s,thrsh,v)
c
c  schmidt orthonormalize the mo's
c  iswtch.eq.1   orthonormalize all mo's
c  iswtch.eq.0   orthonormalize occupied mo's only
c
      implicit real*8 (a-h,o-z)
      parameter (a0=0.0d0, a1=1.0d0)
      dimension c(*), iim1(*), ni(*), np(*), s(*), v(*)
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
      ki=0
      klla=0
      do 72 la=1,lau
        ipu=np(la)
        if(iswtch.eq.0) then
          nnv=ni(la)
        else
          nnv=ipu
        endif
        if(nnv.eq.0) go to 68
        do 64 i=1,nnv
          lik=ki
          call wzero(ipu,v,1)
          kl=klla
          do k=1,ipu
            do l=1,k-1
              v(k)=v(k)+c(lik+l)*s(kl+l)
            enddo
            do l=1,k
              v(l)=v(l)+c(lik+k)*s(kl+l)
            enddo
            kl=kl+k
          enddo
          ki=ki+ipu
c
c         # normalize the mo's
c
          temp=a0
          do l=1,ipu
            temp=temp+v(l)*c(lik+l)
          enddo
c
          if(temp.le.thrsh) then
            if(temp.le.a0) then
              write (nlist,
     &         '(/''mo'',i4,'' of'',i3,'' irrep cannot be '//
     &         'normalized,'',g18.8/)') i, la, temp
              write(nlist,*)'Is the geometry reasonable?'
              call bummer('schmit: norm<=zero i=', i, faterr )
            endif
            write (nlist,
     &       '(/''normalization of mo'',i4,'' of'',i3,'//
     &        ''' irrep below threshold,'',g18.8/)') i, la, temp
            temp=thrsh
          endif
c
          temp=a1/sqrt(temp)
          do l=1,ipu
            v(l)=temp*v(l)
            c(lik+l)=temp*c(lik+l)
          enddo
          if(i.eq.nnv) go to 64
c
c         # orthogonalize the mo's
c
          lj=ki
          do j=i+1,nnv
            temp=a0
            do l=1,ipu
              temp=temp+v(l)*c(lj+l)
            enddo
cdir$ ivdep
            do l=1,ipu
              c(lj+l)=c(lj+l)-temp*c(lik+l)
            enddo
            lj=lj+ipu
          enddo
   64   enddo
   68   ki=ki+(ipu-nnv)*ipu
        klla=klla+iim1(ipu+1)
   72 enddo
      return
      end
c deck vkx
      subroutine vkx(lau,np,d1,c,xsv)
c
c  from scfarg; modified to use gmxm: d1 = xsv x c (blocked)
c
      implicit real*8(a-h,o-z)
      dimension np(lau),d1(*),c(*),xsv(*)
      parameter (a1=1.0d0)
c
      lapi=1
      do la=1,lau
        ipu=np(la)
        if(ipu.ne.0) then
          call gmxm(xsv(lapi),ipu,c(lapi),ipu,d1(lapi),ipu,a1)
          lapi=lapi+ipu*ipu
        endif
      enddo
      return
      end
c deck tfao
      subroutine tfao(np,eig,y,ffb,lau,nic,nio)
c
c  from scfarg
c
      implicit real*8(a-h,o-z)
      dimension np(*),eig(*),y(*),ffb(*),nic(*),nio(*)
c
      kl=0
      max = 0
      kk=0
      do la=1,lau
        ipu=np(la)
        ihbd = (ipu*ipu+ipu)/2
        call wzero (ihbd,ffb(kk+1),1)
csi     # for compatibility reasons with scfarg
        if( (nic(la) + nio(la)) .eq. 0 ) call wzero(ipu,eig(kl+1),1)
        do i=1,ipu
          kl=kl+1
          min = max + 1
          max = max + ipu
          cm1=eig(kl)
          k=0
          do iq = min,max
            tmp1 = cm1 * y(iq)
            do ip = min, iq
              k = k+1
              ffb(kk+k)=ffb(kk+k)+tmp1*y(ip)
            enddo
          enddo
        enddo
        kk=kk + ihbd
      enddo
      return
      end
c deck outgrd
      subroutine outgrd(alpha,beta,ci1,ci2,fmta,igrad,iofdg,i2con,jofdg,
     &  lau,nic,nio,nlist,nochg,nofdg,nost,np,valofd)
c
c  write the scf information onto the vector file 'vectgrd'
c  taken from program 'scfarg' (si aug9-95)
c
c  s.b., 15.8.1982 , modified for open shell ( cme 10.12.1982 )
c  vectors and occupation numbers of occupied orbitals are written
c  out only  ( cme 30.9.1983 )
c
      implicit real*8(a-h,o-z)
      character*80 fmta
      dimension alpha(*),beta(*),iofdg(*),jofdg(*),nic(*),nio(*),np(*),
     &  valofd(*),nocc(8)
      parameter (a1=1.0d0)
c
c     # use the default format.
      fmta = '(1p4e20.12)'
c     # file vectgrd scf vector file for gradient
      do i=1,lau
        nocc(i)=nic(i)+nio(i)
        if(nocc(i).ne.0) nnsym=i
      enddo
      icmin=0
      ipmax=0
      do l=1,nnsym
        icl=nocc(l)
        if(icl.eq.0) icl=1
        ll=np(l)
        ipmax=ipmax+icl
        jcl=ll
        icx=icmin
        do ic=1,icl
          icx=icx+jcl
        enddo
        ipmax=ipmax+ll-icl
        icmin=icmin+jcl*jcl
      enddo
      write (nlist,50)
   50 format('scf gradient information written to file vectgrd')
      if(nost.eq.0) return
      if(i2con.eq.2) then
        ci11=ci1*ci1
        ci22=ci2*ci2
        ci12=ci1*ci2
        alpha(1)=a1-a1/ci11
        alpha(2)=a1
        alpha(3)=a1-a1/ci22
        beta (1)=a1-a1/ci11
        beta (2)=a1+a1/ci12
        beta (3)=a1-a1/ci22
      endif
      jl=0
      do iost=1,nost
        do jost=1,iost
          jl=jl+1
          if(i2con.ne.2) beta(jl)=alpha(jl)-beta(jl)
          write (nlist,'(2i5,2f12.6)') iost,jost,alpha(jl),beta(jl)
          write (igrad,'(2e20.12)') alpha(jl),beta(jl)
        enddo
      enddo
      write(igrad,'(2i4/(2(2i4,z32)))') nochg,nofdg,
     &  (iofdg(i),jofdg(i),valofd(i), i=1,nofdg)
      write (nlist,'(20i4)')  nochg,nofdg
      do i=1,nofdg
        write (nlist,'(2i5,d20.10)') iofdg(i),jofdg(i),valofd(i)
      enddo
      if(i2con.ne.0) write (nlist,
     &  "('ci  1:',f20.10,/,'ci  2:',f20.10)") ci1,ci2
      close(igrad)
      return
      end
c deck cprint
      subroutine cprint(an,bfnl,c,eig,iswtch,ityp,lau,next,ni,nlist,np,
     &  title)
c
c  print mo's in column format with mo and so labels
c  iswtch.eq.1   print all mo's
c  iswtch.eq.0   print occupied mo's only
c
      implicit real*8 (a-h,o-z)
      character*80 title
      character*8 bfnl
      character*3 ityp
      dimension an(*),bfnl(*),c(*),eig(*),ityp(*),ni(*),np(*),title(*)
c
      if(next.ne.0) then
        if(iswtch.ne.0) then
          write (nlist,900) title(1), title(3)
        else
          write (nlist,910) title(1), next, title(3)
        endif
      endif
      lap=0
      lapi=0
      do 80 la=1,lau
      ipu=np(la)
      iu=ni(la)
      if(iswtch.ne.0) iu=ipu
      if(iu.ne.0) then
        write (nlist,920) ityp(la)
        do il=1,iu,5
          ir=min(il+4,iu)
c         if(il.ne.1) write (nlist,*)
          write (nlist,930) (i, ityp(la), i=il,ir)
          lapi=lapi-ipu
          do ip=1,ipu
            lapi=lapi+1
c           write (nlist,940) ip, ityp(la), mtype(ms(lap+ip)),
c    1        lblnl(mnl(lap+ip)), (c(lapi+ipu*i), i=il,ir)
            write (nlist,941) ip, ityp(la), bfnl(lap+ip),
     1        (c(lapi+ipu*i),i=il,ir)
          enddo
          if(next.ne.0) write (nlist,950) (eig(lap+i), i=il,ir)
          write (nlist,960) (an(lap+i), i=il,ir)
        enddo
      endif
      lap=lap+ipu
      lapi=lapi+ipu*ipu
   80 enddo
      return
  900 format(/5x,'occupied and virtual orbitals'/a/a)
  910 format(/a,28x,'iteration no.',i4/a)
  920 format(/25x,a3,19h molecular orbitals)
  930 format(6x,9hsym. orb.,i8,a3,4(i9,a3))
c 940 format(i4,a3,',',a3,a2,5f12.6)
  941 format(i4,a3,',',a8,5f12.6)
  950 format(/6x,8horb. en.,f14.6,4f12.6)
  960 format(/6x,8hocc. no.,f14.6,4f12.6/)
      end
c deck vksvn
      subroutine vksvn(s,c,xsv,lau,np,v0)
c
c  from scfarg; modified to use mulfs: xsv = c(trans) x s (blocked)
c
      implicit real*8(a-h,o-z)
      dimension s(*),c(*),xsv(*),np(lau),v0(*)
c
      lapq=0
      lapi=0
      do la=1,lau
        ipu=np(la)
        call mulfs(c(lapi+1),s(lapq+1),xsv(lapi+1),ipu)
        lapq=lapq+(ipu*ipu+ipu)/2
        lapi=lapi+ipu*ipu
      enddo
      do k=1,lapi
        v0(k)=c(k)
      enddo
      return
      end
c deck diisa
      subroutine diisa(an,ff,ffso,s,cc,orthtr,olde,oldfoc,oldb,np,cinv,
     &  cinvc0,ep,sd,ethrsh,epslon,iread,lau,mxpul,ndim,ndel,nlist,nmat)
c
c  transform ff(mo) to ffso(so); form error matrix (mo) and
c  transform to olde (orthogonal so)
c
      implicit real*8 (a-h,o-z)
      dimension an(*),ff(*),ffso(*),s(*),cc(*),orthtr(*),olde(*),
     1  oldfoc(*),oldb(*),np(*),cinv(*),cinvc0(*),ep(*),sd(*)
c     (cinv and ep occupy the same space)
      parameter (a1=1.0d0)
c
      lap = 0
      lapq = 0
      lapi = 0
      do 30 la = 1,lau
        ipu = np(la)
        if (ipu.le.0) go to 30
        call mulfs(cc(lapi+1),s(lapq+1),cinv,ipu)
        call tritrn(ipu,ffso(lapq+1),ff(lapq+1),cinv,sd)
        call wzero((ipu*ipu),cinvc0,1)
        call gmxm(cinv,ipu,orthtr(lapi+1),ipu,cinvc0,ipu,a1)
        ipq=0
        do ip=1,ipu
          do iq=1,ip
            ep(ipq+iq)=(an(lap+iq)-an(lap+ip))*ff(lapq+ipq+iq)
          enddo
          ipq=ipq+ip
        enddo
        lap=lap+ipu
        call atritr(ipu,olde(iread*nmat+lapq+1),ep,cinvc0,sd)
        lapq = lapq + ipq
        lapi = lapi + (ipu*ipu)
   30 enddo
c
c  extrapolate ffso
c
      call diis(ffso,olde,oldfoc,oldb,ethrsh,epslon,iread,mxpul,
     $  ndim,ndel,nlist,nmat)
c
c  transform ffso(so) back to ff(mo)
c
      lapq = 0
      lapi = 0
      do la = 1,lau
        ipu = np(la)
        if (ipu.gt.0) then
          call tritrn(ipu,ff(lapq+1),ffso(lapq+1),cc(lapi+1),sd)
          lapq = lapq + (ipu*(ipu+1))/2
          lapi = lapi + ipu*ipu
        endif
      enddo
c
      return
      end
c deck mulfs
      subroutine mulfs(aful,bsym,cful,ipu)
c
c  cful(square) = aful(square,transposed) * bsym(triangular)
c
      implicit real*8 (a-h,o-z)
      dimension aful(ipu,ipu),cful(ipu,ipu),bsym(*)
c
      call wzero(ipu*ipu,cful,1)
c
      lj = 0
      do l = 1,ipu
        do j = 1,l-1
          lj = lj + 1
          do i = 1,ipu
            cful(i,j) = cful(i,j) + aful(l,i)*bsym(lj)
          enddo
          do i = 1,ipu
            cful(i,l) = cful(i,l) + aful(j,i)*bsym(lj)
          enddo
        enddo
        lj = lj + 1
        do i = 1,ipu
          cful(i,l) = cful(i,l) + aful(l,i)*bsym(lj)
        enddo
      enddo
c
      return
      end
c deck tritrn
      subroutine tritrn(n,x,b,c,di)
c
c  transform symmetric matrix stored as lower triangle
c
      implicit real*8 (a-h,o-z)
      dimension x(*), b(*), c(*), di(*)
      parameter (a1s2=0.5d0)
c
      kl=0
      call wzero((n*(n+1))/2,x,1)
      do k=1,n
        call wzero(n,di,1)
        do l=1,k
          kl=kl+1
          val=b(kl)
          if(k.eq.l) val=a1s2*val
          do j=1,n
            di(j)=di(j)+val*c((l-n)+n*j)
          enddo
        enddo
c
        ijs=0
        ki=k
        do i=1,n
          do j=1,i
            x(ijs+j)=x(ijs+j)+di(i)*c((k-n)+n*j)+di(j)*c(ki)
          enddo
          ijs=ijs+i
          ki=ki+n
        enddo
      enddo
      return
      end
c deck atritr
      subroutine atritr(n,x,b,c,di)
c
c  transform antisymmetric matrix stored as lower triangle
c
      implicit real*8 (a-h,o-z)
      dimension x(*), b(*), c(*), di(*)
c
      kl=0
      call wzero((n*(n+1))/2,x,1)
      do k=1,n
        call wzero(n,di,1)
        do l=1,k
          kl=kl+1
          val=b(kl)
          do j=1,n
            di(j)=di(j)+val*c((l-n)+n*j)
          enddo
        enddo
c
        ijs=0
        ki=k
        do i=1,n
          do j=1,i
            x(ijs+j)=x(ijs+j)-di(i)*c((k-n)+n*j)+di(j)*c(ki)
          enddo
          ijs=ijs+i
          ki=ki+n
        enddo
      enddo
      return
      end
c deck diis
      subroutine diis(ffso,olde,oldfoc,oldb,ethrsh,epslon,iread,mxpul,
     $  ndim,ndel,nlist,nmat)
c
c*******************************************************************
c
c....direct inversion in iterative subspace extrapolation procedure:
c        p. pulay, j.comp.chem. 3, 556-560, (1982).
c        h.j. werner, molpro subroutines.
c
c.......variables:
c
c......note: olde,ffso arrays stored as lower triangular symmetry blocks
c
c         ffso = fock matrix (one-hamiltonian form), so basis
c
c         mxpul = input value giving maximum number of saved error
c                 vectors; maximum value = 8; < = 0 means no diis
c         ndim = column dimension of b matrix
c         mxdim = maximum value for ndim = mxpul+1 = 9
c         lenb = ndim*(ndim+1)/2 = dimension of b matrix
c         mxlenb = maximum lenb = 9*(9+1)/2 = 45
c         nndim = np*(np+1)/2 per irrep
c         nmat = size of error matrix = sum of nndim per irrep
c
c         oldb(mxpul*(mxpul+1)/2) = previous b matrix; b(ij)=ei(dot)ej;
c               the lagrange multiplier row is not saved
c         olde(mxpul*nmat) = set of up to mxpul old error matrices
c         oldfoc(mxpul*nmat) = set of up to mxpul old fock matrices
c         dummy1(mxdim) = diis coefficients
c         bb(lenb) = current b matrix
c         beval(lenb) = unit matrix, eigenvalues of b
c         bevect(ndim**2) = eigenvectors of bb
c
c         ndel = counter for deleted b values; bfak = scale for b
c         iread = position to begin reading olde
c         pulthr = 10**(-7) = min value for b elements.
c
c*******************************************************************
c
c  11-sep-91 general cleanup. ia,ibb initialized, bfak initialized
c            and saved. -rls
c
       implicit none
      integer     n1
      parameter ( n1=1 )
      integer     mxdim,   mxlenb
      parameter ( mxdim=9, mxlenb=(mxdim*(mxdim+1))/2 )
      real*8      zero,       one,       pulthr
      parameter ( zero=0.0d0, one=1.0d0, pulthr=1.0d-7 )
c
c     # dummy...
      integer iread, mxpul, ndim, ndel, nlist, nmat
      real*8 ethrsh, epslon, ffso(*), olde(*), oldfoc(*), oldb(*)
c
c     # local...
      integer lapq,ncurr,lenb,i,n,n2,ij,j,nplace,ia,idiag,ijj,j1,ji,
     &  ibb,l
      real*8 emax, bfak, bmax, c
      real*8 dummy1(mxdim), bb(mxlenb), beval(mxlenb), bevect(mxdim**2)
      logical diioff
c
      save bfak
c
      data bfak / one /
c
      ia = 0
      ibb = 0
      ncurr = iread*nmat
c
c  save fock matrix in oldfoc(ncurr+1)
c
      do lapq = 1,nmat
        oldfoc(ncurr+lapq) = ffso(lapq)
      enddo
c
c  if largest element of error < 0.1*energy, only store diis
c  information for use in future iterations.
c
      emax = zero
      do lapq = 1,nmat
        emax = max(emax,abs(olde(ncurr+lapq)))
      enddo
c
      diioff = emax .ge. ethrsh
      lenb = (ndim*(ndim + 1))/2
      n2 = n1 + lenb
c
      if ( ndim .ne. 1 ) then
c       # use the old b-matrix and the old bfak.
        do i = 1, lenb
          bb(i) = oldb(i)
        enddo
      else
        bfak = one
      endif
c
c.....form bij = <ei/ej>  (last row)
c
      do n = 1,ndim
        nplace = mod(iread+mxpul-ndim+n,mxpul)*nmat
        bb(n2+n) = zero
          do lapq = 1,nmat
            bb(n2+n) = bb(n2+n) + olde(ncurr+lapq)*olde(nplace+lapq)
          enddo
        bb(n2+n) = (bfak+bfak)*bb(n2+n)
      enddo
c
      bb(n1) = zero
      bb(n2) = -one
c
      ij = 1
      do i = 1,ndim
        ij = ij + i
        bb(ij) = -one
      enddo
c
      ndim = ndim + 1
      lenb = (ndim*(ndim + 1))/2
c
  200 bmax = zero
c
      ij = n1
      do i = 2,ndim
        do j = 2,i
          bmax = max(bmax,abs(bb(ij+j)))
        enddo
        ij = ij + i
      enddo
c
      bmax = one / bmax
      bfak = bfak * bmax
c
      ij = n1
      do i = 2,ndim
        do j = 2,i
          bb(ij+j) = bb(ij+j)*bmax
        enddo
         ij = ij + i
      enddo
c
      if (diioff) then
        write (nlist,"('diis info stored; extrapolation off')")
        go to  370
      endif
c
c  diagonalize b. beval = eval; bevect = evect.
c
      do i = 1,lenb
        beval(i) = bb(i)
      enddo
c
      call wzero ( ndim*ndim, bevect, 1 )
      do i = 1,ndim
        bevect( (ndim+1)*i - ndim ) = one
      enddo
      call erduw(beval,bevect,ndim,min(pulthr,epslon))
c
c  delete b values with eval < pulthr until all > pulthr.
c
      ia = 1
      idiag = 0
      do i = 1,ndim
        idiag = idiag + i
        if(beval(idiag).le.pulthr) ia = ia + 1
      enddo
c
      if ( ia .ge. ndim+1 ) then
        write (nlist,"(/'?warning: diis matrix ill conditioned'/)")
        return
      endif
c
      if ( ia .ne. 2 ) then
        ijj = n1
        ij = (ia*(ia - 1))/2
        do i = ia,ndim
          ijj = ijj + 1
          bb(ijj) = -one
cdir$ ivdep
          do j = ia,i
            ijj = ijj + 1
            bb(ijj) = bb(ij+j)
          enddo
          ij = ij + i
        enddo
c
        ndim = ndim - (ia - 2)
        ndel = ndel + (ia - 2)
        go to 200
      endif
c
      j1 = n1 - ndim
      idiag = 0
      do j = 1,ndim
        idiag = idiag + j
        beval(idiag) = bevect(j1+j*ndim)/beval(idiag)
      enddo
c
      do i = 1,ndim
        c = zero
        ji = i - ndim
        idiag = 0
        do j = 1,ndim
          idiag = idiag + j
          c = c + bevect(ji + j*ndim)*beval(idiag)
        enddo
        dummy1(i) = -c
      enddo
c
c  scale fock matrix by last b coefficient.
c
      do lapq = 1,nmat
        ffso(lapq) = dummy1(ndim)*ffso(lapq)
      enddo
c
c  modify fock matrix:  f(i) = f(i) + bcoeff(i)*oldfoc(i).
c
      do i = 2,ndim-1
        nplace = mod(iread+mxpul-ndim+i,mxpul)*nmat
        do lapq = 1,nmat
          ffso(lapq) = ffso(lapq) + dummy1(i)*oldfoc(nplace+lapq)
        enddo
      enddo
c
c  save desired part of bmatrix in oldb
c
  370 continue
      if ( ndim .eq. mxpul+1 ) then
        ibb = 0
      else
        ibb = 1
      endif
      l = 1
      ij = -ibb
c
      do i = 2-ibb,ndim
        do j = 2-ibb,i
          oldb(l) = bb(ij + j + 1)
          l = l + 1
        enddo
        ij = ij + i
      enddo
c
c  increment counters
c
      ndim = min(ndim,mxpul)
      iread=mod(iread+1,mxpul)
c
      return
      end
c deck pulay
      subroutine pulay (f,delf,nmat,thr,lau,nic,nio,np,c,v0,esh,msh,
     &  niter,ttr,tmr,tnr,pconv,mxpul)
c
c  diis using dF as error vector; from scfarg
c     coded by r.a. version oct.23 83
c     convergence acceleration a la pulay : diis, c.p.l. 73,393(1980)
c     niter = iteration counter within subroutine
c     nmat = dimension of f in ao basis
c     f = current fock operator
c     delf(nmat,i) = f(i+1) - f(i)
c     also holds old f operator
c     algor. starts if differnce in one-electron energies lt 0.1
c     update if estimated sqrt(delf**2)/nmat.lt.thr
c     or available core exhausted (in delf)
c     g,gg,cc,mm,ll = scratch arrays
c
      implicit real*8(a-h,o-z)
      dimension f(*),delf(*),nic(*),nio(*),np(lau),c(*),v0(*),esh(*)
      dimension cc(11),ll(11),mm(11),g(66),gg(121)
      parameter (a0=0.0d0, a1m14=1.0d-14, a1m12=1.0d-12, a1=1.0d0,
     &   a3=3.0d0)
      save g, tnrold, ttrold
c
c     if(niter.eq.1) then
c       call wzero(121,gg,1)
c       call wzero(66,g,1)
c     endif
      nmax = min(mxpul,8)
      thr2 = thr**2
      niter = niter + 1
      if((niter.eq.1).or.(mxpul.le.0)) go to 70
      ist = (niter-2)*nmat
      do i=1,nmat
        delf(ist+i)=f(i)-delf(ist+i)
      enddo
c
c     set up g matrix: g(ij) = delf(i)*delf(j)
c
      n = niter-1
      nn = (n*niter)/2
      ij = nn - n
      jsta = 0
      do j=1,n
        x=a0
        do k=1,nmat
          x=x+delf(ist+k)*delf(jsta+k)
        enddo
        g(ij+j) = x
        jsta = jsta + nmat
        g(nn+j) = -a1
      enddo
      if (niter.le.2) go to 70
      g(nn+niter) = a0
c
c     invert g, first transferred to gg
c
      ij = 0
      do i=1,niter
        do j=1,i
          ij = ij + 1
          iji = (i-1)*niter + j
          ijj = (j-1)*niter + i
          gg(iji) = g(ij)
          gg(ijj) = g(ij)
        enddo
      enddo
      tol = a1m14*g(nn)
      call osinv1 (gg,niter,d,tol,ll,mm)
c
c     form g**(-1)*b = cc where g**(-1) on gg, note only b(niter).ne.0
c     b(niter) = -1
c
      i = n*niter
      do j=1,niter
        cc(j) = - gg(i+j)
      enddo
      if (cc(niter).ge.thr2.and.niter.le.nmax) go to 70
      pconv=cc(niter)
      if (pconv.le.a0) write(6,40) pconv
   40 format (/'warning: negative norm in pulay',e13.5/)
      pconv=sqrt(abs(pconv))
      fnorm = sqrt(g(nn))
      write (6,'(''pulay for last '',i4,'' iterations d='',d10.2,'//
     & ''' norm(f)='',d10.2,'' lambda='',d10.2))') n,d,fnorm,pconv
c
c     form linear combnation of fock operators
c
      ist = 0
      temp = a0
      do i=1,n
        temp = temp - cc(i)
        do k=1,nmat
          f(k)=f(k)+delf(ist+k)*temp
        enddo
        ist = ist + nmat
      enddo
      thr2 = thr2/a3
      tnr=tnrold
      ttr=ttrold-tnr
      ttr=max(tmr,ttr)
      niter = 0
c
c     diagonalize extrapolated fock matrix and calculate extrapolated
c     eigenvectors
c
      isu=1
      isf=1
      k0p=0
      do la=1,lau
        ipu=np(la)
        if((nic(la)+nio(la)).ne.0) then
          call erduw(f(isf),v0(isu),ipu,a1m12)
          do ip=1,ipu
            issf=isf+(ip+ip*ip)/2-1
            c(k0p+ip)=f(issf)
          enddo
          if(msh.ne.0) then
            do ip=1,ipu
              c(k0p+ip)=c(k0p+ip)-esh(k0p+ip)
            enddo
          endif
          call bubble(v0(isu),c(k0p+1),ipu,ipu)
        endif
        isf=isf+(ipu*(ipu+1))/2
        k0p=k0p+ipu
        isu=isu+ipu*ipu
      enddo
      isu=isu-1
      do k=1,isu
        c(k)=v0(k)
      enddo
      return
c
c     save current f
c
   70 ist = (niter-1)*nmat
      if(niter.eq.1) then
        ttrold=ttr
        tnrold=tnr
        tnr=a0
      endif
      do k=1,nmat
        delf(ist+k)=f(k)
      enddo
      return
      end
c deck osinv1
      subroutine osinv1(a,n,d,tol,l,m)
c
c  from scfarg
c
      implicit real*8(a-h,o-z)
      dimension a(*),m(*),l(*)
      parameter (a0=0.0d0, a1=1.0d0)
c
      d=a1
      nk=-n
      do 190 k=1,n
        nk=nk+n
        l(k)=k
        m(k)=k
        kk=nk+k
        biga=a(kk)
        do j=k,n
          iz=n*(j-1)
          do i=k,n
            ij=iz+i
            if(abs(biga).le.abs(a(ij))) then
              biga=a(ij)
              l(k)=i
              m(k)=j
            endif
          enddo
        enddo
        j=l(k)
        if(j.gt.k) then
          ki=k-n
          do i=1,n
            ki=ki+n
            holo=-a(ki)
            ji=ki-k+j
            a(ki)=a(ji)
            a(ji)=holo
          enddo
        endif
        i=m(k)
        if(i.gt.k) then
          jp=n*(i-1)
          do j=1,n
            jk=nk+j
            ji=jp+j
            holo=-a(jk)
            a(jk)=a(ji)
            a(ji)=holo
          enddo
        endif
        if(abs(biga).lt.tol) then
          d=a0
          return
        endif
        do i=1,n
          if(i.ne.k) then
            ik=nk+i
            a(ik)=a(ik)/(-biga)
          endif
        enddo
        do i=1,n
          ik=nk+i
          ij=i-n
          do j=1,n
            ij=ij+n
            if((i.ne.k).and.(j.ne.k)) then
              kj=ij-i+k
              a(ij)=a(ik)*a(kj)+a(ij)
            endif
          enddo
        enddo
        kj=k-n
        do j=1,n
          kj=kj+n
          if(j.ne.k) a(kj)=a(kj)/biga
        enddo
        d=d*biga
        a(kk)=a1/biga
  190 enddo
c
      do 220 k=n,1,-1
        i=l(k)
        if(i.gt.k) then
          jq=n*(k-1)
          jr=n*(i-1)
          do j=1,n
            jk=jq+j
            holo=a(jk)
            ji=jr+j
            a(jk)=-a(ji)
            a(ji)=holo
          enddo
        endif
        j=m(k)
        if(j.gt.k) then
          ki=k-n
          do i=1,n
            ki=ki+n
            holo=a(ki)
            ji=ki+j-k
            a(ki)=-a(ji)
            a(ji)=holo
          enddo
        endif
  220 enddo
      return
      end
c deck erduw
      subroutine erduw (a,b,na,epslon)
c
c  taken mainly from version 4, aug., 1971, of jacscf, u. of wa.
c  matrix diagonalization by the jacobi method.
c     a = real symmetric matrix to be diagonalized. it is stored
c       by columns with all sub-diagonal elements omitted, so a(i,j) is
c       stored as a((j*(j-1))/2+i).
c     b = matrix to be multiplied by the matrix of eigenvectors.
c     na = dimension of the matrices.
c      epslon is the convergence criterion for off-diagonal elements.
c     modified nov 82 r.a.
c     program keeps track of non-diagonal norm and adjusts thresh
c     dynamically. the eventually quadratic convergence of jacobi is
c     exploited to reduce thresh faster at the end.
c
      implicit real*8 (a-h,o-z)
      parameter (a0=0.0d0, apt03=0.03d0, a1s2=0.5d0, loopi=128)
      dimension a(*), b(*)
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
      if (na.le.1) return
      loopc=loopi
      sumnd=a0
      sum=a0
      ij=0
      do i=1,na
        do j=1,i
          ij=ij+1
          term=a(ij)*a(ij)
          sum=sum+term
          if (i.ne.j) sumnd=sumnd+term
        enddo
      enddo
      thrshg=sqrt((sum+sumnd)/na)*epslon
      small=sumnd*epslon*na*na
c
   32 if (sumnd.ge.small) then
        thresh=sqrt(sumnd+sumnd)/na
      else
        thresh=thresh*apt03
      endif
      thresh=max(thresh,thrshg)
      n=0
      ij=2
      jj=1
      do 88 j=2,na
        jj=jj+j
        jm1=j-1
        ii=0
        do 80 i=1,jm1
          ii=ii+i
          if (abs(a(ij)).lt.thresh) go to 72
          n=n+1
          sumnd=sumnd-a(ij)*a(ij)
          sum=a1s2*(a(jj)+a(ii))
          term=a1s2*(a(jj)-a(ii))
          amax=sign(sqrt(term*term+a(ij)*a(ij)),term)
          c=sqrt((amax+term)/(amax+amax))
          s=a(ij)/(c*(amax+amax))
          a(ii)=sum-amax
          a(jj)=sum+amax
          a(ij)=a0
          im1=i-1
c
          if (im1.ne.0) then
            ki=ii-i
            kj=jj-j
            do k=1,im1
              term=c*a(ki+k)-s*a(kj+k)
              a(kj+k)=s*a(ki+k)+c*a(kj+k)
              a(ki+k)=term
            enddo
          endif
c
          if (jm1.ne.i) then
            ip1=i+1
            ik=ii+i
            kj=ij
            do k=ip1,jm1
              kj=kj+1
              term=c*a(ik)-s*a(kj)
              a(kj)=s*a(ik)+c*a(kj)
              a(ik)=term
              ik=ik+k
            enddo
          endif
c
          if (j.ne.na) then
            jp1=j+1
            ik=jj+i
            jk=jj+j
            do k=jp1,na
              term=c*a(ik)-s*a(jk)
              a(jk)=s*a(ik)+c*a(jk)
              a(ik)=term
              ik=ik+k
              jk=jk+k
            enddo
          endif
c
          ki=im1*na
          kj=jm1*na
          do k=1,na
            ki=ki+1
            kj=kj+1
            term=c*b(ki)-s*b(kj)
            b(kj)=s*b(ki)+c*b(kj)
            b(ki)=term
          enddo
c
   72     ij=ij+1
   80   enddo
        ij=ij+1
   88 enddo
      loopc=loopc-1
      if (loopc.eq.0) then
        call bummer('erduw: too many diagonalization rotations, loopi=',
     &    loopi, faterr)
      endif
      if (thresh.le.thrshg.and.n.eq.0) return
      go to 32
c
      end
c deck bubble
      subroutine bubble ( c, e, n, m )
c
c  sort n energies and n vectors (of length m)
c
      implicit real*8 (a-h,o-z)
      dimension c(*), e(*)
c
      if ( n .eq. 1 ) return
      ipt = 0
c
      do 40 i= 1, n-1
        low = i
        elow = e( i )
c
        do j= i+1 , n
          if ( e(j) .le. elow ) then
            low = j
            elow = e( j )
          endif
        enddo
c
        if ( low .ne. i ) then
c
c         # swap low vector and energy
c
          tmp = e( i )
          e( i ) = e( low )
          e( low ) = tmp
          jpt = ( low-1 ) * m
          do k= 1, m
            tmp = c( ipt+k )
            c( ipt+k ) = c( jpt+k )
            c( jpt+k ) = tmp
          enddo
c
        endif
        ipt = ipt + m
   40 enddo
c
      return
      end
c deck mulpot
      subroutine mulpot(an,c,gap,iim1,ityp,lau,mnl,ms,mtype,ni,nlist,np,
     &  ns,pgap,s,title)
c
c  simple mulliken population analysis
c
      implicit real*8 (a-h,o-z)
      character*80 title
      character*3 ityp, mtype
      real*8 numel
      character*2 lbll
      parameter (a0=0.0d0, lp1u=9)
      dimension an(*), c(*), gap(ns,lp1u), iim1(*), ityp(*), mnl(*),
     1  ms(*), mtype(*), ni(*), np(*), pgap(ns,lp1u,*), s(*), title(*)
      dimension lbll(21), lfrnl(121), tot(6)
      data lbll /' s',' p',' d',' f',' g',' h',' i',' k',' l',' m',
     &      ' n',' o',' q',' r',' t',' u',' v',' w',' x',' y',' z'/,
     &  lfrnl /1,2,1,3,2,4,1,3,5,2,4,6,1,3,5,7,2,4,6,8,1,3,5,7,9,
     & 2,4,6,8,10,1,3,5,7,9,11,2,4,6,8,10,12,1,3,5,7,9,11,13,
     & 2,4,6,8,10,12,14,1,3,5,7,9,11,13,15,2,4,6,8,10,12,14,16,
     & 1,3,5,7,9,11,13,15,17,2,4,6,8,10,12,14,16,18,
     & 1,3,5,7,9,11,13,15,17,19,2,4,6,8,10,12,14,16,18,20,
     & 1,3,5,7,9,11,13,15,17,19,21/
c
      write (nlist,"(/5x,'population analysis'/a/a)") title(1), title(3)
      write(nlist,*)
     & ' NOTE: For HERMIT use spherical harmonics basis sets !!!'
      write(nlist,*)
      call wzero((lp1u*ns),gap,1)
      lapq=0
      lap=0
      lai=0
      lapi=0
      numel=0.0d+00
      do 250 la=1,lau
      ipu=np(la)
      if(ipu.eq.0) go to 250
      iu=ni(la)
      if(iu.eq.0) then
        lap=lap+ipu
        lapq=lapq+iim1(ipu+1)
        go to 240
      endif
      call wzero((lp1u*ns)*iu,pgap,1)
      lapqis=lapq
      lapis=lap
      do 160 i=1,iu
        lapq=lapqis
        lap=lapis
        lai=lai+1
        lapips=lapi
        do ip=1,ipu
          lap=lap+1
          isp=ms(lap)
          lp1p=lfrnl(mnl(lap))
          lapi=lapi+1
          laq=lapis
          laqi=lapips
          do iq=1,ip
            lapq=lapq+1
            laq=laq+1
            isq=ms(laq)
            lp1q=lfrnl(mnl(laq))
            laqi=laqi+1
            prd=c(lapi)*s(lapq)*c(laqi)
            pgap(isp,lp1p,i)=pgap(isp,lp1p,i)+prd
            if(ip.ne.iq) pgap(isq,lp1q,i)=pgap(isq,lp1q,i)+prd
          enddo
        enddo
        do is=1,ns
          do lp1=1,lp1u
            pgap(is,lp1,i)=an(lai)*pgap(is,lp1,i)
            gap(is,lp1)=gap(is,lp1)+pgap(is,lp1,i)
          enddo
        enddo
  160 enddo
      write (nlist,910) ityp(la)
      do il=1,iu,6
        ir=min(il+5,iu)
        if(il.ne.1) write (nlist,*)
        write (nlist,920) (i, ityp(la), i=il,ir)
        do is=1,ns
          do 210 lp1=1,lp1u
            do i=il,ir
              if(pgap(is,lp1,i).ne.a0) then
                write (nlist,930) mtype(is),lbll(lp1),
     &            (pgap(is,lp1,ii),ii=il,ir)
                go to 210
              endif
            enddo
  210     enddo
        enddo
      enddo
  240 lai=lai+(ipu-iu)
      lapi=lapi+ipu*(ipu-iu)
  250 enddo
      write (nlist,940)
      do isl=1,ns,6
        isr = min(isl+5,ns)
        if(isl.ne.1) write (nlist,*)
        call wzero((isr-isl+1),tot,1)
        write (nlist,950) (mtype(is), is=isl,isr)
        do 320 lp1=1,lp1u
          do is=isl,isr
            if(gap(is,lp1).ne.a0) then
              write (nlist,960) lbll(lp1), (gap(iis,lp1), iis=isl,isr)
              do iis=isl,isr
                tot(iis-isl+1) = tot(iis-isl+1) + gap(iis,lp1)
                numel = numel + gap(iis,lp1)
              enddo
              go to 320
            endif
          enddo
  320   enddo
        write(nlist,970) (tot(is-isl+1), is=isl,isr)
      enddo
      write(nlist,'(/,'' Total number of electrons: '',f13.8/)')numel
      return
  910 format(/24x,a3,33h partial gross atomic populations)
  920 format(3x,8hao class,6(i8,a3))
  930 format(4x,a3,a2,4x,6f11.6)
  940 format(//24x,24hgross atomic populations)
  950 format(5x,2hao,3x,6(8x,a3))
  960 format(5x,a2,6x,6f11.6)
  970 format(4x,'total',4x,6f11.6)
      end


      subroutine contr3b(alpha,a,beta,b,dt,do,p1,p2,nndx,ifile1,iju,
     .  info,
     &  iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,i2con,
     .   fordint)
      implicit real*8 (a-h,o-z)
c
c  Compute elements of fock matrix which is the P and Q matrix (see R.M.
c  Pitzer's O.S.U.-T.C.G. report No. 101) directly from AO integrals,
c  rather than the supermatrix of AO integrals. When this routine is
c  called, the routines contr2 and twoint will be skipped.
c
c  Vudhichai Parasuk, U. Vienna, on leave from Chulalongkorn U, Bangkok
c
c  For closed- and open-shell RHF and 2-configuration MCSCF calculations
c
      parameter (nbuf = 4096)
c     # integral continuation parameter (cf. sifsanl.doc, sifrd2)
      parameter (msame = 0)
c
      character*(*) fordint
      logical initrd
      real*8 alpha(*),a(lau,*),beta(*),b(lau,*),dt(*),do(*),p1(*),p2(*)
      integer nndx(*),ifile1,iju(*),info(6),iopen(*),lapu,lau,lmd(*),
     &  nbskp(*),nmat,nnbskp(*),norbsm(*),nost,np(*),i2con
c
      real*8 valin(nbuf), bufin(nbuf), val, fac
      integer labin(4,nbuf),ibitv(nbuf),ierr,ita,itb,ifmt,last,numi
      integer ifile,iost
      integer pq1,rs1,pr1,qs1,ps1,qr1,p,q,r,s,psym,qsym,rsym,ssym,x
c
      real*8 quar, half, one, thrhf, two, three, four
      parameter (quar = 0.25d0, half = 0.5d0, one = 1.0d0,
     &  thrhf = 1.5d0, two = 2.0d0, three = 3.0d0, four = 4.0d0)
      real*8, allocatable :: buffer(:)
      integer mult(8,8)
      integer qmax,smax,imatrd,ibufrd,ioff
      integer ptst,qtst,rtst,stst
      integer lnndx
       integer  mcpsym,mcqsym,mcrsym,mcssym,irc,iflag,nmatl,nnbuf,
     .    nmatrd,i1,i2
c
      logical ordopened
      data ordopened /.false./
      character*130 fname
      data mult /
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
      save mult,ordopened
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
      lnndx(i)=i*(i-1)/2
c
c     # initialize p1 (P matrix) and p2 (Q matrix) to zero
c
c
c     # adjust diagonal elements of dt and do to appropriate values
c
      do lap = 1, lapu
        isym = norbsm(lap)
        is = lap - nbskp(isym)
        ii = nndx(is) + is + nnbskp(isym)
c       write(6,*) 'DTL:',lap,isym,is,ii
        dt(ii) = dt(ii)*two
        do(ii) = do(ii)*two
      enddo
c
c     # reindex alpha and beta by symmetry of irreps
c
      call izero_wr(lau,iopen,1)
      do 30 iost = 1, nost
        x = 0
        do la = 1, lau
          x = x + nndx(np(la)+1)
          if (iju(iost).eq.x) then
            lmd(iost) = la
            iopen(la) = 1
            go to 30
          endif
        enddo
  30  enddo
      do iost = 1, nost
        ii = nndx(iost)
        do jost = 1, iost
          a(lmd(iost),lmd(jost)) = alpha(ii+jost)
          a(lmd(jost),lmd(iost)) = alpha(ii+jost)
          b(lmd(iost),lmd(jost)) = beta(ii+jost)
          b(lmd(jost),lmd(iost)) = beta(ii+jost)
        enddo
      enddo
c        write(6,*) 'np=',np(1:lau)
c        write(6,*) 'nbskp=',nbskp(1:lau)
c        write(6,*) 'nnbskp=',nnbskp(1:lau)
c        write(6,*) 'nndx=',nndx(1:lapu)
c        write(6,*) 'norbsm=',norbsm(1:lau)
c        write(6,*) 'i2con,nost,nmat=',i2con,nost,nmat
c        write(6,*) 'ddot (dt)=',ddot(nmat,dt,1,dt,1)
c        write(6,'(5f12.6)') dt(1:nmat)
 
c       if (.not.ordopened) then
         i2=0 
         i1=45
          call mcOpnOrd(irc,i2,fordint(1:7),i1)
c         ordopened=.true.
c       endif
       allocate (buffer(5000000))
       maxspace=5000000
       do  psym=1,lau
        do qsym=1,psym
          do rsym=1,psym
            do ssym=1,rsym
             if (mult(mult(psym,qsym),mult(rsym,ssym)).ne.1) cycle 
             if (lnndx(psym)+qsym.lt.lnndx(rsym)+ssym) cycle 
             if (rsym.eq.ssym) then
               nnbuf=np(rsym)*(np(rsym)+1)/2
             else           
               nnbuf=np(rsym)*(np(ssym))
             endif
             if (nnbuf.eq.0) cycle 
             if (psym.eq.qsym) then
               nnmat=np(psym)*(np(psym)+1)/2
             else
               nnmat=np(psym)*np(qsym)
             endif
             if (nnmat.eq.0) cycle 
            if (psym.eq.qsym) then
               if (psym.eq.rsym) then
                 itype = 1
               else
                 itype = 2
               endif
            elseif(psym.eq.rsym) then
                 itype = 3
            else
                 itype = 4
                 cycle 
            endif

             nmatl=(maxspace-1)/nnbuf
             p=1
             q=0
             if (psym.eq.qsym) then
                qmax=p
             else
                qmax=np(qsym)
             endif
             do ii=1,(nnmat-1)/nmatl+1
             if(ii.eq.1) then
                  iflag=1
             else
                  iflag=2
             endif
             mcpsym=psym
             mcqsym=qsym
             mcrsym=rsym 
             mcssym=ssym
             call mcrdord(irc,iflag,psym,qsym,rsym,ssym,buffer,
     .                    maxspace,nmatrd)
c
c        psym!=qsym   always p>q
c        rsym!=ssym   always r>s
c        pqsym!=rssym always pq>rs
c
c             write(6,151) nmatrd,iflag,psym,qsym,rsym,ssym
 151    format('mcordrd:',i8,' ij blocks, iflag=',i3,' pqrssym=',4i3)
              ioff=0
              do imatrd=1,nmatrd
               q=q+1
               if (q.gt.qmax) then
                  q=1
                  p=p+1
                   if (psym.eq.qsym) qmax=p
               endif
               r=1
               s=0
               if (rsym.eq.ssym) then
                   smax=r
               else
                   smax=np(ssym)
               endif
               do ibufrd=1,nnbuf
c
c       each buffer contains  all r,s combinations
c
                ioff=ioff+1
                val=buffer(ioff)
                s=s+1
                if (s.gt.smax) then
                   s=1
                   r=r+1
                   if (rsym.eq.ssym) smax=r
                endif
                if (abs(val).lt.1.d-10) cycle  

                 if (psym.eq.rsym .and. p.lt.r) cycle      
                 if (psym.eq.rsym .and. qsym.eq.ssym .and. p.eq.r
     .                .and. q.lt.s) cycle 


                ptst = p + nbskp(psym)
                qtst = q + nbskp(qsym)
                rtst = r + nbskp(rsym)
                stst = s + nbskp(ssym)
c
c          always  r>=s  (in absolute index)
c          always  p>=q  (in absolute index)
c          always  pq >= rs (in absolute index)

c          write(6,81) ptst,qtst,rtst,stst, val
  81       format('pqrs=',4i4,' val=',f18.8)
c
c
c       # test for two-configuration mcscf calculation
c
        if (i2con.ne.2) then
c
c       # test for closed- or open-shell calculation
c
        if (nost.eq.0) then
c
c       # closed-shell calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
          else
            if ( q .gt. r ) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
            endif
          elseif ( q .eq. s ) then
c           # q = s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
            endif
          endif
        endif
c       write(6,154) ddot(nmat,p1,1,p1,1)
154     format('ddot(p1)=',f15.8)
        else
c
c       # open-shell calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = (a(psym,psym) + b(psym,psym))*half
                  p2(pq1) = p2(pq1) + do(pq1)*fac*val
                endif
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = (a(psym,psym) - b(psym,psym))*half
                  p2(pr1) = p2(pr1) - do(pr1)*fac*val
                endif
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = (a(psym,psym) + b(psym,psym))
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = (a(psym,psym) + b(psym,psym))*half
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                endif
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = two*a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = (a(psym,psym) - b(psym,psym))*half
                  p2(pr1) = p2(pr1) - do(qs1)*fac*val
                  p2(qs1) = p2(qs1) - do(pr1)*fac*val
                endif
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
              if (iopen(psym).ne.0) then
                fac = (a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = (a(psym,psym) + b(psym,psym))
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
              endif
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = (three*a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = (three*a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
            if (iopen(psym).ne.0) then
              fac = (three*a(psym,psym) + b(psym,psym))*half
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
            if (iopen(psym).ne.0) then
              fac = (three*a(psym,psym) + b(psym,psym))*half
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
          else
            if ( q .gt. r ) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
            if (iopen(psym).ne.0) then
              fac = two*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = a(psym,rsym)
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two*a(psym,rsym)
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = a(psym,rsym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two*a(psym,rsym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p  =  r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          elseif ( q .eq. s ) then
c           # q = s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          endif
        endif
        endif
        else
c
c       # two-configuration mcscf calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)*half
                  p2(pq1) = p2(pq1) + do(pq1)*fac*val
                endif
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = a(psym,psym)*half
                  p2(pr1) = p2(pr1) - do(pr1)*fac*val
                endif
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)*half
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                endif
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = two*a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = a(psym,psym)*half
                  p2(pr1) = p2(pr1) - do(qs1)*fac*val
                  p2(qs1) = p2(qs1) - do(pr1)*fac*val
                endif
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
              endif
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = thrhf*a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = thrhf*a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                fac = a(psym,psym)*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
            if (iopen(psym).ne.0) then
              fac = thrhf*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              fac = a(psym,psym)
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
            if (iopen(psym).ne.0) then
              fac = thrhf*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              fac = a(psym,psym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
          else
            if ( q .gt. r) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
            if (iopen(psym).ne.0) then
              fac = two*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = one
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = one
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p  =  r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          elseif ( q .eq. s ) then
c           # q  =  s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          endif
        endif
        endif
c
c     # get another buffer?
c
c--------------- molcas loops ---------------------------
       enddo
       enddo
       enddo
       enddo
       enddo
       enddo
       enddo
c
c     # compute p1(i,j) = quar * fock(i,j)
c     # compute p2(i,j) = half * fock(i,j)
c
c     if (nost.ne.0) call dscal_wr(nmat,half,p2,1)
c
c     # readjust diagonal elements of dt and do back
c
      do lap = 1, lapu
        isym = norbsm(lap)
        is = lap - nbskp(isym)
        ii = nndx(is) + is + nnbskp(isym)
        dt(ii) = dt(ii)*half
c       if (nost.ne.0) do(ii) = do(ii)*half
        do(ii) = do(ii)*half
      enddo
c
       deallocate(buffer)
      call daclos(i1)

      return
      end

      subroutine contr3c(alpha,a,beta,b,dt,do,p1,p2,nndx,ifile1,iju,
     .  info,
     &  iopen,lapu,lau,lmd,nbskp,nmat,nnbskp,norbsm,nost,np,i2con)
      implicit real*8 (a-h,o-z)
c
c  Compute elements of fock matrix which is the P and Q matrix (see R.M.
c  Pitzer's O.S.U.-T.C.G. report No. 101) directly from AO integrals,
c  rather than the supermatrix of AO integrals. When this routine is
c  called, the routines contr2 and twoint will be skipped.
c
c  Vudhichai Parasuk, U. Vienna, on leave from Chulalongkorn U, Bangkok
c
c  For closed- and open-shell RHF and 2-configuration MCSCF calculations
c
      parameter (nbuf = 4096)
c     # integral continuation parameter (cf. sifsanl.doc, sifrd2)
      parameter (msame = 0)
      integer*4 m8,m16
      parameter (m8=255,m16=65535)
      integer*4 n8,n16,n24
      parameter (n8=-8,n16=-16,n24=-24)
c
      real*8 alpha(*),a(lau,*),beta(*),b(lau,*),dt(*),do(*),p1(*),p2(*)
      integer nndx(*),ifile1,iju(*),info(6),iopen(*),lapu,lau,lmd(*),
     &  nbskp(*),nmat,nnbskp(*),norbsm(*),nost,np(*),i2con
c
      real*8 val, fac
      integer ierr,ita,itb,ifmt,last,numi
      integer ifile,iost
      integer pq1,rs1,pr1,qs1,ps1,qr1,p,q,r,s,psym,qsym,rsym,ssym,x
      real*8, allocatable :: buf(:)
      integer*4, allocatable :: ibuf(:,:)
c
      real*8 quar, half, one, thrhf, two, three, four
      parameter (quar = 0.25d0, half = 0.5d0, one = 1.0d0,
     &  thrhf = 1.5d0, two = 2.0d0, three = 3.0d0, four = 4.0d0)
      real*8, allocatable :: buffer(:)
      integer mult(8,8)
      integer qmax,smax,imatrd,ibufrd,ioff
      integer ptst,qtst,rtst,stst
      integer NSYM,NAOS(8),LBUF,NIBUF,NBITS
       integer  mcpsym,mcqsym,mcrsym,mcssym,irc,iflag,nmatl,nnbuf,
     .    nmatrd,i1,i2
c
      logical ordopened
      data ordopened /.false./
      character*130 fname
      data mult /
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
      save mult,ordopened
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
c     # initialize p1 (P matrix) and p2 (Q matrix) to zero
c
      call wzero (nmat,p1,1)
      call wzero (nmat,p2,1)
c
c     # adjust diagonal elements of dt and do to appropriate values
c
      do lap = 1, lapu
        isym = norbsm(lap)
        is = lap - nbskp(isym)
        ii = nndx(is) + is + nnbskp(isym)
c       write(6,*) 'DTL:',lap,isym,is,ii
        dt(ii) = dt(ii)*two
        do(ii) = do(ii)*two
      enddo
c
c     # reindex alpha and beta by symmetry of irreps
c
      call izero_wr(lau,iopen,1)
      do 30 iost = 1, nost
        x = 0
        do la = 1, lau
          x = x + nndx(np(la)+1)
          if (iju(iost).eq.x) then
            lmd(iost) = la
            iopen(la) = 1
            go to 30
          endif
        enddo
  30  enddo
      do iost = 1, nost
        ii = nndx(iost)
        do jost = 1, iost
          a(lmd(iost),lmd(jost)) = alpha(ii+jost)
          a(lmd(jost),lmd(iost)) = alpha(ii+jost)
          b(lmd(iost),lmd(jost)) = beta(ii+jost)
          b(lmd(jost),lmd(iost)) = beta(ii+jost)
        enddo
      enddo

         CALL GPOPEN(LUINTA,'AOTWOINT','OLD',' ',' ',IDUMMY,.FALSE.)
         CALL MOLLAB('BASINFO ',LUINTA,LUPRI)
         READ (LUINTA) NSYM,(NAOS(I),I=1,8),LBUF,NIBUF,NBITS
c        write(6,*) 'AOTWOINT BASINFO:'
c        write(6,53) nsym,(naos(i),i=1,nsym)
c        write(6,54) lbuf,nibuf,nbits 
 53      format('nsym=',i3,' nbfpsy=',8i4)
 54      format('buflen=',i6,' nibuf=',i2,' nbits=',i3)
         if (nibuf.eq.2 .and. nbits.ne.16) 
     .    call bummer('cntr3c: inconsistent nibuf,nbits',nibuf,2)
         if (nibuf.eq.1 .and. nbits.ne.8) 
     .    call bummer('cntr3c: inconsistent nibuf,nbits',nibuf,2)
         if (nsym.ne.lau) 
     .    call bummer('cntr3c: inconsistent nsym,lau',nsym,2)
         do i=1,lau
           if (np(i).ne.naos(i)) 
     .    call bummer('cntr3c: inconsistent np(i),naos(i) i=',i,2)
         enddo
         
         allocate(ibuf(lbuf,nibuf),buf(lbuf))

         CALL MOLLAB('BASTWOEL',LUINTA,LUPRI)
   50 continue
        READ(LUINTA,err= 61) buf(1:lbuf),ibuf(1:lbuf,1:nibuf),numi
       if (numi.le.0) goto 51
c
c     # loop over integrals
c
      do iint = 1, numi
        if (nibuf.eq.2) then
          p=iand(ishft(ibuf(iint,1),n16),m16)
          q=iand(ibuf(iint,1),m16)
          r=iand(ishft(ibuf(iint,2),n16),m16)
          s=iand(ibuf(iint,2),m16)
        else
          p=iand(ishft(ibuf(iint,1),n24),m8)
          q=iand(ishft(ibuf(iint,1),n16),m8)
          r=iand(ishft(ibuf(iint,1),n8),m8)
          s=iand(ibuf(iint,1),m8)
        endif
          val = buf(iint)
c     we should find canonically ordered indices p>q, r>s , pq>rs
c     at this stage already
c
        psym = norbsm(p)
        qsym = norbsm(q)
        rsym = norbsm(r)
        ssym = norbsm(s)
c
        p = p - nbskp(psym)
        q = q - nbskp(qsym)
        r = r - nbskp(rsym)
        s = s - nbskp(ssym)
c
c       # determine the symmetry block type.  note that itype=4
c       # integrals do not contribute to the fock matrix.
c
        if (psym.eq.qsym) then
          if (psym.eq.rsym) then
            itype = 1
          else
            itype = 2
          endif
        elseif(psym.eq.rsym) then
          itype = 3
        else
          itype = 4
        endif

c
c       # test for two-configuration mcscf calculation
c
        if (i2con.ne.2) then
c
c       # test for closed- or open-shell calculation
c
        if (nost.eq.0) then
c
c       # closed-shell calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
          else
            if ( q .gt. r ) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
            endif
          elseif ( q .eq. s ) then
c           # q = s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
            endif
          endif
        endif
c       write(6,154) ddot(nmat,p1,1,p1,1)
154     format('ddot(p1)=',f15.8)
        else
c
c       # open-shell calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = (a(psym,psym) + b(psym,psym))*half
                  p2(pq1) = p2(pq1) + do(pq1)*fac*val
                endif
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = (a(psym,psym) - b(psym,psym))*half
                  p2(pr1) = p2(pr1) - do(pr1)*fac*val
                endif
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = (a(psym,psym) + b(psym,psym))
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = (a(psym,psym) + b(psym,psym))*half
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                endif
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = two*a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = (a(psym,psym) - b(psym,psym))*half
                  p2(pr1) = p2(pr1) - do(qs1)*fac*val
                  p2(qs1) = p2(qs1) - do(pr1)*fac*val
                endif
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
              if (iopen(psym).ne.0) then
                fac = (a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = (a(psym,psym) + b(psym,psym))
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
              endif
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = (three*a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = (three*a(psym,psym) + b(psym,psym))*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                fac = (a(psym,psym) - b(psym,psym))*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
            if (iopen(psym).ne.0) then
              fac = (three*a(psym,psym) + b(psym,psym))*half
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
            if (iopen(psym).ne.0) then
              fac = (three*a(psym,psym) + b(psym,psym))*half
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
          else
            if ( q .gt. r ) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
            if (iopen(psym).ne.0) then
              fac = two*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = (a(psym,psym) - b(psym,psym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = a(psym,rsym)
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two*a(psym,rsym)
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = a(psym,rsym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two*a(psym,rsym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p  =  r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          elseif ( q .eq. s ) then
c           # q = s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = (a(psym,qsym) - b(psym,qsym))*half
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = (a(psym,qsym) - b(psym,qsym))
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          endif
        endif
        endif
        else
c
c       # two-configuration mcscf calculation
c
        if ( itype .eq. 1 ) then
c
c         # symmetry block type (aa:aa)
c
          nsymof = nnbskp(psym)
          pq1 = nndx(p) + q + nsymof
          rs1 = nndx(r) + s + nsymof
c
          if ( p .eq. q ) then
            if ( r .eq. s ) then
              if ( p .eq. r ) then
c               # (11:11)
                p1(pq1) = p1(pq1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)*half
                  p2(pq1) = p2(pq1) + do(pq1)*fac*val
                endif
              else
c               # (22:11)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                p1(pr1) = p1(pr1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = a(psym,psym)*half
                  p2(pr1) = p2(pr1) - do(pr1)*fac*val
                endif
              endif
            else
              if ( p .eq. r ) then
c               # (22:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*two
                p1(rs1) = p1(rs1) + dt(pq1)*val
                if (iopen(psym).ne.0) then
                  fac = a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)*half
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                endif
              else
c               # (33:21)
                p1(pq1) = p1(pq1) + dt(rs1)*val*four
                p1(rs1) = p1(rs1) + dt(pq1)*val*two
                pr1 = nndx(p) + r + nsymof
                qs1 = nndx(q) + s + nsymof
                p1(pr1) = p1(pr1) - dt(qs1)*val
                p1(qs1) = p1(qs1) - dt(pr1)*val
                if (iopen(psym).ne.0) then
                  fac = two*a(psym,psym)
                  p2(pq1) = p2(pq1) + do(rs1)*fac*val
                  fac = a(psym,psym)
                  p2(rs1) = p2(rs1) + do(pq1)*fac*val
                  fac = a(psym,psym)*half
                  p2(pr1) = p2(pr1) - do(qs1)*fac*val
                  p2(qs1) = p2(qs1) - do(pr1)*fac*val
                endif
              endif
            endif
          elseif ( r .eq. s ) then
            if ( q .lt. r ) then
c             # (31:22)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            elseif ( q .eq. r ) then
c             # (21:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val
              p1(rs1) = p1(rs1) + dt(pq1)*val*two
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)*half
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
              endif
            else
c             # (32:11)
              p1(pq1) = p1(pq1) + dt(rs1)*val*two
              p1(rs1) = p1(rs1) + dt(pq1)*val*four
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = two*a(psym,psym)
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( p .eq. r ) then
            if ( q .eq. s ) then
c             # (21:21)
              p1(pq1) = p1(pq1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = thrhf*a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                fac = a(psym,psym)*half
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # (32:31)
              p1(pq1) = p1(pq1) + dt(rs1)*val*three
              p1(rs1) = p1(rs1) + dt(pq1)*val*three
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0) then
                fac = thrhf*a(psym,psym)
                p2(pq1) = p2(pq1) + do(rs1)*fac*val
                p2(rs1) = p2(rs1) + do(pq1)*fac*val
                fac = a(psym,psym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
                fac = a(psym,psym)*half
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          elseif ( q .eq. r ) then
c           # (32:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            ps1 = nndx(p) + s + nsymof
            qr1 = nndx(q) + r + nsymof
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val*two
            if (iopen(psym).ne.0) then
              fac = thrhf*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              fac = a(psym,psym)
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          elseif ( q .eq. s ) then
c           # (31:21)
            p1(pq1) = p1(pq1) + dt(rs1)*val*three
            p1(rs1) = p1(rs1) + dt(pq1)*val*three
            pr1 = nndx(p) + r + nsymof
            qs1 = nndx(q) + s + nsymof
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val*two
            if (iopen(psym).ne.0) then
              fac = thrhf*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              fac = a(psym,psym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
          else
            if ( q .gt. r) then
c             # (43:21)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(q) + r + nsymof
            elseif ( q .lt. s ) then
c             # (41:32)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(s) + q + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            else
c             # (42:31)
              pr1 = nndx(p) + r + nsymof
              qs1 = nndx(q) + s + nsymof
              ps1 = nndx(p) + s + nsymof
              qr1 = nndx(r) + q + nsymof
            endif
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            p1(pr1) = p1(pr1) - dt(qs1)*val
            p1(qs1) = p1(qs1) - dt(pr1)*val
            p1(ps1) = p1(ps1) - dt(qr1)*val
            p1(qr1) = p1(qr1) - dt(ps1)*val
            if (iopen(psym).ne.0) then
              fac = two*a(psym,psym)
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
              fac = a(psym,psym)*half
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
              p2(ps1) = p2(ps1) - do(qr1)*fac*val
              p2(qr1) = p2(qr1) - do(ps1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 2 ) then
c
c         # symmetry block type (aa:bb).
c
          pq1 = nndx(p) + q + nnbskp(psym)
          rs1 = nndx(r) + s + nnbskp(rsym)
          if ( p .eq. q ) then
c           # p equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = one
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          else
c           # p not equal q
            p1(rs1) = p1(rs1) + dt(pq1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two
              p2(rs1) = p2(rs1) + do(pq1)*fac*val
            endif
          endif
          if ( r .eq. s ) then
c           # r equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*two
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = one
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          else
c           # r not equal s
            p1(pq1) = p1(pq1) + dt(rs1)*val*four
            if (iopen(psym).ne.0.and.iopen(rsym).ne.0) then
              fac = two
              p2(pq1) = p2(pq1) + do(rs1)*fac*val
            endif
          endif
c
        elseif ( itype .eq. 3 ) then
c
c         # symmetry block type (ab:ab)
c
          pr1 = nndx(p) + r + nnbskp(psym)
          if ( q .lt. s ) then
c           # q < s
            qs1 = nndx(s) + q + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p  =  r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          elseif ( q .eq. s ) then
c           # q  =  s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(pr1) = p1(pr1) - dt(qs1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(pr1) = p2(pr1) - do(qs1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(qs1) = p1(qs1) - dt(pr1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            else
c             # p = r
              p1(qs1) = p1(qs1) - dt(pr1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(qs1) = p2(qs1) - do(pr1)*fac*val
              endif
            endif
          else
c           # q > s
            qs1 = nndx(q) + s + nnbskp(qsym)
            p1(qs1) = p1(qs1) - dt(pr1)*val
            if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
              fac = half - half*a(psym,qsym)
              p2(qs1) = p2(qs1) - do(pr1)*fac*val
            endif
            if ( p .gt. r ) then
c             # p > r
              p1(pr1) = p1(pr1) - dt(qs1)*val
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = half - half*a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            else
c             # p = r
              p1(pr1) = p1(pr1) - dt(qs1)*val*two
              if (iopen(psym).ne.0.and.iopen(qsym).ne.0) then
                fac = one - a(psym,qsym)
                p2(pr1) = p2(pr1) - do(qs1)*fac*val
              endif
            endif
          endif
        endif
        endif
c
      enddo
      go to 50
 51   continue
c
c     # close 2-electron integral file
c
      call gpclose(LUINTA,'KEEP')
c---------------end of branching -----------------------
c
c     # compute p1(i,j) = quar * fock(i,j)
c     # compute p2(i,j) = half * fock(i,j)
c
      call dscal_wr( nmat, quar, p1, 1 )
c     if (nost.ne.0) call dscal_wr(nmat,half,p2,1)
      call dscal_wr(nmat,half,p2,1)
c
c     # readjust diagonal elements of dt and do back
c
      do lap = 1, lapu
        isym = norbsm(lap)
        is = lap - nbskp(isym)
        ii = nndx(is) + is + nnbskp(isym)
        dt(ii) = dt(ii)*half
c       if (nost.ne.0) do(ii) = do(ii)*half
        do(ii) = do(ii)*half
      enddo
       deallocate(ibuf,buf)
       return
 61    write(0,*) 'Error reading integrals from AOTWOINT' 
c
      return
      end

*@ifdef corehole
**
**
*       subroutine getcorehole_info
*       implicit none
*       logical lexist
*       integer  ncorehole,nchindex,ioption
*       common /coreholeinfo/ ncorehole,ioption, nchindex(2,20)
*       character*80 title
*       character*10 option
*       integer i
*       inquire(file='core_hole_states',exist=lexist)
*       if (lexist) then
*         open (file='core_hole_states',unit=15,form='formatted')
*         read(15,'(a80)') title
*         read(15,*) ncorehole
*         if (ncorehole.gt.20) 
*     .       call bummer('core_hole_states: too many orbs',ncorehole,2)
*         do i=1,ncorehole
*          read(15,*) nchindex(1:2,i)
*         enddo
*          read(15,*) option
*          if (option.ne.'ignorecore' .and. option.ne.'freezecore'
*     .          .and. option.ne.'freezeoccp') then
*          call bummer('invalid option in core_hole_states' //
*     .                 '(ignorecore|freeezcore|freezeoccp)',0,2)
*          endif
*         if (option.eq.'ignorecore') ioption=0
*         if (option.eq.'freezecore') ioption=1
*         if (option.eq.'freezeoccp') ioption=-1
*         close(15)
*         write(6,*) 'getcorehole_info'
*         write(6,*) title
*         write(6,*) 'ncorehole=',ncorehole
*         write(6,'(a,20(i2,":",i2,2x))') 'nchindex(*)=',
*     .                       nchindex(1:2,1:ncorehole)
*         write(6,*) 'ioption=',ioption,'option=',option
*       else
*         ncorehole=0
*         ioption=0
*         write(6,*) 'no corehole info'
*       endif 
*        return
*       end
**
*       subroutine apply_corehole(fock,isym,n,nocc)
*       implicit none
*       real*8 fock(*) 
*       integer n,isym,nocc
*       integer i,icore,nn,irow,m,l(1023)
*       character*4 block
**
*c      this is the upper-triangular fock matrix block of 
*c      irrep isym (dimension n) 
*       integer  ncorehole,nchindex,ioption
*       common /coreholeinfo/ ncorehole,ioption, nchindex(2,20)
**
*       if (ioption.eq.0) then 
*c        write(block,'(a3,i1)') 'irr',isym
*c       call plblkt('unmodified corehole fock',fock,n,block,3,6)
*        return
*       endif
*       if (ioption.eq.1) then 
*          do icore=1,ncorehole
*           if (nchindex(2,icore).ne.isym) cycle
*c            remove all off-diagonal elements belonging to this orbital
*           nn=nchindex(1,icore) 
*c          delete column nn
*           m=nn*(nn-1)/2+1
*           fock(m:m+nn-2)=0.0d0
*c          delete row nn
*           m=nn*(nn+1)/2
*           do irow=nn,n
*            m=m+irow
*            fock(m)=0.0d0
*           enddo
*         enddo
*         write(block,'(a3,i1)') 'irr',isym
*        call plblkt('modified corehole fock',fock,n,block,3,6)
*         return
*       endif 
*       if (ioption.eq.-1) then
*            l(1:nocc)=0
*            do icore=1,ncorehole
*            if (nchindex(2,icore).eq.isym) l(nchindex(1,icore))=1
*            enddo
*c          remove all columns
*           do i=1,nocc
*            if (l(i).eq.0) then
*               m=i*(i-1)/2+1
*               fock(m:m+i-2)=0.0d0
*            endif
*           enddo
*c          remove all rows
*            do i=1,nocc
*              if (l(i).eq.0) then
*              m=i*(i+1)/2
*              do irow=i,n
*                m=m+irow
*                fock(m)=0.0d0
*              enddo
*              endif
*            enddo
*         write(block,'(a3,i1)') 'irr',isym
*        call plblkt('modified occupied fock',fock,n,block,3,6)
*           return
*         endif
*         return
*         end
*@endif 

            
 



