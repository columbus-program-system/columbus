      LOGICAL SOLVNT
      COMMON /CBISOL/ ORICAV(3),RCAV(3),EPDIEL,
     &                LCAVMX,LMTOT,LMNTOT,NCNTCV,SOLVNT
C     ... LCAVMX = max l in moment expansion for solvent cavity
C         LMTOT  = number of spherical components for R(l,m)
C         LMNTOT = number of cartesian components for RC(L,M,N)
