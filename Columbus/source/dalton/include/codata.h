C
C  From
C  "The 1986 CODATA Recommended Values of the Fundamental Physical 
C  Constants"

C  E.R.Cohen and B.N.Taylor, J.Phys.Chem.Ref.Data, 17,1795 (1988)
C
      PARAMETER ( XTJ = 4.359 748 2 D-18, XTKAYS = 21 974 4.63067 D0,
     *            XTHZ = 6.579 683 899 9D15, XTEV = 27.211 396 D0,
     *            XTANG = .529 177 249 D0)
      PARAMETER (ALPHAC = 7.29735308 D-3)
