      LOGICAL BIGVEC, SEGMEN, SEGMSH, SPHRSH
      COMMON /BLOCKS/ CENTSH(MXSHEL,3),
     &                MAXSHL, BIGVEC, SEGMEN,NLRGBL,
     &                NHKTSH(MXSHEL), KHKTSH(MXSHEL), KCKTSH(MXSHEL),
     &                ISTBSH(MXSHEL), NUCOSH(MXSHEL), NORBSH(MXSHEL),
     &                NSTRSH(MXSHEL), NCNTSH(MXSHEL), NSETSH(MXSHEL,2),
     &                JSTRSH(MXSHEL,MXAOVC,2),
     &                NPRIMS(MXSHEL,MXAOVC,2),
     &                NCONTS(MXSHEL,MXAOVC,2),
     &                IORBSH(MXSHEL,MXAOVC),
     &                IORBSB(0:MXCORB-1), NRCSH(MXSHEL), SEGMSH(MXSHEL),
     &                LCLASH(MXSHEL),NO2INT(MXSHEL), SPHRSH(MXCORB),
     &                NLRBL
