!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
#ifdef UNDEF
/* Comdeck last_change */
C  CHANGES INCLUDED STARTING MAY 9, 1987 JO
C  14-May-1987 hjaaj: NTEST as parameter
C  23-JUNE-87  jo: ADDITIONS DUE TO RAS MODIFICATIONS
C  27-Jun-1987 hjaaj: IDMTYP in DENSI1
C   1-Jul-1987 hjaaj: SNGLET in CISIG1
#endif
C  /* Deck rewino */
      SUBROUTINE REWINO( LU )
C
C REWIND SEQ FILE LU WITH FASTIO ROUTINES
C
C?    WRITE(6,*) ' TO REWIND FILE ',LU
C     CALL SQFILE(LU,10,IDUM,IDUM)
      REWIND LU
C?    WRITE(6,*) ' FILE REWOUND '
C
      RETURN
      END
