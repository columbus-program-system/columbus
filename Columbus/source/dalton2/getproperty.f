!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/

      subroutine getpropint(label,parr,pel,irrep,antsym,
     .    nsym,nbpsy,nel)
      implicit none
      integer i,nsym,nbpsy(8),pel,irrep,nel
      logical antsym
      real*8 parr(pel)
      character*8 tmptxt,label
      character*3 slab 
      
c
c     input label   property label (see her1pro, cmtyp)
c           parr    property array
c           pel     size of property array
c     output  irrep   symmetry of operator
c             antisym  anti symmetric matrix
c             nel     returned number of elements
c
C
      CHARACTER*8 RTNLBL(2)
      LOGICAL lsquare,OPEND, FNDLB2
      integer idummy,luprop
       
C
c      check for supported labels
       character*8 labsupported(10)
       logical square(10)
       data labsupported /'CM000000','CM010000','CM000100','CM000001',
     .    'CM010100','CM010001','CM000101','CM020000','CM000200', 
     .    'CM000002'/
       data square/ .false.,.false.,.false.,.false.,.false.,.false.,
     .   .false.,.false.,.false.,.false./
c
       do i=1,9
         if (labsupported(i)(1:8).eq.label(1:8)) then
             lsquare=square(i)
             goto 100
         endif 
       enddo
       call bummer('getpropint: label not supported',0,2)
 100   continue
        if (lsquare) then
           nel=0
           do i=1,nsym
            nel=nel+nbpsy(i)*nbpsy(i)
           enddo
        else
           nel=0
           do i=1,nsym
            nel=nel+nbpsy(i)*(nbpsy(i)+1)/2
           enddo
        endif
        if (pel.lt.nel) 
     .  call bummer('insufficient memory',pel,2)
c
      CALL GPOPEN(LUPROP,'AOPROPER',
     &            'OLD',' ','UNFORMATTED',IDUMMY,.FALSE.)
      IF ( FNDLB2(label,RTNLBL,LUPROP)) THEN
         IF (RTNLBL(2).EQ.'SYMMETRI') THEN
            ANTSYM = .FALSE.
         ELSE IF (RTNLBL(2).EQ.'ANTISYMM') THEN
            ANTSYM = .TRUE.
         ELSEif (rtnlbl(2).eq.'SQUARE  ') THEN
            lsquare=.true.
         else
         call bummer(' getpropint: no antisymmetry label on AOPROPER',
     .                0,2)
         END IF
         write(6,*) 'RTNLBL1=',RTNLBL(1)
         write(6,*) 'RTNLBL2=',RTNLBL(2)
         TMPTXT = RTNLBL(1)
         READ(TMPTXT(1:5),'(I1,1x,A3)') irrep,slab
         write(6,*) 'irrep,slab=',irrep,slab
         CALL READT(LUPROP,nel,parr)
      ELSE
         write(6,*) 'Property ',label,' not found'
         nel=-1
      END IF
      CALL GPCLOSE(LUPROP,'KEEP')
      RETURN
      END

