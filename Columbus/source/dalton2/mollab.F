!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      SUBROUTINE MOLLAB(A,LU,LUERR)
C
C  16-Jun-1986 hjaaj
C  (as SEARCH but CHARACTER*8 instead of REAL*8 labels)
C
C  Purpose:
C     Search for MOLECULE labels on file LU
C
      CHARACTER*8 A, B(4), C
#if defined (VAR_SPLITFILES)
#include "dummy.h"
      CHARACTER FNNAME*80
#endif
      DATA C/'********'/
      IRDERR = 0
#if defined (VAR_SPLITFILES)
      INQUIRE (UNIT=LU,NAME=FNNAME)
      LN = 1
 10   CONTINUE
      IF (FNNAME(LN:LN) .EQ. '-') THEN
         LN = LN - 1
         LUBKP = LU
         CALL GPCLOSE(LU,'KEEP')
         LU = LUBKP
         CALL GPOPEN(LU,FNNAME(1:LN),'OLD','SEQUENTIAL','UNFORMATTED',
     &               IDUMMY,.FALSE.)
      INQUIRE (UNIT=LU,NAME=FNNAME)
         REWIND (LU)
         GOTO 1
      ELSE IF (FNNAME(LN:LN) .EQ. ' ') THEN
         GOTO 1
      END IF
      LN = LN + 1
      GOTO 10
#endif
    1 READ (LU,END=3,ERR=6,IOSTAT=IOSVAL) B
      IRDERR = 0
      IF (B(1).NE.C) GO TO 1
      IF (B(4).NE.A) GO TO 1
      IF (LUERR.LT.0) LUERR = 0
      RETURN
C
    3 IF (LUERR.LT.0) THEN
#if defined (SYS_CRAY) || defined (VAR_MFDS) || defined (SYS_T3D) || defined (SYS_T90)
C 880916-hjaaj -- attempt to fix an IBM problem
C IBM shifts to new file after END= branch (e.g. FTxxF002 instead
C     of FTxxF001), backspace makes LU ready for append.
C 940510-hjaaj: same change for Cray's multifile datasets
         BACKSPACE LU
#endif
         LUERR = -1
         RETURN
      ELSE
         WRITE(LUERR,4)A,LU
         CALL QTRACE(LUERR)
         CALL QUIT('ERROR (MOLLAB) MOLECULE label not found on file')
      END IF
    4 FORMAT(/' *** ERROR (MOLLAB), MOLECULE label ',A8,
     *        ' not found on unit',I4)
C
    6 IRDERR = IRDERR + 1
      IF (IRDERR .LT. 10) GO TO 1
      IF (LUERR.LT.0) THEN
         LUERR = -2
         RETURN
      ELSE
         WRITE (LUERR,7) LU,A,IOSVAL
         CALL QTRACE(LUERR)
         CALL QUIT('ERROR (MOLLAB) error reading file')
      END IF
    7 FORMAT(/' *** ERROR (MOLLAB), error reading unit',I4,
     *       /T22,'when searching for label ',A8,
     *       /T22,'IOSTAT value :',I7)
      END
