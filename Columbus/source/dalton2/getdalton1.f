!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine getdalton1(label,unit,stvc,nstvc,
     .           symvec,noff,nnoff,nsym)
      implicit none
      character*8 label
      integer nstvc,unit,nsym
      real*8 stvc(nstvc),buf(600),xa
      integer i,icount,ibuf(600),ia,ib,newindx
      integer noff(nsym),nnoff(nsym),symvec(*)
      integer mxindx,indx,is,isym
      logical noindexing
      noindexing=.false.
      noindexing=noindexing.or.(label(1:8).eq.'OVERLAP ')
      noindexing=noindexing.or.(label(1:8).eq.'KINETINT')
      noindexing=noindexing.or.(label(1:8).eq.'ONEHAMIL')

      rewind(unit)
      mxindx=0
      call wzero(nstvc,stvc,1)
      call mollab(label,unit,0)
  78  read(unit) buf,ibuf,icount
       if (icount.gt.0 ) then
         do i=1,icount
           indx=ibuf(i)
           if (noindexing) then
               if (indx.gt.nstvc) 
     .            call bummer('indx.gt.nstvc, nstvc=',nstvc,2)
               stvc(indx)=buf(i)
c              mxindx=max(indx,mxindx) 
           else
           isym=nsym
           do is=1,nsym
             if ((indx-nnoff(is)).lt.0) then
               isym=is
             endif
           enddo
           indx=indx-nnoff(isym)
           xa=sqrt(2*indx+0.25d0)+0.5d0
           if (xa-int(xa).lt.1.d-8) then
              ia=int(xa)-1
           else
              ia =int(xa)
           endif
           ia=ia+noff(isym)
           ib= indx-ia*(ia-1)/2+noff(isym)
           if (ib.gt.ia) call bummer('invalid index',0,2)
           write(6,*) 'ia,ib=',ia,ib
           if (symvec(ia).ne.symvec(ib)) then
          write(6,*) 'i,ibuf(i),ia,ib,symvec(ia),symvec(ib)=',
     .          i,ibuf(i),ia,ib,symvec(ia),symvec(ib)
         call bummer('non-totally symmetric hamiltonian integrals',0,2)
          endif
           newindx=nnoff(symvec(ia))+
     .             (ia-noff(symvec(ia)))*(ia-noff(symvec(ia))-1)/2
     .              +ib-noff(symvec(ib))
           if (newindx.gt.nstvc) 
     .       call bummer('newindx.gt.nstvc, nstvc=',nstvc,2)
           stvc(newindx)=buf(i)
           endif
         enddo
       else
         goto 77
       endif
        goto 78
 77     continue
c       write(6,*) 'getdalton1: mxindx=',mxindx,'label=',label
       return
       end


