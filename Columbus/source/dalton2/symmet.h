      INTEGER MAXREP, MAXOPR, MULT, ISYMAX, ISYMAO, NPARSU,
     &        NAOS, NPARNU, IPTSYM, IPTCNT, NCRREP, IPTCOR,
     &        NAXREP, IPTAX, IPTXYZ, IPTNUC, ISOP, 
     &        NROTS, NINVC, NREFL, IXVAL, NCOS, ICLASS, ICNTAO
#if defined (SYS_CRAY)
      REAL FMULT, PT
#else
      DOUBLE PRECISION FMULT, PT
#endif
      COMMON /SYMMET/ FMULT(0:7), PT(0:7), MAXREP, MAXOPR, MULT(0:7),
     &                ISYMAX(3,2), ISYMAO(MXQN,MXAQN), NPARSU(8),
     &                NAOS(8), NPARNU(8,8), IPTSYM(MXCORB,0:7),
     &                IPTCNT(3*MXCENT,0:7,2), NCRREP(0:7,2),
     &                IPTCOR(3*MXCENT,2), NAXREP(0:7,2), IPTAX(3,2),
     &                IPTXYZ(3,0:7,2), IPTNUC(MXCENT,0:7),ISOP(0:7),
     &                NROTS,NINVC,NREFL,IXVAL(0:7,0:7),NCOS(8,2),
     &                ICLASS(MXCORB), ICNTAO(MXCORB)
