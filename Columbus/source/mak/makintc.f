!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      PROGRAM IINTC
cversion 5.0
c
c  version log:
c  09-nov-01 open(unit=13...) added. -rls
c
      PARAMETER (MNA=30)
      IMPLICIT REAL*8 (A-H,O-Z)
       LOGICAL READH,READH2,COMPABB
      REAL*8 ANG,X,Y,Z
      REAL*8 NAS(MNA)
      DIMENSION R(MNA)
      CHARACTER*70 TITLE
      CHARACTER*4 KEYW(5)
      character*2 type
      DATA KEYW/'B631','NOOF','NOFD','BLOW','RADI'/
c
      IR=7
      IW=8
      OPEN(UNIT=IR,FILE='geom',STATUS='UNKNOWN')
      OPEN(UNIT=IW,FILE='intcin',STATUS='UNKNOWN')
c     # unit=13 is referenced by the get*() subprograms. -rls
      open(unit=13,file='intcky',status='unknown')
      NAT=0
      WRITE(6,*)
c     WRITE(6,*)' what is the title of the input?'
c     READ(5,*) TITLE
      WRITE(IW,9)'TEXAS'
      WRITE(6,*)
c
      ANG=1.889726342D0
      READH=.false.
   30 READ(IR,2,END=10)TYPE,ATN,X,Y,Z,ATMAS
         NAT=NAT+1
         X=X/ANG
         Y=Y/ANG
         Z=Z/ANG
         WRITE(IW,4)TYPE,ATN,X,Y,Z
ctm
         call strupp(type)
         READH2=compabb('H',type,1)
         if (READH .and. (.not. READH2)) then
         write(*,*) 'Hydrogen atoms are required to be found at ',
     .             ' the end of the geom file '
         rewind(ir)
         call echoin (ir,6,ierr)
         call bummer('makintc: make new input with iargos',0,2)
         endif
         READH=READH2
ctm
         GO TO 30
   10 CONTINUE
c
  100 CONTINUE
      WRITE(6,*)
      WRITE(6,*)
      WRITE(6,*)' please enter one option at a time,'
      WRITE(6,*)' if there are more then one.'
      WRITE(6,*)' NOTE! if 6 is one of your options, be sure it is'
      WRITE(6,*)' the very last option put in.'
      write(6,*)
      WRITE(6,*)' 1. (default) offset forces produced by 4-21(*)'
      WRITE(6,*)' 2. offset forces produced by 6-31G*'
      WRITE(6,*)' 3. no offset forces at all will be punched'
      WRITE(6,*)' 4. no force constants will be punched'
      WRITE(6,*)' 5. blowing up all radii uniformly'
      WRITE(6,*)' 6. changing individual radii'
      WRITE(6,*)' 7. proceed'
      write(6,*)
      CALL GETINT('which option do you need?',NO,'7.')
      GO TO (21,22,23,24,25,26,27),NO
   21 CONTINUE
      GO TO 100
   22 CONTINUE
      WRITE(IW,12) KEYW(1)
      GO TO 100
   23 CONTINUE
      WRITE(IW,12) KEYW(2)
      GO TO 100
   24 CONTINUE
      WRITE(IW,12) KEYW(3)
      GO TO 100
   25 CONTINUE
      WRITE(6,*)' the multiple of the original radii you want to'
      CALL GETREAL('change:',blowfact,'1.25')
      WRITE(IW,13) KEYW(4),blowfact
      GO TO 100
   26 CONTINUE
      CALL GETINT('how many atoms will you chang?',NA,' ')
      WRITE(6,*)' now enter the serial numbers of those atoms:'
      DO 200 I=1,NA
      NAS(I)=0.
      R(I)=0.
      WRITE(6,*)' for atom',I
      CALL GETREAL('the serial number:',NAS(I),' ')
      WRITE(6,*)' the multiple of the original radii you want to'
      CALL GETREAL('change:',R(I),'1.25')
  200 CONTINUE
      WRITE(IW,14) KEYW(5),NA
      DO 40 I=1,NA
      WRITE(IW,15) NAS(I),R(I)
   40 CONTINUE
C
   27 CONTINUE
      WRITE(6,*)
      WRITE(6,*)' the input intcin has been written.'
      WRITE(6,*)
      REWIND IR
    1 FORMAT(A1)
    2 FORMAT(1X,A2,2X,F5.1,4F14.8)
    3 FORMAT(3F14.8)
    4 FORMAT(2X,A2,6X,4F10.5)
    5 FORMAT(20X)
    9 FORMAT(A5)
   11 FORMAT(A70)
   12 FORMAT(A4)
   13 FORMAT(A4,6X,F10.5)
   14 FORMAT(A4,6X,I9,'.')
   15 FORMAT(2F10.5)
      call bummer ('normal termination',0,3)

      STOP
      END
C
