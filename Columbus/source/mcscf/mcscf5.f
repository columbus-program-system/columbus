!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf5.f
cmcscf part=5 of 10.  hessian and gradient routines.
cversion=5.6 last modified: 16-jan-2001
c deck baaaa
      subroutine baaaa(d2,h2,b,x2,iorder,bufpq,nuvx,pqpt,prd,
     +  lenx2,off1,addaa)
c
c  hessian block construction for the aaaa block of the orbital-
c  orbital hessian matrix.
c
c  integrals of the type (pq|rs) are in h2(*).  two particle
c  density matrix elements are in d2(*).  x2(*) is a scratch array
c  used to store vectors of h2 and d2 elements for efficient dot
c  products.
c
c  this version performs the dot products one-at-a-time as required
c  for the hessian matrix, maximally exploiting the sparsness in
c  iorder(*) for typical wave functions.  for vector processors,
c  this may be modified to evaluate all dot products involving
c  the (rs) vector simultaneously, as a matrix-vector product, for
c  increased efficiency.
c
c  iorder(*)      maps from (pq) to a column of the hessian matrix.
c  bufpq(nbuf)    points from x2 buffer to (pq).
c  nuvx(nbuf)     number of elements copied for each buffer (or -1).
c  pqpt(na*na)    points from (p,q) to buffer number.
c  prd(2,na*na)   gives p, q for each (pq) for a particular symmetry.
c  x2(*)          scratch space for holding vectors of d2(*) and h2(*).
c
c  where nbuf is calculated based on lenx2 for each symmetry type.
c  nbuf should be at least 2 and may be as large as npsym*nqsym+1 for
c  psym.ne.qsym and as large as nndx(npsym+1)+1 for psym=qsym.
c
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 d2(*),h2(*),b(*),x2(lenx2)
      integer iorder(*),bufpq(*),pqpt(*),nuvx(*),prd(2,*)
      integer off1(*),addaa(*)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(10),nact)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer perm2(2,2)
      real*8 xpqrs,ddot_wr,xfctor,xfac(2,3)
      real*8 two,twom
      parameter(two=2d0,twom=-two)
      data perm2/2,1,1,2/
      data xfac/twom,twom,two,twom,two,two/
c
      nndxf(i)=(i*(i-1))/2
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  direct-type dot product contributions to the aaaa block of the
c  orbital-orbital hessian matrix.
c
      ncopy=0
      nsdot=0
      do 1000 pqsym=1,nsym
c
      pq=0
      nprd1=0
      do 6 psym=1,nsym
      qsym=mult(psym,pqsym)
      if(qsym.gt.psym)go to 6
      np=napsy(psym)
      if(np.eq.0)go to 6
      nq=napsy(qsym)
      if(nq.eq.0)go to 6
      npnq=np*nq
      if(psym.eq.qsym)npnq=nndx(np+1)
      nprd1=nprd1+npnq
      p1=ira1(psym)
      p2=ira2(psym)
      q1=ira1(qsym)
      do 4 p=p1,p2
      q2=ira2(qsym)
      if(psym.eq.qsym)q2=p
      do 2 q=q1,q2
      pq=pq+1
      prd(1,pq)=p
      prd(2,pq)=q
2     continue
4     continue
6     continue
      if(nprd1.le.1)go to 1000
      lenbuf=nprd1
      nbuf=min( lenx2/(2*lenbuf), nprd1)
      if(nbuf.lt.2)then
         write(nlist,9050)nbuf
         call bummer('baaaa: d nprd1=',nprd1,faterr)
      endif
      d2off=nbuf*lenbuf
      do 10 i=1,nbuf
      nuvx(i)=-1
      bufpq(i)=0
10    continue
      do 20 i=1,nprd1
      pqpt(i)=0
20    continue
      bnext=1
      npqblk=nbuf-1
c
c  loop over blocks of pq products
c
      do 400 pq1=1,nprd1,npqblk
c
c  loop over pq products in this pq block.
c
      pq2=min(pq1+npqblk-1,nprd1)
c
c  allow for possible match of pq with a previous rs.
c
      bnext=mod(bnext,nbuf)+1
c
      do 40 pq=pq1,pq2
      p=prd(1,pq)
      q=prd(2,pq)
      if(pqpt(pq).ne.0)go to 40
      bn=bufpq(bnext)
      if(bn.ne.0)pqpt(bn)=0
      pqpt(pq)=bnext
      bufpq(bnext)=pq
      nuvx(bnext)=-1
      bnext=mod(bnext,nbuf)+1
40    continue
c
c  loop over rs products backwards.  this allows for the maximal
c  coincidences of pq and rs and the minimal copies for pq.
c
      do 200 rs=nprd1,pq1,-1
      r=prd(1,rs)
      s=prd(2,rs)
      rsbuf=pqpt(rs)
      if(rsbuf.eq.0)then
         nuvx(bnext)=-1
         rsbuf=bnext
      endif
      rsbpt=(rsbuf-1)*lenbuf+1
c
c  form dot products of rs with the pq products in this block.
c
      do 100 pq=pq1,pq2
      if(pq.gt.rs)go to 110
      p=prd(1,pq)
      q=prd(2,pq)
      pr=nndx(r)+p
      ipr=iorder(pr)
      qr=nndx(r)+q
      iqr=iorder(qr)
      if(ipr.eq.0 .and. iqr.eq.0)go to 100
      qs=nndx(max(q,s))+min(q,s)
      iqs=iorder(qs)
      ps=nndx(max(p,s))+min(p,s)
      ips=iorder(ps)
c
c  four cases: both terms, bprqs term, bpsqr term, neither term.
c
      case=1
      if(r.eq.s .or. p.eq.q .or. ips.eq.0 .or. iqr.eq.0)case=2
      if(ipr.eq.0 .or. iqs.eq.0)case=case+2
c
c  case=4: dot product term not needed.
c
      if(case.eq.4)go to 100
c
c  check pq**
c
      pqbuf=pqpt(pq)
      pqbpt=(pqbuf-1)*lenbuf+1
      if(nuvx(pqbuf).lt.0)then
      call caaaa(p,q,pqsym,h2,d2,x2(pqbpt),x2(d2off+pqbpt),nuvx(pqbuf),
     +  off1,addaa,1)
      ncopy=ncopy+1
      endif
c
c  check rs**
c
      if(nuvx(rsbuf).lt.0)then
      br=bufpq(rsbuf)
      if(br.ne.0)pqpt(br)=0
      pqpt(rs)=rsbuf
      bufpq(rsbuf)=rs
      call caaaa(r,s,pqsym,h2,d2,x2(rsbpt),x2(d2off+rsbpt),nuvx(rsbuf),
     +  off1,addaa,1)
      ncopy=ncopy+1
      endif
c
c  calculate xpqrs=sum(uv) h2(pquv)*d2(rsuv)+h2(rsuv)*d2(pquv)
c
      nuv=nuvx(pqbuf)
      xpqrs=ddot_wr(nuv,x2(pqbpt),1,x2(d2off+rsbpt),1)
     +     +ddot_wr(nuv,x2(rsbpt),1,x2(d2off+pqbpt),1)
      nsdot=nsdot+1
c
c     xpqrs coefficients:
c     itype    indices  bprqs    bpsqr
c       1       4132     (-2)     (-2)
c       2       4231     (+2)     (-2)
c       3       4321     (+2)     (+2)
c
      itype=1
      if(s.gt.q)itype=2
      if(s.gt.p)itype=3
c
      go to (80,70,90),case
70    prqs=nndxf(ipr)+iqs
      b(prqs)=b(prqs)+xpqrs*xfac(1,itype)
      go to 100
80    prqs=nndxf(ipr)+iqs
      b(prqs)=b(prqs)+xpqrs*xfac(1,itype)
90    psqr=nndxf(max(iqr,ips))+min(iqr,ips)
      b(psqr)=b(psqr)+xpqrs*xfac(2,itype)
c
100   continue
110   continue
c
200   continue
400   continue
c
1000  continue
c
c  exchange-type contributions to the hessian matrix.
c
      ncopy=0
      nsdot=0
      do 2000 pqsym=1,nsym
c
      pq=0
      nprd1=0
      nprd2=0
      do 1006 psym=1,nsym
      qsym=mult(psym,pqsym)
      np=napsy(psym)
      if(np.eq.0)go to 1006
      nq=napsy(qsym)
      if(nq.eq.0)go to 1006
      npnq=np*nq
      if(qsym.gt.psym)go to 1005
      if(psym.eq.qsym)npnq=nndx(np+1)
      nprd1=nprd1+npnq
      p1=ira1(psym)
      p2=ira2(psym)
      q1=ira1(qsym)
      do 1004 p=p1,p2
      q2=ira2(qsym)
      if(psym.eq.qsym)q2=p
      do 1002 q=q1,q2
      pq=pq+1
      prd(1,pq)=p
      prd(2,pq)=q
1002  continue
1004  continue
1005  nprd2=nprd2+np*nq
1006  continue
      if(nprd1.le.1)go to 2000
      lenbuf=nprd2
      nbuf=min( lenx2/(2*lenbuf),nprd1+1)
      if(nbuf.lt.2)then
         write(nlist,9050)nbuf
         call bummer('baaaa: x nprd1=',nprd1,faterr)
      endif
      d2off=nbuf*lenbuf
      do 1010 i=1,nbuf
      nuvx(i)=-1
      bufpq(i)=0
1010  continue
      n2act=nact*nact
      do 1020 i=1,n2act
      pqpt(i)=0
1020  continue
      bnext=1
      npqblk=nbuf-1
c
c  loop over blocks of pq products
c
      do 1400 pq1=1,nprd1,npqblk
c
c  loop over pq products in this pq block.
c
      pq2=min(pq1+npqblk-1,nprd1)
c
c  allow for possible match of pq with a previous rs.
c
      bnext=mod(bnext,nbuf)+1
c
      do 1040 pqx=pq1,pq2
      p=prd(1,pqx)
      q=prd(2,pqx)
      pq=(q-1)*nact+p
      if(pqpt(pq).ne.0)go to 1040
      bn=bufpq(bnext)
      if(bn.ne.0)pqpt(bn)=0
      pqpt(pq)=bnext
      bufpq(bnext)=pq
      nuvx(bnext)=-1
      bnext=mod(bnext,nbuf)+1
1040  continue
c
c  loop over rs products backwards to increase the chance
c  of coincidences.
c  two types of rs products for exchange contributions:
c  it=1  r.lt.s  (not used for pq).
c  it=2  r.ge.s  (may coincide with pq of the next block).
c
      do 1300 it=1,2
c
      do 1200 rsx=nprd1,pq1,-1
      r=prd(perm2(1,it),rsx)
      s=prd(perm2(2,it),rsx)
      if(it.eq.1 .and. r.eq.s)go to 1200
      rsmn=min(r,s)
      rs=(s-1)*nact+r
      rsbuf=pqpt(rs)
      if(rsbuf.eq.0)then
         nuvx(bnext)=-1
         rsbuf=bnext
      endif
      rsbpt=(rsbuf-1)*lenbuf+1
c
c  form dot products of rs with the pq products in this block.
c
      do 1100 pqx=pq1,pq2
      if(pqx.gt.rsx)go to 1110
      p=prd(1,pqx)
      q=prd(2,pqx)
      if(it.eq.1 .and. p.eq.q)go to 1100
      pq=(q-1)*nact+p
c
c  six possible index combinations:
c
c    case   pqrs  element    x-coefficient
c     1     3241   brpqs       (-4)
c     2     3142   brpsq       (+4)
c     3     2143   brpsq       (+4)
c     4     3214   bsqpr       (-4)
c     5     3124   bsqpr       (-4)
c     6     2134   bsqrp       (+4)
c
      case=1
      if(rsmn.gt.q)case=2
      if(rsmn.gt.p)case=3
      if(s.gt.r)case=case+3
      go to (1051,1052,1052,1054,1054,1056),case
1051  continue
      rp=nndx(r)+p
      irp=iorder(rp)
      if(irp.eq.0)go to 1100
      qs=nndx(q)+s
      iqs=iorder(qs)
      if(iqs.eq.0)go to 1100
      add=nndxf(irp)+iqs
      xfctor=-4d0
      go to 1058
1052  continue
      rp=nndx(r)+p
      irp=iorder(rp)
      if(irp.eq.0)go to 1100
      sq=nndx(s)+q
      isq=iorder(sq)
      if(isq.eq.0)go to 1100
      add=nndxf(irp)+isq
      xfctor=+4d0
      go to 1058
1054  continue
      sq=nndx(s)+q
      isq=iorder(sq)
      if(isq.eq.0)go to 1100
      pr=nndx(p)+r
      ipr=iorder(pr)
      if(ipr.eq.0)go to 1100
      add=nndxf(max(isq,ipr))+min(isq,ipr)
      xfctor=-4d0
      go to 1058
1056  continue
      sq=nndx(s)+q
      isq=iorder(sq)
      if(isq.eq.0)go to 1100
      rp=nndx(r)+p
      irp=iorder(rp)
      if(irp.eq.0)go to 1100
      add=nndxf(isq)+irp
      xfctor=+4d0
1058  continue
c
c  check pq**
c
      pqbuf=pqpt(pq)
      pqbpt=(pqbuf-1)*lenbuf+1
      if(nuvx(pqbuf).lt.0)then
         call caaaa(p,q,pqsym,h2,d2,x2(pqbpt),x2(d2off+pqbpt),
     &    nuvx(pqbuf),off1,addaa,2)
         ncopy=ncopy+1
      endif
c
c  check rs**
c
      if(nuvx(rsbuf).lt.0)then
         br=bufpq(rsbuf)
         if(br.ne.0)pqpt(br)=0
         pqpt(rs)=rsbuf
         bufpq(rsbuf)=rs
         call caaaa(r,s,pqsym,h2,d2,x2(rsbpt),x2(d2off+rsbpt),
     &    nuvx(rsbuf),off1,addaa,2)
         ncopy=ncopy+1
      endif
c
c  calculate xpqrs=sum(uv) h2(puqv)*d2(rusv)+h2(rusv)*d2(puqv)
c
      nuv=nuvx(pqbuf)
      xpqrs=ddot_wr(nuv,x2(pqbpt),1,x2(d2off+rsbpt),1)
     +     +ddot_wr(nuv,x2(rsbpt),1,x2(d2off+pqbpt),1)
      nsdot=nsdot+1
c
      b(add)=b(add)+xfctor*xpqrs
c
1100  continue
1110  continue
c
1200  continue
1300  continue
1400  continue
2000  continue
c
      if(flags(3))call timer('baaaa',4,event,nlist)
      return
9050  format(' baaaa: nbuf =',i2)
      end
      subroutine baaaaf(b,u,d,f,iorder)
c
c  include the 1-e hamiltonian and fock matrix terms in the
c  aaaa block of the orbital-orbital hessian matrix.
c
c  baaaa(pqrs)=two*(upr*dqs+uqs*dpr-ups*dqr-uqr*dps)
c              +(fsp+fps)*delta(qr) + (frq+fqr)*delta(ps)
c              -(frp+fpr)*delta(qs) - (fqs+fsq)*delta(pr)
c
c  input:
c  b(*)      = aaaa hessian block.
c  u(*)      = aa block of the effective 1-e hamiltonain matrix.
c  d(*)      = 1-particle density matrix.
c  f(*)      = aa block of the fock matrix.
c  iorder(*) = active-active rotation ordering vector.
c
c  output:
c  b(*)      = updated hessian block.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      real*8 b(*),u(*),d(*),f(*)
      integer iorder(*)
      real*8 two
      parameter(two=2d0)
c
      nndxf(i)=(i*(i-1))/2
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      do 4000 isym=1,nsym
      na=napsy(isym)
      if(na.lt.2)go to 4000
c
c  do-loop parameters.
c  p.gt.q, r.gt.s, (pq).ge.(rs)
c
      x0=nsa(isym)
      nnoff=nnsa(isym)
      n2off=n2sa(isym)
c
c  include 1-e hamiltonian terms:
c
      do 400 p=2,na
      px0=nndx(x0+p)+x0
      do 300 q=1,p-1
      ipq=iorder(px0+q)
      if(ipq.eq.0)go to 300
      do 200 r=2,p
      pr=nnoff+nndx(p)+r
      qr=nnoff+nndx(max(q,r))+min(q,r)
      rx0=nndx(x0+r)+x0
      s2=r-1
      if(r.eq.p)s2=q
      do 100 s=1,s2
      irs=iorder(rx0+s)
      if(irs.eq.0)go to 100
      qs=nnoff+nndx(max(q,s))+min(q,s)
      ps=nnoff+nndx(p)+s
      pqrs=nndxf(ipq)+irs
      b(pqrs)=b(pqrs)+
     +  two*( u(pr)*d(qs) + u(qs)*d(pr) - u(ps)*d(qr) - u(qr)*d(ps) )
100   continue
200   continue
300   continue
400   continue
c
c  include (fsp+fps)*delta(qr) terms:
c
      if(na.lt.3)go to 2000
      do 1300 p=3,na
      px0=nndx(x0+p)+x0
      do 1200 q=2,p-1
      ipq=iorder(px0+q)
      if(ipq.eq.0)go to 1200
      qx0=nndx(x0+q)+x0
      sp=n2off+(p-1)*na
      ps=n2off+(-na)+p
      do 1100 s=1,q-1
      sp=sp+1
      ps=ps+na
      iqs=iorder(qx0+s)
      if(iqs.eq.0)go to 1100
      pqrs=nndxf(ipq)+iqs
      b(pqrs)=b(pqrs)+f(sp)+f(ps)
1100  continue
1200  continue
1300  continue
c
c  include -(frp+fpr)*delta(qs) terms:
c
2000  continue
      do 2300 p=2,na
      px0=nndx(x0+p)+x0
      do 2200 q=1,p-1
      ipq=iorder(px0+q)
      if(ipq.eq.0)go to 2200
      rp=n2off+(p-1)*na+q
      pr=n2off+(q-1)*na+p
      do 2100 r=q+1,p
      rp=rp+1
      pr=pr+na
      irs=iorder(nndx(x0+r)+x0+q)
      if(irs.eq.0)go to 2100
      pqrs=nndxf(ipq)+irs
      b(pqrs)=b(pqrs)-(f(rp)+f(pr))
2100  continue
2200  continue
2300  continue
c
c  include -(fqs+fsq)*delta(pr) terms:
c
      do 3300 p=2,na
      px0=nndx(x0+p)+x0
      do 3200 q=1,p-1
      ipq=iorder(px0+q)
      if(ipq.eq.0)go to 3200
      sq=n2off+(q-1)*na
      qs=n2off+(-na)+q
      do 3100 s=1,q
      sq=sq+1
      qs=qs+na
      irs=iorder(px0+s)
      if(irs.eq.0)go to 3100
      pqrs=nndxf(ipq)+irs
      b(pqrs)=b(pqrs)-(f(sq)+f(qs))
3100  continue
3200  continue
3300  continue
c
4000  continue
c
      if(flags(3))call timer('baaaaf',4,event,nlist)
      return
      end
      subroutine baaad(d1,d2,h2,b,x,
     +  iorder,off1,off2,addaa,addad)
c
c  2-e integral contributions to the aaad block of the
c  orbital-orbital hessian matrix.
c
c  b(ir,pq) = sum   2*(iq|uv)*d2(pruv) -2*(ip|uv)*d2(qruv)
c            (uv)
c                 +4*(iv|qu)*d2(rvpu) -4*(iv|pu)*d2(rvqu)
c
c           +sum  -2*x(irqt)*d1(pt) +2*x(irpt)*d1(qt)
c            (t)
c
c  where x(irqt) = 4*(ir|qt)-(iq|rt)-(it|qr) is calculated
c  from the sorted integral list.
c  outer-product algorithms are used to exploit the zeros
c  in d1(*) and d2(*).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
      integer tsymad(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 d1(*),d2(*),h2(*),b(nadt,*),x(*)
      integer iorder(*)
      integer off1(*),off2(*),addaa(*),addad(ndimd,ndima)
c
      real*8 zero,two,four,factor,term,sterm
      parameter(zero=0d0,two=2d0,four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  two-particle density matrix terms.
c
      uv=0
      do 4100 u=1,nact
      usym=symx(ix0a+u)
      do 4000 v=1,u
      vsym=symx(ix0a+v)
      uvsym=mult(vsym,usym)
      uv=uv+1
      if(tsymad(uvsym).eq.0)go to 4000
c
c  b(ir,pq)=sum(uv) 2*(uv|qi)*d(uvpr) -2*(uv|pi)*d(uvqr)  terms
c
      factor=four
      if(u.eq.v)factor=two
      do 500 psym=1,nsym
      if(napsy(psym).lt.2)go to 500
      rsym=mult(psym,uvsym)
      if(napsy(rsym).eq.0)go to 500
c  isym=rsym
      if(ndpsy(rsym).eq.0)go to 500
c
      p1=ira1(psym)
      p2=ira2(psym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      i1=ird1(rsym)
      ni=ndpsy(rsym)
c
      do 400 p=p1,p2
c
      do 300 r=r1,r2
      pr=nndx(max(p,r))+min(p,r)
      uvpr=off1(max(pr,uv))+addaa(min(pr,uv))
      term=factor*d2(uvpr)
      if(term.eq.0d0)go to 300
      i1r=addad(i1,r)
c
      do 200 q=p1,p2
      if(p.eq.q)go to 200
      if(p.lt.q)then
          sterm=-term
          pq=nndx(q)+p
      else
          sterm=term
          pq=nndx(p)+q
      endif
      ipq=iorder(pq)
      if(ipq.eq.0)go to 200
      iquv=off2(uv)+addad(i1,q)
c
c  saxpy(ni,sterm,h2(iquv),1,b(i1r,ipq),1)
c
      do 100 i=1,ni
100   b(i1r-1+i,ipq)=b(i1r-1+i,ipq)+sterm*h2(iquv-1+i)
c
200   continue
300   continue
400   continue
500   continue
c
c  b(ir,pu)=sum(vq)  4*(iq|uv)*d(pvrq)      p.gt.u
c  b(ir,up)=sum(vq) -4*(iq|uv)*d(pvrq)      u.gt.p
c
      if(napsy(usym).lt.2)go to 2000
      do 1500 rsym=1,nsym
      if(napsy(rsym).eq.0)go to 1500
      if(ndpsy(rsym).eq.0)go to 1500
      qsym=mult(rsym,uvsym)
      if(napsy(qsym).eq.0)go to 1500
c
      p1=ira1(usym)
      p2=ira2(usym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      q1=ira1(qsym)
      q2=ira2(qsym)
      i1=ird1(rsym)
      ni=ndpsy(rsym)
c
      do 1400 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
      ipu=iorder(pu)
      if(ipu.eq.0)go to 1400
      pv=nndx(max(p,v))+min(p,v)
c
      do 1300 r=r1,r2
      i1r=addad(i1,r)
c
      do 1200 q=q1,q2
      rq=nndx(max(r,q))+min(r,q)
      pvrq=off1(max(pv,rq))+addaa(min(pv,rq))
      term=four*d2(pvrq)
      if(term.eq.0d0)go to 1200
      if(p.lt.u)term=-term
c
      iquv=off2(uv)+addad(i1,q)
c
c  saxpy(ni,term,h2(iquv),1,b(i1r,ipu),1)
c
      do 1100 i=1,ni
1100  b(i1r-1+i,ipu)=b(i1r-1+i,ipu)+term*h2(iquv-1+i)
c
1200  continue
1300  continue
1400  continue
1500  continue
c
2000  continue
c
      if(u.eq.v)go to 3000
c
c  b(ir,pv)=sum(uq)  4*(iq|uv)*d(purq)  p.gt.v
c  b(ir,vp)=sum(uq) -4*(iq|uv)*d(purq)  v.gt.p
c
      if(napsy(vsym).lt.2)go to 3000
      do 2500 rsym=1,nsym
      if(napsy(rsym).eq.0)go to 2500
      if(ndpsy(rsym).eq.0)go to 2500
      qsym=mult(rsym,uvsym)
      if(napsy(qsym).eq.0)go to 2500
c
      p1=ira1(vsym)
      p2=ira2(vsym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      q1=ira1(qsym)
      q2=ira2(qsym)
      i1=ird1(rsym)
      ni=ndpsy(rsym)
c
      do 2400 p=p1,p2
      pv=nndx(max(p,v))+min(p,v)
      ipv=iorder(pv)
      if(ipv.eq.0)go to 2400
      pu=nndx(max(p,u))+min(p,u)
c
      do 2300 r=r1,r2
      i1r=addad(i1,r)
c
      do 2200 q=q1,q2
      rq=nndx(max(r,q))+min(r,q)
      purq=off1(max(pu,rq))+addaa(min(pu,rq))
      term=four*d2(purq)
      if(term.eq.0d0)go to 2200
      if(p.lt.v)term=-term
c
      iquv=off2(uv)+addad(i1,q)
c
c  saxpy(ni,term,h2(iquv),1,b(i1r,ipv),1)
c
      do 2100 i=1,ni
2100  b(i1r-1+i,ipv)=b(i1r-1+i,ipv)+term*h2(iquv-1+i)
c
2200  continue
2300  continue
2400  continue
2500  continue
c
3000  continue
c
4000  continue
4100  continue
c
c  one-particle density matrix terms.
c  use index symmetry in x(*) to avoid redundant x(*) computation.
c  for u.ge.v, the x(*) contributions are computed as:
c  b(ir,wu)=sum(v)  -2*x(iruv)*d(wv)  ;for w>u
c  b(ir,uw)=sum(v)   2*x(iruv)*d(wv)  ;for w<u
c  b(ir,wv)=sum(u)  -2*x(iruv)*d(wu)  ;for w>v
c  b(ir,vw)=sum(u)   2*x(iruv)*d(wu)  ;for w<v
c
      do 6000 usym=1,nsym
      if(napsy(usym).eq.0)go to 6000
      u0=nsa(usym)
      u1=ira1(usym)
      u2=ira2(usym)
c
      do 5900 u=u1,u2
      do 5800 v=u1,u
      uv=nndx(u)+v
c
c  form x(**uv).
c
      do 5300 rsym=1,nsym
      if(napsy(rsym).eq.0)go to 5300
      if(ndpsy(rsym).eq.0)go to 5300
      r1=ira1(rsym)
      r2=ira2(rsym)
      i1=ird1(rsym)
      ni=ndpsy(rsym)
c
      do 5200 r=r1,r2
      ru=nndx(max(r,u))+min(r,u)
      rv=nndx(max(r,v))+min(r,v)
      ir=addad(i1,r)
      iruv=off2(uv)+addad(i1,r)
      iurv=off2(rv)+addad(i1,u)
      ivru=off2(ru)+addad(i1,v)
c
      if(u.ne.v)then
      do 5100 i=1,ni
5100  x(ir-1+i)=four*h2(iruv-1+i)-h2(iurv-1+i)-h2(ivru-1+i)
      else
      do 5120 i=1,ni
5120  x(ir-1+i)=four*h2(iruv-1+i)-two*h2(iurv-1+i)
      endif
c
5200  continue
5300  continue
c
c  include x(*) contributions.
c
      do 5400 w=u1,u-1
      iuw=iorder(nndx(u)+w)
      if(iuw.eq.0)go to 5400
      term=two*d1(nnsa(usym)+nndx(max(w-u0,v-u0))+min(w-u0,v-u0))
      if(term.ne.zero)call daxpy_wr(nadt,term,x,1,b(1,iuw),1)
5400  continue
c
      do 5500 w=u+1,u2
      iwu=iorder(nndx(w)+u)
      if(iwu.eq.0)go to 5500
      term=-two*d1(nnsa(usym)+nndx(w-u0)+(v-u0))
      if(term.ne.zero)call daxpy_wr(nadt,term,x,1,b(1,iwu),1)
5500  continue
c
      if(u.eq.v)go to 5800
c
      do 5600 w=u1,v-1
      ivw=iorder(nndx(v)+w)
      if(ivw.eq.0)go to 5600
      term=two*d1(nnsa(usym)+nndx(u-u0)+(w-u0))
      if(term.ne.zero)call daxpy_wr(nadt,term,x,1,b(1,ivw),1)
5600  continue
c
      do 5700 w=v+1,u2
      iwv=iorder(nndx(w)+v)
      if(iwv.eq.0)go to 5700
      term=-two*d1(nnsa(usym)+nndx(max(w-u0,u-u0))+min(w-u0,u-u0))
      if(term.ne.zero)call daxpy_wr(nadt,term,x,1,b(1,iwv),1)
5700  continue
c
5800  continue
5900  continue
6000  continue
c
      if(flags(3))call timer('baaad',4,event,nlist)
      return
      end
      subroutine baaadf(b,uad,fad,qad,d1,iorder,addad)
c
c  fad(*), qad(*), and uad(*) contributions to the aaad block of the
c  orbital-orbital hessian.
c
c  b(ir,pq) = 2*(uad(qi)*d(pr)-uad(pi)*d1(qr))
c            +delta(qr)*(fad(ip)+qad(ip))
c            -delta(pr)*(fad(iq)+qad(iq))
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer tsymad(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(nadt,*),uad(*),fad(*),qad(*),d1(*)
      integer iorder(*),addad(ndimd,ndima)
c
      real*8 zero,two,term
      parameter(zero=0d0,two=2d0)
c
      if(flags(3))call timer('baaadf',1,event,nlist)
c
      do 500 isym=1,nsym
      if(napsy(isym).eq.0)go to 500
      if(ndpsy(isym).eq.0)go to 500
      p0=nsa(isym)
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      ni=ndpsy(isym)
c
      do 400 p=p1,p2
      i1p=addad(i1,p)
c
      do 300 q=p1,p
      ipq=iorder(nndx(p)+q)
      if(ipq.eq.0)go to 300
      i1q=addad(i1,q)
c
      do 200 r=p1,p2
      i1r=addad(i1,r)
c
c  2*uiq*dpr term.
c
      term=two*d1(nnsa(isym)+nndx(max(p-p0,r-p0))+min(p-p0,r-p0))
      if(term.ne.zero)call daxpy_wr(ni,term,uad(i1q),1,b(i1r,ipq),1)
c
c  -2*uip*dqr term.
c
      term=-two*d1(nnsa(isym)+nndx(max(q-p0,r-p0))+min(q-p0,r-p0))
      if(term.ne.zero)call daxpy_wr(ni,term,uad(i1p),1,b(i1r,ipq),1)
c
c  delta(qr)*(fip+qip) term.
c
      if(q.eq.r)then
      do 10 i=1,ni
10    b(i1r-1+i,ipq)=b(i1r-1+i,ipq)+(fad(i1p-1+i)+qad(i1p-1+i))
      endif
c
c  -delta(pr)*(fiq+qiq) term.
c
      if(p.eq.r)then
      do 20 i=1,ni
20    b(i1r-1+i,ipq)=b(i1r-1+i,ipq)-(fad(i1q-1+i)+qad(i1q-1+i))
      endif
c
200   continue
300   continue
400   continue
500   continue
c
      if(flags(3))call timer('baaadf',4,event,nlist)
      return
      end
      subroutine badad(b,h2,pmat,pt,fdd,
     +  udd,bufh2,d1,d2,off1,
     +  addaa,off13)
c
c  two-electron integral contributions to the adad block of
c  the orbital-orbital hessian and fdd(*) matrix construction.
c
c    fdd(ij) = 2*udd(ij) + sum(uv) (2*(ij|uv) - (iu|jv))*d1(uv)
c
c    badad(ip,jr)=b(j,i,pr)= -2*sum(t) p(j,i,pt)*(d1(rt)-delta(r,t))
c                            -2*sum(t) p(j,i,tr)*(d1(pt)-delta(p,t))
c                            +2*sum(uv) (ij|uv)*d(pr,uv)
c                            +4*sum(uv) (iu|jv)*d(pu,rv)
c
c  where p(j,i,pq) = p(pi,qj) = 4(pi|qj)-(pq|ij)-(pj|qi) is constructed
c  from the sorted integral list.
c
c  ni*nj saxpy version 30-nov-84 (ron shepard).
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer tsymdd(8),tsmdd2(8)
      equivalence (nxy(1,21),tsymdd(1))
      equivalence (nxy(1,22),tsmdd2(1))
      equivalence (nxtot(9),n2td)
      equivalence (nxtot(10),nact)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      common/cbufs/stape,lenbfs,h2bpt
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(*),h2(*),pmat(*),pt(*),fdd(*)
      real*8 bufh2(lenbfs),udd(*),d1(*),d2(*)
      integer off1(*),addaa(*),off13(*)
c
      integer sdpt(8),sxpt(8),sxtpt(8)
      real*8 zero,two,four,term
      parameter(zero=0d0,two=2d0,four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  although fdd(*) is symmetric, it is easier to use square-packed than
c  triangular packed.  include udd(*) terms and scale.
c
      call dcopy_wr(n2td,udd,1,fdd,1)
      call dscal_wr(n2td,two,fdd,1)
c
c  both direct and exchange type integrals are interleaved and sorted
c  together.  for a given (uv), direct integrals, (uv|ij), are lower
c  triangular packed, followed by exchange integrals, (ui|vj), which
c  are rectangular packed.
c
      uv1=0
      uv=0
      do 800 u=1,nact
      usym=symx(ix0a+u)
      uoff=nsa(usym)
      u1=ira1(usym)
      u2=ira2(usym)
      ux=u-uoff
      uuoff=nnsa(usym)
      ni=ndpsy(usym)
c
      do 700 v=1,u
      uv=uv+1
      vsym=symx(ix0a+v)
      if(usym.eq.vsym)uv1=uv1+1
      uvsym=mult(vsym,usym)
      voff=nsa(vsym)
      v1=ira1(vsym)
      v2=ira2(vsym)
      vx=v-voff
      vvoff=nnsa(vsym)
      ninj=ni*ndpsy(vsym)
c
c  get direct integrals.
c  usym.eq.vsym blocks are expanded to square symmetry-packed.
c  for isym.gt.jsym, sdpt(isym) points to the (j1,i1) element.
c
c  if there are no direct integrals for the u,v symmetry combination,
c  (which means there are no exchange integrals also) then skip to the
c  next v and try again.
c
      nterm=tsymdd(uvsym)
      if(nterm.eq.0)go to 700
      if(h2bpt+nterm-1 .gt. lenbfs)then
         read(stape)bufh2
         h2bpt=1
      endif
      ioff=0
      call cxxaa(bufh2(h2bpt),h2,sdpt,ndpsy,nterm,uvsym,ioff)
      h2bpt=h2bpt+nterm
c
c  get exchange integrals x(j,i,uv)=(ui|vj)
c  for usym.eq.vsym, symmetry blocks are returned in order and
c  x(transpose) follows x.
c  for isym.gt.jsym, sxpt(isym) points to (j1,i1) and
c  sxtpt(jsym) points to (i1,j1).
c
c  for isym.ne.jsym cases, the smallest symmetry index corresponds to
c  the most rapidly varying orbital index.
c
      nterm=tsmdd2(uvsym)
      if(h2bpt+nterm-1 .gt. lenbfs)then
         read(stape)bufh2
         h2bpt=1
      endif
      call cxaxa(bufh2(h2bpt),h2,sxpt,sxtpt,ndpsy,
     +  n2td,nterm,uvsym,ioff)
      h2bpt=h2bpt+nterm
c
c
c  form p(j,i) for the isym=usym and jsym=vsym block of integrals.
c
c  p(j,i) = 4*x(j,i) -h(j,i) -x(i,j)
c
      ijx=sxpt(usym)
      ijxt=sxtpt(vsym)
      ijd=sdpt(usym)
      do 100 ij=1,ninj
100   pmat(ij)=four*h2(ijx-1+ij) -h2(ijd-1+ij) -h2(ijxt-1+ij)
c
      if(usym.ne.vsym)go to 600
c
c*****************************************************************
c  usym.eq.vsym blocks of integrals.
c*****************************************************************
c
c  include integral contributions to fdd(*).
c
c  fdd(ij) = 2*udd(ij) + sum(uv) (2*(ij|uv) - (iu|jv))*d1(uv)
c          = 2*udd(ij) + sum(u.gt.v) (4*(ij|uv)-(iu|jv)-(iv|ju))*d1(uv)
c                      + sum(u) ( 2*(ij|uu) - (iu|ju) )*d1(uu)
c
      if(d1(uv1).ne.zero)then
         if(u.eq.v)then
c  u.eq.v terms...
            call daxpy_wr(n2td,two*d1(uv1),h2(sdpt(1)),1,fdd,1)
            call daxpy_wr(n2td,-d1(uv1),h2(sxpt(1)),1,fdd,1)
         else
c  u.ne.v terms...
            call daxpy_wr(n2td,four*d1(uv1),h2(sdpt(1)),1,fdd,1)
            call daxpy_wr(n2td,-d1(uv1),h2(sxpt(1)),1,fdd,1)
            call daxpy_wr(n2td,-d1(uv1),h2(sxtpt(1)),1,fdd,1)
         endif
      endif
c
      if(ninj.eq.0)go to 400
c
c  ninj=0 means there are no p(*) contributions for this symmetry
c  block of integrals.  otherwise, form pt(j,i) = p(i,j) and include
c  the p(*) contributions.
c
      call mtrans(pmat,1,pt,1,ni,ni)
c
      do 210 t=u1,v
      tx=t-uoff
      vt=off13(nndx(v)+t)+1
      ut=uuoff+nndx(ux)+tx
      term=-two*d1(ut)
      if(u.eq.t)term=term+two
      if(term.ne.zero)call daxpy_wr(ninj,term,pt,1,b(vt),1)
210   continue
c
      do 220 t=v,u2
      tx=t-uoff
      tv=off13(nndx(t)+v)+1
      tu=uuoff+nndx(max(tx,ux))+min(tx,ux)
      term=-two*d1(tu)
      if(u.eq.t)term=term+two
      if(term.ne.zero)call daxpy_wr(ninj,term,pmat,1,b(tv),1)
220   continue
c
      if(u.eq.v)go to 500
c
c  u.ne.v term...
c
      do 310 t=u1,u
      tx=t-uoff
      ut=off13(nndx(u)+t)+1
      tv=uuoff+nndx(max(tx,vx))+min(tx,vx)
      term=-two*d1(tv)
      if(t.eq.v)term=term+two
      if(term.ne.zero)call daxpy_wr(ninj,term,pmat,1,b(ut),1)
310   continue
c
c  u.ne.v term...
c
      do 320 t=u,u2
      tx=t-uoff
      tu=off13(nndx(t)+u)+1
      tv=uuoff+nndx(tx)+vx
      term=-two*d1(tv)
      if(t.eq.v)term=term+two
      if(term.ne.zero)call daxpy_wr(ninj,term,pt,1,b(tu),1)
320   continue
c
400   if(u.eq.v)go to 500
c
c  usym.eq.vsym, u.ne.v two-particle density matrix terms.
c
      do 430 psym=1,nsym
      if(napsy(psym).eq.0)go to 430
      if(ndpsy(psym).eq.0)go to 430
      p1=ira1(psym)
      p2=ira2(psym)
      ninjp=ndpsy(psym)**2
c
      do 420 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
      pv=nndx(max(p,v))+min(p,v)
c
      do 410 r=p1,p
      ru=nndx(max(r,u))+min(r,u)
      rv=nndx(max(r,v))+min(r,v)
      pr=nndx(p)+r
      prij=off13(pr)+1
c  direct term...
      ijd=sdpt(psym)
      pruv=off1(max(pr,uv))+addaa(min(pr,uv))
      term=four*d2(pruv)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijd),1,b(prij),1)
c  first exchange term...
      ijx=sxpt(psym)
      purv=off1(max(pu,rv))+addaa(min(pu,rv))
      term=four*d2(purv)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijx),1,b(prij),1)
c  second exchange term...
      ijxt=sxtpt(psym)
      pvru=off1(max(pv,ru))+addaa(min(pv,ru))
      term=four*d2(pvru)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijxt),1,b(prij),1)
410   continue
420   continue
430   continue
c
      go to 700
c
500   continue
c
c  u.eq.v two-particle density matrix terms.
c
      do 530 psym=1,nsym
      if(napsy(psym).eq.0)go to 530
      if(ndpsy(psym).eq.0)go to 530
      p1=ira1(psym)
      p2=ira2(psym)
      ninjp=ndpsy(psym)**2
c
      do 520 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
c
      do 510 r=p1,p
      ru=nndx(max(r,u))+min(r,u)
      pr=nndx(p)+r
      prij=off13(pr)+1
c  direct term...
      ijd=sdpt(psym)
      pruv=off1(max(pr,uv))+addaa(min(pr,uv))
      term=two*d2(pruv)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijd),1,b(prij),1)
c  exchange term...
      ijx=sxpt(psym)
      puru=off1(pu)+addaa(ru)
      term=four*d2(puru)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijx),1,b(prij),1)
c
510   continue
520   continue
530   continue
      go to 700
c
600   continue
c
c*****************************************************************
c  usym.ne.vsym block of integrals.
c*****************************************************************
c
      do 610 t=v1,v2
      tx=t-voff
      ut=off13(nndx(u)+t)+1
      tv=vvoff+nndx(max(tx,vx))+min(tx,vx)
      term=-two*d1(tv)
      if(t.eq.v)term=term+two
      if(term.ne.zero)call daxpy_wr(ninj,term,pmat,1,b(ut),1)
610   continue
c
      do 620 t=u1,u2
      tx=t-uoff
      tv=off13(nndx(t)+v)+1
      tu=uuoff+nndx(max(tx,ux))+min(tx,ux)
      term=-two*d1(tu)
      if(u.eq.t)term=term+two
      if(term.ne.zero)call daxpy_wr(ninj,term,pmat,1,b(tv),1)
620   continue
c
c  usym.ne.vsym two-particle density matrix terms.
c
      do 650 psym=1,nsym
      rsym=mult(psym,uvsym)
      if(rsym.gt.psym)go to 650
      if(napsy(psym).eq.0)go to 650
      if(ndpsy(psym).eq.0)go to 650
      if(napsy(rsym).eq.0)go to 650
      if(ndpsy(rsym).eq.0)go to 650
      ninjp=ndpsy(psym)*ndpsy(rsym)
      p1=ira1(psym)
      p2=ira2(psym)
      r1=ira1(rsym)
      r2=ira2(rsym)
c
      do 640 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
      pv=nndx(max(p,v))+min(p,v)
c
      do 630 r=r1,r2
      ru=nndx(max(r,u))+min(r,u)
      rv=nndx(max(r,v))+min(r,v)
      pr=nndx(p)+r
      prij=off13(pr)+1
c  direct term...
      ijd=sdpt(psym)
      pruv=off1(max(pr,uv))+addaa(min(pr,uv))
      term=four*d2(pruv)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijd),1,b(prij),1)
c  first exchange term...
      ijx=sxpt(psym)
      purv=off1(max(pu,rv))+addaa(min(pu,rv))
      term=four*d2(purv)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijx),1,b(prij),1)
c  second exchange term...
      ijxt=sxtpt(rsym)
      pvru=off1(max(pv,ru))+addaa(min(pv,ru))
      term=four*d2(pvru)
      if(term.ne.zero)call daxpy_wr(ninjp,term,h2(ijxt),1,b(prij),1)
630   continue
640   continue
650   continue
c
700   continue
800   continue
c
      if(flags(3))call timer('badad',4,event,nlist)
      return
      end
      subroutine badadf(b,udd,d1,fdd,qaa,faa,off13)
c
c  include udd(*), fdd(*), qaa(*), and faa(*) contributions to
c  the adad hessian block.
c
c  b(j,i,pr) = b(pi,rj) = 2*u(ij)*d(pr)
c                        -delta(pr)(f(ij)+f(ji))
c                        +delta(ij)(2*q(pr)-(f(pr)+f(rp)))
c
c  ni*ni saxpy version 14-jan-85 (ron shepard).
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),n2sd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,12),n2sd(1))
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(*),udd(*),d1(*),fdd(*),qaa(*),faa(*)
      integer off13(*)
c
      real*8 zero,two,term
      parameter(zero=0d0,two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      do 300 isym=1,nsym
      if(ndpsy(isym).eq.0)go to 300
      if(napsy(isym).eq.0)go to 300
      ni=ndpsy(isym)
      nini=ni*ni
      nip1=ni+1
      np=napsy(isym)
      pr1=nnsa(isym)
      x0=nsa(isym)
      ij1=n2sd(isym)+1
c
      do 200 p=1,np
      pr=nndx(x0+p)+x0
      pr2=n2sa(isym)+(0-1)*np+p
      rp2=n2sa(isym)+(p-1)*np+0
c
      do 100 r=1,p
      pr=pr+1
      pr1=pr1+1
      pr2=pr2+np
      rp2=rp2+1
      jipr=off13(pr)+1
c
c  u(*) terms.
c
      term=two*d1(pr1)
      if(term.ne.zero)call daxpy_wr(nini,term,udd(ij1),1,b(jipr),1)
c
c  fdd(*) terms.
c  note that f(ij)=f(ji).
c
      if(p.eq.r)call daxpy_wr(nini,-two,fdd(ij1),1,b(jipr),1)
c
c  qaa(*) and faa(*) terms.
c
      term=two*qaa(pr1)-(faa(pr2)+faa(rp2))
      if(term.ne.zero)then
         iipr=jipr
         do 10 i=1,ni
            b(iipr)=b(iipr)+term
            iipr=iipr+nip1
10       continue
      endif
c
100   continue
200   continue
300   continue
c
      if(flags(3))call timer('badadf',4,event,nlist)
      return
      end
      subroutine bvaaa(d2,h2,b,iorder,off1,off5,addaa,addva)
c
c  2-e integral contributions to the vaaa block of the
c  orbital-orbital hessian matrix.
c
c  b(ar,pq) = sum   2*(aq|uv)*d2(pruv) -2*(ap|uv)*d2(qruv)
c            (uv)
c                 +4*(av|qu)*d2(rvpu) -4*(av|pu)*d2(rvqu)
c
c  outer-product algorithm is used to exploit the zeros in d2(*).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      integer tsymva(8)
      equivalence (nxy(1,26),tsymva(1),nvat)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 d2(*),h2(*),b(nvat,*)
      integer iorder(*)
      integer off1(*),off5(*),addaa(*),addva(ndimv,ndima)
c
      real*8 zero,two,four,factor,term,sterm
      parameter(zero=0d0,two=2d0,four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      uv=0
      do 10100 u=1,nact
      usym=symx(ix0a+u)
      do 10000 v=1,u
      vsym=symx(ix0a+v)
      uvsym=mult(vsym,usym)
      uv=uv+1
      if(tsymva(uvsym).eq.0)go to 10000
c
c  b(ar,pq)=sum(uv) 2*(uv|qa)*d(uvpr) -2*(uv|pa)*d(uvqr)  terms
c
      factor=four
      if(u.eq.v)factor=two
      do 500 psym=1,nsym
      if(napsy(psym).lt.2)go to 500
      rsym=mult(psym,uvsym)
      if(napsy(rsym).eq.0)go to 500
c  asym=rsym
      if(nvpsy(rsym).eq.0)go to 500
c
      p1=ira1(psym)
      p2=ira2(psym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      a1=irv1(rsym)
      na=nvpsy(rsym)
c
      do 400 p=p1,p2
c
      do 300 r=r1,r2
      pr=nndx(max(p,r))+min(p,r)
      uvpr=off1(max(pr,uv))+addaa(min(pr,uv))
      term=factor*d2(uvpr)
      if(term.eq.zero)go to 300
      a1r=addva(a1,r)
c
      do 200 q=p1,p2
      if(p.eq.q)go to 200
      if(p.lt.q)then
          sterm=-term
          pq=nndx(q)+p
      else
          sterm=term
          pq=nndx(p)+q
      endif
      ipq=iorder(pq)
      if(ipq.eq.0)go to 200
      aquv=off5(uv)+addva(a1,q)
c
c  saxpy(na,sterm,h2(aquv),1,b(a1r,ipq),1)
c
      do 100 a=1,na
100   b(a1r-1+a,ipq)=b(a1r-1+a,ipq)+sterm*h2(aquv-1+a)
c
200   continue
300   continue
400   continue
500   continue
c
c  b(ar,pu)=sum(vq)  4*(aq|uv)*d(pvrq)      p.gt.u
c  b(ar,up)=sum(vq) -4*(aq|uv)*d(pvrq)      u.gt.p
c
      if(napsy(usym).lt.2)go to 2000
      do 1500 rsym=1,nsym
      if(napsy(rsym).eq.0)go to 1500
      if(nvpsy(rsym).eq.0)go to 1500
      qsym=mult(rsym,uvsym)
      if(napsy(qsym).eq.0)go to 1500
c
      p1=ira1(usym)
      p2=ira2(usym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      q1=ira1(qsym)
      q2=ira2(qsym)
      a1=irv1(rsym)
      na=nvpsy(rsym)
c
      do 1400 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
      ipu=iorder(pu)
      if(ipu.eq.0)go to 1400
      pv=nndx(max(p,v))+min(p,v)
c
      do 1300 r=r1,r2
      a1r=addva(a1,r)
c
      do 1200 q=q1,q2
      rq=nndx(max(r,q))+min(r,q)
      pvrq=off1(max(pv,rq))+addaa(min(pv,rq))
      term=four*d2(pvrq)
      if(term.eq.zero)go to 1200
      if(p.lt.u)term=-term
c
      aquv=off5(uv)+addva(a1,q)
c
c  saxpy(na,term,h2(aquv),1,b(a1r,ipu),1)
c
      do 1100 a=1,na
1100  b(a1r-1+a,ipu)=b(a1r-1+a,ipu)+term*h2(aquv-1+a)
c
1200  continue
1300  continue
1400  continue
1500  continue
c
2000  continue
      if(u.eq.v)go to 3000
c
c  b(ar,pv)=sum(uq)  4*(aq|uv)*d(purq)  p.gt.v
c  b(ar,vp)=sum(uq) -4*(aq|uv)*d(purq)  v.gt.p
c
      if(napsy(vsym).lt.2)go to 3000
      do 2500 rsym=1,nsym
      if(napsy(rsym).eq.0)go to 2500
      if(nvpsy(rsym).eq.0)go to 2500
      qsym=mult(rsym,uvsym)
      if(napsy(qsym).eq.0)go to 2500
c
      p1=ira1(vsym)
      p2=ira2(vsym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      q1=ira1(qsym)
      q2=ira2(qsym)
      a1=irv1(rsym)
      na=nvpsy(rsym)
c
      do 2400 p=p1,p2
      pv=nndx(max(p,v))+min(p,v)
      ipv=iorder(pv)
      if(ipv.eq.0)go to 2400
      pu=nndx(max(p,u))+min(p,u)
c
      do 2300 r=r1,r2
      a1r=addva(a1,r)
c
      do 2200 q=q1,q2
      rq=nndx(max(r,q))+min(r,q)
      purq=off1(max(pu,rq))+addaa(min(pu,rq))
      term=four*d2(purq)
      if(term.eq.zero)go to 2200
      if(p.lt.v)term=-term
c
      aquv=off5(uv)+addva(a1,q)
c
c  saxpy(na,term,h2(aquv),1,b(a1r,ipv),1)
c
      do 2100 a=1,na
2100  b(a1r-1+a,ipv)=b(a1r-1+a,ipv)+term*h2(aquv-1+a)
c
2200  continue
2300  continue
2400  continue
2500  continue
c
3000  continue
c
10000 continue
10100 continue
      if(flags(3))call timer('bvaaa',4,event,nlist)
      return
      end
      subroutine bvaaaf(b,u,d,f,iorder)
c
c  include the 1-e hamaltonian and fock matrix contributions
c  to the vaaa block of the orbital-orbital hessian matrix.
c
c  b(ar,pq)=  2*u(aq)*d(pr)   - 2*u(ap)*d(qr)
c          + f(ap)*delta(qr) - f(aq)*delta(pr)
c
c  input:
c  b(*)      = vaaa block of the hessian matrix.
c  u(*)      = va block of the effective 1-e hamiltonain matrix.
c  d(*)      = 1-particle density matrix.
c  f(*)      = va block of the fock matrix.
c  iorder(*) = active-active indexing array.
c
c  output:
c  b(*)      = updated hessian matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer tsymva(8)
      equivalence (nxy(1,26),tsymva(1),nvat)
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(nvat,*),u(*),d(*),f(*)
      integer iorder(*)
c
      real*8 term1,term2,two
      parameter(two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  1-e hamiltonain terms:
c
      na=0
      nv=0
      a1p1=1
      do 500 isym=1,nsym
      na=napsy(isym)
      if(na.eq.0)go to 500
      nv=nvpsy(isym)
      if(nv.eq.0)go to 500
      x0=nsa(isym)
      nnoff=nnsa(isym)
c
      a1p=a1p1
      do 400 p=1,na
      px0=nndx(x0+p)+x0
c
      a1q=a1p1
      do 300 q=1,p
      ipq=iorder(px0+q)
      if(ipq.eq.0)go to 300
      a1r=a1p1
c
      do 200 r=1,na
      pr=nnoff+nndx(max(p,r))+min(p,r)
      qr=nnoff+nndx(max(q,r))+min(q,r)
      term1=two*d(pr)
      term2=-two*d(qr)
c
c  saxpy(nv,term1,u(a1q),1,b(a1r,ipq),1)
c  saxpy(nv,term2,u(a1p),1,b(a1r,ipq),1)
c
      do 100 a=1,nv
      b(a1r-1+a,ipq)=b(a1r-1+a,ipq)+term1*u(a1q-1+a)+term2*u(a1p-1+a)
100   continue
c
200   a1r=a1r+nv
c
300   a1q=a1q+nv
c
400   a1p=a1p+nv
c
500   a1p1=a1p1+na*nv
c
c  fock matrix terms:
c
      a1p1=1
      do 1500 isym=1,nsym
      na=napsy(isym)
      if(na.eq.0)go to 1500
      nv=nvpsy(isym)
      if(nv.eq.0)go to 1500
      x0=nsa(isym)
c
      a1p=a1p1
      do 1400 p=1,na
      px0=nndx(x0+p)+x0
c
      a1q=a1p1
      do 1300 q=1,p
      ipq=iorder(px0+q)
      if(ipq.eq.0)go to 1300
c
      aq=a1q
      ap=a1p
c
c  saxpy(nv, one,f(ap),1,b(aq,ipq),1)
c  saxpy(nv,-one,f(aq),1,b(ap,ipq),1)
c
      do 1100 a=1,nv
1100  b(aq-1+a,ipq)=b(aq-1+a,ipq)+f(a1p-1+a)
      do 1200 a=1,nv
1200  b(ap-1+a,ipq)=b(ap-1+a,ipq)-f(a1q-1+a)
c
1300  a1q=a1q+nv
c
1400  a1p=a1p+nv
c
1500  a1p1=a1p1+nv*na
c
      if(flags(3))call timer('bvaaaf',4,event,nlist)
      return
      end
      subroutine bvaad(b,hd,hx,x,d1,
     +  d2,off1,off3,off4,off14,
     +  addaa,addvd)
c
c  two-electron integral contributions to the vaad block
c  of the orbital-orbital hessian matrix.
c
c  b(a,i,r,p) = b(ar,ip) = sum     2*(ai|uv)*d2(pr,uv)
c                          (u,v)
c                                 +4*(av|iu)*d2(pu,rv)
c
c                         -sum     2*x(at,ip)*d1(rt)
c                          (t)
c
c  where x(at,ip) = 4*(at|ip)-(ai|pt)-(ap|it)  is computed
c  from the sorted integral list.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(*),hd(*),hx(*),x(*),d1(*),d2(*)
      integer off1(*),off3(*),off4(ndima,ndima),off14(ndima,ndima)
      integer addaa(*),addvd(ndimv,ndimd)
c
      real*8 zero,two,three,four,dterm,term
      parameter(zero=0d0,two=2d0,three=3d0,four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  one-particle density matrix terms.
c
      do 170 psym=1,nsym
      if(napsy(psym).eq.0)go to 170
      if(ndpsy(psym).eq.0)go to 170
      p1=ira1(psym)
      p2=ira2(psym)
      i1=ird1(psym)
c
      do 160 rsym=1,nsym
      if(napsy(rsym).eq.0)go to 160
      if(nvpsy(rsym).eq.0)go to 160
      r0=nsa(rsym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      a1=irv1(rsym)
      nai=nvpsy(rsym)*ndpsy(psym)
c
      do 150 p=p1,p2
      do 140 t=r1,r2
      pt=nndx(max(p,t))+min(p,t)
c
      aipt=off3(pt)+addvd(a1,i1)
      atip=off4(t,p)+addvd(a1,i1)
      apit=off4(p,t)+addvd(a1,i1)
c
c  compute x(*) for this p-t block.
c
      if(p.eq.t)then
      do 110 ai=1,nai
110   x(ai)=three*hx(atip-1+ai)-hd(aipt-1+ai)
      else
      do 120 ai=1,nai
120   x(ai)=four*hx(atip-1+ai)-hx(apit-1+ai)-hd(aipt-1+ai)
      endif
c
c  include x(*) contributions.
c
      do 130 r=r1,r2
      term=-two*d1(nnsa(rsym)+nndx(max(r-r0,t-r0))+min(r-r0,t-r0))
      airp=off14(r,p)+1
      if(term.ne.zero)call daxpy_wr(nai,term,x,1,b(airp),1)
130   continue
c
140   continue
150   continue
c
160   continue
170   continue
c
c  include two-particle density matrix and hd(*) terms.
c
      do 250 u=1,nact
      usym=symx(ix0a+u)
      do 240 v=1,u
      vsym=symx(ix0a+v)
      uvsym=mult(vsym,usym)
      uv=nndx(u)+v
      dterm=four
      if(u.eq.v)dterm=two
c
      do 230 psym=1,nsym
      rsym=mult(psym,uvsym)
      if(napsy(psym).eq.0)go to 230
      if(napsy(rsym).eq.0)go to 230
      if(ndpsy(psym).eq.0)go to 230
      if(nvpsy(rsym).eq.0)go to 230
      p1=ira1(psym)
      p2=ira2(psym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      i1=ird1(psym)
      a1=irv1(rsym)
      nai=ndpsy(psym)*nvpsy(rsym)
      aiuv=off3(uv)+addvd(a1,i1)
c
      do 220 p=p1,p2
      do 210 r=r1,r2
      pr=nndx(max(p,r))+min(p,r)
      pruv=off1(max(pr,uv))+addaa(min(pr,uv))
      term=dterm*d2(pruv)
      if(term.eq.zero)go to 210
      airp=off14(r,p)+1
      call daxpy_wr(nai,term,hd(aiuv),1,b(airp),1)
210   continue
220   continue
c
230   continue
c
240   continue
250   continue
c
c  include two-particle density matrix and hx(*) terms.
c
      do 350 u=1,nact
      usym=symx(ix0a+u)
      do 340 v=1,nact
      vsym=symx(ix0a+v)
      uvsym=mult(vsym,usym)
c
      do 330 psym=1,nsym
      rsym=mult(psym,uvsym)
      if(napsy(psym).eq.0)go to 330
      if(napsy(rsym).eq.0)go to 330
      if(ndpsy(psym).eq.0)go to 330
      if(nvpsy(rsym).eq.0)go to 330
      p1=ira1(psym)
      p2=ira2(psym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      i1=ird1(psym)
      a1=irv1(rsym)
      nai=ndpsy(psym)*nvpsy(rsym)
      aiuv=off4(v,u)+addvd(a1,i1)
c
      do 320 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
      do 310 r=r1,r2
      rv=nndx(max(r,v))+min(r,v)
      purv=off1(max(pu,rv))+addaa(min(pu,rv))
      term=four*d2(purv)
      if(term.eq.0)go to 310
      airp=off14(r,p)+1
      call daxpy_wr(nai,term,hx(aiuv),1,b(airp),1)
310   continue
320   continue
c
330   continue
c
340   continue
350   continue
c
      if(flags(3))call timer('bvaad',4,event,nlist)
      return
      end
      subroutine bvaadf(b,d1,fvd,uvd,off14)
c
c  fvd(*) and uvd(*) contributions to the vaad block of the
c  orbital-orbital hessian matrix.
c
c  b(a,i,r,p) = b(ar,ip) = 2*uvd(ai)*d1(pr) - delta(pr)*fvd(ai)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer nsvd(8)
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxtot(18),ndima)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(*),d1(*),fvd(*),uvd(*)
      integer off14(ndima,ndima)
c
      real*8 zero,one,two,term
      parameter(zero=0d0,one=1d0,two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      do 300 isym=1,nsym
      if(napsy(isym).eq.0)go to 300
      if(ndpsy(isym).eq.0)go to 300
      if(nvpsy(isym).eq.0)go to 300
      p0=nsa(isym)
      p1=ira1(isym)
      p2=ira2(isym)
      ai=nsvd(isym)+1
      nai=nvpsy(isym)*ndpsy(isym)
c
      do 200 p=p1,p2
      do 100 r=p1,p2
      airp=off14(r,p)+1
      term=two*d1(nnsa(isym)+nndx(max(p-p0,r-p0))+min(p-p0,r-p0))
      if(term.ne.zero)call daxpy_wr(nai,term,uvd(ai),1,b(airp),1)
      if(p.eq.r)call daxpy_wr(nai,-one,fvd(ai),1,b(airp),1)
100   continue
200   continue
c
300   continue
c
      if(flags(3))call timer('bvaadf',4,event,nlist)
      return
      end
      subroutine bvava(d2,bufh2,b,off1,addaa,off12,hx,qvv,uvv,d1,
     & szh10)
c
c  two-electron integral contributions to the vava block of the
c  orbital-orbital hessian and qvv(*) matrix construction.
c
c    qvv(ac)= 2*uvv(ac) + sum(uv) (2*(ac|uv) - (au|cv))*d1(uv)
c
c    b(c,a,pr)=b(cr,ap)= sum(uv) 2*(ac|uv)*d2(pr,uv)
c                               +4*(au|cv)*d2(pu,rv)
c
c  na*nc saxpy version 10-oct-84 (ron shepard).
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(10),nact)
      integer nvpsy(8)
      equivalence (nxtot(15),n2tv)
      equivalence (nxy(1,17),nvpsy(1))
      integer tsymvv(8),tsmvv2(8)
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,28),tsmvv2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      common/cbufs/stape,lenbfs,h2bpt
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 d2(*),bufh2(lenbfs),b(*),hx(*)
      real*8 qvv(*),uvv(*),d1(*)
      integer off1(*),off12(*),addaa(*)
c
      real*8 zero,one,two,four
      parameter(zero=0d0,one=1d0,two=2d0,four=4d0)
      real*8 uvfac,term
      integer sym(8),symt(8)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  qvv(*) is stored square-packed by symmetry. expand and scale uvv(*).
c
      call xpnd1(uvv,qvv,nvpsy,nsym)
      call dscal_wr(n2tv,two,qvv,1)
c
c  direct-type contributions:
c  b(c,a,pr)=b(cr,ap)=sum(uv) 2*(ca|uv)*d(uvpr)
c           =  sum(u.gt.v) 4*(ca|uv)*d(uvpr)
c            + sum(u)      2*(ca|uu)*d(uupr)
c
      uv1=0
      uv=0
      do 1000 u=1,nact
      usym=symx(ix0a+u)
      do 900 v=1,u
      uv=uv+1
      vsym=symx(ix0a+v)
      if(usym.eq.vsym)uv1=uv1+1
      uvsym=mult(vsym,usym)
      nterm=tsymvv(uvsym)
      if(nterm.eq.0)go to 900
      uvfac=four
      if(u.eq.v)uvfac=two
c
c  copy integrals from 2-e buffer into rectangular matrix blocks
c  in hx(*,*).  usym=vsym triangular blocks are expanded.
c
      if(h2bpt+nterm-1 .gt. lenbfs)then
         read(stape)bufh2
         h2bpt=1
      endif
      ioff=0
      call cxxaa(bufh2(h2bpt),hx,sym,nvpsy,nterm,uvsym,ioff)
      h2bpt=h2bpt+nterm
c
c  qvv(*) direct-type contributions.
c
      term=uvfac*d1(uv1)
      if(usym.eq.vsym .and. term.ne.zero)
     +  call daxpy_wr(n2tv,term,hx,1,qvv,1)
c
      do 500 psym=1,nsym
      if(napsy(psym).eq.0)go to 500
      na=nvpsy(psym)
      if(na.eq.0)go to 500
      rsym=mult(psym,uvsym)
      if(rsym.gt.psym)go to 500
      if(napsy(rsym).eq.0)go to 500
      nc=nvpsy(rsym)
      if(nc.eq.0)go to 500
c
      p1=ira1(psym)
      p2=ira2(psym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      ca1=sym(psym)
      nanc=na*nc
c
      do 400 p=p1,p2
c
      r2x=min(p,r2)
      do 300 r=r1,r2x
c
c  update the (pr) block.
c
      pr=nndx(p)+r
      capr=off12(pr)+1
      uvpr=off1(max(pr,uv))+addaa(min(pr,uv))
      term=uvfac*d2(uvpr)
c
      if (szh10.ne.0) then
      if(term.ne.zero)
     +  call daxpy_wr(nanc,term,hx(ca1),1,b(capr),1)
      endif
c
300   continue
400   continue
500   continue
c
900   continue
1000  continue
c
c  exchange-type contributions:
c  b(c,a,pr)=b(cr,ap) = sum(uv) 4*(au|cv)*d(purv)
c           =  sum(u.ge.v) 4*(au|cv)*d(purv)
c            + sum(u.gt.v) 4*(cu|av)*d(pvru)
c
      uv1=0
      do 2000 u=1,nact
      usym=symx(ix0a+u)
      do 1900 v=1,u
      vsym=symx(ix0a+v)
      if(usym.eq.vsym)uv1=uv1+1
      uvsym=mult(vsym,usym)
      nterm=tsmvv2(uvsym)
      if(nterm.eq.0)go to 1900
c
c  copy integrals from 2-e buffer into rectangular matrix blocks
c  in hx(*,*).
c  for usym.eq.vsym, the hx(transpose) is also returned.
c  for usym.ne.vsym the most rapidly varying orbital index corresponds
c  to the smaller of the symmetry labels.
c  (pa|pb) integrals are in both locations (b,a) and (a,b).
c
      if(h2bpt+nterm-1 .gt. lenbfs)then
         read(stape)bufh2
         h2bpt=1
      endif
      ioff=0
      call cxaxa(bufh2(h2bpt),hx,sym,symt,nvpsy,n2tv,nterm,uvsym,ioff)
      h2bpt=h2bpt+nterm
c
c  qvv exchange-type contributions.
c    qvv(ac) = sum(uv) -(au|cv)*d1(uv)
c            = sum(u.gt.v) ( (au|cv) + (cu|av) ) * (-d1(uv))
c             +sum(u) (au|cu)*(-d1(uv))
c
c
      term=-d1(uv1)
      if(usym.eq.vsym .and. term.ne.zero)then
         call daxpy_wr(n2tv,term,hx,1,qvv,1)
         if(u.ne.v)call daxpy_wr(n2tv,term,hx(symt(1)),1,qvv,1)
      endif
c
      do 1500 psym=1,nsym
      if(napsy(psym).eq.0)go to 1500
      na=nvpsy(psym)
      if(na.eq.0)go to 1500
      rsym=mult(psym,uvsym)
      if(rsym.gt.psym)go to 1500
      if(napsy(rsym).eq.0)go to 1500
      nc=nvpsy(rsym)
      if(nc.eq.0)go to 1500
c
      p1=ira1(psym)
      p2=ira2(psym)
      r1=ira1(rsym)
      r2=ira2(rsym)
      ca1=sym(psym)
      cat1=symt(rsym)
      nanc=na*nc
c
      do 1400 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
      pv=nndx(max(p,v))+min(p,v)
c
      r2x=min(p,r2)
      do 1300 r=r1,r2x
      pr=nndx(p)+r
      capr=off12(pr)+1
c
      rv=nndx(max(r,v))+min(r,v)
      purv=off1(max(pu,rv))+addaa(min(pu,rv))
      term=four*d2(purv)
      if (szh10.ne.0) then
      if(term.ne.zero)
     +  call daxpy_wr(nanc,term,hx(ca1),1,b(capr),1)
      endif
c
      if(u.eq.v)go to 1300
      ru=nndx(max(r,u))+min(r,u)
      pvru=off1(max(pv,ru))+addaa(min(pv,ru))
      term=four*d2(pvru)
c
      if (szh10.ne.0) then
      if(term.ne.zero)
     +  call daxpy_wr(nanc,term,hx(cat1),1,b(capr),1)
      endif
c
1300  continue
1400  continue
1500  continue
c
1900  continue
2000  continue
c
      if(flags(3))call timer('bvava',4,event,nlist)
      return
      end
      subroutine bvavaf(b,u,ux,d,f,off12)
c
c  include the 1-e hamiltonian and fock matrix terms to the
c  vava block of the orbital-orbital hessian matrix.
c
c  b(c,a,pr) = b(crap) = 2*u(ca)*d(pr) - (frp+fpr)*delta(ac)
c
c  input:
c  b(*)       = vava block of the orbital hessian matrix.
c  u(*)       = symmetry blocked lower-triangular packed uvv matrix.
c  ux(*)      = scratch space for expanded u(*).
c  d(*)       = 1-particle density matrix.
c  f(*)       = symmetry blocked aa fock matrix.
c  off12(*)   = addressing array for b(*).
c
c  output:
c  b(*)       = updated hessian matrix blocks.
c
c  written 10-oct-84 by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      integer nvpsy(8),n2sv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,20),n2sv(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(*),u(*),ux(*),d(*),f(*)
      integer off12(*)
c
      real*8 term
      real*8 zero,two
      parameter (zero=0d0,two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  expand lower-triangular packed uvv array into square-packed
c  array to allow efficient matrix operations.
c
      call xpnd1(u,ux,nvpsy,nsym)
c
      do 500 isym=1,nsym
      na=napsy(isym)
      if(na.eq.0)go to 500
      nv=nvpsy(isym)
      if(nv.eq.0)go to 500
      x0=nsa(isym)
      ca1=n2sv(isym)+1
      nvnv=nv*nv
      nvp1=nv+1
c
      pr1=nnsa(isym)
      do 400 p=1,na
      pr=nndx(x0+p)+x0
      pr2=n2sa(isym)+(0-1)*na+p
      rp2=n2sa(isym)+(p-1)*na+0
c
      do 300 r=1,p
      pr1=pr1+1
      pr=pr+1
      pr2=pr2+na
      rp2=rp2+1
      capr=off12(pr)+1
c
c  1-e hamiltonian terms:
c
      term=two*d(pr1)
      if(term.ne.zero)
     +  call daxpy_wr(nvnv,term,ux(ca1),1,b(capr),1)
c
c  fock matrix terms:
c
      term=f(pr2)+f(rp2)
      if(term.ne.zero)then
      aapr=capr
      do 200 a=1,nv
      b(aapr)=b(aapr)-term
200   aapr=aapr+nvp1
      endif
c
300   continue
c
400   continue
c
500   continue
c
      if(flags(3))call timer('bvavaf',4,event,nlist)
c
      return
      end
      subroutine bvavd(b,bufh2,d1,dx)
c
c  2-e integral contributions to the vavd block of the
c  orbital-orbital hessian matrix.
c
c  b(a,p,ci) = sum 2*p(a,t,ci)*d1(tp)
c              (t)
c
c  where p(a,t,ci) = 4*(ci|at)-(ct|ai)-(ca|it) has been
c  computed during the integral sort.
c
c  in this version, the floating point operations are performed
c  within the c-i loops with matrix multiplications.  alternative
c  loop structures would move the c and/or the i loops to within
c  the p loop resulting in matrix multiplications involving
c  matrices of larger dimension: (na*nc,np), (na*ni,np) or
c  (na*nc*ni,np) instead of the present (na,np).  the present
c  choice allows computation directly from the integral buffer
c  without accumulation of several vectors into a scratch matrix.
c  on processors that require the larger dimensions to operate
c  efficiently, the accumulation of the vectors requires only
c  a trivial amount of overhead along with the extra storage.
c
c  matrix multiplication version 15-jan-85 by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer tsymvd(8),tsymva(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxy(1,26),tsymva(1),nvat)
c
      common/cbufs/stape,lenbfs,h2bpt
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(nvat,nvdt),bufh2(lenbfs),d1(*),dx(*)
      real*8 two
      parameter(two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  expand triangular-packed d1 matrix to square-packed form
c  for efficient matrix operations and include factor of two.
c
      call xpnd1(d1,dx,napsy,nsym)
      call dscal_wr(n2ta,two,dx,1)
c
      do 200 ci=1,nvdt
c
c  adjust integral pointers for the ci block of integrals.
c
      if(h2bpt+nvat-1 .gt. lenbfs)then
         read(stape)bufh2
         h2bpt=1
      endif
      atci=h2bpt
      h2bpt=h2bpt+nvat
c
      ap=1
      tp=1
      do 100 psym=1,nsym
      np=napsy(psym)
      na=nvpsy(psym)
c
c  form b = h * dx   for b(na,np), h(na,np), dx(np,np).
c  if possible, matrix multiplication should take advantage
c  of zeros in the dx() matrix.
c
      if(np.ne.0 .and. na.ne.0)call mxma
     + (bufh2(atci),1,na, dx(tp),1,np, b(ap,ci),1,na, na,np,np)
      ap=ap+na*np
      atci=atci+na*np
100   tp=tp+np*np
200   continue
c
      if(flags(3))call timer('bvavd',4,event,nlist)
      return
      end
      subroutine bvavdf(b,qad,fad)
c
c  include the q(*) and f(*) contributions to the vavd
c  block of the orbital-orbital hessian matrix.
c
c  b(a,p,ci) = -delta(ac)*(qad(ip)+fad(ip))
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      integer tsymvd(8),tsymva(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxy(1,26),tsymva(1),nvat)
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(19),ndimv)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(nvat,nvdt),qad(*),fad(*)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      do 400 isym=1,nsym
      if(ndpsy(isym).eq.0)go to 400
      if(nvpsy(isym).eq.0)go to 400
      if(napsy(isym).eq.0)go to 400
      ni=ndpsy(isym)
      nc=nvpsy(isym)
      np=napsy(isym)
      ci=nsvd(isym)
c
      do 300 i=1,ni
      do 200 c=1,nc
      ci=ci+1
      cp=nsva(isym)+c
      ip=nsad(isym)+i
c
      do 100 p=1,np
      b(cp,ci)=b(cp,ci)-(qad(ip)+fad(ip))
      ip=ip+ni
      cp=cp+nc
100   continue
c
200   continue
300   continue
400   continue
c
      if(flags(3))call timer('bvavdf',4,event,nlist)
      return
      end
      subroutine bvdaa(b,hd,hx,d,x,off3,off4,iorder)
c
c  2-e integral contributions to the vdaa block of the
c  orbital-orbital hessian matrix.
c
c  b(ai,pq) = sum 2*x(ai,qt)*d(pt) - 2*x(ai,pt)*d(qt)
c             (t)
c
c  where x(ai,qt)=4*(ai|qt)-(aq|it)-(at|iq) is computed from
c  the sorted integrals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxtot(18),ndima)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(nvdt,*),hd(*),hx(*),d(*),x(*)
      integer off3(*),off4(ndima,ndima),iorder(*)
c
      real*8 zero,two,four,term
      parameter(zero=0d0,two=2d0,four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  to avoid redundant x(*) computation, terms are calculated
c  for u.ge.v as:
c  b(ai,uw) = -2*x(ai,uv)*d(wv)  ;w<u
c  b(ai,wu) =  2*x(ai,uv)*d(wv)  ;w>u
c  b(ai,vw) = -2*x(ai,uv)*d(wu)  ;w<v
c  b(ai,wv) =  2*x(ai,uv)*d(wu)  ;w>v
c
      do 800 usym=1,nsym
      if(napsy(usym).eq.0)go to 800
      u0=nsa(usym)
      u1=ira1(usym)
      u2=ira2(usym)
c
      do 700 u=u1,u2
      do 600 v=u1,u
      uv=nndx(u)+v
c
c  compute x(**uv).
c
      aiuv=off3(uv)+1
      auiv=off4(u,v)+1
      aviu=off4(v,u)+1
c
      if(u.ne.v)then
c
      do 100 ai=1,nvdt
100   x(ai)=four*hd(aiuv-1+ai)-hx(auiv-1+ai)-hx(aviu-1+ai)
c
      else
c
      do 110 ai=1,nvdt
110   x(ai)=four*hd(aiuv-1+ai)-two*hx(auiv-1+ai)
c
      endif
c
c  include x(*) contributions.
c
      do 200 w=u1,u-1
      iuw=iorder(nndx(u)+w)
      if(iuw.eq.0)go to 200
      term=-two*d(nnsa(usym)+nndx(max(w-u0,v-u0))+min(w-u0,v-u0))
      if(term.ne.zero)call daxpy_wr(nvdt,term,x,1,b(1,iuw),1)
200   continue
c
      do 300 w=u+1,u2
      iwu=iorder(nndx(w)+u)
      if(iwu.eq.0)go to 300
      term=two*d(nnsa(usym)+nndx(w-u0)+(v-u0))
      if(term.ne.zero)call daxpy_wr(nvdt,term,x,1,b(1,iwu),1)
300   continue
c
      if(u.eq.v)go to 600
c
      do 400 w=u1,v-1
      ivw=iorder(nndx(v)+w)
      if(ivw.eq.0)go to 400
      term=-two*d(nnsa(usym)+nndx(u-u0)+(w-u0))
      if(term.ne.zero)call daxpy_wr(nvdt,term,x,1,b(1,ivw),1)
400   continue
c
      do 500 w=v+1,u2
      iwv=iorder(nndx(w)+v)
      if(iwv.eq.0)go to 500
      term=two*d(nnsa(usym)+nndx(max(w-u0,u-u0))+min(w-u0,u-u0))
      if(term.ne.zero)call daxpy_wr(nvdt,term,x,1,b(1,iwv),1)
500   continue
c
600   continue
700   continue
c
800   continue
c
      if(flags(3))call timer('bvdaa',4,event,nlist)
      return
      end
      subroutine bvdad(b,bufh2,d1,dx)
c
c  2-e integral contributions to the vdad block of the
c  orbital-orbital hessian matrix.
c
c  b(ip,aj) = sum p(it,aj)*dx(tp)
c             (t)
c
c  where p(it,aj) = 4*(it|aj)-(ia|jt)-(ij|at) has been
c  computed during the integral sort, and where
c  dx(tp) = 2*d1(tp) -4*delta(tp).
c
c  in this version, the floating point operations are performed
c  within the aj loops with matrix multiplications. alternative
c  loop structures would move the a and/or the j loops to within
c  the p loop resulting in matrix multiplications involving
c  matrices of larger dimension: (ni*na,np), (ni*nj,np), or
c  (ni*na*nj,np) instead of the present (ni,np).  the present
c  choice allows computation directly from the integral buffer
c  without accumulation of several vectors into a scratch matrix.
c  on processors that require the larger dimensions to operate
c  efficiently, the accumulation of the vectors requires only
c  a trivial amount of overhead along with the extra storage.
c
c  matrix multiplication version 16-jan-85 by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer tsymad(8),tsymvd(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
c
      common/cbufs/stape,lenbfs,h2bpt
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(nadt,*),bufh2(lenbfs),d1(*),dx(*)
      real*8 two,four
      parameter(two=2d0,four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  expand triangular-packed d1 matrix to square-packed form
c  for efficient matrix operations, scale by two, and shift
c  the diagonal elements.
c
      call xpnd1(d1,dx,napsy,nsym)
      call dscal_wr(n2ta,two,dx,1)
      do 20 psym=1,nsym
      if(napsy(psym).eq.0)go to 20
      np=napsy(psym)
      npp1=np+1
      pp=n2sa(psym)+1
      do 10 p=1,np
      dx(pp)=dx(pp)-four
10    pp=pp+npp1
20    continue

c
      do 200 aj=1,nvdt
c
c  adjust integral pointers for the aj block of integrals.
c
      if(h2bpt+nadt-1 .gt. lenbfs)then
         read(stape)bufh2
         h2bpt=1
      endif
      itaj=h2bpt
      h2bpt=h2bpt+nadt
c
      ip=1
      tp=1
      do 100 psym=1,nsym
      np=napsy(psym)
      ni=ndpsy(psym)
c
c  form b = h * dx  for b(ni,np), h(ni,np), dx(np,np).
c  if possible, matrix multiplication should take advantage
c  of zeros in the dx() matrix.
c
      if(np.ne.0 .and. ni.ne.0)call mxma
     +  (bufh2(itaj),1,ni, dx(tp),1,np, b(ip,aj),1,ni, ni,np,np)
      ip=ip+ni*np
      itaj=itaj+ni*np
      tp=tp+np*np
100   continue
200   continue
c
      if(flags(3))call timer('bvdad',4,event,nlist)
999   return
      end
      subroutine bvdadf(b,qva,fva)
c
c  include the q(*) and f(*) contributions to the vdad
c  block of the orbital-orbital hessian matrix.
c
c  b(ip,aj) = delta(ij)*(fva(ap)-2*qva(ap))
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      integer tsymad(8),tsymvd(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(19),ndimv)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 b(nadt,*),qva(*),fva(*)
c
      real*8 two
      parameter(two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      do 400 isym=1,nsym
      if(ndpsy(isym).eq.0)go to 400
      if(nvpsy(isym).eq.0)go to 400
      if(napsy(isym).eq.0)go to 400
      nj=ndpsy(isym)
      na=nvpsy(isym)
      np=napsy(isym)
      aj=nsvd(isym)
c
      do 300 j=1,nj
      do 200 a=1,na
      aj=aj+1
      jp=nsad(isym)+j
      ap=nsva(isym)+a
c
      do 100 p=1,np
      b(jp,aj)=b(jp,aj)+(fva(ap)-two*qva(ap))
      ap=ap+na
      jp=jp+nj
100   continue
c
200   continue
300   continue
400   continue
c
      if(flags(3))call timer('bvdadf',4,event,nlist)
      return
      end
      subroutine bvdvd(b,qvv,fdd,off11,bufh2)
c
c  bvdvd(*) matrix construction.
c
c  b(ai,cj) = 2*delta(ij)*qvv(ac) - 2*delta(ac)*fdd(ij) +4*p(ai,cj)
c
c  where p(ai,cj) = ( 4*(ai|cj) -(ac|ij) - (aj|ci) ) has been
c  calculated during the mo integral sort.  both p(*) and b(*) are
c  stored as a sequence of matrices (1...ndd) with elements indexed
c  by virtual orbitals a and c.  this is redundant for i=j blocks,
c  but allows for more efficient use of these blocks of the b matrix.
c  note that fdd(*) is stored square-packed.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxtot(7),ndot)
      integer nvpsy(8),n2sv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,20),n2sv(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d)
c
      common/cbufs/stape,lenbfs,h2bpt
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer off11(*)
      real*8 b(*),qvv(*),fdd(*)
      real*8 bufh2(lenbfs)
      real*8 term
      real*8 zero,two,four
      parameter(zero=0d0, two=2d0, four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      ij0=0
      ij=0
      do 400 i=1,ndot
      isym=symx(ix0d+i)
      ni=ndpsy(isym)
      na=nvpsy(isym)
      nap1=na+1
      ij1=ij0
c
      do 300 j=1,i
      ij=ij+1
      jsym=symx(ix0d+j)
      if(isym.eq.jsym)ij1=ij1+1
      nc=nvpsy(jsym)
      nanc=na*nc
      if(nanc.eq.0)go to 300
c
      bpt=off11(ij)+1
cMD
c     skip the 2-e contribution
c     if (flags(22)) go to 100
c     go to 100
cMD
c  get next block of p(*) elements.
c
      if(h2bpt+nanc-1 .gt. lenbfs)then
         read(stape)bufh2
         h2bpt=1
      endif
c
c  copy from integral buffer to b(*) and scale.
c
      call dcopy_wr(nanc,bufh2(h2bpt),1,b(bpt),1)
      h2bpt=h2bpt+nanc
      call dscal_wr(nanc,four,b(bpt),1)
c
c  qvv(*) terms.
c
 100  if(i.eq.j)then
         ca=n2sv(isym)+1
         call daxpy_wr(nanc,two,qvv(ca),1,b(bpt),1)
      endif
c
c  fdd(*) terms.
c
      if(isym.ne.jsym)go to 300
      term=-two*fdd(ij1)
      if(term.eq.zero)go to 300
      ca=bpt
      do 200 a=1,na
      b(ca)=b(ca)+term
200   ca=ca+nap1
c
300   continue
400   ij0=ij0+ni
c
      if(flags(3))call timer('bvdvd',4,event,nlist)
c
      return
      end
      subroutine c1aa(td1,ncsf,uaa,caa,iorder)
c
c  include the one-particle density contributions to the caa(*) matrix.
c
c     caa(n,pq) = sum 4*uaa(pt)*td1(n,qt) - 4*uaa(qt)*td1(n,pt)
c                 (t)
c
c  where td1(n,pt) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|ept+etp|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  uaa(*)    :active-active blocks of the effective 1-e h matrix.
c  iorder(*) :mapping vector for allowed active-active rotations.
c
c  output:
c  caa(*)    :updated caa(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 td1(ncsf),uaa(*),caa(ncsf,*)
      integer iorder(*)
      real*8 four,factor
      parameter(four=4d0)
c
      integer rangel(2,2),rangeh(2,2)
      logical skipl(2),skiph(2)
c
c  determine the do-loop ranges.
c
      do 10 i=1,2
      rangel(1,i)=ira1(inds)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(inds)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      noff=nsa(inds)
      nnoff=nnsa(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      t=ind(p2)
      rt=t-noff
c
c  include 4*uaa(pt)*d1(qt) terms.
c
      if(skiph(p1))go to 120
      q=ind(p1)
c
      rp=q-noff
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      rp=rp+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nnoff+nndx(max(rp,rt))+min(rp,rt)
      factor=four*uaa(pt)
      call daxpy_wr(ncsf,factor,td1,1,caa(1,ipq),1)
100   continue
120   continue
c
c  include -4*uaa(qt)*d1(pt) terms.
c
      if(skipl(p1))go to 220
      p=ind(p1)
c
      rq=0
      do 200 q=rangel(1,p1),rangel(2,p1)
      rq=rq+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nnoff+nndx(max(rq,rt))+min(rq,rt)
      factor=-four*uaa(qt)
      call daxpy_wr(ncsf,factor,td1,1,caa(1,ipq),1)
200   continue
220   continue
c
300   continue
c
      return
      end
      subroutine c1ad(td1,ncsf,uad,h2,cad,off2,addad)
c
c  include the one-particle density contributions to the cad(*) matrix.
c
c     cad(n,ip) = sum 4*(2*(ip|uv)-(iu|pv))*td1(n,uv)
c
c                +sum -4*uad(it)*td1(n,pt)
c                 (t)
c
c  where td1(n,pt) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|ept+etp|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  uad(*)    :double-active blocks of the effective 1-e h matrix.
c  addad(*)  :addressing array for 1-e integrals.
c  off2(*)   :addressing array for 2-e integrals.
c
c  output:
c  cad(*)    :updated cad(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 td1(ncsf),uad(*),cad(ncsf,*),h2(*)
      integer off2(*),addad(ndimd,ndima)
c
      real*8 zero,two,four,term
      parameter(zero=0d0,two=2d0,four=4d0)
c
c  2-e integral terms are computed as:
c
c    cad(n,ip) =  sum  4*( 4*(ip|uv) -(iu|pv) -(iv|pu) )*td1(n,uv)
c                 (u>v)
c
c                +sum  4*( 2*(ip|uu) - (iu|pu) )*td1(n,uu)
c                 (u)
c
      u=ind(1)
      v=ind(2)
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      ip=0
      ipuv=off2(nndx(max(u,v))+min(u,v))
      do 130 isym=1,nsym
      if(napsy(isym).eq.0)go to 130
      if(ndpsy(isym).eq.0)go to 130
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 120 p=p1,p2
      iupv=off2(nndx(max(p,v))+min(p,v))+addad(i1,u)
      ivpu=off2(nndx(max(p,u))+min(p,u))+addad(i1,v)
      do 110 i=i1,i2
      ip=ip+1
      ipuv=ipuv+1
      term=four*(four*h2(ipuv)-h2(iupv)-h2(ivpu))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cad(1,ip),1)
      iupv=iupv+1
      ivpu=ivpu+1
110   continue
120   continue
130   continue
c
      else
c
c  u.eq.v terms.
c
      ip=0
      ipuu=off2(nndx(u)+u)
      do 230 isym=1,nsym
      if(napsy(isym).eq.0)go to 230
      if(ndpsy(isym).eq.0)go to 230
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 220 p=p1,p2
      iupu=off2(nndx(max(p,u))+min(p,u))+addad(i1,u)
      do 210 i=i1,i2
      ip=ip+1
      ipuu=ipuu+1
      term=four*(two*h2(ipuu)-h2(iupu))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cad(1,ip),1)
      iupu=iupu+1
210   continue
220   continue
230   continue
c
      endif
c
c  include 1-e terms.
c
      ni=ndpsy(inds)
      if(ni.eq.0)return
      i1=ird1(inds)
c
      do 320 iperm=1,npassp
      p=ind(perm(1,iperm))
      t=ind(perm(2,iperm))
c
c  include -4*uad(it)*td1(pt) terms.
c
      ip=addad(i1,p)
      it=addad(i1,t)
c
      do 310 i=1,ni
      term=-four*uad(it)
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cad(1,ip),1)
      ip=ip+1
      it=it+1
310   continue
c
320   continue
c
      return
      end
      subroutine c1va(td1,ncsf,uva,cva,addva)
c
c  include the one-particle density contributions to the cva(*) matrix.
c
c     cva(n,ap) = sum -4*uva(at)*td1(n,pt)
c                 (t)
c
c  where td1(n,pt) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|ept+etp|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  uva(*)    :virtual-active blocks of the effective 1-e h matrix.
c  addva(*)  :addressing array.
c
c  output:
c  cva(*)    :updated cva(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      integer nsva(8)
      equivalence (nxy(1,31),nsva(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 td1(ncsf),uva(*),cva(ncsf,*)
      integer addva(ndimv,ndima)
c
      real*8 four,factor
      parameter(four=4d0)
c
      na=nvpsy(inds)
      if(na.eq.0)return
      a1=irv1(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      p=ind(p1)
      t=ind(p2)
c
c  include -4*uva(at)*td1(pt) terms.
c
      ap=addva(a1,p)
      at=addva(a1,t)
c
      do 200 a=1,na
      factor=-four*uva(at)
      call daxpy_wr(ncsf,factor,td1,1,cva(1,ap),1)
      ap=ap+1
      at=at+1
200   continue
c
300   continue
c
      return
      end
      subroutine c1vd(td1,ncsf,hd,hx,cvd,off3,off4)
c
c  include the one-particle density contributions to the cvd(*) matrix.
c
c     cvd(n,ai) = sum -4*( 2*(ai|uv)-(au|iv))*td1(n,uv)
c                 (uv)
c
c  where td1(n,uv) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|euv+euv|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  hd(*)     :2-e integrals of the type (ai|uv).
c  hx(*)     :2-e integrals of the type (au|iv).
c  off3(*)   :addressing array for hd(*) integrals.
c  off4(*)   :addressing array for hx(*) integrals.
c
c  output:
c  cvd(*)    :updated cvd(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxtot(18),ndima)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
      equivalence (ind(1),u),(ind(2),v)
c
      real*8 td1(ncsf),cvd(ncsf,*),hd(*),hx(*)
      integer off3(*),off4(ndima,ndima)
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 zero,two,four,term
      parameter(zero=0d0,two=2d0,four=4d0)
c
c  the matrix is computed as:
c
c     cvd(n,ai) = sum -4*( 4*(ai|uv) -(au|iv) -(av|iu) )*td1(n,uv)
c                 (u>v)
c
c                +sum -4*( 2(ai|uu) - (au|iu) )*td1(n,uu)
c                 (u)
c
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      uvd=off3(nndx(max(u,v))+min(u,v))
      uvx=off4(u,v)
      vux=off4(v,u)
      do 100 ai=1,nvdt
      term=-four*(four*hd(uvd+ai)-hx(uvx+ai)-hx(vux+ai))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cvd(1,ai),1)
100   continue
c
      else
c
c  u=v terms.
c
      uud=off3(nndx(u)+u)
      uux=off4(u,u)
      do 200 ai=1,nvdt
      term=-four*(two*hd(uud+ai)-hx(uux+ai))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cvd(1,ai),1)
200   continue
c
      endif
      return
      end
      subroutine c2aa(icol,td2,ncsf,h2,off1,addaa,caa,iorder)
c
c  include the two-particle density contributions to the caa matrix.
c
c     caa(n,pq) = 4 sum (pt|uv)*td2(n,qt,uv) - (qt|uv)*td2(n,pt,uv)
c                  (tuv)
c
c  where td2(n,pt,uv) is a symmetrized transition 2-particle density
c  matrix element.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),caa(ncsf,*),td2(ncsf)
      integer icol(*),iorder(*),off1(*),addaa(*)
c
      integer rangel(2,4),rangeh(2,4)
      logical skipl(4),skiph(4)
      real*8 four,eight,dterm,factor
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      rangel(1,i)=ira1(sym)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(sym)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include 4*(pt|uv)*td2(qt,uv) terms.
c
      if(skiph(p1))go to 120
      q=ind(p1)
      t=ind(p2)
c
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nndx(max(p,t))+min(p,t)
      factor=dterm*h2(off1(max(pt,uv))+addaa(min(pt,uv)))
      call daxpy_wr(ncsf,factor,td2,1,caa(1,ipq),1)
100   continue
120   continue
c
c  include -4*(qt|uv)*td2(pt,uv) terms.
c
      if(skipl(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      do 200 q=rangel(1,p1),rangel(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nndx(max(q,t))+min(q,t)
      factor=-dterm*h2(off1(max(qt,uv))+addaa(min(qt,uv)))
      call daxpy_wr(ncsf,factor,td2,1,caa(1,ipq),1)
200   continue
220   continue
c
300   continue
c
      return
      end
      subroutine c2ad(icol,td2,ncsf,h2,off2,addad,cad)
c
c  include the two-particle density contributions to the cad matrix.
c
c     cad(n,ip) = sum -4*(it|uv)*td2(n,pt,uv)
c                (tuv)
c
c  where td2(n,pt,uv) is a symmetrized transition 2-particle density
c  matrix element.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),cad(ncsf,*),td2(ncsf)
      integer icol(*),off2(*),addad(ndimd,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm,factor
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=ird1(sym)
      range(2,i)=ird2(sym)
      skip(i)=ndpsy(sym).eq.0
10    continue
c
      do 300 ipt=1,npassp
      iperm=icol(ipt)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include -4*(it|uv)*td2(pt,uv) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      ituv=off2(uv)+addad(range(1,p1),t)
      ip=addad(range(1,p1),p)
      do 200 i=range(1,p1),range(2,p1)
      factor=-dterm*h2(ituv)
      call daxpy_wr(ncsf,factor,td2,1,cad(1,ip),1)
      ip=ip+1
      ituv=ituv+1
200   continue
220   continue
c
300   continue
c
      return
      end
      subroutine c2va(icol,td2,ncsf,h2,off5,addva,cva)
c
c  include the two-particle density contributions to the cva matrix.
c
c     cva(n,ap) = sum -4*(at|uv)*td2(n,pt,uv)
c                (tuv)
c
c  where td2(n,pt,uv) is a symmetrized transition 2-particle density
c  matrix element.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),cva(ncsf,*),td2(ncsf)
      integer icol(*),off5(*),addva(ndimv,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm,factor
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=irv1(sym)
      range(2,i)=irv2(sym)
      skip(i)=nvpsy(sym).eq.0
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include -4*(at|uv)*td2(pt,uv) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      atuv=off5(uv)+addva(range(1,p1),t)
      ap=addva(range(1,p1),p)
      do 200 a=range(1,p1),range(2,p1)
      factor=-dterm*h2(atuv)
      call daxpy_wr(ncsf,factor,td2,1,cva(1,ap),1)
      ap=ap+1
      atuv=atuv+1
200   continue
220   continue
c
300   continue
c
      return
      end
      subroutine caaaa(p,q,pqsym,h2,d2,h2x,d2x,nuv,
     +  off1,addaa,type)
c
c  special copy routine to move the pq distribution of
c  2-e integrals and 2-particle density matrix elements
c  into the buffers h2x and d2x.  density matrix elements
c  have a redundancy factor included.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 h2(*),d2(*),h2x(*),d2x(*)
      integer off1(*),addaa(*)
c
      real*8 two
      parameter(two=2d0)
c
      pquv=1
      if(type.eq.2)go to 2000
c
c  direct-distributions (pquv):
c  only u.ge.v terms are copied with the factor of two included
c  for the u.ne.v terms in the density matrix elements.
c
      pq=nndx(p)+q
      if(pqsym.eq.1)go to 1000
c
c  usym.ne.vsym case: loop over usym
c
      nuv=0
      do 300 usym=1,nsym
      vsym=mult(usym,pqsym)
      if(vsym.gt.usym)go to 300
      nu=napsy(usym)
      if(nu.eq.0)go to 300
      nv=napsy(vsym)
      if(nv.eq.0)go to 300
      u1=ira1(usym)
      u2=ira2(usym)
      v1=ira1(vsym)
      v2=ira2(vsym)
      do 200 u=u1,u2
      uoff=nndx(u)
      do 100 v=v1,v2
      uv=uoff+v
      nuv=nuv+1
      pquv=off1(max(pq,uv))+addaa(min(pq,uv))
      h2x(nuv)=h2(pquv)
      d2x(nuv)=d2(pquv)*two
100   continue
200   continue
300   continue
      return
c
1000  continue
c
c  usym.eq.vsym case: loop over usym
c
      nuv=0
      do 1300 usym=1,nsym
      nu=napsy(usym)
      if(nu.eq.0)go to 1300
      u1=ira1(usym)
      u2=ira2(usym)
      do 1200 u=u1,u2
      uoff=nndx(u)
      do 1100 v=u1,u
      uv=uoff+v
      nuv=nuv+1
      pquv=off1(max(pq,uv))+addaa(min(pq,uv))
      h2x(nuv)=h2(pquv)
      d2x(nuv)=d2(pquv)*two
1100  continue
c  u=v term does not have factor of two.
      d2x(nuv)=d2(pquv)
1200  continue
1300  continue
      return
c
2000  continue
c
c  exchange-distributions (puqv):
c
      nuv=0
      do 2300 usym=1,nsym
      vsym=mult(usym,pqsym)
      nu=napsy(usym)
      if(nu.eq.0)go to 2300
      nv=napsy(vsym)
      if(nv.eq.0)go to 2300
      u1=ira1(usym)
      u2=ira2(usym)
      v1=ira1(vsym)
      v2=ira2(vsym)
      do 2200 u=u1,u2
      pu=nndx(max(p,u))+min(p,u)
      do 2100 v=v1,v2
      qv=nndx(max(q,v))+min(q,v)
      nuv=nuv+1
      puqv=off1(max(pu,qv))+addaa(min(pu,qv))
      h2x(nuv)=h2(puqv)
      d2x(nuv)=d2(puqv)
2100  continue
2200  continue
2300  continue
      return
      end
      subroutine cadu(uad,hvect,cad,ncsf)
c
c  include uad(*) contributions to the cad(*) matrix.
c
c  cad(ip,n) = 8*uad(ip)*hvect(n)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer tsymad(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 uad(*),hvect(*),cad(ncsf,*)
c
      real*8 zero,eight,term
      parameter(zero=0d0,eight=8d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      do 100 ip=1,nadt
      term=eight*uad(ip)
      if(term.ne.zero)call daxpy_wr(ncsf,term,hvect,1,cad(1,ip),1)
100   continue
c
      if(flags(3))call timer('cadu',4,event,nlist)
      return
      end
      subroutine cone(qc,ii,jj,isym,tden,ncsf,add,iorder,
     +  uaa,uad,uva,
     +  h2t2,h2t3,h2t4,
     +  caa,cad,cvd,cva)
c
c  include the 1-particle contributions from tden(*) to the required
c  blocks of the c matrix.
c
c  input:
c  qc(*)     :logical array indicating the required c blocks.
c  tden(*)   :transition density vector -- (1/2)<mc|eil+eli|n>.
c  iorder(*) :allowed active-active rotations.
c  u**(*)    :1-e h integral matrix element blocks.
c  h2t*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  c**(*)    :updated c matrix blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 tden(*),uaa(*),uad(*),uva(*)
      real*8 h2t2(*),h2t3(*),h2t4(*)
      real*8 caa(*),cad(*),cvd(*),cva(*)
      integer add(*),iorder(*)
      logical qc(4)
c
      integer npass(2)
      data npass/2,1/
c
c  determine the number of permutaiion passes required to
c  process this vector of 1-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1        ij            2     1,2
c    2        ii            1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      npassp=npass(itype)
c
c  initialize ind(*) and inds.
c
      ind(1)=ii
      ind(2)=jj
      inds=isym
c
      if(qc(1))call c1aa(tden,ncsf,uaa,caa,iorder)
c
      if(qc(2))call c1ad(tden,ncsf,uad,h2t2,cad,
     +  add(addpt(11)),add(addpt(3)))
c
      if(qc(3))call c1vd(tden,ncsf,h2t3,h2t4,cvd,
     +  add(addpt(12)),add(addpt(13)))
c
      if(qc(4))call c1va(tden,ncsf,uva,cva,add(addpt(6)))
c
      return
      end
c
c*******************************************************************+
c
      subroutine ctwo(qc,ii,jj,kk,ll,isym,jsym,ksym,lsym,
     +  tden,ncsf,add,iorder,
     +  h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
c
c  include the 2-particle contributions from tden(*) in the required
c  blocks of the c matrix.
c
c  input:
c  qc(*)     :logical array indicating the required c blocks.
c  tden(*)   :symmetrized transition density vector.
c  iorder(*) :allowed active-active rotations.
c  h2i*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  c**(*)    :updated c matrix blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      real*8 tden(*)
      real*8 h2t1(*),h2t2(*),h2t3(*),h2t4(*),h2t5(*)
      real*8 caa(*),cad(*),cva(*)
      integer add(*),iorder(*)
      logical qc(4)
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      integer icol(7),itypex(8),npass(6),colpt(6)
c
      data itypex/1,2,3,4,5,0,0,6/
      data npass/4,3,3,2,2,1/
      data colpt/1,5,1,5,1,1/
      data icol/1,2,3,4,  1,3,4/
c
c  determine the number of permutation passes required to
c  process this vector of 2-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1       ijkl          4     1,2,3,4
c    2       iikl          3     1,3,4
c    3       ijkk          3     1,2,3
c    4       iikk          2     1,3
c    5       ijij          2     1,2
c    6       iiii          1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      if(kk.eq.ll)itype=itype+2
      ij=nndx(max(ii,jj))+min(ii,jj)
      kl=nndx(max(kk,ll))+min(kk,ll)
      if(ij.eq.kl)itype=itype+4
      itype=itypex(itype)
      npassp=npass(itype)
      cpt=colpt(itype)
c
c  initialize ind(*) and inds(*).
c
      ind(1)=ii
      ind(2)=jj
      ind(3)=kk
      ind(4)=ll
      inds(1)=isym
      inds(2)=jsym
      inds(3)=ksym
      inds(4)=lsym
c
      if(qc(1))call c2aa(icol(cpt),
     +  tden,ncsf,h2t1,add(addpt(10)),add(addpt(4)),caa,iorder)
c
      if(qc(2))call c2ad(icol(cpt),
     +  tden,ncsf,h2t2,add(addpt(11)),add(addpt(3)),cad)
c
      if(qc(4))call c2va(icol(cpt),
     +  tden,ncsf,h2t5,add(addpt(14)),add(addpt(6)),cva)
c
      return
      end
      subroutine cvdu(uvd,hvect,cvd,ncsf)
c
c  include uvd(*) contributions to the cvd(*) matrix.
c
c  cvd(ai,n) = -8*uvd(ai)*hvect(n)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 uvd(*),hvect(*),cvd(ncsf,*)
c
      real*8 zero,eight,term
      parameter(zero=0d0,eight=8d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      do 100 ai=1,nvdt
      term=-eight*uvd(ai)
      if(term.ne.zero)call daxpy_wr(ncsf,term,hvect,1,cvd(1,ai),1)
100   continue
c
      if(flags(3))call timer('cvdu',4,event,nlist)
      return
      end
      subroutine hbcon(avcore,core,d1,d2,lastb,add,
     +  lenbft,lenbuk,npbuk,
     +  hvect,iorder,naar,
     +  xbar,xp,z,r,ind,modrt,
     +  udd,uad,uaa,uvd,uva,uvv,
     +  fdd,fad,faa,fvd,fva,qad,qaa,qva,qvv)
c
c  control the hessian block construction, formula tape processing,
c  and integral block processing.
c
c  written by ron shepard.
c
cmb   extended for the possibility of density matrix averaging.
cmb   two dummy par's added (navst,wavst) passed to rdft.
cmb   dimensioning of tden changed.

      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
      equivalence (iunits(23),cfile)
c
      common/chbci/hbci(4,3)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(9),n2td)
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
      equivalence (nxtot(12),n2ta)
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(15),n2tv)
      integer tsymad(8),tsymvd(8),tsymva(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxy(1,26),tsymva(1),nvat)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cbufs/stape,lenbfs,h2bpt
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 core(*),d1(*),d2(*),hvect(*)
      real*8 udd(*),uad(*),uaa(*),uvd(*),uva(*),uvv(*)
      real*8 fdd(*),fad(*),faa(*),fvd(*),fva(*)
      real*8 qad(*),qaa(*),qva(*),qvv(*)
      integer lastb(*),add(*),iorder(*)
      real*8  xbar(*),xp(*),z(*),r(*),ind(*),modrt(*)
c

c****************************************************************
c     avepar.h
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c****************************************************************
c

c**************************************************************
c     avstat.h
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c**************************************************************
c

c****************************************************************
c     size.h
c
      integer msize,csize
      common/size/ msize(maxnst),csize(4,maxnst)
c****************************************************************
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst(ist)
c
      real*8 emc_ave
      integer nwalk
c


c****************************************************************
c     drtf.h
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c****************************************************************
c

c****************************************************************
c     counter.h
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
c
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c****************************************************************
c
c
      logical qind
c
      integer  forbyt
      external forbyt
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      logical qb(4),qc(4),qft,qbk1
      integer ibpt(6),intpt(5),cpt(4)
      integer cmap(4)
      data cmap/12,11,13,14/
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  loop over 4 possible segments.
c
      blk2=0
      do 5000 iseg=1,4
      nbi=hbci(iseg,2)
      if(nbi.eq.0)go to 5100
      blk1=blk2+1
      blk2=blk2+nbi
      do 100 i=1,4
      qb(i)=.false.
100   qc(i)=.false.
      qft=.false.
      do 200 i=blk1,blk2
      blk=hbci(i,1)
      qb(blk)=.true.
      qc(blk)=hbci(i,3).ne.0
200   qft=qft.or.qc(blk)
c
c  ft is processed if a c-block is construced
c  or if the density matrices are to be constructed.
c
      qft=qft.or.((iseg.eq.1).and.(nact.ne.0))
c
c  get required integrals into core.
c  by construction, each segment requires only a single bucket
c  chain (except for type 1 integrals, which are sorted separately).
c
      do 1200 i=1,5
1200  intpt(i)=1
      icptx=1
      bpt=1
      ibpt(bpt)=icptx
      buk=0
      qbk1=.false.
      do 1700 i=blk1,blk2
      blk=hbci(i,1)
      go to(1300,1400,1500,1600),blk
1300  continue
c  type 1 integrals.
      intpt(1)=icptx
      icptx=icptx+numint(1)
      bpt=bpt+1
      ibpt(bpt)=icptx
      qbk1=.true.
      if (nact.eq.0) qbk1=.false.
      go to 1700
1400  continue
c  type 2 integrals.
      buk=bukpt(2)
      intpt(2)=icptx
      icptx=icptx+numint(2)
      bpt=bpt+1
      ibpt(bpt)=icptx
      go to 1700
1500  continue
c  type 3 and 4 integrals
      buk=bukpt(3)
      intpt(3)=icptx
      icptx=icptx+numint(3)
      intpt(4)=icptx
      icptx=icptx+numint(4)
      bpt=bpt+1
      ibpt(bpt)=icptx
      go to 1700
1600  continue
c  type 5 integrals
      buk=bukpt(5)
      intpt(5)=icptx
      icptx=icptx+numint(5)
      bpt=bpt+1
      ibpt(bpt)=icptx
1700  continue
c
c  zero out space for integrals.
c
      call wzero(icptx-1,core,1)
c
c  read in chained records of required integrals.
c   core:int...int,buk,ibuk
c
      bkpt=icptx
      ibkpt=bkpt+lenbuk
      if(ibkpt+forbyt(npbuk+1)-1.gt.avcore)then
         call bummer('hbcon: 1 avcore=',avcore,faterr)
      endif
      if(qbk1)call rdbks(lastb(bukpt(1)),
     +  core(intpt(1)),core(bkpt),core(ibkpt),lenbuk,npbuk,'hbcon')
      if(buk.ne.0)call rdbks(lastb(buk),
     +  core(1),core(bkpt),core(ibkpt),lenbuk,npbuk,'hbcon')
c
c  assign c matrix core space.
c   core:int...int,c...c

c     cpt    block type   order in Hessian
c
c      1      (csf,caa)        12
c      2      (csf,cad)        11
c      3      (csf,cvd)        13
c      4      (csf,cva)        14
c
      do 2100 i=1,4
2100  cpt(i)=1
      do 2200 i=blk1,blk2
      blk=hbci(i,1)
      if(.not.qc(blk))go to 2200
      cpt(blk)=icptx
      icptx=icptx+szh(cmap(blk))
2200  continue
c
c  read and process ft. density matrix and c blocks are constructed.
c   core:int...int,c...c,tden,bufft
c
cmb   tden has the dimensions of (ncsf,3,navst)
cmd   tden has the dimension of (ncsf_max,3,navst_max)
      tdpt=icptx
      bufpt=tdpt+3*ncsf_max*navst_max
      if(bufpt+lenbft-1.gt.avcore)then
         call bummer('hbcon: 2 avcore=',avcore,faterr)
c         write(nlist,4567) tdpt,lenbft,ncsf_max,navst_max,bufpt,avcore
c 4567    format(3x,'tdpt,lenbft,ncsf_max,navst_max,bufpt,avcore=',
c     &  6(i8,1x))
         stop 'no space for rdft call'
      endif
c
      call wzero(nnta,d1,1)
      call wzero(numint(1),d2,1)

      do ist = 1,nst
c
      qind = nwalk_f(ist) .ne. ncsf_f(ist)
c
      if(qft)call rdft(core(bufpt),lenbft,
     +  d1,d2,hvect(cpt4(ist)+1),iorder,
     +  ncsf_f(ist),xbar(cpt2(ist)+1),xp(cpt2(ist)+1),
     +  z(cpt2(ist)+1),r(cpt3(ist)+1),ind(cpt3(ist)+1),
     +  modrt,qind, core(tdpt),uaa,uad,uvd,uva,
     +  core(intpt(1)),core(intpt(2)),core(intpt(3)),
     +      core(intpt(4)),core(intpt(5)),
     +  add(addpt(10)),add(addpt(4)),add,
     +  core(cpt(1)),core(cpt(2)),
     +  core(cpt(3)),core(cpt(4)),
     +  qc,navst(ist),ist)
c
cmb   scale and
c  write out any c blocks constructed during the ft read.
c
      nncsf=navst(ist)*ncsf_f(ist)
      do 2300 i=1,4
      if(.not.qc(i)) go to 2300
      ii = cpt(i)
      nsize = csize(cmap(i)-10,ist)/nncsf
      do 2301 j=1,navst(ist)
      do 2301 ij=1,ncsf_f(ist)
      call dscal_wr(nsize,wavst(ist,j),core(ii),nncsf)
2301  ii=ii+1
      call wrthb(csize(cmap(i)-10,ist),core(cpt(i)),cmap(i))
2300  continue
      if(qc(1).and.flags(6))call prblkt('caa matrix',
     +  core(cpt(1)),nncsf,nncsf,naar,' csf','(aa)',1,nlist)
      if(qc(2).and.flags(6))call prblkt('cad matrix',
     +  core(cpt(2)),nncsf,nncsf,nadt,' csf','(ad)',1,nlist)
      if(qc(3).and.flags(6))call prblkt('cvd matrix',
     +  core(cpt(3)),nncsf,nncsf,nvdt,' csf','(vd)',1,nlist)
      if(qc(4).and.flags(6))call prblkt('cva matrix',
     +  core(cpt(4)),nncsf,nncsf,nvat,' csf','(va)',1,nlist)
c
      if (qc(1)) then
         cpt(1) = cpt(1) + csize(2,ist)
      else
         cpt(1) = cpt(1)
      endif
      if (qc(2)) then
         cpt(2) = cpt(2) + csize(1,ist)
      else
         cpt(2) = cpt(2)
      endif
      if (qc(3)) then
         cpt(3) = cpt(3) + csize(3,ist)
      else
         cpt(3) = cpt(3)
      endif
      if (qc(4)) then
         cpt(4) = cpt(4) + csize(4,ist)
      else
         cpt(4) = cpt(4)
      endif
c
      enddo ! ist
cmb  nncsf<----ncsf changed

c
c  loop over integral blocks currently in-core and construct the
c  corresponding b hessian blocks.
c
      do 3600 i=blk2,blk1,-1
      blk=hbci(i,1)
      go to (3100,3200,3300,3400),blk
3100  continue
c
c  type 1 integrals.  hessian block 3. faa. qaa.
c    q(pq)= 2*u(pq) + sum(uv) (2(pq|uv)-(pu|qv))*d(uv)
c    f(pq)= sum(t) u(pt)*d(tq) + sum(tuv) (pt|uv)*d(qtuv)
c    b(pq,rs)=2*(upr*dqs+uqs*dpr-ups*dqr-uqr*dps)
c             +delta(ps)*f(qr)+delta(qr)*f(ps)
c             -delta(pr)*f(qs)-delta(qs)*f(pr)
c             +sum(uv) 2*(pr|uv)*d(qs,uv) + 2*(qs|uv)*d(pr,uv)
c                     -2*(ps|uv)*d(qr,uv) - 2*(qr|uv)*d(ps,uv)
c                     +4*(pu|rv)*d(qu,sv) + 4*(qu|sv)*d(pu,rv)
c                     -4*(pu|sv)*d(qu,rv) - 4*(qu|rv)*d(pu,sv)
c   core:...int1,baaaa,bufpq,nuvx,pqpt,prd,x2
c
      if(nact.eq.0)go to 3500
      nnact=nndx(nact+1)
      bipt=ibpt(bpt)
      bpqpt=bipt+szh(3)
      nuvxpt=bpqpt+forbyt(nnact+1)
      pqptpt=nuvxpt+forbyt(nnact+1)
      prdpt=pqptpt+forbyt(nact**2)
      x2pt=prdpt+forbyt(2*nact**2)
      hxpt=bipt
      dxpt=hxpt+n2ta
      fxpt=dxpt+n2ta
      lenx2=avcore-x2pt+1
      if(lenx2.lt.4*n2ta)then
         call bummer('hbcon: lenx2=',lenx2,faterr)
      endif
      call mqaa(qaa,uaa,core(intpt(1)),d1,add(addpt(10)),add(addpt(4)))
      call mfaa(faa,uaa,d1,core(intpt(1)),d2,
     +  add(addpt(10)),add(addpt(4)),core(fxpt),core(hxpt),
     +  core(dxpt))
      if(szh(3).eq.0)go to 3500
      call wzero(szh(3),core(bipt),1)
      call baaaa(d2,core(intpt(1)),core(bipt),core(x2pt),
     +  iorder,core(bpqpt),core(nuvxpt),core(pqptpt),core(prdpt),
     +  lenx2,add(addpt(10)),add(addpt(4)))
      call baaaaf(core(bipt),uaa,d1,faa,iorder)
      call wrthb(szh(3),core(bipt),3)
      if(flags(6))call plblkt('baaaa matrix',
     +  core(bipt),naar,'(aa)',1,nlist)
      go to 3500
3200  continue
c  type 2 integrals.  hessian block 2, fad, qad.
c    q(ip) =  2*u(ip) + sum(uv) (2*(ip|uv)-(iu|pv))*d(uv)
c    f(ip) = q(ip) +sum(t) u(it)*d(tp) + sum(tuv) (it|uv)*d(pt,uv)
c    b(ir,pq) = 2*u(iq)*d(pr) -2*u(pi)*d(qr)
c              +delta(qr)*f(ip) -delta(pr)*f(iq)
c              +sum(uv) 2*(iq|uv)*d(pr,uv) - 2*(ip|uv)*d(qr,uv)
c                      +4*(iv|qu)*d(pu,rv) - 4*(iv|pu)*d(qu,rv)
c              +sum(t) 2*(4(ir|pt)-(ip|rt)-(it|pr))*d(qt)
c                     -2*(4(ir|qt)-(iq|rt)-(it|qr))*d(pt)
c    core:...int2,b,x
c
      if(nact.eq.0)go to 3500
      if(ndot.eq.0)go to 3500
      bipt=ibpt(bpt)
      xpt=bipt+szh(2)
      dxpt=bipt
      fxpt=dxpt+n2ta
      call mfad(fad,uad,d1,core(intpt(2)),d2,
     +  add(addpt(10)),add(addpt(4)),core(fxpt),core(dxpt))
      call mqad(qad,uad,core(intpt(2)),d1,add(addpt(11)),
     +  add(addpt(3)),core(dxpt),core(fxpt))
      if(szh(2).eq.0)go to 3500
      call wzero(szh(2),core(bipt),1)
      call baaad(d1,d2,core(intpt(2)),core(bipt),core(xpt),iorder,
     +  add(addpt(10)),add(addpt(11)),add(addpt(4)),add(addpt(3)))
      call baaadf(core(bipt),uad,fad,qad,d1,iorder,add(addpt(3)))
      if(flags(6))call prblkt('baaad matrix',
     +  core(bipt),nadt,nadt,naar,'(ad)','(aa)',1,nlist)
      call wrthb(szh(2),core(bipt),2)
      go to 3500
c
3300  continue
c  type 3 and 4 integrals.  hessian blocks 5 and 7. fvd.
c    f(ai)=2*u(ai) + sum(uv) (2*(ai|uv)-(au|iv))*d(uv)
c    b(ai,pq)= sum(t) 2*(4(ai|qt)-(at|iq)-(aq|it))*d(tp)
c                    -2*(4(ai|pt)-(at|ip)-(ap|it))*d(tq)
c    b(ar,ip)= 2*u(ai)*d(pr) - delta(pr)*f(ai)
c             -sum(t) 2*(4(at|ip)-(ai|pt)-(ap|it))*d(tr)
c             +sum(uv) 2*(ai|uv)*d(pr,uv) + 4*(av|iu)*d(pu,rv)
c
      if(nvdt.ne.0)call mfvd(fvd,uvd,core(intpt(3)),core(intpt(4)),d1,
     +  add(addpt(12)),add(addpt(13)))
      if(szh(5).eq.0)go to 3350
      bipt=ibpt(bpt)
      xpt=bipt+szh(5)
      call wzero(szh(5),core(bipt),1)
      call bvdaa(core(bipt),core(intpt(3)),core(intpt(4)),d1,
     +  core(xpt),add(addpt(12)),add(addpt(13)),iorder)
      if(flags(6))call prblkt('bvdaa matrix',
     +  core(bipt),nvdt,nvdt,naar,'(vd)','(aa)',1,nlist)
      call wrthb(szh(5),core(bipt),5)
3350  continue
      if(szh(7).eq.0)go to 3500
      bipt=ibpt(bpt)
      xpt=bipt+szh(7)
      call wzero(szh(7),core(bipt),1)
      call bvaad(core(bipt),core(intpt(3)),core(intpt(4)),
     +  core(xpt),d1,d2,add(addpt(10)),add(addpt(12)),add(addpt(13)),
     +  add(addpt(23)),add(addpt(4)),add(addpt(5)))
      call bvaadf(core(bipt),d1,fvd,uvd,add(addpt(23)))
      if(flags(6))call pbvaad(core(bipt))
      call wrthb(szh(7),core(bipt),7)
      go to 3500
c
3400  continue
c
c  type 5 integrals.  hessian block 8. fva. qva
c    q(ap) = 2*u(ap) + sum(uv) (2*(ap|uv) - (au|pv))*d(uv)
c    f(ap) = sum(t) u(at)*d(tp) + sum(tuv) (at|uv)*d(pt,uv)
c    b(ar,pq) = 2*u(qa)*d(pr) -2*u(pa)*d(qr)
c               +delta(qr)*f(ap)-delta(pr)*f(aq)
c              +sum(uv) 2*(ap|uv)*d(pr,uv) -2*(ap|uv)*d(qr,uv)
c                      +4*(av|qu)*d(pu,rv) -4*(av|pu)*d(qu,rv)
c   core:...int5,bvaaa
c
      if(nvat.eq.0)go to 3500
      bipt=ibpt(bpt)
      fxpt=bipt
      dxpt=fxpt+nvat
      if(bipt+szh(8)-1.gt.avcore)then
         call bummer('hbcon: 3 avcore=',avcore,faterr)
      endif
      call mqva(qva,uva,core(intpt(5)),d1,add(addpt(14)),
     +  add(addpt(6)),core(dxpt),core(fxpt))
      call mfva(fva,uva,d1,core(intpt(5)),d2,
     +  add(addpt(10)),add(addpt(4)),core(fxpt),core(dxpt))
      if(szh(8).eq.0)go to 3500
      call wzero(szh(8),core(bipt),1)
      call bvaaa(d2,core(intpt(5)),core(bipt),
     +  iorder,add(addpt(10)),add(addpt(14)),add(addpt(4)),
     +  add(addpt(6)))
      call bvaaaf(core(bipt),uva,d1,fva,iorder)
      call wrthb(szh(8),core(bipt),8)
      if(flags(6))call prblkt('bvaaa matrix',
     +  core(bipt),nvat,nvat,naar,'(va)','(aa)',1,nlist)
3500  continue
      bpt=bpt-1
3600  continue
c
c  end of segment...
5000  continue
5100  continue
c
c  construct remaining b blocks.
c   core:bufh2,b
c
      rewind stape
      h2bpt=lenbfs+1
      h2pt=1
      bipt=h2pt+lenbfs
c
c  integral block 6.  hessian block 1. fdd.
c    f(ij)= 2*u(ij) + sum(uv) (2*(ij|uv) - (iu|jv))*d(uv)
c    b(ip,jr)= 2*u(ij)*d(pr) -delta(pr)*2*f(ij)
c             +delta(ij)*(2*q(pr)-f(pr))
c             +4*(4(pi|rj)-(pr|ij)-(pj|ri))
c             -sum(t) 2*(4(pi|jt)-(ij|pt)-(pj|it))*d(rt)
c                    +2*(4(rj|it)-(ij|rt)-(ri|jt))*d(pt)
c             +sum(uv) 2*(ij|uv)*d(pr,uv) +4*(iu|jv)*d(pu,rv)
c   core:bufh2,b,h2x,pmat,ptmat
c
      if(ndot.eq.0)go to 5210
      call wzero(szh(1),core(bipt),1)
      h2xpt=bipt+szh(1)
      pmpt=h2xpt+3*n2td
      ptmpt=pmpt+n2td
      call badad(core(bipt),core(h2xpt),core(pmpt),core(ptmpt),fdd,
     +  udd,core(h2pt),d1,d2,add(addpt(10)),
     +  add(addpt(4)),add(addpt(22)))
      call badadf(core(bipt),udd,d1,fdd,qaa,faa,add(addpt(22)))
      if(flags(6))call pbadad(core(bipt))
      if(szh(1).ne.0)call wrthb(szh(1),core(bipt),1)
c
5210  continue
c
c  integral blocks 7 and 8.  hessian block 10. qab
c    q(ab)= u(ab) + sum(uv) (2(ab|uv)-(au|bv))*d(uv)
c    b(c,a,pr)=b(cr,ap)= 2*u(ac)*d(pr) - delta(ac)*f(pr)
c              + sum(uv) 2*(ac|uv)*d(pr,uv) +4*(au|cv)*d(pu,rv)
c   core:bufh2,bvava,h2x
c
cmd   if(szh(10).eq.0)go to 5220
      h2xpt=bipt+szh(10)
      if(h2xpt+3*n2tv-1.gt.avcore)then
         call bummer('hbcon: 4 avcore=',avcore,faterr)
      endif
      call wzero(szh(10),core(bipt),1)
      call bvava(d2,core(h2pt),core(bipt),
     +  add(addpt(10)),add(addpt(4)),add(addpt(21)),
     +  core(h2xpt),qvv,uvv,d1,szh(10))
      if(szh(10).eq.0)go to 5220
      call bvavaf(core(bipt),uvv,core(h2xpt),d1,faa,add(addpt(21)))
      call wrthb(szh(10),core(bipt),10)
      if(flags(6))call pbvava(core(bipt))
c
5220  continue
c
c  integral block 9.  hessian block 9.
c    b(ap,ib) = -delta(ab)*f(ip)
c               +sum(t) (4(bi|at)-(ai|bt)-(ab|it))*d(pt)
c   core:bufh2,b,dx
c
      if(szh(9).eq.0)go to 5230
      dxpt=bipt+szh(9)
c     if(.not.flags(22))call bvavd(core(bipt),core(h2pt),d1,core(dxpt))
c     if (flags(22)) call wzero(szh(9),core(bipt),1)
c     call bvavdf(core(bipt),qad,fad)
         if (flags(22)) then
         call wzero(szh(9),core(bipt),1)
         else
         call bvavd(core(bipt),core(h2pt),d1,core(dxpt))
      if(flags(6))call prblkt('bvavd matrix post bvavd',
     +  core(bipt),nvat,nvat,nvdt,'(va)','(vd)',1,nlist)
         call bvavdf(core(bipt),qad,fad)
         endif
      if(flags(6))call prblkt('bvavd matrix',
     +  core(bipt),nvat,nvat,nvdt,'(va)','(vd)',1,nlist)
      call wrthb(szh(9),core(bipt),9)
c
5230  continue
c
c  integral block 10.  hessian block 4.
c    b(aj,ip)= -delta(ij)*(2*q(ap)-f(ap))
c              -4*(4(aj|ip)-(ai|pj)-(ap|ij))
c              +sum(t) 2*(4(aj|it)-(ai|jt)-(at|ij))*d(pt)
c   core:bufh2,b,dx
c
      if(szh(4).eq.0)go to 5240
      dxpt=bipt+szh(4)
c     if(.not.flags(22))call bvdad(core(bipt),core(h2pt),d1,core(dxpt))
c     if(flags(22)) call wzero(szh(4),core(bipt),1)
c     call bvdadf(core(bipt),qva,fva)
         if (flags(22)) then
         call wzero(szh(4),core(bipt),1)
         else
         call bvdad(core(bipt),core(h2pt),d1,core(dxpt))
         call bvdadf(core(bipt),qva,fva)
         endif
      if(flags(6))call prblkt('bvdad matrix',
     +  core(bipt),nadt,nadt,nvdt,'(ad)','(vd)',1,nlist)
      call wrthb(szh(4),core(bipt),4)
c
5240  continue
c
c  integral block 11.  hessian block 6.
c    b(ai,bj) = -delta(ab)*2*f(ij) +2*delta(ij)*q(ab)
c               +4*(4(ai|bj)-(bi|aj)-(ab|ij))
c   core:bufh2,b
c
      if(szh(6).eq.0)go to 5250
      call wzero(szh(6),core(bipt),1)
      if (.not.flags(22)) call bvdvd(core(bipt),qvv,fdd,
     &   add(addpt(20)),core(h2pt))
      call wrthb(szh(6),core(bipt),6)
      if(flags(6))call pbvdvd(core(bipt))
5250  continue
c
c  all hessian blocks have been constructed and written to htape.
c  fock matrices and density matrices have been calculated and
c  are returned in f*, q*, d1(*), and d2(*).
c
      if(flags(4))then
      call plblks('d1 matrix',
     +  d1,nsym,napsy,' amo',1,nlist)
      if(n2td.ne.0)call prblks('fdd matrix',
     +  fdd,nsym,ndpsy,ndpsy,' dmo',' dmo',1,nlist)
      if(nadt.ne.0)call prblks('fad matrix',
     +  fad,nsym,ndpsy,napsy,' dmo',' amo',1,nlist)
      if(n2ta.ne.0)call prblks('faa matrix',
     +  faa,nsym,napsy,napsy,' amo',' amo',1,nlist)
      if(nvdt.ne.0)call prblks('fvd matrix',
     +  fvd,nsym,nvpsy,ndpsy,' vmo',' dmo',1,nlist)
      if(nvat.ne.0)call prblks('fva matrix',
     +  fva,nsym,nvpsy,napsy,' vmo',' amo',1,nlist)
      if(nadt.ne.0)call prblks('qad matrix',
     +  qad,nsym,ndpsy,napsy,' dmo',' amo',1,nlist)
      call plblks('qaa matrix',
     +  qaa,nsym,napsy,' amo',1,nlist)
      if(nvat.ne.0)call prblks('qva matrix',
     +  qva,nsym,nvpsy,napsy,' vmo',' amo',1,nlist)
      if(n2tv.ne.0)call prblks('qvv matrix',
     +  qvv,nsym,nvpsy,nvpsy,' vmo',' vmo',1,nlist)
      endif
c
      if(flags(3))call timer('hbcon',4,event,nlist)
9999  return
      end
      subroutine madad(b,x,y,s,addad)
c
c  form the matrix-vector product  b * x  using the scratch
c  vector s and update the vector y.
c
c  y(ip) = y(ip) + sum(jq) b(ip,jq)*x(jq)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer tsymad(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*),x(nadt),y(nadt),s(*)
      integer addad(ndimd,ndima)
c
      pq=1
      do 400 p=1,nact
      psym=symx(ix0a+p)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 400
      ip=addad(ird1(psym),p)
c
      do 300 q=1,p
      qsym=symx(ix0a+q)
      nj=ndpsy(qsym)
      if(nj.eq.0)go to 300
      jq=addad(ird1(qsym),q)
c
c  form s(ip)=sum(j) b(j,i,pq)*x(jq)  and update y(*).
c  b(nj,ni), x(nj), s(ni).
c
      call mxma(b(pq),nj,1, x(jq),1,1, s,1,1, ni,nj,1)
c
      do 100 i=1,ni
100   y(ip-1+i)=y(ip-1+i)+s(i)
c
      if(p.eq.q)go to 300
c
c  form s(jq)=sum(i) b(j,i,pq)*x(ip)  and update y(*).
c  b(nj,ni), x(ni), s(nj).
c
      call mxma(b(pq),1,nj, x(ip),1,1, s,1,1, nj,ni,1)
c
      do 200 j=1,nj
200   y(jq-1+j)=y(jq-1+j)+s(j)
c
300   pq=pq+ni*nj
400   continue
c
      return
      end
      subroutine madva(b,x,y,s,addva,addad)
c
c  form the matrix-vector product  b * x using the scratch
c  vector s and update y.
c
c  y(ip) = y(ip) + sum(ar) b(ar,ip)*x(ar)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer tsymad(8),tsymva(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxy(1,26),tsymva(1),nvat)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*),x(*),y(*),s(*)
      integer addva(ndimv,ndima),addad(ndimd,ndima)
c
      pr=1
      do 400 p=1,nact
      psym=symx(ix0a+p)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 400
      ip=addad(ird1(psym),p)
c
      do 300 r=1,nact
      rsym=symx(ix0a+r)
      na=nvpsy(rsym)
      if(na.eq.0)go to 300
      ar=addva(irv1(rsym),r)
c
c  form s(ip)=sum(a) b(a,i,pr)*x(ar)  and update y(*).
c  b(na,ni), x(na), s(ni).
c
      call mxma(b(pr),na,1, x(ar),1,1, s,1,1, ni,na,1)
c
      do 100 i=1,ni
100   y(ip-1+i)=y(ip-1+i)+s(i)
c
      pr=pr+na*ni
300   continue
400   continue
c
      return
      end
      subroutine mfaa(faa,uaa,d1,h2,d2,off1,addaa,fx,hx,dx)
c
c  active-active block of the fock matrix.
c
c  faa(pq)= sum(t) uaa(pt)*d1(tq) + sum(tuv) h2(ptuv)*d2(qtuv)
c
c  where uaa(pt) is the effective 1-e hamiltonian matrix, h2(ptuv)
c  the 2-e integrals, and where d1(tq) and d2(qtuv) are the 1-
c  and 2-particle density matrices over the active orbitals.
c
c  floating point operations are actually performed with matrix
c  multiplications.  this requires gathering blocks of integrals
c  and density matrix elements into scratch arrays.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      real*8 faa(*),uaa(*),d1(*),h2(*),d2(*)
      real*8 fx(*),hx(*),dx(*)
      integer off1(*),addaa(*)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(12),n2ta)
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer smpt(8)
      real*8 uvfac
      real*8 zero,one,two
      parameter(zero=0d0,one=1d0,two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  1-e terms.  gather the lower-triangle-packed integrals and
c  density matrix elements to square-packed form.
c  if possible, matrix multiplication should take advantage
c  of sparsness in d(*).
c
      call xphd1(uaa,d1,hx,dx,napsy,nsym)
c
      do 300 psym=1,nsym
      np=napsy(psym)
      if(np.eq.0)go to 300
      i=n2sa(psym)+1
      call mxma(hx(i),1,np, dx(i),1,np, faa(i),1,np, np,np,np)
300   continue
c
c  2-e terms.
c
      uv=0
      do 1500 u=1,nact
      usym=symx(ix0a+u)
      do 1400 v=1,u
      uvsym=mult(symx(ix0a+v),usym)
      uv=uv+1
      uvfac=two
      if(u.eq.v)uvfac=one
c
      call wzero(n2ta,fx,1)
c
c  gather the (uv) block of integrals and density matrix elements
c  for matrix multiplications.
c  if possible, matrix multiplications should take advantage
c  of sparsness in d(*).
c  only tsym.ge.psym blocks are produced.
c
      call xphd2(h2,d2,hx,dx,uvsym,uv,off1,addaa,smpt)
c
      do 1300 psym=1,nsym
      np=napsy(psym)
      if(np.eq.0)go to 1300
      tsym=mult(psym,uvsym)
      nt=napsy(tsym)
      if(nt.eq.0)go to 1300
      if(psym-tsym)1000,1100,1200
1000  continue
c
c  tsym.gt.psym case.  form h * d(transpose) for h(np,nt), d(np,nt).
c
      ipt=smpt(tsym)
      ipq=n2sa(psym)+1
      call mxma(hx(ipt),1,np, dx(ipt),np,1, fx(ipq),1,np, np,nt,np)
      go to 1300
c
1100  continue
c
c  tsym.eq.psym case.  form h * d for h(np,np), d(np,np).
c
      ipt=smpt(tsym)
      ipq=n2sa(psym)+1
      call mxma(hx(ipt),1,np, dx(ipt),1,np, fx(ipq),1,np, np,np,np)
      go to 1300
c
1200  continue
c
c  tsym.lt.psym case.  form h(transpose) * d for h(nt,np), d(nt,np).
c
      ipt=smpt(psym)
      ipq=n2sa(psym)+1
      call mxma(hx(ipt),nt,1, dx(ipt),1,nt, fx(ipq),1,np, np,nt,np)
c
1300  continue
c
c  include fx(*) terms into faa(*) for this (uv).
c
      call daxpy_wr(n2ta,uvfac,fx,1,faa,1)
c
1400  continue
1500  continue
      if(flags(3))call timer('mfaa',4,event,nlist)
      return
      end
      subroutine mfad(fad,uad,d1,h2,d2,off1,addaa,fx,dx)
c
c  fad(*) matrix construction.
c
c  fad(ip)= sum(t) uad(it)*d1(pt) + sum(tuv) h2(ituv)*d2(ptuv)
c
c  where uad(it) is an effective 1-e hamiltonian matrix element,
c  h2(ituv) a 2-e integral, and where d1(pt) and d2(ptuv) are 1-
c  and 2-particle density matrix elements over the active orbitals.
c
c  floating point operations are actually performed with matrix
c  multiplications.  this requires gathering blocks of density
c  matrix elements into scratch arrays.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      real*8 fad(*),uad(*),d1(*),h2(*),d2(*)
      real*8 fx(*),dx(*)
      integer off1(*),addaa(*)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(10),nact)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer tsymad(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      integer nsad(8)
      equivalence (nxy(1,29),nsad(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer smpt(8)
      real*8 uvfac
      real*8 zero,one,two
      parameter(zero=0d0,one=1d0,two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  1-e terms.  gather d1 into a square-packed array
c  for efficient matrix operations.
c  if possible, matrix multiplication should take advantage
c  of sparsness in d(*).
c
      call xpnd1(d1,dx,napsy,nsym)
c
      np=0
      ni=0
      do 300 psym=1,nsym
      np=napsy(psym)
      if(np.eq.0)go to 300
      ni=ndpsy(psym)
      if(ni.eq.0)go to 300
c
      ip=nsad(psym)+1
      pt=n2sa(psym)+1
      call mxma(uad(ip),1,ni, dx(pt),1,np, fad(ip),1,ni, ni,np,np)
300   continue
c
c  2-e terms.  integral blocks are processed sequentially by
c  distribution and by symmetry within each distribution.
c
      ituv=1
      uv=0
      do 1500 u=1,nact
      usym=symx(ix0a+u)
      do 1400 v=1,u
      uv=uv+1
      uvsym=mult(symx(ix0a+v),usym)
      uvfac=two
      if(u.eq.v)uvfac=one
c
      call wzero(nadt,fx,1)
c
c  gather the (uv) block of the 2-particle denstiy matrix
c  for efficient matrix operations.
c  if possible, matrix multiplication should take advantage
c  of sparsness in d(*).
c  only tsym.ge.psym blocks are gathered.
c
      call xpnd2(d2,dx,uvsym,uv,off1,addaa,smpt)
c
      nt=0
      ni=0
      np=0
      do 1300 tsym=1,nsym
      nt=napsy(tsym)
      if(nt.eq.0)go to 1300
      psym=mult(tsym,uvsym)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 1300
      np=napsy(psym)
      if(np.eq.0)go to 1300
      if(tsym.le.psym)go to 1100
c
c  tsym.gt.psym case.  form h * d(transpose) for h(ni,nt), d(np,nt).
c
      ipt=smpt(tsym)
      ipq=nsad(psym)+1
      call mxma(h2(ituv),1,ni, dx(ipt),np,1, fx(ipq),1,ni, ni,nt,np)
      go to 1300
c
1100  continue
c
c  tsym.le.psym case.  form h * d for h(ni,nt), d(nt,np).
c
      ipt=smpt(psym)
      ipq=nsad(psym)+1
      call mxma(h2(ituv),1,ni, dx(ipt),1,nt, fx(ipq),1,ni, ni,nt,np)
c
1300  ituv=ituv+ni*nt
c
c  include fx(*) terms into fad(*) for this (uv).
c
      call daxpy_wr(nadt,uvfac,fx,1,fad,1)
c
1400  continue
1500  continue
c
      if(flags(3))call timer('mfad',4,event,nlist)
      return
      end
      subroutine mfva(fva,uva,d1,h2,d2,off1,addaa,fx,dx)
c
c  virtual-active block of the fock matrix.
c
c  fva(ap)= sum(t) uva(at)*d1(pt) + sum(tuv) h2(atuv)*d2(ptuv)
c
c  where uva(at) is an effective 1-e hamiltonian matrix element,
c  h2(atuv) a 2-e integral, and where d1(pt) and d2(ptuv) are 1-
c  and 2-particle density matrix elements over the active orbitals.
c
c  floating point operations are actually performed with matrix
c  multiplications.  this requires gathering blocks of density
c  matrix elements into scratch arrays.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      real*8 fva(*),uva(*),d1(*),h2(*),d2(*)
      real*8 fx(*),dx(*)
      integer off1(*),addaa(*)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(10),nact)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer nsva(8)
      equivalence (nxy(1,31),nsva(1))
      integer tsymva(8)
      equivalence (nxy(1,26),tsymva(1))
      equivalence (tsymva(1),nvat)
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer smpt(8)
      real*8 uvfac
      real*8 zero,one,two
      parameter(zero=0d0,one=1d0,two=2d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  1-e terms.  gather d1 into a square-packed array
c  for efficient matrix operations.
c  if possible, matrix multiplication should take advantage
c  of sparsness in d(*).
c
      call xpnd1(d1,dx,napsy,nsym)
c
      np=0
      na=0
      do 300 psym=1,nsym
      np=napsy(psym)
      if(np.eq.0)go to 300
      na=nvpsy(psym)
      if(na.eq.0)go to 300
c
      iap=nsva(psym)+1
      ipt=n2sa(psym)+1
      call mxma(uva(iap),1,na, dx(ipt),1,np, fva(iap),1,na, na,np,np)
300   continue
c
c  2-e terms.  integral blocks are processed sequentially by
c  distribution and by symmetry within each distribution.
c
      atuv=1
      uv=0
      do 1500 u=1,nact
      usym=symx(ix0a+u)
      do 1400 v=1,u
      uv=uv+1
      uvsym=mult(symx(ix0a+v),usym)
      uvfac=two
      if(u.eq.v)uvfac=one
c
      call wzero(nvat,fx,1)
c
c  gather the (uv) block of the 2-particle denstiy matrix
c  for efficient matrix operations.
c  if possible, matrix multiplication should take advantage
c  of sparsness in d(*).
c  only tsym.ge.psym blocks are gathered.
c
      call xpnd2(d2,dx,uvsym,uv,off1,addaa,smpt)
c
      nt=0
      na=0
      np=0
      do 1300 tsym=1,nsym
      nt=napsy(tsym)
      if(nt.eq.0)go to 1300
      psym=mult(tsym,uvsym)
      na=nvpsy(psym)
      if(na.eq.0)go to 1300
      np=napsy(psym)
      if(np.eq.0)go to 1300
      if(tsym.le.psym)go to 1100
c
c  tsym.gt.psym case.  form h * d(transpose) for h(na,nt), d(np,nt).
c
      ipt=smpt(tsym)
      ipq=nsva(psym)+1
      call mxma(h2(atuv),1,na, dx(ipt),np,1, fx(ipq),1,na, na,nt,np)
      go to 1300
c
1100  continue
c
c  tsym.le.psym case.  form h * d for h(na,nt), d(nt,np).
c
      ipt=smpt(psym)
      ipq=nsva(psym)+1
      call mxma(h2(atuv),1,na, dx(ipt),1,nt, fx(ipq),1,na, na,nt,np)
c
1300  atuv=atuv+na*nt
c
c  include fx(*) terms into fva(*) for this (uv).
c
      call daxpy_wr(nvat,uvfac,fx,1,fva,1)
c
1400  continue
1500  continue
      if(flags(3))call timer('mfva',4,event,nlist)
      return
      end
      subroutine mfvd(fvd,uvd,hd,hx,d1,off3,off4)
c
c  fvd (=qvd) matrix construction.
c
c  fvd(ai) = 2*uvd(ai) + sum  ( 2*(ai|uv) - (au|iv) )*d1(uv)
c                        (uv)
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxtot(18),ndima)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 fvd(*),uvd(*),hd(*),hx(*),d1(*)
      integer off3(*),off4(ndima,ndima)
c
      real*8 zero,two,four,term
      parameter(zero=0d0,two=2d0,four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  include 2*uvd() terms.
c
      call dcopy_wr(nvdt,uvd,1,fvd,1)
      call dscal_wr(nvdt,two,fvd,1)
c
c  2-e integral terms are computed as:
c    sum ( 4*(ai|uv)-(au|iv)-(av|iu) )*duv
c   (u>v)
c
c   +sum ( 2*(ai|uu)-(au|iu) )*duu
c    (u)
c
      uv1=0
      do 500 usym=1,nsym
      if(napsy(usym).eq.0)go to 500
      u1=ira1(usym)
      u2=ira2(usym)
c
      do 400 u=u1,u2
      do 300 v=u1,u
      uv1=uv1+1
      term=d1(uv1)
      if(term.eq.zero)go to 300
      uv=nndx(u)+v
      if(u.ne.v)then
c
c  u .ne. v terms:
c
      aiuv=off3(uv)+1
      auiv=off4(u,v)+1
      aviu=off4(v,u)+1
      do 100 ai=1,nvdt
      fvd(ai)=fvd(ai)+term*
     +  (four*hd(aiuv-1+ai)-hx(auiv-1+ai)-hx(aviu-1+ai))
100   continue
c
      else
c
c  u .eq. v terms:
c
      aiuu=off3(uv)+1
      auiu=off4(u,u)+1
      do 200 ai=1,nvdt
200   fvd(ai)=fvd(ai)+term*(two*hd(aiuu-1+ai)-hx(auiu-1+ai))
c
      endif
c
300   continue
400   continue
500   continue
c
      if(flags(3))call timer('mfvd',4,event,nlist)
      return
      end
      subroutine mqaa(qaa,uaa,h,d,off1,addaa)
c
c  construct qaa(*).  qaa(*) is stored triangular-packed by symmetry.
c
c    qaa(pr) = 2*uaa(pr) + sum(uv) (2*(pr|uv) - (pu|rv))*d(uv)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(11),nnta)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 qaa(*),uaa(*),h(*),d(*)
      integer off1(*),addaa(*)
c
      real*8 duv,zero,two,four
      parameter (zero=0d0, two=2d0, four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  2*u(pr) contributions:
c
      call dcopy_wr(nnta,uaa,1,qaa,1)
      call dscal_wr(nnta,two,qaa,1)
c
c  super-matrix contributions:
c
      uv1=0
      do 430 usym=1,nsym
      if(napsy(usym).eq.0)go to 430
      u1=ira1(usym)
      u2=ira2(usym)
c
      do 420 u=u1,u2
c
      do 410 v=u1,u
      uv1=uv1+1
      duv=d(uv1)
      if(duv.eq.zero)go to 410
      uv=nndx(u)+v
      if(u.eq.v)go to 200
c
c  u.ne.v case:
c    q(pr)=sum(uv) (...)
c         =sum(u.gt.v) (4*(pr|uv)-(pu|rv)-(pv|ru))*d(uv)
c
      pr1=0
      do 130 psym=1,nsym
      if(napsy(psym).eq.0)go to 130
      p1=ira1(psym)
      p2=ira2(psym)
c
      do 120 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
      pv=nndx(max(p,v))+min(p,v)
c
      do 110 r=p1,p
      pr1=pr1+1
      ru=nndx(max(r,u))+min(r,u)
      rv=nndx(max(r,v))+min(r,v)
      pr=nndx(p)+r
      pruv=off1(max(pr,uv))+addaa(min(pr,uv))
      purv=off1(max(pu,rv))+addaa(min(pu,rv))
      pvru=off1(max(pv,ru))+addaa(min(pv,ru))
c
      qaa(pr1)=qaa(pr1)+(four*h(pruv)-h(purv)-h(pvru))*duv
c
110   continue
120   continue
130   continue
      go to 410
c
200   continue
c
c  u.eq.v case:
c
      pr1=0
      do 230 psym=1,nsym
      if(napsy(psym).eq.0)go to 230
      p1=ira1(psym)
      p2=ira2(psym)
c
      do 220 p=p1,p2
      pu=nndx(max(p,u))+min(p,u)
c
      do 210 r=p1,p
      pr1=pr1+1
      rv=nndx(max(r,v))+min(r,v)
      pr=nndx(p)+r
      pruv=off1(max(pr,uv))+addaa(min(pr,uv))
      purv=off1(max(pu,rv))+addaa(min(pu,rv))
c
      qaa(pr1)=qaa(pr1)+(two*h(pruv)-h(purv))*duv
c
210   continue
220   continue
230   continue
c
410   continue
420   continue
430   continue
c
      if(flags(3))call timer('mqaa',4,event,nlist)
c
      return
      end
      subroutine mqad(qad,uad,h,d,off2,addad,dx,qx)
c
c  construct qad(*).  qad(*) is stored rectangular-packed by symmetry.
c
c    qad(ip) = 2*uad(ip) + sum(uv) (2*(ip|uv) - (iu|pv))*d(uv)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(10),nact)
      integer tsymad(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 qad(*),uad(*),h(*),d(*),dx(*),qx(*)
      integer off2(*),addad(ndimd,ndima)
c
      real*8 term,zero,one,two,four
      parameter (zero=0d0, one=1d0, two=2d0, four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  2*u(ip) contributions:
c
      call dcopy_wr(nadt,uad,1,qad,1)
      call dscal_wr(nadt,two,qad,1)
c
c  direct-type contributions:
c    qad(ip) = sum(uv) 2*(ip|uv) * d(uv)
c
      uv1=0
      do 130 usym=1,nsym
      if(napsy(usym).eq.0)go to 130
      u1=ira1(usym)
      u2=ira2(usym)
c
      do 120 u=u1,u2
c
      do 110 v=u1,u
      uv1=uv1+1
      if(d(uv1).eq.zero)go to 110
      term=four*d(uv1)
      if(u.eq.v)term=two*d(uv1)
c
      ip=off2(nndx(u)+v)+1
      call daxpy_wr(nadt,term,h(ip),1,qad,1)
c
110   continue
120   continue
130   continue
c
c  exchange-type contributions:
c    qad(ip) = sum(uv) -(iu|pv) * d(uv)
c
c  expand d(*) to square-packed for efficient matrix operations.
c  contributions are calculated as a series of matrix-vector products.
c
      call xpnd1(d,dx,napsy,nsym)
c
      i1p=1
      do 240 p=1,nact
      isym=symx(ix0a+p)
      ni=ndpsy(isym)
      if(ni.eq.0)go to 240
      i1=ird1(isym)
c
      u1v=1
      do 230 v=1,nact
      usym=symx(ix0a+v)
      nu=napsy(usym)
      u1=ira1(usym)
      pv=nndx(max(p,v))+min(p,v)
c
      iupv=off2(pv)+addad(i1,u1)
      call mxma(h(iupv),1,ni, dx(u1v),1,1, qx,1,1, ni,nu,1)
      call daxpy_wr(ni,-one,qx,1,qad(i1p),1)
c
230   u1v=u1v+nu
240   i1p=i1p+ni
c
      if(flags(3))call timer('mqad',4,event,nlist)
c
      return
      end
      subroutine mqva(qva,uva,h,d,off5,addva,dx,qx)
c
c  construct qva(*).  qva(*) is stored rectangular-packed by symmetry.
c
c    qva(ap) = uva(ap) + sum(uv) (2*(ap|uv) - (au|pv))*d(uv)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      integer tsymva(8)
      equivalence (nxy(1,26),tsymva(1),nvat)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 qva(*),uva(*),h(*),d(*),dx(*),qx(*)
      integer off5(*),addva(ndimv,ndima)
c
      real*8 term,zero,one,two,four
      parameter (zero=0d0, one=1d0, two=2d0, four=4d0)
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  2*u(ap) contributions:
c
      call dcopy_wr(nvat,uva,1,qva,1)
      call dscal_wr(nvat,two,qva,1)
c
c  direct-type contributions:
c    qva(ap) = sum(uv) 2*(ap|uv) * d(uv)
c
      uv1=0
      do 130 usym=1,nsym
      if(napsy(usym).eq.0)go to 130
      u1=ira1(usym)
      u2=ira2(usym)
c
      do 120 u=u1,u2
c
      do 110 v=u1,u
      uv1=uv1+1
      if(d(uv1).eq.zero)go to 110
      term=four*d(uv1)
      if(u.eq.v)term=two*d(uv1)
c
      ap=off5(nndx(u)+v)+1
      call daxpy_wr(nvat,term,h(ap),1,qva,1)
c
110   continue
120   continue
130   continue
c
c  exchange-type contributions:
c    qva(ap) = sum(uv) -(au|pv) * d(uv)
c
c  expand d(*) to square-packed for efficient matrix operations.
c  contributions are calculated as a series of matrix-vector products.
c
      call xpnd1(d,dx,napsy,nsym)
c
      a1p=1
      do 240 p=1,nact
      asym=symx(ix0a+p)
      nv=nvpsy(asym)
      if(nv.eq.0)go to 240
      a1=irv1(asym)
c
      u1v=1
      do 230 v=1,nact
      usym=symx(ix0a+v)
      na=napsy(usym)
      u1=ira1(usym)
      pv=nndx(max(p,v))+min(p,v)
c
      aupv=off5(pv)+addva(a1,u1)
      call mxma(h(aupv),1,nv, dx(u1v),1,1, qx,1,nv, nv,na,1)
      call daxpy_wr(nv,-one,qx,1,qva(a1p),1)
c
230   u1v=u1v+na
240   a1p=a1p+nv
c
      if(flags(3))call timer('mqva',4,event,nlist)
c
      return
      end
      subroutine mvaad(b,x,y,s,addva,addad)
c
c  form the matrix-vector product b * x using the scratch
c  vector s and update y.
c
c  y(ar) = y(ar) + sum(ip) b(ar,ip)*x(ip)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer tsymad(8),tsymva(8)
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxy(1,26),tsymva(1),nvat)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*),x(nvat),y(nadt),s(*)
      integer addva(ndimv,ndima),addad(ndimd,ndima)
c
      pr=1
      do 400 p=1,nact
      psym=symx(ix0a+p)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 400
      ip=addad(ird1(psym),p)
c
      do 300 r=1,nact
      rsym=symx(ix0a+r)
      na=nvpsy(rsym)
      if(na.eq.0)go to 300
      ar=addva(irv1(rsym),r)
c
c  form s(ar)=sum(i) b(a,i,pr)*x(ip)  and update y(*).
c  b(na,ni), x(ni), s(na).
c
      call mxma(b(pr),1,na, x(ip),1,1, s,1,1, na,ni,1)
c
      do 100 a=1,na
100   y(ar-1+a)=y(ar-1+a)+s(a)
c
      pr=pr+na*ni
300   continue
400   continue
c
      return
      end
      subroutine mvava(b,x,y,s,addva)
c
c  form the matrix-vector product  b * x using the scratch
c  vector s and update y.
c
c  y(ap) = y(ap) + sum(cr) b(ap,cr)*x(cr)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer tsymva(8)
      equivalence (nxy(1,26),tsymva(1),nvat)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*),x(nvat),y(nvat),s(*)
      integer addva(ndimv,ndima)
c
      pq=1
      do 400 p=1,nact
      psym=symx(ix0a+p)
      na=nvpsy(psym)
      if(na.eq.0)go to 400
      ap=addva(irv1(psym),p)
c
      do 300 r=1,p
      rsym=symx(ix0a+r)
      nc=nvpsy(rsym)
      if(nc.eq.0)go to 300
      cr=addva(irv1(rsym),r)
c
c  form s(ap)=sum(c) b(c,a,pr)*x(cr)  and update y(*).
c  b(nc,na), x(nc), s(na).
c
      call mxma(b(pq),nc,1, x(cr),1,1, s,1,1, na,nc,1)
c
      do 100 a=1,na
100   y(ap-1+a)=y(ap-1+a)+s(a)
c
      if(p.eq.r)go to 300
c
c  form s(cr)=sum(a) b(c,a,pr)*x(ap)  and update y(*).
c  b(nc,na), x(na), s(nc).
c
      call mxma(b(pq),1,nc, x(ap),1,1, s,1,1, nc,na,1)
c
      do 200 c=1,nc
200   y(cr-1+c)=y(cr-1+c)+s(c)
c
300   pq=pq+na*nc
400   continue
c
      return
      end
      subroutine mvdvd1(b,x,y,s,addvd)
c
c  form the matrix-vector product b * x using the scratch
c  vector s and update y.
c
c  y(ai) = y(ai) + sum(cj) b(ai,cj)*x(cj)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(7),ndot)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer irv1(8),irv2(8)
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(19),ndimv)
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d)
c
      real*8 b(*),x(nvdt),y(nvdt),s(*)
      integer addvd(ndimv,ndimd)
c
      ij=1
      do 400 i=1,ndot
      isym=symx(ix0d+i)
      na=nvpsy(isym)
      if(na.eq.0)go to 400
      ai=addvd(irv1(isym),i)
c
      do 300 j=1,i
      jsym=symx(ix0d+j)
      nc=nvpsy(jsym)
      if(nc.eq.0)go to 300
      cj=addvd(irv1(jsym),j)
c
c  form s(ai)=sum(c) b(c,a,ij)*x(cj)  and update y(*).
c  b(nc,na), x(nc), s(na).
c
      call mxma(b(ij),nc,1, x(cj),1,1, s,1,1, na,nc,1)
c
      do 100 a=1,na
100   y(ai-1+a)=y(ai-1+a)+s(a)
c
      if(i.eq.j)go to 300
c
c  form s(cj)=sum(a) b(c,a,ij)*x(ai)  and update y(*).
c  b(nc,na), x(na), s(nc).
c
      call mxma(b(ij),1,nc, x(ai),1,1, s,1,1, nc,na,1)
c
      do 200 c=1,nc
200   y(cj-1+c)=y(cj-1+c)+s(c)
c
300   ij=ij+na*nc
400   continue
c
      return
      end
      subroutine pbadad(b)
c
c  print the badad hessian block.
c  matrix is stored as a sequence (1...naa) of rectangular
c  matrices indexed by the inactive-orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxtot(10),nact)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      ijpr=1
      do 200 p=1,nact
      psym=symx(ix0a+p)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 200
      i0=nsd(psym)
      do 100 r=1,p
      rsym=symx(ix0a+r)
      nj=ndpsy(rsym)
      if(nj.eq.0)go to 100
      j0=nsd(rsym)
c
      write(nlist,6010)p,r
      call prblk(b(ijpr),nj,nj,ni,j0,i0,'  j=','  i=',1,nlist)
c
100   ijpr=ijpr+ni*nj
200   continue
      return
6010  format(/t10,'b(ip,jr) (p=',i3,',r=',i3,')')
6020  format(/10x,'badad matrix')
      end
      subroutine pbvaad(b)
c
c  print the bvaad hessian block.
c  matrix is stored as a sequence (1...na*na) of rectangular
c  matrices indexed by the virtual and inactive orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      airp=1
      do 200 p=1,nact
      psym=symx(ix0a+p)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 200
      i0=nsd(psym)
      do 100 r=1,nact
      rsym=symx(ix0a+r)
      na=nvpsy(rsym)
      if(na.eq.0)go to 100
      a0=nsv(rsym)
c
      write(nlist,6010)p,r
      call prblk(b(airp),na,na,ni,a0,i0,'  a=','  i=',1,nlist)
c
100   airp=airp+na*ni
200   continue
      return
6010  format(/t10,'b(ar,ip) (p=',i3,',r=',i3,')')
6020  format(/10x,'bvaad matrix')
      end
      subroutine pbvava(b)
c
c  print the bvava hessian block.
c  matrix is stored as a sequence (1...naa) of rectangular
c  matrices indexed by the virtual orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      capr=1
      do 200 p=1,nact
      psym=symx(ix0a+p)
      na=nvpsy(psym)
      if(na.eq.0)go to 200
      a0=nsv(psym)
      do 100 r=1,p
      rsym=symx(ix0a+r)
      nc=nvpsy(rsym)
      if(nc.eq.0)go to 100
      c0=nsv(rsym)
c
      write(nlist,6010)p,r
      call prblk(b(capr),nc,nc,na,c0,a0,'  c=','  a=',1,nlist)
c
100   capr=capr+na*nc
200   continue
      return
6010  format(/t10,'b(ap,cr) (p=',i3,',r=',i3,')')
6020  format(/10x,'bvava matrix')
      end
      subroutine pbvdvd(b)
c
c  print bvdvd hessian block.
c  matrix is stored as a sequence (1...ndd) of rectangular
c  matrices indexed by the virtual orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(7),ndot)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      caij=1
      do 200 i=1,ndot
      isym=symx(ix0d+i)
      na=nvpsy(isym)
      if(na.eq.0)go to 200
      a0=nsv(isym)
      do 100 j=1,i
      jsym=symx(ix0d+j)
      nc=nvpsy(jsym)
      if(nc.eq.0)go to 100
      c0=nsv(jsym)
c
      write(nlist,6010)i,j
      call prblk(b(caij),nc,nc,na,c0,a0,'  c=','  a=',1,nlist)
c
100   caij=caij+na*nc
200   continue
      return
6010  format(/t10,'b(ai,cj) (i=',i3,',j=',i3,')')
6020  format(/10x,'bvdvd matrix')
      end
      subroutine wcon(fad,qad,faa,fvd,fva,
     +  iorder,nadt,naar,nvdt,nvat,ndimw,w,wnorm)
c
c  construct the gradient vector, w(*).  the elements are the
c  generalized brillouin theorem terms:
c
c  w(xy) = i*<mc| [h,t(xy)] |mc>
c        = <mc| [h, e(yx)-e(xy)] |mc>
c        = 2*<mc| h (e(yx)-e(xy)) |mc>
c        = 2*( f(yx) - f(xy) )
c
c  where e(xy) is a generator of the unitary group.
c
c  the vector is constructed using the last form
c  where the fock matrices involving various orbital types have
c  been separately computed.
c
c  w(ip) = 2 * ( fad(ip) - qad(ip) )
c
c  w(pq) = 2 * ( faa(qp) - faa(pq) )
c
c  w(ai) = 2 * fvd(ai)
c
c  w(ap) = 2 * fva(ap)
c
c  written by ron shepard.
c  version date: 08-mar-85
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,16),n2sa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
        parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 fad(*),qad(*),faa(*),fvd(*),fva(*)
      real*8 w(ndimw),wnorm
      integer iorder(*)
c
      real*8 dnrm2_wr
      real*8 two
      parameter (two=2d0)
c
c  active-inactive terms.
c
      i0=0
      do 10 ip=1,nadt
10    w(i0+ip)=fad(ip)-qad(ip)
c
c  active-active terms.
c
      i0=i0+nadt
      do 40 psym=1,nsym
      np=napsy(psym)
      if(np.eq.0)go to 40
      p0=nsa(psym)
      pq0=n2sa(psym)
      p1=ira1(psym)
      p2=ira2(psym)
      do 30 p=p1,p2
      do 20 q=p1,p
      ipq=iorder(nndx(p)+q)
      if(ipq.ne.0)then
         pq=pq0+(q-p0-1)*np+(p-p0)
         qp=pq0+(p-p0-1)*np+(q-p0)
         w(i0+ipq)=faa(qp)-faa(pq)
      endif
20    continue
30    continue
40    continue
c
c  virtual-inactive terms.
c
      i0=i0+naar
      if(nvdt.ne.0)call dcopy_wr(nvdt,fvd,1,w(i0+1),1)
c
c  virtual-active terms.
c
      i0=i0+nvdt
      if(nvat.ne.0)call dcopy_wr(nvat,fva,1,w(i0+1),1)
c
c  include the factor of two and compute the norm.
c
      call dscal_wr(ndimw,two,w,1)
      wnorm=dnrm2_wr(ndimw,w,1)
c
      if(flags(4))write(nlist,6010)wnorm,w
6010  format(/' wnorm=',1pe12.4,'  w(*)='/(1x,10e12.4))
c
      return
      end
