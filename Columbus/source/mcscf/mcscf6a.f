!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf6a.f
cmcscf part=6_old of 10.  integral transformation routines.
cversion=5.6 last modified: 16-jan-2001
c  these routines will be substantially rewritten, or replaced, in the
c  future.  porting changes should be kept minimal and local to this
c  file. -rls
c deck trmain
      subroutine trmain_o(core,iadd,icore,nblock,nsym,nbft,
     +  sortfg,flag,kbufad,aointf)
c
c  core=workspace.
c  iadd=address of symmetry packed orbital coefficients
c  icore=amount of core available in working precision words
c  nblock=number of symmetry blocks (same as nsym)
c  nsym=number of irreps
c  nbft=total number of basis functions
c  sortfg=flag to determine if the basis function integrals require
c         sorting
c  flag=flag to determine whether to sort or to transform integrals
c  kbufad=address of first available space in core(*) for transformation
c
      implicit integer(a-z)
c

ctm  start
      integer nbfmxp
      parameter (nbfmxp=1023)
      integer ninfo
      parameter (ninfomx=5)
       integer nenrgymx
      parameter (nenrgymx=5)
       integer nmapmx
       parameter (nmapmx=2)
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx)
     . ,slabelao, bfnlabao
ctm end

      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)

      character*8 fordint
      integer iordint,imc1,imc2,mcirc
      logical lexist


      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
c
      real*8 core(icore)
      real*8 thresh
      real*8 valbuf
c
      dimension length(14)
      integer ijsmix(36),klsmix(36),ismtyp(106)
      dimension nsybk(8),mult(8,8)
c
      common /acracy/ thresh
c
      common /blocks/ nbksym(8),nbknbf(8),nbkorb(8),nbfoff(8),
     1   nnbfof(8),nbf2of(8),orbbff(8),symnbk(8),nfrzc(8),orboff(8)
      save blocks
c
c  /buf1/ is used for buffering input integrals and transformed output
c  integrals.
      real*8 cmbuf1, vbuf
c  NOTE, there is a dependency of the buffer sizes to 8bit/16bit
c        packing mode and initial record size of the AO integral file
c        n2ebf(16bit) < n2ebf(8bit)!
c        should be changed!

       parameter(len2e=4096, n2ebf=2730, nipv=4)
      common /buf1/ cmbuf1(len2e),vbuf(n2ebf),lbuf(nipv,n2ebf),
     & inout,ntotot,nrec,trinto
c
c  the length of valbuf(*) should be length(1).
c
      common /buf2/ valbuf(1610)
c
      common /imsort/ ihrpim(200),indpsg(200),imdgsg(200)
c
      common /indces/ isoff,jsoff,ksoff,lsoff,intlbl(4)
c
      common /indx/ iim1(nmotx)
c
      common /op/ traop(5)
c
      common /otrb/ iotorb(nmotx)
c
      common /save/ iorb(nmotx),icd(10),start(106),incore(106),incr,
     &              outcr
c
      common/sort/irecpt(200),ioff(200),ifull(200),
     +  ibufad,iword,nbuk,numel,npbuk,irec
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      data ijsmix/
     + 0, 1, 2, 4, 5, 6, 9,11,13,15,
     +19,20,21,22,23,28,30,32,34,36,
     +39,45,48,51,54,57,60,63,70,74,
     +78,82,86,90,94,98/
      data klsmix/
     + 1, 1, 2, 1, 1, 3, 2, 2, 2, 4,
     + 1, 1, 1, 1, 5, 2, 2, 2, 2, 3,
     + 6, 3, 3, 3, 3, 3, 3, 7, 4, 4,
     + 4, 4, 4, 4, 4, 8/
      data ismtyp/
     + 1, 3, 2, 1, 3, 3, 2, 2, 1, 4,
     + 3, 4, 3, 4, 3, 2, 2, 2, 1, 3,
     + 3, 3, 3, 2, 2, 2, 2, 1, 4, 3,
     + 4, 3, 4, 3, 4, 3, 4, 4, 3, 2,
     + 2, 2, 2, 2, 1, 4, 4, 3, 4, 4,
     + 3, 4, 4, 3, 4, 4, 3, 4, 4, 3,
     + 4, 4, 3, 2, 2, 2, 2, 2, 2, 1,
     + 4, 4, 4, 3, 4, 4, 4, 3, 4, 4,
     + 4, 3, 4, 4, 4, 3, 4, 4, 4, 3,
     + 4, 4, 4, 3, 4, 4, 4, 3, 2, 2,
     + 2, 2, 2, 2, 2, 1/
c
      data length /1610,796,524,389,307,253,214,185,162,144,
     1   130,117,107,98/
c
      data nsybk /1,4,0,19,0,0,0,106/
c
      data mult /1,2,3,4,5,6,7,8,
     a           2,1,4,3,6,5,8,7,
     b           3,4,1,2,7,8,5,6,
     c           4,3,2,1,8,7,6,5,
     d           5,6,7,8,1,2,3,4,
     e           6,5,8,7,2,1,4,3,
     f           7,8,5,6,3,4,1,2,
     g           8,7,6,5,4,3,2,1/
c
c      data aointf,trintf,scrtfl /1,4,34/
      data trintf,scrtfl /18,34/
c
      if(flag.ne.0) go to 1425
c
c...............................files.................................
c
c  aointf    : ao integral file from pitzer's integral program
c
c  trintf    : final transformed integral file
c
c  scrtfl    : fortran scratch file 1
c
c  da1       : direct access file 1
c
c  da2       : direct access file 2
c
c......................................................................
c
c
c  threshhold value  (thresh = 1.0d-12)
c
      thresh=1.0d-12
c
      norbt=nbft
c
c  calculate iorb
c
      ij=0
      do 155 i=1,nblock
      nbf=nbknbf(i)
      do 154 j=1,nbf
      ij=ij+1
      iorb(ij)=j
 154  continue
 155  continue
c
c  check validity of nsym
c
      if(nsym.eq.1.or.nsym.eq.2.or.nsym.eq.4.or.nsym.eq.8) go to 170
      write(nlist,160) nsym
 160  format(//' program error : nsym is not valid :'/
     1'  nsym = ',i10,' which is not 1,2,4,or 8.'//
     2' program stops execution.')
      call bummer('trmain: nsym=',nsym,faterr)
 170  continue
c
c  set traop
c
      do 180 i=1,5
      traop(i)=0
 180  continue
c
      do 230 ibk=1,nblock
      symnbk(nbksym(ibk))=ibk
 230  continue
      ibf=0
c
      do 280 ibk=1,nblock
      nbfbk=nbknbf(ibk)
c
      do 270 ibkbf=1,nbfbk
      ibf=ibf+1
      if(iorb(ibf)) 250,255,260
c
 250  if(traop(1).ge.1) write(nlist,251) ibk,ibkbf
 251  format(' block ',i2,' orbital ',i4,' is a frozen core orbital.')
      nfrzc(ibk)=nfrzc(ibk)+1
      go to 270
c
 255  if(traop(1).ge.1) write(nlist,256) ibk,ibkbf
 256  format(' block ',i2,' orbital ',i4,' is a frozen virtual',
     1' orbital.')
      go to 270
c
 260  if(traop(1).ge.1) write(nlist,261) ibk,ibkbf,iorb(ibf)
 261  format(' block ',i2,' orbital ',i4,' has index ',i4,' .')
 270  continue
 280  continue
c
c  calculate block offsets for integral labels
c
      iim1(1)=0
      do 360 ibf=2,nbft
      iim1(ibf)=iim1(ibf-1)+ibf-1
 360  continue
c
      if(nblock.eq.1) go to 390
c
      do 370 ibk=2,nblock
      nbfbk=nbknbf(ibk-1)
      nbfoff(ibk)=nbfoff(ibk-1)+nbfbk
      nnbfof(ibk)=nnbfof(ibk-1)+nbfbk*(nbfbk+1)/2
      nbf2of(ibk)=nbf2of(ibk-1)+nbfbk**2
      orboff(ibk)=orboff(ibk-1)+nbkorb(ibk-1)
 370  continue
c
      do 380 ibk=2,nblock
      orbbff(ibk)=orbbff(ibk-1)+nbkorb(ibk-1)*nbknbf(ibk-1)
 380  continue
c
 390  continue
c
c  determine space available for transforming integrals
c
c
c...reduced transformation matrix (rtr)
      icd(1)=iadd
      sprtr=orbbff(nblock)+nbkorb(nblock)*nbknbf(nblock)
c
c...beginning of space for transformation
      icd(2)=kbufad
c
c...total required
      itotal = icd(2)
c
c...avcore - available core for transforming integrals
      avcore=icore-icd(2)-10
c
c...sizesg - size of space for ao integral list segments.
c            maximum da record length is length(1).
      sizesg=avcore-length(1)
c
c  check to make sure space is available
c
      if(itotal.lt.icore-1) go to 400
c
      write(nlist,396) (nbksym(i),i=1,8),(nbknbf(i),i=1,8),
     1  (nbkorb(i),i=1,8),(nbfoff(i),i=1,8),
     2   (nnbfof(i),i=1,8),(nbf2of(i),i=1,8),
     3   (orbbff(i),i=1,8)
 396  format(//' nbksym :',8i8/' nbknbf :',8i8,/
     1' nbkorb :',8i8/' nbfoff :',8i8/
     2' nnbfof :',8i8/' nbf2of :',8i8/' orbbff :',8i8/)
      write(nlist,395) icore,(icd(i),i=1,2),itotal
 395  format(//' >>>>>> not enough core available <<<<<<'//
     1' icore = ',i10/' core divided by ',6i10/
     2' total needed is ',i10///
     3' >>>>>>>>>>program stops execution<<<<<<<<<<')
      call bummer('trmain: itotal=',itotal,faterr)
 400  continue
c      b1=rclok1(1.)-b0
c      write(nlist,405) b1
c 405  format(//' transformation preparation required ',f10.4,
c     1' seconds.'//)
c      b2=rclok1(1.)
c
c  initialize start(*) and incore(*) arrays
c
      nsymbk=nsybk(nsym)
      do 420 isymbk=1,nsymbk
         start(isymbk)=0
         incore(isymbk)=0
 420  continue
c
c  determine the number of ao and mo integrals for each
c  symmetry block
c
c  if the symmetry block can be transformed in core:
c     1.) incore(isymbk)=-1
c     2.) start(isymbk) = the starting position of this symmetry
c         block of ao integrals in the ao ordered list for the
c         incore transformations.
c
c  if the symmetry block cannot be transformed in core:
c     1.) incore(isymbk) = number of ao integrals per segment
c     2.) start(isymbk) = the starting segment number (bin number) for
c         this block of integrals relative to the first segment of ao
c         out-of-core integrals.
c
      if(traop(1).ge.1) write(nlist,425)
 425  format(/' statistics for non-zero blocks of integrals'//
     a'  iblock  isym jsym ksym lsym  itype     incore    start',
     b'     num. ao. ints     needed core for incore trans'//)
      naopos=0
      incr=0
      outcr=0
      outseg=0
      do 800 isym=1,nsym
      if(nbknbf(symnbk(isym)).eq.0) go to 800
      isbf=nbknbf(symnbk(isym))
      isob=nbkorb(symnbk(isym))
c
      do 780 jsym=1,isym
      if(nbknbf(symnbk(jsym)).eq.0) go to 780
      jsbf=nbknbf(symnbk(jsym))
      jsob=nbkorb(symnbk(jsym))
      ijsym=mult(isym,jsym)
      ij=iim1(isym)+jsym
c
      do 760 ksym=1,isym
      if(nbknbf(symnbk(ksym)).eq.0) go to 760
      ksbf=nbknbf(symnbk(ksym))
      ksob=nbkorb(symnbk(ksym))
      lsymx=ksym
      if(isym.eq.ksym) lsymx=jsym
c
      do 740 lsym=1,lsymx
      klsym=mult(ksym,lsym)
      if(ijsym.ne.klsym) go to 740
      if(nbknbf(symnbk(lsym)).eq.0) go to 740
      lsbf=nbknbf(symnbk(lsym))
      lsob=nbkorb(symnbk(lsym))
      kl=iim1(ksym)+lsym
      iblock=ijsmix(ij)+klsmix(kl)
      itype=ismtyp(iblock)
      go to (510,520,530,540),itype
c
c  type (aa:aa)
c
 510  spincr=(isob**4+6*isob**3+11*isob**2+6*isob-1)/8+1
      if(spincr.lt.avcore) go to 515
      nszdis=isbf*(isbf+1)/2
      naoint=nszdis*nszdis
      ndpseg=sizesg/nszdis-1
      incore(iblock)=nszdis*ndpseg
      nseg=(naoint-1)/incore(iblock)+1
      start(iblock)=outseg
      outseg=outseg+nseg
      outcr=outcr+1
      go to 730
 515  naoint=(isbf**4+2*isbf**3+3*isbf**2+2*isbf-1)/8+1
      incore(iblock)=-1
      start(iblock)=naopos
      naopos=naopos+naoint
      incr=incr+1
      go to 730
c
c  type (aa:bb)
c
 520  spincr=(isob+2)*(isob+1)*ksob*(ksob+1)/4
      if(spincr.lt.avcore) go to 525
      naoint=isbf*(isbf+1)*ksbf*(ksbf+1)/4
      nszdis=ksbf*(ksbf+1)/2
      ndpseg=sizesg/nszdis-1
      incore(iblock)=nszdis*ndpseg
      nseg=(naoint-1)/incore(iblock)+1
      start(iblock)=outseg
      outseg=outseg+nseg
      outcr=outcr+1
      go to 730
 525  naoint=isbf*(isbf+1)*ksbf*(ksbf+1)/4
      incore(iblock)=-1
      start(iblock)=naopos
      naopos=naopos+naoint
      incr=incr+1
      go to 730
c
c  type (ab:ab)
c
 530  spincr=(isob+2)*(jsob+1)*isob*jsob/2
      if(spincr.lt.avcore) go to 535
      naoint=(isbf*jsbf)**2
      nszdis=isbf*jsbf
      ndpseg=sizesg/nszdis-1
      incore(iblock)=nszdis*ndpseg
      nseg=(naoint-1)/incore(iblock)+1
      start(iblock)=outseg
      outseg=outseg+nseg
      outcr=outcr+1
      go to 730
 535  naoint=isbf*jsbf*(isbf*jsbf+1)/2
      incore(iblock)=-1
      start(iblock)=naopos
      naopos=naopos+naoint
      incr=incr+1
      go to 730
c
c  type (ab:cd)
c
 540  spincr=((isob+1)*jsob+1)*ksob*lsob
      if(spincr.lt.avcore) go to 545
      naoint=isbf*jsbf*ksbf*lsbf
      nszdis=ksbf*lsbf
      ndpseg=sizesg/nszdis-1
      incore(iblock)=nszdis*ndpseg
      nseg=(naoint-1)/incore(iblock)+1
      start(iblock)=outseg
      outseg=outseg+nseg
      outcr=outcr+1
      go to 730
 545  naoint=isbf*jsbf*ksbf*lsbf
      incore(iblock)=-1
      start(iblock)=naopos
      naopos=naopos+naoint
      incr=incr+1
      go to 730
c
 730  if(traop(1).ge.1) write(nlist,735) iblock,isym,jsym,ksym,lsym,
     1   itype,incore(iblock),start(iblock),naoint,spincr
 735  format(i6,6x,4(i1,4x),i2,4x,i9,i9 ,i17,8x,i12)
 740  continue
 760  continue
 780  continue
 800  continue
c
      write(nlist,810) incr,outcr
 810  format(/' number of blocks to be transformed in-core is',i4,
     a /' number of blocks to be transformed out of core is ',i4)
      inseg=(naopos-1)/sizesg+1
      if(incr.eq.0) inseg=0
      write(nlist,820) inseg,outseg,sizesg
 820  format(/' in-core ao list requires ',i4,' segments.'/
     a' out of core ao list requires ',i4,' segments.'/
     b' each segment has length ',i6,' working precision words.')
      totseg=inseg+outseg
c
c  for out of core blocks, reset start with respect to the
c  first segment of the sort.
c
      if(outcr.eq.0) go to 850
      do 830 ibk=1,nsymbk
      if(incore(ibk).gt.0)  start(ibk)=start(ibk)+inseg
 830  continue
 850  continue
c
c  calculate iotorb
c
c      iotorb(iorb)=the corresponding index of iorb within the
c      full transformation matrix.
c
      iorbpt=1
      do 1350 iob=1,nbft
      if(iorb(iob).le.0) go to 1350
      iotorb(iorbpt)=iob
      iorbpt=iorbpt+1
 1350 continue
      if(traop(1).le.2) go to 1400
      write(nlist,1370) (iotorb(i),i=1,norbt)
 1370 format(//' iotorb = '//(i6,19i5))
 1400 continue
      if(sortfg.ne.0) return
c
c...basis function integral buffer
c  coefficients have not been placed in core(*).  use the rest of
c  core(*) starting at iadd.
      icd(3)=iadd
c
c...norbsm(*) array
      icd(4)=icd(3)
c
c...addressing array iim1lg
      icd(5)=icd(4)+forbyt(nbft)
c
c  calculate largest block of orbitals
c
      nbfmx=0
      do 1010 ibk=1,nblock
      nbfmx=max0(nbfmx,nbknbf(ibk))
 1010 continue
c
c  indexing arrays iim1(*) and im1ns(*) must be at least 8 integers
c  long for symmetry block indexing
c     4/28/82  rls
c
      spiim1=max0(forbyt(nbfmx*nbfmx),forbyt(8))
c
c...addressing array im1ns
      icd(6)=icd(5)+spiim1
c
c.. total required
      itotal=icd(6)+max0(forbyt(nsym*nbfmx),forbyt(8))
c
c  check to make sure space is available
c
      if(itotal.lt.icore-1) go to 1050
      write(nlist,1030) icore,(icd(i),i=1,8),itotal
 1030 format(//' >>>>> not enough core available <<<<<'//
     1' icore = ',i10/' core divided by ',8i10/
     2' total needed is ',i10///
     3' >>>>>>>>>>program stops execution<<<<<<<<<<')
      call bummer('trmain: itotal=',itotal,faterr)
c
c  determine length of direct access records
c
 1050 avcr=icore-itotal-10
      iword1=avcr/totseg
      do 1060 i=1,14
      if(iword1.ge.length(i)) go to 1070
 1060 continue
      write(nlist,1065) icore,itotal,avcr,iword1
 1065 format(//' need more core for the program to sort',
     1' efficiently the ao integrals.'/' icore = ',i10/
     2' itotal = ',i10/' core left for sorting is ',i10/
     3' iword1 is ',i5///
     4' >>>>>>>>>>program stops execution<<<<<<<<<<')
      call bummer('trmain: itotal=',itotal,faterr)
 1070 iword1=length(i)
      iword=iword1
      nbuk=totseg
      irec=1
      ibufad=itotal+1
      write(nlist,1075) iword,nbuk,ibufad
 1075 format(/15x,'ao integral sort statistics'/
     1' length of records:',i10/' number of buckets:',i4/
     2' scratch file used is da1:',/
     +  ' amount of core needed for in-core',
     3' arrays:',i6)
c
c  open scratch direct access file number 1 for writing.
c
      call da1ow(iword)
      call insort_o
c      b2=rclok1(1.)
c
c  sort two electron  ao integrals
c
      if (flags(26)) then
        do iordint=0,9
        if (iordint.eq.0) then
           fordint(1:8)='ORDINT  '
        else
           write(fordint(1:8),'(a,i1,a1)') 'ORDINT',iordint,' '
        endif
        inquire(file=fordint(1:len_trim(fordint)),exist=lexist)
        if (lexist) then 
         imc1=0
          imc2=45
          call mcOpnOrd(mcirc,imc1,fordint(1:len_trim(fordint)),imc2)
          if (mcirc.ne.0)
     . call bummer('opening ordint failed',0,2)
           write(0,*) '6a: processing ',fordint(1:len_trim(fordint))
           call twoao_os(core,incore,start,
     &        core(icd(5)),core(icd(6)),nbfmx,aointf,nsym,
     &        ijsmix,klsmix,ismtyp,sizesg,core(icd(4)),
     .        fordint(1:len_trim(fordint)))
          call daclos(45)
        endif
        enddo
      else
      call twoao_o(core,incore,start,
     & core(icd(5)),core(icd(6)),nbfmx,aointf,nsym,
     & ijsmix,klsmix,ismtyp,sizesg,core(icd(4)))
      endif
c      b1=rclok1(1.)-b2
c      write(nlist,1210) b1
c 1210 format(//' twoao_o required',f10.4,' seconds.'//)
c
c  finish writing out partially filled buckets
c
      call finsrt_o(core)
c
c  close da1 for writing
c
      call da1cw
c
c  sort incore two-electron integrals
c
      if(incr.eq.0) return
c
c  open da1 for reading
c
      call da1or
      icd(3)=icd(1)+iword
      call srtinc_o(inseg,naopos,sizesg,scrtfl,core(icd(3)),
     +  core(icd(1)))
c
      return
c
c.....................................................................
c
c  this part of the program actually performs the transformation
c  and is called when flag.ne.0.
c
c.....................................................................
c
 1425 continue
c
c...reduced transformation matrix (rtr)
      icd(1)=iadd
c
c...beginning of space for transformation
      icd(2)=kbufad
c
      rewind scrtfl
      rewind trintf
c
c  open da1 for reading
c
      call da1or
c
c  transform the in-core blocks of 2-e integrals
c
c  initialize output buffer
c
      inout=0
      nrec=0
      ntotot=0
      trinto=trintf
      if(incr.eq.0) go to 1500
      if(traop(1).gt.1)write(nlist,3001) nbksym,nbknbf,nbkorb,nbfoff,
     +  nnbfof,nbf2of,orbbff,symnbk,nfrzc,orboff
 3001 format(//' nbksym,nbknbf,nbkorb,nbfoff,nnbfof,nbf2of,',
     1' orbbff,symnbk,nbrzc,orboff='//(//8i8))
      call trainc_o(ijsmix,klsmix,ismtyp,mult,nsym,scrtfl,
     1   incore,core(icd(1)),core(icd(2)))
c
c  transform the out-of-core blocks of 2-e integrals
c
 1500 if(outcr.eq.0) go to 2900
c
      avcore=icore-icd(2)-10
c
      if(traop(1).gt.4)write(nlist,3001) nbksym,nbknbf,nbkorb,
     +  nbfoff,nnbfof,nbf2of,orbbff,symnbk,nfrzc,orboff
c
c  loop over symmetries
c
      do 2800 isym=1,nsym
      if(nbknbf(symnbk(isym)).eq.0) go to 2800
      isbf=nbknbf(symnbk(isym))
      isob=nbkorb(symnbk(isym))
      isoff=orboff(symnbk(isym))
c
      do 2780 jsym=1,isym
      if(nbknbf(symnbk(jsym)).eq.0) go to 2780
      jsbf=nbknbf(symnbk(jsym))
      jsob=nbkorb(symnbk(jsym))
      jsoff=orboff(symnbk(jsym))
      ijsym=mult(isym,jsym)
      ij=iim1(isym)+jsym
c
      do 2760 ksym=1,isym
      if(nbknbf(symnbk(ksym)).eq.0) go to 2760
      ksbf=nbknbf(symnbk(ksym))
      ksob=nbkorb(symnbk(ksym))
      ksoff=orboff(symnbk(ksym))
      lsymx=ksym
      if(isym.eq.ksym) lsymx=jsym
c
      do 2740 lsym=1,lsymx
      klsym=mult(ksym,lsym)
      if(ijsym.ne.klsym) go to 2740
      if(nbknbf(symnbk(lsym)).eq.0) go to 2740
      lsbf=nbknbf(symnbk(lsym))
      lsob=nbkorb(symnbk(lsym))
      lsoff=orboff(symnbk(lsym))
      kl=iim1(ksym)+lsym
      iblock=ijsmix(ij)+klsmix(kl)
      if(incore(iblock).le.0)go to 2740
      itype=ismtyp(iblock)
      go to (2000,2200,2400,2600),itype
c
c  type (aa:aa)
c
 2000 strt=start(iblock)+1
      call seg_o(itype,isbf,isob,jsbf,jsob,ksbf,ksob,lsbf,lsob,
     1   incore,iblock,length,iwrdim,nseg,mseg,nszdis,mszdis,avcore)
c     write(nlist,2003) iwrdim,nseg,mseg,nszdis,mszdis
c2003 format(/' iwrdim,nseg,mseg,nszdis,mszdis = '/(5i10))
      pta=orbbff(symnbk(ksym))+1
      icd(3)=icd(2)+nszdis
      call trouta_o(core(icd(1)),ksbf,ksob,pta,lsbf,lsob,pta,
     1   itype,core(icd(2)),core(icd(3)),strt,valbuf,
     2   nszdis,nseg,mseg,iwrdim)
      call troutb_o(core(icd(1)),isbf,isob,pta,jsbf,jsob,pta,
     1   itype,core(icd(2)),valbuf,mszdis,mseg,isob)
      go to 2740
c
c  type (aa:bb)
c
 2200 strt=start(iblock)+1
      call seg_o(itype,isbf,isob,jsbf,jsob,ksbf,ksob,lsbf,lsob,
     1   incore,iblock,length,iwrdim,nseg,mseg,nszdis,mszdis,avcore)
      pta1=orbbff(symnbk(ksym))+1
      pta2=orbbff(symnbk(isym))+1
      icd(3)=icd(2)+nszdis
      call trouta_o(core(icd(1)),ksbf,ksob,pta1,lsbf,lsob,pta1,
     1   itype,core(icd(2)),core(icd(3)),strt,valbuf,
     2   nszdis,nseg,mseg,iwrdim)
      call troutb_o(core(icd(1)),isbf,isob,pta2,jsbf,jsob,pta2,
     1   itype,core(icd(2)),valbuf,mszdis,mseg,isob)
      go to 2740
c
c  type (ab:ab)
c
 2400 strt=start(iblock)+1
      call seg_o(itype,isbf,isob,jsbf,jsob,ksbf,ksob,lsbf,lsob,
     1   incore,iblock,length,iwrdim,nseg,mseg,nszdis,mszdis,avcore)
      pta=orbbff(symnbk(ksym))+1
      ptb=orbbff(symnbk(lsym))+1
      icd(3)=icd(2)+nszdis
      call trouta_o(core(icd(1)),ksbf,ksob,pta,lsbf,lsob,ptb,
     1   itype,core(icd(2)),core(icd(3)),strt,valbuf,
     2   nszdis,nseg,mseg,iwrdim)
      call troutb_o(core(icd(1)),isbf,isob,pta,jsbf,jsob,ptb,
     1   itype,core(icd(2)),valbuf,mszdis,mseg,isob)
      go to 2740
c
c  type (ab:cd)
c
 2600 strt=start(iblock)+1
c     idbblk=iblock
      call seg_o(itype,isbf,isob,jsbf,jsob,ksbf,ksob,lsbf,lsob,
     1   incore,iblock,length,iwrdim,nseg,mseg,nszdis,mszdis,avcore)
      pta=orbbff(symnbk(ksym))+1
      ptb=orbbff(symnbk(lsym))+1
      icd(3)=icd(2)+nszdis
      call trouta_o(core(icd(1)),ksbf,ksob,pta,lsbf,lsob,ptb,
     1   itype,core(icd(2)),core(icd(3)),strt,valbuf,
     2   nszdis,nseg,mseg,iwrdim)
      pta=orbbff(symnbk(isym))+1
      ptb=orbbff(symnbk(jsym))+1
      call troutb_o(core(icd(1)),isbf,isob,pta,jsbf,jsob,ptb,
     1   itype,core(icd(2)),valbuf,mszdis,mseg,ksob)
c
 2740 continue
 2760 continue
 2780 continue
 2800 continue
 2900 continue
c
c  write out the last integral buffer.
c
      nrec=nrec+1
      ntotot=ntotot+inout
      nomore=1
      call
     .  wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,inout,nomore)
      rewind trinto
      write(nlist,2910) ntotot,nrec
 2910 format(/' number of transformed integrals put on file:',i10/
     1' number of records written:',i6)
      return
      end
      subroutine blk2tr_o(nsym,nblock,nbpsy,nmpsy,orbtyp,nlistx,nmot)
c
c  initialize some common block info for the the 2-e transformation.
c
      implicit integer (a-z)
c
      common/cnlist/nlist
c
      integer   nmotx
      parameter(nmotx=1023)
c
      integer occl
      common/ocupy/occl(nmotx)
c
      integer nbpsy(8),nmpsy(8),orbtyp(nmot)
c
      common /blocks/ nbksym(8),nbknbf(8),nbkorb(8),nbfoff(8),
     1   nnbfof(8),nbf2of(8),orbbff(8),symnbk(8),nfrzc(8),orboff(8)
      save blocks
c
      integer mskorb(0:4)
      data mskorb/1,1,1,0,0/
c
      nlist=nlistx
c
c  zero out these arrays.
c
      do 100 i=1,8
      nbksym(i)=0
      nbknbf(i)=0
      nbkorb(i)=0
      nbfoff(i)=0
      nnbfof(i)=0
      nbf2of(i)=0
      orbbff(i)=0
      symnbk(i)=8
      nfrzc(i)=0
      orboff(i)=0
 100  continue
c
c  place values in arrays
c
      nblock=nsym
      do 200 iblk=1,nblock
      nbksym(iblk)=iblk
      nbknbf(iblk)=nbpsy(iblk)
      nbkorb(iblk)=nmpsy(iblk)
 200  continue
c
c  orbtyp(*) = orbital type for each orbital (fc=0,d=1,a=2,v=3,fv=4).
c  occl(*)   = 0 for virtual, 1 for occupied.
c
      do 300 i=1,nmot
300   occl(i)=mskorb(orbtyp(i))
      return
      end
      subroutine insort_o
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      common/cnlist/nlist
      common/sort/irecpt(200),ioff(200),ifull(200),
     +  ibufad,iword,nbuk,numel,npbuk,irec
      data idimax/200/
c
c  this routine initializes arrays used in the ao sort.
c
c
c  irecpt(*)  direct access record pointers
c  ioff(*)    offsets for buckets within blank common
c  ifull(*)   integral pointers used during ao sort
c  ibufad     location of the start of the buckets
c  iword      number of working precision words in eack bucket
c  nbuk       number of buckets (.le.idimax=200)
c  numel      number of elements sorted
c  npbuk      number of elements held in eack bucket
c  irec       pointer to next available direct access record
c
      if(nbuk.gt.idimax)go to 500
      do 100 i=1,nbuk
      irecpt(i)=0
      ifull(i)=0
      ioff(i)=ibufad+(i-1)*iword
100   continue
      numel=0
      irec=1
c
c  each bucket (length iword) holds npbuk integrals, npbuk labels, and
c  two integers for chaining.
c  in general npbuk is the largest integer satisfying the relation:
c  npbuk+inl(npbuk+2) .le. iword
c
c  e.g. vax double precision: npbuk=(((iword-1)*2)/3)
c  e.g. vax single precision: npbuk=((iword-2)/2)
c
*@ifdef future
*CC  anyone?
*@else
      npbuk = (((iword - 1) * 2) / 3)
*@endif
      return
500   write(nlist,6010)nbuk,idimax
6010  format(/' insort: nbuk,idimax=',2i6,' need larger sorting',
     +  ' arrays')
      call bummer('insort: nbuk=',nbuk,faterr)
      return
      end
      subroutine sortot_o(buf,val,lab,ibuk)
c
c  this routine places val and lab into the ibuk bucket,
c  dumps the bucket if full, and increments the appropriate pointers.
c
      common/cnlist/nlist
      common/sort/irecpt(200),ioff(200),ifull(200),
     +  ibufad,iword,nbuk,numel,npbuk,irec
c
      real*8 val,buf(*)
c
c-----------------------------------------------------------------------
c
c  function inl(k) gives the position of the k'th label in the
c  working precision array used to hold the labels.
c  in general:  inl(k)=(k+nlpw-1)/nlpw
c  where nlpw is the number of labels per working precision word
c
c  function iposl(k) is complementary to inl(k) and gives the position
c  of the k'th label within the working precision word (from 1 to nlpw)
c  in general:  iposl(k)=mod(k-1,nlpw)+1
c
*@ifdef future
*CC  anyone?
*@else
      inl(k)=(k+1)/2
      iposl(k)=mod(k-1,2)+1
*@endif
c
c-----------------------------------------------------------------------
c
      numel=numel+1
      k=ifull(ibuk)+1
      ifull(ibuk)=k
      buf(ioff(ibuk)+k)=val
      ilpt=ioff(ibuk)+npbuk+inl(k)
      ipos=iposl(k)
c
c  put label in the ipos position of the ilpt word
c
      call labput(buf(ilpt),lab,ipos)
      if(k.lt.npbuk)return
c
c  bucket is full.  write out bucket and adjust pointers.
c
      k=npbuk+1
      ilpt=ioff(ibuk)+npbuk+inl(k)
      ipos=iposl(k)
c
c  put the number of integrals in the first integer position after
c  the labels.
c
      call labput(buf(ilpt),ifull(ibuk),ipos)
c
      k=npbuk+2
      ilpt=ioff(ibuk)+npbuk+inl(k)
      ipos=iposl(k)
c
c  last record pointer for this bucket goes into the next integer slot.
c
      call labput(buf(ilpt),irecpt(ibuk),ipos)
c
c  write out record
c
      loc=ioff(ibuk)+1
      call da1w(buf(loc),irec)
      irecpt(ibuk)=irec
      irec=irec+1
      ifull(ibuk)=0
      return
      end
      subroutine finsrt_o(buf)
c
c  this routine writes out any partially filled buckets from the ao
c  integral sort.
c
      common/cnlist/nlist
      common/sort/irecpt(200),ioff(200),ifull(200),
     +  ibufad,iword,nbuk,numel,npbuk,irec
c
      real*8 buf(*)
c
c-----------------------------------------------------------------------
c
c  function inl(k) gives the position of the k'th label in the
c  working precision array used to hold the labels.
c  in general:  inl(k)=(k+nlpw-1)/nlpw
c  where nlpw is the number of labels per working precision word
c
c  function iposl(k) is complementary to inl(k) and gives the position
c  of the k'th label within the working precision word (from 1 to nlpw)
c  in general:  iposl(k)=mod(k-1,nlpw)+1
c
*@ifdef future
*CC  anyone?
*@else
      inl(k)=(k+1)/2
      iposl(k)=mod(k-1,2)+1
*@endif
c
c-----------------------------------------------------------------------
c
      do 100 ibuk=1,nbuk
      if(ifull(ibuk).eq.0)go to 100
c
c  put the number of integrals in the first integer position after
c  the labels.
c
      k=npbuk+1
      ilpt=ioff(ibuk)+npbuk+inl(k)
      ipos=iposl(k)
      call labput(buf(ilpt),ifull(ibuk),ipos)
c
c  last record pointer for this bucket goes into the next integer slot.
c
      k=npbuk+2
      ilpt=ioff(ibuk)+npbuk+inl(k)
      ipos=iposl(k)
      call labput(buf(ilpt),irecpt(ibuk),ipos)
c
c  write out record
c
      loc=ioff(ibuk)+1
      call da1w(buf(loc),irec)
      irecpt(ibuk)=irec
      irec=irec+1
100   continue
      nrecs=irec-1
      write(nlist,200)numel,nrecs
200   format(/1x,i10,' ao integrals were written into',i5,' records')
      return
      end

      subroutine twoao_o(buf,incore,start,
     +  iim1,im1ns,nbfmx,aointf,nsym,
     +  ijsmix,klsmix,ismtyp,sizesg,norbsm)
c
c  this routine reads in the ao two-electron integrals and sorts
c  them into buckets.
c
      implicit integer (a-z)
      common/cnlist/nlist

      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)

c
c  /buf1/ is used for buffering input integrals and transformed output
c  integrals.
      real*8 cmbuf1, vbuf
c  NOTE, there is a dependency of the buffer sizes to 8bit/16bit
c        packing mode and initial record size of the AO integral file
c        n2ebf(16bit) < n2ebf(8bit)!
c        should be changed!

      parameter (len2e=4096,n2ebf=2730,nipv=4)
      common /buf1/ cmbuf1(len2e),vbuf(n2ebf),lbuf(nipv,n2ebf),
     & inout,ntotot,nrec,trinto
c
      integer nbfmxp
      parameter (nbfmxp=1023)
      integer ninfo
      parameter (ninfomx=5)
       integer nenrgymx
      parameter (nenrgymx=5)
       integer nmapmx
       parameter (nmapmx=2)
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx)
     . ,slabelao, bfnlabao

 
      integer mcpsym,mcqsym,mcrsym,mcssym,nmatl,nnbuf,nmatrd,iflag,mcirc
      integer mclen,imc1,imc2
      real*8, allocatable :: molcasbuffer(:)
      integer pmax,smax,nnmat,molcasbuflen,np(8)
      integer ibufrd,imatrd,ii,irc
      logical ordopened

      integer luinta,itmp,itmp2(8),lmxbuf,nibuf,nbits
      integer*4 m8,m16
      parameter (m8=255,m16=65535)
      integer*4 n8,n16,n24
        parameter (n8=-8,n16=-16,n24=-24)
      real*8, allocatable :: dbuf(:)
      integer*4, allocatable :: ibuf(:,:)




      integer ierr,itypea,itypeb,ibitv,ifmt
      real*8 val,buf(*)
c
      dimension incore(1),start(1)
      integer ijsmix(36),klsmix(36),ismtyp(106),mult(8,8)
      dimension iim1(1),im1ns(nbfmx,nsym),nnp1s(8),nanb(36)
      integer norbsm(*)
c
      common /blocks/ nbksym(8),nbknbf(8),nbkorb(8),nbfoff(8),
     1   nnbfof(8),nbf2of(8),orbbff(8),symnbk(8),nfrzc(8),orboff(8)
      save blocks
c
      common /op/ traop(5)
c
      data mult /1,2,3,4,5,6,7,8,
     1           2,1,4,3,6,5,8,7,
     2           3,4,1,2,7,8,5,6,
     3           4,3,2,1,8,7,6,5,
     4           5,6,7,8,1,2,3,4,
     5           6,5,8,7,2,1,4,3,
     6           7,8,5,6,3,4,1,2,
     7           8,7,6,5,4,3,2,1/

c
c  function inl(k) gives the location of the buffer
c  word for the k'th integral label.  function iposl(k) gives
c  the position (from 1 to nlpw) of the k'th label within the buffer
c  word. in general inl(k)=(k+nlpw-1)/nlpw  and iposl(k)=mod(k-1,nlpw)+1
c
c  set up iim1 array
c  must be at least 8 long for symmetry addressing.  (4/28/82  rls)
c
      nnmx=max0(nbfmx*nbfmx,8)
      iim1(1)=0
      do 10 kt=2,nnmx
      iim1(kt)=iim1(kt-1)+kt-1
 10   continue
c
c  set up im1ns
c
      do 30 sym=1,nsym
      nbf=nbknbf(symnbk(sym))
      im1ns(1,sym)=0
      do 20 kt=2,nbfmx
      im1ns(kt,sym)=im1ns(kt-1,sym)+nbf
 20   continue
 30   continue
c
c  set up nnp1s
c
      do 40 sym=1,nsym
      nnp1s(sym)=nbknbf(symnbk(sym))*(nbknbf(symnbk(sym))+1)/2
 40   continue
c
c  set up nanb
c
      ipt=0
      do 43 sym=1,nsym
      nbf1=nbknbf(symnbk(sym))
      do 42 sym2=1,sym
      ipt=ipt+1
      nanb(ipt)=nbf1*nbknbf(symnbk(sym2))
 42   continue
 43   continue
c
c  set norbsm(i) to give the symmetry block of orbital i
c
      do 55 sym=1,nsym
      iblk=symnbk(sym)
      nbf=nbknbf(iblk)
      if(nbf.le.0)go to 55
      ipt=nbfoff(iblk)
      do 50 i=1,nbf
      ipt=ipt+1
      norbsm(ipt)=iblk
50    continue
55    continue
c
      if(traop(1).lt.3)  go to 70
c
c  write out these addressing arrays
c
      write(nlist,45 ) (iim1(kt),kt=1,nnmx)
 45   format(//' from twoao_o : iim1 = ',//(' ',10i10))
      do 47 sym=1,nsym
      write(nlist,46 ) sym,(im1ns(kt,sym),kt=1,nbfmx)
 46   format(//' iim1ns  : sym = ',i5//(' ',10i10))
 47   continue
      write(nlist,48 )(nnp1s(sym),sym=1,nsym)
 48   format(//' nnp1s = ',8i8//)
      ipt=iim1(nsym)+nsym
      write(nlist,49 ) (nanb(it),it=1,ipt)
 49   format(//' nanb = ',//(' ',10i10))
 70   continue

       if (flags(27)) then
         CALL GPOPEN(LUINTA,'AOTWOINT','OLD',' ',' ',IDUMMY,.FALSE.)
         CALL MOLLAB('BASINFO ',LUINTA,LUPRI)
         READ (LUINTA) itmp,(itmp2(I),I=1,8),LMXBUF,NIBUF,NBITS
 53      format('nsym=',i3,' nbfpsy=',8i4)
 54      format('buflen=',i6,' nibuf=',i2,' nbits=',i3)
         if (nibuf.eq.2 .and. nbits.ne.16) 
     .    call bummer('cntr3c: inconsistent nibuf,nbits',nibuf,2)
         if (nibuf.eq.1 .and. nbits.ne.8)
     .    call bummer('cntr3c: inconsistent nibuf,nbits',nibuf,2)
         if (itmp.ne.nsym)
     .    call bummer('twoao_o: inconsistent itmp,nsym',itmp,2)
c        do i=1,nsym
c          if (itmp2(i).ne.nbfpsy(i))
c    .    call bummer('cntr3c: inconsistent np(i),naos(i) i=',i,2)
c        enddo
       allocate(ibuf(lmxbuf,nibuf),dbuf(lmxbuf))
       CALL MOLLAB('BASTWOEL',LUINTA,6)
       endif 

c
c  process 2-e integrals
c
      numin2=0

100   continue
c
c  read and unpack the next buffer of integrals and labels.
c
c     call rdlab2(aointf,len2e,cmbuf1,n2ebf,vbuf,4,lbuf,nbuf,last)

      if (flags(27)) then 
       READ(LUINTA,err= 61) dbuf(1:lmxbuf),ibuf(1:lmxbuf,1:nibuf),nbuf
       if (nbuf.le.0) goto 51
      else
      if (len2e.lt.info(4))
     .  call bummer('len2e.ne.info(4), len2e=',len2e,2)
      if (n2ebf.lt.info(5))
     .  call bummer('n2ebf.ne.info(5), n2ebf=',n2ebf,2)
       call sifrd2(aointf,info,4,0,cmbuf1,nbuf,last,itypea,
     .  itypeb,ifmt,ibvtype,vbuf,lbuf,0,ierr)
      endif 

c
c  process buffer by unpacking blocks and labels
c
      do 500 iint=1,nbuf

      if (flags(27)) then 
        val=dbuf(iint)
        if (nibuf.eq.2) then
          p=iand(ishft(ibuf(iint,1),n16),m16)
          q=iand(ibuf(iint,1),m16)
          r=iand(ishft(ibuf(iint,2),n16),m16)
          s=iand(ibuf(iint,2),m16)
        else
          p=iand(ishft(ibuf(iint,1),n24),m8)
          q=iand(ishft(ibuf(iint,1),n16),m8)
          r=iand(ishft(ibuf(iint,1),n8),m8)
          s=iand(ibuf(iint,1),m8)
        endif
      else
      val=vbuf(iint)
      p=lbuf(1,iint)
c
c  check for p=0 flag.  ignore integral if set.
c
      if(p.eq.0)go to 500
c
      q=lbuf(2,iint)
      r=lbuf(3,iint)
      s=lbuf(4,iint)
      endif 
      numin2=numin2+1
      pblk=norbsm(p)
      qblk=norbsm(q)
      rblk=norbsm(r)
      sblk=norbsm(s)
      p=p-nbfoff(pblk)
      q=q-nbfoff(qblk)
      r=r-nbfoff(rblk)
      s=s-nbfoff(sblk)
c
c  convert blocks to symmetries and place symmetries in canonical order
c
      psym=nbksym(pblk)
      qsym=nbksym(qblk)
      if(psym.ge.qsym) go to 110
      temp=psym
      psym=qsym
      qsym=temp
      temp=p
      p=q
      q=temp
c
 110  rsym=nbksym(rblk)
      ssym=nbksym(sblk)
      if(rsym.ge.ssym) go to 120
      temp=rsym
      rsym=ssym
      ssym=temp
      temp=r
      r=s
      s=temp
c
 120  pq=iim1(psym)+qsym
      rs=iim1(rsym)+ssym
      if(pq.ge.rs) go to 130
      temp=psym
      psym=rsym
      rsym=temp
      temp=p
      p=r
      r=temp
      temp=qsym
      qsym=ssym
      ssym=temp
      temp=q
      q=s
      s=temp
c
c  determine block index
c
c  the addressing scheme was modified 4/13/82 to account
c  for non-canonical ordering of the ao indices. (rls)
c
130   block=ijsmix(iim1(psym)+qsym)+klsmix(iim1(rsym)+ssym)
      itype=ismtyp(block)
      if(incore(block).lt.0)go to 425
c
c  out-of-core types
c
      go to (370,380,390,400),itype
c
c  type (aa:aa)
c
 370  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      lad=(pq-1)*nnp1s(psym)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      if(pq.eq.rs) go to 500
      lad=(rs-1)*nnp1s(psym)+pq
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (aa:bb)
c
 380  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      lad=(pq-1)*nnp1s(rsym)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:ab)
c
 390  pq=im1ns(q,psym)+p
      rs=im1ns(s,psym)+r
      sympr=iim1(psym)+qsym
      lad=(pq-1)*nanb(sympr)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      if(pq.eq.rs) go to 500
      lad=(rs-1)*nanb(sympr)+pq
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:cd)
c
 400  pq=im1ns(q,psym)+p
      rs=im1ns(s,rsym)+r
      sympr=iim1(rsym)+ssym
      lad=(pq-1)*nanb(sympr)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  incore types
c
 425  go to (430,440,450,460),itype
c
c  type (aa:aa)
c
 430  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      mpq=max0(pq,rs)
      mrs=min0(pq,rs)
      lad=(mrs-1)*nnp1s(psym)-iim1(mrs)+mpq+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (aa:bb)
c
 440  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      lad=(pq-1)*nnp1s(rsym)+rs+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:ab)
c
 450  if(p-r) 452,455,458
 452  temp=p
      p=r
      r=temp
      temp=q
      q=s
      s=temp
      go to 458
 455  if(q.ge.s) go to 458
      temp=q
      q=s
      s=temp
 458  continue
      pq=im1ns(p,qsym)+q
      rs=im1ns(r,ssym)+s
      lad=iim1(pq)+rs+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:cd)
c
 460  pq=im1ns(p,qsym)+q
      rs=im1ns(r,ssym)+s
      sympr=iim1(rsym)+ssym
      lad=(pq-1)*nanb(sympr)+rs+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
c
 500  continue
c
      if (flags(27)) then
        goto 100
      else
        if(last.eq.0) go to 100
      endif
 51   continue
      if (flags(27)) then
        call gpclose(LUINTA,'KEEP')
        deallocate(dbuf,ibuf)
      endif 
      write(nlist,600) numin2
 600  format(/' twoao_o processed ',i10,' two electron ao integrals.')
      return
 61   call bummer('Error reading integrals from AOTWOINT',0,2)
      end


      subroutine srtinc_o(inseg,totint,sizesg,scrtfl,ints,bufin)
c
c  this routine performs the second-half sort for the incore
c  two - electron ao integrals.
c
      implicit integer (a-z)
      common/cnlist/nlist
c
      real*8 ints(1),bufout,bufin(1)
c
      common /buf2/ bufout(1610)
c
      common/sort/irecpt(200),ioff(200),ifull(200),
     +  ibufad,iword,nbuk,numel,npbuk,irec
c
c-----------------------------------------------------------------------
c
c  function inl(k) gives the position of the k'th label in the
c  working precision array used to hold the labels.
c  in general:  inl(k)=(k+nlpw-1)/nlpw
c  where nlpw is the number of labels per working precision word
c
c  function iposl(k) is complementary to inl(k) and gives the position
c  of the k'th label within the working precision word (from 1 to nlpw)
c  in general:  iposl(k)=mod(k-1,nlpw)+1
c
*@ifdef future
*CC  anyone?
*@else
      inl(k)=(k+1)/2
      iposl(k)=mod(k-1,2)+1
*@endif
c
c-----------------------------------------------------------------------
c
c  intialize some variables
c
      intfin=0
      maxout=1610
      outint=0
      outpt=1
c
c  loop over incore segments
c
      do 1000 iseg=1,inseg
c
c  zero out segment area
c
      nleft=totint-intfin
      mintfn=nleft-totint
      npseg=min0(sizesg,nleft)
      call wzero(npseg,ints,1)
      intpt=1
c
c  read in record
c
      ircp=irecpt(iseg)
 100  call da1r(bufin,ircp)
c
c  unpack number of integrals and record pointer
c
      k=npbuk+1
      ilpt=npbuk+inl(k)
      ipos=iposl(k)
      call labget(bufin(ilpt),nin,ipos)
      k=npbuk+2
      ilpt=npbuk+inl(k)
      ipos=iposl(k)
      call labget(bufin(ilpt),ircp,ipos)
c
c  process record
c
      do 500 iin=1,nin
c
c  unpack integral label and process integral
c
      ilpt=npbuk+inl(iin)
      ipos=iposl(iin)
      call labget(bufin(ilpt),lab,ipos)
      lad=lab+mintfn
      if(lad.lt.1.or.lad.gt.npseg+1) go to 490
      ints(lad)=bufin(iin)
      go to 500
 490  write(nlist,61)lad,iin,nin
 61   format(' lad,iin,nin = ',3i10)
 500  continue
      if(ircp.gt.0) go to 100
c
c  write sorted integral list onto scratch file
c
      if(outpt.ne.1) go to 800
c
 650  nrec=(npseg-intpt)/maxout+1
      if(nrec.eq.1) go to 750
c
c  fill and dump buffers
c
      nrecm1=nrec-1
      do 700 irec=1,nrecm1
      call dcopy_wr(maxout,ints(intpt),1,bufout,1)
      write(scrtfl) bufout
      outint=outint+maxout
      intpt=intpt+maxout
 700  continue
c
c  fill partial buffer with remaining integrals
c
 750  nlft=npseg-intpt+1
      call dcopy_wr(nlft,ints(intpt),1,bufout,1)
      outpt=nlft+1
      intpt=intpt+nlft
      go to 900
c
c  fill remaining buffer space and if necessary, dump buffer.
c
 800  nsp=maxout-outpt+1
      nlft=npseg-intpt+1
      if(nsp-nlft) 815,825,850
 815  call dcopy_wr(nsp,ints(intpt),1,bufout(outpt),1)
      write(scrtfl) bufout
      outint=outint+maxout
      intpt=intpt+nsp
      outpt=1
      go to 650
c
 825  call dcopy_wr(nsp,ints(intpt),1,bufout(outpt),1)
      write(scrtfl) bufout
      intpt=1
      outint=outint+maxout
      go to 900
c
 850  call dcopy_wr(nlft,ints(intpt),1,bufout(outpt),1)
      outpt=outpt+nlft
      intpt=intpt+nlft
c
 900  intfin=intfin+npseg
c
 1000 continue
      if(intpt.ne.1) write(scrtfl) bufout
      outint=outint+outpt-1
      write(nlist,1010) intfin,outint
 1010 format(/' srtinc_o read in ',i10,' integrals and',
     1' wrote out ',i10,' integrals.')
      return
      end
      subroutine inabcd_o (ucofa,nbfa,norba,ucofb,nbfb,norbb,
     1   ucofc,nbfc,norbc,ucofd,nbfd,norbd,tint,fsum,esum)
c
c  this routine performs a full four-index in-core transformation
c  using the bender-shavitt method.  this routine transforms
c  blocks of integrals of the type (a,b : c,d) or 'sss'.
c  (p,q : r,s) --> (i,j : k,l)
c
c  ucofa, ucofb, ucofc, ucofd are mo coefficients for symmetries a,b,c,d
c  tint - final transformed integrals
c  fsum,esum,and dsum are partial sums
c
c  written by frank brown  jan. 15, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer (a-z)
c
      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 ucofa,ucofb,ucofc,ucofd,tint,fsum,esum,dsum
      real*8 intbuf
      dimension ucofa(nbfa,norba),ucofb(nbfb,norbb)
      dimension ucofc(nbfc,norbc),ucofd(nbfd,norbd)
      dimension tint(1),fsum(1),esum(1)
      common /buf/ intbuf(1610),intpt,intmx
      common /narray/ dsum(nmotx),idum(nmotx,2)
c
      mcmd=norbc*norbd
      mbmcmd=norbb*mcmd
      mmmm=norba*mbmcmd
c...zero target array
      call wzero (mmmm,tint,1)
c
c  loop over p
c
      do 1400 p=1,nbfa
      call wzero (mbmcmd,fsum,1)
c
c  loop over q
c
      do 900 q=1,nbfb
      call wzero (mcmd,esum,1)
c
c  loop over r
c
      do 500 r=1,nbfc
      call wzero (norbd,dsum,1)
c
c  loop over s
c
      do 200 s=1,nbfd
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
c
c  calculate dsum(l),l=1,norbd
c
      call daxpy_wr(norbd,intbuf(intpt),ucofd(s,1),nbfd,dsum(1),1)
c
 200  continue
c
c  loop over k and l
c
      klpt=1
      do 400 k=1,norbc
      call daxpy_wr(norbd,ucofc(r,k),dsum(1),1,esum(klpt),1)
      klpt=klpt+norbd
 400  continue
c
 500  continue
c
c  loop over j, k, and l
c
      jklpt=1
      do 800 j=1,norbb
      call daxpy_wr(mcmd,ucofb(q,j),esum(1),1,fsum(jklpt),1)
      jklpt=jklpt+mcmd
 800  continue
c
 900  continue
c
c  loop over i, j, k, and l
c
      ijklpt=1
      do 1300 i=1,norba
      call daxpy_wr(mbmcmd,ucofa(p,i),fsum(1),1,tint(ijklpt),1)
      ijklpt=ijklpt+mbmcmd
 1300 continue
c
 1400 continue
      return
      end
      subroutine inabab_o(ucofa,nbfa,norba,ucofb,nbfb,norbb,
     1   tint,fsum,esum)
c
c  this routine performs a full four-index in-core transformation
c  using the bender-shavitt method.  this routine transforms
c  blocks of integrals of the type (a,b : a,b) or 'sst'.
c  (p,q : r,s) --> (i,j : k,l)
c
c  ucofa and ucofb are mo coefficients for symmetries a and b
c  tint - final transformed integrals
c  fsum, esum, and d sum are partial sums
c
c  written by frank brown  jan. 16, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer (a-z)
c
      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 ucofa,ucofb,tint,fsum,esum,dsum
      real*8 int,intbuf
      dimension ucofa(nbfa,norba),ucofb(nbfb,norbb)
      dimension tint(1),fsum(1),esum(1)
      common /buf/ intbuf(1610),intpt,intmx
      common /narray/ dsum(nmotx),im1mb(nmotx),imamb(nmotx)
c
      mamb=norba*norbb
      mambsp=(mamb*(mamb+1))/2
      maabsp=mamb*norbb
c...zero target array
      call wzero (mambsp,tint,1)
c
c  loop over p
c
      do 1400 p=1,nbfa
      call wzero (maabsp,fsum,1)
c
c  loop over q
c
      do 900 q=1,nbfb
      call wzero (mamb,esum,1)
      smax=nbfb
      comp=0
c
c  loop over r
c
      do 500 r=1,p
      call wzero (norbb,dsum,1)
      if(p.ne.r) go to 50
      smax=q
      comp=q
c
c  loop over s
c
 50   do 200 s=1,smax
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)
      if(s.eq.comp) int=int*0.5d+00
c
c  calculate dsum(l),l=1,norbb
c
      call daxpy_wr(norbb,int,ucofb(s,1),nbfb,dsum(1),1)
c
 200  continue
c
c  loop over k and l
c
      klpt=1
      do 400 k=1,norba
      call daxpy_wr(norbb,ucofa(r,k),dsum(1),1,esum(klpt),1)
      klpt=klpt+norbb
 400  continue
c
 500  continue
c
c  loop over j, k, and l
c
      jklpt=1
      do 800 j=1,norbb
      call daxpy_wr(mamb,ucofb(q,j),esum(1),1,fsum(jklpt),1)
      jklpt=jklpt+mamb
 800  continue
c
 900  continue
c
c  loop over i, j, k, and l
c
      ijklpt=1
      ijpt=0
      do 1300 i=1,norba
      joff=0
      do 1200 j=1,norbb
      ijpt=ijpt+1
      jkpt=joff+1
      lmax=norbb
      do 1100 k=1,i
      if(i.eq.k) lmax=j
      call daxpy_wr(lmax,ucofa(p,i),fsum(jkpt),1,tint(ijklpt),1)
      call daxpy_wr(lmax,ucofa(p,k),fsum(ijpt),mamb,tint(ijklpt),1)
      jkpt=jkpt+norbb
      ijklpt=ijklpt+lmax
 1100 continue
      joff=joff+mamb
 1200 continue
 1300 continue
c
 1400 continue
      return
      end
      subroutine inaabb_o(ucofa,nbfa,norba,ucofb,nbfb,norbb,
     1   tint,fsum,esum)
c
c  this routine performs a full four-index in-core transformation
c  using the bender-shavitt method.  this routine transforms
c  blocks of integrals of the type (a,a : b,b) or 'tts'.
c  (p,q : r,s) --> (i,j : k,l)
c
c  ucofa and ucofb are mo coefficients for symmetries a and b
c  tint - final transformed integrals
c  fsum, esum, and dsum are partial sums
c
c  written by frank brown  jan. 16, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer (a-z)
c
      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 ucofa,ucofb,tint,fsum,esum,dsum
      real*8 int,intbuf
      logical pqflag
      dimension ucofa(nbfa,norba),ucofb(nbfb,norbb)
      dimension tint(1),fsum(1),esum(1)
      common /buf/ intbuf(1610),intpt,intmx
      common /indx/ iim1(nmotx)
      common /narray/ dsum(nmotx),imbmb1(nmotx),idum(nmotx)
c
      mbmb1=iim1(norbb)+norbb
      spfsum=norba*mbmb1
      sptint=(iim1(norba)+norba)*mbmb1
c...zero target array
      call wzero (sptint,tint,1)
c
c  loop over p
c
      do 1400 p=1,nbfa
      call wzero (spfsum,fsum,1)
      pqflag=.false.
c
c  loop over q
c
      do 900 q=1,p
      if(p.eq.q) pqflag=.true.
      call wzero (mbmb1,esum,1)
c
c  loop over r
c
      do 500 r=1,nbfb
      call wzero (norbb,dsum,1)
c
c  loop over s  (case a : s < r)
c
      if(r.eq.1) go to 205
      rm1=r-1
      do 200 s=1,rm1
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)
      if(pqflag) int=int*0.5d+00
c
c  calculate dsum(l),l=1,norbb
c
      call daxpy_wr(norbb,int,ucofb(s,1),nbfb,dsum(1),1)
c
 200  continue
c
c  (case b : s = r)
c
 205  intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.5d+00
      if(pqflag) int=int*0.5d+00
      call daxpy_wr(norbb,int,ucofb(r,1),nbfb,dsum(1),1)
c
c  loop over k and l
c
      klpt=1
      do 400 k=1,norbb
      call daxpy_wr(k,ucofb(r,k),dsum(1),1,esum(klpt),1)
      call daxpy_wr(k,dsum(k),ucofb(r,1),nbfb,esum(klpt),1)
      klpt=klpt+k
 400  continue
c
 500  continue
c
c  loop over j, k, and l
c
      jklpt=1
      do 800 j=1,norba
      call daxpy_wr(mbmb1,ucofa(q,j),esum(1),1,fsum(jklpt),1)
      jklpt=jklpt+mbmb1
 800  continue
c
 900  continue
c
c  loop over i, j, k, and l
c
      ijklpt=1
      ipt=1
      mbmb1i=0
      do 1300 i=1,norba
      mbmb1i=mbmb1i+mbmb1
      call daxpy_wr(mbmb1i,ucofa(p,i),fsum(1),1,tint(ijklpt),1)
      do 1200 j=1,i
      call daxpy_wr(mbmb1,ucofa(p,j),fsum(ipt),1,tint(ijklpt),1)
      ijklpt=ijklpt+mbmb1
 1200 continue
      ipt=ipt+mbmb1
 1300 continue
c
 1400 continue
      return
      end
      subroutine inaaaa_o(ucofa,nbfa,norba,tint,fsum,esum)
c
c  this routine performs a full four-index in-core transformation
c  using the bender-shavitt method.  this routine transforms
c  blocks of integrals of the type (a,a : a,a) or 'ttt'.
c  (p,q : r,s) --> (i,j : k,l)
c
c  ucofa is the matrix of mo coefficients for symmetry a
c  tint - final transformed integrals
c  fsum, esum, and dsum are partial sums
c
c  written by frank brown  jan. 22, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer (a-z)
c
      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 ucofa,tint,fsum,esum,dsum
      real*8 int,intbuf
      dimension ucofa(nbfa,norba)
      dimension tint(1),fsum(1),esum(1)
      logical pqflag
      common /buf/ intbuf(1610),intpt,intmx
      common /indx/ iim1(nmotx)
      common /narray/ dsum(nmotx),trang(nmotx),idum(nmotx)
c
      mma=(norba*(norba+1))/2
      mma2=(mma+1)*mma/2
      mamma=norba*mma
c...zero out target array
      call wzero (mma2,tint,1)
c
c  loop over p
c
      do 1400 p=1,nbfa
      call wzero (mamma,fsum,1)
      pqflag=.false.
c
c  loop over q
c
      do 900 q=1,p
      if(p.eq.q) pqflag=.true.
      call wzero (mma,esum,1)
c
c  loop over r. a modification of s. elbert's loop structure is
c  used where the ao integrals have been sorted to allow for a more
c  efficient use of intermediate sums.
c
c  (case a : r = p)
c
      r=p
      call wzero (norba,dsum,1)
c
c  loop over s
c
      if(q+1-r) 30,45,60
c
c  s - loop 1   q,r     q<=r-2
c
c  case a.)  s=q
c
 30   s=q
c...(pq:pq)
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.5d+00
      call daxpy_wr(norba,int,ucofa(s,1),nbfa,dsum(1),1)
c...case b.)  q<s<r
      rm1=r-1
      qp1=q+1
      do 36 s=qp1,rm1
c...(pq:ps)
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      call daxpy_wr(norba,intbuf(intpt),ucofa(s,1),nbfa,dsum(1),1)
 36   continue
c...case c.)  s=r
      s=r
c...(pq:pp)
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.5d+00
      call daxpy_wr(norba,int,ucofa(s,1),nbfa,dsum(1),1)
      go to 80
c
c  s - loop 2   q,r   q=r-1
c
c...(pq:pq) and (pq:pp)
c
 45   do 55 s=q,r
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.5d+00
      call daxpy_wr(norba,int,ucofa(s,1),nbfa,dsum(1),1)
 55   continue
      go to 80
c
c  s - loop 3   q,r   q=r
c
c...(pp:pp)
c
 60   s=r
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.125d+00
      call daxpy_wr(norba,int,ucofa(s,1),nbfa,dsum(1),1)
c
c  loop over k and l
c
 80   klpt=1
      do 100 k=1,norba
      call daxpy_wr(k,ucofa(r,k),dsum(1),1,esum(klpt),1)
      call daxpy_wr(k,dsum(k),ucofa(r,1),nbfa,esum(klpt),1)
      klpt=klpt+k
 100  continue
c
c
      if(p.eq.nbfa) go to 550
c
c
      pp1=p+1
c
c  loop over r   case b.)  r>p and p<>q
c
      if(pqflag) go to 310
c
c
      do 300 r=pp1,nbfa
      call wzero(norba,dsum,1)
c
c  loop over s   case a.)  1<=s<r
c
      if(r.eq.1) go to 200
      rm1=r-1
      do 130 s=1,rm1
c...(pq:rs) and (pq:rq)
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      call daxpy_wr(norba,intbuf(intpt),ucofa(s,1),nbfa,dsum(1),1)
 130  continue
c
c  case b.) s=r
c
 200  s=r
c...(pq:rr)
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.5d+00
      call daxpy_wr(norba,int,ucofa(s,1),nbfa,dsum(1),1)
c
c  loop over k and l
c
      klpt=1
      do 240 k=1,norba
      call daxpy_wr(k,ucofa(r,k),dsum(1),1,esum(klpt),1)
      call daxpy_wr(k,dsum(k),ucofa(r,1),nbfa,esum(klpt),1)
      klpt=klpt+k
 240  continue
 300  continue
      go to 550
c
c  loop over r  case b.)  r>p and p=q
c
 310  do 500 r=pp1,nbfa
      call wzero(norba,dsum,1)
c
c  loop over s  case a.) 1<=s<r
c
      if(r.eq.1) go to 420
      rm1=r-1
      do 350 s=1,rm1
c...(pp:rs)
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.5d+00
      call daxpy_wr(norba,int,ucofa(s,1),nbfa,dsum(1),1)
 350  continue
c
c  case b.) s=r
c
 420  s=r
c...(pp:rr)
      intpt=intpt+1
      if(intpt.gt.intmx) call reints_o
      int=intbuf(intpt)*0.25d+00
      call daxpy_wr(norba,int,ucofa(s,1),nbfa,dsum(1),1)
c
c  loop over k and l
c
      klpt=1
      do 470 k=1,norba
      call daxpy_wr(k,ucofa(r,k),dsum(1),1,esum(klpt),1)
      call daxpy_wr(k,dsum(k),ucofa(r,1),nbfa,esum(klpt),1)
      klpt=klpt+k
 470  continue
 500  continue
 550  continue
c
c  loop over j, k, and l
c
      jklpt=1
      do 800 j=1,norba
      call daxpy_wr(mma,ucofa(q,j),esum(1),1,fsum(jklpt),1)
      jklpt=jklpt+mma
 800  continue
c
 900  continue
c
c  loop over i,j,k, and l
c
      ijklpt=1
      ipt=1
      ij=0
      do 1300 i=1,norba
      jpt=1
      do 1200 j=1,i
      ij=ij+1
      call daxpy_wr(ij,ucofa(p,j),fsum(ipt),1,tint(ijklpt),1)
      call daxpy_wr(ij,ucofa(p,i),fsum(jpt),1,tint(ijklpt),1)
      kpt=ij
      do 1100 k=1,i
      lmax=k
      if(k.eq.i) lmax=j
      call daxpy_wr(lmax,fsum(kpt),ucofa(p,1),nbfa,tint(ijklpt),1)
      call daxpy_wr(lmax,ucofa(p,k),fsum(ij),mma,tint(ijklpt),1)
      ijklpt=ijklpt+lmax
      kpt=kpt+mma
 1100 continue
      jpt=jpt+mma
 1200 continue
      ipt=ipt+mma
 1300 continue
c
 1400 continue
c
      return
      end
      subroutine trainc_o(ijsmix,klsmix,ismtyp,mult,nsym,scrtfl,
     1  incore,rtr,core)
c
c  this routine controls the transformation of all of the
c  symmetry integral blocks which can be transformed in core.
c  the routine then writes the transformed integrals and
c  labels onto the transformed integrals file.
c
      implicit integer (a-z)
c
ctm  start
      integer nbfmxp
      parameter (nbfmxp=1023)
      integer ninfo
      parameter (ninfomx=5)
       integer nenrgymx
      parameter (nenrgymx=5)
       integer nmapmx
       parameter (nmapmx=2)
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx)
     . ,slabelao, bfnlabao
ctm end


      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
c
      real*8 rtr(1),core(1),inbuf,ints,thresh
c
      integer intlbl(4)
c
      integer ijsmix(36),klsmix(36),ismtyp(106),mult(8,8),incore(1)
c
      common /acracy/ thresh
      common /blocks/ nbksym(8),nbknbf(8),nbkorb(8),nbfoff(8),nnbfof(8),
     1   nbf2of(8),orbbff(8),symnbk(8),nfrzc(8),orboff(8)
      save blocks
c
      common /buf/ ints(1610),intpt,intmx
c
c  /buf1/ is used for buffering input integrals and transformed output
c  integrals.
      real*8 cmbuf1, vbuf
c  NOTE, there is a dependency of the buffer sizes to 8bit/16bit
c        packing mode and initial record size of the AO integral file
c        n2ebf(16bit) < n2ebf(8bit)!
c        should be changed!

      parameter(len2e=4096, n2ebf=2730, nipv=4)
      common /buf1/ cmbuf1(len2e),vbuf(n2ebf),lbuf(nipv,n2ebf),
     & inout,ntotot,nrec,trinto
c
      common /indx/ iim1(nmotx)
      common /ocupy/occl(nmotx)
      common /op/ traop(5)
      common /otrb/ iotorb(nmotx)
c
c  function inl(k) gives the position of the k'th 2-e label in the
c  buffer.  function iposl(k) gives the position of the k'th label
c  in the buffer word.
c  inl(k)=(k+nlpw-1)/nlpw
c  iposl(k)=mod(k-1,nlpw)+1
c  where nlpw is the number of labels in each buffer word (=2 for vax)
c
c  initialize some values
c
      intpt=1610
      intmx=1610
      rewind scrtfl
c
c  loop over symmetries
c
      do 1000 psym=1,nsym
      if(nbknbf(symnbk(psym)).eq.0) go to 1000
      psbf=nbknbf(symnbk(psym))
      psob=nbkorb(symnbk(psym))
c
      do 980 qsym=1,psym
      if(nbknbf(symnbk(qsym)).eq.0) go to 980
      qsbf=nbknbf(symnbk(qsym))
      qsob=nbkorb(symnbk(qsym))
      pqsym=mult(psym,qsym)
      ij=iim1(psym)+qsym
c
      do 960 rsym=1,psym
      if(nbknbf(symnbk(rsym)).eq.0) go to 960
      rsbf=nbknbf(symnbk(rsym))
      rsob=nbkorb(symnbk(rsym))
      ssymx=rsym
      if(psym.eq.rsym) ssymx=qsym
c
      do 940 ssym=1,ssymx
      rssym=mult(rsym,ssym)
      if(rssym.ne.pqsym) go to 940
      if(nbknbf(symnbk(ssym)).eq.0) go to 940
      ssbf=nbknbf(symnbk(ssym))
      ssob=nbkorb(symnbk(ssym))
      kl=iim1(rsym)+ssym
      block=ijsmix(ij)+klsmix(kl)
      if(incore(block).ne.-1)go to 940
      itype=ismtyp(block)
      go to (200,400,600,800), itype
c
c  transform symmetry blocks of integrals
c  process transformed integrals
c   1. assign labels to each integral
c   2. pack these labels into an integer word (i,j,k,l)
c   3. put the integral and label in the output buffer
c
c  type (aa:aa)
c
 200  sptint=(psob*(psob+1)/2)*(psob*(psob+1)/2+1)/2
      spfsum=psob*(psob*(psob+1))/2
      tintpt=1
      fsumpt=tintpt+sptint
      esumpt=fsumpt+spfsum
      rtrpt=orbbff(symnbk(psym))+1
      call inaaaa_o(rtr(rtrpt),psbf,psob,core(tintpt),
     1   core(fsumpt),core(esumpt))
c
      int=tintpt-1
      orbof=orboff(symnbk(psym))
      do 380 i=1,psob
      iuoc=0
      if(occl(orbof+i).eq.0) iuoc=1
      intlbl(1)=iotorb(orbof+i)
      do 370 j=1,i
      juoc=iuoc
      if(occl(orbof+j).eq.0) juoc=juoc+1
      intlbl(2)=iotorb(orbof+j)
      do 360 k=1,i
      intlbl(3)=iotorb(orbof+k)
      lmax=k
      if(i.eq.k) lmax=j
      kuoc=juoc
      if(occl(orbof+k).eq.0) kuoc=kuoc+1
      if(kuoc.le.2) go to 330
      int=int+lmax
      go to 360
 330  do 350 l=1,lmax
      int=int+1
      luoc=kuoc
      if(occl(orbof+l).eq.0) luoc=luoc+1
      if(luoc.gt.2) go to 350
      if(abs(core(int)).lt.thresh) go to 350
      intlbl(4)=iotorb(orbof+l)
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=core(int)
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
 350  continue
 360  continue
 370  continue
 380  continue
      go to 940
c
c  type (aa:bb)
c
 400  sptint=(psob*(psob+1)/2)*(rsob*(rsob+1)/2)
      spfsum=psob*(rsob*(rsob+1)/2)
      tintpt=1
      fsumpt=tintpt+sptint
      esumpt=fsumpt+spfsum
      rtrpta=orbbff(symnbk(psym))+1
      rtrptb=orbbff(symnbk(rsym))+1
      call inaabb_o(rtr(rtrpta),psbf,psob,rtr(rtrptb),rsbf,rsob,
     1   core(tintpt),core(fsumpt),core(esumpt))
c
      int=tintpt-1
      orbofa=orboff(symnbk(psym))
      orbofb=orboff(symnbk(rsym))
      do 580 i=1,psob
      iuoc=0
      if(occl(orbofa+i).eq.0) iuoc=1
      intlbl(1)=iotorb(orbofa+i)
      do 570 j=1,i
      juoc=iuoc
      if(occl(orbofa+j).eq.0) juoc=juoc+1
      intlbl(2)=iotorb(orbofa+j)
      do 560 k=1,rsob
      kuoc=juoc
      if(occl(orbofb+k).eq.0) kuoc=kuoc+1
      intlbl(3)=iotorb(orbofb+k)
      if(kuoc.le.2) go to 540
      int=int+k
      go to 560
 540  do 550 l=1,k
      int=int+1
      luoc=kuoc
      if(occl(orbofb+l).eq.0) luoc=luoc+1
      if(luoc.gt.2) go to 550
      if(abs(core(int)).lt.thresh) go to 550
      intlbl(4)=iotorb(orbofb+l)
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=core(int)
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
 550  continue
 560  continue
 570  continue
 580  continue
      go to 940
c
c  type (ab:ab)
c
 600  sptint=psob*qsob*(psob*qsob+1)/2
      spfsum=qsob*psob*qsob
      tintpt=1
      fsumpt=tintpt+sptint
      esumpt=fsumpt+spfsum
      rtrpta=orbbff(symnbk(psym))+1
      rtrptb=orbbff(symnbk(qsym))+1
      call inabab_o(rtr(rtrpta),psbf,psob,rtr(rtrptb),qsbf,qsob,
     1   core(tintpt),core(fsumpt),core(esumpt))
c
      int=tintpt-1
      orbofa=orboff(symnbk(psym))
      orbofb=orboff(symnbk(qsym))
      do 780 i=1,psob
      intlbl(1)=iotorb(orbofa+i)
      iuoc=0
      if(occl(orbofa+i).eq.0) iuoc=1
      do 770 j=1,qsob
      juoc=iuoc
      if(occl(orbofb+j).eq.0) juoc=juoc+1
      intlbl(2)=iotorb(orbofb+j)
      lmax=qsob
      do 760 k=1,i
      intlbl(3)=iotorb(orbofa+k)
      if(i.eq.k) lmax=j
      kuoc=juoc
      if(occl(orbofa+k).eq.0) kuoc=kuoc+1
      if(kuoc.le.2) go to 740
      int=int+lmax
      go to 760
 740  do 750 l=1,lmax
      int=int+1
      luoc=kuoc
      if(occl(orbofb+l).eq.0) luoc=luoc+1
      if(luoc.gt.2) go to 750
      if(abs(core(int)).lt.thresh) go to 750
      intlbl(4)=iotorb(orbofb+l)
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=core(int)
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
 750  continue
 760  continue
 770  continue
 780  continue
      go to 940
c
c  type (ab:cd)
c
 800  sptint=psob*qsob*rsob*ssob
      spfsum=qsob*rsob*ssob
      tintpt=1
      fsumpt=tintpt+sptint
      esumpt=fsumpt+spfsum
      rtrpta=orbbff(symnbk(psym))+1
      rtrptb=orbbff(symnbk(qsym))+1
      rtrptc=orbbff(symnbk(rsym))+1
      rtrptd=orbbff(symnbk(ssym))+1
      call inabcd_o(rtr(rtrpta),psbf,psob,rtr(rtrptb),qsbf,qsob,
     1   rtr(rtrptc),rsbf,rsob,rtr(rtrptd),ssbf,ssob,core(tintpt),
     2   core(fsumpt),core(esumpt))
c
      int=tintpt-1
      orbofa=orboff(symnbk(psym))
      orbofb=orboff(symnbk(qsym))
      orbofc=orboff(symnbk(rsym))
      orbofd=orboff(symnbk(ssym))
      do 880 i=1,psob
      intlbl(1)=iotorb(orbofa+i)
      iuoc=0
      if(occl(orbofa+i).eq.0) iuoc=1
      do 870 j=1,qsob
      intlbl(2)=iotorb(orbofb+j)
      juoc=iuoc
      if(occl(orbofb+j).eq.0) juoc=juoc+1
      do 860 k=1,rsob
      intlbl(3)=iotorb(orbofc+k)
      kuoc=juoc
      if(occl(orbofc+k).eq.0) kuoc=kuoc+1
      if(kuoc.le.2) go to 840
      int=int+ssob
      go to 860
 840  do 850 l=1,ssob
      int=int+1
      luoc=kuoc
      if(occl(orbofd+l).eq.0) luoc=luoc+1
      if(luoc.gt.2) go to 850
      if(abs(core(int)).lt.thresh) go to 850
      intlbl(4)=iotorb(orbofd+l)
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=core(int)
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
 850  continue
 860  continue
 870  continue
 880  continue
 940  continue
 960  continue
 980  continue
 1000 continue
      return
      end
      subroutine reints_o
c
c  this routine reads in one buffer of integrals from the scratch
c  file.
c
      implicit integer (a-z)
      real*8 ints
c
      common /buf/ ints(1610),intpt,intmx
c
      data scrtfl /34/
c
      read(scrtfl) ints
c-      write(6,25) (ints(i),i=1,10)
c-25    format(' reints_o:  buf(1:10) = ',/(1x,10f13.8))
      intpt=1
      return
      end
      subroutine attra1_o(a,nbfa,norba,tr,scr)
c
c  this routine performs the matrix multiplication
c   (a-transpose)(tr)(a).
c
c  a     : the matrix of mo coefficients for symmetry a
c  tr    : a lower triangular distribution of integrals
c  scr   : scratch array of length norba*(norba+1)/2
c
c  written by frank brown feb 10, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer(a-z)
c
      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 a(nbfa,norba),tr(1),scr(1),s1
      real*8 ddot_wr
      common /indx/ iim1(nmotx)
      common /narray/ s1(nmotx),idum(nmotx,2)
c
      ij=0
      do 400 i=1,norba
c
c  form the i'th column of (tr*a) and store in s1(*)
c
      call wzero(nbfa,s1,1)
      s1(1)=tr(1)*a(1,i)
      if(nbfa.eq.1)go to 200
c
c  pq points to the beginning of the p'th row of tr(*)
c
      pq=2
      do 100 p=2,nbfa
      s1(p)=s1(p)+ddot_wr(p,tr(pq),1,a(1,i),1)
      pm1=p-1
      call daxpy_wr(pm1,a(p,i),tr(pq),1,s1(1),1)
      pq=pq+p
100   continue
200   continue
c
c  for the i'th column of (tr*a), form a(j)*(tr*a)(i)
c
      do 300 j=1,i
      ij=ij+1
      scr(ij)=ddot_wr(nbfa,a(1,j),1,s1(1),1)
300   continue
c
400   continue
c
      nnorb=(norba*(norba+1))/2
      call dcopy_wr(nnorb,scr,1,tr,1)
      return
      end
      subroutine trouta_o(rtr,nbfa,norba,pta,nbfb,norbb,ptb,
     1  itype,scr,ints,strt,bufin,nszdis,
     2   nseg,mseg,iwrdim)
c
c  this routine controls the first half of the out-of-core
c  transformations.
c
      implicit integer(a-z)
      common/cnlist/nlist
      real*8 rtr(1),scr(1),ints(1),bufin(1)
c
      common/sort/irecpt(200),ioff(200),ifull(200),
     +  ibufad,iword,nbuk,numel,npbuk,irec
c
      common /imsort/ ihrpim(200),ndpseg(200),mdpseg(200)
      common /op/ traop(5)
c
c-----------------------------------------------------------------------
c
c  function inl(k) gives the position of the k'th label in the
c  working precision array used to hold the labels.
c  in general:  inl(k)=(k+nlpw-1)/nlpw
c  where nlpw is the number of labels per working precision word
c
c  function iposl(k) is complementary to inl(k) and gives the position
c  of the k'th label within the working precision word (from 1 to nlpw)
c  in general:  iposl(k)=mod(k-1,nlpw)+1
c
*@ifdef future
*CC  anyone?
*@else
      inl(k)=(k+1)/2
      iposl(k)=mod(k-1,2)+1
*@endif
c
c-----------------------------------------------------------------------
c
c
c  initialize some variables
c
      nprdis=0
c
c  loop over n-segments
c
      do 5000 inseg=1,nseg
      if(traop(1).ge.1)write(nlist,6010)inseg
6010  format(' trouta_o: inseg=',i5)
      finint=-nprdis*nszdis
      ndpsg=ndpseg(inseg)
      spints=ndpsg*nszdis
      call wzero(spints,ints(1),1)
c
c  read in ao integrals
c
      ircp=irecpt(strt)
      strt=strt+1
c
c  read in record
c
 100  call da1r(bufin,ircp)
c
c  unpack number of integrals and next record pointer
c
      k=npbuk+1
      ilpt=npbuk+inl(k)
      ipos=iposl(k)
      call labget(bufin(ilpt),nin,ipos)
      k=npbuk+2
      ilpt=npbuk+inl(k)
      ipos=iposl(k)
      call labget(bufin(ilpt),ircp,ipos)
c
c  process record
c
      do 200 iin=1,nin
c
c  unpack integral label and process integral
c
      ilpt=npbuk+inl(iin)
      ipos=iposl(iin)
      call labget(bufin(ilpt),lab,ipos)
      lad=lab+finint
      if(lad.lt.1.or.lad.gt.spints) go to 150
      ints(lad)=bufin(iin)
      go to 200
 150  write(nlist,175)spints,lad,iin,nin
 175  format(' trouta_o: spints,lad,iin,nin=',4i10)
 200  continue
      if(ircp.gt.0) go to 100
c
c  transform each distribution
c
      if(itype.gt.2) go to 240
c
c  types (aa:aa) and (aa:bb)
c
      idispt=1
      do 230 idist=1,ndpsg
      call attra1_o(rtr(ptb),nbfb,norbb,ints(idispt),scr)
      idispt=idispt+nszdis
 230  continue
      go to 400
c
c  types (ab:ab) and (ab:cd)
c
 240  idispt=1
      do 250 idist=1,ndpsg
      call atrb1_o(rtr(pta),nbfa,norba,rtr(ptb),nbfb,norbb,
     1   ints(idispt),scr)
      idispt=idispt+nszdis
 250  continue
c
c  write out the half transformed integrals onto the intermediate
c  sort file, da2.
c
 400  gpsize=ndpsg
      starte=nprdis+1
      numgpx=(iwrdim-inl(5))/gpsize
      mprdis=0
c
c  loop over m-segments
c
      do 2000 imseg=1,mseg
      msgknt=mdpseg(imseg)
 500  numgrp=numgpx
      if(numgrp.gt.msgknt) numgrp=msgknt
      msgknt=msgknt-numgrp
      startd=mprdis+1
c
c  initilize output buffer pointer (each output record
c  has five integer labels).  the bufin(*) array is reused for
c  the half transformed integral output buffer.
c
      ibufpt=inl(5)+1
c
c  loop over groups to be put into record
c
      do 1000 igp=1,numgrp
      irpt=mprdis+1
c
c  loop over individual group integrals
c
      do 900 iel=1,gpsize
      bufin(ibufpt)=ints(irpt)
      ibufpt=ibufpt+1
      irpt=irpt+nszdis
 900  continue
      mprdis=mprdis+1
 1000 continue
c
c  fill up first integers in record and write it out
c
      ilpt=inl(1)
      ipos=iposl(1)
      call labput(bufin(ilpt),ihrpim(imseg),ipos)
      ilpt=inl(2)
      ipos=iposl(2)
      call labput(bufin(ilpt),numgrp,ipos)
      ilpt=inl(3)
      ipos=iposl(3)
      call labput(bufin(ilpt),gpsize,ipos)
      ilpt=inl(4)
      ipos=iposl(4)
      call labput(bufin(ilpt),startd,ipos)
      ilpt=inl(5)
      ipos=iposl(5)
      call labput(bufin(ilpt),starte,ipos)
c
c  write out buffer on da2
c
      call da2w(bufin,irecot)
      ihrpim(imseg)=irecot
      if(msgknt.ne.0) go to 500
 2000 continue
c
      nprdis=nprdis+ndpsg
 5000 continue
c
c  close da2 for writing.
c
      call da2cw
      return
      end
      subroutine troutb_o(rtr,nbfa,norba,pta,nbfb,norbb,ptb,
     1   itype,ints,bufin,mszdis,mseg,norbj)
c
c  this routine controls the last half of the out-of-core
c  transformation.
c
      implicit integer(a-z)
c
      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 rtr(1),ints(1),bufin(1)
c
      common /imsort/ ihrpim(200),ndpseg(200),mdpseg(200)
      common /indces/ isoff,jsoff,ksoff,lsoff,intlbl(4)
      common /op/ traop(5)
      common /otrb/ iotorb(nmotx)
c
c
c-----------------------------------------------------------------------
c
c  function inl(k) gives the position of the k'th label in the
c  working precision array used to hold the labels.
c  in general:  inl(k)=(k+nlpw-1)/nlpw
c  where nlpw is the number of labels per working precision word
c
c  function iposl(k) is complementary to inl(k) and gives the position
c  of the k'th label within the working precision word (from 1 to nlpw)
c  in general:  iposl(k)=mod(k-1,nlpw)+1
c
*@ifdef future
*CC  anyone?
*@else
      inl(k)=(k+1)/2
      iposl(k)=mod(k-1,2)+1
*@endif
c
c-----------------------------------------------------------------------
c
c  initialize some variables
c
      i=1
      j=0
      mdpros=0
c
c  open da2 for reading
c
      call da2or
      go to (10,20,30,40), itype
 10   assign 1000 to typgo
      go to 50
 20   assign 1500 to typgo
      go to 50
 30   assign 2000 to typgo
      go to 50
 40   assign 2500 to typgo
 50   continue
c
c  loop over m-segments
c
      do 5000 imseg=1,mseg
      if(traop(1).ge.1)write(nlist,6010)imseg
6010  format(' troutb_o: imseg=',i5)
      mdpsg=mdpseg(imseg)
      irec=ihrpim(imseg)
c
c  read in record
c
 100  call da2r(bufin,irec)
c
c  unpack label from record
c
      ilpt=inl(1)
      ipos=iposl(1)
      call labget(bufin(ilpt),irec,ipos)
      ilpt=inl(2)
      ipos=iposl(2)
      call labget(bufin(ilpt),numgrp,ipos)
      ilpt=inl(3)
      ipos=iposl(3)
      call labget(bufin(ilpt),gpsize,ipos)
      ilpt=inl(4)
      ipos=iposl(4)
      call labget(bufin(ilpt),startd,ipos)
      ilpt=inl(5)
      ipos=iposl(5)
      call labget(bufin(ilpt),starte,ipos)
c
c
c  process record
c
      distpt=(startd-mdpros-1)*mszdis
      bufpt=inl(5)+1
c
c  loop over groups
c
      do 500 igp=1,numgrp
      iintpt=distpt+starte
c
c  loop over elements
c
      do 400 iel=1,gpsize
      ints(iintpt)=bufin(bufpt)
      iintpt=iintpt+1
      bufpt=bufpt+1
 400  continue
c
      distpt=distpt+mszdis
 500  continue
      if(irec.ne.0) go to 100
 205  spints=mszdis*mdpsg
c
c  transform each distribution
c
      go to typgo, (1000,1500,2000,2500)
c
c  type (aa:aa)
c
 1000 idispt=1
      do 1100 idist=1,mdpsg
      j=j+1
      intlbl(3)=iotorb(i+ksoff)
      intlbl(4)=iotorb(j+ksoff)
      call attra3_o(rtr(pta),nbfa,norba,ints(idispt),i,j)
      if(j.lt.i) go to 1050
      i=i+1
      j=0
 1050 idispt=idispt+mszdis
 1100 continue
      go to 4500
c
c  type (aa:bb)
c
 1500 idispt=1
      do 1600 idist=1,mdpsg
      j=j+1
      intlbl(1)=iotorb(i+ksoff)
      intlbl(2)=iotorb(j+ksoff)
      call attra2_o(rtr(pta),nbfa,norba,ints(idispt))
      if(j.lt.i) go to 1550
      i=i+1
      j=0
 1550 idispt=idispt+mszdis
 1600 continue
      go to 4500
c
c  type (ab:ab)
c
 2000 idispt=1
      do 2100 idist=1,mdpsg
      j=j+1
      intlbl(1)=iotorb(j+ksoff)
      intlbl(2)=iotorb(i+lsoff)
      call atrb2_o(rtr(pta),nbfa,norba,rtr(ptb),nbfb,norbb,
     1   ints(idispt),itype,j,i)
      if(j.lt.norbj) go to 2050
      i=i+1
      j=0
 2050 idispt=idispt+mszdis
 2100 continue
      go to 4500
c
c  type (ab:cd)
c
 2500 idispt=1
      do 2600 idist=1,mdpsg
      j=j+1
      intlbl(1)=iotorb(j+ksoff)
      intlbl(2)=iotorb(i+lsoff)
      call atrb2_o(rtr(pta),nbfa,norba,rtr(ptb),nbfb,norbb,
     1   ints(idispt),itype,j,i)
      if(j.lt.norbj) go to 2550
      i=i+1
      j=0
 2550 idispt=idispt+mszdis
 2600 continue
c
 4500 mdpros=mdpros+mdpsg
 5000 continue
c
c  close da2 so that it may be opened with different
c  record length parameters.
c
      call da2cr
      return
      end
      subroutine seg_o(itype,isbf,isob,jsbf,jsob,ksbf,ksob,lsbf,lsob,
     1   incore,iblock,length,iwrdim,nseg,mseg,nszdis,mszdis,avcrm)
c
c  this routine sets up the intermediate sorting and
c  segmentation for the out-of-core transformations.
c  this routine is called for each symmetry block combination of
c  2-e integrals.
c
      implicit integer (a-z)
      common/cnlist/nlist
      dimension length(14),incore(106)
      common /imsort/ ihrpim(200),indpsg(200),imdpsg(200)
      common /op/ traop(5)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c immax = dimension of the arrays in /imsort/
c
      data immax/200/
c
c-----------------------------------------------------------------------
c
c  function inl(k) gives the position of the k'th label in the
c  working precision array used to hold the labels.
c  in general:  inl(k)=(k+nlpw-1)/nlpw
c  where nlpw is the number of labels per working precision word
c
c  function iposl(k) is complementary to inl(k) and gives the position
c  of the k'th label within the working precision word (from 1 to nlpw)
c  in general:  iposl(k)=mod(k-1,nlpw)+1
c
*@ifdef future
*CC  anyone?
*@else
      inl(k)=(k+1)/2
*@endif
c
c-----------------------------------------------------------------------
c
c  check itype
c
      go to (100,200,300,400),itype
c
c  type (aa:aa)
c
 100  nszdis=isbf*(isbf+1)/2
      numdps=incore(iblock)/nszdis
      ndistt=nszdis
      mszdis=nszdis
      mumdps=numdps+1
      mdistt=isob*(isob+1)/2
      go to 500
c
c  type (aa:bb)
c
 200  nszdis=ksbf*(ksbf+1)/2
      numdps=incore(iblock)/nszdis
      ndistt=isbf*(isbf+1)/2
      mszdis=ndistt
      mumdps=avcrm/mszdis
      mdistt=ksob*(ksob+1)/2
      go to 500
c
c  type (ab:ab)
c
 300  nszdis=isbf*jsbf
      numdps=incore(iblock)/nszdis
      ndistt=nszdis
      mszdis=nszdis
      mumdps=numdps+1
      mdistt=ksob*lsob
      go to 500
c
c  type (ab:cd)
c
 400  nszdis=ksbf*lsbf
      numdps=incore(iblock)/nszdis
      ndistt=isbf*jsbf
      mszdis=ndistt
      mumdps=avcrm/mszdis
      mdistt=ksob*lsob
c
c  determine nseg and mseg
c
 500  nseg=(ndistt-1)/numdps+1
      mseg=(mdistt-1)/mumdps+1
      if(nseg.le.immax .and. mseg.le.immax)go to 510
      write(nlist,6010)iblock,nseg,mseg,immax
6010  format(/' seg:  iblock,nseg,mseg,immax=',4i6,' need larger',
     + ' intermediate sorting arrays')
      call bummer('insort: immax=',immax,faterr)
510   continue
c
c  determine indpsg and imdpsg
c
      kntdis=0
      if(nseg.eq.1)go to 530
      nsegm1=nseg-1
      do 520 insg=1,nsegm1
      indpsg(insg)=numdps
      kntdis=kntdis+numdps
 520  continue
 530  indpsg(nseg)=ndistt-kntdis
c
      kntdis=0
      if(mseg.eq.1) go to 550
      msegm1=mseg-1
      do 540 imsg=1,msegm1
      imdpsg(imsg)=mumdps
      kntdis=kntdis+mumdps
 540  continue
 550  imdpsg(mseg)=mdistt-kntdis
c
c  determine iwrdim
c
      outpsg=imdpsg(1)*numdps
      iwrdim=min(outpsg+inl(5),
     +  ((length(1)-inl(5))/numdps)*numdps+inl(5))
      if(iwrdim.lt.70) go to 900
c
c  open da2 for writing
c
      call da2ow(iwrdim)
      go to 950
c
 900  write(nlist,910) iwrdim,nszdis,numdps,mszdis,mumdps,avcrm
 910  format(//' ********** not enough core **********'//
     1' intermediate sort will be inefficient for out-of-core',
     2' transformation.'//' iwrdim,nszdis,numdps,mszdis,mumdps,',
     3'avcrm =',6i8//' ********** program stops execution **********'//)
      call bummer('insort: avcrm=',avcrm,faterr)
 950  if(traop(1).ge.1) write(nlist,960) iblock,nseg,mseg,nszdis,
     1   mszdis,iwrdim
 960  format(/' intermediate sort statistics for block ',i4/
     1' nseg,mseg,nszdis,mszdis,iwrdim = ',2i4,2i6,i6/)
c
c  zero out ihrpim
c
 1000 do 1100 imsg=1,mseg
      ihrpim(imsg)=0
 1100 continue
      return
      end
      subroutine atrb1_o(a,nbfa,norba,b,nbfb,norbb,r,scr)
c
c  this routine performs the matrix multiplication
c   (a-transpose)(r)(b).
c
c  a     : the matrix of mo coefficients for symmetry a
c  b     : the matrix of mo coefficients for symmetry b
c  r     : a rectanglular (square) distribution of integrals
c  scr   : scratch array dimension (norba,norbb)
c
c  written by frank brown feb 9, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer (a-z)
c
      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 a(nbfa,norba),b(nbfb,norbb),r(nbfa,nbfb)
      real*8 scr(norba,norbb),s1
      real*8 ddot_wr
      common /narray/ s1(nmotx),idum(nmotx,2)
c
c  loop over j
c
      do 1000 j=1,norbb
      call wzero(nbfa,s1,1)
c
c  loop over q
c
      do 800 q=1,nbfb
c
      call daxpy_wr(nbfa,b(q,j),r(1,q),1,s1(1),1)
 800  continue
c
c  loop over i
c
      do 900 i=1,norba
      scr(i,j)=ddot_wr(nbfa,a(1,i),1,s1,1)
 900  continue
 1000 continue
c
      nnorb=norba*norbb
      call dcopy_wr(nnorb,scr(1,1),1,r(1,1),1)
      return
      end
      subroutine atrb2_o(a,nbfa,norba,b,nbfb,norbb,r,itype,kref,lref)
c
c  this routine performs the matrix multiplication
c   (a-transpose)(r)(b).
c
c  a     : the matrix of mo coefficients for symmetry a
c  b     : the matrix of mo coefficients for symmetry b
c  r     : a rectangular (square) distribution of integrals
c
c  written by frank brown feb 9, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer (a-z)
c
ctm  start
      integer nbfmxp
      parameter (nbfmxp=1023)
      integer ninfo
      parameter (ninfomx=5)
       integer nenrgymx
      parameter (nenrgymx=5)
       integer nmapmx
       parameter (nmapmx=2)
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx)
     . ,slabelao, bfnlabao
ctm end


      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 a(nbfa,norba),b(nbfb,norbb),r(nbfa,nbfb)
      real*8 ddot_wr
      real*8 val,thresh,s1
      common /acracy/ thresh
c
c  /buf1/ is used for buffering input integrals and transformed output
c  integrals.
      real*8 cmbuf1, vbuf
c  NOTE, there is a dependency of the buffer sizes to 8bit/16bit
c        packing mode and initial record size of the AO integral file
c        n2ebf(16bit) < n2ebf(8bit)!
c        should be changed!

      parameter(len2e=4096, n2ebf=2730, nipv=4)
      common /buf1/ cmbuf1(len2e),vbuf(n2ebf),lbuf(nipv,n2ebf),
     & inout,ntotot,nrec,trinto
c
      common /indces/ isoff,jsoff,ksoff,lsoff,intlbl(4)
      common /narray/ s1(nmotx),idum(nmotx,2)
      common /otrb/ iotorb(nmotx)
      integer occl
      common/ocupy/occl(nmotx)
c
c  function inl(k) gives the position of the k'th 2-e label in the
c  buffer.  function iposl(k) gives the position of the k'th label
c  in the buffer word.
c  inl(k)=(k+nlpw-1)/nlpw
c  iposl(k)=mod(k-1,nlpw)+1
c  where nlpw is the number of labels in each buffer word (=2 for vax)
c
c
      kluoc=0
      if(occl(intlbl(1)).eq.0)kluoc=kluoc+1
      if(occl(intlbl(2)).eq.0)kluoc=kluoc+1
c
c  determine if type (ab:ab) or type (ab:cd)
c
      if(itype.eq.3) go to 1100
c
c  type (ab:cd)
c
c
c  loop over j
c
      do 1000 j1=1,norbb
      intlbl(4)=iotorb(j1+jsoff)
      juoc=kluoc
      if(occl(intlbl(4)).eq.0)juoc=juoc+1
      if(juoc.gt.2)go to 1000
c
      call wzero(nbfa,s1,1)
c
c  loop over q
c
      do 800 q=1,nbfb
c
      call daxpy_wr(nbfa,b(q,j1),r(1,q),1,s1(1),1)
 800  continue
c
c  loop over i
c
      do 900 i1=1,norba
      intlbl(3)=iotorb(i1+isoff)
      iuoc=juoc
      if(occl(intlbl(3)).eq.0)iuoc=iuoc+1
      if(iuoc.gt.2)go to 900
c
      val=ddot_wr(nbfa,a(1,i1),1,s1,1)
      if(abs(val).lt.thresh) go to 900
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=val
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
900   continue
1000  continue
      return
c
c  type (ab:ab)
c
c  loop over j
c
 1100 do 2000 j1=1,lref
      intlbl(4)=iotorb(j1+jsoff)
      juoc=kluoc
      if(occl(intlbl(4)).eq.0)juoc=juoc+1
      if(juoc.gt.2)go to 2000
c
      call wzero(nbfa,s1,1)
c
c  loop over q
c
      do 1800 q=1,nbfb
c
      call daxpy_wr(nbfa,b(q,j1),r(1,q),1,s1(1),1)
 1800 continue
c
c  loop over i
c
      imax=norba
      if(j1.eq.lref) imax=kref
      do 1900 i1=1,imax
      intlbl(3)=iotorb(i1+isoff)
      iuoc=juoc
      if(occl(intlbl(3)).eq.0)iuoc=iuoc+1
      if(iuoc.gt.2)go to 1900
c
      val=ddot_wr(nbfa,a(1,i1),1,s1,1)
      if(abs(val).lt.thresh) go to 1900
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=val
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
1900  continue
2000  continue
      return
      end
      subroutine attra2_o(a,nbfa,norba,tr)
c
c  this routine performs the matrix multiplication
c   (a-transpose)(tr)(a).
c
c  a     : the matrix of mo coefficients for symmetry a
c  tr    : a lower triangular distribution of integrals
c
c  written by frank brown feb 10, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer(a-z)
c
ctm  start
      integer nbfmxp
      parameter (nbfmxp=1023)
      integer ninfo
      parameter (ninfomx=5)
       integer nenrgymx
      parameter (nenrgymx=5)
       integer nmapmx
       parameter (nmapmx=2)
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx)
     . ,slabelao, bfnlabao
ctm end


      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 a(nbfa,norba),tr(1),s1,val,thresh
      real*8 ddot_wr
      common /acracy/ thresh
c
c  /buf1/ is used for buffering input integrals and transformed output
c  integrals.
      real*8 cmbuf1, vbuf
c  NOTE, there is a dependency of the buffer sizes to 8bit/16bit
c        packing mode and initial record size of the AO integral file
c        n2ebf(16bit) < n2ebf(8bit)!
c        should be changed!

      parameter(len2e=4096, n2ebf=2730, nipv=4)
      common /buf1/ cmbuf1(len2e),vbuf(n2ebf),lbuf(nipv,n2ebf),
     & inout,ntotot,nrec,trinto
c
      common /indces/ isoff,jsoff,ksoff,lsoff,intlbl(4)
      common /indx/ iim1(nmotx)
      common /narray/ s1(nmotx),idum(nmotx,2)
      common /otrb/ iotorb(nmotx)
c
      integer occl
      common/ocupy/occl(nmotx)
c
c  function inl(k) gives the position of the k'th 2-e label in the
c  buffer.  function iposl(k) gives the position of the k'th label
c  in the buffer word.
c  inl(k)=(k+nlpw-1)/nlpw
c  iposl(k)=mod(k-1,nlpw)+1
c  where nlpw is the number of labels in each buffer word (=2 for vax)
c
      kluoc=0
      if(occl(intlbl(1)).eq.0)kluoc=kluoc+1
      if(occl(intlbl(2)).eq.0)kluoc=kluoc+1
c
c  loop over j
c
      do 1000 j1=1,norba
      intlbl(4)=iotorb(j1+jsoff)
      juoc=kluoc
      if(occl(intlbl(4)).eq.0)juoc=juoc+1
      if(juoc.gt.2)go to 1000
c
      call wzero(nbfa,s1,1)
c
      s1(1)=tr(1)*a(1,j1)
      if(nbfa.eq.1)go to 400
      pq=2
      do 200 p=2,nbfa
      s1(p)=s1(p)+ddot_wr(p,tr(pq),1,a(1,j1),1)
      pm1=p-1
      call daxpy_wr(pm1,a(p,j1),tr(pq),1,s1(1),1)
      pq=pq+p
200   continue
400   continue
c
c  loop over i
c
      do 900 i1=j1,norba
      intlbl(3)=iotorb(i1+isoff)
      iuoc=juoc
      if(occl(intlbl(3)).eq.0)iuoc=iuoc+1
      if(iuoc.gt.2)go to 900
c
      val=ddot_wr(nbfa,a(1,i1),1,s1,1)
      if(abs(val).lt.thresh) go to 900
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=val
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
900   continue
1000  continue
      return
      end
      subroutine attra3_o(a,nbfa,norba,tr,iref,jref)
c
c  this routine performs the matrix multiplication
c   (a-transpose)(tr)(a) where matrix (tr) is the  'iref,jref'
c   triangular distribution for integrals of symmetry type (aa : aa).
c   this routine uses elbert's loop structure.
c
c  a     : the matrix of mo coefficients for symmetry a
c  tr    : the 'iref,jref' lower triangular distribution of integrals
c
c  written by frank brown  feb 11, 1981
c  modified and adapted by ron shepard
c  blas version written june 12, 1982 by rls
c
      implicit integer (a-z)
c
ctm  start
      integer nbfmxp
      parameter (nbfmxp=1023)
      integer ninfo
      parameter (ninfomx=5)
       integer nenrgymx
      parameter (nenrgymx=5)
       integer nmapmx
       parameter (nmapmx=2)
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx)
     . ,slabelao, bfnlabao
ctm end


      integer   nmotx
      parameter(nmotx=1023)
c
      common/cnlist/nlist
      real*8 a(nbfa,norba),tr(1),s1,val,thresh
      real*8 ddot_wr
      common /acracy/ thresh
c
c  /buf1/ is used for buffering input integrals and transformed output
c  integrals.
      real*8 cmbuf1, vbuf
c  NOTE, there is a dependency of the buffer sizes to 8bit/16bit
c        packing mode and initial record size of the AO integral file
c        n2ebf(16bit) < n2ebf(8bit)!
c        should be changed!

      parameter(len2e=4096, n2ebf=2730, nipv=4)
      common /buf1/ cmbuf1(len2e),vbuf(n2ebf),lbuf(nipv,n2ebf),
     & inout,ntotot,nrec,trinto
c
      common /indces/ isoff,jsoff,ksoff,lsoff,intlbl(4)
      common /indx/ iim1(nmotx)
      common /narray/ s1(nmotx),idum(nmotx,2)
      common /otrb/ iotorb(nmotx)
c
      integer occl
      common/ocupy/occl(nmotx)
c
c  function inl(k) gives the position of the k'th 2-e label in the
c  buffer.  function iposl(k) gives the position of the k'th label
c  in the buffer word.
c  inl(k)=(k+nlpw-1)/nlpw
c  iposl(k)=mod(k-1,nlpw)+1
c  where nlpw is the number of labels in each buffer word (=2 for vax)
c
      ijuoc=0
      if(occl(intlbl(3)).eq.0)ijuoc=ijuoc+1
      if(occl(intlbl(4)).eq.0)ijuoc=ijuoc+1
c
c  loop over k
c
      do 1000 k1=iref,norba
      intlbl(1)=iotorb(k1+isoff)
      kuoc=ijuoc
      if(occl(intlbl(1)).eq.0)kuoc=kuoc+1
      if(kuoc.gt.2)go to 1000
c
      call wzero(nbfa,s1,1)
c
      s1(1)=tr(1)*a(1,k1)
      if(nbfa.eq.1)go to 400
      pq=2
      do 200 p=2,nbfa
      s1(p)=s1(p)+ddot_wr(p,tr(pq),1,a(1,k1),1)
      pm1=p-1
      call daxpy_wr(pm1,a(p,k1),tr(pq),1,s1(1),1)
      pq=pq+p
200   continue
400   continue
c
c  loop over l
c
      lmin=1
      if(k1.eq.iref) lmin=jref
      do 900 l1=lmin,k1
      intlbl(2)=iotorb(l1+jsoff)
      luoc=kuoc
      if(occl(intlbl(2)).eq.0)luoc=luoc+1
      if(luoc.gt.2)go to 900
c
      val=ddot_wr(nbfa,a(1,l1),1,s1,1)
      if(abs(val).lt.thresh) go to 900
      if(inout.eq.info(5))then
         ntotot=ntotot+inout
         nrec=nrec+1
         more=0
         call wtlab2(trinto,len2e,cmbuf1,info(5),vbuf,nipv,lbuf,
     &    inout,more)
      endif
      inout=inout+1
      vbuf(inout)=val
      lbuf(1,inout)=intlbl(1)
      lbuf(2,inout)=intlbl(2)
      lbuf(3,inout)=intlbl(3)
      lbuf(4,inout)=intlbl(4)
900   continue
1000  continue
      return
      end


      subroutine twoao_os(buf,incore,start,
     +  iim1,im1ns,nbfmx,aointf,nsym,
     +  ijsmix,klsmix,ismtyp,sizesg,norbsm,fordint)
c
c  this routine reads in the ao two-electron integrals and sorts
c  them into buckets.
c
      implicit integer (a-z)
      common/cnlist/nlist
      character*(*) fordint

c
c  /buf1/ is used for buffering input integrals and transformed output
c  integrals.
      real*8 cmbuf1, vbuf
c  NOTE, there is a dependency of the buffer sizes to 8bit/16bit
c        packing mode and initial record size of the AO integral file
c        n2ebf(16bit) < n2ebf(8bit)!
c        should be changed!

      parameter (len2e=4096,n2ebf=2730,nipv=4)
      common /buf1/ cmbuf1(len2e),vbuf(n2ebf),lbuf(nipv,n2ebf),
     & inout,ntotot,nrec,trinto
c
      integer nbfmxp
      parameter (nbfmxp=1023)
      integer ninfo
      parameter (ninfomx=5)
       integer nenrgymx
      parameter (nenrgymx=5)
       integer nmapmx
       parameter (nmapmx=2)
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx)
     . ,slabelao, bfnlabao

 
      integer mcpsym,mcqsym,mcrsym,mcssym,nmatl,nnbuf,nmatrd,iflag,mcirc
      integer mclen,imc1,imc2
      real*8, allocatable :: molcasbuffer(:)
      integer pmax,smax,nnmat,molcasbuflen,np(8)
      integer ibufrd,imatrd,ii,irc
      logical ordopened


      integer ierr,itypea,itypeb,ibitv,ifmt
      real*8 val,buf(*)
c
      dimension incore(1),start(1)
      integer ijsmix(36),klsmix(36),ismtyp(106),mult(8,8)
      dimension iim1(1),im1ns(nbfmx,nsym),nnp1s(8),nanb(36)
      integer norbsm(*)
c
      common /blocks/ nbksym(8),nbknbf(8),nbkorb(8),nbfoff(8),
     1   nnbfof(8),nbf2of(8),orbbff(8),symnbk(8),nfrzc(8),orboff(8)
      save blocks
c
      common /op/ traop(5)
c
      data mult /1,2,3,4,5,6,7,8,
     1           2,1,4,3,6,5,8,7,
     2           3,4,1,2,7,8,5,6,
     3           4,3,2,1,8,7,6,5,
     4           5,6,7,8,1,2,3,4,
     5           6,5,8,7,2,1,4,3,
     6           7,8,5,6,3,4,1,2,
     7           8,7,6,5,4,3,2,1/
       data  ordopened /.false./
       save ordopened

c
c  function inl(k) gives the location of the buffer
c  word for the k'th integral label.  function iposl(k) gives
c  the position (from 1 to nlpw) of the k'th label within the buffer
c  word. in general inl(k)=(k+nlpw-1)/nlpw  and iposl(k)=mod(k-1,nlpw)+1
c
c  set up iim1 array
c  must be at least 8 long for symmetry addressing.  (4/28/82  rls)
c
      nnmx=max0(nbfmx*nbfmx,8)
      iim1(1)=0
      do 10 kt=2,nnmx
      iim1(kt)=iim1(kt-1)+kt-1
 10   continue
c
c  set up im1ns
c
      do 30 sym=1,nsym
      nbf=nbknbf(symnbk(sym))
      im1ns(1,sym)=0
      do 20 kt=2,nbfmx
      im1ns(kt,sym)=im1ns(kt-1,sym)+nbf
 20   continue
 30   continue
c
c  set up nnp1s
c
      do 40 sym=1,nsym
      nnp1s(sym)=nbknbf(symnbk(sym))*(nbknbf(symnbk(sym))+1)/2
 40   continue
c
c  set up nanb
c
      ipt=0
      do 43 sym=1,nsym
      nbf1=nbknbf(symnbk(sym))
      do 42 sym2=1,sym
      ipt=ipt+1
      nanb(ipt)=nbf1*nbknbf(symnbk(sym2))
 42   continue
 43   continue
c
c  set norbsm(i) to give the symmetry block of orbital i
c
      do 55 sym=1,nsym
      iblk=symnbk(sym)
      nbf=nbknbf(iblk)
      if(nbf.le.0)go to 55
      ipt=nbfoff(iblk)
      do 50 i=1,nbf
      ipt=ipt+1
      norbsm(ipt)=iblk
50    continue
55    continue
c
      if(traop(1).lt.3)  go to 70
c
c  write out these addressing arrays
c
      write(nlist,45 ) (iim1(kt),kt=1,nnmx)
 45   format(//' from twoao_o : iim1 = ',//(' ',10i10))
      do 47 sym=1,nsym
      write(nlist,46 ) sym,(im1ns(kt,sym),kt=1,nbfmx)
 46   format(//' iim1ns  : sym = ',i5//(' ',10i10))
 47   continue
      write(nlist,48 )(nnp1s(sym),sym=1,nsym)
 48   format(//' nnp1s = ',8i8//)
      ipt=iim1(nsym)+nsym
      write(nlist,49 ) (nanb(it),it=1,ipt)
 49   format(//' nanb = ',//(' ',10i10))
 70   continue
c
c  process 2-e integrals
c

c     # determine maximum buffer length
      molcasbuflen=0
      do psym=1,nsym
       do qsym=1,psym
         do rsym=1,psym
           do ssym=1,rsym
             if (mult(mult(psym,qsym),mult(rsym,ssym)).ne.1) cycle
             if (iim1(psym)+qsym.lt.iim1(rsym)+ssym) cycle
             if (rsym.eq.ssym) then
               nnbuf=nbknbf(rsym)*(nbknbf(rsym)+1)/2
             else
               nnbuf=nbknbf(rsym)*(nbknbf(ssym))
             endif
             if (nnbuf.eq.0) cycle
             if (psym.eq.qsym) then
               nnmat=nbknbf(psym)*(nbknbf(psym)+1)/2
             else
               nnmat=nbknbf(psym)*nbknbf(qsym)
             endif
              molcasbuflen=max(molcasbuflen,nnmat*nnbuf)
          enddo
        enddo
       enddo
      enddo
      molcasbuflen=molcasbuflen+1
c     write(6,*) 'twoao_os: molcasbuflen determined to ',molcasbuflen
      numin2=0
      allocate (molcasbuffer(molcasbuflen+10))
c     if (.not.ordopened) then
c        imc1=0
c         imc2=45
c         call mcOpnOrd(mcirc,imc1,'ORDINT',imc2)
c         if (mcirc.ne.0) 
c    . call bummer('opening ordint failed',0,2)
c         ordopened=.true.
c     endif
      do psym=1,nsym
       do qsym=1,psym
         do rsym=1,psym
           do ssym=1,rsym
             if (mult(mult(psym,qsym),mult(rsym,ssym)).ne.1) cycle
             if (iim1(psym)+qsym.lt.iim1(rsym)+ssym) cycle
             if (rsym.eq.ssym) then
               nnbuf=nbknbf(rsym)*(nbknbf(rsym)+1)/2
             else
               nnbuf=nbknbf(rsym)*(nbknbf(ssym))
             endif
             if (nnbuf.eq.0) cycle
             if (psym.eq.qsym) then
               nnmat=nbknbf(psym)*(nbknbf(psym)+1)/2
             else
               nnmat=nbknbf(psym)*nbknbf(qsym)
             endif
             if (nnmat.eq.0) cycle
             nmatl=(molcasbuflen-1)/nnbuf
             p=1
             q=0
             if (psym.eq.qsym) then
                qmax=p
             else
                qmax=nbknbf(qsym)
             endif

             do ii=1,(nnmat-1)/nmatl+1
             if(ii.eq.1) then
                  iflag=1
             else
                  iflag=2
             endif
             mcpsym=psym
             mcqsym=qsym
             mcrsym=rsym
             mcssym=ssym
             mclen=nmatl*nnbuf+10
         call mcrdord(mcirc,iflag,mcpsym,mcqsym,mcrsym,mcssym,
     .        molcasbuffer, mclen,nmatrd)
              irc=mcirc
             if (irc.ne.0) call bummer('rdord failed',irc,2)
              ioff=0
              do imatrd=1,nmatrd
               q=q+1
               if (q.gt.qmax) then
                  q=1
                  p=p+1
                   if (psym.eq.qsym) qmax=p
               endif
               r=1
               s=0
               if (rsym.eq.ssym) then
                   smax=r
               else
                   smax=nbknbf(ssym)
               endif
               do ibufrd=1,nnbuf
c
c       each buffer contains  all r,s combinations
c
                ioff=ioff+1
                val=molcasbuffer(ioff)
                s=s+1
                if (s.gt.smax) then
                   s=1
                   r=r+1
                   if (rsym.eq.ssym) smax=r
                endif
                if (abs(val).lt.1.d-10) cycle

                 if (psym.eq.rsym .and. p.lt.r) cycle
                 if (psym.eq.rsym .and. qsym.eq.ssym .and. p.eq.r
     .                .and. q.lt.s) cycle
                 numin2=numin2+1

c
c  determine block index
c
c  the addressing scheme was modified 4/13/82 to account
c  for non-canonical ordering of the ao indices. (rls)
c
130   block=ijsmix(iim1(psym)+qsym)+klsmix(iim1(rsym)+ssym)
      itype=ismtyp(block)
      if(incore(block).lt.0)go to 425
c
c  out-of-core types
c
      go to (370,380,390,400),itype
c
c  type (aa:aa)
c
 370  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      lad=(pq-1)*nnp1s(psym)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      if(pq.eq.rs) go to 500
      lad=(rs-1)*nnp1s(psym)+pq
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (aa:bb)
c
 380  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      lad=(pq-1)*nnp1s(rsym)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:ab)
c
 390  pq=im1ns(q,psym)+p
      rs=im1ns(s,psym)+r
      sympr=iim1(psym)+qsym
      lad=(pq-1)*nanb(sympr)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      if(pq.eq.rs) go to 500
      lad=(rs-1)*nanb(sympr)+pq
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:cd)
c
 400  pq=im1ns(q,psym)+p
      rs=im1ns(s,rsym)+r
      sympr=iim1(rsym)+ssym
      lad=(pq-1)*nanb(sympr)+rs
      iblk=(lad-1)/incore(block)+1+start(block)
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  incore types
c
 425  go to (430,440,450,460),itype
c
c  type (aa:aa)
c
 430  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      mpq=max0(pq,rs)
      mrs=min0(pq,rs)
      lad=(mrs-1)*nnp1s(psym)-iim1(mrs)+mpq+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (aa:bb)
c
 440  pq=iim1(max0(p,q))+min0(p,q)
      rs=iim1(max0(r,s))+min0(r,s)
      lad=(pq-1)*nnp1s(rsym)+rs+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:ab)
c
 450  if(p-r) 452,455,458
 452  temp=p
      p=r
      r=temp
      temp=q
      q=s
      s=temp
      go to 458
 455  if(q.ge.s) go to 458
      temp=q
      q=s
      s=temp
 458  continue
      pq=im1ns(p,qsym)+q
      rs=im1ns(r,ssym)+s
      lad=iim1(pq)+rs+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
      go to 500
c
c  type (ab:cd)
c
 460  pq=im1ns(p,qsym)+q
      rs=im1ns(r,ssym)+s
      sympr=iim1(rsym)+ssym
      lad=(pq-1)*nanb(sympr)+rs+start(block)
      iblk=(lad-1)/sizesg+1
      call sortot_o(buf,val,lad,iblk)
c
 500  continue
             enddo
            enddo
           enddo
          enddo
         enddo
        enddo
       enddo
      deallocate(molcasbuffer)
      write(nlist,600) numin2
 600  format(/' twoao_o processed ',i10,' two electron ao integrals.')
      return
      end
