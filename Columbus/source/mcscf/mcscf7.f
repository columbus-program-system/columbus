!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf7.f
cmcscf part=7 of 10.  formula file related routines.
cversion=5.6 last modified: 16-jan-2001
      subroutine sdmat(
     & symw,    nsv,    ism,    yb,
     & yk,      xbar,   xp,     z,
     & r,       ind,    val,    icode,
     & hvect,   tden,   ncsf,   qt,
     & qind,    navst)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for off-diagonal loops.
c
c  tden(n,ij)  = (1/2)*<mc| eij + eji |n>
c  tden(n,ijkl)= (1/4)*<mc| eijkl + ejikl + eijlk + ejilk |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
cmb   changes due to the density matrix averaging -2 new dummy parameter
cmb   navst, wavst added; hvect, tden redimensioned
c
       implicit none
c  ##  parameter & common section
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      real*8 half,fourth
      parameter(half=5d-1, fourth=25d-2)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c****************************************************************
c
c  ##  integer section
c
      integer nsv
c
      integer b, bmax
      integer db
      integer icode,ind(*),iop,igo, isv, i, ib, ik, ism(nsv), ix, ixx
      integer hsym
      integer j
      integer k
      integer nwalk,ncsf,navst
      integer r(*)
      integer symw
      integer tsym
      integer xbar(nsym),xp(nsym),xt,xbh
      integer yb(nsv),yk(nsv), ytb, ytk
      integer z(nsym),zt
c
c  ##  real*8 section
c
      real*8 emc_ave
      real*8 hvect(ncsf,navst)
      real*8 t(3),tden(ncsf,navst,3)
      real*8 val(3)
c
c  ##  logical section
c
      logical qt(3),qind,qtx(3)
c
c----------------------------------------------------------------
c
c  account for factors and assign the go to variable.
c
      iop=icode-1
      go to(2,3,4,5,6,7,8,9,10,11,12,13,14,15),iop
c
2     continue
c  iiil, illl, il
      igo = 7
      t(1)=half*val(1)
      t(2)=half*val(2)
      t(3)=half*val(3)
      qt(1)=.true.
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
3     continue
c  ijlk, iklj
      igo = 4
      t(1)=fourth*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
4     continue
c  ijkl, ilkj
      igo = 5
      t(1)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
5     continue
c  ikjl, iljk
      igo = 6
      t(2)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
6     continue
c  iikl, ilki
c  ijll, illj
      igo = 4
      t(1)=half*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
7     continue
c  ikkl, ilkk
      igo = 5
      t(1)=fourth*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
8     continue
c  iiil, il
      igo = 5
      t(1)=half*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
9     continue
c  illl, il
      igo = 6
      t(2)=half*val(1)
      t(3)=half*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
10    continue
c  ijkl
c  ijlk
c  ikkl
c  iklk
      igo = 1
      t(1)=fourth*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
11    continue
c  iikl
c  ijll
      igo = 1
      t(1)=half*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
12    continue
c  iklj
c  ikjl
c  ilki
c  ikli
c  ilik
c  illj
c  iljl
      igo = 2
      t(2)=fourth*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
13    continue
c  illi
c  half*ilil
      igo = 2
      t(2)=half*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
14    continue
c  ilkj
c  iljk
      igo = 3
      t(3)=fourth*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
15    continue
c  ilkk
c  il
      igo = 3
      t(3)=half*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
      ytb=yb(isv)
      ytk=yk(isv)
      tsym=ism(isv)
      xt=xp(tsym)
      zt=z(tsym)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym)
c
      if(qind)then
c
c  csf selection case.  ind(*) vector is required.
c
      do 1200 j=1,xt
      b=ytb+r(zt+j)
      k=ytk+r(zt+j)
      bmax=ytb+r(zt+j)+xbh
100   continue
      b=b+1
      k=k+1
110   continue
      if(b.gt.bmax)go to 1200
      ib=ind(b)
      ik=ind(k)
      db=min(ib,ik)
      if(db.gt.0)go to (1001,1002,1003,1012,1013,1023,1123),igo
      b=b-db
      k=k-db
      go to 110
c
cmb   changes...
1001  continue
      do 2001 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
2001  tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      go to 100
c
1002  continue
      do 2002 ixx=1,navst
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
2002  tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
c
1003  continue
      do 2003 ixx=1,navst
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2003  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1012  continue
      do 2012 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
2012  tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
c
1013  continue
      do 2013 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2013  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1023  continue
      do 2023 ixx=1,navst
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
      tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2023  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1123  continue
      do 2123 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
      tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2123  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
      do 1240 ix=1,3
c
      if(qtx(ix))then
      do 1230 j=1,xt
      ib=ytb+r(zt+j)
      ik=ytk+r(zt+j)
      do 1230 ixx=1,navst
      do 1210 i=1,xbh
1210  tden(ik+i,ixx,ix)=tden(ik+i,ixx,ix)+hvect(ib+i,ixx)*t(ix)
      do 1220 i=1,xbh
1220  tden(ib+i,ixx,ix)=tden(ib+i,ixx,ix)+hvect(ik+i,ixx)*t(ix)
1230  continue
      endif
c
1240  continue
c
      endif
c
c
1300  continue
c
      return
      end
c
c
      subroutine dmat(
     & symw,    nsv,    ism,    yb,
     & yk,      xbar,   xp,     z,
     & r,       ind,    val,    icode,
     & hvect,   tden,   atden, ncsf,   qt,
     & qind,    navst)
c  adapted from sdmat by felix plasser
c  15-sep-09
c  this routine could possibly be called also by hbcon
c   with atden as a dummy argument
c   then sdmat could be eliminated.
c
c  construct the symmetric one- and two-particle and antisymmetric
c  one-particle transition density
c  matrix element contributions for off-diagonal loops.
c
c  tden(n,ij)  = (1/2)*<mc| eij + eji |n>
c  atden(n,ij)  = (1/2)*<mc| eij - eji |n>
c  tden(n,ijkl)= (1/4)*<mc| eijkl + ejikl + eijlk + ejilk |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
       implicit none
c  ##  parameter & common section
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      real*8 half,fourth
      parameter(half=5d-1, fourth=25d-2)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer dst,ndens,densind,ndbra,brai,brinv
      common/dens/dst,ndens,densind(2,mxavst),ndbra,brai(mxavst),
     & brinv(mxavst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c****************************************************************
c
c  ##  integer section
c
      integer nsv
c
      integer b, bmax
      integer db
      integer icode,ind(*),iop,igo, isv, i, ib, ik, ism(nsv), ix, ixx
      integer hsym
      integer j
      integer k
      integer nwalk,ncsf,navst
      integer r(*)
      integer symw
      integer tsym
      integer xbar(nsym),xp(nsym),xt,xbh
      integer yb(nsv),yk(nsv), ytb, ytk
      integer z(nsym),zt
c
c  ##  real*8 section
c
      real*8 emc_ave
      real*8 hvect(ncsf,navst)
      real*8 t(3),tden(ncsf,ndbra,3),atden(ncsf,ndbra)
      real*8 val(3)
c
c  ##  logical section
c
      logical qt(3),qind,qtx(3)
c
c----------------------------------------------------------------
c
c  account for factors and assign the go to variable.
c
      iop=icode-1
      go to(2,3,4,5,6,7,8,9,10,11,12,13,14,15),iop
c
2     continue
c  iiil, illl, il
      igo = 7
      t(1)=half*val(1)
      t(2)=half*val(2)
      t(3)=half*val(3)
      qt(1)=.true.
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
3     continue
c  ijlk, iklj
      igo = 4
      t(1)=fourth*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
4     continue
c  ijkl, ilkj
      igo = 5
      t(1)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
5     continue
c  ikjl, iljk
      igo = 6
      t(2)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
6     continue
c  iikl, ilki
c  ijll, illj
      igo = 4
      t(1)=half*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
7     continue
c  ikkl, ilkk
      igo = 5
      t(1)=fourth*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
8     continue
c  iiil, il
      igo = 5 
      t(1)=half*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
9     continue
c  illl, il
      igo = 6
      t(2)=half*val(1)
      t(3)=half*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
10    continue
c  ijkl
c  ijlk
c  ikkl
c  iklk
      igo = 1
      t(1)=fourth*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
11    continue
c  iikl
c  ijll
      igo = 1
      t(1)=half*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
12    continue
c  iklj
c  ikjl
c  ilki
c  ikli
c  ilik
c  illj
c  iljl
      igo = 2
      t(2)=fourth*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
13    continue
c  illi
c  half*ilil
      igo =3 
      t(2)=half*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
14    continue
c  ilkj
c  iljk
      igo = 3 
      t(3)=fourth*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
15    continue
c  ilkk
c  il
      igo = 3
      t(3)=half*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
      ytb=yb(isv)
      ytk=yk(isv)
      tsym=ism(isv)
      xt=xp(tsym)
      zt=z(tsym)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym)
c
      if(qind)then
c
c  csf selection case.  ind(*) vector is required.
c
      do 1200 j=1,xt
      b=ytb+r(zt+j)
      k=ytk+r(zt+j)
      bmax=ytb+r(zt+j)+xbh
100   continue
      b=b+1
      k=k+1
110   continue
      if(b.gt.bmax)go to 1200
      ib=ind(b)
      ik=ind(k)
      db=min(ib,ik)
      if(db.gt.0)go to (1001,1002,1003,1012,1013,1023,1123), igo
      b=b-db
      k=k-db
      go to 110
c
cmb   changes...
cfp: anti-symmetric contributions added
1001  continue
      do 2001 ixx=1,ndbra
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,brai(ixx))*t(1)
2001  tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,brai(ixx))*t(1)
      go to 100
c
1002  continue
      do 2002 ixx=1,ndbra
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,brai(ixx))*t(2)
2002  tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,brai(ixx))*t(2)
      go to 100
c
1003  continue
      do 2003 ixx=1,ndbra
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,brai(ixx))*t(3)
      tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,brai(ixx))*t(3)
      atden(ib,ixx)=atden(ib,ixx)+hvect(ik,brai(ixx))*t(3)
2003  atden(ik,ixx)=atden(ik,ixx)-hvect(ib,brai(ixx))*t(3)
      go to 100
c
1012  continue
      do 2012 ixx=1,ndbra
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,brai(ixx))*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,brai(ixx))*t(1)
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,brai(ixx))*t(2)
2012  tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,brai(ixx))*t(2)
      go to 100
c
1013  continue
      do 2013 ixx=1,ndbra
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,brai(ixx))*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,brai(ixx))*t(1)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,brai(ixx))*t(3)
      tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,brai(ixx))*t(3)
      atden(ib,ixx)=atden(ib,ixx)+hvect(ik,brai(ixx))*t(3)
2013  atden(ik,ixx)=atden(ik,ixx)-hvect(ib,brai(ixx))*t(3)
      go to 100
c
1023  continue
      do 2023 ixx=1,ndbra
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,brai(ixx))*t(2)
      tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,brai(ixx))*t(2)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,brai(ixx))*t(3)
      tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,brai(ixx))*t(3)
      atden(ib,ixx)=atden(ib,ixx)+hvect(ik,brai(ixx))*t(3)
2023  atden(ik,ixx)=atden(ik,ixx)-hvect(ib,brai(ixx))*t(3)
      go to 100
c
1123  continue
      do 2123 ixx=1,ndbra
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,brai(ixx))*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,brai(ixx))*t(1)
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,brai(ixx))*t(2)
      tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,brai(ixx))*t(2)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,brai(ixx))*t(3)
      tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,brai(ixx))*t(3)
      atden(ib,ixx)=atden(ib,ixx)+hvect(ik,brai(ixx))*t(3)
2123  atden(ik,ixx)=atden(ik,ixx)-hvect(ib,brai(ixx))*t(3)
      go to 100
c
1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
      do 1240 ix=1,3
c
      if(qtx(ix))then
      do 1230 j=1,xt
      ib=ytb+r(zt+j)
      ik=ytk+r(zt+j)
      do 1230 ixx=1,ndbra
      do 1210 i=1,xbh
1210  tden(ik+i,ixx,ix)=tden(ik+i,ixx,ix)+hvect(ib+i,brai(ixx))*t(ix)
      do 1220 i=1,xbh
1220  tden(ib+i,ixx,ix)=tden(ib+i,ixx,ix)+hvect(ik+i,brai(ixx))*t(ix)
1230  continue
      endif
1240  continue
c
cfp antisymmetric contributions
      if(qtx(3))then
      do 1231 j=1,xt
      ib=ytb+r(zt+j)
      ik=ytk+r(zt+j)
      do 1231 ixx=1,ndbra
      do 1211 i=1,xbh
1211  atden(ik+i,ixx)=atden(ik+i,ixx)+hvect(ib+i,brai(ixx))*t(3)
      do 1221 i=1,xbh
1221  atden(ib+i,ixx)=atden(ib+i,ixx)-hvect(ik+i,brai(ixx))*t(3)
1231  continue
      endif
c
      endif
c
1300  continue
c
      return
      end
c
      subroutine sdmatd(symw,nsv,ism,yb,xbar,xp,z,r,ind,
     +  val,icode,hvect,tden,ncsf,qind,navst)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for diagonal loops.
c
c  tden(n,ii)  =<mc| eii   |n>
c  tden(n,iill)=<mc| eiill |n>
c  tden(n,ilil)=(1/2)* <mc| eilli |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  modifications to make it compatible with rdft_grd
c    -felix plasser, 22-dec-09
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
cmb   changes due to the density matrix averaging -2 new dummy parameter
cmb   navst, wavst added; hvect, tden redimensioned

      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)

      real*8 emc_ave
c

c****************************************************************
c     avepar.h
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c****************************************************************
c
      integer nwalk
c

c****************************************************************
c

c****************************************************************
c     drtf.h
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c****************************************************************
c

c****************************************************************
c     counter.h
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
c
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c****************************************************************
c
      integer ism(nsv),yb(nsv)
      integer xbar(nsym),xp(nsym),z(nsym)
c
cfp
      integer dst,ndens,densind,ndbra,brai,brinv
      common/dens/dst,ndens,densind(2,mxavst),ndbra,brai(mxavst),
     & brinv(mxavst)
c      real*8 val(3),hvect(ncsf,navst),tden(ncsf,navst,3)
      real*8 val(3),hvect(ncsf,navst),tden(ncsf,ndbra,3)
c
      integer r(*),ind(*)
      logical qind,qtx(2)
      real*8 t(2),half,two
      parameter(half=5d-1, two=2d0)
c
c  account for factors and assign the go-to variable.
c
      iop=icode-1
      go to(2,3,4,5,6,7),iop
c
2     continue
c  iill, illi
      t(1)=val(1)
      t(2)=half*val(2)
      igo = 3
      qtx(1)=.true.
      qtx(2)=.true.
      go to 50
3     continue
c  iill
      t(1)=val(1)
      igo = 1
      qtx(1)=.true.
      qtx(2)=.false.
      go to 50
4     continue
c  illi
      t(2)=half*val(1)
      igo = 2
      qtx(1)=.false.
      qtx(2)=.true.
      go to 50
c
5     continue
c  half*llll, ll
      t(1)=two*val(1)
      t(2)=val(2)
      igo = 3
      qtx(1)=.true.
      qtx(2)=.true.
      go to 50
6     continue
c  half*llll
      t(1)=two*val(1)
      igo = 1
      qtx(1)=.true.
      qtx(2)=.false.
      go to 50
7     continue
c  ll
      t(2)=val(1)
      igo = 2
      qtx(1)=.false.
      qtx(2)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
      ytb=yb(isv)
      tsym=ism(isv)
      xt=xp(tsym)
      zt=z(tsym)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym)
c
      if(qind)then
c
c  csf selection case.  ind(*) is required.
c
      do 1200 j=1,xt
      b=ytb+r(zt+j)
      bmax=ytb+r(zt+j)+xbh
100   continue
      b=b+1
110   continue
      if(b.gt.bmax)go to 1200
      ib=ind(b)
      if(ib.gt.0)go to (1001,1002,1012), igo 
      b=b-ib
      go to 110
c
1001  continue
      do 2001 ixx=1,ndbra
2001  tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ib,brai(ixx))*t(1)
      go to 100
1002  continue
      do 2002 ixx=1,ndbra
2002  tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ib,brai(ixx))*t(2)
      go to 100
1012  continue
      do 2012 ixx=1,ndbra
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ib,brai(ixx))*t(1)
2012  tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ib,brai(ixx))*t(2)
      go to 100
c
1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
      do 1240 ix=1,2
c
      if(qtx(ix))then
      do 1230 j=1,xt
      ib=ytb+r(zt+j)
      do 1210 ixx=1,ndbra
      do 1210 i=1,xbh
1210  tden(ib+i,ixx,ix)=tden(ib+i,ixx,ix)+hvect(ib+i,brai(ixx))*t(ix)
1230  continue
      endif
c
1240  continue
c
      endif
c
1300  continue
c
      return
      end
      subroutine hmcft(qod,repnuc,ecore0,ftbuf,
     +  lenbft,modrt,uaa,h2,off1,addaa,ncsf,
     +  xp,z,xbar,rev,ind,
     +  hdiag,hmc,qind,ist)
c
c  read the diagonal- and off-diagonal formula tapes and construct
c  the diagonal and off-diagonal hamiltonian matrix elements.
c
c  arguments:
c  qod     = flag for constructing off-diagonal elements.
c  repnuc  = nuclear repulsion and frozen core energy.
c  ecore0  = inactive-orbital contribution to the core energy.
c  ftbuf(*)= ft buffer.
c  lenbft  = length of ft buffer.
c  modrt   = level to orbital mapping.
c  uaa(*)  = one-electron (+frozen-core +inactive) hamiltonian.
c  h2(*)   = two-electron integrals over active orbitals.
c  off1(*) = h2 addressing array.
c  addaa(*)= h2 addressing array.
c  ncsf    = number of csfs in the wave function expansion.
c  xp(*)   = drt array.
c  z(*)    = drt array.
c  xbar(*) = drt array.
c  rev(*)  = drt array.
c  ind(*)  = drt array.
c  hdiag(*)= diagonal elements of hmc.
c  hmc(*)  = lower-triangular packed hmc matrix.
c  qind    = true if ind(*) must be used.
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           11ab       ppqq, pqqp
c   3           11a        ppqq
c   4           11b        pqqp
c
c   5           14ab       half*pppp, pp
c   6           14a        half*pppp
c   7           14b        pp
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      sssp, sppp, sp
c   3           2a'b'      srpq, sqpr
c   4           1ab        srqp, spqr
c   5           3ab        sqrp, sprq
c   6           4ab        ssqp, spqs
c               8ab        srpp, sppr
c   7           6a'b'      sqqp, spqq
c   8           12ac       sssp, sp
c   9           12bc       sppp, sp
c  10           1a         srqp
c               2a'        srpq
c               6a'        sqqp
c               7'         sqpq
c  11           4a         ssqp
c               8a         srpp
c  12           2b'        sqpr
c               3a         sqrp
c               4b         spqs
c               4b'        sqps
c               5          spsq
c               8b         sppr
c               10         sprp
c  13           11b        spps
c               13         half*spsp
c  14           1b         spqr
c               3b         sprq
c  15           6b'        spqq
c               12c        sp
c==================================================
c
c  written by ron shepard.
c  explicit variable declaration: 15-apr-02, m.dallos
c  verstion date: 20-mar-85
c
       implicit none
c  ##  parameter & common block section
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist, nft, ndft
      equivalence (iunits(1),nlist)
      equivalence (iunits(10),nft)
      equivalence (iunits(11),ndft)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8),nact
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)

      real*8 zero
      parameter(zero=0d0)
c
      integer nmotx
       parameter (nmotx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c  ##  integer section
c
      integer addaa(*)
      integer b,bmax
      integer db
      integer event
      integer ism(8),igd(0:7),igod(0:15),ind(*),i,ist,icode,isv,ib,
     &        ii,ik,ik1,ik2,ikx,ibik,ism2(8)
      integer j
      integer head,ht,hsym
      integer kntft,k
      integer lenbft
      integer modrt(0:*)
      integer ncsf,nw,nv,nsv,nncsf
      integer off1(*)
      integer p,pl,psym,pp,pppp,pp1,ppqq,pq,pqpq,p0,pqsym,pr,ps,pqrs,
     &        prqs,psqr,pqrsym
      integer q,ql,qqq,qr,qq,qs
      integer rev(*),rl,r,rs
      integer symw,sl,s,ss,sp,sssp,sppp,sp1
      integer tail,tsym
      integer xp(nsym,*),xbar(nsym,*),xt,xbh
      integer yb(8),yk(8),ytb,ytk
      integer z(nsym,*),zt,spindens
c
c  ##  real*8
c
      real*8 ecore0
      real*8 ftbuf(lenbft)
      real*8 htrip(3),h2(*),hdiag(*),hmc(*)
      real*8 term
      real*8 repnuc
      real*8 uaa(*)
      real*8 val(3)
c
c  ##  logical section
c
      logical qod,qind
c
c  ##  character section
c
      character*60 fname1,fname2
c
c  ##  data section
c
      data igd/1,2,5,3,4,5,3,4/
      data igod/1,2,9,6,7,8,6,7,7,8,3,3,4,4,5,5/
c
c-----------------------------------------------------------------------
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      if (nst.eq.1) then
         fname1 = 'mcdftfl'
         fname2 = 'mcoftfl'
      elseif (ist.ge.10) then
         write(fname1,'(a8,i2)')'mcdftfl.',ist
         write(fname2,'(a8,i2)')'mcoftfl.',ist
      else
         write(fname1,'(a8,i1)')'mcdftfl.',ist
         write(fname2,'(a8,i1)')'mcoftfl.',ist
      endif
      call trnfln( 1, fname1 )
      call trnfln( 1, fname2 )
      open(unit=ndft,file=fname1,status='old',form='unformatted')
      open(unit=nft,file=fname2,status='old',form='unformatted')
c
c  diagonal elements:
c
      do 10 i=1,ncsf
10    hdiag(i)=(repnuc+ecore0)
c
      rewind ndft
      kntft=1
      nw=0
      read(ndft)ftbuf
c
      do 320 pl=1,nact
      p=orbidx(modrt(pl))
      psym=symm(modrt(pl))
      pp=nndx(p+1)
      pppp=off1(pp)+addaa(pp)
      pp1=nnsa(psym)+nndx(p-nsa(psym)+1)
c
      do 310 ql=1,pl
      q=orbidx(modrt(ql))
c
c  move integrals into htrip(*).
c
      if(pl.eq.ql)then
         htrip(1)=h2(pppp)
         htrip(2)=uaa(pp1)
      else
         qq=nndx(q+1)
         ppqq=off1(max(pp,qq))+addaa(min(pp,qq))
         pq=nndx(max(p,q))+min(p,q)
         pqpq=off1(pq)+addaa(pq)
         htrip(1)=h2(ppqq)
         htrip(2)=h2(pqpq)
      endif
c
90    continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2, nwalk_f(ist),spindens )
c
      go to (100,310,101,102,112),igd(icode)
c
100   kntft=1
      nw=0
      read(ndft)ftbuf
      go to 90
101   term=val(1)*htrip(1)
      go to 120
102   term=val(1)*htrip(2)
      go to 120
112   term=val(1)*htrip(1)+val(2)*htrip(2)
c
120   if(term.eq.zero)go to 90
c
c  for each symmetry type, perform upper and lower walks.
c
      do 260 isv=1,nsv
      ytb=yb(isv)
      tsym=ism(isv)
      xt=xp(tsym,tail)
      zt=z(tsym,tail)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym,head)
c
      if(qind)then
c
c  csf selection case...
c
         do 220 j=1,xt
            b=ytb+rev(zt+j)
            bmax=b+xbh
200         continue
            b=b+1
210         continue
            if(b.gt.bmax)go to 220
            ib=ind(b)
            if(ib.gt.0)then
               hdiag(ib)=hdiag(ib)+term
               go to 200
            else
               b=b-ib
               go to 210
            endif
c
220      continue
      else
c
c  no csf selection case...
c  loops over upper and lower walks reduce to diagonal sequences
c  of csf indices.
c
         do 250 j=1,xt
            ib=ytb+rev(zt+j)
            do 240 i=1,xbh
               hdiag(ib+i)=hdiag(ib+i)+term
240         continue
250      continue
      endif
c
260   continue
c
      go to 90
c
310   continue
320   continue
c
c  hdiag(*) construction complete.
c
      if(flags(3))call timer('hdiag(*) construction',2,event,nlist)
c
      if(qod)then
c
c  construct the full hmc(*) matrix.
c
      nncsf=(ncsf*(ncsf+1))/2
      call wzero(nncsf,hmc,1)
c
      ii=0
      do 980 i=1,ncsf
      ii=ii+i
980   hmc(ii)=hdiag(i)
c
      rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 1440 pl=2,nact
      p=orbidx(modrt(pl))
      psym=symm(modrt(pl))
      pp=nndx(p+1)
      p0=nsa(psym)
c
      do 1430 ql=1,pl
      q=orbidx(modrt(ql))
      pqsym=mult(symm(modrt(ql)),psym)
      pq=nndx(max(p,q))+min(p,q)
c
      do 1420 rl=1,ql
      r=orbidx(modrt(rl))
      pqrsym=mult(symm(modrt(rl)),pqsym)
      pr=nndx(max(p,r))+min(p,r)
      qr=nndx(max(q,r))+min(q,r)
c
      do 1410 sl=1,rl
      if(sl.eq.ql)go to 1420
      if(symm(modrt(sl)).ne.pqrsym)go to 1410
      s=orbidx(modrt(sl))
c
c  move integrals into htrip(*) in triple-sort order.
c
      if(rl.eq.pl)then
         ss=nndx(s+1)
         sp=nndx(max(s,p))+min(s,p)
         sssp=off1(max(ss,sp))+addaa(min(ss,sp))
         sppp=off1(max(sp,pp))+addaa(min(sp,pp))
         sp1=nnsa(psym)+nndx(max(s-p0,p-p0))+min(s-p0,p-p0)
         htrip(1)=h2(sssp)
         htrip(2)=h2(sppp)
         htrip(3)=uaa(sp1)
      else
         rs=nndx(max(r,s))+min(r,s)
         qs=nndx(max(q,s))+min(q,s)
         ps=nndx(max(p,s))+min(p,s)
         pqrs=off1(max(pq,rs))+addaa(min(pq,rs))
         prqs=off1(max(pr,qs))+addaa(min(pr,qs))
         psqr=off1(max(ps,qr))+addaa(min(ps,qr))
         htrip(1)=h2(pqrs)
         htrip(2)=h2(prqs)
         htrip(3)=h2(psqr)
      endif
c
990   continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2,nwalk_f(ist), spindens )
c
      go to (1000,1410,1001,1002,1003,1012,1013,1023,1123),igod(icode)
c
1000  kntft=1
      nw=0
      read(nft)ftbuf
      go to 990
1001  term=val(1)*htrip(1)
      go to 1200
1002  term=val(1)*htrip(2)
      go to 1200
1003  term=val(1)*htrip(3)
      go to 1200
1012  term=val(1)*htrip(1)+val(2)*htrip(2)
      go to 1200
1013  term=val(1)*htrip(1)+val(2)*htrip(3)
      go to 1200
1023  term=val(1)*htrip(2)+val(2)*htrip(3)
      go to 1200
1123  term=val(1)*htrip(1)+val(2)*htrip(2)+val(3)*htrip(3)
c
1200  if(term.eq.zero)go to 990
c
c  for each symmetry type, perform upper and lower walks.
c
      do 1360 isv=1,nsv
      ytb=yb(isv)
      ytk=yk(isv)
      tsym=ism(isv)
      xt=xp(tsym,tail)
      zt=z(tsym,tail)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym,head)
c
      if(qind)then
c
c  csf selection case...
c
         do 1320 j=1,xt
            b=ytb+rev(zt+j)
            k=ytk+rev(zt+j)
            bmax=b+xbh
1300        continue
            b=b+1
            k=k+1
1310        continue
            if(b.gt.bmax)go to 1320
            ib=ind(b)
            ik=ind(k)
            db=min(ib,ik)
            if(db.gt.0)then
               ibik=(ik*(ik-1))/2+ib
               hmc(ibik)=hmc(ibik)+term
               go to 1300
            else
               b=b-db
               k=k-db
               go to 1310
            endif
c
1320     continue
      else
c
c  no csf selection case...
c  loops over upper and lower walks reduce to co-diagonal sequences
c  of csf indices.
c
         do 1350 j=1,xt
            ib=ytb+rev(zt+j)
            ik=ytk+rev(zt+j)
            ibik=(ik*(ik-1))/2+ib
            ik1=ik+1
            ik2=ik+xbh
            do 1340 ikx=ik1,ik2
               ibik=ibik+ikx
               hmc(ibik)=hmc(ibik)+term
1340        continue
1350     continue
      endif
c
1360  continue
c
      go to 990
c
1410  continue
1420  continue
1430  continue
1440  continue
c
c  hmc(*) construction complete.
c
      endif
c
      close(unit=ndft)
      close(unit=nft)
c
      if(flags(3))call timer('hmcft',4,event,nlist)
      return
      end
c
c****************************************************************
c
      subroutine hmcxv(qinc,nvec,ncsf,hmc,hdiag,v,hv,ftbuf,
     +  lenbft,modrt,uaa,h2,off1,addaa,
     +  xp,z,xbar,rev,ind,qind,ist)
c
c  compute the matrix-vector products of the hmc(*) matrix
c  and the nvec trial vectors in v(*).  if qinc=.true. then
c  these products are computed directly from hmc(*).  if
c  qinc=.false. they are computed from the integrals and
c  off-diagonal formula tape.
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           11ab       ppqq, pqqp
c   3           11a        ppqq
c   4           11b        pqqp
c
c   5           14ab       half*pppp, pp
c   6           14a        half*pppp
c   7           14b        pp
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      sssp, sppp, sp
c   3           2a'b'      srpq, sqpr
c   4           1ab        srqp, spqr
c   5           3ab        sqrp, sprq
c   6           4ab        ssqp, spqs
c               8ab        srpp, sppr
c   7           6a'b'      sqqp, spqq
c   8           12ac       sssp, sp
c   9           12bc       sppp, sp
c  10           1a         srqp
c               2a'        srpq
c               6a'        sqqp
c               7'         sqpq
c  11           4a         ssqp
c               8a         srpp
c  12           2b'        sqpr
c               3a         sqrp
c               4b         spqs
c               4b'        sqps
c               5          spsq
c               8b         sppr
c               10         sprp
c  13           11b        spps
c               13         half*spsp
c  14           1b         spqr
c               3b         sprq
c  15           6b'        spqq
c               12c        sp
c==================================================
c
c  written by ron shepard.
c  verstion date: 27-mar-85
c
       implicit none
c  ##  parameter & common block section
c
      real*8 zero
      parameter(zero=0d0)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nft, nlist
      equivalence (iunits(1),nlist)
      equivalence (iunits(10),nft)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max

      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)

c
      integer nxy, mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8),nact
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c  ##  integer section
c
      integer addaa(*)
      integer b,bmax
      integer db
      integer event
      integer ism(8),igd(0:7),igod(0:15),ind(*),i,ist,ib,ik,ivec,
     &        icode, isv,ism2(8)
      integer j
      integer kntft,k
      integer lenbft
      integer head,hsym
      integer modrt(0:*)
      integer ncsf,nvec,nw,nv,nsv
      integer off1(*)
      integer pl,p,psym,pp,p0,pqsym,pq,pr,ps,pqrs,prqs,psqr,pqrsym
      integer ql,q,qr,qs
      integer rev(*),rl,rs,r
      integer ss, sp, sssp,sppp,sp1,symw,s,sl
      integer tail,tsym
      integer xp(nsym,*),xbar(nsym,*),xt,xbh
      integer yb(8),yk(8),ytb,ytk
      integer z(nsym,*),zt, spindens
c
c  ##  real*8 section
c
      real*8 ftbuf(lenbft)
      real*8 htrip(3),hmc(*),hdiag(ncsf),hv(ncsf,nvec),h2(*)
      real*8 term
      real*8 uaa(*)
      real*8 val(3),v(ncsf,nvec)
c
c  ##  logical section
c
      logical qinc,qind
c
c  ##  character section
c
      character*60 fname
c
c  ##  data section
c
      data igd/1,2,5,3,4,5,3,4/
      data igod/1,2,9,6,7,8,6,7,7,8,3,3,4,4,5,5/
c-----------------------------------------------------------------------
c
      if(qinc)then
c
c  compute the products from hmc(*).
c
         do 10 i=1,nvec
            call spmxv(hmc,v(1,i),hv(1,i),ncsf)
10       continue
         return
      endif
c
c  compute the products from the integrals and formula tape.
c  products for all the trial vectors are computed during
c  a single ft read.
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  diagonal elements.
c
      do 820 ivec=1,nvec
         do 810 i=1,ncsf
            hv(i,ivec)=hdiag(i)*v(i,ivec)
810      continue
820   continue
c
c  off-diagonal elements.
c
      if (nst.eq.1) then
         fname = 'mcoftfl'
      elseif (ist.ge.10) then
         write(fname,'(a8,i2)')'mcoftfl.',ist
      else
         write(fname,'(a8,i1)')'mcoftfl.',ist
      endif
      call trnfln( 1, fname )
      open(unit=nft,file=fname,status='old',form='unformatted')
c
      rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 1440 pl=2,nact
      p=orbidx(modrt(pl))
      psym=symm(modrt(pl))
      pp=nndx(p+1)
      p0=nsa(psym)
c
      do 1430 ql=1,pl
      q=orbidx(modrt(ql))
      pqsym=mult(symm(modrt(ql)),psym)
      pq=nndx(max(p,q))+min(p,q)
c
      do 1420 rl=1,ql
      r=orbidx(modrt(rl))
      pqrsym=mult(symm(modrt(rl)),pqsym)
      pr=nndx(max(p,r))+min(p,r)
      qr=nndx(max(q,r))+min(q,r)
c
      do 1410 sl=1,rl
      if(sl.eq.ql)go to 1420
      if(symm(modrt(sl)).ne.pqrsym)go to 1410
      s=orbidx(modrt(sl))
c
c  move integrals into htrip(*) in triple-sort order.
c
      if(rl.eq.pl)then
         ss=nndx(s+1)
         sp=nndx(max(s,p))+min(s,p)
         sssp=off1(max(ss,sp))+addaa(min(ss,sp))
         sppp=off1(max(sp,pp))+addaa(min(sp,pp))
         sp1=nnsa(psym)+nndx(max(s-p0,p-p0))+min(s-p0,p-p0)
         htrip(1)=h2(sssp)
         htrip(2)=h2(sppp)
         htrip(3)=uaa(sp1)
      else
         rs=nndx(max(r,s))+min(r,s)
         qs=nndx(max(q,s))+min(q,s)
         ps=nndx(max(p,s))+min(p,s)
         pqrs=off1(max(pq,rs))+addaa(min(pq,rs))
         prqs=off1(max(pr,qs))+addaa(min(pr,qs))
         psqr=off1(max(ps,qr))+addaa(min(ps,qr))
         htrip(1)=h2(pqrs)
         htrip(2)=h2(prqs)
         htrip(3)=h2(psqr)
      endif
c
990   continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2, nwalk_f(ist),spindens )
c
      go to (1000,1410,1001,1002,1003,1012,1013,1023,1123),igod(icode)
c
1000  kntft=1
      nw=0
      read(nft)ftbuf
      go to 990
1001  term=val(1)*htrip(1)
      go to 1200
1002  term=val(1)*htrip(2)
      go to 1200
1003  term=val(1)*htrip(3)
      go to 1200
1012  term=val(1)*htrip(1)+val(2)*htrip(2)
      go to 1200
1013  term=val(1)*htrip(1)+val(2)*htrip(3)
      go to 1200
1023  term=val(1)*htrip(2)+val(2)*htrip(3)
      go to 1200
1123  term=val(1)*htrip(1)+val(2)*htrip(2)+val(3)*htrip(3)
c
1200  if(term.eq.zero)go to 990
c
c  note that the loop over trial vectors is independent of the
c  drt loops and may be placed anywhere relative to them.  this
c  version assumes a relatively small number of vectors and
c  therefore places the vector loop outside of the drt loops to
c  reduce the overhead associated with the trial vector loop.
c  for large numbers of trial vectors, it would be better to have
c  the vector loop inside the drt loops. -ron shepard
c
c  for each symmetry type, perform upper and lower walks.
c
      do 1370 isv=1,nsv
      ytb=yb(isv)
      ytk=yk(isv)
      tsym=ism(isv)
      xt=xp(tsym,tail)
      zt=z(tsym,tail)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym,head)
c
c  loop over trial vectors.
c  the two set of loops are for the cases where there is
c  or is not csf selection.  csf selection requires the
c  use of the ind(*) array.
c
      if(qind)then
c
c  selection case...
c
         do 1330 ivec=1,nvec
            do 1320 j=1,xt
               b=ytb+rev(zt+j)
               k=ytk+rev(zt+j)
               bmax=b+xbh
1300           continue
               b=b+1
               k=k+1
1310           continue
               if(b.gt.bmax)go to 1320
               ib=ind(b)
               ik=ind(k)
               db=min(ib,ik)
               if(db.gt.0)then
                  hv(ik,ivec)=hv(ik,ivec)+term*v(ib,ivec)
                  hv(ib,ivec)=hv(ib,ivec)+term*v(ik,ivec)
                  go to 1300
               else
                  b=b-db
                  k=k-db
                  go to 1310
               endif
1320        continue
1330     continue
c
      else
c
c  no selection case...
c  loops over upper and lower walks reduce to co-diagonal sequences
c  of csf indices.
c
         do 1360 ivec=1,nvec
            do 1350 j=1,xt
               ib=ytb+rev(zt+j)
               ik=ytk+rev(zt+j)
               do 1340 i=1,xbh
                  hv(ik+i,ivec)=hv(ik+i,ivec)+term*v(ib+i,ivec)
1340           continue
               do 1342 i=1,xbh
                  hv(ib+i,ivec)=hv(ib+i,ivec)+term*v(ik+i,ivec)
1342           continue
1350        continue
1360     continue
      endif
c
1370  continue
c
      go to 990
c
1410  continue
1420  continue
1430  continue
1440  continue
c
      close(unit=nft)
c
      if(flags(3))call timer('hmcxv',4,event,nlist)
      return
      end
c
c**********************************************************************
c
      subroutine rdft(ftbuf,lenbft,
     +  d1,d2,hvect,iorder,
     +  ncsf,xbar,xp,z,r,ind,modrt,qind,
     +  tden,uaa,uad,uvd,uva,
     +  h2t1,h2t2,h2t3,h2t4,h2t5,
     +  off1,addaa,add,
     +  caa,cad,cvd,cva,
     +  qc,navst_act,ist)
c
c  read the unitary group formula tape and construct
c  the one- and two- particle symmetric density and
c  transition density matrix elements over the active orbitals.
c  an index driven algorithm is used to facilitate the construction
c  and use of the density matrix elements.  the required blocks of
c  the c matrix are constructed as the transition density matrix
c  elements are available. density matrix elements of the form
c  <mc| e |mc> are stored in d1(*) and d2(*).
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2       11ab        eiill, eilli
c   3       11a         eiill
c   4       11b         eilil
c
c   5       14ab        half*ellll, ell
c   6       14a         half*ellll
c   7       14b         ell
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      iiil, illl, il
c   3           2a'b'      ijlk, iklj
c   4           1ab        ijkl, ilkj
c   5           3ab        ikjl, iljk
c   6           4ab        iikl, ilki
c               8ab        ijll, illj
c   7           6a'b'      ikkl, ilkk
c   8           12ac       iiil, il
c   9           12bc       illl, il
c  10           1a         ijkl
c               2a'        ijlk
c               6a'        ikkl
c               7'         iklk
c  11           4a         iikl
c               8a         ijll
c  12           2b'        iklj
c               3a         ikjl
c               4b         ilki
c               4b'        ikli
c               5          ilik
c               8b         illj
c               10         iljl
c  13           11b        illi
c               13         half*ilil
c  14           1b         ilkj
c               3b         iljk
c  15           6b'        ilkk
c               12c        il
c==================================================
c  explicit variable declaration: 15-apr-02, m.dallos
c  symmetry ft version: 18-oct-84, ron shepard.
c
       implicit none
c  ##  parameter & common block section
c
      integer iunits
      common/cfiles/iunits(55)
      integer nft, ndft, nlist
      equivalence (iunits(1),nlist)
      equivalence (iunits(10),nft)
      equivalence (iunits(11),ndft)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8),nact,nnta
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
c
      integer addpt,numint,bukpt,intoff,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      real*8 zero
      parameter (zero = 0.0d+00)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer msize,csize
      common/size/ msize(maxnst),csize(4,maxnst)
c
      integer dst,ndens,densind,ndbra,brai,brinv
      common/dens/dst,ndens,densind(2,mxavst),ndbra,brai(mxavst),
     & brinv(mxavst)
c
c  ##  integer section
c
      integer addaa(*),add(*)
      integer event
      integer ist,iorder(*),ind(*),ism(8),ii,isym,iidx,icode,
     &        imo,igo,ixx,it,ism2(8)
      integer jidx,jklsym,jsym,jj,jmo
      integer head
      integer kntft,kidx,kjdx,klsym,ksym,kk,kmo
      integer lenbft,lmo,ll,lsym,lldx,lidx,llii,lit,llli,liii,
     &        likj,ljki,lkji,ljdx,lkdx,llt,lt,llll,lili
      integer modrt(0:*)
      integer navst_act,nwalk,ncsf,ncsfx2,ncsfx3,nw,nv,nsv
      integer off1(*)
      integer r(*)
      integer symw
      integer tail
      integer xbar(nsym,*),xp(nsym,*)
      integer yb(8),yk(8)
      integer z(nsym,*),spindens
c
c  ##  real*8 section
c
      real*8 caa(*),cad(*),cvd(*),cva(*)
      real*8 d1(*),d2(*)
      real*8 emc_ave
      real*8 ftbuf(lenbft)
      real*8 h2t1(*),h2t2(*),h2t3(*),h2t4(*),h2t5(*),
     &       hvect(ncsf,navst_act)
      real*8 tden(ncsf,navst_act,3)
      real*8 uaa(*),uad(*),uvd(*),uva(*)
      real*8 val(3)
c
c  ##  logical section
c
      logical qind,qc(4),qt(3),qcx
c
c  ##  character section
c
      character*60 fname1,fname2
c
c  ##  external section
c
      real*8 ddot_wr
      external ddot_wr
c
c----------------------------------------------------------------
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      if (nst.eq.1) then
         fname1 = 'mcdftfl'
         fname2 = 'mcoftfl'
      elseif (ist.ge.10) then
         write(fname1,'(a8,i2)')'mcdftfl.',ist
         write(fname2,'(a8,i2)')'mcoftfl.',ist
      else
         write(fname1,'(a8,i1)')'mcdftfl.',ist
         write(fname2,'(a8,i1)')'mcoftfl.',ist
      endif
      call trnfln( 1, fname1 )
      call trnfln( 1, fname2 )
      open(unit=ndft,file=fname1,status='old',form='unformatted')
      open(unit=nft,file=fname2,status='old',form='unformatted')
c
cfp: set the required values for the dens common block
c  this way sdmatd can also be used by rdft_grd
      ndbra=navst_act
      do ii=1,navst_act
       brai(ii)=ii
      enddo 
c
      ncsfx2=2*ncsf
      ncsfx3=3*ncsf
      qcx=qc(1).or.qc(2).or.qc(3).or.qc(4)
cmd   if(qc(1))call wzero(szh(12),caa,1)
cmd   if(qc(2))call wzero(szh(11),cad,1)
cmd   if(qc(3))call wzero(szh(13),cvd,1)
cmd   if(qc(4))call wzero(szh(14),cva,1)
      if(qc(1))call wzero(csize(2,ist),caa,1)
      if(qc(2))call wzero(csize(1,ist),cad,1)
      if(qc(3))call wzero(csize(3,ist),cvd,1)
      if(qc(4))call wzero(csize(4,ist),cva,1)
cmd
c
c  initialize inactive-orbital blocks of the c(*) matrix
c  with u(*) contributions:
c
cmb   make the impression of a navst_act*ncsf problem, inst. of ncsf
      if(qc(2))call cadu(uad,hvect(1,1),cad,ncsf*navst_act)
      if(qc(3))call cvdu(uvd,hvect(1,1),cvd,ncsf*navst_act)
c
c  diagonal elements.
c
      rewind ndft
      kntft=1
      nw=0
      read(ndft)ftbuf
c
      do 1700 lmo=1,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 1600 imo=1,lmo
      ii=orbidx(modrt(imo))
      isym=symm(modrt(imo))
      iidx=nndx(ii+1)
      lidx=nndx(max(ii,ll))+min(ii,ll)
cmb   zero all partial and averaged tdens (no averaged one..)
      call wzero(ncsfx2*navst_act,tden,1)
c
1000  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2, nwalk_f(ist),spindens )
c
      igo=min(icode,2)+1
      go to (1100,1400,1200),igo
c
1100  continue
c
c     new record.
c
      kntft=1
      nw=0
      read(ndft)ftbuf
      go to 1000
c
1200  continue
c
      call sdmatd(symw,nsv,ism,yb,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,tden,ncsf,qind,navst_act)
      go to 1000
c
1400  continue
c
c  change level.  process density contributions at the present level.
c
      if(imo.eq.lmo)go to 1500
c  i.ne.l case
      llii=off1(max(lldx,iidx))+addaa(min(lldx,iidx))
cmb
c     d2(llii)=ddot_wr(ncsf,tden(1,1),1,hvect,1)
c     lili=off1(lidx)+addaa(lidx)
c     d2(lili)=ddot_wr(ncsf,tden(1,2),1,hvect,1)
c     if(qcx)then
c     call ctwo(qc,ll,ll,ii,ii,lsym,lsym,isym,isym,tden(1,1),ncsf,
c    +  add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
c    +  caa,cad,cvd,cva)
c     call ctwo(qc,ll,ii,ll,ii,lsym,isym,lsym,isym,tden(1,2),ncsf,
c    +  add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
c    +  caa,cad,cvd,cva)
c     endif
      do 1401 ixx=1,navst_act
1401  d2(llii)=d2(llii) +
     $ wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,hvect(1,ixx),1)
      lili=off1(lidx)+addaa(lidx)
      do 1402 ixx=1,navst_act
1402  d2(lili)=d2(lili) +
     $ wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,hvect(1,ixx),1)
      if(qcx)then
      call ctwo(qc,ll,ll,ii,ii,lsym,lsym,isym,isym,tden(1,1,1),
     +  ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      call ctwo(qc,ll,ii,ll,ii,lsym,isym,lsym,isym,tden(1,1,2),
     +  ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      endif
      go to 1600
c
1500  continue
c  i.eq.l case
cmb
c     llll=off1(lldx)+addaa(lldx)
c     d2(llll)=ddot_wr(ncsf,tden(1,1),1,hvect,1)
c     lt=ll-nsa(lsym)
c     llt=nnsa(lsym)+nndx(lt+1)
c     d1(llt)=ddot_wr(ncsf,tden(1,2),1,hvect,1)
c     if(qcx)then
c     call ctwo(qc,ll,ll,ll,ll,lsym,lsym,lsym,lsym,tden(1,1),ncsf,
c    +  add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
c    +  caa,cad,cvd,cva)
c     call cone(qc,ll,ll,lsym,tden(1,2),ncsf,add,iorder,
c    +  uaa,uad,uva,h2t2,h2t3,h2t4,caa,cad,cvd,cva)
c     endif
c
      llll=off1(lldx)+addaa(lldx)
      do 1501 ixx=1,navst_act
1501  d2(llll)=d2(llll) +
     $ wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,hvect(1,ixx),1)
      lt=ll-nsa(lsym)
      llt=nnsa(lsym)+nndx(lt+1)
      do 1502 ixx=1,navst_act
1502  d1(llt)=d1(llt) +
     $ wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,hvect(1,ixx),1)
      if(qcx)then
      call ctwo(qc,ll,ll,ll,ll,lsym,lsym,lsym,lsym,tden(1,1,1),
     +  ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      call cone(qc,ll,ll,lsym,tden(1,1,2),ncsf*navst_act,add,iorder,
     +  uaa,uad,uva,h2t2,h2t3,h2t4,caa,cad,cvd,cva)
      endif
1600  continue
1700  continue
c
c  off-diagonal loop contributions.
c
      rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 2740 lmo=2,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 2730 kmo=1,lmo
      kk=orbidx(modrt(kmo))
      ksym=symm(modrt(kmo))
      klsym=mult(lsym,ksym)
      lkdx=nndx(max(ll,kk))+min(ll,kk)
c
      do 2720 jmo=1,kmo
      jj=orbidx(modrt(jmo))
      jsym=symm(modrt(jmo))
      jklsym=mult(jsym,klsym)
      ljdx=nndx(max(ll,jj))+min(ll,jj)
      kjdx=nndx(max(kk,jj))+min(kk,jj)
c
      do 2710 imo=1,jmo
      if(imo.eq.kmo)go to 2710
      isym=symm(modrt(imo))
      if(isym.ne.jklsym)go to 2710
      ii=orbidx(modrt(imo))
      lidx=nndx(max(ll,ii))+min(ll,ii)
      kidx=nndx(max(kk,ii))+min(kk,ii)
      jidx=nndx(max(jj,ii))+min(jj,ii)
      iidx=nndx(ii+1)
      call wzero(ncsfx3*navst_act,tden,1)
      qt(1)=.false.
      qt(2)=.false.
      qt(3)=.false.
c
2100  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2, nwalk_f(ist),spindens )
c
      igo=min(icode,2)+1
      go to (2200,2500,2300),igo
2200  continue
c
c  new record
c
      kntft=1
      nw=0
      read(nft)ftbuf
      go to 2100
c
2300  continue
c
      call sdmat(symw,nsv,ism,yb,yk,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,tden,ncsf,qt,qind,navst_act)
      go to 2100
c
2500  continue
c
c  change level.  process density contributions.
c
      if(jmo.eq.lmo)go to 2600
c
       if(qt(1))then
         lkji=off1(max(lkdx,jidx))+addaa(min(lkdx,jidx))
cmb
         do 2501 ixx=1,navst_act
2501     d2(lkji)=d2(lkji)+
     $   wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,hvect(1,ixx),1)
      endif
c  allow for 11b contributions from diagonal loops.
      if(qt(2))then
         ljki=off1(max(ljdx,kidx))+addaa(min(ljdx,kidx))
cmb
         do 2502 ixx=1,navst_act
2502     d2(ljki)=d2(ljki)+
     $   wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,hvect(1,ixx),1)
      endif
      if(qt(3))then
         likj=off1(max(lidx,kjdx))+addaa(min(lidx,kjdx))
         do 2503 ixx=1,navst_act
2503     d2(likj)=d2(likj)+
     $   wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,3),1,hvect(1,ixx),1)
      endif
c
      if(qcx.and.qt(1))
     +call ctwo(qc,ll,kk,jj,ii,lsym,ksym,jsym,isym,tden(1,1,1),
     +ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      if(qcx.and.qt(2))
     +call ctwo(qc,ll,jj,kk,ii,lsym,jsym,ksym,isym,tden(1,1,2),
     +ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      if(qcx.and.qt(3))
     +call ctwo(qc,ll,ii,kk,jj,lsym,isym,ksym,jsym,tden(1,1,3),
     +ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      go to 2710
c
2600  continue
c
c ll,ii,ii,ii; ll,ll,ll,ii; ll,ii  special case.
c
      if(qt(1))then
         liii=off1(max(lidx,iidx))+addaa(min(lidx,iidx))
cmb
         do 2601 ixx=1,navst_act
2601     d2(liii)=d2(liii) +
     +   wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,hvect(1,ixx),1)
      endif
      if(qt(2))then
         llli=off1(max(lldx,lidx))+addaa(min(lldx,lidx))
         do 2602 ixx=1,navst_act
2602     d2(llli)=d2(llli)+
     $   wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,hvect(1,ixx),1)
      endif
      if(qt(3))then
         lt=ll-nsa(lsym)
         it=ii-nsa(lsym)
         lit=nnsa(lsym)+nndx(max(lt,it))+min(lt,it)
         do 2603 ixx=1,navst_act
2603     d1(lit)=d1(lit)+
     $   wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,3),1,hvect(1,ixx),1)
      endif
c
      if(qcx.and.qt(1))
     +call ctwo(qc,ll,ii,ii,ii,lsym,isym,isym,isym,tden(1,1,1),
     +ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      if(qcx.and.qt(2))
     +call ctwo(qc,ll,ll,ll,ii,lsym,lsym,lsym,isym,tden(1,1,2),
     +ncsf*navst_act,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cva)
      if(qcx.and.qt(3))
     +call cone(qc,ll,ii,lsym,tden(1,1,3),ncsf*navst_act,add,iorder,
     +  uaa,uad,uva,h2t2,h2t3,h2t4,caa,cad,cvd,cva)
c
2710  continue
2720  continue
2730  continue
2740  continue
c
      close(unit=ndft)
      close(unit=nft)
c
      if(flags(3))call timer('rdft',4,event,nlist)
c
      return
      end
c
c**********************************************************************
c
      subroutine rdtmin
c
c  written by felix plasser 22-dec-09
c  (adaptation from existing transmomin reading routine)
c
c  read the mcdenin file and adjust the pointers
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist,tmin
      equivalence (iunits(1),nlist)
      equivalence (iunits(23),tmin)
c
      integer mxavst
      parameter (mxavst = 50)
      integer maxnst
      parameter (maxnst=8)
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer dst,ndens,densind,ndbra,brai,brinv
      common/dens/dst,ndens,densind(2,mxavst),ndbra,brai(mxavst),
     & brinv(mxavst)
c internal variables
      integer idrt1,state1,idrt2,state2
      integer iostatus
      logical DBRA(mxavst)
c
      do state1=1,mxavst
       DBRA(state1) = .false.
      enddo
c
      ndens=0
      open(unit=tmin,file='mcdenin',form='formatted')
      REWIND tmin
      READ(tmin,*)
      do
       READ(tmin,*,iostat=iostatus)idrt1,state1,idrt2,state2
       if (iostatus > 0) then
        call bummer('mcdenin cannot be read',1,2)
       else if (iostatus < 0) then
        exit
       else
        if (ndens.eq.0) then
         dst=idrt1
         if (dst.ne.1) then
          call bummer('only densities for DRT #1 are implemented,
     & please redo the input or rename the mcdrtin.* files',1,2)
cfp: indices at many positions would have to be changed or a
c  separate caller routine for rdft_grd (cf. hbcon) would be
c  needed for consistent indexing with a different DRT
         endif
        endif
c
        if (idrt1.ne.idrt2) then
         call bummer('only densities within one DRT can be
     & computed',1,2)
cfp: cross DRT densities are not possible with this formalism
        else if (idrt1.ne.dst) then
         call bummer('to compute densities within different DRTs,
     & please perform separate computations. computing only densities
     & for DRT #',dst,0)
cfp: potentially this could be rewritten (cf. comment above)
c   but it does not seem necessary
        else
         ndens=ndens+1
         densind(1,ndens) = state1
         densind(2,ndens) = state2
c the tden for state2 will have to be computed
         DBRA(state1) = .true. 
        endif
       endif
      enddo
      close(tmin)
c
c assign the tden-bras that have to be computed to the indexing array.
c - it is assumed that the transition moments are arranged properly
c - optionally some bra and ket could be switched to minimize the number
c  of tden-bras computed
      ndbra=0
      do state1=1,mxavst
       if (DBRA(state1)) then
        ndbra=ndbra+1
        brai(ndbra)=state1
        brinv(state1)=ndbra
       endif
      enddo
c      write(nlist,*) 'Number of densities:',ndens
c      write(nlist,*) 'Number of tden-bras:',ndbra
c      write(nlist,*) 'brai:',(brai(i),i=1,ndbra)
c      write(nlist,*) 'brinv:', (brinv(i),i=1,mxavst)
      end
c**********************************************************************
c
      subroutine rdft_grd(ftbuf,lenbft,
     + hvect,iorder,
     + ncsf,xbar,xp,z,r,ind,modrt,
     + add,navst_act,
     + sd1s,ad1s,sd2s,tden,atden,
     + nelt,smult,spind,alpham,betam)
c
c  modified by rene spada to include the spin-density calculation
c  09-mar-2021
c
c  adapted from rdft by felix plasser
c  30-jun-09
c
c  read the unitary group formula tape and construct
c  the density matrices needed by the cigrd.x program
c  for computation of gradients and non-adiabatic couplings
c
c  transition density matrix elements between two states are
c  computed according to:
c  < mc_i | e | mc_j > = hmc_i . tden_j
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2       11ab        eiill, eilli
c   3       11a         eiill
c   4       11b         eilil
c
c   5       14ab        half*ellll, ell
c   6       14a         half*ellll
c   7       14b         ell
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      iiil, illl, il
c   3           2a'b'      ijlk, iklj
c   4           1ab        ijkl, ilkj
c   5           3ab        ikjl, iljk
c   6           4ab        iikl, ilki
c               8ab        ijll, illj
c   7           6a'b'      ikkl, ilkk
c   8           12ac       iiil, il
c   9           12bc       illl, il
c  10           1a         ijkl
c               2a'        ijlk
c               6a'        ikkl
c               7'         iklk
c  11           4a         iikl
c               8a         ijll
c  12           2b'        iklj
c               3a         ikjl
c               4b         ilki
c               4b'        ikli
c               5          ilik
c               8b         illj
c               10         iljl
c  13           11b        illi
c               13         half*ilil
c  14           1b         ilkj
c               3b         iljk
c  15           6b'        ilkk
c               12c        il
c==================================================
c  explicit variable declaration: 15-apr-02, m.dallos
c  symmetry ft version: 18-oct-84, ron shepard.
c
       implicit none
c  ##  parameter & common block section
c
      integer iunits
      common/cfiles/iunits(55)
      integer nft, ndft, nlist
      equivalence (iunits(1),nlist)
      equivalence (iunits(10),nft)
      equivalence (iunits(11),ndft)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8),nact,nnta
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
c
      integer addpt,numint,bukpt,intoff,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c    #  maximal No. of different DRTs
cfp   only 1 DRT supported
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      real*8 zero
      parameter (zero = 0.0d+00)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer msize,csize
      common/size/ msize(maxnst),csize(4,maxnst)
c
c  ##  integer section
c
cfp      integer addaa(*),add(*)
      integer add(*)
      integer event
      integer iorder(*),ind(*),ism(8),ii,isym,iidx,icode,
     &        imo,igo,it,nxx,ism2(8)
      integer jidx,jklsym,jsym,jj,jmo
      integer head
      integer kntft,kidx,kjdx,klsym,ksym,kk,kmo
      integer lenbft,lmo,ll,lsym,lldx,lidx,llii,lit,llli,liii,
     &        likj,ljki,lkji,ljdx,lkdx,llt,lt,llll,lili
      integer modrt(0:*)
      integer navst_act,nwalk,ncsf,ncsfx2,ncsfx3,nw,nv,nsv
cfp      integer off1(*)
      integer r(*)
      integer symw
      integer tail
      integer xbar(nsym,*),xp(nsym,*)
      integer yb(8),yk(8)
      integer z(nsym,*), spindens
cfp
      integer dst,ndens,densind,ndbra,brai,brinv
      common/dens/dst,ndens,densind(2,mxavst),ndbra,brai(mxavst),
     & brinv(mxavst)
c
c  ##  real*8 section
c
      real*8 ftbuf(lenbft)
      real*8 hvect(ncsf,navst_act)
cfp: tden always only stored for one set of orbital indices
c      3 numbers stored for different loops with the same indices, cf. sdmatd
      real*8 tden(ncsf,ndbra,3)
      real*8 atden(ncsf,ndbra)
      real*8 val(3)
      real*8 sd1s(nnta,ndens)
      real*8 ad1s(nnta,ndens)
cfp: antisymmetric actually needs less space
      real*8 sd2s(numint(1),ndens)
c
c  ##  logical section
c
      logical qind,qc(4),qt(3),qcx
c
c  ##  character section
c
      character*60 fname1,fname2
c
c  ##  external section
c
      real*8 ddot_wr
      external ddot_wr

c       Spin-Density (Rene)
      real*8 spind(nnta,ndens), temp, sumk(nnta,ndens)
      real*8 alpham(nnta,ndens),betam(nnta,ndens)
      real*8 sdcontr(ncsf,navst_act,3)
      integer ltsd, ktsd, jtsd, itsd, lltsd, iitsd
      integer litsd, lktsd, jitsd
      logical qtsd(3)
      integer nelt, smult,napsy(8)
      equivalence (nxy(1,13),napsy(1))
      real*8 sspin
      character*25 print_title
      integer iprint, jprint ! For debugging reasons

c      write(nlist,*)' ind', ind

c      write(nlist,*) nnta, ndens 
c      stop
c----------------------------------------------------------------
c
c -> -1 bei add !!
      write(nlist,*)'*** Starting rdft_grd'
c
      if(flags(3))call timer(' ',1,event,nlist)
c
      if (nst.eq.1) then
         fname1 = 'mcdftfl'
         fname2 = 'mcoftfl'
      elseif (dst.ge.10) then
         write(fname1,'(a8,i2)')'mcdftfl.',dst
         write(fname2,'(a8,i2)')'mcoftfl.',dst
      else
         write(fname1,'(a8,i1)')'mcdftfl.',dst
         write(fname2,'(a8,i1)')'mcoftfl.',dst
      endif
      call trnfln( 1, fname1 )
      call trnfln( 1, fname2 )
      open(unit=ndft,file=fname1,status='old',form='unformatted')
      open(unit=nft,file=fname2,status='old',form='unformatted')
c
cfp_temp
c     write(nlist,*)'Files opened'
c     write(nlist,*)'navst_act',navst_act
cfp
      qind=nwalk_f(dst) .ne. ncsf_f(dst)
      write(nlist,*)'qind:',qind
c
      ncsfx2=2*ncsf
      ncsfx3=3*ncsf
cfp: qc is passed to original rdft subroutine
      qcx=qc(1).or.qc(2).or.qc(3).or.qc(4)
cfp
      call wzero(nnta*ndens,sd1s,1)
      call wzero(nnta*ndens,ad1s,1)
      call wzero(nnta*ndens,sumk,1)
      call wzero(numint(1)*ndens,sd2s,1)
c -> auch dens. mat.
cmd
c
c  diagonal elements.
c

      rewind ndft
      kntft=1
      nw=0
      read(ndft)ftbuf
c     write(nlist,*)'Formula tape read (1)'

c      write(nlist,*)'Adicionei esse write pra entender o que é o ftbuf'
c      write(nlist,*) ftbuf
c
      do 1700 lmo=1,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 1600 imo=1,lmo
      ii=orbidx(modrt(imo))
      isym=symm(modrt(imo))
      iidx=nndx(ii+1)
      lidx=nndx(max(ii,ll))+min(ii,ll)
c     call wzero(ncsfx2*ndbra,tden,1)
      call wzero(ncsfx3*ndbra,tden,1)
cfp: ncsfx2 should also be ok because the first part is not needed
c

c      write(nlist,*) iidx, lldx, lidx

1000  continue
      kntft=kntft+nw
cfp: read diagonal formula tape
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2, nwalk_f(dst),spindens )
c
c      write(nlist,*)'decodf called, icode:',icode
      igo=min(icode,2)+1
cfp: decide whether to start the computation or read more formula tape
      go to (1100,1400,1200),igo
c
1100  continue
c
c     new record.
c
      kntft=1
      nw=0
      read(ndft)ftbuf
      go to 1000
c
1200  continue
c
cfp: construct diagonal tden
      call sdmatd(symw,nsv,ism,yb,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,tden,ncsf,qind,navst_act)
c     write(nlist,*)'sdmatd called'
      go to 1000
1400  continue
c
c  change level.  process density contributions at the present level.
c
      if(imo.eq.lmo)go to 1500
c  i.ne.l case
      llii=add(addpt(10)-1+max(lldx,iidx))
     $ +add(addpt(4)-1+min(lldx,iidx))

      ltsd=ll-nsa(lsym)
      lltsd=nnsa(lsym)+nndx(ltsd+1)
      itsd=ii-nsa(isym)
      iitsd=nnsa(isym)+nndx(itsd+1)

      do 1411 nxx=1,ndens

c       llii term, loop 11a
        sd2s(llii,nxx)=sd2s(llii,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),1),1,hvect(1,densind(2,nxx)),1)

c       We can get everything from the 11b loop!!!
c       No need for 11a

1411  continue

1407  format(4i5,f15.8,a8,i5)
c
      lili=add(addpt(10)-1+lidx)+add(addpt(4)-1+lidx)

      ltsd=ll-nsa(lsym)
      lltsd=nnsa(lsym)+nndx(ltsd+1)
      itsd=ii-nsa(isym)
      iitsd=nnsa(isym)+nndx(itsd+1)

      do 1421 nxx=1,ndens

c       lili term, loop 11b

        sd2s(lili,nxx)=sd2s(lili,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),2),1,hvect(1,densind(2,nxx)),1)

        temp=ddot_wr(ncsf,tden(1,brinv(densind(1,nxx))
     $ ,2),1,hvect(1,densind(2,nxx)),1)

        sumk(lltsd,nxx)=sumk(lltsd,nxx)+2d0*temp
c        write(nlist,*) 'l =', lltsd, sumk(lltsd,nxx)
        sumk(iitsd,nxx)=sumk(iitsd,nxx)+2d0*temp
c        write(nlist,*) 'i =', iitsd, sumk(iitsd,nxx)

1421  continue

       go to 1600
cfp: only a part of lili comes here, second part at ljki
c
1500  continue
c  i.eq.l case
      llll=add(addpt(10)-1+lldx)+add(addpt(4)-1+lldx)

      ltsd=ll-nsa(lsym)
      lltsd=nnsa(lsym)+nndx(ltsd+1)

c       Changed the loop to include the Spin-Density calculation (Rene)

       do 1511 nxx=1,ndens

c       Loop 14

        sd2s(llll,nxx)=sd2s(llll,nxx)+ddot_wr(ncsf,
     $  tden(1,brinv(densind(1,nxx)),1),1,hvect(1,densind(2,nxx)),1)

        temp=ddot_wr(ncsf,
     $  tden(1,brinv(densind(1,nxx)),1),1,hvect(1,densind(2,nxx)),1)

        sumk(lltsd,nxx)=sumk(lltsd,nxx)+temp
c        write(nlist,*) 'l14 =', lltsd, sumk(lltsd,nxx)

1511   continue

c
      lt=ll-nsa(lsym)
      llt=nnsa(lsym)+nndx(lt+1)
c      write(nlist,*) 'llt',llt
      do 1520 nxx=1,ndens
1520  sd1s(llt,nxx)=sd1s(llt,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),2),1,hvect(1,densind(2,nxx)),1)
c      write(nlist,1407),0,0,lmo,lmo,ddot_wr(ncsf, tden(1,brinv(
c     $ densind(1,nxx)),2),1,hvect(1,densind(2,nxx)),1),'ll',ll
1600  continue
1700  continue

c
c  off-diagonal loop contributions.
c

      rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 2740 lmo=2,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 2730 kmo=1,lmo
      kk=orbidx(modrt(kmo))
      ksym=symm(modrt(kmo))
      klsym=mult(lsym,ksym)
      lkdx=nndx(max(ll,kk))+min(ll,kk)
c
      do 2720 jmo=1,kmo
      jj=orbidx(modrt(jmo))
      jsym=symm(modrt(jmo))
      jklsym=mult(jsym,klsym)
      ljdx=nndx(max(ll,jj))+min(ll,jj)
      kjdx=nndx(max(kk,jj))+min(kk,jj)
c
      do 2710 imo=1,jmo
c      write(nlist,'(4I3)') imo, jmo, kmo, lmo
      if(imo.eq.kmo)go to 2710
      isym=symm(modrt(imo))
      if(isym.ne.jklsym)go to 2710
      ii=orbidx(modrt(imo))
      lidx=nndx(max(ll,ii))+min(ll,ii)
      kidx=nndx(max(kk,ii))+min(kk,ii)
      jidx=nndx(max(jj,ii))+min(jj,ii)
      iidx=nndx(ii+1)
      call wzero(ncsfx3*ndbra,tden,1)
      call wzero(ncsfx3*ndbra,sdcontr,1)
      call wzero(ncsf*ndbra,atden,1)
      qt(1)=.false.
      qt(2)=.false.
      qt(3)=.false.
      qtsd(1)=.false.
      qtsd(2)=.false.
      qtsd(3)=.false.

c      iprint=3; jprint=3

2100  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2,nwalk_f(dst),spindens )
c      write(nlist,'(A,I3,A,I3)')'decodf called, icode:',icode,
c     $ ' spindens:',spindens
      igo=min(icode,2)+1
      go to (2200,2500,2300),igo
2200  continue
c
c  new record
c
      kntft=1
      nw=0
      read(nft)ftbuf
      go to 2100

c
2300  continue
c

crs: read spin density matrix off-diagonal contributions

c      write(nlist,*) 'icode =', icode
      call spin_dmat(symw,nsv,ism,yb,yk,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,sdcontr,ncsf,qtsd,qind,navst_act,spindens)
c      write(nlist,*) sdcontr
cfp: read symmetric and antisymmetric off-diagonal tden
      call dmat(symw,nsv,ism,yb,yk,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,tden,atden,ncsf,qt,qind,navst_act)
      go to 2100
c
2500  continue
c
c  change level.  process density contributions.
c
      if(jmo.eq.lmo)go to 2600
c
       if(qt(1))then
         lkji=add(addpt(10)-1+max(lkdx,jidx))
     $    +add(addpt(4)-1+min(lkdx,jidx))

         do 2511 nxx=1,ndens

          sd2s(lkji,nxx)=sd2s(lkji,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),1),1,hvect(1,densind(2,nxx)),1)

2511     continue
      endif

2566   format(A12,I3,A5,I3,A6,F12.8,A10,F12.8,A10,I2)
       if(qtsd(1))then
         ltsd=ll-nsa(lsym)
         ktsd=kk-nsa(ksym)
         jtsd=jj-nsa(jsym)
         itsd=ii-nsa(isym)
         litsd=nnsa(lsym)+nndx(max(ltsd,itsd))+min(ltsd,itsd)
         lktsd=nnsa(lsym)+nndx(max(ltsd,ktsd))+min(ltsd,ktsd)
         jitsd=nnsa(jsym)+nndx(max(jtsd,itsd))+min(jtsd,itsd)
         do 2512 nxx=1,ndens

          temp=ddot_wr(ncsf,
     $ sdcontr(1,brinv(densind(1,nxx)),1),1,hvect(1,densind(2,nxx)),1)

          if (imo.eq.jmo .and. jmo.ne.kmo .and. kmo.ne.lmo) then
c      This seem to be useless. Can I remove?
              sumk(lktsd,nxx) = sumk(lktsd,nxx) + 0d0*temp

          else if (imo.ne.jmo .and. jmo.ne.kmo .and. kmo.eq.lmo) then
c      This seem to be useless. Can I remove?
              sumk(jitsd,nxx) = sumk(jitsd,nxx) + 0d0*temp

          else if (imo.ne.jmo .and. jmo.eq.kmo .and. kmo.ne.lmo) then
              sumk(litsd,nxx) = sumk(litsd,nxx) + 2d0*temp

          end if

2512    continue
      endif
c  allow for 11b contributions from diagonal loops.???
      if(qt(2))then
         ljki=add(addpt(10)-1+max(ljdx,kidx))
     $    +add(addpt(4)-1+min(ljdx,kidx))

         do 2520 nxx=1,ndens

          sd2s(ljki,nxx)=sd2s(ljki,nxx)+ddot_wr(ncsf,
     $  tden(1,brinv(densind(1,nxx)),2),1,hvect(1,densind(2,nxx)),1)

2520     continue
      endif

      if(qtsd(2))then
         ltsd=ll-nsa(lsym)
         ktsd=kk-nsa(ksym)
         jtsd=jj-nsa(jsym)
         itsd=ii-nsa(isym)
         litsd=nnsa(lsym)+nndx(max(ltsd,itsd))+min(ltsd,itsd)
         lktsd=nnsa(lsym)+nndx(max(ltsd,ktsd))+min(ltsd,ktsd)
         jitsd=nnsa(jsym)+nndx(max(jtsd,itsd))+min(jtsd,itsd)
         lltsd=nnsa(lsym)+nndx(ltsd+1)
         iitsd=nnsa(isym)+nndx(itsd+1)

         do 2521 nxx=1,ndens
          temp=ddot_wr(ncsf,
     $  sdcontr(1,brinv(densind(1,nxx)),2),1,hvect(1,densind(2,nxx)),1)

          if (imo.eq.jmo .and. jmo.ne.kmo .and. kmo.ne.lmo) then
              sumk(lktsd,nxx) = sumk(lktsd,nxx) + 2d0*temp

          else if (imo.ne.jmo .and. jmo.ne.kmo .and. kmo.eq.lmo) then
              sumk(jitsd,nxx) = sumk(jitsd,nxx) + 2d0*temp

          else if (imo.ne.jmo .and. jmo.eq.kmo .and. kmo.ne.lmo) then
c     This case seems to never happen
              sumk(litsd,nxx) = sumk(litsd,nxx) + temp

          else 
              sumk(lltsd,nxx) = sumk(lltsd,nxx) +2d0* temp
              sumk(iitsd,nxx) = sumk(iitsd,nxx) +2d0* temp

          end if


2521     continue
      endif


      if(qt(3))then
         likj=add(addpt(10)-1+max(lidx,kjdx))
     $    +add(addpt(4)-1+min(lidx,kjdx))
         do 2530 nxx=1,ndens
      sd2s(likj,nxx)=sd2s(likj,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),3),1,hvect(1,densind(2,nxx)),1)

2530     continue
      endif


      go to 2710
c
2600  continue
c
c ll,ii,ii,ii; ll,ll,ll,ii; ll,ii  special case.
c
      if(qt(1))then
         liii=add(addpt(10)-1+max(lidx,iidx))
     $    +add(addpt(4)-1+min(lidx,iidx))

         do 2610 nxx=1,ndens

           sd2s(liii,nxx)=sd2s(liii,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),1),1,hvect(1,densind(2,nxx)),1)

2610     continue
      end if 

      if(qtsd(1))then
         ltsd=ll-nsa(lsym)
         itsd=ii-nsa(isym)
         litsd=nnsa(lsym)+nndx(max(ltsd,itsd))+min(ltsd,itsd)
         do 2611 nxx=1,ndens
           temp=ddot_wr(ncsf,
     $ sdcontr(1,brinv(densind(1,nxx)),1),1,hvect(1,densind(2,nxx)),1)

           sumk(litsd,nxx) = sumk(litsd,nxx) + temp

2611     continue
      endif

      if(qt(2))then
         llli=add(addpt(10)-1+max(lldx,lidx))
     $    +add(addpt(4)-1+min(lldx,lidx))

         do 2620 nxx=1,ndens

           sd2s(llli,nxx)=sd2s(llli,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),2),1,hvect(1,densind(2,nxx)),1)

2620     continue
      end if 

      if(qtsd(2))then
         ltsd=ll-nsa(lsym)
         itsd=ii-nsa(isym)
         litsd=nnsa(lsym)+nndx(max(ltsd,itsd))+min(ltsd,itsd)
         do 2621 nxx=1,ndens
           temp=ddot_wr(ncsf,
     $ sdcontr(1,brinv(densind(1,nxx)),2),1,hvect(1,densind(2,nxx)),1)

           sumk(litsd,nxx) = sumk(litsd,nxx) + temp

2621       continue
      endif

      if(qt(3))then
         lt=ll-nsa(lsym)
         it=ii-nsa(lsym)
cfp_todo: different adressing for ad1s -> lit-nlines ; lit-max(lt,it)
         lit=nnsa(lsym)+nndx(max(lt,it))+min(lt,it)
         do 2630 nxx=1,ndens
       ad1s(lit,nxx)=ad1s(lit,nxx)+ddot_wr(ncsf,
     $ atden(1,brinv(densind(1,nxx))),1,hvect(1,densind(2,nxx)),1)
2630   sd1s(lit,nxx)=sd1s(lit,nxx)+ddot_wr(ncsf,
     $ tden(1,brinv(densind(1,nxx)),3),1,hvect(1,densind(2,nxx)),1)
c        write(nlist,1407)lmo,imo,0,0,ddot_wr(ncsf,
c     $ atden(1,densind(1,1)),1,hvect(1,densind(2,1)),1),'**antis',lit
c        write(nlist,1407)lmo,imo,0,0,ddot_wr(ncsf,
c     $ tden(1,densind(1,1),3),1,hvect(1,densind(2,1)),1),'**symm',lit
      endif
c

2710  continue
2720  continue
2730  continue
2740  continue
c
      close(unit=ndft)
      close(unit=nft)
        
c      write(nlist,*) 'Only lines presenting non-zero elements'
c     * // ' are printed'
c      write(nlist,*) ' '
c      write(nlist,*) 'Spin Multiplicity', smult
c      write(nlist,*) 'Number of Electrons', nelt 
      sspin = (float(smult) - 1d0)/2d0
c      write(nlist,*) 'Spin Quantum Number', sspin

c      write(nlist,*) 'Coef of sd1s', ((2d0-.5d0*nelt)/(sspin+1))
c      write(nlist,*) 'Coef of sumk', -(1/(sspin+1))

      spind = ((2d0-.5d0*nelt)/(sspin+1))*sd1s - (1/(sspin+1))*sumk

c      sd1s = alpham + betam
c      spind = alpham - betam

      alpham = (sd1s + spind)
      alpham = alpham / 2d0
      betam = (sd1s - spind)
      betam = betam / 2d0
c
      if(flags(3))call timer('rdft_grd',4,event,nlist)
c
c
c      write(nlist,*) nnta, numint(1)
2800  format('    <',I2,'|E_s|',I2,'>')
c2801  format('    <',I2,'|E_a|',I2,'>')
c2802  format('    <',I2,'|e_s|',I2,'>')
c2803  format('    <',I2,'|Sum_k|',I2,'>')
2804  format('    <',I2,'|SD|',I2,'>')
c2805  format('    <',I2,'|Sd2s|',I2,'>')
2806  format('    <',I2,'|alpha|',I2,'>')
2807  format('    <',I2,'|beta|',I2,'>')

      do nxx=1,ndens

c        write(nlist,*) ' '
c        write(nlist,*)'Sum_k matrix:'
c        write(print_title,2803) densind(1,nxx),densind(2,nxx)
c        call plblks(print_title, 
c     *              sumk(1,nxx) ,nsym,napsy,'  mo',1,nlist)

       if (smult .ne. 1 ) then 

        write(nlist,*) ' '
        write(nlist,*)'Spin density matrix:'
        write(print_title,2804) densind(1,nxx),densind(2,nxx)
        call plblks(print_title,
     *              spind(1,nxx) ,nsym,napsy,'  mo',1,nlist)


        write(nlist,*) ' '
        write(nlist,*)'Alpha density matrix:'
        write(print_title,2806) densind(1,nxx),densind(2,nxx)
        call plblks(print_title,
     *              alpham(1,nxx) ,nsym,napsy,'  mo',1,nlist)

       write(nlist,*) ' '

        write(nlist,*) ' '
        write(nlist,*)'Beta density matrix:'
        write(print_title,2807) densind(1,nxx),densind(2,nxx)
        call plblks(print_title,
     *              betam(1,nxx) ,nsym,napsy,'  mo',1,nlist)

       write(nlist,*) ' '
       else 
           write(nlist,*) ' '
           write(nlist,*) ' '
     &                 // 'The spin-density matrix is not printed '
     &                 // 'for singlet states'       
     &                 // ' '      
       
       end if

        write(nlist,*) ' '
        write(nlist,*)'One particle density matrix:'
        write(print_title,2800) densind(1,nxx),densind(2,nxx)
        call plblks(print_title,
     *              sd1s(1,nxx) ,nsym,napsy,'  mo',1,nlist)

       write(nlist,*) ' '

      enddo
      write(nlist,*)'*** rdft_grd finished'
      return
      end

      subroutine spin_dmat(
     & symw,    nsv,      ism,    yb,
     & yk,      xbar,     xp,     z,
     & r,       ind,      val,    icode,
     & hvect,   sdcontr,  ncsf,   qt,
     & qind,    navst,    spindens)
c
c  construct the spin density matrix
c  element contributions for off-diagonal loops.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  This routine should not change anything, just colect
c  loop contributions.
c
c  written by Rene F. K. Spada 10-2019
c
c
       implicit none
c  ##  parameter & common section
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      real*8 half,fourth
      parameter(half=5d-1, fourth=25d-2)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)

      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c****************************************************************
c
c  ##  integer section
c
      integer nsv
c
      integer b, bmax
      integer db
      integer icode,ind(*),iop,igo, isv, i, ib, ik, ism(nsv), ix, ixx
      integer hsym
      integer j
      integer k
      integer nwalk,ncsf,navst
      integer r(*)
      integer symw
      integer tsym
      integer xbar(nsym),xp(nsym),xt,xbh
      integer yb(nsv),yk(nsv), ytb, ytk
      integer z(nsym),zt
      integer spindens
c
c  ##  real*8 section
c
      real*8 emc_ave
      real*8 hvect(ncsf,navst)
      real*8 t(3),sdcontr(ncsf,navst,3)
      real*8 val(3)
c
c  ##  logical section
c
      logical qt(3),qind,qtx(3)
c
c----------------------------------------------------------------
c
c  account for factors and assign the go to variable.
c
      iop=icode-1
      go to(2,3,4,5,6,7,8,9,10,11,12,13,14,15),iop
c
2     continue
c  iiil, illl, il
      igo = 7
      t(1)=half*val(1)
      t(2)=half*val(2)
      t(3)=0d0
      qt(1)=.true.
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
3     continue
c  ijlk, iklj
      igo = 4
      t(1)=0d0
      t(2)=0d0
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
4     continue
c  ijkl, ilkj
      igo = 5
      t(1)=0d0
      t(3)=0d0
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
5     continue
c  ikjl, iljk
      igo = 6
      t(2)=0d0
      t(3)=0d0
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
6     continue
c  iikl, ilki
c  ijll, illj
      igo = 4
      t(1)=half*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
7     continue
c  ikkl, ilkk
      igo = 5
      t(1)=fourth*val(1)
      t(3)=0
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
8     continue
c  iiil, il
      igo = 5
      t(1)=half*val(1)
      t(3)=0d0
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
9     continue
c  illl, il
      igo = 6
      t(2)=half*val(1)
      t(3)=0
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
10    continue
c  ijkl
c  ijlk
c  ikkl
c  iklk
      igo = 1
      if (spindens .ne. 0) then
          t(1)=fourth*val(1)
      else
          t(1)=0d0
      end if
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
11    continue
c  iikl
c  ijll
      igo = 1
      t(1)=half*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
12    continue
c  iklj
c  ikjl
c  ilki
c  ikli
c  ilik
c  illj
c  iljl
      igo = 2
      if (spindens .ne. 0) then
          t(2)=fourth*val(1)
      else
          t(2)=0d0
      end if
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
13    continue
c  illi
c  half*ilil
      igo = 3
      if (spindens .ne. 0) then
          t(2)=half*val(1)
      else
          t(2)=0d0
      end if
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
14    continue
c  ilkj
c  iljk
      igo = 3
      t(3)=0d0
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
15    continue
c  ilkk
c  il
      igo = 3
      t(3)=0
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
      ytb=yb(isv)
      ytk=yk(isv)
      tsym=ism(isv)
      xt=xp(tsym)
      zt=z(tsym)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym)
c
      if(qind)then
c
c  csf selection case.  ind(*) vector is required.
c
      do 1200 j=1,xt
      b=ytb+r(zt+j)
      k=ytk+r(zt+j)
      bmax=ytb+r(zt+j)+xbh
100   continue
      b=b+1
      k=k+1
110   continue
      if(b.gt.bmax)go to 1200
      ib=ind(b)
      ik=ind(k)
      db=min(ib,ik)
      if(db.gt.0)go to (1001,1002,1003,1012,1013,1023,1123),igo
      b=b-db
      k=k-db
      go to 110
c
cmb   changes...
1001  continue
      do 2001 ixx=1,navst
      sdcontr(ib,ixx,1)=sdcontr(ib,ixx,1)+hvect(ik,ixx)*t(1)
2001  sdcontr(ik,ixx,1)=sdcontr(ik,ixx,1)+hvect(ib,ixx)*t(1)
      go to 100
c
1002  continue
      do 2002 ixx=1,navst
      sdcontr(ib,ixx,2)=sdcontr(ib,ixx,2)+hvect(ik,ixx)*t(2)
2002  sdcontr(ik,ixx,2)=sdcontr(ik,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
c
1003  continue
      do 2003 ixx=1,navst
      sdcontr(ib,ixx,3)=sdcontr(ib,ixx,3)+hvect(ik,ixx)*t(3)
2003  sdcontr(ik,ixx,3)=sdcontr(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1012  continue
      do 2012 ixx=1,navst
      sdcontr(ib,ixx,1)=sdcontr(ib,ixx,1)+hvect(ik,ixx)*t(1)
      sdcontr(ik,ixx,1)=sdcontr(ik,ixx,1)+hvect(ib,ixx)*t(1)
      sdcontr(ib,ixx,2)=sdcontr(ib,ixx,2)+hvect(ik,ixx)*t(2)
2012  sdcontr(ik,ixx,2)=sdcontr(ik,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
c
1013  continue
      do 2013 ixx=1,navst
      sdcontr(ib,ixx,1)=sdcontr(ib,ixx,1)+hvect(ik,ixx)*t(1)
      sdcontr(ik,ixx,1)=sdcontr(ik,ixx,1)+hvect(ib,ixx)*t(1)
      sdcontr(ib,ixx,3)=sdcontr(ib,ixx,3)+hvect(ik,ixx)*t(3)
2013  sdcontr(ik,ixx,3)=sdcontr(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1023  continue
      do 2023 ixx=1,navst
      sdcontr(ib,ixx,2)=sdcontr(ib,ixx,2)+hvect(ik,ixx)*t(2)
      sdcontr(ik,ixx,2)=sdcontr(ik,ixx,2)+hvect(ib,ixx)*t(2)
      sdcontr(ib,ixx,3)=sdcontr(ib,ixx,3)+hvect(ik,ixx)*t(3)
2023  sdcontr(ik,ixx,3)=sdcontr(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1123  continue
      do 2123 ixx=1,navst
      sdcontr(ib,ixx,1)=sdcontr(ib,ixx,1)+hvect(ik,ixx)*t(1)
      sdcontr(ik,ixx,1)=sdcontr(ik,ixx,1)+hvect(ib,ixx)*t(1)
      sdcontr(ib,ixx,2)=sdcontr(ib,ixx,2)+hvect(ik,ixx)*t(2)
      sdcontr(ik,ixx,2)=sdcontr(ik,ixx,2)+hvect(ib,ixx)*t(2)
      sdcontr(ib,ixx,3)=sdcontr(ib,ixx,3)+hvect(ik,ixx)*t(3)
2123  sdcontr(ik,ixx,3)=sdcontr(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
      do 1240 ix=1,3
c
      if(qtx(ix))then
      do 1230 j=1,xt
      ib=ytb+r(zt+j)
      ik=ytk+r(zt+j)
      do 1230 ixx=1,navst
      do 1210 i=1,xbh
1210  sdcontr(ik+i,ixx,ix)=sdcontr(ik+i,ixx,ix)+hvect(ib+i,ixx)*t(ix)
      do 1220 i=1,xbh
1220  sdcontr(ib+i,ixx,ix)=sdcontr(ib+i,ixx,ix)+hvect(ik+i,ixx)*t(ix)
1230  continue
      endif
c
1240  continue
c
      endif
c
c
1300  continue
c
      return
      end
