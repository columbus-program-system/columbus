!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C*************************************************************
cmcscf10.f
cmcscf part=10 of 10.  miscellaneous mcscf utility routines.
cversion=5.0 last modified: 13-jun-96
C*************************************************************
      subroutine kora_md()


      call bummer('DIRECT MCSCF IS NOT AVAILABLE',0,2)

      return
      end
C*************************************************************
      subroutine drive_oneint()


      return
      end
C*************************************************************
      subroutine mosave()


      return
      end

      subroutine ivcmax()


      return
      end
C*************************************************************
      subroutine u_2e()


      return
      end
C*************************************************************
      subroutine mvdad1()


      return
      end
C*************************************************************
      subroutine md3()


      return
      end
C*************************************************************
      subroutine fpseu()


      return
      end
C*************************************************************
      subroutine md1()


      return
      end
C*************************************************************
      subroutine mvavd2()


      return
      end
C*************************************************************
      subroutine md4()


      return
      end
C*************************************************************
      subroutine mvdvd2()


      return
      end
C*************************************************************
      subroutine md5()


      return
      end
C*************************************************************
      subroutine mvdad2()


      return
      end
C*************************************************************
      subroutine md2()


      return
      end
C*************************************************************
      subroutine mvavd1()


      return
      end
C*************************************************************
      subroutine bdiag1e()


      return
      end
C*************************************************************
