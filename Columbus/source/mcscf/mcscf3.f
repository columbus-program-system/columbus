!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf3.f
cmcscf part=3 of 10.  miscellaneous routines.
cversion=5.6 last modified: 16-jan-2001
c deck add1
      subroutine add1
c
c  calculate some of the arrays in /csymb/.
c  assign symmetry labels to basis functions and orbitals.
c  calculate integer pointers for addressing arrays.
c
c  input required:
c  nbpsy(*)  =number of basis functions of each irrep.
c  nmpsy(*)  =number of orbitals of each irrep.
c  orbtyp(*) =orbital type for each orbital (fc=0,d=1,a=2,v=3,fv=4).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nsb(8),nnsb(8),n2sb(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,2),nsb(1))
      equivalence (nxy(1,3),nnsb(1))
      equivalence (nxy(1,4),n2sb(1))
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(2),nntb)
      equivalence (nxtot(3),n2tb)
      integer nmpsy(8),nsm(8),nnsm(8),n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(5),nntm)
      equivalence (nxtot(6),n2tm)
      integer ndpsy(8),nsd(8),nnsd(8),n2sd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxy(1,11),nnsd(1))
      equivalence (nxy(1,12),n2sd(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(8),nntd)
      equivalence (nxtot(9),n2td)
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8),nsv(8),nnsv(8),n2sv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxy(1,19),nnsv(1))
      equivalence (nxy(1,20),n2sv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(14),nntv)
      equivalence (nxtot(15),n2tv)
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
      equivalence (nxtot(16),nbmt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irb1(8),irb2(8)
      equivalence (nxy(1,33),irb1(1))
      equivalence (nxy(1,34),irb2(1))
      integer irm1(8),irm2(8)
      equivalence (nxy(1,35),irm1(1))
      equivalence (nxy(1,36),irm2(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symb(nmotx),symm(nmotx),symx(nmotx),invx(nmotx)
      integer orbidx(nmotx),orbtyp(nmotx),nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,2),symb(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (iorbx(1,6),orbidx(1))
      equivalence (iorbx(1,7),orbtyp(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a),(ix0(3),ix0v)
c
      integer ntype(3)
c
c  initialize nndx(*)
c
      nndx(1)=0
      do 10 i=2,nmotx
10    nndx(i)=nndx(i-1)+(i-1)
c
c  calculate ndpsy(*), napsy(*), and nvpsy(*) from orbtyp(*).
c
      i2=0
      do 30 isym=1,8
      ndpsy(isym)=0
      napsy(isym)=0
      nvpsy(isym)=0
      i1=i2+1
      i2=i2+nmpsy(isym)
      do 20 i=i1,i2
      t=orbtyp(i)
      if(t.eq.1)then
         ndpsy(isym)=ndpsy(isym)+1
      elseif(t.eq.2)then
         napsy(isym)=napsy(isym)+1
      elseif(t.eq.3)then
         nvpsy(isym)=nvpsy(isym)+1
      endif
20    continue
30    continue
c
c  fill in arrays in /csymb/.
c
      nbft=0
      nntb=0
      n2tb=0
      nmot=0
      nntm=0
      n2tm=0
      ndot=0
      nntd=0
      n2td=0
      nact=0
      nnta=0
      n2ta=0
      nvrt=0
      nntv=0
      n2tv=0
c
      do 50 i=1,8
c
      nb=nbpsy(i)
      nm=nmpsy(i)
      nd=ndpsy(i)
      na=napsy(i)
      nv=nvpsy(i)
c
      nsb(i)=nbft
      nsm(i)=nmot
      nsd(i)=ndot
      nsa(i)=nact
      nsv(i)=nvrt
      nbft=nbft+nb
      nmot=nmot+nm
      ndot=ndot+nd
      nact=nact+na
      nvrt=nvrt+nv
c
      irb1(i)=nsb(i)+1
      irm1(i)=nsm(i)+1
      ird1(i)=nsd(i)+1
      ira1(i)=nsa(i)+1
      irv1(i)=nsv(i)+1
c
      irb2(i)=nbft
      irm2(i)=nmot
      ird2(i)=ndot
      ira2(i)=nact
      irv2(i)=nvrt
c
      nnsb(i)=nntb
      nnsm(i)=nntm
      nnsd(i)=nntd
      nnsa(i)=nnta
      nnsv(i)=nntv
      nntb=nntb+nndx(nb+1)
      nntm=nntm+nndx(nm+1)
      nntd=nntd+nndx(nd+1)
      nnta=nnta+nndx(na+1)
      nntv=nntv+nndx(nv+1)
c
      n2sb(i)=n2tb
      n2sm(i)=n2tm
      n2sd(i)=n2td
      n2sa(i)=n2ta
      n2sv(i)=n2tv
      n2tb=n2tb+nb**2
      n2tm=n2tm+nm**2
      n2td=n2td+nd**2
      n2ta=n2ta+na**2
      n2tv=n2tv+nv**2
50    continue
c
c  calculate offsets for ad(*),vd(*), and va(*) totally
c  symmetric arrays and orbital coefficients.
c
      nad1=0
      nvd1=0
      nva1=0
      nbmt=0
      do 60 i=1,8
      nsad(i)=nad1
      nsvd(i)=nvd1
      nsva(i)=nva1
      nsbm(i)=nbmt
      nad1=nad1+napsy(i)*ndpsy(i)
      nvd1=nvd1+nvpsy(i)*ndpsy(i)
      nva1=nva1+nvpsy(i)*napsy(i)
      nbmt=nbmt+nbpsy(i)*nmpsy(i)
60    continue
c
c  allocate space for addressing arrays.
c
      ndd=nndx(ndot+1)
      ndd2=ndot*ndot
      nad=nact*ndot
      naa=nndx(nact+1)
      naa2=nact*nact
      nvd=nvrt*ndot
      nva=nvrt*nact
      nvv=nndx(nvrt+1)
      nvv2=nvrt*nvrt
c
c  add**(*) arrays
c
      nptot=1
c  1:dd
      addpt(1)=nptot
      nptot=nptot+ndd
c  2:dd2
      addpt(2)=nptot
      nptot=nptot+ndd2
c  3:ad
      addpt(3)=nptot
      nptot=nptot+nad
c  4:aa
      addpt(4)=nptot
      nptot=nptot+naa
c  5:vd
      addpt(5)=nptot
      nptot=nptot+nvd
c  6:va
      addpt(6)=nptot
      nptot=nptot+nva
c  7:vv
      addpt(7)=nptot
      nptot=nptot+nvv
c  8:vv2
      addpt(8)=nptot
      nptot=nptot+nvv2
c  9:vv3
      addpt(9)=nptot
      nptot=nptot+nvv2
c
c  off*(*) arrays.
c
c  10:off1(pq)
      addpt(10)=nptot
      nptot=nptot+naa
c  11:off2(pq)
      addpt(11)=nptot
      if(nad.ne.0)nptot=nptot+naa
c  12:off3(pq)
      addpt(12)=nptot
      if(nvd.ne.0)nptot=nptot+naa
c  13:off4(q,p)
      addpt(13)=nptot
      if(nvd.ne.0)nptot=nptot+naa2
c  14:off5(pq)
      addpt(14)=nptot
      if(nva.ne.0)nptot=nptot+naa
c  15:off6(pq)
      addpt(15)=nptot
      if(ndd.ne.0)nptot=nptot+naa
c  16:off7(pq)
      addpt(16)=nptot
      if(nvv.ne.0)nptot=nptot+naa
c  17:off8(q,p)
      addpt(17)=nptot
      if(nvv2.ne.0)nptot=nptot+naa
c  18:off9(ai)
      addpt(18)=nptot
      if(nva.ne.0)nptot=nptot+nvd1
c  19:off10(ai)
      addpt(19)=nptot
      if(nad.ne.0)nptot=nptot+nvd1
c  20:off11(ij)
      addpt(20)=nptot
      if(nvv2.ne.0)nptot=nptot+ndd
c  21:off12(pq)
      addpt(21)=nptot
      if(nvv2.ne.0)nptot=nptot+naa
c  22:off13(pq)
      addpt(22)=nptot
      if(ndd2.ne.0)nptot=nptot+naa
c  23:off14(p,q)
      addpt(23)=nptot
      if(nvd.ne.0)nptot=nptot+naa2
c  24:total space required.
      nptot=nptot-1
      addpt(24)=nptot
c
c-    write(nlist,6010)nptot,addpt
c-6010  format(/' space required for addressing arrays:',i8/(1x,8i8))
c
c  assign symmetry labels to basis functions.
c
      do 120 i=1,8
      do 110 j=irb1(i),irb2(i)
      symb(j)=i
110   continue
120   continue
c-    write(nlist,6020)'symb:',(symb(i),i=1,nbft)
c-6020  format(1x,a,(5(1x,10i1)))
c
c  calculate orbital type offsets, ix0*.
c
      ix0d=0
      ix0a=ix0d+ndot
      ix0v=ix0a+nact
c
c  assign symmetry labels to orbitals.
c
      do 140 i=1,8
      do 130 j=ird1(i),ird2(i)
      symx(ix0d+j)=i
130   continue
140   continue
c-    write(nlist,6020)'symd:',(symx(ix0d+i),i=1,ndot)
c
      do 160 i=1,8
      do 150 j=ira1(i),ira2(i)
      symx(ix0a+j)=i
150   continue
160   continue
c-    write(nlist,6020)'syma:',(symx(ix0a+i),i=1,nact)
c
      do 180 i=1,8
      do 170 j=irv1(i),irv2(i)
      symx(ix0v+j)=i
170   continue
180   continue
c-    write(nlist,6020)'symv:',(symx(ix0v+i),i=1,nvrt)
c
      do 200 i=1,8
      do 190 j=irm1(i),irm2(i)
      symm(j)=i
190   continue
200   continue
c-    write(nlist,6020)'symm:',(symm(i),i=1,nmot)
c
c  calculate orbital mapping and inverse mapping arrays
c  for  inactive, active and virtual orbitals.
c
      ntype(1)=0
      ntype(2)=0
      ntype(3)=0
      do 220 i=1,nmot
      orbidx(i)=0
      t=orbtyp(i)
      if(t.ge.1 .and. t.le.3)then
         ntype(t)=ntype(t)+1
         orbidx(i)=ntype(t)
         ir=i-nsm(symm(i))
         invx(ix0(t)+ntype(t))=ir
      endif
220   continue
c-    write(nlist,6020)'orbtyp:',(orbtyp(i),i=1,nmot)
c-    write(nlist,6030)'orbidx:',(orbidx(i),i=1,nmot)
c-    write(nlist,6030)'invd:',(invx(ix0d+i),i=1,ndot)
c-    write(nlist,6030)'inva:',(invx(ix0a+i),i=1,nact)
c-    write(nlist,6030)'invv:',(invx(ix0v+i),i=1,nvrt)
c-6030  format(1x,a,(5(1x,10i3,3x,10i3/)))
c-    write(nlist,*)' ntype(*)=',ntype
c
c  set diminsions parameters.
c
      ndimd=max(1,ndot)
      ndima=max(1,nact)
      ndimv=max(1,nvrt)
      return
      end
c deck add2
      subroutine add2(naar,ic)
c
c  divide the core space, ic(*), for integral addressing
c  array calculation using the pointers, addpt(*).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      integer ic(*)
c
      call add2s(naar,
     +  ic(addpt( 1)),ic(addpt( 2)),ic(addpt( 3)),ic(addpt( 4)),
     +  ic(addpt( 5)),ic(addpt( 6)),ic(addpt( 7)),ic(addpt( 8)),
     +  ic(addpt( 9)),ic(addpt(10)),ic(addpt(11)),ic(addpt(12)),
     +  ic(addpt(13)),ic(addpt(14)),ic(addpt(15)),ic(addpt(16)),
     +  ic(addpt(17)),ic(addpt(18)),ic(addpt(19)),ic(addpt(20)),
     +  ic(addpt(21)),ic(addpt(22)),ic(addpt(23)))
      return
      end
c
c*****************************************************************
c
      subroutine add2s(naar,
     +  adddd,adddd2,addad,addaa,addvd,addva,addvv,addvv2,addvv3,
     +  off1,off2,off3,off4,off5,off6,off7,off8,
     +  off9,off10,off11,off12,off13,off14)
c
c  calculate addressing arrays for the required
c  for the hessian and gradient construction.
c  arrays are used for addressing integrals, integral combinations,
c  and hessian blocks.
c
c  let i,j      be inactive orbital labels 1 to ndot,
c      p,q,r,s  be active orbital labels 1 to nact, and
c      a,b      be virtual orbital labels 1 to nvrt.  then
c  the various integral types and hessian blocks are addressed
c  as follows:
c
c  [(pq|rs)] = off1(pq)+ addaa(rs)
c  [(pq|ri)] = off2(pq)+ addad(i,r)
c  [(pq|ia)] = off3(pq)+ addvd(a,i)
c  [(pi|qa)] = off4(q,p)+addvd(a,i)
c  [(pq|ra)] = off5(pq)+ addva(a,r)
c  [(pq|ij)] = off6(pq)+ adddd(ij)
c  [(pi|qj)] = off6(pq)+ adddd2(j,i)
c  [(pq|ab)] = off7(pq)+ addvv(ab)
c  [(pa|qb)] = off8(pq)+ addvv2(b,a)
c
c  [p(bp,ai)]= off9(ai)+ addva(b,p)  =[b(bp,ai)]
c  [p(ai,jp)]= off10(ai)+addad(j,p)  =[b(ai,jp)]
c  [p(ai,bj)]= off11(ij)+addvv3(b,a) =[b(ai,bj)]
c
c  [b(qb,pa)]= off12(pq) + (b,a)
c  [b(ip,jr)]= off13(pr) + (j,i)
c  [b(ap,iq)]= off14(p,q)+ (a,i)
c
c  where e.g., pq=(p*(p-1))/2+q with p.ge.q and p=1 to nact.
c  p(wx,yz) = 4(wx|yz) - (wy|xz) - (wz|yx)
c
c  written by ron shepard.
c
c     navst(ist) - number of states to be averaged in symetry No. ist
c     navst(ist) as parameter added; when state averaging,
c     increase space needed for the c and m matrices (szh(11-15)).

      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      integer tsymdd(8),tsmdd2(8),tsymad(8),tsymaa(8)
      integer tsymvd(8),tsymva(8),tsymvv(8),tsmvv2(8)
      equivalence (nxy(1,21),tsymdd(1))
      equivalence (nxy(1,22),tsmdd2(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,24),tsymaa(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,28),tsmvv2(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
      equivalence (numint(12),totint)
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a),(ix0(3),ix0v)
c
      integer lvlprt
      common/prtout/lvlprt
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer adddd(*)
      integer adddd2(ndimd,ndimd)
      integer addaa(*),addvv(*)
      integer addad(ndimd,ndima),addvd(ndimv,ndimd),addva(ndimv,ndima)
      integer addvv2(ndimv,ndimv),addvv3(ndimv,ndimv)
      integer off1(*),off2(*),off3(*)
      integer off4(ndima,ndima)
      integer off5(*),off6(*),off7(*),off8(*),off9(*)
      integer off10(*),off11(*),off12(*),off13(*)
      integer off14(ndima,ndima)
c

c****************************************************************
c     avepar.h
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c****************************************************************
c

c****************************************************************
c     size.h
c
      integer msize,csize
      common/size/ msize(maxnst),csize(4,maxnst)
c****************************************************************
c

c**************************************************************
c     avstat.h
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c**************************************************************
c

c****************************************************************
c     drtf.h
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c****************************************************************
c
      integer cdir
      common/c_block/ cdir
c
      integer icount
      character*14 inttyp(11)
c
      nndxf(i)=(i*(i-1))/2
c
      totint=0
      do 5 i=1,12
5     numint(i)=0
c
c    # initialize local variable
      inttyp(1)='(pq|rs)'
      inttyp(2)='(pq|ri)'
      inttyp(3)='(pq|ia)'
      inttyp(4)='(pi|qa)'
      inttyp(5)='(pq|ra)'
      inttyp(6)='(pq|ij)(pq|ij)'
      inttyp(7)='(pq|ab)'
      inttyp(8)='(pa|qb)'
      inttyp(9)='p(ai,bp)'
      inttyp(10)='p(ai,jp)'
      inttyp(11)='p(ai,bj)'
c
c  calculate add** arrays and tsym** arrays.
c
      do 10 i=1,8
      tsymdd(i)=0
      tsmdd2(i)=0
      tsymad(i)=0
      tsymaa(i)=0
      tsymvd(i)=0
      tsymva(i)=0
      tsymvv(i)=0
      tsmvv2(i)=0
10    continue
c
c  dd...
c
      if(ndot.ne.0)then
         ij=0
         do 22 i=1,ndot
            do 20 j=1,i
               ij=ij+1
               ijsym=mult(symx(ix0d+j),symx(ix0d+i))
               tsymdd(ijsym)=tsymdd(ijsym)+1
               adddd(ij)=tsymdd(ijsym)
20          continue
22       continue
c      write(nlist,6020)'adddd:',(adddd(i),i=1,ij)
c6020  format(1x,a10,/(1x,10i6))
      endif
c
c  dd2...
c  smallest symmetry index has the most rapidly varying orbital index.
c  all entries are offset by the tsymdd(*) totals to allow the two
c  sets of integral types to be interleaved.
c
      if(ndot.ne.0)then
         do 38 isym=1,nsym
            if(ndpsy(isym).eq.0)go to 38
            i1=ird1(isym)
            i2=ird2(isym)
            do 36 jsym=1,nsym
               if(ndpsy(jsym).eq.0)go to 36
               j1=ird1(jsym)
               j2=ird2(jsym)
               ijsym=mult(jsym,isym)
               if(jsym.gt.isym)then
                  do 32 j=j1,j2
                     do 31 i=i1,i2
                        tsmdd2(ijsym)=tsmdd2(ijsym)+1
                        adddd2(j,i)=tsmdd2(ijsym)+tsymdd(ijsym)
31                   continue
32                continue
               else
                  do 34 i=i1,i2
                     do 33 j=j1,j2
                        tsmdd2(ijsym)=tsmdd2(ijsym)+1
                        adddd2(j,i)=tsmdd2(ijsym)+tsymdd(ijsym)
33                   continue
34                continue
c
               endif
36          continue
38       continue
      endif
c
c  ad...
c
      if(nact.ne.0 .and. ndot.ne.0)then
         do 42 p=1,nact
            do 40 i=1,ndot
               ipsym=mult(symx(ix0d+i),symx(ix0a+p))
               tsymad(ipsym)=tsymad(ipsym)+1
               addad(i,p)=tsymad(ipsym)
40          continue
42       continue
c      write(nlist,6020)'addad:',addad
      endif
c
c  aa...
c
      if(nact.ne.0)then
         pq=0
         do 62 p=1,nact
            do 60 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               tsymaa(pqsym)=tsymaa(pqsym)+1
               addaa(pq)=tsymaa(pqsym)
60          continue
62       continue
c      write(nlist,6020)'addaa:',(addaa(i),i=1,pq)
      endif
c
c  vd...
c
      if(nvrt.ne.0 .and. ndot.ne.0)then
      do 82 i=1,ndot
         do 80 a=1,nvrt
            iasym=mult(symx(ix0v+a),symx(ix0d+i))
            tsymvd(iasym)=tsymvd(iasym)+1
            addvd(a,i)=tsymvd(iasym)
80       continue
82    continue
c      write(nlist,6020)'addvd:',addvd
      endif
c
c  va...
c
      if(nvrt.ne.0 .and. nact.ne.0)then
         do 102 p=1,nact
            do 100 a=1,nvrt
               pasym=mult(symx(ix0v+a),symx(ix0a+p))
               tsymva(pasym)=tsymva(pasym)+1
               addva(a,p)=tsymva(pasym)
100         continue
102      continue
c      write(nlist,6020)'addva:',addva
      endif
c
c  vv...
c
      if(nvrt.ne.0)then
         ab=0
         do 122 a=1,nvrt
            do 120 b=1,a
               ab=ab+1
               absym=mult(symx(ix0v+b),symx(ix0v+a))
               tsymvv(absym)=tsymvv(absym)+1
               addvv(ab)=tsymvv(absym)
120         continue
122      continue
c      write(nlist,6020)'addvv:',(addvv(i),i=1,ab)
      endif
c
c  vv2...
c  smallest symmetry index has the most rapidly varying orbital index.
c
      if(nvrt.ne.0)then
         do 140 asym=1,nsym
            if(nvpsy(asym).eq.0)go to 140
            a1=irv1(asym)
            a2=irv2(asym)
            do 138 bsym=1,nsym
               if(nvpsy(bsym).eq.0)go to 138
               b1=irv1(bsym)
               b2=irv2(bsym)
               absym=mult(bsym,asym)
               if(bsym.gt.asym)then
                  do 133 b=b1,b2
                     do 132 a=a1,a2
                        tsmvv2(absym)=tsmvv2(absym)+1
                        addvv2(b,a)=tsmvv2(absym)
132                  continue
133               continue
               else
                  do 135 a=a1,a2
                     do 134 b=b1,b2
                        tsmvv2(absym)=tsmvv2(absym)+1
                        addvv2(b,a)=tsmvv2(absym)
134                  continue
135               continue
               endif
138         continue
140      continue
c      write(nlist,6020)'addvv2:',addvv2
      endif
c
c  vv3...
c  each symmetry block is addressed independently.
c
      if(nvrt.ne.0)then
         do 160 asym=1,nsym
            if(nvpsy(asym).eq.0)go to 160
            a1=irv1(asym)
            a2=irv2(asym)
            do 158 bsym=1,nsym
               if(nvpsy(bsym).eq.0)go to 158
               b1=irv1(bsym)
               b2=irv2(bsym)
c
               ba=0
               do 157 a=a1,a2
                  do 156 b=b1,b2
                     ba=ba+1
                     addvv3(b,a)=ba
156               continue
157            continue
158         continue
160      continue
c      write(nlist,6020)'addvv3',addvv3
      endif
c
c  calculate offset arrays for various integral types.
c
c*********************************************************
c  [(pq|rs)] = off1(pq)+addaa(rs)
c*********************************************************
c
      if(nact.ne.0)then
         totoff=0
         pq=0
         do 212 p=1,nact
            do 210 q=1,p
               pq=pq+1
               off1(pq)=totoff
               totoff=totoff+addaa(pq)
210         continue
212      continue
         numint(1)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off1:',(off1(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ri)] = off2(pq)+addad(i,r)
c*********************************************************
c
      if(nact.ne.0 .and. ndot.ne.0)then
         totoff=0
         pq=0
         do 412 p=1,nact
            do 410 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off2(pq)=totoff
               totoff=totoff+tsymad(pqsym)
410         continue
412      continue
         numint(2)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off2:',(off2(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ia)] = off3(pq)+addvd(a,i)
c*********************************************************
c
      if((nact.ne.0).and.(ndot.ne.0).and.(nvrt.ne.0))then
         totoff=0
         pq=0
         do 712 p=1,nact
            do 710 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off3(pq)=totoff
               totoff=totoff+tsymvd(pqsym)
710         continue
712      continue
         numint(3)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off3:',(off3(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pi|qa)] = off4(q,p)+addvd(a,i)
c*********************************************************
c
      if(nact.ne.0 .and. ndot.ne.0 .and. nvrt.ne.0)then
         totoff=0
         do 812 p=1,nact
            do 810 q=1,nact
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off4(q,p)=totoff
               totoff=totoff+tsymvd(pqsym)
810         continue
812      continue
         numint(4)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off4:',off4,totoff
      endif
c
c*********************************************************
c  [(pq|ra)] = off5(pq)+addva(a,r)
c*********************************************************
c
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 312 p=1,nact
            do 310 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off5(pq)=totoff
               totoff=totoff+tsymva(pqsym)
310         continue
312      continue
         numint(5)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off5:',(off5(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ij)] = off6(pq)+adddd(ij)
c  [(pi|qj)] = off6(pq)+adddd2(j,i)
c*********************************************************
c
      if(nact.ne.0 .and. ndot.ne.0)then
         totoff=0
         pq=0
         do 512 p=1,nact
            do 510 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off6(pq)=totoff
               totoff=totoff+tsymdd(pqsym)+tsmdd2(pqsym)
510         continue
512      continue
         numint(6)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off6:',(off6(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ab)] = off7(pq)+addvv(ab)
c*********************************************************
c
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 912 p=1,nact
            do 910 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off7(pq)=totoff
               totoff=totoff+tsymvv(pqsym)
910         continue
912      continue
         numint(7)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off7:',(off7(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pa|qb)] = off8(pq)+addvv(b,a)
c*********************************************************
c
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 1012 p=1,nact
            do 1010 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off8(pq)=totoff
               totoff=totoff+tsmvv2(pqsym)
1010        continue
1012     continue
         numint(8)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off8:',(off8(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [p(ai,bp)] = off9(ai)+addva(b,p)
c*********************************************************
c
      if(tsymvd(1).ne.0 .and. tsymva(1).ne.0)then
         totoff=0
         do 1510 ai=1,tsymvd(1)
            off9(ai)=totoff
            totoff=totoff+tsymva(1)
1510     continue
         numint(9)=totoff
         totint=totint+totoff
      endif
c
c*********************************************************
c  [p(ai,jp)] = off10(ai)+addad(j,p)
c*********************************************************
c
      if(tsymad(1).ne.0 .and. tsymvd(1).ne.0)then
         totoff=0
         do 1710 ai=1,tsymvd(1)
            off10(ai)=totoff
            totoff=totoff+tsymad(1)
1710     continue
         numint(10)=totoff
         totint=totint+totoff
      endif
c
c*********************************************************
c  [p(ai,bj)] = off11(ij)+addvv3(b,a)
c*********************************************************
c
      szh6=0
      if(ndot.ne.0 .and. nvrt.ne.0)then
         totoff=0
         ij=0
         do 1812 i=1,ndot
            na=nvpsy(symx(ix0d+i))
            do 1810 j=1,i
               ij=ij+1
               nb=nvpsy(symx(ix0d+j))
               off11(ij)=totoff
               totoff=totoff+na*nb
1810        continue
1812     continue
         szh6=totoff
         numint(11)=totoff
c      write(nlist,6020)'off11:',(off11(i),i=1,ij),totoff
      endif
c
      if (lvlprt.gt.1) then
      write(nlist,'(/'' Total number of integrals addressed: '',i8/)')
     + totint
      write(nlist,'('' Int.type'',10x,''no.of ints'')')
         do i=1,11
         write(nlist,'(2x,a14,3x,i8)')inttyp(i),numint(i)
         enddo
      endif
c
c*********************************************************
c  [b(qb,pa)]  = off12(pq)+ (b,a)
c*********************************************************
c
      szh10=0
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 1912 p=1,nact
            na=nvpsy(symx(ix0a+p))
            do 1910 q=1,p
               pq=pq+1
               nb=nvpsy(symx(ix0a+q))
               off12(pq)=totoff
               totoff=totoff+na*nb
1910        continue
1912     continue
         szh10=totoff
      endif
c
c*********************************************************
c  [b(ip|jq)] = off13(pq) + (j,i)
c*********************************************************
c
      szh1=0
      if(ndot.ne.0 .and. nact.ne.0)then
         totoff=0
         pq=0
         do 2012 p=1,nact
            ni=ndpsy(symx(ix0a+p))
            do 2010 q=1,p
               nj=ndpsy(symx(ix0a+q))
               pq=pq+1
               off13(pq)=totoff
               totoff=totoff+ni*nj
2010        continue
2012     continue
         szh1=totoff
      endif
c
c*********************************************************
c  [b(ap,iq)] = off14(p,q) + (a,i)
c*********************************************************
c
      szh7=0
      if(nvrt.ne.0 .and. nact.ne.0 .and. ndot.ne.0)then
         totoff=0
         do 2111 q=1,nact
            ni=ndpsy(symx(ix0a+q))
            do 2110 p=1,nact
               na=nvpsy(symx(ix0a+p))
               off14(p,q)=totoff
               totoff=totoff+na*ni
2110        continue
2111     continue
         szh7=totoff
      endif
c
c  save hessian sizes for the various blocks.
c  naar=number of allowed active-active rotations.
c  nadr=number of active-inactive rotations.
c  nvdr=number of virtual-inactive rotations.
c  nvar=number of virtual-active rotations.
c  ncsf=number of csfs in the wave function expansion.
c
      nadr=tsymad(1)
      nvdr=tsymvd(1)
      nvar=tsymva(1)
c  ad,ad
c  stored as a sequence (1...naa) of rectangular (nj,ni) blocks.
      szh(1)=szh1
c  aa,ad
      szh(2)=naar*nadr
c  aa,aa
      szh(3)=nndxf(naar+1)
c  vd,ad
      if (flags(22)) then
         szh(4)=min(1,nvdr*nadr)
      else
         szh(4)=nvdr*nadr
      endif
c  vd,aa
      szh(5)=nvdr*naar
c  vd,vd
c  stored as a sequence (1...ndd) of rectangular (nb,na) blocks.
      if (flags(22)) then
         szh(6)=min(1,szh6)
      else
         szh(6)=szh6
      endif
c  va,ad
c  stored as a sequence (1...na*na) of rectangular (na,ni) blocks.
      szh(7)=szh7
c  va,aa
      szh(8)=nvar*naar
c  va,vd
      if (flags(22)) then
         szh(9)=min(1,nvar*nvdr)
      else
         szh(9)=nvar*nvdr
      endif
c  va,va
c  stored as a sequence (1...naa) of rectangular (nb,na) blocks.
      szh(10)=szh10
c    for state averaging change the C and M matrix dimesions
c
      icount = 0
      do ist=1,nst
      icount = icount + navst(ist)*ncsf_f(ist)
      enddo
c  csf,ad
      szh(11)=icount*nadr
      if (cdir.eq.1) szh(11) = 0
c  csf,aa
      szh(12)=icount*naar
      if (cdir.eq.1) szh(12) = 0
c  csf,vd
      szh(13)=icount*nvdr
      if (cdir.eq.1) szh(13) = 0
c  csf,va
      szh(14)=icount*nvar
      if (cdir.eq.1) szh(14) = 0
c
c     dimensions for nst C matrix blocks
c
      do ist=1,nst
c
c  csf,ad
      csize(1,ist) = navst(ist)*ncsf_f(ist)*nadr
      if (cdir.eq.1) csize(1,ist) = 0
c  csf,aa
      csize(2,ist) = navst(ist)*ncsf_f(ist)*naar
      if (cdir.eq.1) csize(2,ist) = 0
c  csf,vd
      csize(3,ist) = navst(ist)*ncsf_f(ist)*nvdr
      if (cdir.eq.1) csize(3,ist) = 0
c  csf,vd
      csize(4,ist) = navst(ist)*ncsf_f(ist)*nvar
      if (cdir.eq.1) csize(4,ist) = 0
c
c    dimensions for nst M matrix blocks
c
c  csf,csf
      if (.not.flags(11).and.flags(12)) then
      szh(15)=0
      msize(ist) = 0
      else
      msize(ist) = nndxf(navst(ist)*ncsf_f(ist)+1)
      szh(15) = szh(15) + msize(ist)
      endif
c
      enddo

      return
      end
c
c************************************************************
c
      subroutine blkasn(avchc,avc2is,qcoupl)
c
c  assign hessian block construction steps to ft reads.
c  assign integral blocks to buckets for sorting.
c  (pq|rs) integrals, if any, are always placed in bucket number 1.
c  other integral blocks follow.
c
c  input:
c  avchc  = amount of core available during hessian construction steps.
c  avc2is = amount of core available during integral sorting step.
c  qcoupl = .true. if c* blocks are to be constructed.
c  lenbfs = length of the sequential integral buffers.
c
c  output:
c  bukpt(*) = bucket pointers for sorting of the required 2e integrals.
c  intoff(*)= integral address offsets used during sorting.
c  szh(*)   = size of the 14 hessian blocks.
c  hbci(*,1)= hessian block ordering during construction.
c  hbci(*,2)= segment divisions of hessian blocks.
c  hbci(*,3)= blocks requiring transition density matrices (c-blocks).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(9),n2td)
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(15),n2tv)
      integer tsymad(8),tsymvd(8),tsymva(8)
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxy(1,26),tsymva(1),nvat)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
      equivalence (bukpt(12),nbukt)
c
      common/chbci/hbci(4,3)
c
      common/cbufs/stape,lenbfs,h2bpt
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer cdir
      common/c_block/ cdir
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer lvlprt
      common/prtout/lvlprt
c
      logical qcoupl
c
      logical qcx,qix,qc(4),qi(4)
      integer leftb(10),sph(14)
      integer si(4),sb1(4),sb2(4),sc(4),ioff(4)
      integer order(4),ift(4),ibuk(4),iseg(4)
      integer iftmin(4),ibkmin(4),segmin(4)
      equivalence (hbci(1,1),order(1)),(hbci(1,3),iftmin(1))
c
      integer  forbyt
      external forbyt
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer perm(4,24)
      data perm/1,2,3,4, 2,1,3,4, 1,3,2,4, 3,1,2,4, 2,3,1,4, 3,2,1,4,
     +          1,2,4,3, 2,1,4,3, 1,4,2,3, 4,1,2,3, 2,4,1,3, 4,2,1,3,
     +          1,3,4,2, 3,1,4,2, 1,4,3,2, 4,1,3,2, 3,4,1,2, 4,3,1,2,
     +          2,3,4,1, 3,2,4,1, 2,4,3,1, 4,2,3,1, 3,4,2,1, 4,3,2,1/
c
c  initialize hbci(*) and sorting arrays.
c
      do 1 j=1,3
      do 1 i=1,4
1     hbci(i,j)=0
      do 2 i=1,12
      bukpt(i)=0
2     intoff(i)=0
c
c  assign space requirements for hessian block construction.
c  space = size-of-hessian + scratch-workspace
c  see hbcon and individual block construction routines for details
c  of the workspace allocation.
c
      nnact=nndx(nact+1)
      sph(1)=szh(1)+5*n2td+lenbfs
      sph(2)=max(szh(2),2*n2ta)
      sph(3)=szh(3)+2*forbyt(nnact+1)+forbyt(2*nact**2)+4*n2ta
      sph(4)=szh(4)+n2ta+lenbfs
      sph(5)=szh(5)+nvdt
      sph(6)=szh(6)+lenbfs
      sph(7)=szh(7)+nvdt
      sph(8)=max(szh(8),nvat,n2ta)
      sph(9)=szh(9)+n2ta+lenbfs
      sph(10)=szh(10)+3*n2tv+lenbfs
      if(qcoupl)then
          sph(11)=szh(11)
          sph(12)=szh(12)
          sph(13)=szh(13)
          sph(14)=szh(14)
      else
          sph(11)=0
          sph(12)=0
          sph(13)=0
          sph(14)=0
      endif
c
c-    write(nlist,6050)'integral dimensions:',numint
c-    write(nlist,6050)'hessian dimensions:',szh
c-    write(nlist,6050)'hessian space requirements:',sph
6050  format(1x,a,/(1x,5i8))
c
c  hessian matrix must be constructed in one read of the integrals.
c  up to four ft reads are allowed.  hessian blocks 11,12,13,14 must
c  be constructed while reading the ft.  blocks 5,7,8,2,3 must also be
c  constructed when the integrals are available during the ft reads.
c  the remaining blocks, 1,4,6,9,10, are constructed independently
c  of the ft reads.
c
c  check space for b matrix blocks to be constructed.
c
      leftb(5)=avchc-sph(5)-numint(3)-numint(4)
      leftb(7)=avchc-sph(7)-numint(3)-numint(4)
      leftb(8)=avchc-sph(8)-numint(5)
      leftb(2)=avchc-sph(2)-numint(2)
      leftb(3)=avchc-sph(3)-numint(1)
c
      leftb(1)=avchc-sph(1)
      leftb(4)=avchc-sph(4)
      leftb(6)=avchc-sph(6)
      leftb(9)=avchc-sph(9)
      leftb(10)=avchc-sph(10)
      left=avchc
      do 10 i=1,10
10    left=min(leftb(i),left)
c-    write(nlist,6080)left,leftb
6080  format(' minimum core left during b-block construction=',i8/
     +  1x,10i8)
      if(left.lt.0)go to 2000
c
c  calculate space requirements for the first blocks to
c  be constructed.
c
      nfirst=1
      si(nfirst)=numint(1)
      sb1(nfirst)=sph(3)
      sb2(nfirst)=0
      sc(nfirst)=sph(12)
      qi(nfirst)=.false.
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
      nfirst=2
      si(nfirst)=numint(2)
      sb1(nfirst)=sph(2)
      sb2(nfirst)=0
      sc(nfirst)=sph(11)
      qi(nfirst)=si(nfirst).ne.0
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
      nfirst=3
      si(nfirst)=numint(3)+numint(4)
      sb1(nfirst)=sph(5)
      sb2(nfirst)=sph(7)
      sc(nfirst)=sph(13)
      qi(nfirst)=si(nfirst).ne.0
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
      nfirst=4
      si(nfirst)=numint(5)
      sb1(nfirst)=sph(8)
      sb2(nfirst)=0
      sc(nfirst)=sph(14)
      qi(nfirst)=si(nfirst).ne.0
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
c  determine the number of ft reads and integral buckets
c  for all possible orders of the blocks of the c matrix.
c
      leftm1=0
      leftmx=0
      nftmin=99
      nbkmin=99
      pmin=1
c
      do 600 iperm=1,24
c
c  for a given order, determine the number of segments required.
c
      sit=0
      sct=0
      nseg=1
      left1=avchc
      left=avchc
c
      do 400 n=1,4
      iblk=perm(n,iperm)
c
c  try to put current blocks into current segment.
c
100   continue
      sctx=sct+sc(iblk)
      sitx=sit+si(iblk)
      s1=sctx+sitx
      s2=sitx+sb1(iblk)
      s3=sitx+sb2(iblk)
      if(max(s1,s2,s3).le.avchc)go to 200
c
c  extension fails.
c
      if(sit.eq.0 .and. sct.eq.0)then
           left=avchc-max(s1,s2,s3)
           go to 2000
      endif
c
      nseg=nseg+1
      sit=0
      sct=0
      go to 100
c
200   continue
c
c  extension fits.
c
      sit=sitx
      sct=sctx
      left=min(left,avchc-max(s1,s2,s3))
      if(iblk.eq.1)left1=avchc-s2
      iseg(n)=nseg
400   continue
c
c  for the resulting segment structure, determine the number
c  of ft reads and integral buckets.
c
      nbuk=0
      if(numint(1).ne.0)nbuk=1
      nft=0
      do 440 is=1,nseg
      qcx=.false.
      qix=.false.
      do 420 i=1,4
      if(iseg(i).ne.is)go to 420
      iblk=perm(i,iperm)
      qcx=qcx.or.qc(iblk)
      qix=qix.or.qi(iblk)
420   continue
      if(qcx)nft=nft+1
      if(qix)nbuk=nbuk+1
      do 430 i=1,4
      if(iseg(i).ne.is)go to 430
      iblk=perm(i,iperm)
      ift(i)=0
      if(qc(iblk))ift(i)=nft
      ibuk(i)=0
      if(qi(iblk))ibuk(i)=nbuk
      if(iblk.eq.1 .and. numint(1).ne.0)ibuk(i)=1
430   continue
440   continue
c
      if (lvlprt.ge.1)
     & write(nlist,6010)iperm,nft,nbuk,left,ift,ibuk,left1
6010  format(1x,i2,' nft=',i1,' nbuk=',i1,' left=',i7,
     +  ' ift(*)=',4i2,' ibuk(*)=',4i2,' left1=',i7)
c
c  determine if present permutation is the best.
c
      if(nft-nftmin)490,460,600
460   if(nbuk-nbkmin)490,470,600
470   if(leftm1-left1)490,480,600
480   if(leftmx-left)490,600,600
490   continue
c
c  save info: new order produces the fewest ft reads, the fewest buckets
c  the largest amount of core left during baaaa construction, and
c  the largest amount of core left during ft reads (in that order).
c
      nftmin=nft
      nbkmin=nbuk
      leftm1=left1
      leftmx=left
      pmin=iperm
      do 500 i=1,4
      order(i)=perm(i,pmin)
      segmin(i)=iseg(i)
      iftmin(i)=ift(i)
      ibkmin(i)=ibuk(i)
500   continue
c
600   continue
c
      if (lvlprt.ge.1)
     + write(nlist,6030)nftmin,nbkmin,leftm1,
     +  leftmx,order,iftmin,ibkmin,segmin
6030  format(/' number of ft reads required=',i2/
     +  ' number of buckets required=',i2/
     +  ' amount of core left during baaaa=',i8/
     +  ' amount of core left=',i8/
     +  ' order(*)= ',4i2/
     +  ' iftmin(*)=',4i2/
     +  ' ibkmin(*)=',4i2/
     +  ' segmin(*)=',4i2)
c
c  calculate integral offsets and bucket pointers for the first
c  five blocks of integrals.
c
      do 640 i=1,4
640   ioff(i)=0
      do 750 i=1,4
      buk=ibkmin(i)
      if(buk.eq.0)go to 750
      is=segmin(i)
      iblk=order(i)
      go to (710,720,730,740),iblk
710   continue
c
c  block 1. integral type 1, (pq|rs).
c
      bukpt(1)=buk
      intoff(1)=ioff(is)
      ioff(is)=ioff(is)+numint(1)
      go to 750
720   continue
c
c  block 2. integral type 2, (pq|ri).
c
      bukpt(2)=buk
      intoff(2)=ioff(is)
      ioff(is)=ioff(is)+numint(2)
      go to 750
730   continue
c
c  block 3. integral types 3 and 4, (pq|ia) and (pi|qa).
c
      bukpt(3)=buk
      intoff(3)=ioff(is)
      ioff(is)=ioff(is)+numint(3)
      bukpt(4)=buk
      intoff(4)=ioff(is)
      ioff(is)=ioff(is)+numint(4)
      go to 750
740   continue
c
c  block 4. integral type 5, (pq|ra)
c
      bukpt(5)=buk
      intoff(5)=ioff(is)
      ioff(is)=ioff(is)+numint(5)
750   continue
c
c  remaining integral offsets for integral types 6 thru 11.
c  bukpt(i)=0 is the flag to not sort integral type i.
c
      do 760 i=6,11
760   if(numint(i).ne.0)bukpt(i)=nbuk+1
c
      nit=0
c
c  integral types 6 contribute to fdd(*) and badad.
c
      intoff(6)=nit
      nit=nit+numint(6)
c
c  integral types 7 and 8 contribute to qvv(*) and bvdva.
c
      intoff(7)=nit
      nit=nit+numint(7)
      intoff(8)=nit
      nit=nit+numint(8)
c
c  integral types 9 contribute to bvavd.
c
      intoff(9)=nit
      if(szh(9).ne.0)nit=nit+numint(9)
      if(szh(9).eq.0)bukpt(9)=0
c
c  integral types 10 contribute to bvdad.
c
      intoff(10)=nit
      if(szh(4).ne.0)nit=nit+numint(10)
      if(szh(4).eq.0)bukpt(10)=0
c
c  integral types 11 contribute to bvdvd.
c
      intoff(11)=nit
      if(szh(6).ne.0)nit=nit+numint(11)
      if(szh(6).eq.0)bukpt(11)=0
c
      intoff(12)=nit
      nbukt=nbuk+ ((nit-1)/avc2is) +1
c
c-    write(nlist,6090)ioff,intoff,bukpt,nbukt,nit
6090  format(' ioff(*)=',4i8/' intoff(*)=',6i8/1x,6i8/
     +  ' bukpt(*)=',12i2/' nbukt=',i6,/' nit=',i8)
c
c  save info for hessian block construction steps.
c
      do 800 i=1,4
      is=segmin(i)
      if(is.ne.0)hbci(is,2)=hbci(is,2)+1
800   continue
c
      if (lvlprt.ge.1)
     & write(nlist,6040)hbci
6040  format(' hbci(*,*)='/(1x,4i3))
c
      sz=0
      do imd=1,10
        sz=sz+szh(imd)
      enddo
      write(nlist,
     & '(/," Size of orbital-Hessian matrix B: ",t45,i15)')sz
      if (flags(25)) then
      write(nlist,'("  1. B[ad,ad]",i15)')szh(1)
      write(nlist,'("  2. B[aa,ad]",i15)')szh(2)
      write(nlist,'("  3. B[aa,aa]",i15)')szh(3)
      write(nlist,'("  4. B[vd,ad]",i15)')szh(4)
      write(nlist,'("  5. B[vd,aa]",i15)')szh(5)
      write(nlist,'("  6. B[vd,vd]",i15)')szh(6)
      write(nlist,'("  7. B[va,ad]",i15)')szh(7)
      write(nlist,'("  8. B[va,aa]",i15)')szh(8)
      write(nlist,'("  9. B[va,vd]",i15)')szh(9)
      write(nlist,'("  10.B[va,va]",i15,/)')szh(10)
      endif

      sz=0
      do imd=11,14
      sz=sz+szh(imd)
      enddo
      if (qcoupl) write(nlist,
     & '(" Size of the orbital-state Hessian matrix C:",t45,i15)')
     & sz
      if (flags(25)) then
      write(nlist,'("  11.C[csf,ad]",i15)')szh(11)
      write(nlist,'("  12.C[csf,aa]",i15)')szh(12)
      write(nlist,'("  13.C[csf,vd]",i15)')szh(13)
      write(nlist,'("  14.C[csf,va]",i15,/)')szh(14)
      endif
c
      write(nlist,
     & '(" Total size of the state Hessian matrix M:",t45,i15)')
     &  szh(15)
c
      sz=0
      if (flags(2).and.qcoupl) then
         do imd=1,15
         sz=sz+szh(imd)
         enddo
         write(nlist,3000)sz
      else
         do imd=1,10
         sz=sz+szh(imd)
         enddo
         sz = sz + szh(15)
         write(nlist,3010)sz
      endif
3000  format(
     & ' Size of HESSIAN-matrix for quadratic conv.:',
     & t45,i15,/)
3010  format(
     & ' Total size of HESSIAN-matrix for linear conv.:',
     & t45,i15,/)
c
      if (sz.gt.avc2is) then
         write(nlist,'("Not enough memory for SOLVEK")')
         left = avc2is - sz
         call bummer('blkasn (test for solvek): left=',left,faterr)
      endif
c
      return
c
2000  write(nlist,6200)avchc,left
cfp
      write(nlist,*)'*** Not enough core!'
      write(nlist,*)'*** Try uncoupled iterations: npath=-2 in mcscfin'
6200  format(/' *** blkasn: avchc=',i15,' left=',i15,' ***')
      call bummer('blkasn: left=',left,faterr)
      return
      end
      subroutine cxaxa(h2,hx,sym,symt,nvpsy,n2tv,nterm,absym,ioff)
c
c  copy exchange type integrals, (px|qy), from the input
c  buffer, h2(*), to workspace area, hx(*).  indices x and y
c  may both be virtual or both be inactive (the code
c  is written using virtual orbital indices).
c  for absym=1, the integral blocks are also transposed.
c
c  input:
c    h2(*)    = input buffer.
c    nvpsy(*) = orbitals per symmetry block.
c    n2tv     = square-packed matrix length.
c    nterm    = number of input elements.
c    absym    = a,b product symmetry.
c    ioff     = integral offset for hx(*).
c  output:
c    hx(*)    = workspace.
c    sym(*)   = symmetry pointer array.
c    symt(*)  = symmetry pointer array.
c    ioff     = updated to include copied integrals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer sym(8),symt(8),nvpsy(8)
      real*8 h2(*),hx(*)
c
c  copy integrals from buffer to workspace area.
c
      call dcopy_wr(nterm,h2,1,hx(ioff+1),1)
c
      if(absym.eq.1)go to 1000
c
c  asym.ne.bsym case.  integrals in h2(*) are in the correct
c  order.  adjust symmetry block pointers.
c  for asym.gt.bsym, sym(asym) is used to point to (b1,a1),
c  while symt(bsym) is used to point to (a1,b1).
c
      ab=ioff+1
      do 100 asym=1,nsym
      sym(asym)=ab
      symt(asym)=ab
      ab=ab+nvpsy(asym)*nvpsy(mult(asym,absym))
100   continue
      ioff=ab-1
      return
c
1000  continue
c
c  asym=bsym case.  integral blocks must be transposed and
c  the appropriate pointers adjusted.
c
      a1=ioff+1
      at1=a1+n2tv
      do 1300 asym=1,nsym
      sym(asym)=a1
      symt(asym)=at1
c
      na=nvpsy(asym)
      if(na.eq.0)go to 1300
      call mtrans(hx(a1),1,hx(at1),1,na,na)
      a1=a1+na*na
      at1=at1+na*na
1300  continue
      ioff=at1-1
      return
      end
      subroutine cxxaa(h2,hx,sym,nvpsy,nterm,absym,ioff)
c
c  copy direct type integrals, (pq|xy), from the input
c  buffer, h2(*), to workspace area, hx(*).  indices x and y
c  may both be virtual or both be inactive (the code
c  is written using virtual orbital indices).
c  absym=1 integrals are expanded from triangular-packed to
c  square-packed by symmetry blocks.
c
c  input:
c    h2(*)    = input buffer.
c    nvpsy(*) = orbitals per symmetry block.
c    nterm    = number of input elements.
c    absym    = a,b product symmetry.
c    ioff     = integral offset for hx(*).
c  output:
c    hx(*)    = workspace.
c    sym(*)   = symmetry pointer array.
c    ioff     = updated to include copied integrals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer sym(8),nvpsy(8)
      real*8 h2(*),hx(*)
c
      ab=ioff+1
      do 20 asym=1,nsym
      sym(asym)=ab
      bsym=mult(asym,absym)
      if(bsym.le.asym)ab=ab+nvpsy(asym)*nvpsy(bsym)
20    continue
      ioffx=ab-1
c
      if(absym.eq.1)go to 1000
c
c  asym.ne.bsym case.  integrals in h2(*) are in the correct
c  order.  copy to hx(*) and return.
c
      call dcopy_wr(nterm,h2,1,hx(ioff+1),1)
      ioff=ioffx
      return
c
1000  continue
c
c  asym=bsym case.  integrals must be expanded from lower-triangular
c  packed by symmetry to rectangular packed by symmetry.
c
      aboff=ioff
      add0=0
      ba0=ioff
      do 1300 asym=1,nsym
      na=nvpsy(asym)
      if(na.eq.0)go to 1300
      do 1200 a=1,na
      ab=aboff+a
cdir$ ivdep
cvocl loop,novrec
      do 1100 b=1,a
      hx(ab)=h2(add0+b)
      hx(ba0+b)=h2(add0+b)
      ab=ab+na
1100  continue
      ba0=ba0+na
      add0=add0+a
1200  continue
      aboff=aboff+na*na
1300  continue
      ioff=ioffx
      return
      end
      subroutine dxyxy(b,d,offxx,nsym,irx1,irx2,nxpsy,nypsy)
c
c  copy the diagonal elements from a diagonal hessian matrix block
c  in b(*) to the vector d(*).  the hessian elements are assumed
c  to be referenced as:
c
c  [b(xy,xy)] = offxx(xx) + [(y,y)]
c
c  this addressing method is used for the adad, vdvd, and vava blocks
c  of the hessian matrix where xy=ad,dv, and av orbital types.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 b(*),d(*)
      integer offxx(*)
      integer irx1(nsym),irx2(nsym),nxpsy(nsym),nypsy(nsym)
c
      yx=1
      do 200 isym=1,nsym
      if(nxpsy(isym).eq.0)go to 200
      if(nypsy(isym).eq.0)go to 200
      x1=irx1(isym)
      x2=irx2(isym)
      ny=nypsy(isym)
      nyp1=ny+1
c
      do 100 x=x1,x2
      xyxy=offxx(nndx(x+1))+1
      call dcopy_wr(ny,b(xyxy),nyp1,d(yx),1)
      yx=yx+ny
100   continue
200   continue
      return
      end
      subroutine ecore(x,ecore0)
c
c  calculate core reference energy.
c
c  equation used is:
c
c      ecore0 = sum(i) ( h(ii) + u(ii) )
c
c  where h(ii) is the one-electron hamiltonian matrix element,
c  including any frozen core contributions, corresponding to
c  inactive orbital number i, and where u(ii) is the modified
c  hamiltonian matrix element for inactive orbital number i.
c  note that this routine is called twice each mcscf iteration.
c
c  first call:
c    input:
c      x(*)    = h(*).
c      ecore0  = should be initialized to zero.
c    output:
c      ecore0  = the first part of the summation.
c
c  second call:
c    input:
c      x(*)    = u(*).
c      ecore0  = result from first call.
c    output:
c      ecore0  = result from both summations.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 x(*),ecore0
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsm(8),nnsm(8)
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxtot(7),ndot)
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx),invx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (ix0(1),ix0d)
c
      if(ndot.eq.0)return
c
c  loop over non-frozen doubly-occupied (inactive) orbitals.
c
      do 100 i=1,ndot
100   ecore0=ecore0+x(nnsm(symx(ix0d+i))+nndx(invx(ix0d+i)+1))
c
      return
      end
      subroutine expmk(n,k,a,b,x)
c
c  form the matrix exp(-k).
c
c  input:
c  n    = matrix dimension.
c  k(*) = anti-symmetric matrix k.
c  a(*) = scratch matrix.
c  b(*) = scratch matrix.
c  x(*) = scratch vector.
c
c  output:
c  k(*) = overwritten with exp(-k).
c
c  generic functions used: cos,max,sin,sqrt
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 k(n,n),a(n,n),b(n,n),x(n)
c
      real*8 zero,one,term
      parameter(zero=0d0,one=1d0)
c
      if(n.eq.1)then
         k(1,1)=one
         return
      endif
c
c  if  k2 = k * k  defines the secular equation,
c       k2 * u = u * l
c  then l(i) .le. zero for all i.
c  let d(i)=sqrt(-l(i)) then:
c
c  exp(-k) = -k*u*sin(d)*d**(-1)*u(transpose)
c            +u*cos(d)*u(transpose)
c
c  this allows the exp(-k) matrix to be calculated directly without
c  recourse to expansions and requires only the diagonalization of the
c  symmetric matrix k2.
c
c  form k2 = k * k
c  a=k*k
c  only the lower triangle is actually required in the eispac call.
c
      call mxma(k,1,n, k,1,n, a,1,n, n,n,n)
c
c  solve k2*u=u*l.
c  a=u, x=l
c
      call eigip(n,n,a,x,b)
c
c  form d=sqrt(-l).
c  x=d
c
      do 10 i=1,n
10    x(i)=sqrt(max(zero,-x(i)))
c
c  form k * u.
c  b=k*a
c
      call mxma(k,1,n, a,1,n, b,1,n, n,n,n)
c
c  form -k * u * sin(d) * d**(-1).
c  b=b*(-sin(x)/x)
c
      do 20 i=1,n
      term=-one
      if(one+x(i).ne.one)term=-sin(x(i))/x(i)
20    call dscal_wr(n,term,b(1,i),1)
c
c  form u * cos(d).
c  k=a*cos(x)
c
      call dcopy_wr(n*n,a,1,k,1)
      do 30 i=1,n
      term=cos(x(i))
30    call dscal_wr(n,term,k(1,i),1)
c
c  form ( -k*u*sin(d)*d**(-1) + u*cos(d) ).
c  b=b+k
c
      call daxpy_wr(n*n,one,k,1,b,1)
c
c  multiply on the right by u(transpose).
c  k=b*a(transpose)
c
      call mxma(b,1,n, a,n,1, k,1,n, n,n,n)
c
c  exp(-k) is stored in k(*).
c
      return
      end
      subroutine faar(naar,iorder,iwop)
c
c  find the allowed active-active rotations.  check the
c  fcimsk(*) array for invariant subspace specifications.
c
c  input:
c  fcimsk(i) has non-zero decimal digits in the fields
c            corresponding to the subspaces to which orbital i belongs.
c
c  output:
c  iorder(pq) is non-zero for allowed active-active rotations and
c             is zero for disallowed rotations.
c  iwop(*,r)  contains the p, q indices for allowed rotation number r.
c  naar       is the number of allowed active-active rotations.
c
c  example:
c    input:
c    fcimsk(1)=11  ;in this example rotations between orbitals (2,1)
c    fcimsk(2)=10  ;and (3,1) are disallowed while the rotation (3,2)
c    fcimsk(3)=01  ;is allowed. all rotations (4,*) are allowed. note
c    fcimsk(4)=00  ;that usually the subspaces involve disjoint sets
c                  ;of orbitals.
c    output:
c    iorder(*)=0, 0,0, 0,1,0, 2,3,4,0
c    iwop(*,*)=3,2, 4,1, 4,2, 4,3
c    naar=4
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsm(8)
      equivalence (nxy(1,6),nsm(1))
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(10),nact)
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer invx(nmotx),nndx(nmotx),fcimsk(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (iorbx(1,8),fcimsk(1))
      equivalence (ix0(2),ix0a)
c
      integer iwop(2,*),iorder(*)
c
      nnact=nndx(nact+1)
      do 10 i=1,nnact
10    iorder(i)=0
c
      naar=0
      naarp=0
      do 300 psym=1,nsym
      np=napsy(psym)
      if(np.lt.2)go to 300
      naarp=naarp+nndx(np)
c
      m0=nsm(psym)
      q1=ira1(psym)
      p1=q1+1
      p2=ira2(psym)
      do 200 p=p1,p2
      px=invx(ix0a+p)+m0
      pmask=fcimsk(px)
      do 100 q=q1,p-1
      qx=invx(ix0a+q)+m0
      qmask=fcimsk(qx)
c
c  check fields in the masks for non-zero coincidences.
c
      do 60 field=1,8
      power=field-1
      pfield=mod(pmask/10**power,10)
      qfield=mod(qmask/10**power,10)
      if((pfield.ne.0) .and. (qfield.ne.0))go to 100
60    continue
c
c  ...exit from loop means p-q rotation is allowed.
c
      naar=naar+1
      iwop(1,naar)=px
      iwop(2,naar)=qx
      pq=nndx(p)+q
      iorder(pq)=naar
c
100   continue
200   continue
300   continue
c
      write(nlist,6010)naar,naarp,(j,(iwop(i,j),i=1,2),j=1,naar)
6010  format(/' faar:',i4,
     +  ' active-active rotations allowed out of:',i4,' possible.'/
     +  (1x,5(i4,':(',i3,',',i3,')')))
      return
      end
c
c****************************************************************
c
      subroutine hmcit(
     & nmaxv,     ncsf,     hr,     vr,
     & e,         ex,       v,      hv,
     & hvec,      emc,      n,      qinc,
     & hmc,       hdiag,    ftbuf,  lenbft,
     & modrt,     uaa,      h2,     off1,
     & addaa,     xp,       z,      xbar,
     & rev,       ind,      qind,   ist)
c
c  iteratively solve for ncol(ist) roots of the hmc(*) matrix.
c  the iterative method used is a variation of the simultanous
c  expansion method of b. liu as described in "nrcc report.
c  numberical algorithms in chemistry: algegraic methods" 49 (1978).
c  the vector and energy corresponding to nstate are returned
c  in hvec(*) and emc.
c  in this version, there is no i/o performed on the trial vectors
c  or matrix-vector products.
c
c  written by ron shepard.
c  modified by: michal dallos
c  version date: 16-jan-2001
c
c  arguments:
c  nmaxv     = maximum subspace dimension.
c  ncsf      = wave function expansion length.
c  hr(*)     = subspace hamiltonian matrix.
c  vr(*)     = subspace eigenvectors.
c  e(*)      = subspace eigenvalues.
c  ex(*)     = residual norms.
c  v(*)      = expansion vector space.
c  hv(*)     = matrix-vector product space.
c  hvec(*)  = vector corresponding to nstate.
c  emc       = energy of nstate.
c  qinc      = .true. for in-core hmc(*).
c  hmc(*)    = hmc matrix.
c  hdiag(*)  = diagonal hmc(*) elements.
c  ftbuf(*)  = ft buffer.
c  lenbft    = ft buffer length.
c  modrt(*)  = drt array.
c  uaa(*)    = effective 1-e hamiltonian.
c  h2(*)     = 2-e integrals.
c  off1(*)   = 2-e addressing array.
c  addaa(*)  = 2-e addressing array.
c  xp(*)     = drt array.
c  z(*)      = drt array.
c  xbar(*)   = drt array.
c  rev(*)    = drt array.
c  ind(*)    = drt array.
c  qind      = if qind=true then ind(*) is required.
c
c****************************************************
c
       implicit none
c  ##  parameter & common block section
c
      real*8 zero,one
      parameter(zero=0d0,one=1d0)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist,nvfile
      equivalence (iunits(1),nlist)
      equivalence (iunits(8),nvfile)
c
      integer niter,nstate,ncol,ncoupl,noldv,noldhv,nunitv,nciitr,
     &        mxvadd,nvcimx,nmiter,nvrsmx
      common /inputc/niter,nstate(8),ncol(8),ncoupl,
     +  noldv(8),noldhv(8),nunitv,nciitr,mxvadd,nvcimx,nmiter,nvrsmx
c
      real*8 tol,rtolci
      common/toler/tol(12),rtolci(mxavst)
      real*8 dtol
      equivalence (tol(5),dtol)
c
c  ##  integer section
c
      integer addaa(*)
      integer ciiter
      integer ist,ind(*),i,ij,iter,ii,ij1,ij2
      integer j
      integer lenbft
      integer modrt(0:*)
      integer nmaxv,ncsf,nr,n,nncol,nv
      integer off1(*)
      integer rev(*)
      integer xp(*),xbar(*)
      integer z(*)
c
c  ##  real*8 section
c
      real*8 emc,e(nmaxv),ex(nmaxv)
      real*8 ftbuf(lenbft)
c  maximum dimensions: hr(nnmaxv),vr(nmaxv,nmaxv),e(nmaxv),ex(nmaxv)
      real*8 hmc(*),hdiag(ncsf),hvec(ncsf),hv(ncsf,nmaxv),h2(*),
     &       hr(nmaxv*nmaxv)
      real*8 sovl(nmaxv*(nmaxv+1)/2),scrt(nmaxv*nmaxv),scrc(nmaxv*nmaxv)
      real*8 term
      real*8 uaa(*)
      real*8 v(ncsf,nmaxv),vr(*)
c
c  ##  logical section
c
      logical qinc,qind
      logical qtran
c
c  ##  external section
c
      real*8 ddot_wr,dnrm2_wr
      external ddot_wr,dnrm2_wr
c
      integer nndxf
      nndxf(i)=(i*(i-1))/2
c
c------------------------------------------------------------------
c
      call wzero(nmaxv,e,1)
      call wzero(nmaxv,ex,1)

c
c  construct the subspace matrix elements for the input vectors and
c  matrix-vector products.
c
      ij=0
      do  i=1,noldv(ist)
      do  j=1,i
      ij=ij+1
      sovl(ij)=ddot_wr(ncsf,v(1,i),1,v(1,j),1)
      enddo
      enddo


      ij=0
      do 110 i=1,noldhv(ist)
      do 110 j=1,i
      ij=ij+1
110   hr(ij)=ddot_wr(ncsf,v(1,i),1,hv(1,i),1)
c
c  start the iterative eigenvector solution.
c
      qtran=.true.
      ciiter=0
      do 500 iter=1,nciitr
      ciiter=iter
c
c  form matrix-vector products for any new trial vectors.
c
      nv=noldv(ist)-noldhv(ist)
      if(nv.gt.0)call hmcxv(qinc,nv,ncsf,hmc,hdiag,
     +  v(1,noldhv(ist)+1),hv(1,noldhv(ist)+1),
     +  ftbuf,lenbft,modrt,uaa,h2,off1,addaa,
     +  xp,z,xbar,rev,ind,qind,ist)
c
c  form the new subspace matrix elements.
c
      ij=nndxf(noldhv(ist)+1)
      do 210 i=noldhv(ist)+1,noldv(ist)
      do 210 j=1,i
      ij=ij+1
      sovl(ij)=ddot_wr(ncsf,v(1,i),1,v(1,j),1)
210   hr(ij)=ddot_wr(ncsf,v(1,i),1,hv(1,j),1)
c
      noldhv(ist)=noldv(ist)
c
      if(flags(7)) then 
         call plblkt(' hr(*) matrix',hr,noldhv,' row',1,nlist)
         call plblkt(' sovl(*) matrix',sovl,noldv,' row',1,nlist)
      endif 
c
c  solve the secular equation in the subspace.
c  eispac routines only require the lower-triangle.
c  vectors are in vr(*), eigenvalues are in e(*), and ex(*) is scratch.
c

      call eigno(noldhv(ist),hr,sovl,scrt,scrc,vr,e,nlist)
c
      if(flags(7))then
      write(nlist,6020)(e(i),i=1,noldhv(ist))
      call prblkt('hr(*) matrix eigenvectors',
     +  vr,noldhv(ist),noldhv(ist),noldhv(ist),' row',' col',1,nlist)
      endif
c
      qtran=.false.
      if(noldhv(ist).eq.nmaxv)then
      qtran=.true.
c
c  reduce the dimension of the expansion space using the
c  latest expansion coefficients.
c
      nncol=nndxf(ncol(ist)+1)
      call wzero(nncol,hr,1)
      ii=0
      do 310 i=1,ncol(ist)
      ii=ii+i
      sovl(ii)=one
310   hr(ii)=e(i)
c
c  use ex(*) for scratch and transform the vectors and
c  the matrix-vector products.
c
      call maaxb( v,1,ncsf, vr,1,noldhv(ist), ex,1, ncsf,noldhv(ist),
     & ncol(ist))
      call maaxb(hv,1,ncsf, vr,1,noldhv(ist), ex,1, ncsf,noldhv(ist),
     & ncol(ist))
c
c  update the expansion coefficients.
c
      call wzero(ncol(ist)*ncol(ist),vr,1)
      ii=1
      do 320 i=1,ncol(ist)
      vr(ii)=one
320   ii=ii+ncol(ist)+1
c
      noldhv(ist)=ncol(ist)
      noldv(ist)=ncol(ist)
c
      endif
c
c  expand the subspace.  the subspace is increased by ncol(ist)
c  vectors at a time where ncol(ist) is the number of roots that
c  are required to be converged.  converged roots are not
c  used for expansion, and the number of expansion vectors may
c  be reduced by space requirements and by the variable mxvadd.
c
      call wzero(nmaxv,ex,1)
      nr=min(noldhv(ist),ncol(ist))
      do 440 i=1,nr
c
c  form the residual vector for the i'th root.
c        r(i) = (h -e(i))*v(i)
c  store r(*,i) in hvec(*) and its norm in ex(i).
c
      call wzero(ncsf,hvec,1)
      do 410 j=1,noldhv(ist)
      term=vr((i-1)*noldhv(ist)+j)
      if(term.ne.zero)then
         call daxpy_wr(ncsf,term,hv(1,j),1,hvec,1)
         call daxpy_wr(ncsf,-e(i)*term,v(1,j),1,hvec,1)
      endif
410   continue
      ex(i)=dnrm2_wr(ncsf,hvec,1)
      if(ex(i).le.rtolci(i))go to 440
c
c  construct a new trial vector for the i'th root.
c
      do 420 j=1,ncsf
      term=hdiag(j)-e(i)
      if(abs(term).lt.dtol)term=dtol
      hvec(j)=hvec(j)/term
420   continue
c
c  orthonormalize the new expansion vector.
c
      do 430 j=1,noldv(ist)
      term=-ddot_wr(ncsf,hvec,1,v(1,j),1)
      if(term.ne.zero)call daxpy_wr(ncsf,term,v(1,j),1,hvec,1)
430   continue
c
      term=dnrm2_wr(ncsf,hvec,1)
      if(term.eq.zero)go to 440
c
      term=one/term
      call dscal_wr(ncsf,term,hvec,1)
      noldv(ist)=noldv(ist)+1
      call dcopy_wr(ncsf,hvec,1,v(1,noldv(ist)),1)
c
c  try for another expansion vector?
c
      if(noldv(ist)-noldhv(ist).ge.mxvadd .or. noldv(ist).eq.nmaxv)
     & go to 441
440   continue
441   continue
c
c  end of the ci iteration.
c
      if(flags(7))then
         write(nlist,6030)ciiter,noldhv(ist),noldv(ist)
         write(nlist,6040)(ex(i),i=1,nr)
      endif
c
      if(noldhv(ist).eq.noldv(ist))go to 510
500   continue
c
c  ...exit from loop means convergence is not reached.
c
      write(nlist,*)'*** hmc(*) convergence not reached ***'
c
510   continue
c
      write(nlist,6030)ciiter,noldhv(ist),noldv(ist)
c
c  transform and save trial vectors for the ncol(ist) roots.
c  prepare for the next mcscf iteration.
c
      n=min(ncol(ist),noldhv(ist))
      if(.not.qtran)
     +  call maaxb(v,1,ncsf, vr,1,noldhv(ist), hr,1, ncsf,noldhv(ist),n)
c

      do 520 i=1,n
520   call seqw(nvfile,ncsf,v(1,i))
      rewind nvfile
      noldhv(ist)=0
      noldv(ist)=n
c
c  return the vector and energy corresponding to nstate.
c  energies and residual norms are also returned in e(*) and ex(*).
c
      n=min(nstate(ist)+1,noldv(ist))
      call dcopy_wr(ncsf,v(1,n),1,hvec,1)
      emc=e(n)
c
      return
6020  format(' subspace eigenvalues e(*)='/(1x,10f13.6))
6030  format(' ciiter=',i4,' noldhv=',i3,' noldv=',i3)
6040  format(' rnorm(*)='/(1x,10f13.6))
      end
c
c****************************************************************
c
      subroutine hmcvec(
     & core,    avchd,    ncsf,     repnuc,
     & ecore0,  lenbft,   lastb,    lenbuk,
     & nipbuk,  modrt,    uaa,      add,
     & xbar,    xp,
     & z,       rev,
     & ind,     wnorm,    emc,      hvec,
     & ist)
c
c  solve for the appropriate eigenvector of the hamiltonian matrix
c  in the csf basis of the mc wave function.
c
c  arguments:
c  core(*)  = workspace.
c  avchd    = amount of core(*) available.
c  ncsf     = length of wave function expansion.
c  repnuc   = nuclear repulstion + frozen core energy.
c  ecore0   = inactive orbital core energy.
c  lenbft   = length of ft buffers.
c  lastb(*) = integral da file pointers.
c  lenbuk   = da record length.
c  nipbuk   = number of integrals per bucket.
c  modrt(*) = level to orbital mapping.
c  uaa(*)   = active-orbital effective one-electron hamiltonian.
c  add(*)   = addressing arrays.
c  xp(*)    = drt array.
c  z(*)     = drt array.
c  xbar(*)  = drt array.
c  rev(*)   = drt array.
c  ind(*)   = drt array.
c  qind     = drt indexing flag.
c  wnorm    = w(*) vector norm of the previous mcscf iteration.
c  emc      = mc wave function energy.
c  hvec     = mc wave function expansion coefficients.
c  navst(ist) = number of states to average over in the ist-th DRT
c  wavst(ist,i) = weight of the i-th states  and  ist-th DRT
c  heig(*)  = eigenvalues (energies) of averaged states
c
c  this routine controls the three different diagonalization modes:
c  (1) the hmc(*) matrix is computed, held in-core, and diagonalized
c      using a direct diagonalization method.
c  (2) the hmc(*) matrix is computed, held in-core, and diagonalized
c      using an iterative diagonalization method.
c  (3) the hmc-vector products are computed from the integrals and
c      formula tapes and iteratively diagonalized.
c  the parameters that determine which mode is chosen are:
c    flags(11)=t hmc(*) matrix is explicitly constructed.
c    flags(12)=t hmc(*) matrix is diagonalized iteratively.
c
c  note that the flags(11)=f .and. flags(12)=f combination is not
c  legal since the matrix cannot be directly diagonalized if it is
c  not computed.  in this case, flags(12)=t is assumed.
c
c  written by ron shepard.
c  modified by: michal dallos
c  version date: 16-jan-2001
c
       implicit none
c  ##  parameter & common block section
c
      real*8 two,eight
      parameter(two=2d0,eight=8d0)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist,nvfile
      equivalence (iunits(1),nlist)
      equivalence (iunits(8),nvfile)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer hblkw,nhbw,htape
      common/chbw/hblkw(15*maxstat),nhbw,htape
c
      integer addpt,numint,bukpt,intoff,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      integer niter,nstate,ncol,ncoupl,noldv,noldhv,nunitv,nciitr,
     &        mxvadd,nvcimx,nmiter,nvrsmx
      common /inputc/niter,nstate(8),ncol(8),ncoupl,
     +  noldv(8),noldhv(8),nunitv,nciitr,mxvadd,nvcimx,nmiter,nvrsmx
c
      real*8 tol,rtolci
      common/toler/tol(12),rtolci(mxavst)
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nact
      equivalence (nxtot(10),nact)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst),
     & heig(maxnst,mxavst), navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c  ##  integer section
c
      integer add(*),avchd,avcit
      integer cpt(11)
      integer event1,event2
      integer ind(*),ist,i,ipt,ixx,i1
      integer j1i,j
      integer lastb(*),lenbuk,lenx,lenbft
      integer modrt(*)
      integer ncsf,nipbuk,nv1,nv2,nmaxv,nv,nncsf,n
      integer rev(*),req
      integer shmc
      integer xp(*),xbar(*)
      integer z(*)
c
c  ## real*8 section
c
      real*8 core(avchd)
      real*8 ecore0,emc,emc1,etotal,eelec
      real*8 hvec(ncsf,*)
      real*8 repnuc,rnorm
      real*8 uaa(*)
      real*8 wnorm
c
c  ## logical section
c
      logical qind
      character*1 cflg
c
c  ##  character section
c
      character*60 fname
c
c  ##  external section
c
      integer  forbyt
      external forbyt
c
      integer nndxf
      nndxf(i)=(i*(i-1))/2
c
c-----------------------------------------------------------------
c
      if(flags(3))call timer(' ',1,event1,nlist)
c
      if (ist.ge.10) then
       write(fname,'(a6,i2)')'mcvfl.',ist
      else
       write(fname,'(a6,i1)')'mcvfl.',ist
      endif
      open(unit=nvfile,file=fname,status='unknown',
     &  form='unformatted')
c
      nncsf=(ncsf*(ncsf+1))/2
c
c  initialize /chbw/ and htape in case ist = 1.
c
      if (ist.eq.1) then
c
        nhbw=0
        do 10 i=1,25
10      hblkw(i)=0
        rewind htape
c
c     # ind(*) is referenced only if necessary.
c
        qind = nwalk_f(ist) .ne. ncsf_f(ist)
c
      endif
c
c  allocate core for hmc(*) and read in h2(*).
c  1:hmc,2:h2,3:buk,4:ibuk
c
      shmc=0
      if(flags(11))shmc=nncsf
      cpt(1)=1
      cpt(2)=cpt(1)+shmc
      if (nact.eq.0) then
      cpt(3)=cpt(2)+numint(11)
      else
      cpt(3)=cpt(2)+numint(1)
      endif
      cpt(4)=cpt(3)+lenbuk
      cpt(5)=cpt(4)+forbyt(nipbuk+1)
      if(cpt(5)-1 .gt. avchd)then
         write(nlist,6010)1,avchd,(cpt(i),i=1,5)
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    cpt(5),avchd
         call bummer('hmcvec: 1 avchd=',avchd,faterr)
      endif
      call wzero(numint(1),core(cpt(2)),1)
cmd   call rdbks(lastb(bukpt(1)),
cmd  +  core(cpt(2)),core(cpt(3)),core(cpt(4)),lenbuk,nipbuk)
      if (nact.eq.0) then
      call rdbks(lastb(bukpt(11)),
     +  core(cpt(2)),core(cpt(3)),core(cpt(4)),lenbuk,nipbuk,'hmcvec')
      else
      call rdbks(lastb(bukpt(1)),
     +  core(cpt(2)),core(cpt(3)),core(cpt(4)),lenbuk,nipbuk,'hmcvec')
      endif
cmd
c
c  alloca te core for hmc(*) and hdiag(*) construction.
c  1:hmc,2:h2,3:hdiag,4:ftbuf
c
      cpt(4)=cpt(3)+ncsf
      cpt(5)=cpt(4)+lenbft
      if(cpt(5)-1 .gt. avchd)then
         write(nlist,6010)2,avchd,(cpt(i),i=1,5)
         write(nlist,6010)1,avchd,(cpt(i),i=1,5)
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    cpt(5),avchd
         call bummer('hmcvec: 2 avchd=',avchd,faterr)
      endif
      call hmcft(flags(11),repnuc,ecore0,core(cpt(4)),
     +  lenbft,modrt,uaa,core(cpt(2)),
     +  add(addpt(10)),add(addpt(4)),ncsf,
     +  xp,z,xbar,rev,ind,
     +  core(cpt(3)),core(cpt(1)),qind,ist)
c
c  the csf-csf hessian block will be computed from the hmc(*) matrix.
c      m(ij) = 2*(h(ij)-emc*delta(ij))
c
      if(flags(11))call wrthb(nncsf,core(cpt(1)),15)
c
      if(flags(11).and.flags(5))call plblkt('hmc(*) matrix',
     +  core(cpt(1)),ncsf,'csf ',1,nlist)
c
c  solve for the appropriate eigenvector of the hmc(*) matrix.
c
      if(flags(11).and.(.not.flags(12)))then
c
c  diagonalize hmc(*).
c  1:hmc,2:eval,3:evec,4:scr
c
      cpt(3)=cpt(2)+ncol(ist)
      cpt(4)=cpt(3)+ncol(ist)*ncsf
      cpt(5)=cpt(4)+5*ncsf
      if(cpt(5)-1 .gt. avchd)then
         write(nlist,6010)3,avchd,(cpt(i),i=1,5)
         write(nlist,6010)1,avchd,(cpt(i),i=1,5)
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    cpt(5),avchd
         call bummer('hmcvec: 3 avchd=',avchd,faterr)
      endif
c
      if(flags(3))call timer(' ',1,event2,nlist)
      call givens(ncsf,ncol(ist),ncsf,core(cpt(1)),
     +  core(cpt(4)),core(cpt(2)),core(cpt(3)),ncol(ist))
      if(flags(3))call timer('hmc(*) diagonalization',4,event2,nlist)
c
c    #  save the energies and vectors of the states to be averaged
c    #  over.
c
      ipt=cpt(3)+nstate(ist)*ncsf
      do 200 ixx=1,navst(ist)
      heig(ist,ixx) = core(cpt(2)+nstate(ist)-1+ixx)
      emc=emc+wavst(ist,ixx)*heig(ist,ixx)
      call dcopy_wr(ncsf,core(ipt),1,hvec(1,ixx),1)
200   ipt=ipt+ncsf
c
c  print energies and vectors.
c
      write(nlist,6040)
      do 20 i=1,ncol(ist)
      etotal=core(cpt(2)-1+i)
      eelec=etotal-repnuc
      cflg=' '
      if(i.eq.nstate(ist)+1)cflg='*'
      write(nlist,6050)i,cflg,etotal,eelec
20    continue
      if(.not.flags(10))call prblkt('hmc(*) vectors',
     +  core(cpt(3)),ncsf,ncsf,ncol(ist),'csf ','vec ',1,nlist)
c
c  save vectors on file.
c
      rewind nvfile
      j1i=cpt(3)
      do 30 i=1,ncol(ist)
      call seqw(nvfile,ncsf,core(j1i))
30    j1i=j1i+ncsf
      rewind nvfile
      noldhv(ist)=0
      noldv(ist)=ncol(ist)
c
      else
c
c  iterative diagonalization...
c  1:hmc,2:h2,3:hdiag,4:ftbuf,5:v,6:hv,7:hr,8:vr,9:e,10:ex
c
c  determine the number of expansion vectors to use in the iterative
c  diagonalization.  the iterative method used is most effective if
c  more then 2*ncol(ist) vectors are available.  the maximum number is
c  problem-dependent and depends on the relative effort required for
c  the matrix-vector product construction and the reduced matrix and
c  vector manipulations.
c
      lenx=lenbft
      if(flags(11))lenx=0
      cpt(5)=cpt(4)+lenx
      avcit=avchd-cpt(5)+1
cmd   nv1=ncol(ist)+1
      nv1=min(ncol(ist)+1,ncsf)
      nv2=min(ncsf,nvcimx)
      nmaxv=nv1
      do 110 nv=nv1,nv2
      req=2*ncsf*nv+nndxf(nv+1)+nv*nv+2*nv
      if(req.gt.avcit)go to 111
      nmaxv=nv
110   continue
111   continue
c
c  allocate space for diagonalization.
c  1:hmc,2:h2,3:hdiag,4:ftbuf,5:v,6:hv,7:hr,8:vr,9:e,10:ex
c
      cpt(6)=cpt(5)+nmaxv*ncsf
      cpt(7)=cpt(6)+nmaxv*ncsf
      cpt(8)=cpt(7)+nndxf(nmaxv+1)
      cpt(9)=cpt(8)+nmaxv*nmaxv
      cpt(10)=cpt(9)+nmaxv
      cpt(11)=cpt(10)+nmaxv
      if(cpt(11)-1 .gt. avchd)then
         write(nlist,6010)5,avchd,(cpt(i),i=1,11)
         write(nlist,6010)1,avchd,(cpt(i),i=1,5)
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &     cpt(11),avchd
         call bummer('hmcvec: 5 avchd=',avchd,faterr)
      endif
c
c  get initial trial vectors.
c  hv(*) is used for scratch space.
c
      call inciv(ncsf,nmaxv,core(cpt(3)),core(cpt(5)),core(cpt(6)),ist)
c
c  adjust convergence tolerance for the vector corresponding to nstate
c  (other states are not changed).  set to wnorm**4/8 and clip to
c  ensure that:
c    tol(6) .le. rtol .le. tol(7)
c  this choice assures that slightly more than the minimal accuracy
c  required for second-order convergence is returned in the hmc(*)
c  eigenvector.
c
      do i = 1,mxavst
         rtolci(i) = 1.0d-2
      enddo
      do i=1,navst(ist)
      if (wavst(ist,i).ne.0.0d+00)
     &  rtolci(nstate(ist)+i)=min(max(tol(6),wnorm**4/eight),tol(7))
      enddo
c
c  iteratively diagonalize hmc(*).
c
      if(flags(3))call timer(' ',1,event2,nlist)
      call hmcit(
     &  nmaxv,          ncsf,           core(cpt(7)),  core(cpt(8)),
     &  core(cpt(9)),   core(cpt(10)),  core(cpt(5)),  core(cpt(6)),
     &  hvec,           emc1,           n,             flags(11),
     &  core(cpt(1)),   core(cpt(3)),   core(cpt(4)),  lenbft,
     &  modrt,          uaa,            core(cpt(2)),  add(addpt(10)),
     &  add(addpt(4)),  xp,             z,             xbar,
     &  rev,            ind,            qind,          ist)
c
      if(flags(3))call timer('hmc(*) diagonalization',4,event2,nlist)
c
      ipt=cpt(5)+nstate(ist)*ncsf
      do  ixx=1,navst(ist)
      heig(ist,ixx) = core(cpt(9)+n-2+ixx)
      emc=emc+wavst(ist,ixx)*heig(ist,ixx)
      call dcopy_wr(ncsf,core(ipt),1,hvec(1,ixx),1)
      ipt=ipt+ncsf
      enddo
c
c  write out eigenvalues, vectors, and residual norms.
c
      ipt = cpt(3)
      write(iunits(20))(core(ipt+j-1),j=1,ncsf_f(ist))
c
      write(nlist,6060)
      do 120 i=1,ncol(ist)
      etotal=core(cpt(9)-1+i)
      eelec=etotal-repnuc
      rnorm=core(cpt(10)-1+i)
      cflg=' '
      if(i.eq.nstate(ist)+1)cflg='*'
      write(nlist,6050)i,cflg,etotal,eelec,rnorm,rtolci(i)
120   continue
c
      if (.not.(flags(10))) then
        do i=1,navst(ist)
        write(nlist,6070)(hvec(i1,i),i1=1,ncsf)
        enddo
      endif
c
      endif
c
      close(unit=nvfile)
c
      if(flags(3))call timer('hmcvec',4,event1,nlist)
c
      return
6010  format(/' *** error *** hmcvec: ie,avchd,cpt(*)=',i2,i10/
     +  (1x,10i10))
6040  format(/' Eigenvalues of the hmc(*) matrix'/t14,'total energy',
     +  t31,'electronic energy')
6050  format(1x,i4,a,4f20.10)
6060  format(/' Eigenvalues of the hmc(*) matrix'/t14,'total energy',
     +  t31,'electronic energy',t54,'residual norm',t77,'rtolci(*)')
6070  format(/' hmc(*) eigenvector:'/10(1x,10f10.6/))
      end
c
c*********************************************************************
c
      subroutine inorbc( c, h, s, t, ht, u, sx )
c
c  read initial orbital coefficients.
c  arguments:
c  c(*)  = orbital coefficients on output.
c  h(*)  = ao 1-e hamiltonian matrix.
c  s(*)  = ao overlap matrix.
c  t(*)  = scratch matrix (nbmt).
c  ht(*) = scratch matrix (nntm).
c  u(*)  = scratch matrix (n2tm).
c  sx(*) = scratch vector (nmot).
c
c  28-sep-91 rdmoc() added. -rls
c  written by ron shepard.
c
      implicit integer(a-z)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
      equivalence (iunits(7),mocoef)
      equivalence (iunits(12),nrestp)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nnsb(9)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,3),nnsb(1))
      integer nmpsy(8)
      equivalence (nxy(1,5),nmpsy(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
      equivalence (nxtot(16),nbmt)
c
      real*8 c(*), h(*), s(*), t(*), ht(*), u(*), sx(*)
c
      character*60 fname
c
      real*8 one
      parameter(one=1d0)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
cvp  --- moread
      integer nmxtitle
      parameter ( nmxtitle= 20 )
      integer filerr, syserr, ntit, nsize
      character*80 titles(nmxtitle), afmt
      character*4 labels(8)
c
      write(nlist,'(/'' Source of the initial MO coeficients:'')')
c
c****************************************************************
      if ( flags(13) ) then
c****************************************************************
c
c        # open the formatted mocoef file and read the coefficients.
c
         fname = 'mocoef'
         call trnfln( 1, fname )
         open(unit=mocoef,file=fname,status='old',
     +    access='sequential',form='formatted',iostat=ierr)
c
         if ( ierr .ne. 0 ) then
            write(nlist,6010) 'inorbc: fname= ' // fname
            call bummer('inorbc: from open, ierr=', ierr, faterr )
         endif
c
         inquire( unit=mocoef, name=fname )
         write(nlist,6010) 'Input MO coefficient file: ' // fname
c
cvp      call rdmoc(
c    &    nlist, mocoef, nsym, nbpsy,
c    &    nmpsy, c,      ierr )
c
c        if ( ierr .ne. 0 ) then
c           call bummer('inorbc: from rdmoc, ierr=', ierr, faterr )
c        endif
cvp
         nsize=0
         call moread(mocoef,10,filerr,syserr,nmxtitle,ntit,titles,afmt,
     &        nsym,nbpsy,nmpsy,labels,nsize,c)
         do i=1,8
           nsize=nsize+nbpsy(i)*nmpsy(i)
         enddo
         call moread(mocoef,20,filerr,syserr,nmxtitle,ntit,titles,afmt,
     &        nsym,nbpsy,nmpsy,labels,nsize,c)
cvp
         close(unit=mocoef)
c
c****************************************************************
      elseif ( flags(14) ) then
c****************************************************************
c
         write(nlist,6010) ' 1-e hamiltonian '
     &    // 'eigenvectors used as initial orbitals.'
c
c        # solve for the 1-e hamiltonian eigenvectors: hc=sce.
c
         do 230 isym=1,nsym
            nbi=nbpsy(isym)
            if(nbi.eq.0)go to 230
            nmi=nmpsy(isym)
            if(nmi.eq.0)go to 230
c
c           # find t(*) such that t(transpose)*s*t = 1.
c           # use sx(*) for scratch.
c
            call wzero(nbi*nmi,t,1)
            xx=1
            do 210 x=1,nmi
               t(xx)=one
               xx=xx+nbi+1
210         continue
c
            xy1=nnsb(isym)+1
            call orthos(nbi,nmi,t,s(xy1),sx)
c
c           # form t(transpose)*h*t.
c           # use sx(*) for scratch.
c
            call uthu(t,h(xy1),ht,sx,nbi,nmi)
c
c           # copy to u(*) and diagonalize.
c           # only the lower triangle is required in the eispac call.
c           # use ht(*) and sx(*) for scratch.
c
            xy1=0
            do 222 x=1,nmi
               do 220 y=1,x
                  xy1=xy1+1
                  xy2=(y-1)*nmi+x
                  u(xy2)=ht(xy1)
220            continue
222         continue
c
            call eigip(nmi,nmi,u,ht,sx)
c
c           # form c = t * u
c
            b1x1=nsbm(isym)+1
            call mxma(t,1,nbi, u,1,nmi, c(b1x1),1,nbi, nbi,nmi,nmi)
c
230      continue
c
c****************************************************************
      elseif ( flags(15) ) then
c****************************************************************
c
         inquire( unit=nrestp, name=fname )
         write(nlist,6010) 'Input MO coefficient file: ' // fname
         write(nlist,5000)
5000   format(
     & ' The resolved MO coefficients (MORB) form the last complete
     &  MCSCF iteration are read in.')
c
c        # read 'morb' coefficients from the restart file.
c
         nbmtx = 0
         call rdlv(nrestp,nbmt,nbmtx,c,'morb',.true.)
         if(nbmtx.ne.nbmt)call bummer('inorbc: nbmtx=',nbmtx,faterr)
c
c****************************************************************
      elseif ( flags(16) ) then
c****************************************************************
c
         inquire( unit=nrestp, name=fname )
         write(nlist,6010) 'Input MO coefficient file: ' // fname
         write(nlist,7000)
 7000    format
     &   ('The unresolved MO coefficients (MORBN) form the last
     &     MCSCF iteration are read in.')
c
c        # read 'morbn' coefficients from the restart file.
c
         call rdlv(nrestp,nbmt,nbmtx,c,'morbn',.true.)
         if(nbmtx.ne.nbmt)call bummer('inorbc: nbmtx=',nbmtx,faterr)
c
c****************************************************************
      elseif ( flags(29) ) then
c****************************************************************
c
c        # open the formatted mocoef file and read the coefficients.
c
         fname = 'mocoef_lumorb'
         call trnfln( 1, fname )
         open(unit=mocoef,file=fname,status='old',
     +    access='sequential',form='formatted',iostat=ierr)
c
         if ( ierr .ne. 0 ) then
            write(nlist,6010) 'inorbc: fname= ' // fname
            call bummer('inorbc: from open, ierr=', ierr, faterr )
         endif
c
         inquire( unit=mocoef, name=fname )
         write(nlist,6010) 'Input MO coefficient file: (LUMORB)'
     .     // fname
c
         call rdmof_molcas(mocoef,nsym, nbpsy, nbpsy, c,'mos')
         close(unit=mocoef)
c****************************************************************
      else
c****************************************************************
c
         write(nlist,6010)
     +    ' Unit matrix used as initial orbitals.'
c
c        # initialize to a unit matrix.
c
         call wzero(nbmt,c,1)
         do 330 isym=1,nsym
            nbi=nbpsy(isym)
            if(nbi.eq.0)go to 330
            nmi=nmpsy(isym)
            if(nmi.eq.0)go to 330
c
            bx=nsbm(isym)+1
            do 310 x=1,nmi
               c(bx)=one
               bx=bx+nbi+1
310         continue
330      continue
c
      endif
c
      if(flags(4))call prblks('initial orbital coefficients',
     + c,nsym,nbpsy,nmpsy,' bfn','  mo',1,nlist)
c
      return
6010  format(/1x,a)
      end
c
c***********************************************************
c
      subroutine inciv(ncsf,nmaxv,hdiag,v,is,ist)
c
c  initialize trial vectors for the iterative hmc(*) diagonalization.
c
c  arguments:
c  ncsf     = expansion length.
c  nmaxv    = maximum number of expansion vectors.
c  hdiag(*) = diagonal hmc(*) elements.
c  v(*)     = expansion vectors.
c  is(*)    = integer scratch vector.
c
c  written by ron shepard.
c  version date: 02-apr-85
c
       implicit none
c  ##  parameter & common block section
c
      integer niter,nstate,ncol,ncoupl,noldv,noldhv,nunitv,
     &        nciitr,mxvadd,nvcimx,nmiter,nvrsmx
      common /inputc/niter,nstate(8),ncol(8),ncoupl,
     +  noldv(8),noldhv(8),nunitv,nciitr,mxvadd,nvcimx,nmiter,nvrsmx
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist,nvfile
      equivalence (iunits(1),nlist)
      equivalence (iunits(8),nvfile)
c
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      real*8 one
      parameter(one=1.0d00)
c
c  ##  integer section
c
      integer ist,i,ilow,is(*)
      integer j
      integer nv,ncsf,nmaxv
c
c  ##  real*8 section
c
      real*8 dlow
      real*8 hdiag(ncsf)
      real*8 v(ncsf,nmaxv)
c
c------------------------------------------------------------------
c
      if(noldv(ist).le.0)then
c
c  initialize nunitv unit vectors using the lowest diagonal
c  elements to determine the most appropriate choices.
c
      do 10 i=1,ncsf
10    is(i)=i
c
      nv=max(1,min(nunitv,nmaxv))
      do 30 i=1,nv
         ilow=i
         dlow=hdiag(is(i))
         do 20 j=i+1,ncsf
            if(hdiag(is(j)).lt.dlow)then
               ilow=j
               dlow=hdiag(is(j))
            endif
20       continue
         j=is(ilow)
         is(ilow)=is(i)
         is(i)=j
30    continue
c
c  is(*) has the indices of the lowest nv diagonal elements in
c  the first nv locations.  generate the corresponding vectors.
c
      write(nlist,6030)
6030  format(/' trial vectors are generated internally.'/)
      do 40 i=1,nv
      call wzero(ncsf,v(1,i),1)
      v(is(i),i)=one
      write(nlist,6010)i,is(i)
6010  format(' trial vector',i3,' is unit matrix column',i6)
40    continue
c
      else
c
c  read trial vectors from nvfile(ist) and orthonormalize.
c
      rewind nvfile
      nv=min(nmaxv,noldv(ist))
      do 60 i=1,nv
60    call seqr(nvfile,ncsf,v(1,i))
      write(nlist,6020)nv,nvfile
6020  format(/1x,i3,' trial vectors read from nvfile (unit=',i3,').')
c
      call orthot(ncsf,nv,v,1)
c
      endif
c
      noldv(ist)=nv
      noldhv(ist)=0
c
      return
      end
c
c****************************************************************
c
      subroutine korder(k,km,iorder,naar)
c
c  expand the vector k(*) into the anti-symmetric matrix km(*,*).
c
c  arguments:
c  k(*)      = input vector.
c  km(*)     = symmetry packed output matrix.
c  iorder(*) = active-active mapping vector.
c  naar      = number of allowed active-active rotations.
c
c  generic functions used: max, sign
c
c  written by ron shepard.
c
       implicit none
c  ##  parameter & common block section
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist
      equivalence (iunits(1),nlist)
c
      real*8 tol,rtolci
      common/toler/tol(12),rtolci(mxavst)
      real*8 kmax
      equivalence (tol(8),kmax)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),n2sm(8),n2tm
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(6),n2tm)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
      integer tsymad(8),tsymvd(8),tsymva(8)
      integer nadt,nvdt,nvat
      equivalence (nxy(1,23),tsymad(1),nadt)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxy(1,26),tsymva(1),nvat)
c
      integer nmotx
         parameter (nmotx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),invx(nmotx),ix0d,ix0a,ix0v
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a),(ix0(3),ix0v)
c
c  ##  integer section
c
      integer ai,a1,a2,a,aq,ap
      integer iorder(*),iad,iaa,ivd,iva,i,ip,isym,i1,i2,ipq
      integer mp,mi,mip,mq,mqp,mpq,ma,mia,mai,map,mpa,mpi
      integer nclip,nk,naar,nm
      integer p1,p2,p
      integer q
c
c  ##  real*8 section
c
      real*8 k(*),km(*)
c
c--------------------------------------------------------------
c
c  set up pointers to various sections of k(*).
c
      iad=0
      iaa=iad+nadt
      ivd=iaa+naar
      iva=ivd+nvdt
      nk=ivd+nvat
c
c  clip k(*) entries by kmax.
c
      nclip=0
      do 10 i=1,nk
      if(abs(k(i)).gt.kmax)then
         nclip=nclip+1
         k(i)=sign(kmax,k(i))
      endif
10    continue
c
      if(nclip.ne.0)then
         write(nlist,6010)nclip,kmax
      endif
c
      call wzero(n2tm,km,1)
c
      if(nadt.ne.0)then
c
c  unpack the active-inactive section.
c
      ip=iad
      do 120 isym=1,nsym
      if(ndpsy(isym).eq.0)go to 120
      if(napsy(isym).eq.0)go to 120
      nm=nmpsy(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      p1=ira1(isym)
      p2=ira2(isym)
c
      do 110 p=p1,p2
      mp=invx(ix0a+p)
cdir$ ivdep
cvocl loop,novrec
      do 110 i=i1,i2
      ip=ip+1
      mi=invx(ix0d+i)
      mip=n2sm(isym)+(mp-1)*nm+mi
      mpi=n2sm(isym)+(mi-1)*nm+mp
      km(mip)=-k(ip)
      km(mpi)=+k(ip)
110   continue
c
120   continue
c
      endif
c
      if(naar.ne.0)then
c
c  unpack the active-active section.
c
      do 230 isym=1,nsym
      if(napsy(isym).lt.2)go to 230
      nm=nmpsy(isym)
      p1=ira1(isym)
      p2=ira2(isym)
c
      do 220 p=p1,p2
      mp=invx(ix0a+p)
      do 210 q=p1,p
      ipq=iorder(nndx(p)+q)
      if(ipq.eq.0)go to 210
      mq=invx(ix0a+q)
      mqp=n2sm(isym)+(mp-1)*nm+mq
      mpq=n2sm(isym)+(mq-1)*nm+mp
      km(mqp)=-k(ipq+iaa)
      km(mpq)=+k(ipq+iaa)
210   continue
220   continue
c
230   continue
c
      endif
c
      if(nvdt.ne.0)then
c
c  unpack the virtual-inactive section.
c
      ai=ivd
      do 320 isym=1,nsym
      if(ndpsy(isym).eq.0)go to 320
      if(nvpsy(isym).eq.0)go to 320
      nm=nmpsy(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      a1=irv1(isym)
      a2=irv2(isym)
c
      do 310 i=i1,i2
      mi=invx(ix0d+i)
cdir$ ivdep
cvocl loop,novrec
      do 310 a=a1,a2
      ai=ai+1
      ma=invx(ix0v+a)
      mia=n2sm(isym)+(ma-1)*nm+mi
      mai=n2sm(isym)+(mi-1)*nm+ma
      km(mia)=+k(ai)
      km(mai)=-k(ai)
310   continue
c
320   continue
c
      endif
c
      if(nvat.ne.0)then
c
c  unpack the virtual-active section.
c
      ap=iva
      do 420 isym=1,nsym
      if(napsy(isym).eq.0)go to 420
      if(nvpsy(isym).eq.0)go to 420
      nm=nmpsy(isym)
      a1=irv1(isym)
      a2=irv2(isym)
      p1=ira1(isym)
      p2=ira2(isym)
c
      do 410 p=p1,p2
      mp=invx(ix0a+p)
cdir$ ivdep
cvocl loop,novrec
      do 410 a=a1,a2
      ap=ap+1
      ma=invx(ix0v+a)
      map=n2sm(isym)+(mp-1)*nm+ma
      mpa=n2sm(isym)+(ma-1)*nm+mp
      km(map)=-k(ap)
      km(mpa)=+k(ap)
410   continue
c
420   continue
c
      endif
c
      return
6010  format(/' korder:',i8,
     +  ' k(*) entries are clipped with kmax=',1pe13.6)
      end
c
c******************************************************************
c
      subroutine micro(
     & nmiter,    rtol,    dtol,    eshsci,
     & b,         ib,      ib_m,    ib_c,
     & w,         hmcvec,  bdiag,   mdiag,
     & iorder,    addad,   addvd,   addva,
     & nadt,      naar,    nvdt,    nvat,
     & noldr,     nnewr,   nolds,   nnews,
     & ndimr,     nmaxr,   ndims,   nmaxs,
     & r,         bxr,     cxr,     s,
     & cxs,       mxs,     br,      cr,
     & mr,        wr,      zr,      pr,
     & scr,       kpvt,    k,       p,
     & apxde,     knorm,   qpcon,   qcoupl,
     & scr1,      scr2,    scr3,    f1,
     & f2,        f3,      cmo,     d1,
     & fva,       qva,     fad,     qad,
     & fdd,       qvv,     ndim,    mxcsao,
     & ftbuf,     lenbft,  modrt,   h2,
     & add,       xp,      z,       xbar,
     & rev,       ind,     uaa,     uad,
     & uvd,       uva,     nipbuk,  lastb,
     & lenbuk,    screst,  avcsk)
c
c
c  to control the micro-iterative solution of the psci equation
c  for the orbital correction vector.
c  input:
c  nmiter  = maximum number of iterations.
c  rtol    = tolerance on residual norms.
c  dtol    = small diagonal-element tolerance.
c  eshsci  = shift parameter for sci solution.
c  b       = hessian matrix blocks (b,c, and m).
c  ib      = pointers to matrix blocks (0:block is not in core).
c  w       = gradient vectors (ad,aa,vd,va).
c  hmcvec  = current hmc eigenvector.
c  bdiag   = diagonal elements of the complete b matrix.
c  mdiag   = diagonal elements of the m matrix.
c  nadt    = number of active-inactive rotations.
c  naar    = number of active-active rotations.
c  nvdt    = number of virtial-inactive rotations.
c  nvat    = number of virtual-active rotations.
c  navst*  = number of states to average over for different DRTs
c  ncfx    = either 0 or the number of csfs.
c  nold*   = number of old trial vectors (m-v products already exist).
c  nnew*   = number of new trial vectors (m-v products must be formed).
c  ndim*   = column length of expansion vectors r and s.
c  nmax*   = maximum number of expansion vectors r and s (.ge.2).
c  r,s     = expansion vector workspace.
c  bx,cxr,cxs,mxs = matrix-vector workspace.
c  br,cr,mr,wr,zr,pr = reduced matrix and vector workspace.
c  kpvt    = integer workspace for reduced psci equation solution.
c
c  output:
c  k       = orbital correction vector (kappa).
c  p       = csf correction vector (normalized to pt*p=1).
c  apxde   = approximant energy lowering for the next mcscf iteration.
c  knorm   = norm of the k(*) vector.
c  qpcon   = .true. if p(*) vector is returned.
c
c  this version employs an in-core method where the b(*) blocks,
c  c(*) blocks, and the m(*) matrix are held in memory.
c  it is presumed that nmaxr and nmaxs are both relatively
c  small so that the manipulations involving the subspace
c  matrices are insignificant compared to their construction effort.
c  the number of matrix-vector and vector-vector operations in the
c  full space are kept to a minimum while retaining subspaces of
c  maximal dimension to reduce the number of iterations.
c  c(*) blocks and m(*) are expressed in the csf basis instead
c  of the orthogonal-complement basis.  this requires projecting
c  the current hmcvec(*) components from the state-space vectors.
c  it is presumed that the nnews expansion vectors in s(*) on input
c  are orthonormal and orthogonal to hmcvec(*) and the nnewr expansion
c  vectors in r(*) are orthonormal.  note that nnewr and nnews cannot
c  both be zero on entry.
c
c  the iterative method used is based on that described in:
c    f.b. brown, i. shavitt, and r. shepard, chem. physics. letts. 105,
c    364 (1984)
c  it consists of constructing subspace representations of the input
c  matrices and vectors, b, c, m, and w, followed by the straight-
c  forward solution of the coupled linear-secular equation within the
c  subspace, followed by expansion of the subspace dimension until
c  the residuals in the full space are small.
c
c  the solution required is proportional to the eigenvector
c  corresponding to the lowest (most negative) eigenvalue of
c  the secular equation:
c
c  (b-c(m**-1)ct-l   w )  ( z) = (0)
c  (wt              d-l)  (z0)   (0)
c
c  where b-c(m**-1)ct is the partitioned orbital hessian matrix, w
c  is the gradient vector, l is the lowest energy eigenvalue, d is
c  an energy shift parameter to increase the radius of convergence,
c  and k=z*sign(z0) is the orbital correction vector.  the solution
c  of the above equation is equivalent to the solution of the following
c  unfolded equation:
c
c  (b-l  c         w)  ( z)   (0)
c  (ct      m      0)  ( p) = (0)
c  (wt      0    d-l)  (z0)   (0)
c
c  where the normalization, (z0*z0 + zt*z)=1, applies to both
c  equations.  in the second equation the normalization condition
c  (z0*z0 + pt*p + zt*z)=1 may also be used to damp the solution
c  in cases where there is strong coupling between the orbitals
c  and the csfs.  both methods result in second-order iterative
c  mcscf methods in some neighborhood of the solution.
c
c  the p vector, in the csf basis, is returned normalized to
c  pt*p=1 to be used in the iterative solution of the hmc eigenvector
c  equation since it contains the csf coefficient corrections due to
c  the orbital changes induced by k (correct through second order
c  in k).
c
c  *note* fortran-77 do-loop conventions are assumed.  all do-loops
c   should be zero-trip-count protected.

c
c  written by: ron shepard.
c  modified by: michal dallos
c  version date: 16-jan-2001
c
c    for the state averaging:during the whole algorithm, use ncfx*navst
c    instead of only ncfx. the only exception is the projection part...
c

       implicit none
c  ##  parameter & common section
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8 zero,half,one
      parameter(zero=0d0,half=5d-1,one=1d0)
c
      real*8 aones2
      parameter(aones2=.7071)
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer asprojection
      common/mprojection/ asprojection
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist, cfile
      equivalence (iunits(1),nlist)
      equivalence (iunits(23),cfile)
c
      integer cdir
      common/c_block/ cdir
c
      integer addpt, numint, bukpt, intoff, szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer msize,csize
      common/size/ msize(maxnst),csize(4,maxnst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nntm
      equivalence (nxtot(5),nntm)
c
c  ##  integer section
c
      integer addad(*), addvd(*), addva(*), add(*), avcsk
      integer event, event2, event3
      integer ib_m(maxstat), ib_c(4,maxstat), ib(15), iorder(*),
     &        i, i2, icount2, ist, info, iter, ij, icount, iad,
     &        imxov, iaa, iva, ivd, ij2, ij1, idamax_wr, ii2, ipt,
     &        ir, is, ii, iterx, ixx, ipos,imd2,imd
      integer j,jj,jjstart
      integer kpvt(*)
      integer lastb(*), lenbuk, lenbft
      integer modrt(0:*), mxcsao
      integer nipbuk, nndxf, ndim, nmaxs, nmaxr, ndims, ncfx, nns,
     &        nsci, nnewr, noldr, nolds, ndimr, nnews, nadt, nmiter,
     &        naar, nvat,  nvdt
c
c  ## real*8 section
c
c
c  effective dimensions br(nnmaxr),cr(nmaxr,nmaxs),mr(nnmaxs)
c                       wr(nmaxr),kpvt(nmaxs)
c                       scr( mxrs*(mxrs+1)/2 ),pr(mxrs1),zr(mxrs2)
c  mxrs  = max(nmaxr,nmaxs)
c  mxrs1 = max(nmaxr+1,nmaxs)
c  mxrs2 = mxrs1**2
c  nmaxs may be zero (if ncsf=1 or if c* matrix is not calculated).
c
      real*8 apxde
      real*8 b(*), bdiag(*), bxr(ndimr,*), br(*)
      real*8 cxr(ndims,*),cxs(ndimr,*), cr(nmaxr,*), cmo(*)
      real*8 dtol, d1(*), dumf
      real*8 eshsci, esci
      real*8 fva(*), fad(*), fdd(*), f1(*), f2(*), f3(*),
     &       ftbuf(lenbft)
      real*8 hmcvec(*), h2(*)
      real*8 ind(*)
      real*8 knorm, k(ndimr)
      real*8 mxs(ndims,*), mdiag(*), mr(*)
      real*8 p(ndims), pr(*), pnorm
      real*8 qva(*), qad(*), qvv(*)
      real*8 r(ndimr,*), rtol, rznorm, rpnorm, rev(*)
      real*8 s(ndims,*), scr(*), scr1(*), scr2(*), scr3(*),
     &       screst(*)
      real*8 term
      real*8 uaa(*),uad(*),uvd(*),uva(*)
      real*8 w(*), wr(nmaxr)
      real*8 xp(*), xbar(*)
      real*8 zr(*), z0, z(*)
c
c
c  ##  logical section
c
      logical qind, qpcon, qcoupl, qc(4)
      logical lcxs,lcxr
c
      real*8 ddot_wr,dnrm2_wr
      external ddot_wr, dnrm2_wr
c
      nndxf(i)=(i*(i-1))/2
c
c    # calculate the total number CSFs
      ncfx = 0
      if (qcoupl) then
         do ist = 1, nst
         ncfx = ncfx + navst(ist)*ncsf_f(ist)
         enddo
      endif
c
c    # calculate the information for C*v calculation (FT read)
      qc(1) = (ncfx*naar.ne.0)
      qc(2) = (ncfx*nadt.ne.0)
      qc(3) = (ncfx*nvdt.ne.0)
      qc(4) = (ncfx*nvat.ne.0)
c
      iter=0
      info=0
      imxov=0
      z0=zero
      esci=zero
      pnorm=zero
      rznorm=zero
      rpnorm=zero
c
c  set up pointers to the various components of the r* vectors.
c
      iad=1
      iaa=iad+nadt
      ivd=iaa+naar
      iva=ivd+nvdt
c
c  set up reduced matrices br,cr,mr,wr for any old vectors.
c  matrix-vector products have already been calculated for these
c  vectors and passed through the argument list.
c
      ij=0
      do 10 i=1,noldr
      do 10 j=1,i
      ij=ij+1
      br(ij)=ddot_wr(ndimr,r(1,i),1,bxr(1,j),1)
10    continue
c
      do 20 i=1,nolds
      do 20 j=1,noldr
      cr(j,i)=ddot_wr(ndimr,r(1,j),1,cxs(1,i),1)
20    continue
c
      ij=1
      do 30 i=1,nolds
      do 30 j=1,i
      ij=ij+1
      mr(ij)=ddot_wr(ncfx,s(1,i),1,mxs(1,j),1)
30    continue
c
      do 40 i=1,noldr
40    wr(i)=ddot_wr(ndimr,w,1,r(1,i),1)
c
c  begin iteration loop:
c
      if (flags(3)) call timer(' ',1, event,nlist)
      if (flags(3)) call timer(' ',5, event,nlist)
      if (flags(3)) call timer(' ',1, event2,nlist)
      if (flags(3)) call timer(' ',5, event2,nlist)
      if (flags(3)) call timer(' ',1, event3,nlist)
      if (flags(3)) call timer(' ',5, event3,nlist)
c
c
      do 600 iterx=1,nmiter
      iter=iterx
c
      if (flags(8)) then
      write(nlist,'(125(1h-),/,1x,''Starting the miter No.: '',i3)')
     & iter
      endif
c
c
c    # calculation of the B*r and C*s matrix-vector products
c    # vectors k(*) and p(*) are used for local scratch space.
c
      do 100 i=noldr+1,nnewr
c
      call wzero(ndimr,bxr(1,i),1)
      if(ncfx.ne.0)call wzero(ncfx,cxr(1,i),1)
c
       if (flags(8)) then
        write(nlist,'(/,3x,"Actual r vector:")')
        call prtvec(nlist,'r[ad]',r(iad,i),nadt)
        call prtvec(nlist,'r[aa]',r(iaa,i),naar)
        call prtvec(nlist,'r[vd]',r(ivd,i),nvdt)
        call prtvec(nlist,'r[va]',r(iva,i),nvat)
       endif ! (flags(8))
c
      if(flags(22)) then
*@ifdef direct
*        if (flags(3)) call timer(' ',6, event,nlist)
*        call fpseu(cmo,d1,r(ivd,i),r(iad,i),r(iva,i)
*     &    ,scr1,scr2,scr3,f1,f2,f3,ndim,mxcsao,screst,avcsk)
*        if (flags(3)) call timer(' ',5, event,nlist)
*@endif
      endif

      if(ib(1).ne.0)call madad(b(ib(1)),r(iad,i),bxr(iad,i),k,addad)
c
      if(ib(2).ne.0)then
         call mxma(b(ib(2)),1,nadt, r(iaa,i),1,1, k,1,1, nadt,naar,1)
         call daxpy_wr(nadt,one,k,1,bxr(iad,i),1)
         call mxma(b(ib(2)),nadt,1, r(iad,i),1,1, k,1,1, naar,nadt,1)
         call daxpy_wr(naar,one,k,1,bxr(iaa,i),1)
      endif
c
      if(ib(3).ne.0)then
         call spmxv(b(ib(3)),r(iaa,i),k,naar)
         call daxpy_wr(naar,one,k,1,bxr(iaa,i),1)
      endif
c
4000  if(ib(4).ne.0)then
          if (flags(22)) then
*@ifdef direct
*            if (flags(3)) call timer(' ',6, event,nlist)
*            call md1(fva,qva,r(ivd,i),bxr(iad,i))
*            call mvdad1(cmo,scr2,scr3,f1,d1,bxr(iad,i))
*            call md2(fva,qva,r(iad,i),bxr(ivd,i))
*            call mvdad2(scr2,scr3,f2,cmo,bxr(ivd,i))
*            if (flags(3)) call timer(' ',5, event,nlist)
*@endif
         else
            call mxma(b(ib(4)),1,nadt, r(ivd,i),1,1, k,1,1,
     &         nadt,nvdt,1)
            call daxpy_wr(nadt,one,k,1,bxr(iad,i),1)
            call mxma(b(ib(4)),nadt,1, r(iad,i),1,1, k,1,1,
     &         nvdt,nadt,1)
            call daxpy_wr(nvdt,one,k,1,bxr(ivd,i),1)
             endif
            if (flags(8)) then
            write(nlist,
     &       '(/,3x,4h####,2x,"Bxr for the B[vd,ad]:",2x,4h####)')
            call prtvec(nlist,'Bxr[ad]',bxr(iad,i),nadt)
            call prtvec(nlist,'Bxr[vd]',bxr(ivd,i),nvdt)
            endif
      endif
c
      if(ib(5).ne.0)then
         call mxma(b(ib(5)),1,nvdt, r(iaa,i),1,1, k,1,1, nvdt,naar,1)
         call daxpy_wr(nvdt,one,k,1,bxr(ivd,i),1)
         call mxma(b(ib(5)),nvdt,1, r(ivd,i),1,1, k,1,1, naar,nvdt,1)
         call daxpy_wr(naar,one,k,1,bxr(iaa,i),1)
      endif
c
6000  if(ib(6).ne.0) then
          if (flags(22)) then
*@ifdef direct
*            if(flags(3)) call timer(' ',6, event,nlist)
*            call md5(fdd,qvv,r(ivd,i),bxr(ivd,i))
*            call mvdvd2(cmo,scr2,scr3,f1,bxr(ivd,i))
*            if(flags(3)) call timer(' ',5, event,nlist)
*@endif
          else
             call mvdvd1(b(ib(6)),r(ivd,i),bxr(ivd,i),k,addvd)
          endif
          if (flags(8)) then
            write(nlist,
     &       '(/,3x,4h####,2x,"Bxr for the B[vd,vd]:",2x,4h####)')
            call prtvec(nlist,'Bxr[vd]',bxr(ivd,i),nvdt)
          endif
      endif
c
      if(ib(7).ne.0)then
         call mvaad(b(ib(7)),r(iad,i),bxr(iva,i),k,addva,addad)
         call madva(b(ib(7)),r(iva,i),bxr(iad,i),k,addva,addad)
      endif
c
      if(ib(8).ne.0)then
         call mxma(b(ib(8)),1,nvat, r(iaa,i),1,1, k,1,1, nvat,naar,1)
         call daxpy_wr(nvat,one,k,1,bxr(iva,i),1)
         call mxma(b(ib(8)),nvat,1, r(iva,i),1,1, k,1,1, naar,nvat,1)
         call daxpy_wr(naar,one,k,1,bxr(iaa,i),1)
      endif
c
9000  if(ib(9).ne.0)then
         if(flags(22)) then
*@ifdef direct
*           if (flags(3)) call timer(' ',6, event,nlist)
*           call md3(fad,qad,r(ivd,i),bxr(iva,i))
*           call mvavd1(cmo,scr2,scr3,f1,d1,bxr(iva,i))
*           call md4(fad,qad,r(iva,i),bxr(ivd,i))
*           call mvavd2(scr1,scr3,f3,cmo,bxr(ivd,i))
*           if (flags(3)) call timer(' ',5, event,nlist)
*@endif
         else
            call mxma(b(ib(9)),1,nvat, r(ivd,i),1,1, k,1,1, nvat,nvdt,1)
            call daxpy_wr(nvat,one,k,1,bxr(iva,i),1)
            call mxma(b(ib(9)),nvat,1, r(iva,i),1,1, k,1,1, nvdt,nvat,1)
            call daxpy_wr(nvdt,one,k,1,bxr(ivd,i),1)
         endif
         if (flags(8)) then
            write(nlist,
     &       '(/,3x,4h####,2x,"Bxr for the B[va,vd]:",2x,4h####)')
            call prtvec(nlist,'Bxr[va]',bxr(iva,i),nvat)
            call prtvec(nlist,'Bxr[vd]',bxr(ivd,i),nvdt)
         endif
      endif
c
      if(ib(10).ne.0)call mvava(b(ib(10)),r(iva,i),bxr(iva,i),k,addva)
c
      if (qcoupl.and.(cdir.ne.1)) then
c
       icount = 1
          do ist=1,nst
c
           if(ib_c(1,ist).ne.0)then
            call mxma(b(ib_c(1,ist)),1,ncsf_f(ist)*navst(ist), r(iad,i),
     &       1,1,p,1,1, ncsf_f(ist)*navst(ist),nadt,1)
            call daxpy_wr(ncsf_f(ist)*navst(ist),one,p,1,cxr(icount,
     + i),1)

             if (flags(8)) then
              write(nlist,
     &         '(/,3x,"Cxr after Cxr[csf,ad]*r[ad] for DRT Nr.",i3)')
     &         ist
              call prtvec(nlist,'Cxr[csf]',cxr(icount,i),
     &         ncsf_f(ist)*navst(ist))
             endif ! (flags(8))
           endif ! (ib_c(1,ist).ne.0)
c
           if(ib_c(2,ist).ne.0)then
            call mxma(b(ib_c(2,ist)),1,ncsf_f(ist)*navst(ist), r(iaa,i),
     &       1,1, p,1,1, ncsf_f(ist)*navst(ist),naar,1)
            call daxpy_wr(ncsf_f(ist)*navst(ist),one,p,1,cxr(icount,
     + i),1)

             if (flags(8)) then
              write(nlist,
     &         '(/,3x,"Cxr after Cxr[csf,aa]*r[aa] for DRT Nr.",i3)')
     &         ist
              call prtvec(nlist,'Cxr[csf]',cxr(icount,i),
     &         ncsf_f(ist)*navst(ist))
             endif ! (flags(8))
           endif ! (ib_c(2,ist).ne.0)
c
           if(ib_c(3,ist).ne.0)then
            call mxma(b(ib_c(3,ist)),1,ncsf_f(ist)*navst(ist), r(ivd,i),
     &       1,1,p,1,1, ncsf_f(ist)*navst(ist),nvdt,1)
            call daxpy_wr(ncsf_f(ist)*navst(ist),one,p,1,cxr(icount,
     + i),1)

             if (flags(8)) then
              write(nlist,
     &         '(/,3x,"Cxr after Cxr[csf,vd]*r[vd] for DRT Nr.",i3)')
     &         ist
              call prtvec(nlist,'Cxr[csf]',cxr(icount,i),
     &         ncsf_f(ist)*navst(ist))
             endif ! (flags(8))
           endif ! (ib_c(3,ist).ne.0)
c
           if(ib_c(4,ist).ne.0)then
            call mxma(b(ib_c(4,ist)),1,ncsf_f(ist)*navst(ist),r(iva,i),
     &       1,1,p,1,1, ncsf_f(ist)*navst(ist),nvat,1)
            call daxpy_wr(ncsf_f(ist)*navst(ist),one,p,1,cxr(icount,
     + i),1)

             if (flags(8)) then
              write(nlist,
     &         '(/,3x,"Cxr after Cxr[csf,vd]*r[vd] for DRT Nr.",i3)')
     &         ist
              call prtvec(nlist,'Cxr[csf]',cxr(icount,i),
     &         ncsf_f(ist)*navst(ist))
             endif ! (flags(8))
           endif ! (ib_c(4,ist).ne.0)
c
          icount = icount + ncsf_f(ist)*navst(ist)
          enddo ! ist
c
      endif ! qcoupl.and.(cdir.ne.1)
c
100   continue
c
c    # conventional calculation of the C*s and M*s
c    #  matrix-vector products
c
c     write(nlist,*)'nolds,nnews= ',nolds,nnews
      do 120 i=nolds+1,nnews
c
      call wzero(ndimr,cxs(1,i),1)
      call wzero(ncfx,mxs(1,i),1)
       if (flags(8)) then
        write(nlist,'(/,3x,"Actual s vector:")')
        call prtvec(nlist,'s',s(1,i),ncfx)
       endif ! (flags(8))
c
      if (cdir.ne.1) then
c
         icount = 1
         do ist =1,nst
c
          if(ib_c(1,ist).ne.0) then
           call gmxma
     +      (b(ib_c(1,ist)),ncsf_f(ist)*navst(ist),1, s(icount,i),1,1,
     +      cxs(iad,i),1,1,nadt,ncsf_f(ist)*navst(ist),1,1.0d+00,
     +      1.0d+00)
            if (flags(8)) then
             write(nlist,
     &        '(/,3x,"Cxs after Cxs[csf,ad]*s[csf] for DRT Nr.",i3)')
     &        ist
             call prtvec(nlist,'Cxs[ad]',cxs(iad,i),nadt)
            endif ! (flags(8))
          endif ! (ib_c(1,ist).ne.0)
c
          if(ib_c(2,1).ne.0) then
           call gmxma
     +      (b(ib_c(2,ist)),ncsf_f(ist)*navst(ist),1, s(icount,i),1,1,
     +      cxs(iaa,i),1,1, naar,ncsf_f(ist)*navst(ist),1,1.0d+00,
     +      1.0d+00)
            if (flags(8)) then
             write(nlist,
     &        '(/,3x,"Cxs[csf,aa]*s[csf] for DRT Nr.",i3)')
     &        ist
             call prtvec(nlist,'Cxs[aa]',cxs(iaa,i),naar)
            endif ! (flags(8))
          endif ! (ib_c(2,1).ne.0)
c
          if(ib_c(3,1).ne.0) then
           call gmxma
     +      (b(ib_c(3,ist)),ncsf_f(ist)*navst(ist),1, s(icount,i),1,1,
     +      cxs(ivd,i),1,1, nvdt,ncsf_f(ist)*navst(ist),1,1.0d+00,
     +      1.0d+00)
            if (flags(8)) then
             write(nlist,
     &       '(/,3x,"Cxs[csf,va]*s[csf] for DRT Nr.",i3)')
     &       ist
             call prtvec(nlist,'Cxs[vd]',cxs(ivd,i),nvdt)
            endif ! (flags(8))
           endif ! (ib_c(3,1).ne.0)
c
           if(ib_c(4,1).ne.0) then
            call gmxma
     +       (b(ib_c(4,ist)),ncsf_f(ist)*navst(ist),1, s(icount,i),1,1,
     +       cxs(iva,i),1,1, nvat,ncsf_f(ist)*navst(ist),1,1.0d+00,
     +       1.0d+00)
             if (flags(8)) then
              write(nlist,
     &        '(/,3x,"Cxs[csf,va]*s[csf] for DRT Nr.",i3)')
     &        ist
              call prtvec(nlist,'Cxs[va]',cxs(iva,i),nvat)
             endif ! (flags(8))
           endif ! (ib_c(4,1).ne.0)
c
          icount = icount + ncsf_f(ist)*navst(ist)
          enddo ! ist =1,nst
c
      endif ! cdir.ne.1
c
c   #   M*s calculation
c
      if ((.not.flags(11)).and.flags(12)) then
      if (flags(3)) call timer(' ',6, event3,nlist)
c
c   #   Direct M*s calculations
c
      icount = 1
      icount2 = 1
c
      do ist=1,nst
c
c     # ind(*) is referenced only if necessary.
      qind = nwalk_f(ist) .ne. ncsf_f(ist)
         do j=1,navst(ist)
         call hmcxv(.false.,nnews-nolds,ncsf_f(ist),dumf,mdiag(icount2),
     &     s(icount,i),mxs(icount,i),ftbuf,lenbft,
     &     modrt,uaa,h2,add(addpt(10)),
     &     add(addpt(4)),xp(1+cpt2(ist)),z(1+cpt2(ist)),
     &     xbar(1+cpt2(ist)),rev(1+cpt3(ist)),ind(1+cpt3(ist)),
     &     qind,ist)
         call daxpy_wr(ncsf_f(ist),-1.0d+00*heig(ist,j),
     &    s(icount,i),1, mxs(icount,i),1)
         call dscal_wr(ncsf_f(ist),2.0d+00*wavst(ist,j),
     &    mxs(icount,i),1)
c
c
         if (flags(8)) then
         write(nlist,'(/2x,''Mxs DRT Nr.'',i2,2x,''state Nr.'',i3)')
     &   ist,j
         call prtvec(nlist,'Mxs',mxs(icount,i),ncsf_f(ist))
         endif
c
         icount = icount + ncsf_f(ist)
         enddo !  j --> navst(ist)
       icount2 = icount2 + ncsf_f(ist)
       enddo ! ist --> nst
c
       if (flags(3)) call timer(' ',5, event3,nlist)
       else
c
c    #    convectional M*s calculation
c
      icount = 1
         do ist = 1,nst
         icount2 = 0
            do j=1,navst(ist)
             if(ib_m(ist).ne.0) then
              call spmxv(b(ib_m(ist)+icount2),s(icount,i),
     &         mxs(icount,i),ncsf_f(ist))
c
               if (flags(8)) then
               write(nlist,
     &          '(/2x,''DRT Nr. '',i2,2x,''state Nr. '',i3)')
     &           ist,j
                call prtvec(nlist,'Mxs',mxs(icount,i),ncsf_f(ist))
               endif ! (flags(8))
c
              icount = icount + ncsf_f(ist)
              icount2 = icount2 + ncsf_f(ist)*(ncsf_f(ist)+1)/2
             endif
            enddo ! i2,=1,navst(i)
         enddo ! do ist = 1,nst
      endif ! (.not.flags(11)).and.flags(12)
c
120   continue
c
c   ##
c   ##  Direct calculation of the Cxs and Cxr matrix-vector products
c   ##
c
      if (cdir.eq.1) then
c
c    # lcxs evaluation
      lcxs = .false.
      if ((nnews - nolds).eq.1) lcxs = .true.
      if ((nnews - nolds).gt.1) then
      stop
      endif
c
c    # lcxr evaluation
      lcxr = .false.
      if (qcoupl.and.(nnewr - noldr.eq.1)) lcxr = .true.
      if ((nnewr - noldr).gt.1) then
      stop
      endif
      if ((.not.lcxs).and.(.not.lcxr)) go to 1250
      if (flags(3)) call timer(' ',6, event2,nlist)
c
      icount = 1
      if (lcxs) is = nolds+1
      if (lcxr) ir = noldr+1
      if (lcxs) call wzero(ndimr,cxs(1,is),1)
c
         do ist = 1,nst
c
         call mcxv(
     &    qc,               xbar(1+cpt2(ist)),       xp(1+cpt2(ist)),
     &    z(1+cpt2(ist)),   rev(1+cpt3(ist)),        ind(1+cpt3(ist)),
     &    modrt,         add,             iorder,       hmcvec(icount),
     &    s(icount,is),  r(iad,ir),       r(iaa,ir),    r(ivd,ir),
     &    r(iva,ir),     cxr(icount,ir),  cxs(iad,is),  cxs(iaa,is),
     &    cxs(ivd,is),   cxs(iva,is),     uad,          uaa,
     &    uvd,           uva,             nadt,         nvdt,
     &    ist,           nipbuk,          lastb,        lenbuk,
     &    screst,        avcsk,           lcxr,         lcxs)
c
         if (lcxr) then
          ipos = icount
            do ixx=1,navst(ist)
            call dscal_wr(ncsf_f(ist),wavst(ist,ixx),cxr(icount,ir),1)
            icount = icount + ncsf_f(ist)
            enddo ! ixx=1,navst(ist)
          if (flags(8)) then
           write(nlist,'(/,3x,''Total Cxr for the DTR Nr.'',i3)')ist
           call prtvec(nlist,'Cxr[csf]',cxr(ipos,ir),
     &     ncsf_f(ist)*navst(ist))
          endif ! (flags(8))
         endif ! (lcxr)
c
          if (lcxs.and.flags(8)) then
           write(nlist,'(/,3x,''Cxs for the DTR Nr.'',i3)')ist
           call prtvec(nlist,'Cxs[ad]',cxs(iad,is),nadt)
           call prtvec(nlist,'Cxs[aa]',cxs(iaa,is),naar)
           call prtvec(nlist,'Cxs[vd]',cxs(ivd,is),nvdt)
           call prtvec(nlist,'Cxs[va]',cxs(iva,is),nvat)
          endif ! (lcxs.and.flags(8))
c
          if (lcxs.and.(.not.lcxr))
     &     icount = icount + ncsf_f(ist)*navst(ist)
c
         enddo ! ist --> nst
      if (flags(3)) call timer(' ',5, event2,nlist)
      endif ! cdir.eq.1
c
c
c  form reduced matrix elements for new vectors r and s.
c
1250  ij=nndxf(noldr+1)
      do 140 i=noldr+1,nnewr
      do 140 j=1,i
      ij=ij+1
      br(ij)=ddot_wr(ndimr,r(1,i),1,bxr(1,j),1)
140   continue
c
      do 150 i=1,nolds
      do 150 j=noldr+1,nnewr
      cr(j,i)=ddot_wr(ndimr,r(1,j),1,cxs(1,i),1)
150   continue
c
      do 160 i=nolds+1,nnews
      do 160 j=1,nnewr
      cr(j,i)=ddot_wr(ndimr,r(1,j),1,cxs(1,i),1)
160   continue
c
      ij=nndxf(nolds+1)
      do 170 i=nolds+1,nnews
      do 170 j=1,i
      ij=ij+1
      mr(ij)=ddot_wr(ncfx,s(1,i),1,mxs(1,j),1)
170   continue
c
      do 180 i=noldr+1,nnewr
180   wr(i)=ddot_wr(ndimr,w,1,r(1,i),1)
c
c  update vector counts.
c
      noldr=nnewr
      nolds=nnews
c
      if(flags(8))call plblkt('br(*) matrix',br,noldr,'row ',2,nlist)
      if(flags(8).and.nolds.ne.0)
     +  call plblkt('mr(*) matrix',mr,nolds,'row ',2,nlist)
      if(flags(8).and.nolds.ne.0) call prblkt('cr(*) matrix',
     +  cr,nmaxr,noldr,nolds,'row ','col ',2,nlist)
c
c  solve psci equations in the subspace spanned by r(*) and s(*)
c  for the optimal expansion coefficients.
c
      nsci=noldr+1
c
c  first form m**-1 in factored form. store in scr(*).
c
      if(nolds.ne.0)then
         nns=nndxf(nolds+1)
         call dcopy_wr(nns,mr,1,scr,1)
         call wspfa(scr,nolds,kpvt,info)
         if(info.ne.0)then
            write(nlist,6010)info
            call bummer('micro: from wspfa, info=',info,faterr)
         endif
      endif
c
c  loop over rows and columns of the psci matrix stored in the
c  matrix zr(nsci,nsci).
c  only the lower-triangle needs to be calculated for the eispac call.
c  for each row and column, include the c*(m**-1)*ct contribution.
c  use pr(*) as workspace to hold a column of (m**-1)*ct.
c
      ij1=0
      do 220 i=1,noldr
         if(nolds.ne.0)then
            call dcopy_wr(nolds,cr(i,1),nmaxr,pr,1)
            call wspsl(scr,nolds,kpvt,pr)
         endif
         do 210 j=1,i
            ij1=ij1+1
            ij2=(j-1)*nsci+i
            if(nolds.ne.0)then
               zr(ij2)=br(ij1)-ddot_wr(nolds,cr(j,1),nmaxr,pr,1)
            else
               zr(ij2)=br(ij1)
            endif
210      continue
220   continue
c
c  wr(*) terms.
c
      call dcopy_wr(noldr,wr,1,zr(nsci),nsci)
c
c  usually psci(nsci,nsci)=zero.
c  to increase the radius of convergence, shift by the
c  energy shift parameter, eshsci.  for second-order convergence
c  eshsci should be of order wnorm or smaller.
c
      zr(nsci*nsci)=-eshsci
c
c  diagonalize using k(*) and pr(*) as eigenvalue storage and
c  workspace.  eigenvectors are returned in zr(*,*).  usually the
c  lowest eigenvalue and eigenvector are used.  if(flags(20)) then
c  use the vector with the maximum overlap with the current wave
c  function.
c
      call wzero(ndimr,k,1)
      call eigip(nsci,nsci,zr,k,pr)
c
      imxov=1
      if(flags(20))then
         imxov=idamax_wr(nsci,zr(nsci),nsci)
         if(imxov.ne.1)then
            call dswap_wr(1,k(1),1,k(imxov),1)
            call dswap_wr(nsci,zr(1),1,zr((imxov-1)*nsci+1),1)
         endif
      endif
c
      esci=k(1)
      z0=zr(nsci)
      if(flags(8))then
         write(nlist,6030)imxov,z0,(k(i),i=1,nsci)
         write(nlist,6060)(zr(i),i=1,nsci)
      endif
c
c  form pr = -(m**-1)*ct*z  where z(*) is in the first column of
c  the eigenvector matrix. factored m**-1 is still in scr(*).
c
      pnorm=zero
      if(nolds.ne.0)then
         call mxma(cr,nmaxr,1, zr,1,1, pr,1,1, nolds,noldr,1)
         call wspsl(scr,nolds,kpvt,pr)
         call dscal_wr(nolds,-one,pr,1)
         pnorm=dnrm2_wr(nolds,pr,1)
      endif
c
c  use z(*) and pr(*) to form residual vectors.
c  store residual vectors in k(*) and p(*).
c
c  res(z)= (b-esci)*z + c*p + z0*w
c
      call wzero(ndimr,k,1)
      do 320 i=1,noldr
         term=zr(i)
         if(term.ne.zero)then
            call daxpy_wr(ndimr,term,bxr(1,i),1,k,1)
            call daxpy_wr(ndimr,-esci*term,r(1,i),1,k,1)
         endif
320   continue
c
      do 340 i=1,nolds
         term=pr(i)
         if(term.ne.zero)call daxpy_wr(ndimr,term,cxs(1,i),1,k,1)
340   continue
c
      call daxpy_wr(ndimr,z0,w,1,k,1)
c
      rznorm=dnrm2_wr(ndimr,k,1)
c
c  form residual vector for the current p(*) vector.
c
c  r(p) = ct*z + m*p
c
      rpnorm=zero
      if(ncfx.ne.0)then
      call wzero(ncfx,p,1)
c
      do 360 i=1,noldr
      term=zr(i)
      if(term.ne.zero)call daxpy_wr(ncfx,term,cxr(1,i),1,p,1)
360   continue
c
      do 380 i=1,nolds
      term=pr(i)
      if(term.ne.zero)call daxpy_wr(ncfx,term,mxs(1,i),1,p,1)
380   continue
c
c  since c(*) and m(*) are in the csf basis, project out any
c  hmcvec(*) components.
c
      if (asprojection.eq.1) then
      write(nlist,*)'performing all-state projection'
      ii = 1
      jjstart=1
         do ist = 1,nst
            do i=1,navst(ist)
            jj=jjstart
               do j=1,navst(ist)
               term=-ddot_wr(ncsf_f(ist),hmcvec(jj),1,p(ii),1)
               if (abs(term).gt.1.d-8) call
     &          daxpy_wr(ncsf_f(ist),term,hmcvec(jj),1,p(ii),1)
               jj=jj+ncsf_f(ist)
               enddo ! end of: do j=1,navst(ist)
            if (flags(8))call prtvec(nlist,'p(ii) after othog',
     &       p(ii),ncsf_f(ist))
            ii = ii+ncsf_f(ist)
            enddo ! end of: do i=1,navst(ist)
         jjstart=jjstart+navst(ist)*ncsf_f(ist)
         enddo ! end of: do ist = 1,nst
c
c  ....just a test of the prevous orthogonalization
      ii = 1
      jjstart=1
         do ist = 1,nst
            do i=1,navst(ist)
            jj=jjstart
               do j=1,navst(ist)
               term=ddot_wr(ncsf_f(ist),hmcvec(jj),1,p(ii),1)
                if(abs(term).gt.1.d-8) then
                write(nlist,*)'drt =',ist
                write(nlist,*)'hmcvec(jj), jj =',jj
                write(nlist,*)'p(ii), ii =',ii
                write(nlist,*)'overlap =',term
                call bummer('hvec|p overlap too big',0,faterr)
                endif
               jj=jj+ncsf_f(ist)
               enddo ! end of: do j=1,navst(ist)
            ii = ii+ncsf_f(ist)
            enddo ! end of: do i=1,navst(ist)
         jjstart=jjstart+navst(ist)*ncsf_f(ist)
         enddo ! end of: do ist = 1,nst
c
      else ! if (asprojection.eq.1) then
c
      write(nlist,*)'performing signle-state projection'
       ii = 1
         do ist = 1,nst
           do i=1,navst(ist)
           term=-ddot_wr(ncsf_f(ist),hmcvec(ii),1,p(ii),1)
            if(term.ne.zero)call daxpy_wr(ncsf_f(ist),term,
     &       hmcvec(ii),1,p(ii),1)
           ii = ii+ncsf_f(ist)
           enddo ! end of: do i=1,navst(ist)
         enddo ! ist
c
      endif! end of: if (asprojection.eq.1) then

c
      rpnorm=dnrm2_wr(ncfx,p,1)
      endif
c
      if(rznorm.le.rtol .and. rpnorm.le.rtol)go to 610
c
c  residuals are too big.  form new expansion vectors and continue
c  micro-iterations.
c
      if(rznorm.gt.rtol)then
c
      do 400 i=1,ndimr
      term=bdiag(i)-esci
      if(abs(term).lt.dtol)term=dtol
400   k(i)=k(i)/term
c
c  compress the nmaxr r(*) vectors to subspace of dimension (nmaxr-1).
c  unused elements of zr() are used to store the transformation matrix.
c  scr(*) is used for scratch space.
c
        if(noldr.eq.nmaxr)then
         call rcmprs(noldr,ndimr,ncfx,nolds,
     +    br,cr,wr,zr,zr(nsci+1),scr,r,bxr,cxr)
         noldr=noldr-1
         nnewr=noldr
        endif ! end of: if(noldr.eq.nmaxr)then
c
c  orthonormalize the new expansion vector r(*) to the previous r(*)
c  vectors.
c
        do i=1,noldr
        term=-ddot_wr(ndimr,k,1,r(1,i),1)
        if(term.ne.zero)call daxpy_wr(ndimr,term,r(1,i),1,k,1)
        enddo ! end of: do i=1,noldr
c
      term=dnrm2_wr(ndimr,k,1)
      if(one+term .ne. one)then
         term=one/term
         call dscal_wr(ndimr,term,k,1)
         nnewr=nnewr+1
         call dcopy_wr(ndimr,k,1,r(1,nnewr),1)
      endif
c
      endif
c
c  form the new expansion vector s(*).
c
      if(rpnorm.gt.rtol)then
c
      if ((.not.flags(11)).and.flags(12)) then
      ii = 1
          ipt = 1
         do ist=1,nst
           do j=1,navst(ist)
               ii2 = 0
             do i=1,ncsf_f(ist)
             term=2.0d+00*wavst(ist,j)*(mdiag(ipt+ii2)-heig(ist,j))
             if(abs(term).lt.dtol)term=dtol
             p(ii)=p(ii)/term
             ii = ii + 1
             ii2 = ii2 + 1
             enddo ! i --> ncsf_(ist)
           enddo ! j --> navst(ist)
         ipt = ipt + ncsf_f(ist)
         enddo ! ist --> nst
      else
         do i=1,ncfx
         term=mdiag(i)
         if(abs(term).lt.dtol)term=dtol
         p(i)=p(i)/term
c-       write(nlist,'(i3,1x,f13.8,1x,f13.8)')i,p(i),term
         enddo ! do i=1,ncfx
      endif
c
      if(nolds.eq.nmaxs)then
c
c  compress the nmaxs s(*) vectors to subspace of dimension (nmaxs-1).
c  unused zr(*) is used to store the transformation matrix.
c  scr(*) is used for scratch.
c
         call scmprs(nolds,ndimr,ncfx,noldr,nmaxr,mr,
     +    cr,pr,zr(nsci+1),scr,s,cxs,mxs)
         nolds=nolds-1
         nnews=nolds
      endif
c
c  orthonormalize new expansion vector s(*).
c
      if (asprojection.eq.1) then
c
      write(nlist,*)'performing all-state projection'
      ii = 1
      jjstart=1
         do ist = 1,nst
            do i=1,navst(ist)
            jj=jjstart
               do j=1,navst(ist)
               term=-ddot_wr(ncsf_f(ist),hmcvec(jj),1,p(ii),1)
               if(term.ne.zero)call
     &          daxpy_wr(ncsf_f(ist),term,hmcvec(jj),1,p(ii),1)
               jj=jj+ncsf_f(ist)
               enddo ! end of: do j=1,navst(ist)
            if (flags(8)) call prtvec(nlist,'p(ii) after othog',
     &       p(ii),ncsf_f(ist))
            ii = ii+ncsf_f(ist)
            enddo ! end of: do i=1,navst(ist)
         jjstart=jjstart+navst(ist)*ncsf_f(ist)
         enddo ! end of: do ist = 1,nst
c
c  ....just a test of the prevous orthogonalization
      ii = 1
      jjstart=1
         do ist = 1,nst
            do i=1,navst(ist)
            jj=jjstart
               do j=1,navst(ist)
               term=ddot_wr(ncsf_f(ist),hmcvec(jj),1,p(ii),1)
                if(term.gt.1.d-8) then 
                 write(nlist,8900) ist,i,j,term 
8900   format('large overlap: DRT=',i2,' states ',i3,i3,
     .       ' overlap=',f15.10)
              call bummer('large overlap in all-state-projection',0,0)
               endif
               jj=jj+ncsf_f(ist)
               enddo ! end of: do j=1,navst(ist)
            ii = ii+ncsf_f(ist)
            enddo ! end of: do i=1,navst(ist)
         jjstart=jjstart+navst(ist)*ncsf_f(ist)
         enddo ! end of: do ist = 1,nst
c
      else ! if (asprojection.eq.1) then
c
       write(nlist,*)'performing single-state projection'
       ii = 1
         do ist = 1,nst
           do i=1,navst(ist)
           term=-ddot_wr(ncsf_f(ist),hmcvec(ii),1,p(ii),1)
            if(term.ne.zero)call daxpy_wr(ncsf_f(ist),term,
     &       hmcvec(ii),1,p(ii),1)
           ii = ii+ncsf_f(ist)
           enddo ! end of: do i=1,navst(ist)
         enddo ! ist
c
      endif! end of:  if (asprojection.eq.1) then
c
      do 560 i=1,nolds
         term=-ddot_wr(ncfx,p,1,s(1,i),1)
         if(term.ne.zero)call daxpy_wr(ncfx,term,s(1,i),1,p,1)
560   continue
c
cmd
c    the norm based on dnrm2_wr is not aditive (in case of stave),
c     so we use the abs norm
cmd   term=dnrm2_wr(ncfx,p,1)
      term = zero
      do i=1,ncfx
      term = term + abs(p(i))
      enddo ! i=1,ncfx
      if(one+term .ne. one)then
         term=one/term
         call dscal_wr(ncfx,term,p,1)
         nnews=nnews+1
         call dcopy_wr(ncfx,p,1,s(1,nnews),1)
      endif
c
      endif
c
c  end of micro-iteration.
c
      if (flags(8)) then
      write(nlist,
     & '(/,3x,4h####,2x,"r vector for the next miter:",2x,4h####)')
      call prtvec(nlist,'r[ad]',r(iad,nnewr),nadt)
      call prtvec(nlist,'r[aa]',r(iaa,nnewr),naar)
      call prtvec(nlist,'r[vd]',r(ivd,nnewr),nvdt)
      call prtvec(nlist,'r[va]',r(iva,nnewr),nvat)
      write(nlist,*)
      endif
c
      if (flags(8)) then
      write(nlist,'('' miter number: '',i3)') iter
      write(nlist,7000)
     & imxov,z0,pnorm,rznorm,rpnorm,noldr,nnewr,nolds,nnews
7000  format(3x,' imxov=',i3,' z0=',f11.8,' pnorm=',
     & 1pe11.4,' rznorm=',e11.4,' rpnorm=',e11.4,
     & ' noldr=',i3,' nnewr=',i3,' nolds=',i3,' nnews=',i3)
      endif
      if(noldr.eq.nnewr .and. nolds.eq.nnews)go to 601
c
600   continue
601   continue
c
c  exit from loop means convergence was not reached.
c
      write(nlist,*)'*** psci convergence not reached ***'
c
610   continue
c
      write(nlist,'('' Total number of micro iterations: '',i4)')
     & iter
      write(nlist,
     & '(/'' ***  micro: final psci convergence values:  ***'')')
      write(nlist,7000)
     & imxov,z0,pnorm,rznorm,rpnorm,noldr,nnewr,nolds,nnews
      if(abs(z0).lt.aones2)write(nlist,*)'*** warning *** small z0.'
      write(nlist,*)
c
      if (flags(3)) call timer('Direct Bxr time contribution',
     & 4, event,nlist)
      if (flags(3)) call timer('Direct Cxr and Cxs time contr.',
     & 4, event2,nlist)
      if (flags(3)) call timer('Direct Mxs time contribution',
     & 4, event3,nlist)
c
c  compute the last p(*) vector normalized to pt*p=1.
c
      qpcon=pnorm.ne.zero
      if(qpcon)then
         call wzero(ncfx,p,1)
         do 720 i=1,nolds
            term=pr(i)/pnorm
            if(term.ne.zero)call daxpy_wr(ncfx,term,s(1,i),1,p,1)
720      continue
      endif
c
c  compute the last z(*) vector and scale by sign(z0) to form k(*).
c  either (z0**2 + z*z)=1 or (z0'**2 + p'*p' + z'*z')=1 convention
c  may be used.  this version uses the latter, more conservative,
c  scaling factor.
c
      call wzero(ndimr,k,1)
c
      do 740 i=1,noldr
      term=zr(i)*sign( one/sqrt(one+pnorm**2), z0)
      if(term.ne.zero)call daxpy_wr(ndimr,term,r(1,i),1,k,1)
740   continue
      knorm=dnrm2_wr(ndimr,k,1)
      if(flags(4))write(nlist,6050)knorm,k
c
c  compute the approximate energy lowering for the next mcscf
c  iteration due to the orbital corrections in k(*).
c
      apxde=-(esci+eshsci)*half
c
      return
6010  format(/' *** error *** micro: info (from wspfa) =',i8)
6030  format(/' imxov=',i3,' z0=',f11.8/
     +  ' subspace sci eigenvalues esci(*)='/(1x,1p10e12.4))
6050  format(/' knorm=',1pe12.4,'  k(*)='/(1x,10e12.4))
6060  format(/' zr(*)='/(1x,1p10e12.4))
      end
c
c*****************************************************************
c
      subroutine mcxv(
     & qc,        xbar,       xp,
     & z,         rev,        ind,
     & modrt,     add,        iorder,     hvec,
     & s,         rad,        raa,        rvd,
     & rva,       cxr,        cxsad,      cxsaa,
     & cxsvd,     cxsva,      uad,        uaa,
     & uvd,       uva,        nadt,       nvdt,
     & ist,       npbuk,      lastb,      lenbuk,
     & scr,       avcsk,      lcxr,       lcxs)
c
c     The subroutine calculates direct the matrix vector product:
c
c        Cxs[vd] = sum(csf) C[csf,vd]*s[csf]
c
c    input:
c     hvec  -  CI vector
c     s     -  vector
c
c    output:
c     cxs - matrix vector product

       implicit none
      integer  forbyt
      external forbyt
c
c   ## parameter section
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c   ##  common section
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbft,nact
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(10),nact)
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
c
      integer hbci
      common/chbci/hbci(4,3)
c
      integer  lena1e, n1eabf,lenbft,
     &        lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2
      common/cfinfo/lena1e,n1eabf,lenbft,
     &  lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2


c
      integer addpt,numint,bukpt,intoff,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
      integer iunits,nlist
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c   ##  integer section
c
      integer avcsk,add(*)
      integer buk,bkpt,blk
      integer cpt(5)
      integer ist,ind(*),i,intpt(5),ibkpt,icount,iorder(*),icptx
      integer j
      integer lastb(*),lenbuk
      integer modrt(0:*)
      integer nadt,nvdt,npbuk
      integer rev(*)
      integer xbar(*),xp(*)
      integer z(*)
c
c   ##  real*8 section
c
      real*8 cxsad(*),cxsaa(*),cxsvd(*),cxsva(*),cxr(*)
      real*8 ddot_wr,fact
      real*8 hvec(*)
      real*8 rad(*),raa(*),rvd(*),rva(*)
      real*8 scr(*),s(*)
      real*8 uaa(*),uad(*),uvd(*),uva(*)
c
c   ##  logical section
c
      logical lcxs,lcxr,qbk1
c
c ####  dummy variables
c
      logical qc(4)
c
c
c   ## get the h2t3 and h2t4 integrals
c
      cpt(1) = 1
      do 1200 i=1,5
1200  intpt(i)=cpt(1)
      icptx=cpt(1)
      buk=0
      do 1700 i=1,4
cmd   if (.not.qc(i)) go to 1700
      blk=hbci(i,1)
      go to(1300,1400,1500,1600),blk
1300  continue
c  type 1 integrals.
      intpt(1)=icptx
      icptx=icptx+numint(1)
      qbk1 = .true.
      if (nact.eq.0) qbk1=.false.
      go to 1700
1400  continue
c  type 2 integrals.
      buk=bukpt(2)
      intpt(2)=icptx
      icptx=icptx+numint(2)
      go to 1700
1500  continue
c  type 3 and 4 integrals
      buk=bukpt(3)
      intpt(3)=icptx
      icptx=icptx+numint(3)
      intpt(4)=icptx
      icptx=icptx+numint(4)
      go to 1700
1600  continue
c  type 5 integrals
      buk=bukpt(5)
      intpt(5)=icptx
      icptx=icptx+numint(5)
1700  continue
c
      bkpt = icptx
      ibkpt=bkpt+lenbuk
c
      if(ibkpt+forbyt(npbuk+1)-1.gt.avcsk)then
         write(nlist,*)'intpt = ', intpt
         write(nlist,*)'ibkpt = ',ibkpt
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    ibkpt+forbyt(npbuk+1)-1,avcsk
         call bummer('mcxv: 1 avcsk =',avcsk,faterr)
      endif
c
c   #  zer out the space for integrals
      call wzero(icptx,scr(cpt(1)),1)
c
c   #  read in the integrals
      if(qbk1) call rdbks(lastb(bukpt(1)),
     + scr(intpt(1)),scr(bkpt),scr(ibkpt),lenbuk,npbuk,'mcxv')
      if(buk.ne.0)call rdbks(lastb(buk),
     +  scr(cpt(1)),scr(bkpt),scr(ibkpt),lenbuk,npbuk,'mcxv')
c
c     1:h2t1+h2t2+h2t3+h2t4+h2t5,2:ftbuf,3:tden
      cpt(2) = cpt(1) + icptx
      cpt(3) = cpt(2) + lenbft
      cpt(4) = cpt(3) + ncsf_f(ist)*navst(ist)*3
c
      if (cpt(4).gt.avcsk)then
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    cpt(4),avcsk
         call bummer('mcxv: 2 avcsk =',avcsk,faterr)
      endif
c
c
c   #
c   #   contributions containing the 1-el and 2-el transition
c   #    density matrix (the FT is read)
c   #
c
      if (flags(25)) then
      write(nlist,'(/," Total memory for RDFT_CXV:",i15)') avcsk-cpt(4)
      write(nlist,'(" int  :",i15)') cpt(2)-cpt(1)
      write(nlist,'(" ftbuf:",i15)') cpt(3)-cpt(2)
      write(nlist,'(" tden :",i15)') cpt(4)-cpt(3)
      endif
c
      call rdft_cxv(
     & scr(cpt(2)),   lenbft,        hvec,          s,
     & rad,           raa,           rvd,           rva,
     & iorder,        ncsf_f(ist),   xbar,          xp,
     & z,             rev,           ind,           modrt,
     & .false.,       scr(cpt(3)),   uad,           uaa,
     & uvd,           uva,           scr(intpt(1)), scr(intpt(2)),
     & scr(intpt(3)), scr(intpt(4)), scr(intpt(5)), add(addpt(10)),
     & add(addpt(4)), add,           cxr,           cxsad,
     & cxsaa,         cxsvd,         cxsva,         qc,
     & navst(ist),    ist,           lcxr,          lcxs)
c
c   #
c   # Cxv[cxs] = C[csf,ad].s[csf]
c   #  one electron contribution:
c   # Cxv_1el(ip) =  8 uad(ip)*[sum(n) hvec(n)*s(n)]
c   #
c   # Cxv[cxs] = C[cxs,vd].s[csf]
c   #  one electron contribution:
c   # Cxv_1el(ai) = -8 uvd(ai)*[sum(n) hvec(n)*s(n)]
c   #
c
      if (lcxs) then
         fact = 0.0d+00
         icount = 1
         do i = 1,navst(ist)
            do j=1,ncsf_f(ist)
            fact = fact + wavst(ist,i)*s(icount)*hvec(icount)
            icount = icount + 1
            enddo
          enddo
c
         call daxpy_wr(nvdt,-8.0d+00*fact,uvd,1,cxsvd,1)
         call daxpy_wr(nadt,8.0d+00*fact,uad,1,cxsad,1)
      endif
c
c   #
c   # Cxv[cxs] = C[csf,ad].r[ad]
c   #  one electron contribution:
c   # Cxv_1el(n) = 8.(sum(ip) uad(ip).r(ip)).hvec(n)
c   #
c
      if (lcxr) then
         fact = ddot_wr(nadt,uad,1,rad,1)
         icount = 1
         do i=1,navst(ist)
            call daxpy_wr(ncsf_f(ist),8.0d+00*fact,
     &       hvec(icount),1,cxr(icount),1)
            icount = icount + ncsf_f(ist)
         enddo
         icount = 1
c
c   #
c   # Cxv[cxs] = C[cxs,vd].r[vd]
c   #  one electron contribution:
c   # Cxv_1el(n) = -8.(sum(ai) uvd(ai).r(ai)).hvec(n)
c   #
c
         fact = ddot_wr(nvdt,uvd,1,rvd,1)
         icount = 1
         do i=1,navst(ist)
            call daxpy_wr(ncsf_f(ist),-8.0d+00*fact,
     &       hvec(icount),1,cxr(icount),1)
            icount = icount + ncsf_f(ist)
         enddo
      endif
c
      return
      end
c
c**************************************************************
c
      subroutine rdft_cxv(
     & ftbuf,       lenbft,    hvect,   svect,
     & rad,         raa,       rvd,     rva,
     & iorder,      ncsf,      xbar,    xp,
     & z,           r,         ind,     modrt,
     & qind,        tden,      uad,     uaa,
     & uvd,         uva,       h2t1,    h2t2,
     & h2t3,        h2t4,      h2t5,    off1,
     & addaa,       add,       cxr,     cxsad,
     & cxsaa,       cxsvd,     cxsva,   qc,
     & navst_act,   ist,       lcxr,    lcxs)
c
c  read the unitary group formula tape and construct
c  the one- and two- particle symmetric density and
c  transition density matrix elements over the active orbitals.
c  an index driven algorithm is used to facilitate the construction
c  and use of the density matrix elements.  the required blocks of
c  the c matrix are constructed as the transition density matrix
c  elements are available. density matrix elements of the form
c  <mc| e |mc> are stored in d1(*) and d2(*).
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2       11ab        eiill, eilli
c   3       11a         eiill
c   4       11b         eilil
c
c   5       14ab        half*ellll, ell
c   6       14a         half*ellll
c   7       14b         ell
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      iiil, illl, il
c   3           2a'b'      ijlk, iklj
c   4           1ab        ijkl, ilkj
c   5           3ab        ikjl, iljk
c   6           4ab        iikl, ilki
c               8ab        ijll, illj
c   7           6a'b'      ikkl, ilkk
c   8           12ac       iiil, il
c   9           12bc       illl, il
c  10           1a         ijkl
c               2a'        ijlk
c               6a'        ikkl
c               7'         iklk
c  11           4a         iikl
c               8a         ijll
c  12           2b'        iklj
c               3a         ikjl
c               4b         ilki
c               4b'        ikli
c               5          ilik
c               8b         illj
c               10         iljl
c  13           11b        illi
c               13         half*ilil
c  14           1b         ilkj
c               3b         iljk
c  15           6b'        ilkk
c               12c        il
c==================================================
c  explicit variable declaration: 15-apr-02, m.dallos
c  symmetry ft version: 18-oct-84, ron shepard.
c
       implicit none
c  ##  parameter & common block section
c
      integer iunits
      common/cfiles/iunits(55)
      integer nft, ndft, nlist
      equivalence (iunits(1),nlist)
      equivalence (iunits(10),nft)
      equivalence (iunits(11),ndft)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8),nact,nnta, nsb(8),napsy(8)
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
      equivalence (nxy(1,2),nsb(1))
      equivalence (nxy(1,13),napsy(1))
c
      integer addpt,numint,bukpt,intoff,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
      integer nmotx
         parameter (nmotx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
c
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer msize,csize
      common/size/ msize(maxnst),csize(4,maxnst)
c
c  ##  integer section
c
      integer addaa(*),add(*)
      integer ist,imd,iorder(*),ind(*),ind2,ism(8),imo,ii,isym,iidx,
     &        igo,ixx,it,icode,ism2(8)
      integer jmo,jj,jklsym,jidx,jsym
      integer head
      integer kntft,kmo,kk,ksym,klsym,kjdx,kidx
      integer lenbft,lmo,ll,lsym,lldx,lkdx,ljdx,lt,lidx
      integer modrt(0:*)
      integer navst_act,nwalk,ncsf,ncsfx2,ncsfx3,nw,ns,nv,nsv
      integer off1(*)
      integer r(*)
      integer symw
      integer tail
      integer xbar(nsym,*),xp(nsym,*)
      integer yb(8),yk(8)
      integer z(nsym,*),spindens
c
c  ##  real*8 section
c
      real*8 cxsad(*),cxsaa(*),cxsvd(*),cxsva(*),cxr(*)
      real*8 d1llt,d1lit,d2llii,d2lili,d2llll,d2lkji,d2ljki,
     &       d2likj,d2liii,d2llli
      real*8 emc_ave
      real*8 fockad,fockaa,fockvd,fockva,ftbuf(lenbft)
      real*8 h2t1(*),h2t2(*),h2t3(*),h2t4(*),h2t5(*),
     &       hvect(ncsf,navst_act)
      real*8 rad(*),raa(*),rvd(*),rva(*)
      real*8 svect(ncsf,*)
      real*8 tden(ncsf,navst_act,3)
      real*8 uaa(*),uad(*),uvd(*),uva(*)
      real*8 val(3)
c
c  ##  logical section
c
      logical qind,qc(4),qt(3),qcx
      logical lcxr,lcxs
c
c  ##  character section
c
      character*60 fname1,fname2
c
c  ##  external section
c
      real*8 ddot_wr
      external ddot_wr
c
c-----------------------------------------------------------------
c
      if (nst.eq.1) then
         fname1 = 'mcdftfl'
         fname2 = 'mcoftfl'
      elseif (ist.ge.10) then
         write(fname1,'(a8,i2)')'mcdftfl.',ist
         write(fname2,'(a8,i2)')'mcoftfl.',ist
      else
         write(fname1,'(a8,i1)')'mcdftfl.',ist
         write(fname2,'(a8,i1)')'mcoftfl.',ist
      endif
      call trnfln( 1, fname1 )
      call trnfln( 1, fname2 )
      open(unit=ndft,file=fname1,status='old',form='unformatted')
      open(unit=nft,file=fname2,status='old',form='unformatted')
c
      ncsfx2=2*ncsf
      ncsfx3=3*ncsf
      qcx=qc(1).or.qc(2).or.qc(3).or.qc(4)
c
c  diagonal elements.
c
      rewind ndft
      kntft=1
      nw=0
      read(ndft)ftbuf
c
      do 1700 lmo=1,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 1600 imo=1,lmo
      ii=orbidx(modrt(imo))
      isym=symm(modrt(imo))
      iidx=nndx(ii+1)
      lidx=nndx(max(ii,ll))+min(ii,ll)
c     zero all partial and averaged tden's (no averaged one..)
      call wzero(ncsfx2*navst_act,tden,1)
c
1000  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft),ism2,nwalk_f(ist),spindens)
c
      igo=min(icode,2)+1
      go to (1100,1400,1200),igo
c
1100  continue
c
c     new record.
c
      kntft=1
      nw=0
      read(ndft)ftbuf
      go to 1000
c
1200  continue
c
      call sdmatd(
     & symw,         nsv,        ism,       yb,
     & xbar(1,head), xp(1,tail), z(1,tail), r,
     & ind,          val,        icode,     hvect,
     & tden,         ncsf,       qind,      navst_act)
      go to 1000
c
1400  continue
c
c  change level.  process density contributions at the present level.
c
      if(imo.eq.lmo)go to 1500
c  i.ne.l case
c
        if (lcxs.and.qcx) then
c        llii=off1(max(lldx,iidx))+addaa(min(lldx,iidx))
         d2llii = 0.0d+00
           do ixx=1,navst_act
           d2llii = d2llii +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
c        lili=off1(lidx)+addaa(lidx)
         d2lili = 0.0d+00
           do ixx=1,navst_act
           d2lili = d2lili +
     $     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
         call ctwo(qc,ll,ll,ii,ii,lsym,lsym,isym,isym,d2llii,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cxsva)
         call ctwo(qc,ll,ii,ll,ii,lsym,isym,lsym,isym,d2lili,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cxsva)
        endif ! if (lcxs.and.qcx)
c
        if (lcxr.and.qcx) then
         call ctwo_cxv(qc,ll,ll,ii,ii,lsym,lsym,isym,isym,
     +    add,iorder,h2t1,h2t2,h2t5,
     +    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),1,c
     + xr,1)

c
         call ctwo_cxv(qc,ll,ii,ll,ii,lsym,isym,lsym,isym,
     +    add,iorder,h2t1,h2t2,h2t5,
     +    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),1,c
     + xr,1)

        endif ! if (lcxr.and.qcx)
c
      go to 1600
c
1500  continue
c  i.eq.l case
c
        if (lcxs.and.qcx) then
c        llll=off1(lldx)+addaa(lldx)
         d2llll = 0.0d+00
           do ixx=1,navst_act
           d2llll =d2llll +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
c        lt=ll-nsa(lsym)
c        llt=nnsa(lsym)+nndx(lt+1)
         d1llt = 0.0d+00
           do ixx=1,navst_act
           d1llt =d1llt +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
         call ctwo(qc,ll,ll,ll,ll,lsym,lsym,lsym,lsym,d2llll,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cxsva)
         call cone(qc,ll,ll,lsym,d1llt,1,add,iorder,
     +    uaa,uad,uva,h2t2,h2t3,h2t4,cxsaa,cxsad,cxsvd,cxsva)
         endif ! if (lcxs.and.qcx)
c
         if (lcxr.and.qcx) then
          call cone_cxv(qc,ll,ll,lsym,add,iorder,h2t2,h2t3,h2t4,
     &     uad,uaa,uva,rad,raa,rvd,rva,fockad,fockaa,fockvd,fockva)
          call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),1,
     + cxr,1)

          call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),1,
     + cxr,1)

          call daxpy_wr(ncsf_f(ist)*navst(ist),fockvd,tden(1,1,2),1,
     + cxr,1)

          call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),1,
     + cxr,1)

c
          call ctwo_cxv(qc,ll,ll,ll,ll,lsym,lsym,lsym,lsym,
     +     add,iorder,h2t1,h2t2,h2t5,
     +     rad,raa,rva,fockad,fockaa,fockva)
          call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),1,
     + cxr,1)

          call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),1,
     + cxr,1)

          call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),1,
     + cxr,1)

         endif ! if (lcxr.and.qcx)
c
c
1600  continue
1700  continue
c
c  off-diagonal loop contributions.
c
1750  rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 2740 lmo=2,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 2730 kmo=1,lmo
      kk=orbidx(modrt(kmo))
      ksym=symm(modrt(kmo))
      klsym=mult(lsym,ksym)
      lkdx=nndx(max(ll,kk))+min(ll,kk)
c
      do 2720 jmo=1,kmo
      jj=orbidx(modrt(jmo))
      jsym=symm(modrt(jmo))
      jklsym=mult(jsym,klsym)
      ljdx=nndx(max(ll,jj))+min(ll,jj)
      kjdx=nndx(max(kk,jj))+min(kk,jj)
c
      do 2710 imo=1,jmo
      if(imo.eq.kmo)go to 2710
      isym=symm(modrt(imo))
      if(isym.ne.jklsym)go to 2710
      ii=orbidx(modrt(imo))
      lidx=nndx(max(ll,ii))+min(ll,ii)
      kidx=nndx(max(kk,ii))+min(kk,ii)
      jidx=nndx(max(jj,ii))+min(jj,ii)
      iidx=nndx(ii+1)
      call wzero(ncsfx3*navst_act,tden,1)
      qt(1)=.false.
      qt(2)=.false.
      qt(3)=.false.
c
2100  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft), ism2, nwalk_f(ist),spindens )
c
      igo=min(icode,2)+1
      go to (2200,2500,2300),igo
2200  continue
c
c  new record
c
      kntft=1
      nw=0
      read(nft)ftbuf
      go to 2100
c
2300  continue
c
      call sdmat(symw,nsv,ism,yb,yk,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,tden,ncsf,qt,qind,navst_act)
      go to 2100
c
2500  continue
c
c  change level.  process density contributions.
c
      if(jmo.eq.lmo)go to 2600
c
      if (qcx.and.lcxs) then

       if(qt(1))then
c       lkji=off1(max(lkdx,jidx))+addaa(min(lkdx,jidx))
        d2lkji = 0.0d+00
          do ixx=1,navst_act
          d2lkji =d2lkji +
     $     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
          enddo ! do ixx=1,navst_act
       endif ! if(qt(1))then

c  allow for 11b contributions from diagonal loops.
       if(qt(2))then
c       ljki=off1(max(ljdx,kidx))+addaa(min(ljdx,kidx))
        d2ljki = 0.0d+00
          do ixx=1,navst_act
          d2ljki =d2ljki +
     $     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
          enddo ! do ixx=1,navst_act
       endif ! if(qt(2))then

       if(qt(3))then
c       likj=off1(max(lidx,kjdx))+addaa(min(lidx,kjdx))
        d2likj = 0.0d+00
          do ixx=1,navst_act
          d2likj =d2likj +
     $     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,3),1,svect(1,ixx),1)
          enddo ! do ixx=1,navst_act
       endif ! if(qt(3))then
c
       if(qt(1))
     +  call ctwo(qc,ll,kk,jj,ii,lsym,ksym,jsym,isym,d2lkji,
     +  1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  cxsaa,cxsad,cxsva)
       if(qt(2))
     +  call ctwo(qc,ll,jj,kk,ii,lsym,jsym,ksym,isym,d2ljki,
     +  1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  cxsaa,cxsad,cxsva)
       if(qt(3))
     +  call ctwo(qc,ll,ii,kk,jj,lsym,isym,ksym,jsym,d2likj,
     +  1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  cxsaa,cxsad,cxsva)
       endif ! if (qcx.and.lcxs)
c
       if (qcx.and.lcxr) then

        if(qt(1)) then
         call ctwo_cxv(qc,ll,kk,jj,ii,lsym,ksym,jsym,isym,
     &    add,iorder,h2t1,h2t2,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),
     &    1,cxr,1)
        endif ! if(qt(1))

        if(qt(2)) then
         call ctwo_cxv(qc,ll,jj,kk,ii,lsym,jsym,ksym,isym,
     &    add,iorder,h2t1,h2t2,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),
     &    1,cxr,1)
        endif ! if(qt(2))

        if(qt(3)) then
         call ctwo_cxv(qc,ll,ii,kk,jj,lsym,isym,ksym,jsym,
     &    add,iorder,h2t1,h2t2,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,3),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,3),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,3),
     &    1,cxr,1)
          endif ! if(qt(3))
       endif ! if (qcx.and.lcxr)
      go to 2710
c
2600  continue
c
c ll,ii,ii,ii; ll,ll,ll,ii; ll,ii  special case.
c
       if (qcx.and.lcxs) then
        if(qt(1))then
c        liii=off1(max(lidx,iidx))+addaa(min(lidx,iidx))
         d2liii = 0.0d+00
           do ixx=1,navst_act
           d2liii =d2liii  +
     +     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
        endif ! if(qt(1))

        if(qt(2))then
c        llli=off1(max(lldx,lidx))+addaa(min(lldx,lidx))
         d2llli = 0.0d+00
           do ixx=1,navst_act
           d2llli =d2llli +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
        endif ! if(qt(2))

5000    if(qt(3))then
         lt=ll-nsa(lsym)
         it=ii-nsa(lsym)
c        lit=nnsa(lsym)+nndx(max(lt,it))+min(lt,it)
         d1lit = 0.0d+00
           do ixx=1,navst_act
           d1lit =d1lit +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,3),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
        endif ! if(qt(3))
c
        if(qt(1))
     +   call ctwo(qc,ll,ii,ii,ii,lsym,isym,isym,isym,d2liii,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cxsva)
        if(qt(2))
     +   call ctwo(qc,ll,ll,ll,ii,lsym,lsym,lsym,isym,d2llli,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cxsva)
        if(qt(3))
     +   call cone(qc,ll,ii,lsym,d1lit,1,add,iorder,
     +    uaa,uad,uva,h2t2,h2t3,h2t4,cxsaa,cxsad,cxsvd,cxsva)
c
       endif ! if (qcx.and.lcxs)
c
       if (qcx.and.lcxr) then
        if(qt(1)) then
         call ctwo_cxv(qc,ll,ii,ii,ii,lsym,isym,isym,isym,
     &    add,iorder,h2t1,h2t2,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),1,c
     + xr,1)

        endif ! if(qt(1))

        if(qt(2)) then
         call ctwo_cxv(qc,ll,ll,ll,ii,lsym,lsym,lsym,isym,
     &    add,iorder,h2t1,h2t2,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),1,c
     + xr,1)

        endif ! if(qt(2))

        if(qt(3)) then
         call cone_cxv(qc,ll,ii,lsym,add,iorder,h2t2,h2t3,h2t4,
     &    uad,uaa,uva,rad,raa,rvd,rva,fockad,fockaa,fockvd,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,3),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,3),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockvd,tden(1,1,3),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,3),1,c
     + xr,1)

        endif ! if(qt(3))
      endif ! if (qcx.and.lcxr)
c
2710  continue
2720  continue
2730  continue
2740  continue
c
      close(unit=ndft)
      close(unit=nft)
c
      return
      end
c
c*****************************************************
c
      subroutine cone_cxv(qc,ii,jj,isym,add,iorder,
     + h2t2,h2t3,h2t4,uad,uaa,uva,rad,raa,rvd,rva,
     + fockad,fockaa,fockvd,fockva)
c
c  include the 1-particle contributions from tden(*) to the required
c  blocks of the c matrix.
c
c  input:
c  tden(*)   :transition density vector -- (1/2)<mc|eil+eli|n>.
c  u**(*)    :1-e h integral matrix element blocks.
c  h2t*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  c**(*)    :updated c matrix blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
c    # avepar.h
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c    ## drtf.h
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c    ## avstat.h
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst),
     & heig(maxnst,mxavst), navst(maxnst),nst,navst_max
c
c
      logical qc(4)
      real*8 rad(*),raa(*),rvd(*),rva(*),fockvd,fockad,fockaa,fockva
      real*8 h2t2(*),h2t3(*),h2t4(*),uad(*),uaa(*),uva(*)
      integer add(*),iorder(*)
c
      integer npass(2)
      data npass/2,1/
c
c
c  determine the number of permutaiion passes required to
c  process this vector of 1-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1        ij            2     1,2
c    2        ii            1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      npassp=npass(itype)
c
c  initialize ind(*) and inds.
c
      ind(1)=ii
      ind(2)=jj
      inds=isym
c
      fockaa = 0.0d+00
      if(qc(1))call c1aa_cxv(raa,uaa,iorder,fockaa)
c
      fockad = 0.0d+00
      if (qc(2)) call c1ad_cxv(rad,uad,h2t2,
     +  add(addpt(11)),add(addpt(3)),fockad)
c
      fockvd = 0.0d+00
      if (qc(3)) call c1vd_cxv(rvd,h2t3,h2t4,
     +    add(addpt(12)),add(addpt(13)),fockvd)
c
      fockva = 0.0d+00
      if (qc(4)) call c1va_cxv(rva,uva,add(addpt(6)),fockva)
c
      return
      end
c
c*****************************************************************
c
      subroutine c1aa_cxv(raa,uaa,iorder,fockaa)
c
c  include the one-particle density contributions to the
c     Cxv[cxs] = C[cxs,aa].r[aa]
c
c    following term is calculated:
c     sum(p) 4*uaa(pt)*raa(pq)
c     sum(q) 4*uaa(qt)*raa(pq)
c
c  input:
c  raa(*)    :orbital rotation parameters for the aa block
c  uaa(*)    :active-active blocks of the effective 1-e h matrix.
c  raa(*)    :orbital rotation parameters for the aa block
c  iorder(*) :mapping vector for allowed active-active rotations.
c
c  output:
c  fockaa    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 uaa(*),raa(*),fockaa
      integer iorder(*)
      real*8 four
      parameter(four=4d0)
c
      integer rangel(2,2),rangeh(2,2)
      logical skipl(2),skiph(2)
c
c  determine the do-loop ranges.
c
      do 10 i=1,2
      rangel(1,i)=ira1(inds)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(inds)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      noff=nsa(inds)
      nnoff=nnsa(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      t=ind(p2)
      rt=t-noff
c
c  include sum(p) 4*uaa(pt)*raa(pq) term.
c
      if(skiph(p1))go to 120
      q=ind(p1)
c
      rp=q-noff
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      rp=rp+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nnoff+nndx(max(rp,rt))+min(rp,rt)
      fockaa = fockaa + four*uaa(pt)*raa(ipq)
100   continue
120   continue
c
c  include sum(q) 4*uaa(qt)*raa(pq) term.
c
      if(skipl(p1))go to 220
      p=ind(p1)
c
      rq=0
      do 200 q=rangel(1,p1),rangel(2,p1)
      rq=rq+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nnoff+nndx(max(rq,rt))+min(rq,rt)
      fockaa = fockaa  -four*uaa(qt)*raa(ipq)
200   continue
220   continue
c
300   continue
c
      return
      end
c
c*****************************************************************
c
      subroutine c1ad_cxv(rad,uad,h2,off2,addad,fockad)
c
c  include the one-particle density contributions to the
c    Cxv[cfs] = C[csf,ad].r[ad]
c
c    following term is calculated:
c     sum(ip) 4*(2*(ip|uv)-(iu|pv))*rad(ip)
c     sum(i)  -4*uad(it)*rad(ip)
c
c  input:
c  uad(*)    :double-active blocks of the effective 1-e h matrix.
c  rad(*)    :orbital rotation parameters for the ad block
c  addad(*)  :addressing array for 1-e integrals.
c  off2(*)   :addressing array for 2-e integrals.
c
c  output:
c  fockad    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 rad(*),uad(*),h2(*),fockad
      integer off2(*),addad(ndimd,ndima)
c
      real*8 zero,two,four
      parameter(zero=0d0,two=2d0,four=4d0)
c
C  2-e integral terms are computed as:
c
c   fockad(uv) =  sum  4*( 4*(ip|uv) -(iu|pv) -(iv|pu) )*rad(ip)
c                 (u>v)
c
c                +sum  4*( 2*(ip|uu) - (iu|pu) )*rad(ip)
c                 (u)
c
      u=ind(1)
      v=ind(2)
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      ip=0
      ipuv=off2(nndx(max(u,v))+min(u,v))
      do 130 isym=1,nsym
      if(napsy(isym).eq.0)go to 130
      if(ndpsy(isym).eq.0)go to 130
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 120 p=p1,p2
      iupv=off2(nndx(max(p,v))+min(p,v))+addad(i1,u)
      ivpu=off2(nndx(max(p,u))+min(p,u))+addad(i1,v)
      do 110 i=i1,i2
      ip=ip+1
      ipuv=ipuv+1
      fockad = fockad + four*(four*h2(ipuv)-h2(iupv)-h2(ivpu)) * rad(ip)
      iupv=iupv+1
      ivpu=ivpu+1
110   continue
120   continue
130   continue
c
      else
c
c  u.eq.v terms.
c
      ip=0
      ipuu=off2(nndx(u)+u)
      do 230 isym=1,nsym
      if(napsy(isym).eq.0)go to 230
      if(ndpsy(isym).eq.0)go to 230
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 220 p=p1,p2
      iupu=off2(nndx(max(p,u))+min(p,u))+addad(i1,u)
      do 210 i=i1,i2
      ip=ip+1
      ipuu=ipuu+1
      fockad = fockad + four*(two*h2(ipuu)-h2(iupu)) * rad(ip)
      iupu=iupu+1
210   continue
220   continue
230   continue
c
      endif
c
c  include 1-e terms.
c
      ni=ndpsy(inds)
      if(ni.eq.0)return
      i1=ird1(inds)
c
      do 320 iperm=1,npassp
      p=ind(perm(1,iperm))
      t=ind(perm(2,iperm))
c
c  include -4*uad(it)*rad(ip) terms.
c
      ip=addad(i1,p)
      it=addad(i1,t)
c
      do 310 i=1,ni
      fockad = fockad  - four*uad(it) * rad(ip)
      ip=ip+1
      it=it+1
310   continue
c
320   continue
c
      return
      end
c
c*****************************************************
c
      subroutine c1vd_cxv(rvd,hd,hx,off3,off4,fockvd)
c
c  include the one-particle density contributions to the cvd(*) matrix.
c    Cxv[cxs] = C[cxs,vd].r[vd]
c
c     fockvd(uv) = sum -4*( 2*(ai|uv)-(au|iv))*rvd(ai)
c                  (ai)
c
c  input:
c  rvd(*)    :orbital rotation parameters for the vd block
c  hd(*)     :2-e integrals of the type (ai|uv).
c  hx(*)     :2-e integrals of the type (au|iv).
c  off3(*)   :addressing array for hd(*) integrals.
c  off4(*)   :addressing array for hx(*) integrals.
c
c  output:
c  fockvd    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxtot(18),ndima)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
      equivalence (ind(1),u),(ind(2),v)
c
      real*8 hd(*),hx(*),rvd(*),fockvd
      integer off3(*),off4(ndima,ndima)
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 zero,two,four
      parameter(zero=0d0,two=2d0,four=4d0)
c
c  the matrix is computed as:
c
c     fockvd(uv)= sum -4*( 4*(ai|uv) -(au|iv) -(av|iu) )*rvd(ai)
c                 (ai, for u>v)
c
c                +sum -4*( 2(ai|uu) - (au|iu) )*rvd(ai)
c                 (ai, for u=v)
c
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      uvd=off3(nndx(max(u,v))+min(u,v))
      uvx=off4(u,v)
      vux=off4(v,u)
      do 100 ai=1,nvdt
      fockvd =
     & fockvd -four*(four*hd(uvd+ai)-hx(uvx+ai)-hx(vux+ai))*rvd(ai)
100   continue
c
      else
c
c  u=v terms.
c
      uud=off3(nndx(u)+u)
      uux=off4(u,u)
      do 200 ai=1,nvdt
      fockvd = fockvd -four*(two*hd(uud+ai)-hx(uux+ai))*rvd(ai)
200   continue
c
      endif
      return
      end
c
c***************************************************************
c
      subroutine c1va_cxv(rva,uva,addva,fockva)
c
c  include the one-particle density contributions to the cva(*) matrix.
c
c     fockva(tp) = sum uva(at)*rvd(ap)
c                  (a)
c
c  input:
c  rva(*)    :orbital rotation parameters for the va block
c  uva(*)    :virtual-active blocks of the effective 1-e h matrix.
c  addva(*)  :addressing array.
c
c  output:
c  fockva    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      integer nsva(8)
      equivalence (nxy(1,31),nsva(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 uva(*),rva(*),fockva
      integer addva(ndimv,ndima)
c
      real*8 four
      parameter(four=4d0)
c
      na=nvpsy(inds)
      if(na.eq.0)return
      a1=irv1(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      p=ind(p1)
      t=ind(p2)
c
c  include sum(a) -4*uva(at)*rva(ap) terms.
c
      ap=addva(a1,p)
      at=addva(a1,t)
c
      do 200 a=1,na
      fockva = fockva -four*uva(at)*rva(ap)
      ap=ap+1
      at=at+1
200   continue
c
300   continue
c
      return
      end
c
c***********************************************************
c
      subroutine ctwo_cxv(qc,ii,jj,kk,ll,isym,jsym,ksym,lsym,
     +  add,iorder,h2t1,h2t2,h2t5,
     +  rad,raa,rva,fockad,fockaa,fockva)
c
c
c  input:
c  qc(*)     :logical array indicating the required c blocks.
c  rad       :orbital rotation parameters for the ad block
c  raa       :orbital rotation parameters for the aa block
c  rva       :orbital rotation parameters for the va block
c  iorder(*) :allowed active-active rotations.
c  h2i*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  fockad
c  fockaa
c  fockva
c
c
      implicit integer(a-z)
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      real*8 h2t1(*),h2t2(*),h2t5(*)
      real*8 rad(*),raa(*),rva(*),fockad,fockaa,fockva
      integer add(*),iorder(*)
      logical qc(4)
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      integer icol(7),itypex(8),npass(6),colpt(6)
c
      data itypex/1,2,3,4,5,0,0,6/
      data npass/4,3,3,2,2,1/
      data colpt/1,5,1,5,1,1/
      data icol/1,2,3,4,  1,3,4/
c
c  determine the number of permutation passes required to
c  process this vector of 2-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1       ijkl          4     1,2,3,4
c    2       iikl          3     1,3,4
c    3       ijkk          3     1,2,3
c    4       iikk          2     1,3
c    5       ijij          2     1,2
c    6       iiii          1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      if(kk.eq.ll)itype=itype+2
      ij=nndx(max(ii,jj))+min(ii,jj)
      kl=nndx(max(kk,ll))+min(kk,ll)
      if(ij.eq.kl)itype=itype+4
      itype=itypex(itype)
      npassp=npass(itype)
      cpt=colpt(itype)
c
c  initialize ind(*) and inds(*).
c
      ind(1)=ii
      ind(2)=jj
      ind(3)=kk
      ind(4)=ll
      inds(1)=isym
      inds(2)=jsym
      inds(3)=ksym
      inds(4)=lsym
c
      fockaa = 0.0d+00
      if(qc(1))call c2aa_cxv(icol(cpt),
     +  h2t1,add(addpt(10)),add(addpt(4)),iorder,raa,fockaa)
c
      fockad = 0.0d+00
      if(qc(2))call c2ad_cxv(icol(cpt),
     +  h2t2,add(addpt(11)),add(addpt(3)),rad,fockad)
c
      fockva = 0.0d+00
      if(qc(4))call c2va_cxv(icol(cpt),
     +  h2t5,add(addpt(14)),add(addpt(6)),rva,fockva)
c
      return
      end
c
c***********************************************************
c
      subroutine c2aa_cxv(icol,h2,off1,addaa,iorder,raa,fockaa)
c
c  include the two-particle density contributions to the
c       Cxv[cxs] = C[cxs,aa].r[aa]
c
c    following terms are calculated:
c     sum(p) 4*(pt|uv)*raa(pq)
c     sum(q) -4*(qt|uv)*raa(pq)
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),raa(*),fockaa
      integer icol(*),iorder(*),off1(*),addaa(*)
c
      integer rangel(2,4),rangeh(2,4)
      logical skipl(4),skiph(4)
      real*8 four,eight,dterm
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      rangel(1,i)=ira1(sym)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(sym)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include sum(p) 4*(pt|uv)*raa(pq) term.
c
      if(skiph(p1))go to 120
      q=ind(p1)
      t=ind(p2)
c
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nndx(max(p,t))+min(p,t)
      fockaa = fockaa +
     &  dterm*h2(off1(max(pt,uv))+addaa(min(pt,uv)))*raa(ipq)
100   continue
120   continue
c
c  include sum(q) -4*(qt|uv)*raa(pq) term.
c
      if(skipl(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      do 200 q=rangel(1,p1),rangel(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nndx(max(q,t))+min(q,t)
      fockaa = fockaa  -
     & dterm*h2(off1(max(qt,uv))+addaa(min(qt,uv)))*raa(ipq)
200   continue
220   continue
c
300   continue
c
      return
      end
c
c***********************************************************
c
      subroutine c2ad_cxv(icol,h2,off2,addad,rad,fockad)
c
c  include the two-particle density contributions to the
c     Cxv[cxs] = C[csf,ad].r[ad]
c
c    following term is calculated:
c     sum(i) -4*(it|uv)*rad(ip)
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),rad(*),fockad
      integer icol(*),off2(*),addad(ndimd,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=ird1(sym)
      range(2,i)=ird2(sym)
      skip(i)=ndpsy(sym).eq.0
10    continue
c
      do 300 ipt=1,npassp
      iperm=icol(ipt)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include sum(i) -4*(it|uv)*rad(ip) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      ituv=off2(uv)+addad(range(1,p1),t)
      ip=addad(range(1,p1),p)
      do 200 i=range(1,p1),range(2,p1)
      fockad = fockad - dterm*h2(ituv)*rad(ip)
      ip=ip+1
      ituv=ituv+1
200   continue
220   continue
c
300   continue
c
      return
      end
c
c***********************************************************
c
      subroutine c2va_cxv(icol,h2,off5,addva,rva,fockva)
c
c  include the two-particle density contributions to the
c     Cxv[cxs] = C[cxs,va].r[va]
c
c    following term is calculated:
c     sum(a) (at|uv)*rva(ap)
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),rva(*),fockva
      integer icol(*),off5(*),addva(ndimv,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=irv1(sym)
      range(2,i)=irv2(sym)
      skip(i)=nvpsy(sym).eq.0
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include sum(a) -4*(at|uv)*rva(ap) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      atuv=off5(uv)+addva(range(1,p1),t)
      ap=addva(range(1,p1),p)
      do 200 a=range(1,p1),range(2,p1)
      fockva = fockva -dterm*h2(atuv)*rva(ap)
      ap=ap+1
      atuv=atuv+1
200   continue
220   continue
c
300   continue
c
      return
      end
c
c****************************************************************
c
      subroutine prtvec(unit,title,vec,vdim)
       implicit none
      integer unit,fmt,vdim
      character*(*)title
      real*8 vec(vdim)
c
      write(unit,'(/,2x,3h***,2x,a,2x,3h***,2x,/)')title
      write(unit,'(10f12.8)')vec
c
      return
      end

      SUBROUTINE eigno(n,hlt,s,scrt,scrc,c,e,nlist)
c
c  compute eigenvectors and eigenvalues for the nonorthogonal eigenvalue
c  equation:
c                 hc = sce
c  input:
c  n = matrix dimensions.
c  hlt(*) = symmetric matrix to be diagonalized. lower-
c           triangular packed.
c  s(*) = symmetric overlap matrix. lower-triangular packed.
c  scrt(*,*),scrc(*,*) = scratch matrices.
c
c  output:
c  hlt(*) and s(*) are not modified.
c  scrt(*,*) = schmidt orthonormalization transformation matrix.
c  scrc(*,*) = eigenvectors in the schmidt basis.
c  c(*,*) = eigenvectors in the original nonorthonormal basis.
c  e(*) = eigenvalues.
c
        IMPLICIT NONE

c====>Begin Module EIGNO                  File eigno.f
c---->Makedcls Options: All variables
c
c     Argument variables
c
        INTEGER n,nlist
c
        REAL*8 c(n,n), e(n), hlt(*), s(*)
        REAL*8 scrc(n,n), scrt(n,n)
c
c     Local variables
c
        INTEGER ierr,i,ierr_invalid,ierr_small,info
        INTEGER lwork,liwork
        REAL*8 tfact
        REAL*8, allocatable:: WORK(:)
        INTEGER, allocatable:: IWORK(:)
c
c====>End Module   EIGNO                  File eigno.f

        ierr = 0
c
c     # compute the transformation matrix scrt(*,*)
c     # such that t(transpose)*s*t=1.
c

c     #check the eigenvalue spectrum of s (inexpensive)
c
        CALL expnds(n,s,scrc)
        lwork=1+5*n+3*n*n + 2*n*n
        liwork=2+6*n
        allocate(WORK(lwork),IWORK(liwork))
        CALL dsyevd_wr('V','U',n,scrc,n,e,work,lwork,iwork,liwork,info)
        if (info.ne.0) call bummer('dsyevd failed: info=',info,2)
        ierr_invalid=0
        ierr_small=0
        do i=1,n
          if (e(i).lt.-1.0d-8) then
             ierr_invalid=ierr_invalid+1
          elseif (e(i).lt.1.d-8) then
              ierr_small=ierr_small+1
          endif
        enddo
         if (ierr_invalid.gt.0) then
              CALL prvblk(scrc,e,n,n,n,0,0,'x:','v:','eig(s)',3,nlist)
              call bummer('invalid overlap matrix: # invalid'//
     .                ' eigenvalues=',ierr_invalid,2)
         endif
         if (ierr_small.gt.0) then 
              CALL prvblk(scrc,e,n,n,n,0,0,'x:','v:','eig(s)',3, nlist)
              call bummer('overlap matrix: # small eigenvalues=',
     .                    ierr_small,0)
         endif 
        CALL wzero(n*n,scrt,1)
        CALL wset(n,1.0d0,scrt,(n+1))
        CALL ortmgs(n,n,scrt,1,s,scrc,tfact,ierr)
        IF (ierr/=0) THEN
          CALL bummer('eigno: from ortmgs, ierr=',ierr,2)
        END IF

c
c     # expand hlt(*) to square form.
c
        CALL expnds(n,hlt,scrc)
c
c     # compute scrc = t(transpose)*h*t
c
        CALL mxm(scrc,n,scrt,n,c,n)
        CALL mtxm(scrt,n,c,n,scrc,n)
c
c     # solve the orthogonal eigenvalue equation: hx * cx = cx * e
c     # cx(*) is returned in scrc(*), c(*) is used for scratch.
c
        CALL dsyevd_wr('V','U',n,scrc,n,e,work,lwork,iwork,liwork,info)
        if (info.ne.0) call bummer('dsyevd failed: info=',info,2)
        deallocate(WORK,IWORK)
c
c     # compute c = t * cx
c
        CALL mxm(scrt,n,scrc,n,c,n)
c
        RETURN
      END 

