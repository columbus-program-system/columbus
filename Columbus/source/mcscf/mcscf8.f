!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf8.f
cmcscf part=8 of 10.  miscellaneous mcscf math routines.
cversion=5.6 last modified: 16-jan-2001
      subroutine dspv(n,a,d)
c
c  copy the diagonal elements from the symmetric, lower-
c  triangular-packed matrix a(*) to the vector d(*).
c
      implicit integer(a-z)
c
      real*8 a(*),d(*)
c
c  compute matrix address with expression instead of recursion to allow
c  vectorization. -rls
      do 10 i=1,n
         d(i)=a( (i*(i+1))/2 )
10    continue
      return
      end
      subroutine eigip(nr,n,a,d,e)
c
c  in-place computation of the eigenvectors and eigenvalues
c  of the matrix a(*).
c
c  input:
c  nr   = number of rows in a(*) (nr.ge.n).
c  n    = dimension of the matrix a(*) and vector d(*).
c  a(*) = matrix to be diagonalized
c  e(*) = scratch vector of length n.
c
c  output:
c  a(*) = eigenvectors.
c  d(*) = eigenvalues.
c
c  this version uses modified eispac routines to perform the
c  diagonalization.
c  only the lower-triangle of a(*) needs to be available on input.
c  eigenvectors are ordered: d(1) < d(2) < d(3) ... < d(n)
c
c  written by ron shepard.
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      real*8 a(*),d(*),e(*)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  check input.
c
      if(n.le.0 .or. nr.lt.n)then
         write(nlist,6010)'nr,n=',nr,n
         call bummer('eigip: argument error.',0,faterr)
      endif
c
c  diagonalize.
c
*@if defined ( cray) || defined ( craylib) || defined ( eispack)
*CC  note the nonstandard use of a(*) twice in the argument list.
*      call tred2(nr,n,a,d,e,a)
*@else
      call tred2i(nr,n,a,d,e)
*@endif
      call tql2(nr,n,d,e,a,ierr)
      if(ierr.ne.0)then
         write(nlist,6010)'ierr=',ierr
         call bummer('eigip: from tql2, ierr=',ierr,faterr)
      endif
c
      return
6010  format(/' *** error *** eigip: ',a,2i10)
      end
      subroutine maaxb(a,iac,iar,b,ibc,ibr,s,is,nar,nac,nbc)
c
c  calculate the matrix product a * b and overwrite the input
c  matrix a() with the result.
c  s() is used as a scratch vector and must contain  at least
c  nac elements.
c
c  input:
c  a   = input matrix a() with effective dimenstions a(nar,nac)
c  iac = spacing between elements in a column of a().
c  iar = spacing between elements in a row of a().
c  b   = input matrix b() with effective dimensions b(nac,nbc)
c  ibc = spacing between elements in a column of b().
c  ibr = spacing between elements in a row of b().
c  s   = scratch vector.
c  is  = spacing between elements of s().
c  nar = number of rows in a().
c  nac = number of columns in a().
c  nbc = number of columns in b().
c
c  output:
c  a   = overwritten with the result of a*b.  iac and iar are
c        used for column and row increments.
c        the effective dimensions are a(nar,nbc).
c
c  the matrix product y = x * y may also be computed using this
c  code by computing it as:
c         y(transpose) = y(transpose) * x(transpose)
c  for example, suppose x() and y() are dimensioned in the calling
c  program as x(n,m),y(m,*).  then the following call will compute
c  the product x*y and return the result in x().
c            call maaxb(x,1,n,y,1,m,...)
c  the following call will return the result in y().
c            call maaxb(y,m,1,x,n,1,...)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 a(*),b(*),s(*)
c
      p1i=1
      do 100 i=1,nar
c
c  copy i'th row of a() to s().
c
      call dcopy_wr(nac,a(p1i),iar,s,is)
c
c  compute s(transpose) * b and overwrite the i'th row of a().
c  for efficiency, the call may compute b(transpose) * s.
c
      call mxma(b,ibr,ibc, s,is,1, a(p1i),iar,1, nbc,nac,1)
c
100   p1i=p1i+iac
c
      return
      end
      subroutine orthos(na,nb,u,s,su)
c
c  use the modified schmidt procedure to produce vectors that
c  are orthonormal with respect to a metric matrix.
c
c  input:
c  na  =  dimension of s(*) and number of rows in u(*).
c  nb  =  number of vectors (columns) in u(*).
c  u(*)=  initial input vectors.
c  s(*)=  basis overlap matrix (metric) stored lower-triangular packed.
c
c  output:
c  s(*)=  unmodified metric matrix.
c  u(*)=  modified vectors such that u(transpose) * s * u = 1 (the
c         unit matrix of dimension nb).  the transformation matrix t,
c         where u(new)=u(old)*t, is upper triangular and is not stored
c         explicitly.
c
c  written by ron shepard.
c  version date: 28-mar-85
c
      implicit real*8 (a-h,o-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      real*8 u(na,nb),s(*),su(na)
c
      real*8 zero,one,term,tnorm
      parameter (zero=0d0,one=1d0)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  check input:
c
      if(nb.le.0  .or.  nb.gt.na)then
         write(nlist,6020)na,nb
         call bummer('orthos: argument error.',0,faterr)
      endif
c
c  sucessively normalize each vector, then subtract that normalized
c  component from each of the remaining updated vectors.
c  (note that the classical grahm-schmidt method subtracts the
c  components of the updated vector with each original vector.)
c
      do 1900 k=1,nb
      call wzero(na,su,1)
c
c  form the k'th column of s*u.
c
      call spmxv(s,u(1,k),su,na)
c
      term=ddot_wr(na,u(1,k),1,su,1)
c
      if(term.eq.zero)then
         write(nlist,6010)k
         call bummer('orthos: k=',k,faterr)
      endif
      tnorm=one/sqrt(term)
c
c  scale the k'th vector to unit norm.
c
      call dscal_wr(na,tnorm,u(1,k),1)
c
      if(k.eq.nb)go to 2000
c
c  calculate overlap of this k'th vector with each successive j'th
c  vector and subtract this component of the k'th vector from the
c  j'th vector.  the dot products are scaled by tnorm since u(*,k) was
c  scaled but not su(*).
c
      kp1=k+1
      do 1800 j=kp1,nb
      term=-tnorm*ddot_wr(na,u(1,j),1,su,1)
      if(term.ne.zero)call daxpy_wr(na,term,u(1,k),1,u(1,j),1)
1800  continue
c
1900  continue
c
2000  return
6010  format(/' ***error*** orthos: zero norm k=',i10)
6020  format(/' ***error*** orthos: input na,nb=',2i10)
      end
c
c*******************************************************************
c
      subroutine orthot(na,nb,u,routcode)
c
c  use the modified schmidt procedure to produce orthonormal vectors.
c
c  input:
c  na   = number of rows in u(*,*).
c  nb   = number of vectors (columns) in u(*,*).
c  u(*) = initial input vectors.
c  routcode = code to identify the calling routine
c           = 0 calling routine not known
c           = 1 calling routine: inciv
c
c  output:
c  u(*) = modified vectors such that u(transpose) * u = 1 (the
c         unit matrix of dimension nb).  the transformation matrix t,
c         where u(new) = u(old)*t, is upper triangular and is not
c         stored explicitly.
c
c  written by ron shepard.
c  version date: 28-mar-85
c
      implicit real*8(a-h,o-z)
c
c  ##  parameter & common block section
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8 zero,one
      parameter(zero=0d0,one=1d0)
c
      real*8 tol
      parameter (tol=1.0d-12)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist
      equivalence (iunits(1),nlist)
c
c  ##  integer section
c
      integer j
      integer k,kp1
      integer na,nb
      integer routcode
c
c  ##  real*8 section
c
      real*8 term
      real*8 u(na,nb)
c
      real*8 ddot_wr,dnrm2_wr
      external ddot_wr,dnrm2_wr
c
c  check input.
c
      if(nb.le.0 .or. nb.gt.na)then
         write(nlist,6010)na,nb
         call bummer('orthot: argument error',0,faterr)
      endif
c
c  successively normalize each vector, then subtract that
c  normalized component from the remaining updated vectors.
c  (note that classical grahm-schmidt subracts the components of
c  the updated vectors with each original vector.)
c
      do 200 k=1,nb
c
c  normalize u(*,k).
c
      term=dnrm2_wr(na,u(1,k),1)
      if(term.lt.tol)then
         if (routcode.eq.1) then
          write(nlist,
     &     '(//'' Symmetry problem in the HMC diagonaliz. procedure'')')
          write(nlist,*)
     &     '    suggested solutions:'
          write(nlist,
     &     '('' 1. Calculate the molecule in a higher symmetry.'')')
          write(nlist,
     &     '('' 2. If 1. not possible, use incore HMC diagonaliz.'')')
          write(nlist,
     &     '(''     procedure   (set in mcscfin: npath=-12).'')')
         endif
         write(nlist,6020)k
         call bummer('see mcscfls for details; orthot: k=',k,faterr)
      endif
      term=one/term
      call dscal_wr(na,term,u(1,k),1)
c
c  orthogonalize the remaining vectors to u(*,k).
c
      if(k.eq.nb)go to 200
      kp1=k+1
      do 100 j=kp1,nb
      term=-ddot_wr(na,u(1,k),1,u(1,j),1)
      if(term.ne.zero)call daxpy_wr(na,term,u(1,k),1,u(1,j),1)
100   continue
c
200   continue
c
      return
6010  format(/' *** error *** orthot: na,nb=',2i10)
6020  format(/' *** error *** orthot: zero norm k=',i10)
      end
c
c*************************************************************
c
      subroutine uthu(u,h,ht,hu,na,nb)
c
c  form the matrix product ht=u(transpose)*h*u.
c
c  input:
c  u(*,*)= transformation matrix.
c  h(*)  = initial matrix stored lower-triangular packed.
c  hu(*) = scratch vector of length na.
c  na    = dimension of h(*) and number of rows in u(*,*).
c  nb    = dimension of ht(*) and number of columns in u(*,*).
c
c  output:
c  u(*) and h(*) are unmodified.
c  ht(*) = transformed matrix stored lower-triangular packed.
c
c  written by ron shepard.
c
      implicit real*8 (a-h,o-z)
      real*8 h(*),ht(*),u(na,nb),hu(na)
c
      ij=0
      do 400 j=1,nb
c
c  form hu(*,j).
c
      call spmxv(h,u(1,j),hu,na)
c
c  include hu(*,j) contributions to ht(*,j).
c
      do 300 i=1,j
      ij=ij+1
300   ht(ij)=ddot_wr(na,u(1,i),1,hu,1)
c
400   continue
      return
      end
      subroutine xpnd1(h,hx,npsy,nsym)
c
c  expand the lower-triangular-packed array h(*) into square-packed
c  symmetric integral array hx(*) by symmetry blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 h(*),hx(*)
      integer npsy(nsym)
c
      n2=1
      p0=0
      do 1300 psym=1,nsym
      np=npsy(psym)
      if(np.eq.0)go to 1300
      ip1=n2
      i0p=n2-1
      do 1200 p=1,np
      ipt=ip1
cdir$ ivdep
cvocl loop,novrec
      do 1100 t=1,p
      hx(i0p+t)=h(p0+t)
      hx(ipt)=h(p0+t)
      ipt=ipt+np
1100  continue
      p0=p0+p
      i0p=i0p+np
      ip1=ip1+1
1200  continue
      n2=n2+np*np
1300  continue
      return
      end
c deck tred2
*@if defined (cray) || defined ( craylib) || defined ( eispack) || defined ( t3d )
*CC  use the library routine.
*@else
      subroutine tred2(nm,n,a,d,e,z)
c
      implicit logical(a-z)
c
      integer i,j,k,l,n,ii,nm,jp1
      real*8 a(nm,n),d(n),e(n),z(nm,n)
      real*8 f,g,h,hh,scale
c*** not fortran-77 ***       real dsqrt,dabs,dsign
c
c     this subroutine is a translation of the algol procedure tred2,
c     num. math. 11, 181-195(1968) by martin, reinsch, and wilkinson.
c     handbook for auto. comp., vol.ii-linear algebra, 212-226(1971).
c
c     this subroutine reduces a real symmetric matrix to a
c     symmetric tridiagonal matrix using and accumulating
c     orthogonal similarity transformations.
c
c     on input:
c
c        nm must be set to the row dimension of two-dimensional
c          array parameters as declared in the calling program
c          dimension statement;
c
c        n is the order of the matrix;
c
c        a contains the real symmetric input matrix.  only the
c          lower triangle of the matrix need be supplied.
c
c     on output:
c
c        d contains the diagonal elements of the tridiagonal matrix;
c
c        e contains the subdiagonal elements of the tridiagonal
c          matrix in its last n-1 positions.  e(1) is set to zero;
c
c        z contains the orthogonal transformation matrix
c          produced in the reduction;
c
c        a and z may coincide.  if distinct, a is unaltered.
c
c     questions and comments should be directed to b. s. garbow,
c     applied mathematics division, argonne national laboratory
c
c     -----------------------------------------------------------------
c
      do 100 i = 1, n
c
          do 100 j = 1, i
              z(i,j) = a(i,j)
  100 continue
c
      if (n .eq. 1) go to 320
c     :::::::::: for i=n step -1 until 2 do -- ::::::::::
      do 300 ii = 2, n
          i = n + 2 - ii
          l = i - 1
          h = 0.0d0
          scale = 0.0d0
          if (l .lt. 2) go to 130
c     :::::::::: scale row (algol tol then not needed) ::::::::::
          do 120 k = 1, l
  120     scale = scale + abs(z(i,k))
c
          if (scale .ne. 0.0d0) go to 140
  130     e(i) = z(i,l)
          go to 290
c
  140     do 150 k = 1, l
              z(i,k) = z(i,k) / scale
              h = h + z(i,k) * z(i,k)
  150     continue
c
          f = z(i,l)
          g = -sign(sqrt(h),f)
          e(i) = scale * g
          h = h - f * g
          z(i,l) = f - g
          f = 0.0d0
c
          do 240 j = 1, l
              z(j,i) = z(i,j) / h
              g = 0.0d0
c     :::::::::: form element of a*u ::::::::::
              do 180 k = 1, j
  180         g = g + z(j,k) * z(i,k)
c
              jp1 = j + 1
              if (l .lt. jp1) go to 220
c
              do 200 k = jp1, l
  200         g = g + z(k,j) * z(i,k)
c     :::::::::: form element of p ::::::::::
  220         e(j) = g / h
              f = f + e(j) * z(i,j)
  240     continue
c
          hh = f / (h + h)
c     :::::::::: form reduced a ::::::::::
          do 260 j = 1, l
              f = z(i,j)
              g = e(j) - hh * f
              e(j) = g
c
              do 260 k = 1, j
                  z(j,k) = z(j,k) - f * e(k) - g * z(i,k)
  260     continue
c
  290     d(i) = h
  300 continue
c
  320 d(1) = 0.0d0
      e(1) = 0.0d0
c     :::::::::: accumulation of transformation matrices ::::::::::
      do 500 i = 1, n
          l = i - 1
          if (d(i) .eq. 0.0d0) go to 380
c
          do 360 j = 1, l
              g = 0.0d0
c
              do 340 k = 1, l
  340         g = g + z(i,k) * z(k,j)
c
              do 360 k = 1, l
                  z(k,j) = z(k,j) - g * z(k,i)
  360     continue
c
  380     d(i) = z(i,i)
          z(i,i) = 1.0d0
          if (l .lt. 1) go to 500
c
          do 400 j = 1, l
              z(i,j) = 0.0d0
              z(j,i) = 0.0d0
  400     continue
c
  500 continue
c
      return
c     :::::::::: last card of tred2 ::::::::::
      end
*@endif
c deck tql2
*@ifdef cray
*CC  use library routine.
*@elif defined  oldvax
*      subroutine tql2(nm,n,d,e,z,ierr)
*CC
*      integer          i,           ierr,        ii,          j
*      integer          k,           l,           l1,          m
*      integer          mml,         n,           nm
*      real*8 b,           c,           d(n),        d1mach
*      real*8 abs,         sign,        sqrt,        e(n)
*      real*8 f,           g,           h,           machep
*      real*8 p,           r,           s,           z(nm,n)
*CC
*CC     this subroutine is a translation of the algol procedure tql2,
*CC     num. math. 11, 293-306(1968) by bowdler, martin, reinsch, and
*CC     wilkinson.
*CC     handbook for auto. comp., vol.ii-linear algebra, 227-240(1971).
*CC
*CC     this subroutine finds the eigenvalues and eigenvectors
*CC     of a symmetric tridiagonal matrix by the ql method.
*CC     the eigenvectors of a full symmetric matrix can also
*CC     be found if  tred2  has been used to reduce this
*CC     full matrix to tridiagonal form.
*CC
*CC     on input
*CC
*CC        nm must be set to the row dimension of two-dimensional
*CC          array parameters as declared in the calling program
*CC          dimension statement;
*CC
*CC        n is the order of the matrix;
*CC
*CC        d contains the diagonal elements of the input matrix;
*CC
*CC        e contains the subdiagonal elements of the input matrix
*CC          in its last n-1 positions.  e(1) is arbitrary;
*CC
*CC        z contains the transformation matrix produced in the
*CC          reduction by  tred2, if performed.  if the eigenvectors
*CC          of the tridiagonal matrix are desired, z must contain
*CC          the identity matrix.
*CC
*CC      on output
*CC
*CC        d contains the eigenvalues in ascending order.  if an
*CC          error exit is made, the eigenvalues are correct but
*CC          unordered for indices 1,2,...,ierr-1;
*CC
*CC        e has been destroyed;
*CC
*CC        z contains orthonormal eigenvectors of the symmetric
*CC          tridiagonal (or full) matrix.  if an error exit is made,
*CC          z contains the eigenvectors associated with the stored
*CC          eigenvalues;
*CC
*CC        ierr is set to
*CC          zero       for normal return,
*CC          j          if the j-th eigenvalue has not been
*CC                     determined after 30 iterations.
*CC
*CC     questions and comments should be directed to b. s. garbow,
*CC     applied mathematics division, argonne national laboratory
*CC
*CC     -----------------------------------------------------------------
*CC
*CC                machep is a machine dependent parameter specifying
*CC                the relative precision of floating point arithmetic.
*CC                machep = 16.0d0**(-13) for long form arithmetic
*CC                on s360
*CC     data machep/z3410000000000000/
*      data machep/1.d-14/
*CC
*      ierr = 0
*      if (n .eq. 1) go to 1001
*CC
*      do 100 i = 2, n
*  100 e(i-1) = e(i)
*CC
*      f = 0.0d0
*      b = 0.0d0
*      e(n) = 0.0d0
*CC
*      do 240 l = 1, n
*         j = 0
*         h = machep * (abs(d(l)) + abs(e(l)))
*         if (b .lt. h) b = h
*CC                look for small sub-diagonal element
*         do 110 m = l, n
*            if (abs(e(m)) .le. b) go to 120
*CC                e(n) is always zero, so there is no exit
*CC                through the bottom of the loop
*  110    continue
*CC
*  120    if (m .eq. l) go to 220
*  130    if (j .eq. 30) go to 1000
*         j = j + 1
*CC                form shift
*         l1 = l + 1
*         g = d(l)
*         p = (d(l1) - g) / (2.0d0 * e(l))
*CC
*CC  changed for limited vax exponent range. 5/26/82 (rls)
*CC  cdabs(*)  works correctly even for very large or small p
*CC-vax         r = sqrt(p*p+1.0d0)
*CC+vax         r=cdabs(dcmplx(p,1.d0))
*         r=cdabs(dcmplx(p,1.d0))
*         d(l) = e(l) / (p + sign(r,p))
*         h = g - d(l)
*CC
*         do 140 i = l1, n
*  140    d(i) = d(i) - h
*CC
*         f = f + h
*CC                ql transformation
*         p = d(m)
*         c = 1.0d0
*         s = 0.0d0
*         mml = m - l
*CC                for i=m-1 step -1 until l do --
*         do 200 ii = 1, mml
*            i = m - ii
*            g = c * e(i)
*            h = c * p
*            if (abs(p) .lt. abs(e(i))) go to 150
*            c = e(i) / p
*            r = sqrt(c*c+1.0d0)
*            e(i+1) = s * p * r
*            s = c / r
*            c = 1.0d0 / r
*            go to 160
*  150       c = p / e(i)
*            r = sqrt(c*c+1.0d0)
*            e(i+1) = s * e(i) * r
*            s = 1.0d0 / r
*            c = c * s
*  160       p = c * d(i) - s * g
*            d(i+1) = h + s * (c * g + s * d(i))
*CC                form vector
*            do 180 k = 1, n
*               h = z(k,i+1)
*               z(k,i+1) = s * z(k,i) + c * h
*               z(k,i) = c * z(k,i) - s * h
*  180       continue
*CC
*  200    continue
*CC
*         e(l) = s * p
*         d(l) = c * p
*         if (abs(e(l)) .gt. b) go to 130
*  220    d(l) = d(l) + f
*  240 continue
*CC                order eigenvalues and eigenvectors
*      do 300 ii = 2, n
*         i = ii - 1
*         k = i
*         p = d(i)
*CC
*         do 260 j = ii, n
*            if (d(j) .ge. p) go to 260
*            k = j
*            p = d(j)
*  260    continue
*CC
*         if (k .eq. i) go to 300
*         d(k) = d(i)
*         d(i) = p
*CC
*         do 280 j = 1, n
*            p = z(j,i)
*            z(j,i) = z(j,k)
*            z(j,k) = p
*  280    continue
*CC
*  300 continue
*CC
*      go to 1001
*CC                set error -- no convergence to an
*CC                eigenvalue after 30 iterations
* 1000 ierr = l
* 1001 return
*CC                last card of tql2
*      end
*@elif defined  reference
*      subroutine tql2(nm,n,d,e,z,ierr)
*CC
*      integer          i,           ierr,        ii,          j
*      integer          k,           l,           l1,          m
*      integer          mml,         n,           nm
*      real*8 b,           c,           d(n),        d1mach
*      real*8 abs,         sign,        sqrt,        e(n)
*      real*8 f,           g,           h,           machep
*      real*8 p,           r,           s,           z(nm,n)
*CC
*CC     this subroutine is a translation of the algol procedure tql2,
*CC     num. math. 11, 293-306(1968) by bowdler, martin, reinsch, and
*CC     wilkinson.
*CC     handbook for auto. comp., vol.ii-linear algebra, 227-240(1971).
*CC
*CC     this subroutine finds the eigenvalues and eigenvectors
*CC     of a symmetric tridiagonal matrix by the ql method.
*CC     the eigenvectors of a full symmetric matrix can also
*CC     be found if  tred2  has been used to reduce this
*CC     full matrix to tridiagonal form.
*CC
*CC     on input
*CC
*CC        nm must be set to the row dimension of two-dimensional
*CC          array parameters as declared in the calling program
*CC          dimension statement;
*CC
*CC        n is the order of the matrix;
*CC
*CC        d contains the diagonal elements of the input matrix;
*CC
*CC        e contains the subdiagonal elements of the input matrix
*CC          in its last n-1 positions.  e(1) is arbitrary;
*CC
*CC        z contains the transformation matrix produced in the
*CC          reduction by  tred2, if performed.  if the eigenvectors
*CC          of the tridiagonal matrix are desired, z must contain
*CC          the identity matrix.
*CC
*CC      on output
*CC
*CC        d contains the eigenvalues in ascending order.  if an
*CC          error exit is made, the eigenvalues are correct but
*CC          unordered for indices 1,2,...,ierr-1;
*CC
*CC        e has been destroyed;
*CC
*CC        z contains orthonormal eigenvectors of the symmetric
*CC          tridiagonal (or full) matrix.  if an error exit is made,
*CC          z contains the eigenvectors associated with the stored
*CC          eigenvalues;
*CC
*CC        ierr is set to
*CC          zero       for normal return,
*CC          j          if the j-th eigenvalue has not been
*CC                     determined after 30 iterations.
*CC
*CC     questions and comments should be directed to b. s. garbow,
*CC     applied mathematics division, argonne national laboratory
*CC
*CC     -----------------------------------------------------------------
*CC
*CC                machep is a machine dependent parameter specifying
*CC                the relative precision of floating point arithmetic.
*CC                machep = 16.0d0**(-13) for long form arithmetic
*CC                on s360
*CC     data machep/z3410000000000000/
*      data machep/1.d-14/
*CC
*      ierr = 0
*      if (n .eq. 1) go to 1001
*CC
*      do 100 i = 2, n
*  100 e(i-1) = e(i)
*CC
*      f = 0.0d0
*      b = 0.0d0
*      e(n) = 0.0d0
*CC
*      do 240 l = 1, n
*         j = 0
*         h = machep * (abs(d(l)) + abs(e(l)))
*         if (b .lt. h) b = h
*CC                look for small sub-diagonal element
*         do 110 m = l, n
*            if (abs(e(m)) .le. b) go to 120
*CC                e(n) is always zero, so there is no exit
*CC                through the bottom of the loop
*  110    continue
*CC
*  120    if (m .eq. l) go to 220
*  130    if (j .eq. 30) go to 1000
*         j = j + 1
*CC                form shift
*         l1 = l + 1
*         g = d(l)
*         p = (d(l1) - g) / (2.0d0 * e(l))
*CC
*CC  changed for limited vax exponent range. 5/26/82 (rls)
*CC  cdabs(*)  works correctly even for very large or small p
*CC-vax         r = sqrt(p*p+1.0d0)
*CC+vax         r=cdabs(dcmplx(p,1.d0))
*         r = sqrt(p*p+1.0d0)
*         d(l) = e(l) / (p + sign(r,p))
*         h = g - d(l)
*CC
*         do 140 i = l1, n
*  140    d(i) = d(i) - h
*CC
*         f = f + h
*CC                ql transformation
*         p = d(m)
*         c = 1.0d0
*         s = 0.0d0
*         mml = m - l
*CC                for i=m-1 step -1 until l do --
*         do 200 ii = 1, mml
*            i = m - ii
*            g = c * e(i)
*            h = c * p
*            if (abs(p) .lt. abs(e(i))) go to 150
*            c = e(i) / p
*            r = sqrt(c*c+1.0d0)
*            e(i+1) = s * p * r
*            s = c / r
*            c = 1.0d0 / r
*            go to 160
*  150       c = p / e(i)
*            r = sqrt(c*c+1.0d0)
*            e(i+1) = s * e(i) * r
*            s = 1.0d0 / r
*            c = c * s
*  160       p = c * d(i) - s * g
*            d(i+1) = h + s * (c * g + s * d(i))
*CC                form vector
*            do 180 k = 1, n
*               h = z(k,i+1)
*               z(k,i+1) = s * z(k,i) + c * h
*               z(k,i) = c * z(k,i) - s * h
*  180       continue
*CC
*  200    continue
*CC
*         e(l) = s * p
*         d(l) = c * p
*         if (abs(e(l)) .gt. b) go to 130
*  220    d(l) = d(l) + f
*  240 continue
*CC                order eigenvalues and eigenvectors
*      do 300 ii = 2, n
*         i = ii - 1
*         k = i
*         p = d(i)
*CC
*         do 260 j = ii, n
*            if (d(j) .ge. p) go to 260
*            k = j
*            p = d(j)
*  260    continue
*CC
*         if (k .eq. i) go to 300
*         d(k) = d(i)
*         d(i) = p
*CC
*         do 280 j = 1, n
*            p = z(j,i)
*            z(j,i) = z(j,k)
*            z(j,k) = p
*  280    continue
*CC
*  300 continue
*CC
*      go to 1001
*CC                set error -- no convergence to an
*CC                eigenvalue after 30 iterations
* 1000 ierr = l
* 1001 return
*CC                last card of tql2
*      end
*@else
c  another attempt at making tql2() machine-independent.
c  see comments below for details. -rls
      subroutine tql2(nm,n,d,e,z,ierr)
c
      implicit logical(a-z)
c
      integer          i,           ierr,        ii,          j
      integer          k,           l,           l1,          m
      integer          mml,         n,           nm
      real*8 b,           c,           d(n),        d1mach
      real*8 abs,         sign,        sqrt,        e(n)
      real*8 f,           g,           h,           machep
      real*8 p,           r,           s,           z(nm,n)
c
      real*8     zero,     one,     two
      parameter( zero=0d0, one=1d0, two=2d0 )
c
c     this subroutine is a translation of the algol procedure tql2,
c     num. math. 11, 293-306(1968) by bowdler, martin, reinsch, and
c     wilkinson.
c     handbook for auto. comp., vol.ii-linear algebra, 227-240(1971).
c
c     30-aug-90 more universal change to avoid vax overflow. -rls
c     26-may-82 vax-specific change to avoid overflow.  this was
c               originally done approximately april 1980 while at
c               batelle columbus laboratory. -ron shepard
c
c     this subroutine finds the eigenvalues and eigenvectors
c     of a symmetric tridiagonal matrix by the ql method.
c     the eigenvectors of a full symmetric matrix can also
c     be found if  tred2  has been used to reduce this
c     full matrix to tridiagonal form.
c
c     on input
c
c        nm must be set to the row dimension of two-dimensional
c          array parameters as declared in the calling program
c          dimension statement;
c
c        n is the order of the matrix;
c
c        d contains the diagonal elements of the input matrix;
c
c        e contains the subdiagonal elements of the input matrix
c          in its last n-1 positions.  e(1) is arbitrary;
c
c        z contains the transformation matrix produced in the
c          reduction by  tred2, if performed.  if the eigenvectors
c          of the tridiagonal matrix are desired, z must contain
c          the identity matrix.
c
c      on output
c
c        d contains the eigenvalues in ascending order.  if an
c          error exit is made, the eigenvalues are correct but
c          unordered for indices 1,2,...,ierr-1;
c
c        e has been destroyed;
c
c        z contains orthonormal eigenvectors of the symmetric
c          tridiagonal (or full) matrix.  if an error exit is made,
c          z contains the eigenvectors associated with the stored
c          eigenvalues;
c
c        ierr is set to
c          zero       for normal return,
c          j          if the j-th eigenvalue has not been
c                     determined after 30 iterations.
c
c     questions and comments should be directed to b. s. garbow,
c     applied mathematics division, argonne national laboratory
c
c     -----------------------------------------------------------------
c
c                machep is a machine dependent parameter specifying
c                the relative precision of floating point arithmetic.
c                machep = 16.0d0**(-13) for long form arithmetic
c                on s360
c     data machep/z3410000000000000/
c
      data machep/1.d-14/
c
      ierr = 0
      if (n .eq. 1) go to 1001
c
      do 100 i = 2, n
  100 e(i-1) = e(i)
c
      f = zero
      b = zero
      e(n) = zero
c
      do 240 l = 1, n
         j = 0
         h = machep * (abs(d(l)) + abs(e(l)))
         if (b .lt. h) b = h
c                look for small sub-diagonal element
         do 110 m = l, n
            if (abs(e(m)) .le. b) go to 120
c                e(n) is always zero, so there is no exit
c                through the bottom of the loop
  110    continue
c
  120    if (m .eq. l) go to 220
  130    if (j .eq. 30) go to 1000
         j = j + 1
c                form shift
         l1 = l + 1
         g = d(l)
         p = (d(l1) - g) / (two * e(l))
c
c  changed for limited vax exponent range. 5/26/82 (rls)
c  cdabs(*)  works correctly even for very large or small p
c-vax         r = sqrt(p*p+1.0d0)
c+vax         r=cdabs(dcmplx(p,1.d0))
c  changed again 30-aug-90 in a more universal way.  this code should
c  work correctly on any machine.  two sqrt()s are required instead of
c  one, but this statement is executed only o(n) times. -ron shepard
c
         if( p .eq. zero )then
            r = one
         else
            r = abs(p)
            r = sqrt(r) * ( sqrt(r + one/r) )
         endif
c
         d(l) = e(l) / (p + sign(r,p))
         h = g - d(l)
c
         do 140 i = l1, n
  140    d(i) = d(i) - h
c
         f = f + h
c                ql transformation
         p = d(m)
         c = one
         s = zero
         mml = m - l
c                for i=m-1 step -1 until l do --
         do 200 ii = 1, mml
            i = m - ii
            g = c * e(i)
            h = c * p
            if (abs(p) .lt. abs(e(i))) go to 150
            c = e(i) / p
            r = sqrt(c*c+one)
            e(i+1) = s * p * r
            s = c / r
            c = one / r
            go to 160
  150       c = p / e(i)
            r = sqrt(c*c+one)
            e(i+1) = s * e(i) * r
            s = one / r
            c = c * s
  160       p = c * d(i) - s * g
            d(i+1) = h + s * (c * g + s * d(i))
c                form vector
            do 180 k = 1, n
               h = z(k,i+1)
               z(k,i+1) = s * z(k,i) + c * h
               z(k,i) = c * z(k,i) - s * h
  180       continue
c
  200    continue
c
         e(l) = s * p
         d(l) = c * p
         if (abs(e(l)) .gt. b) go to 130
  220    d(l) = d(l) + f
  240 continue
c                order eigenvalues and eigenvectors
      do 300 ii = 2, n
         i = ii - 1
         k = i
         p = d(i)
c
         do 260 j = ii, n
            if (d(j) .ge. p) go to 260
            k = j
            p = d(j)
  260    continue
c
         if (k .eq. i) go to 300
         d(k) = d(i)
         d(i) = p
c
         do 280 j = 1, n
            p = z(j,i)
            z(j,i) = z(j,k)
            z(j,k) = p
  280    continue
c
  300 continue
c
      go to 1001
c                set error -- no convergence to an
c                eigenvalue after 30 iterations
 1000 ierr = l
 1001 return
c                last card of tql2
      end
*@endif
      subroutine tred2i(nm,n,z,d,e)
c
      integer i,j,k,l,n,ii,nm,jp1
      real*8 d(n),e(n),z(nm,n)
      real*8 f,g,h,hh,scale
c      real*8 sqrt,abs,sign
c
c     this subroutine is a translation of the algol procedure tred2,
c     num. math. 11, 181-195(1968) by martin, reinsch, and wilkinson.
c     handbook for auto. comp., vol.ii-linear algebra, 212-226(1971).
c
c     modified to perform the diagonalization in-place by removing
c     the references to a(*) in the original version of tred2.
c     the input is simplified accordingly.
c     19-apr-85 ron shepard.
c
c     this subroutine reduces a real symmetric matrix to a
c     symmetric tridiagonal matrix using and accumulating
c     orthogonal similarity transformations.
c
c     on input:
c
c        nm must be set to the row dimension of two-dimensional
c          array parameters as declared in the calling program
c          dimension statement;
c
c        n is the order of the matrix;
c
c        z contains the real symmetric input matrix.  only the
c          lower triangle of the matrix need be supplied.
c
c     on output:
c
c        d contains the diagonal elements of the tridiagonal matrix;
c
c        e contains the subdiagonal elements of the tridiagonal
c          matrix in its last n-1 positions.  e(1) is set to zero;
c
c        z contains the orthogonal transformation matrix
c          produced in the reduction;
c
c     questions and comments should be directed to b. s. garbow,
c     applied mathematics division, argonne national laboratory
c
c     -----------------------------------------------------------------
c
      if (n .eq. 1) go to 320
c     :::::::::: for i=n step -1 until 2 do -- ::::::::::
      do 300 ii = 2, n
         i = n + 2 - ii
         l = i - 1
         h = 0.0d0
         scale = 0.0d0
         if (l .lt. 2) go to 130
c     :::::::::: scale row (algol tol then not needed) ::::::::::
         do 120 k = 1, l
  120    scale = scale + abs(z(i,k))
c
         if (scale .ne. 0.0d0) go to 140
  130    e(i) = z(i,l)
         go to 290
c
  140    do 150 k = 1, l
            z(i,k) = z(i,k) / scale
            h = h + z(i,k) * z(i,k)
  150    continue
c
         f = z(i,l)
         g = -sign(sqrt(h),f)
         e(i) = scale * g
         h = h - f * g
         z(i,l) = f - g
         f = 0.0d0
c
         do 240 j = 1, l
            z(j,i) = z(i,j) / h
            g = 0.0d0
c     :::::::::: form element of a*u ::::::::::
            do 180 k = 1, j
  180       g = g + z(j,k) * z(i,k)
c
            jp1 = j + 1
            if (l .lt. jp1) go to 220
c
            do 200 k = jp1, l
  200       g = g + z(k,j) * z(i,k)
c     :::::::::: form element of p ::::::::::
  220       e(j) = g / h
            f = f + e(j) * z(i,j)
  240    continue
c
         hh = f / (h + h)
c     :::::::::: form reduced a ::::::::::
         do 260 j = 1, l
            f = z(i,j)
            g = e(j) - hh * f
            e(j) = g
c
            do 260 k = 1, j
               z(j,k) = z(j,k) - f * e(k) - g * z(i,k)
  260    continue
c
  290    d(i) = h
  300 continue
c
  320 d(1) = 0.0d0
      e(1) = 0.0d0
c     :::::::::: accumulation of transformation matrices ::::::::::
      do 500 i = 1, n
         l = i - 1
         if (d(i) .eq. 0.0d0) go to 380
c
         do 360 j = 1, l
            g = 0.0d0
c
            do 340 k = 1, l
  340       g = g + z(i,k) * z(k,j)
c
            do 360 k = 1, l
               z(k,j) = z(k,j) - g * z(k,i)
  360    continue
c
  380    d(i) = z(i,i)
         z(i,i) = 1.0d0
         if (l .lt. 1) go to 500
c
         do 400 j = 1, l
            z(i,j) = 0.0d0
            z(j,i) = 0.0d0
  400    continue
c
  500 continue
c
      return
      end
      subroutine givens (nx,nrootx,njx,a,b,root,vect,nvec)
      implicit real*8 (a-h,o-z)
c
c      qcpe program 62.3
c      eigenvalues and eigenvectors by the givens method.
c      by franklin prosser, indiana university.
c      september, 1967
c
c      *** argument nvec added may-1978 by ron shepard ***
c      *** parameters and other minor cleanup 11-july-88 ron shepard ***
c
c      calculates eigenvalues and eigenvectors of real symmetric matrix
c      stored in packed upper triangular form.
c
c      thanks are due to f. e. harris (stanford university) and h. h.
c      michels (united aircraft research laboratories) for excellent
c      work on numerical difficulties with earlier versions of this
c      program.
c
c      the arguments for the routine are...
c          nx     order of matrix
c          nrootx number of roots wanted.  the nrootx smallest (most
c                  negative) roots will be calculated.  if no vectors
c                  are wanted, make this number negative.
c          njx    row dimension of vect array.  see  vect  below.
c                  njx must be not less than nx.
c          a      matrix stored by columns in packed upper triangular
c                 form, i.e. occupying nx*(nx+1)/2 consecutive
c                 locations.
c          b      scratch array used by givens.  must be at least
c                  nx*5 cells.
c          root   array to hold the eigenvalues.  must be at least
c                 nrootx cells long.  the nrootx smallest roots are
c                  ordered largest first in this array.
c          vect   eigenvector array.  each column will hold an
c                  eigenvector for the corresponding root.  must be
c                  dimensioned with  njx  rows and at least  nrootx
c                  columns, unless no vectors
c                  are requested (negative nrootx).  in this latter
c                  case, the argument vect is just a dummy, and the
c                  storage is not used.
c                  the eigenvectors are normalized to unity.
c          nvec   number of vectors to calculate.
c
c      the arrays a and b are destroyed by the computation. the results
c      appear in root and vect.
c      for proper functioning of this routine, the result of a floating
c      point underflow should be a zero.
c
c      the original reference to the givens technique is in oak ridge
c      report number ornl 1574 (physics), by wallace givens.
c      the method as presented in this program consists of four steps,
c      all modifications of the original method...
c      first, the input matrix is reduced to tridiagonal form by the
c      householder technique (j. h. wilkinson, comp. j. 3, 23 (1960)).
c      the roots are then located by the sturm sequence method (j. m.
c      ortega (see reference below).  the vectors of the tridiagonal
c      form are then evaluated (j.h. wilkinson, comp. j. 1, 90 (1958)),
c      and last the tridiagonal vectors are rotated to vectors of the
c      original array (first reference).
c      vectors for degenerate (or near-degenerate) roots are forced
c      to be orthogonal, using a method suggested by b. garbow, argonne
c      national labs (private communication, 1964).  the gram-schmidt
c      process is used for the orthogonalization.
c
c      an excellent presentation of the givens technique is found in
c      j. m. ortega s article in  mathematics for digital computers,
c      volume 2, ed. by ralston and wilf, wiley (1967), page 94.
c
       dimension b(nx,*),a(*),root(*),vect(njx,*)
c
c ** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
c **   users please note...
c **   the following two parameters, eta and theta, should be adjusted
c **   by the user for his particular machine.
c **   eta is an indication of the precision of the floating point
c **   representation on the computer being used (roughly 10**(-m),
c **   where m is the number of decimals of precision ).
c **   theta is an indication of the range of numbers that can be
c **   expressed in the floating point representation (roughly the
c **   largest number).
c **   some recommended values follow.
c **   for ibm 7094, univac 1108, etc. (27-bit binary fraction, 8-bit
c **   binary exponent), eta=1.e-8, theta=1.e37.
c **   for control data 3600 (36-bit binary fraction, 11-bit binary
c **   exponent), eta=1.e-11, theta=1.e307.
c **   for control data 6600 (48-bit binary fraction, 11-bit binary
c **   exponent), eta=1.e-14, theta=1.e307.
c **   for ibm 360/50 and 360/65 real*8 (56-bit hexadecimal
c **   fraction, 7-bit hexadecimal exponent), eta=1.e-16, theta=1.e75.
c **   for telefunken tr440, eta=1.e-11, theta=1.e152.
c **
cdec20 data eta,theta /1.d-14,1.d+37/
c-       data eta,theta /1.d-12,1.d+35/
c
       parameter(eta=1d-12, theta=1d35)
       parameter(zero=0d0, half=5d-1, one=1d0, two=2d0, three=3d0,
     &  four=4d0, r4099=4099d0)
c ** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
c
       del1 = eta/(100)
       delta = eta**2*(100)
       small = eta**2/(100)
       delbig = theta*delta/(1000)
       theta1 = (1000)/theta
c      toler  is a factor used to determine if two roots are close
c      enough to be considered degenerate for purposes of orthogonali-
c      zing their vectors.  for the matrix normed to unity, if the
c      difference between two roots is less than toler, then
c      orthogonalization will occur.
       toler = eta*(100)
c
c      initial value for pseudorandom number generator... (2**23)-3
       rpower = (8388608)
       rpow1 = rpower*half
       rand1 = rpower - three
c
      n = nx
      nroot = abs(nrootx)
      if (nroot.eq.0) go to 1001
      if (n-1) 1001,1003,105
c
c     special section for n=1.
1003  root(1) = a(1)
      if (nvec.gt.0) vect(1,1) = one
      go to 1001
c
  105 if (n.gt.2) goto 106
c     special section for n=2
      if(a(2).ne.zero) goto 107
      root(1) = min(a(1),a(3))
      if (nrootx.lt.0) goto 108
      if(nvec.lt.1)go to 108
      vect(1,1) = one
      vect(2,1) = zero
  108 if (nroot.lt.2) goto 1001
      root(2) = max(a(1),a(3))
      if (nrootx.lt.0) goto 1001
      if(nvec.lt.2)go to 1001
      vect(2,2) = one
      vect(1,2) = zero
      goto 1001
  107 factor = half*sqrt((a(1)-a(3))**2+four*a(2)**2)
      temp  = half* (a(1)+a(3))
      root(1) = temp - factor
      if (nrootx.lt.0) goto 109
      if(nvec.lt.1)go to 109
      temp1 = one/a(2)
      vect(2,1) = (root(1)-a(1))*temp1
      anorm = one/sqrt(one+vect(2,1)**2)
      vect(2,1) = anorm*vect(2,1)
      vect(1,1) = anorm
  109 if (nroot.lt.2) goto 1001
      root(2) = temp + factor
      if (nrootx.lt.0) goto 1001
      if(nvec.lt.2)go to 1001
      vect(1,2) = (root(2)-a(3))*temp1
      anorm = one/sqrt(one+vect(1,2)**2)
      vect(1,2) = anorm*vect(1,2)
      vect(2,2) = anorm
      goto 1001
c
c     section for  n.gt.2 .
c     nsize    number of elements in the packed array
  106 nsize = (n*(n+1))/2
      nm1 = n-1
      nm2 = n-2
      do 20 j=1,5
      do 20 i=1,nx
   20 b(i,j)=zero
c
c     scale matrix to euclidean norm of 1.  scale factor is anorm.
      factor = zero
      do 70 i=1,nsize
      temp = abs(a(i))
70    factor = max(factor,temp)
      if (factor.ne.zero) go to 72
c     null matrix.  fix up roots and vectors, then exit.
      do 78 i=1,nroot
      if (nrootx.lt.0) go to 78
      if(nvec.le.0)go to 78
      do 77 j=1,n
77    vect(j,i) = zero
      vect(i,i) = one
78    root(i) = zero
      go to 1001
c
72    anorm = zero
      j = 1
      k = 1
86    temp = one/factor
      do 80 i=1,nsize
      if (i.ne.j) go to 81
      anorm = anorm + (a(i)*temp)**2*half
      k = k+1
      j = j+k
      go to 80
81    anorm = anorm + (a(i)*temp)**2
80    continue
83    anorm = sqrt(anorm*two)*factor
      temp1 = one/anorm
      do 91 i=1,nsize
91    a(i) = a(i)*temp1
      alimit = one
c
c      tridia section.
c      tridiagonalization of symmetric matrix
       id = 0
       ia = 1
c      if (nm2.eq.0) go to 201
       do 200  j=1,nm2
c      j       counts row  of a-matrix to be diagonalized
c      ia      start of non-codiagonal elements in the row
c      id      index of codiagonal element on row being codiagonalized.
       ia = ia+j+2
       id = id + j + 1
       jp2 = j+2
c      sum squares of non-codiagonal elements in row j
       ii = ia
       sum = zero
       do 100 i=jp2,n
       sum=sum+a(ii)**2
100    ii = ii + i
       temp = a(id)
       if (sum.gt.small) go to 110
c      no transformation necessary if all the non-codiagonal
c      elements are tiny.
120    b(j,1) = temp
       a(id) = zero
       go to 200
c      now complete the sum of off-diagonal squares
110    sum = sqrt(sum + temp**2)
c      new codiagonal element
       b(j,1) = -sign(sum,temp)
c      first non-zero element of this w-vector
       b(j+1,2) = sqrt((one + abs(temp)/sum)*half)
c      form rest of the w-vector elements
       temp = sign(half/(b(j+1,2)*sum),temp)
       ii = ia
       do 130 i=jp2,n
       b(i,2) = a(ii)*temp
130    ii = ii + i
c      form p-vector and scalar.  p-vector = a-matrix*w-vector.
c     scalar = w-vector*p-vector
       ak = zero
c      ic      location of next diagonal element
       ic = id + 1
       j1 = j + 1
       do 190  i=j1,n
       jj = ic
       temp = zero
       do 180  ii=j1,n
c      i       runs over the non-zero p-elements
c      ii      runs over elements of w-vector
       temp = temp + b(ii,2)*a(jj)
c      change incrementing mode at the diagonal elements.
       if (ii.lt.i) go to 210
140    jj = jj + ii
       go to 180
210    jj = jj + 1
180    continue
c      build up the k-scalar (ak)
       ak = ak + temp*b(i,2)
       b(i,1) = temp
c      move ic to top of next a-matrix  row
190    ic = ic + i
c      form the q-vector
       do 150  i=j1,n
150    b(i,1) = b(i,1) - ak*b(i,2)
c      transform the rest of the a-matrix
c      jj      start-1 of the rest of the a-matrix
       jj = id
c      move w-vector into the old a-matrix locations to save space
c      i       runs over the significant elements of the w-vector
       do 160  i=j1,n
       a(jj) = b(i,2)
       do 170  ii=j1,i
       jj = jj + 1
170    a(jj) = a(jj) - two*(b(i,1)*b(ii,2) + b(i,2)*b(ii,1))
160    jj = jj + j
200    continue
c      move last codiagonal element out into its proper place
201    continue
       b(nm1,1) = a(nsize-1)
       a(nsize-1) = zero
c
c     sturm section.
c     sturm sequence iteration to obtain roots of tridiagonal form.
c     move diagonal elements into second n elements of b-vector.
c     this is a more convenient indexing position.
c     also, put square of codiagonal elements in third n elements.
      jump=1
      do 320 j=1,n
      b(j,2)=a(jump)
      b(j,3) = b(j,1)**2
320   jump = jump+j+1
      do 310 i=1,nroot
310   root(i) = +alimit
      rootl = -alimit
c     isolate the roots.  the nroot lowest roots are found, lowest first
      do 330 i=1,nroot
c     find current  best  upper bound
      rootx = +alimit
      do 335 j=i,nroot
335   rootx = min(rootx,root(j))
      root(i) = rootx
c     get improved trial root
500   trial = (rootl + root(i))*half
      if (trial.eq.rootl.or.trial.eq.root(i)) go to 330
c     form sturm sequence ratios, using ortega s algorithm (modified).
c     nomtch is the number of roots less than the trial value.
350   continue
      nomtch = n
      j = 1
360   f0 = b(j,2) - trial
370   continue
      if (abs(f0).lt.theta1) go to 380
      if (f0.ge.zero) nomtch = nomtch - 1
      j = j + 1
      if (j.gt.n) go to 390
c     since matrix is normed to unity, magnitude of b(j,3) is less than
c     one, and since f0 is greater than theta1, overflow is not possible
c     at the division step.
      f0 = b(j,2) - trial - b(j-1,3)/f0
      go to 370
380   j = j + 2
      nomtch = nomtch - 1
      if (j.le.n) go to 360
390   continue
c     fix new bounds on roots
      if (nomtch.ge.i) go to 540
      rootl = trial
      go to 500
540   root(i) = trial
      nom = min(nroot,nomtch)
      root(nom) = trial
      go to 500
330   continue
c     reverse the order of the eigenvalues, since custom dictates
c     'largest first'.  this section may be removed if desired without
c     affecting the remainder of the routine.
c     nrt = nroot/2
c     do 10 i=1,nrt
c     save = root(i)
c     nmip1 = nroot - i + 1
c     root(i) = root(nmip1)
c  10 root(nmip1) = save
c
c     trivec section.
c     eigenvectors of codiagonal form
807   continue
c     quit now if no vectors were requested.
      if (nrootx.lt.0) go to 1002
      if(nvec.lt.1)go to 1002
c     initialize vector array.
      do 15 i=1,n
      do 15 j=1,nvec
15    vect(i,j) = one
      do 700 i=1,nvec
      aroot = root(i)
c     orthogonalize if roots are close.
      if (i.eq.1) go to 710
c     the absolute value in the next test is to assure that the trivec
c     section is independent of the order of the eigenvalues.
715   if (abs(root(i-1)-aroot).lt.toler) go to 720
710   ia = -1
720   ia = ia + 1
      elim1 = a(1) - aroot
      elim2 = b(1,1)
      jump = 1
      do 750  j=1,nm1
      jump = jump+j+1
c     get the correct pivot equation for this step.
      if (abs(elim1).le.abs(b(j,1))) goto 760
c     first (elim1) equation is the pivot this time.  case 1.
      b(j,2) = elim1
      b(j,3) = elim2
      b(j,4) = zero
      temp = b(j,1)/elim1
      elim1 = a(jump) - aroot - temp*elim2
      elim2 = b(j+1,1)
      go to 755
c     second equation is the pivot this time.  case 2.
760   b(j,2) = b(j,1)
      b(j,3) = a(jump) - aroot
      b(j,4) = b(j+1,1)
      temp = one
      if (abs(b(j,1)).gt.theta1) temp = elim1/b(j,1)
      elim1 = elim2 - temp*b(j,3)
      elim2 = -temp*b(j+1,1)
c     save factor for the second iteration.
755   b(j,5) = temp
750   continue
      b(n,2) = elim1
      b(n,3) = zero
      b(n,4) = zero
      b(nm1,4) = zero
      iter = 1
      if (ia.ne.0) go to 801
c     back substitute to get this vector.
790   l = n + 1
      do 780 j=1,n
      l = l - 1
786   continue
      lp1 = l+1
      lp2 = l+2
      elim1=vect(l,i)
      if (lp1.le.n) elim1=elim1-vect(lp1,i)*b(l,3)
      if (lp2.le.n) elim1=elim1-vect(lp2,i)*b(l,4)
c     if overflow is conceivable, scale the vector down.
c     this approach is used to avoid machine-dependent and system-
c     dependent calls to overflow routines.
      if (abs(elim1).gt.delbig) go to 782
      temp = b(l,2)
      if (abs(b(l,2)).lt.delta) temp = delta
      vect(l,i) = elim1/temp
      go to 780
c     vector is too big.  scale it down.
782   temp1 = one/delbig
      do 784 k=1,n
784   vect(k,i) = vect(k,i)*temp1
      go to 786
780   continue
      go to (820,800), iter
c     second iteration.  (both iterations for repeated-root vectors).
820   iter = iter + 1
890   elim1 = vect(1,i)
      do 830 j=1,nm1
      if (b(j,2).eq.b(j,1)) go to 840
c     case one.
850   vect(j,i) = elim1
      elim1 = vect(j+1,i) - elim1*b(j,5)
      go to 830
c     case two.
840   vect(j,i) = vect(j+1,i)
      elim1 = elim1 - vect(j+1,i)*temp
830   continue
      vect(n,i) = elim1
      go to 790
c     produce a random vector
801   continue
      temp1 = one/rpow1
      do 802 j=1,n
c     generate pseudorandom numbers with uniform distribution in (-1,1).
c     this random number scheme is of the form...
c     rand1 = dmod((2**12+3)*rand1,2**23)
c     it has a period of 2**21 numbers.
      rand1 = mod(r4099*rand1,rpower)
802   vect(j,i) = rand1*temp1 - one
      go to 790
c
c     orthogonalize this repeated-root vector to others with this root.
800   if (ia.eq.0) go to 885
      do 860 j1=1,ia
      k = i - j1
      temp = zero
      do 870 j=1,n
870   temp = temp + vect(j,i)*vect(j,k)
      do 880 j=1,n
880   vect(j,i) = vect(j,i) - temp*vect(j,k)
860   continue
885   go to (890,900), iter
c     normalize the vector
900   elim1 = zero
      do 904 j=1,n
904   elim1 = max(abs(vect(j,i)),elim1)
      temp=zero
      temp1 = one/elim1
      do 910 j=1,n
      elim2=vect(j,i)*temp1
      temp=temp+elim2**2
  910 continue
      temp=one/(sqrt(temp)*elim1)
      do 920 j=1,n
      vect(j,i) = vect(j,i)*temp
      if (abs(vect(j,i)).lt.del1) vect(j,i) = zero
920   continue
700   continue
c
c      simvec section.
c      rotate codiagonal vectors into vectors of original array
c      loop over all the transformation vectors
       if (nm2.eq.0) go to 1002
       jump = nsize - (n+1)
       im = nm1
       do 950  i=1,nm2
       j1 = jump
c      move a transformation vector out into better indexing position.
       do 955  j=im,n
       b(j,2) = a(j1)
955    j1 = j1 + j
c      modify all requested vectors.
       do 960  k=1,nvec
       temp = zero
c      form scalar product of transformation vector with eigenvector
       do 970  j=im,n
970    temp = temp + b(j,2)*vect(j,k)
       temp = temp + temp
       do 980  j=im,n
980    vect(j,k) = vect(j,k) - temp*b(j,2)
960    continue
       jump = jump - im
950    im = im - 1
1002   continue
c      restore roots to their proper size.
       do 95 i=1,nroot
95     root(i) = root(i)*anorm
1001   return
      end
      subroutine mtrans(a,ia,b,ib,m,n)
c
c  set b = a(transpose)
c
c  fortran equivalent of fps164 mtrans (10-oct-84, ron shepard).
c  the transpose cannot be done in-place with this code.
c
      integer ia,ib,m,n
      real*8 a(ia,n,m),b(ib,m,n)
c
      integer i,j
c
      do 100 i=1,m
      do 100 j=1,n
100   b(1,i,j)=a(1,j,i)
c
      return
      end
