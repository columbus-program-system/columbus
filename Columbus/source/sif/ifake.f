!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program ifake
cversion 5.0
c
c  write a "fake" integral file.
c
c  this program is useful for testing the SIFS I/O routines and for
c  debugging various electronic structure programs.
c
c  08-oct-90 (columbus day) 1-e fcore change. -rls
c  06-oct-90 fsplit=2 capability added. -rls
c  04-aug-89 SIFS version written by ron shepard.
c
      implicit integer(a-z)
c
      integer   lbufp,      n1maxp,      n2maxp,      nbfmxp
      parameter(lbufp=2048, n1maxp=1636, n2maxp=1364, nbfmxp=99)
c
      integer i, nin, aoints, nlist, nsym, fsplit, nbft, isym, j, nnbft,
     & nnsym, ifmt1, ierr, n1max, l1rec, ifmt2, n2max, l2rec, ntitle,
     & ninfo, nenrgy, nmap, itypea, itypeb, ii0, nbi, ibvtyp, nipv, num,
     & nrec, numtot, last, i2, ij, i1, ijsym, j2, jsym, j1, idummy, k,
     & ijksym, l2, l
c
      real*8 buffer(lbufp)
      integer lab1(2,n1maxp), lab2(4,n2maxp)
      integer ibitv( ((n2maxp+63)/64)*64 )
      real*8 array1( (nbfmxp*(nbfmxp+1))/2 )
      real*8 val1(n1maxp), val2(n2maxp)
      integer symb(nbfmxp), mapout(nbfmxp)
      integer info(5), ietype(5), imtype( 1), map(1), reqnum
      integer symoff(36), kntin(36), kntout(36)
      real*8 energy(5), fcore
c
      real*8     small
      parameter( small = 1d-12 )
c
      character*80 title(2)
      character*8 bfnlab(nbfmxp), chrtyp
      character*4 slabel(8)
c
      real*8 fact
      parameter(fact=.01d0)
c
      parameter( nin=5, nlist=6, aoints=10 )
c
c     # last parameters:
c     # msame  = more of the same specific integral type.
c     # nmsame = no more of the same specific integral type, but more
c     #          1-e integrals follow.
c     # nomore = no more 1-e integrals, or no more 2-e integrals.
c
      integer   msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
      integer   iwait
      parameter(iwait=1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer mult(8,8), nbpsy(8), irb1(8), irb2(8)
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      integer nndxf
      nndxf(i) = (i*(i-1))/2
c
*@ifdef future
*C  open nin and nlist here if necessary.
*@else
c  assume preconnected units.
*@endif
c
      call ibummr(nlist)
c
      title(1)(1:40) = 'SIFS file created by program "ifake".'
      call siftdy( title(1)(41:) )
c
c     # set up identity mapping vector.
      do 10 i = 1, nbfmxp
         mapout(i) = i
10    continue
c
c     # read in symmetry blocking info...
c
      write(nlist,*)'input nsym, nbpsy(1:nsym) :'
      read(nin,*)nsym,(nbpsy(i),i = 1,nsym)
c
      write(nlist,*)'input fsplit ([1],2):'
      fsplit = 1
      read(nin,*)fsplit
c
      nbft = 0
      do 14 isym = 1,nsym
         irb1(isym) = nbft+1
         nbft = nbft+nbpsy(isym)
         irb2(isym) = nbft
         do 12 j = irb1(isym),irb2(isym)
            symb(j) = isym
12       continue
14    continue
c
      nnbft = nndxf(nbft+1)
      nnsym = nndxf(nsym+1)
c
      write(nlist,6010)'nbft=',nbft,'nbpsy(*)=',(nbpsy(i),i=1,nsym)
      write(title(2),6010)'"ifake" parameters: nbft=',nbft,
     & 'nbpsy(*)=',(nbpsy(i),i=1,nsym)
6010  format(1x,a,i4,1x,a,8i4)
c
c     # get a consistent set of configuration parameters
c     #  for 1-e integrals...
c
      call sifcfg( 1, lbufp, nbft, 0,     ifmt1, l1rec, n1max, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('ifake: from 1-e sifcfg, ierr=',ierr,faterr)
      endif

c
c     # and for 2-e integrals with a bit vector...
c
      call sifcfg( 2, lbufp, nbft, 1,     ifmt2, l2rec, n2max, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('ifake: from 2-e sifcfg, ierr=',ierr,faterr)
      endif
c
c     # set up some typical header info...
c
      ntitle = 2
      ninfo = 6
      info(1) = fsplit
      info(2) = l1rec
      info(3) = n1max
      info(4) = l2rec
      info(5) = n2max
      info(6) = ifmt2 
      nenrgy = 5
      ietype(1) = -1
      energy(1) = -1
      ietype(2) = -2
      energy(2) = -2
      ietype(3) = -1024
      energy(3) = -1024
      ietype(4) = 1
      energy(4) = 1
      ietype(5) = 2
      energy(5) = 2
      nmap = 0
      imtype(1) = 0
      map(1) = 0
c
c     # define some symmetry labels...
      do 20 i = 1,nsym
         write(slabel(i),'(a3,i1)')'sym',i
20    continue
c
c     # define some basis function labels...
      do 30 i = 1,nbft
         write(bfnlab(i),'(a6,i2.2)')'fakeao',i
30    continue
c
c     # open the output file.
      open(unit=aoints,file='aoints',form='unformatted',
     & status='unknown')
c
c     # write the header info...
c
      call sifwh( aoints, ntitle, nsym, nbft,
     & ninfo, nenrgy, nmap, title,
     & nbpsy, slabel, info, bfnlab,
     & ietype, energy, imtype, map,    ierr )
      if( ierr.ne.0 ) call bummer('ifake: sifwh ierr=',ierr,faterr)
c
c     # write out some 1-e integral arrays.
c
c     # s1(i,j) = delta(i,j)
c
      itypea=0
      itypeb=0
      fcore  =  (2*nbft)
c
      call wzero( nnbft, array1, 1 )
      call izero_wr( nnsym, kntin,  1 )
      ii0  =  0
      do 110 isym  =  1, nsym
         nbi = nbpsy(isym)
         kntin( nndxf(isym+1) ) = nbi
         do 100 i = 1, nbi
            array1( ii0 + nndxf(i+1) ) = (1)
100      continue
         ii0 = ii0 + nndxf( nbi+1 )
110   continue
c
      ibvtyp = 0
      nipv = 2
      num = 0
      nrec = 0
      numtot = 0
      last = nmsame
      call sifw1x( aoints, info, last, itypea,
     & itypeb, mapout, array1,
     & nsym, nbpsy, symoff, kntin,
     & buffer, val1, lab1, fcore,
     & small, kntout, numtot, nrec,
     & ierr )
      if(ierr.ne.0)call bummer('ifake: s1(*) sifw1x ierr=',ierr,faterr)
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,6060)numtot,chrtyp,nrec
c
c     # t1(i,j) = max(i,j) + fact * min(i,j)
c
      call wzero( nnbft, array1, 1 )
      call izero_wr( nnsym, kntin,  1 )
      i2 = 0
      ij = 0
      do 220 isym = 1, nsym
         kntin( nndxf(isym+1) ) = 1
         i1 = i2 + 1
         i2 = i2 + nbpsy(isym)
         do 210 i = i1, i2
            do 200 j = i1, i
               ij = ij + 1
               array1( ij ) = i + fact * j
200         continue
210      continue
220   continue
c
      itypea = 0
      itypeb = 1
      fcore = (10*itypea + itypeb)
c
      ibvtyp = 0
      nipv = 2
      num = 0
      nrec = 0
      numtot = 0
      last = nmsame
      call sifw1x( aoints, info, last, itypea,
     & itypeb, mapout, array1,
     & nsym, nbpsy, symoff, kntin,
     & buffer, val1, lab1, fcore,
     & small, kntout, numtot, nrec,
     & ierr )
      if(ierr.ne.0)call bummer('ifake: t1(*) sifw1x ierr=',ierr,faterr)
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,6060)numtot,chrtyp,nrec
c
      itypea = 0
      itypeb = 2
      fcore = (10*itypea + itypeb)
c
      ibvtyp = 0
      nipv = 2
      num = 0
      nrec = 0
      numtot = 0
      last = nmsame
      call sifw1x( aoints, info, last, itypea,
     & itypeb,  mapout, array1,
     & nsym, nbpsy, symoff, kntin,
     & buffer, val1, lab1, fcore,
     & small, kntout, numtot, nrec,
     & ierr )
      if(ierr.ne.0)call bummer('ifake: v1(*) sifw1x ierr=',ierr,faterr)
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,6060)numtot,chrtyp,nrec
c
      itypea = 0
      itypeb = 6
      fcore = (10*itypea + itypeb)
c
      ibvtyp = 0
      nipv = 2
      num = 0
      nrec = 0
      numtot = 0
      last = nmsame
      call sifw1x( aoints, info, last, itypea,
     & itypeb,  mapout, array1,
     & nsym, nbpsy, symoff, kntin,
     & buffer, val1, lab1, fcore,
     & small, kntout, numtot, nrec,
     & ierr )
      if(ierr.ne.0)
     & call bummer('ifake: generic h1(*) sifw1x ierr=',ierr,faterr)
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,6060)numtot,chrtyp,nrec
c
c     # write out a itypea=1 property integral.
c
      call wzero( nnbft, array1, 1 )
      call izero_wr( nnsym, kntin,  1 )
      ijsym = 0
      i2 = 0
      ij = 0
      do 350 isym = 1, nsym
         i1 = i2 + 1
         i2 = i2 + nbpsy(isym)
c
         j2 = 0
         do 320 jsym = 1, (isym-1)
            ijsym = ijsym + 1
            symoff(ijsym) = ij
            kntin(ijsym) = 1
c
            j1 = j2 + 1
            j2 = j2 + nbpsy(jsym)
            do 310 j = j1, j2
               do 300 i = i1, i2
                  ij = ij + 1
                  array1( ij ) = i + fact * j
300            continue
310         continue
320      continue
c
         ijsym = ijsym + 1
         symoff(ijsym) = ij
         kntin(ijsym) = 1
c
         do 340 i = i1, i2
            do 330 j = i1, i
               ij = ij + 1
               array1( ij ) = i + fact * j
330         continue
340      continue
350   continue
c
      itypea = 1
      itypeb = 0
      fcore = (10*itypea + itypeb)
c
      ibvtyp = 0
      nipv = 2
      num = 0
      nrec = 0
      numtot = 0
      last = nmsame
      call sifw1x( aoints, info, last, itypea,
     & itypeb, mapout, array1,
     & nsym, nbpsy, symoff, kntin,
     & buffer, val1, lab1, fcore,
     & small, kntout, numtot, nrec,
     & ierr )
      if(ierr.ne.0)call bummer('ifake: x(*) sifw1x ierr=',ierr,faterr)
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,6060)numtot,chrtyp,nrec
c
c     # write out the same array, but as itypea=2.
      itypea = 2
      itypeb = 0
      fcore = (10*itypea + itypeb)
c
      ibvtyp = 0
      nipv = 2
      num = 0
      nrec = 0
      numtot = 0
      last = nomore
      call sifw1x( aoints, info, last, itypea,
     & itypeb,  mapout, array1,
     & nsym, nbpsy, symoff, kntin,
     & buffer, val1, lab1, fcore,
     & small, kntout, numtot, nrec,
     & ierr )
      if(ierr.ne.0)call bummer('ifake: so:x sifw1x ierr=',ierr,faterr)
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,6060)numtot,chrtyp,nrec
c
c     # all done with 1-e integrals.  open the 2-e integral file.
c
      call sifo2f( aoints, aoints, 'aoints2', info, idummy, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('ifake: from sifof2, ierr=',ierr,faterr)
      endif
c
c  write out some 2-e integral records with a packed bit vector...
c
c  (i,j|k,l)=i+fact*j +fact**2*k +fact**3*l
c  ibitv(i) = [99*0,1]
c
      itypea = 3
      itypeb = 0
      ibvtyp = 1
      nipv = 4
      num = 0
      nrec = 0
      numtot = 0
      do 540 i = 1,nbft
         isym = symb(i)
         do 530 j = 1,i
            ijsym = mult(symb(j),isym)
            do 520 k = 1,i
               ijksym = mult(symb(k),ijsym)
               if(nbpsy(ijksym).eq.0)goto 520
               if(k.eq.i)then
                  l2 = j
               else
                  l2 = min(irb2(ijksym),k)
               endif
               do 510 l = irb1(ijksym),l2
                  if(num.eq.n2max)then
                     numtot  =  numtot + num
                     last = msame
                     call sifew2( aoints, info, nipv, num,
     &                last, itypea, itypeb, 
     &                ibvtyp, val2, lab2, ibitv,
     &                buffer, iwait, nrec, reqnum, ierr )
                     if(ierr.ne.0)call bummer('ifake: sifew2 ierr=',
     &                ierr,faterr)
                     numtot = numtot - num
                  endif
                  num = num+1
                  val2(num) = ((l*fact+k)*fact+j)*fact+i
                  lab2(1,num) = i
                  lab2(2,num) = j
                  lab2(3,num) = k
                  lab2(4,num) = l
                  ibitv(num) = mod(numtot,100)/99
510            continue
520         continue
530      continue
540   continue
      numtot = numtot + num
      last = nomore
      call sifew2( aoints, info, nipv, num,
     & last, itypea, itypeb, 
     & ibvtyp, val2, lab2, ibitv,
     & buffer, iwait, nrec, reqnum, ierr )
      if(ierr.ne.0)call bummer('ifake: sifew2 ierr=',ierr,faterr)
c
      call sifc2f( aoints, info, ierr )
      if(ierr.ne.0)call bummer('ifake: sifc2f ierr=',ierr,faterr)
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,6060)numtot,chrtyp,nrec
c
      stop
6060  format(1x,i8,1x,a,' integrals written in',i4,' records.')
      end
