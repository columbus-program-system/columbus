!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program icheck
cversion 5.0
c
c  read an fake integral file created by program "ifake" and compare
c  the 2-e integrals against their integral values.
c
c  19-nov-90 written by ron shepard.
c
      implicit integer(a-z)
      character*80 title(20)
      integer nbpsy(8)
      parameter(lenbuf=2048, n1eabf=1636, n2eabf=1364, nbfmxp=510)
      real*8 buffer(lenbuf)
      integer ilab1(2,n1eabf),ilab2(4,n2eabf)
      equivalence (ilab1,ilab2)
      real*8 val1(n1eabf),val2(n2eabf)
      equivalence (val1,val2)
      integer ietype(20),info(5)
      integer imtype(1),map(1)
      real*8 energy(20), sifsce
      character*4 slabel(8)
      character*8 bfnlab(nbfmxp)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      nin   = 5
      nlist = 6
      aoints= 10
c
*@ifdef future
*C  open nlist here if necessary.
*@else
c  assume preconnected unit.
*@endif
c
      call ibummr(nlist)
c
      write(nlist,6010)
6010  format(/' program "icheck" 1.0b1'
     & //' written by ron shepard'
     & /' version date: 19-nov-90')
c
      open(unit=aoints,file='aoints',
     & form='unformatted',status='old')
c
c     # read the header info.
c
      call sifrh1( aoints, ntitle, nsym, nbft,
     & ninfo, nenrgy, nmap, ierr )
      if(ierr.ne.0)then
         call bummer('icheck: ierr=',ierr,faterr)
      elseif(ntitle.gt.20)then
         call bummer('icheck: ntitle=',ntitle,faterr)
      elseif(nbft.gt.nbfmxp)then
         call bummer('icheck: nbft=',nbft,faterr)
      elseif(ninfo.gt.5)then
         call bummer('icheck: ninfo=',ninfo,faterr)
      elseif(nenrgy.gt.10)then
         call bummer('icheck: nenrgy=',nenrgy,faterr)
      endif
c
c     # ignore map vectors, if any...
      nmap = 0
      call sifrh2( aoints, ntitle, nsym, nbft,
     & ninfo, nenrgy, nmap, title,
     & nbpsy, slabel, info, bfnlab,
     & ietype, energy, imtype, map,    ierr )
      if ( ierr.ne.0 ) call bummer('icheck: ierr=',ierr,faterr)
c
      write(nlist,6020)(title(i),i=1,ntitle)
6020  format(/' file header information:'/(1x,a))
c
      write(nlist,6030)
6030  format(/' core energies:')
      call sifpre( nlist, nenrgy, energy, ietype )
c
      write(nlist,6032) sifsce(nenrgy, energy, ietype )
6032  format(/' total core energy =',1pe20.12)
c
      write(nlist,6040)nsym,nbft
6040  format(/' nsym =',i2,' nbft=',i4/)
      write(nlist,6050)'symmetry =',(i,i=1,nsym)
      write(nlist,6060)'slabel(*) =',(slabel(i),i=1,nsym)
      write(nlist,6050)'nbpsy(*) =',(nbpsy(i),i=1,nsym)
6050  format(1x,a,t15,8i5)
6060  format(1x,a,t15,8(1x,a4))
      write(nlist,6070)(info(i),i=1,ninfo)
6070  format(/' info(*)=',10i6)
      write(nlist,6080)(i,bfnlab(i),i=1,nbft)
6080  format(/' i:bfnlab(i) ='/ (5(i4,':',a8)) )
c
c     # check record length parameters...
c
      if(info(1).ne.1 .and. info(1).ne.2 )then
         call bummer('icheck: fsplit=',info(1),faterr)
      elseif(info(2).gt.lenbuf)then
         call bummer('icheck: l1rec=',info(2),faterr)
      elseif(info(3).gt.n1eabf)then
         call bummer('icheck: n1max=',info(3),faterr)
      elseif(info(4).gt.lenbuf)then
         call bummer('icheck: l2rec=',info(4),faterr)
      elseif(info(5).gt.n2eabf)then
         call bummer('icheck: n2max=',info(5),faterr)
      endif
c
c     # open the 2-e file.
      call sifo2f( aoints, aoints, 'aoints2', info, idummy, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('icheck: from sifo2f, ierr=',ierr,faterr)
      endif
c
c     # check the 2-e integrals.
c
      call chk2e( nlist, aoints, info, buffer, ilab2, val2 )
c
      call sifc2f( aoints, info, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('icheck: from sifc2f, ierr=',ierr,faterr)
      endif
      close (unit = aoints)
c
      call bummer('normal termination',0,3)
      stop
      end
c deck chk2e
      subroutine chk2e( nlist, ntape, info, buf, ilab, val )
c
c  read the 2-e integrals and compare against known values.
c
c  19-nov-90 written by ron shepard.
c
      implicit integer(a-z)
c
c     integer   nipv,   msame
      parameter(nipv=4, msame=0)
      real*8 buf(*),val(*)
      integer info(*),ilab(nipv,*)
c
      parameter(iretbv=0)
      integer ibitv(1)
c
      real*8 temp
c
      real*8    fact,       small
      parameter(fact=.01d0, small=1.d-10)
c
      character*8 chrtyp
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      nndxf(i) = (i * (i - 1)) / 2
c
c     # this routine is written to use asynchronous I/O.  This is not
c     # particularly important in this program for efficiency, but the
c     # structure of this code may be used as a template for other
c     # electronic structure codes.  -rls
c
c     # initiate the read of the first record.
      call sifr2( ntape, 0, info, buf, reqnum, ierr )
      if ( ierr.ne.0 ) call bummer('icheck: sifr2 ierr=',ierr,faterr)
c
      nerror = 0
      nrec = 0
      ntot = 0
100   continue
c
c        # make sure the buffer is ready.
         call sif2w8( ntape, info, reqnum, ierr )
         if ( ierr.ne.0 ) call bummer(
     &    'icheck: sif2w8 ierr=',ierr,faterr)
c
c        # decode the record, ignoring any bit-vector.
         call sifd2( info, nipv, iretbv, buf,
     &    num, last, itypea, itypeb,
     &    ifmt, ibvtyp, val, ilab,
     &    ibitv, ierr )
         if ( ierr .ne. 0 ) then
            call bummer('icheck: from sifd2, ierr=',ierr,faterr)
         endif
         if ( last .eq. msame ) then
c           # initiate the next read while the current buffer is
c           # being processed.
            call sifr2( ntape, 0, info, buf, reqnum, ierr )
            if ( ierr.ne.0 ) call bummer(
     &       'icheck: sif2w8 ierr=',ierr,faterr)
         endif
         nrec = nrec+1
         ntot = ntot+num
         do 200 i = 1,num
            p = ilab(1,i)
            q = ilab(2,i)
            r = ilab(3,i)
            s = ilab(4,i)
            if ( p .lt. q ) then
               t = p
               p = q
               q = t
            endif
            if ( r .lt. s ) then
               t = r
               r = s
               s = t
            endif
            pq = nndxf(p) + q
            rs = nndxf(r) + s
            if ( pq .lt. rs ) then
               t = p
               p = r
               r = t
               t = q
               q = s
               s = t
            endif
            temp = p + fact * (q + fact * (r + fact * s))
            if ( abs( temp - val(i) ) .gt. small ) then
               write(nlist,6010) p,q,r,s,val(i),temp
               nerror = nerror + 1
            endif
200      continue
      if(last.eq.msame)go to 100
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,'(/t20,a/t25,a,i10,a,i7,a,i7)')
     & '=== statistics for '//chrtyp//' array elements  ===',
     & ' ntot=', ntot,' nrec=',nrec,' nerror=',nerror
c
      return
6010  format(' icheck: p,q,r,s,val,temp=',4i3,1p2e20.12)
      end
