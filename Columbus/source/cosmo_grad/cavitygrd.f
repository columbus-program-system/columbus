!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine cavitygrd(isym,natoms,xyz,nps,cosurf,iatsp,qcos,ar,
     &                  nip,sude,isude,lcavity,dcos)

      implicit real*8 ( a-h, o-z )

c.......................................................................
c calculates gradient contribution of the A Matrix and the surface
c derivative term.
c.......................................................................

      include 'cosinc.h'

      parameter(pi=3.141592653589793238d0)

      dimension   dcos(3,natoms)
      dimension   cosurf(3,nps), qcos(nps), iatsp(nps), ar(nps)
      dimension   xk(3),xl(3)
      dimension   xyz(3,natoms)
      dimension   sude(4,nip), isude(4,nip)

c     temp. gradient matrix
      dimension grdhf(3,maxat)

      fdiagr=2.1d0*sqrt(pi)
      do i=1,natoms
         do j=1,3
            grdhf(j,i) = 0.d0
            dcos(j,i) = 0.d0
         enddo
      enddo
c.... debug
c      write(icolum,*)'nps:',nps
c      write(icolum,*)'nip:',nip
c      write(icolum,*)'lcavity:',lcavity
c      write(icolum,*)'natoms:',natoms
c      write(icolum,*)'isym:',isym
c         write(icolum,*)'Coordinates:'
c      do i=1,natoms
c         write(icolum,'(3f14.7)')   xyz(1,i), xyz(2,i),xyz(3,i)
c      enddo
c      write(icolum,'(2a)')'iatsp   cosurf_x  cosurf_y  cosurf_z ',
c     &               '   qcos      ar'
c      do i=1,nps
c        write(icolum,'(i3,6f14.7)') iatsp(i),cosurf(1,i),cosurf(2,i),
c     &               cosurf(3,i) , qcos(i), ar(i)
c      enddo
c      write(icolum,*)'sude amd isude:'
c      do i=1,nip
c        write(icolum,'(4f14.7)')sude(1,i),sude(2,i),sude(3,i),sude(4,i)
c        write(icolum,'(4i5)')isude(1,i),isude(2,i),isude(3,i),isude(4,i
c      enddo

c
c....  contribution of A matrix (q*grad(A)*q)
c

      do k=1,nps
         iak=iatsp(k)
         do ix=1,3
            xk(ix)=cosurf(ix,k)
         enddo
         qsk=qcos(k)
         do l=k+1,nps
            ial=iatsp(l)
            if (ial.ne.iak) then
               dist2=0.d0
               do ix=1,3
                  xxx=cosurf(ix,l)-xk(ix)
                  xl(ix)=xxx
                  dist2=dist2+xxx*xxx
               enddo
               ff=qsk*qcos(l)*dist2**(-1.5d0)
               do ix=1,3
                  grdhf(ix,iak)=grdhf(ix,iak)-xl(ix)*ff
                  grdhf(ix,ial)=grdhf(ix,ial)+xl(ix)*ff
               enddo
            endif
         enddo
      enddo

c
c.... skip surface derivative part, if symmetry is used
c     (small contributions)

      if ((lcavity .eq. 1) .and. (isym .eq. 0)) then
c        --- contributions from analytic surface derivatives for
c        --- the surface closure
         do i=1,nip
            ips0 = isude(4,i)
            ia=isude(1,i)
            ib=isude(2,i)
            suma=0.d0
            sumb=0.d0
            nsab=isude(3,i)
            do ips=ips0+1,ips0+nsab
               iat=iatsp(ips)
               if(iat .eq. ia) then
                  suma=suma+qcos(ips)**2/sqrt(ar(ips))
               else
                  sumb=sumb+qcos(ips)**2/sqrt(ar(ips))
               endif
            enddo
            deab=-0.5d0*fdiagr*(suma*sude(3,i)/sude(1,i)
     &                     +sumb*sude(4,i)/sude(2,i) )**2
            xk(1)=xyz(1,ib)-xyz(1,ia)
            xk(2)=xyz(2,ib)-xyz(2,ia)
            xk(3)=xyz(3,ib)-xyz(3,ia)
            deab=deab/sqrt(xk(1)**2+xk(2)**2+xk(3)**2)
            do ix=1,3
               grdhf(ix,ia)=grdhf(ix,ia)-xk(ix)*deab
               grdhf(ix,ib)=grdhf(ix,ib)+xk(ix)*deab
            enddo
         enddo
      endif

      do n=1,natoms
         do i=1,3
            dcos(i,n) = dcos(i,n) - grdhf(i,n)
         enddo
      enddo

      return
      end
