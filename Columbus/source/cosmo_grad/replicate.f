!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       subroutine replicate(isym,x,y,z,np,xfinal,yfinal,zfinal)
c
c
c Silmar - This subroutine replicates the points for the
c symmetries considered
c
c INPUT ARGUMENTS:
c
c isym -> symmetry point group(integer number)
c 0,1,2,...,7 for C1,Cs,Ci,C2,D2,C2v,C2h,D2h, respectively
c x,y,z -> coordinates of the point
c
c OUTPUT ARGUMENTS:
c
c np -> Total number of replicated points(including the initial)
c xfinal,yfinal,zfinal -> replicated points
c
       implicit none
       integer np,isym
       real*8 x,y,z
       real*8 xfinal(8),yfinal(8),zfinal(8)
c
c  Local variables
c
       integer Cs(8,3),Ci(8,3),C2(8,3),D2(8,3)
       integer C2v(8,3),C2h(8,3),D2h(8,3)
       integer i,j
       logical dist(28)
       integer perm(8,3,7)
       integer npts(7)
       real*8 threshold
       real*8 xnew(8),ynew(8),znew(8)
c
c end of local variables
c
       equivalence(perm(1,1,1),Cs(1,1))
       equivalence(perm(1,1,2),Ci(1,1))
       equivalence(perm(1,1,3),C2(1,1))
       equivalence(perm(1,1,4),D2(1,1))
       equivalence(perm(1,1,5),C2v(1,1))
       equivalence(perm(1,1,6),C2h(1,1))
       equivalence(perm(1,1,7),D2h(1,1))
c
       parameter(threshold=1.0d-8)
c
       data Cs/1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,
     .  1,-1,0,0,0,0,0,0/
c
       data Ci/1,-1,0,0,0,0,0,0,1,-1,0,0,0,0,0,0,
     .  1,-1,0,0,0,0,0,0/
c
       data C2/1,-1,0,0,0,0,0,0,1,-1,0,0,0,0,0,0,
     .  1,1,0,0,0,0,0,0/
c
       data D2/1,-1,1,-1,0,0,0,0,
     .  1,1,-1,-1,0,0,0,0,1,-1,-1,1,0,0,0,0/
c
       data C2v/1,-1,1,-1,0,0,0,0,1,-1,-1,1,0,0,0,0,
     .  1,1,1,1,0,0,0,0/
c
       data C2h/1,-1,1,-1,0,0,0,0,1,-1,1,-1,0,0,0,0,
     .  1,1,-1,-1,0,0,0,0/
c
       data D2h/1,1,1,-1,1,-1,-1,-1, 1,1,-1,1,-1,1,
     . -1,-1,1,-1,1,1,-1,-1,1,-1/
c
c
c npts is the array containing the number of symmetry operations
c for each group,excluding the C1 group
c
       data npts/2,2,2,4,4,4,8/
c
C np corresponds to the total number of points, including the initial
c
c
           do i=1,npts(isym)
c
           xnew(i)=perm(i,1,isym)*x
           ynew(i)=perm(i,2,isym)*y
           znew(i)=perm(i,3,isym)*z
c
c First compares the generated point with the initial point
c
           dist(i)=dsqrt((xnew(i)-x)**2 +
     .     (ynew(i)-y)**2 +
     . (znew(i)-z)**2).gt.threshold
c
c
c and then compares the generated point with
c all previous ones
c
             do j=i-1,2,-1
                if(dist(i)) then
           dist(i)=dsqrt((xnew(i)-xnew(j))**2 +
     .     (ynew(i)-ynew(j))**2 +
     . (znew(i)-znew(j))**2).gt.threshold
                endif
             enddo
           enddo
c
           np = 0
           do i=1,npts(isym)
            if (dist(i) .or. i.eq.1) then
               np=np+1
               xfinal(np)=xnew(i)
               yfinal(np)=ynew(i)
               zfinal(np)=znew(i)
c
            endif
          enddo
c
        if(isym.eq.0) then
        xfinal(1)=x
        yfinal(1)=y
        zfinal(1)=z
        np=1
        endif
c
          return
          end
