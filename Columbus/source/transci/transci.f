!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c
      program transci
      IMPLICIT NONE
      include "../colib/getlcl.h"

c
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer ifirst, lcore
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.
c
c
c

c
c
c     # use standard f90 memory allocation.
c
      integer mem1
      parameter ( lcored = 2 500 000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('transci: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore),stat=ierr )
      if ( ierr .ne. 0 ) then
         call bummer('transci: allocate failed ierr=',ierr,faterr)
      endif

      ifirst = 1
      mem1   = 0
c
c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
c
      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
c
c
c     # return from driver() implies successful execution.
c
c     # message-passing cleanup: stub if not in parallel
c
      call bummer('normal termination',0,3)
      stop 'end of transci'
      end



c deck block data
      block data
      implicit logical(a-z)
c da file record parameters
C     Common variables
C
      INTEGER             vecrln,indxln, ssym1,ssym2
C
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C

      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)

c------------------------------------------------------------------
c     # tape6 = standard listing file.
      integer      tape6
      equivalence (tape6,nunits(1))
      data tape6/6/, fname(1)/'trncils'/
c------------------------------------------------------------------
c     # tape5 = user input file.
      integer      tape5
      equivalence (tape5,nunits(2))
      data tape5/5/, fname(2)/'trnciin'/
c------------------------------------------------------------------
c     # nslist = summary listing file.
      integer      nslist
      equivalence (nslist,nunits(3))
      data nslist/7/, fname(3)/'trncism'/
c------------------------------------------------------------------
c     # filev1 = bra vector file.
c     integer      filev1
c     equivalence (filev1,nunits(7))
c------------------------------------------------------------------
c     # filev2 = ket vector file.
c     integer      filev2
c     equivalence (filev2,nunits(8))
c------------------------------------------------------------------
c     # fildrt1 = bra drt file.
c     integer      fildrt1
c     equivalence (fildrt1,nunits(9))
c------------------------------------------------------------------
c     # fildrt2 = ket drt file.
c     integer      fildrt2
c     equivalence (fildrt2,nunits(10))
c------------------------------------------------------------------
c     # flindx1 = bra index file.
      integer      flindx1
      equivalence (flindx1,nunits(14))
      data flindx1/8/, fname(14)/'index1'/
c------------------------------------------------------------------
c     # flindx2 = ket index file.
      integer      flindx2
      equivalence (flindx2,nunits(15))
      data flindx2/9/, fname(15)/'index2'/
c------------------------------------------------------------------
c     integer      civout1
c     equivalence (civout1,nunits(16))
c     data civout1/20/, fname(16)/'civout1'/
c------------------------------------------------------------------
c     integer      civout2
c     equivalence (civout2,nunits(17))
c     data civout2/21/, fname(17)/'civout2'/
c------------------------------------------------------------------
      integer filtot
      equivalence (nunits(20),filtot)
      data filtot/24/, fname(20)/'ciftotd'/
c------------------------------------------------------------------
      integer filacpf
      equivalence (nunits(21),fiacpf)
       integer fiacpf
      data fiacpf/25/, fname(21)/'flacpfd'/
c------------------------------------------------------------------
c     # cirefv1 = bra ci reference vector (sequential)
      integer      cirefv1
      equivalence (cirefv1,nunits(22))
      data cirefv1/26/, fname(22)/'cirefv1'/
c------------------------------------------------------------------
c     # cirefv2 = ket ci reference vector  (sequential)
      integer      cirefv2
      equivalence (cirefv2,nunits(23))
      data cirefv2/27/, fname(23)/'cirefv2'/
c------------------------------------------------------------------
c     # cirfl1 = bra ci reference vector (random access)
      integer      cirfl1
      equivalence (cirfl1 ,nunits(27))
      data cirfl1/13/, fname(27)/'cirfl1'/
c------------------------------------------------------------------
c     # cirfl2 = ket ci reference vector  (random access)
      integer      cirfl2
      equivalence (cirfl2,nunits(28))
      data cirfl2/14/, fname(28)/'cirfl2'/
c------------------------------------------------------------------
      integer trndens
      equivalence (nunits(25),trndens)
      data trndens/29/, fname(25)/'trndens'/
c------------------------------------------------------------------
      integer geom
      equivalence (nunits(26),geom)
      data geom/30/, fname(26)/'geom'/
c------------------------------------------------------------------
      integer moints
      equivalence (nunits(29),moints)
      data moints/33/, fname(29)/'moints'/
c------------------------------------------------------------------
      integer cid1trfl
      equivalence (nunits(34),cid1trfl)
      data cid1trfl/34/, fname(34)/'cid1trfl'/
c------------------------------------------------------------------
c     # fildrt1 = bra drt file.
c     # last fname is used for various filename manipulations.
c     data nunits(26)/0/, fname(26)/' '/
c------------------------------------------------------------------
c
c
c include(data.common)
      integer maxsym
      parameter (maxsym=8)
      integer symtab,sym12,sym21,nbas,lmda,lmdb,nmbpr,strtbw,strtbx
      integer blst1e,nmsym,mnbas,cnfx,cnfw,scrlen
      common/data/symtab(maxsym,maxsym),sym12(maxsym),sym21(maxsym),
     + nbas(maxsym),lmda(maxsym,maxsym),lmdb(maxsym,maxsym),
     + nmbpr(maxsym),strtbw(maxsym,maxsym),strtbx(maxsym,maxsym),
     + blst1e(maxsym),nmsym,mnbas,cnfx(maxsym),cnfw(maxsym),scrlen
c
      data symtab/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      data sym12/1,2,3,4,5,6,7,8/

c     # da record lengths
c     # record lengths for ci created direct acc. files
c     #
c     # indxln : da record length for index file
c     # vecrln : da record length for vector files
c
c     # should be tuned for your machine....
c

*@if defined  vax || ( ibm && vector )
*C      # vax maximum da record length is 4095
*       data indxln/4095/, vecrln/4095/
*@else
       data indxln/32768/, vecrln/32768/
*@endif


c
c  pointer tables for segment construction and evaluation.
c
c  mu-dependent entries added 7-nov-86 (rls).
c  this version written 7-sep-84 by ron shepard.
c
c
c     # s32=sqrt(3./2.) s34=sqrt(3./4.)
c

C    VARIABLE DECLARATIONS FOR subroutine nummerzwo
C
C     Parameter variables
C
      INTEGER             NST
      PARAMETER           (NST = 40)
      INTEGER             NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER             W00
      PARAMETER           (W00 = 3)
      INTEGER             RT0
      PARAMETER           (RT0 = 8)
      INTEGER             LB0
      PARAMETER           (LB0 = 13)
      INTEGER             RB0
      PARAMETER           (RB0 = 18)
      INTEGER             LT0
      PARAMETER           (LT0 = 23)
      INTEGER             R0
      PARAMETER           (R0 = 28)
      INTEGER             L0
      PARAMETER           (L0 = 33)
      INTEGER             WRT00
      PARAMETER           (WRT00 = 38)
      INTEGER             WRB00
      PARAMETER           (WRB00 = 43)
      INTEGER             WW00
      PARAMETER           (WW00 = 48)
      INTEGER             WR00
      PARAMETER           (WR00 = 53)
      INTEGER             RTRT00
      PARAMETER           (RTRT00 = 58)
      INTEGER             RBRB00
      PARAMETER           (RBRB00 = 63)
      INTEGER             RTRB00
      PARAMETER           (RTRB00 = 68)
      INTEGER             RRT00
      PARAMETER           (RRT00 = 73)
      INTEGER             RRT10
      PARAMETER           (RRT10 = 78)
      INTEGER             RRB00
      PARAMETER           (RRB00 = 83)
      INTEGER             RRB10
      PARAMETER           (RRB10 = 88)
      INTEGER             RR00
      PARAMETER           (RR00 = 93)
      INTEGER             RR10
      PARAMETER           (RR10 = 98)
      INTEGER             RTLB0
      PARAMETER           (RTLB0 = 103)
      INTEGER             RTLT10
      PARAMETER           (RTLT10 = 108)
      INTEGER             RBLB10
      PARAMETER           (RBLB10 = 113)
      INTEGER             RTL10
      PARAMETER           (RTL10 = 118)
      INTEGER             RLB10
      PARAMETER           (RLB10 = 123)
      INTEGER             RLT10
      PARAMETER           (RLT10 = 128)
      INTEGER             RL00
      PARAMETER           (RL00 = 133)
      INTEGER             RL10
      PARAMETER           (RL10 = 138)
      INTEGER             W10
      PARAMETER           (W10 = 143)
      INTEGER             W20
      PARAMETER           (W20 = 148)
      INTEGER             WW10
      PARAMETER           (WW10 = 153)
      INTEGER             WW20
      PARAMETER           (WW20 = 158)
      INTEGER             WRT10
      PARAMETER           (WRT10 = 163)
      INTEGER             WRT20
      PARAMETER           (WRT20 = 168)
      INTEGER             WRB10
      PARAMETER           (WRB10 = 173)
      INTEGER             WRB20
      PARAMETER           (WRB20 = 178)
      INTEGER             WR10
      PARAMETER           (WR10 = 183)
      INTEGER             WR20
      PARAMETER           (WR20 = 188)
      INTEGER             RTRB10
      PARAMETER           (RTRB10 = 193)
      INTEGER             RTRB20
      PARAMETER           (RTRB20 = 198)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              HALF
      PARAMETER           (HALF = 5D-1)
      REAL*8              HALFM
      PARAMETER           (HALFM = -5D-1)
      REAL*8              ONEM
      PARAMETER           (ONEM = -1D0)
      REAL*8              S32
      PARAMETER
     x(S32 = 1.2247448713915890490986420373529458130097385D0)
      REAL*8              S34
      PARAMETER
     x(S34 = .86602540378443864676372317075293616107654866D0)
C
C
      INTEGER             DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER             DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER             DB13(4,0:2), DB14(2,0:2,2)
      INTEGER             DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER             DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER             DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER             DB7(6,0:2),  DB8(6,0:2,2)
      COMMON / CDB    /   DB1,         DB2,         DB3,         DB4
      COMMON / CDB    /   DB4P,        DB5,         DB6,         DB7
      COMMON / CDB    /   DB8,         DB10,        DB11,        DB12
      COMMON / CDB    /   DB13,        DB14
C
C
      INTEGER             BCASEX(6,-2:3),           KCASEX(6,-2:3)
      INTEGER             NEX(-2:3)
      COMMON / CEX    /   NEX,         BCASEX,      KCASEX
C
C
      INTEGER             PT(0:3,0:3,NST5)
      COMMON / CPT    /   PT
C
C
C     Equivalenced common variables
C
      INTEGER             L(0:3,0:3,-2:2),          LB(0:3,0:3,-2:2)
      INTEGER             LT(0:3,0:3,-2:2),         R(0:3,0:3,-2:2)
      INTEGER             RB(0:3,0:3,-2:2),         RBLB1(0:3,0:3,-2:2)
      INTEGER             RBRB0(0:3,0:3,-2:2),      RL0(0:3,0:3,-2:2)
      INTEGER             RL1(0:3,0:3,-2:2),        RLB1(0:3,0:3,-2:2)
      INTEGER             RLT1(0:3,0:3,-2:2),       RR0(0:3,0:3,-2:2)
      INTEGER             RR1(0:3,0:3,-2:2),        RRB0(0:3,0:3,-2:2)
      INTEGER             RRB1(0:3,0:3,-2:2),       RRT0(0:3,0:3,-2:2)
      INTEGER             RRT1(0:3,0:3,-2:2),       RT(0:3,0:3,-2:2)
      INTEGER             RTL1(0:3,0:3,-2:2),       RTLB(0:3,0:3,-2:2)
      INTEGER             RTLT1(0:3,0:3,-2:2),      RTRB0(0:3,0:3,-2:2)
      INTEGER             RTRB1(0:3,0:3,-2:2),      RTRB2(0:3,0:3,-2:2)
      INTEGER             RTRT0(0:3,0:3,-2:2),      W0(0:3,0:3,-2:2)
      INTEGER             W1(0:3,0:3,-2:2),         W2(0:3,0:3,-2:2)
      INTEGER             WR0(0:3,0:3,-2:2),        WR1(0:3,0:3,-2:2)
      INTEGER             WR2(0:3,0:3,-2:2),        WRB0(0:3,0:3,-2:2)
      INTEGER             WRB1(0:3,0:3,-2:2),       WRB2(0:3,0:3,-2:2)
      INTEGER             WRT0(0:3,0:3,-2:2),       WRT1(0:3,0:3,-2:2)
      INTEGER             WRT2(0:3,0:3,-2:2),       WW0(0:3,0:3,-2:2)
      INTEGER             WW1(0:3,0:3,-2:2),        WW2(0:3,0:3,-2:2)
C
      INTEGER             ST1(8),      ST10(6),     ST11(4)
      INTEGER             ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER             ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER             ST5(6),      ST6(6),      ST7(6),      ST8(6)
      COMMON / CST    /   ST1,         ST2,         ST3,         ST4
      COMMON / CST    /   ST4P,        ST5,         ST6,         ST7
      COMMON / CST    /   ST8,         ST10,        ST11D,       ST11
      COMMON / CST    /   ST12,        ST13,        ST14
C
      INTEGER             MULT(8,8)
      COMMON / CSYM   /   MULT
C
      INTEGER             VTPT
      REAL*8              OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
      COMMON / CVTRAN /   VTRAN,       VCOEFF,      OLDVAL,      VTPT
C


c     # nst= the number of segment value types.
c     # the following parameter list is the location of the
c     # delb=0 pointer matrix for the various segment types.
c     # equivalence names are used only locally to this routine
c     # for the purpose of documentation.
      equivalence (w0(0,0,0),pt(0,0,w00))
c
      equivalence (rt(0,0,0),pt(0,0,rt0))
c
      equivalence (lb(0,0,0),pt(0,0,lb0))
c
      equivalence (rb(0,0,0),pt(0,0,rb0))
c
      equivalence (lt(0,0,0),pt(0,0,lt0))
c
      equivalence (r(0,0,0),pt(0,0,r0))
c
      equivalence (l(0,0,0),pt(0,0,l0))
c
      equivalence (wrt0(0,0,0),pt(0,0,wrt00))
c
      equivalence (wrb0(0,0,0),pt(0,0,wrb00))
c
      equivalence (ww0(0,0,0),pt(0,0,ww00))
c
      equivalence (wr0(0,0,0),pt(0,0,wr00))
c
      equivalence (rtrt0(0,0,0),pt(0,0,rtrt00))
c
      equivalence (rbrb0(0,0,0),pt(0,0,rbrb00))
c
      equivalence (rtrb0(0,0,0),pt(0,0,rtrb00))
c
      equivalence (rrt0(0,0,0),pt(0,0,rrt00))
c
      equivalence (rrt1(0,0,0),pt(0,0,rrt10))
c
      equivalence (rrb0(0,0,0),pt(0,0,rrb00))
c
      equivalence (rrb1(0,0,0),pt(0,0,rrb10))
c
      equivalence (rr0(0,0,0),pt(0,0,rr00))
c
      equivalence (rr1(0,0,0),pt(0,0,rr10))
c
      equivalence (rtlb(0,0,0),pt(0,0,rtlb0))
c
      equivalence (rtlt1(0,0,0),pt(0,0,rtlt10))
c
      equivalence (rblb1(0,0,0),pt(0,0,rblb10))
c
      equivalence (rtl1(0,0,0),pt(0,0,rtl10))
c
      equivalence (rlb1(0,0,0),pt(0,0,rlb10))
c
      equivalence (rlt1(0,0,0),pt(0,0,rlt10))
c
      equivalence (rl0(0,0,0),pt(0,0,rl00))
c
      equivalence (rl1(0,0,0),pt(0,0,rl10))
c
      equivalence (w1(0,0,0),pt(0,0,w10))
c
      equivalence (w2(0,0,0),pt(0,0,w20))
c
      equivalence (ww1(0,0,0),pt(0,0,ww10))
c
      equivalence (ww2(0,0,0),pt(0,0,ww20))
c
      equivalence (wrt1(0,0,0),pt(0,0,wrt10))
c
      equivalence (wrt2(0,0,0),pt(0,0,wrt20))
c
      equivalence (wrb1(0,0,0),pt(0,0,wrb10))
c
      equivalence (wrb2(0,0,0),pt(0,0,wrb20))
c
      equivalence (wr1(0,0,0),pt(0,0,wr10))
c
      equivalence (wr2(0,0,0),pt(0,0,wr20))
c
      equivalence (rtrb1(0,0,0),pt(0,0,rtrb10))
c
      equivalence (rtrb2(0,0,0),pt(0,0,rtrb20))
c
c     # for each loop type, assign the segment extension type pointers
c     # and the delb offsets for each range.
c
c     # loop type 1:
      data  st1, db1
     & /  3,     1,     0,    -1,     0,     1,     0,    -1,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 2:
      data  st2, db2
     & /  3,     1,     0,    -1,     0,    -1,     0,     1,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 3:
      data  st3, db3
     & /  3,     1,     0,     1,     0,    -1,     0,    -1,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0/
c
c     # loop type 4:
      data  st4, db4
     & /  3,     0,     0,     1,     0,    -1,
     & rl00,   w00,  rl00,   rb0,    r0,   rt0,
     & rl00,   w10,  rl00,   rb0,    r0,   rt0,
     & rl00,   w20,  rl00,   rb0,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 4':
      data st4p, db4p
     & /  3,      0,     0,    -1,     0,     1,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 5:
      data  st5, db5
     & /  3,      2,     0,    -1,     0,    -1,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0/
c
c     # loop type 6:
      data  st6, db6
     & /  3,     1,     0,     0,     0,    -1,
     & rl00,   rb0,    r0,  wr00,    r0,   rt0,
     & rl00,   rb0,    r0,  wr10,    r0,   rt0,
     & rl00,   rb0,    r0,  wr20,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb00,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb10,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb20,    r0,   rt0/
c
c     # loop type 7:
      data  st7, db7
     & /  3,     1,     0,    -2,     0,     1,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0/
c
c     # loop type 8:
      data  st8, db8
     & /  3,     1,     0,    -1,     0,      0,
     & rl00,   rb0,    r0,   rt0,  rl00,    w00,
     & rl00,   rb0,    r0,   rt0,  rl00,    w10,
     & rl00,   rb0,    r0,   rt0,  rl00,    w20,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10/
c
c     # loop type 10:
      data st10, db10
     & /  3,     1,     0,     1,     0,     -2,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00/
c
c     # loop type 11d, 11:
      data st11d, st11, db11
     & /  3,      3,      3,      3,
     &    3,      0,      0,      0,
     & rl00,    w00,   rl00,    w00,
     & rl00,    w10,   rl00,    w10,
     & rl00,    w20,   rl00,    w20,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10/
c
c     # loop type 12:
      data st12, db12
     & /  3,     1,     0,    -1,
     & rl00, wrb00,    r0,   rt0,
     & rl00, wrb10,    r0,   rt0,
     & rl00, wrb20,    r0,   rt0,
     & rl00,   rb0,    r0, wrt00,
     & rl00,   rb0,    r0, wrt10,
     & rl00,   rb0,    r0, wrt20,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0/
c
c     # loop type 13:
      data st13, db13
     & /  3,      2,     0,     -2,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00/
c
c     # loop type 14:
      data st14, db14
     & /  3,     3,
     & rl00,  ww00,
     & rl00,  ww10,
     & rl00,  ww20,
     & rl00,   w00,
     & rl00,   w10,
     & rl00,   w20/
c
c     # segment values are calculated as:
c     #
c     #        tab( type(bcase,kcase,delb), bket)
c     #
c     # where the entries in tab(*,*) are defined in the table
c     # initialization routine.  type(*,*,*) is equivalenced to the
c     # appropriate  location of pt(*,*,*) and pt(*,*,*) is actually
c     # referenced with the corresponding offset added to delb.  in
c     # this version, this offset is determined from the range offset
c     # and from the reference occupation, imu, of the orbital.
c     #
c     # e.g. tab( pt(bcase,kcase,db0(r,imu)+delb), bket)
c
c**********************************************************
c     # w segment value pointers (assuming mu(*)=0):
      data w0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top segment value pointers:
      data rt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2 ,1 ,1 ,1,  2, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-bottom segment value pointers:
      data lb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  2, 1, 1, 1,  1,20, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1,  1, 1, 1, 1,  1, 1,18, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom segment value pointers:
      data rb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 2, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-top segment value pointers:
      data lt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 2, 1,  1, 1, 1,18,  1, 1, 1,20,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r segment value pointers:
      data r/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l segment value pointers:
      data l/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,33, 1, 1,  1,10, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3,11, 1,  1, 1,33, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=0):
      data wrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=0):
      data wrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=0):
      data ww0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=0):
      data wr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-top, x=0 segment value pointers:
      data rtrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  6, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-r-bottom,x=0  segment value pointers:
      data rbrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 6,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=0):
      data rtrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 2, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 2, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=0 segment value pointers:
      data rrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1, 54, 1, 1, 1,  1, 7, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 44, 1, 1, 1,  1, 1, 1, 1,  1, 1, 7, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=1 segment value pointers:
      data rrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 55, 1, 1, 1,  1,53,24, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 43, 1, 1, 1,  2, 1, 1, 1,  1,14,45, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=0 segment value pointers:
      data rrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,48,49, 1,  1, 1, 1, 7,  1, 1, 1, 7,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=1 segment value pointers:
      data rrb1/
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1,23,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,50,48, 1,  1, 1, 1,46,  1, 1, 1,52,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1,17,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=0 segment value pointers:
      data rr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=1 segment value pointers:
      data rr1/
     & 2, 1, 1, 1,  1, 2, 1, 1,  1,30,41, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,38,27, 1,  1,29,40, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,36,26, 1,  1, 1, 2, 1,  1, 1, 1, 2/
c**********************************************************
c     # r-top-l-bottom segment value pointers:
      data rtlb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 20, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 18, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l-top, x=1 segment value pointers:
      data rtlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,47, 2, 1,  1, 2,51, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-l-bottom, x=1 segment value pointers:
      data rblb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1,34, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,45, 1, 1,  1, 1,53, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1,31, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l, x=1 segment value pointers:
      data rtl1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 51, 1, 1, 1,  1,48,21, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 47, 1, 1, 1,  2, 1, 1, 1,  1,19,50, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-l-bottom, x=1 segment value pointers:
      data rlb1/
     & 1, 1, 1, 1,  1, 1, 1, 1, 34, 1, 1, 1,  1,25, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 45, 1, 1, 1, 53, 1, 1, 1,  1,50,48, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 31, 1, 1, 1,  1, 1, 1, 1,  1, 1,15, 1/
c**********************************************************
c     # r-l-top, x=1 segment value pointers:
      data rlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,53, 2, 1,  1, 1, 1,23,  1, 1, 1,56,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2,45, 1,  1, 1, 1,42,  1, 1, 1,17,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=0 segment value pointers:
      data rl0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=1 segment value pointers:
      data rl1/
     & 2, 1, 1, 1,  1,35, 1, 1,  1,13,35, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,37,28, 1,  1,28,39, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,32, 9, 1,  1, 1,32, 1,  1, 1, 1, 2/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=1):
      data w1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=2):
      data w2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=1):
      data ww1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=2):
      data ww2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 4, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=1):
      data wrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 58, 1, 1, 1, 58, 1, 1, 1,  1,66,67, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=2):
      data wrt2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  3, 1, 1, 1,  3, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=1):
      data wrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,58, 1, 1,  1, 1, 1, 1,  1, 1, 1,68,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1,58, 1,  1, 1, 1,65,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=2):
      data wrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=1):
      data wr1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=2):
      data wr2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 2, 1, 1,  1,60,35, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1,32,59, 1,  1, 1, 2, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottomsegment value pointers (assuming mu(*)=1):
      data rtrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,57, 1, 1,  1,64,70, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,69,63, 1,  1, 1,57, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=2):
      data rtrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1,62,34, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31,61, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c
c     # segment extension tables:
c     # casex(*,i) for i=-2 to 2 are extensions for occupation changes
c     #                     of i.
c     #                i=3 is a special case for diagonal extensions.
c
      data nex/ 1, 4, 6, 4, 1, 4/
*@ifdef reference
*C    # debug versions to agree with ancient ft program. -rls
*     data bcasex/
*     +   0, 0, 0, 0, 0, 0,
*     +   0, 0, 2, 1, 0, 0,
*     +   0, 1, 1, 2, 2, 3,
*     +   1, 2, 3, 3, 0, 0,
*     +   3, 0, 0, 0, 0, 0,
*     +   0, 1, 2, 3, 0, 0/
*     data kcasex/
*     +   3, 0, 0, 0, 0, 0,
*     +   1, 2, 3, 3, 0, 0,
*     +   0, 1, 2, 1, 2, 3,
*     +   0, 0, 1, 2, 0, 0,
*     +   0, 0, 0, 0, 0, 0,
*     +   0, 1, 2, 3, 0, 0/
*@else
      data bcasex/
     & 0, 0, 0, 0, 0, 0,
     & 2, 1, 0, 0, 0, 0,
     & 3, 2, 2, 1, 1, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
      data kcasex/
     & 3, 0, 0, 0, 0, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 2, 1, 2, 1, 0,
     & 2, 1, 0, 0, 0, 0,
     & 0, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
*@endif
c
c     irrep multiplication table.
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c     loop-value to ft-value transformation matrices.
c
      data vtran /
     & one,   zero,  zero,   one,
     & one,   zero,  halfm,  one,
     & one,   onem,  one,    one,
     & half,  zero,  zero,   one,
     & one,   zero,  halfm,  s32 /
c
      data vcoeff /
     & one,   half,  s32,    s34 /

      end






       subroutine aldlp(indsym1,conft1,indxyz1,index1,ci1,ciyz1,
     +        indsym2,conft2,indxyz2,index2,ci2,ciyz2,scr1)

c
c     calculation of all internal diagonal contributions
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine aldlp
C
C     External functions
C
      EXTERNAL            ddot_wr
C
      REAL*8              ddot_wr
C
C     Parameter variables
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.D0)
C
C     Argument variables
C
      INTEGER indsym1(*),conft1(*),indxyz1(*),index1(*)
     .      ,indsym2(*),conft2(*),indxyz2(*),index2(*)
C
      REAL*8  ci1(*),ciyz1(*),ci2(*),ciyz2(*),scr1
C
C     Local variables
C
      INTEGER             M1,          M1LP
      INTEGER             NMB,         NMBA,        NWALK
      INTEGER             NY,          SY,          SYM
      integer  nmb2,nmba2,sy2,sym2
C
C
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             CISTRT(2),   FIRST,       IPTH1,       IPTH2
      INTEGER             IWX,         JUMP,        MLP,         SEGSUM
      INTEGER             WTLPHD
C
      COMMON / DIAG   /   IPTH1,       IPTH2,       IWX,         CISTRT
      COMMON / DIAG   /   FIRST,       SEGSUM,      WTLPHD,      MLP
      COMMON / DIAG   /   JUMP
C
C    END OF VARIABLE DECLARATIONS FOR subroutine aldlp


c  nmotx = maximum number of molecular orbitals
c  mxinor= maximum number of internal orbitals
      scr1=zero
      do 1710 m1=1,wtlphd
          m1lp=mlp+m1
          if(jump.le.2)go to 1711
          if(m1lp.le.0)go to 1710
          if(m1lp.gt.segsum)return
          nmba=index1(m1lp)
          nmba2=index2(m1lp)
          go to 1714
 1711     continue
          nmba=indxyz1(m1lp)
          nmba2=indxyz2(m1lp)
 1714     continue
          if(nmba.lt.0 .or. nmba2.lt.0 )go to 1710
          sy=indsym1(nmba)
          sy2=indsym2(nmba2)
          nmb=conft1(nmba)+1
          nmb2=conft2(nmba2)+1
          sym=sym21(sy)
          sym2=sym21(sy2)
c     write(6,30190)m1,mlp,m1lp,nmb,sym,jump,cistrt
30190 format(' 1710',20i6)
          go to (1720,1730,1750,1800),jump
 1720     continue
c
c     z paths
c
c     write(6,30000)ciyz1(nmb),nmb,ciyz2(nmb2),nmb2
30000 format(' zwalks',f10.6,i6,f10.6,i6)
          scr1=scr1+ciyz1(nmb)*ciyz2(nmb2)
          go to 1710
 1730     continue
c
c     y paths
c
          ny=nbas(sym)
          if(ny.eq.0)go to 1710
c     print*,' ywalks',ny
c     call writex(ciyz(nmb),ny)
          scr1=scr1+ddot_wr(ny,ciyz1(nmb),1,ciyz2(nmb2),1)
          go to 1710
 1750     continue
c
c     x paths
c
          nmb=nmb-cistrt(1)
          nmb2=nmb2-cistrt(2)
          nwalk=cnfx(sym)
          if(nwalk.eq.0)go to 1710
c     print*,' xwalks',nwalk
c         write(*,*) 'xwalks: nwalk,nmb,nmb2=',nwalk,nmb,nmb2
c     call writex(ci(nmb),nwalk)
          scr1=scr1+ddot_wr(nwalk,ci1(nmb),1,ci2(nmb2),1)
          go to 1710
 1800     continue
c
c     w walks
c
          nmb=nmb-cistrt(1)
          nmb2=nmb2-cistrt(2)
          nwalk=cnfw(sym)
          if(nwalk.eq.0)go to 1710
c     print*,' wwalks',nwalk,nmb
c     call writex(ci(nmb),nwalk)
          scr1=scr1+ddot_wr(nwalk,ci1(nmb),1,ci2(nmb2),1)
 1710 continue
c     write(6,30010)scr1
30010 format(' scr1',f10.6)
      return
      end
c
c***********************************************************************
c
      subroutine aldn1(d2sym,d2asym,l1,i1,lsym,next,isym)
c
c     first-order density matrix is calculated
c
       implicit none
c  ##  parameter & common block section
c
c nmotx = maximum number of molecular orbitals
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
c
c mxinor = maximum number of internal orbitals
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
c
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
C
      REAL*8              two, one, mone
      PARAMETER           (TWO = 2.0D0, one=1.0d+00, mone=-1.0d+00)
c
      REAL*8 d1_sym(iprpmx),d1ci_sym(iprpmx),d1_asym(iprpmx),
     &       d1ci_asym(iprpmx)
      COMMON / DEN1P  / d1_sym, d1ci_sym, d1_asym, d1ci_asym
c
      INTEGER             EQVSYM(8),   IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(8),   NFRC(8),     NFRV(8),     NMUVAL
      INTEGER             NOCC(8),     NORBSM(8),   NOXT(8)
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
c
c  ##  real*8 section
c
      real*8 d2sym(*),d2asym(*)
      real*8 factor
c
c  ##  integer section
c
      integer i1ind, ind1, i1lv, isym, i1
      INTEGER l1lv, l1ind, l1, lsym
      integer next
c
c  ## external section
c
      integer  indd1
      external indd1
c
c-----------------------------------------------------------------------
c
      l1lv=l1+next
      l1ind=ivout(l1lv)
      i1lv=i1+next
      i1ind=ivout(i1lv)
      ind1=indd1(isym,lsym,i1ind,l1ind)
      factor=one
      if (l1ind .lt. i1ind) factor=mone
      d1_sym(ind1)=d1_sym(ind1)+two*d2sym(3)
      d1_asym(ind1)=d1_asym(ind1)+two*factor*d2asym(3)
*@ifdef debug
*      write(*,*)'aldn1:sym-adding ',two*d2sym(3),' l,i(sym)=',
*     & l1ind,i1ind,' lsym,isym=',  lsym,isym, ' to ind1', ind1
*      write(*,*)'aldn1:asym-adding ',two*d2asym(3),' l,i(sym)=',
*     & l1ind,i1ind,' lsym,isym=',   lsym,isym, ' to ind1', ind1,
*     & 'factor =',factor
*@endif
      return
      end
c
c***********************************************************************
c
      subroutine alld2(scr,scr2,code,d2,value1,value2,value3
     +,jump)
c
       implicit none
c  ##  integer secton
c
      INTEGER code, codem
      integer jump
C
      REAL*8 d2(3)
      real*8 scr, scr2
      real*8 value1, value2, value3
C
c-----------------------------------------------------------------------

c     multiply by appropriate loop values
c     if j1=k1=l1, d2(3) contains the first order density matrix
      codem=code-3
      go to (340,360,380,400,410,420,430),codem
  340 continue
c
c     value1*(ij/kl)+value2*(jk/il)
c
      if(jump.eq.1)then
       d2(3)=d2(3)+value2*scr2
      else
       d2(3)=d2(3)+value2*scr
      endif
      go to 500
  360 continue
c
c     value1*(ij/kl)+value2*(ik/jl)
c
      call bummer ('should never occur alld2 (1) ',0,2)
      go to 500
  380 continue
c
c     value1*(ik/jl)+value2*(jk/il)
c
      if(jump.eq.1)then
       d2(3)=d2(3)+value2*scr2
      else
       d2(3)=d2(3)+value2*scr
      endif
      go to 500
  400 continue
c
c     value1*(ij/kl)
c
      call bummer ('should never occur alld2 (2) ',0,2)
      go to 500
  410 continue
c
c     value1*(ik/jl)
c
c     if(j1eqk1.or.k1eql1.and..not.jkleq)then
          d2(3)=d2(3)+value1*scr
c         else
c         d2(2)=d2(2)+value1*scr
c     endif
      go to 500
  420 continue
c
c     value1*(jk/il)
c
      if(jump.eq.1)then
       d2(3)=d2(3)+value1*scr2
      else
       d2(3)=d2(3)+value1*scr
      endif
      go to 500
  430 continue
c
c     value1*(ii/il)+value2*(il/ll)+value3*(i/h/l)
c
      if(jump.eq.1)then
       d2(3)=d2(3)+value3*scr2
      else
       d2(3)=d2(3)+value3*scr
      endif
  500 continue
c     call writex(d2,3)
      return
      end
c
c********************************************************************
c
      subroutine allint (
     &    indsym1,   conft1,   indxyz1,   ciyz1,
     &    indsym2,   conft2,   indxyz2,   ciyz2,
     &    index11,   ci11,     index21,   ci21,
     &    index12,   ci12,     index22,   ci22,
     &    ipth1,     segsm,    cist,      isgexc,
     &    nmbseg,    lstseg,   first,     next)
c
c    .         lpbuf,d2il,ipth1,segsm,cist,
c    +isgexc,nmbseg,lstseg,first,iden1,iseg1,iseg2,next)

c*****
c     all internal case
c
c     new version february 1988
c     by
c     hans lischka, university of vienna
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz     ci vector for y and z walks
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     lpbuf    buffer for formula tape
c     d2il     all internal (ii/ll) and (il/il) second-order density
c              matrix elements. the all internal diagonal loops also
c              contribute to d2il (see subr.diagd)
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     isegxc=0  take combination between all segments
c           =1  take combination between all segments excluding zz and
c               yy loops (difference between segment numbers is one)
c           =2  take combination between different segments only
c               excluding zz and yy loops (difference between segment
c               numbers is larger than one)
c     nmbseg    number of segments
c     lstseg=.true.  last segement (ksg2=nseg) has to be taken into
c                    account
c     first=1        skip zz and yy cases
c     iden1=0  compute first-order density matrix only
c          =1  compute second order density matrix
c     iseg1    number of first segment
c     iseg2    number of second segment
c     filwd2    scratch file for d2
c     filrd2    scratch file for d2
c     nseg      number of segments
c     nsifbf     sifs buffer length
c     bufspc    buffer space for sifs io (2-e density file)
c
      implicit none 
C
C  ##  parameter & common block section
C
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 32000)
      REAL*8              ZERO
      PARAMETER           (ZERO = 0.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
C
      INTEGER             CIST1,       CIST2,       JUMP,        MLP
      INTEGER             MLPPR,       MLPPRZ,      SEGSM1,      SEGSM2
      INTEGER             WTLPHD
      COMMON / ALLIN1 /   WTLPHD,      MLP,         MLPPR,       SEGSM1
      COMMON / ALLIN1 /   SEGSM2,      CIST1,       CIST2,       JUMP
      COMMON / ALLIN1 /   MLPPRZ
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             FUEL1(7),    IDUM(12),    INTORB,      KBL3
      INTEGER             KBL4,        LAMDA1,      MAXBL2,      MAXBL3
      INTEGER             MAXBL4,      MAXLP3,      MXBL23,      MXKBL3
      INTEGER             MXKBL4,      MXORB,       N0EXT,       N0XTLP
      INTEGER             N1XTLP,      N2XTLP,      N3XTLP,      NINTPT
      INTEGER             NMBLK3,      NMBLK4,      NMONEL,      PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       IDUM
      COMMON / INF    /   N0EXT,       N3XTLP,      N2XTLP,      N1XTLP
      COMMON / INF    /   N0XTLP,      SPCCI,       CONFYZ,      PTHYZ
      COMMON / INF    /   FUEL1
C
      INTEGER             ORBSYM(MXINOR)
      COMMON / INF1   /   ORBSYM
C
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C
      INTEGER             HEADV(MAXBF),             IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             YBTV(MAXBF), YKTV(MAXBF)
      REAL*8              VALV(MAXBF,3)
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XBTV
      COMMON / SPECIAL/   YBTV,        YKTV,        IBF
c
c  ##  integer section
c
      integer conft1(*), conft2(*), CIST(2,2), CODE, CODEP
      integer dsym
      INTEGER FIRST
      integer indsym1(*),indxyz1(*), indsym2(*),indxyz2(*),index11(*),
     &        index21(*),index12(*),index22(*), IPTH1(2), ISGEXC,
     &        I1, I1END, I1START, ISYM
      integer J1, JKLSYM, JSYM
      INTEGER K1, KLSYM, KSG1, KSG1A, KSG2, KSG2F, KSYM
      INTEGER L1, LOOP8, LSYM
      INTEGER next, NMBSEG ,  SEGSM(2)
c
c  ##  real*8 section
c
      real*8 asymcontrib1, asymcontrib2
      REAL*8 CI11(*), CI21(*), CIYZ1(*), CI12(*), CI22(*), CIYZ2(*)
      REAL*8 d2_sym(3),d2_asym(3)
      real*8 symcontrib1, symcontrib2
      real*8 term_norm1,term_norm2, term_ex1, term_ex2, term_md1,
     &       term_md2
      real*8 VALUE1, VALUE2,      VALUE3
c
c  ##  logical section
c
      LOGICAL J1EQK1, JKLEQ
      logical K1EQL1
      LOGICAL LSTSEG
C
c
c-----------------------------------------------------------------
c
c mxinor = maximum number of internal orbitals
c     if(iter.lt.1) write(6,30000)
30000 format(' allin')
c  stuff from int4
c
c     read block from formula tape
c
c     write(*,*) 'entering allint ... '
c     write(*,*) 'ciyz1 : '
c     call writex(ciyz1,conft1(indxyz1(pthy+pthz)))
c     write(*,*) 'ciyz2 : '
c     call writex(ciyz2,conft2(indxyz2(pthy+pthz)))
c     write(*,*) 'ci11 : '
c     call writex(ci11,conft1(index11(segsm(1))))
c     write(*,*) 'ci21 : '
c     call writex(ci21,conft1(index21(segsm(2))))
c     write(*,*) 'ci12 : '
c     call writex(ci12,conft2(index12(segsm(1))))
c     write(*,*) 'ci22 : '
c     call writex(ci22,conft2(index22(segsm(2))))

       call int4ciuft(.true.,indxyz1,index11,index21,indxyz2,
     .     index12,index22,ipth1,segsm,pthy+pthz,nmbseg)

              dsym=symtab(ssym1,ssym2)
              loop8=1
              jump=1
              if(intorb.eq.2)return
c
              do 600 l1=2,intorb
c
              lsym=orbsym(l1)
c
              k1=l1
c
              ksym=lsym
              klsym=1
              k1eql1=.true.
c
                j1=k1
                  jsym=ksym
                  jklsym=ksym
                  j1eqk1=.true.
                  jkleq=.true.
c
c   j=k=l, i<>j
                   i1start=1
                   i1end=j1-1
                  do 900 i1=i1start,i1end
c
                      isym=orbsym(i1)
c this still holds
                      if(isym.ne.symtab(jklsym,dsym)) go to 1000
c                             write(6,30200)l1,lsym,k1,ksym,j1,jsym,i1,
c    +                        isym,klsym,jklsym,intorb
30200 format('allint lkji',20i4)
c
c     i1=j1=k1<l1 deferred until l1=k1=j1<i1
c
                      call wzero(3,d2_sym,1)
                      call wzero(3,d2_asym,1)
c                             write(6,30200)l1,lsym,k1,ksym,j1,jsym,i1,
c    +                        isym,klsym,jklsym,intorb
  100                 continue
c
c     formula tape
c
c     word8 contains code,wtlphd,mlppr,mlp
c
c     wtlphd   weight of the loop head
c     mlppr    weight of the bra part of the loop
c     mlp      weight of the ket part of the loop
c
c     code   1   new record
c            2   new vertex
c            3   new level(new integral)
c            4   val1*(ij/kl)+val2(jk/il)
c            5   val1*(ij/kl)+val2*(ik/jl)
c            6   val1*(ik/jl)+val2*(jk/il)
c            7   val*(ij/kl)
c            8   val*(ik/jl)
c            9   val*(jk/il)
c           10   val1*(ii/il)+val2*(il/ll)+val3*(i/h/l)
c
c     jump   1   z
c            2   y
c            3   x
c            4   w
c
c                     word8=lpbuf(loop8)
c                     call unplp(word8,code,idum,wtlphd,mlppr,mlp)
c                     loop8=loop8+1
            code=icodev(loop8)
            wtlphd=xbtv(loop8)
            mlppr=ybtv(loop8)
            mlp=yktv(loop8)
*@ifdef debug
*      write(6,30320)code,wtlphd,mlppr,mlp,loop8,
*     .              valv(loop8,1),valv(loop8,2),valv(loop8,3)
*     .             ,i1,l1
*@endif
30320 format('int4 code',5i4,'val(*)=',3f8.5,'i1,l1=',2i4)


              codep=code+1
            go to (10000,110,120,1100,130,130,130,140,140,140,
     +                150), codep
          call bummer('wrong code in allint, code=',code,2)
  110                 continue
c
c     read new block from formula tape
c
c                    stop 110
       call int4ciuft(.false.,indxyz1,index11,index21,indxyz2,
     .     index12,index22,ipth1,segsm,pthy+pthz,nmbseg)
                  loop8=1
                      go to 100
c
  120                 continue
c
                      jump=jump+1
                      loop8=loop8+1
                      go to 100
c
  130                 continue
c
                      value1=valv(loop8,1)
                      value2=valv(loop8,2)
ctm
                      value3=0.0d0
ctm
                      loop8=loop8+1
c
                      go to 160
c
  140                 continue
c
                      value1=valv(loop8,1)
ctm
                      value2=0.0d0
                      value3=0.0d0
ctm
                      loop8=loop8+1
c
c
                      go to 160
c
  150                 continue
c
                      value1=valv (loop8,1)
                      value2=valv (loop8,2)
                      value3=valv (loop8,3)
                      loop8=loop8+1
c
c
  160                 continue
c
c
c     write(6,32000)code,wtlphd,mlppr,mlp,jump
32000 format(' code',20i6)
       if(jump.le.2.and.first.gt.0)go to 100
       symcontrib1=zero
       symcontrib2=zero
       asymcontrib1=zero
       asymcontrib2=zero
       term_norm1=zero
       term_norm2=zero
       term_ex1=zero
       term_ex2=zero
c
ctm    the following stuff applies to the situation with at
ctm    least one x and one w segment

       if (nmbseg.eq.2) then
c     loop over ci segments
c     1,1 only when lstseg=.true.
c     2,1 always
c     2,2 when isgexc.lt.2
c
      if(lstseg)then
          ksg1a=1
          else
          ksg1a=2
      endif
                      do 620 ksg1=ksg1a,nmbseg
c
                          if(jump.gt.2)then
                              mlppr=mlppr-ipth1(ksg1)
                              if((mlppr+wtlphd).le.0.or.mlppr.ge.segsm(
     +                        ksg1))go to 605
                          endif
          if(ksg1.gt.1)then
              if(isgexc.lt.2)then
                  ksg2f=2
                  else
                  ksg2f=1
              endif
              else
              ksg2f=1
          endif
                          segsm1=segsm(ksg1)
                          cist1=cist(1,ksg1)
c
                          do 610 ksg2=1,ksg2f
c
                              if(jump.gt.2)then
c     xx and ww cases
                                  mlp=mlp-ipth1(ksg2)
                                  if((mlp+wtlphd).le.0.or.mlp.ge.segsm(
     +                            ksg2))go to 615
                                  segsm2=segsm(ksg2)
                                  cist2=cist(2,ksg2)
                              endif
c
                              if(ksg1.eq.ksg2)then
                                  if(ksg1.eq.1)then
c
c first half of the work
                                   call  alzyxw(indsym1,conft1,indxyz1,
     &                              indsym2,conft2,indxyz2,index11,ci11,
     &                              index12,ci12,ciyz1,ciyz2,term_norm1,
     &                              term_norm2)
c now second call with segments exchanged
c this also requires to exchange cist1 and cist2
                                   call exch(cist1,cist2)
                                   call  alzyxw(indsym2,conft2,indxyz2,
     &                              indsym1,conft1,indxyz1,index12,ci12,
     &                              index11,ci11,ciyz2,ciyz1,term_ex1,
     &                              term_ex2)
*@ifdef debug
*                                   term_md1=zero
*                                   term_md2=zero
*                                   call  alzyxw(indsym2,conft2,indxyz2,
*     &                             indsym1,conft1,indxyz1,index12,ci12,
*     &                             index11,ci11,ciyz2,ciyz1,term_md1,
*     &                             term_md2)
*                                   write(*,*)'allint case1',
*     &                              term_md1-term_md2
*@endif
                                   call exch(cist1,cist2)
                                  else
                                   call  alzyxw(indsym1,conft1,indxyz1,
     &                              indsym2,conft2,indxyz2,index21,ci21,
     &                              index22,ci22,ciyz1,ciyz2,term_norm1,
     &                              term_norm2)
c now second call with segments exchanged
c this also requires to exchange cist1 and cist2
                                   call exch(cist1,cist2)
                                   call  alzyxw(indsym2,conft2,indxyz2,
     &                              indsym1,conft1,indxyz1,index22,ci22,
     &                              index21,ci21,ciyz2,ciyz1,term_ex1,
     &                              term_ex2)
*@ifdef debug
*                                   term_md1=zero
*                                   term_md2=zero
*                                   call  alzyxw(indsym2,conft2,indxyz2,
*     &                             indsym1,conft1,indxyz1,index22,ci22,
*     &                              index21,ci21,ciyz2,ciyz1,term_md1,
*     &                              term_md2)
*                                   write(*,*)'allint case2',
*     &                              term_md1-term_md2
*@endif
                                   call exch(cist1,cist2)
                                  endif
                              else
                               call alzyxw(indsym1,conft1,indxyz1,
     &                          indsym2,conft2,indxyz2,index11,ci11,
     &                          index22,ci22,ciyz1,ciyz2,term_norm1,
     &                          term_norm2)
c now second call with segments exchanged
c this also requires to exchange cist1 and cist2
                               call exch(cist1,cist2)
                               call alzyxw(indsym2,conft2,indxyz2,
     &                          indsym1,conft1,indxyz1,index22,ci22,
     &                          index11,ci11,ciyz2,ciyz1, term_ex1,
     &                          term_ex2)
*@ifdef debug
*                               term_md1=zero
*                               term_md2=zero
*                               call alzyxw(indsym2,conft2,indxyz2,
*     &                          indsym1,conft1,indxyz1,index22,ci22,
*     &                          index11,ci11,ciyz2,ciyz1, term_md1,
*     &                          term_md2)
*                               write(*,*)'allint case3',
*     &                          term_md1-term_md2
*@endif
                               call exch(cist1,cist2)
                              endif
                            if(jump.le.2)go to 630
  615                       continue
                            mlp=mlp+ipth1(ksg2)
  610                     continue
  605                     continue
                          if(jump.gt.2)then
                              mlppr=mlppr+ipth1(ksg1)
                          endif
  620                 continue
  630                 continue
       elseif (nmbseg.eq.0) then
           call  alzyxw(indsym1,conft1,indxyz1,indsym2,conft2,
     .                  indxyz2,index11,ci11,index22,ci22,ciyz1,ciyz2,
     .                  term_norm1,term_norm2)
c now second call with segments exchanged
c this also requires to exchange cist1 and cist2
           call exch(cist1,cist2)
           call  alzyxw(indsym2,conft2,indxyz2,indsym1,conft1,
     .      indxyz1,index22,ci22,index11,ci11,ciyz2,ciyz1,
     .      term_ex1,term_ex2)
*@ifdef debug
*           term_md1=zero
*           term_md2=zero
*           call  alzyxw(indsym2,conft2,indxyz2,indsym1,conft1,
*     .      indxyz1,index22,ci22,index11,ci11,ciyz2,ciyz1,
*     .      term_md1,term_md2)
*           write(*,*)'allint case4',term_md1-term_md2
*@endif
           call exch(cist1,cist2)
       elseif (nmbseg.eq.1) then
            ksg1=1
            ksg2=1
           if(jump.gt.2)then
            mlppr=mlppr-ipth1(ksg1)
            if((mlppr+wtlphd).le.0.or.mlppr.ge.segsm(ksg1)) then
              mlppr=mlppr+ipth1(ksg1)
              goto 640
            endif
            mlp=mlp-ipth1(ksg2)
            if ((mlp+wtlphd).le.0.or.mlp.ge.segsm(ksg2)) then
              mlp=mlp+ipth1(ksg2)
              goto 640
           endif
           endif
           segsm1=segsm(ksg1)
           cist1=cist(1,ksg1)
           segsm2=segsm(ksg2)
           cist2=cist(2,ksg2)
           call  alzyxw(indsym1,conft1,indxyz1,indsym2,conft2,
     .                  indxyz2,index11,ci11,index12,ci12,ciyz1,ciyz2,
     .                  term_norm1,term_norm2)
c now second call with segments exchanged
c this also requires to exchange cist1 and cist2
           call exch(cist1,cist2)
           call  alzyxw(indsym2,conft2,indxyz2,indsym1,conft1,
     .                  indxyz1,index12,ci12,index11,ci11,ciyz2,ciyz1,
     .                  term_ex1,term_ex2)
*@ifdef debug
*           term_md1=zero
*           term_md2=zero
*           call  alzyxw(indsym2,conft2,indxyz2,indsym1,conft1,
*     .      indxyz1,index12,ci12,index11,ci11,ciyz2,ciyz1,
*     .      term_md1,term_md2)
*           write(*,*)'allint case5',term_md1-term_md2
*@endif
           call exch(cist1,cist2)
       else
        call bummer('allint, wrong number of segments',nmbseg,2)
       endif
c      write(*,*) 'alld2: l1,i1,lsym,isym,next=',
c    .                      l1,i1,lsym,isym,next
       symcontrib1=term_norm1+term_ex1
       symcontrib2=term_norm2+term_ex2
       call alld2(symcontrib1,symcontrib2,code,d2_sym,
     &  value1*half,value2*half,value3*half, jump)
c
       asymcontrib1=term_norm1-term_ex1
       asymcontrib2=term_norm2-term_ex2
       call alld2(asymcontrib1,asymcontrib2,code,d2_asym,
     &  value1*half,value2*half,value3*half, jump)
c
c     go to next loop
c
  640                 continue
                      go to 100
 1100                 continue
                      loop8=loop8+1
c*mdc*if debug
c*        write(*,*) 'allint: l1,i1,lsym,isym,next=',
c*     .                      l1,i1,lsym,isym,next,' d2(3)=',d2(3)
c*mdc*endif
c
c  ##  calculate the density matrix contribution
                      call aldn1(d2_sym,d2_asym,l1,i1, lsym,next,isym)
 1000                 continue
                      jump=1
c
c     end of do loops over internal orbitals
c
  900             continue
  800         continue
  700     continue
  600         continue
c
c     end of formula tape
c
10000         continue
      return
      end
c
c*******************************************************************
c
      subroutine alzyxw(indsym1,conft1,indxyz1,indsym2,conft2,
     .       indxyz2,index1,ci1,index2,ci2,ciyz1,ciyz2,scr1,scr2)
c
c     calculation of all internal elements
c
      implicit none  

C    VARIABLE DECLARATIONS FOR subroutine alzyxw
C
C     External functions
C
      EXTERNAL            ddot_wr
C
      REAL*8              ddot_wr
C
C
C     Argument variables
C
      INTEGER indsym1(*),conft1(*),indxyz1(*),indsym2(*)
      integer conft2(*),indxyz2(*),index1(*),index2(*)
C
C
      REAL*8              CI1(*),      CI2(*),   CIYZ1(2),     SCR1
      REAL*8              ciyz2(*),scr2
      real*8 scr1_md,scr2_md
C
C     Local variables
C
      INTEGER             M1,          M1LP,        M1LPR,       NMB1
      INTEGER             NMB1A,       NMB2,        NMB2A,       NWALK
      INTEGER             NY,          SY,          SYM
      integer             ny2,sy2
C
C
C     Common variables
C
      INTEGER             CIST1,       CIST2,       JUMP,        MLP
      INTEGER             MLPPR,       MLPPRZ,      SEGSM1,      SEGSM2
      INTEGER             WTLPHD
C
      COMMON / ALLIN1 /   WTLPHD,      MLP,         MLPPR,       SEGSM1
      COMMON / ALLIN1 /   SEGSM2,      CIST1,       CIST2,       JUMP
      COMMON / ALLIN1 /   MLPPRZ
C
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C    END OF VARIABLE DECLARATIONS FOR subroutine alzyxw


c     common /inf/pthz,pthy,pthx,pthw,nintpt,dimci,maxbl4,nmblk4,kbl4,
c    +mxkbl4,lamda1
c nmotx = maximum number of molecular orbitals
c mxinor = maximum number of internal orbitals
*@ifdef debug
*      write(*,*) 'entering alzyxw , m1lpr=',m1lpr
*@endif
      do 5200 m1=1,wtlphd
          m1lpr=mlppr+m1
c     write(6,32000)
32000 format(' alzyxw',20i6)
          if(jump.le.2)go to 521
          if(m1lpr.le.0)go to 520
          if(m1lpr.gt.segsm1)return
          nmb1a=index1(m1lpr)
          go to 523
  521     continue
          nmb1a=indxyz1(m1lpr)
  523     continue
          if(nmb1a.lt.0)go to 520
          nmb1=conft1(nmb1a)
          m1lp=mlp+m1
c     write(6,32010)m1lp,segsm2
32010 format(' all',20i6)
          if(jump.le.2)go to 522
          if(m1lp.le.0)go to 520
          if(m1lp.gt.segsm2)return
          nmb2a=index2(m1lp)
          go to 524
  522     continue
          nmb2a=indxyz2(m1lp)
  524     continue
          if(nmb2a.lt.0)go to 520
          nmb2=conft2(nmb2a)
          if(jump.le.2)go to 525
          nmb1=nmb1-cist1
          nmb2=nmb2-cist2
  525     sy=indsym1(nmb1a)
          sy2=indsym2(nmb2a)
          sym=sym21(sy)
c         write(6,30060)mlppr,m1lpr,mlp,m1lp,nmb1,nmb2,sym,jump,cist1,
c    +    cist2
30060 format(' 510',20i6)
          go to (600,610,630,750),jump
  600     continue
c
c     walks passing through z
c
c         write(6,30000)ciyz1(nmb1+1),ciyz2(nmb2+1),scr1
30000 format(' zwalks',3f10.6)
          scr1=scr1+0.5d0*ciyz1(nmb1+1)*ciyz2(nmb2+1)
          scr2=scr2+0.5d0*ciyz1(nmb1+1)*ciyz2(nmb2+1)
*@ifdef debug
*          scr1_md=0.5d0*ciyz1(nmb1+1)*ciyz2(nmb2+1)
*          scr2_md=0.5d0*ciyz1(nmb1+1)*ciyz2(nmb2+1)
*          write(*,*)'allint casez',scr1_md,scr2_md
*@endif
          go to 520
c
c     walks passing through y
c
  610     continue
          ny=nbas(sym)
          ny2=nbas(sy2)
c         write(6,30061)mlppr,m1lpr,m1,nmb1,nmb2
30061  format('mlppr=',i6,' m1lpr=',i6,' m1=',i6,'nmb1,nmb2=',2i6)

          if(ny.eq.0)go to 520
c         write(6,30010)ny
30010 format(' ywalks',20i4)
c         call writex(ciyz1(nmb1+1),ny)
c         call writex(ciyz2(nmb2+1),ny2)
          scr1=scr1+0.50d0*ddot_wr(ny,ciyz1(nmb1+1),1,ciyz2(nmb2+1),1)
*@ifdef debug
*          scr1_md=0.50d0*ddot(ny,ciyz1(nmb1+1),1,ciyz2(nmb2+1),1)
*          write(*,*)'allint casey',scr1_md
*@endif
          go to 520
c
c     walks passing through x
c
  630     continue
          nwalk=cnfx(sym)
c         write(6,30020)nwalk,sym
30020 format(' xwalks',20i4)
c         call writex(ci1(nmb1+1),nwalk)
c         call writex(ci2(nmb2+1),nwalk)
          if(nwalk.eq.0)go to 520
          scr1=scr1+0.5d0*ddot_wr(nwalk,ci1(nmb1+1),1,ci2(nmb2+1),1)
*@ifdef debug
*          scr1_md=0.5d0*ddot(nwalk,ci1(nmb1+1),1,ci2(nmb2+1),1)
*          write(*,*)'allint casex',scr1_md
*@endif
          go to 520
c
c     walks passing through w
c
  750     continue
          nwalk=cnfw(sym)
c         write(6,30030)nwalk,sym
30030 format(' wwalks',20i4)
c         call writex(ci1(nmb1+1),nwalk)
c         call writex(ci2(nmb2+1),nwalk)
          if(nwalk.eq.0)go to 520
          scr1=scr1+0.5d0*ddot_wr(nwalk,ci1(nmb1+1),1,ci2(nmb2+1),1)
*@ifdef debug
*          scr1_md=0.5d0*ddot(nwalk,ci1(nmb1+1),1,ci2(nmb2+1),1)
*          write(*,*)'allint casew',scr1_md
*@endif
c
*@ifdef debug
*  520  write(6,30040)scr1,scr2
* 5200 continue
*@ else
 520   continue
 5200  continue
*@endif
30040 format(' scr1',f13.9,' scr2',f13.9)
      return
      end




       subroutine countindex(info,index,len)
       integer len,index(len)
       character*(*) info
       integer i ,sum, sumsq
       logical skipvecnot

         sum=0
         skipvecnot=.false.
         sumsq=0
         do i=1,len
            sum=sum+index(i)
            sumsq=sumsq+index(i)*index(i)
            if (index(i).lt.-1) skipvecnot=.true.
         enddo
c       write(6,*) 'countindex: info=',info,'sum=',sum,' sumsq=',sumsq
c       write(6,*) 'skipvectornotation=',skipvecnot

        return
        end


      subroutine datain
c
c  read user input.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer      tape6
      equivalence (tape6,nunits(1))
      integer      tape5
      equivalence (tape5,nunits(2))
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      character*60 fnamex
      equivalence (fname(nfilmx),fnamex)
c
      integer nmotx
      parameter (nmotx=1023)

c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

       integer ierr,i,j
c
*@ifndef nonamelist
      namelist /input/ lvlprt, nroot1,nroot2
     . , method,drt1,drt2,cirestart
*@endif
c
      ierr = 0
c
c
      open( unit = tape5, file = fname(2), status = 'unknown')
c
c     read and echo standard input until eof is reached.
c
      write(tape6,6010)
6010  format(' echo of the input for program transci:')
      call echoin( tape5, tape6, ierr )
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind tape5

c defaults
       nroot1=0
       nroot2=0
       lvlprt=0
       method=0
       drt1=0
       drt2=0
       cirestart=0
c
c     # read namelist input and keep processing if eof is reached.
c
c
*@ifdef nonamelist
*@elif defined  nml_required
*      read( tape5, nml=input, end=3 )
*@else
      read( tape5, input, end=3 )
*@endif
c
3     continue
c
      if (nroot1.eq.0)
     . call bummer (' invalid value for nroot1 ',nroot1,2)
      if (nroot2.eq.0)
     . call bummer (' invalid value for nroot2 ',nroot2,2)
      if (drt1.eq.0)
     . call bummer (' invalid value for drt1 ',drt1,2)
      if (drt2.eq.0)
     . call bummer (' invalid value for drt2 ',drt2,2)

      if (method.eq.0) then

          write(6,*) 'calculating the MR-SDCI transition density'

c     CEPA(0)

      elseif (method.eq.1) then

          write(6,*) 'calculating the CEPA(0) transition density'

c     ACPF

      elseif (method.eq.2) then

           write(6,*) 'calculating the ACPF transition density'

c     AQCC

      elseif (method.eq.3) then

            write(6,*) 'calculating the AQCC transition density'

      else
      call bummer ('method not implemented, method=',method,2)
      endif

c     # filev1 = bra vector file.
      nunits(7)=4+drt1
      write(fname(7),'(a9,i1)') 'civfl.drt',drt1
c------------------------------------------------------------------
c     # filev2 = ket vector file.
      nunits(8)=4+drt2
      write(fname(8),'(a9,i1)') 'civfl.drt',drt2
c------------------------------------------------------------------
c     # fildrt1 = bra drt file.
      nunits(9)=17
      write(fname(9),'(a8,i1)') 'cidrtfl.',drt1
c------------------------------------------------------------------
c     # fildrt2 = ket drt file.
      nunits(10)=18
      write(fname(10),'(a8,i1)') 'cidrtfl.',drt2

      nunits(16)=20
      write(fname(16),'(a10,i1)') 'civout.drt',drt1
c------------------------------------------------------------------
      nunits(17)=21
      write(fname(17),'(a10,i1)') 'civout.drt',drt2

c     # cirefv1 = bra ci reference vector (sequential)
      nunits(22)=26
      write(fname(22),'(a10,i1)') 'cirefv.drt',drt1
c------------------------------------------------------------------
c     # cirefv2 = ket ci reference vector  (sequential)
      nunits(23)=27
      write(fname(23),'(a10,i1)') 'cirefv.drt',drt2

      write(tape6,*)'units and filenames:'
c
      do 110 i = 1, (nfilmx - 1)
         if ( nunits(i) .eq. 0 ) goto 110
111      continue
         write(tape6,6050) i, nunits(i), fname(i)
110   continue
6050  format(1x,i4,': (',i2,')',4x,a)
6051  format(1x,i4,': (',i2,')',4x,a,t35,a)
c
      write(tape6,6020)
      close(unit=tape5)
c
      return
6020  format(1x,72('-'))
      end


      subroutine dcopym(n,a,b)
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine dcopym
C
C     Argument variables
C
      INTEGER             N
C
      REAL*8              A(*),        B(*)
C
C     Local variables
C
      INTEGER             I
C
C    END OF VARIABLE DECLARATIONS FOR subroutine dcopym


      do 10 i=1,n
          b(i)=-a(i)
   10 continue
      return
      end
c
c***************************************************************
c
      subroutine densi(nmot, nmsym,core,lfree,lavail)
c
c     calculation of the first and second order density matrix
c
       implicit none
c  ##  parameter & common block section
c
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             MXSYM
      PARAMETER           (MXSYM = 8)
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      integer             mxnseg
      parameter           (mxnseg = 100)
      integer   ninfmx
      parameter(ninfmx=10)
c   SIF related
      integer nbfd2,lend1e, n1edbf
      parameter(nbfd2=4096,lend1e=2047,n1edbf=1636)
C
      INTEGER             NUNITS(NFILMX)
      COMMON / CFILES /   NUNITS
      integer tape6,civfl1,civfl2,cirefv1,cirefv2,cirfl1,cirfl2,cid1trfl
      equivalence (nunits(1),tape6)
      equivalence (nunits(7),civfl1)
      equivalence (nunits(8),civfl2)
      equivalence (nunits(22),cirefv1)
      equivalence (nunits(23),cirefv2)
      equivalence (nunits(27),cirfl1)
      equivalence (nunits(28),cirfl2)
      equivalence (nunits(34),cid1trfl)
C
      CHARACTER*60        FNAME(NFILMX)
      COMMON / CFNAME /   FNAME
C
      INTEGER             BLXT(8),     N2OFFR(8),   N2OFFS(8),   N2ORBT
      INTEGER             NOFFR(8),    NOFFS(8),    NSGES(8) , nsdim(8)
      integer    dkntin (mxsym*(mxsym+1)/2)
      integer    densoff(mxsym*(mxsym+1)/2)
      integer    dsize
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   dsize,densoff,dkntin

C
      INTEGER             EQVSYM(MXSYM),            IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(MXSYM),            NFRC(MXSYM)
      INTEGER             NFRV(MXSYM), NMUVAL,      NOCC(MXSYM)
      INTEGER             NORBSM(MXSYM),            NOXT(MXSYM)
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
      CHARACTER*4         LABELS(8)
      COMMON / SLABEL /   LABELS
C
      INTEGER             CIST(2,MXNSEG), CIVCST(2,MXNSEG)
      INTEGER             INDXST(2,MXNSEG), LENCI(2),  NSEG
      INTEGER             NSEGW, SEGCI(2,MXNSEG)
      INTEGER             SEGEL(2,MXNSEG) ,recci
      COMMON / CIVCT  /   LENCI,       NSEG,        NSEGW, recci,SEGEL
      COMMON / CIVCT  /   CIST,        INDXST,      CIVCST,      SEGCI

C
      INTEGER             MULT(8,8)
      COMMON / CSYM   /   MULT
c
      REAL*8 d1_sym(iprpmx),d1ci_sym(iprpmx),d1_asym(iprpmx),
     &       d1ci_asym(iprpmx)
      COMMON / DEN1P  / d1_sym, d1ci_sym, d1_asym, d1ci_asym
c
      integer  ncsf1,ncsf2,refvec1,refvec2
      real*8  eci1,eci2,a4den1,a4den2
      common   /civoutinfo/ ncsf1,ncsf2,refvec1,refvec2,
     .                      eci1,eci2,a4den1,a4den2
c
      real*8  refphase1(100),refphase2(100)
      real*8  ciphase1(100),ciphase2(100)
      integer cicnt1,cicnt2,rcnt1(100),rcnt2(100)
      common  /phase/rcnt1,rcnt2,refphase1,refphase2,
     .                cicnt1,cicnt2,ciphase1,ciphase2
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
c  ##  integer section
c
      integer cpt(5)
      integer dsym
      integer ios,iroot1,iroot2,isym,icnt1,i,ietype,info(ninfmx),
     &        ibvtyp,iitype,ifmt1,ierr,idummy,irm1(nmotx),
     &        irm2(nmotx),itypea, itypeb
      integer loc1,loc2,loc3,lfree,lavail,lsym,l1rcal,l1rc,l2rc
      integer nmot, nmsym,nenrgy,ntitld,n1mx,nmap,n2mx,map(nmotx)
c
c  ##  real*8 section
c
      real*8 curnorm1,curnorm2,core(*)
      real*8 energy
      real*8 fcilrt1,fcilrt2
      real*8 phasefac
      real*8 refnorm1,refnorm2
c
c  ##  character section
c
      character*80 title(20)
      character*8 bfnlab(nmotx)
c
c  ##  logical section
c
      logical ex
c
c  ##  external section
c
      integer forbyt, atebyt
      external forbyt, atebyt
c
c
c---------------------------------------------------------------------
c
c # setupone replaces tapinn
c #  and sets up ivout(*), /offs/ and /inf4x/

      call setupone

c
c zero out the den1 array
c
      call wzero(n2orbt,d1_sym,1)
      call wzero(n2orbt,d1_asym,1)
c
c********* compute first and second order density matrices *************
c
c
c     calculation of density matrices done in multd2.
c
      inquire(file=fname(7),exist=ex)
      if ( .not. ex )
     . call bummer('civfl1 does not exist',civfl1,2)
      inquire(file=fname(8),exist=ex)
      if ( .not. ex )
     . call bummer('civfl2 does not exist',civfl2,2)
c     call openda(civfl1,fname(7),8*4096,'keep','random')
      if (fname(7).eq.fname(8)) then
        write(6,*) 'civfl1 and civfl2 refer to the same file'
        civfl2=civfl1
c     else
c      call openda(civfl2,fname(8),8*4096,'keep','random')
      endif

c
      call multd2(nroot1,nroot2,core,fname(7),fname(8))
c
c     call closda(civfl1,'keep',4)
c     if (fname(7).ne.fname(8))
c    . call closda(civfl2,'keep',4)

c####################################################################
         icnt1 = 0
         dsym=mult(ssym1,ssym2)
         do i = 1,nmbpr(dsym)
c
c         isym >= lsym
c
            isym=lmdb(dsym,i)
            lsym=lmda(dsym,i)
            if (isym.eq.lsym) then
            icnt1=icnt1+ (nsdim(isym)*(nsdim(isym)+1)/2)
            else
            icnt1=icnt1+nsdim(isym)*nsdim(lsym)
            endif
         enddo

       if (method.ne.0) then

c
c   save the unscaled CI transition density
c
         call dcopy_wr(icnt1,d1_sym,1,d1ci_sym,1)
         call wzero(icnt1,d1_sym,1)
         call dcopy_wr(icnt1,d1_asym,1,d1ci_asym,1)
         call wzero(icnt1,d1_asym,1)
c
c   if AQCC type methods, copy the two ci reference vectors
c   form cirefv1 and cirefv2 (sequential) to cirfl1 and cirfl2
c   (random access) and calculate the transition density
c   note, that ciudg creates a reference vector file only for
c   ntype=3, and there is only ONE! vector on it
c   there may, however, be several vectors on the ci vector
c   file, but only the lowest (or the one root following
c   has been applied to ) is valid - this is NOT necessarily
c   the one given with the froot option. Everything else is
c   meaningless!!
c
      inquire(file=fname(22),exist=ex)
      if ( .not. ex )
     . call bummer('cirefv1 does not exist',cirefv1,2)
      open( unit = cirefv1, file = fname(22), status = 'old' ,
     +       form = 'unformatted',iostat=ios )
      if (ios.ne.0)
     . call bummer ('can not open unit ',cirefv1,2)

c     call openda(cirfl1,fname(27),8*4096,'keep','random')
      call rdcirefv(
     & tape6,        cirefv1,    fname(27),   ncsf1,
     & core(lfree),  refphase1,  rcnt1,    refvec1,
     & cicnt1,       1)
c     close(cirefv1)

      inquire(file=fname(23),exist=ex)
      if ( .not. ex )
     . call bummer('cirefv2 does not exist',cirefv2,2)
      open( unit = cirefv2, file = fname(23), status = 'old' ,
     +       form = 'unformatted',iostat=ios )
c     call openda(cirfl2,fname(28),8*4096,'keep','random')
      call rdcirefv(
     & tape6,        cirefv2,     fname(28),    ncsf2,
     & core(lfree),  refphase2,   rcnt2,     refvec2,
     & cicnt2,       2)
c     close(cirefv2)
c
      call multd2(1,1,core,fname(27),fname(28))

      call closda(cirfl1,'keep',4)
      call closda(cirfl2,'keep',4)

      fcilrt1=sqrt(a4den1)
      fcilrt2=sqrt(a4den1)

      write(6,1001)

      call pstclc(nmsym, core, lavail,0)

      call clcmom(nmsym,core,lavail,0)
c
c  #  symmetric 1e-density
      write(6,1002)
      call dcopy_wr(icnt1,d1_sym,1,core,1)
      call dcopy_wr(icnt1,d1ci_sym,1,d1_sym,1)
c  #  frozen core orbitals contribute to transition density
      call pstclc(nmsym, core(icnt1+1), lavail-icnt1,0 )
      call clcmom(nmsym,core(icnt1+1),lavail-icnt1,0)
      call dcopy_wr(icnt1,core,1,d1_sym,1)
c
c  #  antisymmetric 1e-density
      write(6,1002)
      call dcopy_wr(icnt1,d1_asym,1,core,1)
      call dcopy_wr(icnt1,d1ci_asym,1,d1_asym,1)
      call pstclc(nmsym, core(icnt1+1), lavail-icnt1,1 )
      call calc_lvalue(nmsym,core,lavail,0)
      call dcopy_wr(icnt1,core,1,d1_asym,1)
c
c    # determine the phasefactor
c
       call calcphase(phasefac)

      call dscal_wr(icnt1,phasefac*(1.0d0-a4den1),d1_sym,1)
      call dscal_wr(icnt1,phasefac*(1.0d0-a4den1),d1_asym,1)
c     add scaled ci density to mcscf density
      call daxpy_wr( icnt1 ,a4den1,d1ci_sym,1,d1_sym,1)
      call daxpy_wr( icnt1 ,a4den1,d1ci_asym,1,d1_asym,1)

       write(6,*) 'AQCC transition density: state 1 is the ',
     .   'reference state.'
       write(6,*)
     .  'AQCC t-density= ci_den*a4den+phasefactor*(1-a4den)*mc_den'
       write(6,995)(1.0d0-a4den1)*phasefac
       write(6,996) fcilrt1*fcilrt2
       write(6,1003)

       else
        write(6,1004)
       endif
c
c  ##  write out the 1-e antisymmetric density matrix
c
      open( unit = cid1trfl,file = fname(34), status = 'unknown' ,
     & form = 'unformatted' )
c
      nenrgy=1
      nmap=0
      ietype=-1
      energy=10.0
      do i = 1,nmot
       write (bfnlab(i),fmt='(a5,i3.3)') 'tout:',i
       map(i) = i
cmd
c      if (iout(i).eq.-1) map(i)=-1
cmd
      enddo
      ntitld=1
      title(1)="symmetric and antisymmetric 1-e density matrix"
c  ##  SIF parameters
      call izero_wr(ninfmx,info,1)
c     # info(1) is the fsplit parameter
      info(1) = 2
c     # ignore any bit vectors
      ibvtyp = 0
c     # info(2) and info(3) are the record length parameters for cid1fl
c
c     # the following block is to make sure that they are consistent
c     # let sifcfg decide the buffer length info
      l1rcal = nbfd2
c     # integral type is 1-electron
      iitype = 1
      call sifcfg(iitype,l1rcal,nmot,ibvtyp,ifmt1,l1rc,n1mx,ierr)
      if (ierr.ne.0) call bummer(' densi: from sifcfg:1, ierr=',ierr,
     & faterr)
c     # info(2) and info(3) are the record length parameters for cid1trf
      info(2) = nbfd2
      info(3) = n1mx
c     # info(4) and info(5) are the record length parameters for the
c     #  2-e arrays (no meaning here)
      l1rcal = nbfd2
c     # integral type is 2-electron
      iitype = 2
      call sifcfg(iitype,l1rcal,nmot,ibvtyp,ifmt1,l2rc,n2mx,ierr)
      if (ierr.ne.0) call bummer(' densi: from sifcfg:1, ierr=',ierr,
     & faterr)
      info(4) = nbfd2
      info(5) = n2mx
      info(6) = ifmt1
c
      call sifwh(
     & cid1trfl,   ntitld,    nmsym,    nmot,
     & ninfmx,     nenrgy,    nmap,     title,
     & nbfnsm,     labels,    info,     bfnlab,
     & ietype,     energy,    idummy,   idummy,
     & ierr)
      if (ierr.gt.0) call bummer("error in sifwh for cid1trfl",
     & cid1trfl,faterr)
c
c   | 1:buf,2:labbuf,3:valbuf
      cpt(1)=1
      cpt(2)=cpt(1)+atebyt(lend1e)
      cpt(3)=cpt(2)+forbyt(2*n1edbf)
      cpt(4)=cpt(3)+atebyt(lend1e)
      if (cpt(4).gt.lavail) call bummer(
     & "Not enough memory allocated",cpt(4),faterr)
      irm1(1) = 1
      irm2(1) = nbfnsm(1)
      do i = 2,nmsym
         irm1(i) = irm1(i-1) + nbfnsm(i-1)
         irm2(i) = irm2(i-1) + nbfnsm(i)
      enddo
c
c  ##  write out the d1_sym in sif format
      itypea=0
      itypeb=7
      call wtden1(
     & cid1trfl,      info,          lend1e,        n1edbf,
     & core(cpt(1)),  core(cpt(2)),  core(cpt(3)),  d1_sym,
     & nmsym,         irm1,          irm2,          map,
     & itypea,        itypeb,        1)
c
c  ##  write out the d1_asym in sif format
      itypea=2
      itypeb=9
      call wtden2(
     & cid1trfl,      info,          lend1e,        n1edbf,
     & core(cpt(1)),  core(cpt(2)),  core(cpt(3)),  d1_asym,
     & nmsym,         irm1,          irm2,          map,
     & itypea,        itypeb,        2)
c
c  ##
c  ##  calculate <r> and <L> values
c  ##
c
c  #  print out d1_sym (pstclc call with code=0)
      call pstclc(nmsym, core, lavail,0)

c  #   calculate transition dipole moment components
      call clcmom(nmsym,core,lavail,1)
c
c  #  print out d1_asym (pstclc call with code=1)
      call pstclc(nmsym, core, lavail,1)
c
c  #   calculate <L> value components
      call calc_lvalue(nmsym,core,lavail,1)

 995   format(' MCSCF transition density is scaled by ',f9.6)
 996   format(' CI transition density is scaled by ',f9.6)
 1001  format(// 1x,20('<<'),20('>>')//,
     .        10x,' Results derived from the unscaled ',
     .           ' reference space density'//,1x,20('<<'),20('>>')//)
 1002  format(// 1x,20('<<'),20('>>')//,
     .        12x,' Results derived from the unscaled ',
     .           ' pseudo CI density'//,1x,20('<<'),20('>>')//)
 1003  format(// 1x,20('<<'),20('>>')//,
     .        15x,' Results derived from the properly scaled ',
     .           ' AQCC density'//,1x,20('<<'),20('>>')//)
 1004  format(// 1x,20('<<'),20('>>')//,
     .        21x,' Results derived from the ',
     .           ' CI density'//,1x,20('<<'),20('>>')//)

      return
      end
c
c**********************************************************************
c
       subroutine diaden( indsym1,conft1,index1,ci1,indxyz1,
     .                    ciyz1,indsym2,conft2,index2,ci2,indxyz2,
     .                    ciyz2,d2il,nmbseg)

      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine diaden
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 32000)
C
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
C
C     Argument variables
C
      INTEGER indsym1(*),conft1(*),index1(*),indxyz1(*)
      INTEGER       indsym2(*),conft2(*),index2(*),indxyz2(*)
      INTEGER       nmbseg
C
      REAL*8              CI1(2),CI2(2),CIYZ1(*),D2IL(*)
      REAL*8              ciyz2(*)
C
C     Local variables
C
      INTEGER             CODE,        CODEP,       I1,          L1
      INTEGER             LOOP8,nmbsegd
C
      REAL*8              D2(2),       SCR1,        VAL1
      REAL*8              VAL2
C
C
C     Common variables
C
      INTEGER             CISTRT(2),   FIRST,       IPTH1,       IPTH2
      INTEGER             IWX,         JUMP,        MLP,         SEGSUM
      INTEGER             WTLPHD
C
      COMMON / DIAG   /   IPTH1,       IPTH2,       IWX,         CISTRT
      COMMON / DIAG   /   FIRST,       SEGSUM,      WTLPHD,      MLP
      COMMON / DIAG   /   JUMP
C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             IDUM(3),     IDUM1(9),    INTORB,      KBL3
      INTEGER             KBL4,        LAMDA1,      MAXBL2,      MAXBL3
      INTEGER             MAXBL4,      MAXLP3,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       ND0EXT
      INTEGER             ND2EXT,      ND4EXT,      NEXT,        NINTPT
      INTEGER             NMBLK3,      NMBLK4,      NMONEL,      PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI
      integer             idum2(7)
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        IDUM,        ND4EXT,      ND2EXT
      COMMON / INF    /   ND0EXT,      IDUM1,       SPCCI,       CONFYZ
      COMMON / INF    /   PTHYZ , idum2
C
C
C     Common variables
C
      INTEGER             HEADV(MAXBF),             IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             YBTV(MAXBF), YKTV(MAXBF)
C
      REAL*8              VALV(MAXBF,3)
C
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XBTV
      COMMON / SPECIAL/   YBTV,        YKTV,        IBF
C
C    END OF VARIABLE DECLARATIONS FOR subroutine diaden


      loop8=1
      nmbsegd=min(nmbseg,1)
      call diagciuft(.true.,indxyz1,index1,indxyz2,index2,
     .  ipth1,segsum,pthz+pthy,nmbsegd)

      do 2100 l1=1,intorb
            i1=l1
c
              call wzero(2,d2,1)
cgg2
              jump=1
 1510         continue
c
c     loop8 counts buffer for formula tape in real*8 words
c     word8        real*8    , consists of
c     code 4 bits,  wtlphd 20 bits, mlp 4 bytes
c
c     code = 0 end of formula tape
c            1 new record
c            2 vertex is changed (z-y or y-x or x-w)
c            3 new set of indices i,l (new pair of integrals) and
c              start with vertex z
c            4 11a,b val1*(ii/ll)+val2*(il/il)
c            5 14a,b val*(-0.5*(ll/ll)+(l/h/l))
c            6 14b (l/h/l)*val
c            7 14a,b val*(0.5*(ll/ll)+(l/h/l))
c
c     jump = 1   z
c            2   y
c            3   x
c            4   w
c
c             word8=lpbuf(loop8)
c             call unplp(word8,code,idum1,wtlphd,idum2,mlp)
               code=icodev(loop8)
               wtlphd=xbtv(loop8)
               mlp=ybtv(loop8)
c     write(6,30320)code,wtlphd,mlp,loop8,jump,segsum,ipth1,
c    .              valv(loop8,1),valv(loop8,2)
c     write(6,30319) l1,i1
30319 format(' l1=',i4,'i1=',i4)
30320 format('diag code',7i4,'val(*)=',2f8.5)
c             loop8=loop8+1
              codep=code+1
              go to (10000,1520,1530,2199,1550,1550,1550,1550),codep
 1520         continue
      call diagciuft(.false.,indxyz1,index1,indxyz2,index2,
     . ipth1,segsum,pthz+pthy,nmbsegd)
               loop8=1
              go to 1510
 1530         continue
              jump=jump+1
              loop8=loop8+1
              go to 1510
 1550         continue
c             if(code.eq.4)then
                  val1=valv(loop8,1)
                  val2=valv(loop8,2)
                  loop8=loop8+1
c                 else
c                 val1=valv(loop8,1)
c                 val2=valv(loop8,2)
c                 endif
              if(jump.le.2)then
                  if(first.gt.0)go to 1510
                  else
c
c     check whether segment is available
c
                  mlp=mlp-ipth1
                  if((mlp+wtlphd).le.0.or.mlp.ge.segsum)go to 1510
              endif
              call aldlp(indsym1,conft1,indxyz1,index1,ci1,ciyz1,
     +        indsym2,conft2,indxyz2,index2,ci2,ciyz2,scr1)
              call diagd2(scr1,d2,code,half*val1,half*val2,d2il,i1,l1)
c     write(6,30020)val1,val2,scr1,d2
30020 format('d val',5f10.6)

c
c     go to next loop
c
              go to 1510
c
c     end of do loops over internal orbitals
c
 2199     loop8=loop8+1
 2200     continue
 2100 continue
c
c     end of formula tape
c
10000 continue
      return
      end


       subroutine diagciuft(initial,indxyz1,index1,indxyz2,
     .  index2,ipth1,segsm,ipthyz,nmbseg)


c
c  construct the diagonal formula tape.
c
c  two-internal:
c  code  loop type      description
c  ----  ---------     ------------------
c
c   0  -- new level --
c   1  -- new record --
c   2  -- new vertex --
c
c
c  four-internal:
c  code  loop type      description
c  ----  ---------     ------------------
c
c   1  -- new record --
c   2  -- new vertex --
c   3  -- new level --
c
c   4       11ab        eiill, eilil  (z,y,x,w)
c   5       14b         eil           (z,y,x,w case=0)
c   6       14b         eil           (z,y,x,y case=1,2)
c   7       14b         eil           (z,y,x,y case=3)
c
c  cleanup:
c  code=0  -- end of diagonal ft --
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine diagciuft
C
C     Parameter variables
C
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 32000)
C
C     Argument variables
C
      LOGICAL             INITIAL
       integer indxyz1(*), index1(*),ipth1(2),segsm(2),
     . ipthyz,nmbseg,indxyz2(*),index2(*)

C
C     Local variables
C
C
C
C
C     Common variables
C
      INTEGER             DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER             DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER             DB13(4,0:2), DB14(2,0:2,2)
      INTEGER             DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER             DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER             DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER             DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON / CDB    /   DB1,         DB2,         DB3,         DB4
      COMMON / CDB    /   DB4P,        DB5,         DB6,         DB7
      COMMON / CDB    /   DB8,         DB10,        DB11,        DB12
      COMMON / CDB    /   DB13,        DB14
C
C
C     Common variables
C
      INTEGER             KNTFT,       NFTB,        NFTBD,       NLOOP
C
      REAL*8              FTBUF(LBFT)
C
      COMMON / CFTB   /   FTBUF,       KNTFT,       NLOOP,       NFTB
      COMMON / CFTB   /   NFTBD
C
C
C     Common variables
C
      INTEGER             CODE(4),     HILEV,       MO(0:4),     NIOT
      INTEGER             NMO,         NUMV
C
      LOGICAL             NINT,        QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT,        NUMV,        HILEV
      COMMON / CLI    /   CODE,        MO,          NMO,         NINT
C
C
C     Common variables
C
      INTEGER             QPRT
C
      COMMON / CPRT   /   QPRT
C
C
C     Common variables
C
      INTEGER             ST1(8),      ST10(6),     ST11(4)
      INTEGER             ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER             ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER             ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON / CST    /   ST1,         ST2,         ST3,         ST4
      COMMON / CST    /   ST4P,        ST5,         ST6,         ST7
      COMMON / CST    /   ST8,         ST10,        ST11D,       ST11
      COMMON / CST    /   ST12,        ST13,        ST14
C
C
C     Common variables
C
      INTEGER             VTPT
C
      REAL*8              OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON / CVTRAN /   VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
C
C     Common variables
C
      INTEGER             BTAIL,       CLEV,        ILEV,        IMO
      INTEGER             JKLSYM,      JLEV,        JMO
      INTEGER             JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER             KMO,         KTAIL,       LLEV,        LMO
      INTEGER             LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
C
C     Common variables
C
      INTEGER             HEADV(MAXBF),             IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             YBTV(MAXBF), YKTV(MAXBF)
C
      REAL*8              VALV(MAXBF,3)
C
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XBTV
      COMMON / SPECIAL/   YBTV,        YKTV,        IBF
C
C    END OF VARIABLE DECLARATIONS FOR subroutine diagciuft


      ibf=0
       if (initial) jumpinposition=0

      goto (600,1,2,3,4 ) (jumpinposition+1)
c
c
c     # four-internal diagonal loops:
c
 600   continue

c  initialisation

      break=.false.
      mo(0) = 0
      nloop = 0
      nftbd = 0
      kntft = 0
      qdiag = .true.
      qprt=0

      lmo=1
 500  continue
        imo=lmo
 301     if(qprt.ge.5)write(6,1010)lmo,lmo
c
c        # 14b loops:
c
         nmo = 1
         mo(1) = lmo
c
         numv = 1
         code(1) = 5
         code(2) = 6
         code(3) = 7
         vtpt = 1
c
         ver=1
400      continue
 1           call loop3in( ver , ver , st14, db14(1,0,2), 2, 0,
     .      indxyz1, index1, index1,indxyz2,index2,index2,
     .      ipth1,segsm,ipthyz,nmbseg,2)

            if (break) then
              jumpinposition=1
              return
            endif

 2           if (ver.lt.4) then
              ibf=ibf+1
              if (ibf.eq.maxbf-1) then
               jumpinposition=2
               icodev(ibf)=1
               return
              endif
              icodev(ibf)=2
             endif
          ver=ver+1
          if (ver.lt.5) goto 400
 3           if (lmo.lt.niot) then
              ibf=ibf+1
               if (ibf.eq.maxbf-1) then
                jumpinposition=3
                icodev(ibf)=1
                return
               endif
              icodev(ibf)=3
             endif
c
          lmo=lmo+1
          if (lmo.le.niot) goto 500
 4            ibf=ibf+1
               if (ibf.eq.maxbf-1) then
                jumpinposition=4
                icodev(ibf)=1
                return
               endif

              icodev(ibf)=0
      jumpinposition=0
      write(6,6010)nloop
6010  format(/' diagonal ft construction complete.',
     + i8,' loops constructed.')
c
      return
1000  format(' diag: 2-intnl  lmo = ',i4)
1010  format(' diag: 4-intnl  lmo,imo = ',2i4)
      end
c
c********************************************************************
c
      subroutine diagd(
     & nmsym,    indsym1,  conft1,   indxyz1,  ciyz1,
     . indsym2,  conft2,   indxyz2,  ciyz2,    index11,
     & ci11,     index21,  ci21,     index12,  ci12,
     & index22,  ci22,     d2il,     scr1,     scr_asym,
     & tran1,    tran2,    segsm,    cist1,    ipth,
     & frst,     lstseg,   nmbseg,   iwxx)
c
c     all internal diagonal contributions to the one- and two-particle
c     density matrix and
c     all external contributions to the one-particle density matrix
c
c     nmsym    number of irreps
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz     ci vector for y and z walks
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     d2il(*)  collects the all internal (ii/ll) and (il/il)
c              elements of the two-particle density matrix
c              starting with the contributions from the
c              all internal non-diagonal case
c     scr1,tran1,tran2 scratch arrays
c     ipth(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist1(2,2)   starting number for the ci vector in segments 1 and 2
c     frst=0     first pass through subroutine
c         =1     second or higher pass
c     lstseg=.true.  last segment has to be taken into account
c     nmbseg   number of segments
c     iwxx(2)  =1 x paths  for segments 1 and 2
c              =2 w paths  for segments 1 and 2
c
       implicit none
c  ##  parameter & common block section
c
      integer             mxnseg
      parameter           (mxnseg = 100)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      INTEGER             CIST(2,MXNSEG), CIVCST(2,MXNSEG)
      INTEGER             INDXST(2,MXNSEG), LENCI(2),  NSEG
      INTEGER             NSEGW, SEGCI(2,MXNSEG)
      INTEGER             SEGEL(2,MXNSEG) ,recci
      COMMON / CIVCT  /   LENCI,       NSEG,        NSEGW, recci,SEGEL
      COMMON / CIVCT  /   CIST,        INDXST,      CIVCST,      SEGCI
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
      INTEGER             CISTRT(2),   FIRST,       IPTH1,       IPTH2
      INTEGER             IWX,         JUMP,        MLP,         SEGSUM
      INTEGER             WTLPHD
      COMMON / DIAG   /   IPTH1,       IPTH2,       IWX,         CISTRT
      COMMON / DIAG   /   FIRST,       SEGSUM,      WTLPHD,      MLP
      COMMON / DIAG   /   JUMP
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      TOTVEC
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   TOTVEC,      CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI

C
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
c
c  ##  integer section
c
      INTEGER CIST1(2,2),  CONFT1(*),conft2(*)
      INTEGER index11(*),index12(*),index21(*),index22(*), indsym1(*),
     &        indsym2(*),indxyz1(*),indxyz2(*), IWXX(2), ipth(2), imd
      integer FRST
      INTEGER KSG
      integer N1EL, NMBSEG, NMSYM
      INTEGER SEGSM(2)
c
c  ##  real*8 section
c
      REAL*8 CI11(*),CI12(*),CIYZ1(2),CI21(*),CI22(*),CIYZ2(2)
      REAL*8 D2IL(*)
      real*8 SCR1(*),scr_asym(*)
      REAL*8 TRAN1(*),TRAN2(*)
c
c  ## logical section
c
      logical inclaldlp
      LOGICAL LSTSEG
c
c-------------------------------------------------------------------
c
ctest return
c     write(6,3333)
 3333 format(/' diagonal case')
c
      first=frst
      inclaldlp = ( ssym1.eq.ssym2)
c
c     # the call to diaden calculates the matrix elements
c     # D(l,l) with l being an internal index
c     # for states of different symmetry there are no
c     # diagonal transition density blocks
c
      n1el=cnfw(1)
c
       call wzero(n1el,scr1,1)
       call wzero(n1el,scr_asym,1)
cgg1
c
      if (nmbseg.eq.0) then
            segsum=0
            if (inclaldlp)
     .      call diaden ( indsym1,conft1,index11,ci11,indxyz1,
     .                    ciyz1,indsym2,conft2,index12,ci12,indxyz2,
     .                    ciyz2,d2il,nmbseg)
            call fourd1 ( indsym1,conft1,index11,ci11,indxyz1,
     .                    ciyz1,indsym2,conft2,index12,ci12,indxyz2,
     +                    ciyz2,scr1,scr_asym,tran1,tran2)
      else
cmd
       if (nmbseg.gt.2) call bummer(
     &  'segmentation not possible,increase memory',0,faterr)
cmd
      do 100 ksg=1,nmbseg
c
          ipth1=ipth(ksg)
          segsum=segsm(ksg)
          cistrt(1)=cist1(1,ksg)
          cistrt(2)=cist1(2,ksg)
          iwx=iwxx(ksg)
          if(ksg.eq.1)then
           if (inclaldlp)
     .      call diaden ( indsym1,conft1,index11,ci11,indxyz1,
     .                    ciyz1,indsym2,conft2,index12,ci12,indxyz2,
     .                    ciyz2,d2il,nmbseg)
            call fourd1 ( indsym1,conft1,index11,ci11,indxyz1,
     .                    ciyz1,indsym2,conft2,index12,ci12,indxyz2,
     +                    ciyz2,scr1,scr_asym,tran1,tran2)
              first=1
              else
            if (inclaldlp)
     .      call diaden ( indsym1,conft1,index21,ci21,indxyz1,
     .                    ciyz1,indsym2,conft2,index22,ci22,indxyz2,
     .                    ciyz2,d2il,nmbseg)
            call fourd1 ( indsym1,conft1,index21,ci21,indxyz1,
     .                    ciyz1,indsym2,conft2,index22,ci22,indxyz2,
     +                    ciyz2,scr1,scr_asym,tran1,tran2)
          endif
c
  100 continue
      endif
c
      call fourw1(scr1,scr_asym)
      frst=first
      if(nseg.le.3.or.lstseg)then
       if (inclaldlp)  call diagwr(d2il, next, intorb)
      endif
c     call writex(d2il,intorb*(intorb+1))
      return
      end
c
c***********************************************************************
c
      subroutine diagd2(scr,d2,code,val1,val2,d2il,i1,l1)
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine diagd2
C
C     Parameter variables
C
C
      REAL*8              TWO
      PARAMETER           (TWO = 2.0D0)
C
C     Argument variables
C
      INTEGER             CODE,        I1,          L1
C
      REAL*8              D2(*),       D2IL(*),     SCR,         VAL1
      REAL*8              VAL2
C
C     Local variables
C
      INTEGER             CODEM,       L12
C
C
C    END OF VARIABLE DECLARATIONS FOR subroutine diagd2


c mxinor = maximum number of internal orbitals
c nmotx = maximum number of molecular orbitals
      codem=code-3
      go to(1630,1640,1650,1660),codem
 1630 continue
c
c     val1*(ii/ll)+val2*(il/il)
c
      d2(1)=val1*scr
      d2(2)=val2*scr
      go to 1700
 1640 continue
c
c     val*(-0.5*(ll/ll)+(l/h/l))
c
      d2(1)=-two*val1*scr
      d2(2)=val1*scr
      go to 1700
 1650 continue
c
c     (l/h/l)*val
c
      d2(2)=val1*scr
      go to 1700
 1660 continue
c
c     val*(0.5*(ll/ll)+(l/h/l))
c
      d2(1)=two*val1*scr
      d2(2)=val1*scr
 1700 continue
c     write(6,30000)code,d2(1),d2(2)
30000 format(' diagd2',i4,2f10.6)
              l12=l1*(l1-1)
c             write(6,30010)l1,i1,l12,d2il(l12+2*i1-1),d2il(l12+2*i1)
30010 format(' diagd2 d2il',3i4,2f10.6)
              if(code.ne.6)
     +        d2il(l12+2*i1-1)=d2il(l12+2*i1-1)+d2(1)
              d2il(l12+2*i1)=d2il(l12+2*i1)+d2(2)
      return
      end



      subroutine diagwr(d2il,next,intorb  )
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine diagwr
C
C     External functions
C
      EXTERNAL            ATEBYT,      INDD1
C
      INTEGER             ATEBYT,      INDD1
C
C     Parameter variables
C
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
C
      REAL*8              TWO
      PARAMETER           (TWO = 2.0D0)
C
C     Argument variables
C
      INTEGER            next ,intorb

      REAL*8              D2IL(*)
C
C     Local variables
C
      INTEGER             IND1,        L1,          L12,         L1IND
      INTEGER             L1LV,        LSYM
c
      REAL*8 d1_sym(iprpmx),d1ci_sym(iprpmx),d1_asym(iprpmx),
     &       d1ci_asym(iprpmx)
      COMMON / DEN1P  / d1_sym, d1ci_sym, d1_asym, d1ci_asym
c
C     Common variables
C
      INTEGER             ORBSYM(MXINOR)
C
      COMMON / INF1   /   ORBSYM
C
C
C     Common variables
C
      INTEGER             EQVSYM(8),   IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(8),   NFRC(8),     NFRV(8),     NMUVAL
      INTEGER             NOCC(8),     NORBSM(8),   NOXT(8)
C
C
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
C    END OF VARIABLE DECLARATIONS FOR subroutine diagwr


c nmotx=maximum number of molecular orbitals
c mxinor=maximum number of internal orbitals
c     # pointers for sif buffer allocation
c
c     the non-diagonal all internal loop contributions have still
c     to be added to the (l1,l1/i1,i1) and (l1,i1/l1,i1) cases
c
      do 100 l1=1,intorb
          l12=l1*(l1-1)
          l1lv=l1+next
          l1ind=ivout(l1lv)
          lsym=orbsym(l1)
          ind1=indd1(lsym,lsym,l1ind,l1ind)
          d1_sym(ind1)=d1_sym(ind1)+two*d2il(l12+2*l1)
c*mdc*if debug
c*          write(*,*) 'diagwr: adding',two*d2il(l12+2*l1)
c*     .                ' to d1_sym(',ind1,')=',d1_sym(ind1)
c*mdc*endif
  100 continue
      return
      end


      subroutine diracc(
     & ifile,  rdwr,   a,      number,
     & nrec,   recst,  recend   )
c
      implicit logical(a-z)
c
      integer    irbuff
      parameter (irbuff=32768)
      real*8        buffer
      common/dirbuf/buffer(irbuff)
c
c  there are better ways to do this. fix this later. -rls
      integer   ibuff
*@ifdef int64
      dimension ibuff(irbuff)
*@else
*      dimension ibuff(2*irbuff)
*@endif
      equivalence(buffer(1),ibuff(1))
c
      integer    isfln
      parameter (isfln=4)
      integer         isfl
      common /singfl/ isfl(isfln)
c
c     # dummy:
      integer ifile, rdwr, number, nrec, recst, recend
c     # a(*) is dimensioned real*8 in the calling program.
      integer a(*)
c
c     # local:
      integer is, ifin, sum, i, idiff, j
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    iread,   iwrite
      parameter( iread=1, iwrite=2 )
c
      integer  rl
      external rl
c
c     rdwr=1  read
c     rdwr=2  write
c     a  array to be read or written
c     number length of the array a
c     nrec  record length (in real*8 words)
c     recst  number of record at which read or write starts
c     recend  number of record at which read or write terminates
c
      is     = 0
      ifin   = 0
      sum    = 0
      i      = 0
      idiff  = 0
      j      = 0
      recend = 0
c
      do 1001 is = 1, isfln
         if ( ifile .eq. isfl(is) ) then
c           # short-precision file.
            call dirsng(
     &       ifile,  rdwr,   a,      number,
     &       nrec,   recst,  recend )
            return
         endif
1001  continue
c
c-----     recend=(number-1)/nrec+1+recst-1
      recend = (number - 1) / rl(nrec) + recst
c
      if ( rdwr .eq. iread ) then
         ifin = 0
         if ( mod(number,rl(nrec)) .ne. 0 ) ifin = 1
         sum = 1
         do 10 i = recst, recend
            if ( (i .eq. recend) .and. (ifin .eq. 1) ) then
               sum = sum - 1
               idiff = number - sum
               call readda( ifile, i, buffer, nrec )
               do 40 j = 1, idiff
                  a(sum+j) = ibuff(j)
40             continue
            else
               call readda( ifile, i, a(sum), nrec )
            endif
            sum = sum + rl(nrec)
10       continue
      elseif ( rdwr .eq. iwrite ) then
         sum = 1
         do 110 i = recst, recend
            call writda( ifile, i, a(sum), nrec )
            sum = sum + rl(nrec)
110      continue
      else
         call bummer('diracc: unknown rdwr=', rdwr, faterr )
      endif
c
      return
      end


      subroutine dirsng(ifile,rdwr,a,number,nrec,recst,recend)
c
      implicit logical(a-z)
c
      integer    irbuff
      parameter (irbuff=32768)
      real*8        buffer
      common/dirbuf/buffer(irbuff)
c
      real*4 abuff(irbuff)
      equivalence ( buffer(1), abuff(1) )
c
c     # dummy:
      integer ifile, rdwr, number, nrec, recst, recend
      real*8 a(*)
c
c     # local:
      integer sum, numb1, ifin, i, idiff, j
c
      integer  rl
      external rl
c
c     rdwr=1  read
c     rdwr=2  write
c     a  array to be read or written
c     number length of the array a
c     nrec  record length (in double precision words at ibm)
c     recst  number of record at which read or write starts
c     recend  number of record at which read or write terminates
c
c     recend=(number-1)/nrec+1+recst-1
      recend=(number-1)/rl(nrec)+recst
      numb1=number/2
      ifin=0
      if(mod(number,rl(nrec)).ne.0)ifin=1
      if(rdwr.gt.1)go to 10000
      sum=0
      do 1010 i=recst,recend
          call readda(ifile,i,abuff,nrec/2)
          if(i.eq.recend.and.ifin.eq.1)then
             idiff=numb1-sum
          else
             idiff=nrec
          endif
          do 1040 j=1,idiff
c-              a(sum+j)=dble(abuff(j))
              a(sum+j) = ( abuff(j) )
 1040     continue
          sum=sum+nrec
 1010 continue
      return
10000 continue
      sum=0
      do 1110 i=recst,recend
          if(i.eq.recend.and.ifin.eq.1)then
             idiff=numb1-sum
          else
             idiff=nrec
          endif
          do 1140 j=1,idiff
c-              abuff(j)=sngl(a(sum+j))
             abuff(j) = ( a(sum+j) )
 1140     continue
          call writda(ifile,i,abuff,nrec/2)
          sum=sum+nrec
 1110 continue
      return
      end


      subroutine driver( core, lcore, mem1, ifirst)
c
      implicit none 
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)

      integer lcore,mem1,ifirst
      real*8 core(*)

      integer nslist,tape6
      equivalence (nunits(1),tape6)
      equivalence (nunits(3),nslist)
c
c     open the listing files.
c
      open( unit = tape6, file = fname(1), status = 'unknown')
      open( unit = nslist, file= fname(3), status = 'unknown')

*@ifdef direct
*      call headwr(tape6,'transci','5.5     ')
*@else
      call headwr(tape6,'transci','5.5      ')
*@endif
c
      call who2c( 'transci', tape6 )

c     # read the user input  and open the input file.
c
      call datain
c     # open the necessary info containing files
c
      call opn1st

c
      call ibummr( tape6 )
c
c
c # initialize segment value arrays
      call intab
c # initialize necessary stack arrays
      call istack

      call drivercid(core,lcore)


      return
      end


      subroutine drivercid(core, mxcore)
c
c calculation of the first and second order density matrix
c based on the columbus program system (h.lischka, r.shepard,
c f.brown and i.shavitt int.j.quant.chem. s15,91(1981))
c written by
c hans lischka, university of vienna, december 1987
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine drivercid
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
C
      INTEGER             ATEBYT,      FORBYT
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MXSYM
      PARAMETER           (MXSYM = 8)
      integer             mxnseg
      parameter           (mxnseg = 100)



C
C     Argument variables
C
      INTEGER             MXCORE
C
      REAL*8              CORE(MXCORE)
C
C     Local variables
C
C
      INTEGER             FIn1,fin2
      INTEGER             I,           IBLK,        ICLOCK,      ICNTW
      INTEGER             ICNTX,       ISYM,        JBF
      INTEGER             JSYM,        LOC(20)
      INTEGER             lfree, MAXNEX
      INTEGER             N1EL,        NI
      INTEGER             NIJ,         NMBJ,        NMOT
      INTEGER             TOTBUF
      INTEGER             TOTCNF,      TOTCYZ,      TOTINS,      TOTINX
      INTEGER             TOTNC0,      TOTNC1,      TOTNC2,      TOTNC3
      INTEGER             TOTNC4,      TOTNCD,      TOTNEC,      TOTRD2
      logical firstcall
      data firstcall /.true./
C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS

      character*60    fname
      common /cfname/ fname(nfilmx)

C
C
C     Common variables
C
C
      INTEGER             CIST(2,MXNSEG), CIVCST(2,MXNSEG)
      INTEGER             INDXST(2,MXNSEG), LENCI(2),  NSEG
      INTEGER             NSEGW, SEGCI(2,MXNSEG)
      INTEGER             SEGEL(2,MXNSEG) ,recci


      COMMON / CIVCT  /   LENCI,       NSEG,        NSEGW, recci,SEGEL
      COMMON / CIVCT  /   CIST,        INDXST,      CIVCST,      SEGCI
C
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYM,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      TOTVEC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   TOTVEC,      CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
C     Common variables
C
      INTEGER            sBUFSZI,     sBUFSZL,     sDIMCI
      INTEGER            sINTORB,     sKBL3,       sKBL4
      INTEGER            sLAMDA1,     sMAXBL2,     sMAXBL3,     sMAXBL4
      INTEGER            SMAXLP3,     sMINBL3,     sMINBL4,     sMXBL23
      INTEGER            sMXBLD,      sMXKBL3,     sMXKBL4,     sMXORB
      INTEGER            sN0EXT,      sN0XTLP,     sN1EXT,      sN1XTLP
      INTEGER            sN2EXT,      sN2XTLP,     sN3EXT,      sN3XTLP
      INTEGER            sN4EXT,      sND0EXT,     sND2EXT,     sND4EXT
      INTEGER            sNELI,       sNEXT,       sNFCT,       sNINTPT
      INTEGER            sNMBLK3,     sNMBLK4,     sNMONEL,     sNVALW
      INTEGER            sNVALWK,     sNVALX,      sNVALY,      sNVALZ
      INTEGER            sPTHW,       sPTHX,       sPTHY,       sPTHZ
      INTEGER            sTOTSPC, stotvec,sconfyz,spthyz
C
      COMMON / SECINF /  SPTHZ,       SPTHY,       SPTHX,       SPTHW
      COMMON / SECINF /  SNINTPT,     SDIMCI,      SMAXBL4,     SNMBLK4
      COMMON / SECINF /  SKBL4,       SMXKBL4,     SLAMDA1,     SBUFSZI
      COMMON / SECINF /  SNMONEL,     SINTORB,     SMAXBL3,     SNMBLK3
      COMMON / SECINF /  SKBL3,       SMXKBL3,     SMAXLP3,     SBUFSZL
      COMMON / SECINF /  SMAXBL2,     SMXBL23,     SMXORB,      SMXBLD
      COMMON / SECINF /  SNEXT,       STOTSPC,     SMINBL4,     SMINBL3
      COMMON / SECINF /  SND4EXT,     SND2EXT,     SND0EXT,     SN4EXT
      COMMON / SECINF /  SN3EXT,      SN2EXT,      SN1EXT,      SN0EXT
      COMMON / SECINF /  SN3XTLP,     SN2XTLP,     SN1XTLP,     SN0XTLP
      COMMON / SECINF /  stotvec,sconfyz,spthyz,SNVALWK,SNVALZ,  SNVALY
      COMMON / SECINF /  SNVALX,      SNVALW,      SNFCT,       SNELI


C
C     Common variables
C
      INTEGER             ORBSYM(MXINOR)
C
      COMMON / INF1   /   ORBSYM
C
C
C     Common variables
C
      INTEGER             BL0XT,       BL1XT,       BL2XT,       BL2XTL
      INTEGER             BL3XT,       BL4XT,       BUF2XT,      ISKIP
      INTEGER             NZPTH
C
      COMMON / INF3   /   ISKIP,       BL4XT,       BL3XT,       NZPTH
      COMMON / INF3   /   BL2XT,       BL1XT,       BL0XT,       BL2XTL
      COMMON / INF3   /   BUF2XT
C
C
C     Common variables
C
      INTEGER             NREC35,      NREC36, ssym1,ssym2
C
      COMMON / INF4   /   NREC35,      NREC36, ssym1,ssym2
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
C
c
C
C     Common variables
      integer mxunit
      parameter(mxunit=99)

      character*60   fndrt1,fndrt2

C     Equivalenced common variables
C
      INTEGER tape6,tape5,nslist,fildrt1,fildrt2,findex1,findex2
      INTEGER civout1,civout2,civfl1,civfl2
C

      equivalence (nunits(1),tape6)
      equivalence (nunits(2),tape5)
      equivalence (nunits(3),nslist)
      equivalence (nunits(9),fildrt1)
      equivalence (fname(9),fndrt1)
      equivalence (nunits(10),fildrt2)
      equivalence (fname(10),fndrt2)
      equivalence (nunits(14),findex1)
      equivalence (nunits(15),findex2)
      equivalence (nunits(16),civout1)
      equivalence (nunits(17),civout2)
      equivalence (nunits( 7),civfl1)
      equivalence (nunits( 8),civfl2)

      save
       bufszl=2048

      call izero_wr(20,loc,1)
      call timer(' ',0,iclock,tape6)
c
c  record lengths for direct acc. files
c
c  nrec35, nrec36 already set in block data

      if (firstcall) then
       firstcall=.false.
c
      write(tape6,6010)
 6010 format(/20('####')/' one electron density matrix calculation '/
     + 20('####')/)
      write(tape6,6020)mxcore
 6020 format(/' workspace allocation information:',
     +  ' mxcore=',i8 )
c
      call ibummr(tape6)
c
c  read info from integral and ft files.
c
      totspc = mxcore
      loc(1)=1
c
c # read first drt and index vector
c
      call tapind (ssym1, nmot, nmsym, core(loc(1)),
     .              fildrt1,fndrt1,
     . pthz,pthy,pthx,pthw,nintpt,intorb,nvalwk,nvalz,
     . nvaly,nvalw, nvalx,nfct,neli,totspc)
ctm
ctm  currently defined core(loc(1)): limvec
ctm                    core(loc(2)): indsym
ctm                    core(loc(3)): refvec



       loc(2)= loc(1)+forbyt(nintpt)
       loc(3)= loc(2) + forbyt(nvalwk)
       loc(4)= loc(3) + forbyt(nvalwk)

      if (method.ne.0)
     .call setref(core(loc(1)),core(loc(3)),1,pthz,nvalz)


*@ifdef debug
*       write(tape6,*) 'First DRT : '
*       write(tape6,6601) pthz,pthy,pthx,pthw,nintpt
*       write(tape6,6602) intorb,nvalwk,nvalz,nvaly,nvalw,nvalx,nfct
*     .     ,neli
*6601   format('Internal paths: zyxw,total:',5i8)
*6602   format('Int.orb=',i4,' valid walks total,zywx=',5i8,
*     .        'frozen core=',i4,'#electrons=',i4)
*@endif
c
c # read second drt and  second index vector
c
c
c # core(1 .. nintpt)             index1(*) integer*4
c # core(nintpt+1 .. 2*nintpt)    index2(*) integer*4
c
      call tapind (ssym2, nmot, nmsym, core(loc(4)),
     .              fildrt2,fndrt2,
     . spthz,spthy,spthx,spthw,snintpt,sintorb,snvalwk,snvalz,
     . snvaly,snvalw, snvalx,snfct,sneli,totspc)
*@ifdef debug
*       write(tape6,*) 'Second DRT : '
*       write(tape6,6601) spthz,spthy,spthx,spthw,snintpt
*       write(tape6,6602) sintorb,snvalwk,snvalz,snvaly,snvalw,snvalx,
*     .     snfct,sneli
*@endif

       loc(5)= loc(4) + forbyt(snintpt)
       loc(6)= loc(5) + forbyt(snvalwk)
       loc(7)= loc(6) + forbyt(snvalwk)

      if (method.ne.0)
     .call setref(core(loc(4)),core(loc(6)),2,spthz,snvalz)
*@ifdef debug
*        write(tape6,6603) (loc(i),i=1,7)
*6603    format('memory allocation loc(*)=',10i6)
*@endif
c
c  # apply suitable compatibility checks ...
c


c
c  prepare symmetry arrays.
c
      call smprepcid(nmsym)
c
c  count one electron integrals and external orbitals.
c
      next=0
      do 310 i=1,nmsym
          ni=nbas(i)
          next=next+ni
  310 continue
      nzpth = nvalz
      pthyz = pthy + pthz

      spthyz= pthyz

      call prep1(mxorb)
c
c     # calculate external walks through x and w
c
      mxbl23=0
      do 100 iblk = 1, nmsym
         icntx = 0
         icntw = 0
         nmbj = nmbpr(iblk)
         if (nmbj .gt. 0 ) then
            do 200 jbf = 1, nmbj
               jsym = lmdb(iblk,jbf)
               isym = lmda(iblk,jbf)
               if ( isym .ne. jsym ) then
                  nij = nbas(isym)*nbas(jsym)
                  icntx = icntx + nij
                  icntw = icntw + nij
               else
                  ni = nbas(isym)
                  icntx = icntx + ((ni-1)*ni)/2
                  icntw = icntw + ((ni+1)*ni)/2
               endif
200         continue
         endif
         cnfx(iblk) = icntx
         cnfw(iblk) = icntw
         mxbl23=max(mxbl23,icntw)
100   continue
      maxbl2=mxbl23*3
c
      write(tape6,6001)
6001  format(/' number of external paths / symmetry')
      write(tape6,6002)'x',(cnfx(i),i=1,nmsym)
      write(tape6,6002)'w',(cnfw(i),i=1,nmsym)
6002  format(1x,'vertex',a2,8i8)
c

c
c     # begin memory allocation here
c
c       indsym(nvalwk) , conft(nvalwk) , index(pthyz)
c
      totins = forbyt ( nvalwk ) + forbyt ( snvalwk)
      totcnf = forbyt ( nvalwk ) + forbyt ( snvalwk)
      totinx = forbyt ( pthyz )  + forbyt (spthz+spthy)
c
c     # reference 2-e density matrix:  values and labels
      totrd2 = 0
c
      totbuf = 2*atebyt ( nrec36 ) + 4*forbyt( nrec36 )
c
c
c   ----> wofuer ist bufszl (integrale, formula tape ??? )
c
c  one external
c
      totnc1=atebyt(bufszl)+atebyt(3*mxorb)+3*atebyt(mxorb)+
     +       atebyt(mxorb*mxorb)
c
c  all internal
c
      totnc0=atebyt(bufszl)
c
c  diagonal case
c
      n1el=cnfw(1)
      totncd=atebyt(bufszl)+atebyt(n1el)+2*atebyt(2*n1el)
c
c  total required
c
      maxnex = max( totnc1, totnc0, totncd)
      totnec = totins+totcnf+totinx+totrd2+totbuf+maxnex
c
c     # left overs for segments
c
      totvec = totspc - totnec
      if (totvec .le. 0) then
         call bummer('driver: not enough memeory, total available for ve
     &ctors is ',totvec,faterr)
      endif
c
c  core:
c
c        indx1(nintpt)        integer*4
c        indsym1(nvalwk)      integer*4
c        conft1(nvalwk)       integer*4
c
c        indx2(nintpt)        integer*4
c        indsym2(nvalwk)      integer*4
c        conft2(nvalwk)       integer*4
c
c
      call prepdd( core(loc(1)), core(loc(2)), core(loc(3)),
     .             core(loc(4)), core(loc(5)), core(loc(6)),
     .             findex1, findex2 )

      totcyz = atebyt ( confyz ) + atebyt(sconfyz)
c     totnec = totnec + totvec + totcyz
      totnec = totnec + totcyz
      totvec= totspc - totnec
c
      if(lvlprt.ge.5)then
          write(tape6, * ) 'indx1 ='
          call wrint2( tape6, core(loc(1)), nintpt )
          write(tape6, * ) 'indsym1 ='
          call wrint2( tape6, core(loc(2)), nvalwk )
          write(tape6, * ) 'conft1 ='
          call wrtint( tape6, core(loc(3)), nvalwk )
          write(tape6, * ) 'indx2 ='
          call wrint2( tape6, core(loc(4)), nintpt )
          write(tape6, * ) 'indsym2 ='
          call wrint2( tape6, core(loc(5)), nvalwk )
          write(tape6, * ) 'conft2 ='
          call wrtint( tape6, core(loc(6)), nvalwk )
      endif
      write(tape6, * )
      write(tape6, * ) 'SPACE REQUIRED:'
      write(tape6,*)
      write(tape6,'(4x,a)') 'space required for calls in multd2:'
      write(tape6, 1010 ) 'onex  ', totnc1
      write(tape6, 1010 ) 'allin ', totnc0
      write(tape6, 1010 ) 'diagon', totncd
      write(tape6, '(4x,a6,4x,a9)' ) 'max.  ','---------'
      write(tape6, 1010 ) 'maxnex', maxnex
      write(tape6,*)
      write(tape6,'(4x,a)') 'total core space usage:'
      write(tape6, 1010 ) 'maxnex', maxnex
      write(tape6, 1010 ) 'totvec', totvec
      write(tape6, 1010 ) 'indsym', totins
      write(tape6, 1010 ) 'conft ', totcnf
      write(tape6, 1010 ) 'indxyz', totinx
      write(tape6, 1010 ) 'ciyz  ', totcyz
      write(tape6, 1010 ) 'buffer', totbuf
      write(tape6, '(14x,a9)' ) '---------'
      write(tape6, 1010 ) 'totspc', totspc
c     write(tape6, 1010 ) 'totmax', totnec
 1010 format (4x,a6,4x,i9)
c
c  stop if there is not enough core
c
      if( totnec . gt. totspc ) then
          call bummer('space allocation error',0,faterr)
      endif
c
      if(lvlprt.ge.2) write(tape6,20100)totspc,totvec,totnec
20100 format(/' total core array space available',i9/
     +        ' space available for vectors     ',i9/
     +        ' space for indexing arrays and scr' ,i9)
c
c  move indsym aind conf to beginning of core(*)
c
c             indsym1 (nvalwk)   loc(1)
c             conf1    (nvalwk)  loc(2)
c             indsym2  (snvalwk) loc(3)
c             conf2    (snvalwk) loc(4)
c             free               loc(5)
c
      call icopy_wr(nvalwk,core(loc(2)),1,core(loc(1)),1)
      loc(2)=loc(1)+forbyt(nvalwk)
      call icopy_wr(nvalwk,core(loc(3)),1,core(loc(2)),1)
      loc(3)=loc(2)+forbyt(nvalwk)
      call icopy_wr(snvalwk,core(loc(5)),1,core(loc(3)),1)
      loc(4)=loc(3)+forbyt(snvalwk)
      call icopy_wr(snvalwk,core(loc(6)),1,core(loc(4)),1)
      loc(5)=loc(4)+forbyt(snvalwk)
      endif
c
      lfree= loc(5)
c
c  # collect phase information about CI vectors in case of AQCC
c
      if (method.ne.0) then
c       call openda(nunits(7),fname(7),8*4096,'keep','random')
        call getciphase(fname(7),core(lfree),8*4096,nroot1,1)
c       call closda(nunits(7),'keep',4)
c       call openda(nunits(8),fname(8),8*4096,'keep','random')
        call getciphase(fname(8),core(lfree),8*4096,nroot2,2)
c       call closda(nunits(8),'keep',4)
      endif
c
c  # collect phase information about CI vectors in case of CI-restart
c ???????????
c
      if (method.ne.0) then
c       call openda(nunits(7),fname(7),8*4096,'keep','random')
        call getciphase(fname(7),core(lfree),8*4096,nroot1,1)
c       call closda(nunits(7),'keep',4)
c       call openda(units(8),fname(8),8*4096,'keep','random')
        call getciphase(fname(8),core(lfree),8*4096,nroot2,2)
c       call closda(nunits(8),'keep',4)
      endif

c
c  read index array for y and z paths
c
c      indxyz1   (pthyz)  loc(5)
c      indxyz2   (pthyz)  loc(6)
c      free               loc(7)

      call diracc(findex1,1,core(loc(5)),pthyz,nrec35,indxst(1,1),fin1)
       loc(6)= loc(5)+forbyt(pthyz)
      call diracc(findex2,1,core(loc(6)),
     .     spthyz,nrec35,indxst(2,1),fin2)
       loc(7)= loc(6)+forbyt(spthyz)
       lfree= loc(7)

*@ifdef debug
*        write(tape6,6603) (loc(i),i=1,8)
*@endif

c      call countindex('yz',core(loc3),pthyz)
c
      call densi(nmot,nmsym,core,lfree,mxcore-lfree)
c
      return
      end


      subroutine expda(n,a,b)
c
c  expand a as antisymmetric matrix.
c  a(*) is copied to the upper triangle of b(*,*).
c  -a(*) is copied to the lower triangle of b(*,*).
c
      implicit logical(a-z)
      integer n
      real*8 a(*),b(n,n)
c
      integer i, j, ii
      real*8 xx
      real*8    zero
      parameter(zero=0.0d0)
c
      ii=0
*@if defined  cray && defined reference 
*      do 20 i=2,n
*         call wcopym(i-1,a(ii+1),1,b(i,1),n)
*         call wcopy(i-1,a(ii+1),1,b(1,i),1)
*         ii=ii+(i-1)
*20    continue
*      call wzero(n,b,n+1)
*@elif defined  cray
*Cdir$ ivdep
*      do 20 i=2,n
*         ii= ((i-1)*(i-2))/2
*Cdir$    ivdep
*         do 10 j=1,(i-1)
*            xx=a(ii+j)
*C           # lower triangle
*            b(i,j)=-xx
*C           # upper triangle
*            b(j,i)=xx
*10       continue
*20    continue
*C
*C  the following nonstandard code is required for vectorization
*C  on the cray. -rls
*C
*Cdir$ ivdep
*      do 30 i = 1, n
*         b(i*(n+1)-n,1) = zero
*   30 continue
*@else
c     19-nov-89  carry-around scalar, ii, eliminated. -rls
c
c$doit ivdep  #titan
cvd$   ivdep
cvocl loop,novrec
c
      do 20 i = 2, n
         ii= ((i-1) * (i-2)) / 2
c
c$doit   ivdep  #titan
cvd$     ivdep
cvocl loop,novrec
c
         do 10 j = 1, (i-1)
            xx     = a(ii+j)
c           # lower triangle
            b(i,j) = -xx
c           # upper triangle
            b(j,i) = xx
10       continue
20    continue
c
c$doit ivdep  #titan
cvd$   ivdep
cvocl loop,novrec
c
      do 30 i = 1, n
         b(i,i) = zero
30    continue
*@endif
      return
      end


      subroutine expda1(n,a,b)
c
c  expand a as antisymmetric matrix
c  a(*) is copied to the lower triangle of b(*,*).
c  -a(*) is copied to the upper triangle of b(*,*).
c
      implicit logical(a-z)
      integer n
      real*8 a(*),b(n,n)
c
      integer i, j, ii
      real*8 xx
      real*8    zero
      parameter(zero=0.0d0)
c
      ii=0
*@if defined  cray && defined reference 
*      do 20 i=2,n
*        call wcopy(i-1,a(ii+1),1,b(i,1),n)
*        call wcopym(i-1,a(ii+1),1,b(1,i),1)
*        ii=ii+(i-1)
*   20 continue
*      call wzero(n,b,n+1)
*@elif defined  cray
*Cdir$ ivdep
*      do 20 i=2,n
*         ii=((i-1)*(i-2))/2
*Cdir$    ivdep
*         do 10 j=1,(i-1)
*            xx=a(ii+j)
*C           # lower triangle
*            b(i,j)=xx
*C           # upper triangle
*            b(j,i)=-xx
*10       continue
*20    continue
*C
*C  the following nonstandard code is required for vectorization
*C  on the cray. -rls
*C
*Cdir$ ivdep
*      do 30 i = 1, n
*         b(i*(n+1)-n,1) = zero
*30    continue
*@else
c     19-nov-89  carry-around scalar, ii, eliminated. -rls
c
c$doit ivdep  #titan
cvd$   ivdep
cvocl loop,novrec
c
      do 20 i = 2, n
         ii = ((i-1) * (i-2)) / 2
c
c$doit   ivdep  #titan
cvd$     ivdep
cvocl loop,novrec
c
         do 10 j = 1, (i-1)
            xx     = a(ii+j)
c           # lower triangle
            b(i,j) = xx
c           # upper triangle
            b(j,i) = -xx
10       continue
20    continue
c
c$doit ivdep  #titan
cvd$   ivdep
cvocl loop,novrec
c
      do 30 i = 1, n
         b(i,i) = zero
30    continue
*@endif
      return
      end


      subroutine expds(n,a,b)
c
c  expand a symmetrically and scale the diagonal elements by sqrt(2).
c
      implicit logical(a-z)
      integer n
      real*8 a(*), b(n,n)
c
      integer i, j, ii
      real*8 xx
      real*8    sqrt2
      parameter(sqrt2=1.41421356237309504880168872420970d0)
c
*@if defined  cray && defined reference 
*      ii=2
*      b(1,1)=a(1)
*      do 20 i=2,n
*        call wcopy(i-1,a(ii),1,b(i,1),n)
*        call wcopy(i,a(ii),1,b(1,i),1)
*        ii=ii+i
*   20 continue
*      call wscal(n,sqrt2,b,n+1)
*@elif defined  cray
*      b(1,1)=a(1)
*      do 20 i=2,n
*         ii=(i*(i-1))/2
*Cdir$    ivdep
*         do 10 j=1,i
*            xx=a(ii+j)
*            b(i,j)=xx
*            b(j,i)=xx
*10       continue
*20    continue
*C
*C  the following nonstandard code is required for vectorization
*C  on the cray. -rls
*C
*Cdir$ ivdep
*      do 30 i=1,n
*         b(i*(n+1)-n,1)=b(i*(n+1)-n,1)*sqrt2
*30    continue
*@else
c     19-nov-89  carry-around scalar, ii, eliminated. -rls
c
c$doit ivdep  #titan
cvd$   ivdep
cvocl loop,novrec
c
      b(1,1) = a(1)
      do 20 i = 2, n
         ii = (i*(i-1))/2
c
c$doit   ivdep  #titan
cvd$     ivdep
cvocl loop,novrec
c
         do 10 j = 1, i
            xx     = a(ii+j)
            b(i,j) = xx
            b(j,i) = xx
10       continue
20    continue
c
c$doit ivdep  #titan
cvd$   ivdep
cvocl loop,novrec
c
      do 30 i = 1, n
         b(i,i) = b(i,i) * sqrt2
30    continue
*@endif
      return
      end
c
c******************************************************************
c
      subroutine fourw1(scr_sym,scr_asym)
       implicit none
c  ##  parameter & common block section
c
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
c
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYM,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
c
      REAL*8 d1_sym(iprpmx),d1ci_sym(iprpmx),d1_asym(iprpmx),
     &       d1ci_asym(iprpmx)
      COMMON / DEN1P  / d1_sym, d1ci_sym, d1_asym, d1ci_asym
c
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
c
c  ##  integer section
c
      integer dsym
      INTEGER I1, IJ,IND1,IOFFS, ISYM
      integer J1
      integer l1, lsym
      integer NI, NIJ1, NMIJ, NRXTI, NRXTJ, nl,nrxtl
c
c  ##  real*8 section
c
      REAL*8 scr_sym(*),scr_asym(*)
c
c  ##  external section
c
      EXTERNAL            INDD1,       INDXT
      INTEGER             INDD1,       INDXT
c
c-----------------------------------------------------------------------
c
c     write(*,*)'entered fourw1', (scr_sym(i1),i1=1,3)
      dsym=symtab(ssym1,ssym2)
      nmij = nmbpr(dsym)
      if (nmij.ne.0) then
          ioffs = 0
          do 10 ij = 1,nmij
              isym = lmdb(dsym,ij)
              lsym = lmda(dsym,ij)
              nl = nbas(lsym)
              ni = nbas(isym)
              if (ni.ne.0 .and. nl.ne.0 ) then
                 if (isym.eq.lsym) then
c old stuff
                  do 20 j1 = 1,ni
                      nij1 = ni - j1 + 1
                      nrxtj = indxt(isym,j1)
                      do 30 i1 = 1,nij1
                          nrxti = indxt(isym,i1+j1-1)
                          ind1 = indd1(isym,isym,nrxtj,nrxti)
c                         write (6,fmt=9010) i1,j1,ioffs,nrxtj,nrxti,
c    +                      ind1,scr_sym(ioffs+i1),d1_sym(ind1),
c    &                      scr_asym(ioffs+i1)
                          d1_sym(ind1) = d1_sym(ind1)+scr_sym(ioffs+i1)
c  Note: phase factor adjustment in all-external case due to the
c        ordering of the 2 CI vectors
                          d1_asym(ind1) = d1_asym(ind1) -
     &                     scr_asym(ioffs+i1)
c                         write(*,4000)d1_sym(ind1),d1_asym(ind1)
   30                 continue
                      ioffs = ioffs + nij1
   20             continue
c
               else

c new stuff
                  do 21 l1 = 1,nl
                      nrxtl = indxt(lsym,l1)
                      do 31 i1 = 1,ni
                          nrxti = indxt(isym,i1)
                          ind1 = indd1(lsym,isym,nrxtl,nrxti)
c                         write (6,fmt=9011) i1,l1,ioffs,nrxtl,nrxti,
c    +                      ind1,scr_sym(ioffs+i1),d1_sym(ind1),
c    &                      scr_asym(ioffs+i1)
                          d1_sym(ind1) = d1_sym(ind1)+scr_sym(ioffs+i1)
c  Note: phase factor adjustment in all-external case due to the
c        ordering of the 2 CI vectors
                          d1_asym(ind1) = d1_asym(ind1) -
     &                     scr_asym(ioffs+i1)
c                         write(*,4010)d1_sym(ind1),d1_asym(ind1)
   31                 continue
                      ioffs = ioffs + ni
   21             continue

               endif
              end if
   10     continue
      end if
c
c     write(6,30000)ij,isym,ni
 9000 format (' fourw1',20i4)
 9010 format ('1: d1_sym',6i4,3f10.6)
 9011 format ('2: d1_sym',6i4,3f10.6)
 9020 format ('part1',i4,3f10.6)
 9030 format ('part2',i4,3f10.6)
 4000 format ('part1: d1_sym',f10.6,' d1_asym',f10.6)
 4010 format ('part2: d1_sym',f10.6,' d1_asym',f10.6)
      end


      subroutine getyzinf(index,indsym,conft,confz,confy,pthz,pthy)
      implicit none 
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      integer maxsym
      parameter (maxsym = 8 )

C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


      integer index(*),conft(*),confz,confy,pthy,pthz
      integer ipth,isym,indsym(*),ny

c
c     # get z-walk information from index vector
c
      confz = 0
      do 110 ipth = 1, pthz
         if (index(ipth).ge.0) then
            conft(index(ipth)) = confz
            confz = confz + 1
         endif
110   continue
c
c     # get y-walk information from index vector
c
      confy = 0
      do 120 ipth = (pthz + 1), (pthz + pthy)
         if (index(ipth).ge.0) then
            isym = sym21(indsym(index(ipth)))
            ny = nbas(isym)
            if (ny.le.0) then
               conft(index(ipth)) = -1
               index(ipth) = -1
               call bummer('prepdd: zero externals for path no.',
     &          ipth, wrnerr )
            else
               conft(index(ipth)) = confy + confz
               confy = confy + ny
            endif
         endif
120   continue

        return
        end
c
c*********************************************************************
c
      function indd1(isym,jsym,i,j)
c
c     for an index pair (i,j) of a given symmetry isym this function
c     gives the address in the symmetry blocked one-particle density
c     matrix. i and j are global indices as e.g. produced by the
c     function indxt
c
c
       implicit none
      integer indd1

c
c  ## paramenter & common block section
c
      INTEGER             MAX0,        MIN0
c
c     Parameter variables
c
ctm   ijij should be replaced by statement function

      INTEGER             IJIJSZ
      PARAMETER           (IJIJSZ = 1023)
      integer             mxsym
      parameter           (mxsym = 8)
C
      INTEGER             IJIJ(IJIJSZ)
      COMMON / INF4X  /   IJIJ
c
      INTEGER             BLXT(8),     N2OFFR(8),   N2OFFS(8),   N2ORBT
      INTEGER             NOFFR(8),    NOFFS(8),    NSGES(8), nsdim(8)
      integer    dkntin (mxsym*(mxsym+1)/2)
      integer    densoff(mxsym*(mxsym+1)/2)
      integer    dsize
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   dsize,densoff,dkntin
c
c  ##  integer section
c
      integer i, isym, is, is1
      integer j, jsym, js, js1
c
c-----------------------------------------------------------------------
c
c
c # Note, if the symmetry of vector one equals those of vector two
c #       then the transition density is block diagonal
c #       otherwise it contains nmpairs(isym*jsym) off diagonal blocks
c #       only, equivalent to nmpairs(mult(isym,jsym))
c
c #       corresponding changes are cumulated in setupone
c
      if (noffr(isym).lt.0 .or. noffr(jsym).lt.0)
     .  call bummer('indd1: invalid noffr(isym,jsym)',0,2)


      if (isym.eq.jsym) then
c
c   # diagonal blocked density
c
      is=i-noffr(isym)
      js=j-noffr(isym)
      is1=min0(is,js)
      js1=max0(is,js)
      indd1=n2offr(isym)+ijij(js1)+is1

      elseif (isym.lt.jsym) then
c
c #  offdiagonal blocks
c
       is=i-noffr(isym)
       js=j-noffr(jsym)
c
c  nur fuer isym<=jsym definiert ..
c
       indd1=n2offr(isym)+nsdim(jsym)*(is-1)+js
      else
       js=j-noffr(jsym)
       is=i-noffr(isym)
       indd1=n2offr(jsym)+nsdim(isym)*(js-1)+is
      endif

      return
      end
c
c***********************************************************************
c
      function indxt(isym,i)
c
c     this function converts an external level given by (isym,i) to
c     the orbital number corresponding to the ordering of the mo's
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine indxt
C
C     Parameter variables
C
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             MXSYM
      PARAMETER           (MXSYM  = 8)
C
C     Argument variables
C
      INTEGER           indxt,  I,           ISYM
C
C     Local variables
C
      INTEGER             LEVEL
C
C
C     Common variables
C
      INTEGER             BLXT(8),     N2OFFR(8),   N2OFFS(8),   N2ORBT
      INTEGER             NOFFR(8),    NOFFS(8),    NSGES(8), nsdim(8)
C
      integer    dkntin (mxsym*(mxsym+1)/2)
      integer    densoff(mxsym*(mxsym+1)/2)
      integer    dsize
C
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   dsize,densoff,dkntin
C
C
C     Common variables
C
      INTEGER             EQVSYM(8),   IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(8),   NFRC(8),     NFRV(8),     NMUVAL
      INTEGER             NOCC(8),     NORBSM(8),   NOXT(8)
C
C
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
C    END OF VARIABLE DECLARATIONS FOR subroutine indxt


cmaximum number of internal orbitals=mxinor
c maximum number of molecular orbitals=nmotx
      level=blxt(isym)+i
      indxt=ivout(level)
      return
      end


      subroutine int3ciuft(initial,indxyz1,index11,index21,
     .  indxyz2,index12,index22,ipth1,segsm,ipthyz,nmbseg)

c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2

c     call int3ciuft(.true.,indxyz,index1,index2,ipth1,segsm,cist)

c
c  construct the 3-internal formula tape.
c
c  code  loop type     description
c  ----  ---------     ----------------
c
c   0  -- new level --
c   1  -- new record --
c   2  -- new vertex --
c
c   3       1ab         (yz,xy,wy)
c           6ab         (yz,xy,wy)
c           8ab         (yz,xy,wy)
c   4       2a'b'       (yz,xy,wy)
c   5       3ab         (yz,xy,wy)
c   6       12bc        (yz,xy,wy)
c   7   (not used)
c   8       2b'         (yz,xy,wy)
c           7'          (yz,xy,wy)
c           8b          (yz,xy,wy)
c           10          (yz,xy,wy)
c   9       1b          (yz,xy,wy)
c           6b          (yz,xy,wy)
c

c     integer indxyz(*),index1(*),index2(*),ipth1(2),segsm(2),cist(2)

      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine int3ciuft
C
C     Parameter variables
C
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 32000)
C
C     Argument variables
C
       LOGICAL             INITIAL
      integer indxyz1(*),index11(*),index21(*), ipth1(2),segsm(2)
      integer ipthyz,nmbseg
      integer indxyz2(*),index12(*),index22(*)

C
C     Local variables
C
      INTEGER             BTAILX(3),   KTAILX(3)
C
C
C
C     Common variables
C
      INTEGER             DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER             DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER             DB13(4,0:2), DB14(2,0:2,2)
      INTEGER             DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER             DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER             DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER             DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON / CDB    /   DB1,         DB2,         DB3,         DB4
      COMMON / CDB    /   DB4P,        DB5,         DB6,         DB7
      COMMON / CDB    /   DB8,         DB10,        DB11,        DB12
      COMMON / CDB    /   DB13,        DB14
C
C
C     Common variables
C
      INTEGER             KNTFT,       NFTB,        NFTBD,       NLOOP
C
      REAL*8              FTBUF(LBFT)
C
      COMMON / CFTB   /   FTBUF,       KNTFT,       NLOOP,       NFTB
      COMMON / CFTB   /   NFTBD
C
C
C     Common variables
C
      INTEGER             MAXW1,       NFTB1,       NFTB2,       NFTB3
      INTEGER             NFTB4,       NUMW1
C
      COMMON / CIBI   /   NFTB1,       NFTB2,       NFTB3,       NFTB4
      COMMON / CIBI   /   MAXW1,       NUMW1
C
C
C     Common variables
C
      INTEGER             CODE(4),     HILEV,       MO(0:4),     NIOT
      INTEGER             NMO,         NUMV, nintl
C
      LOGICAL             QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT,        NUMV,        HILEV
      COMMON / CLI    /   CODE,        MO,          NMO,         NINTL
C
C
C     Common variables
C
      INTEGER             QPRT
C
      COMMON / CPRT   /   QPRT
C
C
C     Common variables
C
      INTEGER             ST1(8),      ST10(6),     ST11(4)
      INTEGER             ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER             ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER             ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON / CST    /   ST1,         ST2,         ST3,         ST4
      COMMON / CST    /   ST4P,        ST5,         ST6,         ST7
      COMMON / CST    /   ST8,         ST10,        ST11D,       ST11
      COMMON / CST    /   ST12,        ST13,        ST14
C
C
C     Common variables
C
      INTEGER             VTPT
C
      REAL*8              OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON / CVTRAN /   VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             IDUM(3),     IDUM1(9),    INTORB,      KBL3
      INTEGER             KBL4,        LAMDA1,      MAXBL2,      MAXBL3
      INTEGER             MAXBL4,      MAXLP3,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       ND0EXT
      INTEGER             ND2EXT,      ND4EXT,      NEXT,        NINTPT
      INTEGER             NMBLK3,      NMBLK4,      NMONEL,      PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,idum2(7)
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        IDUM,        ND4EXT,      ND2EXT
      COMMON / INF    /   ND0EXT,      IDUM1,       SPCCI,       CONFYZ
      COMMON / INF    /   PTHYZ, idum2
C
C
C     Common variables
C
      INTEGER             BTAIL,       CLEV,        ILEV,        IMO
      INTEGER             JKLSYM,      JLEV,        JMO
      INTEGER             JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER             KMO,         KTAIL,       LLEV,        LMO
      INTEGER             LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
C
C     Common variables
C
      INTEGER             HEADV(MAXBF),             IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             YBTV(MAXBF), YKTV(MAXBF)
C
      REAL*8              VALV(MAXBF,3)
C
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XBTV
      COMMON / SPECIAL/   YBTV,        YKTV,        IBF
C
C    END OF VARIABLE DECLARATIONS FOR subroutine int3ciuft


      data    btailx/2,3,4/, ktailx/1,2,2/

      ibf=0

      if (initial) jumpinposition=0
      goto (20,1,2,3)
     .  (jumpinposition+1)

20    nintl = 3
      mo(0) = 0
      nloop = 0
      kntft = 0
      qdiag = .false.
      break=.false.
      qprt=0
c
      lmo=1
 400  continue
       kmo=1
c**********************************************************************
c        j=k=l
c**********************************************************************
         if(qprt.ge.5)write(6,6100)lmo,lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c # possibly to be modified for ssym1 .ne. ssym2
c                 do 11  ixx=1,nmbpr(ssym1,ssym2)
c                 is = lmda(symtab(ssym1,ssym2),ixx)
c                 is2 = lmdb(symtab(ssym1,ssym2),ixx)
c                 find the proper ip it ...
c                 if (is2.eq.sym(lmo)) goto 12
c                 enddo
c                 nothing find write new l-value  goto ...
c               12 continue
c
c y/x y/w x/y w/y y/y
         ver=1
390       continue
            btail = btailx(ver)
            ktail = ktailx(ver)

c
c           # 12bc loops:
c
            numv = 2
            vtpt = 1
            code(1) = 6
            code(2) = 0
 1          call loop3in( btail, ktail, st12, db12(1,0,2), 4, 2 ,
     .   indxyz1,index11,index21,
     .   indxyz2,index12,index22,ipth1,segsm,ipthyz,nmbseg,1)

            if (break) then
              jumpinposition=1
              return
             endif
c
 2          if (ver.lt.3) then
             ibf=ibf+1
                   if (ibf.eq.maxbf-1) then
                    jumpinposition=2
                    icodev(ibf)=1
                    return
                   endif

             icodev(ibf)=2
            endif
 389       ver=ver+1
           if (ver.lt.4) goto 390
c
 3         ibf=ibf+1
                   if (ibf.eq.maxbf-1) then
                    jumpinposition=3
                    icodev(ibf)=1
                    return
                   endif

           icodev(ibf)=0
            lmo=lmo+1
            if (lmo.le.niot) goto 400
c
      jumpinposition=0
c
      write(6,6010) ibf
6010  format(/' three-internal ft construction complete.',
     + i8,' loops constructed.')
c
      return
6100  format(' int3:  lmo,kmo,jmo = ',3i4)
      end


      subroutine int4ciuft(initial,indxyz1,index11,index21,
     .   indxyz2,index12,index22,ipth1,segsm,ipthyz,nmbseg)
c
c  construct the 4-internal formula tape.
c
c  code  loop type      description
c  ----  ---------      ------------
c
c   1  -- new record --
c   2  -- new vertex --
c   3  -- new level --
c
c   4       1ab         (z,y,x,w)
c           4ab         (z,y,x,w)
c           6ab         (z,y,x,w)
c           8ab         (z,y,x,w)
c           12ac        (z,y,x,w)
c   5       2a'b'       (z,y,x,w)
c   6       3ab         (z,y,x,w)
c           12bc        (z,y,x,w)
c   7                   (z,y,x,w)
c   8       2b'         (z,y,x,w)
c           7'          (z,y,x,w)
c           10          (z,y,x,w)
c           11b         (z,y,x,w)
c           13          (z,y,x,w)
c   9       1b          (z,y,x,w)
c           4b          (z,y,x,w)
c           4b'         (z,y,x,w)
c           5           (z,y,x,w)
c           6b          (z,y,x,w)
c           8b          (z,y,x,w)
c           12c         (z,y,x,w)
c  10       12abc       (z,y,x,w)
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine int4ciuft
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023 )
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 50)
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 32000)
C
C     Argument variables
C
      LOGICAL             INITIAL
      integer  indxyz1(*), index11(*), index21(*), ipth1(2),segsm(2),
     .    ipthyz,nmbseg, indxyz2(*),index12(*),index22(*)
C
C     Local variables
C
      integer dsym
C
C
C
C     Common variables
C
      INTEGER             DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER             DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER             DB13(4,0:2), DB14(2,0:2,2)
      INTEGER             DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER             DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER             DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER             DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON / CDB    /   DB1,         DB2,         DB3,         DB4
      COMMON / CDB    /   DB4P,        DB5,         DB6,         DB7
      COMMON / CDB    /   DB8,         DB10,        DB11,        DB12
      COMMON / CDB    /   DB13,        DB14
C
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             MU(NIMOMX),  XBAR(NROWMX,3)
C
      COMMON / CDRT   /   B,           XBAR,        MU,          ARCSYM
C
C
C     Common variables
C
      INTEGER             KNTFT,       NFTB,        NFTBD,       NLOOP
C
      REAL*8              FTBUF(LBFT)
C
      COMMON / CFTB   /   FTBUF,       KNTFT,       NLOOP,       NFTB
      COMMON / CFTB   /   NFTBD
C
C
C     Common variables
C
      INTEGER             MAXW1,       NFTB1,       NFTB2,       NFTB3
      INTEGER             NFTB4,       NUMW1
C
      COMMON / CIBI   /   NFTB1,       NFTB2,       NFTB3,       NFTB4
      COMMON / CIBI   /   MAXW1,       NUMW1
C
C
C     Common variables
C
      INTEGER             CODE(4),     HILEV,       MO(0:4),     NIOT
      INTEGER             NMO,         NUMV, NINTL
C
      LOGICAL             QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT,        NUMV,        HILEV
      COMMON / CLI    /   CODE,        MO,          NMO,         NINTL
C
C
C     Common variables
C
      INTEGER             QPRT
C
      COMMON / CPRT   /   QPRT
C
C
C     Common variables
C
      INTEGER             ST1(8),      ST10(6),     ST11(4)
      INTEGER             ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER             ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER             ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON / CST    /   ST1,         ST2,         ST3,         ST4
      COMMON / CST    /   ST4P,        ST5,         ST6,         ST7
      COMMON / CST    /   ST8,         ST10,        ST11D,       ST11
      COMMON / CST    /   ST12,        ST13,        ST14
C
C
C
C     Common variables
C
      INTEGER             BTAIL,       CLEV,        ILEV,        IMO
      INTEGER             JKLSYM,      JLEV,        JMO
      INTEGER             JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER             KMO,         KTAIL,       LLEV,        LMO
      INTEGER             LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
C
C     Common variables
C
      INTEGER             HEADV(MAXBF),             IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             YBTV(MAXBF), YKTV(MAXBF)
C
      REAL*8              VALV(MAXBF,3)
C
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XBTV
      COMMON / SPECIAL/   YBTV,        YKTV,        IBF
C
C     Common variables
C
      INTEGER             vecrln,indxln, ssym1,ssym2
C
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C

C    END OF VARIABLE DECLARATIONS FOR subroutine int4ciuft


      dsym = symtab(ssym1,ssym2)

      ibf=0
      if (initial) jumpinposition=0
      goto (30,1,2,3) (jumpinposition+1)

30    nintl = 4
      mo(0) = 0
      nloop = 0
      kntft = 0
      qdiag = .false.
      break=.false.
c
      qprt=0

c beitraege:  j=k=l ;
        lmo=2
 800   continue
         llev = lmo-1
         lsym = arcsym(1,llev)
c
         kmo=lmo
c        if (kmo.eq.lmo) goto 401
 401     continue
         kmo = lmo
c        jmo=1
         jmo=lmo
c         goto 601
 601      continue
         jmo = kmo
         imo=1
         if (imo.eq.jmo) goto 701
 700     continue
            ilev = imo-1
            if(arcsym(1,ilev).ne.symtab(lsym,dsym))then
            imo=imo+1
            if (imo.lt.jmo) go to 700
            goto 701
            endif
c***********************************************************
c           i<j=k=l loops:
c***********************************************************
            if(qprt.ge.5)write(6,6100)lmo,kmo,jmo,imo
            nmo = 2
            mo(1) = imo
            mo(2) = lmo
c
             ver=1
 690        continue
c
c              # 12abc loops:
c
               numv = 3
               code(1) = 9
               code(2) = 6
               code(3) = 4
               code(4) = 10
C5              call loop( ver , ver , st12, db12, 4, 0 )
1              call loop3in( ver , ver , st12, db12, 4, 0 ,
     .      indxyz1, index11, index21,
     .      indxyz2, index12, index22, ipth1,segsm,ipthyz,nmbseg,3)
               if (break) then
                 jumpinposition=1
                 return
               endif
c
2             if (ver .lt.4) then
               ibf=ibf+1
                if (ibf.eq.maxbf-1) then
                 icodev(ibf)=1
                 jumpinposition=2
                 return
                endif
               icodev(ibf)=2
              endif
             ver=ver+1
             if (ver.lt.5) goto 690

c
3            ibf=ibf+1
             if (ibf.eq.maxbf-1) then
               icodev(ibf)=1
               jumpinposition=3
                return
              endif
              icodev(ibf)=3
         imo=imo+1
          if (imo.lt.jmo) goto 700
 701     continue
         imo = jmo
c***********************************************************
c        i=j=k=l loops (none for off-diagonal contributions):
c***********************************************************
      lmo=lmo+1
      if (lmo.le.niot) goto 800
       ibf=ibf+1
       icodev(ibf)=0
c
      write(6,6010)nloop
6010  format(/' four-internal ft construction complete.',
     + i8,' loops constructed.')
c
      return
6100  format(' int4:  lmo,kmo,jmo,imo = ',4i4)
      end


      subroutine intab
c
c  initialize tables for segment value calculations.
c
c  for details of segment evaluation and the auxiliary functions,
c  see:
c        i. shavitt, in "the unitary group for the evaluation of
c        electronic energy matrix elements", j. hinze, ed.
c        (springer-verlag, berlin, 1981) p. 51. and i. shavitt,
c        "new methods in computational quantum chemistry and
c        their application on modern super-computers" (annual
c        report to the national aeronautics and space administration),
c        battelle columbus laboratories, june 1979.
c
c  mu-dependent segment evaluation entries added 7-nov-86 (rls).
c  this version written 01-aug-84 by ron shepard.
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine intab
C
C     Statement functions
C
      REAL*8              RX,          XA,          XB,          XC
      REAL*8              XD,          XF,          XR
C
C     Parameter variables
C
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
      REAL*8              HALF
      PARAMETER           (HALF = .5D0)
C
C     Local variables
C
      INTEGER             B,           I,           P,           Q
      INTEGER             R,           S
C
      REAL*8              SQRT2,       T
C
C
C     Common variables
C
      REAL*8              TAB(NTAB,0:MAXB)
C
      COMMON / CTAB   /   TAB
C
C    END OF VARIABLE DECLARATIONS FOR subroutine intab


c  table choice is dictated by the requirement that all segment
c  values are to be calculated with array references only and
c  with no conditional tests or goto's required.  this requires
c  an over-complete set of tables to allow for sign changes and
c  constant factors.
c**********************************************************************
c  table entry definitions:
c   1:0             2:1             3:-1            4:2
c   5:-2            6:sqrt2         7:-t            8:1/b
c   9:-sqrt2/b     10:1/(b+1)      11:-1/(b+1)     12:-1/(b+2)
c  13:-sqrt2/(b+2) 14:a(-1,0)      15:-a(-1,0)     16:a(1,0)
c  17:-a(1,0)      18:a(0,1)       19:-a(0,1)      20:a(2,1)
c  21:-a(2,1)      22:a(1,2)       23:-a(1,2)      24:a(3,2)
c  25:-a(3,2)      26:b(-1,0)      27:b(0,1)       28:-b(0,2)
c  29:b(1,2)       30:b(2,3)       31:c(0)         32:-c(0)
c  33:c(1)         34:c(2)         35:-c(2)        36:d(-1)
c  37:d(0)         38:-d(0)        39:d(1)         40:-d(1)
c  41:d(2)         42:ta(-1,0)     43:-ta(-1,0)    44:ta(1,0)
c  45:ta(2,0)      46:-ta(2,0)     47:-ta(-1,1)    48:ta(0,1)
c  49:ta(2,1)      50:-ta(2,1)     51:ta(3,1)      52:ta(0,2)
c  53:-ta(0,2)     54:ta(1,2)      55:ta(3,2)      56:-ta(3,2)
c         added for mu-dependent segment evaluation
c  57:1/2          58:-1/2         59:-1/b         60:1/(b+2)
c  61:(b+1)/b      62:(b+1)/(b+2)  63:(2b+1)/2b    64:(2b+3)/(2b+4)
c  65:1/2*a(1,0)   66:1/2*a(0,1)   67:1/2*a(2,1)   68:1/2*a(1,2)
c  69:1/2*c(0)     70:1/2*c(2)
c**********************************************************************
c     # auxiliary functions:
c     # rx() is a precision-independent integer-to-real conversion
c     # function. -rls
      rx(p)         = (p)
      xa(p,q,b)     = sqrt(rx(b+p) / rx(b+q))
      xb(p,q,b)     = sqrt(two / rx((b+p) * (b+q)))
      xc(p,b)       = sqrt(rx((b+p-1) * (b+p+1))) / rx(b+p)
      xd(p,b)       = sqrt(rx((b+p-1) * (b+p+2)) / rx((b+p) * (b+p+1)))
      xr(p,b)       = one / rx(b+p)
      xf(p,q,r,s,b) = rx(p*b+q) / rx(r*b+s)
c
      sqrt2 = sqrt(two)
      t     = one / sqrt2
c
      do 110 b = 0, maxb
         do 100 i = 1, ntab
            tab(i,b) = zero
100      continue
110   continue
c
      do 200 b = 0, maxb
         tab( 1,b) = zero
         tab( 2,b) = +one
         tab( 3,b) = -one
         tab( 4,b) = +two
         tab( 5,b) = -two
         tab( 6,b) = +sqrt2
         tab( 7,b) = -t
         tab(10,b) = +xr(1,b)
         tab(11,b) = -xr(1,b)
         tab(12,b) = -xr(2,b)
         tab(13,b) = -sqrt2*xr(2,b)
         tab(18,b) = +xa(0,1,b)
         tab(19,b) = -xa(0,1,b)
         tab(20,b) = +xa(2,1,b)
         tab(21,b) = -xa(2,1,b)
         tab(22,b) = +xa(1,2,b)
         tab(23,b) = -xa(1,2,b)
         tab(24,b) = +xa(3,2,b)
         tab(25,b) = -xa(3,2,b)
         tab(29,b) = +xb(1,2,b)
         tab(30,b) = +xb(2,3,b)
         tab(34,b) = +xc(2,b)
         tab(35,b) = -xc(2,b)
         tab(39,b) = +xd(1,b)
         tab(40,b) = -xd(1,b)
         tab(41,b) = +xd(2,b)
         tab(48,b) = +t * xa(0,1,b)
         tab(49,b) = +t * xa(2,1,b)
         tab(50,b) = -t * xa(2,1,b)
         tab(51,b) = +t * xa(3,1,b)
         tab(52,b) = +t * xa(0,2,b)
         tab(53,b) = -t * xa(0,2,b)
         tab(54,b) = +t * xa(1,2,b)
         tab(55,b) = +t * xa(3,2,b)
         tab(56,b) = -t * xa(3,2,b)
         tab(57,b) = +half
         tab(58,b) = -half
         tab(60,b) = +xr(2,b)
         tab(62,b) = +xf(1,1,1,2,b)
         tab(64,b) = +xf(2,3,2,4,b)
         tab(66,b) = +half * xa(0,1,b)
         tab(67,b) = +half * xa(2,1,b)
         tab(68,b) = +half * xa(1,2,b)
         tab(70,b) = +half * xc(2,b)
200   continue
c
      do 300 b = 1, maxb
         tab( 8,b) = +xr(0,b)
         tab( 9,b) = -sqrt2 * xr(0,b)
         tab(14,b) = +xa(-1,0,b)
         tab(15,b) = -xa(-1,0,b)
         tab(16,b) = +xa(1,0,b)
         tab(17,b) = -xa(1,0,b)
         tab(27,b) = +xb(0,1,b)
         tab(28,b) = -xb(0,2,b)
         tab(31,b) = +xc(0,b)
         tab(32,b) = -xc(0,b)
         tab(33,b) = +xc(1,b)
         tab(37,b) = +xd(0,b)
         tab(38,b) = -xd(0,b)
         tab(42,b) = +t * xa(-1,0,b)
         tab(43,b) = -t * xa(-1,0,b)
         tab(44,b) = +t * xa(1,0,b)
         tab(45,b) = +t * xa(2,0,b)
         tab(46,b) = -t * xa(2,0,b)
         tab(47,b) = -t * xa(-1,1,b)
         tab(59,b) = -xr(0,b)
         tab(61,b) = +xf(1,1,1,0,b)
         tab(63,b) = +xf(2,1,2,0,b)
         tab(65,b) = +half * xa(1,0,b)
         tab(69,b) = +half * xc(0,b)
300   continue
c
      do 400 b = 2, maxb
         tab(26,b) = +xb(-1,0,b)
         tab(36,b) = +xd(-1,b)
400   continue
c
      return
      end


      subroutine istack
c
c  initialize necessary stack information for loop construction.
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine istack
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 50)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
C
C     Local variables
C
      INTEGER             I
C
C
C     Common variables
C
      INTEGER             BROW(0:NIMOMX),           EXTBK(0:NIMOMX)
      INTEGER             KROW(0:NIMOMX),           RANGE(0:NIMOMX)
      INTEGER             YB(0:NIMOMX),             YK(0:NIMOMX)
C
      LOGICAL             QEQ(0:NIMOMX)
C
      REAL*8              V(3,0:NIMOMX)
C
      COMMON / CSTACK /   V,           RANGE,       EXTBK,       QEQ
      COMMON / CSTACK /   BROW,        KROW,        YB,          YK
C
C    END OF VARIABLE DECLARATIONS FOR subroutine istack


      do 100 i = 0, nimomx
         extbk(i) = 0
         v(1,i)   = zero
         v(2,i)   = zero
         v(3,i)   = zero
         yb(i)    = 0
         yk(i)    = 0
100   continue
c
      return
      end


      logical function detvalpth
     . (index,conft,indsym,numpth,confw,ipth,cistrt,segspc,numcsf,
     .  sgtype,pthcsf)
       implicit none 
       integer pthcsf,index(*),conft(*),numpth,confw,ipth,cistrt
      integer indsym(*),sgtype
       integer cmpsym,wtype,segspc,numcsf
       parameter (wtype = 2)
       integer maxsym
       parameter (maxsym = 8)
       integer atebyt,forbyt
       external atebyt,forbyt
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C


c  common data



       if (index(ipth).lt.0) then
         detvalpth=.false.
          pthcsf=0
ctm
          segspc=atebyt(numcsf)+forbyt(numpth)
       else
         cmpsym=sym21(indsym(index(ipth)))

         if (sgtype.eq.wtype) then
           pthcsf = cnfw(cmpsym)
           confw  = confw+pthcsf
         else
           pthcsf = cnfx(cmpsym)
         endif

         if (pthcsf.le.0) then
           conft(index(ipth)) = -1
           index(ipth) = -1
           call bummer(' detvalpth: zero externals for path no',
     .                  ipth,0)
         else
          conft(index(ipth)) = numcsf + cistrt
          segspc= atebyt(numcsf+pthcsf)+forbyt(numpth+1)
         endif
         detvalpth=.true.
        endif
        return
        end


      subroutine matmul (a,b,c,ni,nj,nk,itranc)
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine matmul
C
C     Argument variables
C
      INTEGER             ITRANC,      NI,          NJ,          NK
C
      REAL*8              A(NI,NJ),    B(NI,NK),    C(2)
C
C     Local variables
C
      INTEGER             IC,          J,           K
C
      REAL*8              HELP
C
C    END OF VARIABLE DECLARATIONS FOR subroutine matmul


c   matrix multiply  a = b*c(tr)
c   a(i,j) = sum(k) b(i,k)*c(j,k)
c   if itranc.ne.0 c is provided transposed
      if (itranc.ne.0) go to 740
      ic = 0
      do 730 k=1,nk
          do 720 j=1,nj
              help = c(ic+j)
              call daxpy_wr(ni,help,b(1,k),1,a(1,j),1)
  720     continue
          ic = ic + nj
  730 continue
      return
  740 ic = 0
      do 770 j=1,nj
          do 760 k=1,nk
              help = c(ic+k)
              call daxpy_wr(ni,help,b(1,k),1,a(1,j),1)
  760     continue
          ic = ic + nk
  770 continue
      return
      end


      subroutine mtmls (a,b,c,ni,nj,nk,itranc,fac)
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine mtmls
C
C     Argument variables
C
      INTEGER             ITRANC,      NI,          NJ,          NK
C
      REAL*8              A(*),        B(NI,NK),    C(2),fac
C
C     Local variables
C
      INTEGER             IC,          IJ,          J,           K
      INTEGER             NIJ1
C
      REAL*8              HELP
C
C    END OF VARIABLE DECLARATIONS FOR subroutine mtmls


c   matrix multiply  a = b*c(tr)
c   a(i,j) = sum(k) b(i,k)*c(j,k)
c   a is symmetric  i.le.j elements are stored columnwise
c   if itranc.ne.0 c is provided transposed

c     call wzero(ni*(ni+1)/2,a,1)
c     call printmatrix(b,ni,nk,'mtmls b')
c     call printmatrix(c,nk,ni,'mtmls c')
      ij=1
      if (itranc.ne.0) go to 740
      ic = 0
      do 730 k=1,nk
          do 720 j=1,nj
              help = fac*c(ic+j)
              nij1=ni-j+1
              call daxpy_wr(nij1,help,b(1,k),1,a(ij),1)
              ij=ij+nij1
  720     continue
          ic = ic + nj
  730 continue
      call printtrigmat(a,ni,' matrix a (1st)',0)
      return
  740 ic = 0
      do 770 j=1,nj
          nij1=ni-j+1
          do 760 k=1,nk
              help = fac*c(ic+k)
              call daxpy_wr(nij1,help,b(j,k),1,a(ij),1)
  760     continue
          ij=ij+nij1
          ic = ic + nk
  770 continue
c     call printtrigmat(a,ni,' matrix a (2nd)',0)
      return
      end


      subroutine multd2( iroot1,iroot2,core,filv1,filv2 )
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine multd2
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT
C
      INTEGER             ATEBYT,      FORBYT
C
C     Parameter variables
C
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             IJIJSZ
      PARAMETER           (IJIJSZ = 1023) 
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             NSIFBF
      PARAMETER           (NSIFBF = 4095)
C
      REAL*8              TWO
      PARAMETER           (TWO = 2.0D0)
      integer             mxnseg
      parameter           (mxnseg = 100)



C
C     Argument variables
C
      INTEGER             IROOt1,iroot2
C
      REAL*8              CORE(*)
C
C     Local variables
C
      INTEGER             ADR(20), loc(20)
      INTEGER             ADRF
      INTEGER             CISG1(2),    CISG2(2)
      INTEGER             CISTR1(2,2),   CISTRT(2,2)
      INTEGER             D2DIM
      INTEGER         FIN1,FIN2,   FIRST0,      FIRST1,      FIRSTD
      INTEGER             I,           IALL
      INTEGER             IDIAG
      INTEGER             IFOUR,       IMULTC,      IONE
      INTEGER             IPTH1(2),  ISGEXC,    ITHRE
      INTEGER             ITWO,        IWXX(2)
      INTEGER             KSG1,        KSG2,        LENRD2
      INTEGER             LOC11
      INTEGER             LOC12,       LOC13,       LOC14,       LOC15
      INTEGER             LOC12b,      LOC13b,      LOC14B,      LOC15b
      INTEGER             LOC16,       LOC1MX
      INTEGER             LOCD1
      INTEGER             LOCD2,       LOCD3,       LOCD4,       LOCIO
      integer             locd2b
      INTEGER             LOCRD2,      LOCWX,       LOGREC1,     N1EL
      INTEGER             N300,        N300M,       nmsga
      INTEGER             NMSG1
      INTEGER             PTH1S,       PTH2S
      INTEGER             SCR1DM,      SCR2DM,      SCR3DM
      INTEGER             SEG1,     SEG2,     SEGSM(2)
      INTEGER             SEGSM1(2),logrec2,itm

      INTEGER*8           OFFSET
C
      LOGICAL             CALL1
      LOGICAL             CLC12
      LOGICAL             LSTSEG
      logical             skip1x, skipdg,skip0x
C
      REAL*8              D2IL(MXINOR*(MXINOR+1))
C
C
C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS
C
C
C     Equivalenced common variables
C
      character*(*)       FILV1,filv2
      INTEGER             FLINDX1,flindx2,      TAPE6
C
C
C     Common variables
C
C
      INTEGER             CIST(2,MXNSEG), CIVCST(2,MXNSEG)
      INTEGER             INDXST(2,MXNSEG), LENCI(2),  NSEG
      INTEGER             NSEGW, SEGCI(2,MXNSEG)
      INTEGER             SEGEL(2,MXNSEG) ,recci


      COMMON / CIVCT  /   LENCI,       NSEG,        NSEGW, recci,SEGEL
      COMMON / CIVCT  /   CIST,        INDXST,      CIVCST,      SEGCI
C
C
C     Common variables
C
      INTEGER             ICNFST(2,MXNSEG),  ISGCNF(2,MXNSEG)
C
      COMMON / CNFSPC /   ICNFST,      ISGCNF
C
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYM,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      spcci
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
      INTEGER            sBUFSZI,     sBUFSZL,     sCONFYZ,     sDIMCI
      INTEGER            sINTORB,     sKBL3,       sKBL4,       sLAMDA1
      INTEGER            sMAXBL2,     sMAXBL3,     sMAXBL4,     sMAXLP3
      INTEGER            sMINBL3,     sMINBL4,     sMXBL23,     sMXBLD
      INTEGER            sMXKBL3,     sMXKBL4,     sMXORB,      sN0EXT
      INTEGER            sN0XTLP,     sN1EXT,      sN1XTLP,     sN2EXT
      INTEGER            sN2XTLP,     sN3EXT,      sN3XTLP,     sN4EXT
      INTEGER            sND0EXT,     sND2EXT,     sND4EXT,     sNELI
      INTEGER            sNEXT,       sNFCT,       sNINTPT,     sNMBLK3
      INTEGER            sNMBLK4,     sNMONEL,     sNVALW,      sNVALWK
      INTEGER            sNVALX,      sNVALY,      sNVALZ,      sPTHW
      INTEGER            sPTHX,       sPTHY,       sPTHYZ,      sPTHZ
      INTEGER            sTOTSPC,     sspcci
C
      COMMON / SECINF /  sPTHZ,       sPTHY,       sPTHX,       sPTHW
      COMMON / SECINF /  sNINTPT,     sDIMCI,      sMAXBL4,     sNMBLK4
      COMMON / SECINF /  sKBL4,       sMXKBL4,     sLAMDA1,     sBUFSZI
      COMMON / SECINF /  sNMONEL,     sINTORB,     sMAXBL3,     sNMBLK3
      COMMON / SECINF /  sKBL3,       sMXKBL3,     sMAXLP3,     sBUFSZL
      COMMON / SECINF /  sMAXBL2,     sMXBL23,     sMXORB,      sMXBLD
      COMMON / SECINF /  sNEXT,       sTOTSPC,     sMINBL4,     sMINBL3
      COMMON / SECINF /  sND4EXT,     sND2EXT,     sND0EXT,     sN4EXT
      COMMON / SECINF /  sN3EXT,      sN2EXT,      sN1EXT,      sN0EXT
      COMMON / SECINF /  sN3XTLP,     sN2XTLP,     sN1XTLP,     sN0XTLP
      COMMON / SECINF /  sSPCCI,      sCONFYZ,     sPTHYZ,      sNVALWK
      COMMON / SECINF /  sNVALZ,      sNVALY,      sNVALX,      sNVALW
      COMMON / SECINF /  sNFCT,       sNELI

C
      INTEGER             NREC35,      NREC36, ssym1,ssym2
      COMMON / INF4   /   NREC35,      NREC36, ssym1,ssym2
C
C
      INTEGER             IJIJ(IJIJSZ)
      COMMON / INF4X  /   IJIJ
c
c mxinor=maximum number of internal orbitals
c nmotx = maximum number of molecular orbitals
      equivalence (nunits(1),tape6)
      equivalence (nunits(14),flindx1)
      equivalence (nunits(15),flindx2)
c
c-----------------------------------------------------------------------
c
       skip1x = .false.
       skipdg = .false.
       skip0x = .false.

       call izero_wr(20,loc,1)
       call izero_wr(20,adr,1)


      if(intorb.gt.mxinor)then
          write(tape6,20200)intorb,mxinor
20200 format(' not enough space for internal orbitals in multd2',
     +' intorb:',i4,' mxinor:',i4)
          stop
      endif
c
      call timer(' ',1,imultc,tape6)
c
      call timer(' ',1,ifour,tape6)
      call timer(' ',5,ifour,tape6)
c
      call timer(' ',1,ithre,tape6)
      call timer(' ',5,ithre,tape6)
c
      call timer(' ',1,itwo,tape6)
      call timer(' ',5,itwo,tape6)
c
      call timer(' ',1,ione,tape6)
      call timer(' ',5,ione,tape6)
c
      call timer(' ',1,iall,tape6)
      call timer(' ',5,iall,tape6)
c
      call timer(' ',1,idiag,tape6)
      call timer(' ',5,idiag,tape6)
c
c     the space allocation for core is computed
c     in terms of real*8 words
c     core map:
c
c     diese programmweit benutzen Adressen sollten in
c     einen Common Block hinein.
c
c     indsym1 ( nvalwk )   integer*4   loc(1)
c     conft1  ( nvalwk )   integer*4   loc(2)
c     indsym2 ( snvalwk )   integer*4  loc(3)
c     conft2  ( snvalwk )   integer*4  loc(4)
c     indxyz1 ( pthyz  )   integer*4   loc(5)
c     indxyz2 ( pthyz  )   integer*4   loc(6)
c     ciyz1   ( confyz )   real*8      loc(7)
c     ciyz2   (sconfyz )   real*8      loc(8)
c
c  actually calculate the locations
c
      loc(1)  =  1
      loc(2)  =  loc(1)  +  forbyt( nvalwk )
      loc(3)  =  loc(2)  +  forbyt( nvalwk )
      loc(4)  =  loc(3)  +  forbyt( snvalwk )
      loc(5)  =  loc(4)  +  forbyt( snvalwk )
      loc(6)  =  loc(5)  +  forbyt( pthyz )
      loc(7)  =  loc(6)  +  forbyt( spthyz )
      loc(8)  =  loc(7)  + atebyt(confyz)
      loc(9)  =  loc(8)  + atebyt(sconfyz)

      locrd2 = loc(9)
      lenrd2 = atebyt(confyz)
      locio =  locrd2 + lenrd2
      locwx =  locio +  2*atebyt(nsifbf) + 4*forbyt(nsifbf)
c
c     adr(1)  index11        [index1(*)]1    integer*4
c     adr(2)  ci11           [ci1(*)   ]1    real*8
c     adr(3)  index12        [index2(*)]1    integer*4
c     adr(4)  ci12           [ci2(*)   ]1    real*8
c     adr(5)  index21        [index1(*)]2    integer*4
c     adr(6)  ci21           [ci1(*)   ]2    real*8
c     adr(7)  index22        [index1(*)]2    integer*4
c     adr(8)  ci22           [ci2(*)   ]2    real*8
c
      adr(1)=locwx
c
*@ifdef debug
*       write(tape6,22301) (loc(i),i=1,10)
*       write(tape6,22302) (adr(i),i=1,10)
*22301  format('loc(*)=',10i6)
*22302  format('adr(*)=',10i6)
*@endif

      logrec1 = iroot1
      logrec2 = iroot2
      write(tape6,22220) iroot1,iroot2
22220 format(20('####')/
     . '              calculation for roots no ', i4, '+', i4 /
     .       20('####'))

c
c     # get y-z segment from da files
c
c    # bra
c
      call diracc(flindx1,1,core(loc(5)),pthyz,nrec35,
     .             indxst(1,1),fin1)

c    # ket
c
      call diracc(flindx2,1,core(loc(6)),spthyz,nrec35,
     .            indxst(2,1),fin2)

c      call countindex('yz',core(loc3),pthyz)
c
c    # load bra yz segment
c
c     call getvec(filv1,nrec36,logrec1,lenci(1),core(loc(7)),
c    .          cist(1,1)+1, segci(1,1))

      offset=lenci(1)*(logrec1-1)+cist(1,1)+1
      call getputvec(filv1(1:len_trim(filv1)),
     .           core(loc(7)),segci(1,1),offset,'openrd')

c
c    # load ket yz segment
c
c     call getvec(filv2,nrec36,logrec2,lenci(2),core(loc(8)),
c    .       cist(2,1)+1, segci(2,1))
      offset=lenci(2)*(logrec2-1)+cist(2,1)+1
      call getputvec(filv2(1:len_trim(filv2)),
     .         core(loc(8)),segci(2,1),offset,'openrd')
c
c
      pth1s=pthyz+pthx
c was ist mit pth2s
      pth2s=pthyz+pthx
c ????????

      first1=0
      first0=0
      firstd=0
      isgexc=0
      lstseg=.false.
c
      call wzero(intorb*(intorb+1),d2il,1)

c     AVAILABILITY of DA file information
c   --------------------------------------------------------------------
c  #        density calculation           transition density calc.
c  #  ==================================================================
c  #        ciyz,ci(seg1),ci(seg2)        ciyz1,ciyz2, ci1(seg1),ci2(seg
c  #                                      ci1(seg2) ci2(seg2)
c  #        indxyz, index(seg1)           indxyz1,indxyz2,index1(seg1)
c  #                                       index1(seg2),index2(seg1)
c  #        index(seg2)                   index2(seg2)
c

      if (nseg.eq.1) then
c      only one yz segment

c      set variables to default values
        iwxx(1)=0
        iwxx(2)=0
        seg1=0
        seg2=0
         do itm=2,8
           adr(itm)=adr(1)
         enddo
         adrf=adr(1)
        nmsg1=0
        nmsga=0
        lstseg=.true.


c***********************************************************************
c     one external
c***********************************************************************
              call timer(' ',6,ione,tape6)
c
c     core( loc11 )   lpbuf     ( bufszl )            real*8
c     core( loc12 )   d2_sym    ( mxorb * 3     )     real*8
c     core( loc12b)   d2_asym   ( mxorb * 3     )     real*8
c     core( loc13 )   scr1_sym  ( mxorb         )     real*8
c     core( loc13b)   scr1_asym  ( mxorb         )     real*8
c     core( loc14 )   scr3_sym  ( mxorb         )     real*8
c     core( loc14b)   scr3_asym ( mxorb         )     real*8
c     core( loc15 )   tran      ( mxorb * mxorb )     real*8
c
              loc11  = adrf
              loc12  = loc11  + atebyt( bufszl )
              loc12b = loc12  + atebyt( mxorb * 3)
              loc13  = loc12b + atebyt( mxorb * 3)
              loc13b = loc13  + atebyt( mxorb )
              loc14  = loc13b + atebyt( mxorb )
              loc14b = loc14  + atebyt( mxorb )
              loc15  = loc14b + atebyt( mxorb )
              loc1mx = loc15  + atebyt( mxorb * mxorb ) - 1
              if(loc1mx.gt.totspc)then
               write(tape6,20050)totspc,loc1mx
               call bummer('1:multd2: not enough memory ',0,faterr)
              endif
c
                  d2dim = loc12b - loc12 + 1
                  scr1dm = loc13b -loc13 + 1
                  scr2dm = loc14b - loc14 + 1

c   ONEXT(nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               loc11,loc12,loc13,loc14,loc15,loc16,
c               ipth1,segsm(*,*),cistrt(*,*),
c               first1,nmsg1,d2dim,scr1dm,scr2dm,scr3dm)
      call onext(
     .    core(loc(1)), core(loc(2)), core(loc(5)), core(loc(7)),
     .    core(loc(3)), core(loc(4)), core(loc(6)), core(loc(8)),
     .    core(adr(1)), core(adr(2)), core(adr(3)), core(adr(4)),
     .    core(adr(5)), core(adr(6)), core(adr(7)), core(adr(8)),
     +    core(loc12),  core(loc12b), core(loc13),  core(loc13b),
     &    core(loc14),  core(loc14b), core(loc15),  ipth1,
     &    segsm,        cistrt,       first1,       nmsg1,
     &    d2dim,        scr1dm,       scr2dm )

              first1=1
              call timer(' ',5,ione,tape6)
3123          continue
c***********************************************************************
c
c     all internal diagonal and all external one-electron density
c
c***********************************************************************
              call timer(' ',6,idiag,tape6)
              n1el = cnfw(1)
              locd1 = adrf
              locd2 = locd1
              locd2b= locd2 + atebyt( n1el )
              locd3 = locd2b + atebyt( n1el )
              locd4 = locd3 + atebyt( 2 * n1el )
              loc1mx = locd4 + atebyt( 2 * n1el ) - 1
              if(loc1mx.gt.totspc)then
               write(tape6,20080)totspc,loc1mx
               call bummer('2:multd2: not enough memory ',0,faterr)
              endif
c
c   DIAGD (nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               locd1,d2il,locd2, locd3, locd4,
c               segsm(*,*),cistrt(*,*), ipth1,firstd,lstseg,nmsg1,
c               iwxx(*,*), nsifbf, locio
       call diagd(nmsym,
     .  core(loc(1)),  core(loc(2)),  core(loc(5)),  core(loc(7)),
     .  core(loc(3)),  core(loc(4)),  core(loc(6)),  core(loc(8)),
     .  core(adr(1)),  core(adr(2)),  core(adr(3)),  core(adr(4)),
     .  core(adr(5)),  core(adr(6)),  core(adr(7)),  core(adr(8)),
     +  d2il,          core(locd2),   core(locd2b),  core(locd3),
     &  core(locd4),   segsm,         cistrt,        ipth1,
     &  firstd,        lstseg,        nmsg1,         iwxx)

              call timer(' ',5,idiag,tape6)
c********************************************************************
c
c     all internal
c
c********************************************************************
3125          call timer(' ',6,iall,tape6)
c   ALLINT (nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               loc01,d2il,ipth1,segsm,cistrt,isgexc,nmsga,
c               lstseg,first0,0,ksg1,ksg2)

       call allint(
     .            core(loc(1)),core(loc(2)),core(loc(5)),core(loc(7)),
     .            core(loc(3)),core(loc(4)),core(loc(6)),core(loc(8)),
     .            core(adr(1)),core(adr(2)),core(adr(3)),core(adr(4)),
     .            core(adr(5)),core(adr(6)),core(adr(7)),core(adr(8)),
     +            ipth1,       segsm,       cistrt,      isgexc,
     &            nmsga,       lstseg,      first0,      next)
              call timer(' ',5,iall,tape6)

         endif

      if (nseg .eq.2 ) then
ctm
        seg2=0
c
ctm  adr4 and adr5 used for the single segment!

          ksg1=2
          seg1=segel(1,ksg1)
          cisg1(1)=segci(1,ksg1)
          cisg1(2)=segci(2,ksg1)
          adr(2) = adr(1) + forbyt(seg1)
          adr(3) = adr(2) + atebyt(cisg1(1))
          adr(4) = 0
          adr(5) = adr(3)
          adr(6) = adr(5) + forbyt(seg1)
          adr(7) = adr(6) + atebyt(cisg1(2))
          adrf=adr(7)
          lstseg=.true.
ctm x-paths : iwxx=1
ctm w-paths : iwxx=2
          segsm(1)=seg1
          segsm1(1)=seg1

          cistrt(1,1)=cist(1,ksg1)
          cistrt(2,1)=cist(2,ksg1)
          ipth1(1)=pthyz
          iwxx(1)=2
          if(ksg1.ge.nsegw)iwxx(1)=1

c  load the two segments for bra and ket
       call diracc(flindx1,1,core(adr(1)),seg1,nrec35,
     .              indxst(1,ksg1),fin1)
       call diracc(flindx2,1,core(adr(5)),seg1,nrec35,
     .              indxst(2,ksg1),fin2)

c      call getvec(filv1,nrec36,logrec1,lenci(1),core(adr(2)),
c    .     cist(1,ksg1)+1, cisg1(1))

       offset=(logrec1-1)*lenci(1)+cist(1,ksg1)+1 
       call getputvec(filv1(1:len_trim(filv1)),
     .           core(adr(2)),cisg1(1),offset,'openrd')

c      call getvec(filv2,nrec36,logrec2,lenci(2),core(adr(6)),
c    .     cist(2,ksg1)+1, cisg1(2))

       offset=(logrec2-1)*lenci(2)+cist(2,ksg1)+1 
       call getputvec(filv2(1:len_trim(filv2)),
     .                core(adr(6)),cisg1(2),offset,'openrd')

              nmsg1=1
c
c***********************************************************************
c
c     one external
c
c***********************************************************************
              call timer(' ',6,ione,tape6)
c
c     mxorb    blocksize for the two electron integrals(one external)
c     bufszl   buffersize for formula tape
c
c
c  core map:
c
c     core( loc11 )   lpbuf     ( bufszl )            real*8
c     core( loc12 )   d2_sym    ( mxorb * 3     )     real*8
c     core( loc12b)   d2_asym   ( mxorb * 3     )     real*8
c     core( loc13 )   scr1_sym  ( mxorb         )     real*8
c     core( loc13b)   scr1_asym ( mxorb         )     real*8
c     core( loc14 )   scr2      ( mxorb         )     real*8
c     core( loc14 )   scr3_sym  ( mxorb         )     real*8
c     core( loc14b)   scr3_asym ( mxorb         )     real*8
c     core( loc15 )   tran      ( mxorb * mxorb )     real*8
c
              loc11  = adrf
              loc12  = loc11  + atebyt( bufszl )
              loc12b = loc12  + atebyt( mxorb * 3)
              loc13  = loc12b + atebyt( mxorb * 3)
              loc13b = loc13  + atebyt( mxorb )
              loc14  = loc13b + atebyt( mxorb )
              loc14b = loc14  + atebyt( mxorb )
              loc15  = loc14b + atebyt( mxorb )
              loc1mx = loc15  + atebyt( mxorb * mxorb ) - 1
              if(loc1mx.gt.totspc)then
               write(tape6,20050)totspc,loc1mx
               call bummer('3:multd2: not enough memory ',0,faterr)
              endif
c
                  d2dim = loc12b - loc12 + 1
                  scr1dm = loc13b -loc13 + 1
                  scr2dm = loc14b - loc14 + 1
c
c   ONEXT(nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               loc11,loc12,loc13,loc14,loc15,loc16,
c               ipth1,segsm(*,*),cistrt(*,*),
c               first1,nmsg1,d2dim,scr1dm,scr2dm,scr3dm)

      call onext(
     . core(loc(1)),  core(loc(2)),  core(loc(5)),  core(loc(7)),
     . core(loc(3)),  core(loc(4)),  core(loc(6)),  core(loc(8)),
     . core(adr(1)),  core(adr(2)),  core(adr(3)),  core(adr(4)),
     . core(adr(5)),  core(adr(6)),  core(adr(7)),  core(adr(8)),
     + core(loc12),   core(loc12b),  core(loc13),   core(loc13b),
     . core(loc14),   core(loc14b),  core(loc15),   ipth1,
     & segsm,         cistrt,        first1,        nmsg1,
     & d2dim,         scr1dm,        scr2dm )

                  first1=1
              call timer(' ',5,ione,tape6)
c***********************************************************************
c
c     all internal diagonal and all external one-electron density
c
c***********************************************************************
 3130         continue
              call timer(' ',6,idiag,tape6)
c
c     bufszl   buffersize for formula tape
c     n1el     number of one-electron elements
c     d2il(*)   two-particle density matrix elements (i,i/l,l)
c
c  core map:
c
c     core( locd1 )   lpbuf  ( bufszl )            real*8
c     core( locd2 )   scr1   ( n1el          )     real*8
c     core( locd2b)   scr1_asym  ( n1el          )     real*8
c     core( locd3 )   tran1  ( 2 * n1el      )     real*8
c     core( locd4 )   tran2  ( 2 * n1el      )     real*8
c
              n1el = cnfw(1)
              locd1 = adrf
              locd2 = locd1
              locd2b= locd2 + atebyt( n1el )
              locd3 = locd2b + atebyt( n1el )
              locd4 = locd3 + atebyt( 2 * n1el )
              loc1mx = locd4 + atebyt( 2 * n1el ) - 1
              if(loc1mx.gt.totspc)then
               write(tape6,20080)totspc,loc1mx
               call bummer('4:multd2: not enough memory ',0,faterr)
              endif
c
c   DIAGD (nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               locd1,d2il,locd2, locd3, locd4,
c               segsm(*,*),cistrt(*,*), ipth1,firstd,lstseg,nmsg1,
c               iwxx(*,*), nsifbf, locio

       call diagd(nmsym,
     .  core(loc(1)),  core(loc(2)),  core(loc(5)),  core(loc(7)),
     .  core(loc(3)),  core(loc(4)),  core(loc(6)),  core(loc(8)),
     .  core(adr(1)),  core(adr(2)),  core(adr(3)),  core(adr(4)),
     .  core(adr(5)),  core(adr(6)),  core(adr(7)),  core(adr(8)),
     +  d2il,          core(locd2),   core(locd2b),  core(locd3),
     &  core(locd4),   segsm,         cistrt,        ipth1,
     &  firstd,        lstseg,        nmsg1,         iwxx)

c
              call timer(' ',5,idiag,tape6)
c********************************************************************
c
c     all internal
c
c********************************************************************
              call timer(' ',6,iall,tape6)
c
c     bufszl   buffersize for formula tape
c
c
c   ALLINT (nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               loc01,d2il,ipth1,segsm,cistrt,isgexc,nmsga,
c               lstseg,first0,0,ksg1,ksg2)

       call allint(
     .            core(loc(1)),core(loc(2)),core(loc(5)),core(loc(7)),
     .            core(loc(3)),core(loc(4)),core(loc(6)),core(loc(8)),
     .            core(adr(1)),core(adr(2)),core(adr(3)),core(adr(4)),
     .            core(adr(5)),core(adr(6)),core(adr(7)),core(adr(8)),
     +            ipth1,segsm,cistrt,isgexc,1,
     .            lstseg,first0,next)

              first0=1
              call timer(' ',5,iall,tape6)
c zu korrigieren
              pth2s=pth2s+seg2
c
c zu korrigieren
          pth1s=pth1s+seg1
c
      endif
c

      if (nseg.ge.3) then
c
c     loop over segments of the ci and sigma vectors
c
      do 1000 ksg1=2,nseg-1
c
          seg1=segel(1,ksg1)
          cisg1(1)=segci(1,ksg1)
          cisg1(2)=segci(2,ksg1)

          adr(2)=adr(1)+forbyt(seg1)
          adr(5)=adr(2)+atebyt(cisg1(1))
          adr(6)=adr(5)+forbyt(seg1)
          adr(3)=adr(6)+atebyt(cisg1(2))
c
c adr(3) points to the top
c
          if(ksg1.eq.nsegw)pth1s=pthyz
          segsm(1)=seg1
          segsm1(1)=seg1
          cistrt(1,1)=cist(1,ksg1)
          cistrt(2,1)=cist(2,ksg1)
          cistr1(1,2)=cist(1,ksg1)
          cistr1(2,2)=cist(2,ksg1)

          ipth1(1)=pth1s
          iwxx(1)=2
          if(ksg1.ge.nsegw)iwxx(1)=1


*@ifdef debug
*          write(*,*) '--------------------------------------'
*          write(*,*) 'ksg1=',ksg1
*          write(*,*) 'segsm(*)=',segsm(1),segsm(2),
*     .   'segsm1(*)=',segsm1(1),segsm1(2),
*     .   'cistrt(*,*)=',cistrt(1,1),cistrt(1,2),
*     .    cistrt(2,1),cistrt(2,2)
*          write(*,*) 'cistr1(*,*)=',cistr1(1,1),
*     .    cistr1(1,2),cistr1(2,1),cistr1(2,2)
*     .    , 'ipth1(*)=',ipth1(1),ipth1(2),
*     .    'ipth11=',pth1s,'iwxx(*)=',iwxx(1),iwxx(2)
*          write(*,*) 'seg1=',seg1,' seg2=',seg2,
*     .   'cisg1(*)=',cisg1(1),cisg1(2)
*          write(*,*) 'cist(*,ksg1)=',cist(1,ksg1),cist(2,ksg1)
*          write(*,*) '--------------------------------------'
*@endif
c
c # load bra and ket part of the first segment
c
      call diracc(flindx1,1,core(adr(1)),seg1,nrec35,
     .            indxst(1,ksg1),fin1)
      call diracc(flindx2,1,core(adr(5)),seg1,nrec35,
     .            indxst(2,ksg1),fin2)

c     call getvec(filv1,nrec36,logrec1,lenci(1),core(adr(2)),
c    .         cist(1,ksg1)+1,
c    &            cisg1(1))
       offset=(logrec1-1)*lenci(1)+cist(1,ksg1)+1
       call getputvec(filv1(1:len_trim(filv1)),
     .          core(adr(2)),cisg1(1),offset,'openrd')

c     call getvec(filv2,nrec36,logrec2,lenci(2),core(adr(6)),
c    .       cist(2,ksg1)+1,
c    &            cisg1(2))
       offset=(logrec2-1)*lenci(2)+cist(2,ksg1)+1
       call getputvec(filv2(1:len_trim(filv2)),
     .           core(adr(6)),cisg1(2),offset,'openrd')
*@ifdef debug
*       write(tape6,*) ' ###########  POS 1 ############ '
*       write(tape6,22301) (loc(i),i=1,10)
*       write(tape6,22302) (adr(i),i=1,10)
*@endif
c   zu korrigieren
          pth2s=pth1s+seg1
c
          do 1010 ksg2=ksg1+1,nseg
c
              if(ksg1.eq.nseg-1.and.ksg2.eq.nseg)lstseg=.true.
              if(ksg2.eq.nsegw)pth2s=pthyz
              seg2=segel(1,ksg2)
              cisg2(1)=segci(1,ksg2)
              cisg2(2)=segci(2,ksg2)
              adr(4)=adr(3)+forbyt(seg2)
              adr(7)=adr(4)+atebyt(cisg2(1))
              adr(8)=adr(7)+forbyt(seg2)
              adr(9)=adr(8)+atebyt(cisg2(2))
              adrf=adr(9)
              if(adrf-adr(1).gt.spcci)then
c             write(tape6,20070)adr1,adr2,adr3,adr4,adr5,adr6,adrf,
c    +        seg1,seg2,cisg1, cisg2,spcci
20070 format('error in mult adr1...',20i6)
               call bummer('5:multd2: not enough memory ',0,faterr)
              endif
              segsm(2)=seg2
              cistrt(1,2)=cist(1,ksg2)
              cistrt(2,2)=cist(2,ksg2)
              ipth1(2)=pth2s
              iwxx(2)=2
              if(ksg2.ge.nsegw)iwxx(2)=1
*@ifdef debug
*          write(*,*) '--------------------------------------'
*          write(*,*) 'ksg2=',ksg2
*          write(*,*) 'segsm(*)=',segsm(1),segsm(2),
*     .   'segsm1(*)=',segsm1(1),segsm1(2),
*     .   'cistrt(*,*)=',cistrt(1,1),cistrt(1,2),
*     .    cistrt(2,1),cistrt(2,2)
*          write(*,*) 'cistr1(*,*)=',cistr1(1,1),
*     .    cistr1(1,2),cistr1(2,1),cistr1(2,2)
*     .    , 'ipth1(*)=',ipth1(1),ipth1(2),
*     .    'ipth11=',pth1s,'iwxx(*)=',iwxx(1),iwxx(2)
*          write(*,*) 'seg1=',seg1,' seg2=',seg2,
*     .   'cisg1(*)=',cisg1(1),cisg1(2),
*     .   'cisg2(*)=',cisg2(1),cisg2(2)
*          write(*,*) '--------------------------------------'
*@endif
c # load bra and ket part of the first segment
c
      call diracc(flindx1,1,core(adr(3)),seg2,nrec35,
     .            indxst(1,ksg2),fin1)
      call diracc(flindx2,1,core(adr(7)),seg2,nrec35,
     .            indxst(2,ksg2),fin2)

c     call getvec(filv1,nrec36,logrec1,lenci(1),core(adr(4)),
c    .           cist(1,ksg2)+1,
c    &            cisg2(1))
       offset=(logrec1-1)*lenci(1)+cist(1,ksg2)+1
       call getputvec(filv1(1:len_trim(filv1)),
     .          core(adr(4)),cisg2(1),offset,'openrd')

c     call getvec(filv2,nrec36,logrec2,lenci(2),core(adr(8)),
c    .          cist(2,ksg2)+1,
c    &            cisg2(2))
       offset=(logrec2-1)*lenci(2)+cist(2,ksg2)+1
       call getputvec(filv2(1:len_trim(filv2)),
     .          core(adr(8)),cisg2(2),offset,'openrd')

*@ifdef debug
*       write(tape6,*) ' ###########  POS 2 ############ '
*       write(tape6,22301) (loc(i),i=1,10)
*       write(tape6,22302) (adr(i),i=1,10)
*@endif
c     compute segment structure
c
              call1=.false.
              nmsg1=2
              if(ksg2.lt.nsegw)then
                  if(ksg2-ksg1.eq.1)then
                      if(mod(ksg2,2).eq.1)then
                          call1=.true.
                          else
                          if(ksg2.eq.nsegw-1)then
                          endif
                      endif
                  endif
                  else
                  if(ksg2-ksg1.eq.1)then
                      if(mod(ksg2,2).eq.1)then
                          call1=.true.
                          else
                          if(ksg2.eq.nseg)then
                              call1=.true.
                              nmsg1=1
                          endif
                      endif
                  endif
              endif
20030 format(1x,20i6)
500           continue
c***********************************************************************
c
c     one external
c
c***********************************************************************
              call timer(' ',6,ione,tape6)
c
c     mxorb    blocksize for the two electron integrals(one external)
c     bufszl   buffersize for formula tape
c
c
c  core map:
c
c     core( loc11 )   lpbuf     ( bufszl )            real*8
c     core( loc12 )   d2_sym    ( mxorb * 3     )     real*8
c     core( loc12b)   d2_asym   ( mxorb * 3     )     real*8
c     core( loc13 )   scr1_sym  ( mxorb         )     real*8
c     core( loc13b)   scr1_asym ( mxorb         )     real*8
c     core( loc14 )   scr2_sym  ( mxorb         )     real*8
c     core( loc14b)   scr2_asym ( mxorb         )     real*8
c     core( loc15 )   tran      ( mxorb * mxorb )     real*8
c
              loc11  = adrf
              loc12  = loc11 + atebyt( bufszl )
              loc12b = loc12 + atebyt( mxorb * 3)
              loc13  = loc12b + atebyt( mxorb * 3)
              loc13b = loc13 + atebyt( mxorb )
              loc14  = loc13b+ atebyt( mxorb )
              loc14b = loc14 + atebyt( mxorb )
              loc15  = loc14b+ atebyt( mxorb )
              loc1mx = loc15 + atebyt( mxorb * mxorb ) - 1
              if(loc1mx.gt.totspc)then
                  write(tape6,20050)totspc,loc1mx
20050 format(' not enough core storage for one external case'/
     +' available',i6,' wanted',i6)
               call bummer('6:multd2: not enough memory ',0,faterr)
              endif
c
              if(call1)then
                  d2dim = loc12b - loc12 + 1
                  scr1dm = loc13b -loc13 + 1
                  scr2dm = loc14b - loc14 + 1

c   ONEXT(nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               loc11,loc12,loc13,loc14,loc15,loc16,
c               ipth1,segsm(*,*),cistrt(*,*),
c               first1,nmsg1,d2dim,scr1dm,scr2dm,scr3dm)
*@ifdef debug
*       write(tape6,*) ' ###########  ONEXT  ############ '
*       write(tape6,22301) (loc(i),i=1,10)
*       write(tape6,22302) (adr(i),i=1,10)
*@endif


      if (.not. skip1x) then
      call onext(
     &  core(loc(1)),  core(loc(2)),  core(loc(5)),  core(loc(7)),
     &  core(loc(3)),  core(loc(4)),  core(loc(6)),  core(loc(8)),
     &  core(adr(1)),  core(adr(2)),  core(adr(3)),  core(adr(4)),
     &  core(adr(5)),  core(adr(6)),  core(adr(7)),  core(adr(8)),
     &  core(loc12),   core(loc12b),  core(loc13),   core(loc13b),
     &  core(loc14),   core(loc14b),  core(loc15),   ipth1,
     &  segsm,         cistrt,        first1,        nmsg1,
     &  d2dim,         scr1dm,        scr2dm )
      endif

                  first1=1
              endif
              call timer(' ',5,ione,tape6)
c***********************************************************************
c
c     all internal diagonal and all external one-electron density
c
c***********************************************************************
 2222         continue
              call timer(' ',6,idiag,tape6)
c
c     bufszl   buffersize for formula tape
c     n1el     number of one-electron elements
c     d2il(*)   two-particle density matrix elements (i,i/l,l)
c
c  core map:
c
c     core( locd1 )   lpbuf      ( bufszl )            real*8
c     core( locd2 )   scr1       ( n1el          )     real*8
c     core( locd2b)   scr1_asym  ( n1el          )     real*8
c     core( locd3 )   tran1      ( 2 * n1el      )     real*8
c     core( locd4 )   tran2      ( 2 * n1el      )     real*8
c
              n1el = cnfw(1)
              locd1 = adrf
              locd2 = locd1
              locd2b= locd2 + atebyt( n1el )
              locd3 = locd2b+ atebyt( n1el )
              locd4 = locd3 + atebyt( 2 * n1el )
              loc1mx = locd4 + atebyt( 2 * n1el ) - 1
              if(loc1mx.gt.totspc)then
               write(tape6,20080)totspc,loc1mx
20080 format(' not enough core storage for diagonal case'/
     +' available',i6,' wanted',i6)
               call bummer('7:multd2: not enough memory ',0,faterr)
              endif
c
              if(call1)then
c
c   DIAGD (nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               locd1,d2il,locd2, locd3, locd4,
c               segsm(*,*),cistrt(*,*), ipth1,firstd,lstseg,nmsg1,
c               iwxx(*,*), nsifbf, locio

      if (.not. skipdg)
     . call diagd(nmsym,
     .  core(loc(1)),  core(loc(2)),  core(loc(5)),  core(loc(7)),
     .  core(loc(3)),  core(loc(4)),  core(loc(6)),  core(loc(8)),
     .  core(adr(1)),  core(adr(2)),  core(adr(3)),  core(adr(4)),
     .  core(adr(5)),  core(adr(6)),  core(adr(7)),  core(adr(8)),
     +  d2il,          core(locd2),   core(locd2b),  core(locd3),
     &  core(locd4),   segsm,         cistrt,        ipth1,
     &  firstd,        lstseg,        nmsg1,         iwxx)
              endif
              call timer(' ',5,idiag,tape6)
20040 format(' not enough core storage for two external case'/
     +' available',i6,' wanted',i6)
600           continue
c********************************************************************
c
c     all internal
c
c********************************************************************
              call timer(' ',6,iall,tape6)
c
c     bufszl   buffersize for formula tape
c
c  core map:
c
c     core( loc01 )     lpbuf  ( bufszl )    real*8
c
c
              if(isgexc.ne.0)isgexc=ksg2-ksg1
              clc12=.true.
              if(ksg2.ge.nsegw.and.ksg1.lt.nsegw)clc12=.false.
              if(isgexc.le.1.or.(isgexc.gt.1.and.clc12))then

c   ALLINT (nmsym,indsym1,conft1,indxyz1,ciyz1,
c               indsym2,conft2,indxyz2,ciyz2,
c               index11,ci11,index21,ci21,
c               index12,ci12,index22,ci22,
c               loc01,d2il,ipth1,segsm,cistrt,isgexc,nmsga,
c               lstseg,first0,0,ksg1,ksg2)

      if (.not. skip0x)
     . call allint(
     .            core(loc(1)),core(loc(2)),core(loc(5)),core(loc(7)),
     .            core(loc(3)),core(loc(4)),core(loc(6)),core(loc(8)),
     .            core(adr(1)),core(adr(2)),core(adr(3)),core(adr(4)),
     .            core(adr(5)),core(adr(6)),core(adr(7)),core(adr(8)),
     +            ipth1,segsm,cistrt,isgexc,2,
     .            lstseg,first0,next)
              endif
              first0=1
              isgexc=1
              call timer(' ',5,iall,tape6)
c zu korrigieren
              pth2s=pth2s+seg2
c
 1010     continue
c
c zu korrigieren
          pth1s=pth1s+seg1
c
 1000 continue
c
       endif
      call timer('1-external elements required',4,ione,tape6)
      call timer('0-external elements required',4,iall,tape6)
      call timer('diagonal elements required',4,idiag,tape6)
      call timer('density calculation required',4,imultc,tape6)
c
      return
      end
c
c*********************************************************************
c
      subroutine oned2(
     & d23,      scr,    scr2,
     & value2,   code,   ni,
     & ntrypr)
c
       implicit none
C    VARIABLE DECLARATIONS FOR subroutine oned2
C
C     Parameter variables
C
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
C
C     Argument variables
C
      INTEGER             CODE,       NI,          NTRYPR
C
C
      REAL*8              D23(*),      SCR(*)
      REAL*8              SCR2(*),     VALUE2
C
C     Local variables
C
      INTEGER             CODEM
C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
      COMMON / CFILES /   NUNITS
      INTEGER             TAPE6
      equivalence (nunits(1),tape6)
c
c--------------------------------------------------------------------
c
c     multiply by loop value and update two-particle density matrix
c     iden1=0  compute first-order density matrix only
c          =1  compute second order density matrix
c     if(iden1.eq.0) only code=5 or 6 should occur
c
      codem=code-2
       if(codem.ne.3.and.codem.ne.4)then
              write(tape6,1000)codem
 1000 format(' error in oned2, iden1=0, codem=',i4)
              stop
       endif
  160 continue
c
c     value1*(ik/jl)+value2*(jk/il)
c     if j1=k1=l1, the one-particle density matrix is stored in d23(*)
c
          if(ntrypr.eq.1)then
c          call writex(scr2,ni)
           call daxpy_wr(ni,value2,scr2,1,d23,1)
c
           else
c
c     (ii/il) and (ll/li)
c
c          call writex(scr,ni)
           call daxpy_wr(ni,value2,scr,1,d23,1)
          endif ! end of: if(ntrypr.eq.1)then
      return
      end
c
c********************************************************************
c
      subroutine onedn1(
     & d2_sym,   d2_asym,   ni,   isym,
     & j1,       k1,        l1,   jkleq,
     & next,     lsym)
c
c     compute first order density matrix and store it on d1_sym(*)
c
       implicit none
c  ##  parameter & common block section
c
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
c nmotx = maximum number of molecular orbitals
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
c mxinor = maximum number of internal orbitals
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
C
      REAL*8              TWO, ONE, MONE
      PARAMETER           (TWO = 2.0D0,ONE=1.0d+00, MONE=-1.0d+00)
C
      INTEGER             NUNITS(NFILMX)
      COMMON / CFILES /   NUNITS
      INTEGER             TAPE6
      equivalence (nunits(1),tape6)
C
      REAL*8 d1_sym(iprpmx),d1ci_sym(iprpmx),d1_asym(iprpmx),
     &       d1ci_asym(iprpmx)
      COMMON / DEN1P  / d1_sym, d1ci_sym, d1_asym, d1ci_asym
c
      INTEGER             EQVSYM(8),   IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(8),   NFRC(8),     NFRV(8),     NMUVAL
      INTEGER             NOCC(8),     NORBSM(8),   NOXT(8)
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
c
c  ##  integer section
c
      INTEGER ISYM, i1, ind, ind1
      integer J1
      integer K1
      integer L1, lsym, l1ind, l1lv
      INTEGER ni,next
c
c  ## real*8 section
c
      real*8 d2_sym(*), d2_asym(*), factor
c
c  ## logical section
c
      logical jkleq
c
c  ## external section
c
      EXTERNAL INDD1, INDXT
      INTEGER  INDD1, INDXT
c
c----------------------------------------------------------------------
c
c -- modifizieren fuer isym <> jsym ...
c    inklusive min (...) , max( ... )

      l1lv=l1+next
      l1ind=ivout(l1lv)
      if(jkleq)then
          do 10 i1=1,ni
              ind=indxt(isym,i1)
              ind1=indd1(isym,lsym,ind,l1ind)
              d1_sym(ind1)=d1_sym(ind1)+two*d2_sym(i1)
              factor=one
              if (l1ind.lt.ind)factor=mone
              d1_asym(ind1)=d1_asym(ind1)+factor*two*d2_asym(i1)
*@ifdef debug
*       write(*,*) 'onedn1: adding ',two*d2_sym(i1), ' to ',ind1,
*     .  'with i,l(sym)=',ind,l1ind,isym,lsym
*       write(*,*) 'onedn1: adding ',two*d2_asym(i1), ' to ',ind1,
*     .  'with i,l(sym)=',ind,l1ind,isym,lsym,' factor=',factor
*       write(*,*)'i1=',i1
*@endif
c let's put this on the listing also
c     write(6,30000)isym,i1,ind,l1,l1ind,ind1,d2_sym(i1),d1_sym(ind1)
c     write(6,30000)isym,i1,ind,l1,l1ind,ind1,d2_asym(i1),
c    & d1_asym(ind1)
30000 format(' onewr',6i4,2f12.8)
c
   10     continue
          else
          write(tape6,1000)l1,k1,j1
 1000 format(' error in onedn1, jkleq is .false. l1,k1,j1=',3i4)
          stop
      endif
      return
      end
c
c*****************************************************************
c
      subroutine onext(
     &   indsym1,   conft1,    indxyz1,   ciyz1,
     &   indsym2,   conft2,    indxyz2,   ciyz2,
     &   index11,   ci11,      index21,   ci21,
     &   index12,   ci12,      index22,   ci22,
     &   d2_sym,    d2_asym,   scr1_sym,  scr1_asym,
     &   scr2_sym,  scr2_asym, tran,      ipth1,
     &   segsm,     cist,      first,     nmbseg,
     &   d2dim,     scr1dm,    scr2dm)
c*****
c     one external case
c*****
c
c     nmsym    number of irreps
c     indsym1  gives the symmetry of the internal walk bra
c     conft1   ci vector offset number for valid walks bra
c     indxyz1  index vector for y and z paths bra
c     ciyz1    ci vector for y and z walks bra
c     index11  index vector for segment 1 bra
c     index21  index vector for segment 2 bra
c     ci11     ci vector for segment 1 bra
c     ci21     ci vector for segment 2 bra
c     indsym2  gives the symmetry of the internal walk ket
c     conft2   ci vector offset number for valid walks ket
c     indxyz2  index vector for y and z paths ket
c     ciyz2    ci vector for y and z walks ket
c     index12  index vector for segment 1 ket
c     index22  index vector for segment 2 ket
c     ci12     ci vector for segment 1 ket
c     ci22     ci vector for segment 2 ket
c     lpbuf    buffer for formula tape
c     d2       second-order density matrix
c     scr1,scr2,tran scratch arrays
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2,2)   starting number for the ci vector in segments 1 and 2
c                 1,* bra  , 2,* ket
c     first=0  first call (include yz loops)
c          =1  subsequent call (omit yz loops)
c     nmbseg   number of segments
c     iden1=0  compute first-order density matrix only
c          =1  compute second order density matrix
c     lstseg=.true.  last segment has to be taken into account
c     nseg      number of segments
c     isegst    number of segment 1
c
c
      implicit none 
c
c  ##  parameter & common block section
c
c mxinor = maximum number of internal orbitals
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 32000)
C
      REAL*8              TWO
      PARAMETER           (TWO = 2.0D0)
      REAL*8              HALF
      PARAMETER           (HALF = 0.5D0)
c
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      TOTVEC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   TOTVEC,      CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C

      INTEGER            sBUFSZI,     sBUFSZL,     sCONFYZ,     sDIMCI
      INTEGER            sINTORB,     sKBL3,       sKBL4,       sLAMDA1
      INTEGER            sMAXBL2,     sMAXBL3,     sMAXBL4,     sMAXLP3
      INTEGER            sMINBL3,     sMINBL4,     sMXBL23,     sMXBLD
      INTEGER            sMXKBL3,     sMXKBL4,     sMXORB,      sN0EXT
      INTEGER            sN0XTLP,     sN1EXT,      sN1XTLP,     sN2EXT
      INTEGER            sN2XTLP,     sN3EXT,      sN3XTLP,     sN4EXT
      INTEGER            sND0EXT,     sND2EXT,     sND4EXT,     sNELI
      INTEGER            sNEXT,       sNFCT,       sNINTPT,     sNMBLK3
      INTEGER            sNMBLK4,     sNMONEL,     sNVALW,      sNVALWK
      INTEGER            sNVALX,      sNVALY,      sNVALZ,      sPTHW
      INTEGER            sPTHX,       sPTHY,       sPTHYZ,      sPTHZ
      INTEGER            sTOTSPC,     sTOTVEC
C
      COMMON / SECINF /  sPTHZ,       sPTHY,       sPTHX,       sPTHW
      COMMON / SECINF /  sNINTPT,     sDIMCI,      sMAXBL4,     sNMBLK4
      COMMON / SECINF /  sKBL4,       sMXKBL4,     sLAMDA1,     sBUFSZI
      COMMON / SECINF /  sNMONEL,     sINTORB,     sMAXBL3,     sNMBLK3
      COMMON / SECINF /  sKBL3,       sMXKBL3,     sMAXLP3,     sBUFSZL
      COMMON / SECINF /  sMAXBL2,     sMXBL23,     sMXORB,      sMXBLD
      COMMON / SECINF /  sNEXT,       sTOTSPC,     sMINBL4,     sMINBL3
      COMMON / SECINF /  sND4EXT,     sND2EXT,     sND0EXT,     sN4EXT
      COMMON / SECINF /  sN3EXT,      sN2EXT,      sN1EXT,      sN0EXT
      COMMON / SECINF /  sN3XTLP,     sN2XTLP,     sN1XTLP,     sN0XTLP
      COMMON / SECINF /  sTOTVEC,     sCONFYZ,     sPTHYZ,      sNVALWK
      COMMON / SECINF /  sNVALZ,      sNVALY,      sNVALX,      sNVALW
      COMMON / SECINF /  sNFCT,       sNELI
C

      INTEGER             ORBSYM(MXINOR)
      COMMON / INF1   /   ORBSYM
C
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C
      INTEGER             CISTRT,      CODE,        HILEV,   INDI1
      INTEGER             ISYM,        MLP,         MLPPR,       MLPPRZ
      INTEGER             MLPZ,        NI,          NIOT,        RWLPHD
      INTEGER             SEGSUM,      WTLPHD
      REAL*8              SQRT2
      COMMON / OX     /   SQRT2,       WTLPHD,      MLP,         MLPPR
      COMMON / OX     /   MLPPRZ,      MLPZ,        SEGSUM,      CISTRT
      COMMON / OX     /   ISYM,        NI,          CODE,        INDI1
      COMMON / OX     /   RWLPHD,      HILEV,       NIOT
C
      INTEGER             HEADV(MAXBF),             IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             YBTV(MAXBF), YKTV(MAXBF)
      REAL*8              VALV(MAXBF,3)
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XBTV
      COMMON / SPECIAL/   YBTV,        YKTV,        IBF
c  ## integer section
c
      logical skipso, spnorb, spnodd
      integer lxyzir
      common /solxyz/ skipso, spnorb, spnodd, lxyzir(3)
c
c
c  ## integer section
c
      INTEGER  cist(2,2), conft1(*), conft2(*), codep
      integer  D2DIM, dsym
      integer  FIRST
      integer  indsym1(*), indxyz1(*), indsym2(*), indxyz2(*),
     &         index11(*), index21(*), index12(*), index22(*),
     &         IPTH1(2), is, is1, is2,ixx
      integer  j1, js
      integer  k1, ksg
      integer  l1, loop8, lsym
      integer  MLPPRM
      INTEGER  NMBSEG, NI3, NTRYPR
      INTEGER  SCR1DM, SCR2DM, SEGSM(2), STD21, STD22, STD23
c
c  ## real*8 section
c
      REAL*8 CI11(*), CI12(*), CIYZ1(2), CI21(*), CI22(*), CIYZ2(2)
      REAL*8 d2_sym(d2dim), d2_asym(d2dim)
      real*8 norm
      real*8 SCR1_sym(SCR1DM),SCR2_sym(SCR2DM),scr2_asym(SCR2DM),
     &       scr1_asym(SCR1DM),scr1b(nmotx),scr1c(nmotx)
*@ifdef debug
*      real*8 scr1_md(SCR1DM), scr2_md(SCR2DM)
*@endif
      REAL*8 TRAN(*)
      REAL*8 VALUE2
c
c  ##  logical section
c
      LOGICAL             JKLEQ
c
c  ##  external section
c
      EXTERNAL            ATEBYT,dnrm2_wr
      REAL*8    DSQRT,dnrm2_wr
      INTEGER             ATEBYT
c
c-----------------------------------------------------------------------
c
c only formulas generated by int3
      niot = intorb
      sqrt2 = dsqrt(two)
      loop8 = 1

c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2,2)   starting number for the ci vector in segments 1 and 2
c                 bra and ket

      call int3ciuft(.true.,indxyz1,index11,index21,
     .   indxyz2,index12,index22,ipth1,segsm,pthz+pthy,nmbseg)

c
      jkleq=.true.
      do 10 l1 = 1,intorb
c
          hilev = l1
c
             k1=l1
c
                j1=k1
c
c
c # implicitly assumes that density is totally symmetric ...
c
                  is = -1
                  js = orbsym(j1)
                  dsym=symtab(ssym1,ssym2)
                  do ixx = 1,nmbpr(dsym)
                   is1=lmda(dsym,ixx)
                   is2=lmdb(dsym,ixx)
                   if (is1.eq.js) then
                     is=is2
                   elseif (is2.eq.js) then
                     is=is1
                   endif
                  enddo
                  if (is.eq.-1) then
c         write(*,*)'not skipping loops'
                    ni=0
                  else
                    isym = sym21(is)
                    lsym = sym21(js)
c
c # the correct solution is that isym may be choosen such that
c #  isym*jsym = ssym1*ssym2
c     lmdb(i,j),lmda(i,j)   symmetry pairs defined as
c     lmd(i)=lmdb(i,j)*lmda(i,j) , j=1,nmbpr(i)
c     lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1)
c     nmbpr(i)   number of pairs
c

                     ni = nbas(isym)
                  endif
                  ni3 = ni*3
                  std21 = 1
                  std22 = std21 + atebyt(ni)
                  std23 = std22 + atebyt(ni)
                  call wzero(ni3,d2_sym,1)
                  call wzero(ni3,d2_asym,1)
                  ntrypr = 1
   40             continue
c
c     next internal loop
c
c
c     word8 contains code,wtlphd,mlppr,mlp
c
c     wtlphd  weight of the loop head
c     mlppr   weight of the bra part of the loop
c     mlp     weight of the ket part of the loop
c     code   0  new set of indices (l,k,j)
c            1  end of record
c            2  new entry yz-xy,or xy-wy
c            3  val1*(ij/kl)+val2*(jk/il)
c            4  val1*(ij/kl)+val2*(ik/jl)
c            5  val1*(ik/jl)+val2*(jk/il)
c            6  loop 12
c            7  val*(ij/kl)
c            8  val*(ik/jl)
c            9  val*(jk/il)
c
c     ntrypr   1  yz
c              2  xy
c              3  wy
c
c                 word8 = lpbuf(loop8)
c                 call unplp(word8,code,rwlphd,wtlphd,mlppr,mlp)
           code=icodev(loop8)
           rwlphd=headv(loop8)
           wtlphd=xbtv(loop8)
           mlppr= ybtv(loop8)
           mlp  = yktv(loop8)
*@ifdef debug
*      write(6,30320)code,rwlphd,wtlphd,mlppr,mlp,loop8,
*     .              l1,valv(loop8,1),valv(loop8,2)
*@endif
30320 format('int3 code',6i4,'l1=',i3,'val(*)=',2f8.5)

           codep = code + 1
            go to (50,60,70,80,80,80,80,90,90,90) codep
c
          call bummer ('wrong code in onext: code=',code,2)
  60             continue
      call int3ciuft(.false.,indxyz1,index11,index21,
     .   indxyz2,index12,index22,ipth1,segsm,pthz+pthy,nmbseg)

                loop8=1
                  go to 40
c
   70             continue
c
                  ntrypr = ntrypr + 1
                  loop8=loop8+1
                  go to 40
c
   80             continue
c
                  value2 = valv(loop8,2)
                  loop8 = loop8 + 1
c
c     skip loop if first order density matrix only
c
                      go to 100

c
   90             continue
c
                  loop8 = loop8 + 1
c
c     skip loop if first order density matrix only
c
c                 if (.not.jkleq) go to 40
c
  100             continue
c
                  if (ni.ne.0) then
c     write(6,30030)code,wtlphd,rwlphd,mlppr,mlp,ntrypr,ipth1,segsum
                      if (ntrypr.eq.1) then
c
c
                          if (first.gt.0) go to 40

                      else
c
c     skip loop
c
c # all of this stuff should be consistent as long as screening is
c # based on the list of internal paths without explicitly refering
c # to the index vector -> modification of loop3in!
c
                          do 110 ksg = 1,nmbseg
                              mlpprm = mlppr - ipth1(ksg)
                              if ((mlpprm+wtlphd).gt.0 .and.
     +                            mlpprm.lt.segsm(ksg)) go to 120
  110                     continue
                          go to 40

                      end if
c
  120                 call wzero(ni,scr1_sym,1)
                      call wzero(ni,scr1_asym,1)
cgg
c
cgg
                      if (ntrypr.gt.1) then
c
c     yx,yw
c
                          do 130 ksg = 1,nmbseg
                              mlppr = mlppr - ipth1(ksg)
                              if ((mlppr+wtlphd).gt.0 .and.
     +                         mlppr.lt.segsm(ksg)) then
                               segsum = segsm(ksg)
c
c #  cistrt needs to hold indices for bra and ket
c

                                  if (ksg.ne.1) then
                                     if (ntrypr.eq.2 ) then
c
c conft1<> conft2 und muss daher auch uebergeben werden
                                      call wzero(ni,scr1b,1)
                                      call wzero(ni,scr1c,1)

                                      cistrt = cist(2,ksg)
                                      call yx1x(indsym1,conft1,indsym2,
     &                                          conft2,indxyz1,index22,
     &                                          ci22,ciyz1,scr1b,
     &                                          tran)
                                      cistrt = cist(1,ksg)
                                      call yx1x(indsym2,conft2,indsym1,
     &                                          conft1,indxyz2,index21,
     &                                          ci21,ciyz2,scr1c,tran)
c
c                       #   sym contributions
                                      call daxpy_wr(ni,0.5d0,scr1b,1,
     &                                 scr1_sym,1)
                                      call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                 scr1_sym,1)
c                       #   asym contributions
                                      call daxpy_wr(ni,-0.5d0,scr1b,1,
     &                                 scr1_asym,1)
                                      call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                 scr1_asym,1)
*@ifdef debug
*                                      call wzero(ni,scr1_md)
*                                      call daxpy(ni,-0.5d0,scr1b,1,
*     &                                 scr1_md,1)
*                                      call daxpy(ni,0.5d0,scr1c,1,
*     &                                 scr1_md,1)
*                                      norm = dnrm2(ni,scr1_md,1)
*                                      write(*,*)'onext case1',norm
*@endif
c                                     write(*,*) 'onext 1 scr1b'
c                                     call writex(scr1b,ni)
c                                     write(*,*) 'onext 1 scr1c'
c                                     call writex(scr1c,ni)
c
                                     else ! if (ntrypr.eq.2) then
                                      call wzero(ni,scr1b,1)
                                      call wzero(ni,scr1c,1)
                                      cistrt = cist(2,ksg)
                                      call yw1x(indsym1,conft1,indsym2,
     &                                          conft2,indxyz1,index22,
     &                                          ci22,ciyz1,scr1b,tran)
                                      cistrt = cist(1,ksg)
                                      call yw1x(indsym2,conft2,indsym1,
     &                                          conft1,indxyz2,index21,
     &                                          ci21,ciyz2,scr1c,tran)
c  #  sym contributions
                                      call daxpy_wr(ni,0.5d0,scr1b,1,
     &                                 scr1_sym,1)
                                      call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                 scr1_sym,1)
c  #  asym contributions
                                      call daxpy_wr(ni,-0.5d0,scr1b,1,
     &                                 scr1_asym,1)
                                      call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                 scr1_asym,1)
*@ifdef debug
*                                      call wzero(ni,scr1_md)
*                                      call daxpy(ni,-0.5d0,scr1b,1,
*     &                                 scr1_md,1)
*                                      call daxpy(ni,0.5d0,scr1c,1,
*     &                                 scr1_md,1)
*                                      norm = dnrm2(ni,scr1_md,1)
*                                      write(*,*)'onext case2',norm
*@endif
c                                     write(*,*) 'onext 2 scr1b'
c                                     call writex(scr1b,ni)
c                                     write(*,*) 'onext 2 scr1c'
c                                     call writex(scr1c,ni)
c
                                     endif!end of: if (ntrypr.eq.2)then
c
c
                                  else if (ntrypr.eq.2) then
c
                                   call wzero(ni,scr1b,1)
                                   call wzero(ni,scr1c,1)
c
                                   cistrt = cist(2,ksg)
                                   call yx1x(indsym1,conft1,indsym2,
     &                                       conft2,indxyz1,index12,
     &                                       ci12,ciyz1,scr1b,tran)
                                   cistrt = cist(1,ksg)
                                   call yx1x(indsym2,conft2,indsym1,
     &                                       conft1,indxyz2,index11,
     &                                       ci11,ciyz2,scr1c,tran)
c  #  sym contributions
                                   call daxpy_wr(ni,0.5d0,scr1b,1,
     &                                        scr1_sym,1)
                                   call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                        scr1_sym,1)
c  #  asym contributions
                                   call daxpy_wr(ni,-0.5d0,scr1b,1,
     &                                        scr1_asym,1)
                                   call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                        scr1_asym,1)
*@ifdef debug
*                                   call wzero(ni,scr1_md)
*                                   call daxpy(ni,-0.5d0,scr1b,1,
*     &                                        scr1_md,1)
*                                   call daxpy(ni,0.5d0,scr1c,1,
*     &                                        scr1_md,1)
*                                   norm = dnrm2(ni,scr1_md,1)
*                                   write(*,*)'onext case3',norm
*@endif
c
c                   scr1=0.5d0*(scr1+scr1b)
c
c                                  write(*,*) 'onext 3 scr1b'
c                                  call writex(scr1b,ni)
c                                  write(*,*) 'onext 3 scr1c'
c                                  call writex(scr1c,ni)
c
                                  else
c
                                   call wzero(ni,scr1b,1)
                                   call wzero(ni,scr1c,1)
c
                                   cistrt = cist(2,ksg)
                                   call yw1x(indsym1,conft1,indsym2,
     &                                       conft2,indxyz1,index12,
     &                                       ci12,ciyz1,scr1b,tran)
                                   cistrt = cist(1,ksg)
                                   call yw1x(indsym2,conft2,indsym1,
     &                                       conft1,indxyz2,index11,
     &                                       ci11,ciyz2,scr1c,tran)
c  #  sym contributions
                                   call daxpy_wr(ni,0.5d0,scr1b,1,
     &                                        scr1_sym,1)
                                   call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                        scr1_sym,1)
c  #  asym contributions
                                   call daxpy_wr(ni,-0.5d0,scr1b,1,
     &                                        scr1_asym,1)
                                   call daxpy_wr(ni,0.5d0,scr1c,1,
     &                                        scr1_asym,1)
*@ifdef debug
*                                   call wzero(ni,scr1_md)
*                                   call daxpy(ni,-0.5d0,scr1b,1,
*     &                                        scr1_md,1)
*                                   call daxpy(ni,0.5d0,scr1c,1,
*     &                                        scr1_md,1)
*                                   norm = dnrm2(ni,scr1_md,1)
*                                   write(*,*)'onext case4',norm
*@endif
c
c                                  scr1=0.5d0*(scr1+scr1b)
c                                  write(*,*) 'onext 4 scr1b'
c                                  call writex(scr1b,ni)
c                                  write(*,*) 'onext 4 scr1c'
c                                  call writex(scr1c,ni)
c
                                  end if

                               end if
c
                              mlppr = mlppr + ipth1(ksg)
  130                     continue

                      else
c***********************************************************************
c
c     zy
c
c***********************************************************************
c
                          call wzero(ni,scr2_sym,1)
                          call wzero(ni,scr2_asym,1)
cgg

                          CAll zy1x(
     &                     conft1,  indxyz1, ciyz1,    conft2,
     &                     indxyz2, ciyz2,   scr2_sym, scr2_asym,
     &                     jkleq)

c                   write(*,*) 'onext 4 scr2_sym'
c                   call writex(scr2_sym,ni)
c                   write(*,*) 'onext 4 scr2_asym'
c                   call writex(scr2_asym,ni)
                      end if
c
c     multiply by loop value
c
c  #  sym contributions
                      call oned2(
     &                 d2_sym(std23),   scr1_sym,   scr2_sym,
     &                 value2*half,     code,       ni,
     &                 ntrypr)
c  #  asym contributions
                      call oned2(
     &                 d2_asym(std23),  scr1_asym,  scr2_asym,
     &                 value2*half,     code,       ni,
     &                 ntrypr)
c
                  if(spnodd)niot = niot - 1
                  end if


                  go to 40

   50             continue
                  loop8=loop8+1
c
                call onedn1(
     &           d2_sym(std23),    d2_asym(std23),   ni,   isym,
     &           j1,               k1,               l1,   jkleq,
     &           next,             lsym)
   10 continue
c
c     if(iter.lt.1) write(6,30000)
 9000 format (' onex')
c     write(6,30005)l1,k1,j1,lsym,ls,ksym,ks,klsym,kls,jsym,js,isym,is
 9010 format (' 30',20i6)

      end
c
c********************************************************************
c
      subroutine opn1st
c
c  open the required info and listing files
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
c # listing file
      integer      nslist
      equivalence (nslist,nunits(3))
c # drt 1
      integer      fildrt1
      equivalence (fildrt1,nunits(9))
c # drt 2
      integer      fildrt2
      equivalence (fildrt2,nunits(10))
c # civout1
      integer      civout1
      equivalence (civout1,nunits(16))
c # civout2
      integer      civout2
      equivalence (civout2,nunits(17))
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
      integer ios,itype,froot
      integer  ncsf1,ncsf2,refvec1,refvec2
      real*8  eci1,eci2,a4den1,a4den2
      common   /civoutinfo/ ncsf1,ncsf2,refvec1,refvec2,
     .                      eci1,eci2,a4den1,a4den2

c
c

      open( unit = civout1, file = fname(16), status = 'old' ,
     +       form = 'unformatted',iostat=ios )
      if (ios.ne.0)
     . call bummer ('can not open unit ',civout1,2)

      call rdcivout(6,civout1,nroot1,a4den1,itype,froot,refvec1,
     .              eci1,ncsf1,'civout1')
      nroot1=froot
      close (civout1)

      open( unit = civout2, file = fname(17), status = 'old' ,
     +       form = 'unformatted',iostat=ios )
      if (ios.ne.0)
     . call bummer ('can not open unit ',civout2,2)
      call rdcivout(6,civout2,nroot2,a4den2,itype,froot,refvec2,
     .              eci2,ncsf2,'civout2')
      nroot2=froot
      close (civout2)


      return
      end

      subroutine prep1(mxorb)
c
      implicit logical(a-z)
c
      integer mxorb
c*start common block data -eas (8/28/89)
c include(data.common)
      integer maxsym
      parameter (maxsym=8)
      integer symtab,sym12,sym21,nbas,lmda,lmdb,nmbpr,strtbw,strtbx
      integer blst1e,nmsym,mnbas,cnfx,cnfw,scrlen
      common/data/symtab(maxsym,maxsym),sym12(maxsym),sym21(maxsym),
     + nbas(maxsym),lmda(maxsym,maxsym),lmdb(maxsym,maxsym),
     + nmbpr(maxsym),strtbw(maxsym,maxsym),strtbx(maxsym,maxsym),
     + blst1e(maxsym),nmsym,mnbas,cnfx(maxsym),cnfw(maxsym),scrlen
c*end common block data -eas (8/28/89)
c
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer       nunits
      common/cfiles/nunits(nfilmx)
      integer      tape6
      equivalence (tape6,nunits(1))
c
      integer   nciopt
      parameter(nciopt=10)
      integer       uciopt
      common/cciopt/uciopt(nciopt)
      integer      lvlprt
      equivalence (lvlprt,uciopt(1))
c
      INTEGER             BLXT(8),     N2OFFR(8),   N2OFFS(8),   N2ORBT
      INTEGER             NOFFR(8),    NOFFS(8),    NSGES(8), nsdim(8)
C
      integer    dkntin (maxsym*(maxsym+1)/2)
      integer    densoff(maxsym*(maxsym+1)/2)
      integer    dsize
C
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   dsize,densoff,dkntin

c
c     # local:
      integer i
c
      mxorb = nbas(1)
      blxt(1) = 0
      do 10 i = 2, nmsym
         blxt(i) = blxt(i-1) + nbas(i-1)
         mxorb = max( mxorb, nbas(i) )
10    continue
c
      if ( lvlprt .ge. 2 ) write(tape6,30000) mxorb
c
      return
30000 format(' mxorb=',i4)
      end

      subroutine prepdd(index1,indsym1,conft1,index2,indsym2,conft2,
     .           flindx1,flindx2 )
c
c this subroutine sets up the segmentation of the ci vector
c
c  14-dec-90 confx=0 added. -rls/hl
c  last modified ((rewritten) 4/23/90 -eas at osu)
c  segmentation rewritten and simplified. rl dependence
c  nearly eliminated. cannot fully be eliminated until spcci is
c  unused elsewhere in the program.(eas)
c  *** spcci should be in working precision units instead
c      of integer words. -rls ***
c
c
c     # dummy
c
c*start common block data -eas (8/28/89)
c include(data.common)
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine prepdd
C
C     External functions
C
      EXTERNAL            ATEBYT,      FORBYT,      RL
C
      INTEGER             ATEBYT,      FORBYT,      RL
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 100)
      INTEGER             XTYPE
      PARAMETER           (XTYPE = 1)
      INTEGER             WTYPE
      PARAMETER           (WTYPE = 2)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)

C
C     Argument variables
C
      INTEGER             CONFT1(*),    INDEX1(*),    INDSYM1(*)
      INTEGER             CONFT2(*),    INDEX2(*),    INDSYM2(*)
      INTEGER  flindx1,flindx2
C
C     Local variables
C
      CHARACTER*2         SEGTYP
C
      INTEGER             CISTRT,      CONFW,       CONFX
      INTEGER             CONFY,       CONFZ,       ENDIND,      ENDXW
      INTEGER             I
      INTEGER             IPTH,        ISEG,        ISYM
      INTEGER             MAXSIZ,      MXSEG
      INTEGER             NUMCNF, ENDIND2
      INTEGER             NUMCSF,      NUMPTH,      NWALKS
      INTEGER             PTHCSF,      SEGSPC,      SGTYPE,      STCONF
      INTEGER             STRIND,      STRTVC,      STRTXW

      integer   confz2,confy2,confx2,confw2
      integer   strtxw2,endxw2,cistrt2,stconf2,strind2,strtvc2
      integer   index11,index12 , segspc2
      integer   pthcsf2
      integer  numpth2,numcnf2,numcsf2
      logical   val1,val2
      logical  detvalpth
      external detvalpth

C
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
C
C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS
C
C
C     Equivalenced common variables
C
      INTEGER                SHOUT,       TAPE6
C
C
C
C     Common variables
C
      character*60      fname(nfilmx)
C
      COMMON / CFNAME /  fname
C
ctm indxst stores the first internal path of a segment
      INTEGER             CIST(2,MXNSEG), CIVCST(2,MXNSEG)
      INTEGER             INDXST(2,MXNSEG), LENCI(2),  NSEG
      INTEGER             NSEGW, SEGCI(2,MXNSEG)
      INTEGER             SEGEL(2,MXNSEG) ,recci
C
      COMMON / CIVCT  /   LENCI,       NSEG,        NSEGW, recci,SEGEL
      COMMON / CIVCT  /   CIST,        INDXST,      CIVCST,      SEGCI
C
C
C     Common variables
C
      INTEGER             ICNFST(2,MXNSEG),  ISGCNF(2,MXNSEG)
C
      COMMON / CNFSPC /   ICNFST,      ISGCNF
C
C
C     Common variables
C
      INTEGER             BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER             CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER             LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER             NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER             NMSYM,       SCRLEN
      INTEGER             STRTBW(MAXSYM,MAXSYM)
      INTEGER             STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER             SYM21(MAXSYM)
      INTEGER             SYMTAB(MAXSYM,MAXSYM)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      TOTVEC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   TOTVEC,      CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI

C
C     Common variables
C
      INTEGER            sBUFSZI,     sBUFSZL,     sCONFYZ,     sDIMCI
      INTEGER            sINTORB,     sKBL3,       sKBL4,       sLAMDA1
      INTEGER            sMAXBL2,     sMAXBL3,     sMAXBL4,     sMAXLP3
      INTEGER            sMINBL3,     sMINBL4,     sMXBL23,     sMXBLD
      INTEGER            sMXKBL3,     sMXKBL4,     sMXORB,      sN0EXT
      INTEGER            sN0XTLP,     sN1EXT,      sN1XTLP,     sN2EXT
      INTEGER            sN2XTLP,     sN3EXT,      sN3XTLP,     sN4EXT
      INTEGER            sND0EXT,     sND2EXT,     sND4EXT,     sNELI
      INTEGER            sNEXT,       sNFCT,       sNINTPT,     sNMBLK3
      INTEGER            sNMBLK4,     sNMONEL,     sNVALW,      sNVALWK
      INTEGER            sNVALX,      sNVALY,      sNVALZ,      sPTHW
      INTEGER            sPTHX,       sPTHY,       sPTHYZ,      sPTHZ
      INTEGER            sTOTSPC,     sTOTVEC
C
      COMMON / SECINF /  sPTHZ,       sPTHY,       sPTHX,       sPTHW
      COMMON / SECINF /  sNINTPT,     sDIMCI,      sMAXBL4,     sNMBLK4
      COMMON / SECINF /  sKBL4,       sMXKBL4,     sLAMDA1,     sBUFSZI
      COMMON / SECINF /  sNMONEL,     sINTORB,     sMAXBL3,     sNMBLK3
      COMMON / SECINF /  sKBL3,       sMXKBL3,     sMAXLP3,     sBUFSZL
      COMMON / SECINF /  sMAXBL2,     sMXBL23,     sMXORB,      sMXBLD
      COMMON / SECINF /  sNEXT,       sTOTSPC,     sMINBL4,     sMINBL3
      COMMON / SECINF /  sND4EXT,     sND2EXT,     sND0EXT,     sN4EXT
      COMMON / SECINF /  sN3EXT,      sN2EXT,      sN1EXT,      sN0EXT
      COMMON / SECINF /  sN3XTLP,     sN2XTLP,     sN1XTLP,     sN0XTLP
      COMMON / SECINF /  sTOTVEC,     sCONFYZ,     sPTHYZ,      sNVALWK
      COMMON / SECINF /  sNVALZ,      sNVALY,      sNVALX,      sNVALW
      COMMON / SECINF /  sNFCT,       sNELI

C
C     Common variables
C
      INTEGER             NREC35,      NREC36, ssym1,ssym2
C
      COMMON / INF4   /   NREC35,      NREC36, ssym1,ssym2
C

C
C    END OF VARIABLE DECLARATIONS FOR subroutine prepdd


      equivalence (tape6,nunits(1))
      equivalence (shout,nunits(3))
c

c
c
c     nseg number of segments of the ci vector
c        order of the segments z and y paths, w , x paths
c        the x paths start with a new segment
c     nsegw number of the segment at which the x paths start
c     segel(nseg) number of internal paths for each segment
c     cist(nseg) starting number of the first ci vector element of
c                each segment
c     indxst(nseg) first internal walk at which a given segment of
c                  the index vector starts
c     civcst(nseg) number of the record at which a given segment of
c                  the ci vector starts
c     segci(nseg) number of ci elements for each segment
c
c     calculate number of external paths through x and w
c
      endind = 0
      mxseg  = mxnseg
*@ifdef debug
*       write(tape6,*) 'inf4: nrec35=',nrec35,' nrec36=',nrec36
*@endif
       write(tape6,*) 'opening indexfiles'
       call openda(nunits(14),fname(14),nrec35, 'keep','random')
       call openda(nunits(15),fname(15),nrec35,'keep','random')
c
      if ( lvlprt .ge. 6 ) then
         write (tape6,6000) 'index1=',(index1(i),i=1,nintpt)
         write (tape6,6000) 'indsym1=',(indsym1(i),i=1,nintpt)
         write (tape6,6000) 'index2=',(index2(i),i=1,nintpt)
         write (tape6,6000) 'indsym2=',(indsym2(i),i=1,nintpt)
      endif
6000  format(' from prepdd: ',a,/(20i6))
c
c
c # get y and z walk information for bra vector
c
      call getyzinf(index1,indsym1,conft1,confz,confy,pthz,pthy)

c
c # get y and z walk information for ket vector
c
      call getyzinf(index2,indsym2,conft2,confz2,confy2,spthz,spthy)

      confyz = confz + confy
      sconfyz = confz2 + confy2
c
c     # setup yz segment
c
      nseg = 1
      icnfst(1,nseg) = 0
      isgcnf(1,nseg) = nvalz + nvaly
      segel (1,nseg) = pthz + pthy
      cist  (1,nseg) = 0
      civcst(1,nseg) = 1
      segci (1,nseg) = confyz
      indxst(1,nseg) = 1

      icnfst(2,nseg) = 0
      isgcnf(2,nseg) = snvalz + snvaly
      segel (2,nseg) = spthz + spthy
      cist  (2,nseg) = 0
      civcst(2,nseg) = 1
      segci (2,nseg) = sconfyz
      indxst(2,nseg) = 1


       strind =1
       strind2 =1
c
c     # write out zy portion of bra index vector
c
      call diracc(
     & flindx1,        2,      index1(1), segel(1,nseg),
     & nrec35, strind,  endind )

c     # write out zy portion of ket index vector
c
      call diracc(
     & flindx2,        2,      index2(1), segel(2,nseg),
     & nrec35, strind2,     endind2 )

c
      strind = endind+1
      strtvc = 1 + ((confyz-1)/nrec35 + 1)
      strind2 = endind2 +1
      strtvc2 = 1 + ((sconfyz-1)/nrec35 + 1)
c
c     # pre-walk x portion of index vector to count x csfs
c     # this is used later to start the x segments in the right place
c
c  # bra
      confx = 0
      do 130 ipth = (pthy + pthz + 1) , (pthz + pthy + pthx)
         if ( index1(ipth) .ge. 0 ) then
            isym  = sym21( indsym1(index1(ipth)) )
            confx = confx + cnfx(isym)
         endif
130   continue

c  # ket
      confx2 = 0
      do 131 ipth = (spthy + spthz + 1) , (spthz + spthy + spthx)
         if ( index2(ipth) .ge. 0 ) then
            isym  = sym21( indsym2(index2(ipth)) )
            confx2 = confx2 + cnfx(isym)
         endif
131   continue

c
c     # initialize segmentation procedure
c
*@ifdef reference
*C
*C       # the following two lines should be modeled for machines with
*C       # hard vector length limitations
*C       maxsiz = ( frespc - 2 * atebyt(confyz)) / 2
*C       maxsiz = min( maximum vector length, maxsiz)
*@else
c
      maxsiz = ( totvec - atebyt(confyz) - atebyt(sconfyz) ) / 2
c
*@endif
c
c     # account for yz segment and diagd call memory usage
c
      totvec = totvec - atebyt(confyz) - atebyt(sconfyz)
c
c     # this is the point to begin segmentation of either x or w
c
c     # w segments done first time through
c
      confw = 0
      confw2 = 0
      sgtype = wtype
c
1000  continue
c
      if ( sgtype .eq. wtype ) then
c
c        # initialize for w walks
c
         strtxw = pthz + pthy + pthx + 1
         endxw  = strtxw + pthw - 1
         cistrt = confyz + confx
         stconf = nvalz + nvaly + nvalx

         strtxw2 = spthz + spthy + spthx + 1
         endxw2  = strtxw2 + spthw - 1
         cistrt2 = sconfyz + confx2
         stconf2 = snvalz + snvaly + snvalx
c
      else
c
c        # initialize for x walks
c
         strtxw = pthz + pthy + 1
         endxw  = strtxw + pthx - 1
         cistrt = confyz
         stconf = nvalz + nvaly

         strtxw2 = spthz + spthy + 1
         endxw2  = strtxw2 + spthx - 1
         cistrt2 = sconfyz
         stconf2 = snvalz + snvaly
c
      endif
c
c     # initialize segment type independent counts
c
      index11 = strtxw
      numpth = 0
      numcnf = 0
      numcsf = 0

      index12 = strtxw2
      numpth2 = 0
      numcnf2 = 0
      numcsf2 = 0

c
      do 140 ipth=strtxw,endxw
c

       val1 = detvalpth(index1,conft1,indsym1,numpth,confw,ipth,
     .                   cistrt,segspc,numcsf,sgtype,pthcsf)
       val2 = detvalpth(index2,conft2,indsym2,numpth2,confw2,ipth,
     .                   cistrt2,segspc2,numcsf2,sgtype,pthcsf2)

        if (val1.or.val2) then

               if (segspc+segspc2.gt.maxsiz) then
c                 write(*,*) 'new segment: segspc,segspc2,maxsiz',
c    .                                     segspc,segspc2,maxsiz
c
c                 # assign values for the segment up to ipth-1
c
                  if ( nseg .eq. mxseg ) then
                     write (tape6,7000) mxseg,nseg
                     call bummer('prepdd: mxseg=', mxseg, faterr )
                  endif

c
c  # bra
c
                  nseg = nseg + 1
                  segel(1,nseg) = numcsf
                  cist (1,nseg) = cistrt
                  icnfst(1,nseg)= stconf
                  isgcnf(1,nseg)= numcnf
                  indxst(1,nseg)= strind
                  civcst(1,nseg)= strtvc
                  segci (1,nseg)= numcsf
c
c                 # write out index vector for this segment
c
                  call diracc(
     &             flindx1,   2,   index1(index11), numpth,
     &             nrec35, strind, endind  )

c
c # ket
c
                  segel(2,nseg) = numcsf2
                  cist (2,nseg) = cistrt2
                  icnfst(2,nseg)= stconf2
                  isgcnf(2,nseg)= numcnf2
                  indxst(2,nseg)= strind2
                  civcst(2,nseg)= strtvc2
                  segci (2,nseg)= numcsf2
c
c                 # write out index vector for this segment
c
                  call diracc(
     &             flindx2,   2,   index2(index12), numpth2,
     &             nrec35, strind2, endind2 )



c
c                 # set-up values for the potential next segment
c                 # beginning with ipth
c
                  strind = endind +1
                  strtvc = strtvc + ((numcsf-1)/nrec35 + 1)
                  index11 = index11 + numpth
                  numpth = 1
                  numcsf = pthcsf
                  numcnf = 1
                  cistrt = cistrt + numcsf
                  stconf = stconf + numcnf

                  strind2 = endind2+1
                  strtvc2 = strtvc2 + ((numcsf2-1)/nrec35 + 1)
                  index12 = index12 + numpth2
                  numpth2 = 1
                  numcsf2 = pthcsf2
                  numcnf2 = 1
                  cistrt2 = cistrt2 + numcsf2
                  stconf2 = stconf2 + numcnf2
               else
c
c                 # add this path to the segment
c
                  numpth = numpth + 1
                  if (val1) numcnf = numcnf + 1
                  numcsf = numcsf + pthcsf

                  numpth2 = numpth2 + 1
                  if (val2) numcnf2 = numcnf2 + 1
                  numcsf2 = numcsf2 + pthcsf2

               endif
            else
            numpth=numpth+1
            numpth2=numpth2+1
            endif
140   continue
c
c
c  numpth1 = numpth2 !
c
      if ( numpth .gt. 0 ) then
         if ( nseg .eq. mxseg ) then
            write (tape6,7000) mxseg,nseg
            call bummer('prepdd: (nseg-mxseg)=', (nseg-mxseg), faterr )
         endif
         if (numpth.ne.numpth2) then
            write(tape6,*) 'numpth=',numpth,' numpth2=',numpth2
            call bummer('prepdd: (numpth-numpth2)=',(numpth-numpth2),
     .                                                 faterr)
         endif
         nseg = nseg + 1

c # bra

         segel(1,nseg) = numpth
         cist (1,nseg) = cistrt
         icnfst(1,nseg)= stconf
         isgcnf(1,nseg)= numcnf
         indxst(1,nseg)= strind
         civcst(1,nseg)= strtvc
         segci(1,nseg) = numcsf
c
c        # write out index vector for this segment
c
         call diracc(
     &    flindx1,   2,  index1(index11), numpth,
     &    nrec35, strind, endind  )
         strind = endind+1
         strtvc = strtvc + ((numcsf-1)/nrec35 + 1)

c # ket

         segel(2,nseg) = numpth2
         cist (2,nseg) = cistrt2
         icnfst(2,nseg)= stconf2
         isgcnf(2,nseg)= numcnf2
         indxst(2,nseg)= strind2
         civcst(2,nseg)= strtvc2
         segci(2,nseg) = numcsf2
c
c        # write out index vector for this segment
c
         call diracc(
     &    flindx2,   2,  index2(index12), numpth2,
     &    nrec35, strind2, endind2  )
         strind2 = endind2+1
         strtvc2 = strtvc2 + ((numcsf2-1)/nrec35 + 1)
      endif

      if ( sgtype .eq. wtype ) then
         sgtype = xtype
         nsegw = nseg + 1
c
c        # return to do the x segment
c
         goto 1000
      endif

c
c     # write a segmentation summary in tabular form
c
      write(tape6,6101) 'segmentation summary bra'
      write(tape6,6102)
      write(tape6,6105) 'seg.','no. of','no. of','starting',
     + 'starting','starting','internal','starting'
      write(tape6,6105) 'no.','internal','ci','csf',
     + 'index','ci','walks','walk'
      write(tape6,6105) ' ','paths','elements','number',
     + 'record','record','/seg.','number'
      write(tape6,6102)
      do 160 iseg = 1, nseg
         if ( iseg .eq. 1) then
            segtyp = 'yz'
         elseif ( iseg .ge. nsegw ) then
            segtyp = 'x '
         else
            segtyp = 'w '
         endif
         write (tape6,6107) segtyp,iseg,segel(1,iseg),segci(1,iseg),
     +    cist(1,iseg),indxst(1,iseg),civcst(1,iseg),isgcnf(1,iseg),
     .    icnfst(1,iseg)
         write(tape6,6102)
160   continue

      write(tape6,6101) 'segmentation summary ket'
      write(tape6,6102)
      write(tape6,6105) 'seg.','no. of','no. of','starting',
     + 'starting','starting','internal','starting'
      write(tape6,6105) 'no.','internal','ci','csf',
     + 'index','ci','walks','walk'
      write(tape6,6105) ' ','paths','elements','number',
     + 'record','record','/seg.','number'
      write(tape6,6102)
      do 161 iseg = 1, nseg
         if ( iseg .eq. 1) then
            segtyp = 'yz'
         elseif ( iseg .ge. nsegw ) then
            segtyp = 'x '
         else
            segtyp = 'w '
         endif
         write (tape6,6107) segtyp,iseg,segel(2,iseg),segci(2,iseg),
     +    cist(2,iseg),indxst(2,iseg),civcst(2,iseg),isgcnf(2,iseg),
     .    icnfst(2,iseg)
         write(tape6,6102)
161   continue
c
c =====================================================================
c     # validate number of determined walks
c
c # bra
      nwalks = 0
      do 150 i = 1, nseg
         nwalks = nwalks + isgcnf(1,i)
150   continue
      if ( nwalks .ne. nvalwk ) then
         write(tape6,7001) nwalks,nvalwk
       call bummer
     .  ('prepdd: bra (nwalks-nvalwk)=',(nwalks-nvalwk), faterr)
      endif
c
c # ket
      nwalks = 0
      do 151 i = 1, nseg
         nwalks = nwalks + isgcnf(2,i)
151   continue
      if ( nwalks .ne. snvalwk ) then
         write(tape6,7001) nwalks,snvalwk
       call bummer
     .    ('prepdd: ket (nwalks-snvalwk)=',(nwalks-snvalwk), faterr)
      endif


6101  format(///t20,a)
6102  format(1x,70('-'))
6105  format(1x,a4,t8,7(a8,'|'))
6107  format(1x,a2,i2,t8,7(i8,'|'))
c
c     # end summary
c
      dimci = confz + confy + confx + confw
      sdimci = confz2 + confy2 + confx2 + confw2
      lenci(1) = dimci
      lenci(2) = sdimci
c
      write(tape6,6201) maxsiz*2,confyz
6201  format(' space available for w and x part of the ci vector',
     + i10/' number of z and y configurations',i8/)
      if(lvlprt.gt.5) then
         write(tape6,6202)(index1(i),i=1,nintpt)
         write(tape6,6204)(index2(i),i=1,nintpt)
      endif
6202  format(' index1 vector'/(1x,20i6))
6204  format(' index2 vector'/(1x,20i6))
      write(tape6,6203) 1,dimci
      write(shout,6203) 1,dimci
      write(tape6,6203) 2,sdimci
      write(shout,6203) 2,sdimci
6203  format(/' length of the ci-vector #',i2,'  ->>>',i10/)
c
*@ifdef debug
*C     write (tape6,6301)'conft',(conft(i),i=1,nvalwk)
*C     write (tape6,6301)'index',(index(i),i=1,nvalwk)
*C     do 180 iseg=1,nseg
*C     call diracc(flindx,1,index,segel(iseg),dalen(flindx),
*C    +            indxst(iseg),fin)
*C     write (tape6,6302)'index segment',iseg,(index(i),i=1,segel(iseg))
*C180  continue
*C6301 format(1x,a,/(20i4))
*C6302 format(1x,a,i3,/(20i4))
*@endif
c
      return
7000  format(' program-defined mxseg (',i3,') will be exceeded',
     + 'following nseg = ',i3)
7001  format(' error in prepdd :  nwalks',i8,' nvalwk',i8,' stop')
      end
c
c

      subroutine rddrtnx(
     & nmot,   niot,    nsym,    nrowmx,
     & nrow,   nwalk,   lenbuf,  nzwalk,
     & nvwxy,  map,     nmpsy,   nexo,
     & modrt,  syml,    nj,      njskp,
     & a,      b,       l,       y,
     & xbar,   limvec,  ref,     csym,
     & neli,   labels,  buf,     ndrt,ssym,nvalz)

c
c  read some of the drt arrays from the drt file.
c
c  title and dimension information have already been read.
c  the file is assumed to be positioned correctly for the
c  next records.
c  vectors are buffered in records of length lenbuf.
c
c  some of these arrays have already been read from the ft files,
c  and are skipped over here.
c
c  10-dec-90 version=3 drt changes. -rls
c  08-feb-90 neli added. -eas/rls
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine rddrtn
C
C     Parameter variables
C
      INTEGER             LROWMX
      PARAMETER           (LROWMX = 1023)
      INTEGER             NSYMAX
      PARAMETER           (NSYMAX = 8)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*4         LABELS(NSYMAX)
      integer   nrow,lenbuf,nrowmx,nsym,niot,nmot
C
      INTEGER             A(NROW),     B(NROW),     BUF(LENBUF) 
      INTEGER             CSYM(*)
      INTEGER             L(0:3,LROWMX,3)
      INTEGER             LIMVEC(*),   MAP(NMOT),   MODRT(NIOT), NDRT
      INTEGER             NELI,        NEXO(NSYM)
      INTEGER             NJ(0:NIOT),  NJSKP(0:NIOT)
      INTEGER             NMPSY(NSYM)
      INTEGER             NVWXY,       NWALK,       NZWALK
      INTEGER             REF(NZWALK), SYML(NIOT),  XBAR(LROWMX,3)
      INTEGER             Y(0:3,LROWMX,3), ssym,nvalz

C
C
C     Local variables
C
      INTEGER             I,           NC,          NLEVEL
      INTEGER             NX(0:1) , limvecinfo(3)
      integer             numv1,j, ncsft,icd(10)
      integer, allocatable :: csym_local(:)
C
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:50),         IDUM1(LROWMX)
      INTEGER             IDUM2(LROWMX,3),          MU(50)
C
      COMMON / CDRT  /   IDUM1,       IDUM2,       MU,          ARCSYM
C
      INTEGER             MULT(8,8)
      COMMON / CSYM   /   MULT

C    END OF VARIABLE DECLARATIONS FOR subroutine rddrtn


c just playing around
c     # bummer error types.
      nlevel = niot + 1
c
c
c     # slabel(*)...
       call rddbl( ndrt, lenbuf, buf, nsym )
c      do 10 i = 1,nsym
c        write( labels(i), '(a4)' ) buf(i)
c  10  continue
      call wtlabel(labels,nsym,'(a4)',buf)
c      write(6,*) 'label=',(labels(i),i=1,nsym)
c
c
c     # map(*)...
c
      call rddbl( ndrt, lenbuf, map, nmot )
      write(6,6300)'map(*)=',(map(i),i=1,nmot)
c
c     # mu(*)...
c
ctm: transci.x won't produce correct densities
ctm: with mu>0, so skip reading it 
      call rddbl( ndrt, lenbuf, buf, -niot )
c     write(6,6100)'mu(*)=',(buf(i),i=1,niot)
c     do 20 i=1,niot
c         mu(i)=buf(i)
c 20  continue
c
c     # nmpsy(*)...
c
      call rddbl( ndrt, lenbuf, nmpsy, nsym )
c
c     # nexo(*)...
      call rddbl( ndrt, lenbuf, nexo, nsym )
c
c     # nviwsm(*)...
      call rddbl( ndrt, lenbuf, buf,-4*nsym )
c
c     # ncsfsm(*)...
      call rddbl( ndrt, lenbuf, buf,-4*nsym )
c
c     # modrt(*)...
      call rddbl( ndrt, lenbuf, modrt, niot )
c     write(6,6100)'modrt(*)=',(modrt(i),i=1,niot)
c
c
c     # syml(*)...
c
      call rddbl( ndrt, lenbuf, syml, niot )

      do 25 i = 1, niot
         arcsym(0,i-1) = 1
         arcsym(1,i-1) = syml(i)
         arcsym(2,i-1) = syml(i)
         arcsym(3,i-1) = 1
25    continue

c     write( 6, * )'syml(*)=', (syml(i),i=1,niot)
c
c
c     # nj(*)...
c
      call rddbl( ndrt, lenbuf, nj, nlevel )
c
c     # njskp(*)...
c
      call rddbl( ndrt, lenbuf, njskp, nlevel )
c
c     # a(*)...
c
      call rddbl( ndrt, lenbuf, a, nrow )
c
c     # neli = number of internal electrons = 2*a(nrow)+b(nrow)
c
      neli = 2*a(nrow)
c
c     # b(*)...
c
      call rddbl( ndrt, lenbuf, b, nrow )
      neli = neli + b(nrow)
c
c     # l(*,*,*)...
c
      do 30 i = 1, 3
         call rddbl( ndrt, lenbuf, l(0,1,i), 4*nrow )
30    continue
c
c     # y(*,*,*)...
      do 40 i = 1, 3
         call rddbl( ndrt, lenbuf, y(0,1,i), 4*nrow )
40    continue
c
c     # xbar(*,*)...
c
      do 50 i = 1, 3
         call rddbl( ndrt, lenbuf, xbar(1,i) , nrow )
50    continue
c
c
c     # limvec(*)
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed limvec)
      call rddbl( ndrt, lenbuf, limvec, limvecinfo(2) )
      call uncmprlimvec(limvec,nwalk,limvecinfo(1),limvecinfo(2),
     .  limvecinfo(3))

       call skpx01( nwalk, limvec, 0, numv1 )
       write(0,*) 'numv1=',numv1

       icd(1)=1              ! row
       icd(2)=icd(1)+niot+1  ! step
       icd(3)=icd(2)+niot+1  ! wsym
       icd(4)=icd(3)+niot+1  ! ytot
       icd(5)=icd(4)+niot+1  ! nviwsm
       icd(6)=icd(5)+nsym*4  ! ncsfsm
       icd(7)=icd(6)+nsym*4
       if (icd(7).gt.lenbuf) call bummer('insufficient memory',icd(7),2)

        call  symwlk_noso( 
     & niot,   nsym,   nrow,   nrowmx,
     & nwalk,  l,  y, buf(icd(1)),
     & buf(icd(2)),csym,buf(icd(3)),arcsym,  
     & buf(icd(4)),   xbar,   ssym,
     & mult,limvec, ncsft,
     & buf(icd(5)),buf(icd(6)), b, numv1 )
!     # in the diagonalization program.
!     # input: limvec(*)=  0  0  3  2  1  0  0  2  1  0
!     # output:limvec(*)=  1  2 -3 -2 -1  3  4 -2 -1  5
!
        j = 0
        DO i = 1, nwalk
          IF (limvec(i)==0) THEN
            j = j + 1
            limvec(i) = j
          ELSE
            limvec(i) = -limvec(i)
          END IF
        END DO

c
c     # csym(*)  for valid y,x,w walks...
c
*@ifdef debugcsym
*c     # csym(*)  for valid y,x,w walks...
*        allocate (csym_local(nvwxy))
*        CALL rddbl(ndrt,lenbuf,csym_local,nvwxy)
*        write(0,*) 'checking for consistent csym(*)  ...'
*        do i=1,nvwxy
*          if (csym_local(i).ne.csym(nvalz+i)) then
*            write(0,*) i,csym_local(i),csym(i+nvalz)
*          endif
*        enddo
*        write(0,*) '... done'
*        deallocate(csym_local)
*@endif

c     # 0/1 ref(*)...
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed ref)
      call rddbl( ndrt, lenbuf, ref, limvecinfo(2) )
      call uncmprlimvec(ref,nzwalk,limvecinfo(1),limvecinfo(2),
     .  limvecinfo(3))
c
 6100 format(1x,a,(t12,20i3))
 6200 format(1x,a,(t12,20i4))
 6300 format(1x,a,(t12,20i5))
      return
      end


      subroutine rdhdrtn(
     & title,  nmot,   niot,   nfct,
     & nfvt,   nrow,   nsym,   ssym,
     & xbar,   nvalw,  ncsft,  lenbuf,
     & nwalk,  niomx,  nmomx,  nrowmx,
     & buf,    nlist,  ndrt, spnorb,spnodd,lxyzir )
c
c  read the header info from the drt file.
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine rdhdrtn
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*80        TITLE
C
      INTEGER             BUF(*),      LENBUF,      NCSFT,       NDRT
      INTEGER             NFCT,        NFVT,        NIOMX,       NIOT
      INTEGER             NLIST,       NMOMX,       NMOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVALW(4),    NWALK
      INTEGER             SSYM,        XBAR(4)
C
C     Local variables
C
      INTEGER             IERR,        NHDINT,      NVALWT,      VRSION
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif

C
C    END OF VARIABLE DECLARATIONS FOR subroutine rdhdrtn


c     # bummer error types.
c     # title card...
      rewind ndrt
      ierr=0
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
*@ifdef spinorbit
      read(ndrt,*,iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if(spnorb) call bummer('rdhead: no SO-CI calcs',0,faterr)
*@endif


c     write(nlist,6010)title
c
c     # number of integer drt parameters, buffer length, and version.
c
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 6 ) then
         call bummer
     . ('DRT version 6 required (index vector compression, no csym(*))',
     .      vrsion,2)
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning: rdhdrt: drt version .ne. 3 *** vrsion=',vrsion
*      endif
*@endif
c
c     # header info...
c
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      nmot     = buf(1)
      niot     = buf(2)
      nfct     = buf(3)
      nfvt     = buf(4)
      nrow     = buf(5)
      nsym     = buf(6)
      ssym     = buf(7)
      xbar(1)  = buf(8)
      xbar(2)  = buf(9)
      xbar(3)  = buf(10)
      xbar(4)  = buf(11)
      nvalw(1) = buf(12)
      nvalw(2) = buf(13)
      nvalw(3) = buf(14)
      nvalw(4) = buf(15)
      ncsft    = buf(16)
c
      nwalk  = xbar(1)  + xbar(2)  + xbar(3)  + xbar(4)
      nvalwt = nvalw(1) + nvalw(2) + nvalw(3) + nvalw(4)
c
      write(nlist,6020)nmot,niot,nfct,nfvt,nrow,nsym,ssym,lenbuf
      write(nlist,6100)'nwalk,xbar',nwalk,xbar
      write(nlist,6100)'nvalwt,nvalw',nvalwt,nvalw
      write(nlist,6100)'ncsft',ncsft
c
      if(niot.gt.niomx)then
         write(nlist,6900)'niot,niomx',niot,niomx
         call bummer('rdhead: niomx=',niomx,faterr)
      endif
      if(nmot.gt.nmomx)then
         write(nlist,6900)'nmot,nmomx',nmot,nmomx
         call bummer('rdhead: nmomx=',nmomx,faterr)
      endif
      if(nrow.gt.nrowmx)then
         write(nlist,6900)'nrow,nrowmx',nrow,nrowmx
         call bummer('rdhead: nrowmx=',nrowmx,faterr)
      endif
ctm     if(nwalk.gt.nwlkmx)then
ctm         write(nlist,6900)'nwalk,nwlkmx',nwalk,nwlkmx
ctm         call bummer('rdhead: nwlkmx=',nwlkmx,faterr)
ctm      endif
c
      return
c
6010  format(/' drt header information:'/1x,a)
6020  format(
     & ' nmot  =',i6,' niot  =',i6,' nfct  =',i6,' nfvt  =',i6/
     & ' nrow  =',i6,' nsym  =',i6,' ssym  =',i6,' lenbuf=',i6)
6100  format(1x,a,':',t15,5i9)
6900  format(/' error: ',a,3i10)
      end


      subroutine smprepcid(nmsym)
c*****
c     computation of symmetry tables
c*****
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine smprepcid
C
C     Parameter variables
C
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
C
C     Argument variables
C
      INTEGER             NMSYM
C
C     Local variables
C
      INTEGER             I,           ICNT1,       ICNT2,       IJS
      INTEGER             IJSYM,       IS,          ISYM,        IX
      INTEGER             J,           JS,          JSYM,        NI
      INTEGER             NMBJ, isave
C
C
C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS
C
C
C     Equivalenced common variables
C
      INTEGER             TAPE6
C
C
C     Common variables
C
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      DIMCI
      INTEGER             FUEL1(25),   INTORB,      KBL3,        KBL4
      INTEGER             LAMDA1,      MAXBL2,      MAXBL3,      MAXBL4
      INTEGER             MAXLP3,      MXBL23,      MXBLD,       MXKBL3
      INTEGER             MXKBL4,      MXORB,       NEXT,        NINTPT
      INTEGER             NMBLK3,      NMBLK4,      NMONEL,      PTHW
      INTEGER             PTHX,        PTHY,        PTHZ
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        FUEL1
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
C    END OF VARIABLE DECLARATIONS FOR subroutine smprepcid


      equivalence (nunits(1),tape6)
c
c
      do 10 i=1,nmsym
          ix=sym12(i)
   10 sym21(ix)=i
      if(lvlprt.ge.3)
     +write(tape6,30030)(nbas(i),i=1,nmsym)
      if(lvlprt.ge.3)
     +write(tape6,30030)(sym12(i),i=1,nmsym)
      if(lvlprt.ge.3)
     +write(tape6,30030)(sym21(i),i=1,nmsym)
      lamda1=0
      do 15 i=1,nmsym
          if(sym12(i).ne.1)go to 15
          lamda1=i
          go to 17
   15 continue
   17 continue
      if(lvlprt.ge.3)
     +write(tape6,30050)lamda1
30050 format(' lamda1',i4)
      do 20 i=1,nmsym
          nmbpr(i)=0
          do 20 j=1,nmsym
              lmda(i,j)=0
              lmdb(i,j)=0
              strtbx(i,j)=0
              strtbw(i,j)=0
   20 continue
c
c     lmdb(i,j),lmda(i,j)   symmetry pairs defined as
c     lmd(i)=lmdb(i,j)*lmda(i,j) , j=1,nmbpr(i)
c     lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1)
c     nmbpr(i)   number of pairs
c
      do 200 i=1,nmsym
          is=sym12(i)
          nmbj=0
          do 210 j=1,nmsym
              if(nbas(j).eq.0)go to 210
              js=sym12(j)
              ijs=symtab(is,js)
              ijsym=sym21(ijs)
              if(nbas(ijsym).eq.0)go to 210
              if(ijsym.gt.j)go to 210
              nmbj=nmbj+1
              lmdb(i,nmbj)=j
              lmda(i,nmbj)=ijsym
  210     continue

  200 continue
          do 201 i=1,nmsym
          do 211 j=1,nmsym
             if (lmdb(i,j).gt.0) then
                if (nbas(lmdb(i,j))*nbas(lmda(i,j)).gt.0) then
                nmbpr(i)=nmbpr(i)+1
             endif
             endif
  211     continue
  201     continue
      if(lvlprt.ge.3)
     +write(tape6,30010)
30010 format(' lmda,lmdb')
      do 40000 i=1,nmsym
          if(lvlprt.ge.3) write(tape6,30000)(lmdb(i,j),lmda(i,j),j=1,
     +    nmsym)
30000 format(1x,8(2i4,2x))
40000 continue
      if(lvlprt.ge.3)
     +write(tape6,30030)(nmbpr(i),i=1,nmsym)
c
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c
      do 300 i=1,nmsym
          nmbj=nmbpr(i)
          if(nmbj.eq.0)go to 300
          icnt1=0
          icnt2=0
          do 310 j=1,nmbj
              jsym=lmdb(i,j)
              isym=lmda(i,j)
              if(isym.eq.jsym)go to 320
              strtbw(jsym,i)=icnt1
              strtbx(jsym,i)=icnt1
              icnt1=icnt1+nbas(isym)*nbas(jsym)
              go to 310
  320         continue
              ni=nbas(isym)
              strtbw(isym,i)=icnt1
              strtbx(isym,i)=icnt2
              icnt1=icnt1+ni*(ni+1)/2
              icnt2=icnt2+(ni-1)*ni/2
  310     continue
  300 continue
          do  i=1,nmsym
              nmbpr(i)=0
          do  j=1,nmsym
             if (lmdb(i,j).gt.0) then
                nmbpr(i)=nmbpr(i)+1
             endif
          enddo
          enddo
      if(lvlprt.ge.3)
     +write(tape6,30020)
30020 format(' strtbx')
      do 40010 i=1,nmsym
40010 if(lvlprt.ge.3)
     +write(tape6,30030)(strtbx(i,j),j=1,nmsym)
30030 format(1x,20i6)
      if(lvlprt.ge.3)
     +write(tape6,30040)
30040 format(' strtbw')
      do 40020 i=1,nmsym
40020 if(lvlprt.ge.3)
     +write(tape6,30030)(strtbw(i,j),j=1,nmsym)
      return
      end


      subroutine tapind(ssym, nmot, nmsym, core, fildrt,
     . fndrt, pthz,pthy,pthx,pthw,nintpt,intorb,nvalwk,nvalz,
     . nvaly,nvalw, nvalx,nfct,neli,totspc)

      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine tapind
C
C     External functions
C
      EXTERNAL            FORBYT
C
      INTEGER             FORBYT
C
C     Parameter variables
C
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
      INTEGER             NSYMAX
      PARAMETER           (NSYMAX = 8)
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             NWLKMX
      PARAMETER           (NWLKMX = 2**20-1)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 2**10-1)
      INTEGER             LROW
      PARAMETER           (LROW = 1023 )
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             READC
      PARAMETER           (READC = 1)
      integer             nimomx
      parameter           (nimomx = 50 )
C
C     Argument variables
C
      INTEGER             NMOT,        NMSYM
      INTEGER             INTORB
      INTEGER             NVALW
      INTEGER             NVALWK,      NVALX,       NVALY,       NVALZ
      INTEGER             PTHW,        PTHX,        PTHY,        PTHZ
      INTEGER             TOTSPC, ssym
      integer nintpt,nfct,neli
      character*60        fndrt
C
      REAL*8              CORE(*)
C
C     Local variables
C
      CHARACTER*80        TITLE
C
      INTEGER             IBUF,        IL,          INDSYM
      INTEGER             INDX,        IR,          IY,          LENBUF
      INTEGER             NCSFV,    NFVT,        NROW,        NSYM
      INTEGER             NVWALK(4),   NVWXY,       NWALK
      INTEGER             TOTX
      INTEGER             XBAR(4), ios
C
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:50),         B(LROW)
      INTEGER             XBARCDRT(LROW,3),          MU(50)
C
      COMMON / CDRT  /   B, XBARCDRT,       MU,          ARCSYM
C
C
C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS
C
C
C     Equivalenced common variables
C
      INTEGER             FILDRT,    NSLIST,      TAPE6
C
C
C     Common variables
C
      INTEGER             idum(13), NIOT
C
      LOGICAL             QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT, idum
C
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8),     DUMSYM
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    SCRLEN,      STRTBW(8,8)
      INTEGER             STRTBX(8,8), SYM12(8),    SYM21(8)
      INTEGER             SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      DUMSYM,      MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             CASE(0:LROW),             L(0:3,LROW,3)
      INTEGER             ROW(0:LROW), Y(0:3,LROW,3)
      INTEGER             YB(0:LROW),  YK(0:LROW), idum2(2*nimomx)
C
      COMMON / DRT    /   L,           Y,           YB,          YK
      COMMON / DRT    /   CASE,        ROW, idum2
C
C
C     Common variables
C
C
C
C     Common variables
C
      INTEGER             ORBSYM(MXINOR)
C
      COMMON / INF1   /   ORBSYM
C
C
C     Common variables
C
      INTEGER             EQVSYM(8),   IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(8),   NFRC(8),     NFRV(8),     NMUVAL
      INTEGER             NOCC(8),     NORBSM(8),   NOXT(8)
C
C
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
C
C     Common variables
C
      CHARACTER*4         LABELS(NSYMAX)
C
      COMMON / SLABEL /   LABELS

c
c
      logical skipso, spnorb, spnodd
      integer lxyzir
      common /solxyz/ skipso, spnorb, spnodd, lxyzir(3)
      integer ierr
c
C
C
C    END OF VARIABLE DECLARATIONS FOR subroutine tapind


      equivalence (nunits(1),tape6)
      equivalence (nunits(3),nslist)
c
c  bummer error types.
c                             memory for dynamic allocation
c                             location of indx array
c                             location of indsym array


      open( unit = fildrt, file = fndrt, status = 'old' ,
     +       form = 'formatted' ,iostat=ios)
      if (ios.ne.0)
     . call bummer ('can not open unit ',fildrt,2)


      title='xxxxxxxxxxxxxxxxxxxxxxxxxx'
      call rdhdrtn(
     & title,  nmot,       niot,   nfct,
     & nfvt,           nrow,       nsym,   ssym,
     & xbar,           nvwalk,     ncsfv,  lenbuf,
     & nwalk,          mxinor,     nmomx,  lrow,
     & core, tape6,  fildrt,spnorb,spnodd,lxyzir )
c
      pthz=xbar(1)
      pthy=xbar(2)
      pthx=xbar(3)
      pthw=xbar(4)
      nvalz=nvwalk(1)
      nvaly=nvwalk(2)
      nvalx=nvwalk(3)
      nvalw=nvwalk(4)
      nintpt=pthz+pthy+pthx+pthw
      nvalwk=nvalz+nvaly+nvalx+nvalw
      nmsym=nsym
c
       if (spnodd) niot=niot-1
c

      intorb=niot

      indx  =  1
      indsym  =  forbyt( nintpt )  +  indx
      ibuf=indsym+forbyt(nvalwk)
c
c     only indx,indsym and (temporarily) reference index
c     vector ir  are returned to the calling
c     program, ibuf etc is discarded !!
c     core(ir) contains the reference limvec

      ir=ibuf
      ibuf=ir+forbyt(pthz)
      nvwxy=nvwalk(2)+nvwalk(3)+nvwalk(4)
      lowdoc=nmot-(niot+nfct+nfvt)+1
      nmuval=niot
      totx=ibuf+max(forbyt(max(          nrow,nwalk,nvwxy)),
     + forbyt(niot))
      if( totx . gt. totspc ) then
          write(tape6,*)'          nrow,nwalk,nvwky,niot',
     +    nrow,nwalk,nvwxy,niot
          call bummer('space allocation for fildrt',0,faterr)
      endif
c

c     # read the drt arrays.
c
c
c
      call rddrtnx(
     & nmot,       niot,      nsym,        lrow,
     & nrow,       nwalk,     lenbuf,      pthz,
     & nvwxy,      iout,      nbfnsm,      nbas,
     & modrt,      orbsym(1), core(ibuf),  core(ibuf),
     & core(ibuf), b,         l,           y,
     & xbarcdrt,   core(1),   core(ir),    core(indsym),
     & neli,       labels,    core(ibuf),  fildrt, ssym,
     . nvalz)
c
c     call convindsym(core(indsym),nvalz,nvwxy)
      close(unit=fildrt)

c
      return
      end

      subroutine convindsym(indsym,nvalz,nvwxy)
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine convindsym
C
C     Argument variables
C
      INTEGER             INDSYM(*),   NVALZ,       NVWXY
C
C     Local variables
C
      INTEGER             I
C
C    END OF VARIABLE DECLARATIONS FOR subroutine convindsym


        do i=nvwxy,1,-1
         indsym(nvalz+i)=indsym(i)
        enddo
        do i=1,nvalz
         indsym(i)=1
        enddo
      return
      end




      subroutine setupone
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine tapinn
C
C     Parameter variables
C
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
      INTEGER             IJIJSZ
      PARAMETER           (IJIJSZ = 1023)
      integer maxsym
      parameter (maxsym=8)

c     arguments

      integer symv1,symv2
C
C     Local variables
C
      INTEGER             I,           INORB,       IORB,        ISYM
      INTEGER             NBLOCK,nges,nn, n2orbr
      integer   j,jsym,totsym
      real*8    two
C
C
C     Equivalenced common variables
C
      INTEGER             TAPE6
C
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYM,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      TOTVEC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   TOTVEC,      CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
C
C
C     Common variables
C
      INTEGER             EQVSYM(8),   IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(8),   NFRC(8),     NFRV(8),     NMUVAL
      INTEGER             NOCC(8),     NORBSM(8),   NOXT(8)
C
C
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
C     Common variables
C
      INTEGER             BLXT(8),     N2OFFR(8),   N2OFFS(8),   N2ORBT
      INTEGER             NOFFR(8),    NOFFS(8),    NSGES(8), nsdim(8)
C
      integer    dkntin (maxsym*(maxsym+1)/2)
      integer    densoff(maxsym*(maxsym+1)/2)
      integer    dsize
C
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   dsize,densoff,dkntin
C
C
C     Common variables
C
      INTEGER             IJIJ(IJIJSZ)
C
C
      COMMON / INF4X  /   IJIJ
C
C     Common variables
C
      INTEGER             NREC35,      NREC36, ssym1,ssym2
C
      COMMON / INF4   /   NREC35,      NREC36, ssym1,ssym2
C
C
C     Common variables
C
      INTEGER             MULT(8,8)
      COMMON / CSYM   /   MULT
C




C    END OF VARIABLE DECLARATIONS FOR subroutine tapinn

      data two /2.0d0/


c nmotx=maximum number of molecular orbitals
c mxinor=maximum number of internal orbitals
c  bummer error types.
c  initialize some symmetry arrays.
      nblock=nmsym
      do 10 i=1,8
      noxt(i)=0
      nocc(i)=0
      nfrc(i)=0
      nfrv(i)=0
      norbsm(i)=0
10    continue
c
      i=0
      inorb = 0
      do 30 isym=1,nmsym
      do 20 iorb=1,nbfnsm(isym)
      i=i+1
      if(iout(i).eq.-1)then
          nfrc(isym)=nfrc(isym)+1
      elseif(iout(i).eq.0)then
          nfrv(isym)=nfrv(isym)+1
      else
          norbsm(isym)=norbsm(isym)+1
c
c     count active orbitals and
c     create reverse index vector for non-frozen orbitals -eas
c
          inorb = inorb + 1
          ivout(iout(i)) = inorb
      endif
20    continue
30    continue
c
      do 40 isym=1,nmsym
      noxt(isym)=nbas(isym)
      nocc(isym)=norbsm(isym)-noxt(isym)
40    continue


c
c calculate the size of d1_sym and make sure enough space is
c allocated for it.
c
      noffs(1) = 0
      noffr(1) = 0
      nsdim(1)=nocc(1)+noxt(1)
      nsges(1)=nsdim(1)+nfrc(1)+nfrv(1)
      nges = nsdim(1)*(nsdim(1)+1)/2
      do 1010 i = 2,nmsym
          nn = nocc(i) + noxt(i)
          nsdim(i) = nn
          nsges(i) = nfrc(i) + nn + nfrv(i)
          noffs(i) = noffs(i-1) + nsges(i-1)
          noffr(i) = noffr(i-1) + nsdim(i-1)
          nges = nges + nn* (nn+1)/2
 1010 continue

        if (ssym1.eq.ssym2) then

        n2orbt = 0
        n2orbr = 0

       do 1011 i = 1,nmsym
          n2offs(i) = n2orbt
          n2offr(i) = n2orbr
          n2orbt = n2orbt + nsges(i)* (nsges(i)+1)/2
          n2orbr = n2orbr + nsdim(i)* (nsdim(i)+1)/2
 1011 continue

      ijij(1)=0
      do 1110 i=1,ijijsz-1
          ijij(i+1)=ijij(i)+i
 1110 continue

        else

        n2orbt = 0
        n2orbr = 0
        totsym=mult(ssym1,ssym2)
c
c # mark invalid elements by -1
c
       do i=1,nmsym
         n2offs(i)=-1
         n2offr(i)=-1
       enddo

       do 1211 i = 1,nmbpr(totsym)
c
c # assumption : nmbpr returns the possible number of pairs
c #              i.e. the number of non-zero density blocks
c #              lmda(totsym,i) and lmda(totsym,j) return isym,jsym
c #              with isym .le. jsym
          isym=lmda(totsym,i)
          jsym=lmdb(totsym,i)
          n2offs(isym) = n2orbt
          n2offr(isym) = n2orbr
          n2orbt = n2orbt + nsges(isym)* nsges(jsym)
          n2orbr = n2orbr + nsdim(isym)* nsdim(jsym)
 1211 continue
        endif


*@ifdef debug
*        write(*,*) 'n2offs(*)=',(n2offs(i),i=1,8)
*        write(*,*) 'noffs(*)=',(noffs(i),i=1,8)
*        write(*,*) 'n2offr(*)=',(n2offr(i),i=1,8)
*        write(*,*) 'noffr(*)=',(noffr(i),i=1,8)
*        write(*,*) 'nsges(*)=',(nsges(i),i=1,8)
*        write(*,*) 'nsdim(*)=',(nsdim(i),i=1,8)
*        write(*,*) 'n2orbt=',n2orbt
*@endif





      if (n2orbt.gt.iprpmx) then
          write (*,9190) n2orbt,iprpmx
 9190     format (1x,'dimension for d1_sym too small in routine densi ',
     +    ' nges = ',i5,'    iprpmx = ',i5)
          call bummer('n2orbt.gt.iprpmx',n2orbt,faterr)
      endif



      return
      end

      subroutine transp(n1,n2,a,b)
c
c     b=transpose(a)
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine transp
C
C     Argument variables
C
      INTEGER             N1,          N2
C
      REAL*8              A(N1,N2),    B(N2,N1)
C
C     Local variables
C
      INTEGER             I,           J
C
C    END OF VARIABLE DECLARATIONS FOR subroutine transp


      do 10 i=1,n1
          do 10 j=1,n2
              b(j,i)=a(i,j)
   10 continue
      return
      end


      subroutine upwkstcid(hilev,bver,kver,niot,rwlphd,yblp,yklp,
     +clev)
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine upwkstcidcid
C
C     Parameter variables
C
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 2**10-1)
      INTEGER             LROW
      PARAMETER           (LROW = 1023 )
      INTEGER             NFILMX
      PARAMETER           (NFILMX=55 )
      integer   nimomx
      parameter (nimomx=50)
C
C     Argument variables
C
      INTEGER             BVER,        CLEV,        HILEV,       KVER
      INTEGER             NIOT,        RWLPHD,      YBLP,        YKLP
C
C     Local variables
C
      INTEGER             I
C
C
C     Common variables
C
      INTEGER             CASE(0:LROW),             L(0:3,LROW,3)
      INTEGER             ROW(0:LROW), Y(0:3,LROW,3)
      INTEGER             YB(0:LROW),  YK(0:LROW), idum2(2*nimomx)
C
      COMMON / DRT    /   L,           Y,           YB,          YK
      COMMON / DRT    /   CASE,        ROW, idum2
C
C
C     Common variables
C
      INTEGER             BVERC,       CLEVC,       HILEVC,      KVERC
      INTEGER             NIOTC
C
      COMMON / WALKSV /   BVERC,       CLEVC,       HILEVC,      KVERC
      COMMON / WALKSV /   NIOTC
C
C    END OF VARIABLE DECLARATIONS FOR subroutine upwkstcidcid


ctk trying to correct the horrible art of programming - using the
ctk parameters of a former subroutine call in subsequent entry calls,
ctk where "hilev" and "niot" are only subroutine parm's, not entry
ctk parm's. some save's are good, too...
      save /walksv/
c      save hilev,niot,clev,bver,kver
c      hilev = hilv
c      niot = nit
c      bver = bvr
c      kver = kvr
c
c     for the given row of the loop head a valid upper walk is
c     constructed
c
      do 10 i=hilev,niot
          case(i)=4
   10 continue
      clev=hilev
      yk(clev)=yklp
      yb(clev)=yblp
      row(clev)=rwlphd
c save initialized parameters in common block
      clevc = clev
      bverc=bver
      hilevc=hilev
      niotc=niot
      kverc=kver
      return
      end


      subroutine upwlkcid( ket, bra, walk, mul )
c
      implicit logical(a-z)
c
      integer   nrowmx,     nimomx
      parameter(nrowmx=1023 , nimomx=50)
      integer
     & l,               y,               yb,             yk,
     & case,            row,             bord,           bsym
      common /drt/
     & l(0:3,nrowmx,3), y(0:3,nrowmx,3), yb(0:nrowmx),   yk(0:nrowmx),
     & case(0:nrowmx),  row(0:nrowmx),   bord(nimomx),   bsym(nimomx)
c
      integer         bverc, clevc, hilevc, kverc, niotc
      common /walksv/ bverc, clevc, hilevc, kverc, niotc
      save   /walksv/

      INTEGER             ARCSYM(0:3,0:50),         B(NROWMX)
      INTEGER             XBARCDRT(NROWMX,3),          MU(50)
C
      COMMON / CDRT  /   B, XBARCDRT,       MU,          ARCSYM

c
      integer mul
      logical skipso, spnorb, spnodd
      integer lxyzir
      common /solxyz/ skipso, spnorb, spnodd, lxyzir(3)
c
c
c
      integer ket, bra
      logical walk
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer niot, bver, clev, hilev, kver, nlev, icase, rowc,
     & brown, krown
c
      if (mul.gt.1) then
            mul=mul-1
            ket=ket+1
            bra=bra+1
           return
      endif

      rowc  = 0
      krown = 0
      brown = 0
c
c     # reassign saved common block values.
      niot  = niotc
      bver  = bverc
      clev  = clevc
      hilev = hilevc
      kver  = kverc
c
      walk = .true.
c
      if ( hilev .eq. niot ) then
         brown = row( clev )
c
         go to 500
      end if
      go to 100
c
c     # decrement the current level and check for exit.
c
90    continue
      case(clev) = 4
      if ( clev .eq. hilev ) go to 1000
      clev = clev - 1
c
c     # next case at the current level.
c
100   continue
      nlev = clev + 1
      icase = case(clev) - 1
      if ( icase .lt. 0 ) go to 90
      case(clev) = icase
      rowc = row(clev)
      brown = l(icase,rowc,bver)
      if ( brown .eq. 0 ) go to 100
      krown = l(icase,rowc,kver)
      if ( krown .eq. 0 ) go to 100
      if ( brown .ne. krown ) then
         call bummer('upwalk: (brown-krown)=',(brown-krown),faterr)
      endif
      row(nlev) = krown
      yb(nlev) = yb(clev) + y(icase,rowc,bver)
      yk(nlev) = yk(clev) + y(icase,rowc,kver)
      if ( nlev .eq. niot ) go to 500
      clev = nlev
      go to 100
500   continue
      walk = .false.
      bra  = yb(niot)
      ket  = yk(niot)
      mul = 1
      if(spnorb)mul = b(brown) + 1
c
c     # resave scalar parameters.
1000  continue
      clevc  = clev
      bverc  = bver
      hilevc = hilev
      niotc  = niot
      kverc  = kver
c
      return
      end


      subroutine wloop( bver, kver )
c
c  append loop information to ft.
c
c  24-nov-90 l(*), y(*), and xbar(*) moved back to /cdrt/. -rls
c  09-apr-90 rwlphd->head change. rwhdmx eliminated. -rls
c  modification of packing 2-12-87 (p. szalay)
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine wloop
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023 )
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 50)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 32000)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
C
C     Argument variables
C
      INTEGER             BVER,        KVER
C
C     Local variables
C
      INTEGER             HEAD,        I,           ICD4(0:3),   ICODE
      INTEGER             IT3,         IV3(3,4),    NV
      INTEGER             NV3(4),      XBT
C
      REAL*8              DWORD(4),    RELVAL,      VAL(3)
C
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             MU(NIMOMX),  XBAR(NROWMX,3)
C
      COMMON / CDRT  /   B,           XBAR,        MU,          ARCSYM
C
C
C     Common variables
C
      INTEGER             BCASEX(6,-2:3),           KCASEX(6,-2:3)
      INTEGER             NEX(-2:3)
C
      COMMON / CEX    /   NEX,         BCASEX,      KCASEX
C
C
C     Common variables
C
      INTEGER             KNTFT,       NFTB,        NFTBD,       NLOOP
C
      REAL*8              FTBUF(LBFT)
C
      COMMON / CFTB   /   FTBUF,       KNTFT,       NLOOP,       NFTB
      COMMON / CFTB   /   NFTBD
C
C
C     Common variables
C
      INTEGER             MAXW1,       NFTB1,       NFTB2,       NFTB3
      INTEGER             NFTB4,       NUMW1
C
      COMMON / CIBI   /   NFTB1,       NFTB2,       NFTB3,       NFTB4
      COMMON / CIBI   /   MAXW1,       NUMW1
C
C
C     Common variables
C
      INTEGER             CODE(4),     HILEV,       MO(0:4),     NIOT
      INTEGER             NINTL,NMO,         NUMV
C
      LOGICAL                     QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT,        NUMV,        HILEV
      COMMON / CLI    /   CODE,        MO,          NMO,         NINTL
C
C
C     Common variables
C
      INTEGER             QPRT
C
      COMMON / CPRT   /   QPRT
C
C
C     Common variables
C
      INTEGER             BROW(0:NIMOMX),           EXTBK(0:NIMOMX)
      INTEGER             KROW(0:NIMOMX),           RANGE(0:NIMOMX)
      INTEGER             YB(0:NIMOMX),             YK(0:NIMOMX)
C
      LOGICAL             QEQ(0:NIMOMX)
C
      REAL*8              V(3,0:NIMOMX)
C
      COMMON / CSTACK /   V,           RANGE,       EXTBK,       QEQ
      COMMON / CSTACK /   BROW,        KROW,        YB,          YK
C
C
C     Common variables
C
      INTEGER             VTPT
C
      REAL*8              OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON / CVTRAN /   VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
C
C     Common variables
C
      INTEGER             BTAIL,       CLEV,        ILEV,        IMO
      INTEGER             JKLSYM,      JLEV,        JMO
      INTEGER             JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER             KMO,         KTAIL,       LLEV,        LMO
      INTEGER             LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
C
C     Common variables
C
      INTEGER             HEADV(MAXBF),             IBF
      INTEGER             ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER             YBTV(MAXBF), YKTV(MAXBF)
C
      REAL*8              VALV(MAXBF,3)
C
      COMMON / SPECIAL/   VALV,        HEADV,       ICODEV,      XBTV
      COMMON / SPECIAL/   YBTV,        YKTV,        IBF
C
C    END OF VARIABLE DECLARATIONS FOR subroutine wloop


c     # bummer error types.
c     # dummy:
c     # local:
      equivalence ( val(1), dword(2) )
c
      data icd4/1,2,2,3/
      data nv3/1,2,2,3/
      data iv3/3,2,1, 2,3,2, 1,3,2, 1,2,3/
c
c     # the new packing method uses the head, not the weight. -rls
      head = brow(hilev)
      xbt  = xbar(head,bver)
      if ( xbt .eq. 0 .or. xbar(head,kver) .eq. 0 ) then
         write(*,6020)xbt,xbar(head,kver),hilev,head,bver,kver
         return
      endif
c
      go to (1100,1200,1300,1400,1500,1600), nintl
1100  continue
c
c     # 1-internal contributions:
c
      relval = v(1,hilev)/oldval
      if ( abs(relval-one) .gt. small ) then
c
c        # new relative loop value.
c
         icode = code(2)
         nv = 1
         val(1) = relval
         oldval = v(1,hilev)
      else
c
c        # same relative loop value.
c
         icode = code(1)
         nv = 0
      endif
      numw1 = numw1+nv+1
      go to 2000
c
1200  continue
c
c     # 2-internal contributions:
c
      if ( numv .eq. 1 ) then
         nv = 1
         val(1) = vcoeff(vtpt)*v(1,hilev)
         icode = code(1)
         go to 2000
      endif
c
      val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
      val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
      if ( val(1) .eq. zero ) then
         nv = 1
         val(1) = val(2)
         icode = code(2)
      else
         nv = 2
         icode = code(1)
      endif
      go to 2000
c
1300  continue
c
c     # 3-internal contributions:
c
      if(numv.eq.1)then
         nv = 1
         val(1) = vcoeff(vtpt)*v(1,hilev)
         icode = code(1)
      else
         val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
         val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
         if((val(1).eq.zero) .and. (code(2).ne.0))then
            nv = 1
            val(1) = val(2)
            icode = code(2)
         else
            nv = 2
            icode = code(1)
         endif
      endif
      go to 2000
c
1400  continue
c
c     # 4-internal contributions:
c
      go to (1410,1420,1430),numv
1410  continue
c
c     # one-value loops:
c
      nv = 1
      val(1) = vcoeff(vtpt)*v(1,hilev)
      icode = code(1)
      go to 2000
c
1420  continue
c
c     # two-value loops:
c
      val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
      val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
      if ( (val(1) .eq. zero) .and. (code(2) .ne. 0) ) then
         nv = 1
         val(1) = val(2)
         icode = code(2)
      else
         nv = 2
         icode = code(1)
      endif
      go to 2000
c
1430  continue
c
c     # three-value loops:
c
      it3 = 1
      if(v(2,hilev).ne.zero)it3 = it3+1
      if(v(1,hilev).ne.zero)it3 = it3+2
c
c     # it3=1  12c   .ne.0
c     # it3=2  12bc  .ne.0
c     # it3=3  12ac  .ne.0
c     # it3=4  12abc .ne.0
c
      nv = nv3(it3)
      val(1) = v( iv3(1,it3), hilev)
      val(2) = v( iv3(2,it3), hilev)
      val(3) = v( iv3(3,it3), hilev)
      icode = code(it3)
      go to 2000
c
1500  continue
c
c     # two internal diagonal contributions:
c
      nv = numv
      if ( nv .eq. 1 ) then
         val(1) = vcoeff(vtpt)*v(1,hilev)
      else
         val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
         val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
      endif
      icode = code(1)
      go to 2000
c
1600  continue
c
c     # four internal diagonal:
c     # see routine diag for code definitions.
c
      nv = numv
      if(nv.eq.1)then
         val(1) = vcoeff(vtpt)*v(1,hilev)
         icode = code(icd4(bcasex(extbk(hilev-1),3)))
      else
         val(1) = vtran(1,1,vtpt)*v(1,hilev)+vtran(2,1,vtpt)*v(2,hilev)
         val(2) = vtran(1,2,vtpt)*v(1,hilev)+vtran(2,2,vtpt)*v(2,hilev)
         icode = code(1)
      endif
c
2000  continue
c
c     # process loop.
c
      nloop = nloop + 1

      ibf=ibf+1
      headv(ibf) = brow(hilev)
      ybtv(ibf)  = yb(hilev)
      yktv(ibf)  = yk(hilev)
      xbtv(ibf)  = xbar(head,bver)
      icodev(ibf)=icode
      do i=1,nv
       valv(ibf,i)=val(i)
      enddo
      do i=nv+1,3
       valv(ibf,i)=0.0d0
      enddo
c      write(*,6010)icode,xbt,ybt,ykt,head,(val(i),i=1,nv)
      if (ibf.eq.maxbf-1) then
       ibf=ibf+1
       icodev(ibf)=1
       break=.true.
c      write(*,6010)icode,xbt,ybt,ykt,head,(val(i),i=1,nv)
      else
       break =.false.
      endif

      return
6010  format(' wloop: code,xbt,ybt,ykt,head,val=',
     + i2,4i6,3f10.6)
6020  format(' test in wloop, xbt=0',10i4)
      end


      subroutine wrint2( unit, vector, length )
c
c  write a formatted integer vector to unit.
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine wrint2
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             LENGTH,      UNIT,        VECTOR(LENGTH)
C
C     Local variables
C
      INTEGER             ERCODE
C
C    END OF VARIABLE DECLARATIONS FOR subroutine wrint2


c  bummer error types.
      write(unit, '(1x,10i10)', iostat = ercode, err = 900 ) vector
c
      return
c
c  error
c
  900 continue
      write(unit, * ) 'wrint2:  error during write  --', ercode
      write(unit, * ) 'unit  --', unit, '   length  --', length
      call bummer(' ',0,faterr)
      end


      subroutine wrtint( unit, vector, length )
c
c  write a formatted integer vector.
c
      implicit logical(a-z)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer unit, length
      integer vector ( length )
c
      integer ercode
c
      write(unit, '(1x,10i10)', iostat = ercode, err = 900 ) vector
c
      return
c
c  error
c
  900 continue
      write(unit, * ) 'wrtint:  error during write  --', ercode
      write(unit, * ) 'unit  --', unit, '   length  --', length
      call bummer('wrtint: ercode=',ercode,faterr)
      end


      subroutine yw1x( indsymyz,conftyz,
     .           indsym,conft,indxyz,index,ci,ciyz,scr,tran)
c
c     calculation of one external elements for y- w-interactions
c
c     transf    value for the internal part of the loop
c     ci        ci vector for x paths
c     cyz       ci vector for y paths
c     iden1=0  compute first-order density matrix only
c          =1  compute second order density matrix
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine yw1x
C
C     Argument variables
C
      INTEGER             CONFT(2),    INDEX(2)
      INTEGER             INDSYM(2),   INDXYZ(2)
      integer             indsymyz(*), conftyz(*)
C
C
      REAL*8              CI(2),       CIYZ(2),     SCR(*)
      REAL*8              TRAN(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         M1LP
      INTEGER             M1LPR,       NMB1,        NMB1A,       NMB2
      INTEGER             NMB2A,       NY,          STRTWX,      WS
      INTEGER             WSYM,        YS,          YSYM,        MUL
C
      LOGICAL             WALK
C
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             CISTRT,   CODE,        HILEV,       INDI1
      INTEGER             ISYM,        MLP,         MLPPR,       MLPPRZ
      INTEGER             MLPZ,        NI,          NIOT,        RWLPHD
      INTEGER             SEGSUM,      WTLPHD
C
      REAL*8              SQRT2
C
      COMMON / OX     /   SQRT2,       WTLPHD,      MLP,         MLPPR
      COMMON / OX     /   MLPPRZ,      MLPZ,        SEGSUM,      CISTRT
      COMMON / OX     /   ISYM,        NI,          CODE,        INDI1
      COMMON / OX     /   RWLPHD,      HILEV,       NIOT

c
      logical skipso, spnorb, spnodd
      integer lxyzir
      common /solxyz/ skipso, spnorb, spnodd, lxyzir(3)
c

C
C    END OF VARIABLE DECLARATIONS FOR subroutine yw1x


c     common /inf/pthz,pthy,pthx,pthw,nintpt,dimci,maxbl4,nmblk4,kbl4,
c    +       mxkbl4,lamda1,bufszi,nmonel,intorb,maxbl3,nmblk3,kbl3,
c    +       mxkbl3,maxlp3,bufszl,maxbl2,mxbl23,mxorb,idum(11),n1ext,
c    +       n0ext,n3xtlp,n2xtlp,n1xtlp,n0xtlp,spcci,confyz,pthyz,
c    +       nvalwk,nvalz,nvaly,nvalx,nvalw

      external matmul

      if(spnodd)niot = niot + 1
      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0


      call upwkstcid(hilev,3,2,niot,rwlphd,mlppr,mlp,clev)
   10 continue
      call upwlkcid(ket,bra,walk,mul)
      if (.not.walk) then
c
   15      continue
           ket = ket + 1
           bra = bra + 1
c
          m1lp = ket
          nmb1a = indxyz(m1lp)
          if (nmb1a.ge.0) then
              nmb1 = conftyz(nmb1a)
              m1lpr = bra
c             write(6,*) 'yw1x:1: nmb1,m1lpr,segsum=',nmb1,m1lpr,segsum
              if (m1lpr.gt.0) then
                  if (m1lpr.gt.segsum) then
                      return

                  else
                      nmb2a = index(m1lpr)
                      if (nmb2a.ge.0) then
                          nmb2 = conft(nmb2a)
c                         write(6,*) 'yw1x: nmy,,nmb2:',nmb1,nmb2
                          nmb2 = nmb2 - cistrt
                          ys = indsymyz(nmb1a)
                          ws = indsym(nmb2a)
                          ysym = sym21(ys)
                          wsym = sym21(ws)
                          if (ysym-isym) 20,30,40
c
c     lmd(y).lt.lmd(i)
c
c
c y determined through auxilary index p
c Fall II
c
   20                     ny = nbas(ysym)
                          if (ny.ne.0) then
                              strtwx = nmb2 + strtbw(isym,wsym) + 1
                              call matmul(scr,ciyz(nmb1+1),ci(strtwx),1,
     +                                    ni,ny,1)
                          end if

                          go to 50
c
c     lmd(y).eq.lmd(i),lmd(w)=1
c
   30                     strtwx = nmb2 + strtbw(isym,wsym) + 1
c
c # auch fuer den allgemeinen Fall ok
c
                          call expds(ni,ci(strtwx),tran)
                          call matmul(scr,tran,ciyz(nmb1+1),ni,1,ni,1)
                          continue

                          go to 50
c
c     lmd(y).gt.lmd(i)
c
   40                     ny = nbas(ysym)
                          if (ny.ne.0) then

c y determined through auxilary index p
c Fall II
c
                              strtwx = nmb2 + strtbw(ysym,wsym) + 1
                              call matmul(scr,ci(strtwx),ciyz(nmb1+1),
     +                                    ni,1,ny,1)
                          end if

                      end if

                  end if

              end if

          end if

   50    continue
         mul = mul - 1
         if ( mul.gt.0 ) goto 15
          if (hilev.lt.niot) go to 10
      end if
c     write(6,32000)m1lpr,segsum
 9000 format (' yw1',20i6)
c     write(6,30100)mlppr,m1lpr,mlp,m1lp,nmb1,nmb2,ysym,wsym,ni,isym
 9010 format (' 900',20i6)
c     write(6,30300)strtwx,ny,ysym
 9020 format (' 500',20i4)
c     write(6,30110)strtwx,ny,ysym
 9030 format (' 600',20i4)
c     write(6,30120)strtwx
 9040 format (' 700',20i4)

      end


      subroutine yx1x( indsymyz,conftyz,
     .   indsym,conft,indxyz,index,ci,ciyz,scr,tran)
c
c     calculation of one external elements for y- x-interactions
c
c     transf    value for the internal part of the loop must be
c               muliplied by 1 to be closed in the external space
c     ci        ci vector for x paths
c     cyz       ci vector for y paths
c
       implicit none
C    VARIABLE DECLARATIONS FOR subroutine yx1x
C
C     Argument variables
C
      INTEGER             CONFT(2),    INDEX(2),    INDSYM(2)
      INTEGER             INDXYZ(2) , conftyz(*),indsymyz(*)
C
      REAL*8              CI(2),       CIYZ(2),     SCR(*),      TRAN(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         M1LP
      INTEGER             M1LPR,       NMB1,        NMB1A,       NMB2
      INTEGER             NMB2A,       NY,          STRTWX,      XS
      INTEGER             XSYM,        YS,          YSYM,        MUL
C
      LOGICAL             WALK
C
C     Common variables
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
C
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
C
C     Common variables
C
      INTEGER             CISTRT,      CODE,        HILEV,       INDI1
      INTEGER             ISYM,        MLP,         MLPPR,       MLPPRZ
      INTEGER             MLPZ,        NI,          NIOT,        RWLPHD
      INTEGER             SEGSUM,      WTLPHD
C
      REAL*8              SQRT2
C
      COMMON / OX     /   SQRT2,       WTLPHD,      MLP,         MLPPR
      COMMON / OX     /   MLPPRZ,      MLPZ,        SEGSUM,      CISTRT
      COMMON / OX     /   ISYM,        NI,          CODE,        INDI1
      COMMON / OX     /   RWLPHD,      HILEV,       NIOT
c
c
      logical skipso, spnorb, spnodd
      integer lxyzir
      common /solxyz/ skipso, spnorb, spnodd, lxyzir(3)
c

      external matmul
c
c-----------------------------------------------------------------------
c
c  scr(ni)

      if (spnodd) niot=niot+1
      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0

      call upwkstcid(hilev,3,2,niot,rwlphd,mlppr,mlp,clev)
   10 continue
      call upwlkcid(ket,bra,walk,mul)
      if (.not.walk) then
   15     continue
          ket=ket+1
          bra=bra+1
          m1lp = ket
          nmb1a = indxyz(m1lp)
          if (nmb1a.ge.0) then
              nmb1 = conftyz(nmb1a)
              m1lpr = bra
              if (m1lpr.gt.0) then
                  if (m1lpr.gt.segsum) then
                      return

                  else

                      nmb2a = index(m1lpr)
                      if (nmb2a.ge.0) then
                          nmb2 = conft(nmb2a)
                          nmb2 = nmb2 - cistrt
                          ys = indsymyz(nmb1a)
                          xs = indsym(nmb2a)
                          ysym = sym21(ys)
                          xsym = sym21(xs)
                          if (ysym-isym) 20,30,40
c
c     lmd(y).lt.lmd(i)
c
   20                     ny = nbas(ysym)
                          if (ny.ne.0) then
                              strtwx = nmb2 + strtbx(isym,xsym) + 1
c      write(*,*) 'yx1x lmdy.lt.lmdi'
c      write(*,*) 'ciyz(nmb1+1)'
c      call writex (ciyz(nmb1+1),ny)
c      write(*,*) 'ci(strtwx)'
c      call writex (ci(strtwx),ni)
                              call matmul(scr,ciyz(nmb1+1),ci(strtwx),1,
     +                                    ni,ny,1)
                          end if

                          go to 50
c
c     lmd(y)=lmd(i),lmd(x)=1
c
   30                     if (ni.ge.2) then
                              strtwx = nmb2 + strtbx(isym,xsym) + 1
c
c # auch fuer den allgemeinen Fall ok
c
c      write(*,*) 'yx1x lmdy.eq.lmdi'
c      write(*,*) 'ciyz(nmb1+1)'
c      call writex (ciyz(nmb1+1),ni)
c      write(*,*) 'ci(strtwx)'
c      call writex (ci(strtwx),ni)
                              call expda1(ni,ci(strtwx),tran)
                              call matmul(scr,tran,ciyz(nmb1+1),ni,1,ni,
     +                                    1)
                          end if

                          go to 50
c
c     lmd(y).gt.lmd(i)
c
   40                     ny = nbas(ysym)
                          if (ny.ne.0) then
                             strtwx = nmb2 + strtbx(ysym,xsym) + 1
c      write(*,*) 'yx1x lmdy.gt.lmdi'
c      write(*,*) 'ciyz(nmb1+1)'
c      call writex (ciyz(nmb1+1),ny)
c      write(*,*) 'ci(strtwx)'
c      call writex (ci(strtwx),ni)
                              call dcopym(ny,ciyz(nmb1+1),tran)
                              call matmul(scr,ci(strtwx),tran,ni,1,ny,1)
                          end if

                      end if


                  end if

              end if

          end if
c     call writex(scr,ni)
   50     continue
          mul=mul-1
          if (mul.gt.0) goto 15
          if (hilev.lt.niot) go to 10
      end if

c     write(6,32000)m1lpr,segsum
 9000 format (' yx1',20i6)
c     write(6,30050)mlppr,m1lpr,mlp,m1lp,nmb1,nmb2,ysym,xsym,ni,isym
 9010 format (' yx700',20i6)
c     write(6,30100)strtwx,ny,ysym
 9020 format (' 900',20i4)
c     write(6,30110)strtwx,ny,ysym
 9030 format (' 1000',20i4)
c     write(6,30120)strtwx
 9040 format (' 1100',20i4)

      end
c
c***********************************************************************
c
      subroutine zy1x(
     &  conft1,    indxyz1,    ciyz1,    conft2,
     &  indxyz2,   ciyz2,      scr2,     scr2_asym,
     &  jkleq)
c
       implicit none
C    VARIABLE DECLARATIONS FOR subroutine zy1x
C
C     Argument variables
C
      INTEGER  CONFT1(*), conft2(*),  indxyz1(*),INDXYZ2(*)
C
      LOGICAL             JKLEQ
C
      REAL*8  ciyz2(*),   CIYZ1(*),     SCR2(*),     scr2_asym(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         NMB1
      INTEGER             NMB1A,       NMB2,        NMB2A,       MUL
C
      LOGICAL             WALK
C
C
C     Common variables
C
      INTEGER             CISTRT,      CODE,        HILEV,       INDI1
      INTEGER             ISYM,        MLP,         MLPPR,       MLPPRZ
      INTEGER             MLPZ,        NI,          NIOT,        RWLPHD
      INTEGER             SEGSUM,      WTLPHD
C
      REAL*8              SQRT2
C
      COMMON / OX     /   SQRT2,       WTLPHD,      MLP,         MLPPR
      COMMON / OX     /   MLPPRZ,      MLPZ,        SEGSUM,      CISTRT
      COMMON / OX     /   ISYM,        NI,          CODE,        INDI1
      COMMON / OX     /   RWLPHD,      HILEV,       NIOT
C
c
      logical skipso, spnorb, spnodd
      integer lxyzir
      common /solxyz/ skipso, spnorb, spnodd, lxyzir(3)
c

C    END OF VARIABLE DECLARATIONS FOR subroutine zy1x

*@ifdef debug
*      real*8 scr2_md(1000000)
*      real*8 dnrm2,norm
*      external dnrm2
*@endif

c     one external zy loops
c     iden1=0  compute first-order density matrix only
c          =1  compute second order density matrix

c  muss mul nicht initialisiert werden??

      if (spnodd) niot=niot+1

      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0

      call upwkstcid(hilev,2,1,niot,rwlphd,mlppr,mlp,clev)
   10 continue
      call upwlkcid(ket,bra,walk,mul)
        if (.not.walk) then
   20   continue
        ket=ket+1
        bra=bra+1
c
c------------------------------------------------------------
         nmb1a = indxyz2(ket)
           if (nmb1a.ge.0) then
            nmb1 = conft2(nmb1a)
            nmb2a = indxyz1(bra)
              if (nmb2a.ge.0) then
               nmb2 = conft1(nmb2a)
               nmb1 = nmb1 + 1
                 if (jkleq) then
c                 call daxpy_wr(ni,ciyz2(nmb1),ciyz1(nmb2+1),1,scr1,1)
c           #  sym contributions
                  call daxpy_wr(ni,0.5d0*ciyz2(nmb1),ciyz1(nmb2+1),1,
     &             scr2,1)
c           #  asym contributions
                  call daxpy_wr(ni,0.5d0*ciyz2(nmb1),ciyz1(nmb2+1),1,
     &             scr2_asym,1)
*@ifdef debug
*                  call wzero(ni,scr2_md)
*                  call daxpy(ni,0.5d0*ciyz2(nmb1),ciyz1(nmb2+1),1,
*     &             scr2_md,1)
*                    norm = dnrm2(ni,scr2_md,1)
*                    write(*,*)'onext case5',norm
*@endif
                 endif ! end of: if (jkleq) then
              end if ! end of: if (nmb2a.ge.0) then
           endif ! end of: if (nmb1a.ge.0) then
c
c------------------------------------------------------------
         nmb1a = indxyz1(ket)
           if (nmb1a.ge.0) then
            nmb1 = conft1(nmb1a)
            nmb2a = indxyz2(bra)
              if (nmb2a.ge.0) then
               nmb2 = conft2(nmb2a)
               nmb1 = nmb1 + 1
                 if (jkleq) then
c                 call daxpy_wr(ni,ciyz1(nmb1),ciyz2(nmb2+1),1,scr2,1)
c           #  sym contributions
                  call daxpy_wr(ni,0.5d0*ciyz1(nmb1),ciyz2(nmb2+1),1,
     &             scr2,1)
c           #  asym contributions
                  call daxpy_wr(ni,-0.5d0*ciyz1(nmb1),ciyz2(nmb2+1),1,
     &             scr2_asym,1)
*@ifdef debug
*                  call wzero(ni,scr2_md)
*                  call daxpy(ni,-0.5d0*ciyz1(nmb1),ciyz2(nmb2+1),1,
*     &             scr2_md,1)
*                  norm = dnrm2(ni,scr2_md,1)
*                    write(*,*)'onext case6',norm
*@endif
                 endif ! end of: if (jkleq) then
              end if ! end of: if (nmb2a.ge.0) then
           end if ! end of: if (nmb1a.ge.0) then
c
          mul = mul -1
          if (mul.gt.0) goto 20
          if (hilev.lt.niot) go to 10
c
      end if ! end of: if (.not.walk) then
c
c     write(6,30040)nmb1,nmb2,ni,ket,bra,ni,isym
 9000 format (' 600',20i6)
c     write(6,30000)ciyz(nmb1)
 9010 format (' zy700',f10.6)

      return
      end
c
c***********************************************************************
c
       subroutine exch(i,j)
        implicit none 
        integer i,j,t
        t=i
        i=j
        j=t
        return
        end
c
c***********************************************************************
c
        subroutine printmatrix(a,ni,nj,header)
c
c  a,zeilen,spalten
c
         implicit none 
         integer ni,nj,i,j,ij
c       ni = leading dimension
        real*8 a(ni*nj)
         character*(*) header
         character*100 chrmat(100)
         if (ni.gt.100) call bummer('printmatrix ni too large',ni,2)

         write(*,*) 'ni,nj=',ni,nj
         do i=1,nj
          chrmat(i)='                   '
         enddo

         write(*,*) header
         ij=0
         do j=1,nj
          write(*,100) (a(ij+i),i=1,ni)
          do i=1,ni
           if (abs(a(ij+i)).gt.1.d-8) then
             chrmat(j)(i:i)='#'
            else
             chrmat(j)(i:i)='0'
           endif
         enddo
          ij =ij+ni
         enddo
         do i=1,nj
          write(*,102) chrmat(i)
         enddo
  102    format (a20)
  100    format (10(f10.8,1x))
         return
         end



         subroutine printtrigmat(a,ni,header,mode)
          implicit none 
          integer ni,k,i,j,mode
c mode =1 column mode
c mode =0 row mode
         real*8 a(ni*(ni+1)/2)
         character*(*) header
          write(*,*) header
          if (mode.eq.1) then
          do i=1,ni
              write(*,100)(a(i+j*ni-j*(j+1)/2),j=0,i-1)
          enddo
          elseif (mode.eq.0) then
           k=0
           do i=1,ni
              write(*,100)(a(k+j),j=1,i)
             k=k+i
           enddo
          else
           call bummer ('printtrigmat, ivalid mode, mode=',mode,2)
          endif
  100     format( 8(f12.9,2x))
          return
          end

      subroutine loop3in( btail, ktail, stype, db0, nran, ir0,
     .  indxyz,index1,index2,indxyz2,index12,index22,
     .  ipth1,segsm,ipthyz,nmbseg,itype)
c
c  construct loops and accumulate products of segment values.
c
c  input:
c  btail=    bra tail of the loops to be constructed.
c  ktail=    ket tail of the loops to be constructed.
c  stype(*)= segment extension type of the loops.
c  db0(*)=   delb offsets for the vn(*) segment values.
c  nran=     number of ranges for this loop template.
c  ir0=      range offset for this loop.
c  /cpt/     segment value pointer tables.
c  /cex/     segment type extension tables.
c  /ctab/    segment value tables.
c  /cdrt/    drt information.
c  /cdrt2/   vectors spanning number of walks.
c  /cstack/  loop construction stack arrays.
c  /csym/    symmetry information.
c  /cli/     loop information.
c
c  modified for mu-dependent segments 9-nov-86 (-rls).
c  this version written 07-sep-84 by ron shepard.
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine loop
C
C     Parameter variables
C
      INTEGER             NST
      PARAMETER           (NST = 40)
      INTEGER             NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NWLKMX
      PARAMETER           (NWLKMX = 2**20-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 50)
C
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER    nran,    BTAIL,       DB0(NRAN,0:2,*),          IR0
      INTEGER             KTAIL,       STYPE(NRAN)
      integer  indxyz(*),index1(*),index2(*),ipth1(2),segsm(2),
     .  ipthyz,nmbseg,itype
      integer  indxyz2(*), index12(*),index22(*)
C
C     Local variables
C
      INTEGER             BBRA,        BCASE,       BKET,        BROWC
      INTEGER             BROWN,       BVER,        DELB,        EXT
      INTEGER             I,           IGONV,       IL,          IMU
      INTEGER             IR,          KCASE,       KROWC,       KROWN
      INTEGER             KVER,        NEXR,        NLEV
      INTEGER             NUMBEROFLOOPS,            R,           STR
C
      LOGICAL             QBCHK(-4:4)
      logical   valid(2,2),validyz(2)
      integer   kseg,indxx, wtlphdk,wtlphdb
C
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             MU(NIMOMX),  XBAR(NROWMX,3)
C
      COMMON / CDRT   /   B,           XBAR,        MU,          ARCSYM
C

C
C     Common variables
C
      INTEGER             BCASEX(6,-2:3),           KCASEX(6,-2:3)
      INTEGER             NEX(-2:3)
C
      COMMON / CEX    /   NEX,         BCASEX,      KCASEX
C
C
C     Common variables
C
      INTEGER             CODE(4),     HILEV,       MO(0:4),     NIOT
      INTEGER             nintl,NMO,         NUMV
C
      LOGICAL                     QDIAG
C
      COMMON / CLI    /   QDIAG,       NIOT,        NUMV,        HILEV
      COMMON / CLI    /   CODE,        MO,          NMO,         NINtl
C
C
C     Common variables
C
      INTEGER             PT(0:3,0:3,NST5)
C
      COMMON / CPT    /   PT
C
C
C     Common variables
C
      INTEGER             BROW(0:NIMOMX),           EXTBK(0:NIMOMX)
      INTEGER             KROW(0:NIMOMX),           RANGE(0:NIMOMX)
      INTEGER             YB(0:NIMOMX),             YK(0:NIMOMX)
C
      LOGICAL             QEQ(0:NIMOMX)
C
      REAL*8              V(3,0:NIMOMX)
C
      COMMON / CSTACK /   V,           RANGE,       EXTBK,       QEQ
      COMMON / CSTACK /   BROW,        KROW,        YB,          YK
C
C
C     Common variables
C
      INTEGER             MULT(8,8)
      COMMON / CSYM   /   MULT
C
C
C     Common variables
C
      REAL*8              TAB(NTAB,0:MAXB)
C
      COMMON / CTAB   /   TAB
C
C
      INTEGER             L(0:3,NROWMX,3),          Y(0:3,NROWMX,3)
      integer      idum( (nrowmx+1)*4 + 2*nimomx )
C
      COMMON / DRT    /   L,           Y, idum

C
C     Common variables
C
      INTEGER             CLEV,        ILEV,        IMO,         JKLSYM
      INTEGER             JLEV,        JMO,         JUMPINPOSITION
      INTEGER             KLEV,        KLSYM,       KMO,         LLEV
      INTEGER             LMO,         LSYM,        TAILB,       TAILK
      INTEGER             VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  TAILB,       TAILK,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
C    END OF VARIABLE DECLARATIONS FOR subroutine loop


c     # dummy:
c     # local:
      data qbchk/.true.,.true.,.false.,.false.,.false.,
     +  .false.,.false.,.true.,.true./
c
c     write(*,*) 'loop3in: ipth1',ipth1,'segsm',segsm,'ipthyz',ipthyz,
c    .  'nmbseg',nmbseg

      bver = min( btail, 3 )
      kver = min( ktail, 3 )
c
c     # assign a range (ir1 to nran) for each level of the
c     # current loop type.
c
      hilev = mo(nmo)
c
      ir = ir0
      do 120 i = 1, nmo
         ir = ir + 1
         do 110 il = mo(i-1), (mo(i)-2)
            range(il) = ir
110      continue
         ir = ir+1
         range(mo(i)-1) = ir
120   continue
c
c     # initialize stack arrays for loop construction.
c
      if (.not. break) then
      clev    = 0
      yb(0)   = 0
      yk(0)   = 0
      brow(0) = btail
      krow(0) = ktail
      qeq(0)  = btail.eq.ktail
      v(1,0)  = one
      v(2,0)  = one
      v(3,0)  = one
      endif
c
c     # one, two, or three segment value products are accumulated.
c     # begin loop construction:
c
      if (.not. break) go to 1100
c     goto 1100
c
1000  continue
c
c     # decrement the current level, and check if done.
c
      extbk(clev) = 0
      clev = clev-1
      if(clev.lt.0) then
       break=.false.
       return
       endif
c
1100  continue
c
c     # new level:
c
      if(clev.eq.hilev)then
c
c        qdiag=t :allow both diagonal and off-diagonal loops.
c        qdiag=f :allow only off-diagonal loops.
c
         if ( qdiag .or. (.not. qeq(hilev)) ) then
            call wloop( bver, kver )
            if (break) return
         endif
         go to 1000
      endif
c
      nlev = clev+1
      r = range(clev)
      str = stype(r)
      nexr = nex(str)
1200  continue
c
c     # new extension:
c
      ext = extbk(clev)+1
      if(ext.gt.nexr)go to 1000
      extbk(clev) = ext
      bcase = bcasex(ext,str)
      kcase = kcasex(ext,str)
c
c     # check for extension validity.
c
c     # canonical walk check:
c
      if ( qeq(clev) .and. (bcase .lt. kcase) ) go to 1200
      qeq(nlev) = qeq(clev).and.(bcase.eq.kcase)
c
c     # individual segment check:
c
      browc = brow(clev)
      brown = l(bcase,browc,bver)
      if(brown.eq.0)go to 1200
      krowc = krow(clev)
      krown = l(kcase,krowc,kver)
      if(krown.eq.0)go to 1200
      brow(nlev) = brown
      krow(nlev) = krown
c
c     # check b values of the bra and ket walks.
c
      bbra = b(brown)
      bket = b(krown)
      delb = bket-bbra
c
c     # the following check is equivalent to:
c     # if ( abs(delb) .gt. 2 ) go to 1200
c
      if ( qbchk(delb) ) go to 1200
ctm ************************************************************+
c 1     onext:  indxyz|index1
c               indxyz|index2
c               indxyz|indxyz
c 2     diag :  index1|index1
c               index2|index2
c               indxyz|indxyz
c 3    allint:  indxyz|indxyz
c               index1|index1
c               index2|index1
c               index2|index2
c
c        <m'|Ers|m>  m'< m
c       index1 < index2
c
c
c      validyz(1)  bra within yz
c      validyz(2)  ket within yz
c      valid(1,ksg)  bra within ksg
c      valid(2,ksg)  ket within ksg
ctm ************************************************************

c    initialize
c
      valid(1,1)=.false.
      valid(1,2)=.false.
      valid(2,1)=.false.
      valid(2,2)=.false.
      validyz(1)=.false.
      validyz(2)=.false.

c  # determine validyz and valid
c
c     # bra-walk extension check:
c       valid range: yk(nlev)+1 to yk(nlev)+wtlphd
c
      yb(nlev) = yb(clev)+y(bcase,browc,bver)
      wtlphdb = xbar(brown,bver)

      do kseg=1,nmbseg
        if  ( (yb(nlev)+wtlphdb-ipth1(kseg)).gt.0 .and.
     .        (yb(nlev)-ipth1(kseg)).lt.segsm(kseg))
     .  valid(1,kseg)=.true.
      enddo
      if ((yb(nlev)+wtlphdb).ge.0 .and. (yb(nlev).lt.ipthyz))
     .  validyz(1)=.true.

c
c     # ket-walk extension check:
c      valid range: yk(nlev)+1 to yk(nlev)+wtlphd
c
      yk(nlev) = yk(clev)+y(kcase,krowc,kver)
      wtlphdk = xbar(krown,kver)
      do kseg=1,nmbseg
        if  ( (yk(nlev)+wtlphdk-ipth1(kseg)).gt.0 .and.
     .        (yk(nlev)-ipth1(kseg)).lt.segsm(kseg))
     .  valid(2,kseg)=.true.
      enddo
       if ((yk(nlev)+wtlphdk).ge.0 .and. (yk(nlev).lt.ipthyz))
     .  validyz(2)=.true.

c check whether the bra and ket combinations are valid

       if (itype.eq.1) then
c 1     onext:  index1|indxyz
c               index2|indxyz            bra und ket scheinbar vertausch
c               indxyz|indxyz

       if (.not. (validyz(2))) goto 1200
       if (.not. (validyz(1).or.valid(1,1).or.valid(1,2)) ) goto 1200
       elseif (itype.eq.2) then
c 2     diag :  index1|index1
c               index2|index2
c               indxyz|indxyz
         if (.not.( (validyz(1).and.validyz(2))
     .          .or. ( valid(1,1).and.valid(2,1))
     .          .or. ( valid(1,2).and.valid(2,2)) ) ) goto 1200

       elseif (itype.eq.3) then
c 3    allint:  indxyz|indxyz
c               index1|index1
c               index2|index1
c               index2|index2
          if (.not.(  (validyz(1).and.validyz(2))
     .           .or. (valid(1,1).and.valid(2,1))
     .           .or. (valid(1,2).and.valid(2,1))
     .           .or. (valid(1,2).and.valid(2,2)) )) goto 1200
      else
      call bummer ('loop3in: invalid nmbseg, nmbseg=',nmbseg,2)
      endif


c
c check for invalid walks
c
c
c #   bra invalid in yz
c
      if (validyz(1)) then
       if (-indxyz(yb(nlev)+1).ge. wtlphdb .and.
     .     -indxyz2(yb(nlev)+1).ge.wtlphdb ) goto 1200
      endif
c
c #   ket invalid in yz
c
      if (validyz(2)) then
       if (-indxyz(yk(nlev)+1).ge. wtlphdk .and.
     .     -indxyz2(yk(nlev)+1).ge. wtlphdk ) goto 1200
      endif
c
c #   bra invalid in index1
c

      if (valid(1,1)) then
       indxx = yb(nlev)+1-ipth1(1)
       if (indxx.lt.1) then
          wtlphdb = wtlphdb +indxx -1
          indxx=1
       endif
       if (-index1(indxx).ge.wtlphdb .and.
     .     -index12(indxx).ge.wtlphdb ) goto 1200
      endif
c
c #   bra invalid in index2
c
      if (valid(1,2)) then
       indxx = yb(nlev)+1-ipth1(2)
       if (indxx.lt.1) then
          wtlphdb = wtlphdb +indxx -1
          indxx=1
       endif
       if (-index2(indxx).ge.wtlphdb .and.
     .     -index22(indxx).ge.wtlphdb ) goto 1200
       endif
c
c #   ket invalid in index1
c

      if (valid(2,1)) then
       indxx = yk(nlev)+1-ipth1(1)
       if (indxx.lt.1) then
          wtlphdk = wtlphdk +indxx -1
          indxx=1
       endif
      if (-index1(indxx).ge.wtlphdk .and.
     .    -index12(indxx).ge.wtlphdk ) goto 1200
      endif
c
c #   ket invalid in index2
c
      if (valid(2,2)) then
       indxx = yk(nlev)+1-ipth1(2)
       if (indxx.lt.1) then
          wtlphdk = wtlphdk +indxx -1
          indxx=1
       endif
       if (-index2(indxx).ge.wtlphdk .and.
     .     -index22(indxx).ge.wtlphdk ) goto 1200
       endif



ctm ************************************************************

c     # accumulate the appropriate number of products:
c
      imu = mu(nlev)
      if (numv.eq.1) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      if ( abs(v(1,nlev)) .le. small ) go to 1200
      elseif (numv.eq.2) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small      )go to 1200
      else
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      v(3,nlev) = v(3,clev)*tab(pt(bcase,kcase,db0(r,imu,3)+delb),bket)
      if(
     & abs(v(1,nlev)) .le. small .and.
     & abs(v(2,nlev)) .le. small .and.
     & abs(v(3,nlev)) .le. small      )go to 1200
      endif 
c
c     # passed all the tests, this is a
c     # valid segment extension, increment to the next level:
c
      clev = nlev
      go to 1100
c
      end
c
c******************************************************************
c
      subroutine fourd1 ( indsym1,conft1,index1,ci1,indxyz1,
     &                    ciyz1,indsym2,conft2,index2,ci2,indxyz2,
     &                    ciyz2,scr,scr_asym,tran,tran1)

c
c     calculation of all external elements of the one-particle
c     density matrix
c
       implicit none
c  ##  parameter & common block section
c
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
C
      INTEGER             CISTRT(2),   FIRST,       IPTH1,       IPTH2
      INTEGER             IWX,         JUMP,        MLP,         SEGSUM
      INTEGER             WTLPHD
      COMMON / DIAG   /   IPTH1,       IPTH2,       IWX,         CISTRT
      COMMON / DIAG   /   FIRST,       SEGSUM,      WTLPHD,      MLP
      COMMON / DIAG   /   JUMP
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      TOTVEC
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   TOTVEC,      CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
c
c  ##  integer section
c
      integer conft1(*),conft2(*)
      integer dsym
      INTEGER indsym1(*),index1(*),indxyz1(*),indsym2(*),index2(*),
     &        indxyz2(*), ij, ist, isym, ixx, ik, ixxx, i,imd
      integer jsym
      integer l1, len, lsym
      integer m1, m1lp, msym2,msym
      integer ni,nj,nmb, nmba, nmij, ny, nmb2,nmba2,ny2,nl,npa,
     &        npba,np, npb
      integer psym1,psym2,psyma,psymb
      integer strtwx, sy2, sym, strtwx1a,strtwx1b,strtwx2a,strtwx2b,
     &        sym2, sy
c
c  ##  real*8 section
c
      real*8 ci1(*),ci2(*),ciyz1(*),ciyz2(*)
      real*8 signfac,factor
      real*8 SCR(*),scr_asym(*)
      real*8 TRAN(*), TRAN1(*)
c
c  ##  logical section
c
      logical  apart,bpart

      real*8 dnrm2_wr, norm
      external dnrm2_wr
*@ifdef debug
*      integer ndim
*      parameter (ndim=100000)
*      real*8 fmd(ndim),trsh
*      parameter (trsh=1.0d-08)
*@endif
c
c-------------------------------------------------------------------
c
c    calculates all-external contributions of type
c    |
c  -------
c    |c    | \  l
c    |  c  --------
c    |  |      lamda(l)*lamda(i)=lamda(bra)*lamda(ket)
c    |  |
c  --------
c    \  |
c     \ | i
c      \|
c  --------
c       |
C               D(l,i) = ybra(l)*yket(i)+ybra(i)*yket(l)
c
c               For diagonal blocks just use lower triangular part
c               and add both terms
c               for offdiagonal blocks let l,i run over all symmetries
c               if lsym > isym  add the bra*ket part otherwise
c               add the ket*bra part to the same non-redundant D block
c
c
      dsym=symtab(ssym1,ssym2)
      if(first.gt.0)go to 300
c
c     y paths
c
      do 100 m1=1,pthy
          m1lp=pthz+m1
          nmba=indxyz1(m1lp)
          nmba2=indxyz2(m1lp)
c     write(6,30000) m1,m1lp,nmba,pthy,pthz
30000 format(1x,'m1,m1lp,nmba,pthy,pthz',/,10i4)
          if(nmba.lt.0 .or. nmba2.lt.0)go to 100
          sy=indsym1(nmba)
          sy2=indsym2(nmba2)
          sym=sym21(sy)
          sym2=sym21(sy2)
          ny=nbas(sym)
          ny2=nbas(sym2)
          if(ny.eq.0 .or. ny2.eq.0)go to 100
          nmb=conft1(nmba)
          nmb2=conft2(nmba2)
c
c  contributions to  D(i,l) , isym=lsym
c
          if (dsym.eq.1) then
          ist=strtbw(sym,1)+1
           ni=ny
          do 200 l1=1,ny
              len=ny-l1+1
c     write(6,30020)len,ist,ciyz(nmb+l1)
30020 format(' 200y',2i4,f10.6)
c
c  #  symmetric contribution
      call daxpy_wr(len,0.5d0*ciyz1(nmb+l1),ciyz2(nmb2+l1),1,scr(ist),1)
      call daxpy_wr(len,0.5d0*ciyz2(nmb2+l1),ciyz1(nmb+l1),1,scr(ist),1)
c  #  antisymmetric contribution
      call daxpy_wr(len,0.5d0*ciyz2(nmb2+l1),ciyz1(nmb+l1),1,
     & scr_asym(ist),1)
      call daxpy_wr(len,-0.5d0*ciyz1(nmb+l1),ciyz2(nmb2+l1),1,
     & scr_asym(ist),1)
*@ifdef debug
*      call wzero(ndim,fmd,1)
*      call daxpy(len,0.5d0*ciyz2(nmb2+l1),ciyz1(nmb+l1),1,
*     & fmd,1)
*      norm=dnrm2(ndim,fmd,1)
*      if (abs(norm).gt.trsh) write(*,*)'twoext case1a',norm
*C
*      call wzero(ndim,fmd,1)
*      call daxpy(len,-0.5d0*ciyz1(nmb+l1),ciyz2(nmb2+l1),1,
*     & fmd,1)
*      norm=dnrm2(ndim,fmd,1)
*      if (abs(norm).gt.trsh) write(*,*)'twoext case1b',norm
*@endif
          ist=ist+len
  200     continue
c     call printtrigmat(scr(strtbw(sym2,dsym)+1),ni,'Y0',1)
c
c     contributions to D(i,l) i <> l  part one
c
         elseif (sym2.gt.sym) then

         ist = strtbw(max(sym2,dsym),min(sym2,dsym))+1
         ni=nbas(sym2)
         nl=nbas(sym)
c        write(*,*) 'ist,sym2,sym,dsym=',ist,sym2,sym,dsym,
c    .      'ni,nl=',ni,nl
        do 201 l1=1,nl
             len=ni
c  #  symmetric contribution
       call daxpy_wr(len,0.5d0*ciyz1(nmb+l1),ciyz2(nmb2+1),1,scr(ist),1)
c      write(*,*)'3:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
c      Note: exchange term only
       call daxpy_wr(len,-0.5d0*ciyz1(nmb+l1),ciyz2(nmb2+1),1,
     &  scr_asym(ist),1)
*@ifdef debug
*      call wzero(ndim,fmd,1)
*      call daxpy(len,-0.5d0*ciyz1(nmb+l1),ciyz2(nmb2+1),1,
*     &  fmd,1)
*      norm=dnrm2(ndim,fmd,1)
*      if (abs(norm).gt.trsh) write(*,*)'twoext case2',norm
*@endif
             ist=ist+len
  201   continue
c        call printmatrix(scr(strtbw(sym2,dsym)+1),ni,nl,'Y1')
c
c     contributions to D(i,l) i <> l  part two
c
        elseif (sym.gt.sym2) then
         ist=strtbw(max(sym,dsym),min(sym,dsym))+1
         ni=nbas(sym)
         nl=nbas(sym2)
c        write(*,*) 'ist,sym2,sym,dsym=',ist,sym2,sym,dsym,
c    .      'ni,nl=',ni,nl
        do 202 l1=1,nl
            len=ni
c  #  symmetric contribution
       call daxpy_wr(len,0.5d0*ciyz2(nmb2+l1),ciyz1(nmb+1),1,scr(ist),1)
c      write(*,*)'4:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
c      Note: direct term only
       call daxpy_wr(len,0.5d0*ciyz2(nmb2+l1),ciyz1(nmb+1),1,
     &  scr_asym(ist),1)
*@ifdef debug
*      call wzero(ndim,fmd,1)
*      call daxpy(len,0.5d0*ciyz2(nmb2+l1),ciyz1(nmb+1),1,
*     &  fmd,1)
*      norm=dnrm2(ndim,fmd,1)
*      if (abs(norm).gt.trsh) write(*,*)'twoext case3',norm
*@endif
c        call writex(scr(ist),len)
          ist=ist+len
  202    continue
c        call printmatrix(scr(strtbw(sym2,dsym)+1),ni,nl,'Y2')
        else
         call bummer ('y part of fourd, should never occur',0,2)
        endif
c <---------
  100 continue
  300 continue
c
c     x and w paths
c
      do 400 m1=1,segsum
          nmba=index1(m1)
          nmba2=index2(m1)
          if(nmba.lt.0.or.nmba2.lt.0)go to 400
          nmb=conft1(nmba)
          nmb2=conft2(nmba2)
          nmb=nmb-cistrt(1)
          nmb2=nmb2-cistrt(2)
          sy=indsym1(nmba)
          sy2=indsym2(nmba2)
          sym=sym21(sy)
          sym2=sym21(sy2)
*@ifdef debug
*          if (m1.gt.1) then
*          write(*,*) 'xw walks, m1=',m1, 'scr(*)'
*          call writex(scr,25)
*          else
*          call writex(scr,25)
*          endif
*C         call wzero(nmotx,scr,1)
*@endif
c
c #  brasym*ketsym=dsym
c #  brasym=psym*lsym     with lsym*isym=dsym
c #  ketsym=isym*psym
c
c #  gives the number of possible i,l combinations
c #  from that determine i,p and l,p combinations
c #  in general with sym<>sym2 there are two possible
c #  i,l,p combinations
c #
c #  A: CI1(i,pa) CI2(l,pa)
c #  B: CI1(l,pb) CI2(i,pb)
c
          nmij= nmbpr((dsym))
          if(nmij.eq.0)go to 400
          do 500 ij=1,nmij
            isym=lmdb(dsym,ij)
            lsym=lmda(dsym,ij)
            psym1=symtab(sym,lsym)
            psym2=symtab(sym2,isym)
            if (psym1.ne.psym2)
     .         call bummer('psym1.ne.psym2 fatal ... ',0,2)
            psymb=psym1
            psym1=symtab(sym,isym)
            psym2=symtab(sym2,lsym)
            if (psym1.ne.psym2)
     .         call bummer('psym1.ne.psym2 fatal ... ',0,2)
            psyma=psym1
            ni=nbas(isym)
            nl=nbas(lsym)
            npa=nbas(psyma)
            npb=nbas(psymb)
*@ifdef debug
*            write(*,1005) isym,lsym,psyma,psymb,ni,nl,npa,npb
*1005   format('isym=',i2,' lsym=',i2,' psyma=',i2,' psymb=',i2
*     .       , 'ni=',i3,' nl=',i3,' npa=',i3,' npb=',i3)
*@endif
            if (ni*nl.eq.0) then
c             write(*,*) 'skipping pair  ni*nl=0'
              goto 500
            endif
c
c #  strtwx1a = start number of external block CI1(isym,psyma)
c #  strtwx1b = start number of external block CI1(lsym,psymb)
c #  strtwx2a = start number of external block CI2(lsym,psyma)
c #  strtwx2b = start number of external block CI2(isym,psymb)
c
c # iwx=1  x walk   iwx=2  w walk
c

            if(iwx.eq.1)then
               signfac=-1.0d0
               strtwx1a=1+nmb+strtbx(max(isym,psyma),sym)
               strtwx1b=1+nmb+strtbx(max(lsym,psymb),sym)
               strtwx2a=1+nmb2+strtbx(max(lsym,psyma),sym2)
               strtwx2b=1+nmb2+strtbx(max(isym,psymb),sym2)
               else
               signfac=1.0d0
               strtwx1a=1+nmb+strtbw(max(isym,psyma),sym)
               strtwx1b=1+nmb+strtbw(max(lsym,psymb),sym)
               strtwx2a=1+nmb2+strtbw(max(lsym,psyma),sym2)
               strtwx2b=1+nmb2+strtbw(max(isym,psymb),sym2)
            endif
*@ifdef debug
*        write(*,30011) strtwx1a,strtwx1b,strtwx2a,strtwx2b,iwx
*30011 format(' strtwx1=',2i6,' strtwx2=',2i6,'iwx=',i2)
*@endif

           if(psym1.eq.lsym .and. lsym.eq.isym )go to 600

c       |                       |
c       |                       |
c       |\                      c       | \     l                \  
c    +      p

c       |  \                      c       |  |                       |
c       |  |                       |
c       |  |                       |
c       \  |                       |\    l
c        \ |    i                  | c         \|                   
c    +     |  c          |                       |  |

c          |                       |  |
c          |                       |  |
c          \                       \  |
c           \    p                  \ |  i
c            \                       \|
c             |                       |
c             |                       |
c             |                       |
c
c          (I)                      (II)
c
c     D(i,l) = sum(p) C1(i,pa)*C2(pa,l) + C2(i,pb)*C1(pb,l)
c
c
        if (psyma.ne.isym .and. isym.eq.lsym) then
c
c  D(I,L) = ISYM=LSYM
c
              ist=strtbw(max(lsym,dsym),min(lsym,dsym))+1
*@ifdef debug
*              write(*,*) 'OP1a,isym,lsym.psym1,m1=',isym,lsym,psym1,m1
*     .          ,ist
*@endif
c   scr(i,j) = C1*C2(T) + C2*C1(T)
              if (npa.gt.0) then
              if (isym.gt.psyma) then
              call transp(npa,ni,ci1(strtwx1a),tran)
c  #  symmetric contribution
              call mtmls(scr(ist),tran,ci2(strtwx2a),ni,nl,npa,1,0.5d0)
c      write(*,*)'5:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=0.5d0 ! x - walk
              if (iwx .eq. 2) factor=0.5d0 ! w - walk
              call mtmls(scr_asym(ist),tran,ci2(strtwx2a),ni,nl,
     &         npa,1,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call mtmls(fmd,tran,ci2(strtwx2a),ni,nl,npa,1,factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case4',norm
*@endif
c
              call transp(npa,nl,ci2(strtwx2b),tran)
c  #  symmetric contribution
              call mtmls(scr(ist),tran,ci1(strtwx1b),ni,nl,npa,1,0.5d0)
c      write(*,*)'6:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=-0.5d0 ! x - walk
              if (iwx .eq. 2) factor=-0.5d0 ! w - walk
              call mtmls(scr_asym(ist),tran,ci1(strtwx1b),ni,nl,
     &         npa,1,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call mtmls(fmd,tran,ci1(strtwx1b),ni,nl,
*     &         npa,1,factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case5',norm
*@endif
              else
              call transp(ni,npa,ci1(strtwx1a),tran)
c  #  symmetric contribution
              call mtmls(scr(ist),ci2(strtwx2a),tran,nl,ni,npa,1,0.5d0)
c      write(*,*)'7:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=-0.5d0 ! x - walk chaged
              if (iwx .eq. 2) factor=-0.5d0 ! w - walk
              call mtmls(scr_asym(ist),ci2(strtwx2a),tran,nl,ni,
     &         npa,1,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call mtmls(fmd,ci2(strtwx2a),tran,nl,ni,
*     &         npa,1,factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case6',norm
*@endif
c
              call transp(nl,npa,ci2(strtwx2b),tran)
c  #  symmetric contribution
              call mtmls(scr(ist),ci1(strtwx1b),tran,nl,ni,npa,1,0.5d0)
c      write(*,*)'8:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=0.5d0 ! x - walk changed
              if (iwx .eq. 2) factor=0.5d0 ! w - walk changed
              call mtmls(scr_asym(ist),ci1(strtwx1b),tran,nl,ni,
     &         npa,1,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call mtmls(fmd,ci1(strtwx1b),tran,nl,ni,
*     &         npa,1,factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case7',norm
*@endif
              endif


              endif

        elseif (psyma.eq.lsym .or. psymb.eq.lsym ) then
              ist=strtbw(max(isym,dsym),min(isym,dsym))+1
*@ifdef debug
*              write(*,*) 'OP1c,isym,lsym,psyma,psymb=',
*     .         isym,lsym,psyma,psymb
*@endif
c
c  1. Fall i auf CI1 und l auf CI2  psyma
c  2. Fall i auf CI2 und l auf CI1  psymb
c
                if (psyma.eq.lsym) then

                  if (iwx.eq.1) then
c x
c Teil 1 psyma = lsym
                 call expda(nl,ci2(strtwx2a),tran)
c  #  symmetric contribution
                 call gmtxm(ci1(strtwx1a),ni,tran,nl,scr(ist),nl,0.5d0)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=0.5d0 ! x - walk
              if (iwx .eq. 2) factor=0.5d0 ! w - walk
              call gmtxm(ci1(strtwx1a),ni,tran,nl,scr_asym(ist),
     &         nl,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call gmtxm(ci1(strtwx1a),ni,tran,nl,fmd,nl,factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case8',norm
*@endif
*@ifdef debug
*                 call printmatrix(tran,nl,nl,'OPC-A1-X tran')
*                 call printmatrix(ci1(strtwx1a),ni,nl,'OPC-A2-X ci1')
*                 call printmatrix(scr(ist),ni,nl,'OPC-A4-X scr')
*@endif
c Teil 2 psymb = isym
                 call expda (ni,ci2(strtwx2b),tran)
c  #  symmetric contribution
                 call gmxmt(tran,ni,ci1(strtwx1b),ni,scr(ist),nl,0.5d0)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=-0.5d0 ! x - walk
              if (iwx .eq. 2) factor=-0.5d0 ! w - walk
              call gmxmt(tran,ni,ci1(strtwx1b),ni,scr_asym(ist),
     &         nl,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call gmxmt(tran,ni,ci1(strtwx1b),ni,fmd,nl,factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case9',norm
*@endif
*@ifdef debug
*                call printmatrix(tran,ni,ni,'OPC-A4-X tran')
*                call printmatrix(ci1(strtwx1b),ni,nl,'OPC-A5-X ci1')
*                 call printmatrix(scr(ist),ni,nl,'OPC-A6-X scr')
*@endif
                 else
c Teil 1 psyma = lsym
                 call expds(nl,ci2(strtwx2a),tran)
c  #  symmetric contribution
                 call gmtxm(ci1(strtwx1a),ni,tran,nl,scr(ist),nl,0.5d0)
c      write(*,*)'11:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
                 if (iwx .eq. 1) factor=0.5d0 ! x - walk
                 if (iwx .eq. 2) factor=0.5d0 ! w - walk
                 call gmtxm(ci1(strtwx1a),ni,tran,nl,scr_asym(ist),
     &            nl,factor)
*@ifdef debug
*                 call wzero(ndim,fmd,1)
*                 call gmtxm(ci1(strtwx1a),ni,tran,nl,fmd,nl,factor)
*                 norm=dnrm2(ndim,fmd,1)
*                 if (abs(norm).gt.trsh) write(*,*)'twoext case10',norm
*                 call printmatrix(tran,nl,nl,'OPC-A1 tran')
*                 call printmatrix(ci1(strtwx1a),ni,nl,'OPC-A2 ci1')
*                 call printmatrix(scr(ist),ni,nl,'OPC-A3 scr')
*@endif
c Teil 2 psymb = isym
                 call expds (ni,ci2(strtwx2b),tran)
c  #  symmetric contribution
                 call gmxmt(tran,ni,ci1(strtwx1b),ni,scr(ist),nl,0.5d0)
c      write(*,*)'12:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
                 if (iwx .eq. 1) factor=-0.5d0 ! x - walk
                 if (iwx .eq. 2) factor=-0.5d0 ! w - walk
                 call gmxmt(tran,ni,ci1(strtwx1b),ni,scr_asym(ist),
     &            nl,factor)
*@ifdef debug
*                 call wzero(ndim,fmd,1)
*                 call gmxmt(tran,ni,ci1(strtwx1b),ni,fmd,nl,factor)
*                 norm=dnrm2(ndim,fmd,1)
*                 if (abs(norm).gt.trsh) write(*,*)'twoext case11',norm
*                 call printmatrix(tran,ni,ni,'OPC-A4 tran')
*                 call printmatrix(ci1(strtwx1b),ni,nl,'OPC-A5 ci1')
*                 call printmatrix(scr(ist),ni,nl,'OPC-A6 scr')
*@endif
                 endif
              endif

                if (psyma.eq.isym) then

                  if (iwx.eq.1) then
c x
c Teil 1 psyma = isym
                 call expda(ni,ci1(strtwx1a),tran)
c  #  symmetric contribution
                 call gmxmt(tran,ni,ci2(strtwx2a),ni,scr(ist),nl,0.5d0)
c      write(*,*)'13:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
                 if (iwx .eq. 1) factor=0.5d0 ! x - walk
                 if (iwx .eq. 2) factor=0.5d0 ! w - walk
                 call gmxmt(tran,ni,ci2(strtwx2a),ni,scr_asym(ist),
     &            nl,factor)
*@ifdef debug
*                 call wzero(ndim,fmd,1)
*                 call gmxmt(tran,ni,ci2(strtwx2a),ni,fmd,nl,factor)
*                 norm=dnrm2(ndim,fmd,1)
*                 if (abs(norm).gt.trsh) write(*,*)'twoext case12',norm
*                 call printmatrix(scr(ist),ni,nl,'OPC-B1xscr')
*@endif
c Teil 2 psymb = lsym
                 call expda(nl,ci1(strtwx1b),tran)
c  #  symmetric contribution
                 call gmtxm(ci2(strtwx2b),ni,tran,nl,scr(ist),nl,0.5d0)
c      write(*,*)'14:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
                 if (iwx .eq. 1) factor=-0.5d0 ! x - walk
                 if (iwx .eq. 2) factor=-0.5d0 ! w - walk
                 call gmtxm(ci2(strtwx2b),ni,tran,nl,scr_asym(ist),
     &            nl,factor)
*@ifdef debug
*                 call wzero(ndim,fmd,1)
*                 call gmtxm(ci2(strtwx2b),ni,tran,nl,fmd,
*     &            nl,factor)
*                 call mtmls(fmd,tran,ci2(strtwx2a),ni,nl,npa,1,factor)
*                 norm=dnrm2(ndim,fmd,1)
*                 if (abs(norm).gt.trsh) write(*,*)'twoext case13',norm
*               call printmatrix(scr(ist),ni,nl,'OPC-B2xscr')
*@endif
                 else
c Teil 1 psyma = isym
                 call expds(ni,ci1(strtwx1a),tran)
c  #  symmetric contribution
               call gmxmt(tran,ni,ci2(strtwx2a),ni,scr(ist),nl,0.5d0)
c      write(*,*)'15:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=0.5d0 ! x - walk
               if (iwx .eq. 2) factor=0.5d0 ! w - walk
               call gmxmt(tran,ni,ci2(strtwx2a),ni,scr_asym(ist),
     &          nl,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call gmxmt(tran,ni,ci2(strtwx2a),ni,fmd,nl,factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case14',norm
*               call printmatrix(tran,ni,ni,'OPC - B1 tran')
*C    tran ni Zeilen, ni spalten
*C    ci2 nl Zeilen, ni spalten
*               call printmatrix(ci2(strtwx2a),nl,ni,'OPC - B2 ci2')
*               call printmatrix(scr(ist),ni,nl,'OPC-B3 scr')
*@endif
c Teil 2 psymb = lsym
               call expds (nl,ci1(strtwx1b),tran)
c  #  symmetric contribution
              call gmtxm(ci2(strtwx2b),ni,tran,nl,scr(ist),nl,0.5d0)
c      write(*,*)'16:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=-0.5d0 ! x - walk
              if (iwx .eq. 2) factor=-0.5d0 ! w - walk
              call gmtxm(ci2(strtwx2b),ni,tran,nl,scr_asym(ist),
     &         nl,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call gmtxm(ci2(strtwx2b),ni,tran,nl,fmd,nl,factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case15',norm
*              call printmatrix(tran,nl,nl,'OPC - B4 tran')
*              call printmatrix(ci2(strtwx2b),nl,ni,'OPC - B5 ci2')
*              call printmatrix(scr(ist),ni,nl,'OPC-B6 scr')
*@endif
                 endif
              endif

        elseif (psyma.ne.isym .and. psyma.ne.lsym) then
              ist=strtbw(max(isym,dsym),min(isym,dsym))+1
*@ifdef debug
*              write(*,*) 'OP1d,isym,lsym,psyma,psymb=',
*     .         isym,lsym,psyma,psymb
*@endif
c
c  1. Fall i auf CI1 und l auf CI2  psyma
c  2. Fall i auf CI2 und l auf CI1  psymb
c

              if (npa.gt.0 ) then
              if (psyma.gt.isym) then
c  #  symmetric contribution
c  diagonal matrices are stored upper triangular packed
c  off-diagonal matrices are stored unique lower block
c  hence for a consistent treatment we need to reverse the
c  sign
       call gmxmt(ci1(strtwx1a),ni,ci2(strtwx2a),npa,scr(ist),
     .   nl,-0.5d0*signfac)
c      write(*,*)'17:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
              if (iwx .eq. 1) factor=0.5d0 ! x - walk
              if (iwx .eq. 2) factor=0.5d0 ! w - walk
              call gmxmt(ci1(strtwx1a),ni,ci2(strtwx2a),npa,
     &         scr_asym(ist),nl,factor)
*@ifdef debug
*              call wzero(ndim,fmd,1)
*              call gmxmt(ci1(strtwx1a),ni,ci2(strtwx2a),npa,fmd,nl,
*     &factor)
*              norm=dnrm2(ndim,fmd,1)
*              if (abs(norm).gt.trsh) write(*,*)'twoext case16',norm
*              call printmatrix(ci1(strtwx1a),ni,npa,'OPD-1,ci1')
*              call printmatrix(ci2(strtwx2a),nl,npa,'OPD-1,ci2')
*              call printmatrix(scr(ist),ni,nl,'OPD-1 gmxm')
*@endif
              elseif (psyma.gt.lsym) then
c
c  #  symmetric contribution
               call gmtxmt(ci1(strtwx1a),ni,ci2(strtwx2a),npa,scr(ist),
     .          nl, -0.5d0)
c      write(*,*)'18:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=-0.5d0 ! x - walk  changed
               if (iwx .eq. 2) factor=0.5d0 ! w - walk
               call gmtxmt(ci1(strtwx1a),ni,ci2(strtwx2a),npa,
     &          scr_asym(ist),nl, factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call gmtxmt(ci1(strtwx1a),ni,ci2(strtwx2a),npa,fmd,nl,
*     &          factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case17',norm
*               call printmatrix(tran,ni,npa,'OPD-2,ci1')
*               call printmatrix(ci2(strtwx2a),nl,npa,'OPD-2,ci2')
*               call printmatrix(scr(ist),ni,nl,'OPD-2 gmxm')
*@endif
              else
c
c  eigentlich gmxmt
c  #  symmetric contribution
               call gmtxm(ci1(strtwx1a),ni,ci2(strtwx2a),npa,scr(ist),
     .          nl,-0.5d0*signfac)
c      write(*,*)'19:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=0.5d0 ! x - walk
               if (iwx .eq. 2) factor=0.5d0 ! w - walk
               call gmtxm(ci1(strtwx1a),ni,ci2(strtwx2a),npa,
     &          scr_asym(ist),nl,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call gmtxm(ci1(strtwx1a),ni,ci2(strtwx2a),npa,fmd,nl,
*     &          factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case18',norm
*               call printmatrix(ci2(strtwx2a),npa,nl,'OPD-3,ci2')
*               call printmatrix(tran,ni,npa,'OPD-3,ci1')
*               call printmatrix(scr(ist),ni,nl,'OPD-3 gmxm')
*@endif
              endif
              else
              endif

              if (nbas(psymb).gt.0) then
              if (psymb.gt.isym) then
c  #  symmetric contribution
               call gmxmt(ci2(strtwx2b),
     .           ni,ci1(strtwx1b),npb,scr(ist),nl,-0.5d0*signfac)
c      write(*,*)'20:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=-0.5d0 ! x - walk
               if (iwx .eq. 2) factor=-0.5d0 ! w - walk
               call gmxmt(ci2(strtwx2b),
     .          ni,ci1(strtwx1b),npb,scr_asym(ist),nl,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call gmxmt(ci2(strtwx2b),ni,ci1(strtwx1b),npb,fmd,nl,
*     &          factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case19',norm
*               call printmatrix(tran,ni,npb,'OPD-4, tran')
*               call printmatrix(ci1(strtwx1b),npb,nl,'OPD-5,ci1 ')
*               call printmatrix(scr(ist),ni,nl,'OPD-6 gmxm')
*@endif
              elseif (psymb.gt.lsym) then
c  #  symmetric contribution
               call gmtxmt(ci2(strtwx2b),ni,ci1(strtwx1b),npb,scr(ist),
     .          nl,-0.5d0)
c      write(*,*)'21:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=0.5d0 ! x - walk  changed
               if (iwx .eq. 2) factor=-0.5d0 ! w - walk
               call gmtxmt(ci2(strtwx2b),ni,ci1(strtwx1b),npb,
     &          scr_asym(ist),nl,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call gmtxmt(ci2(strtwx2b),ni,ci1(strtwx1b),npb,fmd,nl,
*     &          factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case20',norm
*               call printmatrix(scr(ist),ni,nl,'OPD-7 gmxm')
*@endif
              else
c  #  symmetric contribution
               call gmtxm(ci2(strtwx2b),ni,ci1(strtwx1b),npb,scr(ist),
     .          nl,-0.5d0*signfac)
c      write(*,*)'22:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=-0.5d0 ! x - walk
               if (iwx .eq. 2) factor=-0.5d0 ! w - walk
               call gmtxm(ci2(strtwx2b),ni,ci1(strtwx1b),npb,
     &          scr_asym(ist),nl,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call gmtxm(ci2(strtwx2b),ni,ci1(strtwx1b),npb,fmd,nl,
*     &          factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case21',norm
*               call printmatrix(ci2(strtwx2b),npb,ni,'OPD-8,ci2')
*               call printmatrix(tran,nl,npb,'OPD-8,ci1')
*               call printmatrix(scr(ist),ni,nl,'OPD-8 gmxm')
*@endif
      endif
           else
           endif
      else
         call bummer('xw part of fourd1 - should never occur!',0,2)
      endif
           go to 500
600         continue
c
c     wsym or xsym=1 isym=jsym
c

c   |
c   |c   | \    l
c   |  c   |  |
c   |  |
c   \  c    \  \   p
c     \  c     |  |
c     |  |
c     |  |
c     \  |
c      \ |   i
c       \|
c        |
c
           ist=strtbw(max(isym,dsym),min(isym,dsym))+1
*@ifdef debug
*              write(*,*) 'OP2 '
*@endif
           if(iwx.eq.1)then
c expand lower triangle antisymmetric
               call expda(ni,ci1(strtwx1a),tran)
c expand lower triangle antisymmetric transposed
               call expda1(ni,ci2(strtwx2a),tran1)
c  #  symmetric contribution
               call mtmls(scr(ist),tran,tran1,ni,ni,ni,1,0.5d0)
c      write(*,*)'23:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=0.5d0 ! x - walk
               if (iwx .eq. 2) factor=0.5d0 ! w - walk
               call mtmls(scr_asym(ist),tran,tran1,ni,ni,ni,1,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call mtmls(fmd,tran,tran1,ni,ni,ni,1,factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case22',norm
*               call printmatrix(tran,nl,nl,'OP2, tran 1st')
*               call printmatrix(tran1,ni,npa,'OP2, tran1 1st')
*               call printtrigmat(scr(ist),ni,'OP scr 1st',1)
*@endif
c
               call expda(ni,ci2(strtwx2b),tran)
c expand lower triangle antisymmetric transposed
               call expda1(ni,ci1(strtwx1b),tran1)
c  #  symmetric contribution
               call mtmls(scr(ist),tran,tran1,ni,ni,ni,1,0.5d0)
c      write(*,*)'24:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=-0.5d0 ! x - walk
               if (iwx .eq. 2) factor=-0.5d0 ! w - walk
               call mtmls(scr_asym(ist),tran,tran1,ni,ni,ni,1,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call mtmls(fmd,tran,tran1,ni,ni,ni,1,factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case23',norm
*               call printmatrix(tran,nl,nl,'OP2, tran 2st')
*               call printmatrix(tran1,ni,npa,'OP2, tran1 2st')
*               call printtrigmat(scr(ist),ni,'OP scr 2st',1)
*@endif
           else
c expand lower triangle symmetric
               call expds(ni,ci1(strtwx1a),tran)
               call expds(ni,ci2(strtwx2a),tran1)
c  #  symmetric contribution
               call mtmls(scr(ist),tran,tran1,ni,ni,ni,1,0.5d0)
c      write(*,*)'25:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=0.5d0 ! x - walk
               if (iwx .eq. 2) factor=0.5d0 ! w - walk
               call mtmls(scr_asym(ist),tran,tran1,ni,ni,ni,1,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call mtmls(fmd,tran,tran1,ni,ni,ni,1,factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case24',norm
*               call printmatrix(tran,nl,nl,'OP2b tran 1st')
*               call printmatrix(tran1,ni,npa,'OPb, tran1 1st')
*               call printtrigmat(scr(ist),ni,'OPb scr 1st',1)
*@endif
               call expds(ni,ci2(strtwx2b),tran)
               call expds(ni,ci1(strtwx1b),tran1)
c  #  symmetric contribution
               call mtmls(scr(ist),tran,tran1,ni,ni,ni,1,0.5d0)
c      write(*,*)'26:scr=',(scr(imd),imd=1,3)
c  #  antisymmetric contribution
               if (iwx .eq. 1) factor=-0.5d0 ! x - walk
               if (iwx .eq. 2) factor=-0.5d0 ! w - walk
               call mtmls(scr_asym(ist),tran,tran1,ni,ni,ni,1,factor)
*@ifdef debug
*               call wzero(ndim,fmd,1)
*               call mtmls(fmd,tran,tran1,ni,ni,ni,1,factor)
*               norm=dnrm2(ndim,fmd,1)
*               if (abs(norm).gt.trsh) write(*,*)'twoext case25',norm
*               call printmatrix(tran,nl,nl,'OP2b tran 2st')
*               call printmatrix(tran1,ni,npa,'OPb, tran1 2st')
*               call printtrigmat(scr(ist),ni,'OPb scr 2st',1)
*@endif

           endif
500     continue
400     continue
        return
        end
c
c
c********************************************************************
c
      subroutine calc_lvalue (nsym,core,lcore,iprint)
c
       implicit none
c  ##  parameter & common block section
c
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer  faterr
      parameter (faterr=2)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer moints,geom,nlist
      equivalence (moints ,nunits(29))
      equivalence (geom ,nunits(26))
      equivalence (nunits(1),nlist)
c
      character*60 fmoints
      character*80 fmt
      equivalence (fmoints,fname(29))
      character*60 fgeom
      equivalence (fgeom,fname(26))
c
      integer btypmx
      parameter (btypmx=34)
c

      integer ntitmx,ninfomx,nengmx,maxsym
      parameter (ntitmx=20,ninfomx=10,nengmx=10,maxsym=8)

      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      integer mxinor
      parameter (mxinor=128)


      real*8 one,three,eight,small,planck,clight,bohr,
     & echarg,ev,debye,cm_1
      parameter ( one=1.0D+00,
     *           three=3.0D+00, eight=8.0D+00, small=1.0D-08,
     *           planck=6.626176D-27, clight=2.9979925D+10,
     *           bohr=5.291771D-09, echarg=4.803242D-10,
     *           ev=27.212D+00, debye=2.541766D+00,
     &           cm_1=219474.7d+00)
C
      INTEGER             BLXT(MAXSYM),             N2OFFR(MAXSYM)
      INTEGER             N2OFFS(MAXSYM),           N2ORBT
      INTEGER             NOFFR(MAXSYM),            NOFFS(MAXSYM)
      INTEGER             NSGES(MAXSYM), nsdim(maxsym)
      integer    dkntin (maxsym*(maxsym+1)/2)
      integer    densoff(maxsym*(maxsym+1)/2)
      integer    dsize
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   dsize,densoff,dkntin
C
      INTEGER             EQVSYM(MAXSYM),           IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(MAXSYM),           NFRC(MAXSYM)
      INTEGER             NFRV(MAXSYM),             NMUVAL
      INTEGER             NOCC(MAXSYM),             NORBSM(MAXSYM)
      INTEGER             NOXT(MAXSYM)
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
      integer  ncsf1,ncsf2,refvec1,refvec2
      real*8  eci1,eci2,a4den1,a4den2
      common   /civoutinfo/ ncsf1,ncsf2,refvec1,refvec2,
     .                      eci1,eci2,a4den1,a4den2
C
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
c  ##  integer section
c
      integer aopt
      integer btypes(0:btypmx)
      integer itypea,ios,icnt3,iprint,itype,ierr,ibmpt,iao,imo,isym,
     &        ijsym,itbmx,i,infoao(ninfomx),ietype(nengmx),imtype,imd
      integer jbmpt,jao,jmo,jsym,j
      integer filerr
      integer locbuf,locval,loclab,locdip,locmo,locscrt,locscrb,
     .        locdenao,lfree,ldens,lasta,lastb,last,lsym,lcore,
     &        l1rcao,l2rcao,ll
      integer kntao(maxsym*(maxsym+1)/2),kl
      integer mopt,nmj,map,mapin(nmotx)
      integer nden,nmo,nscrt,nscrb,nskip,nrec,ntitle,nsymmo,n1mxao,
     &        nbfpsy2(maxsym),nsym,nmopsy(maxsym),ntitleao,nsymao,
     &        nbftao,ninfoao,nenrgyao,nmap,nbi,nmi,nbj,ntot,n2mxao,
     &        nbpsy(maxsym),nmax,nbmskp(maxsym)
      integer offsetl(maxsym),off,offx,offy,offz
      integer prnopt
      integer symoff(maxsym*(maxsym+1)/2),syserr
c
c  ##  real*8 section
c
      real*8 a
      real*8 b
      real*8 core(lcore),charge
      real*8 diplen, del_au,del_ev
      real*8 erg,energy(nengmx)
      real*8 fcore2(10),f,f2,fact,freq,freq2
      real*8 omega
      real*8 pos(3),pi
      real*8 scratch(nmotx*nmotx)
      real*8 x, xcore, xnuc
      real*8 y, ycore, ynuc
      real*8 z, zcore, znuc
c
c  ##  character section
c
      character*8 symb(nmotx),chrtyp
      character*8 aolab(nmotx),molab(nmotx)
      character*4 slabel(maxsym)
      character*60 title(ntitmx)
c
c  ##  external section
c
      real*8 dasum_wr,ddot_wr,sifsce
      integer atebyt,forbyt
      external dasum_wr,ddot_wr,sifsce,atebyt,forbyt

      integer nndxf
      nndxf(i)= i*(i-1)/2
c
c-----------------------------------------------------------------------
c
c       loc1 =  buff = locbuf
c       loc2 =  val  = locval
c       loc3 =  lab  = loclab
c       loc4 = x,y,z = locdip
c       loc5 = free  = lfree
c
c open files

       open(unit=moints,file=fmoints,form='unformatted',status='old',
     .   iostat=ios)
       if (ios.ne.0)
     .  call bummer ('cannot open unit ',moints,2)



c read header information

      call sifrh1(
     & moints,   ntitleao,    nsymao,   nbftao,
     & ninfoao,  nenrgyao,    nmap,     ierr)

      if ( ierr .ne. 0 ) then
         call bummer('clcmom: from sifrh1, ierr=',ierr,faterr)
      elseif ( ntitleao .gt. ntitmx ) then
         call bummer('clcmom: from sifrh1, ntitao=',ntitleao,faterr)
      elseif ( nbftao .gt. nmotx ) then
         call bummer('clcmom: from sifrh1, nbft=',nbftao,faterr)
      elseif ( ninfoao .gt. ninfomx ) then
         call bummer('clcmom: from sifrh1, ninfao=',ninfoao,faterr)
      elseif ( nenrgyao .gt. nengmx ) then
         call bummer('clcmom: from sifrh1, nenrgy=',nenrgyao,faterr)
      endif

c  ignore map vectors

       nmap=0

      call sifrh2 (
     & moints,    ntitleao,    nsymao,    nbftao,
     & ninfoao,   nenrgyao,    nmap,      title,
     & nbpsy,     slabel,      infoao,    aolab,
     & ietype,    energy,      imtype,    map,
     & ierr)

      if ( ierr .ne. 0 ) then
         call bummer('clcmom: from sifrh2, ierr=',ierr,faterr)
      endif

         icnt3 = 0
         do i=1,nsymao
          offsetl(i)=icnt3
         icnt3=icnt3+nsdim(i)
         do kl=1,nsdim(i)
         write(molab(offsetl(i)+kl),'(a2,i3,a3)')
     .        'MO',kl,slabel(i)(2:4)
         enddo
         enddo
c
       if (iprint.eq.1) then
       write(nlist,*) '-------------- MO integral information ',
     .                '--------------'
       write(nlist,6100)'input file header information:'
c      call wtitle( nlist, ittl0, ntitxx, ntitle, ntitmx, title )
      write(nlist,6100)'input energy(*) values:'
      call sifpre( nlist, nenrgyao, energy, ietype )
      write(nlist,6200)'total ao core energy =',
     & sifsce( nenrgyao, energy, ietype )
      write(nlist,6040)nsymao,nbftao
6040  format(/' nsym =',i2,' nmot=',i4/)
6050  format(/a11,3x,8i4/)
6051  format(/a,3x,i4,a,i4/)
6060  format(/a11,3x,8a4/)
6100  format(/a11,3x,8i6/)
6200  format(/a22,3x,f12.7/)
      write(nlist,6050)'symmetry  =',(i,i=1,nsymao)
      write(nlist,6060)'slabel(*) =',(slabel(i),i=1,nsymao)
      write(nlist,6050)'nmpsy(*)  =',(nbpsy(i),i=1,nsymao)
      write(nlist,6100)'infomo(*) =',(infoao(i),i=1,ninfoao)
      endif

c     # extract some dimensioning information from infoao(*).
      l1rcao = infoao(2)
      n1mxao = infoao(3)
      l2rcao = infoao(4)
      n2mxao = infoao(5)

      if (nsymao.ne.nsym)
     . call bummer('clcmom: nsymao.ne.nsym ',nsymao,faterr)

      ntot=0
      do i=1,nsym
       ntot=ntot+nsdim(i)
      enddo

      if (ntot.ne.nbftao)
     . call bummer('clcmom: nmot.ne.ntot ',nbftao,faterr)

       do i=1,nsymao
        if (nbpsy(i).ne.nsdim(i))
     .   call bummer('clcmom: nmpsy(i).ne.nsdim(i),i=',i,faterr)
       enddo



c     set addresses
c
c
c mo coefficient offsets (quadratic)
c
      nmax=nsdim(1)
      nbmskp(1)=0
      nmo = nsdim(1)*nsdim(1)
      do i= 2,nsym
        nmax = max(nmax,nsdim(i))
        nbmskp(i)=nbmskp(i-1)+ nsdim(i-1)*nsdim(i-1)
        nmo = nmo + nsdim(i)*nsdim(i)
      enddo
c
c ao integral maximum size
c
      nskip = (nbftao*(nbftao+1))/2
      nscrt = nmax*nmax
      nscrb = nmax*nmax

c     check for correct ao integral file
      ldens  =1
      locbuf =1+dsize
      locval =locbuf + atebyt(l1rcao)
      loclab =locval  + atebyt(n1mxao)
      locdip =loclab  + forbyt(2*n1mxao)
c korr
      lfree  =locdip  + 3* atebyt(nskip)

c initialize denao and dip
      call wzero(3*nskip,core(locdip),1)

c print address array

       write(6,5000) ldens,locbuf,locval,loclab,locdip,lfree
5000   format('MO density     =',i6/,
     .        'IO buffer      =',i6/,
     .        'VAL buffer     =',i6/,
     .        'LAB buffer     =',i6/,
     .        'DIP integrals  =',i6/
     .        'top            =',i6/)

       itypea=2
       itbmx = 8 ! maximal btype value
       call izero_wr(btypmx+1,btypes,1)
       btypes(6)= 1
       btypes(7)= 2
       btypes(8)= 3

       do i=1,nbftao
       mapin(i)=i
       enddo

       i=0
       do isym=1,nsymao
        do jsym=1,isym-1
          ijsym=nndxf(isym)+jsym
          symoff(ijsym)=i
          i=i+nbpsy(isym)*nbpsy(jsym)
        enddo
          ijsym=nndxf(isym)+isym
          symoff(ijsym)=i
          i=i+nbpsy(isym)*(nbpsy(isym)+1)/2
       enddo



       call sifr1n(
     & moints,     infoao,         itypea,       itbmx,
     & btypes,     core(locbuf),   core(locval), core(loclab),
     & nsym,       nbpsy,          symoff,       mapin,
     & nskip,      core(locdip-1), fcore2,       symb,
     & kntao,      lasta,          lastb,        last,
     & nrec,       ierr)

        if (ierr.ne.0) then
          if (ierr.eq.-4)
     .    call bummer ("Lr integrals are missing on moints",0,2)
          call bummer ("error occurred reading moints ierr=",ierr,2)
        endif
        close(moints)

c
c  print mo integrals
c
         if (lvlprt.gt.3) then
         do itype=0,itbmx
        call siftyp(itypea,itype,chrtyp)
         do isym=1,nsym
          do jsym=1,isym
         ijsym = nndxf(isym) + jsym
         if (kntao(ijsym).gt.0) then
        if (jsym.ne.isym .and.
     .      (dasum_wr(nbpsy(isym)*nbpsy(jsym),
     .       core(locdip+nskip*itype+symoff(ijsym)-1),1)
     .         .gt.1.0d-9)) then
        call prblkc('MO integrals  ' // chrtyp,
     .     core(locdip+nskip*itype+symoff(ijsym)-1),nbpsy(isym),
     .       nbpsy(isym),
     .     nbpsy(jsym),molab(offsetl(isym)+1),molab(offsetl(jsym)+1),
     .     1,6)
        elseif (jsym.eq.isym .and.
     .       (dasum_wr(nbpsy(isym)*(nbpsy(isym)+1)/2,
     .       core(locdip+nskip*itype+symoff(ijsym)-1),1)
     .        .gt.1.0d-9)) then
        call plblkc('MO integrals  '//chrtyp,
     .     core(locdip+nskip*itype+symoff(ijsym)-1),nbpsy(isym),
     .     molab(offsetl(isym)+1),1,6)
        endif
        endif
          enddo
         enddo
         enddo
         endif

c check data

*@ifdef debug
*        do isym=1,nsym
*          write(*,*) 'symoff(*)=',(symoff(nndxf(isym)+i),i=1,isym)
*        enddo
*        do isym=1,nsym
*          write(*,*) 'densoff(*)=',(densoff(nndxf(isym)+i),i=1,isym)
*        enddo
**
*        do isym=1,nsym
*         write(*,*) 'dkntin(*)=',(dkntin(nndxf(isym)+i),i=1,isym)
*        enddo
*        do isym=1,nsym
*         write(*,*) 'kntao(*)=',(kntao(nndxf(isym)+i),i=1,isym)
*        enddo
*@endif


          x=0.0d0
          y=0.0d0
          z=0.0d0


         do isym=1,nsym
          do jsym=1,isym
         ll=nbpsy(isym)*nbpsy(jsym)
         ijsym = nndxf(isym) + jsym
c
         if (jsym.ne.isym) then
c
            if ((dkntin(ijsym).ne.0) .and. (kntao(ijsym).gt.0)) then
             x=x+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &           core(locdip+symoff(ijsym)-1),1)
             y=y+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &           core(locdip+nskip+symoff(ijsym)-1),1)
             z=z+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &           core(locdip+2*nskip+symoff(ijsym)-1),1)
*@ifdef debug
*           write(*,*) 'contributions from isym=',isym,'jsym=',jsym
*           write(*,*) 'x,y,z=',x,y,z
*@endif
            endif ! end of: if ((dkntin(ijsym)....
c
                 else
c
            if ((dkntin(ijsym).ne.0) .and. (kntao(ijsym).gt.0)) then
             ll=nbpsy(isym)*(nbpsy(isym)+1)/2
             x=x+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &           core(locdip+symoff(ijsym)-1),1)
             y=y+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &           core(locdip+nskip+symoff(ijsym)-1),1)
             z=z+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &           core(locdip+nskip+nskip+symoff(ijsym)-1),1)
             ll=0
             do  i=1,nbpsy(isym)
             ll=ll+i
             x=x-core(ldens+densoff(ijsym)-2+ll)
     .           *core(locdip+symoff(ijsym)-2+ll)
             y=y-core(ldens+densoff(ijsym)-2+ll)
     .           *core(locdip+nskip+symoff(ijsym)-2+ll)
             z=z-core(ldens+densoff(ijsym)-2+ll)
     .           *core(locdip+2*nskip+symoff(ijsym)-2+ll)
             enddo ! end of: do  i=1,nbpsy(isym)
            endif ! end of: if ((dkntin(ijsym).ne.....
*@ifdef debug
*        write(*,*) 'contributions from isym=',isym,'jsym=',jsym
*        write(*,*) 'x,y,z=',x,y,z
*@endif
         endif ! end of: if (jsym.ne.isym) then
c
       enddo ! end of: do jsym=1,isym
      enddo ! do isym=1,nsym

c scale expectation values and print results

      if (iprint.eq.1) then
      write(nlist,'(//,80("*"),///,12x,"***   FINAL RESULTS   ***",/)')
      else
      write(nlist,
     & '(//,80("*"),///,10x,"*** intermediary results ***",/)')
      endif
      write(nlist,'(//,26x,"State No. 1",12x,"State No. 2")')

      if (method.ne.0) write(nlist,'(//,26x,"(ref. state)")')

      write(nlist,'(/,3x,"Space symmetry:",11x,a4,19x,a4))')
     & slabel(ssym1),slabel(ssym2)
      write(nlist,'(3x,"No. of CSFs:",10x,i8,15x,i8)') ncsf1,ncsf2
      write(nlist,'(3x,"State energies:",4x,f15.8,8x,f15.8,2x,"a.u.")')
     & eci1,eci2
      del_au = eci2-eci1
c    #   1 a.u. = 27.211396 eV
      del_ev = del_au*ev
      write(nlist,'(/,3x,"Transition energy:",3x,f12.8,2x,"a.u.")')
     & del_au
      write(nlist,'(3x,"Transition energy:",3x,f6.2,8x,"eV")')del_ev
c
      erg = echarg * echarg / bohr
      freq = del_au * erg / planck
      freq2 =  del_au * cm_1
c
      write(nlist,'(3x,"Transition frequency:",2x,e11.5,1x,"1/sec")')
     &  freq
      write(nlist,'(3x,"Transition frequency:",2x,e11.5,1x,"cm-1")')
     &  freq2
c
      write(nlist,'(//,7x,"<L> value components:")')
      write(nlist,'(/,8x,9x,"x",12x,"y",12x,"z",/)')
      write(nlist,'("<L> electron",3(3x,f10.6),2x,"e*bohr")') x,y,z
c     write(nlist,'("total   ",3(3x,f10.6),2x,"e*bohr")')
c    . x,y,z
cmd
      if (iprint.eq.1) then 
      open(unit=70,file='matel',form='formatted',position='append')
      write(70,71) nroot1,slabel(ssym1),nroot2,slabel(ssym2), 
     .   nroot1,drt1,nroot2,drt2,x,y,z
71    format('#  transition',i4,a4,'->',i4,a4,
     .  '(nroot1,drt1,nroot2,drt2,lx,ly,lz)'/4i4,3e15.8)
      close(70)
      endif 
cmd
c
      diplen = dsqrt(x*x+y*y+z*z)
c
      write(nlist,'(//,3x,"<L> value length:",3x,f10.6,4x,"e*bohr"/)')
     & diplen
c
c     write(nlist,
c    & '(''  Note, that the transition dipole moment for identical '//
c    & 'bra and''/''ket states does not include contributions due to ''/
c    & ''frozen core electrons or nuclei '')')

          return
          end
c********************************************************************
c
      subroutine clcmom (nsym,core,lcore,iprint)
       implicit none 
       integer lcore,nsym,iprint
      real*8 core(lcore)

      integer   nfilmx
      parameter(nfilmx=55)
      integer  faterr
      parameter (faterr=2)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)


      integer moints
      equivalence (moints ,nunits(29))
      integer geom
      equivalence (geom ,nunits(26))

      character*60 fmoints
      character*80 fmt
      equivalence (fmoints,fname(29))
      character*60 fgeom
      equivalence (fgeom,fname(26))



       integer ntitleao,nsymao,nbftao,ninfoao,nenrgyao,nmap,ierr
       integer ibmpt,iao,imo,jbmpt,jao,jmo,aopt,mopt,prnopt
       integer isym,jsym,ijsym,nbi,nmi,nbj,nmj
       integer itbmx,i,ntot,ntitmx,ninfomx,nengmx,maxsym
       integer l1rcao,n1mxao,l2rcao, n2mxao
       integer j,off,offx,offy,offz,ll

       parameter (ntitmx=20,ninfomx=10,nengmx=10,maxsym=8)
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
       integer mxinor
        parameter (mxinor=128)

       character*60 title(ntitmx)
       integer nbpsy(maxsym),btypes(0:2),map,
     .         infoao(ninfomx),ietype(nengmx),imtype,mapin(nmotx)
     .        , nlist,nmax,nbmskp(maxsym)
       real*8 energy(nengmx)
       character*8 aolab(nmotx),molab(nmotx)
       character*4 slabel(maxsym)

      real*8 one,three,eight,small,planck,clight,bohr,
     & echarg,ev,debye,cm_1
      parameter ( one=1.0D+00,
     *           three=3.0D+00, eight=8.0D+00, small=1.0D-08,
     *           planck=6.626176D-27, clight=2.9979925D+10,
     *           bohr=5.291771D-09, echarg=4.803242D-10,
     *           ev=27.212D+00, debye=2.541766D+00,
     &           cm_1=219474.7d+00)

       real*8 scratch(nmotx*nmotx)


C
C     Common variables
C
      INTEGER             BLXT(MAXSYM),             N2OFFR(MAXSYM)
      INTEGER             N2OFFS(MAXSYM),           N2ORBT
      INTEGER             NOFFR(MAXSYM),            NOFFS(MAXSYM)
      INTEGER             NSGES(MAXSYM), nsdim(maxsym)
      integer    dkntin (maxsym*(maxsym+1)/2)
      integer    densoff(maxsym*(maxsym+1)/2)
      integer    dsize
C
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   dsize,densoff,dkntin
C
C
C
C     Common variables
C
      INTEGER             EQVSYM(MAXSYM),           IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(MAXSYM),           NFRC(MAXSYM)
      INTEGER             NFRV(MAXSYM),             NMUVAL
      INTEGER             NOCC(MAXSYM),             NORBSM(MAXSYM)
      INTEGER             NOXT(MAXSYM)
C
C
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF

C
      integer  ncsf1,ncsf2,refvec1,refvec2
      real*8  eci1,eci2,a4den1,a4den2
      common   /civoutinfo/ ncsf1,ncsf2,refvec1,refvec2,
     .                      eci1,eci2,a4den1,a4den2

C
      INTEGER             vecrln,indxln, ssym1,ssym2
C
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
      integer locbuf,locval,loclab,locdip,locmo,locscrt,locscrb,
     .        locdenao,lfree,ldens,ios
      integer nden,nmo,nscrt,nscrb,itypea,symoff(maxsym*(maxsym+1)/2)
      integer  nskip,kntao(maxsym*(maxsym+1)/2),lasta,lastb,last,nrec
      integer  filerr,syserr,ntitle,nsymmo,nbfpsy2(maxsym)
      integer  nmopsy(maxsym),itype
      integer atebyt,forbyt
      integer  icnt3,lsym,kl,offsetl(maxsym)
      real*8 dasum_wr,ddot_wr,diplen,x,y,z,sifsce, fcore2(10)
      real*8  xcore,ycore,zcore,del_au,del_ev
      real*8  omega,a,b,f,f2,fact,pi,erg,freq,freq2
      real*8  xnuc,ynuc,znuc,charge,pos(3)
      character*8 symb(nmotx),chrtyp
      external dasum_wr,ddot_wr,sifsce,atebyt,forbyt
      integer nndxf
      nndxf(i)= i*(i-1)/2


c       loc1 =  buff = locbuf
c       loc2 =  val  = locval
c       loc3 =  lab  = loclab
c       loc4 = x,y,z = locdip
c       loc5 = free  = lfree



c open files

       open(unit=moints,file=fmoints,form='unformatted',status='old',
     .   iostat=ios)
       if (ios.ne.0)
     .  call bummer ('cannot open unit ',moints,2)



c read header information

      call sifrh1(
     & moints,   ntitleao,   nsymao,   nbftao,
     & ninfoao,  nenrgyao,   nmap,     ierr)

      if ( ierr .ne. 0 ) then
         call bummer('clcmom: from sifrh1, ierr=',ierr,faterr)
      elseif ( ntitleao .gt. ntitmx ) then
         call bummer('clcmom: from sifrh1, ntitao=',ntitleao,faterr)
      elseif ( nbftao .gt. nmotx ) then
         call bummer('clcmom: from sifrh1, nbft=',nbftao,faterr)
      elseif ( ninfoao .gt. ninfomx ) then
         call bummer('clcmom: from sifrh1, ninfao=',ninfoao,faterr)
      elseif ( nenrgyao .gt. nengmx ) then
         call bummer('clcmom: from sifrh1, nenrgy=',nenrgyao,faterr)
      endif

c  ignore map vectors

       nmap=0

      call sifrh2 (
     & moints,    ntitleao,    nsymao,   nbftao,
     & ninfoao,   nenrgyao,    nmap,     title,
     & nbpsy,     slabel,      infoao,   aolab,
     & ietype,    energy,      imtype,   map,
     & ierr)

      if ( ierr .ne. 0 ) then
         call bummer('clcmom: from sifrh2, ierr=',ierr,faterr)
      endif

         icnt3 = 0
         do i=1,nsymao
          offsetl(i)=icnt3
         icnt3=icnt3+nsdim(i)
            do kl=1,nsdim(i)
            write(molab(offsetl(i)+kl),'(a2,i3,a3)')
     .        'MO',kl,slabel(i)(2:4)
            enddo
         enddo


       nlist=6

       if (iprint.eq.1) then
       write(nlist,*) '-------------- MO integral information ',
     .                '--------------'
       write(nlist,6100)'input file header information:'
c      call wtitle( nlist, ittl0, ntitxx, ntitle, ntitmx, title )
      write(nlist,6100)'input energy(*) values:'
      call sifpre( nlist, nenrgyao, energy, ietype )
      write(nlist,6200)'total ao core energy =',
     & sifsce( nenrgyao, energy, ietype )
      write(nlist,6040)nsymao,nbftao
6040  format(/' nsym =',i2,' nmot=',i4/)
6050  format(/a11,3x,8i4/)
6051  format(/a,3x,i4,a,i4/)
6060  format(/a11,3x,8a4/)
6100  format(/a11,3x,8i6/)
6200  format(/a22,3x,f12.7/)
      write(nlist,6050)'symmetry  =',(i,i=1,nsymao)
      write(nlist,6060)'slabel(*) =',(slabel(i),i=1,nsymao)
      write(nlist,6050)'nmpsy(*)  =',(nbpsy(i),i=1,nsymao)
      write(nlist,6100)'infomo(*) =',(infoao(i),i=1,ninfoao)
      endif

c     # extract some dimensioning information from infoao(*).
      l1rcao = infoao(2)
      n1mxao = infoao(3)
      l2rcao = infoao(4)
      n2mxao = infoao(5)

      if (nsymao.ne.nsym)
     . call bummer('clcmom: nsymao.ne.nsym ',nsymao,faterr)

      ntot=0
      do i=1,nsym
       ntot=ntot+nsdim(i)
      enddo

      if (ntot.ne.nbftao)
     . call bummer('clcmom: nmot.ne.ntot ',nbftao,faterr)

       do i=1,nsymao
        if (nbpsy(i).ne.nsdim(i))
     .   call bummer('clcmom: nmpsy(i).ne.nsdim(i),i=',i,faterr)
       enddo



c     set addresses
c
c
c mo coefficient offsets (quadratic)
c
      nmax=nsdim(1)
      nbmskp(1)=0
      nmo = nsdim(1)*nsdim(1)
      do i= 2,nsym
        nmax = max(nmax,nsdim(i))
        nbmskp(i)=nbmskp(i-1)+ nsdim(i-1)*nsdim(i-1)
        nmo = nmo + nsdim(i)*nsdim(i)
      enddo
c
c ao integral maximum size
c
      nskip = (nbftao*(nbftao+1))/2
      nscrt = nmax*nmax
      nscrb = nmax*nmax

c     check for correct ao integral file
      ldens  =1
      locbuf =1+dsize
      locval =locbuf + atebyt(l1rcao)
      loclab =locval  + atebyt(n1mxao)
      locdip =loclab  + forbyt(2*n1mxao)
c korr
      lfree  =locdip  + 3* atebyt(nskip)

c initialize denao and dip
      call wzero(3*nskip,core(locdip),1)

c print address array

       write(6,5000) ldens,locbuf,locval,loclab,locdip,lfree
5000   format('MO density     =',i6/,
     .        'IO buffer      =',i6/,
     .        'VAL buffer     =',i6/,
     .        'LAB buffer     =',i6/,
     .        'DIP integrals  =',i6/
     .        'top            =',i6/)

       itypea=1
       btypes(0)= 1
       btypes(1)= 2
       btypes(2)= 3
       itbmx = 2

       do i=1,nbftao
       mapin(i)=i
       enddo

       i=1
       do isym=1,nsymao
        do jsym=1,isym-1
          ijsym=nndxf(isym)+jsym
          symoff(ijsym)=i
          i=i+nbpsy(isym)*nbpsy(jsym)
        enddo
          ijsym=nndxf(isym)+isym
          symoff(ijsym)=i
          i=i+nbpsy(isym)*(nbpsy(isym)+1)/2
       enddo



       call sifr1n(moints,infoao,itypea,itbmx,btypes,
     .    core(locbuf),core(locval),core(loclab),nsym,
     .    nbpsy,symoff,mapin,nskip,core(locdip-1),fcore2,
     .    symb,kntao,lasta,lastb,last,nrec,ierr)

c
c      test for the case of identical bra and ket vectors
c      core contributions vanish for non-identical bra and ket
c
       if (nroot1.ne.nroot2 .or. drt1.ne.drt2) fcore2=0.0d0


        if (ierr.ne.0) then
          if (ierr.eq.-4)
     .    call bummer ("property integrals are missing on moints",0,2)
          call bummer ("error occurred reading moints ierr=",ierr,2)
        endif
        close(moints)

c
c  print mo integrals
c
         if (lvlprt.gt.3) then
         do itype=0,itbmx
           call siftyp(itypea,itype,chrtyp)
         do isym=1,nsym
          do jsym=1,isym
            ijsym = nndxf(isym) + jsym
            if (kntao(ijsym).gt.0) then
           if (jsym.ne.isym .and.
     .      (dasum_wr(nbpsy(isym)*nbpsy(jsym),
     .       core(locdip+nskip*itype+symoff(ijsym)-1),1)
     .         .gt.1.0d-9)) then
           call prblkc('MO integrals  ' // chrtyp,
     .     core(locdip+nskip*itype+symoff(ijsym)-1),nbpsy(isym),
     .       nbpsy(isym),
     .     nbpsy(jsym),molab(offsetl(isym)+1),molab(offsetl(jsym)+1),
     .     1,6)
           elseif (jsym.eq.isym .and.
     .       (dasum_wr(nbpsy(isym)*(nbpsy(isym)+1)/2,
     .       core(locdip+nskip*itype+symoff(ijsym)-1),1)
     .        .gt.1.0d-9)) then
           call plblkc('MO integrals  '//chrtyp,
     .     core(locdip+nskip*itype+symoff(ijsym)-1),nbpsy(isym),
     .     molab(offsetl(isym)+1),1,6)
           endif
           endif
          enddo
         enddo
         enddo
         endif



c check data

*@ifdef debug
*        do isym=1,nsym
*          write(*,*) 'symoff(*)=',(symoff(nndxf(isym)+i),i=1,isym)
*        enddo
*        do isym=1,nsym
*          write(*,*) 'densoff(*)=',(densoff(nndxf(isym)+i),i=1,isym)
*        enddo
*        do isym=1,nsym
*          write(*,*) 'dkntin(*)=',(dkntin(nndxf(isym)+i),i=1,isym)
*        enddo
*        do isym=1,nsym
*          write(*,*) 'kntao(*)=',(kntao(nndxf(isym)+i),i=1,isym)
*        enddo
*@endif


          x=0.0d0
          y=0.0d0
          z=0.0d0


         do isym=1,nsym
          do jsym=1,isym
            ll=nbpsy(isym)*nbpsy(jsym)
            ijsym = nndxf(isym) + jsym
            if (jsym.ne.isym) then
            if ((dkntin(ijsym).ne.0) .and. (kntao(ijsym).gt.0)) then
            x=x+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &       core(locdip+symoff(ijsym)-1),1)
            y=y+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &       core(locdip+nskip+symoff(ijsym)-1),1)
            z=z+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &       core(locdip+2*nskip+symoff(ijsym)-1),1)
*@ifdef debug
*             write(*,*) 'contributions from isym=',isym,'jsym=',jsym
*             write(*,*) 'x,y,z=',x,y,z
*@endif
            endif
            else
            if ((dkntin(ijsym).ne.0) .and. (kntao(ijsym).gt.0)) then
            ll=nbpsy(isym)*(nbpsy(isym)+1)/2
            x=x+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &       core(locdip+symoff(ijsym)-1),1)
            y=y+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &       core(locdip+nskip+symoff(ijsym)-1),1)
            z=z+2.0d0*ddot_wr(ll,core(ldens+densoff(ijsym)-1),1,
     &       core(locdip+nskip+nskip+symoff(ijsym)-1),1)
             ll=0
            do  i=1,nbpsy(isym)
             ll=ll+i
             x=x-core(ldens+densoff(ijsym)-2+ll)
     .                   *core(locdip+symoff(ijsym)-2+ll)
             y=y-core(ldens+densoff(ijsym)-2+ll)
     .                   *core(locdip+nskip+symoff(ijsym)-2+ll)
             z=z-core(ldens+densoff(ijsym)-2+ll)
     .                   *core(locdip+2*nskip+symoff(ijsym)-2+ll)
           enddo
            endif
*@ifdef debug
*             write(*,*) 'contributions from isym=',isym,'jsym=',jsym
*             write(*,*) 'x,y,z=',x,y,z
*@endif
            endif
          enddo
         enddo


c scale expectation values and print results

      if (iprint.eq.1) then
      write(nlist,'(//,80("*"),///,12x,"***   FINAL RESULTS   ***",/)')
      else
      write(nlist,
     & '(//,80("*"),///,10x,"*** intermediary results ***",/)')
      endif
      write(nlist,'(//,26x,"State No. 1",12x,"State No. 2")')

      if (method.ne.0) write(nlist,'(//,26x,"(ref. state)")')

      write(nlist,'(/,3x,"Space symmetry:",11x,a4,19x,a4))')
     & slabel(ssym1),slabel(ssym2)
      write(nlist,'(3x,"No. of CSFs:",10x,i8,15x,i8)') ncsf1,ncsf2
      write(nlist,'(3x,"State energies:",4x,f15.8,8x,f15.8,2x,"a.u.")')
     & eci1,eci2
      del_au = eci2-eci1
c    #   1 a.u. = 27.211396 eV
      del_ev = del_au*ev
      write(nlist,'(/,3x,"Transition energy:",3x,f12.8,2x,"a.u.")')
     & del_au
      write(nlist,'(3x,"Transition energy:",3x,f6.2,8x,"eV")')del_ev
c
      erg = echarg * echarg / bohr
      freq = del_au * erg / planck
      freq2 =  del_au * cm_1
c
      write(nlist,'(3x,"Transition frequency:",2x,e11.5,1x,"1/sec")')
     &  freq
      write(nlist,'(3x,"Transition frequency:",2x,e11.5,1x,"cm-1")')
     &  freq2
c
      write(nlist,'(//,7x,"Transition moment components:")')
      write(nlist,'(/,8x,9x,"x",12x,"y",12x,"z",/)')
      write(nlist,'("electron     ",3(3x,f10.6),2x,"e*bohr")') x,y,z
      write(nlist,'("frozen core  ",3(3x,f10.6),2x,"e*bohr")') 
     .                                  fcore2(1),fcore2(2),fcore2(3)
      x=x+fcore2(1)
      y=y+fcore2(2)
      z=z+fcore2(3)
      write(nlist,'("total (elec) ",3(3x,f10.6),2x,"e*bohr")') x,y,z
c     write(nlist,'("total   ",3(3x,f10.6),2x,"e*bohr")')
c    . x,y,z
c
      diplen = dsqrt(x*x+y*y+z*z)
c
      write(nlist,'(//,3x,"Tr.dipole length:",3x,f10.6,4x,"e*bohr")')
     & diplen
c
      omega = freq / clight

      f = 2.0d0 * del_au * diplen * diplen/three
      f2 = 3.0381d-06 * freq2 * diplen * diplen
      fact = (debye * 1.0D-18 / planck)
      pi = acos(-one)
      fact = 8.0d0 * pi * pi * pi * fact * fact
      fact = fact/(three*clight)
      b = fact * diplen * diplen
      a = eight * pi * del_au * erg * omega * omega * b

      write(nlist,'(3x,"Oscillator strength :",1x,f9.6)')f
c     write(nlist,'(3x,"Oscillator strength :",1x,f9.6)')f2
      write(nlist,'(3x,"Einstein A coef.:",3x,e12.4,2x,"1/sec")')a
      write(nlist,'(3x,"Einstein B coef.:",3x,e12.4,2x,"sec/g",///)')b

c     write(nlist,
c    & '(''  Note, that the transition dipole moment for identical '//
c    & 'bra and''/''ket states does not include contributions due to ''/
c    & ''frozen core electrons or nuclei '')')

          return
          end
c
c*********************************************************************
c
      subroutine pstclc(nmsym, core, lfree,code)
       implicit none
c
C    VARIABLE DECLARATIONS FOR subroutine pstclc
C
C     Parameter variables
C
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 14)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             MAXREP
      PARAMETER           (MAXREP = 20)
      INTEGER             MXRDIM
      PARAMETER           (MXRDIM = 9)
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXSIZE
      PARAMETER           (MXSIZE = 20)
C
      REAL*8              ZERO, ONE, TWO
      PARAMETER           (ZERO = 0.0D0,ONE = 1.0D0,TWO = 2.0D0)
      REAL*8              THRESH
      PARAMETER           (THRESH = 1D-11)
C
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer tape6,nslist,filden
      equivalence (nunits(1),tape6)
      equivalence (nunits(3),nslist)
      equivalence (nunits(25),filden)
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    NMSYMX,       SCRLEN
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
c
      REAL*8 d1_sym(iprpmx),d1ci_sym(iprpmx),d1_asym(iprpmx),
     &       d1ci_asym(iprpmx)
      COMMON / DEN1P  / d1_sym, d1ci_sym, d1_asym, d1ci_asym
c
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C
      integer mult(8,8)
      common /csym/ mult
C
      INTEGER             BLXT(MAXSYM),             N2OFFR(MAXSYM)
      INTEGER             N2OFFS(MAXSYM),           N2ORBT
      INTEGER             NOFFR(MAXSYM),            NOFFS(MAXSYM)
      INTEGER             NSGES(MAXSYM), nsdim(maxsym)
      integer    kntin (maxsym*(maxsym+1)/2)
      integer    symoff(maxsym*(maxsym+1)/2)
      integer    icnt1
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   icnt1,symoff,kntin
C
      INTEGER             EQVSYM(MAXSYM),           IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(MAXSYM),           NFRC(MAXSYM)
      INTEGER             NFRV(MAXSYM),             NMUVAL
      INTEGER             NOCC(MAXSYM),             NORBSM(MAXSYM)
      INTEGER             NOXT(MAXSYM)
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
      CHARACTER*4         SYMLABEL(MAXSYM)
      COMMON / SLABEL /   SYMLABEL
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
c  ##  integer section
c
      integer code
      INTEGER DUM, DUM1(MAXSYM),DUM2(MAXSYM),dsym
      INTEGER FILERR
      INTEGER IENERGY(MXSIZE), i, i1,ii,ij,in,INFO(MXSIZE), INMAX,
     &        INMIN,IOC,IOCMAX,IOCMIN,ISKIP,iii,icnt2,icnt3,isym,
     &        itypea,itypeb,ifmt,ierr,iroot,icount,i2,imd
      integer j
      INTEGER K,KL,kntout(maxsym*(maxsym+1)/2)
      integer L,loc1,loc2,loc3,loc4,loc5,lsym,lstflg,l1rec,l2rec,
     &        lfree
      INTEGER M, MM, MXREAD,mapout(nmotx)
      integer NBMT,NBMT1, NENRGY,NFC, NGES,NGES1, NGG, NINFO,NMSYMN,
     &        NN, NOCOEF, NORB, NSGES1, NTIT, NTITLE,nmax1,nmax2,
     &        nmap,nrec,numtot,NMSYM
      integer offset,offsetl(8)
      INTEGER SYSERR
c
c  ##  real*8 section
c
      REAL*8 ELNUM,energy(10)
      REAL*8 core(*)
      REAL*8 dasum_wr
      real*8 fcore
      real*8 summd1
c
c  ##  character section
c
      CHARACTER*80        TITLE(MXTITL)
      CHARACTER*8         BFNLAB(nmotx), molab(nmotx)
c
c  ## external section
c
      integer atebyt,forbyt
      external atebyt,forbyt
      integer nndxf
      nndxf(i)= i*(i-1)/2
c
c---------------------------------------------------------------------
c

      elnum = zero
      call izero_wr (nndxf(maxsym),kntin,1)
      call izero_wr (nndxf(maxsym),symoff,1)

c
c count space for all possible blocks

1010   format(10('---'),'Final Density for SYMBLOCK ',i2,i2,10('---'))
         icnt1 = 0
         icnt3 = 0
         do i=1,nmsym
          offsetl(i)=icnt3
         icnt3=icnt3+nsdim(i)
            do kl=1,nsdim(i)
            write(molab(offsetl(i)+kl),'(a2,i3,a3)')
     .        'MO',kl,symlabel(i)(2:4)
            enddo
         enddo

         dsym=mult(ssym1,ssym2)
         do i = 1,nmbpr(dsym)
c
c         isym >= lsym
c
            isym=lmdb(dsym,i)
            lsym=lmda(dsym,i)


            if (isym.eq.lsym) then
            icnt1=icnt1+ (nsdim(isym)*(nsdim(isym)+1)/2)
            else
            icnt1=icnt1+nsdim(isym)*nsdim(lsym)
            endif
         enddo ! end of:  do i = 1,nmbpr(dsym)
c
c space for the complete transition density

c
c core  loc1 : density(icnt1)

         loc1 = 1
         loc2 = loc1+atebyt(icnt1)
         loc3 = loc1+atebyt(icnt1)

         if (loc3.gt.lfree) then
           call bummer ('insufficient space,lfree=',lfree,2)
         endif
         call wzero(icnt1,core(loc1),1)
c
        kl=0
        offset=0
      do 180 i = 1,nmbpr(dsym)
           isym=lmdb(dsym,i)
           lsym=lmda(dsym,i)
         if ( (nsdim(isym)*nsdim(lsym)).gt.0) then
           kntin(nndxf(isym)+lsym)=1
           symoff(nndxf(isym)+lsym)=offset+1
c--------------------------------------------------------------
           if (isym.eq.lsym) then
c
c integrals are lower triangular packed
c den1 represents the lower triangular packed density
c
c      1
c      2 3
c      4 5 6
c      7 8 9 10
c
       ij = nsdim(isym)*(nsdim(isym)+1)/2
c
       if (code.eq.0) then
c
       call dcopy_wr(ij,d1_sym(offset+1),1,core(loc1+offset),1)
c      write(6,*) 'copying from ',offset+1,offset+ij
       call plblkc('symm-trans density matrix: diagonal part',
     .      core(loc1+offset),nsdim(isym),molab(offsetl(isym)+1),1,6)
c
       else
c
       call dcopy_wr(ij,d1_asym(offset+1),1,core(loc1+offset),1)
       call plblkc('asymm-trans density matrix: diagonal part',
     .      core(loc1+offset),nsdim(isym),molab(offsetl(isym)+1),1,6)
c
       endif
       offset=offset+nsdim(isym)*(nsdim(isym)+1)/2

c--------------------------------------------------------------

            else

c integrals
c                  isym
c
c            1  2  3
c            4  5  6
c            7  8  9
c    jsym   10 11 12
c
c
c  d1_sym(lsym,isym) : isym > lsym
c
c  core:  upper triangle
c  integrals are now packed as rectangular arrays with ARRAY(j,i)
c  with i>j ,i.e j lines of length i
c
        ij = nsdim(isym)*nsdim(lsym)
c
       if (code.eq.0) then
c
        call dcopy_wr(ij,d1_sym(offset+1),1,core(loc1+offset),1)
c      write(6,*) 'copying from ',offset+1,offset+ij
       call prblkc('symm-trans density matrix: offdiagonal part',
     .      core(loc1+offset),nsdim(isym),nsdim(isym),nsdim(lsym),
     .      molab(offsetl(isym)+1),molab(offsetl(lsym)+1),1,6)
c
       else
c
        call dcopy_wr(ij,d1_asym(offset+1),1,core(loc1+offset),1)
       call prblkc('asymm-trans density matrix: offdiagonal part',
     .      core(loc1+offset),nsdim(isym),nsdim(isym),nsdim(lsym),
     .      molab(offsetl(isym)+1),molab(offsetl(lsym)+1),1,6)
c
       endif
       offset=offset+nsdim(isym)*nsdim(lsym)
       endif

c--------------------------------------------------------------

            end if

 180   continue
          return

 9000 format (14a4)
 9010 format (/,' maximal number of new irreps',i6)
 9020 format (/,' irrep        :',i4)
 9030 format ('    dimension         :',i6)
 9040 format ('       direction         :',i4)
 9050 format ('       number of orbitals:',i4)
 9060 format (/,/,' vectors are read with a format of: ',a80,/,/)
 9070 format (/,/,' vectors are read from mcscf restart tape')
c
 9080 format (/,1x,' eigenvectors of nos in mo-basis')
 9090 format (1x,' eigenvectors of nos in ao-basis, column ',i4)
 9100 format (a80)
 9110 format (19a4)
 9120 format (' output from ci calculation')
 9130 format (/,/,' first order reduced density matrix in mo basis',/,/)
 9140 format (/,/,' first order reduced density matrix in mo basis',
     +       ' according to new symmetry',/,/)
 9150 format (/,/,'*****   symmetry block ',a4,'   *****')
 9160 format (/,/,'*****   irrep nr ',i4,'  symmetry block nr ',i4,
     +       '   *****')
 9170 format (/,' occupation numbers of nos')
 9180 format (1x,8e15.7)
 9190 format (1x,'dimension for d1_sym too small in routine densi ',
     +       ' nges = ',i5,'    iprpmx = ',i5)
 9200 format (a4)
 9210 format (20i4)
 9220 format (6f12.9)
 9230 format (8e15.7)
 9240 format (5(f12.7,3x))
 9250 format (/,/,' total number of electrons = ',f15.10,/)
c9260 format (1x,'output file for properties is ',a8,/)

      end
c
c****************************************************************
c
       subroutine getciphase (unit,buffer,lenrec,iroot,ivec)
       implicit none 
       integer iroot,lenrec,ivec,i
       character*(*) unit
       real*8 buffer(lenrec)
       integer ncsft,icsf
       integer*8 offset

c      unit = file to read ci vector
c      buffer    = ci buffer
c      iroot     = root to read
c      lenrec    = record length
c      ivec      = bra or ket (1,2)

C
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      KBL3,        KBL4,        LAMDA1
      INTEGER             MAXBL2,      MAXBL3,      MAXBL4,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXKBL3,      MXKBL4,      MXORB,       N0EXT
      INTEGER             N0XTLP,      N1EXT,       N1XTLP,      N2EXT
      INTEGER             N2XTLP,      N3EXT,       N3XTLP,      N4EXT
      INTEGER             ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER             NEXT,        NFCT,        NINTPT,      NMBLK3
      INTEGER             NMBLK4,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             TOTSPC,      TOTVEC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       MAXBL4,      NMBLK4
      COMMON / INF    /   KBL4,        MXKBL4,      LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXBL3,      NMBLK3
      COMMON / INF    /   KBL3,        MXKBL3,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   N3XTLP,      N2XTLP,      N1XTLP,      N0XTLP
      COMMON / INF    /   TOTVEC,      CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI

      INTEGER            sBUFSZI,     sBUFSZL,     sCONFYZ,     sDIMCI
      INTEGER            sINTORB,     sKBL3,       sKBL4,       sLAMDA1
      INTEGER            sMAXBL2,     sMAXBL3,     sMAXBL4,     sMAXLP3
      INTEGER            sMINBL3,     sMINBL4,     sMXBL23,     sMXBLD
      INTEGER            sMXKBL3,     sMXKBL4,     sMXORB,      sN0EXT
      INTEGER            sN0XTLP,     sN1EXT,      sN1XTLP,     sN2EXT
      INTEGER            sN2XTLP,     sN3EXT,      sN3XTLP,     sN4EXT
      INTEGER            sND0EXT,     sND2EXT,     sND4EXT,     sNELI
      INTEGER            sNEXT,       sNFCT,       sNINTPT,     sNMBLK3
      INTEGER            sNMBLK4,     sNMONEL,     sNVALW,      sNVALWK
      INTEGER            sNVALX,      sNVALY,      sNVALZ,      sPTHW
      INTEGER            sPTHX,       sPTHY,       sPTHYZ,      sPTHZ
      INTEGER            sTOTSPC,     sspcci
C
      COMMON / SECINF /  sPTHZ,       sPTHY,       sPTHX,       sPTHW
      COMMON / SECINF /  sNINTPT,     sDIMCI,      sMAXBL4,     sNMBLK4
      COMMON / SECINF /  sKBL4,       sMXKBL4,     sLAMDA1,     sBUFSZI
      COMMON / SECINF /  sNMONEL,     sINTORB,     sMAXBL3,     sNMBLK3
      COMMON / SECINF /  sKBL3,       sMXKBL3,     sMAXLP3,     sBUFSZL
      COMMON / SECINF /  sMAXBL2,     sMXBL23,     sMXORB,      sMXBLD
      COMMON / SECINF /  sNEXT,       sTOTSPC,     sMINBL4,     sMINBL3
      COMMON / SECINF /  sND4EXT,     sND2EXT,     sND0EXT,     sN4EXT
      COMMON / SECINF /  sN3EXT,      sN2EXT,      sN1EXT,      sN0EXT
      COMMON / SECINF /  sN3XTLP,     sN2XTLP,     sN1XTLP,     sN0XTLP
      COMMON / SECINF /  sSPCCI,      sCONFYZ,     sPTHYZ,      sNVALWK
      COMMON / SECINF /  sNVALZ,      sNVALY,      sNVALX,      sNVALW
      COMMON / SECINF /  sNFCT,       sNELI


       integer ii,maxref,nref,iref
       parameter (maxref=8*4096)
       common /reflist/nref(2),iref(maxref,2)
       integer refptr
       real*8  refphase1(100),refphase2(100)
       real*8  ciphase1(100),ciphase2(100)
       integer cicnt1,cicnt2,rcnt1(100),rcnt2(100)
       common  /phase/rcnt1,rcnt2,refphase1,refphase2,
     .                cicnt1,cicnt2,ciphase1,ciphase2


        if (ivec.eq.1) then
        call izero_wr(100,rcnt1,1)
        write(6,'(/1x,a)')
     .     'Dominant configurations in CI vector (bra)'
        refptr=0
        cicnt1=0
        do 100 icsf=1,nvalz,lenrec
          ncsft=min(lenrec,(nvalz-icsf+1))
c         call getvec(unit,lenrec,iroot,dimci,buffer,icsf,ncsft)
          offset=(iroot-1)*dimci+icsf
          call getputvec(unit(1:len_trim(unit)),
     .           buffer,ncsft,offset,'openrd')

c         do ii = 1,ncsft
c          write(*,*) 'ci(',ii,')=',buffer(ii)
c         enddo
          if (cicnt1.ge.100) goto 101
 105      refptr=refptr+1
          if (refptr.le.nref(1)) then
           if (iref(refptr,1).lt.(icsf+ncsft)) then
             if (abs(buffer(iref(refptr,1)-icsf+1)).gt.0.05) then
              cicnt1=cicnt1+1
              rcnt1(cicnt1)=iref(refptr,1)
              ciphase1(cicnt1)=buffer(iref(refptr,1)-icsf+1)
              write(6,*) 'ciphase1(#ref',cicnt1,' #index=',
     .                     iref(refptr,1),' )=',ciphase1(cicnt1)
             endif
             goto 105
           endif
          endif
 100    continue
 101    continue

        elseif (ivec.eq.2) then
        refptr=0
        write(6,'(/1x,a)')
     .     'Dominant configurations in CI vector (ket)'
        call izero_wr(100,rcnt2,1)
        cicnt2=0
        do 200 icsf=1,nvalz,lenrec
          ncsft=min(lenrec,(nvalz-icsf+1))
c         call getvec(unit,lenrec,iroot,sdimci,buffer,icsf,ncsft)
          offset=(iroot-1)*sdimci+icsf
          call getputvec(unit(1:len_trim(unit)),
     .         buffer,ncsft,offset,'openrd')

          if (cicnt2.ge.200) goto 201
 205      refptr=refptr+1
          if (refptr.le.nref(2)) then
           if (iref(refptr,2).lt.(icsf+ncsft)) then
             if (abs(buffer(iref(refptr,2)-icsf+1)).gt.0.05) then
              cicnt2=cicnt2+1
              rcnt2(cicnt2)=iref(refptr,2)
              ciphase2(cicnt2)=buffer(iref(refptr,2)-icsf+1)
              write(6,*) 'ciphase2(#ref ',cicnt2,' #index=',
     .                     iref(refptr,2),')= ',ciphase2(cicnt2)
             endif
             goto 205
           endif
          endif
 200    continue
 201    continue
        else
         call bummer('invalid ivec=',ivec,2)
        endif

        return
        end





       subroutine setref(limvec,refvec,ivec,pthz,nvalz)
        implicit none 
        integer limvec(*),refvec(*),ivec,pthz,nvalz

       integer maxref,nref,iref
       parameter (maxref=8*4096)
       common /reflist/nref(2),iref(2*maxref)

       integer i,ioff,remref,iptr,irefcnt

       remref=0
       iptr =0
       irefcnt =0
       ioff=8*4096*(ivec-1)

       do i=1,pthz
         if (limvec(i).gt.0) then
              iptr=iptr+1
              if (iptr.gt.nvalz)
     .          call bummer('setref: nvalz exceeded',nvalz,2)
              if (refvec(i).eq.1) then
                irefcnt=irefcnt+1
                iref(ioff+irefcnt)=limvec(i)
              endif
         else
           if (refvec(i).eq.1) then
               remref=remref+1
           endif
         endif
        enddo
        if (nvalz.ne.iptr)
     .   call bummer('setref: drt inconsistency nvalz.ne.iptr',
     .                iptr,2)
        write(6,6010) irefcnt,remref,nvalz
        nref(ivec)=irefcnt
6010    format('setref: ',i8, ' references kept'/
     .          t8,i8,' references deleted '/
     .          t8,i8,' initial valid z walks.')
        return
        end
c
c********************************************************************
c
      subroutine calcphase(phasefactor)
c
       implicit none
      real*8 zero
      parameter (zero=0.0d+00)
c
      real*8 phasefactor,scr(100)
      real*8  refphase1(100),refphase2(100)
      real*8  ciphase1(100),ciphase2(100)
       integer cicnt1,cicnt2,rcnt1(100),rcnt2(100)
      common  /phase/rcnt1,rcnt2,refphase1,refphase2,
     .                cicnt1,cicnt2,ciphase1,ciphase2
c
c  ##  integer section
c
      integer i,j
      integer k
      integer rp(2)
c
c-------------------------------------------------------------------
c
c   1. rp1= state1:  determine whether ci and mcscf have the same phase
c      rp2= state2:  determine whether ci and mcscf have the same phase
c   2. if (rp1 = rp2 ) Density = CIdens*a4den + (1-a4den)*MCdens
c      if (rp1 <> rp2) Density = CIdens*a4den - (1-a4den)*MCdens


        do i= 1,cicnt1
        scr(i)=ciphase1(i)*refphase1(i)
        enddo
          if (scr(1) .gt.zero) then
           do i= 2,cicnt1
            if ( scr(i).lt.zero)
     .       write(6,*) 'something wrong: scr(1),i,scr(i)='
     .       ,scr(1),i,scr(i)
           enddo
           rp(1)=1
          elseif (scr(1).lt.zero) then
           do i= 2,cicnt1
            if ( scr(i).gt.zero)
     .       write(6,*) 'something wrong: scr(1),i,scr(i)='
     .       ,scr(1),i,scr(i)
           enddo
           rp(1)=-1
          endif
c
        do i= 1,cicnt2
        scr(i)=ciphase2(i)*refphase2(i)
        enddo
          if (scr(1) .gt.zero) then
           do i= 2,cicnt2
            if ( scr(i).lt.zero)
     .       write(6,*) 'something wrong: scr(1),i,scr(i)='
     .       ,scr(1),i,scr(i)
           enddo
           rp(2)=1
          elseif (scr(1).lt.zero) then
           do i= 2,cicnt2
            if ( scr(i).gt.zero)
     .       write(6,*) 'something wrong: scr(1),i,scr(i)='
     .       ,scr(1),i,scr(i)
           enddo
          rp(2)=-1
          endif
        if (rp(1).eq.rp(2)) then
         phasefactor=1.0d0
        else
         phasefactor=-1.0d0
        endif
        write(6,100)
 100    format(//' Determination of phase factor: '/)
        do i=1,2
        if (rp(i).eq.1) then
         write(6,*) 'SAME phase of MCSCF and CI wave function for',
     .    ' state', i
        else
         write(6,*) 'OPPOSITE phase of MCSCF and CI wave function for',
     .    ' state', i
        endif
        enddo
101     format(' relative phasefactor: ', f4.1 //)
        write(6,101) phasefactor
        return
        end
c
c********************************************************************
c
      subroutine rdcivout (tape6, ciuvfl, lroot,
     .                         a4den,itype,froot,refvec,eci,ncsf,
     .                         fname)

c     analyses civout vector info file written by ciudg
c
      implicit none 
c
c     input:
c     tape6 : listing file
c     lroot : vector to calculate the density for
c             if root following was active for ciudg,
c             lroot corresponds to that ci vector that has a
c             maximum overlap with the lroot-th reference vector
c             in this case there is only ONE record on civout
c             and we check for consistency
c             Otherwise lroot corresponds to the lroot-th ci vector
c             (LRT,CI)
c     output
c     a4den : a4den value for the selected root
c             (froot overrides lroot!)
c     itype : corresponds to method in ciudg
c             0 : CI
c             3 : AQCC
c             30: AQCC-LRT
c
c     froot : the ci vector record to be used for density calculation
c     refvec: reference vector to be used for reference density
c              info(1) of ci vector #froot


C    VARIABLE DECLARATIONS FOR subroutine rdvhd
C
C     Parameter variables
C
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 20)
      INTEGER             CURVER
      PARAMETER           (CURVER = 1)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 10)
      INTEGER             MXINFO
      PARAMETER           (MXINFO = 10)
      INTEGER             MXNRGY
      PARAMETER           (MXNRGY = 10)
      integer mxinor
      parameter (mxinor=128)
      integer nmotx
      parameter (nmotx=1023)
C
C     Argument variables
C
      INTEGER             CIUVFL, lroot,itype,froot,refvec
      INTEGER             TAPE6,ncsf
C
      REAL*8              A4DEN,eci
      character*(*)       fname

C
C     Local variables
C
      CHARACTER*80        TITLE(MXTITL)
C
      INTEGER             I,  IETYPE(MXNRGY)
      INTEGER             LENCI,       MXREAD,      NENRGY
      INTEGER             NINFO,       NTITLE
      INTEGER             VERSN
      integer           info(mxinfo)
      integer           iroot ,idum1,idum2
C
      REAL*8              ENERGY(MXNRGY)
C
C    END OF VARIABLE DECLARATIONS FOR subroutine rdvhd

      rewind(ciuvfl)
      iroot=0
      froot=0
      a4den=0.0d0
      itype=-1
      refvec=0
      eci=0.0d0

c
c     # search for froot
c

 90   read (ciuvfl,end=99) versn,idum1,ncsf
      iroot=iroot+1
      if ( versn .ne. curver ) then
         call bummer('rdvhd:  wrong version number', versn, faterr)
      endif
c
c     # general information
      read (ciuvfl) ninfo, nenrgy, ntitle
c
c     info(1) : maximum overlap with reference vector # info(1)
c     info(2) : corresponding ci vector is record # info(2)
c     info(3) : method CI(0),AQCC(3),AQCC-LRT(30)
c     info(4) : if set to one this is the last record of civout
c     info(5) : overlap of reference vector and ci vector in percent
c

      mxread = min (ninfo, mxinfo)
      read (ciuvfl) (info(i), i=1,mxread)
c     # read titles:
      mxread = min (ntitle,mxtitl)
      read (ciuvfl) (title(i), i=1,mxread)
c
c     # read energies
cps      mxread = min(nenrgy,mxnrgy)
      if ( nenrgy .gt. mxnrgy ) then
c        # must have space for all of the energy entries.
         call bummer('rdhciv: (nenrgy-mxnrgy)=',
     &    (nenrgy-mxnrgy), faterr )
      endif
      mxread = nenrgy

      read(ciuvfl) (ietype(i),i=1,mxread),(energy(i),i=1,mxread)
c     # skip sovref
      read (ciuvfl)
c     # skip tciref
      read (ciuvfl)
c     # skip e,mxov
      read (ciuvfl)

c
c     identify the special case of root following
c     this also includes the special case of ground
c     state calculations: there is only ONE record on civout
c
      if (iroot.eq.1 .and. info(4).eq.1) then
        itype = info(3)
        froot = info(2)
        refvec = info(1)
        if (info(5).lt.50)
     .   call bummer('insufficient overlap, probably intruder states',
     .   0,0)
        if (froot.ne.lroot)
     .   call bummer('lroot (cidenin) and froot/nroot (ciudgin)'//
     .               ' do not match ',0,2)
        if (itype.ne.-1) then
             do 10 i = 1, min(nenrgy,mxnrgy)
           if (ietype(i) .eq. -1024-15) a4den = energy(i)
c          MR-SDCI
           if (ietype(i) .eq. -1026)    eci   = energy(i)
c          MR--AQCC
           if (ietype(i) .eq. -1024-14)    eci   = energy(i)
c          MR-AQCC-LRT
           if (ietype(i) .eq. -1024-16)    eci   = energy(i)
   10        continue
        endif
        goto 92
      else
        if (info(2).eq.lroot) then
          itype= info(3)
          froot= info(2)
          refvec = info(1)
          if (info(5).lt.50)
     . call bummer('insufficient overlap, probably intruder states',
     . 0,0)
        if (itype.ne.-1) then
             do 11 i = 1, min(nenrgy,mxnrgy)
           if (ietype(i) .eq. -1024-15) a4den = energy(i)
c          MR-SDCI
           if (ietype(i) .eq. -1026)    eci   = energy(i)
c          MR-AQCC
           if (ietype(i) .eq. -1024-14)    eci   = energy(i)
c          MR-AQCC-LRT
           if (ietype(i) .eq. -1024-16)    eci   = energy(i)
   11        continue
        endif
        goto 91
        endif
      endif

      if (info(4).eq.0) goto 90
 91   continue

c
c  ##  in case of root following, the correct lroot value in cidenin
c  ##  is set by runsp.perl.  check, if this has been done correctly
c

      if (iroot.lt.lroot) then
        call bummer('lroot larger than number of ci vectors on file',
     &   iroot,faterr)
      endif
  92  continue


      write(tape6,15)
15    format(20('==='))
      write(tape6,*) 'extracting information from ',fname,':'
      write(tape6,*) 'ci vector:',froot,' itype=',itype,
     .       ' reference vector',refvec
      write(tape6,*) 'a4den=',a4den,' eci=',eci
      write(tape6,*) 'titles:'
      do i=1, min(ntitle,mxtitl)
       write(tape6,*) title(i)
      enddo
      write(tape6,15)

      return
 99   continue
      call bummer('civout file incomplete',0,2)
      end
c
c********************************************************************+
c
      subroutine rdcirefv(
     & tape6,   cirefv,     filv,   lenci,
     & core,    phase,      pcnt,   refvec,
     & cicnt,   no)

       implicit none
c     read reference vector from ci reference vector file
c     and copy it to the ci vector file
c
c
c     tape6  listing file
c     cirefv ci reference vector file (written by writer)
c     ncsf   not needed
c     filv   ci vector file
c     lenci  length of ci vector
c     core   work space
c     phase(pcnt) largest coefficients in reference vector
c
      integer cicnt,no
      integer tape6,cirefv,ncsf,lenci,pcnt(cicnt)
      character*(*) filv
      real*8 core(*),phase(*)
      integer mxinor
      parameter (mxinor=128)
      integer nmotx
      parameter (nmotx=1023)

      integer refvec,itype,nrecv
      integer*8 offset

      integer cnt,j,recamt,icsf,i,version,blksize

c     skip all reference vectors 1 .. refvec -1
      do i=1,refvec-1
c     # header information for refvec i
      read (cirefv,end=999) version,blksize,ncsf
c     write(tape6,*) 'iref,blksize,ncsf=',i,blksize,ncsf
c  ninfo,nenrgy,ntitle
      read (cirefv,end=999)
c  info
      read (cirefv,end=999)
c  title
      read (cirefv,end=999)
c  energy
      read(cirefv,end=999)
c    #  skip refvec i
         do icsf=1,ncsf,blksize
            read(cirefv,end=999)
         enddo
      enddo
c
c  #  read the proper reference vector
c

      read(cirefv,end=998) version,blksize,ncsf
      read(cirefv,end=998)
      read(cirefv,end=998)
      read(cirefv,end=998)
      read(cirefv,end=998)

c
c     # pack zeros into the rest of the ci vector.
c     # this is for easy calculation of the reference density matrix
c     # using the existing structure of the program.  This is however
c     # inefficient, and a more efficient method may be desirable.
c
        offset=1
        call getputvec(filv(1:len_trim(filv)),core,2,offset,'new')
        call getputvec(filv(1:len_trim(filv)),core,2,offset,'close')

        nrecv=8*4096
        if (blksize.ne.nrecv .and. ncsf.gt.nrecv )
     .  call bummer('rdciref: invalid block size',blksize,2)

         call wzero(nrecv,core,1)
        do 420 icsf =1,lenci,nrecv
          recamt = min(nrecv, (lenci-icsf+1))
          offset=icsf
          call getputvec(filv(1:len_trim(filv)),
     .              core,recamt,offset,'openwt')
c         call putvec(filv,nrecv,1,lenci,core,icsf,
c    &                  recamt, 2)
 420     continue


       call wzero(100,phase,1)
       cnt=1
c     # copy ci vector onto direct access file
      do 400 icsf=1,ncsf,blksize
c      write(0,*) 'icsf,ncsf,cicnt,cnt=',icsf,ncsf,cicnt,cnt
        recamt = min(blksize, (ncsf-icsf+1))
        call seqrbf(cirefv,core,recamt)
        offset=icsf
        call getputvec(filv(1:len_trim(filv)),
     .          core,recamt,offset,'openwt')
c       call putvec(filv,nrecv,1,lenci,core,icsf,recamt,2)
        if (cnt.gt.cicnt) goto 400
         do j=1,recamt
           if (j.eq.pcnt(cnt)) then
              phase(cnt)=core(j)
              cnt=cnt+1
            endif
            if (cnt.gt.cicnt) goto 400
         enddo
 400  continue
      if (no.eq.1) then
      write(6,'(/1x,a,i3)')
     .  'dominant configuraions in reference vector (bra)',
     .      refvec
      else
      write(6,'(/1x,a,i3)')
     .   'dominant configuraions in reference vector (ket)',
     .      refvec
      endif
      do j=1,cicnt
       write(6,*) 'refphase( #ref ',j,' #index=',pcnt(j),
     .            ' )=',phase(j)
      enddo
      return

 998  call bummer('cirefv too short',refvec,2)
 999  call bummer('cirefv too short',i,2)

      return
      END
c
c
c*********************************************************************+
c
c deck wtden1
      subroutine wtden1(
     & ntape,  info,   lbuf,   nipbuf,
     & buf,    ilab,   val,    d1,
     & nsym,   irx1,   irx2,   map,
     & itypea, itypeb, endmark)
c
c  write the 1-e d1(*) matrix elements to the sifs file
c
c  input:
c  ntape = output unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  d1(*) = 1-e density matrix
c  nsym,irx1(*),irx2(*) = symmetry range info.
c  map(*) = orbital index mapping vector.
c
c  written by gary kedziora
c
      implicit integer(a-z)
c
      integer info(10)
      integer    ifmt,     ibvtyp
      parameter( ifmt = 0, ibvtyp = 0)
      real*8 fcore
c
      integer   nipv
      parameter(nipv=2)
c
c     # sifs continuaton flag values
      integer   msame,   nmsame,  nomore
      parameter(msame=0, nmsame=1,nomore=2)
c
      real*8 buf(lbuf),d1(*),val(nipbuf)
      integer ilab(nipv,nipbuf),irx1(nsym),irx2(nsym),map(*)
c
      real*8    small
      parameter(small=1d-15)
c
c     # bummer error types
      integer wrnerr, nfterr, faterr
      parameter(wrnerr=0, nfterr=1, faterr=2)
c
      integer endmark
c
c specification of the various ietypes
      numtot = 0
c
c fcore contribution gets computed in tran for now.
c
      fcore   = 0.0
c
c  write 1-e density matrix elements and labels to sifs file
c
      last = msame
      pq=0
      iknt=0
      do 130 isym=1,nsym
         do 120 p=irx1(isym),irx2(isym)
            do 110 q=irx1(isym),p
               pq=pq+1
               if(iknt.eq.nipbuf)then
c                 # if the rare event happens that this is the last time
c                 # through the nested loops and iknt.eq.nipbuf then
c                 # set last=nomore
                  if ((isym .eq. nsym) .and. (p .eq. irx2(nsym)) .and.
     +                (q .eq. p)) then
                     last = endmark
                  else
                     last = msame
                  endif
                  numtot = numtot + iknt
                  call sifew1(
     &              ntape,   info,    nipv,    iknt,
     &              last,    itypea,  itypeb,  
     &              ibvtyp,  val,     ilab,    fcore,
     &              ibitv,   buf,     nrec,    ierr)
                  if ( ierr .ne. 0 )
     &               call bummer('wtden1:  sifew1, ierr=', ierr, faterr)
                  numtot = numtot - iknt
                  iknt = 0
               endif
               if (last .eq. endmark) return
c              if (abs(d1(pq)).gt.small) then
                iknt=iknt+1
                val(iknt)=d1(pq)
ctm this is really clever! 
ctm for frozen core orbitals map(i)=-1 which cannot be compressed

                ilab(1,iknt)=map(p)
                ilab(2,iknt)=map(q)
c              endif
110         continue
120      continue
130   continue
c
c dump the rest of the d1 matrix elements
c
      last = endmark
      numtot = iknt + numtot
      call sifew1(
     & ntape,  info,   nipv,     iknt,
     & last,   itypea, itypeb,   
     & ibvtyp, val,    ilab,     fcore,
     & ibitv,  buf,    nrec,     ierr)
      return
c
      end
c
c*******************************************************************
c
      subroutine wtden2(
     & ntape,  info,   lbuf,   nipbuf,
     & buf,    ilab,   val,    d1,
     & nsym,   irx1,   irx2,   map,
     & itypea, itypeb, endmark)
       implicit none
c
C    VARIABLE DECLARATIONS FOR subroutine pstclc
C
C     Parameter variables
c
c     # sifs continuaton flag values
      integer   msame,   nmsame,  nomore
      parameter(msame=0, nmsame=1,nomore=2)
c

      integer    ifmt,     ibvtyp
      parameter( ifmt = 0, ibvtyp = 0)
c
      integer   nipv
      parameter(nipv=2)
C
      INTEGER             nmotx
      PARAMETER           (nmotx = 1023)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 14)
      INTEGER             MXINOR
      PARAMETER           (MXINOR = 128)
      INTEGER             MAXREP
      PARAMETER           (MAXREP = 20)
      INTEGER             MXRDIM
      PARAMETER           (MXRDIM = 9)
      INTEGER             IPRPMX
      PARAMETER           (IPRPMX = 130305)
      INTEGER             NCIOPT
      PARAMETER           (NCIOPT = 10)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXSIZE
      PARAMETER           (MXSIZE = 20)
C
      REAL*8              ZERO, ONE, TWO
      PARAMETER           (ZERO = 0.0D0,ONE = 1.0D0,TWO = 2.0D0)
      REAL*8              THRESH
      PARAMETER           (THRESH = 1D-11)
C
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer tape6,nslist,filden
      equivalence (nunits(1),tape6)
      equivalence (nunits(3),nslist)
      equivalence (nunits(25),filden)
C
      INTEGER             BLST1E(8),   CNFW(8),     CNFX(8)
      INTEGER             LMDA(8,8),   LMDB(8,8),   MNBAS,       NBAS(8)
      INTEGER             NMBPR(8),    SCRLEN ,  NMSYMX
      INTEGER             STRTBW(8,8), STRTBX(8,8), SYM12(8)
      INTEGER             SYM21(8),    SYMTAB(8,8)
      COMMON / DATA   /   SYMTAB,      SYM12,       SYM21,       NBAS
      COMMON / DATA   /   LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON / DATA   /   STRTBX,      BLST1E,      NMSYMX,       MNBAS
      COMMON / DATA   /   CNFX,        CNFW,        SCRLEN
c
      REAL*8 d1_sym(iprpmx),d1ci_sym(iprpmx),d1_asym(iprpmx),
     &       d1ci_asym(iprpmx)
      COMMON / DEN1P  / d1_sym, d1ci_sym, d1_asym, d1ci_asym
c
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C
      integer mult(8,8)
      common /csym/ mult
C
      INTEGER             BLXT(MAXSYM),             N2OFFR(MAXSYM)
      INTEGER             N2OFFS(MAXSYM),           N2ORBT
      INTEGER             NOFFR(MAXSYM),            NOFFS(MAXSYM)
      INTEGER             NSGES(MAXSYM), nsdim(maxsym)
      integer    kntin (maxsym*(maxsym+1)/2)
      integer    symoff(maxsym*(maxsym+1)/2)
      integer    icnt1
      COMMON / OFFS   /   N2ORBT,      BLXT,        NSGES,       NOFFS
      COMMON / OFFS   /   N2OFFS,      NOFFR,       N2OFFR, nsdim
      Common / offs   /   icnt1,symoff,kntin
C
      INTEGER             EQVSYM(MAXSYM),           IALOFF,      IALPR
      INTEGER             IOUT(nmotx),             IVOUT(nmotx)
      INTEGER             LOWDOC,      MODRT(MXINOR)
      INTEGER             NBFNSM(MAXSYM),           NFRC(MAXSYM)
      INTEGER             NFRV(MAXSYM),             NMUVAL
      INTEGER             NOCC(MAXSYM),             NORBSM(MAXSYM)
      INTEGER             NOXT(MAXSYM)
      COMMON / PDINF  /   IOUT,        IVOUT,       MODRT
      COMMON / PDINF  /   EQVSYM,      NFRC,        NOCC,        NOXT
      COMMON / PDINF  /   NFRV,        LOWDOC,      NMUVAL,      IALPR
      COMMON / PDINF  /   NORBSM,      NBFNSM,      IALOFF
C
      CHARACTER*4         SYMLABEL(MAXSYM)
      COMMON / SLABEL /   SYMLABEL
c
      integer nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
      common /indata/
     . nroot1,nroot2,lvlprt,method,drt1,drt2,cirestart
c
c  ##  integer section
c
      integer code
      INTEGER DUM, DUM1(MAXSYM),DUM2(MAXSYM),dsym
      integer endmark
      INTEGER FILERR
      INTEGER IENERGY(MXSIZE), i, i1,ii,ij,in,INFO(MXSIZE), INMAX,
     &        INMIN,IOC,IOCMAX,IOCMIN,ISKIP,iii,icnt2,icnt3,isym,
     &        itypea,itypeb,ierr,iroot,icount,i2,iknt,
     &        irx1(*),irx2(*),ilab(nipv,*),ibitv
      integer j
      INTEGER K,KL,kntout(maxsym*(maxsym+1)/2)
      integer L,loc1,loc2,loc3,loc4,loc5,lsym,lstflg,l1rec,l2rec,
     &        lfree,last,lbuf
      INTEGER M, MM, MXREAD,mapout(nmotx),map(*)
      integer NBMT,NBMT1, NENRGY,NFC, NGES,NGES1, NGG, NINFO,NMSYMN,
     &        NN, NOCOEF, NORB, NSGES1, NTIT, NTITLE,nmax1,nmax2,
     &        nmap,nrec,numtot,NMSYM,nipbuf,nsym,ntape
      integer offset,offsetl(8)
      integer p,pq
      integer q
      INTEGER SYSERR
c
c  ##  real*8 section
c
      real*8 buf(lbuf)
      REAL*8 core(100000)
      real*8 d1(*)
      REAL*8 ELNUM,energy(10)
      REAL*8 dasum_wr
      real*8 fcore
      real*8 summd
      real*8 val(*)
c
c  ##  character section
c
      CHARACTER*80        TITLE(MXTITL)
      CHARACTER*8         BFNLAB(nmotx), molab(nmotx)
c
c  ## external section
c
      integer atebyt,forbyt
      external atebyt,forbyt
      integer nndxf
      nndxf(i)= i*(i-1)/2
c
c---------------------------------------------------------------------

c SIF:specification of the various ietypes
      numtot = 0
c SIF:fcore contribution gets computed in tran for now.
      fcore   = 0.0
c
      elnum = zero
      call izero_wr (nndxf(maxsym),kntin,1)
      call izero_wr (nndxf(maxsym),symoff,1)

c
c count space for all possible blocks

1010   format(10('---'),'Final Density for SYMBLOCK ',i2,i2,10('---'))
         icnt1 = 0
         icnt3 = 0
         do i=1,nmsymx
          offsetl(i)=icnt3
         icnt3=icnt3+nsdim(i)
            do kl=1,nsdim(i)
            write(molab(offsetl(i)+kl),'(a2,i3,a3)')
     .        'MO',kl,symlabel(i)(2:4)
            enddo
         enddo

         dsym=mult(ssym1,ssym2)
         do i = 1,nmbpr(dsym)
c
c         isym >= lsym
c
            isym=lmdb(dsym,i)
            lsym=lmda(dsym,i)


            if (isym.eq.lsym) then
            icnt1=icnt1+ (nsdim(isym)*(nsdim(isym)+1)/2)
            else
            icnt1=icnt1+nsdim(isym)*nsdim(lsym)
            endif
         enddo ! end of:  do i = 1,nmbpr(dsym)
c
c space for the complete transition density

c
c core  loc1 : density(icnt1)

c        loc1 = 1
c        loc2 = loc1+atebyt(icnt1)
c        loc3 = loc1+atebyt(icnt1)

c        if (loc3.gt.lfree) then
c          call bummer ('insufficient space,lfree=',lfree,2)
c        endif
c        call wzero(icnt1,core(loc1),1)
c
      last = msame
      iknt=0
      kl=0
      pq=0
c
        offset=0
      do 180 i = 1,nmbpr(dsym)
           isym=lmdb(dsym,i)
           lsym=lmda(dsym,i)
         if ( (nsdim(isym)*nsdim(lsym)).gt.0) then
           kntin(nndxf(isym)+lsym)=1
           symoff(nndxf(isym)+lsym)=offset+1
c--------------------------------------------------------------
           if (isym.eq.lsym) then
c
c integrals are lower triangular packed
c den1 represents the lower triangular packed density
c
c      1
c      2 3
c      4 5 6
c      7 8 9 10
c
       ij = nsdim(isym)*(nsdim(isym)+1)/2
c
c        write(*,*)'p:',irx1(isym),irx2(isym)
         do 1100 p=irx1(isym),irx2(isym)
         if (map(p).eq.-1) go to 1100
            do 1200 q=irx1(isym),p
            if (map(q).eq.-1) go to 1200
               pq=pq+1
               if(iknt.eq.nipbuf)then
c                 # if the rare event happens that this is the last time
c                 # through the nested loops and iknt.eq.nipbuf then
c                 # set last=nomore
                  if ((isym .eq. nsym) .and. (p .eq. irx2(nsym)) .and.
     +                (q .eq. p)) then
                     last = endmark
                  else
                     last = msame
                  endif
                  numtot = numtot + iknt
                  call sifew1(
     &              ntape,   info,    nipv,    iknt,
     &              last,    itypea,  itypeb,  
     &              ibvtyp,  val,     ilab,    fcore,
     &              ibitv,   buf,     nrec,    ierr)
                  if ( ierr .ne. 0 )
     &               call bummer('wtden1:  sifew1, ierr=', ierr, faterr)
                  numtot = numtot - iknt
                  iknt = 0
               endif
               if (last .eq. endmark) return
c              if (abs(d1(pq)).gt.small) then
                iknt=iknt+1
                val(iknt)=d1(pq)
                ilab(1,iknt)=map(p)
                ilab(2,iknt)=map(q)
c              endif
 1200       continue ! do q=1,irx1(isym),p
 1100    continue ! end of: do p=irx1(isym),irx2(isym)
c      call dcopy_wr(ij,d1_sym(offset+1),1,core(loc1+offset),1)
c      call plblkc('symm-trans density matrix: diagonal part',
c    .      core(loc1+offset),nsdim(isym),molab(offsetl(isym)+1),1,6)
c
       offset=offset+nsdim(isym)*(nsdim(isym)+1)/2

c--------------------------------------------------------------

            else

c integrals
c                  isym
c
c            1  2  3
c            4  5  6
c            7  8  9
c    jsym   10 11 12
c
c
c  d1_sym(lsym,isym) : isym > lsym
c
c  core:  upper triangle
c  integrals are now packed as rectangular arrays with ARRAY(j,i)
c  with i>j ,i.e j lines of length i
c
c       ij = nsdim(isym)*nsdim(lsym)
c
      last = msame
c        write(*,*)'p:',irx1(lsym),irx2(lsym)
c        write(*,*)'q:',irx1(isym),irx2(isym)
         do 2100 p=irx1(lsym),irx2(lsym)
         if (map(p).eq.-1) go to 2100
            do 2200 q=irx1(isym),irx2(isym)
            if (map(q).eq.-1) go to 2200
               pq=pq+1
               if(iknt.eq.nipbuf)then
c                 # if the rare event happens that this is the last time
c                 # through the nested loops and iknt.eq.nipbuf then
c                 # set last=nomore
                  if ((isym .eq. nsym) .and. (p .eq. irx2(nsym)) .and.
     +                (q .eq. p)) then
                     last = endmark
                  else
                     last = msame
                  endif
                  numtot = numtot + iknt
c                 write(*,*)'1: calling sifew1'
                  call sifew1(
     &              ntape,   info,    nipv,    iknt,
     &              last,    itypea,  itypeb, 
     &              ibvtyp,  val,     ilab,    fcore,
     &              ibitv,   buf,     nrec,    ierr)
                  if ( ierr .ne. 0 )
     &               call bummer('wtden1:  sifew1, ierr=', ierr, faterr)
                  numtot = numtot - iknt
                  iknt = 0
               endif
               if (last .eq. endmark) return
c              if (abs(d1(pq)).gt.small) then
                iknt=iknt+1
c              write(*,*)'offdiag:isym,p,q,d1(pq)',isym,p,q,d1(pq)
c              write(*,*)'map(p),map(q)',map(p),map(q)
                val(iknt)=d1(pq)
                ilab(1,iknt)=map(q)
                ilab(2,iknt)=map(p)
c              endif
 2200       continue ! end of: do q=1,irx1(lsym),irx2(lsym)
 2100       continue ! end of: do p=irx1(isym),irx2(isym)
c
       offset=offset+nsdim(isym)*nsdim(lsym)
       endif

c--------------------------------------------------------------

            end if

 180   continue
c
c dump the rest of the d1 matrix elements
c
      last = endmark
      numtot = iknt + numtot
c     write(*,*)'2: calling sifew1',iknt
      call sifew1(
     & ntape,  info,   nipv,     iknt,
     & last,   itypea, itypeb,   
     & ibvtyp, val,    ilab,     fcore,
     & ibitv,  buf,    nrec,     ierr)
c
          return

 9000 format (14a4)
 9010 format (/,' maximal number of new irreps',i6)
 9020 format (/,' irrep        :',i4)
 9030 format ('    dimension         :',i6)
 9040 format ('       direction         :',i4)
 9050 format ('       number of orbitals:',i4)
 9060 format (/,/,' vectors are read with a format of: ',a80,/,/)
 9070 format (/,/,' vectors are read from mcscf restart tape')
c
 9080 format (/,1x,' eigenvectors of nos in mo-basis')
 9090 format (1x,' eigenvectors of nos in ao-basis, column ',i4)
 9100 format (a80)
 9110 format (19a4)
 9120 format (' output from ci calculation')
 9130 format (/,/,' first order reduced density matrix in mo basis',/,/)
 9140 format (/,/,' first order reduced density matrix in mo basis',
     +       ' according to new symmetry',/,/)
 9150 format (/,/,'*****   symmetry block ',a4,'   *****')
 9160 format (/,/,'*****   irrep nr ',i4,'  symmetry block nr ',i4,
     +       '   *****')
 9170 format (/,' occupation numbers of nos')
 9180 format (1x,8e15.7)
 9190 format (1x,'dimension for d1_sym too small in routine densi ',
     +       ' nges = ',i5,'    iprpmx = ',i5)
 9200 format (a4)
 9210 format (20i4)
 9220 format (6f12.9)
 9230 format (8e15.7)
 9240 format (5(f12.7,3x))
 9250 format (/,/,' total number of electrons = ',f15.10,/)
c9260 format (1x,'output file for properties is ',a8,/)

      end
c
c****************************************************************
c
      subroutine  wtlabel(slabel,nsym,fmt,buf)
      implicit none
      integer nsym
      integer i
      character*4 slabel(nsym),buf(nsym)
       character*(*) fmt
      do 10 i = 1, nsym
         write( slabel(i), '(a4)' ) buf(i)
10    continue
      return
      end

      subroutine symwlk_noso(
     . niot,   nsym,   nrow,   nrowmx,
     . nwalk,  l,      y,      row,
     . step,   csym,   wsym,   arcsym,
     . ytot,   xbar,   ssym,
     . mult,   limvec, ncsft,
     . nviwsm, ncsfsm,  b, numv1  )
c
c  this routine calculates the symmetry of all the valid internal
c  walks and computes csf counts.
c
c  output:
c  csym(*) = mult( internal_walk_symmetry, ssym ) for
c            the valid z,y,x,w-walks.
c  ncsft = total number of csfs for this orbital basis and drt.
c  nviwsm(1:4,1:nsym) = number of valid internal walks of each symmetry
c                       through each vertex.
c  ncsfsm(1:4,1:nsym) = number of csfs passing through each vertex
c                       indexed by the internal symmetry.
c
c  07-jun-89  skip-vector based walk generation. rls
c
      implicit none
c====>Begin Module SYMWLK                 File cidrt2.f
c---->Makedcls Options: All variables
c
c     Parameter variables
c
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
c
c     Argument variables
c
c
      INTEGER             ARCSYM(0:3,0:NIOT),       B(*),        CSYM(*)
      INTEGER             L(0:3,NROWMX,3),          LIMVEC(*)
      INTEGER             MULT(8,8),   MULTMX,      NCSFSM(4,NSYM)
      INTEGER             NCSFT,       NIOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVIWSM(4,NSYM)
      INTEGER             NWALK,       ROW(0:NIOT)
      INTEGER             SSYM,        STEP(0:NIOT)
      INTEGER             WSYM(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3),          YTOT(0:NIOT)
c
c
c     Local variables
c
      CHARACTER           WXYZ(4)
c
      INTEGER             CLEV,        I,           IMUL,        ISSYM
      INTEGER             ISTEP,       ISYM,        MUL,         NLEV
      INTEGER             NUMV1,       NVALWT,      ROWC,        ROWN
      INTEGER             VER,         VER1,        VERS(4)
c
c====>End Module   SYMWLK                 File cidrt2.f
      data vers/1,2,3,3/
      data wxyz/'z','y','x','w'/
c
c
c     # limvec(*) in skip-vector form.
c
      nvalwt = 0
c
      call izero_wr( 4*nsym, nviwsm, 1 )
c
c     # loop over z, y, x, and w vertices.
c
      do 200 ver = 1, 4
         ver1 = vers(ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
         wsym(0) = 1
c
         do 50 i = 0, niot
            step(i) = 4
50       continue
c
100      continue
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)go to 180
            clev = clev-1
            goto 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         row(nlev) = rown
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
         if( limvec( ytot(nlev)+1 ) .ge. xbar(rown,ver1) )goto 100
         wsym(nlev) = mult( arcsym(istep,clev), wsym(clev) )
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         endif
c        # a complete valid walk has been generated.
         isym = wsym(nlev)
         issym = mult(isym,ssym)
         nvalwt = nvalwt+1
         csym(nvalwt) = issym
         nviwsm(ver,isym) = nviwsm(ver,isym) + 1
c
         go to 100
c
180      continue
c        # finished with walk generation for this vertex.
200   continue
c
c     # check the valid upper walk count...
      if(numv1.ne.nvalwt)
     . call bummer('symwlk: numv1-nvalwt=', numv1-nvalwt,0)
c
      return
c
6100  format(1x,a,':',(8i8))
      end


