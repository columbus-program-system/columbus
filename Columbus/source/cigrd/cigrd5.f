!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccigrd5.f
ccigrd part=5 of 6.  misc. cigrd routines.
cversion=1.0 last modified: 27-aug-90
c deck add1
      subroutine add1
c
c  compute symmetry arrays and orbital mapping arrays.
c
c  input:
c  nsym = number of symmetry blocks.
c  nmpsy(*) = number of orbitals in each symmetry block.
c  mctype(*) = mcscf orbital type of each orbital
c              (fc=0,d=1,a=2,v=3,fv=4).
c
c  version:
c  feb-90 bug in intoff_ci calculation fixed,
c         nbmt construction added (m.e.)
c  25-aug-87 written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer dblocc,active,virtl
      parameter(dblocc=1,active=2,virtl=3)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nbmt,nsm(8),nnsm(8),n2sm(8),irm1(8),
     &        irm2(8),nmot,nnmt,n2mt,nmpsy(8),nsd(8),ndpsy(8),
     &        nnsd(8),n2sd(8),ird1(8),ird2(8),ndmot,nndt,n2dt,
     &        nnsa(8),nnat,ira1(8),ira2(8),n2sa(8),namot,napsy(8),
     &        n2at,nsa(8),nnsv(8),nnvt(8),irv1(8),irv2(8),
     &        n2sv(8),nvmot,nvpsy(8),n2vt,nsv(8),tsymad(8),
     &        tsymvd(8),tsymva(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxy(1,11),nnsd(1))
      equivalence (nxy(1,12),n2sd(1))
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxy(1,19),nnsv(1))
      equivalence (nxy(1,20),n2sv(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,35),irm1(1))
      equivalence (nxy(1,36),irm2(1))
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(5),nnmt)
      equivalence (nxtot(6),n2mt)
      equivalence (nxtot(7),ndmot)
      equivalence (nxtot(8),nndt)
      equivalence (nxtot(9),n2dt)
      equivalence (nxtot(10),namot)
      equivalence (nxtot(11),nnat)
      equivalence (nxtot(12),n2at)
      equivalence (nxtot(13),nvmot)
      equivalence (nxtot(14),nnvt)
      equivalence (nxtot(15),n2vt)
      equivalence (nxtot(16),nbmt)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
      integer nadt,nvdt,nvat
      equivalence (tsymad(1),nadt)
      equivalence (tsymvd(1),nvdt)
      equivalence (tsymva(1),nvat)
c
c   ##   integer section
c
      integer i,isym,ioff
      integer jsym
      integer ntype(3),ni
      integer uvsym,ut,utype,u
c
c  mapmm(*) is used as an identity mapping vector.
c
      do 100 i=1,nmomx
         mapmm(i)=i
100   continue
      call izero_wr(8,ndpsy,1)
      call izero_wr(8,napsy,1)
      call izero_wr(8,nvpsy,1)
      call izero_wr(8,nipsy,1)
      nimot=0
      nvmot=0
      ndmot=0
      namot=0
      ut=0
      do 300 isym=1,nsym
         do 200 u=1,nmpsy(isym)
            ut=ut+1
            symm(ut)=isym
            utype=mctype(ut)
            if(utype.eq.dblocc)then
               ndpsy(isym)=ndpsy(isym)+1
               ndmot=ndmot+1
               mapmd(ndmot)=ut
               symd(ndmot)=isym
               nimot=nimot+1
               mapivm(ut)=nimot
               mapmi(nimot)=ut
               symi(nimot)=isym
            elseif(utype.eq.active)then
               napsy(isym)=napsy(isym)+1
               namot=namot+1
               mapma(namot)=ut
               syma(namot)=isym
               nimot=nimot+1
               mapivm(ut)=nimot
               mapmi(nimot)=ut
               symi(nimot)=isym
            elseif(utype.eq.virtl)then
               nvpsy(isym)=nvpsy(isym)+1
               nvmot=nvmot+1
               mapivm(ut)=nvmot
               mapmv(nvmot)=ut
               symv(nvmot)=isym
            else
               call bummer('add1: invalid orbital type',utype,faterr)
            endif
200      continue
300   continue
c
cfp
c compute the symmetries for the orbital arrangement in fockm2
c if not samcgrad, else
c      do p=1,nimot
c         symgrd(u)=symi(u)
c      enddo
c      do u=nimot+1,nmot
c         symgrd(u)=symv(u-nimot)
c      enddo
c
c  compute the number of internal orbitals.
c
      do 400 i=1,nsym
         nipsy(i)=ndpsy(i)+napsy(i)
400   continue

      write(6,'(a,t20,8i4)') 'docc:',ndpsy(1:nsym)
      write(6,'(a,t20,8i4)') 'active:',napsy(1:nsym)
      write(6,'(a,t20,8i4)') 'internal:',nipsy(1:nsym)
      write(6,'(a,t20,8i4)') 'virtual:',nvpsy(1:nsym)
c
c  compute general symmetry offset arrays.
c
      call addx1(8,nmpsy,nmot, nsm,n2mt,n2sm,nnmt,nnsm,irm1,irm2)
      call addx1(8,ndpsy,ndmot,nsd,n2dt,n2sd,nndt,nnsd,ird1,ird2)
      call addx1(8,napsy,namot,nsa,n2at,n2sa,nnat,nnsa,ira1,ira2)
      call addx1(8,nvpsy,nvmot,nsv,n2vt,n2sv,nnvt,nnsv,irv1,irv2)
      call addx1(8,nipsy,nimot,nsi,n2it,n2si,nnit,nnsi,iri1,iri2)
      call addx2(8,nmpsy,nipsy,nmit,nsmi)
      call addx2(8,nvpsy,nipsy,nvit,nsvi)
      call addx2(8,napsy,ndpsy,nadt,nsad)
      call addx2(8,nvpsy,ndpsy,nvdt,nsvd)
      call addx2(8,nvpsy,napsy,nvat,nsva)
c
c  compute intoff_ci(*,*) for fock matrix construction.
c  intoff_ci(isym,uvsym) = isym offset for uv-distribution of integrals.
c
      call izero_wr(8*8,intoff_ci,1)
      ioff=0
      do 500 isym=1,8
         intoff_ci(isym,1)=ioff
         ni=nmpsy(isym)
         ioff=ioff+((ni*(ni+1))/2)
500   continue
      do 520 uvsym=2,8
         ioff=0
         do 510 isym=1,8
            jsym=mult(isym,uvsym)
            if(jsym.lt.isym)then
cfp_todo: this should only be some internal offset
c  and can stay the same(?)
               intoff_ci(isym,uvsym)=ioff
               intoff_ci(jsym,uvsym)=ioff
               ioff=ioff+nmpsy(isym)*nmpsy(jsym)
            endif
510      continue
520   continue
c
c compute no construction  related symmetry arrays
c
      nbmt=0
      do 530 isym=1, nsym
        nbmt=nbmt+nmpsy(isym)*nbpsy(isym)
530   continue
      return
      end
c
c*********************************************************************
c
c deck add2
      subroutine add2(
     & niiiit,   nviiit,   nmmmmt,   naar,
     & offmm,    addmm,    offii1,   addii,
     & offii2,   addvi,    nhblk)
c
c  compute addressing arrays for density matrices, effective
c  density matrices, and hessian blocks.
c
c  version:
c  15-feb-88 written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
      integer dblocc,active,virtl
      parameter(dblocc=1,active=2,virtl=3)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmot,nvmot,tsymad(8),tsymvd(8),tsymva(8),nbmt
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(13),nvmot)
      equivalence (nxtot(16),nbmt)
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      integer nadt,nvdt,nvat
      equivalence (tsymad(1),nadt)
      equivalence (tsymvd(1),nvdt)
      equivalence (tsymva(1),nvat)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer numopt
      parameter(numopt=10)
c
      integer cigopt, print, fresdd, fresaa, fresvv, samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
c   ##  integer section
c
      integer apsym,a
      integer addmm(*),addii(*),addvi(nvmot,nimot)
      integer nmmmmt,nviiit,niiiit,nhblk,naar
      integer offmm(*),offii1(*),offii2(*)
      integer p,pq,pqsym
      integer q
      integer symt(8)
c
c  symtmm(*) and addmm(*)...
c  symmetry-blocked lower-triangle addressing by rows.
c
      call izero_wr(8,symtmm,1)
      pq=0
      do 120 p=1,nmot
         do 110 q=1,p
            pq=pq+1
            pqsym=mult(symm(q),symm(p))
            symtmm(pqsym)=symtmm(pqsym)+1
            addmm(pq)=symtmm(pqsym)
110      continue
120   continue
c
c  symtii(*) and addii(*)...
c  symmetry-blocked lower-triangle addressing by rows.
c
      call izero_wr(8,symtii,1)
      pq=0
      do 140 p=1,nimot
         do 130 q=1,p
            pq=pq+1
            pqsym=mult(symi(q),symi(p))
            symtii(pqsym)=symtii(pqsym)+1
            addii(pq)=symtii(pqsym)
130      continue
140   continue
c
c  addvi(*,*)...
c
      call izero_wr(8,symtvi,1)
      do 240 p=1,nimot
         do 230 a=1,nvmot
            apsym=mult(symv(a),symi(p))
            symtvi(apsym)=symtvi(apsym)+1
            addvi(a,p)=symtvi(apsym)
230      continue
240   continue
c
c  diiii(*)...
c  [pqrs] = offii1(pq) + addii(rs) , where (pq).ge.(rs).
c
      niiiit=0
      pq=0
      do 440 p=1,nimot
         do 430 q=1,p
            pq=pq+1
            offii1(pq)=niiiit
            niiiit=niiiit+addii(pq)
430      continue
440   continue
c
c  dviii(*)...
c  [arpq] = offii2(pq) + addvi(a,r)
c
      nviiit=0
      pq=0
      do 460 p=1,nimot
         do 450 q=1,p
            pqsym=mult(symi(q),symi(p))
            pq=pq+1
            offii2(pq)=nviiit
            nviiit=nviiit+symtvi(pqsym)
450      continue
460   continue
c
c  dmmmm(*) and gmmmm(*)...
c  [pqrs] = offmm(pq) + addmm(rs)  (2x expanded)
c
      nmmmmt=0
      pq=0
      do 480 p=1,nmot
         do 470 q=1,p
            pqsym=mult(symm(q),symm(p))
            pq=pq+1
            if(samcflag.gt.0)then
              if((mctype(p).eq.virtl).and.(mctype(q).eq.virtl))then
cfp: addressing changed in SA-MCSCF case because fewer integrals are considered
c  -> the 2-virt distributions are skipped
c                write(6,*) 'Skipping distribution',p,q
                offmm(pq)=-1
                goto 470
               endif
             endif
            offmm(pq)=nmmmmt
            nmmmmt=nmmmmt+symtmm(pqsym)
470      continue
480   continue
c
cfp
c addressing array for SA-MCSCF fock matrix construction
c only the 2-external distributions are left out
c everything else is kept - only the offset array changes
c  addmm stays
c
c  dgrd(*) and ggrd(*)...
c  [vqrs] = offgrd(pq) + addmm(rs)  (2x expanded)
c
c if not samcflag
c offgrd = offmm, 2 indices
c      ngrdt=0
c      pq=0
c      do 490 p=1,nmot
c         do 491 q=1,min(p,nimot)
c symgrd
c            pqsym=mult(symgrd(q),symgrd(p))
c            pq=pq+1
c            offgrd(pq)=ngrdt
c            ngrdt=ngrdt+symtmm(pqsym)
c491      continue
c490   continue
c
c  compute the number of nonzero hessian blocks.
c
      nhblk=0
      if(nadt.ne.0)nhblk=nhblk+1
      if(naar.ne.0)then
         nhblk=nhblk+1
         if(nadt.ne.0)nhblk=nhblk+1
      endif
      if(nvdt.ne.0)then
         nhblk=nhblk+1
         if(nadt.ne.0)nhblk=nhblk+1
         if(naar.ne.0)nhblk=nhblk+1
      endif
      if(nvat.ne.0)then
         nhblk=nhblk+1
         if(nadt.ne.0)nhblk=nhblk+1
         if(naar.ne.0)nhblk=nhblk+1
         if(nvdt.ne.0)nhblk=nhblk+1
      endif
c     # csf-related blocks...
      if (cdir.eq.0) then
      if(nadt.ne.0)nhblk=nhblk+1
       if(naar.ne.0)nhblk=nhblk+1
       if(nvdt.ne.0)nhblk=nhblk+1
       if(nvat.ne.0)nhblk=nhblk+1
      endif
      if (mdir.eq.0) then
       nhblk=nhblk+1
      endif
c
      return
      end
c deck add3
      subroutine add3(mapmd1,maplgv,map2i,
     & mapvv,mapii               )
c
c
c  input:
c
c  output:
c         mapmd1(*,*)= ii orbital index to square packed mcd1 index
c         maplgv(*,*)= vv orbital index to square packed lgmx index
c         mapii(*)   = square packed mcd1 index to absolut number of
c                      mcd1 element
c         mapvv(*)   = square packed lgmx index to absolut number of
c                      mcd1 element
c         map2i(*)   = ii orbital index to absolut number of square
c                      packed internal orbital matrix
c
c  (m.e) jun 90
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nsm(8),n2sm(8),n2mt,irv1(8),nvmot
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(6),n2mt)
      equivalence (nxtot(13),nvmot)
      equivalence (nxy(1,41),irv1(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c   ##   integer section
c
      integer indmo,indwx,induv
      integer mapvv(n2mt),mapii(n2mt),mapmd1(nimot,nimot),
     &        maplgv(nvmot,nvmot),map2i(nimot,nimot)
      integer numvv,numii
      integer u20,usym,u
      integer v
      integer w,w20,wsym
      integer x
c
      call izero_wr(nimot*nimot,mapmd1,1)
      call izero_wr(nvmot*nvmot,maplgv,1)
      call izero_wr(nimot*nimot,map2i,1)
      call izero_wr(n2mt,mapvv,1)
      call izero_wr(n2mt,mapii,1)
c
c  maplgv(*,*)=addressing array for the lgm in the virtual orbital space
c
      numvv=0
      do 120 w=1,nvmot
         wsym=symv(w)
         indmo=mapmv(w)-nsm(wsym)
         w20=n2sm(wsym)+(indmo-1)*nmpsy(wsym)-nsm(wsym)
         do 110 x=irv1(wsym),w
            numvv=numvv+1
            indwx=w20+mapmv(x)
            mapvv(indwx)=numvv
            maplgv(w,x)=indwx
110      continue
120   continue
c
c  mapmd1(*,*)=addressing array for mcd1 in the active orbital space
c
      numii=0
      do 140 u=1,nimot
         usym=symi(u)
         indmo=mapmi(u)-nsm(usym)
         u20=n2sm(usym)+(indmo-1)*nmpsy(usym)-nsm(usym)
         do 130 v=iri1(usym),u
            numii=numii+1
            induv=u20+mapmi(v)
            mapii(induv)=numii
            mapmd1(u,v)=induv
130      continue
140   continue
c
c
c
      do 160 u=1,nimot
         usym=symi(u)
         u20=n2si(usym)
         do 150 v=iri1(usym),iri2(usym)
            map2i(u,v)=u20+(u-1)*nipsy(usym)+v
150      continue
160   continue
c
      return
      end
c deck addx1
      subroutine addx1(nsym,npsy,ntot,ns,n2t,n2s,nnt,nns,ir1,ir2)
c
c  compute symmetry offset arrays involving one orbital type.
c
      implicit integer(a-z)
      integer npsy(nsym),ns(nsym),n2s(nsym),nns(nsym)
      integer ir1(nsym),ir2(nsym)
c
      ntot=0
      n2t=0
      nnt=0
      do 10 i=1,nsym
         n=npsy(i)
c
         ns(i)=ntot
         n2s(i)=n2t
         nns(i)=nnt
         ir1(i)=ntot+1
         ir2(i)=ntot+n
c
         ntot=ntot+n
         n2t=n2t+n*n
         nnt=nnt+(n*(n+1))/2
10    continue
      return
      end
c deck addx2
      subroutine addx2(nsym, nxpsy, nypsy, nxyt, nsxy)
c
c  compute symmetry offset array involving two orbital types.
c
      implicit integer(a-z)
      integer nxpsy(nsym),nypsy(nsym),nsxy(nsym)
c
      nxyt=0
      do 10 i=1,nsym
         nsxy(i)=nxyt
         nxyt=nxyt+nxpsy(i)*nypsy(i)
10    continue
      return
      end
cigrd3.f
cigrd part=3 of 6.  fock matrix construction routines
cnversion=1.0 last modified: 27-aug-90
c deck atotal
      subroutine atotal(n2mt,qfresd,qfresa,qfresv,qresaf,atot,fqa)
c
c  compute atot(*) = 2 * ( ano(*) + fqa(*) )
c
c  input:
c  n2mt = matrix lengths.
c  qfresd = fqa(*) contributions flag.
c  qfresa = ano(*) contributions flag.
c  qresaf = indicates active orbital resolution type
c  qfresv = indicates if virtual orbital resolution is required
c  atot(*) = ano(*) if qfresa=.true., otherwise undefined.
c  fqa(*) = fqa(*) matrix if qfresd=.true., otherwise undefined.
c
c  output:
c  atot(*) = updated atot(*) matrix.
c
      implicit integer(a-z)
c
      logical qfresd,qfresa,qfresv,qresaf
      real*8 atot(*),fqa(*)
c
      real*8    one,    two
      parameter(one=1d0,two=2d0)
c
      if (.not.qresaf.and.qfresa) then
         call dscal_wr(n2mt,two,atot,1)
         if (qfresd.or.qfresv) then
            call daxpy_wr (n2mt,two,fqa,1,atot,1)
         end if
      else if (qfresa.or.qfresv.or.qfresd) then
ctm      call scopy(n2mt,zero,0,atot,1)
         call wzero(n2mt,atot,1)
         call daxpy_wr(n2mt,two,fqa,1,atot,1)
      end if
c
      return
      end
c
c******************************************************************
c
c deck bh15
      subroutine bh15(ncsf,b,ist)
c
c  construct the csf-csf blocks of the hessian matrix from the
c  hamiltonian matrix.
c    m(ij) = 2*(h(ij)-emc*delta(ij))
c
       implicit none
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      real*8    two
      parameter(two=2d0)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer ist, i, ii, ipos
      integer j
      integer nncsf, ncsf
c
      real*8 b(*)
c
      integer nndx
      nndx(i)=(i*(i+1))/2
c
c------------------------------------------------------------
c
      nncsf=nndx(ncsf)
      call dscal_wr(nncsf,two,b,1)
c
c  #  copy the H matrix to all M matrix blocks
         do i=1,navst(ist)-1
         ipos = nncsf*i + 1
         call dcopy_wr(nncsf,b,1,b(ipos),1)
         enddo ! i=1,navst(ist)-1
c
c  #  modiffy the diagonal for each block according to:
c  #    m(ij) = 2*(h(ij)-emc*delta(ij))
         do i=1,navst(ist)
         ipos = (i-1)*nncsf
            do j=1,ncsf_f(ist)
            ii = ipos + nndx(j)
            b(ii)=b(ii)-two*heig(ist,i)
            enddo ! j=1,ncsf_f(ist)
         enddo ! i=1,navst(ist)
c
c  #  weight the individual M matrix bloks:
         do i=1,navst(ist)
         ipos = (i-1)*nncsf + 1
         call dscal_wr(nncsf,wavst(ist,i),b(ipos),1)
         enddo ! i=1,navst(ist)
c
      return
      end
c deck cdspv
      subroutine cdspv(n,a,d)
c
c  copy the diagonal elements from the symmetric, lower-
c  triangular-packed matrix a(*) to the vector d(*).
c
c  written by ron shepard.
c  version: 14-june-88
c
      implicit integer(a-z)
c
      real*8 a(*),d(*)
c
*@if defined( t3e64 ) || defined ( cray )
*!Cwarum dieser Unterschied zwischen cray und sonst
*      do 10 i=1,n
*         d(i)=a( (i*(i+1))/2 )
*10    continue
*@else
      ii=0
      do 10 i=1,n
         ii=ii+i
         d(i)=a(ii)
10    continue
*@endif
      return
      end
cigrd6.f
cigrd part=6 of 6.  misc. cigrd utility routines.
cversion=1.0 last modified: 04-apr-91
c deck checks
      subroutine checks(s,nsym,npsy)
c
c  check the one-electron overlap matrix s(*).
c
      implicit integer(a-z)
      real*8 s(*)
      integer npsy(nsym)
c
      real*8    one,      small
      parameter(one=1.d0, small=1.d-10)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      numerr=0
      ij=0
      do 30 isym=1,nsym
         n=npsy(isym)
         do 20 i=1,n
            do 10 j=1,(i-1)
               ij=ij+1
               if(abs(s(ij)).gt.small)numerr=numerr+1
10          continue
            ij=ij+1
            if(abs(s(ij)-one).gt.small)numerr=numerr+1
20       continue
30    continue
      if(numerr.ne.0)call bummer('checks: numerr=',numerr,faterr)
c
      return
      end
c
c********************************************************************
c
c deck ciener
      subroutine ciener (repnuc,norm_orbl,norm_csfl,energy,lheuristic)
c
c (m.e.)
c
       implicit none
      integer nfilmx
      parameter(nfilmx=31)
c
      integer nmomx
      parameter(nmomx=1023)
c
      real*8 energy,energy1,energy2
      integer nadcalc,drt1,drt2,nroot1,nroot2
      common /nad/ energy1,energy2,nadcalc,drt1,drt2,nroot1,nroot2
c
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 esym1,eesym1,esym2,eesym2
      common/ener/ esym1(8),eesym1(8),esym2(8),eesym2(8)
c
      integer numopt
      parameter(numopt=10)
c
      integer cigopt, print, fresdd, fresaa, fresvv, samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
      real*8 dsym,eci,eeff,repnuc,norm_csfl,norm_orbl
      real*8 out_tr, out_tr_eff
      integer isym,ierr
      character*7 wfprog
      logical lheuristic
c
      if(samcflag.eq.0)then
         wfprog='ciudg.x'
      else
         wfprog='mcscf.x'
      endif
      write(nlist,6060)
6060  format(/1x,60('*'))
      write(nlist,'(/,8x,''***  RESULTS:  ***''/)')
      write(nlist,'(5x,''norm of Lambda(orb) = '',f15.8)') norm_orbl
      write(nlist,'(5x,''norm of Lambda(csf) = '',f15.8/)') norm_csfl

      eci=(0)
      eeff=(0)
      do 100 isym=1, nsym
         dsym=eesym1(isym)-esym1(isym)
         eci=eci+esym1(isym)+(esym2(isym)-esym1(isym))/2
         eeff=eeff+eesym1(isym)+(eesym2(isym)-eesym1(isym))/2
         write(nlist,1000)isym,dsym
100   continue
        out_tr_eff=0.0d0
        out_tr=0.0d0
       do isym=1,nsym
         out_tr_eff=eesym1(isym)+out_tr_eff
         out_tr=esym1(isym)+out_tr
       enddo
       write(nlist,1002),out_tr_eff,out_tr,out_tr_eff-out_tr
1002   format('tr(d1(eff)*h)=',f20.10/
     .        'tr(d1*h)     =',f20.10/
     .        'tr((d1(eff)-d1)*h)=',f20.10)



c
       if (nadcalc.eq.0) then
        write(nlist,1010) eci  + repnuc
        write(nlist,1020) eeff + repnuc
        write(nlist,1035) wfprog,energy
       else
        write(nlist,1015) eci
        write(nlist,1025) eeff
       endif
      write(nlist,1030) abs(eeff-eci)

c
c     # check whether calculated ci energy agrees with the
c     # energy stored in the ci density file
c     # not for transition densities
c

        if (lheuristic) then
          write(nlist,1037) wfprog,abs(eci+repnuc-energy)
          write(nlist,1036) wfprog,abs(eeff+repnuc-energy)
          write(nlist,*)  
     .   'This result was derived from an heuristic MRCI+Q density!'
          call bummer('terminating',0,2)
        endif  


      if (nadcalc.eq.0) then
        write(nlist,1037) wfprog,abs(eci+repnuc-energy)
        write(nlist,1036) wfprog,abs(eeff+repnuc-energy)
        ierr=0
        if (abs(eci+repnuc-energy).gt.1.d-6) then
          write(6,'(a,f12.8)') 
     .    'CI energy deviates from eci by more than 1.d-6' //
     .    ' - this indicates incorrect density matrices!',
     .    eci+repnuc-energy
          ierr=ierr+1
        endif
        if (abs(eci-eeff).gt.5.d-5) then
          write(6,'(a,f12.8)') 
     .    'eci deviates from eci(eff) by more than 5.d-5', 
     .    eci-eeff 
          if (ierr.eq.0) 
     .    write(6,'(a)') ' This indicates incorrect effective' //
     .     ' density matrix construction '
          ierr=ierr+1
        endif
        if (abs(eeff+repnuc-energy).gt.5.d-5) then
          write(6,'(a,f12.8)') 'eci(eff) deviates by more than 5.d-5'//
     .                         ' from the ci value:',
     .                          eeff+repnuc-energy
          ierr=ierr+1
        endif
        if (ierr.gt.0) 
     .    call bummer('inconsistent energies!',ierr,2)
       else
c       # check whether total energy is zero (transition densities)
         ierr=0
          if (abs(eci).gt.1.d-6) then 
            write(6,*) 'eci does not vanish for transition densities'//
     .                  ' this indicates incorrect trans. densities!'
             ierr=ierr+1
          endif
          if (abs(eeff).gt.5.d-5) then 
           write(6,*) 'eeff does not vanish for eff. trans. densities'//
     .                'this indicates incorrect eff. trans. densities!'
             ierr=ierr+1
          endif
        if (ierr.gt.0) 
     .    call bummer('inconsistent transition densities!',ierr,2)
        endif  

      


1000  format((5x,'isym=',i2,3x,'tr(d1(eff)*h) - tr(d1*h)= ',1pf20.12))
1010  format(/5x,' eci = tr(d1*h) + tr(d2*g) + repnuc = ',t60,f20.12)
1015  format(/5x,' eci = tr(d1*h) + tr(d2*g) = ',t60,f20.12)
1020  format(5x,' eci(eff) = tr(d1eff * h) + tr(d2eff * g)'
     & ,' + repnuc = ',t60,f20.12)
1025  format(5x,' eci(eff) = tr(d1eff * h) + tr(d2eff * g) = ',t60,
     & f20.12)
1030  format(/5x,' abs(eci - eci(eff))       = ',1pe11.4)
1035  format(5x,' eci(',A,')              = ',t60,f20.12)
1036  format(/5x,' abs(eci(',A,')-eci(eff))= ',1pe11.4)
1037  format(/5x,' abs(eci(',A,')-eci)= ',1pe11.4)
c
      return
      end
c deck comlgm
      subroutine comlgm(
     & qfresd,  qfresv,  qresaf,  anofqa,
     & lgmx,    af,      n2mt )
c
c    calculate the final lagrange multiplier x matrix
c              lgmx(*) = ado(*) + af(*) + anofqa(*)
c
c  input:
c         qfresd= true if inactive orbital resolution is required
c         qfresv= true if virtual orbital resolution is required
c         qresaf= true if active orbital av.f. resolution is required
c         anofqa(*) = lgmx(*) matrix in the active orbital space
c         af(*) = lgmx(*) matrix in the virtual orbital space
c         lgmx(*) = lgmx(*) matrix in the inactive orbital space
c         n2mt  = n2mt
c  output:
c         lgmx(*)

      implicit integer(a-z)
c
      logical qfresd,qfresv,qresaf
c
      real*8    zero,      one
      parameter(zero=0.0d0,one=1.0d0)
      real*8 anofqa(*),lgmx(*),af(*)
c
      if (.not.qfresd)  call wzero(n2mt,lgmx,1)
c
      if (qresaf)  call daxpy_wr(n2mt,one,anofqa,1,lgmx,1)
c
      if (qfresv)  call daxpy_wr(n2mt,one,af,1,lgmx,1)
c
      return
      end
c deck d1tr1
      subroutine d1tr1(
     & d1,    x,  dii,  dvi,
     & dscr,  xd)
c
c  routine to perform a symmetrized one-index transformation of the
c  one-particle density matrix.
c
c  d' = d * x  - x * d  = -( (x*d) + (x*d)(transpose) )
c
c  the second form is used to take advantage of longer vector lengths
c  in the x*d product.
c
c  input:
c  nmpsy(*),nipsy(*),nvpsy(*),nsmi(*),nnit(*),nmit(*),nvit(*),
c  nsm(*),nsi(*),n2sm(*),nnsi(*),nsvi(*) = symmetry arrays
c  mapmi(*) = internal-to-mo mapping vector.
c  d1(*) = square symmetry packed d1 matrix, indexed by mo's.
c  x(*) = rectangular symmetry packed x(*) matrix, indexed by mo's and
c         by internal orbitals.
c  dscr(*),xd(*) = scratch arrays.
c
c  output:
c  dii(*) = lower-triangular symmetry packed dii matrix, indexed by
c           internal orbitals.
c  dvi(*) = rectangular symmetry packed dvi matrix, indexed by virtual
c           orbitals and by internal orbitals.
c
c  written by ron shepard.
c  formal changes: michal dallos XI/99
c
       implicit none
c  ##  parameter & common section
c
      integer nmomx
      parameter(nmomx=1023)
c
      real*8    one
      parameter(one=1.d0)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8), nvpsy(8), nsm(8), n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxy(1,17),nvpsy(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c  ##  integer section
c
      integer nu, na, np
      integer p0, pp, psym
      integer u0, up
c
c  ##  real*8 section
c
      real*8 d1(*),dii(*),dvi(*),dscr(*)
      real*8 xd(*),x(*)
c
c----------------------------------------------------------
c
c  form x*d and symmetrize by symmetry blocks.
c
      do 100 psym=1,nsym
         np=nipsy(psym)
         if(np.eq.0)go to 100
c
         nu=nmpsy(psym)
         u0=nsm(psym)
         p0=nsi(psym)
         pp=n2sm(psym)+1
         call dtodii(u0,p0,nu,np,mapmi,d1(pp),dscr)
c
         up=nsmi(psym)+1
         call mxm(x(up),nu, dscr,np, xd,np)
c
         pp=nnsi(psym)+1
         up=nsvi(psym)+1
         na=nvpsy(psym)
         call sxd1(nu,np,na,xd,dii(pp),dvi(up))
100   continue
c
c  account for the minus sign...
c
      call dscal_wr(nnit,-one,dii,1)
      if(nvit.ne.0)call dscal_wr(nvit,-one,dvi,1)
c
      return
      end
c deck d1updt
      subroutine d1updt(qfresv,nsym,nsm,nnsm,iri1,
     &                  iri2,mapmi,irv1,irv2,
     &                  mapmv,nndx,dii,dvi,dvv,
     &                  cid1)
c
c  update the ci 1-particle density matrix with the 1-index-transformed
c  mc d1(*) and symmetrized mc transition density contributions.
c
c  input:
c  nsym,nsm,nnsm,iri1,iri2,mapmi = symmetry info.
c  nndx(*) = (i*(i-1))/2 array.
c  dii(*),dvi(*) = effective mc contributions.
c  dvv(*) = virtual orbital resolution contribution.
c  cid1(*) = initial ci 1-particle density matrix.
c
c  output:
c  cid1(*) = updated effective ci 1-particle density matrix.
c
      implicit integer(a-z)
      integer nsm(*),nnsm(*),iri1(*),iri2(*),mapmi(*),irv1(*),irv2(*),
     & mapmv(*),nndx(*)
      real*8 dii(*),dvi(*),dvv(*),cid1(*)
c
      logical qfresv
c
      pq=0
      ai=0
      vv=0
      do 160 isym=1,nsym
         m0=nsm(isym)
         mm0=nnsm(isym)
         do 130 p=iri1(isym),iri2(isym)
            mp=mapmi(p)-m0
            mpmq0=mm0+nndx(mp)-m0
            do 110 q=iri1(isym),p
               pq=pq+1
               mpmq=mpmq0+mapmi(q)
               cid1(mpmq) = cid1(mpmq) + dii(pq)
110         continue
            do 120 a=irv1(isym),irv2(isym)
               ai=ai+1
               ma=mapmv(a)-m0
               mamp=mm0+nndx(max(ma,mp))+min(ma,mp)
               cid1(mamp) = cid1(mamp) + dvi(ai)
120         continue
130      continue
         if (qfresv) then
            do 150 w=irv1(isym),irv2(isym)
               mw=mapmv(w)-m0
               mwmx0=mm0+nndx(mw)-m0
               do 140 x=irv1(isym),w
                  vv=vv+1
                  mwmx=mwmx0+mapmv(x)
                  cid1(mwmx)=cid1(mwmx)+dvv(vv)
140            continue
150         continue
         endif
160   continue
      end
c
c********************************************************************
c
c deck d2tr1
      subroutine d2tr1(
     & niiiit,    nviiit,
     & offii1,    addii,
     & offii2,  addvi,     x,         d2,
     & dx,      xd,        diiii,     dviii)
c
c  routine to perform a symmetrized one-index transformation of the
c  two-particle density matrix.
c
c  written by ron shepard.
c  formal changes: XI/99
c
c
       implicit none
c  ##  parameter & common block section
c
      real*8    one
      parameter(one=1d0)
c
      integer nmomx
      parameter(nmomx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8), nvpsy(8), irv1(8), irv2(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
      integer nvmot
      equivalence (nxtot(13),nvmot)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c  ##  integer section
c
      integer addii(*), addvi(nvmot,nimot), aq
      integer nu, na, niiiit, nviiit, np, nq
      integer offii1(*),offii2(*)
      integer p0, pq, pqpq, p, poff, psym
      integer q0, q, qsym
      integer rs, rsym, r, rssym
      integer smpt(8), s
      integer up
c
c  ##  real*8 section
c
      real*8 d2(*), dx(*), diiii(*), dviii(*)
      real*8 x(*), xd(*)
c
c-------------------------------------------------------
c
      call wzero(niiiit,diiii,1)
      if(nviiit.ne.0)call wzero(nviiit,dviii,1)
c
      do 300 r=1,nimot
         rsym=symm(mapmi(r))
         do 200 s=1,r
            rssym=mult(symm(mapmi(s)),rsym)
            rs=nndx(r)+s
c
c  expand the (rs) block to rectangular symmetry-packed.
c
            call xpnd2(d2,dx,rssym,rs,
     &       nndx,nipsy,iri1,iri2,
     &       mult,offii1,addii,smpt,nsym)
c
c  construct x*d by symmetry blocks.
c
            do 100 qsym=1,nsym
               nq=nipsy(qsym)
               if(nq.eq.0)go to 100
               psym=mult(qsym,rssym)
               np=nipsy(psym)
               if(np.eq.0)go to 100
               nu=nmpsy(psym)
               na=nvpsy(psym)
               up=nsmi(psym)+1
               p0=nsi(psym)
               q0=nsi(qsym)
               aq=offii2(rs)+addvi(irv1(psym),iri1(qsym))
c
               if(psym.eq.qsym)then
                  call mxm(x(up),nu, dx(smpt(psym)),np, xd,nq)
                  call sxd2e
     &             (nu,np,na,p0,rs,nndx,offii1,addii,xd,diiii,dviii(aq))
               elseif(psym.gt.qsym)then
                  call mxm(x(up),nu, dx(smpt(psym)),np, xd,nq)
                  call sxd2(nu,np,nq,na,p0,q0,rs,nndx,offii1,addii,
     &             xd,diiii,dviii(aq))
               elseif(psym.lt.qsym)then
                  call mxmt(x(up),nu, dx(smpt(qsym)),np, xd,nq)
c                  call mxma
c     &             (x(up),1,nu, dx(smpt(qsym)),nq,1, xd,1,nu, nu,np,nq)
                  call sxd2(nu,np,nq,na,p0,q0,rs,nndx,offii1,addii,
     &             xd,diiii,dviii(aq))
               endif
100         continue
c
200      continue
300   continue
c
c  account for factor of 2 in the diagonal diiii elements...
c
      do 420 p=1,nimot
         poff=nndx(p)
         do 410 q=1,p
            pq=poff+q
            pqpq=offii1(pq)+addii(pq)
            diiii(pqpq)=diiii(pqpq)+diiii(pqpq)
410      continue
420   continue
c
c  account for sign factor...
c
      call dscal_wr(niiiit,-one,diiii,1)
      if(nviiit.ne.0)call dscal_wr(nviiit,-one,dviii,1)
c
      return
      end
c deck d2updt
      subroutine d2updt(
     & qfresv, info,   infile, outfil,
     & diiii,  dviii,  bufin,  bufout,
     & val,    ibuf,   nndx,   addii,
     & addvi,  off1,   off2,   mcd1,
     & lgmx,   mapmd1, maplgv, fgiiee,
     & fgeiei, mapvv,  mapii,  map2i,
     & lbufmx)
c
c  update an existing ci 2-particle density matrix file with the 1-
c  index transformed and transition mc density matrix contributions to
c  construct the effective two-particle density matrices required
c  for mrci gradients.
c
c  diiii(*)  = iiii density matrix contributions.
c  dviii(*)  = viii density matrix contributions.
c  bufin(*)  = input density matrix buffer.
c  bufout(*) = output density matrix buffer.
c  ibuf(*,*) = label buffer.
c  addii(*)  = internal-internal addressing array.
c  addvi(*)  = virtual-internal addressing array.
c  off1(*)   = iiii addressing array.
c  off2(*)   = viii addressing array.
c  lenbuf    = input and output buffer length.
c  nvpbf     = number of density elements per buffer = info(5)
c  lbufmx    = dimension of sifs buffers set by mem. allocation above.
c  lgmx      = lagragsche multiplier matrix
c  mcd1      = mcscf density matrix
c  map..     = addressing arrays
c  fgiiee    = flags indicating if the iiee virtual orbital resolution
c              contribution is added to a corresponding nonzero
c              density matrix element
c  fgeiei    = same as above but for the eiei contribution
c
c  version log:
c  mar-96 sifs version gsk
c  jun-90 modified to add the virtual orbital resolution contribution
c                                                            (m.e.)
c  written by ron shepard.
c  version date 19-aug-87.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      real*8    zero,    half,     two
      parameter(zero=0d0,half=.5d0,two=2d0)
c
      integer msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvmot,nnvt,irv1(8),irv2(8)
      equivalence (nxtot(13),nvmot)
      equivalence (nxtot(14),nnvt)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)


      integer assume_fc,mapmo_mc(nmomx) 

c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer perm(4,0:7),itype(4)
      integer    intrnl,   virtul
      parameter (intrnl=1, virtul=2)
      integer ivtype(0:4)
c
      real*8 diiii(*),dviii(*)
      real*8 bufin(*),bufout(*),val(*)
      real*8 lgmx(*),mcd1(*),vden
c
      logical fgiiee(*),fgeiei(*)
      integer    nipv
      parameter (nipv=4)
c
c     ##   integer section
c
      integer lbufmx
      integer addii(*),addvi(nvmot,nimot)
      integer ibuf(nipv,lbufmx),info(10),
     & ifmt,ibitv(1),imcd1,ilgmx,iisym,isym,ierr,ibvtyp,
     & ip,infile,i
      integer kntb
      integer last
      integer mapvv(*),mapii(*),map2i(nimot,nimot),
     & mapmd1(nimot,nimot),maplgv(nvmot,nvmot)
      integer nndx(nmomx),nwrite,num,nvpbf
      integer off1(*),off2(*),outfil
      integer p,psym,pqrs,pqrsym,pqsym
      integer q
      integer reqnum,r
      integer s,s2
      integer uvwx,uv,u,upind,utemp,uvwxt,uvtype
      integer v,vend
      integer wx,w,wtemp,wxtype
      integer x,xrpq
ctm
      integer nrec
      logical qfresv
      integer iretbv,itypea,itypeb,iwait
c
      data ivtype/3*intrnl,2*virtul/
      data nrec /0/
      data iretbv /0/, itypea /3/, itypeb /1/, iwait /1/
      data perm/1,2,3,4, 2,1,3,4, 1,2,4,3, 2,1,4,3,
     & 3,4,1,2, 3,4,2,1, 4,3,1,2, 4,3,2,1/
c
c-------------------------------------------------------------
c
c     # total number of effd2 elements written accumulated with nwrite.
      nwrite = 0
c
      if (info(5) .gt. lbufmx) call bummer(
     & 'd2updt: lbufmx.lt.n2max=', info(5), faterr )
      nvpbf = info(5)
c
      if (qfresv) then
c
c        # initialize fgiiee and fgeiei
c
         upind=nnvt*nnit
         do 150 i=1,upind
            fgiiee(i)=.true.
150      continue
c
         do 155 i=1, nnvt*n2it
            fgeiei(i)=.true.
155      continue
c
      endif
c
c     # read a buffer of 2-p density elements and update the elements.
c
100   continue
      call sifrd2(infile,info,nipv,iretbv,
     & bufin,num,last,itypea,
     & itypeb,ibvtyp,val,
     & ibuf,ibitv,ierr)
      if ( ierr .ne. 0 ) call bummer(
     & 'd2updt: from sifrd2(), ierr=', ierr, faterr )
c
      do 200 kntb=1,num
         if(ibuf(1,kntb).eq.0)go to 200
c
c        # determine index permutation...
         itype(1)=ivtype(mctype(ibuf(1,kntb)))
         itype(2)=ivtype(mctype(ibuf(2,kntb)))
         itype(3)=ivtype(mctype(ibuf(3,kntb)))
         itype(4)=ivtype(mctype(ibuf(4,kntb)))
         ip=0
         if(itype(1).lt.itype(2))ip=1
         if(itype(3).lt.itype(4))ip=ip+2
         uvtype=nndx(itype(perm(1,ip)))+itype(perm(2,ip))
         wxtype=nndx(itype(perm(3,ip)))+itype(perm(4,ip))
         if(uvtype.lt.wxtype)then
            ip=ip+4
            uvwxt=nndx(wxtype)+uvtype
         else
            uvwxt=nndx(uvtype)+wxtype
         endif
c
         if(uvwxt.eq.1)then
c           ...iiii
            u=mapivm(ibuf(perm(1,ip),kntb))
            v=mapivm(ibuf(perm(2,ip),kntb))
            w=mapivm(ibuf(perm(3,ip),kntb))
            x=mapivm(ibuf(perm(4,ip),kntb))
            uv=nndx(max(u,v))+min(u,v)
            wx=nndx(max(w,x))+min(w,x)
            uvwx=off1(max(uv,wx))+addii(min(uv,wx))
            val(kntb)=val(kntb)+diiii(uvwx)
            diiii(uvwx)=zero
         elseif(uvwxt.eq.2)then
c           ...eiii
            u=mapivm(ibuf(perm(1,ip),kntb))
            v=mapivm(ibuf(perm(2,ip),kntb))
            w=mapivm(ibuf(perm(3,ip),kntb))
            x=mapivm(ibuf(perm(4,ip),kntb))
            wx=nndx(max(w,x))+min(w,x)
            uvwx=off2(wx)+addvi(u,v)
            val(kntb)=val(kntb)+dviii(uvwx)
            dviii(uvwx)=zero
c
c        # exit if no virtual orbital resolution is required
c
         elseif (.not.qfresv) then
            goto 200
c
         elseif(uvwxt.eq.4)then
c
c           # iiee
c           # d2ci(uvwx) = d2ci(uvwx) +
c           #              2*lgmx(wx)*d1mc(uv) + 2*lgmx(uv)*d1mc(wx)
c
            w=mapivm(ibuf(perm(1,ip),kntb))
            x=mapivm(ibuf(perm(2,ip),kntb))
            u=mapivm(ibuf(perm(3,ip),kntb))
            v=mapivm(ibuf(perm(4,ip),kntb))
            if (symi(u).eq.symi(v)) then
               if (symv(w).eq.symv(x)) then
                  ilgmx =maplgv(max(w,x),min(w,x))
                  imcd1=mapmd1(max(u,v),min(u,v))
                  val(kntb)=val(kntb)+two*lgmx(ilgmx)*mcd1(imcd1)
                  fgiiee((mapii(imcd1)-1)*nnvt+mapvv(ilgmx))=.false.
               endif
            endif
c
         elseif(uvwxt.eq.3)then
c
c           # eiei
c           # d2ci(uvwx) = d2ci(uvwx)
c           #              -1/2*lgmx(wu)*mcd1(xv)-1/2*lgmx(xu)*mcd1(wv)
c           #              -1/2*lgmx(xv)*mcd1(wu)-1/2*lgmx(wv)*mcd1(ux)
c
            w=mapivm(ibuf(perm(1,ip),kntb))
            u=mapivm(ibuf(perm(2,ip),kntb))
            x=mapivm(ibuf(perm(3,ip),kntb))
            v=mapivm(ibuf(perm(4,ip),kntb))
            if (symi(u).eq.symi(v)) then
               if (symv(w).eq.symv(x)) then
                  ilgmx =maplgv(max(w,x),min(w,x))
                  imcd1=mapmd1(max(u,v),min(u,v))
                  val(kntb)=val(kntb)-half*lgmx(ilgmx)*mcd1(imcd1)
                  if (w.lt.x) then
                     wtemp=w
                     w=x
                     x=wtemp
                     utemp=u
                     u=v
                     v=utemp
                  elseif (w.eq.x) then
                     utemp=max(u,v)
                     v=min(u,v)
                     u=utemp
                  endif
                  fgeiei((mapvv(ilgmx)-1)*n2it+map2i(u,v))=.false.
               endif
            endif
         else
c           not modified
         endif
200   continue
c
c      # if there are more input buffers, then dump the output buffer
c      # and continue input processing...
c
      if(last .eq. msame) then
         nwrite = nwrite + 1
c here, in contrast ifmt is taken over from input file

         call sifew2(outfil,info,nipv,num,
     &    last,itypea,itypeb,
     &    ibvtyp,val,ibuf,ibitv,
     &    bufout,iwait,nrec,reqnum,
     &    ierr)
         if ( ierr .ne. 0 ) call bummer(
     &    'd2updt: from 1 sifew2(), ierr=', ierr, faterr )
         go to 100
      endif
c
c     # last input buffer has been processed.  look for nonzero
c     # elements remaining in the density matrix arrays.
c
      kntb=num
c
c     # diiii(*)...
c
      pqrs=0
      do 340 p=1,nimot
         psym=symi(p)
         do 330 q=1,p
            pqsym=mult(symi(q),psym)
            do 320 r=1,p
               pqrsym=mult(symi(r),pqsym)
               s2=min(r,iri2(pqrsym))
               if(r.eq.p)s2=q
               do 310 s=iri1(pqrsym),s2
                  pqrs=pqrs+1
                  if(diiii(pqrs).ne.zero)then
                     if(kntb.eq.nvpbf) then
c here, in contrast ifmt is taken over from input file
                        call sifew2(outfil,info,nipv,kntb,
     &                   msame,itypea,itypeb,
     &                   ibvtyp,val,ibuf,ibitv,
     &                   bufout,iwait,nrec,reqnum,
     &                   ierr)
                        if ( ierr .ne. 0 ) call bummer(
     &                   'd2updt: from 2 sifew2(), ierr=',ierr,faterr)
                        kntb = 0
                     endif
                     kntb=kntb+1
                     val(kntb)=diiii(pqrs)
                     ibuf(1,kntb)=mapmi(p)
                     ibuf(2,kntb)=mapmi(q)
                     ibuf(3,kntb)=mapmi(r)
                     ibuf(4,kntb)=mapmi(s)
                  endif
310            continue
320         continue
330      continue
340   continue
c
c     # dviii(*)...
c
      xrpq=0
      do 440 p=1,nimot
         psym=symi(p)
         do 430 q=1,p
            pqsym=mult(symi(q),psym)
            do 420 r=1,nimot
               pqrsym=mult(symi(r),pqsym)
               do 410 x=irv1(pqrsym),irv2(pqrsym)
                  xrpq=xrpq+1
                  if(dviii(xrpq).ne.zero)then
                     if(kntb.eq.nvpbf) then
c here, in contrast ifmt is taken over from input file
                        call sifew2(outfil,info,nipv,kntb,
     &                   msame,itypea,itypeb,
     &                   ibvtyp,val,ibuf,ibitv,
     &                   bufout,iwait,nrec,reqnum,
     &                   ierr)
                        if ( ierr .ne. 0 ) call bummer(
     &                   'd2updt: from 3 sifew2(), ierr=',ierr,faterr)
                        kntb = 0
                     endif
                     kntb=kntb+1
                     val(kntb)=dviii(xrpq)
                     ibuf(1,kntb)=mapmv(x)
                     ibuf(2,kntb)=mapmi(r)
                     ibuf(3,kntb)=mapmi(p)
                     ibuf(4,kntb)=mapmi(q)
                  endif
410            continue
420         continue
430      continue
440   continue
c
c     # dvvvv(*) = 2*lgmx(wx)*mcd1(uv) ....
c
      if (qfresv) then
c
         do 190 isym=1, nsym
            do 180 u=iri1(isym), iri2(isym)
               do 170 v=iri1(isym), u
                  imcd1=mapmd1(u,v)
                  do 165 iisym=1, nsym
                     do 160 w=irv1(iisym), irv2(iisym)
                        do 145 x=irv1(iisym),w
                           ilgmx =maplgv(w,x)
                           if (fgiiee((mapii(imcd1)-1)*nnvt
     &                      +mapvv(ilgmx)))               then
                              vden=lgmx(ilgmx)*mcd1(imcd1)
                              if (vden.ne.zero) then
                                 if(kntb.eq.nvpbf) then
c here, in contrast ifmt is taken over from input file
                                    call sifew2(outfil,info,nipv,kntb,
     &                               msame,itypea,itypeb,
     &                               ibvtyp,val,ibuf,ibitv,
     &                               bufout,iwait,nrec,reqnum,
     &                               ierr)
                                    if ( ierr .ne. 0 ) call bummer(
     &                               'd2updt: from 4 sifew2(), ierr=',
     &                               ierr, faterr )
                                    kntb = 0
                                 endif
                                 kntb=kntb+1
                                 val(kntb)=two*vden
                                 ibuf(1,kntb)=mapmv(w)
                                 ibuf(2,kntb)=mapmv(x)
                                 ibuf(3,kntb)=mapmi(u)
                                 ibuf(4,kntb)=mapmi(v)
                              endif
                           endif
145                     continue
160                  continue
165               continue
170            continue
180         continue
190      continue
c
         do 395 isym=1, nsym
            do 390 w=irv1(isym), irv2(isym)
               do 380 x=irv1(isym), w
                  ilgmx =maplgv(w,x)
                  do 370 iisym=1, nsym
                     do 360 u=iri1(iisym),iri2(iisym)
                        vend=iri2(iisym)
                        if (w.eq.x) then
                           vend=u
                        endif
                        do 350 v=iri1(iisym),vend
                           if (fgeiei((mapvv(ilgmx)-1)*n2it
     &                      +map2i(u,v))) then
                              imcd1=mapmd1(max(u,v),min(u,v))
                              vden=lgmx(ilgmx)*mcd1(imcd1)
                              if (vden.ne.zero)then
                                 if(kntb.eq.nvpbf) then
c here, in contrast ifmt is taken over from input file
c similar value of nvpbf, dimensioning of buffers
                                    call sifew2(outfil,info,nipv,kntb,
     &                               msame,itypea,itypeb,
     &                               ibvtyp,val,ibuf,ibitv,
     &                               bufout,iwait,nrec,reqnum,
     &                               ierr)
                                    if ( ierr .ne. 0 ) call bummer(
     &                               'd2updt: from 5 sifew2(), ierr=',
     &                               ierr, faterr )
                                    kntb = 0
                                 endif
                                 kntb=kntb+1
                                 val(kntb)=-half*vden
                                 ibuf(1,kntb)=mapmv(w)
                                 ibuf(2,kntb)=mapmi(u)
                                 ibuf(3,kntb)=mapmv(x)
                                 ibuf(4,kntb)=mapmi(v)
                              endif
                           endif
350                     continue
360                  continue
370               continue
380            continue
390         continue
395      continue
c
      endif
c
c     # dump the last output buffer.
c
      num = kntb
c here, in contrast ifmt is taken over from input file
      call sifew2(outfil,info,nipv,num,
     & nomore,itypea,itypeb,
     & ibvtyp,val,ibuf,ibitv,
     & bufout,iwait,nrec,reqnum,
     & ierr)
      if ( ierr .ne. 0 ) call bummer(
     & 'd2updt: from last sifew2(), ierr=', ierr, faterr )
c
      return
      end
