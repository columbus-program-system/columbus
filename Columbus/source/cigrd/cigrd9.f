!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine  dcopy_x(n,core,from,to)
      real*8 core(*)
      integer n,from,to,i
c
      if(n.le.0)return
c     write(6,*) 'dcopy_x: from,to,n=',from,to,n
      do i=0,n-1
        core(to+i)=core(from+i)
      enddo
      return
      end
c
c
      subroutine cpbcm(
     & b,       ib, ndpsy, napsy, nvpsy, namot, ndmot,
     & nadt, naar, nvdt, ndimr, ib_c, ib_m, ib1, fhm,
     & ncfx, w, fcsf, orbl, csfl, fvec, ipiv, icd,
     & avcsl, core, hmcvec)
c
c     this subroutine constructs the tridiagonal hessian for
c     lapack solution
c     written by Bernhard Sellner
c
      implicit none
      integer i,j,k,p,r,ni,nj,ijpr,rsym,psym,ist
      integer nadt, naar, nvdt, nvat, ndimr, ib1
      integer fdim ,ncfx, ii, jj, kk, gli, glj
      integer info,ipiv(*),avcsl,imp(200),icd,iii
      integer itotal,ktau, tmatp, kwork,lwork
      integer k1,k2,kcr,active,kmr,kbw,kbi
      integer jjj,k3,k4,kcw
c
      integer prtmicro
      common/output/prtmicro
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer ib(15),ib_m(maxstat), ib_c(4,maxstat)
c
      integer namot,ndmot,napsy(8),ndpsy(8),nvpsy(8)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer      iorbx,         ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx),ix0a,ix0d
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
      equivalence (ix0(1),ix0d)
c
      integer nfilmx,iunits
      parameter (nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer numopt
      parameter(numopt=10)
c
      integer cigopt,print,fresdd,fresaa,fresvv,samcflag,solvelpck
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
      equivalence (cigopt(6),solvelpck)
c
      real*8  b(*),fhm(*),w(*),fcsf(*),orbl(*),csfl(*)
      real*8  core(*),hmcvec(*)
      real*8  fvec(ndimr+ncfx,1),wsr1
c
      integer atebyt,forbyt
      external atebyt,forbyt
c
c     # bummer error types.
      integer   wrnerr,   nfterr,   faterr
      parameter(wrnerr=0, nfterr=1, faterr=2)
c
      nvat=ndimr-nadt-naar-nvdt
      write(nlist,fmt='(A)')''
      write(nlist,fmt='(A)')' Solving linear equation with lapack'
      write(nlist,fmt='(A)')''
c
      info=0
c
      if (print.ge.1) then
        write(nlist,fmt='(A)')' Dimensions:'
        write(nlist,fmt='(A,I5)')' nadt  = ',nadt
        write(nlist,fmt='(A,I5)')' naar  = ',naar
        write(nlist,fmt='(A,I5)')' nvdt  = ',nvdt
        write(nlist,fmt='(A,I5)')' ndimr = ',ndimr
        write(nlist,fmt='(A,I5)')' nvat  = ',nvat
        write(nlist,fmt='(A,I5)')' ncfx  = ',ncfx
        write(nlist,fmt='(A)')''
      endif
c
c
c     START
c
c construction of the full hessian matrix: fhm
      fdim=ndimr+ncfx
      do ist=1,nst
        do i=1,navst(ist)
          fdim=fdim-navst(ist)
        enddo
      enddo
c
c     initialize fhm matrix
      do i=1,(fdim*(fdim+1))/2
        fhm(i)=0.0
      enddo
c
c B part:
c   adad:
      if (ib(1).ne.0) then
        write(nlist,fmt='(A)')' copy adad'
        i=ib(1)
        do p=1,namot
          psym=symx(ix0a+p)
          ni=ndpsy(psym)
          if (ni.ne.0) then
            do r=1,p
              rsym=symx(ix0a+r)
              nj=ndpsy(rsym)
              if (nj.ne.0) then
                if (p.eq.r) then
c                 diagonal blocks
                  do ii=1,ni
c                   calculate global index
c                   within the block
                    gli=0
                    glj=0
                    do j=1,(p-1)
                      rsym=symx(ix0a+j)
                      gli=gli+ndpsy(rsym)
                    enddo
                    do j=1,(r-1)
                      psym=symx(ix0a+j)
                      glj=glj+ndpsy(psym)
                    enddo
c                   within the full matrix
                    gli=gli+ii
                    glj=glj+ii
                    kk=(gli+((2*fdim-glj)*(glj-1))/2)
                    do jj=1,nj
                      if (ii.le.jj) then
                        k=i+(ii-1)*nj+jj-1
                        fhm(kk)=b(k)
                        kk=kk+1
                      endif
                    enddo
                  enddo
                else  
c                 ofdiagonal blocks
                  do ii=1,ni
c                   calculate global index
c                   within the block
                    gli=0
                    glj=0
                    do j=1,(p-1)
                      rsym=symx(ix0a+j)
                      gli=gli+ndpsy(rsym)
                    enddo
                    do j=1,(r-1)
                      psym=symx(ix0a+j)
                      glj=glj+ndpsy(psym)
                    enddo
c                   within the full matrix
                    gli=gli+ii
                    glj=glj+1
                    do jj=1,nj
                        kk=(gli+((2*fdim-glj)*(glj-1))/2)
                        k=i+(ii-1)*nj+jj-1
                        fhm(kk)=b(k)
                        glj=glj+1
                    enddo
                  enddo
                endif
              endif
              i=(i+ni*nj)
            enddo
          endif
        enddo
      endif
c
c   aaad:
      if (ib(2).ne.0) then
        write(nlist,fmt='(A)')' copy aaad'
        i=ib(2)
        do ii=1,naar
c         calculate global index
c         within the full matrix
          gli=nadt+ii
          glj=1
          kk=(gli+((2*fdim-glj)*(glj-1))/2)
          do jj=1,nadt
            k=i+(ii-1)*nadt+jj-1
            fhm(kk)=b(k)
            glj=glj+1
            kk=(gli+((2*fdim-glj)*(glj-1))/2)
          enddo
        enddo
      endif
c
c   aaaaa
      if (ib(3).ne.0) then
        write(nlist,fmt='(A)')' copy aaaa'
        i=ib(3)
        do ii=1,naar
c         calculate global index
c         within the full matrix
          gli=nadt+ii
          glj=nadt+1
          kk=(gli+((2*fdim-glj)*(glj-1))/2)
          do jj=1,ii
            k=i+(((ii-1)*ii)/2)+jj-1
            fhm(kk)=b(k)
            glj=glj+1
            kk=(gli+((2*fdim-glj)*(glj-1))/2)
          enddo
        enddo
      endif
c
c   vdad:
      if (ib(4).ne.0) then
        write(nlist,fmt='(A)')' copy vdad'
        i=ib(4)
        do ii=1,nvdt
c         calculate global index
c         within the full matrix
          gli=nadt+naar+ii
          glj=1
          kk=(gli+((2*fdim-glj)*(glj-1))/2)
          do jj=1,nadt
            k=i+(ii-1)*nadt+jj-1
            fhm(kk)=b(k)
            glj=glj+1
            kk=(gli+((2*fdim-glj)*(glj-1))/2)
          enddo
        enddo
      endif
c
c   vdaa:
      if (ib(5).ne.0) then
        write(nlist,fmt='(A)')' copy vdaa'
        i=ib(5)
        do ii=1,naar
c         calculate global index
c         within the block
          gli=nadt+naar
          glj=nadt
c         within the full matrix
          gli=gli+1
          glj=glj+ii
          kk=(gli+((2*fdim-glj)*(glj-1))/2)
          do jj=1,nvdt
            k=i+(ii-1)*nvdt+jj-1
            fhm(kk)=b(k)
            kk=kk+1
          enddo
        enddo
      endif
c
c   vdvd:    
      if (ib(6).ne.0) then
        write(nlist,fmt='(A)')' copy vdvd'
        i=ib(6)
        do p=1,ndmot
          psym=symx(ix0d+p)
          ni=nvpsy(psym)
          if (ni.ne.0) then
            do r=1,p
              rsym=symx(ix0d+r)
              nj=nvpsy(rsym)
              if (nj.ne.0) then
                if (p.eq.r) then
c                 diagonal blocks
                  do ii=1,ni
c                   calculate global index
c                   within the block
                    gli=nadt+naar
                    glj=nadt+naar
                    do j=1,(p-1)
                      rsym=symx(ix0d+j)
                      gli=gli+nvpsy(rsym)
                    enddo
                    do j=1,(r-1)
                      psym=symx(ix0d+j)
                      glj=glj+nvpsy(psym)
                    enddo
c                   within the full matrix
                    gli=gli+ii
                    glj=glj+ii
                    kk=(gli+((2*fdim-glj)*(glj-1))/2)
                    do jj=1,nj
                      if (ii.le.jj) then
                        k=i+(ii-1)*nj+jj-1
                        fhm(kk)=b(k)
                        kk=kk+1
                      endif
                    enddo
                  enddo
                else  
c                 ofdiagonal blocks
                  do ii=1,ni
c                   calculate global index
c                   within the block
                    gli=nadt+naar
                    glj=nadt+naar
                    do j=1,(p-1)
                      rsym=symx(ix0d+j)
                      gli=gli+nvpsy(rsym)
                    enddo
                    do j=1,(r-1)
                      psym=symx(ix0d+j)
                      glj=glj+nvpsy(psym)
                    enddo
c                   within the full matrix
                    gli=gli+ii
                    glj=glj+1
                    do jj=1,nj
                        kk=(gli+((2*fdim-glj)*(glj-1))/2)
                        k=i+(ii-1)*nj+jj-1
                        fhm(kk)=b(k)
                        glj=glj+1
                    enddo
                  enddo
                endif
              endif
              i=(i+ni*nj)
            enddo
          endif
        enddo
      endif
c
c
c   vaad:
      if (ib(7).ne.0) then
        write(nlist,fmt='(A)')' copy vaad'
        i=ib(7)
        do p=1,namot
          psym=symx(ix0a+p)
          ni=ndpsy(psym)
          if (ni.ne.0) then
            do r=1,namot
              rsym=symx(ix0a+r)
              nj=nvpsy(rsym)
              if (nj.ne.0) then
                  do ii=1,ni
c                   calculate global index
c                   within the block
                    gli=nadt+naar+nvdt
                    glj=0
                    do j=1,(r-1)
                      rsym=symx(ix0a+j)
                      gli=gli+nvpsy(rsym)
                    enddo
                    do j=1,(p-1)
                      psym=symx(ix0a+j)
                      glj=glj+ndpsy(psym)
                    enddo
c                   within the full matrix
                    gli=gli+1
                    glj=glj+ii
                    kk=(gli+((2*fdim-glj)*(glj-1))/2)
                    do jj=1,nj
                        k=i+(ii-1)*nj+jj-1
                        fhm(kk)=b(k)
                        kk=kk+1
                    enddo
                  enddo
              endif
              i=(i+ni*nj)
            enddo
          endif
        enddo
      endif
c
c   vaaa:
      if (ib(8).ne.0) then
        write(nlist,fmt='(A)')' copy vaaa'
        i=ib(8)
        do ii=1,naar
c         calculate global index
c         within the block
          gli=nadt+nvdt+naar
          glj=nadt
c         within the full matrix
          gli=gli+1
          glj=glj+ii
          kk=(gli+((2*fdim-glj)*(glj-1))/2)
          do jj=1,nvat
            k=i+(ii-1)*nvat+jj-1
            fhm(kk)=b(k)
            kk=kk+1
          enddo
        enddo
      endif
c
c   vavd:
      if (ib(9).ne.0) then
        write(nlist,fmt='(A)')' copy vavd'
        i=ib(9)
        do ii=1,nvdt
c         calculate global index
c         within the block
          gli=nadt+nvdt+naar
          glj=nadt+naar
c         within the full matrix
          gli=gli+1
          glj=glj+ii
          kk=(gli+((2*fdim-glj)*(glj-1))/2)
          do jj=1,nvat
            k=i+(ii-1)*nvat+jj-1
            fhm(kk)=b(k)
            kk=kk+1
          enddo
        enddo
      endif
c
c   vava:    
      if (ib(10).ne.0) then
        write(nlist,fmt='(A)')' copy vava'
        i=ib(10)
        do p=1,namot
          psym=symx(ix0a+p)
          ni=nvpsy(psym)
          if (ni.ne.0) then
            do r=1,p
              rsym=symx(ix0a+r)
              nj=nvpsy(rsym)
              if (nj.ne.0) then
                if (p.eq.r) then
c                 diagonal blocks
                  do ii=1,ni
c                   calculate global index
c                   within the block
                    gli=nadt+naar+nvdt
                    glj=nadt+naar+nvdt
                    do j=1,(p-1)
                      rsym=symx(ix0a+j)
                      gli=gli+nvpsy(rsym)
                    enddo
                    do j=1,(r-1)
                      psym=symx(ix0a+j)
                      glj=glj+nvpsy(psym)
                    enddo
c                   within the full matrix
                    gli=gli+ii
                    glj=glj+ii
                    kk=(gli+((2*fdim-glj)*(glj-1))/2)
                    do jj=1,nj
                      if (ii.le.jj) then
                        k=i+(ii-1)*nj+jj-1
                        fhm(kk)=b(k)
                        kk=kk+1
                      endif
                    enddo
                  enddo
                else  
c                 ofdiagonal blocks
                  do ii=1,ni
c                   calculate global index
c                   within the block
                    gli=nadt+naar+nvdt
                    glj=nadt+naar+nvdt
                    do j=1,(p-1)
                      rsym=symx(ix0a+j)
                      gli=gli+nvpsy(rsym)
                    enddo
                    do j=1,(r-1)
                      psym=symx(ix0a+j)
                      glj=glj+nvpsy(psym)
                    enddo
c                   within the full matrix
                    gli=gli+ii
                    glj=glj+1
                    do jj=1,nj
                        kk=(gli+((2*fdim-glj)*(glj-1))/2)
                        k=i+(ii-1)*nj+jj-1
                        fhm(kk)=b(k)
                        glj=glj+1
                    enddo
                  enddo
                endif
              endif
              i=(i+ni*nj)
            enddo
          endif
        enddo
      endif
      write(nlist,fmt='(A)')''
c
c project the C and M matrix
c     print the hmcvec
      if (prtmicro.ge.4) then
        write(nlist,fmt='(A)')' hmcvec'
        do ist=1,nst
          do j=1,navst(ist)
            write(nlist,fmt='(2(A,I5))')' ist=  ',ist,' ; navst=',j
            ii=0
            do p=1,ist-1
              do jj=1,navst(p)
                ii=ii+ncsf_f(p)
              enddo
            enddo
            do jj=1,j-1
              ii=ii+ncsf_f(ist)
            enddo
            do i=1,ncsf_f(ist)-1
              write(nlist,fmt='(X,F11.8)',advance='no')hmcvec(ii+i)
            enddo
            write(nlist,fmt='(X,F11.8)',advance='yes')
     &            hmcvec(ii+ncsf_f(ist))
          enddo    
        enddo     
        write(nlist,fmt='(A)')''
      endif
c
c set memory pointers for transformation matrices
      k=1
      imp(k)=icd
      do ist=1,nst
c         T matrix
          imp(k+1)=imp(k)+atebyt(ncsf_f(ist)*(ncsf_f(ist)))
          k=k+1
      enddo
c
      ktau=k
      tmatp=1
c calculate the transformation matrices
      do ist=1,nst
        k=ktau
c       set memory for tau
        imp(k+1)=imp(k)+atebyt(ncsf_f(ist))
        k=k+1
        itotal=imp(k)
        call memcheck(itotal,avcsl,'solvel1b2')
c       position in hmcvec
        ii=0
        do p=1,ist-1
          do jj=1,navst(p)
            ii=ii+ncsf_f(p)
          enddo
        enddo
c       copy hmcvec to T matrix imp(tmatp)
        do j=1,navst(ist)
          do i=1,ncsf_f(ist)
            core(imp(tmatp)+(j-1)*ncsf_f(ist)+i-1)=
     &      hmcvec(ii+(j-1)*ncsf_f(ist)+i)
          enddo
        enddo
c       make initial guess of rest of T
        do ii=(navst(ist)+1),ncsf_f(ist)
          do jj=1,ncsf_f(ist)
            if(jj.eq.ii) then
              core(imp(tmatp)+(ii-1)*ncsf_f(ist)+jj-1)=1.0
            else
              core(imp(tmatp)+(ii-1)*ncsf_f(ist)+jj-1)=0.1
            endif
          enddo
        enddo
        if (prtmicro.ge.4) then
c       print the T matrix
          write(nlist,fmt='(A)')' Tmatrix transposed before  '
          ii=imp(tmatp)-1
          do p=1,ncsf_f(ist)
            do r=1,ncsf_f(ist)-1
              write(nlist,fmt='(X,F11.8)',advance='no')
     &        core(ii+(p-1)*ncsf_f(ist)+r)
            enddo
            r=ncsf_f(ist)
            write(nlist,fmt='(X,F11.8)',advance='yes')
     &      core(ii+(p-1)*ncsf_f(ist)+r)
          enddo
          write(nlist,fmt='(A)')''
        endif
c       set memory for work
        kwork=k
        imp(k+1)=imp(k)+atebyt(ncsf_f(ist))
        k=k+1
        itotal=imp(k)
        call memcheck(itotal,avcsl,'solvel1b2')
c       ! get a orthonormal basis
        lwork=-1
        call DGEQRF(ncsf_f(ist),ncsf_f(ist),core(imp(tmatp)),
     &       ncsf_f(ist),core(imp(ktau)),core(imp(kwork)),lwork,info)
        if (info.ne.0) then
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A)')' ERROR in DGEQRF during calculating a'
          write(nlist,fmt='(A)')' orthonormal basis to hmcvec:'
          write(nlist,fmt='(A)')' Problem in estimation of optimal '
          write(nlist,fmt='(A)')' size of work array.'
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A,I20)')'info= ',info
          write(nlist,fmt='(A)')''
          call bummer ('DGEQRF; info=',info,faterr)
        endif
        lwork=idint(core(imp(kwork)))
c       set memory for work
        imp(kwork+1)=imp(kwork)+atebyt(lwork)
        k=kwork+1
        itotal=imp(k)
        call memcheck(itotal,avcsl,'solvel1b2')
        call DGEQRF(ncsf_f(ist),ncsf_f(ist),core(imp(tmatp)),
     &       ncsf_f(ist),core(imp(ktau)),core(imp(kwork)),lwork,info)
        if (info.ne.0) then
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A)')' ERROR in DGEQRF during calculating a'
          write(nlist,fmt='(A)')' orthonormal basis to hmcvec:'
          write(nlist,fmt='(A)')' Problem in QR factorization.'
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A,I20)')'info= ',info
          write(nlist,fmt='(A)')''
          call bummer ('DGEQRF; info=',info,faterr)
        endif
        call DORGQR(ncsf_f(ist),ncsf_f(ist),ncsf_f(ist),
     &       core(imp(tmatp)),ncsf_f(ist),core(imp(ktau)),
     &       core(imp(kwork)),lwork,info)
        if (info.ne.0) then
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A)')' ERROR in DORGQR during calculating a'
          write(nlist,fmt='(A)')' orthonormal basis to hmcvec:'
          write(nlist,fmt='(A)')' Problem in generation of Q matrix'
          write(nlist,fmt='(A)')' out of QR factorization.'
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A,I20)')'info= ',info
          write(nlist,fmt='(A)')''
          call bummer ('DORGQR; info=',info,faterr)
        endif
        if (prtmicro.ge.2) then
c         print the T matrix
          write(nlist,fmt='(A)')' Tmatrix transposed after  '
          ii=imp(tmatp)-1
          do p=1,ncsf_f(ist)
            do r=1,ncsf_f(ist)-1
              write(nlist,fmt='(X,F11.8)',advance='no')
     &        core(ii+(p-1)*ncsf_f(ist)+r)
            enddo
            r=ncsf_f(ist)
            write(nlist,fmt='(X,F11.8)',advance='yes')
     &      core(ii+(p-1)*ncsf_f(ist)+r)
          enddo
          write(nlist,fmt='(A)')''
        endif
c       set things for next loop iteration
        tmatp=tmatp+1
      enddo
c     end of construction of T matrices
c
c set memory for Cred
      k=ktau
      kcr=k
      do ist=1,nst
        do j=1,navst(ist)
c         C red. matrix
          imp(k+1)=imp(k)+atebyt((ncsf_f(ist)-1)*ndimr)
          k=k+1
        enddo
      enddo
c set memory for C work
      kcw=k
      do ist=1,nst
          imp(k+1)=imp(k)+atebyt((ncsf_f(ist))*ndimr)
          k=k+1
      enddo
c and the memory for Mred
      kmr=k
      do ist=1,nst
        do j=1,navst(ist)
c         C red. matrix
          imp(k+1)=imp(k)+atebyt((ncsf_f(ist)-1)**2)
          k=k+1
        enddo
      enddo
c and work space for M intermediate
      kbi=k
      do ist=1,nst
        imp(k+1)=imp(k)+atebyt((ncsf_f(ist)-1)*ncsf_f(ist))
        k=k+1
      enddo
c and work space for M work
      kbw=k
      do ist=1,nst
        imp(k+1)=imp(k)+atebyt((ncsf_f(ist)-1)*ncsf_f(ist))
        k=k+1
      enddo
      itotal=imp(k)
      call memcheck(itotal,avcsl,'solvel1b2')
c
c do now the transformation
      k1=kcr
      k2=1
      k3=kcw
      do ist=1,nst
        do j=1,navst(ist)
          jj=k2
          r=imp(jj)+navst(ist)*ncsf_f(ist)
          if (ib_c(1,ist).ne.0) then
c           transform cad
            ii=imp(k1)
            kk=ib_c(1,ist)
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' C used cad  '
              do i=1,nadt
                do jjj=ncsf_f(ist)*(j-1),ncsf_f(ist)*j-1
                  write(nlist,fmt='(X,F11.8)')
     &            b(kk+(i-1)*navst(ist)*ncsf_f(ist)+jjj)
                enddo
              enddo
              write(nlist,fmt='(A)')''
            endif
c           copy cad to C work
            iii=0
            do i=1,nadt
              do jjj=ncsf_f(ist)*(j-1),ncsf_f(ist)*j-1
                core(imp(k3)+iii)=
     &          b(kk+(i-1)*navst(ist)*ncsf_f(ist)+jjj)
                iii=iii+1
              enddo
            enddo
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' C work cad  '
              do i=1,nadt*ncsf_f(ist)
                write(nlist,fmt='(X,F11.8)')
     &          core(imp(k3)+i-1)
              enddo
              write(nlist,fmt='(A)')''
              write(nlist,fmt='(A)')' T used   '
              do i=1,ncsf_f(ist)*(ncsf_f(ist)-navst(ist))
                write(nlist,fmt='(X,F11.8)')
     &          core(r+i-1)
              enddo
              write(nlist,fmt='(A)')''
              write(nlist,fmt='(A)')' Cred before   '
              do i=1,nadt*(ncsf_f(ist)-1)
                write(nlist,fmt='(X,F11.8)')
     &          core(ii+i-1)
              enddo
              write(nlist,fmt='(A)')''
            endif
            call DGEMM('t','n',(ncsf_f(ist)-navst(ist)),
     &           nadt,ncsf_f(ist),1.0D0,
     &           core(r),ncsf_f(ist),core(imp(k3)),ncsf_f(ist),0.0D0,
     &           core(ii),(ncsf_f(ist)-navst(ist)))
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' Cred after   '
              do i=1,nadt*(ncsf_f(ist)-navst(ist))
                write(nlist,fmt='(X,F11.8)')
     &          core(ii+i-1)
              enddo
              write(nlist,fmt='(A)')''
            endif
          endif
          if (ib_c(2,ist).ne.0) then
c           transform caa
            ii=imp(k1)+nadt*(ncsf_f(ist)-navst(ist))
            kk=ib_c(2,ist)
c           copy caa to C work
            iii=0
            do i=1,naar
              do jjj=ncsf_f(ist)*(j-1),ncsf_f(ist)*j-1
                core(imp(k3)+iii)=
     &          b(kk+(i-1)*navst(ist)*ncsf_f(ist)+jjj)
                iii=iii+1
              enddo
            enddo
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' C work caa  '
              do i=1,naar*ncsf_f(ist)
                write(nlist,fmt='(X,F11.8)')
     &          core(imp(k3)+i-1)
              enddo
              write(nlist,fmt='(A)')''
            endif
            call DGEMM('T','N',(ncsf_f(ist)-navst(ist)),
     &           naar,ncsf_f(ist),1.0D0,
     &           core(r),ncsf_f(ist),core(imp(k3)),ncsf_f(ist),0.0D0,
     &           core(ii),(ncsf_f(ist)-navst(ist)))
          endif
          if (ib_c(3,ist).ne.0) then
c           transform cvd
            ii=imp(k1)+(nadt+naar)*(ncsf_f(ist)-navst(ist))
            kk=ib_c(3,ist)
c           copy cvd to C work
            iii=0
            do i=1,nvdt
              do jjj=ncsf_f(ist)*(j-1),ncsf_f(ist)*j-1
                core(imp(k3)+iii)=
     &          b(kk+(i-1)*navst(ist)*ncsf_f(ist)+jjj)
                iii=iii+1
              enddo
            enddo
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' C work cvd  '
              do i=1,nvdt*ncsf_f(ist)
                write(nlist,fmt='(X,F11.8)')
     &          core(imp(k3)+i-1)
              enddo
              write(nlist,fmt='(A)')''
            endif
            call DGEMM('T','N',(ncsf_f(ist)-navst(ist)),
     &           nvdt,ncsf_f(ist),1.0D0,
     &           core(r),ncsf_f(ist),core(imp(k3)),ncsf_f(ist),0.0D0,
     &           core(ii),(ncsf_f(ist)-navst(ist)))
          endif
          if (ib_c(4,ist).ne.0) then
c           transform cva
            ii=imp(k1)+(nadt+naar+nvdt)*(ncsf_f(ist)-navst(ist))
            kk=ib_c(4,ist)
c           copy cva to C work
            iii=0
            do i=1,nvat
              do jjj=ncsf_f(ist)*(j-1),ncsf_f(ist)*j-1
                core(imp(k3)+iii)=
     &          b(kk+(i-1)*navst(ist)*ncsf_f(ist)+jjj)
                iii=iii+1
              enddo
            enddo
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' C work cva  '
              do i=1,nvat*ncsf_f(ist)
                write(nlist,fmt='(X,F11.8)')
     &          core(imp(k3)+i-1)
              enddo
              write(nlist,fmt='(A)')''
            endif
            call DGEMM('T','N',(ncsf_f(ist)-navst(ist)),
     &           nvat,ncsf_f(ist),1.0D0,
     &           core(r),ncsf_f(ist),core(imp(k3)),ncsf_f(ist),0.0D0,
     &           core(ii),(ncsf_f(ist)-navst(ist)))
          endif
c         set pointers for next loop
          k1=k1+1
        enddo
        k2=k2+1
        k3=k3+1
      enddo
c     print out the cred matrix
      if (prtmicro.ge.4) then
        write(nlist,fmt='(A)')' Cred transposed   '
        k1=kcr
        do ist=1,nst
          do j=1,navst(ist)
            ii=imp(k1)-1
            do p=1,ndimr
              do r=1,ncsf_f(ist)-navst(ist)-1
                write(nlist,fmt='(X,F11.8)',advance='no')
     &          core(ii+(p-1)*(ncsf_f(ist)-1)+r)
              enddo
              r=ncsf_f(ist)-navst(ist)
              write(nlist,fmt='(X,F11.8)',advance='yes')
     &        core(ii+(p-1)*(ncsf_f(ist)-1)+r)
            enddo
          enddo
        enddo
        write(nlist,fmt='(A)')''
      endif
c
c copy the projected matrix to fhm
      k1=kcr
      do ist=1,nst
        do p=1,navst(ist)
          gli=ndimr+1
          glj=0
c         all prev. drts
          do r=1,ist-1
            gli=gli+(ncsf_f(r)-navst(r))*navst(r)
          enddo
c         all prev states
          do j=1,(p-1)
            gli=gli+(ncsf_f(ist)-navst(ist))
          enddo
          do ii=1,ndimr
c           within the full matrix
            glj=glj+1
            kk=(gli+((2*fdim-glj)*(glj-1))/2)
            do jj=1,ncsf_f(ist)-navst(ist)
              k=imp(k1)+(ii-1)*(ncsf_f(ist)-navst(ist))+jj-1
              fhm(kk)=core(k)
              kk=kk+1
            enddo
          enddo
c         set pointers for next loop
          k1=k1+1
        enddo
      enddo
c do now the transformation of M matrices
      k1=kmr
      k2=1
      k3=kbw
      k4=kbi
      do ist=1,nst
        do j=1,navst(ist)
          jj=k2
c         T mat
          r=imp(jj)+ncsf_f(ist)*navst(ist)
          if(ncsf_f(ist).ne.0 .and. ib_m(ist).gt.0)then
            ii=imp(k1)
c           M
            kk=ib_m(ist)+(j-1)*((ncsf_f(ist)*(ncsf_f(ist)+1))/2)
c           copy the M matrix to kbw
            do iii=1,ncsf_f(ist)
              do jjj=1,iii
                core(imp(k3)+(iii-1)*ncsf_f(ist)+jjj-1)=b(kk)
                kk=kk+1
              enddo
            enddo
c           copy upper to lower triangular
            do iii=1,ncsf_f(ist)
              do jjj=iii+1,ncsf_f(ist)
                core(imp(k3)+(iii-1)*ncsf_f(ist)+jjj-1)=
     &          core(imp(k3)+(jjj-1)*ncsf_f(ist)+iii-1)
              enddo
            enddo
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' M copied '
              do iii=1,ncsf_f(ist)
                do jjj=1,ncsf_f(ist)-1
                  write(nlist,fmt='(X,F11.8)',advance='no')
     &            core(imp(k3)+(iii-1)*ncsf_f(ist)+jjj-1)
                enddo
                jjj=ncsf_f(ist)
                write(nlist,fmt='(X,F11.8)',advance='yes')
     &          core(imp(k3)+(iii-1)*ncsf_f(ist)+jjj-1)
              enddo
              write(nlist,fmt='(A)')''
c             first step kbw=Tt*M
              write(nlist,fmt='(A)')' M red  before '
              do iii=1,ncsf_f(ist)-navst(ist)
                do jjj=1,ncsf_f(ist)-navst(ist)-1
                  write(nlist,fmt='(X,F11.8)',advance='no')
     &            core(ii+(iii-1)*(ncsf_f(ist)-navst(ist))+jjj)
                enddo
                jjj=ncsf_f(ist)-navst(ist)
                write(nlist,fmt='(X,F11.8)',advance='yes')
     &          core(ii+(iii-1)*(ncsf_f(ist)-navst(ist))+jjj)
              enddo
              write(nlist,fmt='(A)')''
              write(nlist,fmt='(A)')' T used   '
              do iii=1,ncsf_f(ist)*(ncsf_f(ist)-navst(ist))
                write(nlist,fmt='(X,F11.8)')
     &          core(r+iii-1)
              enddo
              write(nlist,fmt='(A)')''
            endif
            call DGEMM('t','n',(ncsf_f(ist)-navst(ist)),
     &           ncsf_f(ist),ncsf_f(ist),
     &           1.0D0,core(r),ncsf_f(ist),core(imp(k3)),ncsf_f(ist),
     &           0.0D0,core(imp(k4)),(ncsf_f(ist)-navst(ist)))
c           second step Mred=kbw*T
            if (prtmicro.ge.4) then
              write(nlist,fmt='(A)')' M intermediate '
              do iii=1,(ncsf_f(ist)-navst(ist))*ncsf_f(ist)
                write(nlist,fmt='(X,F11.8)')
     &          core(imp(k4)+iii-1)
              enddo
              write(nlist,fmt='(A)')''
            endif
            call DGEMM('n','n',(ncsf_f(ist)-navst(ist)),
     &           (ncsf_f(ist)-navst(ist)),
     &           ncsf_f(ist),1.0D0,core(imp(k4)),
     &           (ncsf_f(ist)-navst(ist)),
     &           core(r),ncsf_f(ist),0.0D0,
     &           core(ii),(ncsf_f(ist)-navst(ist)))
          endif
          if (prtmicro.ge.4) then
          write(nlist,fmt='(A)')' M red final '
            do iii=1,(ncsf_f(ist)-navst(ist))*(ncsf_f(ist)-navst(ist))
              write(nlist,fmt='(X,F11.8)')
     &        core(ii+iii-1)
            enddo
            write(nlist,fmt='(A)')''
          endif
c         set pointers for next loop
          k1=k1+1
        enddo
        k2=k2+1
        k3=k3+1
        k4=k4+1
      enddo
c
      if (prtmicro.ge.4) then
c     print mred
        write(nlist,fmt='(A)')' M red   '
        k1=kmr
        do ist=1,nst
          do j=1,navst(ist)
            ii=imp(k1)
            do p=1,ncsf_f(ist)-navst(ist)
              do r=1,ncsf_f(ist)-navst(ist)-1
                write(nlist,fmt='(X,F11.8)',advance='no')
     &          core(ii+(p-1)*(ncsf_f(ist)-navst(ist))+r-1)
              enddo
              r=ncsf_f(ist)-navst(ist)
              write(nlist,fmt='(X,F11.8)',advance='yes')
     &        core(ii+(p-1)*(ncsf_f(ist)-navst(ist))+r-1)
            enddo
            k1=k1+1
          enddo
        enddo
        write(nlist,fmt='(A)')''
      endif
c
c copy now the M matrix to fhm
      k1=kmr
      do ist=1,nst
        do p=1,navst(ist)
          if(ncsf_f(ist).ne.0 .and. ib_m(ist).gt.0)then
c           calculate global index i
c           within the full matrix
            gli=ndimr+1
c           all prev. drts
            do r=1,ist-1
              gli=gli+(ncsf_f(r)-navst(r))*navst(r)
            enddo
c           all prev states
            do j=1,(p-1)
              gli=gli+(ncsf_f(ist)-navst(r))
            enddo
            do ii=1,ncsf_f(ist)-navst(r)
c             calculate global index j
              glj=ndimr+1
              do r=1,ist-1
                glj=glj+(ncsf_f(r)-navst(r))*navst(r)
              enddo
c             all prev states
              do j=1,(p-1)
                glj=glj+(ncsf_f(ist)-navst(ist))
              enddo
              do jj=1,ii
                k=imp(k1)+(ii-1)*(ncsf_f(ist)-navst(ist))+jj-1
                kk=(gli+((2*fdim-glj)*(glj-1))/2)
                fhm(kk)=core(k)
                glj=glj+1
              enddo
              gli=gli+1
            enddo
          endif
          k1=k1+1
        enddo
      enddo
c
c      initialize fvec
      do i=1,fdim
        fvec(i,1)=0.0
      enddo
c
c transformation of fcsf
      k1=1
      k2=ndimr+1
      k3=1
      do ist=1,nst
        do j=1,navst(ist)
c         T mat pointer
          r=imp(k1)+ncsf_f(ist)*navst(ist)
          if (prtmicro.ge.2) then
            write(nlist,fmt='(A)')' fcsf   '
            do iii=1,ncsf_f(ist)
              write(nlist,fmt='(X,F11.8)')
     &        fcsf(k3+iii-1)
            enddo
            write(nlist,fmt='(A)')''
          endif
          call DGEMV('t',ncsf_f(ist),(ncsf_f(ist)-navst(ist)),1.0D0,
     &         core(r),ncsf_f(ist),fcsf(k3),1,0.0D0,
     &         fvec(k2,1),1)
          if (prtmicro.ge.2) then
            write(nlist,fmt='(A)')' fcsf red  '
            do iii=1,ncsf_f(ist)-navst(ist)
              write(nlist,fmt='(X,F11.8)')
     &        fvec(k2+iii-1,1)
            enddo
            write(nlist,fmt='(A)')''
          endif
          k2=k2+ncsf_f(ist)-navst(ist)
          k3=k3+ncsf_f(ist)
        enddo
        k1=k1+1
      enddo
c
      if (prtmicro.ge.2) then
        p=fdim/8
        write(nlist,fmt='(A)')' Full matrix:'
        do i=1,p
          do j=1,i-1
            write(nlist,fmt='(A)')''
            write(nlist,fmt='(7X)',advance='no')
            do nj=(j-1)*8+1,(j-1)*8+7
              write(nlist,fmt='(I10,2X)',advance='no')nj
            enddo
            write(nlist,fmt='(I10,2X)',advance='yes')(j-1)*8+8
            do ni=(i-1)*8+1,(i-1)*8+8
              write(nlist,fmt='(I6,X)',advance='no')ni
              do nj=(j-1)*8+1,(j-1)*8+7
                write(nlist,fmt='(F12.8)',advance='no')fhm(ni+((2*fdim-
     &          nj)*(nj-1))/2)
              enddo
              nj=(j-1)*8+8
              write(nlist,fmt='(F12.8)',advance='yes')fhm(ni+((2*fdim-
     &        nj)*(nj-1))/2)
            enddo
          enddo
c         print triangular
          j=i
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(7X)',advance='no')
          do nj=(j-1)*8+1,(j-1)*8+7
            write(nlist,fmt='(I10,2X)',advance='no')nj
          enddo
          write(nlist,fmt='(I10,2X)',advance='yes')(j-1)*8+8
          do ni=(i-1)*8+1,(i-1)*8+8
            write(nlist,fmt='(I6,X)',advance='no')ni
            do nj=(j-1)*8+1,ni-1
              write(nlist,fmt='(F12.8)',advance='no')fhm(ni+((2*fdim-nj)
     &        *(nj-1))/2)
            enddo
            nj=ni
            write(nlist,fmt='(F12.8)',advance='yes')fhm(ni+((2*fdim-nj)*
     &      (nj-1))/2)
          enddo
        enddo
c       print rest
        r=mod(fdim,8)
        if (r.ne.0) then 
          i=p+1
          do j=1,i-1
            write(nlist,fmt='(A)')''
c           print full
            write(nlist,fmt='(7X)',advance='no')
            do nj=(j-1)*8+1,(j-1)*8+7
              write(nlist,fmt='(I10,2X)',advance='no')nj
            enddo
            write(nlist,fmt='(I10,2X)',advance='yes')(j-1)*8+8
            do ni=(i-1)*8+1,(i-1)*8+r
              write(nlist,fmt='(I6,X)',advance='no')ni
              do nj=(j-1)*8+1,(j-1)*8+7
                write(nlist,fmt='(F12.8)',advance='no')fhm(ni+((2*fdim-
     &          nj)*(nj-1))/2)
              enddo
              nj=(j-1)*8+8
              write(nlist,fmt='(F12.8)',advance='yes')fhm(ni+((2*fdim-
     &        nj)*(nj-1))/2)
            enddo
          enddo
c         print triangular
          j=i
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(7X)',advance='no')
          do nj=(j-1)*8+1,(j-1)*8+r-1
            write(nlist,fmt='(I10,2X)',advance='no')nj
          enddo
          write(nlist,fmt='(I10,2X)',advance='yes')(j-1)*8+r
          do ni=(i-1)*8+1,(i-1)*8+r
            write(nlist,fmt='(I6,X)',advance='no')ni
            do nj=(j-1)*8+1,ni-1
              write(nlist,fmt='(F12.8)',advance='no')fhm(ni+((2*fdim-nj)
     &        *(nj-1))/2)
            enddo
            nj=ni
            write(nlist,fmt='(F12.8)',advance='yes')fhm(ni+((2*fdim-nj)*
     &      (nj-1))/2)
          enddo
        endif
        write(nlist,fmt='(A)')''
      endif
c
      do i=1,ndimr
        fvec(i,1)=w(i)
      enddo
c
      if (prtmicro.ge.1) then
        write(nlist,fmt='(A)')' fvec before'
        do i=1,fdim
          write(nlist,fmt='(I5,X,F12.8)')i,fvec(i,1)
        enddo
        write(nlist,fmt='(A)')''
      endif
c
      call DSPSV('L', fdim, 1, fhm, ipiv, fvec, fdim, info)
        if (info.ne.0) then
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A)')' ERROR in DSPSV during solving'
          write(nlist,fmt='(A)')' the linear equation.'
          write(nlist,fmt='(A)')''
          write(nlist,fmt='(A,I20)')'info= ',info
          write(nlist,fmt='(A)')''
          call bummer ('DSPSV; info=',info,faterr)
        endif
c multiply solution with -1
      do i=1,fdim
        fvec(i,1)=-fvec(i,1)
      enddo
c
      i=1
      wsr1=fhm(i+((2*fdim-i)*(i-1))/2)
      do i=2,fdim
        if (fhm(i+((2*fdim-i)*(i-1))/2).lt.1.0D0) then
          wsr1=wsr1*fhm(i+((2*fdim-i)*(i-1))/2)
        endif
      enddo
c
      do i=2,fdim
        if (fhm(i+((2*fdim-i)*(i-1))/2).gt.1.0D0) then
          wsr1=wsr1*fhm(i+((2*fdim-i)*(i-1))/2)
        endif
      enddo
c
      do i=1,fdim
        if (ipiv(i).lt.0) then
          write(nlist,fmt='(A,I5,I5)')'Determinant cannot be used: 
     &            i,ipiv(i)= ',i,ipiv(i)
        endif
        if (i.ne.ipiv(i)) then
          wsr1=wsr1*-1.0D0
        endif
      enddo
c
      write(nlist,fmt='(A)')''
      write(nlist,fmt='(A,E16.8)')' |Determinant| = ',dabs(wsr1)
      write(nlist,fmt='(A)')''
c
      if (prtmicro.ge.1) then
        write(nlist,fmt='(A)')' fvec after'
        do i=1,fdim
          write(nlist,fmt='(I5,X,F12.8)')i,fvec(i,1)
        enddo
        write(nlist,fmt='(A)')''
      endif
c
c backtransformation of fcsf
      k1=1
      k2=ndimr+1
      k3=1
      do ist=1,nst
        do j=1,navst(ist)
c         T mat pointer
          r=imp(k1)+ncsf_f(ist)*navst(ist)
          if (prtmicro.ge.4) then
            write(nlist,fmt='(A)')' T used   '
            do iii=1,ncsf_f(ist)*(ncsf_f(ist)-navst(ist))
              write(nlist,fmt='(X,F11.8)')
     &        core(r+iii-1)
            enddo
            write(nlist,fmt='(A)')''
          endif
          call DGEMV('n',ncsf_f(ist),(ncsf_f(ist)-navst(ist)),1.0D0,
     &         core(r),ncsf_f(ist),fvec(k2,1),1,0.0D0,
     &         csfl(k3),1)
          if (prtmicro.ge.2) then
            write(nlist,fmt='(A)')' fcsf red  '
            do iii=1,ncsf_f(ist)-navst(ist)
              write(nlist,fmt='(X,F11.8)')
     &        fvec(k2+iii-1,1)
            enddo
            write(nlist,fmt='(A)')''
          endif
          k2=k2+ncsf_f(ist)-navst(ist)
          k3=k3+ncsf_f(ist)
        enddo
        k1=k1+1
      enddo
c
      if (prtmicro.ge.2) then
        write(nlist,fmt='(A)')' csfl  '
        do iii=1,ncfx
          write(nlist,fmt='(X,F11.8)')
     &    csfl(iii)
        enddo
      endif
c     copy solution
      do i=1,ndimr
        orbl(i)=fvec(i,1)
      enddo
c
c write solution
       if (print.ge.1) then
         call prblkt('final csfl(*):',csfl,
     &   ncfx,ncfx,1,'csf','vec',1,nlist)
        call  prblkt('final orbl(*):',orbl,
     &  ndimr,ndimr,1,'row','vec',1,nlist)
       endif
c
      write(nlist,fmt='(A)')''
      write(nlist,fmt='(A)')' Linear equation solved'
      write(nlist,fmt='(A)')''
c
      end
