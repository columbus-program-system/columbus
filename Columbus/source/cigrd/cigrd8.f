!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine add1a
c
c  calculate some of the arrays in /csymb/.
c  assign symmetry labels to basis functions and orbitals.
c  calculate integer pointers for addressing arrays.
c
c  input required:
c  nbpsy(*)  =number of basis functions of each irrep.
c  nmpsy(*)  =number of orbitals of each irrep.
c  orbtyp(*) =orbital type for each orbital (fc=0,d=1,a=2,v=3,fv=4).
c
c  written by ron shepard.
c
       implicit none
c  ##  parameter & common section
c
      integer nfilmx,iunits
      parameter (nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nsb(8),nnsb(8),n2sb(8),nbft, nntb, n2tb
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,2),nsb(1))
      equivalence (nxy(1,3),nnsb(1))
      equivalence (nxy(1,4),n2sb(1))
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(2),nntb)
      equivalence (nxtot(3),n2tb)
      integer nmpsy(8),nsm(8),nnsm(8),n2sm(8),nmot, nntm, n2tm
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(5),nntm)
      equivalence (nxtot(6),n2tm)
      integer ndpsy(8),nsd(8),nnsd(8),n2sd(8), ndot, nntd, n2td
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxy(1,11),nnsd(1))
      equivalence (nxy(1,12),n2sd(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(8),nntd)
      equivalence (nxtot(9),n2td)
      integer napsy(8),nsa(8),nnsa(8),n2sa(8), nact, nnta, n2ta
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8),nsv(8),nnsv(8),n2sv(8), nvrt, nntv, n2tv
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxy(1,19),nnsv(1))
      equivalence (nxy(1,20),n2sv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(14),nntv)
      equivalence (nxtot(15),n2tv)
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
      integer nsbm(8), nbmt, ndimd, ndima, ndimv
      equivalence (nxy(1,32),nsbm(1))
      equivalence (nxtot(16),nbmt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irb1(8),irb2(8)
      equivalence (nxy(1,33),irb1(1))
      equivalence (nxy(1,34),irb2(1))
      integer irm1(8),irm2(8)
      equivalence (nxy(1,35),irm1(1))
      equivalence (nxy(1,36),irm2(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      integer addpt, numint, bukpt, intoff_mc, szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx, ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symb(nmotx),symm(nmotx),symx(nmotx),invx(nmotx)
      integer orbidx(nmotx),orbtyp(nmotx),nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,2),symb(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (iorbx(1,6),orbidx(1))
      equivalence (iorbx(1,7),orbtyp(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a),(ix0(3),ix0v)
c
      integer mctype, mapivm, mapmi, mapma, mapmd, mapmv, mapml,
     &        mapmm, symi, symv, symm2, syma, symd, mcres
      common/cmomap/mctype(nmotx),mapivm(nmotx),mapmi(nmotx),
     & mapma(nmotx),mapmd(nmotx),mapmv(nmotx),mapml(0:nmotx),
     & mapmm(nmotx),
     & symi(nmotx),symv(nmotx),symm2(nmotx),
     & syma(nmotx),symd(nmotx),mcres(nmotx)
c
c  ## integer section
c
      integer i, ix0a, ix0v, ix0d, ir
      integer j
      integer ntype(3), nvv2, nptot, nvv, nvd, nva, naa2, nb, nad1,
     &        nvd1, nv, nd, na, nad, naa, ndd2, nva1, ndd, nm
      integer t
c
c----------------------------------------------------------------
c
c  initialize orbtyp(*)
c
      do i=1,nmotx
      orbtyp(i)=mctype(i)
      enddo
c
c  initialize nndx(*)
c
      nndx(1)=0
      do 10 i=2,nmotx
10    nndx(i)=nndx(i-1)+(i-1)
c
c  calculate ndpsy(*), napsy(*), and nvpsy(*) from orbtyp(*).
c
c     i2=0
c     do 30 isym=1,8
c     ndpsy(isym)=0
c     napsy(isym)=0
c     nvpsy(isym)=0
c     i1=i2+1
c     i2=i2+nmpsy(isym)
c     do 20 i=i1,i2
c     t=orbtyp(i)
c     if(t.eq.1)then
c        ndpsy(isym)=ndpsy(isym)+1
c     elseif(t.eq.2)then
c        napsy(isym)=napsy(isym)+1
c     elseif(t.eq.3)then
c        nvpsy(isym)=nvpsy(isym)+1
c     endif
c20    continue
c30    continue
c
c  fill in arrays in /csymb/.
c
      nbft=0
      nntb=0
      n2tb=0
      nmot=0
      nntm=0
      n2tm=0
      ndot=0
      nntd=0
      n2td=0
      nact=0
      nnta=0
      n2ta=0
      nvrt=0
      nntv=0
      n2tv=0
c
      do 50 i=1,8
c
      nb=nbpsy(i)
      nm=nmpsy(i)
      nd=ndpsy(i)
      na=napsy(i)
      nv=nvpsy(i)
c
      nsb(i)=nbft
      nsm(i)=nmot
      nsd(i)=ndot
      nsa(i)=nact
      nsv(i)=nvrt
      nbft=nbft+nb
      nmot=nmot+nm
      ndot=ndot+nd
      nact=nact+na
      nvrt=nvrt+nv
c
      irb1(i)=nsb(i)+1
      irm1(i)=nsm(i)+1
      ird1(i)=nsd(i)+1
      ira1(i)=nsa(i)+1
      irv1(i)=nsv(i)+1
c
      irb2(i)=nbft
      irm2(i)=nmot
      ird2(i)=ndot
      ira2(i)=nact
      irv2(i)=nvrt
c
      nnsb(i)=nntb
      nnsm(i)=nntm
      nnsd(i)=nntd
      nnsa(i)=nnta
      nnsv(i)=nntv
      nntb=nntb+nndx(nb+1)
      nntm=nntm+nndx(nm+1)
      nntd=nntd+nndx(nd+1)
      nnta=nnta+nndx(na+1)
      nntv=nntv+nndx(nv+1)
c
      n2sb(i)=n2tb
      n2sm(i)=n2tm
      n2sd(i)=n2td
      n2sa(i)=n2ta
      n2sv(i)=n2tv
      n2tb=n2tb+nb**2
      n2tm=n2tm+nm**2
      n2td=n2td+nd**2
      n2ta=n2ta+na**2
      n2tv=n2tv+nv**2
50    continue
c
c  calculate offsets for ad(*),vd(*), and va(*) totally
c  symmetric arrays and orbital coefficients.
c
      nad1=0
      nvd1=0
      nva1=0
      nbmt=0
      do 60 i=1,8
      nsad(i)=nad1
      nsvd(i)=nvd1
      nsva(i)=nva1
      nsbm(i)=nbmt
      nad1=nad1+napsy(i)*ndpsy(i)
      nvd1=nvd1+nvpsy(i)*ndpsy(i)
      nva1=nva1+nvpsy(i)*napsy(i)
      nbmt=nbmt+nbpsy(i)*nmpsy(i)
60    continue
c
c  allocate space for addressing arrays.
c
      ndd=nndx(ndot+1)
      ndd2=ndot*ndot
      nad=nact*ndot
      naa=nndx(nact+1)
      naa2=nact*nact
      nvd=nvrt*ndot
      nva=nvrt*nact
      nvv=nndx(nvrt+1)
      nvv2=nvrt*nvrt
c
c  add**(*) arrays
c
      nptot=1
c  1:dd
      addpt(1)=nptot
      nptot=nptot+ndd
c  2:dd2
      addpt(2)=nptot
      nptot=nptot+ndd2
c  3:ad
      addpt(3)=nptot
      nptot=nptot+nad
c  4:aa
      addpt(4)=nptot
      nptot=nptot+naa
c  5:vd
      addpt(5)=nptot
      nptot=nptot+nvd
c  6:va
      addpt(6)=nptot
      nptot=nptot+nva
c  7:vv
      addpt(7)=nptot
      nptot=nptot+nvv
c  8:vv2
      addpt(8)=nptot
      nptot=nptot+nvv2
c  9:vv3
      addpt(9)=nptot
      nptot=nptot+nvv2
c
c  off*(*) arrays.
c
c  10:off1(pq)
      addpt(10)=nptot
      nptot=nptot+naa
c  11:off2(pq)
      addpt(11)=nptot
      if(nad.ne.0)nptot=nptot+naa
c  12:off3(pq)
      addpt(12)=nptot
      if(nvd.ne.0)nptot=nptot+naa
c  13:off4(q,p)
      addpt(13)=nptot
      if(nvd.ne.0)nptot=nptot+naa2
c  14:off5(pq)
      addpt(14)=nptot
      if(nva.ne.0)nptot=nptot+naa
c  15:off6(pq)
      addpt(15)=nptot
      if(ndd.ne.0)nptot=nptot+naa
c  16:off7(pq)
      addpt(16)=nptot
      if(nvv.ne.0)nptot=nptot+naa
c  17:off8(q,p)
      addpt(17)=nptot
      if(nvv2.ne.0)nptot=nptot+naa
c  18:off9(ai)
      addpt(18)=nptot
      if(nva.ne.0)nptot=nptot+nvd1
c  19:off10(ai)
      addpt(19)=nptot
      if(nad.ne.0)nptot=nptot+nvd1
c  20:off11(ij)
      addpt(20)=nptot
      if(nvv2.ne.0)nptot=nptot+ndd
c  21:off12(pq)
      addpt(21)=nptot
      if(nvv2.ne.0)nptot=nptot+naa
c  22:off13(pq)
      addpt(22)=nptot
      if(ndd2.ne.0)nptot=nptot+naa
c  23:off14(p,q)
      addpt(23)=nptot
      if(nvd.ne.0)nptot=nptot+naa2
c  24:total space required.
      nptot=nptot-1
      addpt(24)=nptot
c
c-    write(nlist,6010)nptot,addpt
c-6010  format(/' space required for addressing arrays:',i8/(1x,8i8))
c
c  assign symmetry labels to basis functions.
c
      do 120 i=1,8
      do 110 j=irb1(i),irb2(i)
      symb(j)=i
110   continue
120   continue
c-    write(nlist,6020)'symb:',(symb(i),i=1,nbft)
c-6020  format(1x,a,(5(1x,10i1)))
c
c  calculate orbital type offsets, ix0*.
c
      ix0d=0
      ix0a=ix0d+ndot
      ix0v=ix0a+nact
c
c  assign symmetry labels to orbitals.
c
      do 140 i=1,8
      do 130 j=ird1(i),ird2(i)
      symx(ix0d+j)=i
130   continue
140   continue
c-    write(nlist,6020)'symd:',(symx(ix0d+i),i=1,ndot)
c
      do 160 i=1,8
      do 150 j=ira1(i),ira2(i)
      symx(ix0a+j)=i
150   continue
160   continue
c-    write(nlist,6020)'syma:',(symx(ix0a+i),i=1,nact)
c
      do 180 i=1,8
      do 170 j=irv1(i),irv2(i)
      symx(ix0v+j)=i
170   continue
180   continue
c-    write(nlist,6020)'symv:',(symx(ix0v+i),i=1,nvrt)
c
      do 200 i=1,8
      do 190 j=irm1(i),irm2(i)
      symm(j)=i
190   continue
200   continue
c-    write(nlist,6020)'symm:',(symm(i),i=1,nmot)
c
c  calculate orbital mapping and inverse mapping arrays
c  for  inactive, active and virtual orbitals.
c
      ntype(1)=0
      ntype(2)=0
      ntype(3)=0
      do 220 i=1,nmot
      orbidx(i)=0
      t=orbtyp(i)
      if(t.ge.1 .and. t.le.3)then
         ntype(t)=ntype(t)+1
         orbidx(i)=ntype(t)
         ir=i-nsm(symm(i))
         invx(ix0(t)+ntype(t))=ir
      endif
220   continue
c-    write(nlist,6020)'orbtyp:',(orbtyp(i),i=1,nmot)
c-    write(nlist,6030)'orbidx:',(orbidx(i),i=1,nmot)
c-    write(nlist,6030)'invd:',(invx(ix0d+i),i=1,ndot)
c-    write(nlist,6030)'inva:',(invx(ix0a+i),i=1,nact)
c-    write(nlist,6030)'invv:',(invx(ix0v+i),i=1,nvrt)
c-6030  format(1x,a,(5(1x,10i3,3x,10i3/)))
c-    write(nlist,*)' ntype(*)=',ntype
c
c  set diminsions parameters.
c
      ndimd=max(1,ndot)
      ndima=max(1,nact)
      ndimv=max(1,nvrt)
      return
      end
c
c*********************************************************************
c
c deck add2
      subroutine add2a(naar,ic)
c
c  divide the core space, ic(*), for integral addressing
c  array calculation using the pointers, addpt(*).
c
c  written by ron shepard.
c
       implicit none
      integer addpt, numint, bukpt, intoff_mc, szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      integer ic(*)
      integer naar
c
      call add2s(naar,
     +  ic(addpt( 1)),ic(addpt( 2)),ic(addpt( 3)),ic(addpt( 4)),
     +  ic(addpt( 5)),ic(addpt( 6)),ic(addpt( 7)),ic(addpt( 8)),
     +  ic(addpt( 9)),ic(addpt(10)),ic(addpt(11)),ic(addpt(12)),
     +  ic(addpt(13)),ic(addpt(14)),ic(addpt(15)),ic(addpt(16)),
     +  ic(addpt(17)),ic(addpt(18)),ic(addpt(19)),ic(addpt(20)),
     +  ic(addpt(21)),ic(addpt(22)),ic(addpt(23)))
      return
      end
c
c*************************************************************
c
      subroutine add2s(
     & naar,     adddd,   adddd2,   addad,
     & addaa,    addvd,   addva,    addvv,
     & addvv2,   addvv3,  off1,     off2,
     & off3,     off4,    off5,     off6,
     & off7,     off8,    off9,     off10,
     & off11,    off12,   off13,    off14)
c
c  calculate addressing arrays for the required
c  for the hessian and gradient construction.
c  arrays are used for addressing integrals, integral combinations,
c  and hessian blocks.
c
c  let i,j      be inactive orbital labels 1 to ndot,
c      p,q,r,s  be active orbital labels 1 to nact, and
c      a,b      be virtual orbital labels 1 to nvrt.  then
c  the various integral types and hessian blocks are addressed
c  as follows:
c
c  [(pq|rs)] = off1(pq)+ addaa(rs)
c  [(pq|ri)] = off2(pq)+ addad(i,r)
c  [(pq|ia)] = off3(pq)+ addvd(a,i)
c  [(pi|qa)] = off4(q,p)+addvd(a,i)
c  [(pq|ra)] = off5(pq)+ addva(a,r)
c  [(pq|ij)] = off6(pq)+ adddd(ij)
c  [(pi|qj)] = off6(pq)+ adddd2(j,i)
c  [(pq|ab)] = off7(pq)+ addvv(ab)
c  [(pa|qb)] = off8(pq)+ addvv2(b,a)
c
c  [p(bp,ai)]= off9(ai)+ addva(b,p)  =[b(bp,ai)]
c  [p(ai,jp)]= off10(ai)+addad(j,p)  =[b(ai,jp)]
c  [p(ai,bj)]= off11(ij)+addvv3(b,a) =[b(ai,bj)]
c
c  [b(qb,pa)]= off12(pq) + (b,a)
c  [b(ip,jr)]= off13(pr) + (j,i)
c  [b(ap,iq)]= off14(p,q)+ (a,i)
c
c  where e.g., pq=(p*(p-1))/2+q with p.ge.q and p=1 to nact.
c  p(wx,yz) = 4(wx|yz) - (wy|xz) - (wz|yx)
c
c  written by ron shepard.
c
c     navst(ist) - number of states to be averaged in symetry No. ist
c     navst(ist) as parameter added; when state averaging,
c     increase space needed for the c and m matrices (szh(11-15)).

       implicit none
c  ## parameter & common blockj section
c
      integer numopt
      parameter(numopt=10)
      integer cigopt,print
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
c
      integer nfilmx
      parameter (nfilmx=31)
c
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8),ndot, nact
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8), nvrt
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      integer tsymdd(8),tsmdd2(8),tsymad(8),tsymaa(8)
      integer tsymvd(8),tsymva(8),tsymvv(8),tsmvv2(8)
      integer ndimd, ndima, ndimv
      equivalence (nxy(1,21),tsymdd(1))
      equivalence (nxy(1,22),tsmdd2(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,24),tsymaa(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,28),tsmvv2(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      integer addpt, numint, bukpt, intoff_mc, szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
      integer totint
      equivalence (numint(12),totint)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx, ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      integer ix0d, ix0a, ix0v
      equivalence (ix0(1),ix0d),(ix0(2),ix0a),(ix0(3),ix0v)
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer cdir,mdir
      common/direct/mdir,cdir
c
c  ##  integer section
c
      integer adddd(*), a, adddd2(ndimd,ndimd), addaa(*),addvv(*),
     &        addad(ndimd,ndima),addvd(ndimv,ndimd),addva(ndimv,ndima),
     &        addvv2(ndimv,ndimv),addvv3(ndimv,ndimv), ab, asym,
     &        a1, a2, ai, absym
      integer b, b1, b2, ba, bsym
      integer icount, i, ij, isym, i1, i2, ipsym, iasym, ijsym, ist
      integer j, jsym, j1, j2
      integer na, nb, ni, nj, nadr, nvdr, nvar, naar
      integer off1(*),off2(*),off3(*),off4(ndima,ndima), off5(*),
     &        off6(*),off7(*),off8(*),off9(*),off10(*),off11(*),
     &        off12(*),off13(*),off14(ndima,ndima)
      integer p, pq, pasym, pqsym
      integer q
      integer szh6, szh10, szh1, szh7
      integer totoff
c
      character*14 inttyp(11)
c
      integer nndxf
      nndxf(i)=(i*(i-1))/2
c
c---------------------------------------------------------------
c
      totint=0
      do 5 i=1,12
5     numint(i)=0
c
c    # initialize local variable
      inttyp(1)='(pq|rs)'
      inttyp(2)='(pq|ri)'
      inttyp(3)='(pq|ia)'
      inttyp(4)='(pi|qa)'
      inttyp(5)='(pq|ra)'
      inttyp(6)='(pq|ij)(pq|ij)'
      inttyp(7)='(pq|ab)'
      inttyp(8)='(pa|qb)'
      inttyp(9)='p(ai,bp)'
      inttyp(10)='p(ai,jp)'
      inttyp(11)='p(ai,bj)'
c
c  calculate add** arrays and tsym** arrays.
c
      do 10 i=1,8
      tsymdd(i)=0
      tsmdd2(i)=0
      tsymad(i)=0
      tsymaa(i)=0
      tsymvd(i)=0
      tsymva(i)=0
      tsymvv(i)=0
      tsmvv2(i)=0
10    continue
c
c  dd...
c
      if(ndot.ne.0)then
         ij=0
         do 22 i=1,ndot
            do 20 j=1,i
               ij=ij+1
               ijsym=mult(symx(ix0d+j),symx(ix0d+i))
               tsymdd(ijsym)=tsymdd(ijsym)+1
               adddd(ij)=tsymdd(ijsym)
20          continue
22       continue
c      write(nlist,6020)'adddd:',(adddd(i),i=1,ij)
c6020  format(1x,a10,/(1x,10i6))
      endif
c
c  dd2...
c  smallest symmetry index has the most rapidly varying orbital index.
c  all entries are offset by the tsymdd(*) totals to allow the two
c  sets of integral types to be interleaved.
c
      if(ndot.ne.0)then
         do 38 isym=1,nsym
            if(ndpsy(isym).eq.0)go to 38
            i1=ird1(isym)
            i2=ird2(isym)
            do 36 jsym=1,nsym
               if(ndpsy(jsym).eq.0)go to 36
               j1=ird1(jsym)
               j2=ird2(jsym)
               ijsym=mult(jsym,isym)
               if(jsym.gt.isym)then
                  do 32 j=j1,j2
                     do 31 i=i1,i2
                        tsmdd2(ijsym)=tsmdd2(ijsym)+1
                        adddd2(j,i)=tsmdd2(ijsym)+tsymdd(ijsym)
31                   continue
32                continue
               else
                  do 34 i=i1,i2
                     do 33 j=j1,j2
                        tsmdd2(ijsym)=tsmdd2(ijsym)+1
                        adddd2(j,i)=tsmdd2(ijsym)+tsymdd(ijsym)
33                   continue
34                continue
c
               endif
36          continue
38       continue
      endif
c
c  ad...
c
      if(nact.ne.0 .and. ndot.ne.0)then
         do 42 p=1,nact
            do 40 i=1,ndot
               ipsym=mult(symx(ix0d+i),symx(ix0a+p))
               tsymad(ipsym)=tsymad(ipsym)+1
               addad(i,p)=tsymad(ipsym)
40          continue
42       continue
c      write(nlist,6020)'addad:',addad
      endif
c
c  aa...
c
      if(nact.ne.0)then
         pq=0
         do 62 p=1,nact
            do 60 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               tsymaa(pqsym)=tsymaa(pqsym)+1
               addaa(pq)=tsymaa(pqsym)
60          continue
62       continue
c      write(nlist,6020)'addaa:',(addaa(i),i=1,pq)
      endif
c
c  vd...
c
      if(nvrt.ne.0 .and. ndot.ne.0)then
      do 82 i=1,ndot
         do 80 a=1,nvrt
            iasym=mult(symx(ix0v+a),symx(ix0d+i))
            tsymvd(iasym)=tsymvd(iasym)+1
            addvd(a,i)=tsymvd(iasym)
80       continue
82    continue
c      write(nlist,6020)'addvd:',addvd
      endif
c
c  va...
c
      if(nvrt.ne.0 .and. nact.ne.0)then
         do 102 p=1,nact
            do 100 a=1,nvrt
               pasym=mult(symx(ix0v+a),symx(ix0a+p))
               tsymva(pasym)=tsymva(pasym)+1
               addva(a,p)=tsymva(pasym)
100         continue
102      continue
c      write(nlist,6020)'addva:',addva
      endif
c
c  vv...
c
      if(nvrt.ne.0)then
         ab=0
         do 122 a=1,nvrt
            do 120 b=1,a
               ab=ab+1
               absym=mult(symx(ix0v+b),symx(ix0v+a))
               tsymvv(absym)=tsymvv(absym)+1
               addvv(ab)=tsymvv(absym)
120         continue
122      continue
c      write(nlist,6020)'addvv:',(addvv(i),i=1,ab)
      endif
c
c  vv2...
c  smallest symmetry index has the most rapidly varying orbital index.
c
      if(nvrt.ne.0)then
         do 140 asym=1,nsym
            if(nvpsy(asym).eq.0)go to 140
            a1=irv1(asym)
            a2=irv2(asym)
            do 138 bsym=1,nsym
               if(nvpsy(bsym).eq.0)go to 138
               b1=irv1(bsym)
               b2=irv2(bsym)
               absym=mult(bsym,asym)
               if(bsym.gt.asym)then
                  do 133 b=b1,b2
                     do 132 a=a1,a2
                        tsmvv2(absym)=tsmvv2(absym)+1
                        addvv2(b,a)=tsmvv2(absym)
132                  continue
133               continue
               else
                  do 135 a=a1,a2
                     do 134 b=b1,b2
                        tsmvv2(absym)=tsmvv2(absym)+1
                        addvv2(b,a)=tsmvv2(absym)
134                  continue
135               continue
               endif
138         continue
140      continue
c      write(nlist,6020)'addvv2:',addvv2
      endif
c
c  vv3...
c  each symmetry block is addressed independently.
c
      if(nvrt.ne.0)then
         do 160 asym=1,nsym
            if(nvpsy(asym).eq.0)go to 160
            a1=irv1(asym)
            a2=irv2(asym)
            do 158 bsym=1,nsym
               if(nvpsy(bsym).eq.0)go to 158
               b1=irv1(bsym)
               b2=irv2(bsym)
c
               ba=0
               do 157 a=a1,a2
                  do 156 b=b1,b2
                     ba=ba+1
                     addvv3(b,a)=ba
156               continue
157            continue
158         continue
160      continue
c      write(nlist,6020)'addvv3',addvv3
      endif
c
c  calculate offset arrays for various integral types.
c
c*********************************************************
c  [(pq|rs)] = off1(pq)+addaa(rs)
c*********************************************************
c
      if(nact.ne.0)then
         totoff=0
         pq=0
         do 212 p=1,nact
            do 210 q=1,p
               pq=pq+1
               off1(pq)=totoff
               totoff=totoff+addaa(pq)
210         continue
212      continue
         numint(1)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off1:',(off1(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ri)] = off2(pq)+addad(i,r)
c*********************************************************
c
      if(nact.ne.0 .and. ndot.ne.0)then
         totoff=0
         pq=0
         do 412 p=1,nact
            do 410 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off2(pq)=totoff
               totoff=totoff+tsymad(pqsym)
410         continue
412      continue
         numint(2)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off2:',(off2(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ia)] = off3(pq)+addvd(a,i)
c*********************************************************
c
      if((nact.ne.0).and.(ndot.ne.0).and.(nvrt.ne.0))then
         totoff=0
         pq=0
         do 712 p=1,nact
            do 710 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off3(pq)=totoff
               totoff=totoff+tsymvd(pqsym)
710         continue
712      continue
         numint(3)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off3:',(off3(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pi|qa)] = off4(q,p)+addvd(a,i)
c*********************************************************
c
      if(nact.ne.0 .and. ndot.ne.0 .and. nvrt.ne.0)then
         totoff=0
         do 812 p=1,nact
            do 810 q=1,nact
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off4(q,p)=totoff
               totoff=totoff+tsymvd(pqsym)
810         continue
812      continue
         numint(4)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off4:',off4,totoff
      endif
c
c*********************************************************
c  [(pq|ra)] = off5(pq)+addva(a,r)
c*********************************************************
c
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 312 p=1,nact
            do 310 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off5(pq)=totoff
               totoff=totoff+tsymva(pqsym)
310         continue
312      continue
         numint(5)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off5:',(off5(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ij)] = off6(pq)+adddd(ij)
c  [(pi|qj)] = off6(pq)+adddd2(j,i)
c*********************************************************
c
      if(nact.ne.0 .and. ndot.ne.0)then
         totoff=0
         pq=0
         do 512 p=1,nact
            do 510 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off6(pq)=totoff
               totoff=totoff+tsymdd(pqsym)+tsmdd2(pqsym)
510         continue
512      continue
         numint(6)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off6:',(off6(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pq|ab)] = off7(pq)+addvv(ab)
c*********************************************************
c
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 912 p=1,nact
            do 910 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off7(pq)=totoff
               totoff=totoff+tsymvv(pqsym)
910         continue
912      continue
         numint(7)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off7:',(off7(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [(pa|qb)] = off8(pq)+addvv(b,a)
c*********************************************************
c
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 1012 p=1,nact
            do 1010 q=1,p
               pq=pq+1
               pqsym=mult(symx(ix0a+q),symx(ix0a+p))
               off8(pq)=totoff
               totoff=totoff+tsmvv2(pqsym)
1010        continue
1012     continue
         numint(8)=totoff
         totint=totint+totoff
c      write(nlist,6020)'off8:',(off8(i),i=1,pq),totoff
      endif
c
c*********************************************************
c  [p(ai,bp)] = off9(ai)+addva(b,p)
c*********************************************************
c
      if(tsymvd(1).ne.0 .and. tsymva(1).ne.0)then
         totoff=0
         do 1510 ai=1,tsymvd(1)
            off9(ai)=totoff
            totoff=totoff+tsymva(1)
1510     continue
         numint(9)=totoff
         totint=totint+totoff
      endif
c
c*********************************************************
c  [p(ai,jp)] = off10(ai)+addad(j,p)
c*********************************************************
c
      if(tsymad(1).ne.0 .and. tsymvd(1).ne.0)then
         totoff=0
         do 1710 ai=1,tsymvd(1)
            off10(ai)=totoff
            totoff=totoff+tsymad(1)
1710     continue
         numint(10)=totoff
         totint=totint+totoff
      endif
c
c*********************************************************
c  [p(ai,bj)] = off11(ij)+addvv3(b,a)
c*********************************************************
c
      szh6=0
      if(ndot.ne.0 .and. nvrt.ne.0)then
         totoff=0
         ij=0
         do 1812 i=1,ndot
            na=nvpsy(symx(ix0d+i))
            do 1810 j=1,i
               ij=ij+1
               nb=nvpsy(symx(ix0d+j))
               off11(ij)=totoff
               totoff=totoff+na*nb
1810        continue
1812     continue
         szh6=totoff
         numint(11)=totoff
c      write(nlist,6020)'off11:',(off11(i),i=1,ij),totoff
      endif
c
      if (print.gt.1) then
      write(nlist,'(/'' Total number of integrals addressed: '',i8/)')
     + totint
      write(nlist,'('' Int.type'',10x,''no.of ints'')')
         do i=1,11
         write(nlist,'(2x,a14,3x,i8)')inttyp(i),numint(i)
         enddo
      endif
c
c*********************************************************
c  [b(qb,pa)]  = off12(pq)+ (b,a)
c*********************************************************
c
      szh10=0
      if(nact.ne.0 .and. nvrt.ne.0)then
         totoff=0
         pq=0
         do 1912 p=1,nact
            na=nvpsy(symx(ix0a+p))
            do 1910 q=1,p
               pq=pq+1
               nb=nvpsy(symx(ix0a+q))
               off12(pq)=totoff
               totoff=totoff+na*nb
1910        continue
1912     continue
         szh10=totoff
      endif
c
c*********************************************************
c  [b(ip|jq)] = off13(pq) + (j,i)
c*********************************************************
c
      szh1=0
      if(ndot.ne.0 .and. nact.ne.0)then
         totoff=0
         pq=0
         do 2012 p=1,nact
            ni=ndpsy(symx(ix0a+p))
            do 2010 q=1,p
               nj=ndpsy(symx(ix0a+q))
               pq=pq+1
               off13(pq)=totoff
               totoff=totoff+ni*nj
2010        continue
2012     continue
         szh1=totoff
      endif
c
c*********************************************************
c  [b(ap,iq)] = off14(p,q) + (a,i)
c*********************************************************
c
      szh7=0
      if(nvrt.ne.0 .and. nact.ne.0 .and. ndot.ne.0)then
         totoff=0
         do 2111 q=1,nact
            ni=ndpsy(symx(ix0a+q))
            do 2110 p=1,nact
               na=nvpsy(symx(ix0a+p))
               off14(p,q)=totoff
               totoff=totoff+na*ni
2110        continue
2111     continue
         szh7=totoff
      endif
c
c  save hessian sizes for the various blocks.
c  naar=number of allowed active-active rotations.
c  nadr=number of active-inactive rotations.
c  nvdr=number of virtual-inactive rotations.
c  nvar=number of virtual-active rotations.
c  ncsf=number of csfs in the wave function expansion.
c
      nadr=tsymad(1)
      nvdr=tsymvd(1)
      nvar=tsymva(1)
c  ad,ad
c  stored as a sequence (1...naa) of rectangular (nj,ni) blocks.
      szh(1)=szh1
c  aa,ad
      szh(2)=naar*nadr
c  aa,aa
      szh(3)=nndxf(naar+1)
c  vd,ad
      if (flags(22)) then
         szh(4)=min(1,nvdr*nadr)
      else
         szh(4)=nvdr*nadr
      endif
c  vd,aa
      szh(5)=nvdr*naar
c  vd,vd
c  stored as a sequence (1...ndd) of rectangular (nb,na) blocks.
      if (flags(22)) then
         szh(6)=min(1,szh6)
      else
         szh(6)=szh6
      endif
c  va,ad
c  stored as a sequence (1...na*na) of rectangular (na,ni) blocks.
      szh(7)=szh7
c  va,aa
      szh(8)=nvar*naar
c  va,vd
      if (flags(22)) then
         szh(9)=min(1,nvar*nvdr)
      else
         szh(9)=nvar*nvdr
      endif
c  va,va
c  stored as a sequence (1...naa) of rectangular (nb,na) blocks.
      szh(10)=szh10
c    for state averaging change the C and M matrix dimesions
c
      icount = 0
      do ist=1,nst
      icount = icount + navst(ist)*ncsf_f(ist)
      enddo
c  csf,ad
      szh(11)=icount*nadr
      if (cdir.eq.1) szh(11) = 0
c  csf,aa
      szh(12)=icount*naar
      if (cdir.eq.1) szh(12) = 0
c  csf,vd
      szh(13)=icount*nvdr
      if (cdir.eq.1) szh(13) = 0
c  csf,va
      szh(14)=icount*nvar
      if (cdir.eq.1) szh(14) = 0
c
c    dimensions for nst M matrix blocks
c
c  csf,csf
      szh(15)=0
       do ist =1,nst
       szh(15) = szh(15) +
     &  nndxf(navst(ist)*ncsf_f(ist)+1)
       enddo ! end of: do ist =1,nst

       if (mdir.eq.1) szh(15)=0
c
      return
      end
c
c**********************************************************
c
      subroutine mosort(core,lastb,avcore,avc2is,add,h1,
     +  moints,lenbuf,nipbuf,lenbuk,npbuk,ecore0)
c
c  control 2-e integral sorting.
c
c  input:
c  core(*)  = workspace.
c  avcore   = amount of core available.
c  avc2is   = amount of core allocated for second-half integral sort.
c  add(*)   = pre-calculated address arrays.
c  h1(*)    = mo-h1 integrals.
c  lenbuf   = length of buffers for input integrals.
c  nipbuf   = number of integrals in each input buffer.
c  lenbfs   = length of output buffers of sorted integrals.
c
c  output:
c  h1(*)    = modified to include effective inactive-orbital terms.
c  ecore0   = e0 for core orbitals.
c  lastb(*) = record pointer of last bucket written of each bin.
c  lenbuk   = length of da records of sorted integrals.
c  npbuk    = number of integrals in each da record.
c
c  written by ron shepard.
c  2010-05-25: modified by felix plasser to comply with new
c     transformation in mcscf (by thomas mueller)
c
      implicit integer(a-z)
      real*8 core(avcore),h1(*),ecore0
      integer add(*),lastb(*)
c
      integer nfilmx
      parameter (nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist,stape
      equivalence (iunits(1),nlist)
      equivalence (iunits(21),stape)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      equivalence (nxy(1,25),nvdt)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
      equivalence (bukpt(12),nbuk)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      common/cbufs/lenbfs,h2bpt
c
      common/clda_mc/ldamin,ldamax,ldainc
c
c  ldamin=minimum da record length for integral sorting.
c  ldamax=maximum da record length.
c  ldainc=size increment for da record length
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c  local core pointers:
c
      integer cpt1(8),cpt2(4)
c
      integer  forbyt
      external forbyt
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8 zero
      parameter(zero=0d0)
c
      integer imd
cmd
c
      integer nmotx
       parameter (nmotx=1023)
cfp
      integer ntitmax,nenrgmx
      parameter (ntitmax=10,nenrgmx=10)
      real*8 energy(nenrgmx)
      integer moints,nbft,ierr,nsymx,ntitxx,ninfomo,nenrgy,nmap
      integer ietype(nenrgmx),nmpsy(8),infomo(10)
      integer imtype 
      character*80 titlemo(ntitmax)
      character*4  slabel(8)
      character*8  molab(nmotx)
c
c
c  function numpbf(l) gives the number of integrals held in a
c  working precision buffer of length=l.
c
      if(flags(3))call timer(' ',1,event1,nlist)
c
c  initialize core energy.
c
      ecore0=zero
      call ecore(h1,ecore0)
c
c  allocate space for both halves of the integral sort:
c
c  core allocation for first-half sort:
c  1:bufi,2:vbufi,3:ibufi,4:buk,5:ibuk,6:bknt,7:bufb
c
      lenbuk=0
      npbuk=0
c
c  nbuk has been calculated previously based on avc2is.
c  determine lenbuk.
c
      do 100 lenbkx=ldamin,ldamax,ldainc
      npbkx=numpbf(lenbkx)
      req=lenbuf+nipbuf+forbyt(4*nipbuf)+
     &    (npbkx*nbuk)+forbyt(npbkx*nbuk)+
     &    2*forbyt(nbuk)+lenbkx
      if(req.gt.avcore)go to 200
      lenbuk=lenbkx
      npbuk=npbkx
100   continue
200   if(lenbuk.lt.ldamin)go to 1000
c
c  assign pointers for first-half sort:
c
      cpt1(1)=1
      cpt1(2)=cpt1(1)+lenbuf
      cpt1(3)=cpt1(2)+nipbuf
      cpt1(4)=cpt1(3)+forbyt(4*nipbuf)
      cpt1(5)=cpt1(4)+(npbuk*nbuk)
      cpt1(6)=cpt1(5)+forbyt(npbuk*nbuk)
      cpt1(7)=cpt1(6)+forbyt(nbuk)
      cpt1(8)=cpt1(7)+lenbuk
c-    write(nlist,6010)'cpt1(*)=',cpt1
6010  format(' sortmo: ',a,8i8)
c
c  pointers for second-half sort:
c  1:buk,2:ibuk,3:bufs+core
c
      cpt2(1)=1
      cpt2(2)=cpt2(1)+lenbuk
      cpt2(3)=cpt2(2)+forbyt(npbuk+1)
      cpt2(4)=cpt2(3)+lenbfs
c      write(nlist,*)'lenbuk,forbyt(npbuk+1),lenbfs',
c     & lenbuk,forbyt(npbuk+1),lenbfs
c      write(nlist,'(''  cpt2('',i1,'')= '',i5)')
c     & (imd,cpt2(imd),imd=1,4)
c      write(nlist,*)'avcore= ',avcore
      avcisx=avcore-cpt2(4)+1
      write(nlist,6020)avc2is,avcisx
6020  format(/' mosort: allocated sort2 space, avc2is=',i12,
     +  ' available sort2 space, avcisx=',i12)
      if(avc2is.gt.avcisx)go to 1010
c
c  first-half sort:
c
      open(file='moints',unit=moints,form='unformatted') 
        call sifrh1( 
     &  moints, ntitxx, nsymx, nbft, 
     &  ninfomo, nenrgy, nmap, ierr )
!
      if ( ierr .ne. 0 ) then
         call bummer('mosort: from sifrh1, ierr=',ierr,faterr)
      elseif ( ntitxx .gt. ntitmax ) then
         call bummer('mosort: from sifrh1, ntitao=',ntitxx,faterr)
      elseif ( nbft .gt. nmotx ) then
         call bummer('mosort: from sifrh1, nbft=',nbft,faterr)
      elseif ( ninfomo .gt. 10 ) then
         call bummer('mosort: from sifrh1, ninfomo=',ninfao,faterr)
      elseif ( nenrgy .gt. nenrgmx ) then
         call bummer('mosort: from sifrh1, nenrgy=',nenrgy,faterr)
      endif

      call sifrh2( 
     &  moints, ntitxx, nsymx,   nbft, 
     &  ninfomo, nenrgy, 0,   titlemo, 
     &  nmbpsy,  slabel, infomo, molab, 
     &  ietype, energy, imtype, map, 
     &  ierr   )
c
c  assign pointers for first-half sort:
c
      cpt1(1)=1
      cpt1(2)=cpt1(1)+infomo(4) 
      cpt1(3)=cpt1(2)+infomo(5)
      cpt1(4)=cpt1(3)+forbyt(4*infomo(5))
      cpt1(5)=cpt1(4)+(npbuk*nbuk)
      cpt1(6)=cpt1(5)+forbyt(npbuk*nbuk)
      cpt1(7)=cpt1(6)+forbyt(nbuk)
      cpt1(8)=cpt1(7)+lenbuk
      ifmt=0
      if (nbft.gt.255) ifmt=1
      call sifsk1(moints,infomo,ierr ) 
c
      if(flags(3))call timer(' ',1,event2,nlist)
      call da2ow(lenbuk)
      call mosrt1(avc2is,moints,infomo(4),infomo(5),
     +  core(cpt1(1)),core(cpt1(2)),core(cpt1(3)),h1,
     +  lenbuk,core(cpt1(4)),npbuk,core(cpt1(5)),
     +  lastb,core(cpt1(6)),core(cpt1(7)),
     +  add(addpt( 1)),add(addpt( 2)),add(addpt( 3)),add(addpt( 4)),
     +  add(addpt( 5)),add(addpt( 6)),add(addpt( 7)),add(addpt( 8)),
     +  add(addpt( 9)),add(addpt(10)),add(addpt(11)),add(addpt(12)),
     +  add(addpt(13)),add(addpt(14)),add(addpt(15)),add(addpt(16)),
     +  add(addpt(17)),add(addpt(18)),add(addpt(19)),add(addpt(20)),
     +  moints,infomo,ifmt)
      call da2cw
      close(moints)
      if(flags(3))call timer('mosrt1',3,event2,nlist)
c
c  update core reference energy with modified integrals.
c
      call ecore(h1,ecore0)
c
c  second-half sort:
c  for cases where avc2is is greater than the number of integrals
c  to be addressed, reduce the core allocated.
c
      avc2x=min(avc2is,intoff_mc(12))
c
      call da2or
      rewind stape
      call mosrt2(lastb,core(cpt2(3)),avc2x,
     +  core(cpt2(1)),lenbuk,core(cpt2(2)),npbuk)
      if(flags(3))call timer('mosrt2',4,event2,nlist)
      if(flags(3))call timer('mosort',4,event1,nlist)
      return
c
1000  write(nlist,6100)avcore
6100  format(/' *** mosort: not enough core. avcore=',i8,' ***')
      call bummer('mosort: avcore=',avcore,faterr)
1010  write(nlist,6110)
6110  format(/' *** mosort: avc2is not large enough.')
      call bummer('mosort: avc2is error, avcore=',avcore,faterr)
      return
      end
c
c***************************************************************
c
      subroutine mosrt1(avc2is,itape,lenbuf,nipbuf,
     +  bufi,vali,ibufi,u,
     +  lenbuk,buk,npbuk,ibuk,lastb,bknt,bufb,
     +  adddd,adddd2,addad,addaa,addvd,addva,addvv,addvv2,addvv3,
     +  off1,off2,off3,off4,off5,off6,off7,off8,
     +  off9,off10,off11,
     +  moints,infomo,ifmt)
c
c  read and sorts the 2-e mo integrals.  the
c  integrals are first divided into types depending on the
c  number of inactive, active, and virtual orbitals.
c  partial sums are collected where appropriate, and the
c  integrals are assigned addresses and placed into buckets
c  for direct access i/o.  the following is a list of the
c  integral types, assigned indices, u(*) matrix contributions
c
c  where:    u(xy)= h1(xy) +   sum  [2(xy|kk)-(xk|yk)],
c                            k(=inactive)
c
c  and the addressing scheme used for each integral type.
c  indices i,j,k,l are for inactive orbitals,
c  indices p,q,r,s are for active orbitals, and
c  indices a,b,c,d are for virtual orbitals.
c
c   #    type    labels     u(*)   addressing scheme
c  ---   ----    -------    ----   -----------------
c   1    dddd    (ij|kl)    udd    -----
c   2    addd    (pi|jk)    uad    -----
c   3    adad    (pi|qj)    uaa    off6(pq)+adddd2(j,i)
c   4    aadd    (pq|ij)    uaa    off6(pq)+adddd(ij)
c   5    aaad    (pq|ri)    ---    off2(pq)+addad(i,r)
c   6    aaaa    (pq|rs)    ---    off1(pq)+addaa(rs)
c   7    vddd    (ai|jk)    uvd    -----
c   8    vdad    (ai|pj)    uva    p[off10(ai)+addad(j,p)]
c   9    vdaa    (ai|pq)    ---    off3(pq)+addvd(a,i)
c  10    vdvd    (ai|bj)    uvv    p[off11(ij)+addvv3(b,a)]
c  11    vadd    (ap|ij)    uva    p[off10(ai)+addad(j,p)]
c  12    vaad    (aq|pi)    ---    off4(q,p)+addvd(a,i)
c  13    vaaa    (ar|pq)    ---    off5(pq)+addva(a,r)
c  14    vavd    (ap|bi)    ---    p[off9(ai)+addva(b,p)
c  15    vava    (ap|bq)    ---    off8(pq)+addvv2(b,a)
c  16    vvdd    (ab|ij)    uvv    p[off11(ij)+addvv3(b,a)]
c  17    vvad    (ab|pi)    ---    p[off9(ai)+addva(b,p)
c  18    vvaa    (ab|pq)    ---    off7(pq)+addvv(ab)
c  19    vvvd     -----     ---    -----
c  20    vvva     -----     ---    -----
c  21    vvvv     -----     ---    -----
c
cmd
c   In case you run "direct mcscf" (flags(22)=.true.) following
c   integrals will be omitted:
c
c   #    integral type:        except:
c  ---   -------------         -------
c   8       vdad                ----
c   10      vdvd               (ia|ia)
c   11      vadd                ----
c   14      vavd                ----
c   16      ddvv               (ii|aa)
c   17      vvad                ----
cmd
c  input:
c  u(*)       :1-e h matrix.
c  bufi(*)    :input integral buffer.
c  vali(*)    :input integral value bufer.
c  ibufi(*)   :input integral label buffer.
c  buk(*)     :output integral buffer.
c  ibuk(*)    :output integral label buffer.
c  bufb(*)    :output da record buffer.
c  off*(*)    :addressing arrays for various types of integrals.
c  add**(*)   :addressing arrays.
c  orbtyp(*)  :gives the type (fc=0,d=1,a=2,v=3,fv=4) of each orbital.
c  symm(*)    :gives the symmetry of each orbital.
c  orbidx(*)  :gives the orbital number within its type.
c  avc2is     :available core during second-half of the integral sort.
c
c  output:
c  u(*)       :modified to include the inactive-orbital contributions
c              to form the effective 1-e hamiltonian matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer numopt
      parameter(numopt=10)
      integer cigopt,print
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
c
      integer nfilmx
          parameter (nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
      equivalence (bukpt(12),nbuk)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(4),nmot)
      integer nmpsy(8),nsm(8),nnsm(8),nvpsy(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symm(nmotx),orbidx(nmotx),orbtyp(nmotx),nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
      equivalence (iorbx(1,7),orbtyp(1))
c
      real*8 bufi(lenbuf),vali(nipbuf)
      real*8 u(*),buk(npbuk,nbuk),bufb(lenbuk)
      parameter (nipv=4)
      integer ibufi(nipv,nipbuf),ibuk(npbuk,nbuk),lastb(nbuk),bknt(nbuk)
      integer adddd(*),addaa(*),addvv(*)
      integer adddd2(ndimd,ndimd),addad(ndimd,ndima)
      integer addvd(ndimv,ndimd),addva(ndimv,ndima)
      integer addvv2(ndimv,ndimv),addvv3(ndimv,ndimv)
      integer off1(*),off2(*),off3(*)
      integer off4(ndima,ndima)
      integer off5(*),off6(*),off7(*),off8(*),off9(*),off10(*),off11(*)
      integer moints,infomo(10),ifmt,ierr,last,nbuf
      integer itypeb,itypea
c
      logical qnosrt(0:4)
      integer iknti(22),iknto(22),addx(2),bukx(2)
      real*8 vint,valx(2)
      real*8     two,    three,    four
      parameter (two=2d0,three=3d0,four=4d0)
      parameter (more=0)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)

c
      data qnosrt/.true., .false., .false., .false., .true./
c
c  initialize some sorting arrays.
c
cmd
c     for debug ...
c
cmd   open(unit=99,file='int_test',status='unknown')
cmd   write(99,*)nbft
cmd
      do 10 i=1,22
      iknto(i)=0
10    iknti(i)=0
c
      do 20 i=1,nbuk
      lastb(i)=0
20    bknt(i)=0
c
c  process labeled 2-e integrals.  the integral file must be
c  positioned correctly prior to entry.
c
c
100   continue
c
c  read a buffer of labeled 2-e integrals.
c
cfp      call rdlab2(itape,lenbuf,bufi,nipbuf,vali,nipv,ibufi,num,last)
        itypea=3
        itypeb=0
        call sifrd2(moints,infomo,nipv,0,bufi,num,last,itypea,
     .       itypeb,itmp,vali,ibufi,itmp,ierr)
c
c  process integrals in vali(*)
c
      do 30000 int=1,num
c
      vint=vali(int)
      l1=ibufi(1,int)
      if(l1.eq.0)go to 30000
      l2=ibufi(2,int)
      l3=ibufi(3,int)
      l4=ibufi(4,int)
      t1=orbtyp(l1)
      if(qnosrt(t1))go to 22000
      t2=orbtyp(l2)
      if(qnosrt(t2))go to 22000
      t3=orbtyp(l3)
      if(qnosrt(t3))go to 22000
      t4=orbtyp(l4)
      if(qnosrt(t4))go to 22000
c
      if(t1.lt.t2)then
         t=l1
         l1=l2
         l2=t
         t=t1
         t1=t2
         t2=t
      endif
      if(t3.lt.t4)then
         t=l3
         l3=l4
         l4=t
         t=t3
         t3=t4
         t4=t
      endif
      tt1=nndx(t1)+t2
      tt2=nndx(t3)+t4
      if(tt1.lt.tt2)then
         t=l1
         l1=l3
         l3=t
         t=l2
         l2=l4
         l4=t
         t=tt1
         tt1=tt2
         tt2=t
      endif
      type=nndx(tt1)+tt2
      iknti(type)=iknti(type)+1
      go to (1000,2000,3000,4000,5000,6000,7000,
     +  8000,9000,10000,11000,12000,13000,14000,
     +  15000,16000,17000,18000,19000,20000,21000),type
c*****************************************************************
1000  continue
c
c  dddd  place integral into u(*)
c
c  place indices in canonical order.
c
      if(l1.lt.l2)then
         t=l1
         l1=l2
         l2=t
      endif
      if(l3.lt.l4)then
         t=l3
         l3=l4
         l4=t
      endif
      ll1=nndx(l1)+l2
      ll2=nndx(l3)+l4
      if(ll1.lt.ll2)then
         t=l1
         l1=l3
         l3=t
         t=l2
         l2=l4
         l4=t
      endif
      i=l1
      j=l2
      k=l3
      l=l4
      isym=symm(i)
      jsym=symm(j)
      ksym=symm(k)
      lsym=symm(l)
      if(i.ne.j)go to 1070
      if(k.ne.l)go to 1050
      if(i.ne.k)go to 1040
c (ii|ii)
      i=i-nsm(isym)
      ii=nnsm(isym)+nndx(i)+i
      u(ii)=u(ii)+vint
      go to 1130
1040  continue
c (ii|kk)
      i=i-nsm(isym)
      k=k-nsm(ksym)
      ii=nnsm(isym)+nndx(i)+i
      kk=nnsm(ksym)+nndx(k)+k
      u(ii)=u(ii)+two*vint
      u(kk)=u(kk)+two*vint
      go to 1130
1050  continue
      if(i.ne.k)go to 1060
c (ii|il)
      i=i-nsm(isym)
      l=l-nsm(isym)
      il=nnsm(isym)+nndx(i)+l
      u(il)=u(il)+vint
      go to 1130
1060  continue
c (ii|kl)
      k=k-nsm(ksym)
      l=l-nsm(ksym)
      kl=nnsm(ksym)+nndx(k)+l
      u(kl)=u(kl)+two*vint
      go to 1130
1070  continue
      if(k.ne.l)go to 1090
      if(j.ne.k)go to 1080
c (ij|jj)
      i=i-nsm(isym)
      j=j-nsm(isym)
      ij=nnsm(isym)+nndx(i)+j
      u(ij)=u(ij)+vint
      go to 1130
1080  continue
c (ij|kk)
      i=i-nsm(isym)
      j=j-nsm(isym)
      ij=nnsm(isym)+nndx(i)+j
      u(ij)=u(ij)+two*vint
      go to 1130
1090  continue
      if(j.ne.l)go to 1110
      if(i.ne.k)go to 1100
c (ij|ij)
      i=i-nsm(isym)
      j=j-nsm(jsym)
      ii=nnsm(isym)+nndx(i)+i
      jj=nnsm(jsym)+nndx(j)+j
      u(ii)=u(ii)-vint
      u(jj)=u(jj)-vint
      go to 1130
1100  continue
c (ij|kj)
      i=i-nsm(isym)
      k=k-nsm(isym)
      ik=nnsm(isym)+nndx(i)+k
      u(ik)=u(ik)-vint
      go to 1130
1110  continue
      if(j.ne.k)go to 1120
c (ij|jl)
      i=i-nsm(isym)
      l=l-nsm(isym)
      il=nnsm(isym)+nndx(i)+l
      u(il)=u(il)-vint
      go to 1130
1120  continue
      if(i.ne.k)go to 1130
c (ij|il)
      j=j-nsm(jsym)
      l=l-nsm(jsym)
      jl=nnsm(jsym)+nndx(j)+l
      u(jl)=u(jl)-vint
1130  continue
      go to 30000
c*****************************************************************
2000  continue
c
c  addd     place integral in u(*)
c
      p=l1
      psym=symm(p)
      i=l2
      j=l3
      k=l4
      if(j.ne.k)go to 2020
      if(i.ne.j)go to 2010
c  (pi|ii)
      p=p-nsm(psym)
      i=i-nsm(psym)
      pi=nnsm(psym)+nndx(max(i,p))+min(i,p)
      u(pi)=u(pi)+vint
      go to 2050
2010  continue
c  (pi|jj)
      p=p-nsm(psym)
      i=i-nsm(psym)
      pi=nnsm(psym)+nndx(max(i,p))+min(i,p)
      u(pi)=u(pi)+two*vint
      go to 2050
2020  if(i.ne.j)go to 2030
c  (pi|ik)
      p=p-nsm(psym)
      k=k-nsm(psym)
      pk=nnsm(psym)+nndx(max(k,p))+min(k,p)
      u(pk)=u(pk)-vint
      go to 2050
2030  if(i.ne.k)go to 2050
c  (pi|ji)
      p=p-nsm(psym)
      j=j-nsm(psym)
      pj=nnsm(psym)+nndx(max(j,p))+min(j,p)
      u(pj)=u(pj)-vint
2050  continue
c  (pi|jk)
      go to 30000
c*****************************************************************
3000  continue
c
c  adad   place integral into u(*) and sort.
c
      if(l2.eq.l4)then
         psym=symm(l1)
         p=l1-nsm(psym)
         q=l3-nsm(psym)
         pq=nnsm(psym)+nndx(max(p,q))+min(p,q)
         u(pq)=u(pq)-vint
       endif
      if(bukpt(6).eq.0)go to 30000
      p=orbidx(l1)
      i=orbidx(l2)
      q=orbidx(l3)
      j=orbidx(l4)
      if(p.lt.q)then
         t=p
         p=q
         q=t
         t=i
         i=j
         j=t
      endif
      pq=nndx(p)+q
      addx(1)=intoff_mc(6)+off6(pq)+adddd2(j,i)
      bukx(1)=bukpt(6)+(addx(1)-1)/avc2is
      valx(1)=vint
      if(p.ne.q .or. i.eq.j)go to 24001
c  (pi|pj)=(pj|pi)
      addx(2)=intoff_mc(6)+off6(pq)+adddd2(i,j)
      bukx(2)=bukpt(6)+(addx(2)-1)/avc2is
      valx(2)=vint
      go to 24002
c*****************************************************************
4000  continue
c
c  aadd    place in u(*) and sort.
c
      if(l3.eq.l4)then
         psym=symm(l1)
         p=l1-nsm(psym)
         q=l2-nsm(psym)
         pq=nnsm(psym)+nndx(max(p,q))+min(p,q)
         u(pq)=u(pq)+two*vint
      endif
      if(bukpt(6).eq.0)go to 30000
      p=orbidx(l1)
      q=orbidx(l2)
      i=orbidx(l3)
      j=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      ij=nndx(max(i,j))+min(i,j)
      addx(1)=intoff_mc(6)+off6(pq)+adddd(ij)
      bukx(1)=bukpt(6)+(addx(1)-1)/avc2is
      valx(1)=vint
      go to 24001
c*****************************************************************
5000  continue
c
c  aaad    sort integral
c
      if(bukpt(2).eq.0)go to 30000
      p=orbidx(l1)
      q=orbidx(l2)
      r=orbidx(l3)
      i=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      addx(1)=intoff_mc(2)+off2(pq)+addad(i,r)
      bukx(1)=bukpt(2)
      valx(1)=vint
      go to 24001
c*****************************************************************
6000  continue
c
c  aaaa   sort integral
c
      if(bukpt(1).eq.0)go to 30000
      p=orbidx(l1)
      q=orbidx(l2)
      r=orbidx(l3)
      s=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      rs=nndx(max(r,s))+min(r,s)
      addx(1)=off1(max(pq,rs))+addaa(min(pq,rs))
      bukx(1)=bukpt(1)
      valx(1)=vint
      go to 24001
c*****************************************************************
7000  continue
c
c  vddd     place integral in u(*)
c
      a=l1
      asym=symm(a)
      i=l2
      j=l3
      k=l4
      if(j.ne.k)go to 7020
      if(i.ne.j)go to 7010
c  (ai|ii)
      a=a-nsm(asym)
      i=i-nsm(asym)
      ai=nnsm(asym)+nndx(max(i,a))+min(i,a)
      u(ai)=u(ai)+vint
      go to 7050
7010  continue
c  (ai|jj)
      a=a-nsm(asym)
      i=i-nsm(asym)
      ai=nnsm(asym)+nndx(max(i,a))+min(i,a)
      u(ai)=u(ai)+two*vint
      go to 7050
7020  if(i.ne.j)go to 7030
c  (ai|ik)
      a=a-nsm(asym)
      k=k-nsm(asym)
      ak=nnsm(asym)+nndx(max(k,a))+min(k,a)
      u(ak)=u(ak)-vint
      go to 7050
7030  if(i.ne.k)go to 7050
c  (ai|ji)
      a=a-nsm(asym)
      j=j-nsm(asym)
      aj=nnsm(asym)+nndx(max(j,a))+min(j,a)
      u(aj)=u(aj)-vint
7050  continue
c  (ai|jk)
      go to 30000
c*****************************************************************
8000  continue
c
c  vdad  place in u(*) and sort integral
c
      asym=symm(l1)
      if(l2.eq.l4)then
c   (ai|pi)
         a=l1-nsm(asym)
         p=l3-nsm(asym)
         ap=nnsm(asym)+nndx(max(a,p))+min(a,p)
         u(ap)=u(ap)-vint
      endif
c
c  p(ai,pj) = 4*(ai|pj) - (aj|pi) - (ap|ij)
c  p(aj,pi) = 4*(aj|pi) - (ai|pj) - (ap|ij)
c  p(ai,pi) = 3*(ai|pi) - (ap|ii)
c
      if(bukpt(10).eq.0)go to 30000
      a=orbidx(l1)
      i=orbidx(l2)
      isym=symm(l2)
      p=orbidx(l3)
      psym=symm(l3)
      j=orbidx(l4)
      if(i.eq.j)then
c  p(ai,pi) term.
         if(asym.ne.isym)go to 24000
         ai=addvd(a,i)
         addx(1)=intoff_mc(10)+off10(ai)+addad(i,p)
         bukx(1)=bukpt(10)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         go to 24001
      endif
c  p(ai,pj) term.
      nx=0
      if(asym.eq.isym)then
         nx=1
         ai=addvd(a,i)
         addx(1)=intoff_mc(10)+off10(ai)+addad(j,p)
         bukx(1)=bukpt(10)+(addx(1)-1)/avc2is
         valx(1)=four*vint
      endif
c  p(aj,pi) term.
      if(psym.eq.isym)then
         aj=addvd(a,j)
         nx=nx+1
         addx(nx)=intoff_mc(10)+off10(aj)+addad(i,p)
         bukx(nx)=bukpt(10)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
9000  continue
c
c  vdaa    sort integral
c
      if(bukpt(3).eq.0)go to 30000
      a=orbidx(l1)
      i=orbidx(l2)
      p=orbidx(l3)
      q=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      addx(1)=intoff_mc(3)+off3(pq)+addvd(a,i)
      bukx(1)=bukpt(3)
      valx(1)=vint
      go to 24001
c*****************************************************************
10000 continue
c
c  vdvd    place in u(*) and sort integral.
c    p(ai,bj) = 4*(ai|bj) - (ij|ab) - (bi|aj)
c    p(bi,aj) = 4*(bi|aj) - (ij|ab) - (ai|bj)
c    p(ai,bi) = 3*(ai|bi) - (ii|ab)
c    p(bi,ai) = 3*(bi|ai) - (ii|ab)
c    p(ai,aj) = 3*(ai|aj) - (ij|aa)
c    p(ai,ai) = 3*(ai|ai) - (ii|aa)
c
cmd
cmd   write(99,9999)l1,l2,l3,l4,vint
cmd
      asym=symm(l1)
      if (flags(22)) go to 10010
      if(l2.eq.l4)then
c  (ai|bi)
         a=l1-nsm(asym)
         b=l3-nsm(asym)
         ab=nnsm(asym)+nndx(max(a,b))+min(a,b)
         u(ab)=u(ab)-vint
      endif
10010 if(bukpt(11).eq.0)go to 30000
      a=orbidx(l1)
      i=orbidx(l2)
      isym=symm(l2)
      b=orbidx(l3)
      bsym=symm(l3)
      j=orbidx(l4)
      if(i.lt.j)then
         a=orbidx(l3)
         asym=symm(l3)
         i=orbidx(l4)
         isym=symm(l4)
         b=orbidx(l1)
         bsym=symm(l1)
         j=orbidx(l2)
      endif
      ij=nndx(i)+j
      it=1
      if(a.eq.b)it=it+1
      if(i.eq.j)it=it+2
      go to (10100,10200,10300,10400),it
10100  continue
      nx=0
      if(asym.eq.isym)then
c  p(ai,bj) term.
         nx=1
         addx(1)=intoff_mc(11)+off11(ij)+addvv3(b,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=four*vint
      endif
      if(bsym.eq.isym)then
c  p(bi,aj) term.
         nx=nx+1
         addx(nx)=intoff_mc(11)+off11(ij)+addvv3(a,b)
         bukx(nx)=bukpt(11)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
10200 continue
c  p(ai,aj) term.
      if(asym.eq.isym)then
         addx(1)=intoff_mc(11)+off11(ij)+addvv3(a,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         go to 24001
      endif
      go to 30000
10300 continue
      if(asym.eq.isym)then
c  p(ai,bi) term.
         addx(1)=intoff_mc(11)+off11(ij)+addvv3(b,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=three*vint
c  p(bi,ai) term.
         addx(2)=intoff_mc(11)+off11(ij)+addvv3(a,b)
         bukx(2)=bukpt(11)+(addx(2)-1)/avc2is
         valx(2)=three*vint
         go to 24002
      endif
      go to 30000
10400 continue
      if(asym.eq.isym)then
c  p(ai,ai) term.
         addx(1)=intoff_mc(11)+off11(ij)+addvv3(a,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         go to 24001
      endif
      go to 30000
c*****************************************************************
11000 continue
c
c  vadd    place in u(*) and sort integral
c
c  p(ai,pj) = 4*(ai|pj) - (aj|pi) - (ap|ij)
c  p(aj,pi) = 4*(aj|pi) - (ai|pj) - (ap|ij)
c  p(ai,pi) = 3*(ai|pi) - (ap|ii)
c
      asym=symm(l1)
      if(l3.eq.l4)then
c  (ap|ii)
         a=l1-nsm(asym)
         p=l2-nsm(asym)
         ap=nnsm(asym)+nndx(max(a,p))+min(a,p)
         u(ap)=u(ap)+two*vint
      endif
      if(bukpt(10).eq.0)go to 30000
      a=orbidx(l1)
      p=orbidx(l2)
      psym=symm(l2)
      i=orbidx(l3)
      isym=symm(l3)
      j=orbidx(l4)
      nx=0
c  p(ai,pj) and p(ai,pi) terms.
      if(asym.eq.isym)then
         ai=addvd(a,i)
         nx=1
         addx(1)=intoff_mc(10)+off10(ai)+addad(j,p)
         bukx(1)=bukpt(10)+(addx(1)-1)/avc2is
         valx(1)=-vint
         if(i.eq.j)go to 24001
      endif
c  p(aj,pi) term.
      if(psym.eq.isym)then
         aj=addvd(a,j)
         nx=nx+1
         addx(nx)=intoff_mc(10)+off10(aj)+addad(i,p)
         bukx(nx)=bukpt(10)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
12000 continue
c
c  vaad    sort integral
c
      if(bukpt(4).eq.0)go to 30000
      a=orbidx(l1)
      q=orbidx(l2)
      p=orbidx(l3)
      i=orbidx(l4)
      addx(1)=intoff_mc(4)+off4(q,p)+addvd(a,i)
      bukx(1)=bukpt(4)
      valx(1)=vint
      go to 24001
c*****************************************************************
13000 continue
c
c  vaaa    sort integral
c
      if(bukpt(5).eq.0)go to 30000
      a=orbidx(l1)
      r=orbidx(l2)
      p=orbidx(l3)
      q=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      addx(1)=intoff_mc(5)+off5(pq)+addva(a,r)
      bukx(1)=bukpt(5)
      valx(1)=vint
      go to 24001
c*****************************************************************
14000 continue
c
c  vavd    sort integral
c
c  p(ap,bi) = 4*(ap|bi)-(ab|pi)-(bp|ai)
c  p(bp,ai) = 4*(bp|ai)-(ab|pi)-(ap|bi)
c  p(ap,ai) = 3*(ap|ai)-(aa|pi)
c
      if(bukpt(9).eq.0)go to 30000
      a=orbidx(l1)
      asym=symm(l1)
      p=orbidx(l2)
      psym=symm(l2)
      b=orbidx(l3)
      bsym=symm(l3)
      i=orbidx(l4)
c  p(ap,ai) term.
      if(a.eq.b .and. asym.eq.psym)then
         ai=addvd(a,i)
         addx(1)=intoff_mc(9)+off9(ai)+addva(a,p)
         bukx(1)=bukpt(9)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         go to 24001
      endif
c  p(ap,bi) term.
      nx=0
      if(asym.eq.psym)then
         nx=1
         bi=addvd(b,i)
         addx(1)=intoff_mc(9)+off9(bi)+addva(a,p)
         bukx(1)=bukpt(9)+(addx(1)-1)/avc2is
         valx(1)=four*vint
      endif
c  p(bp,ai) term.
      if(bsym.eq.psym)then
         nx=nx+1
         ai=addvd(a,i)
         addx(nx)=intoff_mc(9)+off9(ai)+addva(b,p)
         bukx(nx)=bukpt(9)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
15000 continue
c
c  vava    sort integral
c
      if(bukpt(8).eq.0)go to 30000
      a=orbidx(l1)
      p=orbidx(l2)
      b=orbidx(l3)
      q=orbidx(l4)
      if(p.lt.q)then
         a=orbidx(l3)
         p=orbidx(l4)
         b=orbidx(l1)
         q=orbidx(l2)
      endif
      pq=nndx(p)+q
      addx(1)=intoff_mc(8)+off8(pq)+addvv2(b,a)
      bukx(1)=bukpt(8)+(addx(1)-1)/avc2is
      valx(1)=vint
      if(p.ne.q .or. a.eq.b)go to 24001
c  (ap|bp) = (bp|ap)
      addx(2)=intoff_mc(8)+off8(pq)+addvv2(a,b)
      bukx(2)=bukpt(8)+(addx(2)-1)/avc2is
      valx(2)=vint
      go to 24002
c*****************************************************************
16000 continue
c
c  vvdd    place in u(*) and sort integral
c    p(ai,bj) = 4*(ai|bj) - (ij|ab) - (bi|aj)
c    p(bi,aj) = 4*(bi|aj) - (ij|ab) - (ai|bj)
c    p(ai,bi) = 3*(ai|bi) - (ii|ab)
c    p(bi,ai) = 3*(bi|ai) - (ii|ab)
c    p(ai,aj) = 3*(ai|aj) - (ij|aa)
c    p(ai,ai) = 3*(ai|ai) - (ii|aa)
c
cmd
cmd   write(99,9999)l1,l2,l3,l4,vint
cmd
      asym=symm(l1)
      if (flags(22)) go to 16100
      if(l3.eq.l4)then
c  (ab|ii)
         a=l1-nsm(asym)
         b=l2-nsm(asym)
         ab=nnsm(asym)+nndx(max(a,b))+min(a,b)
         u(ab)=u(ab)+two*vint
      endif
16100 if(bukpt(11).eq.0)go to 30000
      a=orbidx(l1)
      b=orbidx(l2)
      bsym=symm(l2)
      i=orbidx(l3)
      isym=symm(l3)
      j=orbidx(l4)
      ij=nndx(max(i,j))+min(i,j)
      nx=0
      if(a.ne.b .and. asym.eq.isym)then
c  p(ai,bj) and p(ai,bi) terms.
         nx=1
         addx(1)=intoff_mc(11)+off11(ij)+addvv3(b,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=-vint
      endif
c  p(bi,aj), p(bi,ai), p(ai,aj), and p(ai,ai) terms.
16001 if(bsym.eq.isym)then
         nx=nx+1
         addx(nx)=intoff_mc(11)+off11(ij)+addvv3(a,b)
         bukx(nx)=bukpt(11)+(addx(nx)-1)/avc2is
         valx(nx)=-vint


      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
17000 continue
c
c  vvad    sort integral
c  p(ap,bi) = 4*(ap|bi)-(ab|pi)-(bp|ai)
c  p(bp,ai) = 4*(bp|ai)-(ab|pi)-(ap|bi)
c  p(ap,ai) = 3*(ap|ai)-(aa|pi)
c
      if(bukpt(9).eq.0)go to 30000
      a=orbidx(l1)
      asym=symm(l1)
      b=orbidx(l2)
      bsym=symm(l2)
      p=orbidx(l3)
      psym=symm(l3)
      i=orbidx(l4)
      nx=0
      if(asym.eq.psym)then
         nx=1
         bi=addvd(b,i)
         addx(1)=intoff_mc(9)+off9(bi)+addva(a,p)
         bukx(1)=bukpt(9)+(addx(1)-1)/avc2is
         valx(1)=-vint
         if(a.eq.b)go to 24001
      endif
      if(bsym.eq.psym)then
         nx=nx+1
         ai=addvd(a,i)
         addx(nx)=intoff_mc(9)+off9(ai)+addva(b,p)
         bukx(nx)=bukpt(9)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
18000 continue
c
c  vvaa    sort integral
c
      if(bukpt(7).eq.0)go to 30000
      a=orbidx(l1)
      b=orbidx(l2)
      p=orbidx(l3)
      q=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      ab=nndx(max(a,b))+min(a,b)
      addx(1)=intoff_mc(7)+off7(pq)+addvv(ab)
      bukx(1)=bukpt(7)+(addx(1)-1)/avc2is
      valx(1)=vint
      go to 24001
c*****************************************************************
19000 continue
c
c  vvvd    integral is not needed
c
      go to 30000
c*****************************************************************
20000 continue
c
c  vvva    integral is not needed
c
      go to 30000
c*****************************************************************
21000 continue
c
c  vvvv    integral is not needed
      go to 30000
c*****************************************************************
22000 continue
c
c  ffff,fffx, etc.    integral is not needed
c
      iknti(22)=iknti(22)+1
      go to 30000
c*****************************************************************
c
c  place integrals in appropriate buckets.
c
24002 continue
      iknto(type)=iknto(type)+1
      b=bukx(2)
      if(bknt(b).eq.npbuk)
     + call wtda(buk(1,b),ibuk(1,b),npbuk,bufb,lenbuk,bknt(b),lastb(b))
      bknt(b)=bknt(b)+1
      buk(bknt(b),b)=valx(2)
      ibuk(bknt(b),b)=addx(2)
c
24001 continue
      iknto(type)=iknto(type)+1
      b=bukx(1)
      if(bknt(b).eq.npbuk)
     + call wtda(buk(1,b),ibuk(1,b),npbuk,bufb,lenbuk,bknt(b),lastb(b))
      bknt(b)=bknt(b)+1
      buk(bknt(b),b)=valx(1)
      ibuk(bknt(b),b)=addx(1)
c
24000 continue
c
cmd
c30000 write(99,9999)l1,l2,l3,l4,vint
 9999 format(4i3,3x,d20.13)
cmd
30000 continue
      if(last.eq.more)go to 100
c
c  dump any partially full buckets.
c
      do 30010 b=1,nbuk
      if(bknt(b).gt.0)
     + call wtda(buk(1,b),ibuk(1,b),npbuk,bufb,lenbuk,bknt(b),lastb(b))
30010 continue
c
      ikntit=0
      ikntot=0
      do 30020 i=1,22
         ikntot=ikntot+iknto(i)
         ikntit=ikntit+iknti(i)
30020 continue
      if (print.gt.1) then
      write(nlist,60010)(i,iknti(i),iknto(i),i=1,22)
      write(nlist,60020)ikntit,ikntot
      endif
60010 format(' mosrt1: integrals of each type (type:in:out)'/
     +  (2(1x,'(',i3,':',i8,':',i8,')')))
60020 format(' mosrt1: total in=',i8,4x,'total out=',i8)
c
cmd
cmd   close(99)
cmd
      return
      end
      subroutine mosrt2(lastb,core,avc2is,buk,lenbkx,
     +  ibuk,npbukx)
c
c  perform the second-half bin sort of 2-e integral types 6-11.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer nfilmx
      parameter (nfilmx=31)
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer stape
      equivalence (iunits(21),stape)
c
      common/cis2/add0,ibpt,ic0,icmax,ic0mx,avc2,nterm,ibk,lenbuk,npbuk
c
      common/cbufs/lenbfs,h2bpt
c
      real*8 core(*),buk(*)
      integer lastb(*),ibuk(*)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      integer tsymdd(8),tsmdd2(8),tsymad(8),tsymvd(8)
      integer tsymva(8),tsymvv(8),tsmvv2(8)
      equivalence (nxy(1,21),tsymdd(1))
      equivalence (nxy(1,22),tsmdd2(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,28),tsmvv2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a)
c
c  prepare for sort: initialize /cis2/
c  add0  = address offset for placing integrals in core.
c  ibpt  = beginning of current buffer space.
c  ic0   = running offset within core(*).
c  icmax = address of the last integral in core(*).
c  ic0mx = minimum of icmax and the address of last buffer
c          element in core(*), ic0mx=min(icmax,ibpt+lenbfs-1).
c
      add0=0
      ibpt=1
      ic0=0
      icmax=0
      ic0mx=0
      avc2=avc2is
      lenbuk=lenbkx
      npbuk=npbukx
c
      ibk=0
      do 10 i=1,5
10    ibk=max(bukpt(i),ibk)
c  ...on loop exit, ibk is one less than the first required
c  bucket chain number for the second half sort.
c
c  type-6 integrals: off6(pq)+adddd(ij), off6(pq)+adddd2(j,i)
c
      if(bukpt(6).eq.0)go to 700
      do 620 p=1,nact
      psym=symx(ix0a+p)
      do 610 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsymdd(pqsym)
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
      ic0=ic0+nterm
c
      nterm=tsmdd2(pqsym)
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
610   ic0=ic0+nterm
620   continue
c
700   continue
c
c  type-7 integrals: off7(pq)+addvv(ab)
c
      if(bukpt(7).eq.0)go to 800
      do 720 p=1,nact
      psym=symx(ix0a+p)
      do 710 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsymvv(pqsym)
      if(nterm.eq.0)go to 710
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
710   ic0=ic0+nterm
720   continue
c
800   continue
c
c  type-8 integrals: off8(pq)+addvv2(b,a)
c
      if(bukpt(8).eq.0)go to 900
      do 820 p=1,nact
      psym=symx(ix0a+p)
      do 810 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsmvv2(pqsym)
      if(nterm.eq.0)go to 810
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
810   ic0=ic0+nterm
820   continue
c
900   continue
c
c  type-9 integrals: off9(ai)+addva(b,p)
c
      if(bukpt(9).eq.0)go to 1000
      nai=tsymvd(1)
      do 910 ai=1,nai
      nterm=tsymva(1)
      if(nterm.eq.0)go to 910
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
910   ic0=ic0+nterm
c
1000  continue
c
c  type-10 integrals: off10(ai)+addad(j,p)
c
      if(bukpt(11).eq.0)go to 1100
      nai=tsymvd(1)
      do 1010 ai=1,nai
      nterm=tsymad(1)
      if(nterm.eq.0)go to 1010
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
1010  ic0=ic0+nterm
c
1100  continue
c
c  type-11 integrals: off11(ij)+addvv3(b,a)
c
      if(bukpt(11).eq.0)go to 1200
      do 1120 i=1,ndot
      na=nvpsy(symx(ix0d+i))
      do 1110 j=1,i
      nb=nvpsy(symx(ix0d+j))
      nterm=na*nb
      if(nterm.eq.0)go to 1110
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
1110  ic0=ic0+nterm
1120  continue
c
1200  continue
c
c  all done.  dump the last buffer of sorted integrals.
c
      if(ic0+1.ne.ibpt)call seqw(stape,lenbfs,core(ibpt))
c
      return
      end
      integer function numpbf(l)
c
c  determine the number of 2-e integrals that can be held in a working
c  precision buffer of length l, where the buffer is assumed to be
c  formatted as:
c
c  buffer format: value-buffer...packed-label-buffer...information-word
c
c  examples:
c   1-label/word:  n=(l-1)/2
c   2-labels/word: n=(2*(l-1))/3
c   4-labels/word: n=(4*(l-1))/5
c   ...
c   m-labels/word: n=(m*(l-1))/(m+1)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
*@ifdef future
*!C  anyone?
*@else
      numpbf=(2*(l-1))/3
*@endif
c
      return
      end
c  ***  the following set of da i/o routines are redundant
c       and should be eliminated ***   -rls
      subroutine da2ow(len)
c
c  open the direct access file number 2 for
c  writing.  len is in working precision units.
c
      common/da2/nunit2,len2,irec2
c
      character*60 fname
c
      len2=len
      irec2=1
c
      fname = 'mcsda2'
      call trnfln( 1, fname )
c     # colib call added 28-sep-91 -rls
      call openda( nunit2, fname, len, 'scratch', 'random' )
      return
      end
      subroutine da2cw
c
c  close file da2 for writing.
c
      common/da2/nunit2,len2,irec2
      return
      end
      subroutine da2or
c
c  open da2 for reading.
c
      common/da2/nunit2,len2,irec2
      return
      end
      subroutine wtda(buk,ibuk,npbuk,bufb,lenbuk,num,last)
c
c  write a single da record and pack addresses and
c  chaining information.
c
c  input:
c  buk(*)  = integrals.
c  ibuk(*) = unpacked labels.
c  bufb(*) = da record buffer.
c  npbuk   = number of integrals in each da record.
c  lenbuk  = length of da records.
c  num     = number of integrals in this record.
c  last    = record number of the last da record of this chain.
c
c  output:
c  last    = record number of this da record.
c  num     = set to zero.
c
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 buk(npbuk),bufb(lenbuk)
      integer ibuk(npbuk)
c
      integer iword(2)
c
      call dcopy_wr(num,buk,1,bufb,1)
c
c  ***note*** for odd num, an extra integral label is packed
c  into the buffer.
c
      call plab32(bufb(npbuk+1),ibuk,num)
c
      iword(1)=last
      iword(2)=num
      call plab32(bufb(lenbuk),iword,2)
c
      call da2w(bufb,last)
      num=0
c
      return
      end
      subroutine seqw(iunit,n,a)
c
c  sequential write of a(*) to unit iunit.
c
      integer iunit,n
      real*8 a(n)
      write(iunit)a
      return
      end
      subroutine ecore(x,ecore0)
c
c  calculate core reference energy.
c
c  equation used is:
c
c      ecore0 = sum(i) ( h(ii) + u(ii) )
c
c  where h(ii) is the one-electron hamiltonian matrix element,
c  including any frozen core contributions, corresponding to
c  inactive orbital number i, and where u(ii) is the modified
c  hamiltonian matrix element for inactive orbital number i.
c  note that this routine is called twice each mcscf iteration.
c
c  first call:
c    input:
c      x(*)    = h(*).
c      ecore0  = should be initialized to zero.
c    output:
c      ecore0  = the first part of the summation.
c
c  second call:
c    input:
c      x(*)    = u(*).
c      ecore0  = result from first call.
c    output:
c      ecore0  = result from both summations.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 x(*),ecore0
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsm(8),nnsm(8)
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxtot(7),ndot)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symx(nmotx),invx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (ix0(1),ix0d)
c
      if(ndot.eq.0)return
c
c  loop over non-frozen doubly-occupied (inactive) orbitals.
c
      do 100 i=1,ndot
100   ecore0=ecore0+x(nnsm(symx(ix0d+i))+nndx(invx(ix0d+i)+1))
c
      return
      end
      subroutine da2w(buffer,last)
c
c  write buffer onto da2 in the next available record.
c
      real*8 buffer(len2)
      common/da2/nunit2,len2,irec2
c     # colib call added 28-sep-91 -rls
      call writda( nunit2, irec2, buffer, len2 )
c----      write(nunit2,rec=irec2)buffer
      last=irec2
      irec2=irec2+1
      return
      end
      subroutine sdas(core,buf,lastb,buk,ibuk)
c
c  bookeeping for second-half integral sort.
c  if buf(*) is full, it is dumped.
c  if core(*) is empty, it is filled.
c
c  input:
c  buf(*)  = current buffer space for sorted integrals.
c            buf(1) is equivalenced to core(ibpt) by call.
c  core(*) = workspace of total length equal to avc2+lenbfs.
c  nterm   = number of terms for which the attempt to increment
c            buffer pointers failed.
c  add0    = integral addressing offset.
c  ibpt    = first location in core(*) for current buffer.
c  ic0     = running core(*) offset.
c  icmax   = address in core(*) of last integral.
c  ic0mx   = maximum ic0 value before this routine is called again.
c  avc2    = number of integrals held in each da chain.
c
c  output:
c  add0,ibpt,ic0,icmax,and ic0mx are appropriately updated.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer nfilmx
      parameter (nfilmx=31)
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer nlist,stape
      equivalence (iunits(1),nlist)
      equivalence (iunits(21),stape)
c
      common/cis2/add0,ibpt,ic0,icmax,ic0mx,avc2,nterm,ibk,lenbuk,npbuk
c
      common/cbufs/lenbfs,h2bpt
c
      real*8 core(*),buf(lenbfs),buk(lenbuk)
      integer ibuk(npbuk),lastb(*)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  check buffer space:
c
      if(ic0+nterm .le. ibpt+lenbfs-1)go to 100
c
c  not enough space is left in the buffer.  dump buffer and
c  adjust pointers.
c
      write(stape)buf
      ibpt=ic0+1
c
c  check to make sure output buffer length is sufficient.
c
      if(nterm.gt.lenbfs)then
         write(nlist,6010)nterm,lenbfs
         call bummer('sdas: insufficient buffer length, nterm=',
     &    nterm,faterr)
      endif
c
c  space for a full buffer must remain in core(*).
c
      if(ic0.le.avc2)go to 100
c
c  copy remaining integrals to the beginning of core(*) and
c  adjust pointers.
c
      ncopy=icmax-ic0
      call dcopy_wr(ncopy,core(ibpt),1,core(1),1)
      ic0=0
      ibpt=1
      icmax=ncopy
c
100   continue
c
c  check the number of integrals in core:
c
      if(ic0+nterm.le.icmax)go to 3000
c
c  core(*) has run out of integrals.  copy all the integrals in the
c  current buffer space to the beginning of core(*) and read in the
c  next set of chained da records.
c
      if(ibpt.eq.1)go to 1000
      ncopy=icmax-ibpt+1
      call dcopy_wr(ncopy,core(ibpt),1,core(1),1)
      ic0=ic0-ibpt+1
      ibpt=1
      icmax=ncopy
c
1000  ibk=ibk+1
      next=lastb(ibk)
      if(next.ne.0)call da2f(next)
      call wzero(avc2,core(icmax+1),1)
      a0=add0-icmax
c
      call rdbks0(next,core,buk,ibuk,lenbuk,npbuk,a0)
c
      add0=add0+avc2
      icmax=icmax+avc2
c
3000  continue
      ic0mx=min(ibpt+lenbfs-1,icmax)
c
      return
6010  format(/' sdas: error. nterm,lenbfs=',2i10)
      end
      subroutine rdbks0(bukpt,h2,buk,ibuk,lenbuk,npbuk,i0)
c
c  read a chain of da records of sorted integrals.
c  offset of i0 is used to address integrals in h2(*).
c
c  input:
c  bukpt  = record number for the first bucket in the chain.
c  h2(*)  = array for integrals (initialized prior to entry).
c  buk(*) = da record buffers.
c  ibuk(*)= integral label buffers.
c  lenbuk = length of da records.
c  npbuk  = number of integrals held in each record.
c  i0     = address offset for array h2(*).
c
c  output:
c  h2(*)  = updated array of integrals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 h2(*),buk(lenbuk)
      integer ibuk(npbuk)
c
      integer iword(2)
c
      next=bukpt
      if(next.ne.0)call da2f(next)
100   continue
      if(next.eq.0)return
      call da2r(buk,next)
c
      call ulab32(buk(lenbuk),iword,2)
      next=iword(1)
      num=iword(2)
c
      if(next.ne.0)call da2f(next)
c
c  ***note*** for odd num, an extra integer label is unpacked
c  and not used.  space should be allocated in the calling
c  routine to allow the maximum npbuk labels to be unpacked.
c  this is usually done automatically by allocating integer
c  arrays from a working precision workspace.
c
      call ulab32(buk(npbuk+1),ibuk,num)
c
      do 300 i=1,num
300   h2(ibuk(i)-i0)=h2(ibuk(i)-i0)+buk(i)
c
      go to 100
c
      end
      subroutine da2r(buffer,irec)
c
c  read record number irec from da2.
c
      real*8 buffer(len2)
      common/da2/nunit2,len2,irec2
c     # colib call added 28-sep-91 -rls
      call readda( nunit2, irec, buffer, len2 )
c----      read(nunit2,rec=irec)buffer
      return
      end
      subroutine da2f(irec)
c
c  find record number ired on da2.
c
      common/da2/nunit2,len2,irec2
*@ifdef vax
*      find(nunit2,rec=irec)
*@endif
      return
      end
c
c**********************************************************************
c
      subroutine rdbks_md(bukpt,h2,buk,ibuk,lenbuk,npbuk)
c
c  read a chain of da records of sorted integrals.
c
c  input:
c  bukpt  = record number for the first bucket in the chain.
c  h2(*)  = array for integrals (initialized prior to entry).
c  buk(*) = da record buffer.
c  ibuk(*)= integral label buffer.
c  lenbuk = length of da records.
c  npbuk  = number of integrals held in each record.
c
c  output:
c  h2(*)  = array of integrals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 h2(*),buk(lenbuk)
      integer ibuk(npbuk)
c
      integer iword(2)
c
      next=bukpt
      if(next.ne.0)call da2f(next)
100   continue
      if(next.eq.0)return
      call da2r(buk,next)
c
      call ulab32(buk(lenbuk),iword,2)
      next=iword(1)
      num=iword(2)
c
      if(next.ne.0)call da2f(next)
c
c  ***note*** for odd num, an extra integer label is unpacked
c  and not used.  space should be allocated in the calling
c  routine to allow the maximum npbuk labels to be unpacked.
c  this is usually done automatically by allocating integer
c  arrays from a working precision workspace.
c
      call ulab32(buk(npbuk+1),ibuk,num)
c
      do 300 i=1,num
300   h2(ibuk(i))=buk(i)
c
      go to 100
c
      end

      subroutine blkasn(avchc,avc2is)
c
c  assign hessian block construction steps to ft reads.
c  assign integral blocks to buckets for sorting.
c  (pq|rs) integrals, if any, are always placed in bucket number 1.
c  other integral blocks follow.
c
c  input:
c  avchc  = amount of core available during hessian construction steps.
c  avc2is = amount of core available during integral sorting step.
c  qcoupl = .true. if c* blocks are to be constructed.
c  lenbfs = length of the sequential integral buffers.
c
c  output:
c  bukpt(*) = bucket pointers for sorting of the required 2e integrals.
c  intoff_mc(*)= integral address offsets used during sorting.
c  szh(*)   = size of the 14 hessian blocks.
c  hbci(*,1)= hessian block ordering during construction.
c  hbci(*,2)= segment divisions of hessian blocks.
c  hbci(*,3)= blocks requiring transition density matrices (c-blocks).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer nfilmx
      parameter (nfilmx=30)
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer nlist,stape
      equivalence (iunits(1),nlist)
      equivalence (iunits(21),stape)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(9),n2td)
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(15),n2tv)
      integer tsymad(8),tsymvd(8),tsymva(8)
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxy(1,26),tsymva(1),nvat)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
      equivalence (bukpt(12),nbukt)
c
      common/chbci/hbci(4,3)
c
      common/cbufs/lenbfs,h2bpt
c
c
      integer nmotx
         parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer numopt
      parameter(numopt=10)
      integer cigopt,print
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
c
      logical qcoupl
c
      logical qcx,qix,qc(4),qi(4)
      integer leftb(10),sph(14)
      integer si(4),sb1(4),sb2(4),sc(4),ioff(4)
      integer order(4),ift(4),ibuk(4),iseg(4)
      integer iftmin(4),ibkmin(4),segmin(4)
      equivalence (hbci(1,1),order(1)),(hbci(1,3),iftmin(1))
c
      integer  forbyt
      external forbyt
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer perm(4,24)
      data perm/1,2,3,4, 2,1,3,4, 1,3,2,4, 3,1,2,4, 2,3,1,4, 3,2,1,4,
     +          1,2,4,3, 2,1,4,3, 1,4,2,3, 4,1,2,3, 2,4,1,3, 4,2,1,3,
     +          1,3,4,2, 3,1,4,2, 1,4,3,2, 4,1,3,2, 3,4,1,2, 4,3,1,2,
     +          2,3,4,1, 3,2,4,1, 2,4,3,1, 4,2,3,1, 3,4,2,1, 4,3,2,1/
c
c    temporary zero out qcoupl and szh at this place
c    the size information for Hessian is read in
      qcoupl=.true.
c
c  initialize hbci(*) and sorting arrays.
c
c
      do 1 j=1,3
      do 1 i=1,4
1     hbci(i,j)=0
      do 2 i=1,12
      bukpt(i)=0
2     intoff_mc(i)=0
c
c  assign space requirements for hessian block construction.
c  space = size-of-hessian + scratch-workspace
c  see hbcon and individual block construction routines for details
c  of the workspace allocation.
c
      nnact=nndx(nact+1)
      sph(1)=szh(1)+5*n2td+lenbfs
      sph(2)=max(szh(2),2*n2ta)
      sph(3)=szh(3)+2*forbyt(nnact+1)+forbyt(2*nact**2)+4*n2ta
      sph(4)=szh(4)+n2ta+lenbfs
      sph(5)=szh(5)+nvdt
      sph(6)=szh(6)+lenbfs
      sph(7)=szh(7)+nvdt
      sph(8)=max(szh(8),nvat,n2ta)
      sph(9)=szh(9)+n2ta+lenbfs
      sph(10)=szh(10)+3*n2tv+lenbfs
      if(qcoupl)then
          sph(11)=szh(11)
          sph(12)=szh(12)
          sph(13)=szh(13)
          sph(14)=szh(14)
      else
          sph(11)=0
          sph(12)=0
          sph(13)=0
          sph(14)=0
      endif
c
c-    write(nlist,6050)'integral dimensions:',numint
c-    write(nlist,6050)'hessian dimensions:',szh
c-    write(nlist,6050)'hessian space requirements:',sph
6050  format(1x,a,/(1x,5i8))
c
c  hessian matrix must be constructed in one read of the integrals.
c  up to four ft reads are allowed.  hessian blocks 11,12,13,14 must
c  be constructed while reading the ft.  blocks 5,7,8,2,3 must also be
c  constructed when the integrals are available during the ft reads.
c  the remaining blocks, 1,4,6,9,10, are constructed independently
c  of the ft reads.
c
c  check space for b matrix blocks to be constructed.
c
      leftb(5)=avchc-sph(5)-numint(3)-numint(4)
      leftb(7)=avchc-sph(7)-numint(3)-numint(4)
      leftb(8)=avchc-sph(8)-numint(5)
      leftb(2)=avchc-sph(2)-numint(2)
      leftb(3)=avchc-sph(3)-numint(1)
c
      leftb(1)=avchc-sph(1)
      leftb(4)=avchc-sph(4)
      leftb(6)=avchc-sph(6)
      leftb(9)=avchc-sph(9)
      leftb(10)=avchc-sph(10)
      left=avchc
      do 10 i=1,10
10    left=min(leftb(i),left)
c-    write(nlist,6080)left,leftb
6080  format(' minimum core left during b-block construction=',i8/
     +  1x,10i8)
      if(left.lt.0)go to 2000
c
c  calculate space requirements for the first blocks to
c  be constructed.
c
      nfirst=1
      si(nfirst)=numint(1)
      sb1(nfirst)=sph(3)
      sb2(nfirst)=0
      sc(nfirst)=sph(12)
      qi(nfirst)=.false.
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
      nfirst=2
      si(nfirst)=numint(2)
      sb1(nfirst)=sph(2)
      sb2(nfirst)=0
      sc(nfirst)=sph(11)
      qi(nfirst)=si(nfirst).ne.0
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
      nfirst=3
      si(nfirst)=numint(3)+numint(4)
      sb1(nfirst)=sph(5)
      sb2(nfirst)=sph(7)
      sc(nfirst)=sph(13)
      qi(nfirst)=si(nfirst).ne.0
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
      nfirst=4
      si(nfirst)=numint(5)
      sb1(nfirst)=sph(8)
      sb2(nfirst)=0
      sc(nfirst)=sph(14)
      qi(nfirst)=si(nfirst).ne.0
      qc(nfirst)=sc(nfirst).ne.0
      if (cdir.eq.1) qc(nfirst) = .false.
c
c  determine the number of ft reads and integral buckets
c  for all possible orders of the blocks of the c matrix.
c
      leftm1=0
      leftmx=0
      nftmin=99
      nbkmin=99
      pmin=1
c
      do 600 iperm=1,24
c
c  for a given order, determine the number of segments required.
c
      sit=0
      sct=0
      nseg=1
      left1=avchc
      left=avchc
c
      do 400 n=1,4
      iblk=perm(n,iperm)
c
c  try to put current blocks into current segment.
c
100   continue
      sctx=sct+sc(iblk)
      sitx=sit+si(iblk)
      s1=sctx+sitx
      s2=sitx+sb1(iblk)
      s3=sitx+sb2(iblk)
      if(max(s1,s2,s3).le.avchc)go to 200
c
c  extension fails.
c
      if(sit.eq.0 .and. sct.eq.0)then
           left=avchc-max(s1,s2,s3)
           go to 2000
      endif
c
      nseg=nseg+1
      sit=0
      sct=0
      go to 100
c
200   continue
c
c  extension fits.
c
      sit=sitx
      sct=sctx
      left=min(left,avchc-max(s1,s2,s3))
      if(iblk.eq.1)left1=avchc-s2
      iseg(n)=nseg
400   continue
c
c  for the resulting segment structure, determine the number
c  of ft reads and integral buckets.
c
      nbuk=0
      if(numint(1).ne.0)nbuk=1
      nft=0
      do 440 is=1,nseg
      qcx=.false.
      qix=.false.
      do 420 i=1,4
      if(iseg(i).ne.is)go to 420
      iblk=perm(i,iperm)
      qcx=qcx.or.qc(iblk)
      qix=qix.or.qi(iblk)
420   continue
      if(qcx)nft=nft+1
      if(qix)nbuk=nbuk+1
      do 430 i=1,4
      if(iseg(i).ne.is)go to 430
      iblk=perm(i,iperm)
      ift(i)=0
      if(qc(iblk))ift(i)=nft
      ibuk(i)=0
      if(qi(iblk))ibuk(i)=nbuk
      if(iblk.eq.1 .and. numint(1).ne.0)ibuk(i)=1
430   continue
440   continue
c
c      write(nlist,6010)iperm,nft,nbuk,left,ift,ibuk,left1
6010  format(1x,i2,' nft=',i1,' nbuk=',i1,' left=',i7,
     +  ' ift(*)=',4i2,' ibuk(*)=',4i2,' left1=',i7)
c
c  determine if present permutation is the best.
c
      if(nft-nftmin)490,460,600
460   if(nbuk-nbkmin)490,470,600
470   if(leftm1-left1)490,480,600
480   if(leftmx-left)490,600,600
490   continue
c
c  save info: new order produces the fewest ft reads, the fewest buckets
c  the largest amount of core left during baaaa construction, and
c  the largest amount of core left during ft reads (in that order).
c
      nftmin=nft
      nbkmin=nbuk
      leftm1=left1
      leftmx=left
      pmin=iperm
      do 500 i=1,4
      order(i)=perm(i,pmin)
      segmin(i)=iseg(i)
      iftmin(i)=ift(i)
      ibkmin(i)=ibuk(i)
500   continue
c
600   continue
c
      if (print.ge.1)
     + write(nlist,6030)nftmin,nbkmin,leftm1,
     +  leftmx,order,iftmin,ibkmin,segmin
6030  format(/' number of ft reads required=',i2/
     +  ' number of buckets required=',i2/
     +  ' amount of core left during baaaa=',i8/
     +  ' amount of core left=',i8/
     +  ' order(*)= ',4i2/
     +  ' iftmin(*)=',4i2/
     +  ' ibkmin(*)=',4i2/
     +  ' segmin(*)=',4i2)
c
c  calculate integral offsets and bucket pointers for the first
c  five blocks of integrals.
c
      do 640 i=1,4
640   ioff(i)=0
      do 750 i=1,4
      buk=ibkmin(i)
      if(buk.eq.0)go to 750
      is=segmin(i)
      iblk=order(i)
      go to (710,720,730,740),iblk
710   continue
c
c  block 1. integral type 1, (pq|rs).
c
      bukpt(1)=buk
      intoff_mc(1)=ioff(is)
      ioff(is)=ioff(is)+numint(1)
      go to 750
720   continue
c
c  block 2. integral type 2, (pq|ri).
c
      bukpt(2)=buk
      intoff_mc(2)=ioff(is)
      ioff(is)=ioff(is)+numint(2)
      go to 750
730   continue
c
c  block 3. integral types 3 and 4, (pq|ia) and (pi|qa).
c
      bukpt(3)=buk
      intoff_mc(3)=ioff(is)
      ioff(is)=ioff(is)+numint(3)
      bukpt(4)=buk
      intoff_mc(4)=ioff(is)
      ioff(is)=ioff(is)+numint(4)
      go to 750
740   continue
c
c  block 4. integral type 5, (pq|ra)
c
      bukpt(5)=buk
      intoff_mc(5)=ioff(is)
      ioff(is)=ioff(is)+numint(5)
750   continue
c
c  remaining integral offsets for integral types 6 thru 11.
c  bukpt(i)=0 is the flag to not sort integral type i.
c
      do 760 i=6,11
760   if(numint(i).ne.0)bukpt(i)=nbuk+1
c
      nit=0
c
c  integral types 6 contribute to fdd(*) and badad.
c
      intoff_mc(6)=nit
      nit=nit+numint(6)
c
c  integral types 7 and 8 contribute to qvv(*) and bvdva.
c
      intoff_mc(7)=nit
      nit=nit+numint(7)
      intoff_mc(8)=nit
      nit=nit+numint(8)
c
c  integral types 9 contribute to bvavd.
c
      intoff_mc(9)=nit
      if(szh(9).ne.0)nit=nit+numint(9)
      if(szh(9).eq.0)bukpt(9)=0
c
c  integral types 10 contribute to bvdad.
c
      intoff_mc(10)=nit
      if(szh(4).ne.0)nit=nit+numint(10)
      if(szh(4).eq.0)bukpt(10)=0
c
c  integral types 11 contribute to bvdvd.
c
      intoff_mc(11)=nit
      if(szh(6).ne.0)nit=nit+numint(11)
      if(szh(6).eq.0)bukpt(11)=0
c
      intoff_mc(12)=nit
      nbukt=nbuk+ ((nit-1)/avc2is) +1
c
c-    write(nlist,6090)ioff,intoff_mc,bukpt,nbukt,nit
6090  format(' ioff(*)=',4i8/' intoff_mc(*)=',6i8/1x,6i8/
     +  ' bukpt(*)=',12i2/' nbukt=',i6,/' nit=',i8)
c
c  save info for hessian block construction steps.
c
      do 800 i=1,4
      is=segmin(i)
      if(is.ne.0)hbci(is,2)=hbci(is,2)+1
800   continue
c
c-    write(nlist,6040)hbci
6040  format(' hbci(*,*)='/(1x,4i3))
c
      sz=0
      do imd=1,10
        sz=sz+szh(imd)
      enddo
      write(nlist,
     & '(/," Size of orbital-Hessian matrix B: ",t45,i15)')sz

      write(nlist,'("  1. B[ad,ad]",i15)')szh(1)
      write(nlist,'("  2. B[aa,ad]",i15)')szh(2)
      write(nlist,'("  3. B[aa,aa]",i15)')szh(3)
      write(nlist,'("  4. B[vd,ad]",i15)')szh(4)
      write(nlist,'("  5. B[vd,aa]",i15)')szh(5)
      write(nlist,'("  6. B[vd,vd]",i15)')szh(6)
      write(nlist,'("  7. B[va,ad]",i15)')szh(7)
      write(nlist,'("  8. B[va,aa]",i15)')szh(8)
      write(nlist,'("  9. B[va,vd]",i15)')szh(9)
      write(nlist,'("  10.B[va,va]",i15,/)')szh(10)

      sz=0
      do imd=11,14
      sz=sz+szh(imd)
      enddo
      if (qcoupl) write(nlist,
     & '(" Size of the orbital-state Hessian matrix C:",t45,i15)')
     & sz
      write(nlist,'("  11.C[csf,ad]",i15)')szh(11)
      write(nlist,'("  12.C[csf,aa]",i15)')szh(12)
      write(nlist,'("  13.C[csf,vd]",i15)')szh(13)
      write(nlist,'("  14.C[csf,va]",i15,/)')szh(14)
c
      write(nlist,
     & '(" Total size of the state Hessian matrix M:",t45,i15)')
     &  szh(15)
c
      sz=0
         do imd=1,15
         sz=sz+szh(imd)
         enddo
         write(nlist,3000)sz
3000  format(
     & ' Size of HESSIAN-matrix for quadratic conv.:',
     & t45,i15,/)
c
      if (sz.gt.avc2is) then
         write(nlist,'("Not enough memory for SOLVEK")')
         left = avc2is - sz
         call bummer('blkasn (test for solvek): left=',left,faterr)
      endif
c
      return
c
2000  write(nlist,6200)avchc,left
6200  format(/' *** blkasn: avchc=',i8,' left=',i8,' ***')
      call bummer('blkasn: left=',left,faterr)
      return
      end
