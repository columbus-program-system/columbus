!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccigrd2.f
ccigrd part=2 of 6.  main driver routine and misc. related routines.
cversion=1.0 last modified: 04-apr-91
c deck driver
      subroutine driver( core, lencor, mem1, ifirst )
      use paralib
c
c  main driver routine for program "cigrd".
c
c  this program computes the effective density matrices and the fock
c  matrix for the analytic energy gradient evaluation for general
c  mrci wave functions.
c
c  written by:
c      ron shepard and hans lischka
c
c  version log:
c  10-aug-01  trnfln() call added for all filenames. -rls
c  27-aug-90  cmdc-related changes. -rls
c     jun-90  averaged fockoperator resolution for the doubly occupied
c             active orbitals and the virtual orbital space added.(m.e.)
c     mar-90  symmetrisation of the effective fockmatrix removed
c             check if this marix is symmetric added (m.e.)
c     feb-90  several bugs fixed and effno construction added (m.e.)
c  11-jan-89  dalen() workspace allocation bug fixed. -rls
c  27-dec-88  move sequential open/close statements to the main program.
c  08-aug-88  nothing particularly interesting on this date but it is
c             worth noting that this code was in development on 8/8/88.
c  10-dec-87  program cifock (construction of mrci fock matrix) modified
c             to include mcscf-related routines. -rls
c
c  cmdc keyword  description
c  ------------  -----------------------
c  cray          cray code.
c  crayctss      ctss specific cray code.
c  crayosu       osu specific cray code.
c  vax           vax code.
c  fps           fps code.
c  ibm           ibm code.
c
c  general comments:
c  * this program uses the nonstandard real*8 declarations for working
c  precision data types.  this works correctly on most machines and
c  avoids the requirement of changing "real" to "double precision" when
c  going from short precision machines to full precision machines.
c  * real parameter constants are specified with double precision
c  exponents.  this gives correct results in all cases, the truncation
c  being done at compile time, if necessary, instead of runtime.
c  * nonstandard namelist input is used in this program.  this is
c  supported on all of the anl machines.  it is relatively simple to
c  replace the namelist input with a list-directed read statement if
c  necessary.  such changes should be local to subroutine readin.
c
       implicit none
c     ##   parameter & common section
c
c     #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c     #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer mcendm,ciendm
      parameter(mcendm=20,ciendm=20)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8    zero,    one,     two,    onem
      parameter(zero=0d0,one=1d0, two=2d0,onem=-1.0d+00)
c
      integer nmomx,nsymx
      parameter(nmomx=1023,nsymx=8)
c
      integer   nfilmx
      parameter(nfilmx=31)
      character*60  fname
      common/cfname/fname(nfilmx)
c
      integer iunits,nlist,nin,cidrt,moints,ciden1,ciden2,mcdrt,mcden1,
     & mcden2,mcrest,mchess,effd1,effd2,
     & effno,mcscr2,hdiagf,mcscfin,moints2,d1atrfl,stape,civoutfl
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
      equivalence (iunits(3),cidrt)
      equivalence (iunits(4),moints)
      equivalence (iunits(5),ciden1)
      equivalence (iunits(6),ciden2)
      equivalence (iunits(7),mcdrt)
      equivalence (iunits(8),mcden1)
      equivalence (iunits(9),mcden2)
      equivalence (iunits(12),mcrest)
      equivalence (iunits(13),mchess)
      equivalence (iunits(14),effd1)
      equivalence (iunits(15),effd2)
      equivalence (iunits(16),effno)
      equivalence (iunits(17),mcscr2)
      equivalence (iunits(18),hdiagf)
      equivalence (iunits(19),mcscfin)
      equivalence (iunits(20),moints2)
      equivalence (iunits(21),stape)
      equivalence (iunits(22),d1atrfl)
      equivalence (iunits(11),civoutfl)
c
      integer nmiter,nvrsmx
      real*8       rtol, dtol
      common/cleqi/rtol, dtol, nmiter, nvrsmx
c
      integer      lenbft
      common/cmfti/lenbft
c
      integer       ldamin, ldamax, ldainc
      common/cldain/ldamin, ldamax, ldainc
c
      integer      nxy,       mult,      nsym, nxtot
      common/csymb/nxy(8,42), mult(8,8), nsym, nxtot(24)
      integer nbpsy(8)
      equivalence (nxy(1,1),nbpsy(1))
      integer nmpsy(8),nsm(8),nnsm(8),n2sm(8),irm1(8),irm2(8),
     & irv1(8),irv2(8),nvpsy(8),tsymad(8),tsymvd(8),tsymva(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,35),irm1(1))
      equivalence (nxy(1,36),irm2(1))
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
      integer nmot,nnmt,n2mt,ndmot,namot,nvmot,nnvt,nbmt
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(5),nnmt)
      equivalence (nxtot(6),n2mt)
      equivalence (nxtot(7),ndmot)
      equivalence (nxtot(10),namot)
      equivalence (nxtot(13),nvmot)
      equivalence (nxtot(14),nnvt)
      equivalence (nxtot(16),nbmt)
      integer nadt,nvdt,nvat
      equivalence (tsymad(1),nadt)
      equivalence (tsymvd(1),nvdt)
      equivalence (tsymva(1),nvat)
c
      integer       mdir, cdir
      common/direct/mdir, cdir
c
      integer numopt
      parameter(numopt=10)
      integer cigopt, print, fresdd, fresaa, fresvv, samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
      real*8  wndtol, wnatol, wnvtol, ftol
      logical qfresd, qfresa, qfresv, qresaf
      common/cresi/ wndtol, wnatol, wnvtol, ftol, qfresd, qfresa,
     & qfresv, qresaf
c
      integer      iorbx,         ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer ndafmx, nbkmx
      parameter(ndafmx=2)
      parameter(nbkmx=200)
      integer ibfpt,ircpt,nvpbk,nvpsg,lendar,nbuk
      common/intsrt/ibfpt(nbkmx),ircpt(nbkmx,ndafmx),
     & nvpbk,nvpsg,lendar,nbuk
c
      integer       daunit
      common/cdafls/daunit(ndafmx)
c
      character*60  dafnam
      common/cdanms/dafnam(ndafmx)
c
      integer   ninfmx
      parameter(ninfmx=10)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer      addpt,    numint,    bukpt,    intoff_mc,nbuk_md,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
      equivalence (bukpt(12),nbuk_md)
c
      integer    nunit2, len2, irec2
      common/da2/nunit2, len2, irec2
c
      integer      lenbfs, h2bpt
      common/cbufs/lenbfs, h2bpt
c
      real*8 d1atr(nmomx,nmomx)
      common /d1tr/ d1atr
c
      integer ios,itype,froot
      integer  ncsf1,ncsf2,refvec1,refvec2
      real*8  eci1,eci2,a4den1,a4den2
      common   /civoutinfo/ ncsf1,ncsf2,refvec1,refvec2,
     .                      eci1,eci2,a4den1,a4den2
c
      integer lenm2e_mc,n2embf_mc,lena1e,n1eabf,lena2e,n2eabf,
     & lenbft_md,lend2e,n2edbf,lend1e,n1edbf
      common/cfinfo/lenm2e_mc,n2embf_mc,lena1e,n1eabf,lena2e,n2eabf,
     & lenbft_md,lend2e,n2edbf,lend1e,n1edbf
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist)*navst(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      real*8 energy1,energy2
      integer nadcalc,drt1,drt2,root1,root2
      common /nad/ energy1,energy2,nadcalc,drt1,drt2,root1,root2

       real*8 energy
       integer ienergy
       common /sifcid/ energy,ienergy


      integer assume_fc,nmpsy_fc(nsymx),mapmo_fc(nmomx),nexo_fc(nsymx)
      common /fcversion/ assume_fc,nmpsy_fc,mapmo_fc,nexo_fc

c
c     ##   integer section
c     # infomo=moints info file
c     # infomd=mcdfl  info file
c     # infocd=ciden  info file
c     # infocg=cigrd  info file
      integer infomo(ninfmx), infomd(ninfmx),
     &        infocd(ninfmx), infocg(ninfmx)
c     # icd =  pointers for array allocation...
      integer icd(100)
      integer avc2is,avcsl,avcors
      integer dendaf,d1dim
      integer iucid2,i,ierr,intdaf,imcd2u,iuefd2,itpbin,itotal,
     & icigrd,ifirst,itimer, ist, isym, iumoi2,info_dtr(ninfmx),
     & ijsym,ipair,icount,ind1,ind2,icount2
      integer jsym,j
      integer kntin(36)
      integer lcore1,lcore2,lcore3,lenbuk,lenm1e,lencor,lenm2e,lenbuf,
     & lenbuf_f(maxnst),lenm1e_dtr,lenm2e_dtr,lmda(maxnst,maxnst),
     & lmdb(maxnst,maxnst)
      integer mcietp(mcendm),mem1
      integer numpbf,nhblk,nmmmmt,nviiit,nadmot,nvimot,nnimot,
     & niiiit,nvamot,nvdmot,nipbuk,nmoc,numaa,numado,numaf,
     & ntemp,nmatot,ntitle,n1embf,nlevel,nela,
     & ndimr,nnmot,naar,nenrgy,nnamot,nelt,ntitld,n2embf,
     & nrowmax,n1embf_dtr,n2embf_dtr,nmbpr(maxnst),nm
      integer orbsym(nmomx),offset(maxnst)
      integer req2,nexo(8)
      integer smult,symoff(maxnst*(maxnst+1)/2),soff
      integer totsym
c
c     ##   real*8 section
c
      real*8 ecore0, efrzc, emc
      real*8 core(lencor)
      real*8 mcenrg(mcendm)
      real*8 norm_orbl, norm_csfl
      real*8 repnuc
c
c     ##  character section
c
      character*80 title(20),title_dtr(20)
      character*60 fname2
      character*4 slabel(8)
      character*8 orblab(nmomx),cidlab(nmomx)
c
c     ##   logical section
c
      logical ifeff
      logical pdflag
      logical qind,qfflag,qtflag
      logical used
      logical qopen,lheuristic
c
      integer forbyt, atebyt
      real*8 ddot_wr,dnrm2_wr
      external ddot_wr,forbyt, atebyt, dnrm2_wr
c
      integer le2e_1,n2ebf_1,le2e_2,n2ebf_2
      integer ifmt,ifmtin,mosize,densitysize
      integer nndxf
      nndxf(i)= i*(i-1)/2
c
c  le2e_1=length of buffers for mo 2-e integrals.(if nbft.le.255)
c  le2e_2=length of buffers for mo 2-e integrals.(if nbft.gt.255)
c  n2ebf_1=number of mo 2-e integrals in each buffer.(if nbft.le.255).
c  n2ebf_2=number of mo 2-e integrals in each buffer.(if nbft.gt.255).
ctm
c     data le2e_1/2047/, n2ebf_1/1364/
c     data le2e_2/2047/, n2ebf_2/1023/
      data le2e_1/4096/, n2ebf_1/2730/
      data le2e_2/4096/, n2ebf_2/2047/
c
c-------------------------------------------------------------
c
*@ifdef crayctss
*      call link('unit5=tty,unit6=unit5//')
*@endif
c
c     # translate filenames
      call trnfln(nfilmx, fname)
      call trnfln(ndafmx, dafnam)
c
c     initialize direct access file library
c
      call dainit

c
*@ifdef parallel
*       if (par_me().gt.0) goto 9111
*@endif
      
*@ifdef ibm
*!C  unit 6 is hardwired, so don't open.
*@else
      open(unit=nlist,file=fname(1),status='unknown')
*@endif
c     # open cigrdin file
      open(unit=nin,file=fname(2),status='unknown')
c     # open mcscfin file
      open(unit=mcscfin,file=fname(19),status='unknown')
c
      call ibummr(nlist)
      call timer(' ',0,icigrd,nlist)
      call timer(' ',1,itimer,nlist)
c
      write(nlist,6010)
6010  format(' program cigrd',
     & //' effective density and fock matrix construction for',
     & ' analytic energy',
     & /' gradient evaluation for general mrci wave functions',
     & //' programmed by: ron shepard and hans lischka',
     & //' references:',t15,'r. shepard, int. j. quantum chem.',
     & ' 31, 33 (1987).',
     & //t15,'r. shepard, i. shavitt, r. m. pitzer, d. c. comeau,',
     & ' m. pepper,'
     & /t20,'h. lischka, p. g. szalay, r. ahlrichs, f. b. brown,',
     & /t20,'and j.-g. zhao, int. j. quantum chem. s22, 149 (1988).',
     & //t15,'h. lischka, r. shepard, r. m. pitzer, i. shavitt, ',
     & 'm. dallos, t.',
     & /t20,'muller, p. g. szalay, m. seth, g. s. kedziora, ',
     & 's. yabushita,',
     & /t20,'and z. zhang, phys. chem. chem. phys. 3, 664-673 (2001).',
     & //' version date: 13-aug-01')
c
      write(nlist,6012)
6012  format(/' state-averag modifications:',
     & /t5,'X.1999, by: Michal Dallos',
     & /t5,'University Vienna, Austria')
c
*@ifdef parallel
*      call headwr(nlist,'PCIGRD','7.0beta')
*      write(nlist,6011)
*6011  format('program PCIGRD',
*     . //' pseudo-parallel operation to exploit shared memory',
*     .   '/distributed memory',
*     . //' for in-core sorting of integrals and density matrices ')
*@else
      call headwr(nlist,'CIGRD','5.4.2b')
*@endif
      call who2c( 'cigrd', nlist )
c
      write(nlist,6020) lencor, mem1, ifirst
6020  format(/' workspace allocation information:',
     & ' lencor=',i12,' mem1=',i16,' ifirst=',i16)
c
c     # set up the nndx(*) array...
      do 10 i=1,nmomx
         nndx(i) = (i*(i-1))/2
10    continue
c
c  ###############################################################
c  ##
c  ##   read the user input
c  ##
c  ###############################################################
c
c     ##  read the input from the cigrdin file
      call readin_gr()
c     ##  read the input from the mcscfin file
      call readin_mc()
c
c  ##  in case DCSF is to be calculated read in the energies of the
c  ##  corresponding two states
c
      if (nadcalc.gt.1) then
      if (samcflag.eq.0) then
cfp: in the case of an MRCI calculation the civout file is read
c for a SA-MCSCF NAC, the energies are taken from the
c density matrix file
       write(fname(11),'(a10,i1)') 'civout.drt',drt1
       write(nlist,*)'1: civfl file: ',fname(11)
c      fname(11)='civout'
       open( unit = civoutfl, file = fname(11), status = 'old' ,
     +  form = 'unformatted',iostat=ios )
       if (ios.ne.0)
     . call bummer ('can not open unit ',civoutfl,2)
c
       call rdcivout(
     &  nlist,   civoutfl,   root1,   a4den1,
     &  itype,   froot,      refvec1,  eci1,
     &  ncsf1,   'civout')
      close (civoutfl)
      energy1=eci1
c
      write(fname(11),'(a10,i1)') 'civout.drt',drt2
      write(nlist,*)'2: civfl file: ',fname(11)
       open( unit = civoutfl, file = fname(11), status = 'old' ,
     +  form = 'unformatted',iostat=ios )
       if (ios.ne.0)
     . call bummer ('can not open unit ',civoutfl,2)
c
       call rdcivout(
     &  nlist,   civoutfl,   root2,   a4den2,
     &  itype,   froot,      refvec2,  eci2,
     &  ncsf2,   'civout')
      close (civoutfl)
      energy2=eci2
c
      endif ! end of: if (nadcalc.gt.1) then
      endif  ! of: if (samcflag.eq.0) then


c
      call prtfn(nlist,1,'listing file',nlist)
      call prtfn(nin,2,'input file',nlist)
      close(unit=nin)
      call prtfn(mcscfin,19,'input file',nlist)
      close(unit=mcscfin)
c
c     # read the header info from the transformed integral file.
c
      open(unit=moints,file=fname(4),status='old',form='unformatted')
      call prtfn(moints,4,'mo integrals file',nlist)
c
      lenm1e = 0
      n1embf = 0
      lenm2e = 0
      n2embf = 0
      do i = 1, nmomx
         orblab(i) = ' '
      enddo
c
      call rdhint(moints,ntitle,title,infomo,nmot,nsym,nmpsy,orblab)
c
      lenm1e = max(lenm1e,infomo(2))
      n1embf = max(n1embf,infomo(3))
      lenm2e = max(lenm2e,infomo(4))
      n2embf = max(n2embf,infomo(5))
c
c     # read the header information from the ci one-particle
c     # density matrix file. print and discard symmetry info.
c
      if (nadcalc.ge.1) then
c     #
c     #  to get rid of these awkard, extremely slow and io-intense
c     #  operations coded in runc/cid2tr, trci2den.x, dzero.x 
c     #  replace it by simply using cigrd to carry out the sum up
c     #  process, which is much more efficient
c     #
       fname(5)='cid1fl.tr'
       fname(6)='cid2fl.tr'
      endif
       write(nlist,*)
       write(nlist,*)' 1-e CI density read from file:',fname(5)
       write(nlist,*)' 2-e CI density read from file:',fname(6)
      open(unit=ciden1,file=fname(5),status='unknown',
     & form='unformatted')
      call prtfn(ciden1,5,'ci 1-particle density file',nlist)
c
c
c     for a frozen core calculation the ci density file contains
c     less MOs than anticipated from moints (now the full ao-mo trafo)
c     frozen core mo data is stored in nmpsy_fc 
c
      call rdhcid(nlist,ciden1,infocd,ntitld,title(ntitle+1),nsym,nmot,
     & nmpsy,lheuristic)
      ntitle=ntitle+ntitld
c
      lenm1e = max(lenm1e,infocd(2))
      n1embf = max(n1embf,infocd(3))
      lenm2e = max(lenm2e,infocd(4))
      n2embf = max(n2embf,infocd(5))
c
c     # eliminate any redundant titles.
c
      call unique(ntitle,title)
c
c     # check the cidrt header and check for consistency.
c     # 1:drtbuf
cfp: check only if a CI calculation has been actually performed
      if(samcflag.eq.0)then
       open(unit=cidrt,file=fname(3),status='old')
       call prtfn(cidrt,3,'ci drt file',nlist)
c
c     for a frozen core calculation this is consistent
c     with the data from  moints 
c
      icd(1) = 1
      call rdhdrt_ci(cidrt,nsym,nmot,nmpsy,nexo,title(ntitle+1),
     .   core(icd(1)))
      write(nlist,6060) 'cidrt header read'
      ntitle=ntitle+1

       if (assume_fc.gt.0) then 
c
c     now construct map vector from cidrtfl.ci to cidrtfl
c
      open(unit=iunits(31),file=fname(31),status='old')
      call prtfn(iunits(31),31,'ci drt file (CI frozen core)',nlist)
      call cnstrctmap(core(1),lencor,mapmo_fc,
     .   nmpsy,nmpsy_fc,nsym,nexo,nexo_fc)
      close(unit=iunits(31))
      else
c    
c     just create an identity mapping 
c
        do i=1,nmot
          mapmo_fc(i)=i
        enddo
       endif

       close(unit=cidrt)
      else
cfp_tmp
       write(nlist,*)'SA-MCSCF: skipping consistency check with cidrtfl'
       title='cidrt_title'
      endif
      call unique(ntitle,title)
c
c     ##
c     ## read the MCSCF input
c     ##
c     # 1:bufmcdrt
c
c     #  initialize subcounter array
      cpt2(1) = 0
      cpt3(1) = 0
      cpt4(1) = 0
      cpt2_tot = 0
      cpt3_tot = 0
      cpt4_tot = 0
      nrowmax = 0
c
c     open(unit=mcdrt,file=fname(7),status='old')
c     call prtfn(mcdrt,7,'mcscf drt file',nlist)
c
      write(nlist,6050)
6050  format(/3x,6('*'),'  MCSCF DRT info section  ',6('*'))
c
      ncsf_max=1
      lenbuf=0
      do ist = 1, nst
c
         write(nlist,'(/a,i3)') ' Information for MCSCF DRT number', ist
c        ndrt(ist) = 61+ist
c
         ntitle = ntitle + 1
c        #------------------------------------------
c        #  read the mcscf drt header for all DRTs.
c        #------------------------------------------
         call rdhdrt_mc(
     &    title(ntitle),   nrow_f(ist),    ist,          ssym(ist),
     &    smult,           nelt,           nela,         nwalk_f(ist),
     &    ncsf_f(ist),     lenbuf_f(ist),  nmomx,        namot,
     &    ndmot,           nsym,           core(icd(1)), lencor)
c
         call unique(ntitle,title)
c
         if (ncsf_f(ist) .eq. 0) call bummer(
     &    'driver: no csf in the symmetry number ',ist,faterr)
c
         cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
         cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
         cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist)*navst(ist))
         cpt2_tot = cpt2_tot + forbyt(nsym*nrow_f(ist))
         cpt3_tot = cpt3_tot + forbyt(nwalk_f(ist))
         cpt4_tot = cpt4_tot + atebyt(ncsf_f(ist)*navst(ist))
         if (nrowmax.lt.nrow_f(ist)) nrowmax=nrow_f(ist)
         if (ncsf_max.lt.ncsf_f(ist)) ncsf_max=ncsf_f(ist)
c
         if (lenbuf_f(ist).gt.lenbuf) lenbuf=lenbuf_f(ist)
c
      enddo  ! ist=1,nst
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,
c     # 6:buf,7:doub,8:syml,9:nj,10:njskp,11:a,12:b,13:l,14:y
c
      nlevel= namot + 1
      icd(2)  = icd(1)  + cpt2_tot
      icd(3)  = icd(2)  + cpt2_tot
      icd(4)  = icd(3)  + cpt2_tot
      icd(5)  = icd(4)  + cpt3_tot
      icd(51) = icd(5)  + cpt3_tot
      icd(6)  = icd(51) + forbyt(nlevel)
      icd(7)  = icd(6)  + forbyt(lenbuf)
      icd(8)  = icd(7)  + forbyt(ndmot)
      icd(9)  = icd(8)  + forbyt(nlevel)
      icd(10) = icd(9)  + forbyt(nlevel)
      icd(11) = icd(10) + forbyt(nlevel)
      icd(12) = icd(11) + forbyt(nrowmax)
      icd(13) = icd(12) + forbyt(nrowmax)
      icd(14) = icd(13) + forbyt(4*nrowmax)
      icd(15) = icd(14) + forbyt(nsym*4*nrowmax)
      itotal  = icd(15)
      call memcheck(itotal,lencor,'driver 1')
c
      do ist = 1, nst
c
c        ##  initialize all of the mcscf orbitals to be of type virtual
c        ##  (the orbital types are identical in all the DRTs, so we can
c        ##  have the orbital type information from the last DRT
c
         call iset(nmomx,3,mctype,1)
c
c        #-----------------------------------------
c        #  read the mcscf drt files for all DRTs
c        #-----------------------------------------
         call rddrt_mc(
     &    nsym,          namot,         ist,           ndmot,
     &    nmot,          nrow_f(ist),   nwalk_f(ist),  ncsf_f(ist),
     &    lenbuf_f(ist), core(icd(6)),  core(icd(7)),  core(icd(8)),
     &    core(icd(9)),  core(icd(10)), core(icd(11)), core(icd(12)),
     &    core(icd(13)), core(icd(14)),
     &    core(icd(1)+cpt2(ist)),
     &    core(icd(2)+cpt2(ist)),  core(icd(3)+cpt2(ist)),
     &    core(icd(5)+cpt3(ist)),  core(icd(4)+cpt3(ist)),
     &    nmpsy,         mctype,         mapml,         .true.,
     &    core(icd(51)))
c
         if (navst(ist).gt.ncsf_f(ist)) then
            write(nlist,'(/a,i2,a,i4,a,i4,a)')
     &       ' In the DRT number', ist,
     &       ' there are only', ncsf_f(ist), ' CSFs, so', navst(ist),
     &       ' states are not allowed to be averaged!'
            call bummer('Incorrect navst value',navst(ist),faterr)
         endif
      enddo                     ! ist=1,nst
c
c     # calculate symmetry indexing arrays.
c
      open(unit=mcrest,file=fname(12),status='old',form='unformatted')
      call prtfn(mcrest,12,'mcscf restart file',nlist)
      call rdrsth()
      close(mcrest)
      do isym = 1, nsym
         nbpsy(isym) = nmpsy(isym)
      enddo                     ! isym = 1,nsym
c
c      ##  calculate symmetry arrays and orbital mapping arrays.
      call add1
c      write(nlist,*) 'symm1=',(symm(i),i=1,nmot)
c     ##  calculate additional array for C*s,C*r and M*s calculation
      call add1a
c      write(nlist,*) 'symm2=',(symm(i),i=1,nmot)
c      write(nlist,*) 'mapivm=',(mapivm(i),i=1,nmot)
c
c     # read the mcscf density matrix header info.  discard symmetry and
c     # mapping vector info.
c
c     # mcres(*) classifies the invariant orbital subspaces.
c
      open(unit=mcden1,file=fname(8),status='old',form='unformatted')
      call prtfn(mcden1,8,'mcscf 1-particle density file',nlist)
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,6:iorder,7:nsym,8:nipsy,9:nbpsy,
c     # 10:nimot,11:mapim,12:nmot,13:,14:,15:mapar
      icd(8)  = icd(7)  + forbyt(1)
      icd(9)  = icd(8)  + forbyt(8)
      icd(10) = icd(9)  + forbyt(8)
      icd(11) = icd(10) + forbyt(1)
      icd(12) = icd(11) + forbyt(nmot)
      icd(13) = icd(12) + atebyt(1)
      icd(14) = icd(13)
      icd(15) = icd(14)
      icd(16) = icd(15) + forbyt(nmomx)
      itotal  = icd(16)
      call memcheck(itotal,lencor,'driver 2')
c
      call rdhmcd(
     & nlist,           mcden1,        infomd,        ntitld,
     & title(ntitle+1), core(icd(7)),  core(icd(8)),  core(icd(9)),
     & emc,             mcietp,        mcenrg,        core(icd(10)),
     & core(icd(12)),   core(icd(11)), mcres,         slabel,
     & orblab,          core(icd(15)), nenrgy)
c
      ntitle = ntitle + ntitld
      call unique(ntitle,title)
      write(nlist,6060) 'mcd1fl header information read'
      lenm1e = max(lenm1e,infomd(2))
      n1embf = max(n1embf,infomd(3))
      lenm2e = max(lenm2e,infomd(4))
      n2embf = max(n2embf,infomd(5))
c
c     # find the mcscf allowed/essential active-active rotations.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:iwop
c
      nnamot = nndx(namot+1)
      icd(7) = icd(6) + forbyt(nnamot)
      call faar( naar, core(icd(6)), core(icd(7)))
      ndimr = nadt + naar + nvdt + nvat
c
      nnmot  = nndx(nmot)+nmot
      nnimot = nndx(nimot)+nimot
      nvimot = nvmot*nimot
      nadmot = namot*ndmot
      nvdmot = nvmot*ndmot
      nvamot=nvmot*namot
c
c     # calculate orbital indexing and addressing arrays.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,6:iorder,7:offmm,8:addmm,9:off1,
c     # 10:addii,11:off2,12:addvi,13:add,14:15:
c
      icd(8)  = icd(7)  + forbyt(nnmot)
      icd(9)  = icd(8)  + forbyt(nnmot)
      icd(10) = icd(9)  + forbyt(nnimot)
      icd(11) = icd(10) + forbyt(nnimot)
      icd(12) = icd(11) + forbyt(nnimot)
      icd(13) = icd(12) + forbyt(nvimot)
      icd(14) = icd(13) + forbyt(addpt(24))
      icd(15) = icd(14)
      itotal  = icd(15) -1
      call memcheck(itotal,lencor,'driver 3')
c
      call add2(
     & niiiit,        nviiit,        nmmmmt,       naar,
     & core(icd(7)),  core(icd(8)),  core(icd(9)), core(icd(10)),
     & core(icd(11)), core(icd(12)), nhblk)
c
c  #  calculate integral adressing arrays
      call add2a(naar,core(icd(13)))

ctm
      call getlenbfs(lenbfs,numint,nlist)
ctm
c
      avcors = lencor - itotal
      req2   = ldamax + forbyt(numpbf(ldamax)+1) + lenbfs
      avc2is = avcors - req2
c
      call blkasn(lencor-itotal, avc2is)
c
      call timer('initialization and setup required',3,itimer,nlist)
c
c     #------------------------------------------
c     #  read in the 1-electron arrays
c     #------------------------------------------
c
c     # read in the one-electron hamiltonian and overlap integrals.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:s1,19:buf,20:val,21:ilab,22:scr
c
      icd(16) = icd(15) + forbyt(nvamot)
      icd(17) = icd(16) + atebyt(nnmt)
      icd(18) = icd(17) + atebyt(nnmt)
      icd(19) = icd(18) + atebyt(nnmt)
      icd(20) = icd(19) + atebyt(lenm1e)
      icd(21) = icd(20) + atebyt(n1embf)
      icd(22) = icd(21) + forbyt(2*n1embf)
      itotal  = icd(22) + atebyt(nnmt) - 1
      call memcheck(itotal,lencor,'driver 4')
c
c     #  readin the one-electron hamiltonian and overlap integrals.
      call rdsh1(
     & moints,        lenm1e,        n1embf,        infomo,
     & core(icd(19)), core(icd(21)), core(icd(20)), core(icd(18)),
     & core(icd(16)), nndx,          nnmt,          nsm,
     & symm,          nnsm,          mapmm,         nsym,
     & nmpsy,         kntin )
      if(print .ge. 1) then
         call plblks('mo overlap matrix',core(icd(18)),nsym,nmpsy,
     &    '  mo',1,nlist)
         call plblks('mo 1-e hamiltonian',core(icd(16)),nsym,nmpsy,
     &    '  mo',1,nlist)
      endif
c
c     # check the overlap integrals s1(*).
      call checks( core(icd(18)), nsym, nmpsy)
      write(nlist,6060)
     & 'one electron hamiltonian and overlap integrals read'
c
c     # core allocation table
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii, c  11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:s1,19:buf,20:val,21:ilab,22:scr
c
c     # read in the ci one-particle density matrix.
      call rdden1(
     & ciden1,        lenm1e,        n1embf,        infocd,
     & core(icd(19)), core(icd(21)), core(icd(20)), core(icd(22)),
     & core(icd(17)), nndx,          nnmt,          nsm,
     & symm,          nnsm,          mapmm,         nsym,
     & nmpsy,         kntin ,mapmo_fc, nmpsy_fc,
     & assume_fc, nadcalc)


      close(unit=ciden1)
c
      if(print .ge. 1) then
         call plblks('ci d1 matrix',core(icd(17)),nsym,nmpsy,
     &    '  mo',1,nlist)
      endif
c
      call timer('reading 1-e arrays required',3,itimer,nlist)
c
c     # determine the da sorting parameters.
c     # lcore1 = workspace available during first half sort.
c     # lcore2 = workspace available during fock matrix construction.
c     # lcore3 = workspace available during q matrix construction.
c
      lcore1 = lencor-icd(18)+1 -( atebyt(lenm2e)+forbyt(n2embf*4)+
     &         atebyt(n2embf))
      lcore2 = lencor-icd(18)+1 -( 4*atebyt(n2mt)+2*atebyt(nnmt) )
      lcore3 = lencor-icd(18)+1 -(10*atebyt(n2mt) )
c
cfp: nmmmmt is already adjusted (?)
      call dalen(
     & nlist,  lcore1, lcore2, lcore3,
     & nmmmmt, nbkmx,  nbuk,   lendar,
     & nvpbk,  nvpsg )

c
c     ##------------------------------------------
c     ##  process and sort the 2-electron arrays
c     ##------------------------------------------
c
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:dbufv,19:idbuf,20:dabuf,21:bufin,22:ibufin,23:valin
c
c     #
c     #   2-electron integrals section
c     #
      icd(19) = icd(18) + atebyt(nvpbk*nbuk)
      icd(20) = icd(19) + forbyt(nvpbk*nbuk)
      icd(21) = icd(20) + atebyt(lendar)
      icd(22) = icd(21) + atebyt(lenm2e)
      icd(23) = icd(22) + forbyt(n2embf*4)
      itotal  = icd(23) + atebyt(n2embf)-1
      call memcheck(itotal,lencor,'driver 5')
c
     
      intdaf = 1
      call indast(intdaf)
c     call daini(intdaf)
*@ifdef parallel
* 9111 call ga_sync() 
*      call daini(intdaf,30,'da1scr',0,2)
*      if (par_me().eq.0) call getmosize(nmpsy,mosize,0)
*@else
      call daini(intdaf,30,'da1scr',0,0)
c  compute approx. mosize!
      call getmosize(nmpsy,mosize,0) 
*@endif
*@ifdef parallel
*      call ga_sync()
*      call daopw(intdaf,lendar,mosize) 
*      if (par_me().gt.0) goto 9222
*@else
      call daopw(intdaf,lendar,mosize) 
*@endif
      write(nlist,*)'beginning two-electron integral sort...'
c
c     # open the two electron integral file.
      call sifo2f(moints, moints2, fname(20), infomo, iumoi2, ierr)
      if (ierr .ne. 0) then
         call bummer('driver: integrals, from sifo2f, ierr=',
     &    ierr,faterr)
      endif
c
      call sort1(
     & iumoi2,        infomo,        intdaf,        core(icd(7)),
     & core(icd(8)),  core(icd(18)), core(icd(19)), core(icd(20)),
     & core(icd(21)), core(icd(22)), core(icd(23)), mapmo_fc,
     . 0,core(icd(17)),nmot,0)
c
c     # close the two-electron integral file
      call sifc2f(moints2, infomo, ierr)
      if(ierr .ne. 0) then
         call bummer('driver: integrals, from sifc2f, ierr=',
     &    ierr,faterr)
      endif
c     # close the one-electron integral file.
      close(unit=moints)
      call daclw(intdaf)
c
      call timer('2-e integral sort required',3,itimer,nlist)
c     #
c     # density matrix section
c     #
c
c     # open the 1-particle density matrix file
      open(unit=ciden1,file=fname(5),status='unknown',
     & form='unformatted')
c
c     # open the 2-particle ci density matrix file.
      call sifo2f(ciden1, ciden2, fname(6), infocd, iucid2, ierr)
      if (ierr .ne. 0) then
         call bummer('driver: ci density, from sifo2f, ierr=',
     &    ierr,faterr)
      endif
c
      call prtfn(iucid2,6,'ci 2-particle density file',nlist)
c
      dendaf = 2
      call indast(dendaf)
c     call daini(dendaf)
*@ifdef parallel
* 9222 call ga_sync()
*      call daini(dendaf,31,'da2scr',0,2)
*      densitysize=mosize
*      call daopw(dendaf,lendar,densitysize)
*      if (par_me().gt.0) goto 9333
*@else
      call daini(dendaf,31,'da2scr',0,0)
      call daopw(dendaf,lendar,densitysize)
*@endif
      write(nlist,6060) 'beginning two-particle density matrix sort...'
c
      call sort1(
     & iucid2,        infocd,        dendaf,        core(icd(7)),
     & core(icd(8)),  core(icd(18)), core(icd(19)), core(icd(20)),
     & core(icd(21)), core(icd(22)), core(icd(23)),mapmo_fc,
     . assume_fc,core(icd(17)),nmot,nadcalc)
      call daclw(dendaf)
c     # close the two-particle ci density file.
      call sifc2f(ciden2,infocd,ierr)
      if (ierr.ne.0) call bummer ('problem closing cid2fl, ierr = ',
     & ierr, faterr)
c     # close the one-particle ci density file.
      close(unit=ciden1)
      call timer('2-particle density sort required',3,itimer,nlist)
c
c     #
c     #  construct the the ci fock matrix.
c     #
c
c     # construct the 1-e contributions to the ci fock matrix.
c     #   fci(p,q) = fci(p,q) + sum(t) h1(rt)*d1ci(ts)
c
      ifeff = .false.
c
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:scr1,20:scr2
c
      icd(19) = icd(18) + n2mt
      icd(20) = icd(19) + n2mt
      icd(21) = icd(20) + n2mt
      itotal  = icd(21) - 1
      call memcheck(itotal,lencor,'driver 6')
c
      call wzero( n2mt, core(icd(18)), 1 )
      call fockm1(
     & core(icd(18)), core(icd(16)), core(icd(17)), core(icd(19)),
     & core(icd(20)), ifeff )
      if(print .ge. 1) then
         call prblks('ci h matrix',core(icd(18)),nsym,nmpsy,nmpsy,
     &    '  mo','  mo',1,nlist)
      endif
c
c     # construct the 2-e contributions to the ci fock matrix.
c     #   fci(p,q) = fci(p,q) + sum(tuv) h2(ptuv)*d2ci(qtuv)
c
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:dabufv,20:idabuf,21:dabuf,22:h2,23:d2,
c     # 24:scrh,25:scrd,26:scrf
c
      icd(20) = icd(19) + atebyt(nvpbk)
      icd(21) = icd(20) + forbyt(nvpbk+1)
      icd(22) = icd(21) + atebyt(lendar)
      icd(23) = icd(22) + atebyt(nvpsg+nnmt)
      icd(24) = icd(23) + atebyt(nvpsg+nnmt)
      icd(25) = icd(24) + atebyt(n2mt)
      icd(26) = icd(25) + atebyt(n2mt)
      icd(27) = icd(26) + atebyt(n2mt)-1
      itotal  = icd(27) - 1
      call memcheck(itotal,lencor,'driver 7')
c
      call daopr(intdaf)
      call daopr(dendaf)
cfp_change: AO contributions
      call fockm2(
     & nmmmmt,         core(icd(22)),  core(icd(23)),  core(icd(21)),
     & core(icd(20)),  core(icd(19)),  core(icd(18)),  core(icd(24)),
     & core(icd(25)),  core(icd(26)),  intdaf,         dendaf,
     & ifeff)
      inquire(unit=30,opened=used)
      call timer('fci(*) construction required',3,itimer,nlist)
      if (print.ge.1) then
         call prblks('ci fock matrix',core(icd(18)),nsym,nmpsy,nmpsy,
     &    '  mo','  mo',1,nlist)
      endif                     ! (print.ge.1)
*@ifndef noclose
c     # unit= bug fixed 13-aug-01.  -rls
      inquire(unit=daunit(dendaf),opened=qopen,iostat=ierr)
c     if (qopen .and. ierr.eq.0) call daclr(dendaf)
*@endif
      call daclr(dendaf)
c
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:scr
c
c     #  read in the antisymmetric 1-el density matrix
c
      if (nadcalc.ge.2) then
c
       write(nlist,
     &  '(/''  reading antisymmetric 1-e CI density matrix'')')
       open(unit=d1atrfl,file=fname(22),status='unknown',
     &  form='unformatted')
       call prtfn(d1atrfl,5,'antisymmetric 1-e ci density file',
     &  nlist)
       call rdhcidtr(
     &  nlist,      d1atrfl,  info_dtr,   ntitld,
     &  title_dtr,  nsym,     nmot,       nmpsy,
     &  cidlab)
       lenm1e_dtr = info_dtr(2)
       n1embf_dtr = info_dtr(3)
       lenm2e_dtr = info_dtr(4)
       n2embf_dtr = info_dtr(5)
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:scr
c     # 20:buf,21:val,22:ilab:,23:d1atr
c
       icd(21) = icd(20) + atebyt(lenm1e_dtr)
       icd(22) = icd(21) + atebyt(n1embf_dtr)
       icd(23) = icd(22) + forbyt(2*n1embf_dtr)
       icd(24) = icd(23) + atebyt(nmot*(nmot+1)/2)
       icd(25) = icd(24) + atebyt(nmot*(nmot+1)/2)
       itotal  = icd(25) - 1
       call memcheck(itotal,lencor,'driver 8.2')
c
c  #  initialize symoff(*)
      call izero_wr(maxnst*(maxnst+1)/2,symoff,1)
       i=0
       do isym=1,nsym
        do jsym=1,isym-1
          ijsym=nndxf(isym)+jsym
          symoff(ijsym)=i
          i=i+nmpsy(isym)*nmpsy(jsym)
        enddo
          ijsym=nndxf(isym)+isym
          symoff(ijsym)=i
          i=i+nmpsy(isym)*(nmpsy(isym)+1)/2
       enddo
       call rdcid1tr(
     &  d1atrfl,       lenm1e_dtr,        n1embf_dtr,    info_dtr,
     &  core(icd(20)), core(icd(22)),     core(icd(21)),  core(icd(23)),
     &  core(icd(24)),
     &  nndx,          nmot*(nmot+1)/2,   nsm,           orbsym,
     &  nnsm,          mapmm,             nsym,          nmpsy,
     &  kntin,         symoff,assume_fc,mapmo_fc, nmpsy_fc )
c
c  #  determine the symmetry pairs for the given symmetry group
      call symdet(nsym,nmpsy,lmda,lmdb,nmbpr)
c  #  initialize offset(*)
      do isym=1,maxnst
      offset(isym)=0
      nm=nmpsy(isym)
      enddo ! end of: do isym=1,maxsym
      offset(1)=1
      do isym=2,nsym
      offset(isym)=offset(isym-1)+nmpsy(isym-1)
      enddo
c
      call wzero(nmomx*nmomx,d1atr,1)
cdd
       totsym=1
       write(nlist,*)' Antisymmetric 1-e density matrix'
cdd
       do ipair=1,nmbpr(totsym)
       isym=lmda(totsym,ipair)
       jsym=lmdb(totsym,ipair)
        if (isym.ne.jsym) then
         ijsym=nndxf(isym)+jsym
         soff=symoff(ijsym)
c         if (lprint) then
           write(nlist,*)' Antisymmetr. offdiagonal d1 for symmetry',
     &      isym,jsym
           call prblkc('d1 offdiagonal block',
     .       core(icd(23)+soff),nmpsy(isym),nmpsy(isym),nmpsy(jsym),
     .      cidlab(offset(isym)),cidlab(offset(jsym)),1,6)
c         endif ! end of: if (lprint) then
           icount = 0
           do i=1,nmpsy(isym)
             do j=1,nmpsy(jsym)
             ind1=offset(isym)+i-1
             ind2=offset(jsym)+j-1
             d1atr(ind1,ind2)=core(icd(23)+soff+icount)
             d1atr(ind2,ind1)=onem*core(icd(23)+soff+icount)
             icount = icount+ 1
             enddo ! end of: do j=1,nmpsy(jsym)
           enddo ! end of: do i=1,nmpsy(isym)
        else
         ijsym=nndxf(isym)+isym
         soff=symoff(ijsym)
        write(nlist,*)'ijsym,symoff(ijsym)',ijsym,symoff(ijsym)
c         if (lprint) then
           write(nlist,*)' Antisymmetr. diagonal d1 for symmetry',isym,jsym
           call plblkc('d1_diagonal ',
     &       core(icd(23)+soff),nmpsy(isym),cidlab(offset(isym)),1,6)
           icount = 0
           do i=1,nmpsy(isym)
             do j=1,i
             ind1=offset(isym)+i-1
             ind2=offset(isym)+j-1
             d1atr(ind1,ind2)=core(icd(23)+soff+icount)
             d1atr(ind2,ind1)=onem*core(icd(23)+soff+icount)
             icount = icount+ 1
             enddo ! end of: do j=1,i
           enddo ! end of: do i=1,nmpsy(isym)
c         endif! end of:if (lprint) then
        endif
c
       enddo ! end of: do i=1,nmbpr(ssym)
c
         call prblks('d1atr (remapped)',d1atr,1,nmomx,nmomx,
     &    '  mo','  mo',1,nlist)
c
c  ##  create d1atr in orbital level adressing

      endif ! end of: if (nadcalc.ge.2) then
c
      numaa  = 0
      numado = 0
      numaf  = 0
c
c     # determine if mcscf invariant orbital subspace resolutions are
c     #   required.
c     #   ! just check for required resolutions !
      call iosres(
     & core(icd(18)),  core(icd(6)),  core(icd(19)),  core(icd(19)),
     & core(icd(19)),  core(icd(19)), core(icd(19)),  core(icd(19)),
     & numaa,          numado,        numaf,          0)
      write(nlist,'(/3x,a)') 'Block resolution info:'
      write(nlist,6060) 'numaa  =', numaa
      write(nlist,6060) 'numado =', numado
      write(nlist,6060) 'numaf  =', numaf
      write(nlist,6070) 'qfresd =', qfresd
      write(nlist,6070) 'qfresa =', qfresa
      write(nlist,6070) 'qfresv =', qfresv
      write(nlist,6070) 'qresaf =', qresaf
cfp
      if((samcflag.eq.1).and.(nadcalc.le.1))then
c in the case of MCSCF without additional MR-CI there should
c be no required resolution
         if(qfresd.or.qfresa.or.qfresv.or.qresaf)then
        write(nlist,*)'*** Warning: cigrd requires orbital resolution!'
           write(nlist,*)'For an mcscf wave function this is a
     & numerical error.'
           write(nlist,*)'Consider using tighter convergence
     & criteria in mcscfin!'
        call bummer("cigrd: orb. res. required for mcscf",1,wrnerr)
         endif
      endif
c
c     #
c     #  read the mcscf 1-electron density matrix and fock matrices.
c     #
c
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:anofqa/atot,20:ado,21:mcd1,22:fmc,23:fqdmc,24:buf,
c     # 25:ilab,26:val
c
      nmatot  = max(numaa, numado, numaf)
      icd(20) = icd(19) + atebyt(nmatot)
      icd(21) = icd(20) + atebyt(nmatot)
      icd(22) = icd(21) + atebyt(n2mt)
      icd(23) = icd(22) + atebyt(n2mt)
      icd(24) = icd(23) + atebyt(n2mt)
      icd(25) = icd(24) + atebyt(lenm1e)
      icd(26) = icd(25) + forbyt(2*n1embf)
      itotal  = icd(26) - 1
      call memcheck(itotal,lencor,'driver 8')
c
c     #...note: mc density elements are indexed by mo indices. -rls
c
      call rdstv1(
     & mcden1,        infomd,        lenm1e,        n1embf,
     & core(icd(24)), core(icd(25)), core(icd(26)), core(icd(21)),
     & core(icd(22)), core(icd(23)), nndx,          nnmt,
     & nsm,           symm,          nnsm,          mapmm,ifmtin)
      close(unit=mcden1)
      if(print .ge. 1) then
         call plblks('mc d1 matrix',core(icd(21)),nsym,nmpsy,
     &    '  mo',1,nlist)
         call plblks('mc fock matrix',core(icd(22)),nsym,nmpsy,
     &    '  mo',1,nlist)
         call plblks('fqdmc matrix',core(icd(23)),nsym,nmpsy,
     &    '  mo',1,nlist)
      endif
      write(nlist,6060) 'mcd1fl and fock matrices read'
c
c     # mcd1(*), fmc(*), and fqdmc(*) are returned lower-triangular;
c     # expand them to square (symmetry packed).
c
      icd(25) = icd(24) + atebyt(n2mt)
      itotal  = icd(25) - 1
      call memcheck(itotal,lencor,'driver 8.1')

      call xpnd1(core(icd(21)),core(icd(24)),nmpsy,nsym)
      call dcopy_wr(n2mt,core(icd(24)),1,core(icd(21)),1)
      call xpnd1(core(icd(22)),core(icd(24)),nmpsy,nsym)
      call dcopy_wr(n2mt,core(icd(24)),1,core(icd(22)),1)
      call xpnd1(core(icd(23)),core(icd(24)),nmpsy,nsym)
      call dcopy_wr(n2mt,core(icd(24)),1,core(icd(23)),1)
c
c     #
c     #   compute the Ax(*) matrices for the orbital resolution:
c     #    Aq(i,j) = ( fci(i,j)-fci(j,i) ) / ( (e(jj) - e(ii) )
c     #    Ad(i,j) = ( fci(i,j)-fci(j,i) ) / ( (n(jj) - n(ii) )
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:anofqa,20:ado,21:mcd1,22:fmc,23:fqdmc,24:af
c
      if(nmatot.ne.0)call wzero(nmatot,core(icd(19)),1)
      if(numado.ne.0)call wzero(numado,core(icd(20)),1)
      if(numaf .ne.0)call wzero(numaf, core(icd(24)),1)
c
c     # construct the Aq or Ad matrix for all reguired blocks,
c     # store them separately
      call iosres(
     & core(icd(18)),  core(icd(6)),  core(icd(21)),  core(icd(22)),
     & core(icd(23)),  core(icd(19)), core(icd(20)),  core(icd(24)),
     & numaa,          numado,        numaf,          1)
      if(print.ge.1)then
         if(qfresa)call prblks('anofqa matrix (active-active block)',
     &    core(icd(19)),nsym,nmpsy,nmpsy,'  mo','  mo',1,nlist)
         if(qfresv)call prblks('af matrix (virtual-virtual block)',
     &    core(icd(24)),nsym,nmpsy,nmpsy,'  mo','  mo',1,nlist)
         if(qfresd)call prblks('ado matrix (inactive-inactive block)',
     &    core(icd(20)),nsym,nmpsy,nmpsy,'  mo','  mo',1,nlist)
      endif
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:anofqa,20:Aq,21:mcd1,22:fmc,23:fqdmc,24:af
c
c     # add up the A matrix blocks into one field Aq
c     # (Fock matrix resolution)
      if (qfresd .or. qfresv .or. qresaf) then
         call comlgm(
     &    qfresd,         qfresv,         qresaf,    core(icd(19)),
     &    core(icd(20)),  core(icd(24)),  n2mt )
         if (print .ge. 1) then
            call prblks('lgmx matrix',core(icd(20)),nsym,nmpsy,nmpsy,
     &       '  mo','  mo',1,nlist                       )
         endif
c
c        # compute the averaged fock operator resolution contributions,
c        # in the inactive and/or active orbital and/or virtual orbital
c        # subspaces.
c
c        # use the lgmx(*) to construct the product lgmx*supermatrix
c
c        #  calculate the Qa matrix:
c        #  Qa(uv) = sum(wy) [2g(wyuv)-g(wuyv)]Aq(wy)
c
c        # core allocation table:
c        # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c        # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c        # 18:fci,19:anofqa,20:Aq,21:mcd1,22:fmc,23:fqdmc,24:fqa,25:h2,
c        # 26:dabuf,27:idabuf,28:dabufv,29:scrh,30:scrf,31:dummy
c
         icd(25) = icd(24) + atebyt(n2mt)
         icd(26) = icd(25) + atebyt(nvpsg+nnmt)
         icd(27) = icd(26) + atebyt(lendar)
         icd(28) = icd(27) + forbyt(nvpbk+1)
         icd(29) = icd(28) + atebyt(nvpbk)
         icd(30) = icd(29) + atebyt(n2mt)
         icd(31) = icd(30) + atebyt(n2mt)
         itotal  = icd(31) - 1
         call memcheck(itotal,lencor,'driver 9')
c
         pdflag = qfresv .or. qfresd .or. qresaf
         inquire(unit=30,opened=used)
c
cfp_change: fqa (Q^A) construction in the AO basis
         call fockq(
     &    core(icd(25)),  core(icd(26)),  core(icd(27)),  core(icd(28)),
     &    core(icd(29)),  core(icd(30)),  intdaf,         nmmmmt,
     &    pdflag,         core(icd(24)),  core(icd(20)),  .false.,
     &    core(icd(31)),  core(icd(31)))
         call timer('fqa(*) and fqdmc(*) required',3,itimer,nlist)
         if (print.ge.1) then
            call prblks('fqa matrix',core(icd(24)),nsym,nmpsy,nmpsy,
     &       '  mo','  mo',1,nlist)
         endif
c
c        # calculate fq:
c        # fq(rs) = sum(t) h1(r,t)*d1do(t,s)
c        #          + sum(tuv) h2(rtuv)*d2do(stuv)
c        #  where:
c        #  d1do(u,v) = 2*Aq(u,v)
c        #  d2do(uvwx) = 2*Aq(uv)*dmc(wx) + 2*Aq(wx)*dmc(uv)
c        #              -half*( Aq(uw)*dmc(vx) + Aq(ux)*dmc(vw) +
c        #                      Aq(vx)*dmc(uw) + Aq(vw)*dmc(ux)  )
c        #  add up fq to the fci matrix:
c        #  forb(*) = fci(*) = fci(*) + fq(*)
c
c        # core allocation table:
c        # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c        # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c        # 18:forb,19:atot,20:Aq,21:mcd1,22:fmc,23:fqdmc,24:fqa,25:scr
c
         icd(26) = icd(25) + atebyt(n2mt)
         itotal = icd(26) - 1
         call memcheck(itotal,lencor,'driver 10')
c
cfp_tmp: fdo = F^Q
cfp_tmp: comput forb because ftot = forb + fl = fL
         call fdo(
     &    core(icd(18)), core(icd(20)), core(icd(24)), core(icd(21)),
     &    core(icd(23)), core(icd(25)))
         if (print .ge. 1) then
            call prblks('forb=fci+fq matrix',core(icd(18)),nsym,nmpsy,
     &       nmpsy,'  mo','  mo',1,nlist)
         endif
c
      endif                     ! (qfresd.or.qfresv.or.qresaf)
c
c     # replace anofqa(*) with atot(*). atot(*)= 2*( anofqa(*)+fqa(*) )
c     #  replace Ad with:
c     #  Atot(*) = Ad(*) + Qa(*)
c
      if (qfresa .or. qfresd .or. qfresv) then
         call atotal(
     &    n2mt,   qfresd,        qfresa,      qfresv,
     &    qresaf, core(icd(19)), core(icd(24)) )
         if(print .ge. 1) then
            call prblks('atotal matrix',core(icd(19)),nsym,nmpsy,nmpsy,
     &       '  mo','  mo',1,nlist)
         endif
      endif
c
c     # compute the ci orbital rotation gradient vector from the fock
c     #  matrix: wci = f_ci(pq) = 2(F(p,q) - F(q,p))
c     #  calculate the orbital rotation gradien vector from forb(*):
c     #   f_orb(pq) = 2[ forb(p,q) - forb(q,p) ]
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva
c
      icd(23) = icd(22) + atebyt(nadt)
      icd(24) = icd(23) + atebyt(naar)
      icd(25) = icd(24) + atebyt(nvdt)
      icd(26) = icd(25) + atebyt(nvat)
      itotal  = icd(26) - 1
      call memcheck(itotal,lencor,'driver 11')
c
      call wci(
     & core(icd(18)), core(icd(6)), core(icd(22)), core(icd(23)),
     & core(icd(24)), core(icd(25)) )
      if (print.ge.1) then
       call prblks('wad vector',core(icd(22)),1,nadt,1,
     &    '    ','    ',1,nlist)
       call prblks('waa vector',core(icd(23)),1,naar,1,
     &    '    ','    ',1,nlist)
       call prblks('wvd vector',core(icd(24)),1,nvdt,1,
     &    '    ','    ',1,nlist)
       call prblks('wva vector',core(icd(25)),1,nvat,1,
     &    '    ','    ',1,nlist)
      endif ! end of: if (print.ge.1) then

      call timer('wci(*) construction required',3,itimer,nlist)
c
c     # read the mcscf hamiltonian eigenvector from the mcscf
c     # restart file.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec
c
      open(unit=mcrest,file=fname(12),status='old',form='unformatted')
      call prtfn(mcrest,12,'mcscf restart file',nlist)
c
      icd(27) = icd(26) + cpt4_tot
      itotal  = icd(27) - 1
      call memcheck(itotal,lencor,'driver 12')
c
      call rdlv(mcrest,cpt4_tot,ntemp,core(icd(26)),'hvec',.true.,nlist)
c
c     # if required, compute the fcsf(*) vector from the atot(*) matrix.
c     #  calculate:
c     #  fcsf(n) = 2*<n| sum(u,v) atot(u,v)*e(uv) |mc>
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec,27:fcsf,28:ftbuf,29:tdenv,30:scr
c
c     # cpt4_tot = cpt4_tot + atebyt(ncsf_f(ist)*navst(ist))
      icd(28) = icd(27) + cpt4_tot
      icd(29) = icd(28) + atebyt(lenbft)
      itotal  = icd(29)
      call memcheck(itotal,lencor,'driver 13')
c
      do ist = 1, nst
c
         if (ncsf_f(ist) .ge. 1) then
c
            qind=ncsf_f(ist).ne.nwalk_f(ist)
c
            icd(30) = icd(29) + atebyt(3*ncsf_f(ist))
            itotal  = icd(30) - 1
            call memcheck(itotal,lencor,'driver 13A')
c
c           # core allocation table:
c           # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,
c           # 8:addmm,9:off1,10:addii,11:off2,12:addvi,13:add,14:15:
c           # 16:h1,17:cid1,18:forb,19:atot,20:Aq,21:mcd1,22:wad,
c           # 23:waa,24:wvd,25:wva,26:hmcvec,27:fcsf,28:ftbuf,
c           # 29:tdenv,30:scr
c           #  calculate:
c           #  fcsf(n) = 2*<n| sum(u,v) atot(u,v)*e(uv) |mc>
            if (nmatot.ne.0) then
               call fandt(
     &          core(icd(19)), core(icd(26)+cpt4(ist)), core(icd(30)),
     &          core(icd(28)), lenbft,                  ncsf_f(ist),
     &          core(icd(1)+cpt2(ist)),  core(icd(2)+cpt2(ist)),
     &          core(icd(3)+cpt2(ist)),  core(icd(4)+cpt3(ist)),
     &          core(icd(5)+cpt3(ist)),            qind,
     &          core(icd(9)),  core(icd(10)), core(icd(29)+cpt4(ist)),
     &          .true.,        core(icd(27)+cpt4(ist)), .false.,
     &          core(icd(30)), core(icd(30)),           navst(ist),
     &          ist)
c
               if (print.ge.1) then
                  call prtvec(nlist,'fcsf vector after fandt',
     &             core(icd(27)+cpt4(ist)),ncsf_f(ist)*navst(ist))
               endif            ! (print.ge.1)
            else
               call wzero(cpt4_tot,core(icd(27)),1)
            endif               ! (nmatot.ne.0)
c
         endif                  ! (ncsf-f(ist).gt.1)
c
      enddo ! ist=1,nst
c
      call timer('fcsf(*) construction required',3,itimer,nlist)
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec,27:fcsf,52:h1-sort,53:lastb,28:scr
c
      icd(52) = icd(27) + atebyt(cpt4_tot)
      icd(53) = icd(52) + atebyt(nnmt)
      icd(28) = icd(53) + forbyt(nbuk_md)
      itotal  = icd(28) - 1
      call memcheck(itotal,lencor,'driver 14')
c
      call dcopy_wr(nnmt,core(icd(16)),1,core(icd(52)),1)
c
      call sifcfg(2,le2e_1,nmot,0,ifmt,lenm2e_mc,n2embf_mc,ierr)
      write(nlist,6060) 'lenm2e_mc =', lenm2e_mc
      write(nlist,6060) 'n2embf_mc =', n2embf_mc
      write(nlist,*) 'ifmt,ifmtin:', ifmt,ifmtin
c
c     ###   for direct M*s open the necessery files
c     ##   open Hdiag MCfile
      if (mdir .eq. 1)
     & open(unit=hdiagf,file=fname(18),status='old',form='unformatted')
c
c     ###   for direct M*s,C*r and C*s open the necessery files
c     ##   open transformed MO integral MCfile
c
      if ((mdir .eq. 1) .or. (cdir .eq. 1)) then
c         open(unit=mcscr2,file=fname(17),status='old',
c     &    form='unformatted')
c         call prtfn(mcscr2,17,'mcscf transformed integral file',nlist)
c        # for now, open stape here.  this may change later. -rls
cfp_tmp: stape is the output file, passed through a common block
         open(unit=stape,file=fname(21),status='unknown',
     &    form='unformatted')
c
         ecore0 = zero
         avcors = lencor - icd(28) + 1
         req2   = ldamax+forbyt(numpbf(ldamax)+1)+lenbfs
         avc2is = avcors-req2
c
c   dies sind hier keine SIFS files
c
cfp_tmp: mcscr2 is itape, same as in mcscf2.f
c output stape is passed through the common block
         call mosort(
     &    core(icd(28)), core(icd(53)), avcors, avc2is,
     &    core(icd(13)), core(icd(52)), mcscr2, lenm2e_mc,
     &    n2embf_mc,     lenbuk,        nipbuk, ecore0 )
      endif                     ! ((mdir.eq.1).or.(cdir.eq.1))
c
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec,27:fcsf,52:h1-sort,53:lastb,28:orbl,29:csfl,30:core
c
      open(unit=mchess,file=fname(13),status='old',form='unformatted')
      call prtfn(mchess,13,'mcscf hessian file',nlist)
c
      icd(29) = icd(28) + atebyt(ndimr)
      icd(30) = icd(29) + atebyt(cpt4_tot)
      itotal  = icd(30) - 1
      call memcheck(itotal,lencor,'driver 15')
      avcsl = lencor - icd(30)
c
c     ##  solve the linear system of equations
c     ##  output: orbl - orbital comp. of the lambda vector
c     ##  output: csfl - csf comp. of the lambda vector
c
cfp_tmp: w (22-25) corresponds to the effective orbital gradient f^orb
c  forb (18) is the corresponding fock matrix F^orb
c  f^scf is fcsf in 27
      call solvel(
     & avcsl,         core(icd(30)),  naar,          ndimr,
     & nhblk,          emc,           core(icd(22)),
     & core(icd(27)), core(icd(26)),  core(icd(28)), core(icd(29)),
     & core(icd(1)),  core(icd(2)),   core(icd(3)),  core(icd(4)),
     & core(icd(5)),  core(icd(51)),  core(icd(52)), lenbft_md,
     & nwalk_f(1),    core(icd(13)),  core(icd(53)), nipbuk,
     & lenbuk,        core(icd(6)))
      close(unit=mchess)
      write(nlist,6060)'mcscf hessian file read and processed'
c     # for now, close these scratch files here.  this may change. -rls
      if ((mdir .eq. 1) .or. (cdir .eq. 1)) then
         close(unit=stape,  status='delete')
         close(unit=nunit2, status='delete')
      endif
c
c     #   calculate norms of orbl and csfl for debug purposes
      norm_orbl = dnrm2_wr(ndimr,core(icd(28)),1)
      norm_csfl = dnrm2_wr(cpt4_tot,core(icd(29)),1)
      call timer('solvel required',3,itimer,nlist)
c
c     # read in the mcscf 2-particle density matrix.
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec,27:fcsf,52:h1-sort,53:lastb,28:orbl,29:csfl,30:mcd2,
c     # 31:buf,32:val,33:ilab
c
      call sifo2f(mcden1, mcden2, fname(9), infomd, imcd2u, ierr)
      if (ierr.ne.0) call bummer('driver: problem opening mcden2 ierr=',
     & ierr,faterr)
      call prtfn(mcden2,9,'mcscf 2-particle density file',nlist)
c
      icd(31) = icd(30) + atebyt(niiiit)
      icd(32) = icd(31) + atebyt(lenm2e)
      icd(33) = icd(32) + atebyt(n2embf)
      icd(34) = icd(33) + forbyt(4*n2embf)
      itotal  = icd(34) - 1
      call memcheck(itotal,lencor,'driver 16')
c
      itpbin = 1
      call rdmcd2(
     & mcden2,        infomd,        itpbin,       mapivm,
     & nndx,          core(icd(9)),  core(icd(10)),core(icd(31)),
     & core(icd(32)), core(icd(33)), niiiit,       core(icd(30)))
      close(unit=mcden2)
c
c     # expand orbl(*) and perform the 1-index transformations on
c     # mcd1(*).
c     # core allocation table:
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec,27:fcsf,52:h1-sort,53:lastb,28:orbl,29:csfl,30:mcd2,
c     # 31:x,32:dii,33:dvi,34:dvv,35:diiii,36:dviii,37:dscr,38:xd
c
      icd(32) = icd(31) + atebyt(nmit)
      icd(33) = icd(32) + atebyt(nnit)
      icd(34) = icd(33) + atebyt(nvit)
      icd(35) = icd(34) + atebyt(numaf)
      icd(36) = icd(35) + atebyt(niiiit)
      icd(37) = icd(36) + atebyt(nviiit)
      icd(38) = icd(37) + atebyt(n2mt)
      icd(39) = icd(38) + atebyt(n2mt)-1
      itotal  = icd(39) - 1
      call memcheck(itotal,lencor,'driver 17')
c
      call xorbl(naar, core(icd(28)), core(icd(6)), core(icd(31)))
c
c     # performs a symmetrized one-index transformation of the
c     # one-particle density matrix.
c     #  d' = d * x  - x * d  = -( (x*d) + (x*d)(transpose) )
cfp_change: store d^L and d^l separately or construct d^L+d^l
cfp d^L stored as internal and 1-virt blocks, dvv?
      call d1tr1(
     & core(icd(21)), core(icd(31)), core(icd(32)), core(icd(33)),
     & core(icd(37)), core(icd(38)))
      if(print.ge.1)then
         call plblks('1-index transformed dii(*) matrix',
     &    core(icd(32)),nsym,nipsy,' imo',1,nlist)
         call prblks('1-index transformed dvi(*) matrix',
     &    core(icd(33)),nsym,nvpsy,nipsy,' vmo',' imo',1,nlist)
      endif
c
c     # perform a symmetrized one-index transformation of the
c     # two-particle density matrix
      call d2tr1(
     & niiiit,        nviiit,        core(icd(9)),  core(icd(10)),
     & core(icd(11)), core(icd(12)), core(icd(31)), core(icd(30)),
     & core(icd(37)), core(icd(38)), core(icd(35)), core(icd(36)))
      call timer('1-index transformation required',3,itimer,nlist)
c
c     # include the lgmx contributions to the density matrices.
c
      if(qfresd .or. qresaf .or. qfresv) then
cfp_tmp: adding dens mat. contributions?
         call dupd(
     &    nmpsy,         nsm,        n2sm,   symi,
     &    symv,          nimot,      nvmot,  iri1,
     &    iri2,          irv1,       irv2,   n2mt,
     &    mapmi,         mapmv,      mult,   core(icd(20)),
     &    core(icd(21)), numaf,      qfresv, core(icd(32)),
     &    core(icd(34)), core(icd(35)) )
         call timer('d(lgmx) update required',3,itimer,nlist)
      endif
c
c     # eliminate some extra arrays...
c     # 27:fcsf,52:h1-sort,53:lastb,28:orbl,29:csfl,30:mcd2,31:x,
c     # 32:dii,33:dvi,34:dvv,35:diiii,36:dviii,
c     # --> 27:csfl,52:h1-sort,53:lastb,28:dii,29:dvi,30:dvv,31:diiii,
c     #     32:dviii,33:
c
chl dcopy_x is a Fortran version of dcopy_wr. The ESSL dcopy_wr brea
c    + ks here,

chl most probably since target and source arrays overlap
c      call dcopy_x(cpt4_tot,core(icd(29)),1,core(icd(27)),1)
      call dcopy_x(cpt4_tot,core,icd(29),icd(27))
      icd(28)=icd(27)+atebyt(cpt4_tot)
c      call dcopy_x(nnit,core(icd(32)),1,core(icd(28)),1)
      call dcopy_x(nnit,core,icd(32),icd(28))
      icd(29)=icd(28)+atebyt(nnit)
c      call dcopy_x(nvit,core(icd(33)),1,core(icd(29)),1)
      call dcopy_x(nvit,core,icd(33),icd(29))
      icd(30)=icd(29)+atebyt(nvit)
c      call dcopy_x(numaf,core(icd(34)),1,core(icd(30)),1)
      call dcopy_x(numaf,core,icd(34),icd(30))
      icd(31)=icd(30)+atebyt(numaf)
c      call dcopy_x(niiiit,core(icd(35)),1,core(icd(31)),1)
      call dcopy_x(niiiit,core,icd(35),icd(31))
      icd(32)=icd(31)+atebyt(niiiit)
c      call dcopy_x(nviiit,core(icd(36)),1,core(icd(32)),1)
      call dcopy_x(nviiit,core,icd(36),icd(32))
      icd(33)=icd(32)+atebyt(nviiit)
c
c     # construct the transition density vectors dl1(*) and dl2(*)
c     # and add them to the 1-index transformed density matrices.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec,27:csfl,52:h1-sort,53:lastb,28:dii,29:dvi,30:dvv,
c     # 31:diiii,32:dviii,33:ftbuf,34:tdenv,35:scr
c
      icd(34) = icd(33) + atebyt(lenbft)
      icd(35) = icd(34) + atebyt(3*cpt4_tot)
      icd(36) = icd(35) + atebyt(nnit)
      itotal  = icd(36) - 1
      call memcheck(itotal,lencor,'driver 18')

c     additional changes to treat the dcopy_wr-probl
c     write(nlist,*)'itotal:'
c     write(nlist,*)(itotal)
c     write(nlist,*)'nviiit:'
c     write(nlist,*)(nviiit)
c     write(nlist,*)'lencor:'
c     write(nlist,*)(lencor)
c
c     #  calculate:
c     #   td1(uv)   =  sum(n)  vecl(n) * <n| e(uv) + e(vu) |mc>
c     #   td2(uvwx) =  (1/4) * sum(n) vecl(n) * <n| e(uvwx) + e(vuwx)
c     #                                + e(wxvu) + e(xwvu) |mc>
c
cfp_change: dl computation
c what happens exactly?
      call wzero(nnit,core(icd(35)),1)
      do ist = 1, nst
         if (ncsf_f(ist) .gt. 1) then
            qind = ncsf_f(1) .ne. nwalk_f(1)
            call fandt(
     &       core(icd(35)),  core(icd(26)+cpt4(ist)),
     &       core(icd(27)+cpt4(ist)),
     &       core(icd(33)),  lenbft,         ncsf_f(ist),
     &       core(icd(1)+cpt2(ist)),   core(icd(2)+cpt2(ist)),
     &       core(icd(3)+cpt2(ist)),   core(icd(4)+cpt3(ist)),
     &       core(icd(5)+cpt3(ist)),   qind,
     &       core(icd(9)),   core(icd(10)),  core(icd(34)),
     &       .false.,        core(icd(36)),  .true.,
     &       core(icd(35)),  core(icd(31)),  navst(ist),
     &       ist)
c
         endif                  ! (ncsf_f(ist).gt.1)
c
      enddo ! ist=1,nst
c
c     #    add some td1 contributions to the td2 matrix
      call fandt_d1(
     & core(icd(9)),  core(icd(10)),  core(icd(35)), core(icd(31)))
c
      call daxpy_wr(nnit, one, core(icd(35)), 1, core(icd(28)), 1)
c
      call timer('d1l(*) and d2l(*) required',3,itimer,nlist)
      if(print.ge.1)then
         call plblks('transition_dl(*)+dii(*) matrix',
     &    core(icd(28)),nsym,nipsy,' imo',1,nlist)
      endif                     ! (print.ge.1)
c
c     # update the cid1(*) and cid2(*) matrices with the cumulative
c     # effective density matrix elements.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:forb,19:atot,20:Aq,21:mcd1,22:wad,23:waa,24:wvd,25:wva,
c     # 26:hmcvec, 27:csfl,52:h1-sort,53:lastb,28:dii,29:dvi,30:dvv,
c     # 31:diiii,32:dviii,33:bufin,34:bufout,35:val,36:ibuf,37:mapmd1,
c     # 38:maplgv,39:mapvv,40:mapii,41:map2i,42:fgiiee,43:fgeiei
c
      icd(34) = icd(33) + atebyt(lenm2e)
      icd(35) = icd(34) + atebyt(lenm2e)
      icd(36) = icd(35) + atebyt(n2embf)
      icd(37) = icd(36) + forbyt(4*n2embf)
      itotal  = icd(37) - 1
      call memcheck(itotal,lencor,'driver 18')
c
c     # if virtual orbital resolution is required ...
c
      if (qfresv) then
         icd(38) = icd(37) + forbyt(nimot*nimot)
         icd(39) = icd(38) + forbyt(nvmot*nvmot)
         icd(40) = icd(39) + forbyt(n2mt)
         icd(41) = icd(40) + forbyt(n2mt)
         icd(42) = icd(41) + forbyt(nimot*nimot)
         icd(43) = icd(42) + forbyt(nnvt*nnit)
         icd(44) = icd(43) + forbyt(nnvt*n2it)
         itotal  = icd(44) - 1
         call memcheck(itotal,lencor,'driver 19')
c
         call add3(
     &    core(icd(37)), core(icd(38)), core(icd(41)), core(icd(39)),
     &    core(icd(40))   )
c
      else
         icd(38) = icd(37)
         icd(39) = icd(38)
         icd(40) = icd(39)
         icd(41) = icd(40)
         icd(42) = icd(41)
         icd(43) = icd(42)
         icd(44) = icd(43)
         itotal  = icd(44) - 1
         call memcheck(itotal,lencor,'driver 20')
c
      endif
c
      call d1updt(
     & qfresv,        nsym,          nsm,         nnsm,
     & iri1,          iri2,          mapmi,       irv1,
     & irv2,          mapmv,         nndx,        core(icd(28)),
     & core(icd(29)), core(icd(30)), core(icd(17))   )
      if(print .ge. 1) then
         call plblks('effective d1(*) matrix',
     &    core(icd(17)),nsym,nmpsy,'  mo',1,nlist)
      endif
c
      open(unit=effd1,file=fname(14),status='unknown',
     & form='unformatted')
      call prtfn(effd1,14,'effective 1-particle density file',nlist)

c     # copy the info(*) array from the ci density file...
c     # use infomo(*) instead as this is the dimension of the 
c     # effd basis
      do i = 1, ninfmx
c        infocg(i) = infocd(i)
         infocg(i) = infomo(i)
      enddo
c     # ...except that the output file must be split always because the
c     # 1-e arrays are computed after the 2-e arrays are written.
      infocg(1) = 2

c  
c     **** change d2updt() to use the sorted DA file instead of the
c          sequential density file. -rls ***
c     call sifo2f(ciden1, ciden2, fname(6), infocd, iucid2, ierr)
      call sifo2f(effd1, effd2, fname(15), infocg, iuefd2, ierr)
c     call d2updt(
c    & qfresv,        infocd,        iucid2,        iuefd2,
c    & core(icd(31)), core(icd(32)), core(icd(33)), core(icd(34)),
c    & core(icd(35)), core(icd(36)), nndx,          core(icd(10)),
c    & core(icd(12)), core(icd(9)),  core(icd(11)), core(icd(21)),
c    & core(icd(20)), core(icd(37)), core(icd(38)), core(icd(42)),
c    & core(icd(43)), core(icd(39)), core(icd(40)), core(icd(41)),
c    & n2embf,mapmo_fc,assume_fc)
c     call sifc2f(ciden2,infocd,ierr)

c       44: d2  45: dabuf 46: dabufv 47: idabuf 

         icd(45)=icd(44)+atebyt(nvpsg+nnmt)
         icd(46)=icd(45)+atebyt(lendar)
         icd(47)=icd(46)+atebyt(nvpbk)
         icd(48)=icd(47)+forbyt(nvpbk+1) 
         itotal  = icd(48) - 1
         call memcheck(itotal,lencor,'driver 20a')
         write(6,*) 'icd(44):',1,atebyt(nvpsg+nnmt)
         write(6,*) 'icd(45):',1,atebyt(lendar)
         write(6,*) 'icd(46):',1,atebyt(nvpbk)
         write(6,*) 'icd(47):',1,forbyt(nvpbk+1)
c  
c      directly operates on the sorted DA  2p density 
c      compared to the unsorted integral file the density
c      matrix elements are complete and doubled 
c      instead of canonical ordering  d(i,j,k,l) 
c      both d(i,j,k,l) and d(k,l,i,j) are included !
c
c     write(0,*) 'diiii=',
c    .       ddot_wr(niiiit,core(icd(31)),1,core(icd(31)),1)
c     write(0,*) 'dviii=',
c    .       ddot_wr(nviiit,core(icd(32)),1,core(icd(32)),1)
c      call daopr(dendaf)
cfp: d2update
      call daopr(dendaf)
      call d2updtnew(
     & qfresv,        infocg,  dendaf,   iuefd2,
     & core(icd(31)), core(icd(32)),core(icd(34)),
     & core(icd(35)), core(icd(36)),core(icd(10)),
     & core(icd(12)), core(icd(9)),  core(icd(11)), core(icd(21)),
     & core(icd(20)), core(icd(37)), core(icd(38)), core(icd(42)),
     & core(icd(43)), core(icd(39)), core(icd(40)), core(icd(41)),
     & n2embf,core(icd(44)),core(icd(45)),core(icd(46)),core(icd(47)),
     . nmmmmt,niiiit,nviiit,ifmtin)
      call sifc2f(effd2,infocg,ierr)
      if (ierr.ne.0) call bummer ('problem closing cid2fl, ierr = ',
     & ierr, faterr)
      call timer('cid1(*) and cid2(*) update required',3,itimer,nlist)
 
c     stop 9999
c
c     # read in the mo-coefficients from the restart file
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:cmo
c
      icd(31) = icd(30) + atebyt(nbmt)
      itotal  = icd(31) - 1
      call memcheck(itotal,lencor,'driver 21')
c
cgk
cgk This is a bug.  Should read what tran wrote to transform the
cgk aos to mos.
cgk
      call rdlv(mcrest,nbmt,nmoc,core(icd(18)),'morbl',.true.,nlist)
      close(unit=mcrest)
      if(print.ge.1)then
         call prblks('mo coefficients ',
     &    core(icd(18)),nsym,nbpsy,nmpsy,' bfn','  mo',1,nlist)
      endif
      write(nlist,6060) 'mcscf restart read'
c
c     # calculate the effno's
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,
c     # 11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:cmo,19:cnoao,20:occ,21:sa,22:sb,23:sx
c
cfp_change: keep f_orb in memory
      icd(19) = icd(18) + atebyt(nbmt)
      icd(20) = icd(19) + atebyt(nbmt)
      icd(21) = icd(20) + atebyt(nmot)
      icd(22) = icd(21) + atebyt(nbmt)
      icd(23) = icd(22) + atebyt(nbmt)
      itotal  = icd(23) - 1
      call memcheck(itotal,lencor,'driver 22')
c
      call noeff(
     & core(icd(18)), core(icd(17)), core(icd(19)), core(icd(20)),
     & core(icd(21)), core(icd(22)), core(icd(23))   )
      if(print .ge. 1) then
         write(nlist,6700)(i,core(icd(20)-1+i),i=1,nmot)
6700     format(/' effective natural orbital occupations:'
     &    /(1x,10(i3,':',f8.5)))
         call prblks('effno coefficients ',
     &    core(icd(19)),nsym,nbpsy,nmpsy,' bfn','efno',1,nlist)
      endif
c
c     # write out the effno's for property calculations
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,
c     # 11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:cmo,19:cnoao,20:occ,21:sa,22:sb,23:sx
c
      open(unit=effno,file=fname(16),status='unknown')
c
      call wrnat(core(icd(19)),core(icd(20)),nsym,nmpsy,
     &            n2sm, title   )
      call prtfn(effno,16,'effno and effocc file ',nlist)
c
      close(unit=effno)
c
c     # sort the effective density matrix elements and compute the final
c     # effective fock matrix.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,
c     # 11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:dbufv,19:idbuf,20:dabuf,21:bufin,22:ibufin,23:valin
c
      icd(19) = icd(18) + atebyt(nvpbk*nbuk)
      icd(20) = icd(19) + forbyt(nvpbk*nbuk)
      icd(21) = icd(20) + atebyt(lendar)
      icd(22) = icd(21) + atebyt(lenm2e)
      icd(23) = icd(22) + forbyt(n2embf*4)
      icd(24) = icd(23) + atebyt(n2embf)-1
      itotal  = icd(24) - 1
      call memcheck(itotal,lencor,'driver 23')
c
      call indast(dendaf)
c     call daini(dendaf)
c     sollte nicht noch einmal initialisiert werden!
c     call daini(dendaf,31,'da2scr',0,2)
*@ifdef parallel
* 9333 call ga_sync() 
*      call daclrd(dendaf)
*      call daini(dendaf,31,'da2scr',0,2)
*      call daopw(dendaf,lendar,densitysize)
*      if (par_me().gt.0) goto 9444
*@else
      call daclrd(dendaf)
      call daini(dendaf,31,'da2scr',0,0)
      call daopw(dendaf,lendar,densitysize)
*@endif

      call sifo2f(effd1, effd2, fname(15), infocg, iuefd2, ierr)
      write(nlist,6060)
     & 'beginning effective two-particle density matrix sort...'
c
      call sort1(
     & iuefd2,         infocg,        dendaf,        core(icd(7)),
     & core(icd(8)),  core(icd(18)), core(icd(19)), core(icd(20)),
     & core(icd(21)), core(icd(22)), core(icd(23)), mapmo_fc,
     . 0,core(icd(17)),nmot,nadcalc)

      call daclw(dendaf)
      call sifc2f(effd2,infocg,ierr)
      if ( ierr .ne. 0 ) call bummer(
     & 'driver: effd2, from sifc2f, ierr=', ierr, faterr)
      call timer('effective 2-particle density sort required',
     & 3,itimer,nlist)
c
c    # construct the 1-e contributions to the effective fock matrix.
c    # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c    # 9:off1,10:addii,
c    # 11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c    # 18:fci,19:scr1,20:scr2
c
      ifeff=.true.
c
      icd(19) = icd(18) + n2mt
      icd(20) = icd(19) + n2mt
      icd(21) = icd(20) + n2mt
      itotal  = icd(21) - 1
      call memcheck(itotal,lencor,'driver 24')
c
      call wzero(n2mt,core(icd(18)),1)
      call fockm1(
     & core(icd(18)), core(icd(16)), core(icd(17)), core(icd(19)),
     & core(icd(20)), ifeff   )
      if(print .ge. 1 ) then
         call prblks('effective fock matrix 1-el. contribution',
     &    core(icd(18)),nsym,nmpsy,nmpsy,'  mo','  mo',1,nlist)
      endif
c
c     # construct the 2-e contributions to the ci fock matrix.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,
c     # 11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:dabufv,20:idabuf,21:dabuf,22:h2,23:d2,
c     # 24:scrh,25:scrd,26:scrf
c
      icd(20) = icd(19) + atebyt(nvpbk)
      icd(21) = icd(20) + forbyt(nvpbk)
      icd(22) = icd(21) + atebyt(lendar)
      icd(23) = icd(22) + atebyt(nvpsg+nnmt)
      icd(24) = icd(23) + atebyt(nvpsg+nnmt)
      icd(25) = icd(24) + atebyt(n2mt)
      icd(26) = icd(25) + atebyt(n2mt)
      icd(27) = icd(26) + atebyt(n2mt)
      itotal  = icd(27) - 1
      call memcheck(itotal,lencor,'driver 25')
c
      call daopr(dendaf)
cfp_change: separate contributions of the lambda matrix. add together
c forb+fl+fL
      call fockm2(
     & nmmmmt,         core(icd(22)),  core(icd(23)),  core(icd(21)),
     & core(icd(20)),  core(icd(19)),  core(icd(18)),  core(icd(24)),
     & core(icd(25)),  core(icd(26)),  intdaf,         dendaf,
     & ifeff)
      call timer('feff(*) construction required',3,itimer,nlist)
      if(print .ge. 1) then
         call prblks('effective fock matrix',
     &    core(icd(18)),nsym,nmpsy,nmpsy,'  mo','  mo',1,nlist)
      endif                     ! (print.ge.1)
      inquire(unit=30,opened=used)
*@ifndef noclose
c     # unit= bug fixed 13-aug-01.  -rls
      inquire(unit=daunit(dendaf),opened=qopen,iostat=ierr)
      if (qopen .and. ierr.eq.0) call daclr(dendaf)
      inquire(unit=daunit(intdaf),opened=qopen,iostat=ierr)
      if (qopen .and. ierr.eq.0) call daclr(intdaf)
*@endif
*@ifdef parallel
* 9444    call ga_sync()
*         call daclrd(dendaf)
*         call daclrd(intdaf)
*         if (par_me().gt.0) return
*@else
         call daclrd(dendaf)
         call daclrd(intdaf)
*@endif 
c
c     # calculate some quantities useful for debugging purposes
c
      call ciener (mcenrg(1),norm_orbl,norm_csfl,energy,lheuristic)

c
c     # check if the effective fock matrix is symmetric, return
c     # the lower tiangle
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,
c     # 11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:sfci,
c
      call ftest(core(icd(18)),core(icd(19)),nmpsy,nsym)
c
c     # write out the effective d1(*) matrix and the generalized f(*)
c     # matrix.
c     # 1:xbar,2:xp,3:z,4:r,5:ind,51:modrt,6:iorder,7:offmm,8:addmm,
c     # 9:off1,10:addii,11:off2,12:addvi,13:add,14:15:16:h1,17:cid1,
c     # 18:fci,19:sfci,20:cires,21:buf,22:ilab,23:val
c
      call prtfn(effd1,14,'effective 1-particle density file',nlist)
c
      icd(20) = icd(19) + atebyt(nnmt)
      icd(21) = icd(20) + forbyt(nmot)
      icd(22) = icd(21) + atebyt(lenm1e)
      icd(23) = icd(22) + forbyt(2*n1embf)
      icd(24) = icd(23) + atebyt(n1embf)
      itotal  = icd(24) - 1
      call memcheck(itotal,lencor,'driver 26')
      call wzero(forbyt(nmot),core(icd(20)),1)
      if (nadcalc.eq.0) then 
      call wthmcd(
     & effd1,  ntitle, title,         infocg,
     & energy, ienergy, 1,        nsym,
     & nmpsy,  nmpsy,  zero,          nmot,
     & nmot,   mapmm,  core(icd(20)), orblab,
     & slabel  )
      else
      call wthmcd(
     & effd1,  ntitle, title,         infocg,
     & mcenrg, mcietp, nenrgy,        nsym,
     & nmpsy,  nmpsy,  zero,          nmot,
     & nmot,   mapmm,  core(icd(20)), orblab,
     & slabel  )
       endif
c
      call wtd1f(
     & effd1,          infocg,         core(icd(21)),    core(icd(22)),
     & core(icd(23)),  core(icd(17)),  core(icd(19)),    nsym,
     & irm1,           irm2,           mapmm, ifmtin)
      close(unit=effd1)
      call timer('output of d1eff(*) and feff(*) required',
     & 4,itimer,nlist)
c
c  all done!!!  the mrci gradient may be computed using the effective
c  density matrices and fock matrix.
c
      call timer('cigrd required',4,icigrd,nlist)
      return
6060  format(1x,a,i6)
6070  format(1x,a,L2)
      end
c
c***********************************************************************
c
      block data
c
      implicit integer(a-z)
c
c     # ldamin=minimum da record length for integral sorting.
c     # ldamax=maximum da record length.
c     # ldainc=size increment for da record length
      common/clda_mc/ldamin_mc,ldamax_mc,ldainc_mc
      data ldamin_mc/127/, ldamax_mc/4095/, ldainc_mc/64/
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      common/da2/nunit2,len2,irec2
      data nunit2 /62/
c
      common/cbufs/lenbfs,h2bpt
      data lenbfs/32767/
c
c     # permutation array for 1-particle density matrix indices.
      common/ccone/perm1(2,2),ind1(2),inds1,npass1
      data perm1/1,2, 2,1/
c
c     # permutation array for 2-particle density matrix indices.
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
      data perm/1,2,3,4, 2,1,3,4, 3,4,1,2, 4,3,1,2/
c
c     # temporary da file record length paramaters.
      common/cldain/ldamin,ldamax,ldainc
      data ldamin/511/, ldamax/4095/, ldainc/64/
c
      common/cfinfo/lenm2e_mc,n2embf_mc,lena1e,n1eabf,lena2e,n2eabf,
     &              lenbft_mc,lend2e,n2edbf,lend1e,n1edbf
c
c     # default values (max. 255 basis functions)
      data lend1e/4096/, n1edbf/3272/
      data lend2e/4096/, n2edbf/2730/
      data lena1e/4096/, n1eabf/3272/
      data lena2e/4096/, n2eabf/2730/
ctm
      data lenbft_mc/2047/
c

c     # transformed integral file info:
      common/ctmofi/lenm1e,n1embf,lenm2e,n2embf
      data lenm1e/2047/, n1embf/1636/, lenm2e/2047/, n2embf/1364/

c     # mcscf formula file buffer length:
      common/cmfti/lenbft
      data lenbft/2047/
c
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      character*60 fname
      common/cfname/fname(nfilmx)
c
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
      equivalence (iunits(3),cidrt)
      equivalence (iunits(4),moints)
      equivalence (iunits(5),ciden1)
      equivalence (iunits(6),ciden2)
      equivalence (iunits(7),mcdrt)
      equivalence (iunits(8),mcden1)
      equivalence (iunits(9),mcden2)
      equivalence (iunits(10),ndrt)
      equivalence (iunits(11),civoutfl)
      equivalence (iunits(12),mcrest)
      equivalence (iunits(13),mchess)
      equivalence (iunits(14),effd1)
      equivalence (iunits(15),effd2)
      equivalence (iunits(16),effno)
      equivalence (iunits(17),mcscr2)
      equivalence (iunits(18),hdiagf)
      equivalence (iunits(19),mcscfin)
      equivalence (iunits(20),moints2)
      equivalence (iunits(21),stape)
      equivalence (iunits(22),d1atrfl)
      equivalence (iunits(23),nft)
      equivalence (iunits(24),ndft)
      equivalence (iunits(25),file25)
      equivalence (iunits(26),file26)
      equivalence (iunits(27),file27)
      equivalence (iunits(28),file28)
      equivalence (iunits(29),file29)
      equivalence (iunits(30),file30)
      equivalence (iunits(31),cidrtflci)
      data file25/0/
c
c     # temporary da file info. (should be consistent with dawrt().
c
      parameter(ndafmx=2)
      common/cdafls/daunit(ndafmx)
      character*60 dafnam
      common/cdanms/dafnam(ndafmx)
c
c     # default file unit numbers and filenames:
c
c     # listing file.
      data nlist/6/, fname(1)/'cigrdls'/
c
c     # input file.
      data nin/5/, fname(2)/'cigrdin'/
c
c     # cidrt file.
      data cidrt/10/, fname(3)/'cidrtfl'/
c
c     # transformed integral file.
      data moints/11/, fname(4)/'moints'/
c
c     # ci one-particle density matrix file.
      data ciden1/12/, fname(5)/'cid1fl'/
c
c     # ci two-particle density matrix file.
      data ciden2/13/, fname(6)/'cid2fl'/
c
c     # mcscf drt file.
      data mcdrt/14/, fname(7)/'mcdrtfl'/
c
c     # mcscf one-particle density matrix file.
      data mcden1/15/, fname(8)/'mcd1fl'/
c
c     # mcscf two-particle density matrix file.
      data mcden2/16/, fname(9)/'mcd2fl'/
c
c     # mcscf diagonal formula file.
      data ndrt/17/ fname(10)/'mcdrtfl.*'/
c
c
c     # mcscf restart file.
      data mcrest/19/, fname(12)/'restart'/
c
c     # mcscf hessian file.
      data mchess/20/, fname(13)/'mchess'/
c
c     # output effective 1-particle density matrix file.
      data effd1/21/, fname(14)/'effd1fl'/
c
c     # output effective 2-particle density matrix file.
      data effd2/22/, fname(15)/'effd2fl'/
c
c     # output effective natural orbital file.
      data effno/23/, fname(16)/'nocoef_cigrd'/
cvp   data fname(16)/'effno'/
c
c     # transformed mcscf mo integrals.
      data mcscr2/50/, fname(17)/'mcscr2'/
c
c     # Hdiag MCfile.
      data hdiagf/51/, fname(18)/'hdiagf'/
c
c     # mcscf input file.
      data mcscfin/24/, fname(19)/'mcscfin'/
c
c     # transformed two-electron repulsionintegral file.
      data moints2/25/, fname(20)/'moints2'/
c
c     # sorted mcscf integral file (temporary).
      data stape  /63/, fname(21)/'mciscr'/
c
c     # antisymmetric 1-e CI denisty file
      data d1atrfl/33/, fname(22)/'cid1trfl'/
c
c     # diagonal MCSCF formula tape
      data ndft  /34/, fname(23)/'mcdftfl.*'/
c
c     # off-diagonal MCSCF formula tape
      data nft   /35/, fname(24)/'mcoftfl.*'/
c
c     # temporary direct access file units and names.
      data daunit/30,31/
      data dafnam/'da1scr','da2scr'/
c
c     # civout file for state1 and state2 in case of DSCF calculation
      data civoutfl/36/, fname(11)/'civout.xx'/

c     # cidrtfl.ci file frozen core support 
      data cidrtflci/40/, fname(31)/'cidrtfl.ci'/

c
      end
c
c*********************************************************************
c   decod2 routine from mcscf!

      subroutine decod2(lenbuf,buf,nipbuf,val,nipv,ilab,num,last)
c
c  decode a packed buffer of two-electron integrals.
c
c  input:
c  lenbuf = io buffer length.
c  buf(*) = io buffer.
c  nipbuf = maximum number of values in the record.
c  nipv = number of integer labels per value (=0, 1, 2, or 4).
c
c  output:
c  val(1:num) = integral values (only if nipv.ne.0).
c  ilab(1:nipv,1:num) = unpacked labels (only if nipv.ne.0).
c  num = number of values returned.
c  last = integer code indicating record status.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 buf(lenbuf),val(nipbuf)
      integer ilab(*)
c
      integer iunpck(4)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
cmd
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(1),nbft)
cmd
c
c  unpack buffer info from the last word.
c
      nuw=4
      call ulab16(buf(lenbuf),iunpck,nuw)
      last=iunpck(3)
      num=iunpck(4)
c
      if(nipv.ne.0)then
c
c  move/unpack the values.
c
         call dcopy_wr(num,buf,1,val,1)
c
c  move/unpack the labels.
c
         if(nipv.eq.1)then
            nuw=num
            call ulab32(buf(nipbuf+1),ilab,nuw)
         elseif(nipv.eq.2)then
            nuw=2*num
            call ulab16(buf(nipbuf+1),ilab,nuw)
         elseif(nipv.eq.4)then
            nuw=4*num
cmd
c       to be able to unpack integer labels bigger the 255 use ulab16
            if (nbft.le.255) then
               call ulab8(buf(nipbuf+1),ilab,nuw)
            else
               call ulab16(buf(nipbuf+1),ilab,nuw)
            endif
cmd
         else
            call bummer('rdlab2: nipv=',nipv,faterr)
         endif
      endif
c
      return
      end

c deck diaden
      subroutine diaden(np,u,occ,d1,sa,sx)
c
c  compute the transformation to diagonalize the one-particle
c  density matrix
c
c  arguments:
c  np     = number of active orbitals.
c  u(*)   = natural orbitals in the mo-basis
c  occ(*) = natural orbital occupations.
c  d1(*)  = density matrix.
c  sa(*)  = scratch matrix.
c  sx(*)  = scratch vector.
c
c modified mcscf notran (m.e.)
c
       implicit none
      integer mp
      integer np
      integer p
      integer pq, pq2
      integer q
c
      real*8 u(np,np),occ(np),d1(*),sa(*),sx(*)
c
c  diagonalize the d1(*) matrix.
c  only the lower triangle is required in the eispac call.
c
      pq=0
      do 110 p=1,np
        do 120 q=1,p
          pq=pq+1
          pq2=(q-1)*np+p
          sa(pq2)=d1(pq)
120     continue
110   continue
c
c  put the eigenvectors in sa(*), eigenvalues in sx(*), and
c  use occ(*) for scratch.
c
      call eigip(np,np,sa,sx,occ)
c
c  compute the occ(*) vector.
c
      do 130 p=1,np
        occ(p)=sx(np+1-p)
130   continue
c
c  copy the vectors back into u(*) (in reverse order).
c
      pq2=0
      do 150 p=1,np
        mp=np+1-p
        do 140 q=1,np
          pq2=pq2+1
          u(q,mp)=sa(pq2)
140     continue
150   continue
c
      return
c
      end
c deck dodupd
      subroutine dodupd(nmpsy,nsm,n2sm,symi,nimot,iri1,iri2,
     & mapmi,mult,ado,dmc,
     & dii,diiii)
c
c  update the effective density matrices with the
c  type-i d.o. contributions:
c
c  d(uv;do) = 2 * ado(uv)
c
c  d(uvwx;do) =     2*ado(uv)*dmc(wx) +   2*ado(wx)*dmc(uv)
c               -half*ado(uw)*dmc(vx) -half*ado(vx)*dmc(uw)
c               -half*ado(ux)*dmc(vw) -half*ado(vw)*dmc(ux)
c
c  input:
c  ado(*) = symmetric ado(*) matrix, square blocked, indexed by mo's.
c  dmc(*) = 1-particle mcscf density matrix, square blocked,
c           indexed by mo's.
c  dii(*) = effective 1-particle density, lower-triangular-blocked,
c           indexed by internal orbitals.
c  diiii(*) = effective 2-particle density matrix,
c             canonical-symmetry-blocked, indexed by internal orbitals.
c  mapmi(*) = internal to mo mapping vector.
c  symi(*) = internal orbital symmetry.
c  nimot = total number of internal orbitals.
c  iri1(*),iri2(*) = internal orbital symmetry block ranges.
c  nsm(*),n2sm(*),nmpsy(*) = mo symmetry arrays.
c
c  output:
c  dii(*),diiii(*) = updated density matrices.
c
c  written by ron shepard
c  version: 1-jan-89
c
       implicit none
      real*8    two,      half
      parameter(two=2.d0, half=.5d0)
c
      integer iri1(*),iri2(*)
      integer mapmi(*), mult(8,8)
      integer nsm(*),n2sm(*),nmpsy(*), nimot
      integer symi(*)
      integer u, u20, usym, uv, uvsym, uvwx, uw, uwsym, ux
      integer v, v20, vsym, vw, vwsym, vx
      integer w, w20, wsym, wy, wx
      integer x, x2, xsym
c
      real*8 ado(*),dmc(*),dii(*),diiii(*)
c
c  effective 1-particle density contributions...
c
      uv=0
      do 120 u=1,nimot
         usym=symi(u)
         u20=n2sm(usym)+(mapmi(u)-nsm(usym)-1)*nmpsy(usym)-nsm(usym)
         do 110 v=iri1(usym),u
            uv=uv+1
            dii(uv)=dii(uv)+two*ado(u20+mapmi(v))
110      continue
120   continue
c
c  effective 2-particle density contributions...
c
      uvwx=0
      do 240 u=1,nimot
         usym=symi(u)
         u20=n2sm(usym)+(mapmi(u)-nsm(usym)-1)*nmpsy(usym)-nsm(usym)
         do 230 v=1,u
            vsym=symi(v)
            uvsym=mult(vsym,usym)
            uv=u20+mapmi(v)
            v20=n2sm(vsym)+(mapmi(v)-nsm(vsym)-1)*nmpsy(vsym)-nsm(vsym)
            do 220 w=1,u
               wsym=symi(w)
               uwsym=mult(wsym,usym)
               vwsym=mult(wsym,vsym)
               xsym=mult(wsym,uvsym)
               uw=u20+mapmi(w)
               vw=v20+mapmi(w)
               w20=n2sm(wsym)+
     &          (mapmi(w)-nsm(wsym)-1)*nmpsy(wsym)-nsm(wsym)
               if(w.eq.u)then
                  x2=min(iri2(xsym),v)
               else
                  x2=min(iri2(xsym),w)
               endif
               do 210 x=iri1(xsym),x2
                  uvwx=uvwx+1
                  ux=u20+mapmi(x)
                  vx=v20+mapmi(x)
                  wx=w20+mapmi(x)
                  if(uvsym.eq.1)diiii(uvwx)=diiii(uvwx)
     &             + two * ( ado(uv)*dmc(wx) + ado(wx)*dmc(uv) )
                  if(uwsym.eq.1)diiii(uvwx)=diiii(uvwx)
     &             -half * ( ado(uw)*dmc(vx) + ado(vx)*dmc(uw) )
                  if(vwsym.eq.1)diiii(uvwx)=diiii(uvwx)
     &             -half * ( ado(ux)*dmc(vw) + ado(vw)*dmc(ux) )
210            continue
220         continue
230      continue
240   continue
c
      return
      end
c deck dtodii
      subroutine dtodii(u0,p0,nu,np,mapmi,d1,dii)
c
c  copy a block of the internal-internal d1(*) elements to dii(*)
c
c  input:
c  u0 = mo offset.
c  p0 = internal orbital offset.
c  nu = number of mo's.
c  np = number of internal orbitals.
c  mapmi(*) = internal-to-mo mapping vector
c  d1(*) = square mo matrix.
c
c  output:
c  dii(*) = square internal orbital matrix.
c
       implicit none
      integer mapmi(*), mp, mq
      integer np, nu
      integer p, p0
      integer q
      integer u0
c
      real*8 d1(nu,nu),dii(np,np)
c
      do 20 p=1,np
         mp=mapmi(p+p0)-u0
         do 10 q=1,(p-1)
            mq=mapmi(q+p0)-u0
            dii(p,q)=d1(mp,mq)
            dii(q,p)=d1(mq,mp)
10       continue
         dii(p,p)=d1(mp,mp)
20    continue
c
      return
      end
c deck dupd
      subroutine dupd(nmpsy,nsm,n2sm,symi,symv,nimot,
     &                nvmot,iri1,iri2,irv1,irv2,n2mt,
     &                mapmi,mapmv,mult,lgmx,dmc,numaf,
     &                qfresv,dii,dvv,diiii)
c
c  update the effective density matrices with the
c  lgm contributions:
c
c  d(uv) = 2 * lgmx(uv)
c
c  d(uvwx) =     2*lgmx(uv)*dmc(wx) +   2*lgmx(wx)*dmc(uv)
c               -half*lgmx(uw)*dmc(vx) -half*lgmx(vx)*dmc(uw)
c               -half*lgmx(ux)*dmc(vw) -half*lgmx(vw)*dmc(ux)
c
c  input:
c  lgmx(*) = symmetric lagrangesche multiplier matrix,
c            square blocked, indexed by mo's.
c  dmc(*) = 1-particle mcscf density matrix, square blocked,
c           indexed by mo's.
c  dii(*) = effective 1-particle density, lower-triangular-blocked,
c           indexed by internal orbitals.
c  diiii(*) = effective 2-particle density matrix,
c             canonical-symmetry-blocked, indexed by internal orbitals.
c  mapmi(*),mapmv(*) = internal/virtual to mo mapping vector.
c  symi(*),symv(*) = internal/virtual orbital symmetry.
c  nimot,nvmot = total number of internal/virtual orbitals.
c  iri1(*),iri2(*),irv1(*),irv2(*) = internal/virtual orbital
c                                    symmetry block ranges.
c  nsm(*),n2sm(*),nmpsy(*) = mo symmetry arrays.
c  n2mt = number of mo coeficients
c  numaf = size of the dvv array
c  qfresv = flag if virtual orbital resolution is required
c
c  output:
c  dvv (*)= vv density matrix
c  dii(*),diiii(*) = updated density matrices.
c
c  version log:
c    jun-90 modified to construct the dvv matrix (m.e.)
c  1-jan-89 written by ron shepard
c
       implicit none
      integer iri1(*),iri2(*),irv1(*),irv2(*)
      integer mapmi(*),mapmv(*)
      integer mult(8,8),nsm(*),n2sm(*),nmpsy(*), n2mt, nimot,
     &        numaf, nvmot
      integer symi(*),symv(*)
      integer u, u20, usym, uv, uvsym, uvwx, uw, uwsym, ux
      integer v, v20, vsym, vw, vwsym, vx
      integer w, w20, wsym, wx
      integer x, x2, xsym
c
      logical qfresv
c
      real*8 lgmx(*),dmc(*),dii(*),dvv(*),diiii(*)
c
      real*8    zero,     two,      half
      parameter(zero=0.d0,two=2.d0, half=.5d0)
c
c  effective 1-particle density contributions...
c
      uv=0
      do 120 u=1,nimot
         usym=symi(u)
         u20=n2sm(usym)+(mapmi(u)-nsm(usym)-1)*nmpsy(usym)-nsm(usym)
         do 110 v=iri1(usym),u
            uv=uv+1
            dii(uv)=dii(uv)+two*lgmx(u20+mapmi(v))
110      continue
120   continue
c
      if (qfresv) then
ctm      call scopy(numaf,zero,0,dvv,1)
         call wzero(numaf,dvv,1)
         wx=0
         do 140 w=1,nvmot
            wsym=symv(w)
            w20=n2sm(wsym)+(mapmv(w)-nsm(wsym)-1)*nmpsy(wsym)-nsm(wsym)
            do 130 x=irv1(wsym),w
               wx=wx+1
               dvv(wx)=two*lgmx(w20+mapmv(x))
130         continue
140      continue
      endif
c
c  effective 2-particle density contributions...
c
      uvwx=0
      do 240 u=1,nimot
         usym=symi(u)
         u20=n2sm(usym)+(mapmi(u)-nsm(usym)-1)*nmpsy(usym)-nsm(usym)
         do 230 v=1,u
            vsym=symi(v)
            uvsym=mult(vsym,usym)
            uv=u20+mapmi(v)
            v20=n2sm(vsym)+(mapmi(v)-nsm(vsym)-1)*nmpsy(vsym)-nsm(vsym)
            do 220 w=1,u
               wsym=symi(w)
               uwsym=mult(wsym,usym)
               vwsym=mult(wsym,vsym)
               xsym=mult(wsym,uvsym)
               uw=u20+mapmi(w)
               vw=v20+mapmi(w)
               w20=n2sm(wsym)+
     &          (mapmi(w)-nsm(wsym)-1)*nmpsy(wsym)-nsm(wsym)
               if(w.eq.u)then
                  x2=min(iri2(xsym),v)
               else
                  x2=min(iri2(xsym),w)
               endif
               do 210 x=iri1(xsym),x2
                  uvwx=uvwx+1
                  ux=u20+mapmi(x)
                  vx=v20+mapmi(x)
                  wx=w20+mapmi(x)
                  if(uvsym.eq.1)diiii(uvwx)=diiii(uvwx)
     &             + two * ( lgmx(uv)*dmc(wx) + lgmx(wx)*dmc(uv) )
                  if(uwsym.eq.1)diiii(uvwx)=diiii(uvwx)
     &             -half * ( lgmx(uw)*dmc(vx) + lgmx(vx)*dmc(uw) )
                  if(vwsym.eq.1)diiii(uvwx)=diiii(uvwx)
     &             -half * ( lgmx(ux)*dmc(vw) + lgmx(vw)*dmc(ux) )
210            continue
220         continue
230      continue
240   continue
c
      return
      end
c deck dxyxy
      subroutine dxyxy(b,d,nxmot,nxpsy,nypsy,mapmx,symm)
c
c  copy the diagonal elements from a diagonal hessian matrix block
c  in b(*) to the vector d(*).  the hessian elements are assumed
c  to be referenced as:
c
c  [b(xy,xy)] = offxx(xx) + [(y,y)]
c
c  this addressing method is used for the adad, vdvd, and vava blocks
c  of the hessian matrix where xy=ad, dv, and av orbital types.
c  see the mcscf program for more details.
c
c  written by ron shepard.
c
       implicit none
      integer ib, isym, ix
      integer jsym, jx
      integer nxpsy(*),nypsy(*),mapmx(*), nxmot, ny, nyp1
      integer symm(*)
      integer yx
c
      real*8 b(*),d(*)
c
      yx=1
      ib=1
      do 200 ix=1,nxmot
         isym=symm(mapmx(ix))
         ny=nypsy(isym)
         if(ny.eq.0)go to 200
c
c        skip over the off-diagonal blocks.
c
         do 100 jx=1,(ix-1)
            jsym=symm(mapmx(jx))
            ib=ib+ny*nypsy(jsym)
100      continue
c
c        copy the diagonal elements from the diagonal block.
c
         nyp1=ny+1
         call dcopy_wr(ny,b(ib),nyp1,d(yx),1)
         yx=yx+ny
         ib=ib+ny*ny
200   continue
c
      return
      end
c deck eigip
      subroutine eigip(nr,n,a,d,e)
c
c  in-place computation of the eigenvectors and eigenvalues
c  of the matrix a(*).
c
c  input:
c  nr   = number of rows in a(*) (nr.ge.n).
c  n    = dimension of the matrix a(*) and vector d(*).
c  a(*) = matrix to be diagonalized
c  e(*) = scratch vector of length n.
c
c  output:
c  a(*) = eigenvectors.
c  d(*) = eigenvalues.
c
c  this version uses modified eispac routines to perform the
c  diagonalization.
c  only the lower-triangle of a(*) needs to be available on input.
c  eigenvectors are ordered: d(1) < d(2) < d(3) ... < d(n)
c
c  written by ron shepard.
c
       implicit none
      integer nfilmx
      parameter(nfilmx=30)
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer ierr
      integer n, nr
c
      real*8 a(*),d(*),e(*)
c
c  check input.
c
      if(n.le.0 .or. nr.lt.n)then
         write(nlist,6010)'nr,n=',nr,n
         stop
      endif
c
c  diagonalize.
c
*@if defined( cray) || defined ( fps)
*      call tred2(nr,n,a,d,e,a)
*@else
      call tred2i(nr,n,a,d,e)
*@endif
      call tql2(nr,n,d,e,a,ierr)
      if(ierr.ne.0)then
         write(nlist,6010)'ierr=',ierr
         stop
      endif
c
      return
6010  format(/' *** error *** eigip: ',a,2i10)
      end
c deck faar
      subroutine faar(naar,iorder,iwop)
c
c  find the allowed active-active rotations.  check the
c  mcres(*) array for invariant subspace specifications.
c
c  input:
c  mcres(i) = has nonzero decimal digits in the fields
c             corresponding to the subspaces to which orbital i belongs.
c
c  output:
c  iorder(pq) = nonzero for allowed (essential) active-active rotations
c             = zero for disallowed (redundant) rotations.
c  iwop(*,r)  contains the p, q indices for allowed rotation number r.
c  naar       is the number of allowed active-active rotations.
c
c  example:
c    input:
c    mcres(1)=11  ;in this example rotations between orbitals (2,1)
c    mcres(2)=10  ;and (3,1) are disallowed while the rotation (3,2)
c    mcres(3)=01  ;is allowed. all rotations (4,*) are allowed. note
c    mcres(4)=00  ;that usually the subspaces involve disjoint sets
c                  ;of orbitals.
c    output:
c    iorder(*)=0, 0,0, 0,1,0, 2,3,4,0
c    iwop(*,*)=3,2, 4,1, 4,2, 4,3
c    naar=4
c
c  5-may-88 modified to check for consistent orbital subspace
c      resolution (rls).
c  written by ron shepard.
c
       implicit none
      integer nfilmx
      parameter(nfilmx=30)
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer nmomx
      parameter(nmomx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer namot,ira1(8),ira2(8)
      equivalence (nxtot(10),namot)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer iorder(*),iwop(2,*),i
      integer j
      integer naarp,naar,nnact
      integer mq,mp,pmask
      integer pfield,pq,psym,p
      integer qfield,qmask,q
c
      logical error
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      error=.false.
      nnact=nndx(namot+1)
      call izero_wr(nnact,iorder,1)
c
      naar=0
      naarp=0
      do 300 psym=1,nsym
c
         do 200 p=(ira1(psym)+1),ira2(psym)
            mp=mapma(p)
            pmask=mcres(mp)
            do 100 q=ira1(psym),(p-1)
               naarp=naarp+1
               mq=mapma(q)
               qmask=mcres(mq)
c
c  check fields in the masks for nonzero coincidences.
c
               call gtrfld(pmask,qmask,pfield,qfield)
               if(pfield.eq.0 .and. qfield.eq.0)then
c                 ...essential mcscf orbital rotation.
                  naar=naar+1
                  iwop(1,naar)=mp
                  iwop(2,naar)=mq
                  pq=nndx(p)+q
                  iorder(pq)=naar
               elseif(pfield.ne.qfield)then
c                 ...inconsistent resolution has been specified.
                  error=.true.
                  write(nlist,6020)mp,pmask,mq,pmask
               endif
100         continue
200      continue
300   continue
c
      write(nlist,6010)naar,naarp,(j,(iwop(i,j),i=1,2),j=1,naar)
      if(error)call bummer
     & ('faar: inconsistent orbital subspace resolution',0,faterr)
c
6010  format(/' faar:',i4,
     & ' active-active rotations allowed out of:',i4,' possible.'/
     & (1x,5(i4,':(',i3,',',i3,')')))
6020  format(' inconsistent orbital subspace resolution.'/
     & ' p=',i3,' pmask=',i8.8/' q=',i3,' qmask=',i8.8)
      return
      end
c
c***************************************************************
c
c deck fandt
      subroutine fandt(
     & atot,      hvect,     vecl,
     & ftbuf,     lenbft,    ncsfm,
     & xbar,      xp,        z,
     & r,         ind,       qind,
     & off1,      addii,     tdenv,
     & qfflag,    fvec,      qtflag,
     & td1,       td2,       navst_act,
     & ist)
c
c  compute the matrix-vector product of the symmetric one-electron
c  operator atot(*) within the mcscf expansion csf space
c
c          fvec(n) = 2*<n| sum(u,v) atot(u,v)*e(uv) |mc>
c
c  and/or update the internal-internal and internal-vituell density
c  matrix elemets with the symmetrized transition density matrix
c  elements:
c
c          td1(uv)   =  sum(n)  vecl(n) * <n| e(uv) + e(vu) |mc>
c          td2(uvwx) =  (1/4) * sum(n) vecl(n) * <n| e(uvwx) + e(vuwx)
c                              + e(uvxw) + e(vuxw) + e(wxuv) + e(xwuv)
c                                        + e(wxvu) + e(xwvu) |mc>
c
c  input:
c  atot(*) = symmetric squared symmetry-packed operator matrix, indexed
c            by mo's.
c  hvect(*) = mcscf hamiltonian eigenvector.
c  vecl(*) = transition lambda vector.
c  ftbuf(*) = mcscf formula file buffer.
c  lenbft = formula file buffer length.
c  ncsfm = mcscf expansion length.
c  xbar,xp,z,r,ind = mcscf drt arrays.
c  qind = true if ind(*) is required and false if ind(i)=i.
c  off1(*),addii(*) = occupied orbital addressing arrays.
c  tdenv(*) = work vector for transition density vector construction.
c  qfflag = flag for f(*) vector construction.
c  qtflag = flag for transition density construction.
c  td1(*),td2(*) = initial transtion density elements, indexed by
c                  internal orbitals.
c
c  output:
c  fvec(*) = output fvec(*) vector (only if qfflag=true).
c  td1(*),td2(*) = updated transition density elements (only if
c                  qtflag=true).
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mcuft for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2       11ab        eiill, eilli
c   3       11a         eiill
c   4       11b         eilil
c
c   5       14ab        half*ellll, ell
c   6       14a         half*ellll
c   7       14b         ell
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      iiil, illl, il
c   3           2a'b'      ijlk, iklj
c   4           1ab        ijkl, ilkj
c   5           3ab        ikjl, iljk
c   6           4ab        iikl, ilki
c               8ab        ijll, illj
c   7           6a'b'      ikkl, ilkk
c   8           12ac       iiil, il
c   9           12bc       illl, il
c  10           1a         ijkl
c               2a'        ijlk
c               6a'        ikkl
c               7'         iklk
c  11           4a         iikl
c               8a         ijll
c  12           2b'        iklj
c               3a         ikjl
c               4b         ilki
c               4b'        ikli
c               5          ilik
c               8b         illj
c               10         iljl
c  13           11b        illi
c               13         half*ilil
c  14           1b         ilkj
c               3b         iljk
c  15           6b'        ilkk
c               12c        il
c==================================================
c  written by ron shepard.
c  17-may-88 fvec(*) vector construction added.
c  17-jul-87 adapted from mcscf routine rdft,
c            symmetry ft version: 18-oct-84.
c
       implicit none
c  ## common & parameter section
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nfilmx
      parameter(nfilmx=30)
      integer iunits,nlist,nft,ndft
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
      equivalence (iunits(23),nft)
      equivalence (iunits(24),ndft)
c
      integer nmomx
      parameter(nmomx=1023)
c
      real*8    zero,     two,     four,     half,     small
      parameter(zero=0.d0,two=2.d0,four=4.d0,half=.5d0,small=1d-10)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer n2sm(8),nmpsy(8),n2mt,nsm(8),ndmot,namot,ira1(8)
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxtot(6),n2mt)
      equivalence (nxtot(10),namot)
      equivalence (nxtot(7),ndmot)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
c      include 'cmomap.inc/list'
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)

      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)

c
c  ##   integer section
c
      integer addii(*)
      integer ncsfm,ncsfm2,ncsfm3
      integer ind(*),isv(8),it,ik,ikr,iim,ij,ip,ii,icode,iidx,isym,
     &        iq,ilev,ipr, ist, icount, ixx,isv2(8)
      integer j,jj,jsym,jklsym,jlev,jidx
      integer head
      integer kidx,kjkj,kkjj,kntft,kk,klsym,klev,kj,kkkk,kjdx,kp,
     &        kkpq,kpkq,kq,k,ksym
      integer lenbft,lit,llli,lt,li2,liii,ljdx,lidx,llii,lili,llm,llev,
     &        likj,ljki,lkji,lsym,llll,llr,lkdx,ll ,lldx,llmr,ll2
      integer mk
      integer nsw,nv,nw, navst_act
      integer off1(*)
      integer psym,p,pq0,pq
      integer q,qind
      integer r(*)
      integer symw
      integer tail
      integer xbar(nsym,*),xp(nsym,*)
      integer yb(8),yk(8)
      integer z(nsym,*),spindens
c
c  ##  real*8 section
c
      real*8 atot(n2mt)
      real*8 ftbuf(lenbft),fvec(ncsfm*navst_act)
      real*8 hvect(ncsfm*navst_act)
      real*8 tdenv(ncsfm,navst_act,3),td1(*),td2(*),temp,tmp
      real*8 vecl(ncsfm*navst_act),val(3)
c
c  ##  logical section
c
      logical qfflag,qtflag,qt(3)
c
c  ##  external section
c
      real*8 ddot_wr, dsum
c
c  ##  character section
c
      character*60 fname1,fname2
c
c  ##  data section
c
      logical e1dflg(2:7)
      data e1dflg/3*.false.,.true.,.false.,.true./
c
c---------------------------------------------------------------
c
      ncsfm2=ncsfm*2
      ncsfm3=ncsfm*3
c
       if (nst.eq.1) then
         fname1 = 'mcdftfl'
         fname2 = 'mcoftfl'
       elseif (ist.ge.10) then
         write(fname1,'(a8,i2)')'mcdftfl.',ist
         write(fname2,'(a8,i2)')'mcoftfl.',ist
       else
         write(fname1,'(a8,i1)')'mcdftfl.',ist
         write(fname2,'(a8,i1)')'mcoftfl.',ist
       endif
      call trnfln( 1, fname1 )
      call trnfln( 1, fname2 )
      open(unit=ndft,file=fname1,status='old',form='unformatted')
      open(unit=nft,file=fname2,status='old',form='unformatted')
c
c  (me)
c
      if(qtflag)then
        call dscal_wr(ncsfm*navst_act,two,vecl,1)
      endif ! (qtflag)
c
      if(qfflag)then
c
c  fvec(*) doubly occupied orbital contributions...
c
c  fvec(n) = <n| sum(k) atot(k,k) e(k,k) |mc> + (active orbital part)
c          = 2*(sum(k) atot(k,k) )*<n|mc>     + (active orbital part)
c
         temp=zero
         do 100 k=1,ndmot
            ksym=symd(k)
            mk=mapmd(k)-nsm(ksym)
            temp=temp+atot(n2sm(ksym)+(mk-1)*nmpsy(ksym)+mk)
100      continue
         if(temp.ne.zero)then
            call dcopy_wr(ncsfm*navst_act,hvect,1,fvec,1)
            call dscal_wr(ncsfm*navst_act,two*temp,fvec,1)
         else ! (temp.ne.zero)
            call wzero(ncsfm*navst_act,fvec,1)
         endif ! (temp.ne.zero)
      endif ! (qfflag)
c
c  diagonal elements, active orbital contributions.
c
      rewind ndft
      kntft=1
      nw=0
      read(ndft)ftbuf
c
      do 1700 llev=1,namot
         llm=mapml(llev)
         lsym=symm(llm)
         llmr=llm-nsm(lsym)
         ll2=n2sm(lsym)+(llmr-1)*nmpsy(lsym)+llmr
         ll=mapivm(llm)
         lldx=nndx(ll+1)
         llll=off1(lldx)+addii(lldx)
         llr=nnsi(lsym)+nndx(ll-nsi(lsym)+1)
c
         do 1600 ilev=1,llev
            ii=mapivm(mapml(ilev))
            isym=symm(mapml(ilev))
            iidx=nndx(ii+1)
            lidx=nndx(max(ii,ll))+min(ii,ll)
            call wzero(ncsfm2*navst_act,tdenv,1)
c
1000        continue
            kntft=kntft+nw
            call decodf(nv,nsw,symw,head,tail,icode,val,isv,yb,yk,
     &       nw,ftbuf(kntft),isv2,nwalk_f(ist),spindens)
c
            if(icode.eq.0)then
c              ...new record.
               kntft=1
               nw=0
               read(ndft)ftbuf
               go to 1000
            elseif(icode.eq.1)then
c              ...change level.  process density contributions
c              ... at the present level.
               if(ilev.ne.llev)then
                  if(qtflag)then
c                    llii=off1(max(lldx,iidx))+addii(min(lldx,iidx))
c                    td2(llii)=td2(llii)+ddot_wr(ncsfm,tdenv(1,1),1,
c    + vecl,1)

c                    lili=off1(lidx)+addii(lidx)
c                    td2(lili)=td2(lili)+ddot_wr(ncsfm,tdenv(1,2),1,
c    + vecl,1)

                     llii=off1(max(lldx,iidx))+addii(min(lldx,iidx))
                     lili=off1(lidx)+addii(lidx)
                        icount = 1
                        do ixx=1,navst_act
                        td2(llii)=td2(llii)+wavst(ist,ixx)*
     &                   ddot_wr(ncsfm,tdenv(1,ixx,1),1,vecl(icount),1)
                        td2(lili)=td2(lili)+wavst(ist,ixx)*
     &                   ddot_wr(ncsfm,tdenv(1,ixx,2),1,vecl(icount),1)
                        icount = icount + ncsfm
                        enddo ! ixx=1,navst_act
                  endif ! (qtflag)
               else ! (ilev.ne.llev)
                  if(qtflag)then
c                    td2(llll)=td2(llll)+ddot_wr(ncsfm,tdenv(1,1),1,
c    + vecl,1)

c                    td1(llr)=td1(llr)+ddot_wr(ncsfm,tdenv(1,2),1,ve
c    + cl,1)

                        icount = 1
                        do ixx=1,navst_act
                        td2(llll)=td2(llll)+wavst(ist,ixx)*
     &                   ddot_wr(ncsfm,tdenv(1,ixx,1),1,vecl(icount),1)
                        td1(llr)=td1(llr)+wavst(ist,ixx)*
     &                   ddot_wr(ncsfm,tdenv(1,ixx,2),1,vecl(icount),1)
                        icount = icount + ncsfm
                        enddo ! ixx=1,navst_act
                  endif ! (qtflag)
                  if(qfflag)then
                     if(atot(ll2).ne.zero) then
                     icount = 1
                      call daxpy_wr(ncsfm*navst_act,atot(ll2),
     &                 tdenv(1,1,2),1,fvec,1)
                     endif ! (atot(ll2).ne.zero)
                  endif ! (qtflag)
               endif ! (ilev.ne.llev)
               go to 1600
            else ! (icode.eq.1)
c              ...process ft entries...
               if(qtflag .or. (qfflag.and.e1dflg(icode)))then
                  call sdmatd_md(
     &             symw,         nsw,        isv,        yb,
     &             xbar(1,head), xp(1,tail), z(1,tail),  r,
     &             ind,          val,        icode,      hvect,
     &             tdenv,        ncsfm,      qind,       navst_act)
               endif ! (qtflag .or. (qfflag.and.e1dflg(icode)))
               go to 1000
            endif ! (icode.eq.1)
c
1600     continue
1700  continue
c
c  off-diagonal loop contributions.
c
      rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 2740 llev=2,namot
         llm=mapml(llev)
         lsym=symm(llm)
         llmr=llm-nsm(lsym)
         ll=mapivm(llm)
         lldx=nndx(ll+1)
c
         do 2730 klev=1,llev
            kk=mapivm(mapml(klev))
            ksym=symm(mapml(klev))
            klsym=mult(lsym,ksym)
            lkdx=nndx(max(ll,kk))+min(ll,kk)
c
            do 2720 jlev=1,klev
               jj=mapivm(mapml(jlev))
               jsym=symm(mapml(jlev))
               jklsym=mult(jsym,klsym)
               ljdx=nndx(max(ll,jj))+min(ll,jj)
               kjdx=nndx(max(kk,jj))+min(kk,jj)
c
               do 2710 ilev=1,jlev
                  if(ilev.eq.klev)go to 2710
                  iim=mapml(ilev)
                  isym=symm(iim)
                  if(isym.ne.jklsym)go to 2710
                  ii=mapivm(iim)
                  lidx=nndx(max(ll,ii))+min(ll,ii)
                  kidx=nndx(max(kk,ii))+min(kk,ii)
                  jidx=nndx(max(jj,ii))+min(jj,ii)
                  iidx=nndx(ii+1)
                  call wzero(ncsfm3*navst_act,tdenv,1)
                  qt(1)=.false.
                  qt(2)=.false.
                  qt(3)=.false.
c
2100              continue
                  kntft=kntft+nw
                  call decodf(nv,nsw,symw,head,tail,icode,val,isv,yb,yk,
     &             nw,ftbuf(kntft),isv2,nwalk_f(ist),spindens)
c
                  if(icode.eq.0)then
c                    ...new record.
                     kntft=1
                     nw=0
                     read(nft)ftbuf
                     go to 2100
                  elseif(icode.eq.1)then
c                    ...process density contributions and change level.
                     if(jlev.ne.llev)then
                        if(qtflag)then
                           if(qt(1))then
                              lkji=off1(max(lkdx,jidx))+
     &                         addii(min(lkdx,jidx))
c                             td2(lkji)=td2(lkji)+
c    &                         ddot_wr(ncsfm,tdenv(1,1),1,vecl,1)
                                 icount = 1
                                 do ixx=1,navst_act
                                 td2(lkji)=td2(lkji)+wavst(ist,ixx)*
     &                            ddot_wr(ncsfm,tdenv(1,ixx,1),1,
     &                            vecl(icount),1)
                                 icount = icount + ncsfm
                                 enddo ! ixx=1,navst_act
                           endif ! (qt(1))
                           if(qt(2))then
                              ljki=off1(max(ljdx,kidx))+
     &                         addii(min(ljdx,kidx))
c                             td2(ljki)=td2(ljki)+wavst(ist,ixx)*
c    &                         ddot_wr(ncsfm,tdenv(1,2),1,vecl,1)
                                 icount = 1
                                 do ixx=1,navst_act
                                 td2(ljki)=td2(ljki)+wavst(ist,ixx)*
     &                            ddot_wr(ncsfm,tdenv(1,ixx,2),1,
     &                            vecl(icount),1)
                                 icount = icount + ncsfm
                                 enddo ! ixx=1,navst_act
                           endif ! (qt(2))
                           if(qt(3))then
                              likj=off1(max(lidx,kjdx))+
     &                         addii(min(lidx,kjdx))
c                             td2(likj)=td2(likj)+
c    &                         ddot_wr(ncsfm,tdenv(1,3),1,vecl,1)
                                 icount = 1
                                 do ixx=1,navst_act
                                 td2(likj)=td2(likj)+wavst(ist,ixx)*
     &                            ddot_wr(ncsfm,tdenv(1,ixx,3),1,
     &                            vecl(icount),1)
                                 icount = icount + ncsfm
                                 enddo ! ixx=1,navst_act
                           endif ! (qt(3))
                        endif ! (qtflag)
                     else ! (jlev.ne.llev)
c                       ...liii,llli,li special case.
                        if(qtflag)then
                           if(qt(1))then
                              liii=off1(max(lidx,iidx))+
     &                         addii(min(lidx,iidx))
c                             td2(liii)=td2(liii)+
c    &                         ddot_wr(ncsfm,tdenv(1,1),1,vecl,1)
                                 icount = 1
                                 do ixx=1,navst_act
                                 td2(liii)=td2(liii)+wavst(ist,ixx)*
     &                            ddot_wr(ncsfm,tdenv(1,ixx,1),1,
     &                            vecl(icount),1)
                                 icount = icount + ncsfm
                                 enddo ! ixx=1,navst_act
                           endif ! (qt(1))
                           if(qt(2))then
                              llli=off1(max(lldx,lidx))+
     &                         addii(min(lldx,lidx))
c                             td2(llli)=td2(llli)+
c    &                         ddot_wr(ncsfm,tdenv(1,2),1,vecl,1)
                                 icount = 1
                                 do ixx=1,navst_act
                                 td2(llli)=td2(llli)+wavst(ist,ixx)*
     &                            ddot_wr(ncsfm,tdenv(1,ixx,2),1,
     &                            vecl(icount ),1)
                                 icount = icount + ncsfm
                                 enddo ! ixx=1,navst_act
                           endif ! (qt(2))
                           if(qt(3))then
                              lt=ll-nsi(lsym)
                              it=ii-nsi(lsym)
                              lit=nnsi(lsym)+nndx(max(lt,it))+min(lt,it)
c                             td1(lit)=td1(lit)+
c    &                         ddot_wr(ncsfm,tdenv(1,3),1,vecl,1)
                                 icount = 1
                                 do ixx=1,navst_act
                                 td1(lit)=td1(lit)+wavst(ist,ixx)*
     &                            ddot_wr(ncsfm,tdenv(1,ixx,3),1,
     &                            vecl(icount),1)
                                 icount = icount + ncsfm
                                 enddo ! ixx=1,navst_act
                           endif ! (qt(3))
                        endif ! (qtflag)
                        if(qfflag.and.qt(3))then
                           li2=n2sm(lsym)+(llmr-1)*nmpsy(lsym)+
     &                      (iim-nsm(lsym))
                           if (atot(li2).ne.zero) then
                            call daxpy_wr (ncsfm*navst_act,
     &                       atot(li2),tdenv(1,1,3),1,fvec,1)
                           endif ! (atot(li2).ne.zero)
                        endif ! (qfflag.and.qt(3))
                     endif ! (jlev.ne.llev)
                     go to 2710
                  else
c                       ...process ft entries.
                     if(qtflag .or. (qfflag.and.(jlev.eq.llev)))then
                        call sdmat_md(
     &                  symw,  nsw,          isv,            yb,
     &                  yk,    xbar(1,head), xp(1,tail),     z(1,tail),
     &                  r,     ind,          ftbuf(kntft+1), icode,
     &                  hvect, tdenv,        ncsfm,          qt,
     &                  qind,  navst_act)
                     endif
                     go to 2100
                  endif
c
2710           continue
2720        continue
2730     continue
2740  continue
c
c
c  #  for fvec account for state averaging and factor two
c
      if (qfflag) then
        icount = 1
        do ixx=1,navst_act
        call dscal_wr(ncsfm,two*wavst(ist,ixx),fvec(icount),1)
        icount = icount + ncsfm
        enddo ! ixx=1,navst_act
      end if ! (qfflag)
c
      if(qtflag)then
c
c  include doubly occupied orbital contributions to the symmetrized
c  transition density matrix elements.
c
c  td1(kk) = 2*(hvect*vecl)
c
c  td2(kkkk) = 2*(hvect*vecl)
c  td2(kkjj) = 4*(hvect*vecl)
c  td2(kjkj) =  -(hvect*vecl)
c
c  td2(kkpq) =     2*td1(pq)
c  td2(kpkq) = -half*td1(pq)
c
c        temp=ddot_wr(ncsfm*navst_act,hvect,1,vecl,1)
         temp = zero
         icount = 1
            do ixx=1, navst_act
            temp = temp + wavst(ist,ixx)*ddot_wr(ncsfm,hvect(icount),
     &        1,vecl(icount),1)
            icount = icount + ncsfm
            enddo ! ixx=1, navst_act
         if(abs(temp).gt.small)then
c
c  ...note:  the dot product is usually zero because vecl(*) is
c            in the orthogonal complement to hvect(*).  this code
c            is included for generality. -rls
c
            do 3100 k=1,ndmot
               ksym=symd(k)
               ikr=mapivm(mapmd(k))-nsi(ksym)
               kk=nnsi(ksym)+nndx(ikr+1)
               td1(kk)=td1(kk) +two*temp
3100        continue
c
            do 3300 k=1,ndmot
               ik=mapivm(mapmd(k))
               kk=nndx(ik+1)
               do 3200 j=1,(k-1)
                  ij=mapivm(mapmd(j))
                  jj=nndx(ij+1)
                  kkjj=off1(kk)+addii(jj)
                  td2(kkjj)=td2(kkjj) +four*temp
                  kj=nndx(ik)+ij
                  kjkj=off1(kj)+addii(kj)
                  td2(kjkj)=td2(kjkj) -temp
3200           continue
               kkkk=off1(kk)+addii(kk)
               td2(kkkk)=td2(kkkk) +two*temp
3300        continue
         endif ! (abs(temp).gt.small)
c
       endif ! (qtflag)
c
      close(unit=nft)
      close(unit=ndft)
c
      return
      end
c
c******************************************************
c
      subroutine fandt_d1(
     & off1,      addii,     td1,
     & td2)
c
c
c
c #  in the fandt the following td1 and td2 was calculated:
c #   update the internal-internal and internal-vituell density
c #   matrix elemets with the symmetrized transition density matrix
c #   elements:
c #
c #        td1(uv)   =  sum(n)  vecl(n) * <n| e(uv) + e(vu) |mc>
c #        td2(uvwx) =  (1/4) * sum(n) vecl(n) * <n| e(uvwx) + e(vuwx)
c #                            + e(uvxw) + e(vuxw) + e(wxuv) + e(xwuv)
c #                                      + e(wxvu) + e(xwvu) |mc>
c #
c ##  NOTE: this subroutine is a former part of fandt
c #         the separation is cased by state averaging
c ##  HERE: the td1 contribution to td2 is added to td2
c
c  input:
c  off1(*),addii(*) = occupied orbital addressing arrays.
c  td1(*),td2(*) = initial transtion density elements, indexed by
c                  internal orbitals.
c
c  output:
c  td2(*) = updated transition density elements
c
c   last modified: XI/99 Michal Dallos Uni Wien
c
c==================================================
c
       implicit none
c  ## common & parameter section
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      integer navst,nst,navst_max
      real*8 heig,wavst
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nfilmx
      parameter(nfilmx=30)
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer nmomx
      parameter(nmomx=1023)
c
      real*8    two,     half,     small
      parameter(two=2.d0,half=.5d0,small=1d-10)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer n2sm(8),nmpsy(8),n2mt,nsm(8),ndmot,namot,ira1(8)
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxtot(6),n2mt)
      equivalence (nxtot(10),namot)
      equivalence (nxtot(7),ndmot)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
c      include 'cmomap.inc/list'
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c  ##   integer section
c
      integer addii(*)
      integer ik,ip,iq,ipr
      integer kk,kp,kkpq,kpkq,kq,k
      integer off1(*)
      integer psym,p,pq0,pq
      integer q
c
c  ##  real*8 section
c
      real*8 td1(*), td2(*), temp
c
         do 3600 p=1,namot
            psym=syma(p)
            ip=mapivm(mapma(p))
            ipr=ip-nsi(psym)
            pq0=nnsi(psym)+nndx(ipr)-nsi(psym)
            do 3500 q=ira1(psym),p
               iq=mapivm(mapma(q))
               temp=td1(pq0+iq)
               if(abs(temp).gt.small)then
                  pq=nndx(ip)+iq
                  do 3400 k=1,ndmot
                     ik=mapivm(mapmd(k))
                     kk=nndx(ik)+ik
                     kkpq=off1(max(pq,kk))+addii(min(pq,kk))
                     td2(kkpq)=td2(kkpq) +two*temp
                     kp=nndx(max(ik,ip))+min(ik,ip)
                     kq=nndx(max(ik,iq))+min(ik,iq)
                     kpkq=off1(kp)+addii(kq)
                     td2(kpkq)=td2(kpkq) -half*temp
3400              continue
               endif ! (abs(temp).gt.small)
3500        continue
3600     continue
c
      return
      end

c
c****************************************************************
c
c deck fdo
      subroutine fdo(
     & fci,   ado, fqa, dmc,
     & fqdmc, scr )
c
c  compute the fdo(*) effective fock matrix and add to fci(*).
c
c  fdo(r,s) = sum(t) h1(r,t)*d1do(t,s) + sum(tuv) h2(rtuv)*d2do(stuv)
c
c  with the effective density matrices d1do(*) and d2do(*) defined as
c
c  d1do(u,v) = 2*ado(u,v)
c  d2do(uvwx) = 2*ado(uv)*dmc(wx) + 2*ado(wx)*dmc(uv)
c               -half*( ado(uw)*dmc(vx) + ado(ux)*dmc(vw) +
c                       ado(vx)*dmc(uw) + ado(vw)*dmc(ux)  )
c
c  this simplifies to
c
c  fdo(r,s) = [ fqa*dmc + fqdmc*ado ] (r,s)
c
c  input:
c  fci(*) = squared symmetry-packed fci(*) matrix.
c  ado(*),fqa(*),dmc(*),fqdmc(*) = squared symmetry-packed arrays.
c  scr(*) = scratch array.
c
       implicit none
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,8),n2sm(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 fci(*),ado(*),fqa(*),dmc(*),fqdmc(*),scr(*)
c
c   ##   integer section
c
      integer i,ip
      integer j,jp
      integer p,pq,psym
      integer qsym,q
      integer nj,ni

c
      integer isym
      integer nu,n2pt,nu2
c
      real*8    one,     two
      parameter(one=1d0, two=2d0)
c
      do 100 isym=1,nsym
         nu=nmpsy(isym)
         if(nu.eq.0)go to 100
         nu2=nu*nu
         n2pt=n2sm(isym)+1
c
c        ...fqdmc * ado terms.
c
         call mxm(fqdmc(n2pt),nu, ado(n2pt),nu, scr,nu)
         call daxpy_wr(nu2,one, scr,1, fci(n2pt),1)
c
c        ...fqa * dmc terms.
c
         call mxm(fqa(n2pt),nu, dmc(n2pt),nu, scr,nu)
         call daxpy_wr(nu2,one, scr,1, fci(n2pt),1)
100   continue
c
      return
      end
c deck fndast
      subroutine fndast(idafl,bufv,ibuf,dabuf)
c
c  finish a direct-access sort.
c  dump any nonempty buckets.
c
       implicit none
      integer ndafmx, nbkmx
      parameter(ndafmx=2, nbkmx=200)
      integer ibfpt, ircpt, nvpbk, nvpsg, lendar, nbuk
      common/intsrt/ibfpt(nbkmx),ircpt(nbkmx,ndafmx),
     & nvpbk,nvpsg,lendar,nbuk
c
      integer ibuf(nvpbk,nbuk), ibuk, idafl
c
      real*8 bufv(nvpbk,nbuk),dabuf(lendar)
c
      do 10 ibuk=1,nbuk
         if(ibfpt(ibuk).ne.0)call wrtda(ibfpt(ibuk),ircpt(ibuk,idafl),
     &    bufv(1,ibuk),ibuf(1,ibuk),lendar,dabuf,nvpbk,idafl)
10    continue
c
      return
      end
c deck fockm1
      subroutine fockm1(fci,h1,d1,scr1,scr2,ifeff)
c
c  calculate one-electron contribution to the fock matrix.
c  fci(*) = fock matrix.
c  h1(*) = one-electron integrals.
c  d1(*) = one-particle density matrix.
c
c  changes to calculate ci-energy (m.e.)
c  written by hans lischka
c
       implicit none
      logical ifeff
      real*8 fci(*),h1(*),d1(*),scr1(*),scr2(*)
c
      integer   nfilmx
      parameter(nfilmx=30)
c
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
c     parameter(nmomx=1023)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),n2sm(8),nmot,n2mt,nnsm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(6),n2mt)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 esym1,eesym1,esym2,eesym2
      common /ener/esym1(8),eesym1(8),esym2(8),eesym2(8)
c
      integer if,ind,id,i
      integer j
      integer ni
c
      if (.not.ifeff) then
ctm
         call wzero(nsym,esym1,1)
         call wzero(nsym,eesym1,1)
c        call dcopy_wr(nsym,0.0d0,0,esym1,1)
c        call dcopy_wr(nsym,0.0d0,0,eesym1,1)
ctm
      end if
c
      do 100 i=1,nsym
         ni=nmpsy(i)
         if(ni.eq.0)go to 100
         id=nnsm(i)+1
         if=n2sm(i)+1
         call sm11sq(h1(id),scr1,ni)
         call sm11sq(d1(id),scr2,ni)
         call mxm(scr1,ni, scr2,ni, fci(if),ni)
c        call matmul(fci(if),scr1,scr2,ni,ni,ni,1)
         ind=0
         if (.not.ifeff) then
            do 10 j=1, ni
               esym1(i)=esym1(i) + fci(if+ind)
               ind=ind+ni+1
10          continue
         else
            do 20 j=1, ni
               eesym1(i)=eesym1(i) + fci(if+ind)
               ind=ind+ni+1
20          continue
         endif
100   continue
      return
      end
c
c*************************************************************
c
c deck fockm2
      subroutine fockm2(
     & nmmmmt,     h2,      d2,    dabuf,
     & idabuf,     dabufv,  fci,   scrh,
     & scrd,       scrf,    idaf,  ddaf,
     & ifeff)
c
c  this routine reads the sorted integral and two-particle density
c  matrix files and computes the two-electron contribution to the
c  fock matrix fci(*).
c
c    fci(p,q) = fci(p,q) + sum(tuv) h2(ptuv)*d2(qtuv)
c
c
c  input:
c  nmmmmt = total number of sorted h2 and d2 values (2x expanded).
c  h2(*) = space for 2-e integral segment.
c  d2(*) = space for two-particle density matrix segment.
c  fci(*) = fci fock matrix.
c  scrh(*), scrd(*) and scrf(*) are scratch arrays.
c  dabuf(*) = da record buffer.
c  idabuf(*) = da address buffer.
c  dabufv(*) = da value buffer.
c  symtmm(1:8) = number of elements in a distribution of given symmetry.
c  symm(*) = mo symmetry array.
c  idaf = integral da file.
c  ddaf = density matrix da file.
c  ifeff = fock- or effective-fockmatrix construction
c  fqflag = fq(*) construction flag. (if true then construct fq(*) )
c
c  output:
c  fci(*) = updated fci fock matrix.
c
c  version log:
c     feb-10  adaptations for sa-mcscf gradient -felix plasser
c     feb-90  save diagonal fockmatrixelemets (m.e.)
c  19-nov-88  use matrix-matrix interface routines. -rls
c  15-sep-87  sort changes. -rls
c  written by hans lischka june-1987
c
       implicit none
c  ##  common & parameter section
c
      integer   nfilmx
      parameter(nfilmx=30)
c
      integer nmomx
      parameter(nmomx=1023)
c
      integer ndafmx
      parameter(ndafmx=2)
c
      integer nbkmx
      parameter(nbkmx=200)
c
      real*8    one,     two,     uvfact
      parameter(one=1.d0,two=2.d0)
c
      integer dblocc,active,virtl
      parameter(dblocc=1,active=2,virtl=3)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),n2sm(8),nmot,n2mt
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(6),n2mt)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      real*8 esym1,eesym1,esym2,eesym2
      common/ener/esym1(8),eesym1(8),esym2(8),eesym2(8)
c
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer ibfpt,ircpt,nvpbk,nvpsg,lendar,nbuk
      common/intsrt/ibfpt(nbkmx),ircpt(nbkmx,ndafmx),
     & nvpbk,nvpsg,lendar,nbuk
c
      integer numopt
      parameter(numopt=10)
c
      integer cigopt, print, fresdd, fresaa, fresvv, samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
      real*8 h2(*),d2(*),dabuf(*),dabufv(*),
     & fci(*),scrh(*),scrd(*),scrf(*),hxp(100),dxp(100),fxp(100)
      real*8 rmd1,rmd2,dnrm2_wr
      external dnrm2_wr
c
c   ##   integer section
c
      integer add0
      integer d2pt,ddaf
      integer fincr
      integer idabuf(*),if,isym,ind,ibuk,idaf,intpt
      integer i,j
      integer fpt,finuv
      integer maxrd
      integer np,nmo,nmmmmt,nt
      integer psym
      integer restuv
      integer tsym
      integer u,usym,vsym,uvsym,uvpt
      integer v
cfp
      integer nvirt,skip
c
      logical ifeff
c  initialize record and buffer pointers.
c
cfp
      if(samcflag.gt.0)then
        write(nlist,*)'SA-MCSCF: fockmatrix construction only over
     $ non-zero matrices'
      endif
      maxrd=0
      uvpt=1
      ibuk=1
      restuv=0
      nvirt=0
      skip=0
c
cfp: maybe this loop could be done with symi like add2
      do 120 u=1,nmot
         usym=symm(u)
         do 110 v=1,u
            vsym=symm(v)
            uvsym=mult(vsym,usym)
            if(u.eq.v)then
               uvfact=one
            else
               uvfact=two
            endif
cfp: check if u and/or v are external
c  the entry can be skipped if all d elements are 0
         if(samcflag.gt.0)then
           nvirt=0
           if(mctype(u).eq.virtl) nvirt=nvirt+1
           if(mctype(v).eq.virtl) nvirt=nvirt+1
c
           skip=0
           if(nvirt.eq.2)then
              skip=2
           elseif((nvirt.eq.1).and.(.not.ifeff))then
              skip=1
           endif
         endif
c
c  check whether the complete distribution is in core
c  if this is not the case, move the remaining part of the segment
c  to the beginning and read next segment.
c  note: h2(*) and d2(*) must be (nnmt+nvpsg) in length.
c
cfp: finuv - last index of this distribution
            if(skip.eq.2) goto 110
            finuv=uvpt+symtmm(uvsym)-1
            fincr=maxrd+restuv
            if(finuv.gt.fincr)then
c            if(skip)then
c               restuv=0
c               write(nlist,*)'Reading of new data skipped'
c            else
c              write(nlist,*)'fockm2: opening direct access file'
               restuv=fincr-uvpt+1               
               if(restuv.gt.0)then
                  call dcopy_wr(restuv,h2(uvpt),1,h2(1),1)
                  call dcopy_wr(restuv,d2(uvpt),1,d2(1),1)
               endif
               add0=(ibuk-1)*nvpsg
cfp: nvpsg: number of integrals in the segment(=bucket?)
               maxrd=min(nvpsg,nmmmmt-add0)
               call rdbks(add0,lendar,nvpbk,maxrd,
     &          idaf,ircpt(ibuk,idaf),h2(restuv+1),dabuf,
     &          dabufv,idabuf)
               call rdbks(add0,lendar,nvpbk,maxrd,
     &          ddaf,ircpt(ibuk,ddaf),d2(restuv+1),dabuf,
     &          dabufv,idabuf)
c            endif
               ibuk=ibuk+1
               uvpt=1
            endif
cfp
         if(skip.eq.1) goto 103
            call wzero(n2mt,scrf,1)
            do 100 tsym=1,nsym
               nt=nmpsy(tsym)
               if(nt.eq.0)go to 100
               psym=mult(tsym,uvsym)
               np=nmpsy(psym)
               if(np.eq.0)go to 100
               fpt=n2sm(tsym)+1
               intpt=intoff_ci(tsym,uvsym)+uvpt
               d2pt=intoff_ci(psym,uvsym)+uvpt
               if(psym.lt.tsym)then
c                 ...fci(nt*nt)=transpose(h2(np*nt))*d2(np*nt)
                  call mtxm(h2(intpt),nt, d2(d2pt),np, scrf(fpt),nt)
c                  write(nlist,*) 'plt,h2',(h2(intpt+j),j=1,nt)
c                    write(nlist,*) 'd2',(d2(intpt+j),j=1,np)
               elseif(psym.eq.tsym)then
c                 ...fci(nt*nt)=expand(h2(nt*nt))*expand(d2(nt*nt))
                  call sm11sq(h2(intpt),scrh,nt)
                  call sm11sq(d2(d2pt),scrd,nt)
                  call mxm(scrh,nt, scrd,nt, scrf(fpt),nt)
c                   call plblkt('h2',h2(intpt),nt,'mo',1,nlist)
c                  call plblkt('d2',d2(d2pt),nt,'mo',1,nlist)
c                  write(nlist,*) 'pet,h2',(h2(intpt+j),j=1,nt)
c                  write(nlist,*) 'd2',(d2(intpt+j),j=1,nt)
               else
c                 ...psym.gt.tsym
c                 ...fci(nt*nt)=h2(nt*np)*transpose(d2(nt*np))
                  call mxmt(h2(intpt),nt, d2(d2pt),np, scrf(fpt),nt)
c                  write(nlist,*) 'pgt,h2',(h2(intpt+j),j=1,nt)
c                  write(nlist,*) 'd2',(d2(intpt+j),j=1,np)
               endif
211         format(a,3i4)
100         continue
            call daxpy_wr(n2mt,uvfact,scrf,1,fci,1)
 210        format(a,f10.6,3i6)
cfp
103         continue
            uvpt=uvpt+symtmm(uvsym)
110      continue
120   continue
c
c    save diagonal fockmatrix elements
c
      if (.not.ifeff) then
         call wzero(nsym,  esym2, 1)
         call wzero(nsym, eesym2, 1)
c-         call dcopy_wr(nsym,0.0d0,0,esym2,1)
c-         call dcopy_wr(nsym,0.0d0,0,eesym2,1)
      endif
c
      do 130 isym=1, nsym
         ind=1
         if=n2sm(isym)
         nmo=nmpsy(isym)
c
         if (.not.ifeff) then
            do 10 j=1, nmo
               esym2(isym)=esym2(isym) + fci(if+ind)
               ind=ind+nmo+1
10          continue
         else
            do 20 j=1, nmo
               eesym2(isym)=eesym2(isym) + fci(if+ind)
               ind=ind+nmo+1
20          continue
         endif
130   continue
c
      return
      end
c
c**************************************************************
c
c deck fockq
      subroutine fockq(
     & h2,     dabuf,    idabuf,   dabufv,
     & scrh,   scrf,     idaf,     nmmmmt,
     & fqaflg, fqa,      d1a,      fqbflg,
     & fqb,    d1b)
c
c  this routine reads the sorted integral file and computes the
c  two-electron contribution to the fock matrices fqx(*).
c
c    fqx(p,q) = sum(uv) (2*h2(pquv)-h2(puqv))*d1x(u,v)
c
c  this routine computes the exchange contributions from the direct-
c  sorted integral list.  it is not well-known, but is straightforward
c  to see, that this can be efficiently done with matrix-vector
c  products. -rls
c
c  input:
c  h2(*) = space for 2-e integral segment.
c  scrh(*) and scrf(*) are scratch arrays.
c  dabuf(*) = da record buffer.
c  idabuf(*) = da address buffer.
c  dabufv(*) = da value buffer.
c  symtmm(1:8) = number of elements in a distribution of given symmetry.
c  symm(*) = mo symmetry array.
c  idaf = integral da file.
c  nmmmmt = total number of sorted integrals.
c  fqaflg,fqbflg = construction flags.
c  d1a(*),d1b(*) = squared symmetry-packed effective 1-particle
c                  density matrices.
c
c  output:
c  fqa(*),fqb(*) = output squared symmetry-packed fock matrices.
c
c  written by ron shepard.
c  version log:
c  31-mar-10  adaptation to SA-MCSCF gradient -felix plasser
c  08-jan-89  fixed sign typo in fqb(*). -rls
c  19-nov-88  use matrix-vector interface routines. -rls
c  20-sep-87  first version adapted from fockm2(). -rls
c
       implicit none
      real*8    uvfact,  term
      real*8    zero,     one,     two,     four
      parameter(zero=0.d0,one=1.d0,two=2.d0,four=4.d0)
c
      integer ndafmx
      parameter(ndafmx=2)
c
      integer nbkmx
      parameter(nbkmx=200)
c
      integer nmomx
      parameter(nmomx=1023)
c
      integer dblocc,active,virtl
      parameter(dblocc=1,active=2,virtl=3)
c
      integer numopt
      parameter(numopt=10)
      integer cigopt, print, fresdd, fresaa, fresvv, samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nsm(8),n2sm(8),nmot,n2mt
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(6),n2mt)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer ibfpt,ircpt,nvpbk,nvpsg,lendar,nbuk
      common/intsrt/ibfpt(nbkmx),ircpt(nbkmx,ndafmx),
     & nvpbk,nvpsg,lendar,nbuk
c
      real*8 h2(*),dabuf(*),dabufv(*),
     & scrh(*),scrf(*),fqa(*),d1a(*),fqb(*),d1b(*)
      logical fqaflg,fqbflg
c
c   ##   integer section
c
      integer add0
      integer finuv,fincr
      integer idabuf(*),idaf,ibuk,intpt
      integer maxrd
      integer nmmmmt,nu,nv
      integer restuv
      integer t1u,t1v
      integer uvsym,uv,uvpt,u,usym,ur
      integer v,vr,vsym,nvirt
c
c  initialize fock matrices...
c
      if(fqaflg)call wzero(n2mt,fqa,1)
      if(fqbflg)call wzero(n2mt,fqb,1)
c
c  initialize record and buffer pointers.
c
      maxrd=0
      uvpt=1
      ibuk=1
      restuv=0
c
      do 120 u=1,nmot
         usym=symm(u)
         ur=u-nsm(usym)
         nu=nmpsy(usym)
         do 110 v=1,u
            vsym=symm(v)
            vr=v-nsm(vsym)
            nv=nmpsy(vsym)
            uvsym=mult(vsym,usym)
cfp: skip everything if this is a 2-ext. distribution
            if(samcflag.gt.0)then
               nvirt=0
               if(mctype(u).eq.virtl) nvirt=nvirt+1
               if(mctype(v).eq.virtl) nvirt=nvirt+1
               if(nvirt.eq.2)goto 110
            endif
c
            if(u.eq.v)then
               uvfact=two
            else
               uvfact=four
            endif
c
c  check whether the complete distribution is in core
c  if this is not the case, move the remaining part of the segment
c  to the beginning and read next segment.
c  note: h2(*) must be (nnmt+nvpsg) in length.
c
            finuv=uvpt+symtmm(uvsym)-1
            fincr=maxrd+restuv
c            write(*,*)'u,nmot,finuv,fincr',u,nmot,finuv,fincr
            if(finuv.gt.fincr)then
               restuv=fincr-uvpt+1
               if(restuv.gt.0)then
                  call dcopy_wr(restuv,h2(uvpt),1,h2(1),1)
               endif
               add0=(ibuk-1)*nvpsg
               maxrd=min(nvpsg,nmmmmt-add0)
               call rdbks(add0,lendar,nvpbk,maxrd,
     &          idaf,ircpt(ibuk,idaf),h2(restuv+1),dabuf,
     &          dabufv,idabuf)
               ibuk=ibuk+1
               uvpt=1
            endif
c
c  move/expand integrals to the scratch array.
c  expand usym=vsym distributions to square symmetry-packed.
c
            if(usym .eq. vsym)then
               call xpnd1(h2(uvpt),scrh,nmpsy,nsym)
            else
               call dcopy_wr(symtmm(uvsym),h2(uvpt),1,scrh,1)
            endif
c
c  include direct terms only if usym=vsym.
c
            if(usym.eq.vsym)then
               uv=n2sm(usym)+(ur-1)*nu+vr
               if(fqaflg)then
                  term=uvfact*d1a(uv)
                  if(term.ne.zero)
     &             call daxpy_wr(n2mt,term,scrh,1,fqa,1)
               endif
               if(fqbflg)then
                  term=uvfact*d1b(uv)
                  if(term.ne.zero)
     &             call daxpy_wr(n2mt,term,scrh,1,fqb,1)
               endif
            endif
c
c  exchange contributions are computed as a series of matrix-vector
c  products of the direct-sorted integrals.
c
c  fqx(1:n,v) = fqx(1:n,v) - sum(t) h2(1:n,t,uv)*d(t,u)
c  fqx(1:n,u) = fqx(1:n,u) - sum(t) h2(1:n,t,uv)*d(t,v)
c               (second term is included only if u.ne.v)
c
            t1v=n2sm(vsym)+(vr-1)*nv+1
            t1u=n2sm(usym)+(ur-1)*nu+1
            if(usym.eq.vsym)then
               intpt=n2sm(usym)+1
            else
               intpt=intoff_ci(usym,uvsym)+1
            endif
c           ...f=h2*d1
            if(fqaflg)then
               call mxv(scrh(intpt),nv, d1a(t1u),nu, scrf)
               call daxpy_wr(nv,-one,scrf,1,fqa(t1v),1)
            endif
            if(fqbflg)then
               call mxv(scrh(intpt),nv, d1b(t1u),nu, scrf)
               call daxpy_wr(nv,-one,scrf,1,fqb(t1v),1)
            endif
            if(u.ne.v)then
c              ...f=h2(transpose)*d1  (if usym=vsym, trans. is optional)
               if(fqaflg)then
                  call mtxv(scrh(intpt),nu, d1a(t1v),nv, scrf)
                  call daxpy_wr(nu,-one,scrf,1,fqa(t1u),1)
               endif
               if(fqbflg)then
                  call mtxv(scrh(intpt),nu, d1b(t1v),nv, scrf)
                  call daxpy_wr(nu,-one,scrf,1,fqb(t1u),1)
               endif
            endif
            uvpt=uvpt+symtmm(uvsym)
110      continue
120   continue
c
      return
      end
c deck ftest
      subroutine ftest(f,sf,npsy,nsym)
c
c     check if the effective fockmatrix is symmetric
c     return the lower triangle
c
c  input:
c         f(*)    = square effektive fockmatrix
c         npsy(*) = number of mo's in each symmetrie
c         nsym(*) = number of irrep's
c
c  output:
c         sf(*)   = lower triangle effektive fockmatrix
c
c
       implicit none
      integer i2pt, iipt, isym
      integer npsy(*), n, nsym
c
      real*8 f(*),sf(*)
c
      iipt=1
      i2pt=1
      do 10 isym=1,nsym
         n=npsy(isym)
         if(n.gt.0)call ftestb(f(i2pt),sf(iipt),n,isym)
         iipt=iipt + (n*(n+1))/2
         i2pt=i2pt + n*n
10    continue
c
      return
      end
c deck ftestb
      subroutine ftestb(f,sf,n,isym)
c
c  check if the square matrix f(*,*) symmetric
c  return triangular packed matrix sf(*).
c
c  input:
c         f(*,*)  = square matrix
c         n       = number of rows in f(*,*)
c         isym(*) = number of the symmetrie block
c
c  output:
c         sf(*)   = lower triangular matrix
c
c
c
       implicit none
      integer nfilmx, iunits
      parameter(nfilmx=30)
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      real*8  wndtol, wnatol, wnvtol, ftol
      logical qfresd, qfresa, qfresv, qresaf
      common/cresi/ wndtol, wnatol, wnvtol, ftol, qfresd, qfresa,
     & qfresv, qresaf
c
      real*8    half
      parameter(half=0.5d+00)
c
      integer isym, i, ij0
      integer j
      integer n
c
      real*8 diff
      real*8 f(n,n)
      real*8 sf(*)
c
c
      ij0=0
      do 20 i=1,n
         do 10 j=1,(i-1)
            diff=abs(f(i,j)-f(j,i))
            if (diff.ge.ftol) then
               write(nlist,100)isym,i,j,diff,ftol,f(i,j)
            end if
           sf(ij0+j)=half*(f(i,j)+f(j,i))
10       continue
         ij0=ij0+i
        sf(ij0)=f(i,i)
20    continue
c
100   format('***warning***  effective fock matrix is not symmetric ',
     &       /5x,'isym=',i3,' i,j=',i3,i3,' f(ij)-f(ji)= ',1pe12.4,
     &       ' ftol=',e8.4,'f(ij)=',1pe12.4)
      return
c
      end
c deck gtrfld
      subroutine gtrfld(pmask,qmask,pfield,qfield)
c
c  determine the invarient orbital subspace resolution types.
c
c  input:
c  pmask,qmask = decimal masks for two orbitals.
c
c  output:
c  pfield,qfield = first nonzero coincident field within the masks.
c                  =0 if no coincidence is found (essential rotation).
c
c  examples:
c  if pmask=101, qmask=10 then pfield=qfield=0.
c  if pmask=110, qmask=210 then pfield=qfield=1.
c  if pmask=101, qmask=210 then pfield=1, qfield=2.
c
       implicit none
      integer field
      integer pfield, pmask, ptemp
      integer qfield, qmask, qtemp
c
      ptemp=pmask
      qtemp=qmask
      do 10 field=1,8
         pfield=mod(ptemp,10)
         qfield=mod(qtemp,10)
         if(pfield.ne.0 .and. qfield.ne.0)return
         ptemp=ptemp/10
         qtemp=qtemp/10
10    continue
c
c  ...exit from loop means p-q rotation is allowed.
c
      pfield=0
      qfield=0
      return
      end
c deck indast
      subroutine indast(idafl)
c
c  initialize direct access sorting arrays and check for consistency.
c
       implicit none
      integer nfilmx, iunits
      parameter(nfilmx=30)
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer ndafmx, nbkmx
      parameter(ndafmx=2, nbkmx=200)
      integer ibfpt, ircpt, nvpbk,nvpsg,lendar,nbuk
      common/intsrt/ibfpt(nbkmx),ircpt(nbkmx,ndafmx),
     & nvpbk,nvpsg,lendar,nbuk
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer i, idafl
c
      if(nbuk.gt.nbkmx)then
         write(nlist,6010)nbkmx,nbuk
         call bummer('indast: nbuk=',nbuk,faterr)
      endif
      if(idafl.le.0 .or. idafl.gt.ndafmx)then
         call bummer('indast: idafl=',idafl,faterr)
      endif
c
c  initialize buffer pointers and record pointers.
c
      do 10 i=1,nbkmx
         ibfpt(i)=0
         ircpt(i,idafl)=0
10    continue
c
      return
6010  format(/' *** error *** indast: nbkmx,nbuk=',2i8)
      end
c
c********************************************************************
c
c deck iosres
      subroutine iosres(
     & fci,    iorder,    dmc,    fmc,
     & fqdmc,  anofqd,    ado,    af,
     & numaa,  numado,    numav,  job)
c
c  determine whether mcscf invariant orbital subspace resolutions are
c  required and, optionally, compute the required a(*) matrix elements.
c
c      a(i,j) = ( fci(i,j)-fci(j,i) ) / ( (x(jj) - x(ii) )
c
c  where x(*) depends on the orbital resolution type.
c  for mcscf natural orbital resolution, x(*) is the mcscf d1 matrix.
c  for mcscf inactive-orbital resolution, x(*) is the mcscf fock matrix.
c
c  this routine should be called twice.  the first time with job=0 to
c  determine which resolutions are required and the second time with
c  job=1 to actually compute the required a(*) matrix elements.
c
c  nadcalc=1: DCI(J,I,alpha) calculation
c  nadcalc=2: DCSF(J,I,alpha) calculation
c  nadcalc=3: DCI(J,I,alpha)+DCSF(J,I,alpha) calculation
c
c  here:nadcalc=2 or 3,in both cases the DCSF contr. is calculated here:
c
c   a(i,j) = (fci(i,j)-fci(j,i)+D(i,j)[J,I]*(E[I]-E[J]) )/( (x(jj) - x(i
c
c
c  if (job = 0)then    !just check for required resolutions
c     input:
c        fci(*) = ci fock matrix.
c     output:
c        numaa = 0 if active orbital resolutions are not required.
c               = n2mt if no or fqdmc resolutions are required.
c        numado = 0 if do resolutions are not required.
c               = n2mt if do resolutions are required.
c        numav  = 0 if virtual orbital resolution are not required
c               = n2mt if averaged fock operator resolution are
c                 required.
c     not referenced:
c        dmc(*),fmc(*),fqdmc(*),anofqd(*),ado(*)
c  elseif (job = 1)then    !determine resolutions and compute a(*)s
c     input:
c        fci(*) = ci fock matrix.
c        dmc(*) = mcscf density matrix.
c        fmc(*) = mcscf fock matrix.
c        fqdmc(*) = averaged fock operator
c        numaa =   n2mt if no or lgmx terms in the active
c                 orbital space are to be computed.
c        numav  = n2mt if fqdmc terms in the virtual orbital
c                 space are to be computed
c        numado = n2mt if do terms are to be computed.
c     output:
c        anofqd(*) = n.o. or av.f. a(*) matrix.
c        af(*)  = av.f. a(*) matrix.
c        ado(*) = d.o. a(*) matrix.
c  endif
c
       implicit none
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8    zero,      one
      parameter(zero=0.d0, one=1.d0)
c
      integer numopt
      parameter(numopt=10)
c
      integer nfilmx
      parameter(nfilmx=30)
c
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer nmomx
      parameter(nmomx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nsm(8),n2sm(8),n2mt,ird1(8),ird2(8),ira1(8),
     &        ira2(8),irv1(8),irv2(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
      equivalence (nxtot(6),n2mt)
c
      real*8 d1atr(nmomx,nmomx)
      common /d1tr/ d1atr
c
      real*8 energy1,energy2
      integer nadcalc,drt1,drt2,root1,root2
      common /nad/ energy1,energy2,nadcalc,drt1,drt2,root1,root2
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer cigopt,print,fresdd,fresaa,fresvv,samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
      real*8  wndtol, wnatol, wnvtol, ftol
      logical qfresd, qfresa, qfresv, qresaf
      common/cresi/ wndtol, wnatol, wnvtol, ftol, qfresd, qfresa,
     & qfresv, qresaf
c
c  ##  integer section
c
      integer a,ab,aa
      integer bb,ba,b
      integer iorder(*),ij,i,ii,isym
      integer ji,jj,j
      integer job
      integer ma,mar,mjr,mbr,mb,mj,mir,mi,mqr,mpmq,pmask,mpr,
     &        mp,mq
      integer numaa,numado,nm,numav,n2smi
      integer pp,pq,pfield,p
      integer qmask,qp,qq,qfield,q
c
c  ##  real*8 section
c
      real*8 anofqd(*),ado(*),af(*),anumer
      real*8 dmc(*),denom
      real*8 fci(*),fqdmc(*),fmc(*)
c
c  ##  logical section
c
      logical error
c
c-----------------------------------------------------------------------
c
      write(nlist,*)'IOSRES:wnatol',wnatol
      denom=one
      qfresd=.false.
      qfresa=.false.
      qfresv=.false.
      qresaf=.false.
c
      error=.false.
c
      do 900 isym=1,nsym
         n2smi=n2sm(isym)
         nm=nmpsy(isym)
c
c  active-active rotations:
c
         do 200 p=(ira1(isym)+1),ira2(isym)
            mp=mapma(p)
            mpr=mp-nsm(isym)
            pp=n2smi+(mpr-1)*nm+mpr
            pmask=mcres(mp)
            do 100 q=ira1(isym),(p-1)
               if(iorder(nndx(p)+q).gt.0)go to 100
               mq=mapma(q)
               mqr=mq-nsm(isym)
               qq=n2smi+(mqr-1)*nm+mqr
               pq=n2smi+(mqr-1)*nm+mpr
               qp=n2smi+(mpr-1)*nm+mqr
               anumer=fci(pq)-fci(qp)
                if ((nadcalc.eq.3).or.(nadcalc.eq.2)) then
                 anumer = anumer + d1atr(mp,mq)*(energy1-energy2)
                 if(print.ge.1)then
                  write(nlist,*)'nad:redundant:a-a anumer',anumer,qfresa
                  write(nlist,*)'pq,qp,fci(pq),fci(qp): ',
     &            pq,qp,fci(pq),fci(qp)
                write(nlist,*)'mp,mq,d1atr(mp,mq): ',mp,mq,d1atr(mp,mq)
                  write(nlist,*)'energy1-energy2: ',energy1-energy2
                 endif
                endif
               qmask=mcres(mq)
c
c  check fields in the masks for nonzero coincidences.
c
c               write(nlist,*)'pfield',pfield
               call gtrfld(pmask,qmask,pfield,qfield)
               if(pfield.eq.1)then
c                 ...unresolved orbital rotation. wci must be zero.
                  if(abs(anumer).gt.wnatol)then
                     write(nlist,6010)pfield,mp,mq,anumer
                     error=.true.
                     stop 'error at 141'
                  endif
               elseif(pfield.eq.2)then
c                 ...natural orbital resolution.
                  if(job.eq.1)denom=dmc(qq)-dmc(pp)
c                  write(nlist,*)'anumer,wnatol',anumer,wnatol
                  if(abs(anumer).lt.wnatol)then
c                    ...neglible contribution anyway, just ignore it.
                  else
                     if(abs(denom).lt.wnatol)then
c                       ...can't resolve due to occupation degeneracy.
                        write(nlist,6020)mp,mq,anumer,denom
                        error=.true.
                        stop 'error at 153'
                     else
c                       ...n.o. resolution is required.
                        qfresa=.true.
                        numaa=n2mt
         if(print.ge.1)write(nlist,*)'a-a resolution required',qfresa
                        if(job.eq.1)then
                           anofqd(pq)=anumer/denom
                           anofqd(qp)=anofqd(pq)
                        endif
                     endif
                  endif
c
               elseif(pfield.eq.4)then
c            ...averaged fock operator resolution
c
c               write(nlist,*)'job,anumer,wnatol',job,anumer,wnatol
                  if(job.eq.1)denom=fqdmc(qq)-fqdmc(pp)
                  if(abs(anumer).lt.wnatol)then
c                    ...neglible contribution anyway, just ignore it.
                  else
                     if(abs(denom).lt.wnatol)then
c                       ...can't resolve due to degeneracy.
                        write(nlist,6025)mp,mq,anumer,denom
                        error=.true.
                        stop 'error at 176'
                     else
c
c     check to make sure that only doubly occupied orbitals
c          require averaged fock operator resolution
c         the general case is currently not implemented
c
                        if (job.eq.1) then
                        endif
c                       ...fqdmc resolution is required.
                        qfresa=.true.
                        qresaf=.true.
                        numaa=n2mt
c                        write(nlist,*)'start if'
                        if(job.eq.1)then
                        anofqd(pq)=anumer/denom
                        anofqd(qp)=anofqd(pq)
                        endif
                     endif
                  endif
               else
c                 ...illegal or unimplemented resolution type.
                  write(nlist,6010)pfield,mp,mq,anumer
                  write(nlist,*) 'pmask = ', pmask
                  write(nlist,*) 'qmask = ', qmask
                  write(nlist,*) 'mcres = ', mcres
                  error=.true.
                  stop 'error at 204'
               endif
c            write(nlist,*) '100 done'
100         continue
c         write(nlist,*) '200 done'
200      continue
c
c  inactive-inactive resolutions
c
c         write(nlist,*) 'start do 400'
         do 400 i=(ird1(isym)+1),ird2(isym)
            mi=mapmd(i)
            mir=mi-nsm(isym)
            ii=n2smi+(mir-1)*nm+mir
            do 300 j=ird1(isym),(i-1)
               mj=mapmd(j)
               mjr=mj-nsm(isym)
               jj=n2smi+(mjr-1)*nm+mjr
               ij=n2smi+(mjr-1)*nm+mir
               ji=n2smi+(mir-1)*nm+mjr
               if(job.eq.1)denom=fmc(jj)-fmc(ii)
               anumer=fci(ij)-fci(ji)
                if ((nadcalc.eq.3).or.(nadcalc.eq.2)) then
                 anumer = anumer + d1atr(mi,mj)*(energy1-energy2)
                 if(print.ge.1)then              
                  write(nlist,*)'nad:redundant:d-d anumer',anumer
                  write(nlist,*)'ij,ji,fci(ij),fci(ji): ',
     &             ij,ji,fci(ij),fci(ji)
                write(nlist,*)'mi,mj,d1atr(mi,mj): ',mi,mj,d1atr(mi,mj)
                  write(nlist,*)'energy1-energy2: ',energy1-energy2
                 endif 
                endif
               if(abs(anumer).lt.wndtol)then
c                 ...neglible contribution, just ignore it.
               else
                  if(abs(denom).lt.wndtol)then
c                    ...can't resolve due to orbital energy degeneracy.
                     write(nlist,6030)mi,mj,anumer,denom
                     error=.true.
                     stop 'error at 230'
                  else
c                    ...d.o. resolution is required.
                     qfresd=.true.
                     numado=n2mt
                     if(job.eq.1)then
                        ado(ij)=anumer/denom
                        ado(ji)=ado(ij)
                     endif
                  endif
               endif
300         continue
400      continue
c
c  virtual-virtual resolutions
c
         do 600 a=(irv1(isym)+1),irv2(isym)
            ma=mapmv(a)
            mar=ma-nsm(isym)
            aa=n2smi+(mar-1)*nm+mar
            do 500 b=irv1(isym),(a-1)
               mb=mapmv(b)
               mbr=mb-nsm(isym)
               bb=n2smi+(mbr-1)*nm+mbr
               ab=n2smi+(mbr-1)*nm+mar
               ba=n2smi+(mar-1)*nm+mbr
               if(job.eq.1)denom=fqdmc(bb)-fqdmc(aa)
               anumer=fci(ab)-fci(ba)
                if ((nadcalc.eq.3).or.(nadcalc.eq.2)) then
                 anumer = anumer + d1atr(ma,mb)*(energy1-energy2)
                 if(print.ge.1)then           
                  write(nlist,*)'nad:redundant:v-v anumer',anumer
                  write(nlist,*)'ab,ba,fci(ab),fci(ba): ',
     &             ab,ba,fci(ab),fci(ba)
                 write(nlist,*)'ma,mb,d1atr(ma,mb): ',ma,mb,d1atr(ma,mb)
                  write(nlist,*)'energy1-energy2: ',energy1-energy2
                 endif
                endif
               if(abs(anumer).lt.wnvtol)then
c                    ...neglible contribution, just ignore it.
               else
                  if(abs(denom).lt.wnvtol)then
c                    ...can't resolve due to orbital energy degeneracy.
                     write(nlist,6040)ma,mb,anumer,denom
                     error=.true.
                     stop 'error at 265'
                  else
c                    ...v.o. resolution is required.
                     qfresv =.true.
                     numav = n2mt
                     if (job.eq.1) then
                       af(ab) = anumer/denom
                       af(ba) = af(ab)
                     end if
                  endif
               endif
500         continue
600      continue
c
900   continue
c
      if(qfresd .and. fresdd.eq.0)then
         write(nlist,*)'d.o. resolution is required but fresdd=0'
         error=.true.
         stop 'error at 283'
      endif
      if(qfresa .and. fresaa.eq.0)then
         write(nlist,*)'a.o. resolution is required but fresaa=0'
         error=.true.
         stop 'error at 289'
      endif
      if(qfresv .and. fresvv.eq.0)then
         write(nlist,*)'v.o. resolution is required but fresvv=0'
         error=.true.
         stop 'error at 294'
      endif
      if(error)call bummer('iosres: resolution errors encountered',
     & 0,faterr)
      return
6010  format('iosres: pfield,mp,mq,anumer=',3i4,e13.8)
6020  format('iosres: n.o. mp,mq,anumer,denom=',2i4,2e15.8)
6025  format('iosres: f.o. mp,mq,anumer,denom=',2i4,2e15.8)
6030  format('iosres: d.o. mi,mj,anumer,denom=',2i4,2e15.8)
6040  format('iosres: v.o. ma,mb,anumer,denom=',2i4,2e15.8)
6050  format('iosres: a.o. ma,mb,dmc(pq)=     ',2i4,1e15.8)
      end
c
c***********************************************************************
c
c deck lmicro
      subroutine lmicro(
     & avcsl2,   nmiter,   rtol,     dtol,
     & b,        ib,       w,        fcsf,
     & hmcvec,   bdiag,    mdiag,    naar,
     & ncfx,     noldr,    nnewr,    nolds,
     & nnews,    ndimr,    nmaxr,    ndims,
     & nmaxs,    r,        bxr,      cxr,
     & s,        cxs,      mxs,      br,
     & cr,       mr,       wr,       fr,
     & zr,       pr,       scr,      kpvt,
     & print,    orbl,     csfl,     xp,
     & z,        xbar,     rev,      ind,
     & modrt,    u1,       core,     lenbuf,
     & nwalk,    add,      emc,      lastb,
     & nipbuk,   lenbuk,   iorder,   ib_c,
     & ib_m)
c
c  micro-iterative solution of the mcscf linear equation for the
c  lambda vector.
c
c  input:
c  nmiter  = maximum number of iterations.
c  rtol    = tolerance on residual norms.
c  dtol    = small diagonal-element tolerance.
c  b       = hessian matrix blocks (b,c, and m).
c  ib      = pointers to matrix blocks (0 means block is not in core).
c  w       = gradient vectors (in the order: ad,aa,vd,va).
c  fcsf    = csf gradient vector.
c  hmcvec  = mcscf hamiltonian eigenvector.
c  bdiag   = diagonal elements of the complete b matrix.
c  mdiag   = diagonal elements of the m matrix.
c  nadt    = number of active-inactive rotations.
c  naar    = number of active-active rotations.
c  nvdt    = number of virtial-inactive rotations.
c  nvat    = number of virtual-active rotations.
c  ncfx    = either 0 or the number of csfs.  if 0 then the csf-related
c            matrices are not referenced.
c  nold*   = number of old trial vectors (m-v products already exist).
c  nnew*   = number of new trial vectors (m-v products must be formed).
c  ndim*   = column length of expansion vectors r and s.
c  nmax*   = maximum number of expansion vectors r and s (.ge.2).
c  r,s     = expansion vector workspace.
c  bx,cxr,cxs,mxs = matrix-vector workspace.
c  br,cr,mr,wr,fr,zr,pr = reduced matrix and vector workspace.
c  kpvt    = integer workspace for reduced linear equation solution.
c  print  = integer print flag.
c
c  output:
c  orbl    = orbital part of the lambda vector.
c  csfl    = csf part of the lambda vector.
c
c  written by ron shepard.
c  6-june-88  fcsf(*) added (rls).
c  15-july-87 modified from routine micro() from program uexp2 (rls).
c  15-feb-85 original micro() version date (rls).
c
c  this version employs an in-core method where the b(*) blocks, c(*)
c  blocks, and the m(*) matrix are held in memory.  it is presumed
c  that nmaxr and nmaxs are both relatively small so that the
c  manipulations involving the subspace matrices are insignificant
c  compared to their construction effort.  the number of matrix-vector
c  and vector-vector operations in the full space are kept to a
c  minimum while retaining subspaces of maximal dimension to reduce
c  the number of iterations.  c(*) blocks and m(*) are expressed in
c  the csf basis instead of the orthogonal-complement basis.  this
c  requires projecting the current hmcvec(*) components from the
c  state-space expansion vectors.  it is presumed that the nnews
c  expansion vectors in s(*) on input are orthonormal and orthogonal
c  to hmcvec(*) and the nnewr expansion vectors in r(*) are
c  orthonormal.  note that nnewr and nnews cannot both be zero on
c  entry.
c
c  see routine micro() in program uexp2 for more details (rls).
c
       implicit none
c   ##   parameter & common section
c
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      real*8    zero,    half,     one,    two
      parameter(zero=0d0,half=5d-1,one=1d0,two=2.0d+00)
c
c     # bummer error types.
      integer   wrnerr,   nfterr,   faterr
      parameter(wrnerr=0, nfterr=1, faterr=2)
c
      integer nfilmx
      parameter(nfilmx=30)
      integer iunits,nlist,hdiagf
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
      equivalence (iunits(18),hdiagf)
c
      integer nmomx
      parameter(nmomx=1023)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer invx(nmomx)
      equivalence (iorbx(1,5),invx(1))
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer addpt,numint,bukpt,intoff_mc,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),tsymvv(8),n2dt,n2at,namot,tsymad(8),tsymvd(8),
     &        tsymva(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxtot(9),n2dt)
      equivalence (nxtot(10),namot)
      equivalence (nxtot(12),n2at)
      integer nadt,nvdt,nvat
      equivalence (tsymad(1),nadt)
      equivalence (tsymvd(1),nvdt)
      equivalence (tsymva(1),nvat)
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
      integer prtmicro
      common/output/prtmicro
c
c   ##   integer section
c
      integer add(*),apt,avcsl2,avcsl3, bdim(4)
      integer browmap(10), bcolmap(10)
      integer cpt(15)
      integer dpt
      integer fpt
      integer ind(*), ib(15), i, iter, info, iad, iaa, ivd, iva, ij,
     &        iterx, i0, ij0, itotal, is, ir, iorder(*), ipt, ist,
     &        ib_m(maxstat), ib_c(4,maxstat), icount, ii, icount2, i2,
     &        ixx
      integer j
      integer kpvt(*)
      integer lastb(*),lenbuk,lenbuf
      integer modrt(0:*)
      integer nmiter,ncfx,noldr,nnewr,nolds,nnews,ndimr,nmaxr,
     &        ndims, nmaxs,nnoldr,naar,nipbuk,nwalk, nr, ns
      integer print
      integer rsdim
      integer vpt
      integer wpt
c
c   ##   real*8 section
c
c     # effective dimensions br(nnmaxr), cr(nmaxr,nmaxs), mr(nnmaxs)
c     #                      wr(nmaxr),kpvt(mxrps)
c     #                      scr( mxrps*(mxrps+3)/2 ), pr(nmaxs)
c     #                      zr(nmaxr)
c     # mxrps = nmaxr + nmaxs
c     # nmaxs may be zero (if ncsf=1) but nmaxr must be .ge. 2.
c     # ndimr must be > 0.
c
      real*8 b(*),bdiag(ndimr),bxr(ndimr,*),br(*)
      real*8 cxr(ndims,*), cxs(ndimr,*),core(*),cr(nmaxr,*),
     &       csfl(ndims)
      real*8 dtol
      real*8 emc
      real*8 fcsf(*),fr(*)
      real*8 hmcvec(*)
      real*8 knorm
      real*8 mdiag(*), mxs(ndims,*),mr(*)
      real*8 r(ndimr,*)
      real*8 s(ndims,*),scr(*)
      real*8 orbl(ndimr)
      real*8 pnorm,pr(*)
      real*8 rtol,rznorm,rpnorm,rev(*)
      real*8 term
      real*8 u1(*)
      real*8 w(*),wr(nmaxr)
      real*8 xp(*),xbar(*)
      real*8 zr(*),znorm,z(*)
c
c   ##   logical section
c
      logical qc(4)
      logical lcxr,lcxs
c
      character*5 btype(10)
      character*2 bchar(4)
c
      integer  atebyt,  forbyt
      external atebyt,  forbyt
c
      real*8   ddot_wr, dnrm2_wr
      external ddot_wr, dnrm2_wr
c
      integer nndx
      nndx(i) = (i * (i + 1)) / 2
c
c
      data btype/'ad,ad','aa,ad','aa,aa','vd,ad','vd,aa',
     &           'vd,vd','va,ad','va,aa','va,vd','va,va'/
c
      data bchar/'ad','aa','vd','va'/
c
      data browmap/1,2,2,3,3, 3,4,4,4,4/
      data bcolmap/1,1,2,1,2, 3,1,2,3,4/
c
c-----------------------------------------------------------------
c
c  ##  write out header and basis information
c
      write(nlist,6710)
      write(nlist,
     & '(3x,''Solution of the Linear System of Equations:'')')
      write(nlist,
     & '(/5x,''Desired residual norm convergence ='',1pe11.4)')rtol
      write(nlist,
     & '(5x,''Minimum denominater value: dtol ='',1pe11.4)')dtol
      write(nlist,
     & '(5x,''Maximum subspace dimension for r vector ='', i4)')nmaxr
      write(nlist,
     & '(5x,''Maximum subspace dimension for s vector ='', i4)')nmaxs
      write(nlist,'(5x,''Maximum number of iterations: nmiter ='',i4)')
     & nmiter
      write(nlist,'(5x,''Dimension of Lambda[orb]:'',i4)')ndimr
      write(nlist,'(5x,''Dimension of Lambda[csf]:'',i6,/)')ndims
c
c  #  initialize some variables
c
      iter   = 0
      info   = 0
      znorm  = zero
      pnorm  = zero
      rznorm = zero
      rpnorm = zero
c
c   # set up pointers to the various components of the r* vectors.
c
      iad = 1
      iaa = iad + nadt
      ivd = iaa + naar
      iva = ivd + nvdt
c
c  ## debug information
c
      bdim(1) = nadt
      bdim(2) = naar
      bdim(3) = nvdt
      bdim(4) = nvat
c
      if (prtmicro.ge.1) then
c
       call prtvec(nlist,'fcsf vector',fcsf,ncfx)
c
       if (ib(1).ne.0) call pbadad(b(ib(1)))
       if (ib(2).ne.0) call prblkt('baaad matrix',
     &  b(ib(2)),nadt,nadt,naar,'(ad)','(aa)',1,nlist)
       if (ib(3).ne.0) call plblkt('baaaa matrix',
     &  b(ib(3)),naar,'(aa)',1,nlist)
       if (ib(4).ne.0) call prblkt('bvdad matrix',
     &  b(ib(4)),nadt,nadt,nvdt,'(ad)','(vd)',1,nlist)
       if (ib(5).ne.0) call prblkt('bvdaa matrix',
     &  b(ib(5)),nvdt,nvdt,naar,'(vd)','(aa)',1,nlist)
       if (ib(6).ne.0) call pbvdvd(b(ib(6)))
       if (ib(7).ne.0) call pbvaad(b(ib(7)))
       if (ib(8).ne.0) call prblkt('bvaaa matrix',
     &  b(ib(8)),nvat,nvat,naar,'(va)','(aa)',1,nlist)
       if (ib(9).ne.0) call prblkt('bvavd matrix',
     &  b(ib(9)),nvat,nvat,nvdt,'(va)','(vd)',1,nlist)
       if (ib(10).ne.0) call pbvava(b(ib(10)))
cv
         do ist=1,nst
         write(nlist,
     &    '(/2x,''C matrix DRT Nr. '',i2)') ist
             if (ib_c(1,ist).ne.0) call prblkt('cad matrix',
     &        b(ib_c(1,ist)),ncsf_f(ist),ncsf_f(ist),nadt,
     &        ' csf','(ad)',1,nlist)
             if (ib_c(2,ist).ne.0) call prblkt('caa matrix',
     &        b(ib_c(2,ist)),ncsf_f(ist),ncsf_f(ist),naar,
     &        ' csf','(aa)',1,nlist)
             if (ib_c(3,ist).ne.0) call prblkt('cvd matrix',
     &        b(ib_c(3,ist)),ncsf_f(ist),ncsf_f(ist),nvdt,
     &        ' csf','(vd)',1,nlist)
             if (ib_c(4,ist).ne.0) call prblkt('cva matrix',
     &        b(ib_c(4,ist)),ncsf_f(ist),ncsf_f(ist),nvat,
     &        ' csf','(va)',1,nlist)
         enddo ! ist=1,nst
c
      endif ! (prtmicro.ge.1)
c
c     1:udd,2:uad,3:uaa,4:uvd,5:uva,6:uvv,7:ftbuf,
c     8:h2,9:hdiag,10:dabuf,11:intlabbuf
      cpt(1) = 1
      if ((mdir.eq.1).or.(cdir.eq.1)) then
       cpt(2)=cpt(1)+atebyt(n2dt)
       cpt(3)=cpt(2)+atebyt(nadt)
       cpt(4)=cpt(3)+atebyt(n2at)
       cpt(5)=cpt(4)+atebyt(nvdt)
       cpt(6)=cpt(5)+atebyt(nvat)
       cpt(7)=cpt(6)+atebyt(tsymvv(1))
       cpt(8)=cpt(7)+atebyt(lenbuf)
       cpt(9)=cpt(8)+numint(1)
       cpt(10)=cpt(9)+ncfx
       cpt(11)=cpt(10)+lenbuk
       cpt(12)=cpt(11)+forbyt(nipbuk+1)
      else
       cpt(2)=cpt(1)
       cpt(3)=cpt(2)
       cpt(4)=cpt(3)
       cpt(5)=cpt(4)
       cpt(6)=cpt(5)
       cpt(7)=cpt(6)
       cpt(8)=cpt(7)
       cpt(9)=cpt(8)
       cpt(10)=cpt(9)
       cpt(11)=cpt(10)
       cpt(12)=cpt(11)
      endif
      itotal=cpt(12)
      call memcheck(itotal,avcsl2,'lmicro 1')
6010  format(/' *** error *** lmicro: info (from wspfa) =',i8)
c
c   ## read Hdiag elements form previous MCSCF calculation
c   ## and construct the diagonal M =2(H-Emc) vector
c
      if (mdir.eq.1) then
       rewind(hdiagf)
       icount=0
       icount2=0
          do ist=1,nst
          read(hdiagf)(core(cpt(9)+icount+i2-1),i2=1,ncsf_f(ist))
             do j=1,navst(ist)
                do i=1,ncsf_f(ist)
                mdiag(icount2+i) = core(cpt(9)+icount+i-1)-heig(ist,j)
                mdiag(icount2+i) = mdiag(icount2+i)*two*wavst(ist,j)
                enddo ! i=1,ncsf_f(j)
             icount2 = icount2 + ncsf_f(ist)
             enddo ! j=1,navst(ist)
           icount = icount + ncsf_f(ist)
           enddo ! ist=1,nst
      endif ! (mdir.eq.1)

       if (prtmicro.ge.1) then
        icount = 1
          do ist=1,nst
             do i=1,navst(ist)
             write(nlist,
     &        '(/,2x,''Mdiagonal for DRT Nr.'',i3,'' state Nr.'',i3)')
     &        ist, i
             call prtvec(nlist,'Mdiag',mdiag(icount),ncsf_f(ist))
             icount = icount + ncsf_f(ist)
             enddo ! i=1,navst(ist)
          enddo ! i=1,navst(ist)
        endif ! (prtmicro.ge.1)
1000  format(9d13.7)
c  ##
c  ## read the aaaa block of integrals
c  ##
      if ((mdir.eq.1).or.(cdir.eq.1)) then
c
c      1:udd,2:uad,3:uaa,4:uvd,5:uva,6:uvv,7:ftbuf,
c      8:h2,9:hdiag,10:dabuf,11:intlabbuf
       cpt(11)=cpt(10)+lenbuk
       itotal=cpt(11)+forbyt(nipbuk+1)
       call memcheck(itotal,avcsl2,'lmicro 2')
c
         if (numint(1).ne.0) then
          call wzero(numint(1),core(cpt(8)),1)
          call rdbks_md(lastb(bukpt(1)),core(cpt(8)),core(cpt(10)),
     &     core(cpt(11)),lenbuk,nipbuk)
         endif ! end of: if (numint(1).ne.0) then
c
       dpt=ix0(1)+1
       apt=ix0(2)+1
       vpt=ix0(3)+1
c
c      1:udd,2:uad,3:uaa,4:uvd,5:uva,6:uvv,7:ftbuf,
c      8:h2,9:mdiag
       call wzero(cpt(7)-1,core(cpt(1)),1)
       call tltpsp(invx(dpt),invx(apt),invx(vpt),u1,core(cpt(1)),
     &             core(cpt(2)),core(cpt(3)),
     &             core(cpt(4)), core(cpt(5)),core(cpt(6)))
      endif ! ((mdir.eq.1).or.(cdir.eq.1))
c
c     # set up reduced matrices br,cr,mr,wr for any old vectors.
c     # matrix-vector products have already been calculated for these
c     # vectors and passed in through the argument list.
c
      ij= 0
      do 12 i = 1, noldr
         do 10 j = 1, i
            ij = ij + 1
            br(ij) = ddot_wr( ndimr, r(1,i), 1, bxr(1,j), 1 )
10       continue
12    continue
c
      do 22 i = 1, nolds
         do 20 j = 1, noldr
            cr(j,i) = ddot_wr( ndimr, r(1,j), 1, cxs(1,i), 1 )
20       continue
22    continue
c
      ij = 1
      do 32 i = 1, nolds
         do 30 j = 1, i
            ij = ij+1
            mr(ij) = ddot_wr( ncfx, s(1,i), 1, mxs(1,j), 1 )
30       continue
32    continue
c
      do 40 i = 1, noldr
         wr(i) = ddot_wr( ndimr, w, 1, r(1,i), 1 )
40    continue
c
      do 50 i = 1, nolds
         fr(i) = ddot_wr( ncfx, fcsf, 1, s(1,i), 1 )
50    continue
c
c     # begin iteration loop:
c
      do 600 iterx = 1, nmiter
         iter = iterx
         if (prtmicro.ge.1)
     &    write(nlist,'(/''###  Iter ='',i2,''  ###''/)')iter
c
c        # form matrix-vector products for any new trial r(*) vectors.
c        # vectors orbl(*) and csfl(*) are used for local scratch space.
c
         do 100 i = (noldr + 1), nnewr
c
c        # form matrix-vector products for any new trial s(*) vectors.
c
            call bxrprd(
     &       b,      ib,        ndimr,  r(1,i),
     &       ncfx,   nadt,      naar,   nvdt,
     &       nvat,   iad,       iaa,    ivd,
     &       iva,    orbl,      csfl,   bxr(1,i),
     &       ndims,  cxr(1,i),  ib_c,   ib_m)
100      continue
c
c        # form matrix-vector products for any new trial s(*) vectors.
c
c     1:udd,2:uad,3:uaa,4:uvd,5:uva,6:uvv,7:ftbuf,
c     8:h2,9:mdiag
            do 120 i = (nolds + 1), nnews
            call mxsprd(
     &       b,            ib,           ndims,        s(1,i),
     &       ncfx,         nadt,         naar,         nvdt,
     &       nvat,         iad,          iaa,          ivd,
     &       iva,          mxs(1,i),     ndimr,        cxs(1,i),
     &       xp,           z,            xbar,         rev,
     &       ind,          modrt,        core(cpt(3)), core(cpt(7)),
     &       core(cpt(8)), lenbuf,       nwalk,        nsym,
     &       add,          core(cpt(9)), emc,          ib_c,
     &       ib_m)
120         continue
c
          is = nolds+1
          ir = noldr+1
          ns = nnews - nolds
          nr = nnewr - noldr
c
c     1:udd,2:uad,3:uaa,4:uvd,5:uva,6:uvv,7:ftbuf,
c     8:h2,9:mdiag,10:sscr
            call cxvec(
     &       b,            ib,           ndims,        s(1,is),
     &       r(1,ir),      ncfx,         nadt,         naar,
     &       nvdt,         nvat,         iad,          iaa,
     &       ivd,          iva,          ndimr,
     &       cxs(1,is),    cxr(1,ir),    xp,           z,
     &       xbar,         rev,          ind,          modrt,
     &       core(cpt(3)), core(cpt(7)), core(cpt(8)), lenbuf,
     &       nwalk,        nsym,         add,          core(cpt(9)),
     &       emc,          ib_c,         ib_m,         ns,
     &       nr,           csfl,         avcsl2-itotal,nipbuk,
     &       lastb,        lenbuk,       hmcvec,       core(cpt(10)),
     &       core(cpt(2)), core(cpt(4)), core(cpt(5)), iorder)
c
         if ((prtmicro.ge.1).and.(ns.ne.0)) then
          write(nlist,'(/,3x,''Total Cxs for the iteration Nr.'',i3)')
     &     iter
          call prtvec(nlist,'Cxs[ad]',cxs(iad,is),nadt)
          call prtvec(nlist,'Cxs[aa]',cxs(iaa,is),naar)
          call prtvec(nlist,'Cxs[vd]',cxs(ivd,is),nvdt)
          call prtvec(nlist,'Cxs[va]',cxs(iva,is),nvat)
         endif
         if ((prtmicro.ge.1).and.(nr.ne.0)) then
          write(nlist,'(/,3x,''Total Cxr for the iteration Nr.'',i3)')
     &     iter
          call prtvec(nlist,'Cxr[csf]',cxr(1,ir),ncfx)
         endif
c
c        # form reduced matrix elements for new vectors r and s.
c
1250     continue
         ij = nndx( noldr )
         do 142 i = (noldr + 1), nnewr
            do 140 j = 1, i
               ij = ij + 1
               br(ij) = ddot_wr( ndimr, r(1,i), 1, bxr(1,j), 1 )
140         continue
142      continue
c
         do 152 i = 1, nolds
            do 150 j = (noldr + 1), nnewr
               cr(j,i) = ddot_wr( ndimr, r(1,j), 1, cxs(1,i), 1 )
150         continue
152      continue
c
         do 162 i = (nolds + 1), nnews
            do 160 j = 1, nnewr
               cr(j,i) = ddot_wr( ndimr, r(1,j), 1, cxs(1,i), 1 )
160         continue
162      continue
c
         ij = nndx( nolds )
         do 172 i = (nolds + 1), nnews
            do 170 j = 1, i
               ij     = ij + 1
               mr(ij) = ddot_wr( ncfx, s(1,i), 1, mxs(1,j), 1 )
170         continue
172      continue
c
         do 180 i = (noldr + 1), nnewr
            wr(i) = ddot_wr( ndimr, w, 1, r(1,i), 1 )
180      continue
c
         do 190 i = (nolds + 1), nnews
            fr(i) = ddot_wr( ncfx, fcsf, 1, s(1,i), 1 )
190      continue
c
c        # update the vector counts.
c
         noldr = nnewr
         nolds = nnews
c
         if ( print .ge. 3 ) then
            call plblkt( 'br(*) matrix', br, noldr, 'r(*)', 2, nlist )
            if ( nolds .ne. 0 ) then
               call plblkt( 'mr(*) matrix',
     &          mr, nolds, 's(*)', 2, nlist )
               call prblkt( 'cr(*) matrix',
     &          cr, nmaxr, noldr, nolds, 'r(*)', 's(*)', 2, nlist )
            endif
         endif
c
c        # solve the mcscf linear equations in the reduced space.
c        #
c        # ( b  c ) (z) = -(w)      !all matrices in the reduced space
c        # ( ct m ) (p)    (f)
c
c        # copy b(*) to the lower triangle of scr(*).
c
         nnoldr = nndx( noldr )
         call dcopy_wr( nnoldr, br, 1, scr, 1 )
c
c        # copy cr(*) to the lower triangle of scr(*).
c
         do 212 i = 1, nolds
            i0 = nndx( noldr + i-1 )
            do 210 j = 1, noldr
               scr(i0+j) = cr(j,i)
210         continue
212      continue
c
c        # copy mr(*) to the lower triangle of scr(*).
c
         ij0 = 0
         do 222 i = 1, nolds
            i0 = nndx( noldr + i-1 ) + noldr
            do 220 j = 1, i
               scr(i0+j) = mr(ij0+j)
220         continue
            ij0 = ij0 + i
222      continue
c
c        # copy wr(*) and fr(*) to scr(*) and change sign.
c
         rsdim = noldr + nolds
         wpt = nndx( rsdim ) + 1
         call dcopy_wr( noldr, wr, 1, scr(wpt), 1 )
         if ( nolds .ne. 0 ) then
            fpt = wpt + noldr
            call dcopy_wr( nolds, fr, 1, scr(fpt), 1 )
         endif
         call dscal_wr( rsdim, -one, scr(wpt), 1 )
c
c        # solve the reduced equation.
c        # move the solution to zr(*) and pr(*).
c
         call wspfa( scr, rsdim, kpvt, info )
         if ( info .ne. 0 ) then
            call bummer( 'lmicro: wspfa info=', info, faterr )
         endif
         call wspsl( scr, rsdim, kpvt, scr(wpt) )
         call dcopy_wr( noldr, scr(wpt), 1, zr, 1 )
         znorm = dnrm2_wr( noldr, zr, 1 )
         if ( nolds .ne. 0 ) then
            call dcopy_wr( nolds, scr(fpt), 1, pr, 1 )
            pnorm = dnrm2_wr( nolds, pr, 1 )
         endif
c
c        # use z(*) and pr(*) to form residual vectors.
c        # store residual vectors in orbl(*) and csfl(*).
c        #
c        # res(z)= b*z + c*p + w
c
         call wzero( ndimr, orbl, 1 )
         do 320 i = 1, noldr
            term = zr(i)
            if ( term .ne. zero) call daxpy_wr(
     &       ndimr, term, bxr(1,i), 1, orbl, 1 )
320      continue
         do 340 i = 1, nolds
            term = pr(i)
            if ( term .ne. zero ) call daxpy_wr(
     &       ndimr, term, cxs(1,i), 1, orbl, 1 )
340      continue
         call daxpy_wr( ndimr, one, w, 1, orbl, 1 )
         rznorm = dnrm2_wr( ndimr, orbl, 1 )
c
c        # form residual vector for the current csfl(*) vector.
c        #
c        # r(p) = ct*z + m*p + fcsf
c
         if ( ncfx .ne. 0 ) then
            call wzero( ncfx, csfl, 1 )
            do 360 i = 1, noldr
               term = zr(i)
               if ( term .ne. zero ) call daxpy_wr(
     &          ncfx, term, cxr(1,i), 1, csfl, 1 )
360         continue
            do 380 i = 1, nolds
               term = pr(i)
               if ( term .ne. zero ) call daxpy_wr(
     &          ncfx, term, mxs(1,i), 1, csfl, 1 )
380         continue
            call daxpy_wr( ncfx, one, fcsf, 1, csfl, 1 )
c
c           # since c(*), m(*), and f(*) are in the csf basis, project
c           # out any hmcvec(*) components.
c
c           term = -ddot_wr( ncfx, hmcvec, 1, csfl, 1 )
c           if ( term .ne. zero ) call daxpy_wr(
c    &       ncfx, term, hmcvec, 1, csfl, 1 )
c
            ii =1
               do ist=1,nst
                  do i=1,navst(ist)
                  term = -ddot_wr( ncsf_f(ist),hmcvec(ii),1,csfl(ii),1)
                   if ( term .ne. zero ) call daxpy_wr(
     &              ncsf_f(ist), term, hmcvec(ii), 1, csfl(ii), 1 )
                  ii = ii+ncsf_f(ist)
                  enddo ! i=1,navst(ist)
               enddo ! ist=1,nst
c
            rpnorm = dnrm2_wr( ncfx, csfl, 1 )
         endif ! ( ncfx .ne. 0 )
c
         if ( (rznorm .le. rtol) .and. (rpnorm .le. rtol) ) go to 610
c
c        # residuals are too big.  form new expansion vectors and
c        # continue the micro-iterations.
c
         if ( rznorm .gt. rtol ) then
            do 400 i = 1, ndimr
               if ( abs(bdiag(i)) .lt. dtol ) then
                  orbl(i) = orbl(i) / dtol
               else
                  orbl(i) = orbl(i) / bdiag(i)
               endif
400         continue
c
            if ( noldr .eq. nmaxr ) then
c              # compress the nmaxr r(*) vectors to smaller subspace.
               call rcmprs(
     &          nmaxr,  ndimr,  ndims,  noldr,
     &          nolds,  ncfx,   br,     cr,
     &          wr,     zr,     r,      bxr,
     &          cxr,    scr  )
               nnewr = noldr
            endif ! ( noldr .eq. nmaxr )
c
c           # orthonormalize the new expansion vector r(*) to the
c           # previous r(*) vectors.
c
            do 460 i = 1, noldr
               term = -ddot_wr( ndimr, orbl, 1, r(1,i), 1 )
               if ( term .ne. zero ) call daxpy_wr(
     &          ndimr, term, r(1,i), 1, orbl, 1 )
460         continue
c
            term = dnrm2_wr( ndimr, orbl, 1 )
            if ( (one + term) .ne. one ) then
               term = one / term
               call dscal_wr( ndimr, term, orbl, 1 )
               nnewr = nnewr + 1
               call dcopy_wr( ndimr, orbl, 1, r(1,nnewr), 1 )
            endif ! ( (one + term) .ne. one )
            if (prtmicro.ge.1) call
     &       prtvec(nlist,'R vector',r(1,nnewr),ndimr)
         endif ! ( rznorm .gt. rtol )
c
c        # form the new expansion vector s(*).
c
         if ( rpnorm .gt. rtol ) then
            do 500 i = 1, ncfx
               if ( abs(mdiag(i)) .lt. dtol ) then
                  csfl(i) = csfl(i) / dtol
               else ! ( abs(mdiag(i)) .lt. dtol )
                  csfl(i) = csfl(i) / mdiag(i)
               endif ! ( abs(mdiag(i)) .lt. dtol )
500         continue
c
            if ( (nolds .ne. 0) .and. (nolds .eq. nmaxs) ) then
c              # compress the nmaxs s(*) vectors to a smaller subspace.
               call scmprs(
     &          nmaxr,  ndimr,  ndims,  noldr,
     &          nolds,  ncfx,   mr,     cr,
     &          fr,     pr,     s,      mxs,
     &          cxs,    scr  )
               nnews = nolds
            endif ! ( (nolds .ne. 0) .and. (nolds .eq. nmaxs) )
c
c           # orthonormalize the new expansion vector s(*).
c
c           term = -ddot_wr( ncfx, csfl, 1, hmcvec, 1 )
c           if ( term .ne. zero ) call daxpy_wr(
c    &       ncfx, term, hmcvec, 1, csfl, 1 )
c
            ii=1
               do ist=1,nst
                  do i=1,navst(ist)
                  term = -ddot_wr(ncsf_f(ist),csfl(ii),1,hmcvec(ii),1)
                   if ( term .ne. zero ) call daxpy_wr(
     &              ncsf_f(ist),term,hmcvec(ii),1,csfl(ii),1)
                  ii = ii+ncsf_f(ist)
                  enddo ! i=1,navst(ist)
               enddo ! ist=1,nst
c
            do 560 i = 1, nolds
               term = -ddot_wr( ncfx, csfl, 1, s(1,i), 1 )
               if( term .ne. zero ) call daxpy_wr(
     &          ncfx, term, s(1,i), 1, csfl, 1 )
560         continue
c
c  #  in order to guarantee a correct results for dummy states
c  #  the dnrm2_wr norm is changes to and abs norm
c           term = dnrm2_wr( ncfx, csfl, 1 )
            term = zero
               do i=1,ncfx
               term = term + abs(csfl(i))
               enddo ! i=1,ncfx
            if ( (one + term) .ne. one ) then
               term = one / term
               call dscal_wr( ncfx, term, csfl, 1 )
               nnews = nnews + 1
               call dcopy_wr( ncfx, csfl, 1, s(1,nnews), 1 )
            endif ! ( (one + term) .ne. one )
            if (prtmicro.ge.1) then
             icount = 1
               do ist=1,nst
                  do ixx=1,navst(ist)
                  write(nlist,
     &         '(/,5x,''S vector for DRT Nr.'',i3,'' state Nr.'',i3)')
     &             ist,ixx
                  call prtvec(nlist,'S vector',s(icount,nnews),
     &             ncsf_f(ist))
                  icount = icount + ncsf_f(ist)
                  enddo ! ixx=1,navst(ist)
               enddo ! ist=1,nst
            endif ! (prtmicro.ge.1)
         endif ! ( rpnorm .gt. rtol )
c
c        # end of the micro-iteration.
c
         write(nlist,6040) iter, znorm, pnorm, rznorm, rpnorm,
     &    noldr, nnewr, nolds, nnews
c
c        # check to see if residuals were large but no new vectors
c        # could be added.  if so, then different starting guesses
c        # are required.
c
         if ( (noldr .eq. nnewr) .and. (nolds .eq. nnews) ) go to 601
c
600   continue
601   continue
c
c     # exit from loop means convergence was not reached.
c
      call bummer ('*** lmicro: convergence not reached in iter=',
     &     iter,0)
      write(nlist,'(/,8x,''Possibile solutions:'')')
      write(nlist,'(5x,''1. Check, if MCSCF has converged.'')')
      write(nlist,'(5x,''2. Check the resolution in MCSCFIN file'')')
      write(nlist,
     &'(/,5x,''3. Increase the number of iterations in cigrdin.'')')
      write(nlist,
     &'(5x,''4. Use LAPACK for solving: set solvelpck=1 ''/)')
      write(nlist,'(5x,''5. Play it again Sam...''/)')
      go to 620
c
610   continue
c
      write(nlist,*) 'lmicro: final convergence values.'
      write(nlist,6040) iter, znorm, pnorm, rznorm, rpnorm,
     & noldr, nnewr, nolds, nnews
c
c     # compute the final csfl(*) vector.
c
620   if ( ncfx .ne. 0 ) then
         call wzero( ncfx, csfl, 1 )
         do 720 i = 1, nolds
            term = pr(i)
            if ( term .ne. zero ) call daxpy_wr(
     &       ncfx, term, s(1,i), 1, csfl, 1 )
720      continue
         if (prtmicro.ge.1) then
          call prblkt('final csfl(*):',csfl,
     &     ncfx,ncfx,1,'csf','vec',1,nlist)
         endif
      endif
c
c     # compute the final orbl(*) vector.
c
      call wzero( ndimr, orbl, 1 )
c
      do 740 i = 1, noldr
         term = zr(i)
         if ( term .ne. zero ) call daxpy_wr(
     &    ndimr, term, r(1,i), 1, orbl, 1 )
740   continue
      if (prtmicro.ge.1) call
     & prblkt('final orbl(*):',orbl,
     & ndimr,ndimr,1,'row','vec',1,nlist)
c
      write(nlist,6710)
      return
6040  format(' miter=',i3,' |orbl|=',1pe11.4,' |csfl|=',e11.4,
     & ' rznorm=',e11.4,' rpnorm=',e11.4,
     & ' noldr=',i3,' nnewr=',i3,' nolds=',i3,' nnews=',i3)
6710  format(/1x,60('*')/)
      end
c
c****************************************************************
c
      subroutine madad(b,x,y,s)
c
c  form the matrix-vector product  b * x  using the scratch
c  vector s and update the vector y.
c
c  y(ip) = y(ip) + sum(jq) b(ip,jq)*x(jq)
c
c  written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),namot
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxtot(10),namot)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 b(*),x(*),y(*),s(*)
c
c   ##   integer section
c
      integer i,ip
      integer j,jq
      integer ni,nj
      integer p,pq,psym
      integer qsym,q

c
      pq=1
      ip=1
      do 400 p=1,namot
         psym=symm(mapma(p))
         ni=ndpsy(psym)
         if(ni.eq.0)go to 400
c
         jq=1
         do 300 q=1,p
            qsym=symm(mapma(q))
            nj=ndpsy(qsym)
            if(nj.eq.0)go to 300
c
c  form s(ip)=sum(j) b(j,i,pq)*x(jq)  and update y(*).
c  b(nj,ni), x(nj), s(ni).
c
            call mtxv(b(pq),ni,x(jq),nj,s)
            do 100 i=1,ni
               y(ip-1+i)=y(ip-1+i)+s(i)
100         continue
            if(p.ne.q)then
c
c  form s(jq)=sum(i) b(j,i,pq)*x(ip)  and update y(*).
c  b(nj,ni), x(ni), s(nj).
c
               call mxv(b(pq),nj,x(ip),ni,s)
               do 200 j=1,nj
                  y(jq-1+j)=y(jq-1+j)+s(j)
200            continue
            endif
            pq=pq+ni*nj
            jq=jq+nj
300      continue
         ip=ip+ni
400   continue
c
      return
      end
c
c**********************************************************************
c
      subroutine memcheck(reqmem,availmem,title)
c
        implicit none
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c
      integer nfilmx
      parameter(nfilmx=30)
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
       integer availmem, reqmem
       character*(*) title
       if (reqmem.gt.availmem) then
        write(nlist,
     &   '(//,3x,"requested mem./available mem.: ",i10," / ",i10)')
     &   reqmem,availmem
        call bummer(title,reqmem,faterr)
       endif ! (reqmem.gt.availmem)

      return
      end
c
c**********************************************************************
c
c deck rdhcid
      subroutine rdhcidtr(
     & nlist,   denfl,   infocd,   ntitle,
     & title,   nsym,    nmot,     nbpsy,
     & cidlab)
c
c  read the header info from the ci transition density matrix file.
c
c  written by ron shepard.
c
       implicit none
      character*80 title(*)
      integer infocd(*)
      integer ntitle,nsym,nmot,ninfo,nenrgy,ierr,nmap
      logical error
      integer nmomx
      parameter (nmomx=1023)
      integer i, ntitmx, denfl,nlist,idummy, nsymx, nengmx
      parameter( ntitmx = 30, nsymx=8, nengmx=20)
      real*8 cienrg(nengmx)
      character*4 slabel(nsymx)
      character*8 cidlab(nmomx)
      integer nbpsy(nsymx),cietyp(nengmx),nbpsyn(nsymx)

       real*8 energy
       integer ienergy
       common /sifcid/ energy,ienergy
c
      real*8    one,     zero
      parameter(one=1d0, zero=0d0)
c
      integer nmotn, nsymn
c
      real*8 energy1,energy2
      integer nadcalc,drt1,drt2,root1,root2
      common /nad/ energy1,energy2,nadcalc,drt1,drt2,root1,root2
c
      integer numopt
      parameter(numopt=10)
c
      integer cigopt, print, fresdd, fresaa, fresvv, samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
      error = .false.
c
      rewind denfl
      call sifrh1(
     1 denfl, ntitle, nsymn, nmotn,
     2 ninfo, nenrgy, nmap,  ierr)
c
      if (ierr .ne. 0) then
         call bummer('rdhcid: from sifrh1, ierr=',ierr,faterr)
      elseif (ntitle .gt. ntitmx) then
         call bummer('rdhcid: from sifrh1, ntitle=',ntitle,faterr)
      elseif (ninfo .gt. 10) then
         call bummer('rdhcid: from sifrh1, ninfo=',ninfo,faterr)
      elseif (nsymn .ne. nsym) then
         call bummer('rdhcid: nsym inconsistant ',nsymn,wrnerr)
      elseif (( nenrgy .ne. 1).and.(samcflag.eq.0)) then
         call bummer('rdhcid: from sifrh1, nenrgy=',nenrgy,faterr)
      elseif (nmot .ne. nmotn) then
         call bummer('rdhcid: inconsistent nmot, nmotn=',nmotn,faterr)
      endif
c     #ignore map
      nmap = 0
      call sifrh2(
     1 denfl,   ntitle,  nsym,    nmot,
     2 ninfo,   nenrgy,  nmap,    title,
     3 nbpsyn,  slabel,  infocd,  cidlab,
     4 cietyp,  cienrg,  idummy,  idummy,
     5 ierr)
c     write(nlist,*)'ntitle=',ntitle
c     write(*,*)'nbpsy=',nbpsy
c
cfp: for SA-MCSCF gradients or couplings the energies are read in here
      if (samcflag.gt.0) then
cfp: check if those are really MCSCF energies
        if (cietyp(1).ne.-1025) then
         call bummer('rdhcidtr: cietyp(1)=',cietyp(1),faterr)
        elseif (cietyp(2).ne.-1025) then
         call bummer('rdhcidtr: cietyp(2)=',cietyp(2),faterr)
        endif
        energy1 = cienrg(1)
        energy2 = cienrg(2)
cfp_tmp
      write(nlist,*)'rdhcidtr: energy1, energy2',energy1,energy2
      endif
      do i=1,nsym
         if(nbpsy(i).ne.nbpsyn(i))then
            write(nlist,6010)'nmpsy(*)',i,nbpsy(i),nbpsyn(i)
            error=.true.
         endif
      enddo ! end of: do i=1,nsym
      if (error) call bummer('rdhcid:  inconsitent nbpsy', 0, faterr)
c
6010  format(' *** rdhcid error *** ',a,3i8)
c     write(nlist,6020)(title(i),i=1,ntitle)
6020  format(/' ci density file header information:'/(1x,a))
      write(nlist,6040)(i,nbpsy(i),i=1,nsym)
6040  format(' (isym:nbpsy)',8(i3,':',i3))
      return
      end
c
c**********************************************************************
c
c deck rdcid1tr
      subroutine rdcid1tr(
     & ntape, lbuf,   nipbuf,  infomo,
     & buf,   ilab,   val,     d1, s1,
     & nndx,   nnmt,  ns,      sym,
     & nns,    map,   nsym,    nmpsy,
     & kntin,  symoff,assume_fc, mapmo_fc,nmpsy_fc)
c
c  read a 1-e density matrix
c
c  input:
c  ntape = input unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  infomo = info array
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  sym(*) = symmetry of each orbital
c  ns(*) = symmetry offset for orbitals
c  nns(*) = symmetry offset for lower triangular matrices
c  nnmt = total size of symmetric, symmetry-packed, lower-triangular-
c         packed matrix.
c  map(*) = orbital index mapping vector.
c
c  output:
c  d1(*) = 1-e density matrix
c
c  written by ron shepard.
c  modified by gsk
c
      implicit integer(a-z)
c
      integer   nipv,   msame,  nmsame,     nomore
      parameter(nipv=2, msame=0,nmsame = 1, nomore = 2 )
      real*8 buf(lbuf),d1(*),s1(*),val(nipbuf),fcore
      integer ilab(nipv,nipbuf),nndx(*),sym(*),ns(*),nns(*),map(*)
      integer nmpsy(*),mapmo_fc(*),nmpsy_fc(*) 
      integer infomo(*),kntin(*)
      integer   iretbv,    idummy(1),lasta,lastb
      parameter(iretbv = 0)
      real*8    zero
      parameter(zero = 0.0d0)
      integer    itypea,  itypbd
      parameter( itypea=2,itypbd=9)
      INTEGER             assume_fc,nndxof(8),ndxof(8)
      INTEGER             IJ, ISYM, IBF,JBF,IMO,JMO,N_JMO,N_IMO,TMP,IJNEW

c
      fcore= zero
c
c      write(*,*)'nmmt:',nnmt
      call wzero(nnmt,d1,1)
      call wzero(nnmt,s1,1)
      call sifr1x(
     1 ntape, infomo, itypea, itypbd,
     2 nsym,  nmpsy,  symoff, buf,
     3 val,   ilab,   map,    sym,
     4 nnmt,  s1,     fcore,  kntin,
     5 lasta,lastb,   last,   nrec,
     6 ierr)
      if ( ierr.ne. 0) then
        call bummer('rdden1: from sifr1x, ierr=',ierr,faterr)
      elseif (fcore .ne. zero) then
          write(6,*)' frozen core orbitals are not yet allowed'
        call bummer('rdden1: from sifr1x(), fcore=',fcore,faterr)
      endif

      if (assume_fc.eq.1) then
!
!     remap the one-electron density elements
!
      nndxof(1)=0
      ndxof(1)=0
      do isym=2,nsym
         nndxof(isym)=nndxof(isym-1)+
     . nmpsy_fc(isym-1)*(nmpsy_fc(isym-1)+1)/2
         ndxof(isym)=ndxof(isym-1)+nmpsy_fc(isym-1)
      enddo

      ij=0
      do isym=1,nsym
        do ibf=1,nmpsy_fc(isym)
         do jbf=1,ibf
          ij=ij+1
          imo=ndxof(isym)+ibf
          jmo=ndxof(isym)+jbf
          n_imo=mapmo_fc(imo)
          n_jmo=mapmo_fc(jmo)
          n_imo=n_imo-ns(isym)
          n_jmo=n_jmo-ns(isym)
           if (n_imo.lt.n_jmo) then
               tmp=n_imo
               n_imo=n_jmo
               n_jmo=tmp
           endif
         ijnew=nns(isym)+ n_imo*(n_imo-1)/2+n_jmo
         d1(ijnew)=s1(ij)
         enddo
        enddo
      enddo
      else
       call dcopy_wr(nnmt,s1,1,d1,1) 
      endif

      return
      end
c
c***********************************************************************
c
      subroutine symdet(nsym,nmpsy,lmda,lmdb,nmbpr)
c
       implicit none
      integer maxsym
      parameter(maxsym=8)
c
      integer i, is, ijs, ijsym,ix
      integer j, js
      integer lmda(maxsym,maxsym),lmdb(maxsym,maxsym)
      integer mult(maxsym,maxsym)
      integer nmbpr(maxsym),nmbj,nsym,nmpsy(maxsym)
      integer sym12(maxsym),sym21(maxsym)
c
      data sym12/1,2,3,4,5,6,7,8/
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c------------------------------------------------------------
c
      call izero_wr(maxsym*maxsym,lmda,1)
      call izero_wr(maxsym*maxsym,lmdb,1)
      call izero_wr(maxsym,nmbpr,1)
c
      do i=1,nsym
      ix=sym12(i)
      sym21(ix)=i
      enddo ! end of: do i=1,nsym
c
      do i=1,nsym
        is=sym12(i)
        nmbj=0
         do j=1,nsym
         if(nmpsy(j).eq.0)go to 210
         js=sym12(j)
         ijs=mult(is,js)
         ijsym=sym21(ijs)
         if(nmpsy(ijsym).eq.0)go to 210
         if(ijsym.gt.j)go to 210
         nmbj=nmbj+1
         lmdb(i,nmbj)=j
         lmda(i,nmbj)=ijsym
  210    continue
         enddo ! end of: do j=1,nsym
      enddo ! end of: do i=1,nsym
c
      do i=1,nsym
         do j=1,nsym
          if (lmdb(i,j).gt.0) then
           if (nmpsy(lmdb(i,j))*nmpsy(lmda(i,j)).gt.0) then
           nmbpr(i)=nmbpr(i)+1
           endif
          endif
        enddo! end of: do j=1,nsym
      enddo! end of: do i=1,nsym
c
c      write(*,*)' lmda,lmdb'
      do i=1,nsym
c      write(*,30000)(lmdb(i,j),lmda(i,j),j=1,nsym)
      enddo ! end of: do i=1,nsym
c      write(*,*)' nmbpr'
c      write(*,30030)(nmbpr(i),i=1,nsym)
c
30000 format(1x,8(2i4,2x))
30030 format(1x,20i6)
      return
      end

c
c***********************************************************************
c

      subroutine rdcivout (
     & tape6,    ciuvfl,    lroot,  a4den,
     & itype,    froot,     refvec, eci,
     & ncsf,     fname)

c     analyses civout vector info file written by ciudg
c
      implicit none 
c
c     input:
c     tape6 : listing file
c     lroot : vector to calculate the density for
c             if root following was active for ciudg,
c             lroot corresponds to that ci vector that has a
c             maximum overlap with the lroot-th reference vector
c             in this case there is only ONE record on civout
c             and we check for consistency
c             Otherwise lroot corresponds to the lroot-th ci vector
c             (LRT,CI)
c     output
c     a4den : a4den value for the selected root
c             (froot overrides lroot!)
c     itype : corresponds to method in ciudg
c             0 : CI
c             3 : AQCC
c             30: AQCC-LRT
c
c     froot : the ci vector record to be used for density calculation
c     refvec: reference vector to be used for reference density
c              info(1) of ci vector #froot


C    VARIABLE DECLARATIONS FOR subroutine rdvhd
C
C     Parameter variables
C
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 20)
      INTEGER             CURVER
      PARAMETER           (CURVER = 1)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 10)
      INTEGER             MXINFO
      PARAMETER           (MXINFO = 10)
      INTEGER             MXNRGY
      PARAMETER           (MXNRGY = 10)
      integer mxinor
      parameter (mxinor=128)
      integer nmotx
      parameter (nmotx=1023)
C
C     Argument variables
C
      INTEGER             CIUVFL, lroot,itype,froot,refvec
      INTEGER             TAPE6,ncsf
C
      REAL*8              A4DEN,eci
      character*(*)       fname

C
C     Local variables
C
      CHARACTER*80        TITLE(MXTITL),test
C
      INTEGER             I,  IETYPE(MXNRGY)
      INTEGER             LENCI,       MXREAD,      NENRGY
      INTEGER             NINFO,       NTITLE
      INTEGER             VERSN
      integer           info(mxinfo)
      integer           iroot ,idum1,idum2
C
      REAL*8              ENERGY(MXNRGY)
C
C    END OF VARIABLE DECLARATIONS FOR subroutine rdvhd

      rewind(ciuvfl)
      iroot=0
      froot=0
      a4den=0.0d0
      itype=-1
      refvec=0
      eci=0.0d0

c
c     # search for froot
c

90    read (ciuvfl,end=99) versn,idum1,ncsf
      iroot=iroot+1
      if ( versn .ne. curver ) then
         call bummer('rdvhd:  wrong version number', versn, faterr)
      endif
c
c     # general information
      read (ciuvfl) ninfo, nenrgy, ntitle
c
c     info(1) : maximum overlap with reference vector # info(1)
c     info(2) : corresponding ci vector is record # info(2)
c     info(3) : method CI(0),AQCC(3),AQCC-LRT(30)
c     info(4) : if set to one this is the last record of civout
c     info(5) : overlap of reference vector and ci vector in percent
c

      mxread = min (ninfo, mxinfo)
      read (ciuvfl) (info(i), i=1,mxread)
c     # read titles:
      mxread = min (ntitle,mxtitl)
      read (ciuvfl) (title(i), i=1,mxread)
c
c     # read energies
      mxread = min(nenrgy,mxnrgy)
      read(ciuvfl) (ietype(i),i=1,mxread),(energy(i),i=1,mxread)
c     # skip sovref
      read (ciuvfl)
c     # skip tciref
      read (ciuvfl)
c     # skip e,mxov
      read (ciuvfl)

c
c     identify the special case of root following
c     this also includes the special case of ground
c     state calculations: there is only ONE record on civout
c
      if (iroot.eq.1 .and. info(4).eq.1) then
        itype = info(3)
        froot = info(2)
        refvec = info(1)
        if (info(5).lt.50)
     .   call bummer('insufficient overlap, probably intruder states',
     .   0,0)
        if (froot.ne.lroot)
     .   call bummer('lroot (cidenin) and froot/nroot (ciudgin)'//
     .               ' do not match ',0,2)
        if (itype.ne.-1) then
             do 10 i = 1, min(nenrgy,mxnrgy)
           if (ietype(i) .eq. -1024-15) a4den = energy(i)
c          MR-SDCI
           if (ietype(i) .eq. -1026)    eci   = energy(i)
c          MR--AQCC
           if (ietype(i) .eq. -1024-14)    eci   = energy(i)
   10        continue
        endif
        goto 92
      else
        if (info(2).eq.lroot) then
          itype= info(3)
          froot= info(2)
          refvec = info(1)
          if (info(5).lt.50)
     . call bummer('insufficient overlap, probably intruder states',
     . 0,0)
        if (itype.ne.-1) then
             do 11 i = 1, min(nenrgy,mxnrgy)
           if (ietype(i) .eq. -1024-15) a4den = energy(i)
c          MR-SDCI
           if (ietype(i) .eq. -1026)    eci   = energy(i)
c          MR--AQCC
           if (ietype(i) .eq. -1024-14)    eci   = energy(i)
   11        continue
        endif
        goto 91
        endif
      endif

      if (info(4).eq.0) goto 90
 91   continue

c
c  ##  in case of root following, the correct lroot value in cidenin
c  ##  is set by runsp.perl.  check, if this has been done correctly
c

      if (iroot.lt.lroot) then
        call bummer('lroot larger than number of ci vectors on file',
     &   iroot,faterr)
      endif
  92  continue


      write(tape6,15)
15    format(20('==='))
      write(tape6,*) 'extracting information from ',fname,':'
      write(tape6,*) 'ci vector:',froot,' itype=',itype,
     .       ' reference vector',refvec
      write(tape6,*) 'a4den=',a4den,' eci=',eci
      write(tape6,*) 'titles:'
      do i=1, min(ntitle,mxtitl)
       write(tape6,*) title(i)
      enddo
      write(tape6,15)

      return
 99   continue
      call bummer('civout file incomplete',0,2)
      end


      subroutine cnstrctmap(ibuf,lencor,mapmo_fc,
     .  nmpsy,nmpsy_fc,nsym,nexo,nexo_fc)
c
      implicit integer(a-z)
      integer nmomx
      parameter (nmomx=1023)
      integer lencor,mapmo_fc(nmomx)
      integer  nmpsy_fc(8),nsym,nmpsy(8)
      integer ibuf(lencor),nexo(8),nexo_fc(8)
c
      integer nfilmx, iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
c      include 'option.inc/list'
      integer numopt
      integer cigopt,print,fresdd,fresaa,fresvv,samcflag
c
c symmetry label information
      character*4 labels(8)
      character*80 cfmt
      character*80 title
      integer cpt(10)
c
      integer nmot,nmotd
c
      logical error
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif
       write(6,201) 
 201   format(5('----'),' cnstrctmap ',5('----'))
       write(6,'(a,8i4)') 'nmpsy(*)   =',nmpsy(1:nsym)
       write(6,'(a,8i4)') 'nmpsy_fc(*)=',nmpsy_fc(1:nsym)
       write(6,'(a,8i4)') 'nexo(*)    =',nexo(1:nsym)
       write(6,'(a,8i4)') 'nexo_fc(*) =',nexo_fc(1:nsym)
       nmot=0
       nmotfc=0
       nexofc=0
       icnt=0
       icntfc=0
       mapmo_fc=0
       do i=1,nsym
         nmot=nmot+nmpsy(i)
         nmotfc=nmotfc+nmpsy_fc(i)
         nmotfv=nmotfv+nexo_fc(i)
c
c map vector:  per irrep   fc * non-fc * fv 
c
c  frozen core
         do j=1,nmpsy(i)-nmpsy_fc(i)-(nexo(i)-nexo_fc(i))
          icnt=icnt+1
         enddo
c  active 
         do j=1,nmpsy_fc(i)
           icntfc=icntfc+1
           icnt=icnt+1
           mapmo_fc(icntfc)=icnt
         enddo
c  frozen virtuals
         icnt=icnt+nexo(i)-nexo_fc(i)
       enddo
       icnt=0
       do i=1,nsym
c  frozen core
         do j=1,nmpsy(i)-nmpsy_fc(i)-(nexo(i)-nexo_fc(i))
          icnt=icnt+1
          icntfc=icntfc+1
           mapmo_fc(icntfc)=-icnt
         enddo
          icnt=0
          do j=1,i
           icnt=icnt+nmpsy(j)
          enddo
       enddo

       write(6,112) nmotd,(mapmo_fc(i),i=1,nmot)
 112  format(' FC->full index map vector (nmotd',i4,')'/,20(15i5/))

c
c     mapmo_fc : 
c        if > 0 : density_index -> fulldensity_index
c        if < 0 : frozen core orbital, abs( ) = fulldensity_index
c
c        mapmo_fc (1:nmotfc -> index; nmotfc+1:nmot -> fc index)
c
       write(6,'(10("----"))') 

      return
c
6010  format(' *** rchdrt error *** ',a,3i8)
6030  format(/' ci drt information:'/(1x,a))
6040  format(' nmotd =',i4,' nfctd =',i4,' nfvtd =',i4,' nmot  =',i4/
     & ' niot  =',i4)
      end

      subroutine d2updtnew(
     & qfresv, info,  ddaf,  outfil,
     & diiii,  dviii,          bufout,
     & val,    ilab,           addii,
     & addvi,  off1,   off2,   mcd1,
     & lgmx,   mapmd1, maplgv, fgiiee,
     & fgeiei, mapvv,  mapii,  map2i,
     & lbufmx, d2, dabuf, dabufv , idabuf ,nmmmmt,
     . niiiit,nviiit,ifmt)

c
c  update an existing ci 2-particle density matrix file with the 1-
c  index transformed and transition mc density matrix contributions to
c  construct the effective two-particle density matrices required
c  for mrci gradients.
c
c  diiii(*)  = iiii density matrix contributions.
c  dviii(*)  = viii density matrix contributions.
c  bufin(*)  = input density matrix buffer.
c  bufout(*) = output density matrix buffer.
c  ibuf(*,*) = label buffer.
c  addii(*)  = internal-internal addressing array.
c  addvi(*)  = virtual-internal addressing array.
c  off1(*)   = iiii addressing array.
c  off2(*)   = viii addressing array.
c  lenbuf    = input and output buffer length.
c  nvpbf     = number of density elements per buffer = info(5)
c  lbufmx    = dimension of sifs buffers set by mem. allocation above.
c  lgmx      = lagrange multiplier matrix
c  mcd1      = mcscf density matrix
c  map..     = addressing arrays
c  fgiiee    = flags indicating if the iiee virtual orbital resolution
c              contribution is added to a corresponding nonzero
c              density matrix element
c  fgeiei    = same as above but for the eiei contribution
c
c  version log:
c  mar-96 sifs version gsk
c  jun-90 modified to add the virtual orbital resolution contribution
c                                                            (m.e.)
c  written by ron shepard.
c  version date 19-aug-87.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
      
c
      real*8    one, zero,    half,     two
      parameter(zero=0d0,half=.5d0,two=2d0,one=1.0d0)
c
      integer msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
      integer dblocc,active,virtl
      parameter(dblocc=1,active=2,virtl=3)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvmot,nnvt,irv1(8),irv2(8)
      equivalence (nxtot(13),nvmot)
      equivalence (nxtot(14),nnvt)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))

      integer nmpsy(8),n2sm(8),nmot,n2mt,ns(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),ns(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(6),n2mt)


      integer dendaf

c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer numopt
      parameter(numopt=10)
      integer cigopt, print, fresdd, fresaa, fresvv, samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
      integer perm(4,0:7),itype(4)
      integer    intrnl,   virtul
      parameter (intrnl=1, virtul=2)
      integer ivtype(0:4)
c
      integer niiiit,nviiit
      real*8 diiii(niiiit),dviii(nviiit)
      real*8 bufout(*),val(*)
      real*8 lgmx(*),mcd1(*)
c
      logical fgiiee(*),fgeiei(*)
      integer    nipv
      parameter (nipv=4)
c
c     ##   integer section
c
      integer lbufmx
      integer addii(*),addvi(nvmot,nimot)
      integer ilab(nipv,lbufmx),info(10),
     & ifmt,ibitv(1),imcd1,ilgmx,ierr,ibvtyp,
     & ip,i
      integer kntb
      integer last
      integer mapvv(*),mapii(*),map2i(nimot,nimot),
     & mapmd1(nimot,nimot),maplgv(nvmot,nvmot)
      integer nwrite,num,nvpbf
      integer off1(*),off2(*),outfil
      integer psym
      integer reqnum
      integer uvwx,uv,u,upind,utemp,uvwxt,uvtype
      integer v
      integer wx,w,wtemp,wxtype
      integer x
ctm
      integer nrec
      logical qfresv
      integer iretbv,itypea,itypeb,iwait
cfp
      integer nvirt

c------------------------fockm2--------------------------------------
c  ##  common & parameter section
c
      integer   nfilmx
      parameter(nfilmx=30)
c
      integer ndafmx
      parameter(ndafmx=2)
c
      integer nbkmx
      parameter(nbkmx=200)
c
c
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
c     integer nndx(nmomx)
c     equivalence (iorbx(1,1),nndx(1))
c
      real*8 esym1,eesym1,esym2,eesym2
      common/ener/esym1(8),eesym1(8),esym2(8),eesym2(8)
c
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
c
      integer ibfpt,ircpt,nvpbk,nvpsg,lendar,nbuk
      common/intsrt/ibfpt(nbkmx),ircpt(nbkmx,ndafmx),
     & nvpbk,nvpsg,lendar,nbuk
c
      real*8 d2(*),dabuf(*),dabufv(*)
      real*8 dnrm2_wr
      external dnrm2_wr
c
c   ##   integer section
c
      integer add0
      integer d2pt,ddaf
      integer fincr
      integer idabuf(*),ibuk
      integer fpt,finuv
      integer maxrd
      integer np,nmmmmt,nt
      integer restuv
      integer tsym
      integer usym,uvsym,uvpt
      integer uu,vv,ptoff,nnt,nnp,nsoffp,nsofft,p1,t1
      integer uuvvindx,ttppindx
      real*8  ddot_wr,thresh,haveadded
      parameter (thresh=1.d-10)

      integer nndx
      nndx(i)= i*(i-1)/2

c
c------------------end fockm2-------------------------------------------------

c
      data ivtype/3*intrnl,2*virtul/
      data nrec /0/
      data iretbv /0/, itypea /3/, itypeb /1/, iwait /1/
      data perm/1,2,3,4, 2,1,3,4, 1,2,4,3, 2,1,4,3,
     & 3,4,1,2, 3,4,2,1, 4,3,1,2, 4,3,2,1/
c
c-------------------------------------------------------------
c
c     # total number of effd2 elements written accumulated with nwrite.
      nwrite = 0
c
      if (info(5) .gt. lbufmx) call bummer(
     & 'd2updt: lbufmx.lt.n2max=', info(5), faterr )
      nvpbf = info(5)
c
      if (qfresv) then
c
c        # initialize fgiiee and fgeiei
c
         upind=nnvt*nnit
         do 150 i=1,upind
            fgiiee(i)=.true.
150      continue
c
         do 155 i=1, nnvt*n2it
            fgeiei(i)=.true.
155      continue
c
      endif

c     write(6,*) 'niiiit,nviiit=',niiiit,nviiit
c     write(6,*) 'diiii=',ddot_wr(niiiit,diiii,1,diiii,1)
c     write(6,*) 'dviii=',ddot_wr(nviiit,dviii,1,dviii,1)


c
c  initialize record and buffer pointers.
c
cfp_tmp
c      write(nlist,*) 'Starting d2updt ...'
      maxrd=0
      uvpt=1
      ibuk=1
      restuv=0
      kntb=0
      nvirt=0
c
      do 120 uu=1,nmot
         usym=symm(uu)
         do 110 vv=1,uu
            uvsym=mult(symm(vv),usym)
            if(samcflag.gt.0)then
              nvirt=0
              if(mctype(uu).eq.virtl) nvirt=nvirt+1
              if(mctype(vv).eq.virtl) nvirt=nvirt+1
cfp: skip everything if this is a 2-ext. distribution
              if(nvirt.eq.2)goto 110
            endif
c            write(6,*) '*** uu,vv,nvirt',uu,vv,nvirt
c
c  check whether the complete distribution is in core
c  if this is not the case, move the remaining part of the segment
c  to the beginning and read next segment.
c  note: h2(*) and d2(*) must be (nnmt+nvpsg) in length.
c
            finuv=uvpt+symtmm(uvsym)-1
            fincr=maxrd+restuv
            if(finuv.gt.fincr)then
               restuv=fincr-uvpt+1
               if(restuv.gt.0)then
                  call dcopy_wr(restuv,d2(uvpt),1,d2(1),1)
               endif
               add0=(ibuk-1)*nvpsg
               maxrd=min(nvpsg,nmmmmt-add0)
 2010          format('calling rdbks: off1=',10i8)
c              write(6,2011) add0,lendar,nvpbk,maxrd,
c    .           ibuk,ircpt(ibuk,ddaf),restuv 
 2011         format('add0,lendar.nvpbk,maxrd=',4i10,
     .       'ibuk,ircpt(ibuk,ddaf),restiuv=',3i10)
               call rdbks(add0,lendar,nvpbk,maxrd,
     &          ddaf,ircpt(ibuk,ddaf),d2(restuv+1),dabuf,
     &          dabufv,idabuf)
 2012          format('calling rdbks: d2(',i6,')=',6f10.6)
c              write(6,2012) restuv,d2(restuv+1:restuv+6)
               ibuk=ibuk+1
               uvpt=1
            endif

            uuvvindx=nndx(uu)+vv

            do 100 tsym=1,nsym
               nt=nmpsy(tsym)
               if(nt.eq.0)go to 100
               psym=mult(tsym,uvsym)
               np=nmpsy(psym)
               if(np.eq.0)go to 100
               d2pt=intoff_ci(psym,uvsym)+uvpt-1

c---------------------- fill in the update of the matrix elements ----
               ptoff=0
               if (psym.le.tsym) then
c         d2(np,nt)
                      nnp=np
                      nnt=nt
                      nsoffp=ns(psym)
                      nsofft=ns(tsym)
               elseif (tsym.lt.psym) then
c         d2(nt,np)
                      nnp=nt
                      nnt=np
                      nsoffp=ns(tsym)
                      nsofft=ns(psym)
              endif
c
c  the d2 elements are doubled, so we have to screen have of them
c  say check for correct canonical index
c  this also saves some later permutations.
c
               do 1001 t1=1,nnt
                 if (tsym.eq.psym) nnp=t1
                 do 1002 p1=1,nnp 
c
c   we have  always uu.ge.vv  and t1 > p1  
c                  
                   if (nsofft+t1.lt.nsoffp+p1) then
                     write(6,*) 'something wrong',nsofft+t1,nsoffp+p1
                   endif 
                   ttppindx=nndx(nsofft+t1)+nsoffp+p1
c skip the doubled component
                   ptoff=ptoff+1
                   if (uuvvindx.lt.ttppindx .or. psym.gt.tsym) then
c                     write(6,*) 'skipping ',d2(d2pt+ptoff)
                      goto 1002
                   endif
               

                   itype(1)=ivtype(mctype(uu))
                   itype(2)=ivtype(mctype(vv))
                   itype(3)=ivtype(mctype(nsofft+t1))
                   itype(4)=ivtype(mctype(nsoffp+p1))

                   kntb=kntb+1
                   val(kntb)=d2(d2pt+ptoff)
                   ilab(1,kntb)=uu
                   ilab(2,kntb)=vv
                   ilab(3,kntb)=nsofft+t1
                   ilab(4,kntb)=nsoffp+p1
c             write(6,3010) uu,vv,psym,tsym,t1,p1,nsofft,nsoffp
c 3010   format('uu,vv=',2i4,' psym,tsym=',2i2,' t1,p1=',2i4,
c     .  ' symmoff',2i4)
c             write(6,3011) uu,vv,nsofft+t1,nsoffp+p1,
c    .                      d2(d2pt+ptoff),ptoff
c 3011    format('D2(in)[',4i3,'] ',f10.6,2x,i10)
c                  ptoff=ptoff+1

           haveadded=0.0d0
c-------------------------d2update------------------------------
         ip=0
         if(itype(1).lt.itype(2))ip=1
         if(itype(3).lt.itype(4))ip=ip+2
         uvtype=nndx(itype(perm(1,ip)))+itype(perm(2,ip))
         wxtype=nndx(itype(perm(3,ip)))+itype(perm(4,ip))
         if(uvtype.lt.wxtype)then
            ip=ip+4
            uvwxt=nndx(wxtype)+uvtype
         else
            uvwxt=nndx(uvtype)+wxtype
         endif

         if(uvwxt.eq.1)then
c           ...iiii
            u=mapivm(ilab(perm(1,ip),kntb))
            v=mapivm(ilab(perm(2,ip),kntb))
            w=mapivm(ilab(perm(3,ip),kntb))
            x=mapivm(ilab(perm(4,ip),kntb))
            uv=nndx(max(u,v))+min(u,v)
            wx=nndx(max(w,x))+min(w,x)
            uvwx=off1(max(uv,wx))+addii(min(uv,wx))
            val(kntb)=val(kntb)+diiii(uvwx)
c            write(6,*) diiii(uvwx)
            haveadded=diiii(uvwx)
            diiii(uvwx)=zero
         elseif(uvwxt.eq.2)then
c           ...eiii
            u=mapivm(ilab(perm(1,ip),kntb))
            v=mapivm(ilab(perm(2,ip),kntb))
            w=mapivm(ilab(perm(3,ip),kntb))
            x=mapivm(ilab(perm(4,ip),kntb))
            wx=nndx(max(w,x))+min(w,x)
            uvwx=off2(wx)+addvi(u,v)
c         write(6,3025) wx,off2(wx),u,v,w,x,u,v,
c     .        addvi(u,v),uvwx,dviii(uvwx)
c3025    format('off2(',i4,')=',i6,'u v w x=',4i4,'addvi(',i3,i3,')=',
c     .            i8, 'taking',i6,f8.6)
            val(kntb)=val(kntb)+dviii(uvwx)
            haveadded=dviii(uvwx)
            dviii(uvwx)=zero
c
c        # exit if no virtual orbital resolution is required
c
         elseif (.not.qfresv) then
            goto 1003
         elseif(uvwxt.eq.4)then
c
c           # iiee
c           # d2ci(uvwx) = d2ci(uvwx) +
c           #              2*lgmx(wx)*d1mc(uv) + 2*lgmx(uv)*d1mc(wx)
c
            w=mapivm(ilab(perm(1,ip),kntb))
            x=mapivm(ilab(perm(2,ip),kntb))
            u=mapivm(ilab(perm(3,ip),kntb))
            v=mapivm(ilab(perm(4,ip),kntb))
            if (symi(u).eq.symi(v)) then
               if (symv(w).eq.symv(x)) then
                  ilgmx =maplgv(max(w,x),min(w,x))
                  imcd1=mapmd1(max(u,v),min(u,v))
                  val(kntb)=val(kntb)+two*lgmx(ilgmx)*mcd1(imcd1)
                  haveadded=two*lgmx(ilgmx)*mcd1(imcd1)
                  fgiiee((mapii(imcd1)-1)*nnvt+mapvv(ilgmx))=.false.
               endif
            endif
c
         elseif(uvwxt.eq.3)then
c
c           # eiei
c           # d2ci(uvwx) = d2ci(uvwx)
c           #              -1/2*lgmx(wu)*mcd1(xv)-1/2*lgmx(xu)*mcd1(wv)
c           #              -1/2*lgmx(xv)*mcd1(wu)-1/2*lgmx(wv)*mcd1(ux)
c
            w=mapivm(ilab(perm(1,ip),kntb))
            u=mapivm(ilab(perm(2,ip),kntb))
            x=mapivm(ilab(perm(3,ip),kntb))
            v=mapivm(ilab(perm(4,ip),kntb))
            if (symi(u).eq.symi(v)) then
               if (symv(w).eq.symv(x)) then
                  ilgmx =maplgv(max(w,x),min(w,x))
                  imcd1=mapmd1(max(u,v),min(u,v))
                  val(kntb)=val(kntb)-half*lgmx(ilgmx)*mcd1(imcd1)
                  haveadded=-half*lgmx(ilgmx)*mcd1(imcd1)
                  if (w.lt.x) then
                     wtemp=w
                     w=x
                     x=wtemp
                     utemp=u
                     u=v
                     v=utemp
                  elseif (w.eq.x) then
                     utemp=max(u,v)
                     v=min(u,v)
                     u=utemp
                  endif
                  fgeiei((mapvv(ilgmx)-1)*n2it+map2i(u,v))=.false.
               endif
            endif
         else
c           not modified
         endif

 1003  continue 
cfp_todo: haveadded is just a debug variable!?
c       if (abs(haveadded.gt.1.d-6)) then 
c       write(6,*) 'effd2=',val(kntb),'kntb=',kntb,'hvadded=',haveadded
c       endif
       if (abs(val(kntb)).lt.thresh) kntb=kntb-1
c      if (kntb.eq.lbufmx) then
       if (kntb.eq.nvpbf) then
           nwrite=nwrite+1
c           write(6,*) 'writing effective density, record=',nwrite
           last=msame
c this is hardcoded  8bit label encoding!
           ibvtyp=0
           call sifew2(outfil,info,nipv,kntb,last,itypea,itypeb,
     .      ibvtyp,val,ilab,ibitv,bufout,iwait,nrec,reqnum,ierr)
           if (ierr.ne.0) then
            write(6,'(a,5i6)') 'INFO=',info(1:5) 
            call bummer(
     .      'write error from sifew2 d2updtnew,ierr=',ierr,2)
           endif 
           kntb=0
         endif 
c------------------d2update -------------------------------------

1002           continue
1001          continue
100         continue
            uvpt=uvpt+symtmm(uvsym)
110      continue
120   continue

           nwrite=nwrite+1
c          write(6,*) 'final effective density, record=',nwrite
c          write(6,*) 'kntb,lbufmax=',kntb,lbufmx
           last=nomore
c this is hardcoded 8bit packing!
           ibvtyp=0

           call sifew2(outfil,info,nipv,kntb,last,itypea,itypeb,
     .      ibvtyp,val,ilab,ibitv,bufout,iwait,nrec,reqnum,ierr)
           if (ierr.ne.0) call bummer(
     .      'write error dumpint final sifew2 d2updtnew,ierr=',ierr,2)


c
c   there is no need to look for nonzero elements in the density arrays
c   since the sorted d2 file contains the complete set of integrals 
c   including zeroes.
c


      return
      end

         subroutine getmosize(nmpsy,mosize,prtlvl)
         integer nmpsy(*),mosize
         integer isym,jsym,ksym,lsym,prtlvl 
         integer nxy,mult,nsym,nxtot
         common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
         integer i,j,k,l,idx
         idx(i)=i*(i+1)/2

          mosize=0
c
c         (aa|aa)
c

          do isym=1,nsym
           mosize=mosize+2*idx(nmpsy(isym))*idx(nmpsy(isym))
          enddo
          if (prtlvl.gt.1) write(6,100) '(aa|aa)=',mosize
c
c         (aa|bb) 
c
          do isym=1,nsym
           do jsym=1,isym-1
             mosize=mosize+2*idx(nmpsy(isym))*idx(nmpsy(jsym))
          enddo
          enddo
          if (prtlvl.gt.1) write(6,100) '+(aa|bb)=',mosize
          
c
c         (ab|ab)
c
          do isym=1,nsym
           do jsym=1,isym-1
             mosize=mosize+(nmpsy(isym)*nmpsy(jsym))**2
          enddo
          enddo
          if (prtlvl.gt.1) write(6,100) '+(ab|ab)=',mosize
c
c          (ab|cd)
c

          ijsym=0
          do isym=1,nsym
           do jsym=1,isym-1
              ijsym=ijsym+1
              klsym=0
             do ksym=1,isym-1
              do lsym=1,ksym-1  
               klsym=klsym+1
               if (ijsym.lt.klsym) cycle
               if (mult(mult(isym,jsym),ksym).ne.lsym) cycle  
               mosize=mosize+
     .                2*nmpsy(isym)*nmpsy(jsym)*nmpsy(ksym)*nmpsy(lsym)
              enddo
             enddo
            enddo
           enddo 
          if(prtlvl.gt.1) write(6,100) '+(ab|cd)=',mosize
c
c         stored approximately as twice the number of integrals
c          g_rtsu and g_surt
c         add one 32bit label to each integral

           mosize=mosize*3/2
        
          write(6,100) '+labels=',mosize

 100      format('getmosize:',a,2x,i10)
          return
          end


      subroutine getlenbfs(lenbfsdef,numint,nlist)

c  perform the second-half bin sort of 2-e integral types 6-11.
c
c  written by ron shepard.
c
      implicit integer(a-z)
      integer numint(12),nlist 
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      integer tsymdd(8),tsmdd2(8),tsymad(8),tsymvd(8)
      integer tsymva(8),tsymvv(8),tsmvv2(8)
      equivalence (nxy(1,21),tsymdd(1))
      equivalence (nxy(1,22),tsmdd2(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,28),tsmvv2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a)
c
c  prepare for sort: initialize /cis2/
c  add0  = address offset for placing integrals in core.
c  ibpt  = beginning of current buffer space.
c  ic0   = running offset within core(*).
c  icmax = address of the last integral in core(*).
c  ic0mx = minimum of icmax and the address of last buffer
c          element in core(*), ic0mx=min(icmax,ibpt+lenbfs-1).
c
       size_stape=0
       lenbfs=0
      
c
c  type-6 integrals: off6(pq)+adddd(ij), off6(pq)+adddd2(j,i)
c
      if(numint(6).eq.0)go to 700
      do 620 p=1,nact
      psym=symx(ix0a+p)
      do 610 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsymdd(pqsym)
      lenbfs=max(lenbfs,nterm)
      nterm=tsmdd2(pqsym)
      lenbfs=max(lenbfs,nterm)
610   continue      
620   continue
700   continue
c
c  type-7 integrals: off7(pq)+addvv(ab)
c
      if(numint(7).eq.0)go to 800
      do 720 p=1,nact
      psym=symx(ix0a+p)
      do 710 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsymvv(pqsym)
      lenbfs=max(lenbfs,nterm)
710   continue            
720   continue
c
800   continue
c
c  type-8 integrals: off8(pq)+addvv2(b,a)
c
      if(numint(8).eq.0)go to 900
      do 820 p=1,nact
      psym=symx(ix0a+p)
      do 810 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsmvv2(pqsym)
      lenbfs=max(lenbfs,nterm)
810   continue       
820   continue
c
900   continue
c
c  type-9 integrals: off9(ai)+addva(b,p)
c
      if(numint(9).eq.0)go to 1000
      nai=tsymvd(1)
      do 910 ai=1,nai
      nterm=tsymva(1)
      lenbfs=max(lenbfs,nterm)
910   continue     
1000  continue
c
c  type-10 integrals: off10(ai)+addad(j,p)
c
c     if(bukpt(11).eq.0)go to 1100
      if(numint(10).eq.0)go to 1100
      nai=tsymvd(1)
      do 1010 ai=1,nai
      nterm=tsymad(1)
      lenbfs=max(lenbfs,nterm)
1010  continue        
1100  continue
c
c  type-11 integrals: off11(ij)+addvv3(b,a)
c
      if(numint(11).eq.0)go to 1200
      do 1120 i=1,ndot
      na=nvpsy(symx(ix0d+i))
      do 1110 j=1,i
      nb=nvpsy(symx(ix0d+j))
      nterm=na*nb
      lenbfs=max(lenbfs,nterm)
1110  continue      
1120  continue
1200  continue

      write(nlist,*) 'lenbfsdef=',lenbfsdef,' lenbfs=',lenbfs 
      if (lenbfsdef.lt.lenbfs) then
        write(nlist,*) 'default value of lenbfs too small '
        write(nlist,*) 'increasing lenbfs from',lenbfsdef,' to ', lenbfs
        lenbfsdef=lenbfs
      endif
      write(nlist,*) ' number of integrals per class 1:11 (cf adda '
1201  format(1x,a,i2,a,i12)
      write(nlist,1201) 'class ',1,' (pq|rs):         #',numint(1)
      write(nlist,1201) 'class ',2,' (pq|ri):         #',numint(2)
      write(nlist,1201) 'class ',3,' (pq|ia):         #',numint(3)
      write(nlist,1201) 'class ',4,' (pi|qa):         #',numint(4)
      write(nlist,1201) 'class ',5,' (pq|ra):         #',numint(5)
      write(nlist,1201) 'class ',6,' (pq|ij)/(pi|qj): #',numint(6)
      write(nlist,1201) 'class ',7,' (pq|ab):         #',numint(7)
      write(nlist,1201) 'class ',8,' (pa|qb):         #',numint(8)
      write(nlist,1201) 'class ',9,' p(bp,ai)         #',numint(9)
      write(nlist,1201) 'class ',10,'p(ai,jp):        #',numint(10)
      write(nlist,1201) 'class ',11,'p(ai,bj):        #',numint(11)
      return
      end
