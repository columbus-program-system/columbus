!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c deck smxvu
      subroutine smxvu(a,x,y,n,w,lw)
c
c  compute a symmetric-matrix times vector product and update a vector:
c      y = y + a * x
c  where a(*,*) is stored lower triangle packed by rows.
c  w(1:lw) is optionally used for workspace.
c
      implicit integer(a-z)
      real*8 a(*),x(n),y(n),w(*)
      real*8    zero,    ddot_wr
      parameter(zero=0d0)
c
*@if defined( t3e64) || defined ( cray )
*      if(n*(n+1).le.lw)then
*!C        ...expand the packed matrix to square form, multiply,
*!C           and update.
*         call sm11sq(a,w(n+1),n)
*         call mxv(w(n+1),n,x,n,w)
*         do 10 i=1,n
*            y(i)=y(i)+w(i)
*10       continue
*      else
*         y(1)=y(1)+a(1)*x(1)
*         ij1=2
*         do 20 i=2,n
*            im1=i-1
*            y(i)=y(i)+sdot(i, a(ij1),1, x,1)
*            if(x(i).ne.zero)call saxpy(im1, x(i), a(ij1),1, y,1)
*            ij1=ij1+i
*20       continue
*      endif
*@else
c
c     # compute the product directly from the lt packed matrix.
      y(1)=y(1)+a(1)*x(1)
      ij1=2
      do 20 i=2,n
         im1=i-1
         y(i)=y(i)+ddot_wr(i, a(ij1),1, x,1)
         if(x(i).ne.zero)call daxpy_wr(im1, x(i), a(ij1),1, y,1)
         ij1=ij1+i
20    continue
*@endif
c
      return
      end
c
c***********************************************************************
c
c deck solvel
      subroutine solvel(
     & avcsl,    core,     naar,    ndimr,
     & nhblk,    emc,      w,
     & fcsf,     hmcvec,   orbl,    csfl,
     & xbar,     xp,       z,       rev,
     & ind,      modrt,    u1,      lenbuf,
     & nwalk,    add,      lastb,   nipbuk,
     & lenbuk,   iorder)
c
c  set parameters and allocate workspace for
c  the solution of the mcscf linear equations.
c
c  input:
c  avcsl     = available core.
c  core(*)   = workspace array.
c  naar      = number of active-active rotations.
c  ndimr     = orbital rotation vector length.
c  nhblk     = number of nonzero hessian blocks to be read.
c  emc       = current mcscf energy.
c  w*(*)     = effective orbital gradient vector components
c              ( with ad, aa, vd, and va components contigously stored).
c  fcsf(*)   = effective csf gradient vector.
c  hmcvec(*) = current hmc eigenvector.
c  nvrsmx    = maximum number of r and s expansion vectors.
c
c  output:
c  orbl(*)   = orbital component of the solution vector.
c  csfl(*)   = csf component of the solution vector.
c
c  written by ron shepard.
c  version date: 6-june-88
c  modified from routine solvek() from program uexp2; 15-july-87.
c
       implicit none
c  ##  parameter & common block section
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     # bummer error types.
      integer   wrnerr,   nfterr,   faterr
      parameter(wrnerr=0, nfterr=1, faterr=2)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),ndmot,namot,napsy(8),nvpsy(8),nadt,nvdt,
     &        tsymad(8),tsymvd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxtot(7),ndmot)
      equivalence (nxtot(10),namot)
      equivalence (tsymad(1),nadt)
      equivalence (tsymvd(1),nvdt)
c
      real*8 energy1,energy2
      integer nadcalc,drt1,drt2,nroot1,nroot2
      common /nad/ energy1,energy2,nadcalc,drt1,drt2,nroot1,nroot2
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer numopt
      parameter(numopt=10)
c
      integer cigopt,print,fresdd,fresaa,fresvv,samcflag,solvelpck
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
      equivalence (cigopt(6),solvelpck)
c
      integer prtmicro
      common/output/prtmicro
c
      integer                    nmiter, nvrsmx
      real*8         rtol, dtol
      common /cleqi/ rtol, dtol, nmiter, nvrsmx
c
      integer   nfilmx
      parameter(nfilmx=31)
      integer         iunits
      common /cfiles/ iunits(nfilmx)
      integer                nlist
      equivalence (iunits(1),nlist)
      integer                 mchess
      equivalence (iunits(13),mchess)
c
      integer   nmomx
      parameter(nmomx=1023)
c
      integer
     & mctype,        mapivm,        mapmi,          mapma,
     & mapmd,         mapmv,         mapml,          mapmm,
     & symi,          symv,          symm,           syma,
     & symd,          mcres
      common /cmomap/
     & mctype(nmomx), mapivm(nmomx), mapmi(nmomx),   mapma(nmomx),
     & mapmd(nmomx),  mapmv(nmomx),  mapml(0:nmomx), mapmm(nmomx),
     & symi(nmomx),   symv(nmomx),   symm(nmomx),    syma(nmomx),
     & symd(nmomx),   mcres(nmomx)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist)*navst(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
c   ##   integer section
c
      integer add(*),avcsl,avcsl2
      integer ind(*),icd(19),ib(15),iorder(*),ierr,i,ipt,ii,itotal,
     &        ib_m(maxstat), ib_c(4,maxstat), ist, itemp, icount
      integer lastb(*),lenbuk,lenbuf
      integer modrt(0:*)
      integer nipbuk,nwalk,nr,ns,nmaxr,mnmxrs,mxrsx,nrx,nmaxs,
     &        nnewr,nolds,nnews,noldr,nhblk,naar ,ndimr,ncfx,
     &        ndims
      integer sizeb
c
c  ##  real*8 section
c
      real*8 core(*),csfl(*)
      real*8 emc
      real*8 fcsf(*)
      real*8 hmcvec(*)
      real*8 orbl(*)
      real*8 rev(*)
      real*8 u1(*)
      real*8 w(*)
      real*8 xp(*),xbar(*)
      real*8 z(*)
c
c
      integer atebyt,forbyt
      external atebyt,forbyt
c
      integer nndx
      nndx(i)=(i*(i+1))/2
c
c---------------------------------------------------------------
c
c  #  total number of CSFs (sum of all states and DRTs)
      ncfx=cpt4_tot
c
c
c  begin allocation of core arrays.
c  core: 1:b
c
      icd(1)=1
c
c  read in blocks of the hessian matrix from the external file
c  and set pointers, ib(*). on return, ib(i)=0 means that hessian
c  block does not exist.
c
      call rdmch(
     & mchess,        nlist,   nhblk,   avcsl,
     & core(icd(1)),  ib,      icd(1),  sizeb,
     & ib_m,          ib_c)
c
c  the csf-csf hessian block, m(*), must be constructed from the
c  hmc(*) matrix.
c
         do ist=1,nst
          if(ncsf_f(ist).ne.0 .and. ib_m(ist).gt.0)then
           ii= icd(1)-1 + ib_m(ist)
           call bh15(ncsf_f(ist),core(ii),ist)
            if (prtmicro.ge.1) then
             write(nlist,'(/,3x,''M matrix DRT:'',i3,2x,''block'',i3)')
     &        ist, navst(ist)
               do i=1,navst(ist)
               call plblkt('M matrix ',core(ii),ncsf_f(ist),
     &          ' row',1,nlist)
               ii = ii + nndx(ncsf_f(ist))
               enddo ! i=1,navst(ist)
            endif ! (prtmicro.ge.1)
          endif ! (ncsf_f(ist).ne.0 .and. ib_m(ist).gt.0)
         enddo ! ist=1,nst
c
c  ##  consistency check
c
       itemp=0
          do ist=1,nst
          itemp = itemp + ib_m(ist)
          enddo ! ist=1,nst
        if ((itemp.eq.0).and.(mdir.eq.0)) then
         write(nlist,
     &    '(///,3x,''!!!  input inconsistency  !!!'')')
         write(nlist,
     &    '(//3x,'' no M matrix block read in '',//)')
         write(nlist,
     &    '( 5x,''Solution:''/3x,''set in cigrdin: mdir=1''/)')
         write(0,
     &    '(///,3x,''!!!  input inconsistency  !!!'')')
         write(0,
     &    '(//3x,'' no M matrix block read in '',//)')
         write(0,
     &    '( 5x,''Solution:''/3x,''set in cigrdin: mdir=1''/)')
         call bummer ('input error',0,faterr)
        endif !
c
          do ist=1,nst
          itemp = 0
             do i=1,4
             itemp = itemp + ib_c(i,ist)
             enddo ! i=1,4
           if ((itemp.eq.0).and.(cdir.eq.0)) then
            write(nlist,
     &       '(///3x,''!!!  input inconsistency  !!!'')')
            write(nlist,
     &       '(//3x,'' no C matrix block read in '')')
            write(nlist,
     &       '(5x,''Solution:'',/,3x,''set in cigrdin: cdir=1''/)')
            write(0,
     &       '(///3x,''!!!  input inconsistency  !!!'')')
            write(0,
     &       '(//3x,'' no C matrix block read in '')')
            write(0,
     &       '(5x,''Solution:'',/,3x,''set in cigrdin: cdir=1''/)')
            call bummer ('input error',0,faterr)
           endif !
          enddo ! ist=1,nst
c
        if ((solvelpck.eq.1).and.((mdir.eq.1).or.(cdir.eq.1))) then
            write(nlist,
     &       '(///3x,''!!!  input inconsistency  !!!'')')
            write(nlist,
     &       '(///3x,''!!!  with solvelpck=1     !!!'')')
            write(nlist,
     &       '(//3x,'' no C matrix block read in '')')
            write(nlist,
     &    '(//3x,'' or no M matrix block read in '',//)')
            write(nlist,
     &       '(5x,''Solution:'',/,3x,''set in cigrdin: cdir=0
     & and mdir=0; or solvelpck=0''/)')
            call bummer ('input error',0,faterr)
        endif
c
        if ((solvelpck.eq.1).and.(nsym.gt.1)) then
            write(nlist,
     &       '(///3x,''!!!  input inconsistency  !!!'')')
            write(nlist,
     &       '(///3x,''!!!  with solvelpck=1     !!!'')')
            write(nlist,
     &       '(//3x,'' only C1 symmetry possible '')')
            call bummer ('input error',0,faterr)
        endif
c
c  get diagonal elements of the b(*) matrix and the m(*) matrix.
c  core: 1:b,2:bdiag,3:mdiag
c
      icd(2)=icd(1)+atebyt(sizeb)
      icd(3)=icd(2)+atebyt(ndimr)
      itotal=icd(3)+atebyt(ncfx)-1
      call memcheck(itotal,avcsl,'solvel 1')
c
      ipt=icd(2)
      if(ib(1).gt.0)call dxyxy(core(ib(1)),core(ipt),
     & namot,napsy,ndpsy,mapma,symm)
c
      ipt=ipt+atebyt(nadt)
      if(ib(3).ne.0)call cdspv(naar,core(ib(3)),core(ipt))
c
      ipt=ipt+atebyt(naar)
      if(ib(6).gt.0)call dxyxy(core(ib(6)),core(ipt),
     & ndmot,ndpsy,nvpsy,mapmd,symm)
c
      ipt=ipt+atebyt(nvdt)
      if(ib(10).gt.0)call dxyxy(core(ib(10)),core(ipt),
     & namot,napsy,nvpsy,mapma,symm)
c
c  ##  get the Mdiag
      ipt=icd(3)
         do ist=1,nst
         icount = 0
          if (ib_m(ist).gt.0) then
             do i=1,navst(ist)
             call cdspv(ncsf_f(ist), core(ib_m(ist)+icount),
     &        core(ipt))
             ipt = ipt + ncsf_f(ist)
             icount = icount + nndx(ncsf_f(ist))
             enddo ! i=1,navst(ist)
          endif ! (ib_m(ist).gt.0)
         enddo ! ist=1,nst
c
      if (solvelpck.eq.1) then
      icd(4)=icd(3)+atebyt(ncfx)
      ii=ndimr+ncfx
      do ist=1,nst
        do i=1,navst(ist)
          ii=ii-navst(ist)
        enddo
      enddo
      icd(5)=icd(4)+atebyt(((ii)*(ii+1))/2)
      icd(6)=icd(5)+atebyt((ii))
      icd(7)=icd(6)+forbyt((ii))
      itotal=icd(7)
      call memcheck(itotal,avcsl,'solvel1b')
c  call to construct full tridiagonal hessian
c  and solve the linear equation f=G*lambda
      call cpbcm(
     & core(icd(1)), ib, ndpsy, napsy, nvpsy, namot, ndmot,
     & nadt, naar, nvdt, ndimr, ib_c, ib_m, icd(1), core(icd(4)),
     & ncfx, w, fcsf, orbl, csfl, core(icd(5)),core(icd(6)),icd(7),
     & avcsl, core, hmcvec)
c
c     else (solvelpck=0)
      else 
c  determine the number of expansion vectors r(*) and s(*) to
c  use in the iterative solution of the lambda vector.
c  minimum number of r(*) and s(*) expansion vectors is 2.
c
c  the optimal maximum number of vectors is somewhat machine dependent.
c  each iteration requires the computation of a set of matrix-vector
c  products and the manipulation of the trial vectors and subspace
c  representations.  the smaller the subspace dimensions, the less
c  effort is required for these manipulations.  however, the larger
c  the subspace dimensions the fewer iterations will be required and
c  the fewer matrix-vector products will be needed.
c
c  this version keeps nmaxs=nmaxr when ncfx.ne.0.
c
      ndims=max(1,ncfx)
      mnmxrs=2
      mxrsx=min(nvrsmx,max(ncfx-1,ndimr))
c
      do 210 nrx=mxrsx,mnmxrs,-1
         nr=max(2,min(nrx,ndimr))
         if(ncfx.eq.1)then
            ns=0
         else
            ns=min(nrx,ncfx-1)
         endif
c
         itotal=atebyt( sizeb+ndimr+ncfx+
     &    nr*(2*ndimr+ncfx+ns+2)+
     &    ns*(2*ncfx+ndimr+2)+
     &    nndx(nr)+nndx(ns)+nndx(nr+ns+1) ) + forbyt(nr+ns)
         if(itotal .le. avcsl)go to 220
210   continue
c     ...exit from loop means that not enough space is available.
      write(nlist,6010)'nmaxr',avcsl,itotal
      call bummer('solvel: nmaxr itotal=',itotal,faterr)
c
220   continue
      nmaxr=nr
      nmaxs=ns
c
      if (print.ge.1) then
       call prblks('fcsf vector',fcsf,1,ndims,1,
     &    '    ','    ',1,nlist)
       call prblks('orbl vector',orbl,1,ndimr,1,
     &    '    ','    ',1,nlist)
      endif ! end of: if (print.ge.1) then

c
c  initialize s(*) vectors.
c  core: 1:b,2:bdiag,3:mdiag,4:s
c
      icd(4)=icd(3)+atebyt(ncfx)
      nolds=0
      nnews=0
c
c  initialize r(*) vector: r(*,1)=w(*)
c  core: 1:b,2:bdiag,3:mdiag,4:s,5:r
c
      icd(5)=icd(4)+atebyt(nmaxs*ncfx)
      call dcopy_wr(ndimr,w,1,core(icd(5)),1)
      noldr=0
      nnewr=1
c&removed by PGS after correcting subroutine wci
c&      if (nadcalc.ge.1) then
c&       do i=1,ndimr
c&       core(icd(5)+i-1)=1.0d+00
c&       enddo
c&      endif
       call orthot(ndimr,nnewr,core(icd(5)),ierr)
      if(ierr.ne.0)call bummer('solvel: from orthot, ierr=',ierr,faterr)
c
c  allocate the other workspace arrays from core.
c  core: 1:b,2:bdiag,3:mdiag,4:s,5:r,
c        6:bxr,7:cxr,8:cxs,9:mxs,10:br,11:cr,12:mr,13:wr,14:fr,15:zr,
c        16:pr,17:scr,18:kpvt,19:core
c
      icd(6)=icd(5)+atebyt(nmaxr*ndimr)
      icd(7)=icd(6)+atebyt(nmaxr*ndimr)
      icd(8)=icd(7)+atebyt(nmaxr*ncfx)
      icd(9)=icd(8)+atebyt(nmaxs*ndimr)
      icd(10)=icd(9)+atebyt(nmaxs*ncfx)
      icd(11)=icd(10)+atebyt(nndx(nmaxr))
      icd(12)=icd(11)+atebyt(nmaxr*nmaxs)
      icd(13)=icd(12)+atebyt(nndx(nmaxs))
      icd(14)=icd(13)+atebyt(nmaxr)
      icd(15)=icd(14)+atebyt(nmaxs)
      icd(16)=icd(15)+atebyt(nmaxr)
      icd(17)=icd(16)+atebyt(nmaxs)
      icd(18)=icd(17)+atebyt(nndx(nmaxr+nmaxs+1))
      icd(19)=icd(18)+forbyt(nmaxr+nmaxs)
      itotal=icd(19)
      call memcheck(itotal,avcsl,'solvel 2')
c
c  perform micro-iterations to solve for the lambda vector.
c
      avcsl2 = avcsl-itotal+1
      call lmicro(
     & avcsl2,         nmiter,         rtol,          dtol,
     & core(icd(1)),   ib,             w,             fcsf,
     & hmcvec,         core(icd(2)),   core(icd(3)),  naar,
     & ncfx,           noldr,          nnewr,         nolds,
     & nnews,          ndimr,          nmaxr,         ndims,
     & nmaxs,          core(icd(5)),   core(icd(6)),  core(icd(7)),
     & core(icd(4)),   core(icd(8)),   core(icd(9)),  core(icd(10)),
     & core(icd(11)),  core(icd(12)),  core(icd(13)), core(icd(14)),
     & core(icd(15)),  core(icd(16)),  core(icd(17)), core(icd(18)),
     & print,          orbl,           csfl,          xp,
     & z,              xbar,           rev,           ind,
     & modrt,          u1,             core(icd(19)), lenbuf,
     & nwalk,          add,            emc,           lastb,
     & nipbuk,         lenbuk,         iorder,        ib_c,
     & ib_m)
c
c     solvelpck
      endif
      return
6010  format(/' solvel: ',a,' avcsl=',i10,' itotal=',i10,' icd(*)=',
     & /(1x,10i10))
      end
c
c**********************************************************
c
c deck sort1
      subroutine sort1(sfile,info,dafile,offmm,
     &                 addmm,dabufv,idbufi,dabuf,
     &                 buffer,labs,vals,
     .                 mapmo_fc,assume_fc,d1,nmotd,nadcalc)
c
c  read in the two-electron integrals or two-particle density matrix
c  elements from the sequential file and write out to the da file.
c
c  sfile = sequential input file positioned to read the first record.
c  dafile = direct access output file.
c  offmm(*), addmm(*) = addressing arrays.
c  dabufv(*),idbufi(*) = output da sorting buckets.
c  dabuf(*) = da buffer.
c  buffer(*) = input buffer.
c  labs(4,*) = unpacked label buffer.
c  vals(*) = input value buffer.
c
c
       implicit none
      integer nfilmx, iunits, nlist,nadcalc
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer ndafmx, nbkmx, ibfpt, ircpt, nvpbk, nvpsg, lendar, nbuk
      parameter(ndafmx=2,nbkmx=200)
      common/intsrt/ibfpt(nbkmx),ircpt(nbkmx,ndafmx),
     & nvpbk,nvpsg,lendar,nbuk
c
      integer nmomx
      parameter(nmomx=1023)

      integer mapmo_fc(nmomx),assume_fc,nmotd
      integer i1,j1,k1,l1,ioff
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nnsm(8),nsm(8)
      equivalence(nxy(1,7),nnsm(1))
      equivalence(nxy(1,6),nsm(1))

c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx),symm(nmomx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
c
c
      integer   more
      parameter(more=0)
c
      real*8 valx, d1(*)
c
      integer sfile, info(10), dafile,nipv, itypeb
      parameter(nipv=4)
      integer idbufi(nvpbk,*),labs(nipv,*),offmm(*),addmm(*)
      real*8 dabufv(nvpbk,*),dabuf(*),buffer(*),vals(*)
      integer iretbv, nbuf, last, itypea, ifmt, ibvtyp, ibitv(1), ierr
      parameter (iretbv=0)
c
c     local
      integer kk, ll, ii, jj, iijj, ibuk, ibpt, kkll, addr, nvals,
     &        iint, nvlout, nread, ilabs
      integer iii,jjj,kkk,lll,kmo,lmo,tmp,nmotop
c
      nvals=0
      nvlout=0
      nread = 0
c
c  begin the sort:
c  read a new record of integrals and labels...
c
100   continue
      nread = nread + 1
      call sifrd2(sfile, info,  nipv,  iretbv,
     &            buffer,nbuf,   last,  itypea,
     &            itypeb, ibvtyp,vals,
     &            labs,  ibitv, ierr)
c
      do 2000 iint=1,nbuf
c
         valx=vals(iint)
c
c  check for blocking flag and ignore value if set.
c
         if(labs(1,iint).eq.0)go to 2000
c
         nvals=nvals+1
         if (assume_fc.gt.0) then 
           labs(1,iint)=mapmo_fc(labs(1,iint))
           labs(2,iint)=mapmo_fc(labs(2,iint))
           labs(3,iint)=mapmo_fc(labs(3,iint))
           labs(4,iint)=mapmo_fc(labs(4,iint))
         endif 

         ii=max(labs(1,iint),labs(2,iint))
         jj=min(labs(1,iint),labs(2,iint))
         kk=max(labs(3,iint),labs(4,iint))
         ll=min(labs(3,iint),labs(4,iint))




! cfp_todo: this should stay the same
           iijj=nndx(ii)+jj
           kkll=nndx(kk)+ll

*@ifdef oldversion
*         addr=offmm(iijj)+addmm(kkll)
*         ibuk=(addr-1)/nvpsg+1
*         ibpt=ibfpt(ibuk)+1
*         ibfpt(ibuk)=ibpt
*         dabufv(ibpt,ibuk)=valx
*         idbufi(ibpt,ibuk)=addr
*         nvlout=nvlout+1
*c        write(6,801) 'D2', ii,jj,kk,ll,valx,addr,ibuk,ibpt
*         if(ibpt.eq.nvpbk)call wrtda(ibfpt(ibuk),ircpt(ibuk,dafile),
*     &    dabufv(1,ibuk),idbufi(1,ibuk),lendar,dabuf,nvpbk,dafile)
*c
*         if(iijj.eq.kkll)goto 2000
*@else 
c
cfp_todo: the address has to change if fewer integrals are
c    considered
c    add2 in cigrd5.f
c    take offii1 and addii?
c    only offset has to be changed in the first version (?)
cfp: for SA-MCSCF the 2-virt. distributions are skipped here
c      write(nlist,802)
c        nndx(ii),nndx(kk),iijj,kkll,offmm(iijj),addmm(kkll)
         if(offmm(iijj).lt.0)goto 1999
         addr=offmm(iijj)+addmm(kkll)
c -> omit this if rs are both virtual
         ibuk=(addr-1)/nvpsg+1
         ibpt=ibfpt(ibuk)+1
cfp_tmp: elements in bucket
         ibfpt(ibuk)=ibpt
         dabufv(ibpt,ibuk)=valx
         idbufi(ibpt,ibuk)=addr
         nvlout=nvlout+1
c         write(nlist,801) 'D2', ii,jj,kk,ll,valx,addr,ibuk,ibpt
c801      format(" nndx",2i4," iijj,kkll",2i4," offmm=",i4,"
c     .       addmm=",i4)
         if(ibpt.eq.nvpbk)call wrtda(ibfpt(ibuk),ircpt(ibuk,dafile),
     &    dabufv(1,ibuk),idbufi(1,ibuk),lendar,dabuf,nvpbk,dafile)
c
cfp_tmp: write a second time g_rstu -> g_turs
 1999     continue
c      write(nlist,801) nndx(kk),nndx(ii),kkll,iijj,offmm(kkll),addmm(iijj)
           if(iijj.eq.kkll)goto 2000
          if(offmm(kkll).lt.0)goto 2000
*@endif 



         addr=offmm(kkll)+addmm(iijj)
         ibuk=(addr-1)/nvpsg+1
         ibpt=ibfpt(ibuk)+1
         ibfpt(ibuk)=ibpt
         dabufv(ibpt,ibuk)=valx
         idbufi(ibpt,ibuk)=addr
         nvlout=nvlout+1
c        write(6,801) 'D2', kk,ll,ii,jj,valx,addr,ibuk,ibpt
         if(ibpt.eq.nvpbk)call wrtda(ibfpt(ibuk),ircpt(ibuk,dafile),
     &    dabufv(1,ibuk),idbufi(1,ibuk),lendar,dabuf,nvpbk,dafile)
c
c  end of sequential buffer loop...
2000  continue
c
c  another buffer?...
      if(last.eq.more) go to 100


      if (assume_fc.gt.0) then
c
c      add the remaining density matrix elements
c      only non-redundant canonical ordered d2 elements
c      need for some pre-factor due to permutational symmetry ?
c
c
       do i1=1,nmotd
         if (mapmo_fc(i1).lt.0) then
            ioff=i1
            exit
         endif
       enddo

       nmotop=nmotd+1
       do i1=ioff+1,nmotd
         if (mapmo_fc(i1).eq.0) then
             nmotop=i1
             exit
         endif
       enddo 

      write(6,'(a,i6,a,i6)') 'offset fc:',ioff,' offset fv',nmotop
c
c     ioff is now the first frozen core orbital index
c
c      case 1: closed shell scf density 
c      in case of transition densities, there is no contribution
c      from the closed shell scf density
c
       if (nadcalc.gt.0) goto 802 

c      do i1=ioff, nmotd
       do i1=ioff, nmotop-1
        do j1=ioff, i1
          do k1=1,2
c     D(ii,ii,jj,jj)
          if (k1.eq.1) then  
            ii =abs(mapmo_fc(i1))
            jj = ii
            kk = abs(mapmo_fc(j1))
            ll = kk  
            ii=max(ii,kk)
            ll=min(jj,ll)
            jj=ii
            kk=ll 
            if (i1.eq.j1) then 
                valx=2.0d0 
            else
                valx=4.0d0
            endif
c     D(ii,jj,ii,jj)
          elseif(i1.ne.j1) then
           ii=abs(mapmo_fc(i1))
           jj=abs(mapmo_fc(j1))
           kk=ii
           ll=jj 
           ii=max(ii,jj)
           ll=min(kk,ll)
           jj=ll
           kk=ii
           valx=-1.0d0
          else
            cycle 
          endif 
 801    format(a,4i4,'=',f10.6,' addr=',4i10)
         nvals=nvals+1

         iijj=nndx(ii)+jj
         kkll=nndx(kk)+ll
c
         addr=offmm(iijj)+addmm(kkll)
c        write(6,801) 'D2(scf)', ii,jj,kk,ll,valx,addr
         ibuk=(addr-1)/nvpsg+1
         ibpt=ibfpt(ibuk)+1
         ibfpt(ibuk)=ibpt
         dabufv(ibpt,ibuk)=valx
         idbufi(ibpt,ibuk)=addr
         nvlout=nvlout+1
         if(ibpt.eq.nvpbk)call wrtda(ibfpt(ibuk),ircpt(ibuk,dafile),
     &    dabufv(1,ibuk),idbufi(1,ibuk),lendar,dabuf,nvpbk,dafile)
c
         if(iijj.eq.kkll) cycle
         addr=offmm(kkll)+addmm(iijj)
c        write(6,801) 'D2(scf)', ii,jj,kk,ll,valx,addr
         ibuk=(addr-1)/nvpsg+1
         ibpt=ibfpt(ibuk)+1
         ibfpt(ibuk)=ibpt
         dabufv(ibpt,ibuk)=valx
         idbufi(ibpt,ibuk)=addr
         nvlout=nvlout+1
         if(ibpt.eq.nvpbk)call wrtda(ibfpt(ibuk),ircpt(ibuk,dafile),
     &    dabufv(1,ibuk),idbufi(1,ibuk),lendar,dabuf,nvpbk,dafile)

          enddo  
        enddo
       enddo

  802   continue
c
c      case 2: FC,FC',NON-FC,NON-FC case  
c       again FC=FC'
c       perm = i1 i1 j1>k1 
c              i1 j1 i1 k1
c
c      do i1=ioff, nmotd
c      frozen core  i1 
       do i1=ioff, nmotop-1
c      active   j1  ,k1   
        do j1=1,ioff-1 
          do k1=1,j1
          iii=abs(mapmo_fc(i1))
          kkk=mapmo_fc(j1)
          lll=mapmo_fc(k1)
c         D(kkk,lll) vanishes due to symmetry
          if (symm(kkk).ne.symm(lll)) cycle
          if (kkk.lt.lll) then
               tmp=kkk
               kkk=lll
               lll=tmp
          endif  
          do l1=1,2
           if (l1.eq.1) then
c        i1,i1, j1,k1
           kmo=kkk-nsm(symm(kkk))
           lmo=lll-nsm(symm(kkk))
           valx=2.0d0*d1(nnsm(symm(kkk))+nndx(kmo)+lmo)
c           write(6,*) 
c    .     'd1(',nnsm(symm(kk)),nndx(kmo)+lmo,'kmo,lmo=',kmo,lmo
           if (iii.gt.kkk) then
             ii=iii
             jj=iii
             kk=kkk
             ll=lll
           else
             ii=kkk
             jj=lll
             kk=iii
             ll=iii
           endif
          else
           kmo=kkk-nsm(symm(kkk))
           lmo=lll-nsm(symm(kkk))
           valx=-0.5d0*d1(nnsm(symm(kkk))+nndx(kmo)+lmo)
           if (iii.gt.kkk) then
             ii=iii
             jj=kkk
             kk=iii
             ll=lll
           else
             ii=kkk
             jj=iii
             if (lll.gt.iii) then
             kk=lll
             ll=iii
             else
             kk=iii
             ll=lll
             endif 
           endif
         endif 
          nvals=nvals+1

         iijj=nndx(ii)+jj
         kkll=nndx(kk)+ll
c
         addr=offmm(iijj)+addmm(kkll)
c        write(6,801) 'D2(mixed)', ii,jj,kk,ll,valx,addr
         ibuk=(addr-1)/nvpsg+1
         ibpt=ibfpt(ibuk)+1
         ibfpt(ibuk)=ibpt
         dabufv(ibpt,ibuk)=valx
         idbufi(ibpt,ibuk)=addr
         nvlout=nvlout+1
         if(ibpt.eq.nvpbk)call wrtda(ibfpt(ibuk),ircpt(ibuk,dafile),
     &    dabufv(1,ibuk),idbufi(1,ibuk),lendar,dabuf,nvpbk,dafile)
c
         if(iijj.eq.kkll) cycle 
         addr=offmm(kkll)+addmm(iijj)
c        write(6,801) 'D2(mixed)', ii,jj,kk,ll,valx,addr
         ibuk=(addr-1)/nvpsg+1
         ibpt=ibfpt(ibuk)+1
         ibfpt(ibuk)=ibpt
         dabufv(ibpt,ibuk)=valx
         idbufi(ibpt,ibuk)=addr
         nvlout=nvlout+1
         if(ibpt.eq.nvpbk)call wrtda(ibfpt(ibuk),ircpt(ibuk,dafile),
     &    dabufv(1,ibuk),idbufi(1,ibuk),lendar,dabuf,nvpbk,dafile)

          enddo
          enddo
        enddo
       enddo
      endif



c
c  dump partially filled buckets...
c
      call fndast(dafile,dabufv,idbufi,dabuf)
c
      write(nlist,6010)nvals,nvlout
6010  format(' sort1: nvals=',i10,' nvlout=',i10)
c
      return
      end
c deck sqtotr
      subroutine sqtotr(hx,h,n)
c
c  symmetrize the square matrix hx(*,*) and return the result in the
c  triangular packed matrix h(*).
c
      implicit integer(a-z)
      real*8 hx(n,n),h(*)
c
      real*8    half
      parameter(half=5d-1)
c
      ij0=0
      do 20 i=1,n
         do 10 j=1,(i-1)
            h(ij0+j)=half*(hx(i,j)+hx(j,i))
10       continue
         ij0=ij0+i
         h(ij0)=hx(i,i)
20    continue
c
      return
      end
c deck sqtr1
      subroutine sqtr1(hx,h,npsy,nsym)
c
c  symmetrize the square-packed matrix hx(*) and return the result in
c  the lower-triangular-packed array h(*) by symmetry blocks.
c
      implicit integer(a-z)
      real*8 h(*),hx(*)
      integer npsy(*)
c
      iipt=1
      i2pt=1
      do 10 isym=1,nsym
         n=npsy(isym)
         if(n.gt.0)call sqtotr(hx(i2pt),h(iipt),n)
         iipt=iipt + (n*(n+1))/2
         i2pt=i2pt + n*n
10    continue
c
      return
      end
c deck sxd1
      subroutine sxd1(nu,np,na,xd,dii,dvi)
c
c  symmetrize and unpack a block of the xd1(*) matrix.
c  ii elements go to dii(*),  vi elements go to dvi(*).
c
c  written by ron shepard.
c  version 25-aug-87
c
      implicit integer(a-z)
      real*8 xd(nu,np),dii(*),dvi(*)
c
c  ii elements...
c
      pq0=0
      do 200 p=1,np
         do 100 q=1,p
            dii(pq0+q)=xd(p,q)+xd(q,p)
100      continue
         pq0=pq0+p
200   continue
c
c  vi elements...
c
      if(na.gt.0)then
         np1=np+1
         ap=1
         do 300 p=1,np
            call dcopy_wr(na,xd(np1,p),1,dvi(ap),1)
            ap=ap+na
300      continue
      endif
c
      return
      end
c deck sxd2
      subroutine sxd2(nu,np,nq,na,p0,q0,rs,nndx,offii1,addii,
     & xd,diiii,dviii)
c
c             psym .ne. qsym
c  symmetrize and unpack a block of x*d2.  iiii elements go to diiii(*),
c  viii elements go to dviii(*).
c
c  written by ron shepard.
c  version 25-aug-87
c
      implicit integer(a-z)
      real*8 xd(nu,nq),diiii(*),dviii(*)
      integer nndx(*),offii1(*),addii(*)
c
c  iiii elements...
c
      if(np.ne.0)then
         do 200 q=1,nq
            qt=q0+q
            do 100 p=1,np
               pt=p0+p
               pq=nndx(max(pt,qt))+min(pt,qt)
               pqrs=offii1(max(pq,rs))+addii(min(pq,rs))
               diiii(pqrs)=diiii(pqrs)+xd(p,q)
100         continue
200      continue
      endif
c
c  viii terms...
c
      if(na.gt.0)then
         np1=np+1
         ap=1
         do 300 q=1,nq
            call dcopy_wr(na,xd(np1,q),1, dviii(ap),1)
            ap=ap+na
300      continue
      endif
c
      return
      end
c deck sxd2e
      subroutine sxd2e(nu,np,na,p0,rs,nndx,offii1,addii,xd,diiii,dviii)
c
c                psym = qsym
c  symmetrize and unpack a block of x*d2.  iiii terms go to diiii(*),
c  viii terms go to dviii(*).
c
c  written by ron shepard.
c  version 25-aug-87
c
      implicit integer(a-z)
      real*8 diiii(*),dviii(*),xd(nu,np)
      integer nndx(*),offii1(*),addii(*)
c
c  iiii elements...
c
      do 200 p=1,np
         pq=nndx(p0+p)+p0
         do 100 q=1,p
            pq=pq+1
            pqrs=offii1(max(pq,rs))+addii(min(pq,rs))
            diiii(pqrs)=diiii(pqrs)+xd(p,q)+xd(q,p)
100      continue
200   continue
c
c  viii elements...
c
      if(na.gt.0)then
         np1=np+1
         ap=1
         do 300 p=1,np
            call dcopy_wr(na,xd(np1,p),1, dviii(ap),1)
            ap=ap+na
300      continue
      endif
c
      return
      end
c deck tred2i
      subroutine tred2i(nm,n,z,d,e)
c
      integer i,j,k,l,n,ii,nm,jp1
      real*8  d(n),e(n),z(nm,n)
      real*8  f,g,h,hh,scale
c      double precision sqrt,abs,sign
c
c     this subroutine is a translation of the algol procedure tred2,
c     num. math. 11, 181-195(1968) by martin, reinsch, and wilkinson.
c     handbook for auto. comp., vol.ii-linear algebra, 212-226(1971).
c
c     modified to perform the diagonalization in-place by removing
c     the references to a(*) in the original version of tred2.
c     the input is simplified accordingly.
c     19-apr-85 ron shepard.
c
c     this subroutine reduces a real symmetric matrix to a
c     symmetric tridiagonal matrix using and accumulating
c     orthogonal similarity transformations.
c
c     on input:
c
c        nm must be set to the row dimension of two-dimensional
c          array parameters as declared in the calling program
c          dimension statement;
c
c        n is the order of the matrix;
c
c        z contains the real symmetric input matrix.  only the
c          lower triangle of the matrix need be supplied.
c
c     on output:
c
c        d contains the diagonal elements of the tridiagonal matrix;
c
c        e contains the subdiagonal elements of the tridiagonal
c          matrix in its last n-1 positions.  e(1) is set to zero;
c
c        z contains the orthogonal transformation matrix
c          produced in the reduction;
c
c     questions and comments should be directed to b. s. garbow,
c     applied mathematics division, argonne national laboratory
c
c     -----------------------------------------------------------------
c
      if (n .eq. 1) go to 320
c     :::::::::: for i=n step -1 until 2 do -- ::::::::::
      do 300 ii = 2, n
         i = n + 2 - ii
         l = i - 1
         h = 0.0d0
         scale = 0.0d0
         if (l .lt. 2) go to 130
c     :::::::::: scale row (algol tol then not needed) ::::::::::
         do 120 k = 1, l
  120    scale = scale + abs(z(i,k))
c
         if (scale .ne. 0.0d0) go to 140
  130    e(i) = z(i,l)
         go to 290
c
  140    do 150 k = 1, l
            z(i,k) = z(i,k) / scale
            h = h + z(i,k) * z(i,k)
  150    continue
c
         f = z(i,l)
         g = -sign(sqrt(h),f)
         e(i) = scale * g
         h = h - f * g
         z(i,l) = f - g
         f = 0.0d0
c
         do 240 j = 1, l
            z(j,i) = z(i,j) / h
            g = 0.0d0
c     :::::::::: form element of a*u ::::::::::
            do 180 k = 1, j
  180       g = g + z(j,k) * z(i,k)
c
            jp1 = j + 1
            if (l .lt. jp1) go to 220
c
            do 200 k = jp1, l
  200       g = g + z(k,j) * z(i,k)
c     :::::::::: form element of p ::::::::::
  220       e(j) = g / h
            f = f + e(j) * z(i,j)
  240    continue
c
         hh = f / (h + h)
c     :::::::::: form reduced a ::::::::::
         do 260 j = 1, l
            f = z(i,j)
            g = e(j) - hh * f
            e(j) = g
c
            do 260 k = 1, j
               z(j,k) = z(j,k) - f * e(k) - g * z(i,k)
  260    continue
c
  290    d(i) = h
  300 continue
c
  320 d(1) = 0.0d0
      e(1) = 0.0d0
c     :::::::::: accumulation of transformation matrices ::::::::::
      do 500 i = 1, n
         l = i - 1
         if (d(i) .eq. 0.0d0) go to 380
c
         do 360 j = 1, l
            g = 0.0d0
c
            do 340 k = 1, l
  340       g = g + z(i,k) * z(k,j)
c
            do 360 k = 1, l
               z(k,j) = z(k,j) - g * z(k,i)
  360    continue
c
  380    d(i) = z(i,i)
         z(i,i) = 1.0d0
         if (l .lt. 1) go to 500
c
         do 400 j = 1, l
            z(i,j) = 0.0d0
            z(j,i) = 0.0d0
  400    continue
c
  500 continue
c
      return
      end
c
c***********************************************************************
c
      subroutine wci(fci,iorder,wad,waa,wvd,wva)
c
c  construct the orbital gradient vector from the ci fock matrix
c  for the essential mcscf orbital rotations.
c  index combinations are determined from mcscf orbital types.
c
c              w(uv) = 2 ( f(u,v) - f (v,u) ) * sign
c
c  where the sign depends on the orbital types and must agree with
c  the conventions used in the mcscf program (or where the hessian
c  matrix was computed).
c
c  nadcalc=1: DCI(J,I,alpha) calculation
c  nadcalc=2: DCSF(J,I,alpha) calculation
c  nadcalc=3: DCI(J,I,alpha)+DCSF(J,I,alpha) calculation
c
c  here:nadcalc=2 or 3,in both cases the DCSF contr. is calculated here:
c
c   w(uv)[J,I] = 2 ( f(u,v) - f (v,u) + D(u,v)[J,I](E[I]-E[J]) * sign
c
c  input:
c  fci(*) = ci fock matrix.
c  iorder(*) = essential active-active orbital rotation mapping vector.
c
c  output:
c  wad(*) = active-inactive orbital rotation gradient.
c  waa(*) = nonredundant active-active gradient vector.
c  wvd(*) = virtual-inactive gradient.
c  wva(*) = virtual-active gradient.
c
c  sign of wva,wvd,wad,waa gradients changed (m.e.)
c  written by ron shepard.
c
       implicit none
c   ##  parameter & common block section
c
      integer nfilmx
      parameter(nfilmx=31)
c
      integer nmomx
      parameter(nmomx=1023)
c
      real*8    zero,     two
      parameter(zero=0d0, two=2d0)
c
      real*8 d1atr(nmomx,nmomx)
      common /d1tr/ d1atr
c
      real*8 energy1,energy2
      integer nadcalc,drt1,drt2,nroot1,nroot2
      common /nad/ energy1,energy2,nadcalc,drt1,drt2,nroot1,nroot2
      integer iunits,nlist
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nsm(8),n2sm(8),ird1(8),ird2(8),
     &        ira1(8),ira2(8),irv1(8),irv2(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c   ##  integer section
c
      integer a,ai,ap,at,apx,aix
      integer iorder(*),i,ipx,isym,it,ip,ia
      integer nu,n2m
      integer pi,pqx,pq,pt,pa,p
      integer q,qp,qpx,qt
      integer u0
c
c   ##  real*8 section
c
      real*8 fci(*)
      real*8 wad(*),waa(*),wvd(*),wva(*)
      real*8 wnorm
c
c-----------------------------------------------------------------------
c
      wnorm=zero
      ipx=0
      aix=0
      apx=0
      do 400 isym=1,nsym
         u0=nsm(isym)
         n2m=n2sm(isym)
         nu=nmpsy(isym)
c
c  active-inactive rotations...
c
         do 220 pt=ira1(isym),ira2(isym)
            p=mapma(pt)-u0
            do 210 it=ird1(isym),ird2(isym)
               i=mapmd(it)-u0
               ip=n2m+(p-1)*nu+i
               pi=n2m+(i-1)*nu+p
               ipx=ipx+1
               wad(ipx)=two*(fci(pi)-fci(ip))
                if ((nadcalc.eq.3).or.(nadcalc.eq.2)) then
c& corrected by PGSZ 20/5/2009 Reims:
c& d1atr addressed by absulute indices!!!!!!!
                 wad(ipx) = wad(ipx) 
     &               + two*d1atr(p+u0,i+u0)*(energy1-energy2)
c                write(nlist,*)'nad:essential:a-i wad(ipx)',p,i,
c    &            d1atr(p,i),'sum:',ipx,wad(ipx)
                endif
210         continue
220      continue
c
c  active-active rotations...
c
         do 240 pt=(ira1(isym)+1),ira2(isym)
            p=mapma(pt)-u0
            do 230 qt=ira1(isym),(pt-1)
               q=mapma(qt)-u0
               pq=n2m+(q-1)*nu+p
               qp=n2m+(p-1)*nu+q
               pqx=iorder(nndx(pt)+qt)
               if(pqx.gt.0) then
                waa(pqx)=two*(fci(pq)-fci(qp))
                if ((nadcalc.eq.3).or.(nadcalc.eq.2)) then
c& corrected by PGSZ 20/5/2009 Reims:
c& d1atr addressed by absulute indices!!!!!!!
                 waa(pqx) = waa(pqx) 
     &               + two*d1atr(p+u0,q+u0)*(energy1-energy2)
c                write(nlist,*)'nad:essential:a-a waa(pqx)',p,q,
c    &            d1atr(p,q),'sum:',pqx,waa(pqx)
                endif
               endif ! end of: if(pqx.gt.0) then
230         continue
240      continue
c
c  virtual-inactive rotations...
c
         do 320 it=ird1(isym),ird2(isym)
            i=mapmd(it)-u0
            do 310 at=irv1(isym),irv2(isym)
               a=mapmv(at)-u0
               ai=n2m+(i-1)*nu+a
               ia=n2m+(a-1)*nu+i
               aix=aix+1
               wvd(aix)=two*(fci(ia)-fci(ai))
                if ((nadcalc.eq.3).or.(nadcalc.eq.2)) then
c& corrected by PGSZ 20/5/2009 Reims:
c& d1atr addressed by absulute indices!!!!!!!
                 wvd(aix) = wvd(aix) 
     &            + two*d1atr(a+u0,i+u0)*(energy1-energy2)
c                write(nlist,*)'nad:essential:d-v wvd(aix)',a,i,
c    &            d1atr(a+u0,i+u0),'sum:',aix,wvd(aix)
                endif
310         continue
320      continue
c
c  virtual-active rotations...
c
         write(6,*)'essential virtual-active rotations'
         do 340 pt=ira1(isym),ira2(isym)
            p=mapma(pt)-u0
            do 330 at=irv1(isym),irv2(isym)
               a=mapmv(at)-u0
               ap=n2m+(p-1)*nu+a
               pa=n2m+(a-1)*nu+p
               apx=apx+1
               wva(apx)=two*(fci(pa)-fci(ap))
c              write(*,*)'pa,ap,fci(pa),fci(ap),wva(apx): ',
c     &          pa,ap,fci(pa),fci(ap),wva(apx)
                if ((nadcalc.eq.3).or.(nadcalc.eq.2)) then
c                write(*,*)'a,p,d1atr(a,p): ',a,p,d1atr(a,p)
c  NOTE: d1atr(p,a) addresses the upper diagonal block
c& corrected by PGSZ 20/5/2009 Reims: 
c& d1atr addressed by absulute indices!!!!!!!
                 wva(apx) = wva(apx) 
     &              + two*d1atr(p+u0,a+u0)*(energy1-energy2)
c                write(nlist,*)'pa,ap,fci1pa),fci(ap)',
c    &            pa,ap,fci(pa),fci(ap)
                endif
330         continue
340      continue
c
400   continue
c
      return
      end
c deck wrnat
      subroutine wrnat(cno,ocn,nsym,nmpsy,n2sm,title)
      implicit real*8 (a-h,o-z)
c
c       subroutine write out the natural occupations and orbitals
c       for the property program
c
c feb-90 modified by (m.e.)
c written by p. szalay
c
      dimension cno(*),ocn(*),nmpsy(8),n2sm(8)
      character*80 title,fmt
      character*4  blanc
      integer effno
c
      parameter(nfilmx=31)
      character*60 fname
      common/cfname/fname(nfilmx)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
      equivalence (iunits(16),effno)
cvp
c  --- mowrit
      integer filerr,syserr,faterr
      character*80 tit(10)
      character*4 labels
c
c
      data iprop/-1/,blanc/'    '/,i0/0/,fmt/'(4f20.14)'/
c
      rewind effno
c
c     write(effno,101) title
c     write(effno,101) ' output from cigrd program '
c     write(effno,102) iprop,nsym
c     write(effno,101) fmt
c
c     noc=1
c
c     do 201 isym=1,nsym
c       nmo=nmpsy(isym)
c       nmt=n2sm(isym)
c       write(effno,101) blanc
c       write(effno,102) nmo,nmo,nmo,i0
c       write(effno,103)(ocn(i),i=noc,noc+nmo-1)
c       nmt1=nmt+1
c       do 202 imo=1,nmo
c         write(effno,fmt)(cno(i),i=nmt1,nmt1+nmo-1)
c         nmt1=nmt1+nmo
c202    continue
c       noc=noc+nmo
c201  continue
c
*@ifdef convex
*      write(effno,*)1234567890
*      write(effno,*)1234567890
*@endif
c
c  writing header
c
      call mowrit(effno,10,filerr,syserr,2,tit,fmt,nsym,
     &            nmpsy,nmpsy,labels,cno)
c
c  writing nocoef
c
      call mowrit(effno,20,filerr,syserr,2,tit,fmt,nsym,
     &            nmpsy,nmpsy,labels,cno)
c
c  writing occupation
c
      call mowrit(effno,40,filerr,syserr,2,tit,fmt,nsym,
     &            nmpsy,nmpsy,labels,ocn)
cvp
c
      write(nlist,301)
c
 301  format(/' wrnat: orbital occupations and coefficents',
     x ' for the effective density matrix d1_eff(:,:) were written.')
 101  format(a)
 102  format(20i4)
 103  format(6f12.9)
      return
      end
c deck wrtda
      subroutine wrtda(ibfpt,ircpt,bufv,ibuf,lendar,dabuf,nvpbk,idafl)
      use paralib
c
c  pack values and labels into the da buffer and write it out.
c  buffer and record pointers are updated on return.
c
      implicit integer(a-z)
c
      real*8 bufv(*),dabuf(lendar)
      integer ibuf(*)
c
      integer info(2)
      equivalence (info(1),last), (info(2),num)
c
      num=ibfpt
      last=ircpt
c
c  copy integrals...
c
      call dcopy_wr(num,bufv,1,dabuf,1)
c
c  pack labels...
c
      nuw=num
      call plab32(dabuf(nvpbk+1),ibuf,nuw)
c
c  pack record info into the last buffer word...
c
      nuw=2
      call plab32(dabuf(lendar),info,nuw)
c
c  write the da buffer to the next record and update pointers...
c
      call dawrt(idafl,dabuf,ircpt)
      ibfpt=0
c
      return
      end
c deck wthmcd
      subroutine wthmcd(
     & denfl,  ntitle, title,  info,
     & energy, ietype, nenrgy, nsym,
     & nipsy,  nmpsy,  elast,  nimot,
     & nmot,   mapim,  mcres,  bfnlab,
     & slabel)
c
c  write the header info to the mcscf density matrix file.
c
c  input:
c  denfl = density file unit number.
c  ntitle = number of titles.
c  title(*) = descriptive titles.
c  nsym = number of symmetry blocks.
c  nipsy = number of mcscf-occupied orbitals of each symmetry.
c  nmpsy = number of basis functions of each symmetry.
c  elast = mcscf energy.
c  nimot = total number of mcscf-occupied orbitals.
c  nmot = total number of orbitals.
c  mapim(*) = mo-to-internal mapping vector.
c  mcres(*) = mcscf orbital resolution array.  this array determines
c             the types of orbital transformations that may be applied
c             to the density matrices that are consistent with the
c             original csf expansion set.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 elast
      character*80 title(ntitle)
      integer nipsy(nsym), nmpsy(nsym), mapim(nmot), mcres(nmot)
c
      real*8    one,     zero
      parameter(one=1d0, zero=0d0)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer ninfo, ntitle, denfl, nsym, nmot, nenrgy, nmap
      integer info(10), ietype(*)
      real*8  energy(*)
      character*4 slabel(*)
      character*8 bfnlab(*)
      rewind denfl
c
c     # define some fudged symmetry labels
c
      do 22 i = 1,nsym
         write(slabel(i),'(a3,i1)')'sym',i
 22   continue
c
c     # assign some bogus basis function labels if none have been
c     # set previously.
c
      do 23 ibogus = 1, nmot
         if ( bfnlab(ibogus) .eq. ' ' ) then
            write(bfnlab(ibogus),'(a5,i3.3)') 'effmo', ibogus
         endif
 23   continue
c
      ninfo  = 6
      nmap   = 1
      imtype = 2
c
      call sifwh(
     & denfl,  ntitle,   nsym,  nmot,
     &  ninfo,  nenrgy,   nmap, title,
     &  nmpsy,  slabel,   info, bfnlab,
     & ietype,  energy, imtype,  mcres,
     & ierr )
      if( ierr .ne. 0 ) call bummer('wmcd1f:sifwh ierr=',ierr,faterr)
c
c      write(denfl,iostat=ierr)ntitle,title,nsym,
c     & nipsy,nmpsy,elast,nimot,nmot,mapim,mcres
c      if(ierr.ne.0)call bummer('wthmcd: ierr=',ierr,faterr)
c
      return
      end
c deck xorbl
      subroutine xorbl(naar,orbl,iorder,x)
c
c  expand the orbital lambda matrix elements into the x(*) array for
c  subsequent use in matrix operations.
c
c  signs used must agree with routine wci() and with the mcscf program
c  conventions.
c
c  input:
c  nadt,naar,nvdt= lambda vector component counts.
c  orbl(*) = orbital lambda vector (da,aa,vd,va).
c  iorder(*) = essential active-active mapping vector.
c
c  output:
c  x(*) = symmetry blocked mo by internal orbital expanded vector.
c         internal orbital entries are antisymmetric.
c
c  written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer iorbx,ix0
      common/corbc/iorbx(nmomx,8),ix0(3)
      integer nndx(nmomx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcre
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nsm(8),ird1(8),ird2(8),ira1(8),ira2(8),
     &        irv1(8),irv2(8),tsymad(8),tsymvd(8),nadt,nvdt
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
      equivalence (tsymad(1),nadt)
      equivalence (tsymvd(1),nvdt)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 orbl(*),x(nmit)
c
c   ##  integer section
c
      integer at,ap,aj
      integer iorder(*),iq,isym,i0,ip,ij
      integer jp,jt
      integer mpiq, mqip, maip, ma, mcres, mj, mp, mjip, mpij,
     &        mq, maij
      integer nu,naar
      integer pq,pqx,pt
      integer qt
      integer u0,ui0
c
      call wzero(nmit,x,1)
c
      jp=0
      pq=nadt
      aj=pq+naar
      ap=aj+nvdt
      do 500 isym=1,nsym
         nu=nmpsy(isym)
         u0=nsm(isym)
         i0=nsi(isym)
         ui0=nsmi(isym)
c
c  inactive-active terms...
c
         do 120 pt=ira1(isym),ira2(isym)
            mp=mapma(pt)-u0
            ip=mapivm(mapma(pt))-i0
            do 110 jt=ird1(isym),ird2(isym)
               jp=jp+1
               mj=mapmd(jt)-u0
               ij=mapivm(mapmd(jt))-i0
               mjip=ui0+(ip-1)*nu+mj
               mpij=ui0+(ij-1)*nu+mp
               x(mjip)=+orbl(jp)
               x(mpij)=-orbl(jp)

110         continue
120      continue
c
c  active-active terms...
c
         do 220 pt=ira1(isym),ira2(isym)
            mp=mapma(pt)-u0
            ip=mapivm(mapma(pt))-i0
            do 210 qt=ira1(isym),(pt-1)
               pqx=iorder(nndx(pt)+qt)
               if(pqx.ne.0)then
                  pq=pq+1
                  mq=mapma(qt)-u0
                  iq=mapivm(mapma(qt))-i0
                  mqip=ui0+(ip-1)*nu+mq
                  mpiq=ui0+(iq-1)*nu+mp
                  x(mpiq)=-orbl(pq)
                  x(mqip)=+orbl(pq)
               endif
210         continue
220      continue
c
c  virtual-inactive terms...
c
         do 320 jt=ird1(isym),ird2(isym)
            ij=mapivm(mapmd(jt))-i0
            do 310 at=irv1(isym),irv2(isym)
               aj=aj+1
               ma=mapmv(at)-u0
               maij=ui0+(ij-1)*nu+ma
               x(maij)=+orbl(aj)
310         continue
320      continue
c
c  virtual-active terms...
c
         do 420 pt=ira1(isym),ira2(isym)
            ip=mapivm(mapma(pt))-i0
            do 410 at=irv1(isym),irv2(isym)
               ap=ap+1
               ma=mapmv(at)-u0
               maip=ui0+(ip-1)*nu+ma
               x(maip)=+orbl(ap)
410         continue
420      continue
500   continue
c
      return
      end
c deck xpnd1
      subroutine xpnd1(h,hx,npsy,nsym)
c
c  expand the lower-triangular-packed array h(*) into square-packed
c  symmetric integral array hx(*) by symmetry blocks.
c
c  written by ron shepard.
c  version 31-dec-88
c
      implicit integer(a-z)
      real*8 h(*),hx(*)
      integer npsy(nsym)
c
      iipt=1
      i2pt=1
      do 10 isym=1,nsym
         n=npsy(isym)
         if(n.gt.0)call sm11sq(h(iipt),hx(i2pt),n)
         iipt=iipt + (n*(n+1))/2
         i2pt=i2pt + n*n
10    continue
c
      return
      end
c deck xpnd2
      subroutine xpnd2(d2,dx,uvsym,uv,
     & nndx,npsy,iran1,iran2,
     & mult,off,add,smpt,nsym)
c
c  expand and gather the (uv) distribution of
c  two-particle density matrix elements.
c
c  written by ron shepard.
c  version 25-aug-87
c
      implicit integer(a-z)
c
      real*8 d2(*),dx(*)
      integer nndx(*),off(*),add(*)
      integer npsy(8),smpt(8),iran1(8),iran2(8),mult(8,8)
c
      real*8 dtemp
c
      if(uvsym.ne.1)then
c
c  psym .ne. tsym case:
c  because of matrix symmetry, only tsym.gt.psym blocks are gathered.
c
         ipt=1
         do 300 psym=1,nsym
            tsym=mult(psym,uvsym)
            if(tsym.lt.psym)go to 300
            np=npsy(psym)
            if(np.eq.0)go to 300
            nt=npsy(tsym)
            if(nt.eq.0)go to 300
            smpt(tsym)=ipt
            p1=iran1(psym)
            p2=iran2(psym)
            t1=iran1(tsym)
            t2=iran2(tsym)
            do 200 p=p1,p2
               do 100 t=t1,t2
                  pt=nndx(t)+p
                  ptuv=off(max(pt,uv))+add(min(pt,uv))
                  dx(ipt)=d2(ptuv)
                  ipt=ipt+1
100            continue
200         continue
300      continue
c
      else
c
c  psym.eq.tsym case:
c  symmetric blocks are returned square-packed.
c
         p0=0
         n2=1
         do 1300 psym=1,nsym
            np=npsy(psym)
            if(np.eq.0)go to 1300
            smpt(psym)=n2
            ip1=n2
            i0p=n2-1
            do 1200 p=1,np
               ipt=ip1
               pt0=nndx(p0+p)+p0
cdir$ ivdep
               do 1100 t=1,(p-1)
                  pt=pt0+t
                  dtemp=d2(off(max(pt,uv))+add(min(pt,uv)))
                  dx(i0p+t)=dtemp
                  dx(ipt)=dtemp
                  ipt=ipt+np
1100           continue
               pt=pt0+p
               dx(ipt)=d2(off(max(pt,uv))+add(min(pt,uv)))
               i0p=i0p+np
               ip1=ip1+1
1200        continue
            n2=n2+np*np
            p0=p0+np
1300     continue
c
      endif
c
      return
      end

