!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccigrd1.f
ccigrd part=1 of 9.  workspace allocation routine
cversion=4.1 last modified: 24-apr-92
c deck cigrd
      program cigrd
      use paralib
      implicit none 
      include "../colib/getlcl.h"
c
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer ifirst, lcore,ierr
      integer*4  ierr4
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer lcored
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.
c
!C
!C     # use standard f90 memory allocation.
!C
      integer mem1
      parameter ( lcored = 2 500 000 )
      real*8, allocatable :: a(:)
      allocate( a(1:2500000),stat=ierr )
      deallocate(a)
*@ifdef parallel
*      call mpi_init(ierr4)
*      call ga_initialize()
*@endif 
      ifirst = 1
      mem1   = 0
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('cigrd: from getlcl: ierr=',ierr,faterr)
      endif
*@ifdef parallel
*       if (par_me().gt.0) lcore=100000
*@endif
      allocate( a(1:lcore),stat=ierr )
      write(0,*) 'allocate: status=',ierr
      ifirst=1
      mem1=0
       

c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
c
      call driver ( a(ifirst), (lcore), (mem1), (ifirst) )
c
c
c     # return from driver() implies successful execution.
c
c     # message-passing cleanup: stub if not in parallel
c
*@ifdef parallel
*      call ga_sync()
*      call ga_terminate()
*      call mpi_finalize(ierr4)
*@endif 
      call bummer('normal termination',0,3)
      stop 'end of cigrd'
      end

