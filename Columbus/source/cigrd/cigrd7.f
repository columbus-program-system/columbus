!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c
c***************************************************************
c
      subroutine cxvec(
     & b,      ib,     ndims,    s,
     & r,      ncfx,   nadt,     naar,
     & nvdt,   nvat,   iad,      iaa,
     & ivd,    iva,    ndimr,
     & cxs,    cxr,    xp,       z,
     & xbar,   rev,    ind,      modrt,
     & uaa,    ftbuf,  h2,       lenbft,
     & nwalk,  nsym,   add,      hdiag,
     & emc,    ib_c,   ib_m,     ns,
     & nr,     sscr,   avcsl3,   nipbuk,
     & lastb,  lenbuk, hmcvec,   core,
     & uad,    uvd,    uva,      iorder)
c
c  #  Cxr and Cxs matrix-vector product calculation for
c  #  the actual r abd s vector(s)
c  #  Note: for cdir=1 the number of new s and r vectors must be 1!
c
c   michal dallos XI/99
c-------------------------------------------------------------
c
       implicit none
c     common & parameter section
c
      real*8 one
      parameter (one=1.0d+00)
c
      integer nfilmx
      parameter(nfilmx=31)
c
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist)*navst(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer prtmicro
      common/output/prtmicro
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
      integer addpt,numint,bukpt,szh,intoff_mc
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c
c  ##  integer section
c
      integer add(*), avcsl3
      integer iad, iaa, ivd, iva, ib(15), ib_m(maxstat), iorder(*),
     &        ib_c(4,maxstat), icount, ist, ixx, icount2, is, ir
      integer lenbft, lastb, lenbuk
      integer ndims,ncfx,nadt,naar,nvdt,nvat,ndimr,nvec,
     &        nwalk, nsym, ns, nr, nipbuk
      integer modrt(0:*)

c
c  ##  real*8 section
c
      real*8 b(*)
      real*8 cxs(ndimr,*), cxr(ndims,*), core(*)
      real*8 dumf
      real*8 emc
      real*8 ftbuf(*)
      real*8 ind(*)
      real*8 h2(*), hmcvec(*)
      real*8 hdiag(*)
      real*8 rev(*),r(ndimr,*)
      real*8 s(ndims,*), sscr(ndims)
      real*8 uaa(*),uad(*),uvd(*),uva(*)
      real*8 xp(*),xbar(*)
      real*8 z(*)
c
c  ##  logical section
c
      logical lcxs, lcxr
      logical qinc,qind, qc(4)
c
c
      if (ndimr*ns.ne.0) call wzero(ndimr*ns,cxs,1)
      if (ndims*nr.ne.0) call wzero(ndims*nr,cxr,1)
c
      if (cdir.eq.0) then
c
       icount = 1
          do ist=1,nst
c
             do is=1, ns
c
              if (ib_c(1,ist).gt.0) then
               call gmtxv(b(ib_c(1,ist)),nadt,s(icount,is),
     &          ncsf_f(ist)*navst(ist),cxs(iad,is),one)
                if (prtmicro.ge.2)
     &           call prtvec(nlist,'Cxs[ad]',cxs(iad,is),nadt)
              endif ! (ib_c(1,ist).gt.0)
c
              if (ib_c(2,ist).gt.0) then
               call gmtxv(b(ib_c(2,ist)),naar,s(icount,is),
     &          ncsf_f(ist)*navst(ist),cxs(iaa,is),one)
                if (prtmicro.ge.2)
     &           call prtvec(nlist,'Cxs[aa]',cxs(iaa,is),naar)
              endif ! (ib_c(1,ist).gt.0)
c
              if (ib_c(3,ist).gt.0) then
               call gmtxv(b(ib_c(3,ist)),nvdt,s(icount,is),
     &          ncsf_f(ist)*navst(ist),cxs(ivd,is),one)
                if (prtmicro.ge.2)
     &           call prtvec(nlist,'Cxs[vd]',cxs(ivd,is),nvdt)
              endif ! (ib_c(1,ist).gt.0)
c
              if (ib_c(4,ist).gt.0) then
               call gmtxv(b(ib_c(4,ist)),nvat,s(icount,is),
     &          ncsf_f(ist)*navst(ist),cxs(iva,is),one)
                if (prtmicro.ge.2)
     &           call prtvec(nlist,'Cxs[va]',cxs(iva,is),nvat)
               endif ! (ib_c(1,ist).gt.0)
c
             enddo ! is=1, ns
c
             do ir =1,nr
c
              if((ib_c(1,ist).gt.0).and.(ncsf_f(ist).gt.0)) then
               call mxv(b(ib_c(1,ist)),ncsf_f(ist)*navst(ist),
     &          r(iad,ir),nadt,sscr)
               call daxpy_wr(ncsf_f(ist)*navst(ist),one,sscr,1,
     &          cxr(icount,ir),1)
                if (prtmicro.ge.2) then
                 icount2 = icount
                   do ixx =1,navst(ist)
                   write(nlist,
     &        '(/,3x,''Partial Cxr for DTR Nr.'',i3,'' state Nr.'',i3)')
     &              ist,ixx
                   call prtvec(nlist,'Cxr[csf] after C[csf,ad]',
     &              cxr(icount2,ir),ncsf_f(ist))
                   icount2 = icount2 + ncsf_f(ist)
                   enddo ! ixx =1,navst(ist)
                endif ! (prtmicro.ge.2)
              endif ! ((ib_c(1,ist).gt.0).and.(ncsf_f(ist).gt.0))
c
              if((ib_c(2,ist).gt.0).and.(ncsf_f(ist).gt.0)) then
               call mxv(b(ib_c(2,ist)),ncsf_f(ist)*navst(ist),
     &          r(iaa,ir),naar,sscr)
               call daxpy_wr(ncsf_f(ist)*navst(ist),one,sscr,1,
     &          cxr(icount,ir),1)
                if (prtmicro.ge.2) then
                 icount2 = icount
                   do ixx =1,navst(ist)
                   write(nlist,
     &        '(/,3x,''Partial Cxr for DTR Nr.'',i3,'' state Nr.'',i3)')
     &              ist,ixx
                   call prtvec(nlist,'Cxr[csf] after C[csf,aa]',
     &              cxr(icount2,ir),ncsf_f(ist))
                   icount2 = icount2 + ncsf_f(ist)
                   enddo ! ixx =1,navst(ist)
                endif ! (prtmicro.ge.2)
              endif ! ((ib_c(2,ist).gt.0).and.(ncsf_f(ist).gt.0))
c
              if((ib_c(3,ist).gt.0).and.(ncsf_f(ist).gt.0)) then
               call mxv(b(ib_c(3,ist)),ncsf_f(ist)*navst(ist),
     &          r(ivd,ir),nvdt,sscr)
               call daxpy_wr(ncsf_f(ist)*navst(ist),one,sscr,1,
     &          cxr(icount,ir),1)
                if (prtmicro.ge.2) then
                 icount2 = icount
                   do ixx =1,navst(ist)
                   write(nlist,
     &        '(/,3x,''Partial Cxr for DTR Nr.'',i3,'' state Nr.'',i3)')
     &              ist,ixx
                   call prtvec(nlist,'Cxr[csf] after C[csf,vd]',
     &              cxr(icount2,ir),ncsf_f(ist))
                   icount2 = icount2 + ncsf_f(ist)
                   enddo ! ixx =1,navst(ist)
                endif ! (prtmicro.ge.2)
              endif ! ((ib_c(3,ist).gt.0).and.(ncfx.gt.0))
c
              if((ib_c(4,ist).gt.0).and.(ncsf_f(ist).gt.0)) then
               call mxv(b(ib_c(4,ist)),ncsf_f(ist)*navst(ist),
     &          r(iva,ir),nvat,sscr)
               call daxpy_wr(ncsf_f(ist)*navst(ist),one,sscr,1,
     &          cxr(icount,ir),1)
                if (prtmicro.ge.2) then
                 icount2 = icount
                   do ixx =1,navst(ist)
                   write(nlist,
     &        '(/,3x,''Partial Cxr for DTR Nr.'',i3,'' state Nr.'',i3)')
     &              ist,ixx
                   call prtvec(nlist,'Cxr[csf] after C[csf,va]',
     &              cxr(icount2,ir),ncsf_f(ist))
                   icount2 = icount2 + ncsf_f(ist)
                   enddo ! ixx =1,navst(ist)
                endif ! (prtmicro.ge.2)
              endif ! ((ib_c(4,ist).gt.0).and.(ncfx.gt.0))
c
             enddo ! ir=1,nr
          icount = icount + ncsf_f(ist)*navst(ist)
          enddo ! ist=1,nst
c
       elseif (cdir.eq.1) then
c
c    # lcxs evaluation
          lcxs = .false.
          if (ns.eq.1) lcxs = .true.
          if (ns.gt.1) call
     &     bummer ('cxvecprd: too many new subspace vectors s',
     &     ns,faterr)
c
c    # lcxr evaluation
          lcxr = .false.
          if (nr.eq.1) lcxr = .true.
          if (nr.gt.1) call
     &     bummer ('cxvecprd: too many new subspace vectors s',
     &     ns,faterr)
c
          if ((.not.lcxs).and.(.not.lcxr)) go to 1000
          ir = 1
          is = 1
c
             icount = 1
             do ist=1,nst
c    # calculate the information for C*v calculation (FT read)
             qc(1) = (ncsf_f(ist)*naar.ne.0)
             qc(2) = (ncsf_f(ist)*nadt.ne.0)
             qc(3) = (ncsf_f(ist)*nvdt.ne.0)
             qc(4) = (ncsf_f(ist)*nvat.ne.0)
c
             call mcxv(
     &      qc,          xbar,            xp,            z,
     &      rev,         ind,             modrt,         add,
     &      iorder,      hmcvec(icount), s(icount,is),  r(iad,ir),
     &      r(iaa,ir),   r(ivd,ir),       r(iva,ir),     cxr(icount,ir),
     &      cxs(iad,is), cxs(iaa,is),     cxs(ivd,is),   cxs(iva,is),
     &      uad,         uaa,             uvd,           uva,
     &      nadt,        nvdt,            ist,           nipbuk,
     &      lastb,       lenbuk,          core,          avcsl3,
     &      lcxr,        lcxs)
c
              if (lcxr) then
c
               icount2 = icount
                 do ixx=1,navst(ist)
                 call dscal_wr(ncsf_f(ist),wavst(ist,ixx),
     &            cxr(icount2,ir),1)
                  if (prtmicro.ge.2) then
                   write(nlist,
     &         '(/,3x,''Total Cxr for DTR Nr.'',i3,'' state Nr.'',i3)')
     &              ist,ixx
                   call prtvec(nlist,'Cxr[csf]',cxr(icount2,ir),
     &              ncsf_f(ist))
                  endif ! (prtmicro.ge.2)
                 icount2 = icount2 + ncsf_f(ist)
                 enddo ! ixx=1,navst(ist)
c
             endif ! (lcxr)
c
             if ((lcxs).and.(prtmicro.ge.2)) then
              write(nlist,'(/,5x,''Cxs[csf] after DRT Nr.'',i3)')
     &         ist
              if (ib_c(1,ist).gt.0)
     &         call prtvec(nlist,'Cxs[ad]',cxs(iad,is),nadt)
              if (ib_c(2,ist).gt.0)
     &         call prtvec(nlist,'Cxs[aa]',cxs(iaa,is),naar)
              if (ib_c(3,ist).gt.0)
     &         call prtvec(nlist,'Cxs[vd]',cxs(ivd,is),nvdt)
              if (ib_c(4,ist).gt.0)
     &         call prtvec(nlist,'Cxs[va]',cxs(iva,is),nvat)
             endif ! ((lcxs).and (prtmicro.ge.2))
c
             icount = icount + ncsf_f(ist)*navst(ist)
             enddo ! ist=1,nst
c
       endif ! (cdir.eq.0)
c
c
1000  return
      end
c***************************************************************
c
      subroutine mxsprd(
     &       b,      ib,       ndims,  s,
     &       ncfx,   nadt,     naar,   nvdt,
     &       nvat,   iad,      iaa,    ivd,
     &       iva,    mxs,      ndimr,  cxs,
     &       xp,     z,        xbar,   rev,
     &       ind,    modrt,    uaa,    ftbuf,
     &       h2,     lenbft,   nwalk,  nsym,
     &       add,    hdiag,    emc,    ib_c,
     &       ib_m)
c
c  ## compute a matrix-vector product involving a new s(*) expansion
c  ## vector.
c  ## incore matrix-vector product calculation
c
       implicit none
c     common & parameter section
c
      real*8 one
      parameter (one=1.0d+00)
c
      integer nfilmx
      parameter(nfilmx=31)
c
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist)*navst(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer prtmicro
      common/output/prtmicro
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
      integer addpt,numint,bukpt,szh,intoff_mc
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c
c  ##  integer section
c
      integer add(*)
      integer iad, iaa, ivd, iva, ib(15), ib_m(maxstat),
     &        ib_c(4,maxstat), icount, ist, ixx, icount2, i
      integer lenbft
      integer ndims,ncfx,nadt,naar,nvdt,nvat,ndimr,nvec,
     &        nwalk,nsym
      integer modrt(0:*)

c
c  ##  real*8 section
c
      real*8 b(*)
      real*8 cxs(ndimr)
      real*8 dumf
      real*8 emc
      real*8 ftbuf(*)
      real*8 ind(*)
      real*8 h2(*)
      real*8 mxs(ndims),hdiag(*)
      real*8 rev(*)
      real*8 s(ndims)
      real*8 uaa(*)
      real*8 xp(*),xbar(*)
      real*8 z(*)
c
c  ##  logical section
c
      logical qinc,qind
c
      integer nndx
      nndx(i) = (i * (i + 1)) / 2
c
c-------------------------------------------------------------
c
      call wzero(ncfx,mxs,1)
c
      if (mdir.eq.0) then
c
       icount = 1
          do ist=1,nst
          icount2 = 0
             if (ib_m(ist).gt.0) then
              do ixx=1,navst(ist)
              call smxvu(b(ib_m(ist)+icount2),s(icount),mxs(icount),
     &         ncsf_f(ist),mxs(icount),0)
              icount = icount + ncsf_f(ist)
              icount2 = icount2 + nndx(ncsf_f(ist))
              enddo ! ixx=1,navst(ist)
             endif ! (ib_m(ist).gt.0)
          enddo ! ist=1,nst
c
      elseif(mdir.eq.1) then
c
       nvec=1
       icount = 1
       icount2 = 1
       qinc = .false.
          do ist=1,nst
c     # ind(*) is referenced only if necessary.
          qind = nwalk_f(ist) .ne. ncsf_f(ist)
           if (ncsf_f(ist).gt.0)then
              do ixx=1,navst(ist)
              call hmcxv(
     &         qinc,           nvec,       ncsf_f(ist),     dumf,
     &         hdiag(icount2), s(icount),  mxs(icount),     ftbuf,
     &         lenbft,         modrt,      uaa,             h2,
     &         add(addpt(10)),   add(addpt(4)),     xp(1+cpt2(ist)),
     &         z(1+cpt2(ist)),   xbar(1+cpt2(ist)), rev(1+cpt3(ist)),
     &         ind(1+cpt3(ist)), qind,ist)
              call daxpy_wr(ncsf_f(ist),-1.0d+00*heig(ist,ixx),
     &         s(icount),1, mxs(icount),1)
              call dscal_wr(ncsf_f(ist),2.0d+00*wavst(ist,ixx),
     &         mxs(icount),1)
              icount = icount + ncsf_f(ist)
              enddo ! ixx=1,navst(ist)
           endif ! (ib_m(15).gt.0)
          icount2 = icount2 + ncsf_f(ist)
          enddo ! ! ist=1,nst
c
      else
c
          call bummer('bxsprd: wrong mdir , mdir=',mdir,faterr)
c
      endif
c
      if (prtmicro.ge.1)
     & call prtvec(nlist,'Mxs',mxs,ncfx)
c

      return
      end
c******************************************************************
c deck bxrprd
      subroutine bxrprd(
     &  b,      ib,     ndimr,    r,
     &  ncfx,   nadt,   naar,     nvdt,
     &  nvat,   iad,    iaa,      ivd,
     &  iva,    rscr,   sscr,     bxr,
     &  ndims,  cxr,    ib_c,     ib_m)
c
c  compute a matrix-vector product B*r involving a new r(*) expansion
c  vector.
c
       implicit none
c   ## common & parameter section
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nfilmx,iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
      integer prtmicro
      common/output/prtmicro
c
      real*8    one
      parameter(one=1d0)
c
c   ##   integer section
c
      integer ib(15),iaa,iad,ivd,iva,icount,ib_m(maxstat),
     &        ib_c(4,maxstat), ist
      integer ndims,nvat,ncfx,ndimr,nadt,nvdt,naar
c
c   ## real*8 section
c
      real*8 b(*),bxr(ndimr)
      real*8 cxr(ndims)
      real*8 r(ndimr),rscr(ndimr)
      real*8 sscr(ndims)
c
      call wzero(ndimr,bxr,1)
      if(ncfx.gt.0)call wzero(ncfx,cxr,1)
c
      if(ib(1).gt.0) then
       call madad(b(ib(1)),r(iad),bxr(iad),rscr)
        if (prtmicro.ge.1)
     &   call prtvec(nlist,'Bxr[ad](1)',bxr(iad),nadt)
      endif ! (ib(1).gt.0)
c
      if(ib(2).gt.0)then
       call mxv(b(ib(2)),nadt,r(iaa),naar,rscr)
       call daxpy_wr(nadt,one,rscr,1,bxr(iad),1)
       call mtxv(b(ib(2)),naar,r(iad),nadt,rscr)
       call daxpy_wr(naar,one,rscr,1,bxr(iaa),1)
        if (prtmicro.ge.1) then
         call prtvec(nlist,'Bxr[ad](2)',bxr(iad),nadt)
         call prtvec(nlist,'Bxr[aa](2)',bxr(iaa),naar)
        endif ! (prtmicro.ge.1)
      endif ! (ib(2).gt.0)
c
      if(ib(3).gt.0)then
       call smxvu(b(ib(3)),r(iaa),bxr(iaa),naar,rscr,0)
        if (prtmicro.ge.1)
     &   call prtvec(nlist,'Bxr[aa](3)',bxr(iaa),naar)
      endif ! (ib(3).gt.0)
c
      if(ib(4).gt.0)then
       call mxv(b(ib(4)),nadt,r(ivd),nvdt,rscr)
       call daxpy_wr(nadt,one,rscr,1,bxr(iad),1)
       call mtxv(b(ib(4)),nvdt,r(iad),nadt,rscr)
       call daxpy_wr(nvdt,one,rscr,1,bxr(ivd),1)
        if (prtmicro.ge.1) then
         call prtvec(nlist,'Bxr[ad](4)',bxr(iad),nadt)
         call prtvec(nlist,'Bxr[vd](4)',bxr(ivd),nvdt)
        endif ! (prtmicro.ge.1)
      endif ! (ib(4).gt.0)
c
      if(ib(5).gt.0)then
       call mxv(b(ib(5)),nvdt,r(iaa),naar,rscr)
       call daxpy_wr(nvdt,one,rscr,1,bxr(ivd),1)
       call mtxv(b(ib(5)),naar,r(ivd),nvdt,rscr)
       call daxpy_wr(naar,one,rscr,1,bxr(iaa),1)
        if (prtmicro.ge.1) then
         call prtvec(nlist,'Bxr[vd](5)',bxr(ivd),nvdt)
         call prtvec(nlist,'Bxr[aa](5)',bxr(iaa),naar)
        endif ! (prtmicro.ge.1)
      endif ! (ib(5).gt.0)
c
      if(ib(6).gt.0) then
       call mvdvd(b(ib(6)),r(ivd),bxr(ivd),rscr)
        if (prtmicro.ge.1)
     &   call prtvec(nlist,'Bxr[vd](6)',bxr(ivd),nvdt)
      endif ! (ib(6).gt.0)
c
      if(ib(7).gt.0)then
       call mvaad(b(ib(7)),r(iad),bxr(iva),rscr)
       call madva(b(ib(7)),r(iva),bxr(iad),rscr)
        if (prtmicro.ge.1) then
         call prtvec(nlist,'Bxr[va](7)',bxr(iva),nvat)
         call prtvec(nlist,'Bxr[ad](7)',bxr(iad),nadt)
        endif ! (prtmicro.ge.1)
      endif ! (ib(7).gt.0)
c
      if(ib(8).gt.0)then
       call mxv(b(ib(8)),nvat,r(iaa),naar,rscr)
       call daxpy_wr(nvat,one,rscr,1,bxr(iva),1)
       call mtxv(b(ib(8)),naar,r(iva),nvat,rscr)
       call daxpy_wr(naar,one,rscr,1,bxr(iaa),1)
        if (prtmicro.ge.1) then
         call prtvec(nlist,'Bxr[va](8)',bxr(iva),nvat)
         call prtvec(nlist,'Bxr[aa](8)',bxr(iaa),naar)
        endif ! (prtmicro.ge.1)
      endif ! (ib(8).gt.0)
c
      if(ib(9).gt.0)then
       call mxv(b(ib(9)),nvat,r(ivd),nvdt,rscr)
       call daxpy_wr(nvat,one,rscr,1,bxr(iva),1)
       call mtxv(b(ib(9)),nvdt,r(iva),nvat,rscr)
       call daxpy_wr(nvdt,one,rscr,1,bxr(ivd),1)
        if (prtmicro.ge.1) then
         call prtvec(nlist,'Bxr[va](9)',bxr(iva),nvat)
         call prtvec(nlist,'Bxr[vd](9)',bxr(ivd),nvdt)
        endif ! (prtmicro.ge.1)
      endif
c
      if(ib(10).gt.0) then
       call mvava(b(ib(10)),r(iva),bxr(iva),rscr)
        if (prtmicro.ge.1)
     &   call prtvec(nlist,'Bxr[va](10)',bxr(iva),nvat)
      endif ! (ib(10).gt.0)
c
      continue
      return
      end
c
c********************************************************
c
      subroutine hmcxv(
     & qinc,   nvec,   ncsf,   hmc,
     & hdiag,  v,      hv,     ftbuf,
     & lenbft, modrt,  uaa,    h2,
     & off1,   addaa,  xp,
     & z,      xbar,   rev,
     & ind,    qind,   ist)
c
c  compute the matrix-vector products of the hmc(*) matrix
c  and the nvec trial vectors in v(*).  if qinc=.true. then
c  these products are computed directly from hmc(*).  if
c  qinc=.false. they are computed from the integrals and
c  off-diagonal formula tape.
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           11ab       ppqq, pqqp
c   3           11a        ppqq
c   4           11b        pqqp
c
c   5           14ab       half*pppp, pp
c   6           14a        half*pppp
c   7           14b        pp
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      sssp, sppp, sp
c   3           2a'b'      srpq, sqpr
c   4           1ab        srqp, spqr
c   5           3ab        sqrp, sprq
c   6           4ab        ssqp, spqs
c               8ab        srpp, sppr
c   7           6a'b'      sqqp, spqq
c   8           12ac       sssp, sp
c   9           12bc       sppp, sp
c  10           1a         srqp
c               2a'        srpq
c               6a'        sqqp
c               7'         sqpq
c  11           4a         ssqp
c               8a         srpp
c  12           2b'        sqpr
c               3a         sqrp
c               4b         spqs
c               4b'        sqps
c               5          spsq
c               8b         sppr
c               10         sprp
c  13           11b        spps
c               13         half*spsp
c  14           1b         spqr
c               3b         sprq
c  15           6b'        spqq
c               12c        sp
c==================================================
c
c  written by ron shepard.
c  verstion date: 27-mar-85
c
       implicit none
c  ##  parameter & common section
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c     #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      real*8 zero
      parameter(zero=0d0)
c
      integer nfilmx,iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist, nft
      equivalence (iunits(1),nlist)
      equivalence (iunits(11),nft)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8), nact
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxtot(10),nact)
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx, ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max

      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)

c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c  ##  integer section
c
      integer addaa(*)
      integer b, bmax
      integer db
      integer event
      integer head, hsym
      integer ism(8), ind(*), i, isv, icode, ik, ib, ivec, ist,
     &        igd(0:7), igod(0:15),ism2(8)
      integer j
      integer k, kntft
      integer lenbft,modrt(0:*),off1(*)
      integer ncsf, nsvnv, nsv, nv, nw, nvec
      integer q,qs, ql, qr
      integer ps, psqr, prqs, pqrs, pp, psym, p, pq, pr, pqrsym, pl,
     &        pqsym, p0
      integer rev(*), rl, rs, r
      integer symw, sp1, sp, ss, sppp, sssp, s, sl
      integer tail, tsym
      integer xp(nsym,*),xbar(nsym,*), xt, xbh
      integer yb(8), yk(8), ytk, ytb
      integer z(nsym,*),zt,spindens
c
c  ##  real*8 section
c
      real*8 hmc(*), hdiag(ncsf), hv(ncsf,nvec), htrip(3), h2(*)
      real*8 ftbuf(lenbft)
      real*8 term
      real*8 uaa(*)
      real*8 val(3), v(ncsf,nvec)
c
c  ##  logical section
c
      logical qinc,qind
c
c  ##  character section
c
      character*60 fname
c
c  ##  data section
c
      data igd/1,2,5,3,4,5,3,4/
      data igod/1,2,9,6,7,8,6,7,7,8,3,3,4,4,5,5/
c
c-----------------------------------------------------------------
c
c  ##  open the off-diagonal MCSCF formula tape
       if (nst .eq. 1) then
        fname = 'mcoftfl'
       elseif (ist.ge.10) then
        write(fname,'(a8,i2)')'mcoftfl.',ist
       else
        write(fname,'(a8,i1)')'mcoftfl.',ist
       endif
      call trnfln( 1, fname )
      open(unit=nft,file=fname,status='old',
     & form='unformatted')
c
      if(qinc)then
c
c  compute the products from hmc(*).
c
         do 10 i=1,nvec
            call spmxv(hmc,v(1,i),hv(1,i),ncsf)
10       continue
         return
      endif
c
c  compute the products from the integrals and formula tape.
c  products for all the trial vectors are computed during
c  a single ft read.
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  diagonal elements.
c
      do 820 ivec=1,nvec
         do 810 i=1,ncsf
            hv(i,ivec)=hdiag(i)*v(i,ivec)
810      continue
820   continue
c
c  off-diagonal elements.
c
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 1440 pl=2,nact
      p=orbidx(modrt(pl))
      psym=symm(modrt(pl))
      pp=nndx(p+1)
      p0=nsa(psym)
c
      do 1430 ql=1,pl
      q=orbidx(modrt(ql))
      pqsym=mult(symm(modrt(ql)),psym)
      pq=nndx(max(p,q))+min(p,q)
c
      do 1420 rl=1,ql
      r=orbidx(modrt(rl))
      pqrsym=mult(symm(modrt(rl)),pqsym)
      pr=nndx(max(p,r))+min(p,r)
      qr=nndx(max(q,r))+min(q,r)
c
      do 1410 sl=1,rl
      if(sl.eq.ql)go to 1420
      if(symm(modrt(sl)).ne.pqrsym)go to 1410
      s=orbidx(modrt(sl))
c
c  move integrals into htrip(*) in triple-sort order.
c
      if(rl.eq.pl)then
         ss=nndx(s+1)
         sp=nndx(max(s,p))+min(s,p)
         sssp=off1(max(ss,sp))+addaa(min(ss,sp))
         sppp=off1(max(sp,pp))+addaa(min(sp,pp))
         sp1=nnsa(psym)+nndx(max(s-p0,p-p0))+min(s-p0,p-p0)
         htrip(1)=h2(sssp)
         htrip(2)=h2(sppp)
         htrip(3)=uaa(sp1)
      else
         rs=nndx(max(r,s))+min(r,s)
         qs=nndx(max(q,s))+min(q,s)
         ps=nndx(max(p,s))+min(p,s)
         pqrs=off1(max(pq,rs))+addaa(min(pq,rs))
         prqs=off1(max(pr,qs))+addaa(min(pr,qs))
         psqr=off1(max(ps,qr))+addaa(min(ps,qr))
         htrip(1)=h2(pqrs)
         htrip(2)=h2(prqs)
         htrip(3)=h2(psqr)
      endif
c
990   continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft),ism2,nwalk_f(ist),spindens)
c
      go to (1000,1410,1001,1002,1003,1012,1013,1023,1123),igod(icode)
c
1000  kntft=1
      nw=0
      read(nft)ftbuf
      go to 990
1001  term=val(1)*htrip(1)
      go to 1200
1002  term=val(1)*htrip(2)
      go to 1200
1003  term=val(1)*htrip(3)
      go to 1200
1012  term=val(1)*htrip(1)+val(2)*htrip(2)
      go to 1200
1013  term=val(1)*htrip(1)+val(2)*htrip(3)
      go to 1200
1023  term=val(1)*htrip(2)+val(2)*htrip(3)
      go to 1200
1123  term=val(1)*htrip(1)+val(2)*htrip(2)+val(3)*htrip(3)
c
1200  if(term.eq.zero)go to 990
c
c  note that the loop over trial vectors is independent of the
c  drt loops and may be placed anywhere relative to them.  this
c  version assumes a relatively small number of vectors and
c  therefore places the vector loop outside of the drt loops to
c  reduce the overhead associated with the trial vector loop.
c  for large numbers of trial vectors, it would be better to have
c  the vector loop inside the drt loops. -ron shepard
c
c  for each symmetry type, perform upper and lower walks.
c
      do 1370 isv=1,nsv
      ytb=yb(isv)
      ytk=yk(isv)
      tsym=ism(isv)
      xt=xp(tsym,tail)
      zt=z(tsym,tail)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym,head)
c
c  loop over trial vectors.
c  the two set of loops are for the cases where there is
c  or is not csf selection.  csf selection requires the
c  use of the ind(*) array.
c
      if(qind)then
c
c  selection case...
c
         do 1330 ivec=1,nvec
            do 1320 j=1,xt
               b=ytb+rev(zt+j)
               k=ytk+rev(zt+j)
               bmax=b+xbh
1300           continue
               b=b+1
               k=k+1
1310           continue
               if(b.gt.bmax)go to 1320
               ib=ind(b)
               ik=ind(k)
               db=min(ib,ik)
               if(db.gt.0)then
                  hv(ik,ivec)=hv(ik,ivec)+term*v(ib,ivec)
                  hv(ib,ivec)=hv(ib,ivec)+term*v(ik,ivec)
                  go to 1300
               else
                  b=b-db
                  k=k-db
                  go to 1310
               endif
1320        continue
1330     continue
c
      else
c
c  no selection case...
c  loops over upper and lower walks reduce to co-diagonal sequences
c  of csf indices.
c
         do 1360 ivec=1,nvec
            do 1350 j=1,xt
               ib=ytb+rev(zt+j)
               ik=ytk+rev(zt+j)
               do 1340 i=1,xbh
                  hv(ik+i,ivec)=hv(ik+i,ivec)+term*v(ib+i,ivec)
1340           continue
               do 1342 i=1,xbh
                  hv(ib+i,ivec)=hv(ib+i,ivec)+term*v(ik+i,ivec)
1342           continue
1350        continue
1360     continue
      endif
c
1370  continue
c
      go to 990
c
1410  continue
1420  continue
1430  continue
1440  continue
c
      close (unit=nft)
c
      if(flags(3))call timer('hmcxv',4,event,nlist)
      return
      end
c**************************************************************
c
      subroutine mcxv(
     & qc,     xbar,    xp,     z,
     & rev,    ind,     modrt,  add,
     & iorder, hvec,    s,      rad,
     & raa,    rvd,     rva,    cxr,
     & cxsad,  cxsaa,   cxsvd,  cxsva,
     & uad,    uaa,     uvd,    uva,
     & nadt,   nvdt,    ist,    npbuk,
     & lastb,  lenbuk,  scr,    avcsk,
     & lcxr,   lcxs)
c
c     The subroutine calculates direct the matrix vector product:
c
c        Cxs[vd] = sum(csf) C[csf,vd]*s[csf]
c
c    input:
c     hvec  -  CI vector
c     s     -  vector
c
c    output:
c     cxs - matrix vector product

       implicit none
      integer  forbyt
      external forbyt
c
c   ## parameter section
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c   ##  common section
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbft,nact
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(10),nact)
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
c
      integer hbci
      common/chbci/hbci(4,3)
c
      integer lenm2e,n2embf,lena1e,n1eabf,lena2e,n2eabf,lenbft,
     &  lend2e,n2edbf,lend1e,n1edbf
      common/cfinfo/lenm2e,n2embf,lena1e,n1eabf,lena2e,n2eabf,lenbft,
     &  lend2e,n2edbf,lend1e,n1edbf
c
      integer addpt,numint,bukpt,intoff_mc,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
c
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer nfilmx,iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c   ##  integer section
c
      integer avcsk,add(*)
      integer buk,bkpt,blk
      integer cpt(5)
      integer ist,ind(*),i,intpt(5),ibkpt,icount,iorder(*),icptx
      integer j
      integer lastb(*),lenbuk
      integer modrt(0:*)
      integer nadt,nvdt,npbuk
c
c   ##  real*8 section
c
      real*8 cxsad(*),cxsaa(*),cxsvd(*),cxsva(*),cxr(*)
      real*8 ddot_wr,fact
      real*8 hvec(*)
      real*8 rad(*),raa(*),rvd(*),rva(*)
      real*8 scr(*),s(*)
      real*8 uaa(*),uad(*),uvd(*),uva(*)
      real*8 rev(*)
      real*8 xbar(*),xp(*)
      real*8 z(*)
c
c   ##  logical section
c
      logical lcxs,lcxr,qbk1,qind
c
c ####  dummy variables
c
      logical qc(4)
c
c bernhard
      qind = nwalk_f(ist) .ne. ncsf_f(ist)
c
c   ## get the h2t3 and h2t4 integrals
c
      cpt(1) = 1
      do 1200 i=1,5
1200  intpt(i)=cpt(1)
      icptx=cpt(1)
      buk=0
      do 1700 i=1,4
cmd   if (.not.qc(i)) go to 1700
      blk=hbci(i,1)
      go to(1300,1400,1500,1600),blk
1300  continue
c  type 1 integrals.
      intpt(1)=icptx
      icptx=icptx+numint(1)
      qbk1 = .true.
      if (nact.eq.0) qbk1=.false.
      go to 1700
1400  continue
c  type 2 integrals.
      buk=bukpt(2)
      intpt(2)=icptx
      icptx=icptx+numint(2)
      go to 1700
1500  continue
c  type 3 and 4 integrals
      buk=bukpt(3)
      intpt(3)=icptx
      icptx=icptx+numint(3)
      intpt(4)=icptx
      icptx=icptx+numint(4)
      go to 1700
1600  continue
c  type 5 integrals
      buk=bukpt(5)
      intpt(5)=icptx
      icptx=icptx+numint(5)
1700  continue
c
      bkpt = icptx
      ibkpt=bkpt+lenbuk
c
      if(ibkpt+forbyt(npbuk+1)-1.gt.avcsk)then
         write(nlist,*)'intpt = ', intpt
         write(nlist,*)'ibkpt = ',ibkpt
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    ibkpt+forbyt(npbuk+1)-1,avcsk
         call bummer('mcxv: 1 avcsk =',avcsk,faterr)
      endif
c
c   #  zer out the space for integrals
      call wzero(icptx,scr(cpt(1)),1)
c
c   #  read in the integrals
      if(qbk1) call rdbks_md(lastb(bukpt(1)),
     + scr(intpt(1)),scr(bkpt),scr(ibkpt),lenbuk,npbuk)
      if(buk.ne.0)call rdbks_md(lastb(buk),
     +  scr(cpt(1)),scr(bkpt),scr(ibkpt),lenbuk,npbuk)
c
c     1:h2t1+h2t2+h2t3+h2t4+h2t5,2:ftbuf,3:tden
      cpt(2) = cpt(1) + icptx
      cpt(3) = cpt(2) + lenbft
      cpt(4) = cpt(3) + ncsf_f(ist)*navst(ist)*3
c
      if (cpt(4).gt.avcsk)then
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    cpt(4),avcsk
         call bummer('mcxv: 2 avcsk =',avcsk,faterr)
      endif
c
c
c   #
c   #   contributions containing the 1-el and 2-el transition
c   #    density matrix (the FT is read)
c   #
c
      if (flags(25)) then
      write(nlist,'(/," Total memory for RDFT_CXV:",i15)') avcsk-cpt(4)
      write(nlist,'(" int  :",i15)') cpt(2)-cpt(1)
      write(nlist,'(" ftbuf:",i15)') cpt(3)-cpt(2)
      write(nlist,'(" tden :",i15)') cpt(4)-cpt(3)
      endif
c
      call rdft_cxv(
     &  scr(cpt(2)),   lenbft,       hvec,              s,
     &  rad,           raa,          rvd,               rva,
     &  iorder,        ncsf_f(ist),  xbar(cpt2(ist)+1), xp(cpt2(ist)+1),
     &  z(cpt2(ist)+1),  rev(cpt3(ist)+1),  ind(cpt3(ist)+1),
c     &  modrt,         .false.,      scr(cpt(3)) ,      uad,
     &  modrt,         qind,         scr(cpt(3)) ,      uad,
     &  uaa,           uvd,          uva,               scr(intpt(1)),
     &  scr(intpt(2)), scr(intpt(3)),scr(intpt(4)),     scr(intpt(5)),
     &  add(addpt(10)),add(addpt(4)),add,               cxr,
     &  cxsad,         cxsaa,        cxsvd,             cxsva,
     &  qc,            navst(ist),   ist,               lcxr,
     &  lcxs)
c
c   #
c   # Cxv[cxs] = C[cxs,ad].s[csf]
c   #  one electron contribution:
c   # Cxv_1el(ip) =  8 uad(ip)*[sum(n) hvec(n)*s(n)]
c   #
c   # Cxv[cxs] = C[cxs,vd].s[csf]
c   #  one electron contribution:
c   # Cxv_1el(ai) = -8 uvd(ai)*[sum(n) hvec(n)*s(n)]
c   #
c
      if (lcxs) then
         fact = 0.0d+00
         icount = 1
         do i = 1,navst(ist)
            do j=1,ncsf_f(ist)
            fact = fact + wavst(ist,i)*s(icount)*hvec(icount)
            icount = icount + 1
            enddo
          enddo
c
         call daxpy_wr(nvdt,-8.0d+00*fact,uvd,1,cxsvd,1)
         call daxpy_wr(nadt,8.0d+00*fact,uad,1,cxsad,1)
      endif
c
c   #
c   # Cxv[cxs] = C[cxs,ad].r[ad]
c   #  one electron contribution:
c   # Cxv_1el(n) = 8.(sum(ip) uad(ip).r(ip)).hvec(n)
c   #
c
      if (lcxr) then
         fact = ddot_wr(nadt,uad,1,rad,1)
         icount = 1
         do i=1,navst(ist)
            call daxpy_wr(ncsf_f(ist),8.0d+00*fact,
     &       hvec(icount),1,cxr(icount),1)
            icount = icount + ncsf_f(ist)
         enddo
         icount = 1
c
c   #
c   # Cxv[cxs] = C[cxs,vd].r[vd]
c   #  one electron contribution:
c   # Cxv_1el(n) = -8.(sum(ai) uvd(ai).r(ai)).hvec(n)
c   #
c
         fact = ddot_wr(nvdt,uvd,1,rvd,1)
         icount = 1
         do i=1,navst(ist)
            call daxpy_wr(ncsf_f(ist),-8.0d+00*fact,
     &       hvec(icount),1,cxr(icount),1)
            icount = icount + ncsf_f(ist)
         enddo
      endif
c
      return
      end
c
c**************************************************************
c
      subroutine rdft_cxv(
     & ftbuf,   lenbft,    hvect,    svect,
     & rad,     raa,       rvd,      rva,
     & iorder,  ncsf,      xbar,     xp,
     & z,           r,          ind,
     & modrt,   qind,      tden,     uad,
     & uaa,     uvd,       uva,      h2t1,
     & h2t2,    h2t3,      h2t4,     h2t5,
     & off1,    addaa,     add,      cxr,
     & cxsad,   cxsaa,     cxsvd,    cxsva,
     & qc,      navst_act, ist,      lcxr,
     & lcxs)
c
c  read the unitary group formula tape and construct
c  the one- and two- particle symmetric density and
c  transition density matrix elements over the active orbitals.
c  an index driven algorithm is used to facilitate the construction
c  and use of the density matrix elements.  the required blocks of
c  the c matrix are constructed as the transition density matrix
c  elements are available. density matrix elements of the form
c  <mc| e |mc> are stored in d1(*) and d2(*).
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2       11ab        eiill, eilli
c   3       11a         eiill
c   4       11b         eilil
c
c   5       14ab        half*ellll, ell
c   6       14a         half*ellll
c   7       14b         ell
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      iiil, illl, il
c   3           2a'b'      ijlk, iklj
c   4           1ab        ijkl, ilkj
c   5           3ab        ikjl, iljk
c   6           4ab        iikl, ilki
c               8ab        ijll, illj
c   7           6a'b'      ikkl, ilkk
c   8           12ac       iiil, il
c   9           12bc       illl, il
c  10           1a         ijkl
c               2a'        ijlk
c               6a'        ikkl
c               7'         iklk
c  11           4a         iikl
c               8a         ijll
c  12           2b'        iklj
c               3a         ikjl
c               4b         ilki
c               4b'        ikli
c               5          ilik
c               8b         illj
c               10         iljl
c  13           11b        illi
c               13         half*ilil
c  14           1b         ilkj
c               3b         iljk
c  15           6b'        ilkk
c               12c        il
c==================================================
c  symmetry ft version: 18-oct-84, ron shepard.
c
       implicit none
c  ##  parameter & common section
c
      integer nfilmx
      parameter(nfilmx=31)
c
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer nlist,nft,ndft
      equivalence (iunits(1),nlist)
      equivalence (iunits(23),nft)
      equivalence (iunits(24),ndft)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8), nsb(8), napsy(8)
      equivalence (nxy(1,2),nsb(1))
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nact, nnta
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
c
      integer addpt, numint, bukpt, intoff_mc, szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx, ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      integer nflag
      parameter (nflag=50)
c
      logical flags
      common/lflags/flags(nflag)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot

c
c  ##  integer section
c
      integer addaa(*),add(*)
      integer cvd
      integer head
      integer ist,iorder(*),r(*),ind(*),ind2, ism(8), imo,  ii,
     &        isym, icode, igo, ixx, iidx, it,ism2(8)
      integer jmo, jj, jidx, jsym, jklsym
      integer klsym, ksym, kk, kmo, kidx, kjdx, kntft
      integer lkdx, lili, llt, lt, llll, liii, likj, ljki, lit,
     &        llli, lkji, ljdx, lmo, lldx, lsym, ll, lenbft, llii,
     &        lidx
      integer modrt(0:*)
      integer navst_act, nwalk,  ncsfx2, ncsfx3, ncsf,
     &        nw, nsv,  nv
      integer off1(*)
      integer tail
      integer symw
      integer xbar(nsym,*),xp(nsym,*)
      integer yb(8), yk(8)
      integer z(nsym,*),spindens
c
c  ##  real*8 section
c
      real*8 cxsad(*), cxsaa(*), cxsvd(*), cxsva(*), cxr(*)
      real*8 d1llt, d1lit, d2llii, d2lili, d2llll, d2lkji, d2ljki,
     &       d2likj, d2liii, d2llli
      real*8 emc_ave
      real*8 fockad, fockaa, fockvd, fockva, ftbuf(lenbft)
      real*8 hvect(ncsf,navst_act), h2t1(*), h2t2(*), h2t3(*),
     &       h2t4(*), h2t5(*)
      real*8 rad(*), raa(*), rvd(*), rva(*)
      real*8 svect(ncsf,*)
      real*8 tden(ncsf,navst_act,3)
      real*8 uaa(*),uad(*),uvd(*),uva(*)
      real*8 val(3)
c
c  ##  logical section
c
      logical lcxr,lcxs
      logical qind, qc(4), qt(3), qcx
c
c  ##  character section
c
      character*60 fname1,fname2
c
c  ##  external section
c
      real*8 ddot_wr
      external ddot_wr
c
c---------------------------------------------------------------------
c
       if (nst.eq.1) then
         fname1 = 'mcdftfl'
         fname2 = 'mcoftfl'
       elseif (ist.ge.10) then
         write(fname1,'(a8,i2)')'mcdftfl.',ist
         write(fname2,'(a8,i2)')'mcoftfl.',ist
       else
         write(fname1,'(a8,i1)')'mcdftfl.',ist
         write(fname2,'(a8,i1)')'mcoftfl.',ist
       endif
      call trnfln( 1, fname1 )
      call trnfln( 1, fname2 )
      open(unit=ndft,file=fname1,status='old',form='unformatted')
      open(unit=nft,file=fname2,status='old',form='unformatted')
c
      ncsfx2=2*ncsf
      ncsfx3=3*ncsf
      qcx=qc(1).or.qc(2).or.qc(3).or.qc(4)
c
c  diagonal elements.
c
      rewind ndft
      kntft=1
      nw=0
      read(ndft)ftbuf
c
      do 1700 lmo=1,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 1600 imo=1,lmo
      ii=orbidx(modrt(imo))
      isym=symm(modrt(imo))
      iidx=nndx(ii+1)
      lidx=nndx(max(ii,ll))+min(ii,ll)
c     zero all partial and averaged tden's (no averaged one..)
      call wzero(ncsfx2*navst_act,tden,1)
c
1000  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft),ism2,nwalk_f(ist),spindens)
c
      igo=min(icode,2)+1
      go to (1100,1400,1200),igo
c
1100  continue
c
c     new record.
c
      kntft=1
      nw=0
      read(ndft)ftbuf
      go to 1000
c
1200  continue
c
      call sdmatd_md(symw,nsv,ism,yb,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,tden,ncsf,qind,navst_act)
      go to 1000
c
1400  continue
c
c  change level.  process density contributions at the present level.
c
      if(imo.eq.lmo)go to 1500
c  i.ne.l case
c
       if (lcxs.and.qcx) then
c
c       llii=off1(max(lldx,iidx))+addaa(min(lldx,iidx))
        d2llii = 0.0d+00
          do ixx=1,navst_act
          d2llii = d2llii +
     $     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
          enddo ! do ixx=1,navst_act
c
c       lili=off1(lidx)+addaa(lidx)
        d2lili = 0.0d+00
          do ixx=1,navst_act
          d2lili = d2lili +
     $     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
          enddo ! do ixx=1,navst_act
c
          call ctwo(qc,ll,ll,ii,ii,lsym,lsym,isym,isym,d2llii,
     +     1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +     cxsaa,cxsad,cvd,cxsva)
          call ctwo(qc,ll,ii,ll,ii,lsym,isym,lsym,isym,d2lili,
     +     1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +     cxsaa,cxsad,cvd,cxsva)
       endif ! if (lcxs.and.qcx)
c
       if (lcxr.and.qcx) then
        call ctwo_cxv(qc,ll,ll,ii,ii,lsym,lsym,isym,isym,
     +   add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +   rad,raa,rva,fockad,fockaa,fockva)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),1,cxr,1)
c
        call ctwo_cxv(qc,ll,ii,ll,ii,lsym,isym,lsym,isym,
     +   add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +   rad,raa,rva,fockad,fockaa,fockva)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),1,cxr,1)
       endif ! if (lcxr.and.qcx)
c
      go to 1600
c
1500  continue
c  i.eq.l case
c
       if (lcxs.and.qcx) then
c       llll=off1(lldx)+addaa(lldx)
        d2llll = 0.0d+00
          do ixx=1,navst_act
          d2llll =d2llll +
     $     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
          enddo ! do ixx=1,navst_act

c       lt=ll-nsa(lsym)
c       llt=nnsa(lsym)+nndx(lt+1)
        d1llt = 0.0d+00
          do ixx=1,navst_act
          d1llt =d1llt +
     $    wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
          enddo ! do ixx=1,navst_act
        call ctwo(qc,ll,ll,ll,ll,lsym,lsym,lsym,lsym,d2llll,
     +   1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +   cxsaa,cxsad,cxsvd,cxsva)
        call cone(qc,ll,ll,lsym,d1llt,1,add,iorder,
     +   uaa,uad,uva,h2t2,h2t3,h2t4,cxsaa,cxsad,cxsvd,cxsva)
       endif ! if (lcxs.and.qcx)
c
       if (lcxr.and.qcx) then
        call cone_cxv(qc,ll,ll,lsym,add,iorder,h2t2,h2t3,h2t4,
     &   uad,uaa,uva,rad,raa,rvd,rva,fockad,fockaa,fockvd,fockva)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockvd,tden(1,1,2),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),1,cxr,1)
c
        call ctwo_cxv(qc,ll,ll,ll,ll,lsym,lsym,lsym,lsym,
     +   add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +   rad,raa,rva,fockad,fockaa,fockva)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),1,cxr,1)
        call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),1,cxr,1)
       endif ! if (lcxr.and.qcx)
c
c
1600  continue
1700  continue
c
c  off-diagonal loop contributions.
c
1750  rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      do 2740 lmo=2,nact
      ll=orbidx(modrt(lmo))
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 2730 kmo=1,lmo
      kk=orbidx(modrt(kmo))
      ksym=symm(modrt(kmo))
      klsym=mult(lsym,ksym)
      lkdx=nndx(max(ll,kk))+min(ll,kk)
c
      do 2720 jmo=1,kmo
      jj=orbidx(modrt(jmo))
      jsym=symm(modrt(jmo))
      jklsym=mult(jsym,klsym)
      ljdx=nndx(max(ll,jj))+min(ll,jj)
      kjdx=nndx(max(kk,jj))+min(kk,jj)
c
      do 2710 imo=1,jmo
      if(imo.eq.kmo)go to 2710
      isym=symm(modrt(imo))
      if(isym.ne.jklsym)go to 2710
      ii=orbidx(modrt(imo))
      lidx=nndx(max(ll,ii))+min(ll,ii)
      kidx=nndx(max(kk,ii))+min(kk,ii)
      jidx=nndx(max(jj,ii))+min(jj,ii)
      iidx=nndx(ii+1)
      call wzero(ncsfx3*navst_act,tden,1)
      qt(1)=.false.
      qt(2)=.false.
      qt(3)=.false.
c
2100  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft),ism2,nwalk_f(ist),spindens)
c
      igo=min(icode,2)+1
      go to (2200,2500,2300),igo
2200  continue
c
c  new record
c
      kntft=1
      nw=0
      read(nft)ftbuf
      go to 2100
c
2300  continue
c
      call sdmat_md(symw,nsv,ism,yb,yk,
     +  xbar(1,head),xp(1,tail),z(1,tail),r,ind,
     +  val,icode,hvect,tden,ncsf,qt,qind,navst_act)
      go to 2100
c
2500  continue
c
c  change level.  process density contributions.
c
      if(jmo.eq.lmo)go to 2600
c
       if (qcx.and.lcxs) then

        if(qt(1))then
c        lkji=off1(max(lkdx,jidx))+addaa(min(lkdx,jidx))
         d2lkji = 0.0d+00
           do ixx=1,navst_act
           d2lkji =d2lkji +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
        endif ! if(qt(1))then

c  allow for 11b contributions from diagonal loops.
        if(qt(2))then
         ljki=off1(max(ljdx,kidx))+addaa(min(ljdx,kidx))
         d2ljki = 0.0d+00
           do ixx=1,navst_act
           d2ljki =d2ljki +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
        endif ! if(qt(2))then
c
        if(qt(3))then
         likj=off1(max(lidx,kjdx))+addaa(min(lidx,kjdx))
         d2likj = 0.0d+00
           do ixx=1,navst_act
           d2likj =d2likj +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,3),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
        endif ! if(qt(3))then
c
         if(qt(1))
     +    call ctwo(qc,ll,kk,jj,ii,lsym,ksym,jsym,isym,d2lkji,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cvd,cxsva)
         if(qt(2))
     +    call ctwo(qc,ll,jj,kk,ii,lsym,jsym,ksym,isym,d2ljki,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cvd,cxsva)
         if(qt(3))
     +    call ctwo(qc,ll,ii,kk,jj,lsym,isym,ksym,jsym,d2likj,
     +    1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +    cxsaa,cxsad,cvd,cxsva)
       endif ! if (qcx.and.lcxs) then
c
       if (qcx.and.lcxr) then
c
        if(qt(1)) then
         call ctwo_cxv(qc,ll,kk,jj,ii,lsym,ksym,jsym,isym,
     &    add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),
     &    1,cxr,1)
        endif ! if(qt(1)) then

        if(qt(2)) then
         call ctwo_cxv(qc,ll,jj,kk,ii,lsym,jsym,ksym,isym,
     &    add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),
     &    1,cxr,1)
        endif ! if(qt(2)) then

        if(qt(3)) then
         call ctwo_cxv(qc,ll,ii,kk,jj,lsym,isym,ksym,jsym,
     &    add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,3),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,3),
     &    1,cxr,1)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,3),
     &    1,cxr,1)
        endif ! if(qt(3)) then
c
       endif ! if (qcx.and.lcxr) then
      go to 2710
c
2600  continue
c
c ll,ii,ii,ii; ll,ll,ll,ii; ll,ii  special case.
c
       if (qcx.and.lcxs) then
c
        if(qt(1))then
c        liii=off1(max(lidx,iidx))+addaa(min(lidx,iidx))
         d2liii = 0.0d+00
           do ixx=1,navst_act
           d2liii =d2liii  +
     +     wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,1),1,svect(1,ixx),1)
           enddo ! do ixx=1,navst_act
        endif ! if(qt(1))then
c
        if(qt(2))then
c        llli=off1(max(lldx,lidx))+addaa(min(lldx,lidx))
         d2llli = 0.0d+00
           do ixx=1,navst_act
           d2llli =d2llli +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,2),1,svect(1,ixx),1)
           enddo !  do ixx=1,navst_act
        endif ! if(qt(2))then
c
        if(qt(3))then
c        lt=ll-nsa(lsym)
c        it=ii-nsa(lsym)
c        lit=nnsa(lsym)+nndx(max(lt,it))+min(lt,it)
         d1lit = 0.0d+00
           do ixx=1,navst_act
           d1lit =d1lit +
     $      wavst(ist,ixx)*ddot_wr(ncsf,tden(1,ixx,3),1,svect(1,ixx),1)
           enddo !  do ixx=1,navst_act
        endif ! if(qt(3))then
c
        if(qt(1))
     +   call ctwo(qc,ll,ii,ii,ii,lsym,isym,isym,isym,d2liii,
     +   1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +   cxsaa,cxsad,cvd,cxsva)
        if(qt(2))
     +   call ctwo(qc,ll,ll,ll,ii,lsym,lsym,lsym,isym,d2llli,
     +   1,add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +   cxsaa,cxsad,cvd,cxsva)
        if(qt(3))
     +   call cone(qc,ll,ii,lsym,d1lit,1,add,iorder,
     +   uaa,uad,uva,h2t2,h2t3,h2t4,cxsaa,cxsad,cxsvd,cxsva)

       endif ! if (qcx.and.lcxs) then
c
       if (qcx.and.lcxr) then
        if(qt(1)) then
         call ctwo_cxv(qc,ll,ii,ii,ii,lsym,isym,isym,isym,
     &    add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,1),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,1),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,1),1,c
     + xr,1)

        endif ! if(qt(1)) then

        if(qt(2)) then
         call ctwo_cxv(qc,ll,ll,ll,ii,lsym,lsym,lsym,isym,
     &    add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     &    rad,raa,rva,fockad,fockaa,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,2),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,2),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,2),1,c
     + xr,1)

        endif ! if(qt(2)) then
c
        if(qt(3)) then
         call cone_cxv(qc,ll,ii,lsym,add,iorder,h2t2,h2t3,h2t4,
     &    uad,uaa,uva,rad,raa,rvd,rva,fockad,fockaa,fockvd,fockva)
         call daxpy_wr(ncsf_f(ist)*navst(ist),fockad,tden(1,1,3),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockaa,tden(1,1,3),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockvd,tden(1,1,3),1,c
     + xr,1)

         call daxpy_wr(ncsf_f(ist)*navst(ist),fockva,tden(1,1,3),1,c
     + xr,1)

        endif ! if(qt(3)) then
c
      endif ! if (qcx.and.lcxr) then
c
2710  continue
2720  continue
2730  continue
2740  continue
c
      close (unit=nft)
      close (unit=ndft)
c
      return
      end
c
c*****************************************************
c
      subroutine cone_cxv(qc,ii,jj,isym,add,iorder,
     + h2t2,h2t3,h2t4,uad,uaa,uva,rad,raa,rvd,rva,
     + fockad,fockaa,fockvd,fockva)
c
c  include the 1-particle contributions from tden(*) to the required
c  blocks of the c matrix.
c
c  input:
c  tden(*)   :transition density vector -- (1/2)<mc|eil+eli|n>.
c  u**(*)    :1-e h integral matrix element blocks.
c  h2t*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  c**(*)    :updated c matrix blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
c    # avepar.h
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c    ## drtf.h
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c    ## avstat.h
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst),
     & heig(maxnst,mxavst), navst(maxnst),nst,navst_max
c
c
      logical qc(4)
      real*8 rad(*),raa(*),rvd(*),rva(*),fockvd,fockad,fockaa,fockva
      real*8 h2t2(*),h2t3(*),h2t4(*),uad(*),uaa(*),uva(*)
      integer add(*),iorder(*)
c
      integer npass(2)
      data npass/2,1/
c
c
c  determine the number of permutaiion passes required to
c  process this vector of 1-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1        ij            2     1,2
c    2        ii            1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      npassp=npass(itype)
c
c  initialize ind(*) and inds.
c
      ind(1)=ii
      ind(2)=jj
      inds=isym
c
      fockaa = 0.0d+00
      if(qc(1))call c1aa_cxv(raa,uaa,iorder,fockaa)
c
      fockad = 0.0d+00
      if (qc(2)) call c1ad_cxv(rad,uad,h2t2,
     +  add(addpt(11)),add(addpt(3)),fockad)
c
      fockvd = 0.0d+00
      if (qc(3)) call c1vd_cxv(rvd,h2t3,h2t4,
     +    add(addpt(12)),add(addpt(13)),fockvd)
c
      fockva = 0.0d+00
      if (qc(4)) call c1va_cxv(rva,uva,add(addpt(6)),fockva)
c
      return
      end
c
c*****************************************************************
c
      subroutine c1aa_cxv(raa,uaa,iorder,fockaa)
c
c  include the one-particle density contributions to the
c     Cxv[cxs] = C[cxs,aa].r[aa]
c
c    following term is calculated:
c     sum(p) 4*uaa(pt)*raa(pq)
c     sum(q) 4*uaa(qt)*raa(pq)
c
c  input:
c  raa(*)    :orbital rotation parameters for the aa block
c  uaa(*)    :active-active blocks of the effective 1-e h matrix.
c  raa(*)    :orbital rotation parameters for the aa block
c  iorder(*) :mapping vector for allowed active-active rotations.
c
c  output:
c  fockaa    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 uaa(*),raa(*),fockaa
      integer iorder(*)
      real*8 four
      parameter(four=4d0)
c
      integer rangel(2,2),rangeh(2,2)
      logical skipl(2),skiph(2)
c
c  determine the do-loop ranges.
c
      do 10 i=1,2
      rangel(1,i)=ira1(inds)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(inds)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      noff=nsa(inds)
      nnoff=nnsa(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      t=ind(p2)
      rt=t-noff
c
c  include sum(p) 4*uaa(pt)*raa(pq) term.
c
      if(skiph(p1))go to 120
      q=ind(p1)
c
      rp=q-noff
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      rp=rp+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nnoff+nndx(max(rp,rt))+min(rp,rt)
      fockaa = fockaa + four*uaa(pt)*raa(ipq)
100   continue
120   continue
c
c  include sum(q) 4*uaa(qt)*raa(pq) term.
c
      if(skipl(p1))go to 220
      p=ind(p1)
c
      rq=0
      do 200 q=rangel(1,p1),rangel(2,p1)
      rq=rq+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nnoff+nndx(max(rq,rt))+min(rq,rt)
      fockaa = fockaa  -four*uaa(qt)*raa(ipq)
200   continue
220   continue
c
300   continue
c
      return
      end
c
c*****************************************************************
c
      subroutine c1ad_cxv(rad,uad,h2,off2,addad,fockad)
c
c  include the one-particle density contributions to the
c    Cxv[cxs] = C[cxs,ad].r[ad]
c
c    following term is calculated:
c     sum(ip) 4*(2*(ip|uv)-(iu|pv))*rad(ip)
c     sum(i)  -4*uad(it)*rad(ip)
c
c  input:
c  uad(*)    :double-active blocks of the effective 1-e h matrix.
c  rad(*)    :orbital rotation parameters for the ad block
c  addad(*)  :addressing array for 1-e integrals.
c  off2(*)   :addressing array for 2-e integrals.
c
c  output:
c  fockad    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 rad(*),uad(*),h2(*),fockad
      integer off2(*),addad(ndimd,ndima)
c
      real*8 zero,two,four
      parameter(zero=0d0,two=2d0,four=4d0)
c
C  2-e integral terms are computed as:
c
c   fockad(uv) =  sum  4*( 4*(ip|uv) -(iu|pv) -(iv|pu) )*rad(ip)
c                 (u>v)
c
c                +sum  4*( 2*(ip|uu) - (iu|pu) )*rad(ip)
c                 (u)
c
      u=ind(1)
      v=ind(2)
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      ip=0
      ipuv=off2(nndx(max(u,v))+min(u,v))
      do 130 isym=1,nsym
      if(napsy(isym).eq.0)go to 130
      if(ndpsy(isym).eq.0)go to 130
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 120 p=p1,p2
      iupv=off2(nndx(max(p,v))+min(p,v))+addad(i1,u)
      ivpu=off2(nndx(max(p,u))+min(p,u))+addad(i1,v)
      do 110 i=i1,i2
      ip=ip+1
      ipuv=ipuv+1
      fockad = fockad + four*(four*h2(ipuv)-h2(iupv)-h2(ivpu)) * rad(ip)
      iupv=iupv+1
      ivpu=ivpu+1
110   continue
120   continue
130   continue
c
      else
c
c  u.eq.v terms.
c
      ip=0
      ipuu=off2(nndx(u)+u)
      do 230 isym=1,nsym
      if(napsy(isym).eq.0)go to 230
      if(ndpsy(isym).eq.0)go to 230
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 220 p=p1,p2
      iupu=off2(nndx(max(p,u))+min(p,u))+addad(i1,u)
      do 210 i=i1,i2
      ip=ip+1
      ipuu=ipuu+1
      fockad = fockad + four*(two*h2(ipuu)-h2(iupu)) * rad(ip)
      iupu=iupu+1
210   continue
220   continue
230   continue
c
      endif
c
c  include 1-e terms.
c
      ni=ndpsy(inds)
      if(ni.eq.0)return
      i1=ird1(inds)
c
      do 320 iperm=1,npassp
      p=ind(perm(1,iperm))
      t=ind(perm(2,iperm))
c
c  include -4*uad(it)*rad(ip) terms.
c
      ip=addad(i1,p)
      it=addad(i1,t)
c
      do 310 i=1,ni
      fockad = fockad  - four*uad(it) * rad(ip)
      ip=ip+1
      it=it+1
310   continue
c
320   continue
c
      return
      end
c
c*****************************************************
c
      subroutine c1vd_cxv(rvd,hd,hx,off3,off4,fockvd)
c
c  include the one-particle density contributions to the cvd(*) matrix.
c    Cxv[cxs] = C[cxs,vd].r[vd]
c
c     fockvd(uv) = sum -4*( 2*(ai|uv)-(au|iv))*rvd(ai)
c                  (ai)
c
c  input:
c  rvd(*)    :orbital rotation parameters for the vd block
c  hd(*)     :2-e integrals of the type (ai|uv).
c  hx(*)     :2-e integrals of the type (au|iv).
c  off3(*)   :addressing array for hd(*) integrals.
c  off4(*)   :addressing array for hx(*) integrals.
c
c  output:
c  fockvd    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxtot(18),ndima)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
      equivalence (ind(1),u),(ind(2),v)
c
      real*8 hd(*),hx(*),rvd(*),fockvd
      integer off3(*),off4(ndima,ndima)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 zero,two,four
      parameter(zero=0d0,two=2d0,four=4d0)
c
c  the matrix is computed as:
c
c     fockvd(uv)= sum -4*( 4*(ai|uv) -(au|iv) -(av|iu) )*rvd(ai)
c                 (ai, for u>v)
c
c                +sum -4*( 2(ai|uu) - (au|iu) )*rvd(ai)
c                 (ai, for u=v)
c
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      uvd=off3(nndx(max(u,v))+min(u,v))
      uvx=off4(u,v)
      vux=off4(v,u)
      do 100 ai=1,nvdt
      fockvd =
     & fockvd -four*(four*hd(uvd+ai)-hx(uvx+ai)-hx(vux+ai))*rvd(ai)
100   continue
c
      else
c
c  u=v terms.
c
      uud=off3(nndx(u)+u)
      uux=off4(u,u)
      do 200 ai=1,nvdt
      fockvd = fockvd -four*(two*hd(uud+ai)-hx(uux+ai))*rvd(ai)
200   continue
c
      endif
      return
      end
c
c***************************************************************
c
      subroutine c1va_cxv(rva,uva,addva,fockva)
c
c  include the one-particle density contributions to the cva(*) matrix.
c
c     fockva(tp) = sum uva(at)*rvd(ap)
c                  (a)
c
c  input:
c  rva(*)    :orbital rotation parameters for the va block
c  uva(*)    :virtual-active blocks of the effective 1-e h matrix.
c  addva(*)  :addressing array.
c
c  output:
c  fockva    :
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      integer nsva(8)
      equivalence (nxy(1,31),nsva(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 uva(*),rva(*),fockva
      integer addva(ndimv,ndima)
c
      real*8 four
      parameter(four=4d0)
c
      na=nvpsy(inds)
      if(na.eq.0)return
      a1=irv1(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      p=ind(p1)
      t=ind(p2)
c
c  include sum(a) -4*uva(at)*rva(ap) terms.
c
      ap=addva(a1,p)
      at=addva(a1,t)
c
      do 200 a=1,na
      fockva = fockva -four*uva(at)*rva(ap)
      ap=ap+1
      at=at+1
200   continue
c
300   continue
c
      return
      end
c
c***********************************************************
c
      subroutine ctwo_cxv(qc,ii,jj,kk,ll,isym,jsym,ksym,lsym,
     +  add,iorder,h2t1,h2t2,h2t3,h2t4,h2t5,
     +  rad,raa,rva,fockad,fockaa,fockva)
c
c
c  input:
c  qc(*)     :logical array indicating the required c blocks.
c  rad       :orbital rotation parameters for the ad block
c  raa       :orbital rotation parameters for the aa block
c  rva       :orbital rotation parameters for the va block
c  iorder(*) :allowed active-active rotations.
c  h2i*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  fockad
c  fockaa
c  fockva
c
c
      implicit integer(a-z)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      real*8 h2t1(*),h2t2(*),h2t3(*),h2t4(*),h2t5(*)
      real*8 rad(*),raa(*),rva(*),fockad,fockaa,fockva
      integer add(*),iorder(*)
      logical qc(4)
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      integer icol(7),itypex(8),npass(6),colpt(6)
c
      data itypex/1,2,3,4,5,0,0,6/
      data npass/4,3,3,2,2,1/
      data colpt/1,5,1,5,1,1/
      data icol/1,2,3,4,  1,3,4/
c
c  determine the number of permutation passes required to
c  process this vector of 2-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1       ijkl          4     1,2,3,4
c    2       iikl          3     1,3,4
c    3       ijkk          3     1,2,3
c    4       iikk          2     1,3
c    5       ijij          2     1,2
c    6       iiii          1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      if(kk.eq.ll)itype=itype+2
      ij=nndx(max(ii,jj))+min(ii,jj)
      kl=nndx(max(kk,ll))+min(kk,ll)
      if(ij.eq.kl)itype=itype+4
      itype=itypex(itype)
      npassp=npass(itype)
      cpt=colpt(itype)
c
c  initialize ind(*) and inds(*).
c
      ind(1)=ii
      ind(2)=jj
      ind(3)=kk
      ind(4)=ll
      inds(1)=isym
      inds(2)=jsym
      inds(3)=ksym
      inds(4)=lsym
c
      fockaa = 0.0d+00
      if(qc(1))call c2aa_cxv(icol(cpt),
     +  h2t1,add(addpt(10)),add(addpt(4)),iorder,raa,fockaa)
c
      fockad = 0.0d+00
      if(qc(2))call c2ad_cxv(icol(cpt),
     +  h2t2,add(addpt(11)),add(addpt(3)),rad,fockad)
c
      fockva = 0.0d+00
      if(qc(4))call c2va_cxv(icol(cpt),
     +  h2t5,add(addpt(14)),add(addpt(6)),rva,fockva)
c
      return
      end
c
c***********************************************************
c
      subroutine c2aa_cxv(icol,h2,off1,addaa,iorder,raa,fockaa)
c
c  include the two-particle density contributions to the
c       Cxv[cxs] = C[cxs,aa].r[aa]
c
c    following terms are calculated:
c     sum(p) 4*(pt|uv)*raa(pq)
c     sum(q) -4*(qt|uv)*raa(pq)
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),raa(*),fockaa
      integer icol(*),iorder(*),off1(*),addaa(*)
c
      integer rangel(2,4),rangeh(2,4)
      logical skipl(4),skiph(4)
      real*8 four,eight,dterm
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      rangel(1,i)=ira1(sym)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(sym)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include sum(p) 4*(pt|uv)*raa(pq) term.
c
      if(skiph(p1))go to 120
      q=ind(p1)
      t=ind(p2)
c
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nndx(max(p,t))+min(p,t)
      fockaa = fockaa +
     &  dterm*h2(off1(max(pt,uv))+addaa(min(pt,uv)))*raa(ipq)
100   continue
120   continue
c
c  include sum(q) -4*(qt|uv)*raa(pq) term.
c
      if(skipl(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      do 200 q=rangel(1,p1),rangel(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nndx(max(q,t))+min(q,t)
      fockaa = fockaa  -
     & dterm*h2(off1(max(qt,uv))+addaa(min(qt,uv)))*raa(ipq)
200   continue
220   continue
c
300   continue
c
      return
      end
c
c***********************************************************
c
      subroutine c2ad_cxv(icol,h2,off2,addad,rad,fockad)
c
c  include the two-particle density contributions to the
c     Cxv[cxs] = C[cxs,ad].r[ad]
c
c    following term is calculated:
c     sum(i) -4*(it|uv)*rad(ip)
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),rad(*),fockad
      integer icol(*),off2(*),addad(ndimd,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=ird1(sym)
      range(2,i)=ird2(sym)
      skip(i)=ndpsy(sym).eq.0
10    continue
c
      do 300 ipt=1,npassp
      iperm=icol(ipt)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include sum(i) -4*(it|uv)*rad(ip) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      ituv=off2(uv)+addad(range(1,p1),t)
      ip=addad(range(1,p1),p)
      do 200 i=range(1,p1),range(2,p1)
      fockad = fockad - dterm*h2(ituv)*rad(ip)
      ip=ip+1
      ituv=ituv+1
200   continue
220   continue
c
300   continue
c
      return
      end
c
c***********************************************************
c
      subroutine c2va_cxv(icol,h2,off5,addva,rva,fockva)
c
c  include the two-particle density contributions to the
c     Cxv[cxs] = C[cxs,va].r[va]
c
c    following term is calculated:
c     sum(a) (at|uv)*rva(ap)
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),rva(*),fockva
      integer icol(*),off5(*),addva(ndimv,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=irv1(sym)
      range(2,i)=irv2(sym)
      skip(i)=nvpsy(sym).eq.0
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include sum(a) -4*(at|uv)*rva(ap) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      atuv=off5(uv)+addva(range(1,p1),t)
      ap=addva(range(1,p1),p)
      do 200 a=range(1,p1),range(2,p1)
      fockva = fockva -dterm*h2(atuv)*rva(ap)
      ap=ap+1
      atuv=atuv+1
200   continue
220   continue
c
300   continue
c
      return
      end
c
c****************************************************************
      subroutine cone(qc,ii,jj,isym,tden,ncsf,add,iorder,
     +  uaa,uad,uva,
     +  h2t2,h2t3,h2t4,
     +  caa,cad,cvd,cva)
c
c  include the 1-particle contributions from tden(*) to the required
c  blocks of the c matrix.
c
c  input:
c  qc(*)     :logical array indicating the required c blocks.
c  tden(*)   :transition density vector -- (1/2)<mc|eil+eli|n>.
c  iorder(*) :allowed active-active rotations.
c  u**(*)    :1-e h integral matrix element blocks.
c  h2t*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  c**(*)    :updated c matrix blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 tden(*),uaa(*),uad(*),uva(*)
      real*8 h2t2(*),h2t3(*),h2t4(*)
      real*8 caa(*),cad(*),cvd(*),cva(*)
      integer add(*),iorder(*)
      logical qc(4)
c
      integer npass(2)
      data npass/2,1/
c
c  determine the number of permutaiion passes required to
c  process this vector of 1-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1        ij            2     1,2
c    2        ii            1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      npassp=npass(itype)
c
c  initialize ind(*) and inds.
c
      ind(1)=ii
      ind(2)=jj
      inds=isym
c
      if(qc(1))call c1aa(tden,ncsf,uaa,caa,iorder)
c
      if(qc(2))call c1ad(tden,ncsf,uad,h2t2,cad,
     +  add(addpt(11)),add(addpt(3)))
c
      if(qc(3))call c1vd(tden,ncsf,h2t3,h2t4,cvd,
     +  add(addpt(12)),add(addpt(13)))
c
      if(qc(4))call c1va(tden,ncsf,uva,cva,add(addpt(6)))
c
      return
      end

c**************************************************************
      subroutine c1aa(td1,ncsf,uaa,caa,iorder)
c
c  include the one-particle density contributions to the caa(*) matrix.
c
c     caa(n,pq) = sum 4*uaa(pt)*td1(n,qt) - 4*uaa(qt)*td1(n,pt)
c                 (t)
c
c  where td1(n,pt) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|ept+etp|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  uaa(*)    :active-active blocks of the effective 1-e h matrix.
c  iorder(*) :mapping vector for allowed active-active rotations.
c
c  output:
c  caa(*)    :updated caa(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 td1(ncsf),uaa(*),caa(ncsf,*)
      integer iorder(*)
      real*8 four,factor
      parameter(four=4d0)
c
      integer rangel(2,2),rangeh(2,2)
      logical skipl(2),skiph(2)
c
c  determine the do-loop ranges.
c
      do 10 i=1,2
      rangel(1,i)=ira1(inds)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(inds)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      noff=nsa(inds)
      nnoff=nnsa(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      t=ind(p2)
      rt=t-noff
c
c  include 4*uaa(pt)*d1(qt) terms.
c
      if(skiph(p1))go to 120
      q=ind(p1)
c
      rp=q-noff
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      rp=rp+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nnoff+nndx(max(rp,rt))+min(rp,rt)
      factor=four*uaa(pt)
      call daxpy_wr(ncsf,factor,td1,1,caa(1,ipq),1)
100   continue
120   continue
c
c  include -4*uaa(qt)*d1(pt) terms.
c
      if(skipl(p1))go to 220
      p=ind(p1)
c
      rq=0
      do 200 q=rangel(1,p1),rangel(2,p1)
      rq=rq+1
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nnoff+nndx(max(rq,rt))+min(rq,rt)
      factor=-four*uaa(qt)
      call daxpy_wr(ncsf,factor,td1,1,caa(1,ipq),1)
200   continue
220   continue
c
300   continue
c
      return
      end
      subroutine c1ad(td1,ncsf,uad,h2,cad,off2,addad)
c
c  include the one-particle density contributions to the cad(*) matrix.
c
c     cad(n,ip) = sum 4*(2*(ip|uv)-(iu|pv))*td1(n,uv)
c
c                +sum -4*uad(it)*td1(n,pt)
c                 (t)
c
c  where td1(n,pt) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|ept+etp|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  uad(*)    :double-active blocks of the effective 1-e h matrix.
c  addad(*)  :addressing array for 1-e integrals.
c  off2(*)   :addressing array for 2-e integrals.
c
c  output:
c  cad(*)    :updated cad(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 td1(ncsf),uad(*),cad(ncsf,*),h2(*)
      integer off2(*),addad(ndimd,ndima)
c
      real*8 zero,two,four,term
      parameter(zero=0d0,two=2d0,four=4d0)
c
c  2-e integral terms are computed as:
c
c    cad(n,ip) =  sum  4*( 4*(ip|uv) -(iu|pv) -(iv|pu) )*td1(n,uv)
c                 (u>v)
c
c                +sum  4*( 2*(ip|uu) - (iu|pu) )*td1(n,uu)
c                 (u)
c
      u=ind(1)
      v=ind(2)
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      ip=0
      ipuv=off2(nndx(max(u,v))+min(u,v))
      do 130 isym=1,nsym
      if(napsy(isym).eq.0)go to 130
      if(ndpsy(isym).eq.0)go to 130
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 120 p=p1,p2
      iupv=off2(nndx(max(p,v))+min(p,v))+addad(i1,u)
      ivpu=off2(nndx(max(p,u))+min(p,u))+addad(i1,v)
      do 110 i=i1,i2
      ip=ip+1
      ipuv=ipuv+1
      term=four*(four*h2(ipuv)-h2(iupv)-h2(ivpu))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cad(1,ip),1)
      iupv=iupv+1
      ivpu=ivpu+1
110   continue
120   continue
130   continue
c
      else
c
c  u.eq.v terms.
c
      ip=0
      ipuu=off2(nndx(u)+u)
      do 230 isym=1,nsym
      if(napsy(isym).eq.0)go to 230
      if(ndpsy(isym).eq.0)go to 230
      p1=ira1(isym)
      p2=ira2(isym)
      i1=ird1(isym)
      i2=ird2(isym)
      do 220 p=p1,p2
      iupu=off2(nndx(max(p,u))+min(p,u))+addad(i1,u)
      do 210 i=i1,i2
      ip=ip+1
      ipuu=ipuu+1
      term=four*(two*h2(ipuu)-h2(iupu))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cad(1,ip),1)
      iupu=iupu+1
210   continue
220   continue
230   continue
c
      endif
c
c  include 1-e terms.
c
      ni=ndpsy(inds)
      if(ni.eq.0)return
      i1=ird1(inds)
c
      do 320 iperm=1,npassp
      p=ind(perm(1,iperm))
      t=ind(perm(2,iperm))
c
c  include -4*uad(it)*td1(pt) terms.
c
      ip=addad(i1,p)
      it=addad(i1,t)
c
      do 310 i=1,ni
      term=-four*uad(it)
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cad(1,ip),1)
      ip=ip+1
      it=it+1
310   continue
c
320   continue
c
      return
      end
      subroutine c1va(td1,ncsf,uva,cva,addva)
c
c  include the one-particle density contributions to the cva(*) matrix.
c
c     cva(n,ap) = sum -4*uva(at)*td1(n,pt)
c                 (t)
c
c  where td1(n,pt) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|ept+etp|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  uva(*)    :virtual-active blocks of the effective 1-e h matrix.
c  addva(*)  :addressing array.
c
c  output:
c  cva(*)    :updated cva(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      integer nsva(8)
      equivalence (nxy(1,31),nsva(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      common/ccone/perm(2,2),ind(2),inds,npassp
c
      real*8 td1(ncsf),uva(*),cva(ncsf,*)
      integer addva(ndimv,ndima)
c
      real*8 four,factor
      parameter(four=4d0)
c
      na=nvpsy(inds)
      if(na.eq.0)return
      a1=irv1(inds)
c
      do 300 iperm=1,npassp
      p1=perm(1,iperm)
      p2=perm(2,iperm)
c
      p=ind(p1)
      t=ind(p2)
c
c  include -4*uva(at)*td1(pt) terms.
c
      ap=addva(a1,p)
      at=addva(a1,t)
c
      do 200 a=1,na
      factor=-four*uva(at)
      call daxpy_wr(ncsf,factor,td1,1,cva(1,ap),1)
      ap=ap+1
      at=at+1
200   continue
c
300   continue
c
      return
      end
      subroutine c1vd(td1,ncsf,hd,hx,cvd,off3,off4)
c
c  include the one-particle density contributions to the cvd(*) matrix.
c
c     cvd(n,ai) = sum -4*( 2*(ai|uv)-(au|iv))*td1(n,uv)
c                 (uv)
c
c  where td1(n,uv) is a vector of the symmetrized
c  transition 1-particle density matrix elements,
c  (1/2)*<mc|euv+euv|n> for n=1 to ncsf.
c
c  input:
c  td1(*)    :symmetrized transition density matrix elements.
c  hd(*)     :2-e integrals of the type (ai|uv).
c  hx(*)     :2-e integrals of the type (au|iv).
c  off3(*)   :addressing array for hd(*) integrals.
c  off4(*)   :addressing array for hx(*) integrals.
c
c  output:
c  cvd(*)    :updated cvd(*) matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer tsymvd(8)
      equivalence (nxy(1,25),tsymvd(1),nvdt)
      equivalence (nxtot(18),ndima)
c
      common/ccone/perm(2,2),ind(2),inds,npassp
      equivalence (ind(1),u),(ind(2),v)
c
      real*8 td1(ncsf),cvd(ncsf,*),hd(*),hx(*)
      integer off3(*),off4(ndima,ndima)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 zero,two,four,term
      parameter(zero=0d0,two=2d0,four=4d0)
c
c  the matrix is computed as:
c
c     cvd(n,ai) = sum -4*( 4*(ai|uv) -(au|iv) -(av|iu) )*td1(n,uv)
c                 (u>v)
c
c                +sum -4*( 2(ai|uu) - (au|iu) )*td1(n,uu)
c                 (u)
c
      if(u.ne.v)then
c
c  u.ne.v terms.
c
      uvd=off3(nndx(max(u,v))+min(u,v))
      uvx=off4(u,v)
      vux=off4(v,u)
      do 100 ai=1,nvdt
      term=-four*(four*hd(uvd+ai)-hx(uvx+ai)-hx(vux+ai))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cvd(1,ai),1)
100   continue
c
      else
c
c  u=v terms.
c
      uud=off3(nndx(u)+u)
      uux=off4(u,u)
      do 200 ai=1,nvdt
      term=-four*(two*hd(uud+ai)-hx(uux+ai))
      if(term.ne.zero)call daxpy_wr(ncsf,term,td1,1,cvd(1,ai),1)
200   continue
c
      endif
      return
      end

c**************************************************************
      subroutine tltpsp(id,ia,iv,u,udd,uda,uaa,uvd,uva,uvv)
c
c  transpose lower-triangle packed array u(*) to sub-block packed
c  arrays udd(*), uda(*)...,uvv(*).
cmd
c  In case of "direct mcscf" this subroutine also adds the 2-e
c   contribution (calculated in add_u) to uvv and uva.
cmd
c
c  on input:
c  id(*)  is the inverse mapping array for inactive orbitals.
c  ia(*)  is the inverse mapping array for active orbitals.
c  iv(*)  is the inverse mapping array for virtual orbitals.
c  u(*)  contains the input matrix lower triangle packed by symmetry.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nnsm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,7),nnsm(1))
      integer ndpsy(8),n2sd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,12),n2sd(1))
      integer napsy(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nvpsy(8),nnsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,19),nnsv(1))
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
c
c
      integer nmotx
       parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer id(*),ia(*),iv(*)
      real*8 u(*),udd(*),uda(*),uaa(*),uvd(*),uva(*),uvv(*)
c
      i2=0
      p2=0
      a2=0
      do 700 isym=1,nsym
      if(nmpsy(isym).eq.0)go to 700
      uoff=nnsm(isym)
      nd=ndpsy(isym)
      na=napsy(isym)
      nv=nvpsy(isym)
      i1=i2+1
      i2=i2+nd
      p1=p2+1
      p2=p2+na
      a1=a2+1
      a2=a2+nv
c
c  dd block:
c
      if(nd.eq.0)go to 200
      ij=n2sd(isym)
      do 110 i=i1,i2
      mi=id(i)
      do 110 j=i1,i2
      mj=id(j)
      mij=uoff+nndx(max(mi,mj))+min(mi,mj)
      ij=ij+1
      udd(ij)=u(mij)
110   continue
c
200   continue
c
c  da block:
c
      if(nd.eq.0)go to 300
      if(na.eq.0)go to 300
      pi=nsad(isym)
      do 210 p=p1,p2
      mp=ia(p)
      do 210 i=i1,i2
      mi=id(i)
      mpi=uoff+nndx(max(mp,mi))+min(mp,mi)
      pi=pi+1
      uda(pi)=u(mpi)
210   continue
c
300   continue
c
c  aa block:
c
      if(na.eq.0)go to 400
      pq=nnsa(isym)
      do 310 p=p1,p2
      mp=ia(p)
      do 310 q=p1,p
      mq=ia(q)
      mpq=uoff+nndx(mp)+mq
      pq=pq+1
      uaa(pq)=u(mpq)
310   continue
c
400   continue
c
c  vd block:
c
      if(nd.eq.0)go to 500
      if(nv.eq.0)go to 500
      ai=nsvd(isym)
      do 410 i=i1,i2
      mi=id(i)
      do 410 a=a1,a2
      ma=iv(a)
      mai=uoff+nndx(max(ma,mi))+min(ma,mi)
      ai=ai+1
      uvd(ai)=u(mai)
410   continue
c
500   continue
c
c  va block:
      if(na.eq.0)go to 600
      if(nv.eq.0)go to 600
      ap=nsva(isym)
      do 510 p=p1,p2
      mp=ia(p)
      do 510 a=a1,a2
      ma=iv(a)
      map=uoff+nndx(max(ma,mp))+min(ma,mp)
      ap=ap+1
cmd   uva(ap)=u(map)
      uva(ap)=uva(ap)+u(map)
      u(map)=uva(ap)
510   continue
c
600   continue
c
c  vv block:
c
      if(nv.eq.0)go to 700
      ab=nnsv(isym)
      do 610 a=a1,a2
      ma=iv(a)
      do 610 b=a1,a
      mb=iv(b)
      mab=uoff+nndx(ma)+mb
      ab=ab+1
cmd   uvv(ab)=u(mab)
      uvv(ab)=uvv(ab)+u(mab)
      u(mab)=uvv(ab)
610   continue
c
700   continue
c
      return
      end
      subroutine ctwo(qc,ii,jj,kk,ll,isym,jsym,ksym,lsym,
     +  tden,ncsf,add,iorder,
     +  h2t1,h2t2,h2t3,h2t4,h2t5,
     +  caa,cad,cvd,cva)
c
c  include the 2-particle contributions from tden(*) in the required
c  blocks of the c matrix.
c
c  input:
c  qc(*)     :logical array indicating the required c blocks.
c  tden(*)   :symmetrized transition density vector.
c  iorder(*) :allowed active-active rotations.
c  h2i*(*)   :2-e repulsion integral blocks.
c  add(*)    :integral addressing arrays.
c
c  output:
c  c**(*)    :updated c matrix blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff_mc(12),szh(15)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      real*8 tden(*)
      real*8 h2t1(*),h2t2(*),h2t3(*),h2t4(*),h2t5(*)
      real*8 caa(*),cad(*),cvd(*),cva(*)
      integer add(*),iorder(*)
      logical qc(4)
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      integer icol(7),itypex(8),npass(6),colpt(6)
c
      data itypex/1,2,3,4,5,0,0,6/
      data npass/4,3,3,2,2,1/
      data colpt/1,5,1,5,1,1/
      data icol/1,2,3,4,  1,3,4/
c
c  determine the number of permutation passes required to
c  process this vector of 2-particle density matrix elements.
c
c  itype    index-type   npassp  perms.
c    1       ijkl          4     1,2,3,4
c    2       iikl          3     1,3,4
c    3       ijkk          3     1,2,3
c    4       iikk          2     1,3
c    5       ijij          2     1,2
c    6       iiii          1     1
c
      itype=1
      if(ii.eq.jj)itype=2
      if(kk.eq.ll)itype=itype+2
      ij=nndx(max(ii,jj))+min(ii,jj)
      kl=nndx(max(kk,ll))+min(kk,ll)
      if(ij.eq.kl)itype=itype+4
      itype=itypex(itype)
      npassp=npass(itype)
      cpt=colpt(itype)
c
c  initialize ind(*) and inds(*).
c
      ind(1)=ii
      ind(2)=jj
      ind(3)=kk
      ind(4)=ll
      inds(1)=isym
      inds(2)=jsym
      inds(3)=ksym
      inds(4)=lsym
c
      if(qc(1))call c2aa(icol(cpt),
     +  tden,ncsf,h2t1,add(addpt(10)),add(addpt(4)),caa,iorder)
c
      if(qc(2))call c2ad(icol(cpt),
     +  tden,ncsf,h2t2,add(addpt(11)),add(addpt(3)),cad)
c
      if(qc(4))call c2va(icol(cpt),
     +  tden,ncsf,h2t5,add(addpt(14)),add(addpt(6)),cva)
c
      return
      end
      subroutine c2aa(icol,td2,ncsf,h2,off1,addaa,caa,iorder)
c
c  include the two-particle density contributions to the caa matrix.
c
c     caa(n,pq) = 4 sum (pt|uv)*td2(n,qt,uv) - (qt|uv)*td2(n,pt,uv)
c                  (tuv)
c
c  where td2(n,pt,uv) is a symmetrized transition 2-particle density
c  matrix element.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
c
      integer nmotx
       parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),caa(ncsf,*),td2(ncsf)
      integer icol(*),iorder(*),off1(*),addaa(*)
c
      integer rangel(2,4),rangeh(2,4)
      logical skipl(4),skiph(4)
      real*8 four,eight,dterm,factor
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      rangel(1,i)=ira1(sym)
      rangel(2,i)=ind(i)-1
      skipl(i)=rangel(1,i).gt.rangel(2,i)
      rangeh(1,i)=ind(i)+1
      rangeh(2,i)=ira2(sym)
      skiph(i)=rangeh(1,i).gt.rangeh(2,i)
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include 4*(pt|uv)*td2(qt,uv) terms.
c
      if(skiph(p1))go to 120
      q=ind(p1)
      t=ind(p2)
c
      do 100 p=rangeh(1,p1),rangeh(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 100
      pt=nndx(max(p,t))+min(p,t)
      factor=dterm*h2(off1(max(pt,uv))+addaa(min(pt,uv)))
      call daxpy_wr(ncsf,factor,td2,1,caa(1,ipq),1)
100   continue
120   continue
c
c  include -4*(qt|uv)*td2(pt,uv) terms.
c
      if(skipl(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      do 200 q=rangel(1,p1),rangel(2,p1)
      pq=nndx(p)+q
      ipq=iorder(pq)
      if(ipq.le.0)go to 200
      qt=nndx(max(q,t))+min(q,t)
      factor=-dterm*h2(off1(max(qt,uv))+addaa(min(qt,uv)))
      call daxpy_wr(ncsf,factor,td2,1,caa(1,ipq),1)
200   continue
220   continue
c
300   continue
c
      return
      end
      subroutine c2ad(icol,td2,ncsf,h2,off2,addad,cad)
c
c  include the two-particle density contributions to the cad matrix.
c
c     cad(n,ip) = sum -4*(it|uv)*td2(n,pt,uv)
c                (tuv)
c
c  where td2(n,pt,uv) is a symmetrized transition 2-particle density
c  matrix element.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),cad(ncsf,*),td2(ncsf)
      integer icol(*),off2(*),addad(ndimd,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm,factor
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=ird1(sym)
      range(2,i)=ird2(sym)
      skip(i)=ndpsy(sym).eq.0
10    continue
c
      do 300 ipt=1,npassp
      iperm=icol(ipt)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include -4*(it|uv)*td2(pt,uv) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      ituv=off2(uv)+addad(range(1,p1),t)
      ip=addad(range(1,p1),p)
      do 200 i=range(1,p1),range(2,p1)
      factor=-dterm*h2(ituv)
      call daxpy_wr(ncsf,factor,td2,1,cad(1,ip),1)
      ip=ip+1
      ituv=ituv+1
200   continue
220   continue
c
300   continue
c
      return
      end
c
c*********************************************************
c
      subroutine c2va(icol,td2,ncsf,h2,off5,addva,cva)
c
c  include the two-particle density contributions to the cva matrix.
c
c     cva(n,ap) = sum -4*(at|uv)*td2(n,pt,uv)
c                (tuv)
c
c  where td2(n,pt,uv) is a symmetrized transition 2-particle density
c  matrix element.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      common/cctwo/perm(4,4),ind(4),inds(4),npassp
c
      real*8 h2(*),cva(ncsf,*),td2(ncsf)
      integer icol(*),off5(*),addva(ndimv,ndima)
c
      integer range(2,4)
      logical skip(4)
      real*8 four,eight,dterm,factor
      parameter(four=4d0,eight=8d0)
c
c  determine do-loop ranges:
c
      do 10 i=1,4
      sym=inds(i)
      range(1,i)=irv1(sym)
      range(2,i)=irv2(sym)
      skip(i)=nvpsy(sym).eq.0
10    continue
c
      do 300 ip=1,npassp
      iperm=icol(ip)
      p1=perm(1,iperm)
      p2=perm(2,iperm)
      p3=perm(3,iperm)
      p4=perm(4,iperm)
      u=ind(p3)
      v=ind(p4)
c
      uv=nndx(max(u,v))+min(u,v)
c
c  include 'uv' redundancy factor ( 2-delta(u,v) )
c
      dterm=four
      if(u.ne.v)dterm=eight
c
c  include -4*(at|uv)*td2(pt,uv) terms.
c
      if(skip(p1))go to 220
      p=ind(p1)
      t=ind(p2)
c
      atuv=off5(uv)+addva(range(1,p1),t)
      ap=addva(range(1,p1),p)
      do 200 a=range(1,p1),range(2,p1)
      factor=-dterm*h2(atuv)
      call daxpy_wr(ncsf,factor,td2,1,cva(1,ap),1)
      ap=ap+1
      atuv=atuv+1
200   continue
220   continue
c
300   continue
c
      return
      end
c
c***************************************************************
c
      subroutine sdmat_md(
     & symw,   nsv,   ism,    yb,
     & yk,     xbar,  xp,     z,
     & r,      ind,   val,    icode,
     & hvect,  tden,  ncsf,   qt,
     & qind,   navst)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for off-diagonal loops.
c
c  tden(n,ij)  = (1/2)*<mc| eij + eji |n>
c  tden(n,ijkl)= (1/4)*<mc| eijkl + ejikl + eijlk + ejilk |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
cmb   changes due to the density matrix averaging -2 new dummy parameter
cmb   navst, wavst added; hvect, tden redimensioned
c
       implicit none
c  ##  parameter & common block section
c
      real*8 half,fourth
      parameter(half=5d-1, fourth=25d-2)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
c  ##  integer section
c
      integer b, bmax
      integer db
      integer nwalk
      integer ism(*), ind(*), ix,ixx,i,ib,ik, icode, iop, igo,
     &        isv
      integer j
      integer hsym
      integer k
      integer ncsf, navst, nsv
      integer r(*)
      integer symw
      integer tsym
      integer xbar(nsym), xp(nsym), xbh, xt
      integer yb(nsv), yk(nsv), ytk, ytb
      integer z(nsym), zt
c
c  ##  real*8 section
c
      real*8 emc_ave
      real*8 hvect(ncsf,navst)
      real*8 t(3), tden(ncsf,navst,3)
      real*8 val(3)
c
      logical qt(3),qind,qtx(3)
c
c
c
c  account for factors and assign the go to variable.
      iop=icode-1
      go to(2,3,4,5,6,7,8,9,10,11,12,13,14,15),iop
c
2     continue
c  iiil, illl, il
      igo = 7
      t(1)=half*val(1)
      t(2)=half*val(2)
      t(3)=half*val(3)
      qt(1)=.true.
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
3     continue
c  ijlk, iklj
      igo = 4
      t(1)=fourth*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
4     continue
c  ijkl, ilkj
      igo = 5
      t(1)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
5     continue
c  ikjl, iljk
      igo = 6
      t(2)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
6     continue
c  iikl, ilki
c  ijll, illj
      igo = 4
      t(1)=half*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
7     continue
c  ikkl, ilkk
      igo = 5
      t(1)=fourth*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
8     continue
c  iiil, il
      igo = 5
      t(1)=half*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
9     continue
c  illl, il
      igo = 6
      t(2)=half*val(1)
      t(3)=half*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
10    continue
c  ijkl
c  ijlk
c  ikkl
c  iklk
      igo = 1
      t(1)=fourth*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
11    continue
c  iikl
c  ijll
      igo = 1
      t(1)=half*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
12    continue
c  iklj
c  ikjl
c  ilki
c  ikli
c  ilik
c  illj
c  iljl
      igo = 2
      t(2)=fourth*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
13    continue
c  illi
c  half*ilil
      igo = 2
      t(2)=half*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
14    continue
c  ilkj
c  iljk
      igo = 3
      t(3)=fourth*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
15    continue
c  ilkk
c  il
      igo = 3 
      t(3)=half*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
      ytb=yb(isv)
      ytk=yk(isv)
      tsym=ism(isv)
      xt=xp(tsym)
      zt=z(tsym)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym)
c
      if(qind)then
c
c  csf selection case.  ind(*) vector is required.
c
      do 1200 j=1,xt
      b=ytb+r(zt+j)
      k=ytk+r(zt+j)
      bmax=ytb+r(zt+j)+xbh
100   continue
      b=b+1
      k=k+1
110   continue
      if(b.gt.bmax)go to 1200
      ib=ind(b)
      ik=ind(k)
      db=min(ib,ik)
      if(db.gt.0)go to (1001,1002,1003,1012,1013,1023,1123),igo 
      b=b-db
      k=k-db
      go to 110
c
cmb   changes...
1001  continue
      do 2001 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
2001  tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      go to 100
c
1002  continue
      do 2002 ixx=1,navst
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
2002  tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
c
1003  continue
      do 2003 ixx=1,navst
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2003  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1012  continue
      do 2012 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
2012  tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
c
1013  continue
      do 2013 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2013  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1023  continue
      do 2023 ixx=1,navst
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
      tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2023  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1123  continue
      do 2123 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ik,ixx)*t(1)
      tden(ik,ixx,1)=tden(ik,ixx,1)+hvect(ib,ixx)*t(1)
      tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ik,ixx)*t(2)
      tden(ik,ixx,2)=tden(ik,ixx,2)+hvect(ib,ixx)*t(2)
      tden(ib,ixx,3)=tden(ib,ixx,3)+hvect(ik,ixx)*t(3)
2123  tden(ik,ixx,3)=tden(ik,ixx,3)+hvect(ib,ixx)*t(3)
      go to 100
c
1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
      do 1240 ix=1,3
c
      if(qtx(ix))then
      do 1230 j=1,xt
      ib=ytb+r(zt+j)
      ik=ytk+r(zt+j)
      do 1230 ixx=1,navst
      do 1210 i=1,xbh
1210  tden(ik+i,ixx,ix)=tden(ik+i,ixx,ix)+hvect(ib+i,ixx)*t(ix)
      do 1220 i=1,xbh
1220  tden(ib+i,ixx,ix)=tden(ib+i,ixx,ix)+hvect(ik+i,ixx)*t(ix)
1230  continue
      endif
c
1240  continue
c
      endif
c
c
1300  continue
c
      return
      end
c
c********************************************************
c
      subroutine sdmatd_md(
     & symw,    nsv,    ism,     yb,
     & xbar,    xp,     z,       r,
     & ind,     val,    icode,   hvect,
     & tden,    ncsf,   qind,    navst)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for diagonal loops.
c
c  tden(n,ii)  =<mc| eii   |n>
c  tden(n,iill)=<mc| eiill |n>
c  tden(n,ilil)=(1/2)* <mc| eilli |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
cmb   changes due to the density matrix averaging -2 new dummy parameter
cmb   navst, wavst added; hvect, tden redimensioned

       implicit none
c  ##  parameter & common block section
c
      real*8 half,two
      parameter(half=5d-1, two=2d0)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist)*navst_(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
c  ##  integer section
c
      integer b, bmax
      integer ism(*), ind(*), ix, ixx, i, ib, icode, isv, igo,
     &        iop
      integer j
      integer hsym
      integer nwalk, ncsf, navst, nsv
      integer r(*)
      integer symw
      integer tsym
      integer xbar(nsym), xp(nsym), xbh, xt
      integer yb(nsv), ytb
      integer z(nsym), zt
c
c  ##  real*8 section
c
      real*8 emc_ave
      real*8 hvect(ncsf,navst)
      real*8 t(2), tden(ncsf,navst,3)
      real*8 val(3)
c

      logical qind,qtx(2)
c
c-----------------------------------------------------
c
c  account for factors and assign the go-to variable.
      iop=icode-1

      go to(2,3,4,5,6,7),iop
c
2     continue
c  iill, illi
      t(1)=val(1)
      t(2)=half*val(2)
      igo = 3
      qtx(1)=.true.
      qtx(2)=.true.
      go to 50
3     continue
c  iill
      t(1)=val(1)
      igo = 1
      qtx(1)=.true.
      qtx(2)=.false.
      go to 50
4     continue
c  illi
      t(2)=half*val(1)
      igo = 2
      qtx(1)=.false.
      qtx(2)=.true.
      go to 50
c
5     continue
c  half*llll, ll
      t(1)=two*val(1)
      t(2)=val(2)
      igo = 3
      qtx(1)=.true.
      qtx(2)=.true.
      go to 50
6     continue
c  half*llll
      t(1)=two*val(1)
      igo = 1
      qtx(1)=.true.
      qtx(2)=.false.
      go to 50
7     continue
c  ll
      t(2)=val(1)
       igo = 2
      qtx(1)=.false.
      qtx(2)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
      ytb=yb(isv)
      tsym=ism(isv)
      xt=xp(tsym)
      zt=z(tsym)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym)
c
      if(qind)then
c
c  csf selection case.  ind(*) is required.
c
      do 1200 j=1,xt
      b=ytb+r(zt+j)
      bmax=ytb+r(zt+j)+xbh
100   continue
      b=b+1
110   continue
      if(b.gt.bmax)go to 1200
      ib=ind(b)
      if(ib.gt.0)go to (1001,1002,1012), igo 
      b=b-ib
      go to 110
c
1001  continue
      do 2001 ixx=1,navst
2001  tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ib,ixx)*t(1)
      go to 100
1002  continue
      do 2002 ixx=1,navst
2002  tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
1012  continue
      do 2012 ixx=1,navst
      tden(ib,ixx,1)=tden(ib,ixx,1)+hvect(ib,ixx)*t(1)
2012  tden(ib,ixx,2)=tden(ib,ixx,2)+hvect(ib,ixx)*t(2)
      go to 100
c
1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
      do 1240 ix=1,2
c
      if(qtx(ix))then
      do 1230 j=1,xt
      ib=ytb+r(zt+j)
      do 1210 ixx=1,navst
      do 1210 i=1,xbh
1210  tden(ib+i,ixx,ix)=tden(ib+i,ixx,ix)+hvect(ib+i,ixx)*t(ix)
1230  continue
      endif
c
1240  continue
c
      endif
c
1300  continue
c
      return
      end
c
c****************************************************************
c
      subroutine prtvec(unit,title,vec,vdim)
       implicit none
      integer unit,fmt,vdim
      character*(*)title
      real*8 vec(vdim)
c
      write(unit,'(/,2x,3h***,2x,a,2x,3h***,2x,/)')title
      write(unit,'(10f12.8)')vec
c
      return
      end
c
c**********************************************************************+
c
      subroutine pbadad(b)
c
c  print the badad hessian block.
c  matrix is stored as a sequence (1...naa) of rectangular
c  matrices indexed by the inactive-orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer nfilmx, iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxtot(10),nact)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      ijpr=1
      do 200 p=1,nact
      psym=symx(ix0a+p)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 200
      i0=nsd(psym)
      do 100 r=1,p
      rsym=symx(ix0a+r)
      nj=ndpsy(rsym)
      if(nj.eq.0)go to 100
      j0=nsd(rsym)
c
      write(nlist,6010)p,r
      call prblk(b(ijpr),nj,nj,ni,j0,i0,'  j=','  i=',1,nlist)
c
100   ijpr=ijpr+ni*nj
200   continue
      return
6010  format(/t10,'b(ip,jr) (p=',i3,',r=',i3,')')
6020  format(/10x,'badad matrix')
      end
      subroutine pbvaad(b)
c
c  print the bvaad hessian block.
c  matrix is stored as a sequence (1...na*na) of rectangular
c  matrices indexed by the virtual and inactive orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer nfilmx
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),nsd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      airp=1
      do 200 p=1,nact
      psym=symx(ix0a+p)
      ni=ndpsy(psym)
      if(ni.eq.0)go to 200
      i0=nsd(psym)
      do 100 r=1,nact
      rsym=symx(ix0a+r)
      na=nvpsy(rsym)
      if(na.eq.0)go to 100
      a0=nsv(rsym)
c
      write(nlist,6010)p,r
      call prblk(b(airp),na,na,ni,a0,i0,'  a=','  i=',1,nlist)
c
100   airp=airp+na*ni
200   continue
      return
6010  format(/t10,'b(ar,ip) (p=',i3,',r=',i3,')')
6020  format(/10x,'bvaad matrix')
      end
      subroutine pbvava(b)
c
c  print the bvava hessian block.
c  matrix is stored as a sequence (1...naa) of rectangular
c  matrices indexed by the virtual orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer nfilmx, iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(10),nact)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(2),ix0a)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      capr=1
      do 200 p=1,nact
      psym=symx(ix0a+p)
      na=nvpsy(psym)
      if(na.eq.0)go to 200
      a0=nsv(psym)
      do 100 r=1,p
      rsym=symx(ix0a+r)
      nc=nvpsy(rsym)
      if(nc.eq.0)go to 100
      c0=nsv(rsym)
c
      write(nlist,6010)p,r
      call prblk(b(capr),nc,nc,na,c0,a0,'  c=','  a=',1,nlist)
c
100   capr=capr+na*nc
200   continue
      return
6010  format(/t10,'b(ap,cr) (p=',i3,',r=',i3,')')
6020  format(/10x,'bvava matrix')
      end
      subroutine pbvdvd(b)
c
c  print bvdvd hessian block.
c  matrix is stored as a sequence (1...ndd) of rectangular
c  matrices indexed by the virtual orbitals.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer nfilmx, iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(7),ndot)
      integer nvpsy(8),nsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d)
c
      real*8 b(*)
c
      write(nlist,6020)
c
      caij=1
      do 200 i=1,ndot
      isym=symx(ix0d+i)
      na=nvpsy(isym)
      if(na.eq.0)go to 200
      a0=nsv(isym)
      do 100 j=1,i
      jsym=symx(ix0d+j)
      nc=nvpsy(jsym)
      if(nc.eq.0)go to 100
      c0=nsv(jsym)
c
      write(nlist,6010)i,j
      call prblk(b(caij),nc,nc,na,c0,a0,'  c=','  a=',1,nlist)
c
100   caij=caij+na*nc
200   continue
      return
6010  format(/t10,'b(ai,cj) (i=',i3,',j=',i3,')')
6020  format(/10x,'bvdvd matrix')
      end
c
