!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccipc3.f
ccipc part=3 of 3.  shared utility routines.
cversion=1.0 last modified: 28-sep-91
cversion 5.0
c
c  some of these routines may eventually be moved into colib.
c
c deck inpi
      subroutine inpi( j, txt )
c
c  inputs an integer
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      integer j
      character*(*) txt
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt, j
      read(nin,*,iostat=ios) j
      if ( ios .lt. 0 ) then
         call bummer('inpi: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpi: numerr=',numerr,faterr)
         write(nlist,*) 'input an integer'
         go to 10
      else
         return
      endif
*@ifdef format
6000  format(/1x,a,' [',i3,']:',$)
*@else
*6000  format(/1x,a,' [',i3,']:')
*@endif
      end
c deck inpiv
      subroutine inpiv( n, j, txt )
c
c  inputs an integer array
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      integer n
      character*(*) txt
      integer j(n)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt
      read(nin,*,iostat=ios) j
      if ( ios .lt. 0 ) then
         call bummer('inpiv: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpiv: numerr=', numerr, faterr )
         write(nlist,*)'input an integer array'
         go to 10
      else
         return
      endif
c
*@ifdef format
6000  format(/1x,a,':',$)
*@else
*6000  format(/1x,a,':')
*@endif
      end
c deck inpf
      subroutine inpf( f, txt )
c
c  read in a floating point number.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      character*(*) txt
      real*8 f
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt, f
      read(nin,*,iostat=ios) f
      if ( ios .lt. 0 ) then
         call bummer('inpf: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpf: numerr=', numerr, faterr )
         write(nlist,*)'input a floating point number'
         go to 10
      else
         return
      endif
c
*@ifdef format
6000  format(/1x,a,' [',f7.4,']:',$)
*@else
*6000  format(/1x,a,' [',f7.4,']:')
*@endif
      end
c deck inpfv
      subroutine inpfv( n, fj, txt )
c
c  inputs an floating point array.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      integer n
      character*(*) txt
      real*8 fj(n)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt
      read(nin,*,iostat=ios) fj
      if ( ios .lt. 0 ) then
         call bummer('inpfv: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpfv: numerr=', numerr, faterr )
         write(nlist,*)'input an integer array'
         go to 10
      else
         return
      endif
c
*@ifdef format
6000  format(/1x,a,':',$)
*@else
*6000  format(/1x,a,':')
*@endif
      end
c deck inpcv
      subroutine inpcv( cj, n, txt )
c
c  inputs a character array
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      integer n
      character*(*) cj(n)
      character*(*) txt
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt, cj
      read(nin,*,iostat=ios) cj
      if ( ios .lt. 0 ) then
         call bummer('inpcv: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpcv: numerr=',numerr,faterr)
         write(nlist,*) 'input a character array, using list-directed '
     &    // 'input, of length', n
         go to 10
      else
         return
      endif
6000  format(/1x,a
     & /' (use list-directed input) original values:'/(1x,a))
      end
