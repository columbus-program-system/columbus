!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccipc2.f
ccipc part=2 of 3.  print csf info for mrsdci wave functions.
cversion=1.0 last modified: 28-sep-91
cversion 5.0
c deck cipc
      subroutine driver( core, lencor, mem1, ifirst )
c
c this program prints the csfs according to either csf number or
c indexed csf number based on wave function contribution.
c
c ci coefficients are read from the sequential output ci file.
c csf step vectors are generated from the cidrtfl.
c
c  version log:
c  28-sep-91 written by ron shepard.  based in part on program mcpc.
c
       implicit none
c     # nrowmx = the maximum number of rows in the drt.
c     # nimomx = the maximum number of internal orbitals.
c     # nmotmx = the maximum number of orbitals total.
c
      integer i
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=1023 )
c
c.....include 'cdrt.h'
      integer          l,               y,               syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3), y(0:3,nrowmx,3), syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
c.....end of cdrt
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
cjp
      integer*4 jirec4 ,nroots4
      integer reclunit
      parameter(reclunit=1)
      integer myroot,eivecfile,slaterfile,jirec,nelec,symshift
      common/jiri/myroot,eivecfile,slaterfile,jirec,nelec,symshift

      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      ciuvfl
      equivalence (ciuvfl,iunits(3))
      integer      ndrt
      equivalence (ndrt,iunits(4))
      integer      civfl
      equivalence (civfl,iunits(5))
      integer      nweights
      equivalence (nweights,iunits(6))
      integer      nwbin   
      equivalence (nwbin,iunits(7))
      character*60    flname
      common /cfname/ flname(nflmax)
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
c
      integer nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci

c     # dummy.
      integer lencor, mem1, ifirst
      real*8 core(lencor)
c
c     # local.
      integer ntitle, ninfo, nenrgy, buf, nwlkmx, nfvt, lenbuf,
     & nvwxy, ref, limvec, iwsym, itotal, valw, abpair, icsf0,
     & mxpair, space, csfvec, ixcsf, indcsf, maxcsf, nrow,
     & nwalk, smult, j, nvalwz,arcweight,nodeweight,intweight
c
      integer    nengmx,    ntitmx,    ninfmx
      parameter( nengmx=30, ntitmx=30, ninfmx=30 )
c
      integer mu(1), nj(1), njskp(1), a(1), b(nrowmx) 
      integer xbar(4)
      integer xbarcdrt(nrowmx,3)
      integer ietype(nengmx), info(ninfmx)
      real*8 energy(nengmx)
      character*80 title(ntitmx)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    toindx,   to01
      parameter( toindx=0, to01=1 )
c
      integer  forbyt, atebyt
      external forbyt, atebyt
c
c     # set default unit numbers and file names.
c
      nlist  = 6
      nin    = 5
      ciuvfl = 20
      ndrt   = 21
      civfl  = 22
      nweights = 23
      nwbin    =24
c
      flname(1) = 'cipcls'
      flname(2) = 'cipcin'
      flname(3) = 'civout'
      flname(4) = 'cidrtfl'
      flname(5) = 'civfl'
      flname(6) = 'fweights'
      flname(7) = 'fweights.bin'
c
      call trnfln( 5, flname )
c
c     # open the input and listing files, if necessary...
c
c     # use preconnected units 5 (stdin) and 6 (stdout).
c
      call ibummr( nlist )
c
      write(nlist,6000)
6000  format(/' program cipc      '//
     & ' print the csf info for mrsdci wave functions'//
     & ' written by: ron shepard'//
     & ' version date: 06-jun-96')
c
      call who2c( 'cipc', nlist )
      call headwr(nlist,'CIPC','5.5     ')
c
      write (nlist,6040) lencor, mem1, ifirst
6040  format(/' workspace allocation parameters: lencor=',i10,
     & ' mem1=',i10,' ifirst=',i10)
c
c     # get the header info from the drt file.
c
      open(unit=ndrt,file=flname(4),status='old')
c
c     # core(*)=> buf
c
      buf = 1
c
      nwlkmx = lencor

      call rdhdrt(
     & title,  nmot,      nimot,  nfct,
     & nfvt,   nrow,      nsym,   ssym,
     & xbar,   nvalw,     ncsft,  lenbuf,
     & nwalk,  nimomx,    nmotmx, nrowmx,
     & nwlkmx, core(buf), nlist,  ndrt )
c
      if ( nfct .gt. nimomx ) then
         call bummer('from rdhdrt(), (nfct-nimomx)=',
     &    (nfct-nimomx), faterr )
      endif
c
      nzwalk = xbar(1)
      nvalwz = nvalw(1)
      nvwxy  = nvalw(2) + nvalw(3) + nvalw(4)
      nvalwt = nvalwz + nvwxy
c
c     # core(*)=> ref(nzwalk), iwsym(nvalwt), limvec(nwalk), buf(lenbuf)
c
      ref    = 1
      iwsym  = ref    + forbyt( nzwalk )
      limvec = iwsym  + forbyt( nvalwt )
      buf    = limvec + forbyt( nwalk )
c
      itotal = buf + forbyt( lenbuf ) -1
      if ( itotal .gt. lencor ) then
         call bummer('for rddrt() call, (itotal-lencor)=',
     &    (itotal-lencor), faterr )
      endif
c
      call rddrt(
     & nmot,      nimot,      nsym,        nrow,
     & nwalk,     lenbuf,     level,       nmpsy,
     & nepsy,     modrt,      mu,          syml(1),
     & nj,        njskp,      a,           b,
     & l,         y,          xbarcdrt,    core(limvec),
     & nrowmx,    nvalwz,     nvwxy,       core(iwsym),
     & nzwalk,    core(ref),  slabel,      smult,
     & core(buf) )
c
      close( unit=ndrt )
c
c     # core(*)=> ref(nzwalk), iwsym(nvalwt),limvec(nwalk),valw(nvalwt)
c     # compute the valw(*) array from 0/1 limvec(*).
c     #             1 2 3 4 5 6 7
c     # limvec(*) = 1 1 0 0 1 0 1...
c     # valw(*) = 1,2,5,7...
c
      valw = limvec + forbyt( nwalk )
c
      itotal = valw + forbyt( nvalwt ) -1
      if ( itotal .gt. lencor ) then
         call bummer('before indx01(), (itotal-lencor)=',
     &    (itotal-lencor), faterr )
      endif
c
      call indx01(
     & nlist,      nvalwt, nwalk,  core(limvec),
     & core(valw), toindx )
c
c     # copy the valw(*) elements, discard limvec(*).
      call icopy_wr( nvalwt, core(valw), 1, core(limvec), 1 )
      valw = limvec
c
c     # compute the external orbital indexing arrays.
c     # core(*)=> ref(nzwalk),iwsym(nvalwt),valw(nvalwt),icsf0(nvalwt)
c     #           abpair(nabtot)
c
      icsf0  = valw   + forbyt( nvalwt )
      abpair =  icsf0 + forbyt( nvalwt )
c
      mxpair = (lencor - abpair + 1) * (3 - forbyt(2))
      if ( mxpair .le. 0 ) then
         call bummer('before setxv(), mxpair=', mxpair, faterr )
      endif
c
      call setxv(
     & ncsft,        core(iwsym), core(icsf0), mxpair,
     & core(abpair), level,       mapl,        mapd )
c
c     # open the csf coefficient file.
      open( unit=ciuvfl, file=flname(3), form='unformatted',
     & status='unknown' )
c
c     # read the header from the csf coefficient file.
c
      call rdhciv(
     & nlist,  ciuvfl, ntitmx, ninfmx,
     & nengmx, ncsft,  ntitle, title,
     & ninfo,  info,   nenrgy, energy,
     & ietype, lenbuf )
c
c     call openda(civfl,flname(5),lenbuf,'keep','random')
c
c     # to compute node and arc densities 
c     # as a preparatory step accumulate the weight of
c     # all csfs belonging to a given valid internal walk

*@ifdef shavittgraph
*
      intweight=abpair+forbyt(nabtot)
      nodeweight=intweight+atebyt(nvalwt)
      arcweight=nodeweight+atebyt(nrow)
      buf      =arcweight + atebyt(4*nrow)
      space    =lencor - buf+1
*
      open(file=flname(6),unit=iunits(6),form='formatted')
      open(file=flname(7),unit=iunits(7),form='unformatted')
6313  format(' ** weight of internal walk for root=',i4,' **')
      if (nroots.gt.0) then 
        do j=1,nroots
          write(iunits(6),6313)j 
          call comp_intw(core(icsf0),core(intweight),core(nodeweight),
     .                   core(arcweight),core(buf),core(valw),
     .                 space,ncsft,nvalwt,nrow,nimot,nvalw,j,iunits(6))
          write(iunits(7)) j,ncsft,nvalwt,nrow,nimot,nvalw
          write(iunits(7)) core(intweight:intweight+nvalwt-1) 
          write(iunits(7)) core(nodeweight:nodeweight+nrow-1) 
          write(iunits(7)) core(arcweight:arcweight+4*nrow-1) 
        enddo
      else
          write(iunits(6),6313) froot
          call comp_intw(core(icsf0),core(intweight),core(nodeweight),
     .                   core(arcweight),core(buf),core(valw),
     .             space,ncsft,nvalwt,nrow,nimot,nvalw,froot,iunits(6))
          write(iunits(7)) froot,ncsft,nvalwt,nrow,nimot,nvalw
          write(iunits(7)) core(intweight:intweight+nvalwt-1) 
          write(iunits(7)) core(nodeweight:nodeweight+nrow-1) 
          write(iunits(7)) core(arcweight:arcweight+4*nrow-1) 
      endif        
      close(iunits(6))
      close(iunits(7))
*@endif


c     # allocate space to read in the csf coefficients.
c     # core(*)=> ref(nzwalk),iwsym(nvalwt),valw(nvalwt),icsf0(nvalwt),
c     #           abpair(nabtot),
c     #           buf(lenbuf), csfvec(ncsf), indcsf(ncsf), ixcsf(ncsf)
c
      buf = abpair + forbyt( nabtot )
      csfvec = buf + atebyt( lenbuf )
c
      space  = lencor - csfvec + 1
      maxcsf = (space - 1) / (1 + forbyt(2) )
      write(nlist,6300) maxcsf
6300  format(/' space is available for', i10, ' coefficients.' )
      maxcsf = min( maxcsf, ncsft )
c
      indcsf = csfvec + atebyt( maxcsf )
      ixcsf  = indcsf + forbyt( maxcsf )
c
c     # set up the default csf selection parameters.
      icsfmn = 1
      icsfmx = ncsft
      call reseth
c
      write(nlist,6200)
6200  format(/' this program will print the csfs generated from'/
     & ' the drt according to the following print options :')
c

c
c     # batch mode or interactive mode
c

 4    continue

6208  format(/80('=')/35('='),'VECTOR #',i2,35('=')/80('=')/)
      write(nlist,6209)
6209  format(/
     & ' 1) run in batch mode: all valid roots are automatically'/
     & '    analysed and csf info is printed by default contribution'/
     & '    threshold 0.01 '/
     & ' 2) run in interactive mode'/
     & ' 3) generate files for cioverlap without symmetry'/
     & ' 4) generate files for cioverlap with symmetry')
      batch = 1
      call inpi(batch,'input menu number')
      if (batch.lt.1 .or. batch.gt.4 ) goto 4

cjp
      symshift = batch-3
      if(batch.eq.4) batch=3
      if(symshift.ge.1) then
cjp 12 bits orbital number, 3 bits irrep, 1 bit spin fits into 16bit short integer
           symshift = 4096
           do i=1,nsym
             if(nmpsy(i).ge.4096) then
               write(6,*)'cipc needs more bits for slater'
               stop 'cipc needs more bits for slater'
             endif
           enddo
      else
           symshift=0
      endif

      if (batch.eq.1 .or. batch.ge.3) then
cjp
       csfmn=0.0d0
       if(batch.eq.1) csfmn=0.01d0
       if (nroots.gt.0 ) then
        do j=1,nroots
           selroot=j
           write(nlist,6208) selroot
           call rdcivnew(
     &         nlist,  ciuvfl, maxcsf, core(csfvec),
     &         core(indcsf), core(ixcsf),reclen, core(buf) )
cjp
         myroot=j
         if(myroot.eq.1) then
           jirec=0
           if(batch.eq.3) then
              eivecfile=98
              slaterfile=99
              open(file='eivectors',unit=eivecfile,access='direct',
     & form='unformatted',recl=8/reclunit)
           else
              eivecfile=0
              slaterfile=0
           endif
         endif
cjp
         call prtol(
     &       core(csfvec), core(indcsf), core(ixcsf), core(ref),
     &       core(icsf0),  core(valw),   core(iwsym), core(abpair) )
cjp
         if(myroot.eq.1 .and. slaterfile.ne.0) then
              close(slaterfile)
              open(file='eivectorshead',unit=slaterfile,access='direct',
     & form='unformatted',recl=8/reclunit)
                  nroots4=nroots
                  jirec4=jirec
                  write(slaterfile,rec=1) nroots4,jirec4
                  close(slaterfile)
         endif
         if(myroot.eq.nroots.and.eivecfile.ne.0) close(eivecfile)
cjp
        enddo
      else
           selroot=froot
           write(nlist,6208) selroot
           call rdcivnew(
     &         nlist,  ciuvfl, maxcsf, core(csfvec),
     &         core(indcsf), core(ixcsf),reclen, core(buf) )
         call prtol(
     &       core(csfvec), core(indcsf), core(ixcsf), core(ref),
     &       core(icsf0),  core(valw),   core(iwsym), core(abpair) )
      endif
c     call bummer('normal termination',0,3)
      return
      endif


5     continue

      write(nlist,6210)
6210  format(/
     & ' 1) print csf info by sorted index number.'/
     & ' 2) print csf info by contribution threshold.'/
     & ' 3) print csf info by csf number.'/
     & ' 4) set additional print options.'/
     & ' 5) print the entire selected csf vector in sorted order.'/
     & ' 6) print the entire selected csf vector.'/
     & ' 7) (re)read the csf coefficients.'/
     & ' 8) print the current coefficient histogram.'/
     & ' 0) end.' )
c
      j = 0
      call inpi(j,'input menu number')
c
      if ( j .eq. 0 ) then
         write(nlist,*)
         return
      elseif ( j .eq. 1 ) then
         call prsidx(
     &    core(csfvec), core(indcsf), core(ixcsf), core(ref),
     &    core(icsf0),  core(valw),   core(iwsym), core(abpair) )
      elseif ( j .eq. 2 ) then
         call prtol(
     &    core(csfvec), core(indcsf), core(ixcsf), core(ref),
     &    core(icsf0),  core(valw),   core(iwsym), core(abpair) )
      elseif ( j .eq. 3 ) then
         call prnum(
     &    core(csfvec), core(indcsf), core(ixcsf), core(ref),
     &    core(icsf0),  core(valw),   core(iwsym), core(abpair) )
      elseif ( j .eq. 4 ) then
         call setopt( smult, nsym )
      elseif ( j .eq. 5 ) then
         call privec( core(csfvec), core(indcsf), core(ixcsf) )
      elseif ( j .eq. 6 ) then
         call prvec( core(csfvec), core(indcsf) )
      elseif ( j .eq. 7 ) then
         call rdvec(
     &    maxcsf,       lenbuf,      core(ixcsf), core(indcsf),
     &    core(csfvec), core(buf) )
      elseif ( j .eq. 8 ) then
         call prhist
      else
         write(nlist,*) 'enter a valid menu number?'
      endif
      go to 5
c
      end
c deck block data
      block data
       implicit none
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
      integer         nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci
c
      real*8     zero,     one
      parameter( zero=0d0, one=1d0 )
c
      data slabel /
     & 'sym1', 'sym2', 'sym3', 'sym4',
     & 'sym5', 'sym6', 'sym7', 'sym8'   /
c
      data cstep / '0', '1', '2', '3' /
      data cdet  / '.', '+', '-', '#' /
c
      data csfmn, csfmx / zero, one /
c
      data icsfmn / 1 /
      data icsfmx / 1 /
      data ncsft  / 0 /
      data ncsf   / 0 /
c
      data selroot / 1 /
c
      data nhist  / nhistd /
      data fhist  / dfhist /
      data knthst / nhstmx*0 /
      data chist  / nhstmx*zero /
c
      data qdet / .false. /
      data m2   / 0 /
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      end
c deck setopt
      subroutine setopt( smult, nsym )
c
c  set additional printing options.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer smult, nsym
c
      integer j, i
      real*8 dms
c
      real*8     half
      parameter( half=.5d0 )
c
1     write(nlist,6010)
6010  format(/' set additional printing options:'//
     & ' 1) print determinants.'/
     & ' 2) suppress printing of determinants.'/
     & ' 3) reset the cstep(*) array.'/
     & ' 4) reset the cdet(*) array.'/
     & ' 5) reset the slabel(*) array.'/
     & ' 0) end')
c
      j = 0
      call inpi(j,'input print option')
      if ( j .eq. 0 ) then
          return
      elseif ( j .eq. 1 ) then
          qdet = .true.
          write(nlist,6100)
     &    'determinant representations of csfs will be printed'
2         write(nlist,6100) 'smult=(2S+1)=', smult
          dms = (smult - 1) * half
          call inpf(dms,'input ms value for the determinants')
          m2 = nint(2*dms)
          if ( abs(m2)+1 .gt. smult ) then
              write(nlist,*) 'ms too large'
              go to 2
          elseif ( mod(m2,2) .eq. mod(smult,2) ) then
              write(nlist,*) 'incorrect ms'
              go to 2
          endif
      elseif ( j .eq. 2 ) then
          qdet=.false.
          write(nlist,6100)
     &    'determinant representations of csfs will not be printed'
       elseif ( j .eq. 3 ) then
          call inpcv( cstep, 4, 'resetting the cstep*1(0:3) array' )
          write(nlist,6110) cstep
       elseif ( j .eq. 4 ) then
          call inpcv( cdet, 4, 'resetting the cstep*1(0:3) array' )
          write(nlist,6120) cdet
       elseif ( j .eq. 5 ) then
          call inpcv( slabel, nsym,
     &     'resetting the slabel*4(1:nsym) array' )
          write(nlist,6130) (i,slabel(i), i=1,nsym)
      else
          write(nlist,*) 'enter a valid menu number?'
      endif
      go to 1
c
6100  format(1x,a,i4)
6110  format(/' new values of cstep(*):'
     & /'  step=0123'
     & /' cstep=',4a1)
6120  format(/' new values of cdet(*):'
     & /'  occ=0123  [empty, alpha, beta, double]'
     & /' cdet=',4a1)
6130  format(/' new values of slabel(*):'
     & / 8(i2,':',a) )
      end
c deck rdvec
      subroutine rdvec(
     & ncsfmx, lenrec, ixcsf,  indcsf,
     & csfvec, buffer )
c
c  set the selection options and read the ci vector.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      ciuvfl
      equivalence (ciuvfl,iunits(3))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
      integer nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci
c
c     # dummy.
      integer lenrec, ncsfmx
      integer ixcsf(ncsfmx), indcsf(ncsfmx)
      real*8 csfvec(ncsfmx), buffer(lenrec)
c
c     # local.
      integer j, nknt
      integer icsfx(2)
      real*8 csfx(2)
c
      integer ntimes
      save    ntimes
c
      real*8     zero,     one
      parameter( zero=0d0, one=1d0 )
c
      data ntimes / 0 /
c
      nknt = 0
1     write(nlist,6010)
6010  format(/' reset selection options and read',
     & ' the csf coefficients.')
c
      call prselp( 'current csfvec(*) selection parameters:' )
c
      if (nroots.gt.0) then
      write(nlist,6020) nroots
      else
c
c      # only one valid vector on file
c
       selroot=froot
       write(nlist,6021)
      endif
6020  format( /
     & ' 1) reset csfmn, csfmx.'/
     & ' 2) reset fhist.'/
     & ' 3) reset nhist.'/
     & ' 4) reset icsfmn, icsfmx.'/
     & ' 5) set root number to read (',i2,' available)'/
     & ' 0) read the CI vector and end.')
6021  format( /
     & ' 1) reset csfmn, csfmx.'/
     & ' 2) reset fhist.'/
     & ' 3) reset nhist.'/
     & ' 4) reset icsfmn, icsfmx.'/
     & ' 0) read the CI vector and end.')

c
      j = 0
      call inpi(j,'input option')
      if ( j .eq. 0 ) then
         if ( (ntimes .eq. 0) .or. (nknt .ne. 0) ) then
c           # exit, reading the ci vector with
c           # the current parameters if necessary.
            call rdcivnew(
     &       nlist,  ciuvfl, ncsfmx, csfvec,
     &       indcsf, ixcsf,  lenrec, buffer )
         endif
c        ntimes = ntimes + 1
         return
      elseif ( j .eq. 1 ) then
         nknt = nknt + 1
         csfx(1) = zero
         csfx(2) = one
         write(nlist,6150) 'default', csfx
         call inpfv( 2, csfx, 'input csfmn, csfmx' )
c        # clip input to valid values.
         csfmn = max( zero, min( csfx(1), csfx(2) ) )
         csfmx = max( zero,      csfx(1), csfx(2) )
         if ( csfmn .eq. csfmx ) then
c           # must enforce strict inequality.
            write(nlist,6110) 'invalid csfmn=csfmx=', csfmn
            write(nlist,6100) 'resetting to the default values...'
            csfmn = zero
            csfmx = one
         endif
c        # reset the histogram parameters.
         call reseth
      elseif ( j .eq. 2 ) then
         nknt = nknt + 1
         fhist = dfhist
         call inpf( fhist, 'input fhist' )
         if ( (fhist .le. zero) .or. (fhist .ge. one) ) then
            write(nlist,6110) 'invalid fhist=', fhist
            write(nlist,6100) 'resetting to the default value...'
            fhist = dfhist
         endif
c        # reset the histogram parameters.
         call reseth
      elseif ( j .eq. 3 ) then
         nknt = nknt + 1
         nhist = nhistd
         call inpi( nhist, 'input a new value for nhist' )
c        # silently clip to enforce 2<=nhist<=nhstmx.
         nhist = min( max( 2, nhist ), nhstmx )
c        # reset histogram parameters.
         call reseth
      elseif ( j .eq. 4 ) then
         nknt = nknt + 1
         icsfx(1) = 1
         icsfx(2) = ncsft
         write(nlist,6160) 'default', icsfx
         call inpiv( 2, icsfx, 'input icsfmn, icsfmx' )
c        # clip input silently to valid values.
         icsfmn = min( max( 1, min( icsfx(1), icsfx(2) ) ), ncsft )
         icsfmx = min( max( 1, max( icsfx(1), icsfx(2) ) ), ncsft )
         write(nlist,6160) 'new', icsfmn, icsfmx
      elseif (j.eq.5.and.nroots.gt.0) then
      call inpi(selroot, 'input the number of the selected root')
*@ifdef debug
*C
*@else
      if (selroot.gt.nroots .or. selroot.lt.0) selroot=1
*@endif
      else
         write(nlist,6100) 'enter a valid menu number?'
      endif
      go to 1
c
6100  format(1x,a,i4)
6110  format(1x,a,1pe11.4)
6150  format(1x,a,' csfmn =', 1pe11.4, ' csfmx =', e11.4 )
6160  format(1x,a,' icsfmn =', i12, ' icsfmx =', i12 )
      end
c deck prtol
      subroutine prtol(
     & csfvec, indcsf, ixcsf,  ref,
     & icsf0,  valw,   iwsym,  abpair )
c
c prints csf info according to a given threshold contribution.
c
       implicit none 
       integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
      real*8 csfvec(ncsf)
      integer indcsf(ncsf), ixcsf(ncsf), ref(nzwalk),
     & icsf0(nvalwt), valw(nvalwt), iwsym(nvalwt), abpair(nabtot)
c
      integer i, id, num
      real*8 tol(2), abscsf, cmin, cmax
c
      real*8     onem,      zero
      parameter( onem=-1d0, zero=0d0 )
c
      integer nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci

      write(nlist,'(1x,a)')
     & 'csfs will be printed based on coefficient magnitudes.'
c
      call prselp( 'current csfvec(*) selection parameters:' )
c
      call prsym
c
1     continue
      if (batch.eq.2) then
      tol(1) = onem
      tol(2) = csfmx
      call inpfv( 2, tol, 'input the coefficient threshold range'
     & // ' (end with -1/)' )
      else
cjp
       tol(1) = 0.0
cjp
       tol(2) = 1.00
      endif
c
      if ( tol(1) .lt. zero ) then
         return
      else
         cmin = min( tol(1), tol(2) )
         cmax = max( tol(1), tol(2) )
         write(nlist,6010) cmin, cmax
         call prcsfh
         num  = 0
         do 2 i = 1, ncsf
cjp  print in original order when processing for cioverlap
               if(batch.eq.3) then
                     id = i
               else
                     id = ixcsf(i)
               endif
            abscsf = abs( csfvec(id) )
            if ( abscsf .lt. cmin ) go to 3
            if ( abscsf .le. cmax ) then
               call prtcsf(
     &          indcsf(id), csfvec(id), ref,    icsf0,
     &          valw,       iwsym,      abpair,batch )
               num = num + 1
            endif
2        continue
3        continue
         write(nlist,6020) num
      endif
      if (batch.eq.2) go to 1
c
6010  format(/' printing selected csfs in sorted order from cmin =',
     & f8.5,' to cmax =', f8.5 )
6020  format(1x,i12,' csfs were printed in this range.')
      end
c deck prsidx
      subroutine prsidx(
     & csfvec, indcsf, ixcsf,  ref,
     & icsf0,  valw,   iwsym,  abpair )
c
c  prints csf info according to their sorted index.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc

      integer nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci

c
      real*8 csfvec(ncsf)
      integer indcsf(ncsf), ixcsf(ncsf), ref(nzwalk),
     & icsf0(nvalwt), valw(nvalwt), iwsym(nvalwt), abpair(nabtot)
c
      integer istart, iend, i, id
      integer iarray(2)
c
      write(nlist,'(1x,a)')
     & 'selected csfs will be printed by sorted index ranges.'
c
      call prselp( 'current csfvec(*) selection parameters:' )
c
      call prsym
c
10    continue
      iarray(1) = 0
      iarray(2) = 0
      call inpiv( 2, iarray, 'input istart,iend (end with 0/)')
      istart = min( iarray(1), iarray(2) )
      iend   = max( istart, min( max( iarray(1), iarray(2) ), ncsf ) )
      if ( iarray(1) .eq. 0 ) then
         return
      elseif ( (istart.gt.ncsf) .or. (istart.lt.0) ) then
         write(nlist,*)' 0 < istart <', ncsf
      else
         write(nlist,6010) istart, iend
         call prcsfh
         do 15 i = istart, iend
            id = ixcsf(i)
            call prtcsf(
     &       indcsf(id), csfvec(id), ref,    icsf0,
     &       valw,       iwsym,      abpair ,batch)
15       continue
      endif
      go to 10
c
6010  format(/' printing selected csfs in sorted order from istart =',
     & i8, ' to iend =', i12, '.' )
      end
c deck prnum
      subroutine prnum(
     & csfvec, indcsf, ixcsf,  ref,
     & icsf0,  valw,   iwsym,  abpair )
c
c  prints csf info according to csf index.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2

      integer nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci

c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
      real*8 csfvec(ncsf)
      integer indcsf(ncsf), ixcsf(ncsf), ref(nzwalk),
     & icsf0(nvalwt), valw(nvalwt), iwsym(nvalwt), abpair(nabtot)
c
      integer istart, iend, i, icsf, num
      integer iarray(2)
c
      write(nlist,'(1x,a)') 'csfs will be printed by index ranges.'
c
      call prselp( 'current csfvec(*) selection parameters:' )
c
      call prsym
c
10    continue
      iarray(1) = 0
      iarray(2) = 0
      call inpiv( 2, iarray, 'input istart,iend (end with 0,0)')
      istart = min( iarray(1), iarray(2) )
      iend   = max( istart, min( max( iarray(1), iarray(2) ), ncsft ) )
      if (iarray(1) .eq. 0 ) then
         return
      elseif ( (istart.gt.ncsft) .or. (istart.lt.0) ) then
         write(nlist,*)' 0 < istart <', ncsft
      else
         write(nlist,6010) istart, iend
         call prcsfh
         num = 0
         do 15 i = 1, ncsf
            icsf = indcsf(i)
            if ( icsf .gt. iend ) then
               goto 16
            elseif ( icsf .ge. istart ) then
               call prtcsf(
     &          icsf,   csfvec(i), ref,    icsf0,
     &          valw,   iwsym,     abpair , batch)
               num = num + 1
            endif
15       continue
16       continue
         write(nlist,6020) num
      endif
      go to 10
c
6010  format(/' printing selected csfs from istart =',
     & i8, ' to iend =', i12, '.' )
6020  format(1x,i12,' csfs were printed in this range.')
      end
c deck prsym
      subroutine prsym
c
c  print the internal orbital info.
c
c  28-sep-91 written by ron shepard.
c
       implicit none
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=1023 )
c
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
c.....include 'cdrt.h'
      integer          l,               y,               syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3), y(0:3,nrowmx,3), syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
c.....end of cdrt
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
      integer i
c
      write(nlist,6000) (i,slabel(i), i=1,nsym)
c
      if ( nfct .gt. 0 ) then
         write(nlist,*)
         write(nlist,6100) 'frozen orbital =',
     &    (i,                 i=1,nfct)
         write(nlist,6100) 'symfc(*)       =',
     &    (mapd(1,i),         i=1,nfct)
         write(nlist,6110) 'label          =',
     &    (slabel(mapd(1,i)), i=1,nfct)
         write(nlist,6100) 'rmo(*)         =',
     &    (mapd(2,i),         i=1,nfct)
      endif
c
      write(nlist,*)
      write(nlist,6100) 'internal level =', (i,               i=1,nimot)
      write(nlist,6100) 'syml(*)        =', (syml(i),         i=1,nimot)
      write(nlist,6110) 'label          =', (slabel(syml(i)), i=1,nimot)
      write(nlist,6100) 'rmo(*)         =', (modrt(i),        i=1,nimot)
c
      return
6000  format(/' i:slabel(i) =', 8(i3,':',a4) )
6100  format(1x,a,(10i5))
6110  format(1x,a,(10(1x,a4)))
      end
c deck prcsfh
      subroutine prcsfh
c
c  print the header info for subsequent prtcsf() calls.
c
c  28-sep-91 written by ron shepard.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      write(nlist,6200)
6200  format(
     & /'   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)'
     & /'  ------- -------- ------- - ---- --- ---- --- ------------')
c
      return
      end
c deck prtcsf
      subroutine prtcsf(
     & icsf,   coeff,  ref,    icsf0,
     & valw,   iwsym,  abpair ,batch)
c
c  print the info for csf number icsf.
c
c  28-oct-91 written by ron shepard.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer    ashift
      parameter( ashift = 1000 )
c
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=1023 )
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
c.....include 'cdrt.h'
      integer          l,               y,               syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3), y(0:3,nrowmx,3), syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
c.....end of cdrt
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
c     # dummy.
      integer icsf
      integer ref(nzwalk), icsf0(nvalwt), valw(nvalwt), iwsym(nvalwt),
     & abpair(nabtot),batch
      real*8 coeff
c
c     # local.
      integer i, lowlev, iver, idrt, low, high, middle,
     & iwalk, abnum, ab, a, b
      real*8 coeff2
      character cnum*8, cnum2*7
c
c     # the parameter small should be consistent with
c     # formats 5010, 5020, 5030, and 5040.
      real*8     small
      parameter( small=1.d-4 )
c
      integer step(-1:nimomx)
cjp
      integer orbnum(-1:nimomx)
      integer orbsym(-1:nimomx)
cjp
      character*1 cstar(0:1)
c
      data cstar / ' ', '*' /

cjp
      integer myroot,eivecfile,slaterfile,jirec,nelec,symshift
      common/jiri/myroot,eivecfile,slaterfile,jirec,nelec,symshift
cjp
c
c     # determine the vertex: z, y, x, w.
c
      if ( icsf .lt. ivcsf1(3) ) then
         if ( icsf .lt. ivcsf1(2) ) then
            iver = 1
         else
            iver = 2
         endif
      else
         if ( icsf .lt. ivcsf1(4) ) then
            iver = 3
         else
            iver = 4
         endif
      endif
c
c     # use the correct set of chaining indices.
      idrt = min( iver, 3 )
c
c     # determine the valid internal walk number.
      low  = valw0(iver)
      high = low + nvalw(iver) + 1
100   if ( (high - low) .gt. 1 ) then
c        # linear interpolation may be better here.  for now, just
c        # do a binary search. -rls
         middle = (high + low) / 2
         if ( icsf .le. icsf0(middle) ) then
            high = middle
         else
            low = middle
         endif
         goto 100
      endif
c     # determine the internal walk index.
      iwalk = valw( low )
c
c     # generate the internal step vector.
      call gnstep(
     & iwalk, iver, nimot, l(0,1,idrt),
     & y(0,1,idrt), step(1) )
c
c     # set cnum and cnum2.
c     # the goal is to print the most significant figures within
c     # the available space.  the standard editing is insufficient. -rls
      if ( abs(coeff) .ge. small ) then
         write( cnum, 5010 ) coeff
      else
         write( cnum, 5020 ) coeff
      endif
      coeff2 = coeff * coeff
      if ( coeff2 .ge. small ) then
         write( cnum2, 5030 ) coeff2
      else
         write( cnum2, 5040 ) coeff2
      endif
c
cjp
      do i=1,nimot
       orbnum(i)=modrt(i)
       orbsym(i)=syml(i)
      enddo
cjp
c     # determine the external orbitals, if any.
      if ( iver .eq. 1 ) then
c        # z-walk.  print '*' for reference csfs.
         lowlev = 1
         write(nlist,6100) icsf, cnum, cnum2,
     &    cstar( ref( iwalk) ),
     &    (cstep(step(i)), i=lowlev,nimot)
      else
         abnum = icsf - icsf0(low)
         ab = abpair( aboff( iwsym(low), iver ) + abnum )
         if ( iver .eq. 2 ) then
c           # y walk.  only 1 external orbital.
            lowlev  = 0
            step(0) = 1
cjp
            orbnum(0)=mapl(2,ab)
            orbsym(0)=mapl(1,ab)
cjp
            write(nlist,6110) icsf, cnum, cnum2, 'y',
     &       slabel(mapl(1,ab)), mapl(2,ab),
     &       (cstep(step(i)), i=lowlev,nimot)
         elseif ( iver .eq. 3 ) then
c           # x-walk. two triplet-coupled external orbitals.
            a  = ab / ashift
            b  = mod( ab, ashift )
            lowlev   = -1
            step(-1) = 1
            step( 0) = 1
cjp
            orbnum(-1)=mapl(2,b)
            orbnum(0)=mapl(2,a)
            orbsym(-1)=mapl(1,b)
            orbsym(0)=mapl(1,a)
cjp
            write(nlist,6120) icsf, cnum, cnum2, 'x',
     &       slabel(mapl(1,b)), mapl(2,b),
     &       slabel(mapl(1,a)), mapl(2,a),
     &       (cstep(step(i)), i=lowlev,nimot)
         elseif ( iver .eq. 4 ) then
c           # w-walk. one or two external orbitals.
            a  = ab / ashift
            b  = mod( ab, ashift )
            if ( a .eq. b ) then
c              # a single doubly occupied external orbital.
               lowlev  = 0
               step(0) = 3
cjp
               orbnum(0)=mapl(2,b)
               orbsym(0)=mapl(1,b)
cjp
               write(nlist,6110) icsf, cnum, cnum2, 'w',
     &          slabel(mapl(1,b)), mapl(2,b),
     &          (cstep(step(i)), i=lowlev,nimot)
            else
c              # two singlet-coupled singly occupied external orbitals.
               lowlev   = -1
               step(-1) = 1
               step( 0) = 2
cjp
               orbnum(-1)=mapl(2,b)
               orbnum(0)=mapl(2,a)
               orbsym(-1)=mapl(1,b)
               orbsym(0)=mapl(1,a)
cjp
               write(nlist,6120) icsf, cnum, cnum2, 'w',
     &          slabel(mapl(1,b)), mapl(2,b),
     &          slabel(mapl(1,a)), mapl(2,a),
     &          (cstep(step(i)), i=lowlev,nimot)
            endif
         endif
      endif
c
ctm should be run only, if batch=3 or batch=4 
cjp      if ( qdet ) 
       if (qdet .or. (batch.ge.3)) 
     & call prtdet(lowlev,nimot,m2,step(lowlev),
     &             coeff,orbnum(lowlev),orbsym(lowlev),qdet)
c
      return
5010  format(f8.5)
5020  format(1pe8.1)
5030  format(f7.5)
5040  format(1pe7.1)
6100  format(1x, i10, a9, a8, ' z',
     & a1, 8x,
     & 9x,
     & 3x, 128a1 )
6110  format(1x, i10, a9, a8, 1x, a1,
     & 9x,
     & 1x, a4, ':', i3,
     & 2x, 129a1 )
6120  format(1x, i10, a9, a8, 1x, a1,
     & 1x, a4, ':', i3,
     & 1x, a4, ':', i3,
     & 1x, 130a1 )
      end
c deck prtdet
cjp
      subroutine prtdet(lowlev,nimot,ms2,step,cicoeff,orbnum,orbsym,
     &                  qdet)
cjp
c
c  construct and print the determinant representation of a csf.
c
c  04-Nov-91 lowlev added for external orbitals. -rls
c  28-sep-91 minor cleanup. -rls
c  1982 (approximately) written by ron shepard.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
c     # dummy.
      integer lowlev, nimot, ms2
      integer step(nimot-lowlev+1)
c
c     # construct determinants corresponding to nopen open shells,
c     # nalpha alpha als, and nclosd closed shell orbitals.
c     # this is done by placing the alpha electrons into nopen locations
c     # in all possible ways.  for each determinant, determine its
c     # projection onto the csf.
c
c     # local.
      integer i, nopen, nclosd, btot, dk, nalpha, idet, ndet, low,
     & sign, m2, k, num, denom, nxmot
c
      integer    nimomx,    nxmomx
      parameter( nimomx=128, nxmomx=nimomx+2 )
c
      real*8 coeff, xnorm
c
      integer open(nxmomx)
      integer rdet(nxmomx), det(nxmomx), b(nxmomx)
      integer alpha(nxmomx)
      integer db(0:3)
      integer mz2(2)
      integer del(2)
      integer d1f(2)
      integer d2f(2)
      integer m1p1(0:1)
c
      real*8     zero,     one,     tol
      parameter( zero=0d0, one=1d0, tol=1d-10 )
c
      intrinsic mod, sqrt, abs
c
      integer m1pow
      character*28 cfmt(-1:1)
c
      data db   /  0, 1,-1, 0 /
      data mz2  /  1,-1 /
      data del  /  1, 0 /
      data d1f  /  1,-1 /
      data d2f  / -1, 1 /
      data m1p1 /  1,-1 /

cjp
      integer nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci
      integer reclunit
      parameter(reclunit=1)
      integer myroot,eivecfile,slaterfile,jirec,nelec,symshift
      common/jiri/myroot,eivecfile,slaterfile,jirec,nelec,symshift
      real*8 cicoeff
      logical qdet
      integer orbnum,orbsym
      dimension orbnum(*),orbsym(*)
      integer*2 slater
      dimension slater(1024)
      integer j
cjp
c
c     # cfmt(lowlev) is the format to use for the various cases.
      data cfmt /
     & '(32x,i3,'':'',f14.10,1x,52a1)' ,
     & '(32x,i3,'':'',f14.10,2x,51a1)' ,
     & '(32x,i3,'':'',f14.10,3x,50a1)' /
c
c     #  m1pow(i) = (-1)**i
      m1pow(i) = m1p1( mod( i, 2 ) )
c
c     # nxmot = number of entries in step(*).
      nxmot = nimot - lowlev + 1
c
c     # initialize determinant info.
c
      do 10 i = 1, nxmot
         rdet(i) = 0
10    continue
c
      nopen  = 0
      nclosd = 0
      btot   = 0
c
      do 20 k = 1, nxmot
         dk = step(k)
         btot = btot + db(dk)
         b(k)= btot
         if ( dk .eq. 1 .or. dk .eq. 2 ) then
            nopen = nopen + 1
            open(nopen) = k
            rdet(k) = 2
         elseif ( dk .eq. 3 ) then
            nclosd = nclosd+1
            rdet(k) = 3
         endif
20    continue
c
      nalpha = (ms2 + nopen) / 2
c
c     # det(i)=0  if orbital i is empty.
c     # det(i)=1  if alpha al i is occupied.
c     # det(i)=2  if beta al i is occupied.
c     # det(i)=3  if orbital i is doubly occupied.
c
c     # set up for the first determinant.
c
      xnorm = zero
      alpha(nalpha+1) = nopen
      idet = 0
      ndet = 0
      low = nalpha + 1
c
c     # loop over determinants.
c
100   continue
      ndet = ndet + 1
      alpha(low) = alpha(low) + 1
c
      do 110 i = 1, (low-1)
         alpha(i) = i
110   continue
c
      do 115 i = 1, nxmot
         det(i) = rdet(i)
115   continue
c
      do 120 i = 1, nalpha
         det(open(alpha(i))) = 1
120   continue
c
c     # calculate the coefficient.
c
      coeff = one
      sign  = +1
      m2    = 0
      do 4 k = 1, nxmot
         dk = step(k)
c
c        # note: dk=0 entries do not contribute to the final
c        #       coefficient, so dk=0 external level contributions
c        #       may be safely ignored. -rls
c
         if ( dk .eq. 1 ) then
c
c           # alpha al: factor=sqrt( (b+2*m)/(2*b) )
c           # beta  al: factor=sqrt( (b-2*m)/(2*b) )
c
            m2  = m2 + mz2(det(k))
            num = (b(k)+d1f(det(k))*m2)
            if ( num .eq. 0 ) go to 5
            denom = 2 * b(k)
            coeff = (coeff*num) / denom
c
         elseif ( dk .eq. 2 ) then
c
c           # alpha al:
c           #          factor=(-1)**(b+1) * sqrt( (b+2-2*m)/(2*(b+2)) )
c           # beta  al:
c           #          factor=(-1)**(b)   * sqrt( (b+2+2*m)/(2*(b+2)) )
c
            m2  = m2 + mz2(det(k))
            num = (b(k) + 2 + d2f(det(k))*m2)
            if ( num .eq. 0 ) go to 5
            denom = 2 * (b(k) + 2)
            sign  = sign * m1pow( b(k) + del(det(k)) )
            coeff = (coeff*num) / denom
c
         elseif ( dk .eq. 3 ) then
c
c           # factor=(-1)**(b)
c
            sign = sign * m1pow( b(k) )
c
         endif
4     continue
c
      idet  = idet + 1
      xnorm = xnorm + coeff
      coeff = sqrt(coeff) * sign
cjp
      if(qdet) write( unit=nlist, fmt=cfmt(lowlev) )
     & ndet, coeff*cicoeff, (cdet(det(i)),i=1,nxmot)
c
      j=1
      do i=1,nxmot
        if(det(i).eq.1) then
        slater(j)=orbnum(i) + symshift * (orbsym(i)-1)
        j=j+1
        endif
        if(det(i).eq.2) then
        slater(j)= -orbnum(i) - symshift * (orbsym(i)-1)
        j=j+1
        endif
        if(det(i).eq.3) then
        slater(j)=orbnum(i) + symshift * (orbsym(i)-1)
        slater(j+1)= -orbnum(i) - symshift * (orbsym(i)-1)
        j=j+2
        endif
      enddo
      nelec=j-1
      if(myroot.eq.1 .and. jirec.eq.0.and.slaterfile.ne.0) then
           open(file='slaterfile',unit=slaterfile,access='direct',
     &      form='unformatted',recl=2*nelec/reclunit)
      endif
      jirec=jirec+1
      if(myroot.eq.1.and.slaterfile.ne.0) then
          if(qdet)write(6,*) 'SLATER ',jirec,' : ',(slater(j),j=1,nelec)
          write(slaterfile,rec=jirec) (slater(j),j=1,nelec)
      endif
      if(eivecfile.ne.0) write(eivecfile,rec=jirec) coeff*cicoeff
cjp
5     continue
c
c     # finished with idet.  find next determinant.
c
      do 130 i = 1, nalpha
         low =i
         if ( alpha(i) .lt. (alpha(i+1)-1) ) go to 100
130   continue
c
c     # exit from loop means all through.
c
      if(batch.ne.1) write(nlist,6050) idet, ndet
6050  format(34x,' idet=',i3,' ndet=',i3)
      if ( abs(one-xnorm) .gt. tol ) write(nlist,*)'error in xnorm'
c
      return
      end
c deck prvec
      subroutine prvec( csfvec, indcsf )
c
c  print the entire selected CI vector ordered by csf number.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
      real*8 csfvec(ncsf)
      integer indcsf(ncsf)
c
      integer j1, j2, i, i1, i2, i0
c
      integer    npline,   npblk
      parameter( npline=5, npblk=10*npline )
c
      character*7 cchar(npline)
c
      real*8     small
      parameter( small=1d-4 )
c
      write(nlist,6000)
c
      call prselp( 'current csfvec(*) selection parameters:' )
c
*@ifdef reference
*      do 100 i1 = 1, ncsf, 50
*         i2 = min( ncsf, i1+50-1 )
*         write(nlist,xxxx) i1, i2, (indcsf(i),csfvec(i),  i=i1,i2 )
*100   continue
*@else
      do 300 j1 = 1, ncsf, npblk
         j2 = min( ncsf, j1 + npblk - 1 )
         write(nlist,6100) j1, j2
         do 200 i1 = j1, j2, npline
            i0 = i1 - 1
            i2 = min( j2, i0 + npline )
            do 100 i = i1, i2
c              # fill up cchar(*) to display the maximum number of
c              # significant digits.  standard formats are
c              # insufficient. -rls
               if ( abs( csfvec(i) ) .lt. small ) then
                  write( cchar(i-i0), 5010 ) csfvec(i)
               else
                  write( cchar(i-i0), 5020 ) csfvec(i)
               endif
100         continue
            write(nlist,6200) (indcsf(i),cchar(i-i0),  i=i1,i2 )
200      continue
300   continue
*@endif
c
      return
5010  format(1pe7.0)
5020  format(f7.4)
6000  format(/
     & ' print the entire selected csf vector. (indcsf(i):csfvec(i))')
6100  format(4x,'indcsf(', i7, '):indcsf(', i7, ')' )
6200  format(1x,5(i7,':',a7))
      end
c deck privec
      subroutine privec( csfvec, indcsf, ixcsf )
c
c  print the entire selected CI vector in sorted order.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
      real*8 csfvec(ncsf)
      integer indcsf(ncsf), ixcsf(ncsf)
c
      integer j1, j2, i, i1, i2, i0, ix
c
      integer    npline,   npblk
      parameter( npline=5, npblk=10*npline )
c
      character*7 cchar(npline)
c
      real*8     small
      parameter( small=1d-4 )
c
      write(nlist,6000)
c
      call prselp( 'current csfvec(*) selection parameters:' )
c
*@ifdef reference
*      do 100 i1 = 1, ncsf, 50
*         i2 = min( ncsf, i1+50-1 )
*         write(nlist,6100) i1, i2,
*     &    (indcsf(ixcsf(i)),csfvec(ixcsf(i)),  i=i1,i2 )
*100   continue
*@else
      do 300 j1 = 1, ncsf, npblk
         j2 = min( ncsf, j1 + npblk - 1 )
         write(nlist,6100) j1, j2
         do 200 i1 = j1, j2, npline
            i0 = i1 - 1
            i2 = min( j2, i0 + npline )
            do 100 i = i1, i2
c              # fill up cchar(*) to display the maximum number of
c              # significant digits.  standard formats are
c              # insufficient. -rls
               ix = ixcsf(i)
               if ( abs( csfvec(ix) ) .lt. small ) then
                  write( cchar(i-i0), 5010 ) csfvec(ix)
               else
                  write( cchar(i-i0), 5020 ) csfvec(ix)
               endif
100         continue
            write(nlist,6200) (indcsf(ixcsf(i)),cchar(i-i0),  i=i1,i2 )
200      continue
300   continue
*@endif
c
      return
5010  format(1pe7.0)
5020  format(f7.4)
6000  format(/
     & ' print the entire selected csf vector in sorted order. '
     & // '(indcsf(ixcsf(i)):csfvec(ixcsf(i)))')
6100  format(4x,'ixcsf(', i7, '):ixcsf(', i7, ')' )
6200  format(1x,5(i7,':',a7))
      end
c deck rdhdrt
      subroutine rdhdrt(
     & title,  nmot,   niot,   nfct,
     & nfvt,   nrow,   nsym,   ssym,
     & xbar,   nvalw,  ncsft,  lenbuf,
     & nwalk,  niomx,  nmomx,  nrowmx,
     & nwlkmx, buf,    nlist,  ndrt )
c
c  read the header info from the drt file.
c
       implicit none
      integer nmot, niot, nfct, nfvt, nrow, nsym, ssym, ncsft,
     & lenbuf, nwalk, niomx, nmomx, nrowmx, nwlkmx, nlist, ndrt
      integer xbar(4), nvalw(4), buf(*)
      character*80 title
c
      integer nhdint, vrsion, ierr, nvalwt
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif

c
c     # title card...
c
      rewind ndrt
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
*@ifdef spinorbit
      read(ndrt,'(2L4,3I4)',iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if (spnorb) then
        call bummer('cannot cope with SO-CI',0,0)
        call bummer('normal termination',0,3)
        stop
      endif
*@endif
c
      write(nlist,6010) title
c
c     # number of integer drt parameters, buffer length, and version.
c
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 6 ) then
       call bummer(' drt version 6 required',vrsion,2)
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning: rdhdrt: drt version .ne. 3 *** vrsion=',vrsion
*      endif
*@endif
c
c     # header info...
c
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      nmot     = buf(1)
      niot     = buf(2)
      nfct     = buf(3)
      nfvt     = buf(4)
      nrow     = buf(5)
      nsym     = buf(6)
      ssym     = buf(7)
      xbar(1)  = buf(8)
      xbar(2)  = buf(9)
      xbar(3)  = buf(10)
      xbar(4)  = buf(11)
      nvalw(1) = buf(12)
      nvalw(2) = buf(13)
      nvalw(3) = buf(14)
      nvalw(4) = buf(15)
      ncsft    = buf(16)
*@ifdef spinorbit
      spnorb  = buf(17) .eq. 1
      spnodd  = buf(18) .eq. 1
      lxyzir(1) = buf(19)
      lxyzir(2) = buf(20)
      lxyzir(3) = buf(21)
      if (spnorb) then
         call bummer('cannot cope with SO-CI',0,0)
         call bummer('normal termination',0,3)
         stop
      endif
*@endif
c
      nwalk  = xbar(1)  + xbar(2)  + xbar(3)  + xbar(4)
      nvalwt = nvalw(1) + nvalw(2) + nvalw(3) + nvalw(4)
c
      write(nlist,6020) nmot, niot, nfct, nfvt, nrow, nsym, ssym, lenbuf
*@ifdef spinorbit
     & , spnorb, spnodd, lxyzir
*@endif
      write(nlist,6100)'nwalk,xbar', nwalk, xbar
      write(nlist,6100)'nvalwt,nvalw', nvalwt, nvalw
      write(nlist,6100)'ncsft', ncsft
c
      if ( niot .gt. niomx ) then
         write(nlist,6900)'niot,niomx', niot, niomx
         call bummer('rdhead: niomx=',niomx,faterr)
      endif
      if ( nmot .gt. nmomx ) then
         write(nlist,6900)'nmot,nmomx', nmot, nmomx
         call bummer('rdhead: nmomx=',nmomx,faterr)
      endif
      if ( nrow .gt. nrowmx ) then
         write(nlist,6900)'nrow,nrowmx', nrow, nrowmx
         call bummer('rdhead: nrowmx=',nrowmx,faterr)
      endif
      if ( nwalk .gt. nwlkmx ) then
         write(nlist,6900)'nwalk,nwlkmx', nwalk, nwlkmx
         call bummer('rdhead: nwlkmx=',nwlkmx,faterr)
      endif
c
      return
c
6010  format(/' drt header information:'/1x,a)
6020  format(
     & ' nmot  =',i6,' niot  =',i6,' nfct  =',i6,' nfvt  =',i6/
     & ' nrow  =',i6,' nsym  =',i6,' ssym  =',i6,' lenbuf=',i6:/
     & ' spnorb=',l6,' spnodd=',l6,' lxyzir(1:3)=',3i2)
6100  format(1x,a,':',t15,5i12)
6900  format(/' error: ',a,3i10)
      end
c deck rddrt
      subroutine rddrt(
     & nmot,    niot,    nsym,   nrow,
     & nwalk,   lenbuf,  map,    nmpsy,
     & nexo,    modrt,   mu,     syml,
     & nj,      njskp,   a,      b,
     & l,       y,       xbar,   limvec,
     & nrowmx,  nvalwz,  nvwxy,  iwsym,
     & nzwalk,  ref,     slabel, smult,
     & buf )
c
c  read the drt arrays from the drt file.
c  title and dimension information have already been read.
c  the file is assumed to be positioned correctly for the
c  next records.
c  vectors are buffered in records of length lenbuf.
c
       implicit none
      integer nmot, niot, nsym, nrow, nwalk, lenbuf, nrowmx,
     & nvalwz, nvwxy, nzwalk, smult
      integer map(nmot)
      integer nmpsy(nsym)
      integer nexo(nsym)
      integer modrt(niot)
      integer syml(niot)
      integer nj(nrow)
      integer njskp(nrow)
      integer mu(niot)
      integer a(nrow)
      integer b(nrow)
      integer l(0:3,nrowmx,3)
      integer y(0:3,nrowmx,3)
      integer xbar(nrowmx,3)
      integer limvec(nwalk)
c     #       iwsym(nvalwt)
      integer iwsym(*)
      integer ref(nzwalk)
      integer buf(lenbuf)
      character*4 slabel(8)
c
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      ciuvfl
      equivalence (ciuvfl,iunits(3))
      integer      ndrt
      equivalence (ndrt,iunits(4))
      integer limvecinfo(3)
c
c     # local:
      integer nlevel, i, ntot
      integer nsm(8),numv1,ncsft,icd(10)
      integer, allocatable :: arcsym(:,:)
      integer*4 buf32(8)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

      integer       mult,      nnsym, ssym
      common /csym/ mult(8,8), nnsym, ssym

c
      nlevel = niot + 1
      allocate(arcsym(0:3,0:niot))
c
c     # slabel(*)...
      call rddbl( ndrt, lenbuf, buf32, nsym )
      do 10 i = 1, nsym
         write( slabel(i), fmt='(a4)' ) buf32(i)
 10    continue
c
c     # map(*)...
      call rddbl( ndrt, lenbuf, map, nmot )
      write(nlist,6100)'map(*)=',map
c
c     # mu(*)...
      call rddbl( ndrt, lenbuf, buf, niot )
      write(nlist,6100)'mu(*)=',(buf(i),i=1,niot)
c
c     # nmpsy(*)...
      call rddbl( ndrt, lenbuf, nmpsy, nsym )
c
c     # nexo(*)...
      call rddbl( ndrt, lenbuf, nexo, nsym )
c
c     # nviwsm(*)...
      call rddbl( ndrt, lenbuf, buf, -4*nsym )
c
c     # ncsfsm(*)...
      call rddbl( ndrt, lenbuf, buf, -4*nsym )
c
c     # modrt(*)...
      call rddbl( ndrt, lenbuf, modrt(1), niot )
c
c     # syml(*)...
      call rddbl( ndrt, lenbuf, syml, niot )

      do 25 i = 1, niot
         arcsym(0,i-1) = 1
         arcsym(1,i-1) = syml(i)
         arcsym(2,i-1) = syml(i)
         arcsym(3,i-1) = 1
25    continue

c
c     # change from global orbital to symmetry orbital numbering...
      ntot = 0
      do 20 i = 1, 8
         nsm(i) = ntot
         ntot   = ntot + nmpsy(i)
20    continue
      do 30 i = 1, niot
         modrt(i) = modrt(i) - nsm( syml(i) )
30    continue
c
      write(nlist,6100)'syml(*) =',(syml(i),i=1,niot)
      write(nlist,6100)'rmo(*)=',(modrt(i),i=1,niot)
c
c     # nj(*)...
      call rddbl( ndrt, lenbuf, buf, -nlevel )
c
c     # njskp(*)...
      call rddbl( ndrt, lenbuf, buf, -nlevel )
c
c     # a(*)...
      call rddbl( ndrt, lenbuf, buf, -nrow )
c
c     # b(*)...
      call rddbl( ndrt, lenbuf, b, nrow )
c     # compute smult=b(nrow)+1
      smult = b(nrow)+1
c     smult = buf( mod(nrow-1,lenbuf) + 1 ) + 1
c
c     # l(*,*,*)...
      do 40 i = 1, 3
         call rddbl( ndrt, lenbuf, l(0,1,i), 4*nrow )
40    continue
c
c     # y(*,*,*)...
      do 50 i = 1, 3
         call rddbl( ndrt, lenbuf, y(0,1,i), 4*nrow )
50    continue
c
c     # xbar(*,*)...
      do 60 i = 1, 3
         call rddbl( ndrt, lenbuf, xbar(1,i), nrow )
60    continue
c
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed limvec)
      call rddbl( ndrt, lenbuf, limvec, limvecinfo(2) )
      call uncmprlimvec(limvec,nwalk,limvecinfo(1),limvecinfo(2),
     .  limvecinfo(3))

c
c     # iwsym(*) for valid wxy walks only...

      
       call skpx01( nwalk, limvec, 0, numv1 )
       write(0,*) 'numv1=',numv1

       icd(1)=1              ! row
       icd(2)=icd(1)+niot+1  ! step
       icd(3)=icd(2)+niot+1  ! wsym
       icd(4)=icd(3)+niot+1  ! ytot
       icd(5)=icd(4)+niot+1  ! nviwsm
       icd(6)=icd(5)+nsym*4  ! ncsfsm
       icd(7)=icd(6)+nsym*4
       if (icd(7).gt.lenbuf) call bummer('insufficient memory',icd(7),2)

        call  symwlk_noso(
     & niot,   nsym,   nrow,   nrowmx,
     & nwalk,  l,  y, buf(icd(1)),
     & buf(icd(2)),iwsym,buf(icd(3)),arcsym,
     & buf(icd(4)),   xbar,   ssym,
     & mult,limvec, ncsft,
     & buf(icd(5)),buf(icd(6)), b, numv1 )

       call skpx01( nwalk, limvec, 1, numv1 )

       deallocate(arcsym)


c     call rddbl( ndrt, lenbuf, iwsym(nvalwz+1), nvwxy )
c     # set the initial nzwalk entries for return.
c     call iset( nvalwz, 1, iwsym, 1 )
c
c     # 0/1 ref(*)...
      call rddbl( ndrt, lenbuf, limvecinfo,3)
c     there are only limvecinfo(2) entries on cidrtfl
c     (compressed limvec)
      call rddbl( ndrt, lenbuf, ref, limvecinfo(2) )
      call uncmprlimvec(ref,nzwalk,limvecinfo(1),limvecinfo(2),
     .  limvecinfo(3))
c
      return
6100  format(1x,a,(t12,20i3))
      end
c deck gnstep
      subroutine gnstep(
     & iwalk,  iver,   niot,   l,
     & y,      step )
c
c  generate the step vector for a given internal walk index.
c
c input:
c iwalk = internal walk number.
c iver = initial vertex.
c        (1 for z-walks, 2 for y, 3 for x, and 4 for w walks)
c niot = number of internal orbitals.
c l(0:3,*) = cidrt chaining indices.
c y(0:3,*) = arc weights. (for 3-index drt, 1 piece only).
c
c output arguments:
c step(0:niot-1) = generated step vector.
c
c  08-jun-89 iver added for non-z-walks. -rls
c  07-may-89 written by ron shepard.
c
       implicit none
c     # dummy:
      integer iwalk, iver, niot
      integer l(0:3,*), y(0:3,*)
      integer step(0:niot-1)
c
c     # local:
      integer ytot, rowc, nlev, clev, istept, istep, rown, ytemp
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      rown  = 0
      istep = 0
      ytemp = 0
c
      ytot  = iwalk - 1
      rowc  = iver
      do 20 nlev = 1, niot
         clev = nlev - 1
         do 10 istept = 0, 3
            istep = istept
            rown = l(istep,rowc)
            if ( rown .eq. 0 ) go to 10
            ytemp = ytot - y(istep,rowc)
            if ( ytemp .ge. 0 ) go to 11
10       continue
c        # ...no valid exit from the loop.
         call bummer('gnstep: ytemp=',ytemp,faterr)
11       continue
         ytot = ytemp
         rowc = rown
         step(clev) = istep
20    continue
c
c     # should probably return an error code.  for now just stop.
      if ( ytot .ne. 0 ) call bummer('gnstep: ytot=',ytot,faterr)
c
      return
      end
c deck setxv
      subroutine setxv(
     & ncsft,  iwsym,  icsf0,  mxpair,
     & abpair, level,  mapl,   mapd )
c
c  set some miscellaneous addressing arrays for subsequent
c  external orbital determinations.
c
c  input:
c  ncsft  = total number of expansion csfs.
c  iwsym(1:nvalwt) = internal walk symmetry vector.
c                     iwsym(i) = mult( internal_walk_symmetry, ssym )
c  mxpair = maximum size of abpair(*).
c  nmot = total number of orbitals, including frozen cores and frozen
c         virtuals.
c  level(1:nmot) = orbital-to-level mapping vector.
c  nfct = total number of frozen core orbitals.
c
c  output:
c  icsf0(1:nvalwt) = csf offset vector.  the first csf corresponding to
c                    internal_walk = iwalk is (icsf0(iwalk) + 1).
c  abpair(1:nabtot) = external level pairs encoded as
c                         ab = a * ashift + b
c  mapl(1:2,1:nlevel) = level to orbital mapping vector.
c                       mapl(1,i)=sym_i, mapl(2,i)=rmo_i
c  mapd(1:2,1:nfct) = frozen core orbital list.
c                       mapd(1,i)=sym_i, mapd(2,i)=rmo_i
c
c  28-oct-91 written by ron shepard.
c
       implicit none
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
c.....include 'extc.h'
      integer
     & nmpsy,     nepsy,    ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),  nepsy(8), ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
c     # dummy.
      integer ncsft, mxpair
      integer iwsym(nvalwt), icsf0(nvalwt), abpair(mxpair), level(nmot),
     & mapl(2,*)
c     # actual: mapd(2,nfct)
      integer mapd(2,*)
c
c     # local
      integer asym, numa, bsym, numab, absym, i, nnem1,
     & a, b, a1, a2, b1, b2, xoff, woff, ab, nabx, nabw, ncsfx,
     & iver, valw1, valw2, ra, lev, nfcx
      integer xknt(8)
c
      integer    ashift
      parameter( ashift = 1000 )
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    zver,   yver,   xver,   wver
      parameter( zver=1, yver=2, xver=3, wver=4 )
c
c     # nexw(isym,iver) = number of external walks through vertex iver
c     #                   of symmetry isym.
c
      call izero_wr( 32, nexw, 1 )
c
c     # z-walks.
      nexw(1,zver) = 1
c
      do 20 asym = 1, nsym
         numa = nepsy(asym)
c        # y walks.
         nexw(asym,yver) = numa
         do 10 bsym = 1, (asym - 1)
            absym = mult( bsym, asym )
            numab = nepsy(bsym) * numa
c           # off-diagonal w- and x-walk counts are the same.
            nexw(absym,xver) = nexw(absym,xver) + numab
            nexw(absym,wver) = nexw(absym,wver) + numab
10       continue
c        # diagonal w- and x-walk counts.
         nexw(1,xver) = nexw(1,xver) + (numa * (numa - 1)) / 2
         nexw(1,wver) = nexw(1,wver) + (numa * (numa + 1)) / 2
20    continue
c
c     # compute the number of external levels (=external orbitals.)
      nemot = 0
      do 100 i = 1, nsym
         nemot = nemot + nepsy(i)
100   continue
      nnem1 = (nemot * (nemot - 1)) / 2
c
c     # compute the pair addressing arrays for w- and x- walks.
c     # for external walk number (ab), determine the
c     # pairs of levels a and b to satisfy:
c     #
c     #    abnum = abpair( aboff( absym, iver ) + ab )
c     #    a = abnum / ashift
c     #    b = mod( abnum, ashift )
c
c     # first determine aboff(*,*).
c
      call izero_wr( 32, aboff, 1 )
c
c     # z walks.
      nabtot = 0
      aboff(1,zver) = nabtot
      nabtot = nabtot + 1
c
c     # y walks.
      do 200 asym = 1, nsym
         aboff(asym,yver) = nabtot
         nabtot = nabtot + nexw(asym,yver)
200   continue
c
c     # x walks.
      do 300 absym = 1, nsym
         aboff(absym,xver) = nabtot
         nabtot = nabtot + nexw(absym,xver)
300   continue
c
c     # totally symmetric w walks.
      aboff(1,wver) = nabtot
      nabtot = nabtot + nexw(1,wver)
c
c     # remaining w walks (same as the corresponding x walks).
      do 400 absym = 2, nsym
         aboff(absym,wver) = aboff(absym,xver)
400   continue
c
c     # check nabtot
      if ( nabtot .gt. mxpair ) then
         call bummer('setxv: (nabtot-mxpair)=',
     &    (nabtot-mxpair), faterr )
      elseif ( nabtot .ne. (1 + nemot + nnem1 + nexw(1,wver)) ) then
         call bummer('setxv: incorrect nabtot=', nabtot, faterr )
      endif
c
c     # now compute abpair(1:nabtot).
c
      call izero_wr( 8, xknt, 1 )
c
c     # z walks.
      abpair( aboff(1,zver) + 1 ) = 0 * ashift + 0
c
c     # y walks.
c     # this is equivalent to the symmetry dependent expression
c     # for this case.
      do 500 b = 1, nemot
         abpair( aboff(1,yver) + b ) = 0 * ashift + b
500   continue
c
c     # w and x walks.
      woff = aboff( 1, wver )
      a2 = 0
      do 660 asym = 1, nsym
         a1 = a2 + 1
         a2 = a2 + nepsy(asym)
         b2 = 0
c        # off-diagonal symmetry block pairs...
         do 630 bsym = 1, (asym - 1)
            b1 = b2 + 1
            b2 = b2 + nepsy(bsym)
            absym = mult(bsym,asym )
            xoff = aboff(absym,xver) + xknt(absym)
            nabx = 0
            do 620 a = a1, a2
               do 610 b = b1, b2
                  nabx = nabx + 1
                  ab = a * ashift + b
                  abpair( xoff + nabx ) = ab
610            continue
620         continue
            xknt(absym) = xknt(absym) + nabx
630      continue
c        # diagonal symmetry block pairs...
         xoff = aboff(1,xver) + xknt(1)
         nabx = 0
         nabw = 0
         do 650 a = a1, a2
c           # a<>b terms.
            do 640 b = a1, (a-1)
               nabx = nabx + 1
               nabw = nabw + 1
               ab = a * ashift + b
               abpair( xoff + nabx ) = ab
               abpair( woff + nabw ) = ab
640         continue
c           # a=b terms.
            nabw = nabw + 1
            ab = a * ashift + a
            abpair( woff + nabw ) = ab
650      continue
         xknt(1) = xknt(1) + nabx
         woff    = woff    + nabw
660   continue
c
c     # compute the icsf0(*) offset array.
c     # ncsfv(iver) = number of csfs passing through iver.
c     # valw0(iver) = offset for the number of valid walks through iver.
c
      ncsfx = 0
      valw2 = 0
c     # loop over vertices, z, y, x, w.
      do 720 iver = 1, 4
         valw0(iver) = valw2
         ncsfv(iver) = ncsfx
         valw1 = valw2 + 1
         valw2 = valw2 + nvalw(iver)
c        # loop over upper walks for this vertex.
         do 710 i = valw1, valw2
            icsf0(i) = ncsfx
            ncsfx   = ncsfx + nexw( iwsym(i), iver )
710      continue
         ncsfv(iver) = ncsfx - ncsfv(iver)
720   continue
c
c     # check...
      if ( ncsft .ne. ncsfx ) then
         call bummer('setxv: (ncsft-ncsfx)=', (ncsft-ncsfx), faterr )
      elseif ( valw2 .ne. nvalwt ) then
         call bummer('setxv: (valw2-nvalwt)=', (valw2-nvalwt), faterr )
      endif
c
c     # set the csf index breakpoints.
c     # ivcsf1(1:4) = first csf index through each vertex.
c
      ncsfx  = 1
      do 800 iver = 1, 4
         ivcsf1(iver) = ncsfx
         ncsfx        = ncsfx + ncsfv(iver)
800   continue
c
c     # compute mapl(*,*) and mapd(*,*).
      nfcx = 0
      a    = 0
      do 920 asym = 1, nsym
         do 910 ra = 1, nmpsy(asym)
            a = a + 1
            lev = level(a)
c           # frozen core and virtual orbitals have lev<=0.
            if ( lev .gt. 0 ) then
               mapl(1,lev) = asym
               mapl(2,lev) = ra
            elseif ( lev .lt. 0 ) then
               nfcx = nfcx + 1
               mapd(1,nfcx) = asym
               mapd(2,nfcx) = ra
            endif
910      continue
920   continue
c
c     # check the frozen core orbital count for consistency.
      if ( nfcx .ne. nfct ) then
         call bummer('setxv: (nfcx-nfct)=', (nfcx-nfct), faterr )
      endif
c
      return
      end
c deck rdciv
      subroutine rdciv(
     & nlist,  ciuvfl, ncsfmx, csfvec,
     & indcsf, ixcsf,  lenrec, buffer )
c
c  read the selected elements of the CI vector from the sequential
c  vector file and update the histogram arrays.
c
c  input:
c  nlist = listing unit number.
c  ciuvfl = ci vector file.
c  ncsfmx = maximum number of coefficients that can be read.
c  lenrec = record length for file ciuvfl.
c  buffer(1:lenrec) = scratch buffer.
c
c  output:
c  ncsf = number of csf coefficients that were read.
c  csfvec(1:ncsf) = coefficients.
c  indcsf(1:ncsf) = csf indices of the coefficients.
c  ixcsf(1:ncsf)  = sorted order for csf coefficients.
c
c  24-oct-91 written by ron shepard. based in part on
c            ufvin() written by eric stahlberg.
c
       implicit none
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
c     # dummy.
      integer nlist, ciuvfl, ncsfmx, lenrec
      integer indcsf(ncsfmx), ixcsf(ncsfmx)
      real*8 csfvec(ncsfmx), buffer(lenrec)
c
c     # local variables.
      integer nskip, irec, icsf0, icsf, i, low, high, middle, lenx
      real*8 abscsf, bigcsf, smlcsf
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # skip over the header records.
c     # 1:header, 2:header, 3:info(*), 4:title(*), 5:energy(*)
      rewind ciuvfl
      read (ciuvfl)
      read (ciuvfl)
      read (ciuvfl)
      read (ciuvfl)
      read (ciuvfl)
c
c     # skip over the intital vector records.
      nskip = (icsfmn - 1) / lenrec
      do 20 irec = 1, nskip
         read (ciuvfl )
20    continue
c
c     # set icsf0 to force the read of the next record.
      icsf0 = (nskip - 1) * lenrec
c
c     # read in the remaining records, moving coefficients into
c     # csfvec(*) as appropriate.
c
      bigcsf = (-1)
      smlcsf = (10)
      ncsf = 0
      icsf = icsfmn
c     # do while (icsf .le. icsfmx )...
100   if ( icsf .le. icsfmx ) then
c
c        # refill buffer if exhausted.
         if ( (icsf - icsf0) .gt. lenrec ) then
            lenx = min( lenrec, (ncsft - icsf + 1) )
            call seqrbf( ciuvfl, buffer, lenx )
            icsf0 = icsf0 + lenrec
         endif
c
         abscsf = abs( buffer(icsf-icsf0) )
         if ( (abscsf .ge. csfmn) .and. (abscsf .le. csfmx) ) then
c
c           # update the histogram info.
            low = 0
            high = nhist + 1
140         if ( (high - low) .gt. 1 ) then
               middle = (high + low) / 2
               if ( abscsf .lt. chist(middle) ) then
                  low = middle
               else
                  high = middle
               endif
               goto 140
            endif
            knthst(low) = knthst(low) + 1
c
c           # add the coefficient to the list.
            ncsf = ncsf + 1
            if ( ncsf .le. ncsfmx ) then
               csfvec(ncsf) = buffer(icsf-icsf0)
               indcsf(ncsf) = icsf
            endif
c
c           # update bigcsf and smlcsf.
            bigcsf = max( bigcsf, abscsf )
            smlcsf = min( smlcsf, abscsf )
c
         endif
         icsf = icsf + 1
         goto 100
      endif
c
      write(nlist,6010) ncsf, ncsfmx
6010  format(/' rdciv:', i12, ' coefficients were selected.'
     & /' workspace: ncsfmx=', i12)
      write(nlist,*) ncsfmx
c
      if ( ncsf .gt. ncsfmx ) then
c
c        # reset ncsf to the actual number of csfs stored.
         ncsf = ncsfmx
c
         write(nlist,6020)
cjp this is an error in cioverlap mode
         stop 'too little space for CI coefficients'
      endif
6020  format(/' rdciv: *** warning *** '/
     & ' some coefficients were not stored due to space limitations')
c
c     # print out the histogram info.
c
      call prhist
c
      write(nlist,6030) smlcsf, bigcsf
6030  format(/' from the selected csfs,' /
     & 1p,' min(|csfvec(:)|) =', e11.4,
     & 4x, 'max(|csfvec(:)|) =', e11.4 )
c
c     # sort the csf coefficients into decreasing order of magnitude.
c     # on return, ixcsf(i) gives the i'th most important contribution.
c
      do 300 i = 1, ncsf
         ixcsf(i) = i
300   continue
c
      call srtiad( ncsf, csfvec, ixcsf )
c
      return
      end
c deck rdhciv
      subroutine rdhciv(
     & nlist,  ciuvfl, ntitmx, ninfmx,
     & nengmx, ncsft,  ntitle, title,
     & ninfo,  info,   nenrgy, energy,
     & ietype, lenrec )
c
c  read and print the header info from the sequential ci vector file.
c
c  input:
c  nlist = listing unit number.
c  ciuvfl = input sequential vector file number.
c  ntitmx = maximum number of titles to read.
c  ninfmx = maximum info(*) length.
c  nengmx = maximum energy(*) and ietype(*) length.
c  ncsft  = number of csfs in the ci expansion.
c
c  output:
c  ntitle = number of title(*) entries read.
c  title(1:ntitle) = titles.
c  ninfo = number of info(*) entries read.
c  info(1:ninfo) = info array.
c  nenrgy = number of energy(*) and ietype(*) entries read.
c  energy(1:nenrgy) = energies.
c  ietype(1:nenrgy) = energy types.
c  lenrec = record length for the subsequent CI vector.
c
c  24-oct-91 written by ron shepard. based in part on
c            ufvin() written by eric stahlberg.
c
       implicit none
      integer nlist, ciuvfl, ntitmx, ninfmx, nengmx, ncsft,
     & ntitle, ninfo, nenrgy, lenrec
      integer info(ninfmx), ietype(nengmx)
      real*8 energy(nengmx)
      character*80 title(ntitmx)
      integer nroots,selroot,froot,batch,reclen,lenci
      common /ciinfo/ nroots,selroot,froot,batch,reclen,lenci
c
c
c     # local variables.
      integer i, versn
c
c     # curver is the current version number compatible with this
c     # reading routine.
c
      integer    curver
      parameter (curver = 1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # read the first file header.
c
      nroots=0
      froot = -1
c     write(6,*)'rdhciv:  read ciuvfl'
 1000 read (ciuvfl, END=1011) versn, lenrec, lenci
c     write(6,*)'rdhciv: reclen = ',lenrec
      reclen=lenrec
      nroots=nroots+1
  999 format(35('='),'ROOT #',i2,35('='))
  998 format(82('='))
c
      if ( versn .ne. curver ) then
         call bummer('rdhciv: inconsistent version number, vrsion=',
     &    versn, wrnerr )
      endif
c
      if ( lenci .ne. ncsft ) then
         call bummer('rdhciv: (lenci-ncsft)=', (lenci-ncsft), faterr )
      endif
c
c     # general information.
c
      read (ciuvfl ) ninfo, nenrgy, ntitle
c
      ninfo = min (ninfo, ninfmx)
      read (ciuvfl ) (info(i), i=1,ninfo)
c
      ntitle = min (ntitle, ntitmx)
      read (ciuvfl ) (title(i), i=1,ntitle)
c
      if ( nenrgy .gt. nengmx ) then
c        # must have space for all of the energy entries.
         call bummer('rdhciv: (nenrgy-nengmx)=',
     &    (nenrgy-nengmx), faterr )
      endif
c
      read (ciuvfl )
     & (ietype(i), i=1,nenrgy),
     & (energy(i), i=1,nenrgy)
c
c  # skip sovref
      read (ciuvfl )
c  # skip tciref
      read (ciuvfl)
c  # skip mxov,e(i)
      read (ciuvfl)

      if (nroots.eq.1 .and. info(4).eq.1)  then
            froot = info(2)
cmd         nroots = -1
c  print all roots 1...froot
            nroots = froot
cmd
      endif
c
c    # output to stdout
c
cjp
      write(6,*) 'test nroots froot ',nroots,froot
cjp
      if (froot.ne.-1)   then
       write(nlist,999) froot
      else
       write(nlist,999) nroots
      endif
      write(nlist,6010) (title(i), i=1,ntitle)
6010  format(/' rdhciv: CI vector file information:'/(1x,a))
c
      write(nlist,6020) lenrec, lenci, ninfo, nenrgy, ntitle
6020  format(/' lenrec =', i8, ' lenci =', i10, ' ninfo =', i3,
     & ' nenrgy =', i3, ' ntitle =', i3)
c
      write(nlist,6030) info(1),info(2),info(3),info(5)
6030  format(/' Max. overlap with ref vector #', (1x,i8),
     &       /' Valid ci vector #', (1x,i8),
     &       /' Method:', (1x,i8,1x,i8),'% overlap')
c
      call sifpre( nlist, nenrgy, energy, ietype )
      write(nlist,998)
c
      if (info(4).eq.0) goto 1000
1011  continue
      return
      end


c deck prhist
      subroutine prhist
c
c  print the histogram.
c
       implicit none
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
c     # local.
      integer maxh, i, iscale, num, n, ntot
c
c     # nprmx = the maximum histogram length in characters.
      integer    nprmx
      parameter( nprmx = 40 )
c
      call prselp( 'histogram parameters:' )
c
      write(nlist,6010) nhist, fhist
c
c     # determine iscale, so that no more than nprmx '*'s are printed.
c
      maxh = 0
      do 10 i = 1, nhist
         maxh = max( maxh, knthst(i) )
10    continue
      iscale = max( 1, (maxh + nprmx - 1) / nprmx )
      write(nlist,6020) iscale
c
      ntot = 0
      do 20 i = 1, (nhist - 1)
         num = (knthst(i) + iscale - 1) / iscale
         write(nlist,6030) chist(i+1), chist(i), knthst(i),
     &    ('*', n=1,num)
         ntot = ntot + knthst(i)
20    continue
c
      write(nlist,6040) ntot, ncsf
c
      return
6010  format(' nhist =', i4, ' fhist =', f8.5 )
6020  format(/
     & 4x,'cmin',16x,'cmax', 8x, 'num  ''*''=', i6, ' csfs.'/
     & 1x,10('-'), 10x, 10('-'), 3x, 5('-'), 1x, 4(9('-'),'|'))
6030  format(1x,1pe10.4, ' <= |c| <', e11.4, i8, 1x, 40a1)
6040  format(34x,5('-'), 1x, 4(9('-'),'|') /
     & 18x, 'total read = ', i12, ' total stored =', i12 )
      end
c deck prselp
      subroutine prselp( title )
c
c  print the current selection and histogram parameters.
c
       implicit none
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      character*(*) title
c
      write(nlist,6010)
     & title,
     & csfmn, csfmx, fhist,
     & nhist, icsfmn, icsfmx, ncsft, ncsf
6010  format( 1p / 1x, a /
     & ' csfmn =', e11.4, ' csfmx =', e11.4, ' fhist =', e11.4/
     & ' nhist =', i4,' icsfmn =', i12, ' icsfmx =', i12,
     & ' ncsft =', i12, ' ncsf =', i12 )
c
      return
      end
c deck reseth
      subroutine reseth
c
c  reset the histogram parameters.
c
c  input:
c  nhist = maximum number of histogram breaks (=bins+1)
c  csfmx = upper bound.
c  csfmn = lower bound.
c  fhist = histogram bin factor.
c
c  output:
c  nhist = actual number of histogram breaks (<= input value).
c  chist(*) = histogram break points.
c  knthst(1:nhist) = initialized to zero.
c
c  11-nov-91 written by ron shepard.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
c
      integer i
c
      chist(1)     = csfmx
      do 10 i = 2, (nhist - 1)
         chist(i) = chist(i - 1) * fhist
         if ( chist(i) .le. csfmn ) then
c           # reset nhist and exit loop.
            write(nlist,6200) nhist, i
            nhist = i
            goto 11
         endif
10    continue
11    continue
      chist(nhist) = csfmn
c
      call izero_wr( nhist, knthst, 1 )
c
      ncsf = 0
c
      write(nlist,6100) csfmn, csfmx, fhist, nhist
6100  format(1p/' updated histogram parameters:'
     & /' csfmn =', e11.4, ' csfmx =', e11.4, ' fhist =', e11.4,
     & ' nhist =', i4 )
6200  format(/' nhist =', i4,' is being reset to nhist =', i4
     & /' for consistency with the current csfmn, csfmx, and fhist.')
c
      return
      end



      subroutine rdcivnew(
     & nlist,  ciuvfl, ncsfmx, csfvec,
     & indcsf, ixcsf,  lenrec, buffer )
c
c  read the selected elements of the CI vector from the sequential
c  vector file and update the histogram arrays.
c
c  input:
c  nlist = listing unit number.
c  ciuvfl = ci vector file (civout).
c  civfl  = ci vector file (civfl)
c  ncsfmx = maximum number of coefficients that can be read.
c  lenrec = record length for file ciuvfl.
c  buffer(1:lenrec) = scratch buffer.
c
c  output:
c  ncsf = number of csf coefficients that were read.
c  csfvec(1:ncsf) = coefficients.
c  indcsf(1:ncsf) = csf indices of the coefficients.
c  ixcsf(1:ncsf)  = sorted order for csf coefficients.
c
c  24-oct-91 written by ron shepard. based in part on
c            ufvin() written by eric stahlberg.
c
       implicit none
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c.....include 'ihistc.h'
c     # nhstmx = maximum number of histogram breaks.
c     # nhistd = default number of histogram breaks.
c     #          2 <= nhistd <= nhstmx
c     # dfhist = default histogram factor.
c
      integer    nhstmx,       nhistd
      parameter( nhstmx = 128, nhistd = 20 )
      real*8     dfhist
      parameter( dfhist = 5d-1 )
      real*8           csfmn,  csfmx,  fhist,  chist
      integer
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst
      common /ihistc/  csfmn,  csfmx,  fhist,  chist(nhstmx),
     & nhist,  icsfmn, icsfmx, ncsft,  ncsf,   knthst(nhstmx)
c.....end of ihistc
      integer   iunits, nflmax
      parameter( nflmax=20 )
      common /cfiles/ iunits(nflmax)
       integer civfl
       equivalence (civfl, iunits(5))
      integer nroots,logrec,froot,batch,reclen,lenci
      common /ciinfo/ nroots,logrec,froot,batch,reclen,lenci
c
c     # dummy.
      integer nlist, ciuvfl, ncsfmx, lenrec
      integer indcsf(ncsfmx), ixcsf(ncsfmx)
      real*8 csfvec(ncsfmx), buffer(lenrec)
      integer iversn, ilenrec,ilenci,nrec36
      integer*8 offset
c
c     # local variables.
      integer nskip, irec, icsf0, icsf, i, low, high, middle, lenx
      real*8 abscsf, bigcsf, smlcsf,nnorm
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # skip over the header records.
c     # 1:header, 2:header, 3:info(*), 4:title(*), 5:energy(*)
c once more read the data from the header file
c     rewind ciuvfl
c     read (ciuvfl) iversn, ilenrec, ilenci
c initialize history array
      call izero_wr(nhstmx,knthst,1)
c
c
c     # skip over the intital vector records.
      nskip = (icsfmn - 1) / lenrec
c
c     # set icsf0 to force the read of the next record.
      icsf0 = (nskip - 1) * lenrec
c
c     # read in the remaining records, moving coefficients into
c     # csfvec(*) as appropriate.
c
      nnorm=0.0d0
      bigcsf = (-1)
      smlcsf = (10)
      ncsf = 0
      icsf = icsfmn
c     nrec36=ilenrec
c      lenci=ilenci
      nrec36 = reclen
c     # do while (icsf .le. icsfmx )...
100   if ( icsf .le. icsfmx ) then
c
c        # refill buffer if exhausted.
         if ( (icsf - icsf0) .gt. lenrec ) then
            lenx = min( lenrec, (ncsft - icsf + 1) )
c      write(*,*) 'civfl,nrec36,logrec,lenci,icsf0,lenx=',
c    .             civfl,nrec36,logrec,lenci,icsf0,lenx
c      call getvec(civfl,nrec36,logrec,lenci,buffer,icsf,lenx)
c      
       offset=lenci*(logrec-1)+icsf
       call getputvec('civfl',buffer,lenx,offset,'openrd')
c           call seqrbf( ciuvfl, buffer, lenx )
            icsf0 = icsf - 1
         endif
c
         abscsf = abs( buffer(icsf-icsf0) )
         nnorm=nnorm+abscsf*abscsf
         if ( (abscsf .ge. csfmn) .and. (abscsf .le. csfmx) ) then
c
c           # update the histogram info.
            low = 0
            high = nhist + 1
140         if ( (high - low) .gt. 1 ) then
               middle = (high + low) / 2
               if ( abscsf .lt. chist(middle) ) then
                  low = middle
               else
                  high = middle
               endif
               goto 140
            endif
            knthst(low) = knthst(low) + 1
c
c           # add the coefficient to the list.
            ncsf = ncsf + 1
            if ( ncsf .le. ncsfmx ) then
               csfvec(ncsf) = buffer(icsf-icsf0)
               indcsf(ncsf) = icsf
            endif
c
c           # update bigcsf and smlcsf.
            bigcsf = max( bigcsf, abscsf )
            smlcsf = min( smlcsf, abscsf )
c
         endif
         icsf = icsf + 1
         goto 100
      endif
c
      write(nlist,6010) ncsf, ncsfmx
6010  format(/' rdcivnew:', i12, ' coefficients were selected.'
     & /' workspace: ncsfmx=', i12)
       write(nlist,*) 'ncsfmx=',ncsfmx
c
      if ( ncsf .gt. ncsfmx ) then
c
c        # reset ncsf to the actual number of csfs stored.
         ncsf = ncsfmx
c
         write(nlist,6020)
cjp this is an error in cioverlap mode
         stop 'too little space for CI coefficients'
      endif
6020  format(/' rdcivnew: *** warning *** '/
     & ' some coefficients were not stored due to space limitations')
c
c     # print out the histogram info.
c
      call prhist
c
      write(nlist,6030) smlcsf, bigcsf
6030  format(/' from the selected csfs,' /
     & 1p,' min(|csfvec(:)|) =', e11.4,
     & 4x, 'max(|csfvec(:)|) =', e11.4 )
c
c     # sort the csf coefficients into decreasing order of magnitude.
c     # on return, ixcsf(i) gives the i'th most important contribution.
c
      do 300 i = 1, ncsf
         ixcsf(i) = i
300   continue
      write(nlist,*) 'norm=',nnorm
c
      call srtiad( ncsf, csfvec, ixcsf )
c
      return
      end
      subroutine  wtlabel(slabel,nsym,fmt,buf)
      implicit none
      integer nsym
      integer i
      character*4 slabel(nsym),buf(nsym)
       character*(*) fmt
      do 10 i = 1, nsym
         write( slabel(i), '(a4)' ) buf(i)
10    continue
      return
      end

      subroutine symwlk_noso(
     . niot,   nsym,   nrow,   nrowmx,
     . nwalk,  l,      y,      row,
     . step,   csym,   wsym,   arcsym,
     . ytot,   xbar,   ssym,
     . mult,   limvec, ncsft,
     . nviwsm, ncsfsm,  b, numv1  )
c
c  this routine calculates the symmetry of all the valid internal
c  walks and computes csf counts.
c
c  output:
c  csym(*) = mult( internal_walk_symmetry, ssym ) for
c            the valid z,y,x,w-walks.
c  ncsft = total number of csfs for this orbital basis and drt.
c  nviwsm(1:4,1:nsym) = number of valid internal walks of each symmetry
c                       through each vertex.
c  ncsfsm(1:4,1:nsym) = number of csfs passing through each vertex
c                       indexed by the internal symmetry.
c
c  07-jun-89  skip-vector based walk generation. rls
c
      implicit none
c====>Begin Module SYMWLK                 File cidrt2.f
c---->Makedcls Options: All variables
c
c     Parameter variables
c
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
c
c     Argument variables
c
c
      INTEGER             ARCSYM(0:3,0:NIOT),       B(*),        CSYM(*)
      INTEGER             L(0:3,NROWMX,3),          LIMVEC(*)
      INTEGER             MULT(8,8),   MULTMX,      NCSFSM(4,NSYM)
      INTEGER             NCSFT,       NIOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVIWSM(4,NSYM)
      INTEGER             NWALK,       ROW(0:NIOT)
      INTEGER             SSYM,        STEP(0:NIOT)
      INTEGER             WSYM(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3),          YTOT(0:NIOT)
c
c
c     Local variables
c
      CHARACTER           WXYZ(4)
c
      INTEGER             CLEV,        I,           IMUL,        ISSYM
      INTEGER             ISTEP,       ISYM,        MUL,         NLEV
      INTEGER             NUMV1,       NVALWT,      ROWC,        ROWN
      INTEGER             VER,         VER1,        VERS(4)
c
c====>End Module   SYMWLK                 File cidrt2.f
      data vers/1,2,3,3/
      data wxyz/'z','y','x','w'/
c
c
c     # limvec(*) in skip-vector form.
c
      nvalwt = 0
c
      call izero_wr( 4*nsym, nviwsm, 1 )
c
c     # loop over z, y, x, and w vertices.
c
      do 200 ver = 1, 4
         ver1 = vers(ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
         wsym(0) = 1
c
         do 50 i = 0, niot
            step(i) = 4
50       continue
c
100      continue
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)go to 180
            clev = clev-1
            goto 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         row(nlev) = rown
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
         if( limvec( ytot(nlev)+1 ) .ge. xbar(rown,ver1) )goto 100
         wsym(nlev) = mult( arcsym(istep,clev), wsym(clev) )
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         endif
c        # a complete valid walk has been generated.
         isym = wsym(nlev)
         issym = mult(isym,ssym)
         nvalwt = nvalwt+1
         csym(nvalwt) = issym
         nviwsm(ver,isym) = nviwsm(ver,isym) + 1
c
         go to 100
c
180      continue
c        # finished with walk generation for this vertex.
200   continue
c
c     # check the valid upper walk count...
      if(numv1.ne.nvalwt)
     . call bummer('symwlk: numv1-nvalwt=', numv1-nvalwt,0)
c
      return
c
6100  format(1x,a,':',(8i8))
      end

*@ifdef shavittgraph
*
        subroutine comp_intw(icsf0,intweight,nodeweight,arcweight,
     .               heap,valw,space,ncsft,nvalwt,nrow,nimot,
     .               nvalw,logrec,wunit)
        implicit none
        integer space,ncsft,nvalwt,logrec
        integer icsf0(nvalwt),valw(nvalwt)
        real*8 intweight(nvalwt),heap(space),ddot_wr,dasum_wr
        real*8 nodeweight(nrow), arcweight(0:3,nrow) 
        integer i,icsf1,icsf2,n,offset,iwalk,nimot
        integer siwalk,eiwalk,sicsf,eicsf,nrow,irow
        integer iwalkoff,idrt,iver,nvalw(4),wunit
c       real*16  norm,temp
*
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=1023 )
*
      integer          l,               y,               syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3), y(0:3,nrowmx,3), syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
*
*
*
        offset=ncsft*(logrec-1)+1
         intweight(1:nvalwt)=0.0d0
      
        siwalk=1
        sicsf=0
c       norm=0.0q0
 1000   continue
c
c        get maximum available space
c
        do iwalk=siwalk,nvalwt
c         last csf of iwalk-1  = icsf0(iwalk)
          if (icsf0(iwalk)-sicsf .gt. space) then
c           eiwalk = last valid walk to be included
c           eicsf  = last csf+1 to be included 
            eiwalk=iwalk-2
            eicsf=icsf0(eiwalk+1)
            exit
          elseif (iwalk.eq.nvalwt) then
            eiwalk=nvalwt
            eicsf=ncsft
          endif
        enddo
          write(6,1008) siwalk,eiwalk,sicsf+1,eicsf
          write(wunit,1008) siwalk,eiwalk,sicsf+1,eicsf
 1008     format('space sufficient for valid walk range ',2i11,/,
     .           '               respectively csf range ',2i11)
c
c          read and analyze ci vector
c        
         call getputvec('civfl',heap,eicsf-sicsf,offset+sicsf,
     .                 'openrd') 
 
c         do i=1,eicsf-sicsf
c           temp=heap(i)
c           norm=norm+temp*temp
c         enddo
*
*@ifdef debug
*         if (sicsf.eq.0) then
*           write(wunit,*) ' THE FIRST 1516 CSFs IN FULL PREC'  
*           write(wunit,'(i8,f20.15)') (i,heap(i),i=1,1516) 
*         endif
*@endif
*
         icsf1=icsf0(siwalk)+1
         do iwalk=siwalk,eiwalk-1
          icsf2=icsf0(iwalk+1)+1
          n=icsf2-icsf1
          intweight(iwalk)=
     .       ddot_wr(n,heap(icsf1-sicsf),1,heap(icsf1-sicsf),1)
          if (intweight(iwalk).gt.1.d-6) then 
            write(wunit,1009) iwalk,icsf1,icsf1+n-1,
     .                      intweight(iwalk),sicsf
          endif
          icsf1=icsf2
        enddo
         if (eiwalk.eq.nvalwt) then
           icsf2=ncsft+1
         else
           icsf2=icsf0(eiwalk+1)+1
         endif 
         n=icsf2-icsf1
         intweight(eiwalk)=
     .       ddot_wr(n,heap(icsf1-sicsf),1,heap(icsf1-sicsf),1)
         if (intweight(eiwalk).gt.1.d-6) then 
           write(wunit,1009) eiwalk,icsf1,icsf1+n-1,
     .          intweight(eiwalk),sicsf
         endif 
1009   format(' ivwalk:',i10,' (',i10,':',i10,')=',f12.8, 'sicsf=',i10)
         siwalk=eiwalk+1
         sicsf = eicsf 
         if (eiwalk.lt.nvalwt) goto 1000
*
       write(wunit,*) 'Norm of intweight=',dasum_wr(nvalwt,intweight,1)
c      write(wunit,*) 'Norm at quadruple precision=',norm
*
c
c        # analysis in terms of node and arcweights
c
         nodeweight(1:nrow)=0.0d0
         arcweight(0:3,1:nrow)=0.0d0
         iwalk=0
         do iver=1,4
          idrt = min( iver, 3 )
          do n=1,nvalw(iver)
             iwalk=iwalk+1 
            call arcnodeweight 
     .         (valw(iwalk), iver, nimot, l(0,1,idrt),y(0,1,idrt),
     .          intweight(iwalk),nodeweight,arcweight,nrow,wunit)
          enddo
         enddo
*
         write(wunit,*) '---------------NODE WEIGHTS-----------------'
         do irow=1,nrow
          write(wunit,1011) irow,nodeweight(irow)
         enddo
 1011    format(' node #',i4,' weight=',f12.6)
         write(wunit,*) '---------------ARC  WEIGHTS-----------------'
         do irow=1,nrow
          write(wunit,1012) irow,arcweight(0:3,irow)
         enddo
 1012    format(' node #',i4,' step 0123',4f12.6)
*
         return        
        end
*
      subroutine arcnodeweight(
     & iwalk,  iver,   niot,   l,
     & y,    weight,nodeweight,arcweight,nrow,wunit)
c
c  generate the step vector for a given internal walk index.
c
c input:
c iwalk = internal walk number.
c iver = initial vertex.
c        (1 for z-walks, 2 for y, 3 for x, and 4 for w walks)
c niot = number of internal orbitals.
c l(0:3,*) = cidrt chaining indices.
c y(0:3,*) = arc weights. (for 3-index drt, 1 piece only).
c
c output arguments:
c step(0:niot-1) = generated step vector.
c
c  08-jun-89 iver added for non-z-walks. -rls
c  07-may-89 written by ron shepard.
c
       implicit none
c     # dummy:
      integer iwalk, iver, niot,nrow,wunit
      integer l(0:3,*), y(0:3,*)
      real*8 arcweight(0:3,nrow) 
      real*8 nodeweight(nrow),weight 
c
c     # local:
      integer ytot, rowc, nlev, clev, istept, istep, rown, ytemp
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      rown  = 0
      istep = 0
      ytemp = 0
c
*@ifdef debug
*      write(wunit,*) '-------- iwalk=',iwalk,'-----------------'
*@endif
      ytot  = iwalk - 1
      rowc  = iver
      do 20 nlev = 1, niot
         clev = nlev - 1
         do 10 istept = 0, 3
            istep = istept
            rown = l(istep,rowc)
            if ( rown .eq. 0 ) go to 10
            ytemp = ytot - y(istep,rowc)
            if ( ytemp .ge. 0 ) go to 11
10       continue
c        # ...no valid exit from the loop.
         call bummer('gnstep: ytemp=',ytemp,faterr)
11       continue
         ytot = ytemp
         arcweight(istep,rowc)=arcweight(istep,rowc)+weight
*@ifdef debug
*         write(wunit,119) weight,rowc,nodeweight(rowc)
*119      format('adding ',f8.6,' to node',i4,'old nodew=',f8.6)
*@endif
         nodeweight(rowc)=nodeweight(rowc)+weight
         clev = nlev - 1
         rowc = rown
20    continue
      nodeweight(rowc)=nodeweight(rowc)+weight
c
c     # should probably return an error code.  for now just stop.
      if ( ytot .ne. 0 ) call bummer('gnstep: ytot=',ytot,faterr)
c
      return
      end
*
*@endif
