!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C Revision 1.1.1.1  2001/10/07 02:36:23  shepard
C archive of COLUMBUS 5.8
C
C Revision 1.4  1997/07/27 14:22:59  michal
C coments + istep.eq.3 removed
C
C Revision 1.3  1997/07/27 14:02:22  michal
C small changes for alpha
C
C Revision 1.2  1997/03/17 14:31:58  michal
C new structure of the restart file introduced
C
C Revision 1.1  1997/01/22 11:04:36  michal
C Initial revision
C
C Revision 1.7  1997/01/22 11:00:07  michal
C *** empty log message ***
C
C Revision 1.6  1997/01/13 15:52:31  michal
C corrected an error in wloop
C
C Revision 1.5  1996/12/10 15:17:14  michal
C full working version for at least CH2 (up to all orbitals active and C
C
C Revision 1.4  1996/11/27 12:58:59  michal
C safety save, probably not different to 1.3
C
C Revision 1.3  1996/10/14 09:09:48  michal
C primitive version for two different drt table (also with different sym
C
C Revision 1.2  1996/10/10 14:00:46  michal
C invariant extesion for 2 drt files
C
C Revision 1.1  1996/10/10 13:28:45  michal
C Initial revision
C
C Revision 5.0  1996/06/26 12:57:34  calderon
C Columbus 5.0
C
C Revision 1.1  1996/04/23 15:51:49  calderon
C Initial revision
C
cmcuft part=1 of 1.  mcscf formula tape program.
cversion=4.1 last modified: 24-apr-92
cversion 5.0
cc deck mcuft
      program transft
c***********************************************************************
c
c   the following computer programs contain work performed
c   partially or completely by the argonne national laboratory
c   theoretical chemistry group under the auspices of the office
c   of basic energy sciences, division of chemical sciences,
c   u.s. department of energy, under contract w-31-109-eng-38.
c
c   these programs may not be (re)distributed without the
c   written consent of the argonne theoretical chemistry group.
c
c   since these programs are under development, correct results
c   are not guaranteed.
c
c***********************************************************************
c
c  mcscf formula tape generation.
c  written by ron shepard.
c
c***********************************************************************
c
c  for TRANSITION DIPOLE MOMENT calculations
c  adapted by: Dallos Michal (University Vienna)
c
c  The program generates a formula tape based on
c   two DRTs which may differ in the space symmetry.
c   In this case the bra is from DRT1 and ket from DRT2
c   The input is interactive + based on the 'restart' file of mcscf
c
c***********************************************************************
c  flags for machine dependent code:
c  vax       vax code.
c  cray      generic cray code.
c  crayctss  ctss specific cray code.
c  unicos    unicos specific cray code.
c  sun       sun code.
c  alliant   alliant code.
c  titan     titan code.
c
       implicit none
c     # nrowmx and nwlkmx should be consistent with ft packing in wloop.
c
      integer   nrowmx,        nwlkmx,         namomx
      parameter(nrowmx=2**10-1, nwlkmx=2**20-1, namomx=99)
c
      integer   ntab,   maxb
      parameter(ntab=56,maxb=19)
      real*8        tab
      common /ctab/ tab(ntab,0:maxb)
c
      integer   nst,    nst5
      parameter(nst=28, nst5=5*nst)
      integer      pt
      common /cpt/ pt(0:3,0:3,nst5)
c
      integer      nex,       bcasex,         kcasex
      common /cex/ nex(-2:3), bcasex(6,-2:3), kcasex(6,-2:3)
c
      integer
     & b,            l,             y,                   xbar,
     & xp,           z,             arcsym,
     & nj,           njskp,nwalk
      common /cdrt/
     & b(nrowmx),    l(0:3,nrowmx), y(8,0:3,nrowmx),     xbar(8,nrowmx),
     & xp(8,nrowmx), z(8,nrowmx),   arcsym(0:3,0:namomx),
     & nj(0:namomx), njskp(0:namomx),nwalk
cmd
      integer
     & b2,            l2,             y2,                   xbar2,
     & xp2,           z2,             arcsym2,
     & nj2,           njskp2,nwalk2
      common /cdrt_2/
     & b2(nrowmx),    l2(0:3,nrowmx), y2(8,0:3,nrowmx),xbar2(8,nrowmx),
     & xp2(8,nrowmx), z2(8,nrowmx),   arcsym2(0:3,0:namomx),
     & nj2(0:namomx), njskp2(0:namomx),nwalk2
cmd

      integer        limvec,         rev
      common /cdrt2/ limvec(nwlkmx), rev(nwlkmx)
cmd
      integer        limvec2,         rev2
      common /cdrt2_2/ limvec2(nwlkmx), rev2(nwlkmx)
c
      integer ssym1,ssym2
      logical symdiff
      common /ssym/ ssym1,ssym2,symdiff
c
      integer maxnavst
cmd

c
      real*8  v
      logical qeq
      integer          range,           extbk,
     & brow,           krow,            bsym,            ksym,
     & yb,             yk
      common /cstack/
     & v(3,0:namomx),  range(0:namomx), extbk(0:namomx), qeq(0:namomx),
     & brow(0:namomx), krow(0:namomx),  bsym(0:namomx),  ksym(0:namomx),
     & yb(8,0:namomx), yk(8,0:namomx)
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
      logical      qdiag
      integer             nact, numv, lowlev, hilev, code,    mo,    nmo
      common /cli/ qdiag, nact, numv, lowlev, hilev, code(4), mo(4), nmo
c
      integer
     & st1,      st2,     st3,
     & st4,      st4p,    st5,     st6,     st7,    st8,    st10,
     & st11d,    st11,    st12,    st13,
     & st14
      common /cst/
     & st1(7),   st2(7),  st3(7),
     & st4(5),   st4p(5), st5(5),  st6(5),  st7(5), st8(5), st10(5),
     & st11d(3), st11(3), st12(3), st13(3),
     & st14(1)
c
      integer
     & db1,       db2,       db3,
     & db4,       db4p,      db5,      db6,      db7,    db8,
     & db10,
     & db11,      db12,      db13,
     & db14
      common /cdb/
     & db1(7,2),  db2(7,2),  db3(7,2),
     & db4(5,2),  db4p(5),   db5(5),   db6(5,2), db7(5), db8(5,2),
     & db10(5),
     & db11(3,2), db12(3,3), db13(3),
     & db14(1,2)
c
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
cmd
c
      real*8          vtran,        vcoeff
      integer                                  vtpt
      common /cvtran/ vtran(2,2,4), vcoeff(2), vtpt
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer   lbft
      parameter(lbft=2047)
      real*8        ftbuf
      integer                    kntft, nloop, nftb
      common /cftb/ ftbuf(lbft), kntft, nloop, nftb
c
      character*80 title
      character*80 fdrt(2),fdft,foft,fname
c
c     # timer operation parameters...
      integer   tmin0
      parameter(tmin0=0)
      integer   tminit,  tmprt,  tmrein,  tmclr,  tmsusp,  tmresm
      parameter(tminit=1,tmprt=2,tmrein=3,tmclr=4,tmsusp=5,tmresm=6)
c
      integer imcuft, iclock, ndot, lenbuf, ncsf,  nela,
     & nelt, smult, nrow
c
c    variables for restart header read
      integer ibuf,fbuf(8),imd,jmd
      character*80 chbuf(50)
      logical lbuf
c
      integer maxnst,mxavst
      parameter (maxnst=8)
      parameter (mxavst = 50)
      real*8 wavst(maxnst,mxavst),heig(maxnst,mxavst)
      integer ncsf_f(maxnst),navst(maxnst),nstres,ntitle2
c
      integer iciset1,iciset2,iorder1,iorder2,nfile1,nfile2
      integer istep,last,icount,ist
c
      character*80 yesno
c
c
c     # open the listing file.
c     # use fdrt for temporary translation workspace.
c
c     fdrt = 'mcuftls'
c     call trnfln( 1, fdrt )
c     open(unit=nlist,file=fdrt,status='unknown',
c    & access='sequential',form='formatted')
c
      write(nlist,6010)
6010  format(/' program transft'
     & //' formula tape construction for mcscf wave functions'
     & /' using an index driven graphical unitary group approach.'
     & //' programmed by: ron shepard'
     & //' version date: 24-apr-92'/)
c
      call who2c( 'TRANSFT', nlist )
      call headwr (nlist,'TRANSFT','5.5     ')
c
c  version log:
c  24-apr-92 who2c() call added. minor cleanup. -rls
c  10-jan-91 version=2 mcdrtfl changes. -rls
c  01-apr-89 sun, alliant, titan port. general cleanup. -rls
c  10-jul-88 unicos version (rls).
c  14-mar-86 master version with vax/cray machine dependent code (rls).
c  03-jun-85 conversion to cray (rls).
c  07-aug-84 original vax version written at anl by rls.
c***********************************************************************
c
c    initialize for different run type
c
      istep = 1
      last = 0
c
c    open restart file and read the header
c
      fname = 'restart'
      call trnfln( 1, fname )
      open(unit=nrestp,file=fname,status='unknown',
     &  form='unformatted')
c
      read(nrestp)ibuf,ibuf,ibuf,ibuf,ibuf,nstres,
     &  (ncsf_f(imd),imd=1,nstres),lbuf,ntitle2,
     &  fbuf,fbuf,(chbuf(imd),imd=1,ntitle2),
     &  (navst(imd),imd=1,nstres),maxnavst,
     &  ((wavst(imd,jmd),imd=1,nstres),jmd=1,maxnavst),
     &  ((heig(imd,jmd),imd=1,nstres),jmd=1,maxnavst)
c
c    #   check the No. of different states do not exceedes maximum
      if (nstres .gt. maxnst) call bummer(
     & 'Number of DRT exceedes maximum = ',maxnst,faterr)
c
c    #   check the No. of states in one DRT do not exceedes maximum
      if (maxnavst .gt. mxavst) call bummer(
     & 'Number of states in one DRT exceedes maximum = ',maxnavst,
     & faterr)
c
c    write the header informations to the user output
c
      write(nlist,'(//,7x,"CI INFORMATIONS",/)')
      write(nlist,'(3x,"Number of different sets of CI vectors:",i3)')
     &  nstres
      write(nlist,'(4x,"(defined through different DRT)"))')
      write(nlist,9000)
9000  format(/,3x,"CI set",3x,"No of CI vectors",3x,"No of csf",/)
      do imd=1,nstres
      write(nlist,'(4x,i2,10x,i3,13x,i6)')imd,navst(imd),ncsf_f(imd)
      enddo
c
      write(nlist,'(//,3x,"Would you like to list state energies ?")')
1500  read(nin,'(a)')yesno
      write(*,*)yesno
      if ((yesno.eq.'yes').or.(yesno.eq.'y')) then
        icount = 1
        do ist = 1,nstres
        write(nlist,'(/,"Energies from DRT No.:",i3,/)')ist
           do imd=1,navst(ist)
           write(nlist,'(i3,3x,f15.8)')imd,heig(ist,imd)
           enddo
        enddo
      elseif((yesno.eq.'no').or.(yesno.eq.'n')) then
        go to 1510
      else
        go to 1500
      endif
1510  write(nlist,
     & '(//,3x,"Input the set number for the ci vector No.1")')
      read (nin,*)iciset1
      if (iciset1.gt.maxnst) call bummer(
     & 'Set number exceeds the set dimenstion =',maxnst,faterr)
      if (navst(iciset1).le.0) call bummer(
     &  'Notexisting CI set No',iciset1,faterr)
      write(nlist,
     & '(3x,"Input the sequence number for the ci vector No.1")')
      read (nin,*)iorder1
      if((iorder1.gt.navst(iciset1)).or.(iorder1.le.0)) call bummer(
     &  'In this set there is no vector No',iorder1,faterr)
c
      write(nlist,
     & '(3x,"Input the set number for the ci vector No.2")')
      read (nin,*)iciset2
      if (iciset2.gt.maxnst) call bummer(
     & 'Set number exceeds the set dimenstion =',maxnst,faterr)
      if (navst(iciset2).le.0) call bummer(
     &  'Notexisting CI set No',iciset2,faterr)
      write(nlist,
     & '(3x,"Input the sequence number for the ci vector No.2")')
      read (nin,*)iorder2
      if((iorder2.gt.navst(iciset2)).or.(iorder2.le.0)) call bummer(
     &  'In this set there is no vector No',iorder2,faterr)
c
c    test for identical ci vectorts
c
cmd   if ((iciset1.eq.iciset2).and.(iorder1.eq.iorder2)) then
c     call bummer(
c    & 'Transition moment between identical states has no sense!',
c    & 0,faterr)
cmd   endif
c
c    write the CI vector informations ti 'transci' file
c     This file will be read  by 'trans.x' program
c
      fname = 'transci'
      call trnfln( 1, fname )
      open(unit=trsci,file=fname,status='unknown',
     &  form='formatted')
c
      write(trsci,'(i3,2x,"# set No for CI vec 1")')iciset1
      write(trsci,'(i3,2x,"# sequence No for CI vec 1")')iorder1
      write(trsci,'(i3,2x,"# set No for CI vec 2")')iciset2
      write(trsci,'(i3,2x,"# sequence No for CI vec 2")')iorder2
c
      close(trsci)

c
c    test the existence of the needed DRT files and
c     define the corect names
c    # first drt file
      if ((iciset1.eq.1).and.(nstres.eq.1)) then
         fdrt(1)= 'mcdrtfl'
      elseif(iciset1.ge.10) then
         write(fdrt(1),'(a8,i2)')'mcdrtfl.',iciset1
      else
         write(fdrt(1),'(a8,i1)')'mcdrtfl.',iciset1
      endif
      inquire(file=fdrt(1),exist= lbuf)
      if (.not.lbuf) call bummer(
     & 'DRT file do not exist for the co set No ',iciset1,faterr)
      call trnfln( 1, fname )
c
c    # second drt file
      if (iciset1.ne.iciset2) then
         if(iciset2.ge.10) then
            write(fdrt(2),'(a8,i2)')'mcdrtfl.',iciset2
         else
            write(fdrt(2),'(a8,i1)')'mcdrtfl.',iciset2
         endif
         inquire(file=fdrt(2),exist= lbuf)
         if (.not.lbuf) call bummer(
     &    'DRT file do not exist for the ci set No ',iciset1,faterr)
      endif
c
c     define the proper FT name
c     1-st for normal run
c     2-nd for different space symetry run
c
500   continue
c
      if (istep.eq.1) then
c    #  normal FT construction
      fdft = 'mcdftfl'
      foft = 'mcoftfl'
      nfile1 = 1
      nfile2 = 2
c    #  exchange bra and ket vector
      elseif(istep.eq.2) then
      fdft = 'mcdftfl.2'
      foft = 'mcoftfl.2'
      nfile1 = 2
      nfile2 = 1
      endif



      call ibummr(nlist)
      call timer( ' ', tmin0,  imcuft, nlist )
      call timer( ' ', tminit, iclock, nlist )
c
c     # initialize segment value arrays.
c
      call intab
c
c     # initialize necessary stack arrays.
c
      call istack
c
c     # open drt file, diagional ft file, and off-diagonal ft file.
c
cmd
      call trnfln( 1, fdrt(nfile1) )
      open(unit=ndrt1,file=fdrt(nfile1),status='old',
     & access='sequential',form='formatted')
c
      if ((iciset1.ne.iciset2).and.(nfile1.ne.nfile2)) then
         call trnfln( 1, fdrt(nfile2) )
         open(unit=ndrt2,file=fdrt(nfile2),status='old',
     &    access='sequential',form='formatted')
      endif
cmd
c
      call trnfln( 1, fdft )
      open(unit=ndft,file=fdft,status='unknown',
     & access='sequential',form='unformatted')
c
      call trnfln( 1, foft )
      open(unit=noft,file=foft,status='unknown',
     & access='sequential',form='unformatted')
c
c     inquire(unit=ndrt1,name=fdrt(1))
c     inquire(unit=ndrt2,name=fdrt(2))
c     inquire(unit=ndft,name=fdft)
c     inquire(unit=noft,name=foft)
c
      write(nlist,6030) fdrt, fdft, foft
6030  format(/' drt file: ',2a/' dft file: ',a/' oft file: ',a)
c
c     # read dimension information from the drt file.
c     # (use rev(*) as input buffer and as scratch for unused arrays.)
c
c    read the first drt (bra vector definition)
c
      call rdhead(
     & title,  ndot,   nact,   nsym,
     & nrow,   ssym1,  smult,  nelt,
     & nela,   nwalk,  ncsf,   lenbuf,
     & rev,    namomx, nrowmx, nwlkmx,
     & nlist,  ndrt1 )
c
c     # read drt arrays from the drt file.
c
      call rddrt(
     & ndrt1,   ndot,   nact,   nsym,
     & nrow,   nwalk,  ncsf,   lenbuf,
     & rev,    nj,     njskp,  b,
     & l,      y,      xbar,   xp,
     & z,      limvec, rev,    arcsym )
c
c    read the second drt (ket vector definition)
c
      if ((iciset1.eq.iciset2).or.(nfile1.eq.nfile2)) then
      ndrt2 = ndrt1
      rewind ndrt1
      endif
      call rdhead(
     & title,  ndot,   nact,   nsym,
     & nrow,   ssym2,  smult,  nelt,
     & nela,   nwalk2,  ncsf,   lenbuf,
     & rev2,   namomx, nrowmx, nwlkmx,
     & nlist,  ndrt2  )
c
c     # read drt arrays from the drt file.
c
      call rddrt(
     & ndrt2,   ndot,    nact,    nsym,
     & nrow,    nwalk2,   ncsf,    lenbuf,
     & rev2,    nj2,     njskp2,  b2,
     & l2,      y2,      xbar2,   xp2,
     & z2,      limvec2, rev2,    arcsym2 )
cmd
c
      if ((istep.eq.1).and.(ssym1.ne.ssym2))symdiff = .true.
      write(nlist,*)
      call timer( 'setup required:', tmrein, iclock, nlist )
c
c     # construct the diagonal formula tape.
c
      call diag
      write(nlist,*)
      call timer( 'diag required:', tmrein, iclock, nlist )
c
c     # construct the off-diagonal formula tape.
c
      call offdia
      write(nlist,*)
      call timer( 'offdia required:', tmrein, iclock, nlist )
c
c     # calculate ft statistics.
c
c     call ftstat
      write(nlist,*)
      call timer( 'ftstat required:', tmclr, iclock, nlist )
c
c     # formula tape generation finished.
c
      write(nlist,*)
      call timer( 'mcuft required:', tmclr, imcuft, nlist )
c
c     for different space symetry change bra and ket vector
c
      if ((ssym1.ne.ssym2).and.(last.ne.1)) then
      last = 1
      close(ndrt1)
      if (ndrt1.ne.ndrt2) close(ndrt2)
      close(ndft)
      close(noft)
      istep = istep + 1
      go to 500
      endif
c
      call bummer('normal termination',0,3)
      stop 'end of transft'
      end
c deck block data
      block data
c
c  pointer tables for segment construction and evaluation.
c
c  this version written 06-aug-84 by ron shepard.
c
       implicit none
      real*8    zero,     one,     half
      parameter(zero=0d0, one=1d0, half=5d-1)
      real*8    onem,     halfm
      parameter(onem=-one,halfm=-half)
c
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
cmd
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
c     # nst= the number of segment value types.
c
      integer   nst,    nst5
      parameter(nst=28, nst5=5*nst)
      integer      pt
      common /cpt/ pt(0:3,0:3,nst5)
c
      integer      nex,       bcasex,         kcasex
      common /cex/ nex(-2:3), bcasex(6,-2:3), kcasex(6,-2:3)
c
      real*8          vtran,        vcoeff
      integer                                  vtpt
      common /cvtran/ vtran(2,2,4), vcoeff(2), vtpt
c
      integer
     & st1,      st2,     st3,
     & st4,      st4p,    st5,     st6,     st7,    st8,    st10,
     & st11d,    st11,    st12,    st13,
     & st14
      common /cst/
     & st1(7),   st2(7),  st3(7),
     & st4(5),   st4p(5), st5(5),  st6(5),  st7(5), st8(5), st10(5),
     & st11d(3), st11(3), st12(3), st13(3),
     & st14(1)
c
      integer
     & db1,       db2,       db3,
     & db4,       db4p,      db5,      db6,      db7,    db8,
     & db10,
     & db11,      db12,      db13,
     & db14
      common /cdb/
     & db1(7,2),  db2(7,2),  db3(7,2),
     & db4(5,2),  db4p(5),   db5(5),   db6(5,2), db7(5), db8(5,2),
     & db10(5),
     & db11(3,2), db12(3,3), db13(3),
     & db14(1,2)
c
c     # the following parameter list is the location of the
c     # delb=0 pointer matrix for the various segment types.
c
      integer w0, rt0, lb0, rb0, lt0, r0, l0, wrt0, wrb0, ww0, wr0,
     & rtrt00, rbrb00, rtrb0, rrt00, rrt10, rrb00, rrb10, rr00, rr10,
     & rtlb0, rtlt10, rblb10, rtl10, rlb10, rlt10, rl00, rl10
c
      parameter(w0=3)
      parameter(rt0=8)
      parameter(lb0=13)
      parameter(rb0=18)
      parameter(lt0=23)
      parameter(r0=28)
      parameter(l0=33)
      parameter(wrt0=38)
      parameter(wrb0=43)
      parameter(ww0=48)
      parameter(wr0=53)
      parameter(rtrt00=58)
      parameter(rbrb00=63)
      parameter(rtrb0=68)
      parameter(rrt00=73)
      parameter(rrt10=78)
      parameter(rrb00=83)
      parameter(rrb10=88)
      parameter(rr00=93)
      parameter(rr10=98)
      parameter(rtlb0=103)
      parameter(rtlt10=108)
      parameter(rblb10=113)
      parameter(rtl10=118)
      parameter(rlb10=123)
      parameter(rlt10=128)
      parameter(rl00=133)
      parameter(rl10=138)
c
c     # equivalence names are used only locally to this routine
c     # for the purpose of documentation.
c
      integer w(0:3,0:3,-2:2)
      equivalence (w(0,0,0),pt(0,0,w0))
c
      integer rt(0:3,0:3,-2:2)
      equivalence (rt(0,0,0),pt(0,0,rt0))
c
      integer lb(0:3,0:3,-2:2)
      equivalence (lb(0,0,0),pt(0,0,lb0))
c
      integer rb(0:3,0:3,-2:2)
      equivalence (rb(0,0,0),pt(0,0,rb0))
c
      integer lt(0:3,0:3,-2:2)
      equivalence (lt(0,0,0),pt(0,0,lt0))
c
      integer r(0:3,0:3,-2:2)
      equivalence (r(0,0,0),pt(0,0,r0))
c
      integer l(0:3,0:3,-2:2)
      equivalence (l(0,0,0),pt(0,0,l0))
c
      integer wrt(0:3,0:3,-2:2)
      equivalence (wrt(0,0,0),pt(0,0,wrt0))
c
      integer wrb(0:3,0:3,-2:2)
      equivalence (wrb(0,0,0),pt(0,0,wrb0))
c
      integer ww(0:3,0:3,-2:2)
      equivalence (ww(0,0,0),pt(0,0,ww0))
c
      integer wr(0:3,0:3,-2:2)
      equivalence (wr(0,0,0),pt(0,0,wr0))
c
      integer rtrt0(0:3,0:3,-2:2)
      equivalence (rtrt0(0,0,0),pt(0,0,rtrt00))
c
      integer rbrb0(0:3,0:3,-2:2)
      equivalence (rbrb0(0,0,0),pt(0,0,rbrb00))
c
      integer rtrb(0:3,0:3,-2:2)
      equivalence (rtrb(0,0,0),pt(0,0,rtrb0))
c
      integer rrt0(0:3,0:3,-2:2)
      equivalence (rrt0(0,0,0),pt(0,0,rrt00))
c
      integer rrt1(0:3,0:3,-2:2)
      equivalence (rrt1(0,0,0),pt(0,0,rrt10))
c
      integer rrb0(0:3,0:3,-2:2)
      equivalence (rrb0(0,0,0),pt(0,0,rrb00))
c
      integer rrb1(0:3,0:3,-2:2)
      equivalence (rrb1(0,0,0),pt(0,0,rrb10))
c
      integer rr0(0:3,0:3,-2:2)
      equivalence (rr0(0,0,0),pt(0,0,rr00))
c
      integer rr1(0:3,0:3,-2:2)
      equivalence (rr1(0,0,0),pt(0,0,rr10))
c
      integer rtlb(0:3,0:3,-2:2)
      equivalence (rtlb(0,0,0),pt(0,0,rtlb0))
c
      integer rtlt1(0:3,0:3,-2:2)
      equivalence (rtlt1(0,0,0),pt(0,0,rtlt10))
c
      integer rblb1(0:3,0:3,-2:2)
      equivalence (rblb1(0,0,0),pt(0,0,rblb10))
c
      integer rtl1(0:3,0:3,-2:2)
      equivalence (rtl1(0,0,0),pt(0,0,rtl10))
c
      integer rlb1(0:3,0:3,-2:2)
      equivalence (rlb1(0,0,0),pt(0,0,rlb10))
c
      integer rlt1(0:3,0:3,-2:2)
      equivalence (rlt1(0,0,0),pt(0,0,rlt10))
c
      integer rl0(0:3,0:3,-2:2)
      equivalence (rl0(0,0,0),pt(0,0,rl00))
c
      integer rl1(0:3,0:3,-2:2)
      equivalence (rl1(0,0,0),pt(0,0,rl10))
c
c     # for each loop type, assign the segment extension type pointers
c     # and the delb offsets for each range.
c
c     # loop type 1:
      data  st1/     1,     0,    -1,     0,     1,     0,    -1/
      data  db1/   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     &             rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 2:
      data  st2/     1,     0,    -1,     0,    -1,     0,     1/
      data  db2/   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     &             rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 3:
      data  st3/     1,     0,     1,     0,    -1,     0,    -1/
      data  db3/   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     &             rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0/
c
c     # loop type 4:
      data  st4/     0,     0,     1,     0,    -1/
      data  db4/    w0,  rl00,   rb0,    r0,   rt0,
     &          rblb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 4':
      data st4p/     0,     0,    -1,     0,     1/
      data db4p/rblb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 5:
      data  st5/     2,     0,    -1,     0,    -1/
      data  db5/rbrb00,  rr00, rrt00,    r0,   rt0/
c
c     # loop type 6:
      data  st6/     1,     0,     0,     0,    -1/
      data  db6/   rb0,    r0, rtrb0,    r0,   rt0,
     &             rb0,    r0,   wr0,    r0,   rt0/
c
c     # loop type 7:
      data  st7/     1,     0,    -2,     0,     1/
      data  db7/   rb0,    r0, rtlb0,    l0,   lt0/
c
c     # loop type 8:
      data  st8/     1,     0,    -1,     0,     0/
      data  db8/   rb0,    r0,   rt0,  rl00,    w0,
     &             rb0,    r0, rlb10,  rl10,rtlt10/
c
c     # loop type 10:
      data st10/     1,     0,     1,     0,    -2/
      data db10/   rb0,    r0, rrb00,  rr00,rtrt00/
c
c     # loop type 11d, 11:
      data st11d/   3,      3,     3/
      data st11/    0,      0,     0/
      data db11/   w0,   rl00,    w0,
     &         rblb10,   rl10,rtlt10/
c
c     # loop type 12:
      data st12/     1,     0,    -1/
      data db12/  wrb0,    r0,   rt0,
     &             rb0,    r0,  wrt0,
     &             rb0,    r0,   rt0/
c
c     # loop type 13:
      data st13/     2,     0,    -2/
      data db13/rbrb00,  rr00,rtrt00/
c
c     # loop type 14:
      data st14/     3/
      data db14/   ww0,
     &              w0/
c
c     # segment values are calculated as:
c
c     #     tab( type(bcase,kcase,delb), bket)
c
c     # where the entries in tab(*,*) are defined in the table
c     # intiialization routine.  type(*,*,*) is equivalenced to the
c     # appropriate  location of pt(*,*,*) and pt(*,*,*) is actually
c     # referenced with the corresponding offset added to delb.
c
c     #     e.g. tab( pt(bcase,kcase,db0(r)+delb), bket)
c
c**********************************************************
c     # w segment value pointers (assuming mu(*)=0):
      data w/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 4,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top segment value pointers:
      data rt/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  2 ,1 ,1 ,1,  2, 1, 1, 1,  1,18,20, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-bottom segment value pointers:
      data lb/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  2, 1, 1, 1,  1,20, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  2, 1, 1, 1,  1, 1, 1, 1,  1, 1,18, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom segment value pointers:
      data rb/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 2, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 2, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-top segment value pointers:
      data lt/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 2, 2, 1,  1, 1, 1,18,  1, 1, 1,20,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r segment value pointers:
      data r/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 3,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 3,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l segment value pointers:
      data l/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1,33, 1, 1,  1,10, 3, 1,  1, 1, 1, 3,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1, 3,11, 1,  1, 1,33, 1,  1, 1, 1, 3,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=0):
      data wrt/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1,18,20, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=0):
      data wrb/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=0):
      data ww/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 4,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=0):
      data wr/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 5,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 5,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-top, x=0 segment value pointers:
      data rtrt0/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  6, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-r-bottom,x=0  segment value pointers:
      data rbrb0/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 6,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=0):
      data rtrb/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 2, 1, 1,  1, 2, 1, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 2, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=0 segment value pointers:
      data rrt0/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1, 54, 1, 1, 1,  1, 7, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1, 44, 1, 1, 1,  1, 1, 1, 1,  1, 1, 7, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=1 segment value pointers (assuming mu(*)=0):
      data rrt1/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  2, 1, 1, 1, 55, 1, 1, 1,  1,53,24, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1, 43, 1, 1, 1,  2, 1, 1, 1,  1,14,45, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=0 segment value pointers:
      data rrb0/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1,48,49, 1,  1, 1, 1, 7,  1, 1, 1, 7,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=1 segment value pointers:
      data rrb1/
     &   1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1,23,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1,50,48, 1,  1, 1, 1,46,  1, 1, 1,52,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 3, 1,  1, 1, 1,17,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=0 segment value pointers:
      data rr0/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=1 segment value pointers:
      data rr1/
     &   2, 1, 1, 1,  1, 2, 1, 1,  1,30,41, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1,38,27, 1,  1,29,40, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1,36,26, 1,  1, 1, 2, 1,  1, 1, 1, 2/
c**********************************************************
c     # r-top-l-bottom segment value pointers:
      data rtlb/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 20, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 18, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l-top, x=1 segment value pointers:
      data rtlt1/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1,47, 2, 1,  1, 2,51, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-l-bottom, x=1 segment value pointers:
      data rblb1/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1,34, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1,45, 1, 1,  1, 1,53, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1,31, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l, x=1 segment value pointers:
      data rtl1/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  2, 1, 1, 1, 51, 1, 1, 1,  1,48,21, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1, 47, 1, 1, 1,  2, 1, 1, 1,  1,19,50, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-l-bottom, x=1 segment value pointers:
      data rlb1/
     &   1, 1, 1, 1,  1, 1, 1, 1, 34, 1, 1, 1,  1,25, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1, 45, 1, 1, 1, 53, 1, 1, 1,  1,50,48, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1, 31, 1, 1, 1,  1, 1, 1, 1,  1, 1,15, 1/
c**********************************************************
c     # r-l-top, x=1 segment value pointers:
      data rlt1/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1,53, 2, 1,  1, 1, 1,23,  1, 1, 1,56,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 2,45, 1,  1, 1, 1,42,  1, 1, 1,17,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=0 segment value pointers:
      data rl0/
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=1 segment value pointers:
      data rl1/
     &   2, 1, 1, 1,  1,35, 1, 1,  1,13,35, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1,37,28, 1,  1,28,39, 1,  1, 1, 1, 2,
     &   1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     &   2, 1, 1, 1,  1,32, 9, 1,  1, 1,32, 1,  1, 1, 1, 2/
c
c     # segment extension tables:
c     # casex(*,i) for i=-2 to 2 are extensions for occupation changes
c     #                          of i.
c     #                i=3 is a special case for diagonal extensions.
c
      data nex/ 1, 4, 6, 4, 1, 4/
      data bcasex/
     &   0, 0, 0, 0, 0, 0,
     &   2, 1, 0, 0, 0, 0,
     &   3, 2, 2, 1, 1, 0,
     &   3, 3, 2, 1, 0, 0,
     &   3, 0, 0, 0, 0, 0,
     &   3, 2, 1, 0, 0, 0/
      data kcasex/
     &   3, 0, 0, 0, 0, 0,
     &   3, 3, 2, 1, 0, 0,
     &   3, 2, 1, 2, 1, 0,
     &   2, 1, 0, 0, 0, 0,
     &   0, 0, 0, 0, 0, 0,
     &   3, 2, 1, 0, 0, 0/
c
c     # irrep multiplication table.
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c     # loop-value to ft-value transformation matrices.
c
      data vtran/
     & one,  zero,  zero,   one,
     & one,  zero,  halfm,  one,
     & one,  onem,  one,    one,
     & half, zero,  zero,   one /
      data vcoeff/ one, half/
c
c     # file unit numbers.
c
      data nlist /6/
      data ndrt1 /19/
      data ndrt2 /20/
      data ndft  /21/
      data noft  /22/
      data nrestp/23/
      data trsci /24/
      data nin   /5/
c
      end
c deck istack
      subroutine istack
c
c  initialize necessary stack information for loop construction.
c
       implicit none
      integer   namomx
      parameter(namomx=99)
c
      real*8  v
      logical qeq
      integer          range,           extbk,
     & brow,           krow,            bsym,            ksym,
     & yb,             yk
      common /cstack/
     & v(3,0:namomx),  range(0:namomx), extbk(0:namomx), qeq(0:namomx),
     & brow(0:namomx), krow(0:namomx),  bsym(0:namomx),  ksym(0:namomx),
     & yb(8,0:namomx), yk(8,0:namomx)
c
      integer i, j
c
      real*8    zero
      parameter(zero=0d0)
c
      do 200 i = 0, namomx
         extbk(i) = 0
         v(1,i)   = zero
         v(2,i)   = zero
         v(3,i)   = zero
         do 100 j = 1, 8
            yb(j,i) = 0
            yk(j,i) = 0
100      continue
200   continue
c
      return
      end
c deck intab
      subroutine intab
c
c  initialize tables for segment value calculations.
c
c  for details of segment evaluation and the auxiliary functions,
c  see:
c        i. shavitt, in "the unitary group for the evaluation of
c        electronic energy matrix elements", j. hinze, ed.
c        (springer-verlag, berlin, 1981) p. 51. and i. shavitt,
c        "new methods in computational quantum chemistry and
c        their application on modern super-computers" (annual
c        report to the national aeronautics and space administration),
c        battelle columbus laboratories, june 1979.
c
c  this version written 01-aug-84 by ron shepard.
c
       implicit none
      integer   ntab,   maxb
      parameter(ntab=56,maxb=19)
      real*8        tab
      common /ctab/ tab(ntab,0:maxb)
c
      integer p, q, b, i
      real*8 sqrt2,t
c
      real*8    zero,    one,    two
      parameter(zero=0d0,one=1d0,two=2d0)
c
c  table choice is dictated by the requirement that all segment
c  values are to be calculated with array references only and
c  with no conditional tests or goto's required.  this requires
c  an over-complete set of tables to allow for sign changes and
c  constant factors.
c
c  table entry definitions:
c
c  1:0          2:1          3:-1            4:2           5:-2
c  6:sqrt2      7:-t         8:1/b           9:-sqrt2/b   10:1/(b+1)
c 11:-1/(b+1)  12:-1/(b+2)  13:-sqrt2/(b+2) 14:a(-1,0)    15:-a(-1,0)
c 16:a(1,0)    17:-a(1,0)   18:a(0,1)       19:-a(0,1)    20:a(2,1)
c 21:-a(2,1)   22:a(1,2)    23:-a(1,2)      24:a(3,2)     25:-a(3,2)
c 26:b(-1,0)   27:b(0,1)    28:-b(0,2)      29:b(1,2)     30:b(2,3)
c 31:c(0)      32:-c(0)     33:c(1)         34:c(2)       35:-c(2)
c 36:d(-1)     37:d(0)      38:-d(0)        39:d(1)       40:-d(1)
c 41:d(2)      42:ta(-1,0)  43:-ta(-1,0)    44:ta(1,0)    45:ta(2,0)
c 46:-ta(2,0)  47:-ta(-1,1) 48:ta(0,1)      49:ta(2,1)    50:-ta(2,1)
c 51:ta(3,1)   52:ta(0,2)   53:-ta(0,2)     54:ta(1,2)    55:ta(3,2)
c 56:-ta(3,2)
c
c  auxiliary functions:
c
      real*8 xa, xb, xc, xd, xr, rx
      rx(p)     = (p)
      xa(p,q,b) = sqrt(rx(b+p) / rx(b+q))
      xb(p,q,b) = sqrt(two / rx((b+p) * (b+q)))
      xc(p,b)   = sqrt(rx((b+p-1) * (b+p+1))) / rx(b+p)
      xd(p,b)   = sqrt(rx((b+p-1) * (b+p+2)) / rx((b+p)*(b+p+1)))
      xr(p,b)   = one / rx(b+p)
c
      sqrt2 = sqrt(two)
      t     = one / sqrt2
c
      do 110 b= 0, maxb
         do 100 i = 1, ntab
            tab(i,b) = zero
100      continue
110   continue
c
      do 200 b = 0, maxb
         tab( 1,b) = zero
         tab( 2,b) = +one
         tab( 3,b) = -one
         tab( 4,b) = +two
         tab( 5,b) = -two
         tab( 6,b) = +sqrt2
         tab( 7,b) = -t
         tab(10,b) = +xr(1,b)
         tab(11,b) = -xr(1,b)
         tab(12,b) = -xr(2,b)
         tab(13,b) = -sqrt2*xr(2,b)
         tab(18,b) = +xa(0,1,b)
         tab(19,b) = -xa(0,1,b)
         tab(20,b) = +xa(2,1,b)
         tab(21,b) = -xa(2,1,b)
         tab(22,b) = +xa(1,2,b)
         tab(23,b) = -xa(1,2,b)
         tab(24,b) = +xa(3,2,b)
         tab(25,b) = -xa(3,2,b)
         tab(29,b) = +xb(1,2,b)
         tab(30,b) = +xb(2,3,b)
         tab(34,b) = +xc(2,b)
         tab(35,b) = -xc(2,b)
         tab(39,b) = +xd(1,b)
         tab(40,b) = -xd(1,b)
         tab(41,b) = +xd(2,b)
         tab(48,b) = +t*xa(0,1,b)
         tab(49,b) = +t*xa(2,1,b)
         tab(50,b) = -t*xa(2,1,b)
         tab(51,b) = +t*xa(3,1,b)
         tab(52,b) = +t*xa(0,2,b)
         tab(53,b) = -t*xa(0,2,b)
         tab(54,b) = +t*xa(1,2,b)
         tab(55,b) = +t*xa(3,2,b)
         tab(56,b) = -t*xa(3,2,b)
200   continue
c
      do 300 b = 1, maxb
         tab( 8,b) = +xr(0,b)
         tab( 9,b) = -sqrt2*xr(0,b)
         tab(14,b) = +xa(-1,0,b)
         tab(15,b) = -xa(-1,0,b)
         tab(16,b) = +xa(1,0,b)
         tab(17,b) = -xa(1,0,b)
         tab(27,b) = +xb(0,1,b)
         tab(28,b) = -xb(0,2,b)
         tab(31,b) = +xc(0,b)
         tab(32,b) = -xc(0,b)
         tab(33,b) = +xc(1,b)
         tab(37,b) = +xd(0,b)
         tab(38,b) = -xd(0,b)
         tab(42,b) = +t*xa(-1,0,b)
         tab(43,b) = -t*xa(-1,0,b)
         tab(44,b) = +t*xa(1,0,b)
         tab(45,b) = +t*xa(2,0,b)
         tab(46,b) = -t*xa(2,0,b)
         tab(47,b) = -t*xa(-1,1,b)
300   continue
c
      do 400 b = 2, maxb
         tab(26,b) = +xb(-1,0,b)
         tab(36,b) = +xd(-1,b)
400   continue
c
      return
      end
c deck rdhead
      subroutine rdhead(
     & title,  ndot,   nact,   nsym,
     & nrow,   ssym,   smult,  nelt,
     & nela,   nwalk,  ncsf,   lenbuf,
     & buf,    namomx, nrowmx, nwlkmx,
     & nlist,  ndrt    )
c
c  read the header information from the drt file.
c
       implicit none
      integer ndot, nact, nsym, nrow, ssym, smult, nelt, nela, nwalk,
     & ncsf, lenbuf, namomx, nrowmx, nwlkmx, nlist, ndrt
      integer buf(*)
      character*80 title
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer vrsion, nhdint, ierr
c
c     # title.
      read(ndrt, '(a)',iostat=ierr) title
      if ( ierr .ne. 0 ) then
         call bummer('rdhead: title read, ierr=',ierr,faterr)
      endif
c
      write(nlist,6010) title
6010  format(/' drt header information:'/1x,a)
c
c     # first header record.
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
      if ( vrsion .ne. 2 ) then
         write(nlist,*)
     &    '*** warning: rdhead: drt version .ne. 2 *** vrsion=',vrsion
      endif
c
c     # second header record.
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      ndot  = buf(1)
      nact  = buf(2)
      nsym  = buf(3)
      nrow  = buf(4)
      ssym  = buf(5)
      smult = buf(6)
      nelt  = buf(7)
      nela  = buf(8)
      nwalk = buf(9)
      ncsf  = buf(10)
c
      write(nlist,6100)
     & ndot, nact, nsym, nrow, ssym, smult,
     & nelt, nela, nwalk, ncsf, lenbuf
6100  format(
     & ' ndot=  ',i6,4x,'nact=  ',i6,4x,'nsym=  ',i6,4x,'nrow=  ',i6/
     & ' ssym=  ',i6,4x,'smult= ',i6,4x,'nelt=  ',i6,4x,'nela=  ',i6/
     & ' nwalk= ',i6,4x,'ncsf=  ',i6,4x,'lenbuf=',i6)
c
      if ( nact .gt. namomx ) then
         write(nlist,6900)'nact,namomx', nact, namomx
         call bummer('rdhead: nact=',nact,faterr)
      elseif ( nrow .gt. nrowmx ) then
         write(nlist,6900)'nrow,nrowmx', nrow, nrowmx
         call bummer('rdhead: nrow=',nrow,faterr)
      elseif ( nwalk .gt. nwlkmx ) then
         write(nlist,6900)'nwalk,nwlkmx', nact, nwlkmx
         call bummer('rdhead: nwalk=',nwalk,faterr)
      elseif ( lenbuf .gt. nwlkmx ) then
c        # rev(*) is used as drt buffer(*).
         write(nlist,6900)'lenbuf,nwlkmx', lenbuf, nwlkmx
         call bummer('rdhead: lenbuf=',lenbuf,faterr)
      endif
c
      return
6900  format(/' error: ',a,2i10)
      end
c deck rddrt
      subroutine rddrt(
     & ndrt,   ndot,   nact,   nsym,
     & nrow,   nwalk,  ncsf,   lenbuf,
     & buf,    nj,     njskp,  b,
     & l,      y,      xbar,   xp,
     & z,      limvec, r,      arcsym )
c
c  read the drt arrays from the drt file.
c  title and dimension information have already been read.
c  the file is assumed to be positioned correctly for the
c  next records.
c
       implicit none
      integer ndrt, ndot, nact, nsym, nrow, nwalk, ncsf, lenbuf
      integer buf(lenbuf)
      integer nj(0:nact)
      integer njskp(0:nact)
      integer b(nrow)
      integer l(0:3,nrow)
      integer y(8,0:3,nrow)
      integer xbar(8,nrow)
      integer xp(8,nrow)
      integer z(8,nrow)
      integer limvec(nwalk)
      integer r(nwalk)
      integer arcsym(0:3,0:nact)
c
      integer i, nlevel, nleft, bpt, irow, icase, isym, nread, ncsfx
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    toskp,   to01
      parameter( toskp=0, to01=1 )
c
      nlevel = nact + 1
c
c     # slabel(*)...
      call rddbl( ndrt, lenbuf, buf, -nsym )
c
      if ( ndot .gt. 0 ) then
c
c        # doub(*)...
         call rddbl( ndrt, lenbuf, buf, -ndot )
c
c        # symd(*)...
         call rddbl( ndrt, lenbuf, buf, -ndot )
      endif
c
c     # modrt(*)...
      call rddbl( ndrt, lenbuf, buf, -nact )
c
c     # syml(*)...
      call rddbl( ndrt, lenbuf, buf, nact )
      buf(nlevel)=0
      do 10 i=1,nlevel
         arcsym(0,i-1) = 1
         arcsym(1,i-1) = buf(i)
         arcsym(2,i-1) = buf(i)
         arcsym(3,i-1) = 1
10    continue
c
c     # nj(*)...
      call rddbl( ndrt, lenbuf, nj, nlevel )
c
c     # njskp(*)...
      call rddbl( ndrt, lenbuf, njskp, nlevel )
c
c     # a(*)...
      call rddbl( ndrt, lenbuf, buf, -nrow )
c
c     # b(*)...
      call rddbl( ndrt, lenbuf, b, nrow )
c
c     # l(*)...
      call rddbl( ndrt, lenbuf, l, 4*nrow )
c
c     # y(*,*,*)...
      nleft = 4 * nrow * nsym
      bpt = lenbuf
      do 130 irow = 1, nrow
         do 120 icase = 0, 3
            do 110 isym = 1, nsym
               if ( bpt .eq. lenbuf ) then
                  nread = min( nleft, lenbuf )
                  call rddbl( ndrt, lenbuf, buf, nread )
                  bpt = 0
               endif
               bpt = bpt + 1
               y(isym,icase,irow) = buf(bpt)
               nleft = nleft - 1
110         continue
120      continue
130   continue
c
c     # xbar(*,*)...
      nleft = nrow * nsym
      bpt = lenbuf
      do 230 irow = 1, nrow
         do 210 isym = 1, nsym
            if ( bpt .eq. lenbuf ) then
               nread = min( nleft, lenbuf )
               call rddbl( ndrt, lenbuf, buf, nread )
               bpt = 0
            endif
            bpt = bpt + 1
            xbar(isym,irow) = buf(bpt)
            nleft = nleft - 1
210      continue
230   continue
c
c     # xp(*,*)...
      nleft = nrow * nsym
      bpt = lenbuf
      do 330 irow = 1, nrow
         do 310 isym = 1, nsym
            if ( bpt .eq. lenbuf ) then
               nread = min( nleft, lenbuf )
               call rddbl( ndrt, lenbuf, buf, nread )
               bpt = 0
            endif
            bpt = bpt + 1
            xp(isym,irow) = buf(bpt)
            nleft = nleft - 1
310      continue
330   continue
c
c     # z(*,*)...
      nleft = nrow * nsym
      bpt = lenbuf
      do 430 irow = 1, nrow
         do 410 isym = 1, nsym
            if ( bpt .eq. lenbuf ) then
               nread = min( nleft, lenbuf )
               call rddbl( ndrt, lenbuf, buf, nread )
               bpt = 0
            endif
            bpt = bpt + 1
            z(isym,irow) = buf(bpt)
            nleft = nleft - 1
410      continue
430   continue
c
c     # limvec(*)...
      call rddbl( ndrt, lenbuf, limvec, nwalk )
c
c     # r(*)...
      call rddbl( ndrt, lenbuf, r, nwalk )
c
c     # change 0/1 limvec(*) to skip-vector form.
c
      call skpx01( nwalk, limvec, toskp, ncsfx )
c
      if ( ncsf .ne. ncsfx ) then
         call bummer('rddrt: toskp, (ncsf-ncsfx)=',
     &    (ncsf-ncsfx), faterr )
      endif
c
      return
      end
c deck diag
      subroutine diag
c
c  construct the diagonal formula tape.
c
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2       11ab        eiill, eilli
c   3       11a         eiill
c   4       11b         eilil
c
c   5       14ab        half*ellll, ell
c   6       14a         half*ellll
c   7       14b         ell
c
       implicit none
      integer   nrowmx,        nwlkmx,         namomx
      parameter(nrowmx=2**10-1, nwlkmx=2**20-1, namomx=99)
c
      integer
     & b,            l,             y,                   xbar,
     & xp,           z,             arcsym,
     & nj,           njskp,nwalk
      common /cdrt/
     & b(nrowmx),    l(0:3,nrowmx), y(8,0:3,nrowmx),     xbar(8,nrowmx),
     & xp(8,nrowmx), z(8,nrowmx),   arcsym(0:3,0:namomx),
     & nj(0:namomx), njskp(0:namomx),nwalk
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
cmd
      integer
     & b2,            l2,             y2,               xbar2,
     & xp2,           z2,             arcsym2,
     & nj2,           njskp2,nwalk2
      common /cdrt_2/
     & b2(nrowmx),l2(0:3,nrowmx),y2(8,0:3,nrowmx),xbar2(8,nrowmx),
     & xp2(8,nrowmx), z2(8,nrowmx),   arcsym2(0:3,0:namomx),
     & nj2(0:namomx), njskp2(0:namomx),nwalk2
cmd
      integer
     & st1,      st2,     st3,
     & st4,      st4p,    st5,     st6,     st7,    st8,    st10,
     & st11d,    st11,    st12,    st13,
     & st14
      common /cst/
     & st1(7),   st2(7),  st3(7),
     & st4(5),   st4p(5), st5(5),  st6(5),  st7(5), st8(5), st10(5),
     & st11d(3), st11(3), st12(3), st13(3),
     & st14(1)
c
      integer
     & db1,       db2,       db3,
     & db4,       db4p,      db5,      db6,      db7,    db8,
     & db10,
     & db11,      db12,      db13,
     & db14
      common /cdb/
     & db1(7,2),  db2(7,2),  db3(7,2),
     & db4(5,2),  db4p(5),   db5(5),   db6(5,2), db7(5), db8(5,2),
     & db10(5),
     & db11(3,2), db12(3,3), db13(3),
     & db14(1,2)
c
      logical      qdiag
      integer             nact, numv, lowlev, hilev, code,    mo,    nmo
      common /cli/ qdiag, nact, numv, lowlev, hilev, code(4), mo(4), nmo
c
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
c
      integer ssym1,ssym2
      logical symdiff
      common /ssym/ ssym1,ssym2,symdiff
cmd
c
      real*8          vtran,        vcoeff
      integer                                  vtpt
      common /cvtran/ vtran(2,2,4), vcoeff(2), vtpt
c
      integer   lbft
      parameter(lbft=2047)
      real*8        ftbuf
      integer                    kntft, nloop, nftb
      common /cftb/ ftbuf(lbft), kntft, nloop, nftb
c
*@ifdef debug
*      logical       qprt
*      common /cprt/ qprt
*@endif
c
      integer lmo, llev, imo, ilev, ver1, ver2, tail
c
      real*8    zero,     one,     half
      parameter(zero=0d0, one=1d0, half=5d-1)
c
*@ifdef debug
*      qprt  = .true.
*@endif
      nloop = 0
      nftb  = 0
      kntft = 0
      qdiag = .true.
      nfile = ndft
      rewind nfile
c
      do 500 lmo = 1, nact
         llev = lmo - 1
c
c        # diagonal loops 11a,b.
c
         numv    = 2
         code(1) = 2
         code(2) = 3
         code(3) = 4
c
         vtpt    = 2
c
         do 200 imo = 1, (lmo-1)
            ilev = imo - 1
c
            nmo   = 2
            mo(1) = imo
            mo(2) = lmo
c
            ver1 = njskp(ilev) + 1
            ver2 = njskp(ilev) + nj(ilev)
            do 100 tail = ver1, ver2
cmd
c     if the two states belong to different symmetries than
c       there are no diagonal elements
c      (calling this subroutine in this case would lead to
c        wrong transition density matrix)
c
cmd
               if (.not.symdiff) then
               call loop( tail, st11d, db11, 3 )
               endif
100         continue
c
            call wcode(1)
200      continue
c
c        # diagonal loops 14a,b.
c
         nmo   = 1
         mo(1) = lmo
c
         numv    = 2
         code(1) = 5
         code(2) = 6
         code(3) = 7
c
         vtpt    = 4
c
         ver1 = njskp(llev) + 1
         ver2 = njskp(llev) + nj(llev)
         do 400 tail = ver1, ver2
cmd
c     if the two states belong to different symmetries than
c       there are no diagonal elements
c      (calling this subroutine in this case would lead to
c        wrong transition density matrix)
c
cmd
            if (.not.symdiff) then
            call loop( tail, st14, db14, 1 )
            endif
400      continue
c
         call wcode(1)
500   continue
c
600   call dump(1)
c
      write(nlist,6010) nloop, nftb
6010  format(/' diagonal ft construction complete.',
     & i8,' loops constructed.',i6,' ft buffers used.')
c
      return
      end
c deck offdia
      subroutine offdia
c
c  construct the off-diagonal formula tape.
c
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      iiil, illl, il
c   3           2a'b'      ijlk, iklj
c   4           1ab        ijkl, ilkj
c   5           3ab        ikjl, iljk
c   6           4ab        iikl, ilki
c               8ab        ijll, illj
c   7           6a'b'      ikkl, ilkk
c   8           12ac       iiil, il
c   9           12bc       illl, il
c  10           1a         ijkl
c               2a'        ijlk
c               6a'        ikkl
c               7'         iklk
c  11           4a         iikl
c               8a         ijll
c  12           2b'        iklj
c               3a         ikjl
c               4b         ilki
c               4b'        ikli
c               5          ilik
c               8b         illj
c               10         iljl
c  13           11b        illi
c               13         half*ilil
c  14           1b         ilkj
c               3b         iljk
c  15           6b'        ilkk
c               12c        il
c
c  where the ' after a loop type signifies some difference
c  between the loops constructed here and the conventional
c  definitions.  these differences are either the interchange
c  of the bra and ket partial walks or the interchange of the
c  a and b definitions of a loop.  ft values are chosen to
c  facilitate h matrix construction while the codes are chosen
c  to facilitate density matrix construction.
c
c  written 08-aug-84 by ron shepard.
c
       implicit none
      integer   nrowmx,        namomx
      parameter(nrowmx=2**10-1, namomx=99)
c
      integer
     & b,            l,             y,                   xbar,
     & xp,           z,             arcsym,
     & nj,           nwalk,njskp
      common /cdrt/
     & b(nrowmx),    l(0:3,nrowmx), y(8,0:3,nrowmx),     xbar(8,nrowmx),
     & xp(8,nrowmx), z(8,nrowmx),   arcsym(0:3,0:namomx),
     & nj(0:namomx), njskp(0:namomx),nwalk
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
cmd
      integer
     & b2,            l2,             y2,                   xbar2,
     & xp2,           z2,             arcsym2,
     & nj2,           njskp2,nwalk2
      common /cdrt_2/
     & b2(nrowmx),l2(0:3,nrowmx),y2(8,0:3,nrowmx),xbar2(8,nrowmx),
     & xp2(8,nrowmx), z2(8,nrowmx),   arcsym2(0:3,0:namomx),
     & nj2(0:namomx), njskp2(0:namomx),nwalk2
c
      integer ssym1,ssym2
      logical symdiff
      common /ssym/ ssym1,ssym2,symdiff
cmd
c
      integer
     & st1,      st2,     st3,
     & st4,      st4p,    st5,     st6,     st7,    st8,    st10,
     & st11d,    st11,    st12,    st13,
     & st14
      common /cst/
     & st1(7),   st2(7),  st3(7),
     & st4(5),   st4p(5), st5(5),  st6(5),  st7(5), st8(5), st10(5),
     & st11d(3), st11(3), st12(3), st13(3),
     & st14(1)
c
      integer
     & db1,       db2,       db3,
     & db4,       db4p,      db5,      db6,      db7,    db8,
     & db10,
     & db11,      db12,      db13,
     & db14
      common /cdb/
     & db1(7,2),  db2(7,2),  db3(7,2),
     & db4(5,2),  db4p(5),   db5(5),   db6(5,2), db7(5), db8(5,2),
     & db10(5),
     & db11(3,2), db12(3,3), db13(3),
     & db14(1,2)
c
      logical      qdiag
      integer             nact, numv, lowlev, hilev, code,    mo,    nmo
      common /cli/ qdiag, nact, numv, lowlev, hilev, code(4), mo(4), nmo
c
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
cmd
c
      integer   lbft
      parameter(lbft=2047)
      real*8        ftbuf
      integer                    kntft, nloop, nftb
      common /cftb/ ftbuf(lbft), kntft, nloop, nftb
c
*@ifdef debug
*      logical       qprt
*      common /cprt/ qprt
*@endif
c
      real*8          vtran,        vcoeff
      integer                                  vtpt
      common /cvtran/ vtran(2,2,4), vcoeff(2), vtpt
c
      integer lmo, llev, lsym, kmo, klev, klsym, jmo, jlev, jklsym,
     & imo, ilev, ver1, ver2, tail
c
      real*8    zero,     one,     half
      parameter(zero=0d0, one=1d0, half=5d-1)
c
c     # numv=     the number of loop values to calculate.
c     # code(*)=  possible ft codes for each loop type.
c     # nmo=      number of mos in the loop.
c     # mo(*)=    the mos in level order defining the loop.
c     # vtpt=     pointer to the appropriate transformation matrix.
c     # vtran(*)= loop-value to ft-value transformation matrices.
c     # stxx(*)=  segment type extensions for various loop types.
c     # dbxx(*)=  table offsets for segment values for loop types.
c
*@ifdef debug
*      qprt  = .true.
*@endif
      nloop = 0
      nftb  = 0
      kntft = 0
      qdiag = .false.
      nfile = noft
      rewind nfile
c
      do 800 lmo = 2, nact
         llev = lmo - 1
         lsym = arcsym(1,llev)
c
         do 700 imo = 1, (lmo-1)
            ilev = imo - 1
cmd
            if ( mult(arcsym2(1,ilev),lsym) .ne.
     &       mult(ssym1,ssym2)) go to 700
cmd
c***********************************************************
c           i<j=k=l loops:
c***********************************************************
c
            nmo   = 2
            mo(1) = imo
            mo(2) = lmo
c
            ver1 = njskp(ilev) + 1
            ver2 = njskp(ilev) + nj(ilev)
            do 690 tail = ver1, ver2
c
c              # 12abc loops:
c
               numv    = 3
               code(1) = 2
               code(2) = 8
               code(3) = 9
               code(4) = 15
               call loop( tail, st12, db12, 3 )
690         continue
c
            call wcode(1)
700      continue
c***********************************************************
c        i=j=k=l loops (none for off-diagonal contributions):
c***********************************************************
800   continue
c
      call dump(1)
c
      write(nlist,6010) nloop, nftb
6010  format(/' off-diagonal ft construction complete.',
     & i8,' loops constructed.',i6,' ft buffers used.')
c
      return
      end
c deck loop
      subroutine loop( tail, stype, db0, nran )
c
c  construct loops and accumulate products of segment values.
c
c  input:
c  tail=     tail of the loops to be constructed.
c  stype(*)= segment extension type of the loops.
c  db0(*)=   delb offsets for the vn(*) segment values.
c  /cpt/     segment value pointer tables.
c  /cex/     segment type extension tables.
c  /ctab/    segment value tables.
c  /cdrt/    drt information.
c  /cdrt2/   vectors spanning number of walks.
c  /cstack/  loop construction stack arrays.
c  /csym/    symmetry information.
c  /cli/     loop information.
c
c  this version written 06-aug-84 by ron shepard.
c
       implicit none
      integer   nst,    nst5
      parameter(nst=28, nst5=5*nst)
      integer      pt
      common /cpt/ pt(0:3,0:3,nst5)
c
      integer      nex,       bcasex,         kcasex
      common /cex/ nex(-2:3), bcasex(6,-2:3), kcasex(6,-2:3)
c
      integer   ntab,   maxb
      parameter(ntab=56,maxb=19)
      real*8        tab
      common /ctab/ tab(ntab,0:maxb)
c
      integer   nrowmx,        nwlkmx,         namomx
      parameter(nrowmx=2**10-1, nwlkmx=2**20-1, namomx=99)
c
      integer
     & b,            l,             y,                   xbar,
     & xp,           z,             arcsym,
     & nj,           nwalk,njskp
      common /cdrt/
     & b(nrowmx),    l(0:3,nrowmx), y(8,0:3,nrowmx),     xbar(8,nrowmx),
     & xp(8,nrowmx), z(8,nrowmx),   arcsym(0:3,0:namomx),
     & nj(0:namomx), njskp(0:namomx),nwalk
c
cmd
      integer
     & b2,            l2,             y2,               xbar2,
     & xp2,           z2,             arcsym2,
     & nj2,           njskp2,nwalk2
      common /cdrt_2/
     & b2(nrowmx),l2(0:3,nrowmx),y2(8,0:3,nrowmx),xbar2(8,nrowmx),
     & xp2(8,nrowmx), z2(8,nrowmx),   arcsym2(0:3,0:namomx),
     & nj2(0:namomx), njskp2(0:namomx),nwalk2
cmd

      integer        limvec,         rev
      common /cdrt2/ limvec(nwlkmx), rev(nwlkmx)
cmd
      integer        limvec2,         rev2
      common /cdrt2_2/ limvec2(nwlkmx), rev2(nwlkmx)
cmd

c
      real*8  v
      logical qeq
      integer          range,           extbk,
     & brow,           krow,            bsym,            ksym,
     & yb,             yk
      common /cstack/
     & v(3,0:namomx),  range(0:namomx), extbk(0:namomx), qeq(0:namomx),
     & brow(0:namomx), krow(0:namomx),  bsym(0:namomx),  ksym(0:namomx),
     & yb(8,0:namomx), yk(8,0:namomx)
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
      logical      qdiag
      integer             nact, numv, lowlev, hilev, code,    mo,    nmo
      common /cli/ qdiag, nact, numv, lowlev, hilev, code(4), mo(4), nmo
c
      integer tail, nran
      integer stype(nran), db0(nran,*)
c
      integer ir, i, il, is, clev, igonv, nlev, r, str, nexr, ext,
     & bcase, kcase, browc, brown, krowc, krown, bbra, bket, delb,
     & bsymc, jsym, bsymn, tsym, xt, hsym, xbh, ybt, zt, j, ksymc,
     & ksymn, ykt
c
      real*8    zero,    one
      parameter(zero=0d0,one=1d0)
c
      logical qbchk(-4:4)
c
      data qbchk/.true.,.true.,.false.,.false.,.false.,
     & .false.,.false.,.true.,.true./
c
c     # assign a range (1 to nran) for each level of the current loop
c     # type.  this completes the loop template definition.
c
      lowlev = mo(1) - 1
      hilev  = mo(nmo)
c
      ir = 1
      range(mo(1) - 1) = ir
c
      do 120 i = 2, nmo
         ir = ir + 1
         do 110 il = mo(i-1), (mo(i)-2)
            range(il) = ir
110      continue
         ir = ir + 1
         range(mo(i) - 1) = ir
120   continue
c
c     # initialize stack arrays for loop construction.
c
      do 210 is = 1, nsym
         yb(is,lowlev) = 0
         yk(is,lowlev) = 0
210   continue
c
      clev       = lowlev
      brow(clev) = tail
      krow(clev) = tail
      bsym(clev) = 1
      ksym(clev) = 1
      qeq(clev)  = .true.
      v(1,clev)  = one
      v(2,clev)  = one
      v(3,clev)  = one
c
c     # one, two, or three segment value products are accumulated.
c     # begin loop construction:
c
      go to 1100
c
1000  continue
c
c     # decrement the current level and check if finished.
c
      extbk(clev) = 0
      clev = clev - 1
      if ( clev .lt. lowlev ) return
c
1100  continue
c
c     # new level:
c
      if ( clev .eq. hilev ) then
         if ( qdiag .eqv. qeq(hilev) ) call wloop
         go to 1000
      endif
c
      nlev = clev + 1
      r    = range(clev)
      str  = stype(r)
      nexr = nex(str)
1200  continue
c
c     # new extension at the current level:
c
      ext = extbk(clev) + 1
      if ( ext .gt. nexr ) go to 1000
      extbk(clev) = ext
      bcase       = bcasex(ext,str)
      kcase       = kcasex(ext,str)
c
c     # check for extension validity.
c
c     # canonical walk check ( bwalk .lt. kwalk ).
c
      if ( qeq(clev) .and. (bcase .lt. kcase) ) go to 1200
      qeq(nlev) = qeq(clev) .and. (bcase .eq. kcase)
c
c     # individual segment check.
c
      browc = brow(clev)
      brown = l(bcase,browc)
      if ( brown .eq. 0 ) go to 1200
      krowc = krow(clev)
cmd   krown = l(kcase,krowc)
      krown = l2(kcase,krowc)
      if ( krown .eq. 0 ) go to 1200
      brow(nlev) = brown
      krow(nlev) = krown
c
c     # check b values of the bra and ket walks.
c
      bbra = b(brown)
cmd   bket = b(krown)
      bket = b2(krown)
      delb = bket - bbra
c
c     # the following check is equivalent to:
c     #    if ( abs(delb) .gt. 2 ) go to 1200
c
      if ( qbchk(delb) ) go to 1200
c
c     # accumulate the appropriate number of products:
c
      if (numv.eq.1) then 
      v(1,nlev) = v(1,clev) * tab( pt(bcase,kcase,db0(r,1)+delb),bket)
      if( v(1,nlev) .eq. zero ) go to 1200
      elseif (numv.eq.2) then 
      v(1,nlev) = v(1,clev) * tab(pt(bcase,kcase,db0(r,1)+delb),bket)
      v(2,nlev) = v(2,clev) * tab(pt(bcase,kcase,db0(r,2)+delb),bket)
      if(    (v(1,nlev) .eq. zero)
     & .and. (v(2,nlev) .eq. zero) )go to 1200
      else
      v(1,nlev) = v(1,clev) * tab(pt(bcase,kcase,db0(r,1)+delb),bket)
      v(2,nlev) = v(2,clev) * tab(pt(bcase,kcase,db0(r,2)+delb),bket)
      v(3,nlev) = v(3,clev) * tab(pt(bcase,kcase,db0(r,3)+delb),bket)
      if(    (v(1,nlev) .eq. zero)
     & .and. (v(2,nlev) .eq. zero)
     & .and. (v(3,nlev) .eq. zero) )go to 1200
      endif 
c
c     # bra-walk symmetry extension check:
c
      bsymc = bsym(clev)
      do 1410 is = 1, nsym
         jsym = mult(is,bsymc)
         yb(is,nlev) = yb(is,clev) + y(jsym,bcase,browc)
1410  continue
      bsymn = mult( arcsym(bcase,clev), bsymc)
      bsym(nlev) = bsymn
      do 1430 tsym = 1, nsym
         xt = xp(tsym,tail)
         if ( xt .eq. 0 ) go to 1430
         hsym = mult(tsym,bsymn)
         xbh = xbar(hsym,brown)
         if ( xbh .eq. 0 ) go to 1430
         ybt = yb(tsym,nlev)
         zt = z(tsym,tail)
c
         do 1420 j = 1, xt
            if ( limvec(ybt+rev(zt+j)+1) .lt. xbh ) go to 1500
1420     continue
1430  continue
c
c     # ...exit means no valid symmetry extensions.
c
      go to 1200
c
1500  continue
c
c     # ket-walk symmetry extension check:
c
      ksymc = ksym(clev)
      do 1510 is = 1, nsym
         jsym = mult(is,ksymc)
         yk(is,nlev) = yk(is,clev) + y2(jsym,kcase,krowc)
1510  continue
      ksymn = mult( arcsym(kcase,clev), ksymc )
      ksym(nlev) = ksymn
      do 1530 tsym = 1, nsym
cmd
         xt = xp2(tsym,tail)
cmd
         if ( xt .eq. 0 ) go to 1530
         hsym = mult(tsym,ksymn)
cmd
         xbh  = xbar2(hsym,krown)
cmd
         if ( xbh .eq. 0 ) go to 1530
         ykt = yk(tsym,nlev)
cmd
         zt  = z2(tsym,tail)
cmd
c
         do 1520 j = 1, xt
cmd
            if ( limvec2(ykt+rev2(zt+j)+1) .lt. xbh ) go to 1600
cmd
1520     continue
1530  continue
c
c     # ...exit means no valid symmetry extensions.
c
      go to 1200
c
1600  continue
c
c  valid segment extension, increment to the next level:
c
      clev = nlev
      go to 1100
c
      end
c deck wloop
      subroutine wloop
c
c  process loop information and append to the formula tape.
c  written by ron shepard.
c
       implicit none
      integer   nrowmx,        nwlkmx,         namomx
      parameter(nrowmx=2**10-1, nwlkmx=2**20-1, namomx=99)
c
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
cmd
c
      integer
     & b,            l,             y,                   xbar,
     & xp,           z,             arcsym,
     & nj,           nwalk,njskp
      common /cdrt/
     & b(nrowmx),    l(0:3,nrowmx), y(8,0:3,nrowmx),     xbar(8,nrowmx),
     & xp(8,nrowmx), z(8,nrowmx),   arcsym(0:3,0:namomx),
     & nj(0:namomx), njskp(0:namomx),nwalk
c
cmd
      integer
     & b2,            l2,             y2,               xbar2,
     & xp2,           z2,             arcsym2,
     & nj2,           nwalk2,njskp2
      common /cdrt_2/
     & b2(nrowmx),l2(0:3,nrowmx),y2(8,0:3,nrowmx),xbar2(8,nrowmx),
     & xp2(8,nrowmx), z2(8,nrowmx),   arcsym2(0:3,0:namomx),
     & nj2(0:namomx), njskp2(0:namomx),nwalk2
cmd

      real*8  v
      logical qeq
      integer          range,           extbk,
     & brow,           krow,            bsym,            ksym,
     & yb,             yk
      common /cstack/
     & v(3,0:namomx),  range(0:namomx), extbk(0:namomx), qeq(0:namomx),
     & brow(0:namomx), krow(0:namomx),  bsym(0:namomx),  ksym(0:namomx),
     & yb(8,0:namomx), yk(8,0:namomx)
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
      logical      qdiag
      integer             nact, numv, lowlev, hilev, code,    mo,    nmo
      common /cli/ qdiag, nact, numv, lowlev, hilev, code(4), mo(4), nmo
c
      integer        limvec,         rev
      common /cdrt2/ limvec(nwlkmx), rev(nwlkmx)
cmd
      integer        limvec2,         rev2
      common /cdrt2_2/ limvec2(nwlkmx), rev2(nwlkmx)
c
      integer zt1,zt2,j1,j2,xt1,xt2,tsym1,tsym2
c
      integer ssym1,ssym2
      logical symdiff
      common /ssym/ ssym1,ssym2,symdiff
c
      integer imd
cmd

c
      real*8          vtran,        vcoeff
      integer                                  vtpt
      common /cvtran/ vtran(2,2,4), vcoeff(2), vtpt
c
      integer   lbft
      parameter(lbft=2047)
      real*8        ftbuf
      integer                    kntft, nloop, nftb
      common /cftb/ ftbuf(lbft), kntft, nloop, nftb
c
*@ifdef debug
*      logical       qprt
*      common /cprt/ qprt
*@endif
c
      integer nv, icode, it2, it3, head, tail, symw, nsv, tsym, xt,
     & hsym, xbh, ytb, ytk, zt, j, bwalk, kwalk, bwmax, mbk, nw, i
c
      real*8    small
      parameter(small=1d-10)
c
      real*8 val(3)
      integer isv(8),isv2(8)
      integer nv2(3), iv2(2,3), ic2(3), nv3(4), iv3(3,4), ic3(4)
c
      real*8 wword(8)
c
      data nv2 /1,1,2/
      data iv2 /1,2, 2,1, 1,2/
      data ic2 /2,3,1/
      data nv3 /1,2,2,3/
      data iv3 /3,2,1, 2,3,2, 1,3,2, 1,2,3/
      data ic3 /4,3,2,1/
c     # isv(*) is initialized to avoid illegal addresses in encodf().
      data isv /8*1/
      data isv2 /8*1/
c
      go to (1000, 2000, 3000), numv
c
1000  continue
c
c     # one-value loops:
c
      if( abs(v(1,hilev)) .le. small ) return
      nv     = 1
      val(1) = vcoeff(vtpt)*v(1,hilev)
      icode  = code(1)
      go to 4000
c
2000  continue
c
c     # two-value loops:
c     # perform 2x2 transformation from loop values to ft values, and
c     # pick out the non-zero values and appropriate code.
c
      val(1) = vtran(1,1,vtpt) * v(1,hilev)+vtran(2,1,vtpt) * v(2,hilev)
      val(2) = vtran(1,2,vtpt) * v(1,hilev)+vtran(2,2,vtpt) * v(2,hilev)
      it2 = 0
      if ( abs(val(1)) .gt. small ) it2 = it2 + 1
      if ( abs(val(2)) .gt. small ) it2 = it2 + 2
c
c     # it2=0    both values are zero.
c     # it2=1    val(1) .ne. zero, val(2)=zero.
c     # it2=2    val(1)=zero, val(2) .ne. zero.
c     # it2=3    both values are non-zero.
c
      if ( it2 .eq. 0 ) return
      nv     = nv2(it2)
      val(1) = val(iv2(1,it2))
      val(2) = val(iv2(2,it2))
      icode = code(ic2(it2))
      go to 4000
c
3000  continue
c
c     # three-value loops:
c
      it3 = 1
      if ( abs(v(2,hilev)) .gt. small ) it3 = it3 + 1
      if ( abs(v(1,hilev)) .gt. small ) it3 = it3 + 2
c
c     # it3=1    12c   .ne.0.
c     # it3=2    12bc  .ne.0.
c     # it3=3    12ac  .ne.0.
c     # it3=4    12abc .ne.0.
c
      nv     = nv3(it3)
      val(1) = v( iv3(1,it3), hilev)
      val(2) = v( iv3(2,it3), hilev)
      val(3) = v( iv3(3,it3), hilev)
      icode  = code(ic3(it3))
c
4000  continue
c
c     # determine the number of symmetry versions of the loop:
c
      head = brow(hilev)
      tail = brow(lowlev)
      symw = bsym(hilev)
      nsv  = 0
c
cmd
c        do 5040 tsym = 1, nsym
c
c        # determine if symmetry tsym (=lower-walk-symmetry * ssym )
c        # walks contribute to any matrix elements.
c
c        xt = xp(tsym,tail)
c        if ( xt .eq. 0 ) go to 5040
c        hsym = mult(tsym,symw)
c        xbh  = xbar(hsym,head)
c        if ( xbh .eq. 0 ) go to 5040
c
c        # both upper walks from the head and lower walks from the
c        # tail exists for tsym.  use limvec(*) to determine if the
c        # loop actually contributes to some matrix element.
c
c        # loop over lower walks:
c
c        ytb = yb(tsym,hilev)
c        ytk = yk(tsym,hilev)
c        zt  = z(tsym,tail)
c        do 5030 j = 1, xt
c
c           # loop over upper walks using skip vector, limvec(*).
c           # bwalk = ytb + rev(zt+j) + i  ;where i=1 to xbh.
c
c           bwalk = ytb+rev(zt+j) + 1
c           kwalk = ytk+rev(zt+j) + 1
c           bwmax = ytb+rev(zt+j) + xbh
c5010        continue
c           if ( bwalk .gt. bwmax ) go to 5030
c           mbk = max( limvec(bwalk), limvec(kwalk) )
c           if ( mbk .eq. 0 ) then
c              # ...actual loop contribution found:
c              #    note: tsym is monotonic so that isv(*) could be bit
c              #    encoded and packed into 8 bits.
c              nsv = nsv + 1
c              isv(nsv) = tsym
c              go to 5040
c           endif
c           bwalk = bwalk + mbk
c           kwalk = kwalk + mbk
c           go to 5010
c5030     continue
c5040  continue
cmd
         do 5040 tsym1 = 1, nsym

c           if ((ssym1.eq.ssym2).and.(tsym1.ne.tsym2))go to 5040
         if (ssym1.eq.ssym2) then
            tsym2 = tsym1
         else
            imd = mult(ssym1,ssym2)
            tsym2 = mult(imd,tsym1)
         endif
c
c        # determine if symmetry tsym (=lower-walk-symmetry * ssym )
c        # walks contribute to any matrix elements.
c
         xt1 = xp(tsym1,tail)
         xt2 = xp2(tsym2,tail)
         if (( xt1.eq. 0 ).or.( xt2.eq. 0 )) go to 5040
         hsym = mult(tsym1,symw)
         xbh  = xbar(hsym,head)
         if ( xbh .eq. 0 ) go to 5040
c
c        # both upper walks from the head and lower walks from the
c        # tail exists for tsym.  use limvec(*) to determine if the
c        # loop actually contributes to some matrix element.
c
c        # loop over lower walks:
c
         ytb = yb(tsym1,hilev)
         ytk = yk(tsym2,hilev)
         zt1  = z(tsym1,tail)
         zt2  = z2(tsym2,tail)
         do 5030 j1 = 1, xt1
            do 5030 j2 = 1, xt2
c
c           # loop over upper walks using skip vector, limvec(*).
c           # bwalk = ytb + rev(zt+j) + i  ;where i=1 to xbh.
c
            bwalk = ytb+rev(zt1+j2) + 1
            kwalk = ytk+rev2(zt2+j2) + 1
            bwmax = ytb+rev(zt1+j1) + xbh
5010        continue
            if ( bwalk .gt. bwmax ) go to 5030
            mbk = max( limvec(bwalk), limvec2(kwalk) )
            if ( mbk .eq. 0 ) then
c              # ...actual loop contribution found:
c              #    note: tsym is monotonic so that isv(*) could be bit
c              #    encoded and packed into 8 bits.
               nsv = nsv + 1
               isv(nsv) = tsym1
               isv2(nsv) = tsym2
               go to 5040
            endif
            bwalk = bwalk + mbk
            kwalk = kwalk + mbk
            go to 5010
5030     continue
5040  continue
cmd
c
      if ( nsv .eq. 0 ) return
      nloop = nloop + 1
*@ifdef debug
*      if ( qprt ) write(nlist,6300) icode, (val(i),i=1,nv)
*6300  format(' wloop: code=',i4,' val(*)=',3f15.10)
*@endif
c
c  append loop information to ft.
c
c  *note*  formula tape packing is machine dependent:
c
c  packing scheme uses working precision word units.
c  wword(1)             : (isv(i),i=1,nsv),nsv,symw,head,tail,nw,icode
c  wword(2)-wword(1+nv) : (val(i),i=1,nv)
c  wword(2+nv)...       : (yb(i),yk(i),i=1,nsv)
c  resulting in 3 to 8 words used for each loop.
c
      nw = (nv+1) + (nsv+1) / 2 + 1
      call encodf(
     & nv,          nsv,         symw,   head,
     & tail,        icode,       val,    isv,
     & yb(1,hilev), yk(1,hilev), nw,     wword, 
     & isv2, max(nwalk,nwalk2),0)
c
      if ( (kntft + nw) .ge. (lbft - 1) ) call wcode(0)
      do 7030 i = 1, nw
         ftbuf(kntft+i) = wword(i)
7030  continue
      kntft = kntft + nw
c
      return
      end
c deck wcode
      subroutine wcode( icode )
c
c  write icode to formula tape.
c  if icode=0 then code is written and ft buffer is dumped.
c
       implicit none
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
cmd
c
      integer   lbft
      parameter(lbft=2047)
      real*8        ftbuf
      integer                    kntft, nloop, nftb
      common /cftb/ ftbuf(lbft), kntft, nloop, nftb
c
      integer icode
c
      integer nw
      real*8 wword(1)
c
*@ifdef debug
*      write(nlist,6010) icode
*6010  format(' wcode: code=',i4)
*@endif
c
      if ( icode .eq. 0 ) then
         call pcodef( icode, nw, wword )
         kntft = kntft + 1
         ftbuf(kntft) = wword(1)
         call dump(0)
         return
      endif
c
      if( (kntft + 1) .ge. (lbft - 1) )then
c        # ...dump the current buffer before appending icode.
         call pcodef( 0, nw, wword )
         kntft = kntft + 1
         ftbuf(kntft) = wword(1)
         call dump(0)
      endif
c
      call pcodef( icode, nw, wword )
      kntft = kntft + 1
      ftbuf(kntft) = wword(1)
c
      return
      end
c deck dump
      subroutine dump( last )
c
c  store last and current buffer count in the output buffer.
c  dump buffer to formula tape.
c  set buffer count to zero.
c
c  all i/o to the formula tape occurs in this routine.
c
       implicit none
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
cmd
c
      integer   lbft
      parameter(lbft=2047)
      real*8        ftbuf
      integer                    kntft, nloop, nftb
      common /cftb/ ftbuf(lbft), kntft, nloop, nftb
c
      integer last
c
      real*8 wword(1)
      integer iword(2)
c
c  check for empty buffer...
      if ( kntft .eq. 0 ) write(nlist,6010) nfile
6010  format(/' dump: error. kntft=0 on unit=',i3)
c
      iword(1) = last
      iword(2) = kntft
      call plab32( wword, iword, 2 )
      ftbuf(lbft) = wword(1)
      write(nfile) ftbuf
      kntft = 0
      nftb  = nftb + 1
c
      return
      end
c deck ftstat
      subroutine ftstat
c
c  calculate formula tape statistics:
c
c      nloopx(*)= number of loops on the ft.
c
c      nwp(*)   = possible number of walks.
c      ncp(*)   = possible number of matrix contributions.
c
c      nwx(*)   = actual number of walks.
c      ncx(*)   = actual number of matrix contributions.
c
c  if no csf selection has been performed, these two sets of
c  statistics are identical.  after csf selection, the actual
c  walk and contribution totals will be somewhat lower.
c  in the worst case, ncx(*)=nloopx(*) and this procedure is no
c  more efficient than a conventional formula tape method.
c  statistics are summed individually for 1-, 2-, and 3-component
c  loops.
c
c  these statistics could be generated as the formula files are created.
c  they are computed separately so that the expense of formula file
c  processing and generation may be individually evaluated.  this also
c  allows for consistency checks between pcodef() and decodf().
c
c  written 16-aug-84 by ron shepard.
c
       implicit none
      integer   nrowmx,        nwlkmx,         namomx
      parameter(nrowmx=2**10-1, nwlkmx=2**20-1, namomx=99)
c
cmd
      integer        nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
      common /cfiles/nlist,ndrt1,ndrt2,ndft,noft,nfile,nrestp,nin,trsci
cmd
c
      logical      qdiag
      integer             nact, numv, lowlev, hilev, code,    mo,    nmo
      common /cli/ qdiag, nact, numv, lowlev, hilev, code(4), mo(4), nmo
c
      integer
     & b,            l,             y,                   xbar,
     & xp,           z,             arcsym,
     & nj,           nwalk,njskp
      common /cdrt/
     & b(nrowmx),    l(0:3,nrowmx), y(8,0:3,nrowmx),     xbar(8,nrowmx),
     & xp(8,nrowmx), z(8,nrowmx),   arcsym(0:3,0:namomx),
     & nj(0:namomx), njskp(0:namomx),nwalk
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
cmd
      integer
     & b2,            l2,             y2,               xbar2,
     & xp2,           z2,             arcsym2,
     & nj2,           njskp2,nwalk2
      common /cdrt_2/
     & b2(nrowmx),l2(0:3,nrowmx),y2(8,0:3,nrowmx),xbar2(8,nrowmx),
     & xp2(8,nrowmx), z2(8,nrowmx),   arcsym2(0:3,0:namomx),
     & nj2(0:namomx), njskp2(0:namomx),nwalk2
cmd
      integer        limvec,         rev
      common /cdrt2/ limvec(nwlkmx), rev(nwlkmx)
cmd
      integer        limvec2,         rev2
      common /cdrt2_2/ limvec2(nwlkmx), rev2(nwlkmx)
c
      integer ssym1,ssym2
      logical symdiff
      common /ssym/ ssym1,ssym2,symdiff
cmd
c
      integer   lbft
      parameter(lbft=2047)
      real*8        ftbuf
      integer                    kntft, nloop, nftb
      common /cftb/ ftbuf(lbft), kntft, nloop, nftb
c
      integer i, nw, lmo, imo, nv, icode, tail, head, symw, nsv, isv,
     & tsym, xt, hsym, xbh, ytb, zt, j, bwalk, bwmax, mbk, lsym, kmo,
     & klsym, jmo, jklsym, ytk, kwalk
      integer nloopx(4), nwp(4), ncp(4), nwx(4), ncx(4)
      integer ism(8),ism2(8), yb(8), yk(8),spindens
      real*8 gain, val(3)
c
      real*8 rx
      rx(i) = (i)
c
c  diagonal loops:
c
      do 100 i = 1, 4
         nloopx(i) = 0
         nwp(i)    = 0
         ncp(i)    = 0
         nwx(i)    = 0
         ncx(i)    = 0
100   continue
c
      rewind ndft
      kntft = 1
      nw = 0
      read(ndft) ftbuf
c
      do 1400 lmo = 1, nact
         do 1300 imo = 1, lmo
c
1000        continue
            kntft = kntft + nw
            call decodf(
     &       nv,     nsv,    symw,   head,
     &       tail,   icode,  val,    ism,
     &       yb,     yk,     nw,     ftbuf(kntft),
     .       ism2,max(nwalk,nwalk2),spindens )
            if ( icode .eq. 0 ) then
c              # ...new record.
               kntft = 1
               nw    = 0
               read(ndft) ftbuf
               go to 1000
            endif
c
            if ( icode .eq. 1 ) go to 1300
c
c           # determine loop statistics for this loop.
c
            nloopx(nv) = nloopx(nv) + 1
c
            do 1240 isv = 1, nsv
               tsym = ism(isv)
               xt   = xp(tsym,tail)
               hsym = mult(tsym,symw)
               xbh  = xbar(hsym,head)
c
               nwp(nv) = nwp(nv) + xt * xbh
c
c              # loop over lower walks:
c
               ytb = yb(isv)
               zt  = z(tsym,tail)
               do 1230 j = 1, xt
c
c                 # loop over upper walks using skip vector, limvec(*).
c                 # bwalk = ytb + rev(zt+j) + i  ;where i=1 to xbh.
c
                  bwalk = ytb + rev(zt+j) + 1
                  bwmax = ytb + rev(zt+j) + xbh
c                 # do while ( bwalk .le. bwmax ) ...
1210              if ( bwalk .le. bwmax ) then
                     mbk = limvec(bwalk)
                     if ( mbk .eq. 0 ) then
c                       # actual loop contribution found.
                        nwx(nv) = nwx(nv) + 1
                        bwalk   = bwalk   + 1
                        go to 1210
                     endif
                     bwalk = bwalk + mbk
                     go to 1210
                  endif
1230           continue
1240        continue
            go to 1000
1300     continue
1400  continue
c
      ncp(1)    = nwp(1)
      ncp(2)    = 2*nwp(2)
      ncp(3)    = 3*nwp(3)
      ncx(1)    = nwx(1)
      ncx(2)    = 2*nwx(2)
      ncx(3)    = 3*nwx(3)
      nloopx(4) = nloopx(1)+nloopx(2)+nloopx(3)
      nwp(4)    = nwp(1)+nwp(2)+nwp(3)
      ncp(4)    = ncp(1)+ncp(2)+ncp(3)
      nwx(4)    = nwx(1)+nwx(2)+nwx(3)
      ncx(4)    = ncx(1)+ncx(2)+ncx(3)
      gain      = rx(ncx(4))/rx(max(1,nloopx(4)))
      write(nlist,6010) 'diagonal loop statistics:'
      write(nlist,6020) 'number of loops:', nloopx
      write(nlist,6020) 'possible number of walks:', nwp
      write(nlist,6020) 'possible number of matrix contributions:', ncp
      write(nlist,6020) 'actual number of walks:', nwx
      write(nlist,6020) 'actual number of matrix contributions:', ncx
      write(nlist,6030) gain
c
c     # off-diagonal loops:
c
      do 1500 i = 1, 4
         nloopx(i) = 0
         nwp(i)    = 0
         ncp(i)    = 0
         nwx(i)    = 0
         ncx(i)    = 0
1500  continue
c
      rewind noft
      kntft = 1
      nw    = 0
      read(noft) ftbuf
c
c     do 2600 lmo = 1, nact
c        lsym = arcsym(1,lmo-1)
c        do 2500 kmo = 1, lmo
c           klsym = mult(arcsym(1,kmo-1),lsym)
c           do 2400 jmo = 1, kmo
c              jklsym = mult(arcsym(1,jmo-1),klsym)
c              do 2300 imo = 1, jmo
c                 if ( imo .eq. kmo ) go to 2300
c                 if ( arcsym(1,imo-1) .ne. jklsym ) go to 2300
c
      do 2600 lmo = 2, nact
         lsym = arcsym(1,lmo-1)
         do 2300 imo = 1, (lmo-1)
         if ( mult(arcsym2(1,imo-1),ssym2) .ne. lsym ) go to 2300
c
2000              continue
                  kntft = kntft + nw
                  call decodf(
     &             nv,     nsv,    symw,   head,
     &             tail,   icode,  val,    ism,
     &             yb,     yk,     nw,     ftbuf(kntft),ism2,
     &             max(nwalk2,nwalk),spindens )
                  if ( icode .eq. 0 ) then
c                    # ...new record.
                     kntft = 1
                     nw    = 0
                     read(noft) ftbuf
                     go to 2000
                  endif
                  if ( icode .eq. 1 ) go to 2300
c
c                 # determine loop statistics for this loop.
c
                  nloopx(nv) = nloopx(nv) + 1
c
                  do 2240 isv = 1, nsv
                     tsym = ism(isv)
                     xt   = xp(tsym,tail)
                     hsym = mult(tsym,symw)
                     xbh  = xbar(hsym,head)
c
                     nwp(nv) = nwp(nv) + xt * xbh
c
c                    # loop over lower walks:
c
                     ytb = yb(isv)
                     ytk = yk(isv)
                     zt  = z(tsym,tail)
                     do 2230 j = 1, xt
c
c                       # loop over upper walks using skip vector,
c                       # limvec(*).
c                       # bwalk = ytb + rev(zt+j) + i ;where i=1 to xbh.
c
                        bwalk =ytb + rev(zt+j) + 1
                        kwalk =ytk + rev(zt+j) + 1
                        bwmax =ytb + rev(zt+j) + xbh
c                       # do while ( bwalk .le. bwmax) ...
2210                    if ( bwalk .le. bwmax ) then
                           mbk = max( limvec(bwalk), limvec(kwalk) )
                           if ( mbk .eq. 0 ) then
c                             # actual loop contribution found.
                              nwx(nv) = nwx(nv) + 1
                              bwalk   = bwalk   + 1
                              kwalk   = kwalk   + 1
                              go to 2210
                           endif
                           bwalk = bwalk + mbk
                           kwalk = kwalk + mbk
                           go to 2210
                        endif
2230                 continue
2240              continue
                  go to 2000
2300           continue
2400        continue
2500     continue
2600  continue
c
      ncp(1)    = nwp(1)
      ncp(2)    = 2*nwp(2)
      ncp(3)    = 3*nwp(3)
      ncx(1)    = nwx(1)
      ncx(2)    = 2*nwx(2)
      ncx(3)    = 3*nwx(3)
      nloopx(4) = nloopx(1)+nloopx(2)+nloopx(3)
      nwp(4)    = nwp(1)+nwp(2)+nwp(3)
      ncp(4)    = ncp(1)+ncp(2)+ncp(3)
      nwx(4)    = nwx(1)+nwx(2)+nwx(3)
      ncx(4)    = ncx(1)+ncx(2)+ncx(3)
      gain      = rx(ncx(4)) / rx( max( 1, nloopx(4) ) )
      write(nlist,6010) 'off-diagonal loop statistics:'
      write(nlist,6020) 'number of loops:', nloopx
      write(nlist,6020) 'possible number of walks:', nwp
      write(nlist,6020) 'possible number of matrix contributions:', ncp
      write(nlist,6020) 'actual number of walks:', nwx
      write(nlist,6020) 'actual number of matrix contributions:', ncx
      write(nlist,6030) gain
c
      return
c
6010  format(/1x,a/t45,7x,'1v',7x,'2v',7x,'3v',4x,'total')
6020  format(1x,a,t45,4i9)
6030  format(' unitary group approach gain factor=',f9.3)
      end
