MEP program package (MEPPACK)
version 2.3
(c) by Bernhard Sellner 2011

I) The program package contains the following files

    analyze.pl
    apaga
    check_Rtrust.f90
    clmep
    defaults.pm
    def_mod.f90
    general_mod.f90
    guessgeom.f90
    help
    hessian.f90
    makefile
    mepinp.pl
    mwdisp.f90
    NEB.f90
    nebhess.f90
    nml_mod.f90
    orientgeom.f90
    RS_PCO.f90
    runirc.pl
    runmep.pl
    runneb.pl
    template
    units_mod.f90
    zmat.f90
 
    located in the source directory, and the install.pl 
    and README.txt files.

II) For installation, modify the makefile located in the  
    source directory. Afterwards run install.pl.

III)Export $MEP="installdir"/bin 
    For more details run $MEP/help.
