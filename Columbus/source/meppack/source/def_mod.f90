!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
! (c) Bernhard Sellner 2011
! part of MEPPACK

MODULE def_mod

implicit none

contains

subroutine def_ircpar(rpsteps,maxhypstep,inirtrust,Rhyp,&
                 &conv_grad,conv_disp,conv_uwdisp,conv_cons,miciter,&
                 &conv_alpha,conv_rad,soph_l,maxrtrust,minrtrust,lratio,uratio)
! This subroutine sets the default values for ircpar

use prec_mod

implicit none
integer rpsteps,maxhypstep,miciter,inidirect,nmode
real(kind=dpr) inirtrust,Rhyp
real(kind=dpr) conv_grad,conv_disp,conv_uwdisp,conv_cons
real(kind=dpr) conv_alpha,conv_rad,maxrtrust,minrtrust,lratio,uratio
logical soph_l

rpsteps    =   15
maxhypstep =   100
inidirect  =   0
nmode      =   1
inirtrust  =   0.5_dpr
Rhyp       =   0.3_dpr
conv_grad  =   0.0001_dpr
conv_disp  =   0.0001_dpr
conv_uwdisp=   0.0001_dpr
conv_cons  =   0.0001_dpr
miciter    =   40
conv_alpha =   0.0001_dpr
conv_rad   =   0.0001_dpr
soph_l     =   .false.
maxrtrust  =   0.5_dpr
minrtrust  =   0.001_dpr
lratio     =   0.25_dpr
uratio     =   0.75_dpr

end subroutine

subroutine def_nebpar(optdim,licneb,adjneb,initneb,doneb,maxit,nprocneb,&
                 &matfact,tangtype,nebtype,startci,startdiis,maxgdiis,maxddiis,&
                 &nofidiis,bmupd,stepres,scmin,scmax,conv_grad,conv_disp,cipoints)
! This subroutine gets optdim and sets the default values for nebpar
use prec_mod

implicit none
integer optdim,licneb,adjneb,initneb,doneb,maxit,nprocneb,cipoints
integer startci,startdiis,maxgdiis,nofidiis,bmupd,nim
character*6 tangtype,nebtype
real(kind=dpr) matfact,maxddiis,stepres,scmin,scmax,conv_grad,conv_disp
integer lines,filestat

lines = 0
filestat = 0
open(unit=30,file='displfl',status='old',iostat=filestat)
if (filestat > 0) stop "No displfl file for routine def_nebpar"
do while (filestat == 0)
  read(30,*,iostat=filestat)
  if (filestat /= 0) exit
  lines=lines+1
enddo
close(30)

lines=lines-3
optdim=lines-2

!write(unit=1,fmt='((A,I4))')'optdim= ',optdim
licneb     =   0
nim        =   0
adjneb     =   0
initneb    =   0
doneb      =   1
maxit      =   200
nprocneb   =   1
matfact    =  40
tangtype   =   'ITNEB'
nebtype    =   'CINEB'
startci    =   5
startdiis  =   70
maxgdiis   =   10
maxddiis   =   0.2_dpr
nofidiis   =   optdim
bmupd      =   1
stepres    =   0.2_dpr
scmin      =   0.01_dpr
scmax      =   0.01_dpr
conv_grad  =  0.0001_dpr
conv_disp  =  0.0001_dpr
cipoints   =  1

end subroutine

subroutine def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
                      &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
                      &consttyp,fixed_seed,iseed,restart,linconv,hdisp)
! This subroutine sets defaults related to geninp namelist

use prec_mod

implicit none
integer filestat
character*3 typ
integer mem,nproc,printl,prtstep,mld,hessstep,consttyp,iseed
integer restart,linconv,nintc,nat
real(kind=dpr) hdisp
character*6 inihess,inihmod,mixmod,hessmod,elprog,coordprog
logical fixed_seed,chess

!Defaults for irc and neb
mem        = 2000
nproc      = 1
elprog     = 'col'
coordprog  = 'pulay'
nintc      = 0
nat        = 0
printl     = 1
prtstep    = 50
mld        = 1
consttyp   = 1
fixed_seed = .false.
iseed      = 1
restart    = 0
linconv    = 0
hdisp      = 0.001_dpr

filestat=0
open(unit=1239,file='nebpar',status='old',iostat=filestat)
if (filestat.eq.0) then
  typ='neb'
endif
close(1239)
filestat=0
open(unit=1239,file='ircpar',status='old',iostat=filestat)
if (filestat.eq.0) then
  typ='irc'
endif
close(1239)

if (typ.eq.'neb') then
  inihess    = 'unitma'
  inihmod    = 'newh'
  hessmod    = 'update'
  mixmod     = 'const'
  hessstep   = 2000000000
  chess      = .false.
elseif (typ.eq.'irc') then
  inihess    = 'intcfl'
  inihmod    = 'newh'
  hessmod    = 'update'
  mixmod     = 'update'
  hessstep   = 1
  chess      = .true.
! flag for perl : DO NOT REMOVE
endif

end subroutine

END MODULE def_mod
