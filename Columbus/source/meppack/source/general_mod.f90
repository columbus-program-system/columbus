!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
MODULE general_mod
use prec_mod
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This module contains the general input information and subroutines needed in several programs 
! which belong to the MEP program package

character :: outfmt *10 ='(E23.14E3)'

contains

subroutine error_sub(string,errtyp)
use prec_mod

implicit none
character (LEN=*) string
integer errtyp

if (errtyp.eq.1) then
  write(1,fmt='(A)')''
  write (unit=*,fmt='(A,A)')'WARNING: ',string
  write(1,fmt='(A)')''
elseif (errtyp.eq.2) then
  write(1,fmt='(A)')''
  write (unit=*,fmt='(A,A)')'ERROR: ',string
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'ERROR'
  write(1,fmt='(A)')''
  stop
endif

end subroutine


subroutine crossprod(x,y,z)
! This subroutine calculates the 
! cross product of two vectors

use prec_mod
use units_mod
implicit none
real(kind=dpr) x(3),y(3),z(3)

z(1)=x(2)*y(3)-x(3)*y(2)

z(2)=-(x(1)*y(3)-x(3)*y(1))

z(3)=x(1)*y(2)-x(2)*y(1)

end subroutine

subroutine orient(geom1,geom2,geomf,nat,M,printl)
! It orients one geometry to a given reference 
! minimizing the least squares fit. The method 
! using quaternions is implemented.

use prec_mod
use units_mod

implicit none
integer printl
real(kind=dpr) geom3(nat,3),geom2(nat,3),geom1(nat,3),Z(nat),M(nat)
real(kind=dpr) xavg(3),yavg(3),geom4(nat,3),geomf(nat,3)
real(kind=dpr) wvr1(3),wvr2(3),amat(4,4),w(nat),wtot
real(kind=dpr) U(4,4),d(4),e(4),work(:),mtot,wsr2
real(kind=dpr) tau(4),qrot(4),Rq(3,3),disp(3),wsr1
integer i,j,k,l,info,lwork,nat
character*5 version
character version_number,S(nat)
allocatable work

! geom* ... geometries .... [a.u.]
! M ....... atomic mass ... [amu]

w=dsqrt(M)

!get mtot
wtot=0.0_dpr
do i=1,nat
  wtot=wtot+w(i)
enddo

! get average position
xavg=0.0_dpr
yavg=0.0_dpr

do i=1,nat
  xavg=xavg+geom1(i,:)*w(i)
  yavg=yavg+geom2(i,:)*w(i)
enddo
xavg=xavg/wtot
yavg=yavg/wtot

do i=1,nat
  geom3(i,:)=geom1(i,:)-xavg  !x'
  geom4(i,:)=geom2(i,:)-yavg  !y'
enddo

U=0.0_dpr
do i=1,nat
  amat=0.0_dpr
  wvr1=geom4(i,:)+geom3(i,:) !a
  wvr2=geom4(i,:)-geom3(i,:) !b
  amat(1,2:4)=amat(1,2:4)-wvr2
  amat(2:4,1)=amat(2:4,1)+wvr2
  amat(2,3)=amat(2,3)-wvr1(3)
  amat(2,4)=amat(2,4)+wvr1(2)
  amat(3,2)=amat(3,2)+wvr1(3)
  amat(3,4)=amat(3,4)-wvr1(1)
  amat(4,2)=amat(4,2)-wvr1(2)
  amat(4,3)=amat(4,3)+wvr1(1)
  call DGEMM('t','n',4,4,4,w(i),amat,4,amat,4,1.0_dpr,U,4)
enddo

U=U/wtot

!bring into tridiagonal form
info=0
allocate(work(4))
call DSYTRD('U',4,U,4,d,e,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub orient",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DSYTRD('U',4,U,4,d,e,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub orient",2)
call DORGTR('U',4,U,4,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub orient",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DORGTR('U',4,U,4,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub orient",2)
!calculate Eval and Evecs
call DSTEQR('V',4,d,e,U,4,work,info)
if (info.ne.0) call error_sub("Problem with DSTEQR in sub orient",2)
deallocate(work)
!Evec stored by columns
qrot=U(:,1)

if (printl.ge.1) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Eigenvalues [a.u.]:'
  write(1,fmt='(A)')''
  write(1,fmt='(4(A,F6.3))')'b1= ',d(1),'  b2= ',d(2),'  b3= ',d(3),'  b4= ',d(4)
  write(1,fmt='(A)')''
endif

! get rotation matrix
Rq(1,1)=1-2.0_dpr*(qrot(3)**2)-2.0_dpr*(qrot(4)**2)
Rq(1,2)=2.0_dpr*qrot(2)*qrot(3)-2.0_dpr*qrot(1)*qrot(4)
Rq(1,3)=2.0_dpr*qrot(2)*qrot(4)+2.0_dpr*qrot(1)*qrot(3)

Rq(2,1)=2.0_dpr*qrot(3)*qrot(2)+2.0_dpr*qrot(1)*qrot(4)
Rq(2,2)=1-2.0_dpr*(qrot(4)**2)-2.0_dpr*(qrot(2)**2)
Rq(2,3)=2.0_dpr*qrot(3)*qrot(4)-2.0_dpr*qrot(1)*qrot(2)

Rq(3,1)=2.0_dpr*qrot(4)*qrot(2)-2.0_dpr*qrot(1)*qrot(3)
Rq(3,2)=2.0_dpr*qrot(4)*qrot(3)+2.0_dpr*qrot(1)*qrot(2)
Rq(3,3)=1-2.0_dpr*(qrot(2)**2)-2.0_dpr*(qrot(3)**2)

wsr1=calcdet(Rq)

!get displacement
call DGEMV('n',3,3,1.0_dpr,Rq,3,xavg,1,0.0_dpr,wvr1,1)
disp=yavg-wvr1

if (printl.ge.2) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Displacement [a.u.]:'
  write(1,fmt='(A)')''
  write(1,fmt='(3(A,F6.3))')'x=',disp(1),'  y=',disp(2),'  z=',disp(3)
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Rotation matrix:'
  write(1,fmt='(A)')''
  do i=1,3
    write(1,fmt='(3(F6.3,X))')(Rq(i,j),j=1,3)
  enddo
endif

!displace geometry
do i=1,nat
  call DGEMV('n',3,3,1.0_dpr,Rq,3,geom1(i,:),1,0.0_dpr,wvr1,1)
  geomf(i,:)=wvr1+disp
enddo

!calculate new mw distance
wvr1=0.0_dpr
wsr1=0.0_dpr
do i=1,nat
  wvr1=wvr1+((geom2(i,:)-geomf(i,:))*w(i)*au2ang)**2
enddo
wsr1=dsqrt(wvr1(1)+wvr1(2)+wvr1(3))

write(1,fmt='(A)')''
write(1,fmt='(A,X,F16.10)')'MW Distance [A*sqrt(amu)] =',wsr1
write(1,fmt='(A)')''



end subroutine

subroutine builtT(Tcb,Tmti,gradC_q,nintc)
! constructs the spaces used in the procedure
! Tcb is the space of the constraint based on its gradient
! Tmti is the orthogonal complement constructed
! via a Gram Schmidt procedure

! input
! gradC_q ... gradient of the constraint in internal coordinates ... [1]

! output
! Tmti ...... orthogonal complement to constraint .................. [1]
! Tcb ....... space of constraint .................................. [1]

use prec_mod
use units_mod

integer i,j,k,l,nintc
real(kind=dpr) Tcb(nintc,1),Tmti(nintc,(nintc-1)),gradC_q(nintc)
real(kind=dpr) work(nintc),work2(nintc),work3(nintc)

if (norm(gradC_q,nintc).lt. 1.0E-7_dpr) then
  call error_sub('Gradient of constraint too close to Nullvector',2)
endif

Tcb(:,1)=gradC_q/norm(gradC_q,nintc)

do i=2,nintc
  work2=0.0_dpr
  work2(i)=1
  work3=sp(work2,Tcb(:,1),nintc)*Tcb(:,1)
  do j=2,(i-1)
    work3=work3+sp(work2,Tmti(:,(j-1)),nintc)*Tmti(:,(j-1))
  enddo
  Tmti(:,(i-1))=work2-work3
  Tmti(:,(i-1))=Tmti(:,(i-1))/norm(Tmti(:,(i-1)),nintc)
enddo

end subroutine

subroutine uconst_dx(dx,Tmti,hess,gradE_q,gradC_q,Tcb,C_angs,redhess,redgrad,nintc)
! calculates the unconstrained solution in the orthogonal complement
! of the constraint.

! input:
! Tmti ...... orthogonal complement to constraint ............ [1]
! hess ...... hessian ........................................ [aJ/(A^2*amu)]
! gradC_q ... gradient of the constraint in mw int. coord. ... [1]
! gradE_q ... energy gradient in mw internal coordinates ..... [aJ/(A*sqrt(amu))]
! Tcb ....... space of constraint ............................ [1]
! C_angs .... constraint ..................................... [A*sqrt(amu)]

! output
! dx ........ solution ....................................... [A*sqrt(amu)]
! redhess ... reduced hessian ................................ [aJ/(A^2*amu)]
! redgrad ... reduced gradient ............................... [aJ/(A*sqrt(amu))]

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) dx(nintc-1),Tmtit_hess(nintc-1,nintc),hess(nintc,nintc),dy
real(kind=dpr) redhess(nintc-1,nintc-1),workv1(nintc-1),workmat1(nintc-1,1)
real(kind=dpr) Tcb(nintc,1),Tmti(nintc,(nintc-1)),gradE_q(nintc),gradC_q(nintc)
real(kind=dpr) invredhess(nintc-1,nintc-1),C_angs,work(:),redgrad(nintc-1)
integer info,ipiv(:),lwork
allocatable ipiv,work
character*1 n,t

dx=0.0_dpr

! Tmtit_hess=(Tmti)t*hess
call DGEMM('t','n',(nintc-1),nintc,nintc,1.0_dpr,Tmti,nintc,hess,nintc,0.0_dpr,Tmtit_hess,(nintc-1))

! redhess=Tmtit_hess*Tmti
call DGEMM('n','n',(nintc-1),(nintc-1),nintc,1.0_dpr,Tmtit_hess,(nintc-1),Tmti,nintc,0.0_dpr,redhess,(nintc-1))

!invredhess=redhess^-1
allocate(ipiv(nintc-1))
allocate(work(nintc-1))

info=0
invredhess=redhess
call dgetrf((nintc-1),(nintc-1),invredhess,(nintc-1),ipiv,info) !PLU fact. necessary first
if (info.ne.0) call error_sub("Problem with dgetrf in sub uconst_dx",2)
call dgetri((nintc-1),invredhess,(nintc-1),ipiv,work,-1,info)
if (info.ne.0) call error_sub("Problem with dgetri in sub uconst_dx",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call dgetri((nintc-1),invredhess,(nintc-1),ipiv,work,lwork,info)
if (info.ne.0) call error_sub("Problem with dgetri in sub uconst_dx",2)

!  workv1= (Tmti)t*gradE_q
call DGEMV('t',nintc,(nintc-1),1.0_dpr,Tmti,nintc,gradE_q,1,0.0_dpr,workv1,1)

dy=(-C_angs)/(norm(gradC_q,nintc))
!  workv2= dy * Tmtit_hess*Tcb
call DGEMM('n','n',(nintc-1),1,nintc,dy,Tmtit_hess,(nintc-1),Tcb,nintc,0.0_dpr,workmat1,(nintc-1))

workv1=workv1+workmat1(:,1)

redgrad=workv1
! dx= - invredhess * workv1
call DGEMV('n',(nintc-1),(nintc-1),-1.0_dpr,invredhess,(nintc-1),workv1,1,0.0_dpr,dx,1)

end subroutine

subroutine RS_langr(redgrad,redhess,dx,rtrust,miciter,conv_alpha,conv_rad,dimn,printl)
! performing a restricted step optimization on the hypersphere
! using the reduced Hessian and the reduced gradient,
! an optimization is performed iteratively under the constraint
! that the norm of the solution eqals rtrust

! input
! redgrad ...... reduced gradient ............................ [aJ/(A*sqrt(amu))]
! redhess ...... reduced hessian ............................. [aJ/(A^2*amu)]
! rtrust ....... trust radius ................................ [A*sqrt(amu)]
! miciter ...... maximum number of iterations
! conv_alpha ... threshold for alpha ......................... [aJ/(A^2*amu)]
! conv_rad ..... threshold for deviation from rtrust ......... [A*sqrt(amu)]
! dimn ......... dimension of the problem (nintc-1) 

! output
! dx ........... step with length according to restriction ... [A*sqrt(amu)]

use prec_mod
use units_mod

implicit none
integer dimn,printl
real(kind=dpr) redgrad(dimn),redhess(dimn,dimn),dx(dimn)
real(kind=dpr) conv_alpha,conv_rad,rtrust,alpha,work(:),wm1(dimn,dimn)
real(kind=dpr) d(dimn),e(dimn-1),tau(dimn),qmat(dimn,dimn)
real(kind=dpr) f(dimn),du1,oldalpha,wsr1,wvr1(dimn)
integer i,j,k,l,mu,miciter
integer info,lwork
allocatable work
character*1 n,t,U,V

info=0
wm1=redhess
!bring redhess into tridiagonal form
allocate(work(dimn))
call DSYTRD('U',dimn,wm1,dimn,d,e,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub RS_langr",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DSYTRD('U',dimn,wm1,dimn,d,e,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub RS_langr",2)
call DORGTR('U',dimn,wm1,dimn,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub RS_langr",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DORGTR('U',dimn,wm1,dimn,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub RS_langr",2)
!calculate Eval and Evecs
call DSTEQR('V',dimn,d,e,wm1,dimn,work,info)
if (info.ne.0) call error_sub("Problem with DSTEQR in sub RS_langr",2)

if(d(1).lt.0.0_dpr) then
  write(1,fmt='(A)',advance='no')'WARNING: '
  write(1,fmt='(A)')'Reduced hessian is not positive definite'
  write(1,fmt='(A)')''
endif

do i=1,dimn
  f(i)=sp(wm1(:,i),redgrad,dimn)
enddo
! initialize alpha
do i=1,dimn
  wvr1(i)=(dabs(f(i))/rtrust-d(i))
enddo
wsr1=maxval(wvr1)

alpha=max(wsr1,0.0_dpr)

if (printl .ge. 1) then
  write(1,fmt='(A)')'Convergence of restricted step:'
  write(1,fmt='(A)')'   It. ||dx||-Rtrust          Threshold_rad          |a(i-1)-a(i)|          Threshold_alpha'
endif

do mu=1,miciter
  dx=0.0_dpr
  do i=1,dimn
    dx=dx-(f(i)/(d(i)+alpha))*wm1(:,i)
  enddo
  if (mu.eq.1) then
    if (printl .ge. 1) then
      write(1,fmt='(I5)',advance='no')mu
      write(1,fmt=outfmt,advance='no')(dabs(norm(dx,dimn)-rtrust))
      write(1,fmt=outfmt,advance='yes')conv_rad
    endif
    if (dabs(norm(dx,dimn)-rtrust).lt.conv_rad) then
      if (printl .ge. 1) then
        write(1,fmt='(A)')''
        write(1,fmt='(A)',advance='no')'Converged within '
        write(1,fmt='(I5)',advance='no')mu
        write(1,fmt='(A)',advance='yes')' iteration'
        write(1,fmt='(A)')' '
      endif
      print*,'Converged within',mu,'iteration'
      exit
    else
      oldalpha=alpha
      du1=0.0_dpr
      do i=1,dimn
        du1=du1-(f(i)**2/(d(i)+alpha)**3)
      enddo
      du1=du1/norm(dx,dimn)
      alpha=alpha+(1.0_dpr-norm(dx,dimn)/rtrust)*(norm(dx,dimn)/du1)
    endif
  else
    if (printl .ge. 1) then
      write(1,fmt='(I5)',advance='no')mu
      write(1,fmt=outfmt,advance='no')(dabs(norm(dx,dimn)-rtrust))
      write(1,fmt=outfmt,advance='no')conv_rad
      write(1,fmt=outfmt,advance='no')dabs(oldalpha-alpha)
      write(1,fmt=outfmt,advance='yes')conv_alpha
    endif
    if ((dabs(norm(dx,dimn)-rtrust).lt.conv_rad).and.(dabs(oldalpha-alpha).lt.conv_alpha)) then
      if (printl .ge. 1) then
        write(1,fmt='(A)')''
        write(1,fmt='(A)',advance='no')'Converged within '
        write(1,fmt='(I5)',advance='no')mu
        write(1,fmt='(A)',advance='yes')' iterations'
        write(1,fmt='(A)')' '
      endif
      exit
    else
      oldalpha=alpha
      du1=0.0_dpr
      do i=1,dimn
        du1=du1-(f(i)**2/(d(i)+alpha)**3)
      enddo
      du1=du1/norm(dx,dimn)
      alpha=alpha+(1.0_dpr-norm(dx,dimn)/rtrust)*(norm(dx,dimn)/du1)
    endif
  endif
  if (mu.eq.miciter) then
    write(1,fmt='(A)',advance='no')'Not converged within'
    write(1,fmt='(I5)',advance='no')mu
    write(1,fmt='(A)',advance='yes')'iterations'
    write(1,fmt='(A)')''
    call error_sub("Not converged, increase max micro iterations.",2)
  endif
enddo

end subroutine

subroutine springcons(c_E,optdim,scons,scmin,scmax)
! This subroutine calculates the spring constants.

use prec_mod
use units_mod

implicit none
integer i,j,k,l,ii,optdim
real(kind=dpr) c_E(0:optdim+1)
real(kind=dpr) scons(optdim)
real(kind=dpr) wsr1,wsr2,scmin,scmax

! scons ... array of spring constants ........... [aJ/(A^2*amu)]
! c_E ..... array of energies ................... [aJ]
! scmin ... minimal value for spring constant ... [aJ/(A^2*amu)] 
! scmax ... maximal value for spring constant ... [aJ/(A^2*amu)]

! spring constants
scons=0.0_dpr

if (dabs(scmin-scmax).lt.1E-5) then
  do i=1,optdim
    scons(i)=scmin
  enddo
else
  do i=1,optdim
    ! set E0
    wsr1=max(c_E(0),c_e(optdim+1))
    ! set Emax
    wsr2=maxval(c_E)
    if (c_E(i).gt.wsr1) then
      ! original implementation
      !scons(i)=scmax-scmin*((wsr2-c_E(i))/(wsr2-wsr1))
      ! Henkelmann
      scons(i)=scmax-(scmax-scmin)*((wsr2-c_E(i))/(wsr2-wsr1))
    else
      ! original implementation
      !scons=scmax-scmin
      ! Henkelmann
      scons=scmin
    endif
  enddo
endif

end subroutine

subroutine tangcons_cart(c_E,geom,optdim,scons,tang,M,scmin,scmax,imdim,tangtype,printl,nat)
! This subroutine calculates the tangent for each image.
! Cartesian coordinate version

use prec_mod
use units_mod

implicit none
integer i,j,k,l,ii,optdim,it,imdim,printl,nat
real(kind=dpr) c_E(0:optdim+1),geom(0:optdim+1,imdim)
real(kind=dpr) tang(optdim,imdim),scons(optdim),wvr1(imdim)
real(kind=dpr) wsr1,wsr2,scmin,scmax,angles(optdim),wvr2(imdim)
real(kind=dpr) angl(optdim,optdim)
real(kind=dpr) M(0:optdim+1,nat)
real(kind=dpr) sprgrd(optdim)
integer filestat
character*6 tangtype

! scons ... array of spring constants ........... [aJ/(A^2*amu)]
! c_E ..... array of energies ................... [aJ]
! geom .... array of cart. geometry ............. [A*sqrt(amu)]
! tang  ... array of tangents ................... [1]
! M ....... atomic mass ......................... [amu]
! scmin ... minimal value for spring constant ... [aJ/(A^2*amu)] 
! scmax ... maximal value for spring constant ... [aJ/(A^2*amu)]

filestat=0

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: geometries for construction of tangents'
  write(1,fmt='(A)')''
  do ii=0,(optdim+1)
    write(1,fmt='(A)')'Cartesian geometry [A*sqrt(amu)]'
    do i=1,nat-1
      write(1,fmt='(3(X,F14.8))')geom(ii,(i-1)*3+1),geom(ii,(i-1)*3+2),geom(ii,(i-1)*3+3)
    enddo
    write(1,fmt='(3(X,F14.8))')geom(ii,(nat-1)*3+1),geom(ii,(nat-1)*3+2),geom(ii,(nat-1)*3+3)
    write(1,fmt='(A)')''
  enddo
endif

! Tangents
tang=0.0_dpr

if (tangtype .eq. 'BNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Tangent construction: BNEB'
  write(1,fmt='(A)')''

  ! bisect tangent
  do i=1,optdim
    wvr1=geom(i,:)-geom(i-1,:)
    tang(i,:)=wvr1/norm(wvr1,imdim)

    wvr1=geom(i+1,:)-geom(i,:)
    tang(i,:)=tang(i,:)+wvr1/norm(wvr1,imdim)
  enddo
elseif (tangtype .eq. 'ITNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Tangent construction: ITNEB'
  write(1,fmt='(A)')''
  do i=1,optdim
    if ((c_E(i+1).gt.c_E(i)).and.(c_E(i).ge.c_E(i-1))) then
      tang(i,:)=geom(i+1,:)-geom(i,:) ! acc to paper

      if (printl.ge.2) then
        write(1,fmt='(A,I8)')'Image ',i
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'uphill'
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'Tangent '
        do ii=1,(imdim-1)
          write(1,fmt=outfmt)tang(i,ii)
        enddo
        write(1,fmt=outfmt)tang(i,imdim)
        write(1,fmt='(A)')''
      endif
    elseif ((c_E(i+1).le.c_E(i)).and.(c_E(i).lt.c_E(i-1))) then
      tang(i,:)=geom(i,:)-geom(i-1,:)  ! acc to paper

      if (printl.ge.2) then
        write(1,fmt='(A,I8)')'Image ',i
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'downhill'
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'Tangent '
        do ii=1,(imdim-1)
          write(1,fmt=outfmt)tang(i,ii)
        enddo
        write(1,fmt=outfmt)tang(i,imdim)
        write(1,fmt='(A)')''
      endif
    elseif (((c_E(i+1).gt.c_E(i)).and.(c_E(i).lt.c_E(i-1))).or.&
           &((c_E(i+1).lt.c_E(i)).and.(c_E(i).gt.c_E(i-1)))) then
      wsr1=max(dabs(c_E(i+1)-c_E(i)),dabs((c_E(i)-c_E(i-1))))
      wsr2=min(dabs(c_E(i+1)-c_E(i)),dabs((c_E(i)-c_E(i-1))))
      wvr1=geom(i,:)-geom(i-1,:)
      wvr2=geom(i+1,:)-geom(i,:)
      if (c_E(i+1).gt.c_E(i-1)) then
        tang(i,:)=wvr2*wsr1+wvr1*wsr2
      else
        tang(i,:)=wvr2*wsr2+wvr1*wsr1
      endif
      if (printl.ge.2) then
        write(1,fmt='(A,I8)')'Image ',i
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'minmax'
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'Tangent '
        do ii=1,(imdim-1)
          write(1,fmt=outfmt)tang(i,ii)
        enddo
        write(1,fmt=outfmt)tang(i,imdim)
        write(1,fmt='(A)')''
      endif 
    endif
  enddo
endif

!normalize
do i=1,optdim
  tang(i,:)=tang(i,:)/norm(tang(i,:),imdim)
enddo

!angle between images
do i=1,optdim
  wvr1=geom(i,:)-geom(i-1,:)
  wvr2=geom(i,:)-geom(i+1,:)
  wsr1=sp(wvr1,wvr2,imdim)
  wsr1=wsr1/(norm(wvr1,imdim)*norm(wvr2,imdim))
  if (wsr1.lt.-1.01_dpr) then
    call error_sub("Problem evaluating angle",1)
  elseif (wsr1.lt.-1.00_dpr) then
    wsr1=-1.0_dpr
  endif
  wsr1=dacos(wsr1)*(360.0_dpr/(2.0_dpr*pi))
  angles(i)=wsr1 
enddo

wsr1=minval(angles)
if (wsr1.lt.120) then
  if (printl.ge.1) then
    write(*,fmt='(A)')''
    write(*,fmt='(A)')'Critical angles between adjacent images'
    do ii=1,optdim
      if (angles(ii).lt.120) then
        write(*,fmt='(A,I8,3X,A,F8.3)')'Image ',ii,' angle= ',angles(ii)
      endif
    enddo
    write(*,fmt='(A)')''
  endif
endif

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: angles between adjacent images'
  write(1,fmt='(A)')''
  do ii=1,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt=outfmt)angles(ii)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 1) then
  open(unit=27,file='see_angles.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_angles.dat in sub tangcons_cart",2)
  do i=1,(optdim-1)
    write(27,fmt='(F5.1,X)',advance='no')angles(i)
  enddo
  write(27,fmt='(F5.1,X)',advance='yes')angles(optdim)
  close(27)
endif

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for sub tangcons_cart",2)
  read(9,fmt=*)it
close(9)

if (printl .ge. 1) then
  open(unit=27,file='see_angles3D.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_angles3D.dat in sub tangcons_cart",2)
  do i=1,optdim
    write(27,fmt='(I8,X,I8,X,E23.14E3,X)')it,i,angles(i)
  enddo
  close(27)
endif

call springcons(c_E,optdim,scons,scmin,scmax)

!angle between tangents
angles=0.0_dpr
do i=2,optdim
  wvr1=scons(i)*tang(i,:)
  wvr2=scons(i-1)*tang(i-1,:)
  wsr1=sp(wvr1,wvr2,imdim)
  wsr1=wsr1/(norm(wvr1,imdim)*norm(wvr2,imdim))
  wsr1=dacos(wsr1)*(360.0_dpr/(2.0_dpr*pi))
  angles(i)=wsr1 
enddo

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: angles between adjacent tangents'
  write(1,fmt='(A)')''
  do ii=2,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt=outfmt)angles(ii)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_tangangles.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_tangangles.dat in sub tangcons_cart",2)
  do i=2,(optdim-1)
    write(27,fmt='(F12.6,X)',advance='no')angles(i)
  enddo
  write(27,fmt='(F12.6,X)',advance='yes')angles(optdim)
  close(27)
endif

end subroutine

subroutine tangcons(c_E,intgeom,optdim,scons,tang,bm,M,scmin,scmax,nintc,nat,tangtype,printl,consttyp)
! calculates the tangent for each image. 
! Internal coordinate version

use prec_mod
use units_mod

implicit none
integer i,j,k,l,ii,optdim,it,nintc,nat,printl,consttyp
real(kind=dpr) c_E(0:optdim+1),intgeom(0:optdim+1,nintc)
real(kind=dpr) tang(optdim,nintc),scons(optdim),wvr1(nintc)
real(kind=dpr) wsr1,wsr2,scmin,scmax,angles(optdim),wvr2(nintc)
real(kind=dpr) angl(optdim,optdim),Gn12(nintc,nintc),Gp12(nintc,nintc)
real(kind=dpr) bm(0:optdim+1,3*nat,nintc),M(0:optdim+1,nat)
real(kind=dpr) wbm1(3*nat,nintc),sprgrd(optdim)
integer filestat
character*6 tangtype

! c_E ....... array of energies ................... [aJ]
! intgeom ... internal geometry ................... [A|rad] 
! scons ..... array of spring constants ........... [aJ/(A^2*amu)]
! tang  ..... array of tangents ................... [1]
! M ......... atomic mass ......................... [amu]
! bm ........ array of bmatrices .................. [A|rad/A]
! scmin ..... minimal value for spring constant ... [aJ/(A^2*amu)] 
! scmax ..... maximal value for spring constant ... [aJ/(A^2*amu)]

filestat=0

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: internal geometries for construction of tangents'
  write(1,fmt='(A)')''
  do ii=0,(optdim+1)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Internal geometry'
    do i=1,(nintc-1)
      write(1,fmt=outfmt)intgeom(ii,i)
    enddo
    write(1,fmt=outfmt)intgeom(ii,nintc)
    write(1,fmt='(A)')''
  enddo
endif

! Tangents
tang=0.0_dpr

if (tangtype .eq. 'BNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Tangent construction: BNEB'

  ! bisect tangent
  do i=1,optdim
    wvr1=intgeom(i,:)-intgeom(i-1,:)
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if (consttyp .eq. 1) then 
      call mwdpl(wvr1,Gn12,nintc)
    endif
    tang(i,:)=wvr1/norm(wvr1,nintc)

    wvr1=intgeom(i+1,:)-intgeom(i,:)
    if (consttyp .eq. 1) then
      call mwdpl(wvr1,Gn12,nintc)
    endif
    tang(i,:)=tang(i,:)+wvr1/norm(wvr1,nintc)
  enddo
elseif (tangtype .eq. 'ITNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Tangent construction: ITNEB'
  do i=1,optdim
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if ((c_E(i+1).gt.c_E(i)).and.(c_E(i).ge.c_E(i-1))) then
      tang(i,:)=intgeom(i+1,:)-intgeom(i,:) ! acc to paper
      if (consttyp .eq. 1) then
        call mwdpl(tang(i,:),Gn12,nintc)
      endif

      if (printl.ge.2) then
        write(1,fmt='(A,I8)')'Image ',i
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'uphill'
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'Tangent '
        do ii=1,(nintc-1)
          write(1,fmt=outfmt)tang(i,ii)
        enddo
        write(1,fmt=outfmt)tang(i,nintc)
        write(1,fmt='(A)')''
      endif
    elseif ((c_E(i+1).le.c_E(i)).and.(c_E(i).lt.c_E(i-1))) then
      tang(i,:)=intgeom(i,:)-intgeom(i-1,:)  ! acc to paper
      if (consttyp .eq. 1) then
        call mwdpl(tang(i,:),Gn12,nintc)
      endif

      if (printl.ge.2) then
        write(1,fmt='(A,I8)')'Image ',i
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'downhill'
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'Tangent '
        do ii=1,(nintc-1)
          write(1,fmt=outfmt)tang(i,ii)
        enddo
        write(1,fmt=outfmt)tang(i,nintc)
        write(1,fmt='(A)')''
      endif
    elseif (((c_E(i+1).gt.c_E(i)).and.(c_E(i).lt.c_E(i-1))).or.((c_E(i+1).lt.c_E(i)).and.(c_E(i).gt.c_E(i-1)))) then
      wsr1=max(dabs(c_E(i+1)-c_E(i)),dabs((c_E(i)-c_E(i-1))))
      wsr2=min(dabs(c_E(i+1)-c_E(i)),dabs((c_E(i)-c_E(i-1))))
      wvr1=intgeom(i,:)-intgeom(i-1,:)
      wvr2=intgeom(i+1,:)-intgeom(i,:)
      if (consttyp .eq. 1) then
        call mwdpl(wvr1,Gn12,nintc)
        call mwdpl(wvr2,Gn12,nintc)
      endif
      if (c_E(i+1).gt.c_E(i-1)) then
        tang(i,:)=wvr2*wsr1+wvr1*wsr2
      else
        tang(i,:)=wvr2*wsr2+wvr1*wsr1
      endif
      if (printl.ge.2) then
        write(1,fmt='(A,I8)')'Image ',i
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'minmax'
        write(1,fmt='(A)')''
        write(1,fmt='(A)')'Tangent '
        do ii=1,(nintc-1)
          write(1,fmt=outfmt)tang(i,ii)
        enddo
        write(1,fmt=outfmt)tang(i,nintc)
        write(1,fmt='(A)')''
      endif 
    endif
  enddo
endif

!normalize
do i=1,optdim
  tang(i,:)=tang(i,:)/norm(tang(i,:),nintc)
enddo

!angle between images
do i=1,optdim
  wvr1=intgeom(i,:)-intgeom(i-1,:)
  wvr2=intgeom(i,:)-intgeom(i+1,:)
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if (consttyp .eq. 1) then
      call mwdpl(wvr1,Gn12,nintc)
      call mwdpl(wvr2,Gn12,nintc)
    endif
  wsr1=sp(wvr1,wvr2,nintc)
  wsr1=wsr1/(norm(wvr1,nintc)*norm(wvr2,nintc))
  if (wsr1.lt.-1.01_dpr) then
    call error_sub("Problem evaluating angle",1)
  elseif (wsr1.lt.-1.00_dpr) then
    wsr1=-1.0_dpr
  endif
  wsr1=dacos(wsr1)*(360.0_dpr/(2.0_dpr*pi))
  angles(i)=wsr1 
enddo

wsr1=minval(angles)
if (wsr1.lt.120) then
  if (printl.ge.1) then
    write(*,fmt='(A)')''
    write(*,fmt='(A)')'Critical angles between adjacent images'
    do ii=1,optdim
      if (angles(ii).lt.120) then
        write(*,fmt='(A,I8,3X,A,F8.3)')'Image ',ii,' angle= ',angles(ii)
      endif
    enddo
    write(*,fmt='(A)')''
  endif
endif

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: angles between adjacent images'
  write(1,fmt='(A)')''
  do ii=1,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt=outfmt)angles(ii)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 1) then
  open(unit=27,file='see_angles.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_angles.dat in sub tangcons",2)
  do i=1,(optdim-1)
    write(27,fmt='(F5.1,X)',advance='no')angles(i)
  enddo
  write(27,fmt='(F5.1,X)',advance='yes')angles(optdim)
  close(27)
endif

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for sub tangcons",2)
  read(9,fmt=*)it
close(9)

if (printl .ge. 1) then
  open(unit=27,file='see_angles3D.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_angles3D.dat in sub tangcons",2)
  do i=1,optdim
    write(27,fmt='(I8,X,I8,X,E23.14E3,X)')it,i,angles(i)
  enddo
  close(27)
endif

call springcons(c_E,optdim,scons,scmin,scmax)

!angle between tangents
angles=0.0_dpr
do i=2,optdim
  wvr1=scons(i)*tang(i,:)
  wvr2=scons(i-1)*tang(i-1,:)
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if (consttyp .eq. 1) then
      call mwdpl(wvr1,Gn12,nintc)
      call mwdpl(wvr2,Gn12,nintc)
    endif
  wsr1=sp(wvr1,wvr2,nintc)
  wsr1=wsr1/(norm(wvr1,nintc)*norm(wvr2,nintc))
  wsr1=dacos(wsr1)*(360.0_dpr/(2.0_dpr*pi))
  angles(i)=wsr1 
enddo

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: angles between adjacent tangents'
  write(1,fmt='(A)')''
  do ii=2,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt=outfmt)angles(ii)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_tangangles.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_tangangles.dat in sub tangcons",2)
  do i=2,(optdim-1)
    write(27,fmt='(F12.6,X)',advance='no')angles(i)
  enddo
  write(27,fmt='(F12.6,X)',advance='yes')angles(optdim)
  close(27)
endif

end subroutine

subroutine adjgrd_cart(geom,scons,grd,tang,gradE,gradspr,c_E,optdim,M,imdim,&
                      &tangtype,printl,startci,nebtype,cipoints)
! This subroutine calculates the adjusted gradient:
! parallel component of the spring gradient and
! perpendicular component of the true gradient

use prec_mod
use units_mod

implicit none
integer optdim,imdim,printl,startci,cipoints
real(kind=dpr) geom(0:optdim+1,imdim),scons(optdim),grd(0:optdim+1,imdim),tang(optdim,imdim)
real(kind=dpr) gradE(optdim,imdim),gradspr(optdim,imdim),wvr1(imdim),wvr2(imdim),wsr1,c_E(0:optdim+1)
real(kind=dpr) sprf(optdim)
real(kind=dpr) M(0:optdim+1,(imdim/3)),wsr2,angles(optdim)
real(kind=dpr) sprgrd(optdim),norms(optdim),disti(2,optdim)
integer i,j,k,l,ii,it
integer filestat
character*6 tangtype,nebtype

! scons ..... spring constants .......... [aJ/(A^2*amu)]
! c_E ....... energies .................. [aJ]
! geom ...... cart. geometry ............ [A*sqrt(amu)]
! tang  ..... tangents .................. [1]
! M ......... atomic mass ............... [amu]
! grd ....... cartesian E grad .......... [aJ/(A*sqrt(amu))]
! gradE ..... projected cart. E grad .... [aJ/(A*sqrt(amu))]
! gradspr ... projected spring grad ..... [aJ/(A*sqrt(amu))]

filestat=0
gradE=0.0_dpr
gradspr=0.0_dpr

! gradient of the spring
if (tangtype .eq. 'BNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adjustment of gradients according to BNEB method'
  write(1,fmt='(A)')''

  do i=1,optdim
    wvr1=geom(i+1,:)-geom(i,:)
    wvr2=geom(i,:)-geom(i-1,:)
    disti(2,i)=norm(wvr1,imdim)
    disti(1,i)=norm(wvr2,imdim)

    wsr1=scons(i)*(sp(wvr1,tang(i,:),imdim)-sp(wvr2,tang(i,:),imdim))
    gradspr(i,:)=-wsr1*tang(i,:)

    wsr1=(norm(wvr1,imdim)-norm(wvr2,imdim))
    norms(i)=wsr1
  enddo
elseif (tangtype.eq. 'ITNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adjustment of gradients according to ITNEB method'
  write(1,fmt='(A)')''

  do i=1,optdim
    wvr1=geom(i+1,:)-geom(i,:)
    wvr2=geom(i,:)-geom(i-1,:)
    disti(2,i)=norm(wvr1,imdim)
    disti(1,i)=norm(wvr2,imdim)
    wsr1=(norm(wvr1,imdim)-norm(wvr2,imdim))
    norms(i)=wsr1
    gradspr(i,:)=-1.0_dpr*scons(i)*(norm(wvr1,imdim)-norm(wvr2,imdim))*tang(i,:)

    wvr2=1.0_dpr*scons(i)*norm(wvr1,imdim)*tang(i,:)
    sprgrd(i)=norm(wvr2,imdim)
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_imgdist.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_imgdist.dat in sub adjgrd_cart",2)
  do i=1,optdim,2
    do j=1,2
      write(27,fmt='(F10.6,X)',advance='no')disti(j,i)
    enddo
  enddo
  write(27,fmt='(A)',advance='yes')''
  write(27,fmt='(11X,A)',advance='no')''
  do i=2,optdim,2
    do j=1,2
      write(27,fmt='(F10.6,X)',advance='no')disti(j,i)
    enddo
  enddo
  close(27)
endif

if (printl .ge. 2) then
  open(unit=27,file='see_spacing.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spacing.dat in sub adjgrd_cart",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')norms(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')norms(optdim)
  close(27)
endif

do i=1,optdim
  sprf(i)=norm(gradspr(i,:),imdim)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_springgrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_springgrad.dat in sub adjgrd_cart",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')sprf(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')sprf(optdim)
  close(27)
endif

if (printl .ge. 2) then
  open(unit=27,file='see_spring.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spring.dat in sub adjgrd_cart",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')sprgrd(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')sprgrd(optdim)
  close(27)
endif

!angle between gradients
angles=0.0_dpr
do i=2,optdim
  wvr1=scons(i)*gradspr(i,:)
  wvr2=scons(i-1)*gradspr(i-1,:)
  wsr1=sp(wvr1,wvr2,imdim)
  wsr1=wsr1/(norm(wvr1,imdim)*norm(wvr2,imdim))
  wsr1=dacos(wsr1)*(360.0_dpr/(2.0_dpr*pi))
  angles(i)=wsr1 
enddo

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: angles between adjacent gradients'
  write(1,fmt='(A)')''
  do ii=2,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt=outfmt)angles(ii)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_gradangles.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_gradangles.dat in sub adjgrd_cart",2)
  do i=2,(optdim-1)
    write(27,fmt='(F12.6,X)',advance='no')angles(i)
  enddo
  write(27,fmt='(F12.6,X)',advance='yes')angles(optdim)
  close(27)
endif

! true gradient
open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for sub adjgrd_cart",2)
  read(9,fmt=*)it
close(9)

do i=1,optdim
  if ((it.ge.startci).and.(nebtype.eq.'CINEB')) then
    if (((c_E(i+1).lt.c_E(i)).and.(c_E(i).gt.c_E(i-1))).and.&
      &((cipoints.eq.0).or.((cipoints.eq.1).and.(c_E(i).eq.maxval(c_E))))) then
      write(1,fmt='(A,I10)')'CINEB on image',i
      write(1,fmt='(A)')''
      gradspr(i,:)=0.0_dpr
      gradE(i,:)=0.0_dpr
      wvr1=grd(i,:)-2.0_dpr*sp(grd(i,:),tang(i,:),imdim)*tang(i,:)
    else
      wvr1=grd(i,:)-sp(grd(i,:),tang(i,:),imdim)*tang(i,:)
    endif
  else
    wvr1=grd(i,:)-sp(grd(i,:),tang(i,:),imdim)*tang(i,:)
  endif
  grd(i,:)=wvr1
  gradE(i,:)=wvr1
enddo

end subroutine

subroutine adjgrd(intgeom,scons,intgrd,tang,gradE,gradspr,c_E,optdim,bm,M,nintc,&
                 &nat,tangtype,printl,startci,nebtype,consttyp,cipoints)
! This subroutine calculates the adjusted gradient:
! parallel component of the spring gradient and
! perpendicular component of the true gradient

use prec_mod
use units_mod

implicit none
integer optdim,nintc,nat,printl,startci,consttyp,cipoints
real(kind=dpr) intgeom(0:optdim+1,nintc),scons(optdim),intgrd(0:optdim+1,nintc),tang(optdim,nintc)
real(kind=dpr) gradE(optdim,nintc),gradspr(optdim,nintc),wvr1(nintc),wvr2(nintc),wsr1,c_E(0:optdim+1)
real(kind=dpr) Gn12(nintc,nintc),Gp12(nintc,nintc),sprf(optdim),wbm1(3*nat,nintc)
real(kind=dpr) bm(0:optdim+1,3*nat,nintc),M(0:optdim+1,nat),wsr2,angles(optdim)
real(kind=dpr) sprgrd(optdim),norms(optdim),disti(2,optdim)
integer i,j,k,l,ii,it
integer filestat
character*6 tangtype,nebtype

! intgeom ... internal geometry ....... [A|rad]
! scons ..... spring constants ........ [aJ/(A^2*amu)]
! c_E ....... energies ................ [aJ]
! tang  ..... tangents ................ [1]
! M ......... atomic mass ............. [amu]
! intgrd .... internal E grad ......... [aJ/(A*sqrt(amu))]
! gradE ..... projected int. E grad ... [aJ/(A*sqrt(amu))]
! gradspr ... projected spring grad ... [aJ/(A*sqrt(amu))]
! bm ........ array of bmatrices ...... [A|rad/A]
! Gn12 ...... =G^(-1/2) ............... [(sqrt(amu)*A)/(A|rad)]
! Gp12 ...... =G^(+1/2) ............... [(A|rad/(sqrt(amu)*A)]

filestat=0
gradE=0.0_dpr
gradspr=0.0_dpr

! gradient of the spring
if (tangtype .eq. 'BNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adjustment of gradients according to BNEB method'
  write(1,fmt='(A)')''

  do i=1,optdim
    wvr1=intgeom(i+1,:)-intgeom(i,:)
    wvr2=intgeom(i,:)-intgeom(i-1,:)
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if (consttyp .eq. 1) then
      call mwdpl(wvr1,Gn12,nintc)
      call mwdpl(wvr2,Gn12,nintc)
    endif
    disti(2,i)=norm(wvr1,nintc)
    disti(1,i)=norm(wvr2,nintc)

    wsr1=scons(i)*(sp(wvr1,tang(i,:),nintc)-sp(wvr2,tang(i,:),nintc))
    gradspr(i,:)=-wsr1*tang(i,:)

    wsr1=(norm(wvr1,nintc)-norm(wvr2,nintc))
    norms(i)=wsr1
  enddo
elseif (tangtype.eq. 'ITNEB') then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adjustment of gradients according to ITNEB method'
  write(1,fmt='(A)')''

  do i=1,optdim
    wvr1=intgeom(i+1,:)-intgeom(i,:)
    wvr2=intgeom(i,:)-intgeom(i-1,:)
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if (consttyp .eq. 1) then
      call mwdpl(wvr2,Gn12,nintc)
    endif
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if (consttyp .eq. 1) then
      call mwdpl(wvr1,Gn12,nintc)
    endif
    disti(2,i)=norm(wvr1,nintc)
    disti(1,i)=norm(wvr2,nintc)
    wsr1=(norm(wvr1,nintc)-norm(wvr2,nintc))
    norms(i)=wsr1
    gradspr(i,:)=-1.0_dpr*scons(i)*(norm(wvr1,nintc)-norm(wvr2,nintc))*tang(i,:)

    wvr2=1.0_dpr*scons(i)*norm(wvr1,nintc)*tang(i,:)
    sprgrd(i)=norm(wvr2,nintc)
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_imgdist.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_imgdist.dat in sub adjgrd",2)
  do i=1,optdim,2
    do j=1,2
      write(27,fmt='(F10.6,X)',advance='no')disti(j,i)
    enddo
  enddo
  write(27,fmt='(A)',advance='yes')''
  write(27,fmt='(11X,A)',advance='no')''
  do i=2,optdim,2
    do j=1,2
      write(27,fmt='(F10.6,X)',advance='no')disti(j,i)
    enddo
  enddo
  close(27)
endif

if (printl .ge. 2) then
  open(unit=27,file='see_spacing.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spacing.dat in sub adjgrd",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')norms(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')norms(optdim)
  close(27)
endif

do i=1,optdim
  sprf(i)=norm(gradspr(i,:),nintc)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_springgrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_springgrad.dat in sub adjgrd",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')sprf(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')sprf(optdim)
  close(27)
endif

if (printl .ge. 2) then
  open(unit=27,file='see_spring.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spring.dat in sub adjgrd",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')sprgrd(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')sprgrd(optdim)
  close(27)
endif

!angle between gradients
angles=0.0_dpr
do i=2,optdim
  wvr1=scons(i)*gradspr(i,:)
  wvr2=scons(i-1)*gradspr(i-1,:)
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    if (consttyp .eq. 1) then
      call mwdpl(wvr1,Gn12,nintc)
      call mwdpl(wvr2,Gn12,nintc)
    endif
  wsr1=sp(wvr1,wvr2,nintc)
  wsr1=wsr1/(norm(wvr1,nintc)*norm(wvr2,nintc))
  wsr1=dacos(wsr1)*(360.0_dpr/(2.0_dpr*pi))
  angles(i)=wsr1 
enddo

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: angles between adjacent gradients'
  write(1,fmt='(A)')''
  do ii=2,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt=outfmt)angles(ii)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_gradangles.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_gradangles.dat in sub adjgrd",2)
  do i=2,(optdim-1)
    write(27,fmt='(F12.6,X)',advance='no')angles(i)
  enddo
  write(27,fmt='(F12.6,X)',advance='yes')angles(optdim)
  close(27)
endif

! true gradient
open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for sub adjgrd",2)
  read(9,fmt=*)it
close(9)

do i=1,optdim
  if ((it.ge.startci).and.(nebtype.eq.'CINEB')) then
    if (((c_E(i+1).lt.c_E(i)).and.(c_E(i).gt.c_E(i-1))).and.&
      &((cipoints.eq.0).or.((cipoints.eq.1).and.(c_E(i).eq.maxval(c_E))))) then
      write(1,fmt='(A,I10)')'CINEB on image',i
      write(1,fmt='(A)')''
      gradspr(i,:)=0.0_dpr
      gradE(i,:)=0.0_dpr
      wvr1=intgrd(i,:)-2.0_dpr*sp(intgrd(i,:),tang(i,:),nintc)*tang(i,:)
    else
      wvr1=intgrd(i,:)-sp(intgrd(i,:),tang(i,:),nintc)*tang(i,:)
    endif
  else
    wvr1=intgrd(i,:)-sp(intgrd(i,:),tang(i,:),nintc)*tang(i,:)
  endif
  intgrd(i,:)=wvr1
  gradE(i,:)=wvr1
enddo

end subroutine

subroutine get_lambda(lambda,hess,dq,gradC_q,gradE_q,soph_l,nintc)
! calculates the langrangian multiplier
! soph_l false calculates the l.m. in the usual way
! soph_l true calculates the l.m. which would solve the general
! problem of constraint minimization (dq,lambda)
! see for details:
! M.J.D. Powell, Math. Prog., 14, 224 (1978) 

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) dq(nintc),gradE_q(nintc),hess(nintc,nintc),lambda,gradC_q(nintc)
real(kind=dpr) workv1(nintc)
logical soph_l

! input:
! hess ...... hessian mw ........................................... [aJ/(A^2*amu)]
! dq ........ step in internal coordinates ......................... [A*sqrt(amu)]
! gradC_q ... gradient of the constraint in internal coordinates ... [1]
! gradE_q ... mass weigthed gradient ............................... [aJ/(A*sqrt(amu))]
! soph_l .... decide in which way lambda is evaluated

!output:
! lambda .... langrangian multiplier ............................... [aJ/(A*sqrt(amu))]

if (soph_l .eqv. .true.) then
  write(1,fmt='(A)')'Calculating sophisticated langrange multiplier'
  write(1,fmt='(A)')''
  ! workv1 = hess * dq
  call DGEMV('n',nintc,nintc,1.0_dpr,hess,nintc,dq,1,0.0_dpr,workv1,1)
  workv1=workv1+gradE_q
else
  write(1,fmt='(A)')'Calculating conventional langrange multiplier'
  write(1,fmt='(A)')''
  workv1=gradE_q
endif

lambda=sp(gradC_q,workv1,nintc)/sp(gradC_q,gradC_q,nintc)

end subroutine

subroutine tp(wm1,vec1,vec2,nintc)
!calculates tensor product:
!vec1 x vec2T

!input:
! vec* ... n dimensional vectors

!output:
! wm1 .... n x n matrix

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) wm1(nintc,nintc),vec1(nintc),vec2(nintc)
integer i,j

wm1=0.0_dpr

do i=1,nintc
  do j=1,nintc
    wm1(i,j)=vec1(i)*vec2(j)
  enddo
enddo

end subroutine

subroutine constraint(refintg,currintg,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
! This subroutine calculates the constraint in mass
! weighted internal coordinates and its derivative

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) Gn12(nintc,nintc),refintg(nintc),currintg(nintc)
real(kind=dpr) wv1(nintc),C_angs,gradC_q(nintc),mwintdisp(nintc)
real(kind=dpr) du1,du2,Gp12(nintc,nintc),wv2(nintc),Rhyp
real(kind=dpr) conhess(nintc,nintc)
integer i,j,k,printl

! input:
! refintg .... reference internal geometry ... [A|rad]
! currintg ... current internal geometry ..... [A|rad]
! Gn12 ....... =G^(-1/2) ..................... [(sqrt(amu)*A)/(A|rad)]
! Gp12 ....... =G^(+1/2) ..................... [(A|rad/(sqrt(amu)*A)]

! output:
! C_angs ..... constraint .................... [A*sqrt(amu)] 
! gradC_q .... gradient of the constraint .... [1]
! conhess .... hessian of the constraint ..... [1/(A*sqrt(amu)]

wv1=currintg-refintg

if (printl .ge. 1) then
  write(1,fmt='(A)')'Current internal coordinates:'
  do i=1,nintc
    write(1,fmt=outfmt)currintg(i)
  enddo
  write(1,fmt='(A)')''
endif

if (printl .ge. 2) then
  write(1,fmt='(A)')'Reference internal coordinates:'
  do i=1,nintc
    write(1,fmt=outfmt)refintg(i)
  enddo
  write(1,fmt='(A)')''
endif

if (printl .ge. 1) then
  write(1,fmt='(A)')'Internal coordinate difference (constraint):'
  do i=1,nintc
    write(1,fmt=outfmt)wv1(i)
  enddo
  write(1,fmt='(A)')''
endif


call DGEMV('n',nintc,nintc,1.0_dpr,Gn12,nintc,wv1,1,0.0_dpr,mwintdisp,1)

if (printl .ge. 1) then
  write(1,fmt='(A)')'MW Internal coordinate difference (constraint):'
  do i=1,nintc
    write(1,fmt=outfmt)mwintdisp(i)
  enddo
  write(1,fmt='(A)')''
endif


! get constraint
du1=norm(mwintdisp,nintc)
du2=(Rhyp/2.0_dpr)*au2ang

C_angs=du1-du2

! get derivative of constraint
gradC_q=(1.0_dpr/norm(mwintdisp,nintc))*mwintdisp

! calculate second derivative
do i=1,(nintc-1)
  do j=(i+1),nintc
    conhess(i,j)=-(mwintdisp(i)*mwintdisp(j))/((norm(mwintdisp,nintc))**3)
    conhess(j,i)=conhess(i,j)
  enddo
enddo

do i=1,nintc
  conhess(i,i)=1.0_dpr/norm(mwintdisp,nintc)-(mwintdisp(i)**2)/((norm(mwintdisp,nintc))**3)
enddo

end subroutine

subroutine uwdisp(disp,mwdisp,Gp12,nintc)
! This subroutine calculates the displacement 
! out of the mass weighted displacement:
! dq=G^(1/2)T*dqM

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) Gp12(nintc,nintc),disp(nintc)
real(kind=dpr) mwdisp(nintc)
integer i,j,k

! input:
! Gp12 ..... =G^(+1/2) ......... [(A|rad/(sqrt(amu)*A)]
! mwdisp ... mw displacement ... [A*sqrt(amu)]

! output:
! disp .... displacement........ [A|rad]

disp=0.0_dpr
call DGEMV('t',nintc,nintc,1.0_dpr,Gp12,nintc,mwdisp,1,0.0_dpr,disp,1)

end subroutine

subroutine mwhess(hess,Gp12,nintc)
! This subroutine calculates the mass weighted hessian
! Hm=G^(1/2)*H*G^(1/2)T

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) wm1(nintc,nintc),Gp12(nintc,nintc),hess(nintc,nintc)
integer i,j,k

! Gp12 ... =G^(+1/2) .... [(A|rad/(sqrt(amu)*A)]

! input:
! hess ... hessian ...... [aJ/((A|rad)^2)]

! output:
! hess ... mw hessian ... [aJ/(A^2*amu)]

!wm1=hess*Gp12T
call DGEMM('n','t',nintc,nintc,nintc,1.0_dpr,hess,nintc,Gp12,nintc,0.0_dpr,wm1,nintc)
!hess=Gp12*wm1
call DGEMM('n','n',nintc,nintc,nintc,1.0_dpr,Gp12,nintc,wm1,nintc,0.0_dpr,hess,nintc)

end subroutine

subroutine uwhess(hess,Gn12,nintc)
! This subroutine calculates the hessian out of the mass weighted one

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) wm1(nintc,nintc),Gn12(nintc,nintc),hess(nintc,nintc)
integer i,j,k

! Gn12 ... =G^(-1/2) ... [(sqrt(amu)*A)/(A|rad)]

! input:
! hess ... mw hessian ... [aJ/(A^2*amu)]

! output:
! hess ... hessian ...... [aJ/((A|rad)^2)]

call DGEMM('n','n',nintc,nintc,nintc,1.0_dpr,hess,nintc,Gn12,nintc,0.0_dpr,wm1,nintc)
call DGEMM('t','n',nintc,nintc,nintc,1.0_dpr,Gn12,nintc,wm1,nintc,0.0_dpr,hess,nintc)

end subroutine

subroutine mwgrad(gradE_q,Gp12,nintc)
! This subroutine calculates the mass weithed gradient
! gM=G^(1/2)*g

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) wv1(nintc),Gp12(nintc,nintc),gradE_q(nintc)
integer i,j,k

! Gp12 ... =G^(+1/2) ........ [(A|rad/(sqrt(amu)*A)]

! input:
! gradE_q ... gradient ...... [aJ/(A|rad)]

! output:
! gradE_q ... mw gradient ... [aJ/(A*sqrt(amu))]

wv1=gradE_q

call DGEMV('n',nintc,nintc,1.0_dpr,Gp12,nintc,wv1,1,0.0_dpr,gradE_q,1)

end subroutine

subroutine mwdpl(dpl,Gn12,nintc)
! This subroutine calculates the mass weithed displacement
! dqM=G^(-1/2)*dq

use prec_mod
use units_mod

implicit none
integer nintc
real(kind=dpr) wv1(nintc),Gn12(nintc,nintc),dpl(nintc)
integer i,j,k

! Gn12 ... =G^(-1/2) ......... [(sqrt(amu)*A)/(A|rad)]

! input:
! dpl .... displacement ...... [A|rad]

! output:
! dpl .... mw displacement ... [A*sqrt(amu)]

wv1=dpl

call DGEMV('n',nintc,nintc,1.0_dpr,Gn12,nintc,wv1,1,0.0_dpr,dpl,1)

end subroutine

subroutine getGmatrices(bmatrix,M,Gn12,Gp12,nintc,nat)
! This subroutine calculates the required G matrices
! needed for mass weigthed internal coordinates

use prec_mod
use units_mod

implicit none
integer nintc,nat
real(kind=dpr) Gn12(nintc,nintc),Gp12(nintc,nintc)
real(kind=dpr) bmatrix(3*nat,nintc),M(nat),btb(nintc,nintc)
real(kind=dpr) G1(nintc,nintc),U(nintc,nintc),d(nintc),e(nintc),work(:)
real(kind=dpr) G2(nintc,nintc),tau(nintc),wbm(3*nat,nintc)
integer i,j,k,info,lwork
allocatable work

! input:
! bmatrix ... bmatrix ..... [A|rad/A]
! M ......... masses ...... [amu]

! output:
! Gn12 ...... =G^(-1/2) ... [(sqrt(amu)*A)/(A|rad)]
! Gp12 ...... =G^(+1/2) ... [(A|rad/(sqrt(amu)*A)]

! no conversion !
wbm=bmatrix

! do massweighting
do j=1,nat
  do i=1,3
    do k=1,nintc
      wbm((j-1)*3+i,k)=(wbm((j-1)*3+i,k))/dsqrt(M(j))
    enddo
  enddo
enddo

!new:btb= bmatrixT*bmatrix
call DGEMM('t','n',nintc,nintc,3*nat,1.0_dpr,wbm,3*nat,wbm,3*nat,0.0_dpr,btb,nintc)

info=0
U=btb
!bring into tridiagonal form
allocate(work(nintc))
call DSYTRD('U',nintc,U,nintc,d,e,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub getGmatrices",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DSYTRD('U',nintc,U,nintc,d,e,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub getGmatrices",2)
call DORGTR('U',nintc,U,nintc,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub getGmatrices",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DORGTR('U',nintc,U,nintc,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub getGmatrices",2)
!calculate Eval and Evecs
call DSTEQR('V',nintc,d,e,U,nintc,work,info)
if (info.ne.0) call error_sub("Problem with DSTEQR in sub getGmatrices",2)
deallocate(work)

! get G^(-1/2)
G1=0.0_dpr
G2=0.0_dpr
do i=1,nintc
  G1(i,i)=1.0_dpr/dsqrt(d(i))
  G2(i,i)=dsqrt(d(i))
enddo

call DGEMM('n','t',nintc,nintc,nintc,1.0_dpr,G1,nintc,U,nintc,0.0_dpr,Gn12,nintc)
call DGEMM('n','t',nintc,nintc,nintc,1.0_dpr,G2,nintc,U,nintc,0.0_dpr,Gp12,nintc)

end subroutine

function col_norm(vec,nat)
! This function calculates the norm of a vector arranged
! in a geometry format, 2D array, (first index for atom,
! second index for x,y,z)

use prec_mod
use units_mod

implicit none
integer i,j,k,nat
real(kind=dpr) col_norm,vec(nat,3)

col_norm=0.0_dpr

do i=1,nat
  do j=1,3
    col_norm=col_norm+vec(i,j)**2
  enddo
enddo

col_norm=dsqrt(col_norm)

end function

function col_sp(vec1,vec2,nat)
! This function calculates the scalar product of 
! two vectors arranged in a geometry format, 2D array, 
! (first index for atom, second index for x,y,z)

use prec_mod
use units_mod

implicit none
integer i,j,k,nat
real(kind=dpr) col_sp,vec1(nat,3),vec2(nat,3)

col_sp=0.0_dpr

do i=1,nat
  do j=1,3
    col_sp=col_sp+vec1(i,j)*vec2(i,j)
  enddo
enddo

end function

function norm(vec,nintc)
! This function calculates the norm of a vector of length nintc

use prec_mod
use units_mod

implicit none
integer i,j,k,nintc
real(kind=dpr) norm,vec(nintc)

norm=0.0_dpr

do i=1,nintc
  norm=norm+vec(i)**2
enddo

norm=dsqrt(norm)

end function

function calcdet(x)
! This function calculates the determinant 
! of a 3x3 matrix.

use prec_mod
use units_mod

implicit none
real(kind=dpr) x(3,3),calcdet

calcdet=x(1,1)*x(2,2)*x(3,3)+x(1,2)*x(2,3)*x(3,1)+x(1,3)*x(2,1)*x(3,2)&
&-x(3,1)*x(2,2)*x(1,3)-x(3,2)*x(2,3)*x(1,1)-x(3,3)*x(2,1)*x(1,2)

end function


function sp(vec1,vec2,nintc)
! This function calculates the scalar product of 
! two vectors of length nintc

use prec_mod
use units_mod

implicit none
integer i,j,k,nintc
real(kind=dpr) sp,vec1(nintc),vec2(nintc)

sp=0.0_dpr

do i=1,nintc
  sp=sp+vec1(i)*vec2(i)
enddo

end function

end module general_mod
