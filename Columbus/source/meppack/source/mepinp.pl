#! /usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use strict;
use warnings;
use Shell qw(pwd cd cp mkdir date mv cat);
use lib $ENV{"MEP"};
use defaults qw(test writerestart getkeyw definit prepdef);

# (c) Bernhard Sellner 2011
# part of MEPPACK
# This is the input facility for the MEP program package

my ($i,$j,$end,$choice);

$choice=0;
until (($choice eq "11")) {
  system("clear");
  print ("\n");
  print ("                        MEPPACK                  \n");
  print ("\n");
  print (" Minimum energy path input facility for MEP version 2.3\n\n");
  print ("          written by Bernhard Sellner 2011             \n\n\n");
  print (" Input for IRC calculation \n\n");
  print (" Interactive (1,2,3):\n");
  print ("   1  ... Minimal input (easy & fast)           #  ####   ###      \n");
  print ("   2  ... Advanced input                        #  #  #  #         \n");
  print ("   3  ... Full input                            #  ####  #         \n");
  print ("                                                #  ##    #         \n");
  print ("   4  ... Write input files for IRC using       #  # #   #         \n"); 
  print ("          default values if possible            #  #  #   ###      \n\n");
  print ("   5  ... Check for IRC files (optional)\n\n\n\n");
  print (" Input for nudged elastic band (NEB) calculation \n\n");
  print (" Interactive (6,7,8):\n");
  print ("   6  ... Minimal input (easy & fast)           ##  #  ####  ###   \n");
  print ("   7  ... Advanced input                        ##  #  #     #  #  \n");
  print ("   8  ... Full input                            # # #  ###   ####  \n");
  print ("                                                # # #  #     #  #  \n");
  print ("   9  ... Write input files for NEB using       #  ##  #     #  #  \n");
  print ("          default values if possible            #  ##  ####  ###   \n\n");
  print ("  10  ... Check for NEB files (optional)\n\n\n\n\n");
  print ("  11  ... Exit input facility \n\n");
  print ("\n\n\nSelection: ");
  $choice=<STDIN>;
  chomp($choice);

  if ($choice == 1) {

    system("clear");
    print ("\n\n\n Fast input for IRC \n\n");
    fast_irc();
    print ("\n\n\n Fast input for IRC finished. Hit enter to continue ... ");
    $i=<STDIN>;  


  } elsif ($choice == 2) {

    system("clear");
    print ("\n\n\n Advanced input for IRC \n\n");
    med_irc();
    print ("\n\n\n Advanced input for IRC finished. Hit enter to continue ... ");
    $i=<STDIN>;  

  } elsif ($choice == 3) {
    system("clear");
    print ("\n\n\n Full input for IRC \n\n");
    full_irc();
    print ("\n\n\n Full input for IRC finished. Hit enter to continue ... ");
    $i=<STDIN>;  
 
  } elsif ($choice == 4) {

    system("clear");
    print ("\n\n\n Writing input files for IRC \n\n");
    onlywrite_irc();
    print ("\n\n\n Writing input files finished. Hit enter to continue ... ");
    $i=<STDIN>;  

  } elsif ($choice == 5) {

    checkirc();
    print ("\n\n\nHit enter to continue ... ");
    $i=<STDIN>;  


  } elsif ($choice == 6) {

    system("clear");
    print ("\n\n\n Fast input for NEB \n\n");
    fast_neb();
    print ("\n\n\n Fast input for NEB finished. Hit enter to continue ... ");

  } elsif ($choice == 7) {

    system("clear");
    print ("\n\n\n Advanced input for NEB \n\n");
    med_neb();
    print ("\n\n\n Advanced input for NEB finished. Hit enter to continue ... ");
    $i=<STDIN>;

  } elsif ($choice == 8) {

    system("clear");
    print ("\n\n\n Full input for NEB \n\n");
    full_neb();
    print ("\n\n\n Full input for NEB finished. Hit enter to continue ... ");
    $i=<STDIN>;

  } elsif ($choice == 9) {

    system("clear");
    print ("\n\n\n Writing input files for NEB \n\n");
    onlywrite_neb();
    print ("\n\n\n Writing input files finished. Hit enter to continue ... ");
    $i=<STDIN>;

  } elsif ($choice == 10) {

    checkneb();
    print ("\n\n\nHit enter to continue ... ");
    $i=<STDIN>;  

  }

}



print ("\n\nmepinp.pl finished\n\n");

system("clear");


sub onlywrite_neb{

  my($trash,$usrin,$i,@genp,@nenp,$wdir,$meppath,@nonge,@nonne,$k);

  # nebpar namelist
  my($licneb,$adjneb,$initneb,$doneb,$maxit,$nprocneb,$matfact);
  my($tangtype,$nebtype,$startci,$startdiis,$maxgdiis,$maxddiis);
  my($bmupd,$stepres,$scmin,$scmax,$conv_grad,$conv_disp,$nim,$nofidiis);
  my($cipoints);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  @nonge=("nintc","nat");
  @nonne=("nofidiis");

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $nenp[0][0]=("licneb");
  $nenp[1][0]=("adjneb");
  $nenp[2][0]=("initneb");
  $nenp[3][0]=("doneb");
  $nenp[4][0]=("maxit");
  $nenp[5][0]=("nprocneb");
  $nenp[6][0]=("matfact");
  $nenp[7][0]=("tangtype");
  $nenp[8][0]=("nebtype");
  $nenp[9][0]=("startci");
  $nenp[10][0]=("startdiis");
  $nenp[11][0]=("maxgdiis");
  $nenp[12][0]=("maxddiis");
  $nenp[13][0]=("bmupd");
  $nenp[14][0]=("stepres");
  $nenp[15][0]=("scmin");
  $nenp[16][0]=("scmax");
  $nenp[17][0]=("conv_grad");
  $nenp[18][0]=("conv_disp");
  $nenp[19][0]=("nim");
  $nenp[20][0]=("nofidiis");
  $nenp[21][0]=("cipoints");

  &defaults::prepdef($meppath,$wdir,'neb');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@nenp);

  system("rm -f $wdir/def");

  # copy final variables
  $licneb=$nenp[0][1];
  $adjneb=$nenp[1][1];
  $initneb=$nenp[2][1];
  $doneb=$nenp[3][1];
  $maxit=$nenp[4][1];
  $nprocneb=$nenp[5][1];
  $matfact=$nenp[6][1];
  $tangtype=$nenp[7][1];
  $nebtype=$nenp[8][1];
  $startci=$nenp[9][1];
  $startdiis=$nenp[10][1];
  $maxgdiis=$nenp[11][1];
  $maxddiis=$nenp[12][1];
  $bmupd=$nenp[13][1];
  $stepres=$nenp[14][1];
  $scmin=$nenp[15][1];
  $scmax=$nenp[16][1];
  $conv_grad=$nenp[17][1];
  $conv_disp=$nenp[18][1];
  $nim=$nenp[19][1];
  $nofidiis=$nenp[20][1];
  $cipoints=$nenp[21][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # start input


  $trash=1;
  (-e "geninp" || -e "nebpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (NP,">nebpar") or die "Cannot write nebpar\n";
  print NP ("&nebpar\n");

  for ($i=0;$i<@genp;$i++){
    if (!($genp[$i][0] eq "hdisp")) {
      $k=0;
      for ($j=0;$j<@nonge;$j++){
        if ($genp[$i][0] eq $nonge[$j]) {
          $k=1;
        }
      }
      if ($k == 0) {        
        if (($genp[$i][0] eq "elprog")||
            ($genp[$i][0] eq "coordprog")||
            ($genp[$i][0] eq "inihess")||
            ($genp[$i][0] eq "inihmod")||
            ($genp[$i][0] eq "hessmod")||
            ($genp[$i][0] eq "mixmod")) {
          print GI (" $genp[$i][0] = \'$genp[$i][1]\'\n");
        } else {
          print GI (" $genp[$i][0] = $genp[$i][1]\n");
        }
      } elsif ($k == 1) {
        print GI (" $genp[$i][0] =\n");
      }
    }
  }

  for ($i=0;$i<@nenp;$i++){
    if (!(($nenp[$i][0] eq "nim")||($nenp[$i][0] eq "licneb"))) {
      $k=0;
      for ($j=0;$j<@nonne;$j++){
        if ($nenp[$i][0] eq $nonne[$j]) {
          $k=1;
        }
      }
      if ($k == 0) {        
        if (($nenp[$i][0] eq "tangtype")||
            ($nenp[$i][0] eq "nebtype")) {
          print NP (" $nenp[$i][0] = \'$nenp[$i][1]\'\n");
        } else {
          print NP (" $nenp[$i][0] = $nenp[$i][1]\n");
        }
      } elsif ($k == 1) {
        print NP (" $nenp[$i][0] =\n");
      }
    }
  }



  print GI ("&end\n");
  print NP ("&end\n");

  close(GI);
  close(NP);

}





sub full_neb{

  my($trash,$usrin,$i,@genp,@nenp,$wdir,$meppath,@hessarr);

  # nebpar namelist
  my($licneb,$adjneb,$initneb,$doneb,$maxit,$nprocneb,$matfact);
  my($tangtype,$nebtype,$startci,$startdiis,$maxgdiis,$maxddiis);
  my($bmupd,$stepres,$scmin,$scmax,$conv_grad,$conv_disp,$nim,$nofidiis);
  my($cipoints);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $nenp[0][0]=("licneb");
  $nenp[1][0]=("adjneb");
  $nenp[2][0]=("initneb");
  $nenp[3][0]=("doneb");
  $nenp[4][0]=("maxit");
  $nenp[5][0]=("nprocneb");
  $nenp[6][0]=("matfact");
  $nenp[7][0]=("tangtype");
  $nenp[8][0]=("nebtype");
  $nenp[9][0]=("startci");
  $nenp[10][0]=("startdiis");
  $nenp[11][0]=("maxgdiis");
  $nenp[12][0]=("maxddiis");
  $nenp[13][0]=("bmupd");
  $nenp[14][0]=("stepres");
  $nenp[15][0]=("scmin");
  $nenp[16][0]=("scmax");
  $nenp[17][0]=("conv_grad");
  $nenp[18][0]=("conv_disp");
  $nenp[19][0]=("nim");
  $nenp[20][0]=("nofidiis");
  $nenp[21][0]=("cipoints");

  &defaults::prepdef($meppath,$wdir,'neb');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@nenp);

  system("rm -f $wdir/def");

  # copy final variables
  $licneb=$nenp[0][1];
  $adjneb=$nenp[1][1];
  $initneb=$nenp[2][1];
  $doneb=$nenp[3][1];
  $maxit=$nenp[4][1];
  $nprocneb=$nenp[5][1];
  $matfact=$nenp[6][1];
  $tangtype=$nenp[7][1];
  $nebtype=$nenp[8][1];
  $startci=$nenp[9][1];
  $startdiis=$nenp[10][1];
  $maxgdiis=$nenp[11][1];
  $maxddiis=$nenp[12][1];
  $bmupd=$nenp[13][1];
  $stepres=$nenp[14][1];
  $scmin=$nenp[15][1];
  $scmax=$nenp[16][1];
  $conv_grad=$nenp[17][1];
  $conv_disp=$nenp[18][1];
  $nim=$nenp[19][1];
  $nofidiis=$nenp[20][1];
  $cipoints=$nenp[21][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # initialize hessarr
  $hessarr[1][1]='unitma';
  $hessarr[1][2]='newh';
  $hessarr[1][3]='update';
  $hessarr[1][4]='const';
  $hessarr[1][5]='2000000000';
  $hessarr[2][1]='intcfl';
  $hessarr[2][2]='newh';
  $hessarr[2][3]='update';
  $hessarr[2][4]='const';
  $hessarr[2][5]='2000000000';
  $hessarr[3][1]='calc';
  $hessarr[3][2]='newh';
  $hessarr[3][3]='update';
  $hessarr[3][4]='const';
  $hessarr[3][5]='2000000000';

  # start input

  $trash=1;
  (-e "geninp" || -e "nebpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (NP,">nebpar") or die "Cannot write nebpar\n";
  print NP ("&nebpar\n");

  print ("\nHow many atoms are in the molecule\n");
  $usrin=<STDIN>;
  chomp($usrin);
  print GI ("  nat        =   $usrin\n");

  if ($usrin >= 3) {
    $nintc=3*$usrin-6;
  }  elsif ($usrin == 2) {
    $nintc=1;
  }

  print GI ("  nintc      =   $nintc\n");

  print ("\nShould the images be adjusted as a first step ?\n");
  print ("Default (NO) : $adjneb\n");
  print ("YES          : 1\n");        
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$adjneb\n"}
  $adjneb=$usrin;
  print NP ("  adjneb     =   $usrin");

  print ("\nShould the images be initialized ?\n");
  print ("(after adjustement and before relaxing the NEB)\n");
  print ("Default (NO) : $initneb\n");
  print ("YES          : 1\n");        
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$initneb\n"}
  $initneb=$usrin;
  print NP ("  initneb    =   $usrin");

  print ("\nShould the NEB relaxation be done ?\n");
  print ("Default (YES) : $doneb\n");
  print ("NO            : 0\n");        
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$doneb\n"}
  $doneb=$usrin;
  print NP ("  doneb      =   $usrin");

  print ("\nHow much memory can be used in the COLUMBUS calculation (Mwords for 5.9.*; MB for 7.0) ?\n");
  print ("Default : $mem\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mem\n"}
  $mem=$usrin;
  print GI ("  mem        =   $usrin");

  print ("\nWhich program will be used to transform the coordiantes ?\n");
  print ("Default               : $coordprog\n");
  print ("Z-matrix              : zmat\n");
  print ("No (Cartesian coord.) : cart\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$coordprog\n"}
  $coordprog=$usrin;
  chomp($usrin);
  print GI ("  coordprog  =   \'$usrin\'\n");

  print ("\nWhat is the maximum allowed number of iterations ?\n");
  print ("Default : $maxit\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxit\n"}
  $maxit=$usrin;
  print NP ("  maxit      =   $usrin");

  print ("\nHow many processors can be used (parallelization in MEP program) ?\n");
  print ("For optimum workload, the number of images relaxed should\n");
  print ("(ends are fixed !) be a multiple of the number of processors \n");
  print ("Default : $nprocneb\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$nprocneb\n"}
  $nprocneb=$usrin;
  print NP ("  nprocneb   =   $usrin");

  print ("\nWhich factor should be used in scaling of the matrix ?\n");
  print ("Default : $matfact\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$matfact\n"}
  $matfact=$usrin;
  print NP ("  matfact    =   $usrin");

  print ("\nWhich method should be used for construction of the tangent:\n");
  print ("BNEB (bisection) or ITNEB (improved tangent) ? \n");
  print ("Default : $tangtype\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$tangtype\n"}
  $tangtype=$usrin;
  chomp($usrin);
  print NP ("  tangtype   =   \'$usrin\'\n");

  print ("\nShould images higher in energy than their neighbours converge\n");
  print ("to saddlepoints (Climbing Image NEB)?\n");
  print ("Default : $nebtype\n");
  print ("No      : STD\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$nebtype\n"}
  $nebtype=$usrin;
  chomp($usrin);
  print NP ("  nebtype    =   \'$usrin\'\n");

  if ($nebtype eq "CINEB\n") {
    print ("\nAfter how many iterations should this image(s) start to be converged to saddlepoint(s) ?\n");
    print ("Default : $startci\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$startci\n"}
    $startci=$usrin;
    print NP ("  startci    =   $usrin");

    print ("\nShould only the highest energy candidate be affected ?\n");
    print ("Default (1 possible saddlepoint) : $cipoints\n");
    print ("All                              : 0\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$cipoints\n"}
    $cipoints=$usrin;
    print NP ("  cipoints   =   $usrin");
  }

  print ("\nAfter how many iterations should DIIS interpolation\n");
  print ("be used to accelerate the convergence ?\n");
  print ("Default : $startdiis\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$startdiis\n"}
  $startdiis=$usrin;
  print NP ("  startdiis  =   $usrin");

  print ("\nHow many sets of geometries in the DIIS procedure are (max) allowed ?\n");
  print ("Default : $maxgdiis\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxgdiis\n"}
  $maxgdiis=$usrin;
  print NP ("  maxgdiis   =   $usrin");

  print ("\nWhat is the maximal allowed displacement for a step \n");
  print ("to participate in DIIS ? (A for cartesian; A|rad for internal)\n");
  print ("Default : $maxddiis\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxddiis\n"}
  $maxddiis=$usrin;
  print NP ("  maxddiis   =   $usrin");

  print ("\nAfter how many iteration should the bmatrix be updated ?\n");
  print ("Default (each) : $bmupd\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$bmupd\n"}
  $bmupd=$usrin;
  print NP ("  bmupd      =   $usrin");

  print ("\nWhat is the maximal allowed displacement in each coordinate in A|rad ?\n");
  print ("Default : $stepres\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$stepres\n"}
  $bmupd=$usrin;
  print NP ("  stepres    =   $usrin");

  print ("\nWhat is the spring constant in aJ/(A^2*amu) ?\n");
  print ("Default : $scmin\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$scmin\n"}
  print NP ("  scmin      =   $usrin");
  print NP ("  scmax      =   $usrin");

  print ("\nWhat is the convergence threshold for the average gradient of each image in aJ/(A*sqrt(amu))?\n");
  print ("Default : $conv_grad\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_grad\n"}
  $conv_grad=$usrin;
  print NP ("  conv_grad  =   $usrin");

  print ("\nWhat is the convergence threshold for the average displacment of each image in sqrt(amu)*A ?\n");
  print ("Default : $conv_disp\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_disp\n"}
  $conv_disp=$usrin;
  print NP ("  conv_disp  =   $usrin");

  print ("\nWhich print level should be used ?\n");
  print ("Minimum output              : 0\n");
  print ("Standard output (Default)   : $printl\n");
  print ("Debug (huge amount of data) : 2\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$printl\n"}
  $printl=$usrin;
  print GI ("  printl     =   $usrin");

  print ("\nShould molden output be created ?\n");
  print ("Default (YES) : $mld\n");
  print ("NO            : 0\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mld\n"}
  $mld=$usrin;
  print GI ("  mld        =   $usrin");

  print ("\nAfter how many steps, the complete information should be kept in the DEBUG directory ?\n");
  print ("Default   : $prtstep\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$prtstep\n"}
  $prtstep=$usrin;
  print GI ("  prtstep    =   $usrin");

  print ("\nHessian treatement:\n\n");
  #print ("\n -1) Full input\n\n");
  print ("  1) Use a unit matrix\n");
  print ("  2) Use a diagonal guess from intcfl\n");
  print ("  3) Calculate Hessian with COLUMBUS in the 1st step\n");
  $usrin=<STDIN>;
  print GI ("  inihess    =   \'$hessarr[$usrin][1]\'\n");
  print GI ("  inihmod    =   \'$hessarr[$usrin][2]\'\n");
  print GI ("  hessmod    =   \'$hessarr[$usrin][3]\'\n");
  print GI ("  mixmod     =   \'$hessarr[$usrin][4]\'\n");
  print GI ("  hessstep   =   $hessarr[$usrin][5]\n");
  $inihess=$hessarr[$usrin][1];
  $inihmod=$hessarr[$usrin][2];
  $hessmod=$hessarr[$usrin][3];
  $mixmod=$hessarr[$usrin][4];
  $hessstep=$hessarr[$usrin][5];


  print GI ("&end\n");
  print NP ("&end\n");

  close(GI);
  close(NP);

}











sub med_neb{

  my($trash,$usrin,$i,@genp,@nenp,$wdir,$meppath);

  # nebpar namelist
  my($licneb,$adjneb,$initneb,$doneb,$maxit,$nprocneb,$matfact);
  my($tangtype,$nebtype,$startci,$startdiis,$maxgdiis,$maxddiis);
  my($bmupd,$stepres,$scmin,$scmax,$conv_grad,$conv_disp,$nim,$nofidiis);
  my($cipoints);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $nenp[0][0]=("licneb");
  $nenp[1][0]=("adjneb");
  $nenp[2][0]=("initneb");
  $nenp[3][0]=("doneb");
  $nenp[4][0]=("maxit");
  $nenp[5][0]=("nprocneb");
  $nenp[6][0]=("matfact");
  $nenp[7][0]=("tangtype");
  $nenp[8][0]=("nebtype");
  $nenp[9][0]=("startci");
  $nenp[10][0]=("startdiis");
  $nenp[11][0]=("maxgdiis");
  $nenp[12][0]=("maxddiis");
  $nenp[13][0]=("bmupd");
  $nenp[14][0]=("stepres");
  $nenp[15][0]=("scmin");
  $nenp[16][0]=("scmax");
  $nenp[17][0]=("conv_grad");
  $nenp[18][0]=("conv_disp");
  $nenp[19][0]=("nim");
  $nenp[20][0]=("nofidiis");
  $nenp[21][0]=("cipoints");

  &defaults::prepdef($meppath,$wdir,'neb');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@nenp);

  system("rm -f $wdir/def");

  # copy final variables
  $licneb=$nenp[0][1];
  $adjneb=$nenp[1][1];
  $initneb=$nenp[2][1];
  $doneb=$nenp[3][1];
  $maxit=$nenp[4][1];
  $nprocneb=$nenp[5][1];
  $matfact=$nenp[6][1];
  $tangtype=$nenp[7][1];
  $nebtype=$nenp[8][1];
  $startci=$nenp[9][1];
  $startdiis=$nenp[10][1];
  $maxgdiis=$nenp[11][1];
  $maxddiis=$nenp[12][1];
  $bmupd=$nenp[13][1];
  $stepres=$nenp[14][1];
  $scmin=$nenp[15][1];
  $scmax=$nenp[16][1];
  $conv_grad=$nenp[17][1];
  $conv_disp=$nenp[18][1];
  $nim=$nenp[19][1];
  $nofidiis=$nenp[20][1];
  $cipoints=$nenp[21][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # start input

  $trash=1;
  (-e "geninp" || -e "nebpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (NP,">nebpar") or die "Cannot write nebpar\n";
  print NP ("&nebpar\n");

  print ("\nHow many atoms are in the molecule\n");
  $usrin=<STDIN>;
  chomp($usrin);
  print GI ("  nat        =   $usrin\n");

  if ($usrin >= 3) {
    $nintc=3*$usrin-6;
  }  elsif ($usrin == 2) {
    $nintc=1;
  }

  print GI ("  nintc      =   $nintc\n");

  print ("\nHow much memory can be used in the COLUMBUS calculation (Mwords for 5.9.*; MB for 7.0) ?\n");
  print ("Default : $mem\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mem\n"}
  $mem=$usrin;
  print GI ("  mem        =   $usrin");

  print ("\nWhich program will be used to transform the coordiantes ?\n");
  print ("Default               : $coordprog\n");
  print ("Z-matrix              : zmat\n");
  print ("No (Cartesian coord.) : cart\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$coordprog\n"}
  $coordprog=$usrin;
  chomp($usrin);
  print GI ("  coordprog  =   \'$usrin\'\n");

  if ($coordprog ne "pulay\n") {
    print ("\nIn case of zmat or cart the program needs adjusted images.\n");
    print ("Should this be done before relaxing the NEB (y/n) ?\n");
    print ("(Default y)\n");
    $usrin=<STDIN>;
    if (($usrin eq "\n")||($usrin eq "y\n")) {
      print NP ("  adjneb     =   1\n");
      print NP ("  initneb    =   1\n");
    }
  }

  print ("\nWhat is the maximum allowed number of iterations ?\n");
  print ("Default : $maxit\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxit\n"}
  $maxit=$usrin;
  print NP ("  maxit      =   $usrin");

  print ("\nHow many processors can be used (parallelization in MEP program) ?\n");
  print ("For optimum workload, the number of images relaxed should\n");
  print ("(ends are fixed !) be a multiple of the number of processors \n");
  print ("Default : $nprocneb\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$nprocneb\n"}
  $nprocneb=$usrin;
  print NP ("  nprocneb   =   $usrin");

  print ("\nWhich factor should be used in scaling of the matrix ?\n");
  print ("Default : $matfact\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$matfact\n"}
  $matfact=$usrin;
  print NP ("  matfact    =   $usrin");

  print ("\nWhich method should be used for construction of the tangent:\n");
  print ("BNEB (bisection) or ITNEB (improved tangent) ? \n");
  print ("Default : $tangtype\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$tangtype\n"}
  $tangtype=$usrin;
  chomp($usrin);
  print NP ("  tangtype   =   \'$usrin\'\n");

  print ("\nShould images higher in energy than their neighbours converge\n");
  print ("to saddlepoints (Climbing Image NEB)?\n");
  print ("Default : $nebtype\n");
  print ("No      : STD\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$nebtype\n"}
  $nebtype=$usrin;
  chomp($usrin);
  print NP ("  nebtype    =   \'$usrin\'\n");

  if ($nebtype eq "CINEB\n") {
    print ("\nAfter how many iterations should this image(s) start to be converged to saddlepoint(s) ?\n");
    print ("Default : $startci\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$startci\n"}
    $startci=$usrin;
    print NP ("  startci    =   $usrin");
  }

  print ("\nAfter how many iterations should DIIS interpolation\n");
  print ("be used to accelerate the convergence ?\n");
  print ("Default : $startdiis\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$startdiis\n"}
  $startdiis=$usrin;
  print NP ("  startdiis  =   $usrin");


  print GI ("&end\n");
  print NP ("&end\n");

  close(GI);
  close(NP);

}











sub fast_neb{

  my($trash,$usrin,$i,@genp,@nenp,$wdir,$meppath);

  # nebpar namelist
  my($licneb,$adjneb,$initneb,$doneb,$maxit,$nprocneb,$matfact);
  my($tangtype,$nebtype,$startci,$startdiis,$maxgdiis,$maxddiis);
  my($bmupd,$stepres,$scmin,$scmax,$conv_grad,$conv_disp,$nim,$nofidiis);
  my($cipoints);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $nenp[0][0]=("licneb");
  $nenp[1][0]=("adjneb");
  $nenp[2][0]=("initneb");
  $nenp[3][0]=("doneb");
  $nenp[4][0]=("maxit");
  $nenp[5][0]=("nprocneb");
  $nenp[6][0]=("matfact");
  $nenp[7][0]=("tangtype");
  $nenp[8][0]=("nebtype");
  $nenp[9][0]=("startci");
  $nenp[10][0]=("startdiis");
  $nenp[11][0]=("maxgdiis");
  $nenp[12][0]=("maxddiis");
  $nenp[13][0]=("bmupd");
  $nenp[14][0]=("stepres");
  $nenp[15][0]=("scmin");
  $nenp[16][0]=("scmax");
  $nenp[17][0]=("conv_grad");
  $nenp[18][0]=("conv_disp");
  $nenp[19][0]=("nim");
  $nenp[20][0]=("nofidiis");
  $nenp[21][0]=("cipoints");

  &defaults::prepdef($meppath,$wdir,'neb');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@nenp);

  system("rm -f $wdir/def");

  # copy final variables
  $licneb=$nenp[0][1];
  $adjneb=$nenp[1][1];
  $initneb=$nenp[2][1];
  $doneb=$nenp[3][1];
  $maxit=$nenp[4][1];
  $nprocneb=$nenp[5][1];
  $matfact=$nenp[6][1];
  $tangtype=$nenp[7][1];
  $nebtype=$nenp[8][1];
  $startci=$nenp[9][1];
  $startdiis=$nenp[10][1];
  $maxgdiis=$nenp[11][1];
  $maxddiis=$nenp[12][1];
  $bmupd=$nenp[13][1];
  $stepres=$nenp[14][1];
  $scmin=$nenp[15][1];
  $scmax=$nenp[16][1];
  $conv_grad=$nenp[17][1];
  $conv_disp=$nenp[18][1];
  $nim=$nenp[19][1];
  $nofidiis=$nenp[20][1];
  $cipoints=$nenp[21][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # start input

  $trash=1;
  (-e "geninp" || -e "nebpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (NP,">nebpar") or die "Cannot write nebpar\n";
  print NP ("&nebpar\n");

  print ("\nHow many atoms are in the molecule\n");
  $usrin=<STDIN>;
  chomp($usrin);
  print GI ("  nat        =   $usrin\n");

  if ($usrin >= 3) {
    $nintc=3*$usrin-6;
  }  elsif ($usrin == 2) {
    $nintc=1;
  }

  print GI ("  nintc      =   $nintc\n");

  print ("\nHow much memory can be used in the COLUMBUS calculation (Mwords for 5.9.*; MB for 7.0) ?\n");
  print ("Default : $mem\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mem\n"}
  $mem=$usrin;
  print GI ("  mem        =   $usrin");

  print ("\nWhat is the maximum allowed number of iterations ?\n");
  print ("Default : $maxit\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxit\n"}
  $maxit=$usrin;
  print NP ("  maxit      =   $usrin");


  print GI ("&end\n");
  print NP ("&end\n");

  close(GI);
  close(NP);

}



sub onlywrite_irc{

  my($trash,$usrin,$i,@genp,@irnp,$wdir,$meppath,@nonge,@nonir,$k);

  # ircpar namelist
  my($rpsteps,$maxhypstep,$inidirect,$nmode,$inirtrust,$Rhyp,$conv_grad);
  my($conv_disp,$conv_uwdisp,$conv_cons,$miciter,$conv_alpha,$conv_rad);
  my($soph_l,$maxrtrust,$minrtrust,$lratio,$uratio);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  @nonge=("nintc","nat");
  @nonir=("inidirect","nmode");

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $irnp[0][0]=("rpsteps");
  $irnp[1][0]=("maxhypstep");
  $irnp[2][0]=("inidirect");
  $irnp[3][0]=("nmode");
  $irnp[4][0]=("inirtrust");
  $irnp[5][0]=("Rhyp");
  $irnp[6][0]=("conv_grad");
  $irnp[7][0]=("conv_disp");
  $irnp[8][0]=("conv_uwdisp");
  $irnp[9][0]=("conv_cons");
  $irnp[10][0]=("miciter");
  $irnp[11][0]=("conv_alpha");
  $irnp[12][0]=("conv_rad");
  $irnp[13][0]=("soph_l");
  $irnp[14][0]=("maxrtrust");
  $irnp[15][0]=("minrtrust");
  $irnp[16][0]=("lratio");
  $irnp[17][0]=("uratio");

  &defaults::prepdef($meppath,$wdir,'irc');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@irnp);

  system("rm -f $wdir/def");

  # copy final variables
  $rpsteps=$irnp[0][1];
  $maxhypstep=$irnp[1][1];
  $inidirect=$irnp[2][1];
  $nmode=$irnp[3][1];
  $inirtrust=$irnp[4][1];
  $Rhyp=$irnp[5][1];
  $conv_grad=$irnp[6][1];
  $conv_disp=$irnp[7][1];
  $conv_uwdisp=$irnp[8][1];
  $conv_cons=$irnp[9][1];
  $miciter=$irnp[10][1];
  $conv_alpha=$irnp[11][1];
  $conv_rad=$irnp[12][1];
  $soph_l=$irnp[13][1];
  $maxrtrust=$irnp[14][1];
  $minrtrust=$irnp[15][1];
  $lratio=$irnp[16][1];
  $uratio=$irnp[17][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # start input

  $trash=1;
  (-e "geninp" || -e "ircpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (IP,">ircpar") or die "Cannot write ircpar\n";
  print IP ("&ircpar\n");

  for ($i=0;$i<@genp;$i++){
    if (!(($genp[$i][0] eq "prtstep")||($genp[$i][0] eq "restart"))) {
      $k=0;
      for ($j=0;$j<@nonge;$j++){
        if ($genp[$i][0] eq $nonge[$j]) {
          $k=1;
        }
      }
      if ($k == 0) {
        if (($genp[$i][0] eq "elprog")||
            ($genp[$i][0] eq "coordprog")||        
            ($genp[$i][0] eq "inihess")||        
            ($genp[$i][0] eq "inihmod")||        
            ($genp[$i][0] eq "hessmod")||        
            ($genp[$i][0] eq "mixmod")) {        
          print GI (" $genp[$i][0] = \'$genp[$i][1]\'\n");
        } else {
          print GI (" $genp[$i][0] = $genp[$i][1]\n");
        }
      } elsif ($k == 1) {
        print GI (" $genp[$i][0] =\n");
      }
    }
  }

  for ($i=0;$i<@irnp;$i++){
      $k=0;
      for ($j=0;$j<@nonir;$j++){
        if ($irnp[$i][0] eq $nonir[$j]) {
          $k=1;
        }
      }
      if ($k == 0) {        
        print IP (" $irnp[$i][0] = $irnp[$i][1]\n");
      } elsif ($k == 1) {
        print IP (" $irnp[$i][0] =\n");
      }
  }



  print GI ("&end\n");
  print IP ("&end\n");

  close(GI);
  close(IP);

}



sub full_irc{

  my($trash,$usrin,$i,@genp,@irnp,$wdir,$meppath,@hessarr);

  # ircpar namelist
  my($rpsteps,$maxhypstep,$inidirect,$nmode,$inirtrust,$Rhyp,$conv_grad);
  my($conv_disp,$conv_uwdisp,$conv_cons,$miciter,$conv_alpha,$conv_rad);
  my($soph_l,$maxrtrust,$minrtrust,$lratio,$uratio);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $irnp[0][0]=("rpsteps");
  $irnp[1][0]=("maxhypstep");
  $irnp[2][0]=("inidirect");
  $irnp[3][0]=("nmode");
  $irnp[4][0]=("inirtrust");
  $irnp[5][0]=("Rhyp");
  $irnp[6][0]=("conv_grad");
  $irnp[7][0]=("conv_disp");
  $irnp[8][0]=("conv_uwdisp");
  $irnp[9][0]=("conv_cons");
  $irnp[10][0]=("miciter");
  $irnp[11][0]=("conv_alpha");
  $irnp[12][0]=("conv_rad");
  $irnp[13][0]=("soph_l");
  $irnp[14][0]=("maxrtrust");
  $irnp[15][0]=("minrtrust");
  $irnp[16][0]=("lratio");
  $irnp[17][0]=("uratio");

  &defaults::prepdef($meppath,$wdir,'irc');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@irnp);

  system("rm -f $wdir/def");

  # copy final variables
  $rpsteps=$irnp[0][1];
  $maxhypstep=$irnp[1][1];
  $inidirect=$irnp[2][1];
  $nmode=$irnp[3][1];
  $inirtrust=$irnp[4][1];
  $Rhyp=$irnp[5][1];
  $conv_grad=$irnp[6][1];
  $conv_disp=$irnp[7][1];
  $conv_uwdisp=$irnp[8][1];
  $conv_cons=$irnp[9][1];
  $miciter=$irnp[10][1];
  $conv_alpha=$irnp[11][1];
  $conv_rad=$irnp[12][1];
  $soph_l=$irnp[13][1];
  $maxrtrust=$irnp[14][1];
  $minrtrust=$irnp[15][1];
  $lratio=$irnp[16][1];
  $uratio=$irnp[17][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # start input

  # initialize hessarr
  $hessarr[1][1]='intcfl';
  $hessarr[1][2]='newh';
  $hessarr[1][3]='update';
  $hessarr[1][4]='update';
  $hessarr[1][5]='1';
  $hessarr[2][1]='calc';
  $hessarr[2][2]='newh';
  $hessarr[2][3]='calc';
  $hessarr[2][4]='update';
  $hessarr[2][5]='1';
  $hessarr[3][1]='calc';
  $hessarr[3][2]='newh';
  $hessarr[3][3]='calc';
  $hessarr[3][4]='update';
  $hessarr[3][5]='1000000000';
  $hessarr[4][1]='calcmw';
  $hessarr[4][2]='newh';
  $hessarr[4][3]='calcmw';
  $hessarr[4][4]='update';
  $hessarr[4][5]='1';
  $hessarr[5][1]='calcmw';
  $hessarr[5][2]='newh';
  $hessarr[5][3]='calcmw';
  $hessarr[5][4]='update';
  $hessarr[5][5]='1000000000';

  $trash=1;
  (-e "geninp" || -e "ircpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (IP,">ircpar") or die "Cannot write ircpar\n";
  print IP ("&ircpar\n");

  print ("\nHow many atoms are in the molecule\n");
  $usrin=<STDIN>;
  chomp($usrin);
  print GI ("  nat        =   $usrin\n");

  if ($usrin >= 3) {
    $nintc=3*$usrin-6;
  }  elsif ($usrin == 2) {
    $nintc=1;
  }

  print GI ("  nintc      =   $nintc\n");

  print ("\nHow much memory can be used in the COLUMBUS calculation (Mwords for 5.9.*; MB for 7.0) ?\n");
  print ("Default : $mem\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mem\n"}
  $mem=$usrin;
  print GI ("  mem        =   $usrin");

  print ("\nWhich program will be used to transform the coordiantes ?\n");
  print ("Default  : $coordprog\n");
  print ("Z-matrix : zmat\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$coordprog\n"}
  $coordprog=$usrin;
  chomp($usrin);
  print GI ("  coordprog  =   \'$usrin\'\n");

  if ($coordprog eq "pulay\n") {
    print ("\nShould linear convergence be allowed in the iterative\n");
    print ("backtransformation of the coordinates\n");
    print ("Default NO (linconv) : $linconv\n");
    print ("YES                  : 1\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$linconv\n"}
    $linconv=$usrin;
    print GI ("  linconv    =   $usrin");
  }


  print ("\nWhich print level should be used ?\n");
  print ("Minimum output              : 0\n");
  print ("Standard output (Default)   : $printl\n");
  print ("Debug (huge amount of data) : 2\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$printl\n"}
  $printl=$usrin;
  print GI ("  printl     =   $usrin");

  print ("\nShould molden output be created ?\n");
  print ("Default (YES) : $mld\n");
  print ("NO            : 0\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mld\n"}
  $mld=$usrin;
  print GI ("  mld        =   $usrin");

  print ("\nHessian treatement:\n\n");
  print ("\n -1) Full input\n\n");
  print ("  1) Use a diagonal guess from intcfl and update\n");
  print ("  2) Calculate Hessian with COLUMBUS in each step\n");
  print ("  3) Calculate Hessian with COLUMBUS in the 1st step and update\n");
  print ("  4) Calculate Hessian using the MEP program in each step \n");
  print ("  5) Calculate Hessian using the MEP program in the 1st step and update\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {
    $usrin=1;
  } else { 
    chomp($usrin);
  }
  if ($usrin == -1) {
    print ("\nInitial hessian: \n");
    print ("Calculate with COLUMBUS            : calc\n");
    print ("Calculate with MEP using gradjob   : calcmw\n");
    print ("Approximate (Default)              : $inihess\n");
    print ("Unit matrix                        : unitma\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$inihess\n"}
    $inihess=$usrin;
    chomp($usrin);
    print GI ("  inihess    =   \'$usrin\'\n");

    if ($inihess eq "calc\n") {
      -e "./hessjob/DISPLACEMENT/REFPOINT/geom" or print ("\nHessian job in directory \"hessjob\" is still missing\n\n\n");
    }

    print ("\nMode for initial hessian on subsequent hypershperes: \n");
    print ("Old hessian from the last hypershere  : oldh\n");
    print ("New initial hessian (Default)         : $inihmod\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$inihmod\n"}
    $inihmod=$usrin;
    chomp($usrin);
    print GI ("  inihmod    =   \'$usrin\'\n");

    print ("\nHessian mode:\n");
    print ("Calculate with COLUMBUS            : calc\n");
    print ("Calculate with MEP using gradjob   : calcmw\n");
    print ("Update hessian (Default)           : $hessmod\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$hessmod\n"}
    $hessmod=$usrin;
    chomp($usrin);
    print GI ("  hessmod    =   \'$usrin\'\n");

    if ($hessmod eq "calc\n") {
      -e "./hessjob/DISPLACEMENT/REFPOINT/geom" or print ("\nHessian job in directory \"hessjob\" is still missing\n\n\n");
    }

    print ("\nMixed hessian mode: If the hessian wont be calculated or updated each step,  \n");
    print ("                      what happens to the hessian \n");
    print ("Keep old hessian             : const\n");
    print ("Update (Default)             : $mixmod\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$mixmod\n"}
    $mixmod=$usrin;
    chomp($usrin);
    print GI ("  mixmod     =   \'$usrin\'\n");

    print ("\nAfter how many iterations should the hessian be calculated/updated ?\n");
    print ("Default (each iteration)    : $hessstep\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$hessstep\n"}
    $hessstep=$usrin;
    print GI ("  hessstep   =   $usrin");

  } else {

    print GI ("  inihess    =   \'$hessarr[$usrin][1]\'\n");
    print GI ("  inihmod    =   \'$hessarr[$usrin][2]\'\n");
    print GI ("  hessmod    =   \'$hessarr[$usrin][3]\'\n");
    print GI ("  mixmod     =   \'$hessarr[$usrin][4]\'\n");
    print GI ("  hessstep   =   $hessarr[$usrin][5]\n");
    $inihess=$hessarr[$usrin][1];
    $inihmod=$hessarr[$usrin][2];
    $hessmod=$hessarr[$usrin][3];
    $mixmod=$hessarr[$usrin][4];
    $hessstep=$hessarr[$usrin][5];

  }

  if (($inihess eq "calcmw\n") || ($hessmod eq "calcmw\n")) {
    print ("\nDisplacement for numerical differentiation in Hessian calculation A*sqrt(amu) ?\n");
    print ("Default: $hdisp \n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$hdisp\n"}
    $hdisp=$usrin;
    print GI ("  hdisp      =   $usrin");
  }

  print ("\nShould the Hessian of the langrangian contain the Hessian of the constraint ?\n");
  print ("Default               : $chess\n");
  print ("Do NOT include it     : F\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$chess\n"}
  $chess=$usrin;
  print GI ("  chess      =   $usrin");

  print ("\nHow many steps should be calculated\n");
  $usrin=<STDIN>;
  chomp($usrin);
  $rpsteps=$usrin;
  print IP ("  rpsteps    =   $usrin\n");

  print ("\nWhat is the maximum allowed number of iterations on one hypershere ?\n");
  print ("Default: $maxhypstep\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxhypstep\n"}
  $maxhypstep=$usrin;
  print IP ("  maxhypstep =   $usrin");

  print ("\nRadius of the hypershere in sqrt(amu)*a.u. ? \n");
  print ("Default: $Rhyp \n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$Rhyp\n"}
  $Rhyp=$usrin;
  print IP ("  Rhyp       =   $usrin");

  print ("\nHow should the initial displacement be done (give the direction with the sign)?\n");
  print ("\nFor IRC: ");
  print ("Using normal modes : 1/-1 (provide the cartesian_frequencies file named suscalls) \n");
  print ("\nOther options:\n");
  print (" Using random numbers (not recommended): 0\n");
  print (" Using the gradient : 2/-2  \n");
  print (" Using a geometry   : 3/-3 (filename=pointinggeom)\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$inidirect\n"}
  $inidirect=$usrin;
  print IP ("  inidirect  =   $usrin");

  if (abs($inidirect)==1) {
    print ("\nWhich normal mode should be used ?\n");
    print ("First mode : 1 etc\n");
    print ("Default : $nmode\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$nmode\n"}
    $inidirect=$usrin;
    print IP ("  nmode      =   $usrin");
  }

  if (abs($inidirect)==0) {
    print ("\nFor random geometry: use fixed seed ?\n");
    print ("Default: $fixed_seed \n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$fixed_seed\n"}
    print GI ("  fixed_seed =   $usrin");

    print ("\nFor random geometry: give iseed \n");
    print ("Default: $iseed \n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$iseed\n"}
    print GI ("  iseed      =   $usrin");
  }

  print ("\nWhat is the convergence threshold for the average gradient of each image in aJ/(A*sqrt(amu))?\n");
  print ("Default : $conv_grad\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_grad\n"}
  $conv_grad=$usrin;
  print IP ("  conv_grad  =   $usrin");

  print ("\nWhat is the convergence threshold for the average displacment of each image in sqrt(amu)*A ?\n");
  print ("Default : $conv_disp\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_disp\n"}
  $conv_disp=$usrin;
  print IP ("  conv_disp  =   $usrin");

  print ("\nConvergence threshold for alpha in case of micro iteration aJ/(A^2*amu)?\n");
  print ("Default: $conv_alpha\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_alpha\n"}
  print IP ("  conv_alpha =   $usrin");


  print ("\nConvergence threshold for constraint on the  restricted step A/sqrt(amu)?\n");
  print ("Default: $conv_rad\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_rad\n"}
  print IP ("  conv_rad   =   $usrin");

  print ("\nConvergence threshold for the individual displacement in internal coordinates A|rad ?\n");
  print ("Default: $conv_uwdisp\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_uwdisp\n"}
  print IP ("  conv_uwdisp  = $usrin");


  print ("\nConvergence threshold for the constraint A*sqrt(amu)?\n");
  print ("Default: $conv_cons\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$conv_cons\n"}
  print IP ("  conv_cons  =   $usrin");

  print ("\nHow many micro iterations are allowed in case of restricted step ?\n");
  print ("Default: $miciter\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$miciter\n"}
  print IP ("  miciter    =   $usrin");

  print ("\nCalculate sophisticated langrangian multipier ?\n");
  print ("YES             : T\n");
  print ("No (Default)    : $soph_l\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$soph_l\n"}
  print IP ("  soph_l     =   $usrin");

  print ("\nMaximal allowed trust radius A*sqrt(amu) ?\n");
  print ("Default: $maxrtrust\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxrtrust\n"}
  print IP ("  maxrtrust  =   $usrin");

  print ("\nMinimal allowed trust radius A*sqrt(amu)?\n");
  print ("Default: $minrtrust\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$minrtrust\n"}
  print IP ("  minrtrust  =   $usrin");

  print ("\nLower bound for ratio ?\n");
  print ("Default: $lratio\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$lratio\n"}
  print IP ("  lratio     =   $usrin");

  print ("\nUpper bound for ratio ?\n");
  print ("Default: $uratio\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$uratio\n"}
  print IP ("  uratio     =   $usrin");

  print ("\nHow large should be the initial trust radius A*sqrt(amu) ?\n");
  print ("Default: $inirtrust\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$inirtrust\n"}
  print IP ("  inirtrust  =   $usrin");

  print GI ("&end\n");
  print IP ("&end\n");

  close(GI);
  close(IP);

}



sub med_irc{

  my($trash,$usrin,$i,@genp,@irnp,$wdir,$meppath,@hessarr);

  # ircpar namelist
  my($rpsteps,$maxhypstep,$inidirect,$nmode,$inirtrust,$Rhyp,$conv_grad);
  my($conv_disp,$conv_uwdisp,$conv_cons,$miciter,$conv_alpha,$conv_rad);
  my($soph_l,$maxrtrust,$minrtrust,$lratio,$uratio);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $irnp[0][0]=("rpsteps");
  $irnp[1][0]=("maxhypstep");
  $irnp[2][0]=("inidirect");
  $irnp[3][0]=("nmode");
  $irnp[4][0]=("inirtrust");
  $irnp[5][0]=("Rhyp");
  $irnp[6][0]=("conv_grad");
  $irnp[7][0]=("conv_disp");
  $irnp[8][0]=("conv_uwdisp");
  $irnp[9][0]=("conv_cons");
  $irnp[10][0]=("miciter");
  $irnp[11][0]=("conv_alpha");
  $irnp[12][0]=("conv_rad");
  $irnp[13][0]=("soph_l");
  $irnp[14][0]=("maxrtrust");
  $irnp[15][0]=("minrtrust");
  $irnp[16][0]=("lratio");
  $irnp[17][0]=("uratio");

  &defaults::prepdef($meppath,$wdir,'irc');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@irnp);

  system("rm -f $wdir/def");

  # copy final variables
  $rpsteps=$irnp[0][1];
  $maxhypstep=$irnp[1][1];
  $inidirect=$irnp[2][1];
  $nmode=$irnp[3][1];
  $inirtrust=$irnp[4][1];
  $Rhyp=$irnp[5][1];
  $conv_grad=$irnp[6][1];
  $conv_disp=$irnp[7][1];
  $conv_uwdisp=$irnp[8][1];
  $conv_cons=$irnp[9][1];
  $miciter=$irnp[10][1];
  $conv_alpha=$irnp[11][1];
  $conv_rad=$irnp[12][1];
  $soph_l=$irnp[13][1];
  $maxrtrust=$irnp[14][1];
  $minrtrust=$irnp[15][1];
  $lratio=$irnp[16][1];
  $uratio=$irnp[17][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # start input

  # initialize hessarr
  $hessarr[1][1]='intcfl';
  $hessarr[1][2]='newh';
  $hessarr[1][3]='update';
  $hessarr[1][4]='update';
  $hessarr[1][5]='1';
  $hessarr[2][1]='calc';
  $hessarr[2][2]='newh';
  $hessarr[2][3]='calc';
  $hessarr[2][4]='update';
  $hessarr[2][5]='1';
  $hessarr[3][1]='calc';
  $hessarr[3][2]='newh';
  $hessarr[3][3]='calc';
  $hessarr[3][4]='update';
  $hessarr[3][5]='1000000000';
  $hessarr[4][1]='calcmw';
  $hessarr[4][2]='newh';
  $hessarr[4][3]='calcmw';
  $hessarr[4][4]='update';
  $hessarr[4][5]='1';
  $hessarr[5][1]='calcmw';
  $hessarr[5][2]='newh';
  $hessarr[5][3]='calcmw';
  $hessarr[5][4]='update';
  $hessarr[5][5]='1000000000';

  $trash=1;
  (-e "geninp" || -e "ircpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (IP,">ircpar") or die "Cannot write ircpar\n";
  print IP ("&ircpar\n");

  print ("\nHow many atoms are in the molecule\n");
  $usrin=<STDIN>;
  chomp($usrin);
  print GI ("  nat        =   $usrin\n");

  if ($usrin >= 3) {
    $nintc=3*$usrin-6;
  }  elsif ($usrin == 2) {
    $nintc=1;
  }

  print GI ("  nintc      =   $nintc\n");

  print ("\nHow much memory can be used in the COLUMBUS calculation (Mwords for 5.9.*; MB for 7.0) ?\n");
  print ("Default : $mem\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mem\n"}
  $mem=$usrin;
  print GI ("  mem        =   $usrin");

  print ("\nWhich program will be used to transform the coordiantes ?\n");
  print ("Default  : $coordprog\n");
  print ("Z-matrix : zmat\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$coordprog\n"}
  $coordprog=$usrin;
  chomp($usrin);
  print GI ("  coordprog  =   \'$usrin\'\n");

  print ("\nHessian treatement:\n\n");
  print (" 1)Use a diagonal guess from intcfl and update\n");
  print (" 2)Calculate Hessian with COLUMBUS in each step\n");
  print (" 3)Calculate Hessian with COLUMBUS in the 1st step and update\n");
  print (" 4)Calculate Hessian using the MEP program in each step \n");
  print (" 5)Calculate Hessian using the MEP program in the 1st step and update\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {
    $usrin=1;
  } else {
    chomp($usrin);
  }
  print GI ("  inihess    =   \'$hessarr[$usrin][1]\'\n");
  print GI ("  inihmod    =   \'$hessarr[$usrin][2]\'\n");
  print GI ("  hessmod    =   \'$hessarr[$usrin][3]\'\n");
  print GI ("  mixmod     =   \'$hessarr[$usrin][4]\'\n");
  print GI ("  hessstep   =   $hessarr[$usrin][5]\n");
  $inihess=$hessarr[$usrin][1];
  $inihmod=$hessarr[$usrin][2];
  $hessmod=$hessarr[$usrin][3];
  $mixmod=$hessarr[$usrin][4];
  $hessstep=$hessarr[$usrin][5];

  print ("\nHow many steps should be calculated\n");
  $usrin=<STDIN>;
  chomp($usrin);
  $rpsteps=$usrin;
  print IP ("  rpsteps    =   $usrin\n");

  print ("\nWhat is the maximum allowed number of iterations on one hypershere ?\n");
  print ("Default: $maxhypstep\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$maxhypstep\n"}
  $maxhypstep=$usrin;
  print IP ("  maxhypstep =   $usrin");

  print ("\nRadius of the hypershere in sqrt(amu)*a.u. ? \n");
  print ("Default: $Rhyp \n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$Rhyp\n"}
  $Rhyp=$usrin;
  print IP ("  Rhyp       =   $usrin");

  print ("\nHow should the initial displacement be done (give the direction with the sign)?\n");
  print ("\nFor IRC: ");
  print ("Using normal modes : 1/-1 (provide the cartesian_frequencies file named suscalls) \n");
  print ("\nOther options:\n");
  print (" Using random numbers (not recommended): 0\n");
  print (" Using the gradient : 2/-2  \n");
  print (" Using a geometry   : 3/-3 (filename=pointinggeom)\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$inidirect\n"}
  $inidirect=$usrin;
  print IP ("  inidirect  =   $usrin");

  if (abs($inidirect)==1) {
    print ("\nWhich normal mode should be used ?\n");
    print ("First mode : 1 etc\n");
    print ("Default : $nmode\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$nmode\n"}
    $inidirect=$usrin;
    print IP ("  nmode      =   $usrin");
  }

  print GI ("&end\n");
  print IP ("&end\n");

  close(GI);
  close(IP);

}

sub fast_irc{

  my($trash,$usrin,$i,@genp,@irnp,$wdir,$meppath);

  # ircpar namelist
  my($rpsteps,$maxhypstep,$inidirect,$nmode,$inirtrust,$Rhyp,$conv_grad);
  my($conv_disp,$conv_uwdisp,$conv_cons,$miciter,$conv_alpha,$conv_rad);
  my($soph_l,$maxrtrust,$minrtrust,$lratio,$uratio);

  # geninp namelist
  my($mem,$nproc,$elprog,$coordprog,$printl,$prtstep,$mld,$consttyp);
  my($fixed_seed,$iseed,$restart,$linconv,$hdisp,$inihess,$inihmod);
  my($hessmod,$mixmod,$hessstep,$chess,$nintc,$nat);

  $wdir=pwd;
  chomp($wdir);

  $meppath=$ENV{"MEP"};

  # Initialize default variables
  $genp[0][0]=("mem");
  $genp[1][0]=("nproc");
  $genp[2][0]=("elprog");
  $genp[3][0]=("coordprog");
  $genp[4][0]=("printl");
  $genp[5][0]=("prtstep");
  $genp[6][0]=("mld");
  $genp[7][0]=("consttyp");
  $genp[8][0]=("fixed_seed");
  $genp[9][0]=("iseed");
  $genp[10][0]=("restart");
  $genp[11][0]=("linconv");
  $genp[12][0]=("hdisp");
  $genp[13][0]=("inihess");
  $genp[14][0]=("inihmod");
  $genp[15][0]=("hessmod");
  $genp[16][0]=("mixmod");
  $genp[17][0]=("hessstep");
  $genp[18][0]=("chess");
  $genp[19][0]=("nintc");
  $genp[20][0]=("nat");

  $irnp[0][0]=("rpsteps");
  $irnp[1][0]=("maxhypstep");
  $irnp[2][0]=("inidirect");
  $irnp[3][0]=("nmode");
  $irnp[4][0]=("inirtrust");
  $irnp[5][0]=("Rhyp");
  $irnp[6][0]=("conv_grad");
  $irnp[7][0]=("conv_disp");
  $irnp[8][0]=("conv_uwdisp");
  $irnp[9][0]=("conv_cons");
  $irnp[10][0]=("miciter");
  $irnp[11][0]=("conv_alpha");
  $irnp[12][0]=("conv_rad");
  $irnp[13][0]=("soph_l");
  $irnp[14][0]=("maxrtrust");
  $irnp[15][0]=("minrtrust");
  $irnp[16][0]=("lratio");
  $irnp[17][0]=("uratio");

  &defaults::prepdef($meppath,$wdir,'irc');

  &defaults::definit($wdir,@genp);
  &defaults::definit($wdir,@irnp);

  system("rm -f $wdir/def");

  # copy final variables
  $rpsteps=$irnp[0][1];
  $maxhypstep=$irnp[1][1];
  $inidirect=$irnp[2][1];
  $nmode=$irnp[3][1];
  $inirtrust=$irnp[4][1];
  $Rhyp=$irnp[5][1];
  $conv_grad=$irnp[6][1];
  $conv_disp=$irnp[7][1];
  $conv_uwdisp=$irnp[8][1];
  $conv_cons=$irnp[9][1];
  $miciter=$irnp[10][1];
  $conv_alpha=$irnp[11][1];
  $conv_rad=$irnp[12][1];
  $soph_l=$irnp[13][1];
  $maxrtrust=$irnp[14][1];
  $minrtrust=$irnp[15][1];
  $lratio=$irnp[16][1];
  $uratio=$irnp[17][1];

  $mem=$genp[0][1];
  $nproc=$genp[1][1];
  $elprog=$genp[2][1];
  $coordprog=$genp[3][1];
  $printl=$genp[4][1];
  $prtstep=$genp[5][1];
  $mld=$genp[6][1];
  $consttyp=$genp[7][1];
  $fixed_seed=$genp[8][1];
  $iseed=$genp[9][1];
  $restart=$genp[10][1];
  $linconv=$genp[11][1];
  $hdisp=$genp[12][1];
  $inihess=$genp[13][1];
  $inihmod=$genp[14][1];
  $hessmod=$genp[15][1];
  $mixmod=$genp[16][1];
  $hessstep=$genp[17][1];
  $chess=$genp[18][1];
  $nintc=$genp[19][1];
  $nat=$genp[20][1];

  # start input

  $trash=1;
  (-e "geninp" || -e "ircpar") or $trash=0;
  if ($trash eq "1") {
    print ("\nDo you want to overwrite the old input (y/n) ?\n");
    $usrin=<STDIN>;
    if ($usrin ne "y\n") { return }
  }

  system("rm -f $wdir/nebpar $wdir/geninp $wdir/ircpar");


  open (GI,">geninp") or die "Cannot write geninp\n";
  print GI ("&geninp\n");
  open (IP,">ircpar") or die "Cannot write ircpar\n";
  print IP ("&ircpar\n");

  print ("\nHow many atoms are in the molecule\n");
  $usrin=<STDIN>;
  chomp($usrin);
  print GI ("  nat        =   $usrin\n");

  if ($usrin >= 3) {
    $nintc=3*$usrin-6;
  }  elsif ($usrin == 2) {
    $nintc=1;
  }

  print GI ("  nintc      =   $nintc\n");

  print ("\nHow much memory can be used in the COLUMBUS calculation (Mwords for 5.9.*; MB for 7.0) ?\n");
  print ("Default : $mem\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$mem\n"}
  $mem=$usrin;
  print GI ("  mem        =   $usrin");

  print ("\nHow many steps should be calculated\n");
  $usrin=<STDIN>;
  chomp($usrin);
  $rpsteps=$usrin;
  print IP ("  rpsteps    =   $usrin\n");

  print ("\nHow should the initial displacement be done (give the direction with the sign)?\n");
  print ("\nFor IRC: ");
  print ("Using normal modes : 1/-1 (provide the cartesian_frequencies file named suscalls) \n");
  print ("\n(Other options:\n");
  print (" Using random numbers (not recommended): 0\n");
  print (" Using the gradient : 2/-2  \n");
  print (" Using a geometry   : 3/-3 (filename=pointinggeom))\n");
  $usrin=<STDIN>;
  if ($usrin eq "\n") {$usrin="$inidirect\n"}
  $inidirect=$usrin;
  print IP ("  inidirect  =   $usrin");

  if (abs($inidirect)==1) {
    print ("\nWhich normal mode should be used ?\n");
    print ("First mode : 1 etc\n");
    print ("Default : $nmode\n");
    $usrin=<STDIN>;
    if ($usrin eq "\n") {$usrin="$nmode\n"}
    $inidirect=$usrin;
    print IP ("  nmode      =   $usrin");
  }

  print GI ("&end\n");
  print IP ("&end\n");

  close(GI);
  close(IP);

}

sub checkneb {
  system("clear");
  print ("Checking for files \n");
  -e "geninp" or print ("\nFile geninp is still missing\n");
  -e "nebpar" or print ("\nFile nebpar is still missing\n");
  -e "DISPLACEMENT/displfl" or print ("\nDISPLACEMENT (displfl) is still missing\n");

}

sub checkirc {
  system("clear");
  print ("Checking for files \n");

  -e "gradjob/control.run" or print ("\nThe gradjob (control.run) is still missing\n");
  -e "geninp" or print ("\nFile geninp is still missing\n");
  -e "ircpar" or print ("\nFile ircpar is still missing\n");
  -e "geom.start" or print ("\nFile geom.start is still missing\n");
  -e "mocoef.start" or print ("\nFile mocoef.start is still missing\n\n");
  -e "hessjob/control.run" or print ("\nThe hessjob (control.run) may be missing\n");
  -e "suscalls" or print ("\nFile suscalls may be missing\n");
  -e "pointinggeom" or print ("\nFile pointinggeom may be missing\n");

}

