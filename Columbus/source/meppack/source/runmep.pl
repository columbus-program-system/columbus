#! /usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use strict;
use warnings;
use Shell qw(pwd cd cp mkdir date mv cat);

# (c) Bernhard Sellner 2011
# part of MEPPACK
my ($cbus,$wdir,$meppath);

$meppath=$ENV{"MEP"};

$wdir=pwd;
chomp($wdir);

###################################################################
#
# Start of program
#
###################################################################


if ((-e "$wdir/ircpar") and (-e "$wdir/nebpar")) {
  die "Please delete one of the files:\n\nnebpar\nircpar\n\n";
}elsif (-e "$wdir/ircpar") {
  system("$meppath/runirc.pl");
}elsif (-e "$wdir/nebpar") {
  system("$meppath/runneb.pl");
}

