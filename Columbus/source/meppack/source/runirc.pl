#! /usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use strict;
use warnings;
use Shell qw(pwd cd cp mkdir date mv cat);
use lib $ENV{"MEP"};
use defaults qw(test writerestart getkeyw definit prepdef);
# (c) Bernhard Sellner 2011
# part of MEPPACK
my ($wdir,$step,$prev,$iter,$see,$s,$ref,$inidirect,$elprog);
my ($coordprog,$printl,$nintc,$nat,$inihess,$hessmod,$trash);
my ($nmode,$rpsteps,$maxhypstep,$datetime,$consttyp,$version);
my ($mem,$cbus,@geni,$i,$inihmod,$mixmod,$hessstep,$chess);
my ($restart,$ii,$it,@genp,@irnp,$meppath,$inirtrust,$Rhyp);
my ($conv_grad,$conv_disp,$conv_uwdisp,$conv_cons,$miciter);
my ($conv_alpha,$conv_rad,$soph_l,$maxrtrust,$minrtrust,$lratio);
my ($uratio,$nproc,$prtstep,$mld,$fixed_seed,$iseed);
my ($linconv,$hdisp);

@geni=('rpsteps','maxhypstep','elprog','coordprog','printl','nintc','Nat','inidirect','inihess','inihmod','hessmod','mixmod','hessstep','nmode','consttyp','inirtrust','Rhyp','fixed_seed','iseed');

# initialize
$cbus=$ENV{"COLUMBUS"};
$meppath=$ENV{"MEP"};

$wdir=pwd;
chomp($wdir);

###################################################################
#
# Start of program
#
###################################################################

$version='2.3';

open(LOG,">>$wdir/irc.log") or die "Cannot write logfile";
print LOG ("********************************************************\n");
print LOG ("***                                                  ***\n");
print LOG ("***     MINIMUM ENERGY PATH PROGRAM PACKAGE          ***\n");
print LOG ("***                  MEPPACK                         ***\n");
print LOG ("***               version $version                        ***\n");
print LOG ("***                                                  ***\n");
print LOG ("***          University of Vienna                    ***\n");
print LOG ("***          Institute for Theoretical Chemistry     ***\n");
print LOG ("***          Bernhard Sellner, Hans Lischka          ***\n");
print LOG ("***                                                  ***\n");
print LOG ("***       bernhard.sellner\@univie.ac.at              ***\n");
print LOG ("***       hans.lischka\@univie.ac.at                  ***\n");
print LOG ("***                                                  ***\n");
print LOG ("********************************************************\n\n\n");

$datetime=date;

print ("\n\nMEP calculation started on $datetime\n\n");

print LOG ("\n\nMEP calculation started on $datetime\n\n");

# Initialize variables
$genp[0][0]=("mem");
$genp[1][0]=("nproc");
$genp[2][0]=("elprog");
$genp[3][0]=("coordprog");
$genp[4][0]=("printl");
$genp[5][0]=("prtstep");
$genp[6][0]=("mld");
$genp[7][0]=("consttyp");
$genp[8][0]=("fixed_seed");
$genp[9][0]=("iseed");
$genp[10][0]=("restart");
$genp[11][0]=("linconv");
$genp[12][0]=("hdisp");
$genp[13][0]=("inihess");
$genp[14][0]=("inihmod");
$genp[15][0]=("hessmod");
$genp[16][0]=("mixmod");
$genp[17][0]=("hessstep");
$genp[18][0]=("chess");
$genp[19][0]=("nintc");
$genp[20][0]=("nat");

$irnp[0][0]=("rpsteps");
$irnp[1][0]=("maxhypstep");
$irnp[2][0]=("inidirect");
$irnp[3][0]=("nmode");
$irnp[4][0]=("inirtrust");
$irnp[5][0]=("Rhyp");
$irnp[6][0]=("conv_grad");
$irnp[7][0]=("conv_disp");
$irnp[8][0]=("conv_uwdisp");
$irnp[9][0]=("conv_cons");
$irnp[10][0]=("miciter");
$irnp[11][0]=("conv_alpha");
$irnp[12][0]=("conv_rad");
$irnp[13][0]=("soph_l");
$irnp[14][0]=("maxrtrust");
$irnp[15][0]=("minrtrust");
$irnp[16][0]=("lratio");
$irnp[17][0]=("uratio");

&defaults::prepdef($meppath,$wdir,'irc');

&defaults::definit($wdir,@genp);
&defaults::definit($wdir,@irnp);

system("rm -f $wdir/def");

# read the input files
for($i=0;$i<@genp;$i++){
  $genp[$i][1]=&defaults::getkeyw('geninp',$genp[$i][0],$genp[$i][1],$wdir);
}
for($i=0;$i<@irnp;$i++){
  $irnp[$i][1]=&defaults::getkeyw('ircpar',$irnp[$i][0],$irnp[$i][1],$wdir);
}

# copy final variables
$rpsteps=$irnp[0][1];
$maxhypstep=$irnp[1][1];
$inidirect=$irnp[2][1];
$nmode=$irnp[3][1];
$inirtrust=$irnp[4][1];
$Rhyp=$irnp[5][1];
$conv_grad=$irnp[6][1];
$conv_disp=$irnp[7][1];
$conv_uwdisp=$irnp[8][1];
$conv_cons=$irnp[9][1];
$miciter=$irnp[10][1];
$conv_alpha=$irnp[11][1];
$conv_rad=$irnp[12][1];
$soph_l=$irnp[13][1];
$maxrtrust=$irnp[14][1];
$minrtrust=$irnp[15][1];
$lratio=$irnp[16][1];
$uratio=$irnp[17][1];

$mem=$genp[0][1];
$nproc=$genp[1][1];
$elprog=$genp[2][1];
$coordprog=$genp[3][1];
$printl=$genp[4][1];
$prtstep=$genp[5][1];
$mld=$genp[6][1];
$consttyp=$genp[7][1];
$fixed_seed=$genp[8][1];
$iseed=$genp[9][1];
$restart=$genp[10][1];
$linconv=$genp[11][1];
$hdisp=$genp[12][1];
$inihess=$genp[13][1];
$inihmod=$genp[14][1];
$hessmod=$genp[15][1];
$mixmod=$genp[16][1];
$hessstep=$genp[17][1];
$chess=$genp[18][1];
$nintc=$genp[19][1];
$nat=$genp[20][1];

print LOG (" Input setting for geninp:\n\n");
for ($i=0;$i<@genp;$i++){
  print LOG (" $genp[$i][0] = $genp[$i][1]\n");
}
print LOG ("\n Input setting for ircpar:\n\n");
for ($i=0;$i<@irnp;$i++){
  print LOG (" $irnp[$i][0] = $irnp[$i][1]\n");
}

print LOG ("\n\nPerforming optimization on hypershere\n");

check();


if ($printl >= 1) {
  # write input for program to log file
  print LOG ("\nContent of input files:\n");

  print LOG ("\nContent of geninp:\n\n");
  open(GEN,"$wdir/geninp") or die "Cannot read geninp\n";
  while($_=<GEN>) {
    print LOG ("$_");
  }

  print LOG ("\nContent of ircpar:\n\n");
  open(RSP,"$wdir/ircpar") or die "Cannot read ircpar\n";
  while($_=<RSP>) {
    print LOG ("$_");
  } 


}

if ($restart == 0) {

print LOG ("\n\n Start calculating MEP \n\n");

# avoid errors due to old files:
electronic('col','cleanup','');

$step=0;


#initialize things in case of first step 
-e "curr_step" or $step=1 ;
if ($step == 1){
  open(STE,">curr_step") or die "Cannot write curr_step";
  print STE "$step\n";
  close(STE);
  mkdir ("temp");
  mkdir ("results");
  mkdir ("calcgrad");
  mkdir ("calcopt");
  mkdir ("restart");
  mkdir ("$wdir/results/DISPLACEMENT");
  if (($hessmod eq "calc") or ($inihess eq "calc")) {
    system ("cp -r $wdir/hessjob $wdir/restart");
  }
  if ($printl >= 2) {
    system("mkdir $wdir/DEBUG");
  }
} else {
  open(STE,"<curr_step") or die "Cannot read from curr_step";
  chop($_=<STE>);
  $step=$_;
  close(STE);
}

&defaults::writerestart("2",$wdir);

open(DIS,">$wdir/results/DISPLACEMENT/displfl") or die "Cannot write dispfl";
print DIS ("$nintc   /number of internal coordinates\n");
print DIS ("no  /calculate dip.mom.derivatives\n");
print DIS ("no  /calculate reference point\n");
print DIS ("$nmode   0\n");

} elsif ($restart == 1) {

   -e "$wdir/restart/curr_step" or die "\n\n Restarting failed : $wdir/restart/curr_step missing \n\n";
   -e "$wdir/restart/curr_iter" or die "\n\n Restarting failed : $wdir/restart/curr_iter missing \n\n";

   open(DIS,">>$wdir/results/DISPLACEMENT/displfl") or die "Cannot append dispfl";

   print LOG ("\n Restarting MEP job \n");

  open(ITE,"<$wdir/restart/curr_step") or die "Cannot read from curr_step";
  chop($_=<ITE>);
  $step=$_;
  close(ITE);

  open(ITE,"<$wdir/restart/calcopt/curr_iter") or die "Cannot read from curr_iter";
  chop($_=<ITE>);
  $it=$_;
  close(ITE);
  $it++;

  system("rm -rf $wdir/calcgrad/*");
  system ("rm -rf $wdir/calcopt");
  system ("rm -rf $wdir/temp/*");
  system ("cp -r $wdir/restart/calcopt $wdir/");
  if (-e "$wdir/DEBUG/$step/DISPLACEMENT/CALC.c$nmode.d$it/geom") {
    system ("rm -rf $wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter");
  }

} elsif ($restart == 2) {
  # restart a new step
   -e "$wdir/restart/curr_step" or die "\n\n Restarting failed \n\n";
   open(DIS,">>$wdir/results/DISPLACEMENT/displfl") or die "Cannot append dispfl";

   print LOG ("\n Continuing MEP job \n");

  open(ITE,"<$wdir/restart/curr_step") or die "Cannot read from curr_step";
  chop($_=<ITE>);
  $step=$_;
  close(ITE);
  $step++;
}

##################################################################
#
#  main loop
#
##################################################################

for($s=$step;$s<=$rpsteps;$s++){

  if ($restart != 1) {

  open(STE,">curr_step") or die "Cannot write curr_step";
  print STE "$s\n";
  close(STE);

  cp ("$wdir/curr_step", "$wdir/restart/");

  if ($s == 1) {
    electronic('col','ini',$mem);
    if ($mld eq 1) {
      molden("ini");
    }
  }  


  $ref=$s-1;
  $prev=$s-2;

  print LOG ("\n#################################################################################################\n");
  print LOG ("\n                                           STEP $s                                               \n");
  print LOG ("\n#################################################################################################\n\n");


  # make a guess for the geometry on the hyperspehre
  chdir "temp";
  cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/geom", "$wdir/temp/geom");
  cp ("$wdir/curr_step", "$wdir/temp/");
  cp ("$wdir/geninp", "$wdir/temp/");
  cp ("$wdir/ircpar", "$wdir/temp/");


  electronic('col','initstep',$mem);

  runprog('guessgeom');

  cp ("$wdir/temp/geom", "$wdir/calcopt/curr_geom");
  cp ("$wdir/temp/ref_geom", "$wdir/calcopt");

  system("rm -f $wdir/temp/*");
  chdir "$wdir";


  # calculate everything for reference geometry
  electronic('col','ref',$mem);


  # get cartesian gradient and geometry
  electronic('col','curr',$mem);

  cp ("$wdir/calcopt/curr_bmatrix", "$wdir/calcopt/first_bmatrix");

  # convert gradients to internal
  coordsystem($coordprog,'grad2int','curr');

  cp ("$wdir/calcopt/curr_gradE_q", "$wdir/calcopt/gradE_q.all");
  
  open(AGE,">>$wdir/calcopt/gradE_q.all") or die "Cannot append to gradE_q.all";
  print AGE ("\n");
  close(AGE);
  open(ABM,">>$wdir/calcopt/bmatrix.all") or die "Cannot append to bmatrix.all";
  print ABM ("\n");
  close(ABM);
  open(AIG,">>$wdir/calcopt/intgeom.all") or die "Cannot append to intgeom.all";
  print AIG ("\n");
  close(AIG);


  if ($printl >= 2) {
    mkdir("$wdir/DEBUG/$s");
    mkdir("$wdir/DEBUG/$s/DISPLACEMENT");
    open(DIS2,">$wdir/DEBUG/$s/DISPLACEMENT/displfl") or die "Cannot write dispfl";
    print DIS2 ("$nintc   /number of internal coordinates\n");
    print DIS2 ("no  /calculate dip.mom.derivatives\n");
    print DIS2 ("no  /calculate reference point\n");
  }


  open(STE,">$wdir/calcopt/curr_iter") or die "Cannot write curr_iter";
  print STE "0\n";
  close(STE);
  cp ("$wdir/geninp", "$wdir/calcopt/");
  cp ("$wdir/ircpar", "$wdir/calcopt/");

  # make initial hessian

  $iter=0;

  if ($s == 1) {
    if ($inihess eq "calc")  {
      electronic('col','colhess',$mem);
    } elsif ($inihess eq "calcmw")  {
      electronic('col','mephess',$mem);
    } elsif ($inihess eq "intcfl")   {
      cp ("$wdir/calcopt/curr_intcfl", "$wdir/calcopt/intcfl");
      cp ("$wdir/calcopt/curr_geom", "$wdir/calcopt/geom");
      cp ("$wdir/calcopt/curr_intgeom", "$wdir/calcopt/prev_intgeom");
      chdir "$wdir/calcopt";
      runprog('hessian');
      chdir "$wdir";
    } elsif ($inihess eq "unitma") {
      chdir "$wdir/calcopt";
      cp ("$wdir/calcopt/curr_geom", "$wdir/calcopt/geom");
      runprog('hessian');
      chdir "$wdir"; 
    }
  } else {
    if ($inihmod eq "newh") {
      if ($inihess eq "calc")  {
        electronic('col','colhess',$mem);
      } elsif ($inihess eq "calcmw")  {
        electronic('col','mephess',$mem);
      } elsif ($inihess eq "intcfl")   {
        cp ("$wdir/calcopt/curr_intcfl", "$wdir/calcopt/intcfl");
        cp ("$wdir/calcopt/curr_geom", "$wdir/calcopt/geom");
        cp ("$wdir/calcopt/curr_intgeom", "$wdir/calcopt/prev_intgeom");
        chdir "$wdir/calcopt";
        runprog('hessian');
        chdir "$wdir";
      } elsif ($inihess eq "unitma") {
        chdir "$wdir/calcopt";
        cp ("$wdir/calcopt/curr_geom", "$wdir/calcopt/geom");
        runprog('hessian');
        chdir "$wdir"; 
      }
    } elsif ($inihmod eq "oldh")  {
      print LOG ("Taking the last available hessian from:\n");
      print LOG ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/mwhessian\n\n");
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/mwhessian", "$wdir/calcopt/");
    }
  }

  $it=1;
  
  } else {     # if ($restart != 1)

  $restart=0;
  $ref=$s-1;
  if ($printl >= 2) {
    open(DIS2,">>$wdir/DEBUG/$s/DISPLACEMENT/displfl") or die "Cannot write dispfl";
  }

  }

##############################################################
#
#  loop over steps on one hyperspehre
#
##############################################################

  for($iter=$it;$iter<=$maxhypstep;$iter++){

    print LOG ("\n§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§\n");
    print LOG ("                                 Iteration $iter on hypershere                                     \n");
    print LOG ("§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§\n\n");

    if ($iter ==2) {
      &defaults::writerestart("1",$wdir);
    }

    if ($iter >=2) {
      system ("mv $wdir/restart/calcopt $wdir/restart/bk_calcopt");
      system ("cp -r $wdir/calcopt $wdir/restart/");
      system ("rm -rf $wdir/restart/bk_calcopt");
    } elsif ($iter == 1) {
      system ("cp -r $wdir/calcopt $wdir/restart/");
    } 

    open(STE,">$wdir/calcopt/curr_iter") or die "Cannot write curr_iter";
    print STE "$iter\n";
    close(STE);

    cp ("$wdir/calcopt/curr_iter", "$wdir/restart/");

    print ("\n Iteration $iter of step $s \n\n");



  
    # make RS PCO optimization
    chdir "$wdir/calcopt";
    cp ("$wdir/geninp", "$wdir/calcopt/");
    cp ("$wdir/ircpar", "$wdir/calcopt/");
    cp ("$wdir/curr_step", "$wdir/calcopt/");
    cp ("$wdir/geom.start", "$wdir/calcopt/geom.start");

    runprog('RS_PCO');
    chdir "$wdir";

    # convert new_intgeom to cartesian
    coordsystem($coordprog,'i2c','');

    # write molden file
    if ($mld eq 1) {
      molden("next");
    } 

    # calculate new Energy
    electronic('col','new',$mem);

    # check convergence
    chdir "calcopt";
    open(CON,"convergence") or die "Cannot open convergence\n";
    $_=<CON>;
    chop($_);
    close(CON);
    if ($_ eq 'T'){
      if ($printl >= 2) {
        print DIS2 ("$nmode   $iter\n");
        mkdir("$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter");
        electronic('col','copydebug','');
        close (DIS2);
      }
      chdir "$wdir";
      print DIS ("$nmode   $s\n");
      mkdir ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s");
      electronic('col','copyresults','');
      system("rm -rf $wdir/calcgrad/*");
      $datetime=date;
      print LOG ("\n\nStep $s converged within $iter iterations\n\n");
      #print LOG ("\n\nStep $s converged within $iter iterations on $datetime\n\n");
      print ("\n\nStep $s converged on $datetime \n\n");
      #  exit this loop
      if ($printl >= 2) {
        cp ("$wdir/calcopt/see_*", "$wdir/DEBUG/$s/");
        cp ("$wdir/calcopt/*.all", "$wdir/DEBUG/$s/");
      }
      cp ("$wdir/calcopt/curr_step", "$wdir/restart/");
      last;
    }
    if (($_ eq 'F') and ($iter == $maxhypstep)) {
      print LOG ("Failed to converge within $maxhypstep iterations on\n");
      print LOG ("the hypershere.\n\n");
      print LOG ("PROGRAM TERMINATION WITH ERROR\n\n\n");
      die 'ERROR';
    }
    chdir "$wdir";


  
 
    # calculate ratio and update trustradius
    chdir "calcopt";
    cp ("$wdir/geninp", "$wdir/calcopt/");
    cp ("$wdir/ircpar", "$wdir/calcopt/");
    runprog('check_Rtrust'); 
    $see=(!(-e "reject"));
    chdir "$wdir";
    if ($see eq "1"){
      $datetime=date;
      if ($printl >= 0) {
        print LOG ("Iteration $iter on hyperspehre accepted on $datetime \n\n");
      }
      print ("Iteration $iter on hyperspehre accepted on $datetime \n");
      # convert gradients to internal
      coordsystem($coordprog,'grad2int','new');  
   

      if ($printl >= 2) {
        print DIS2 ("$nmode   $iter\n");
        mkdir("$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter");
        electronic('col','copydebug','');
        system("rm -rf $wdir/calcgrad/*");
      }

      chdir "$wdir/calcopt";
      electronic('col','newtocurr','');
      # hessian
      if ($hessmod eq "update") {
        chdir "$wdir/calcopt";
        cp ("$wdir/calcopt/curr_geom", "$wdir/calcopt/geom");
        runprog('hessian');
        chdir "$wdir";
      } elsif (($hessmod eq "calc") or ($hessmod eq "calcmw")) {
        if (( $iter % $hessstep)  ==0) {
          if ($hessmod eq "calc") {
            electronic('col','colhess',$mem);
          } elsif ($hessmod eq "calcmw") {
            electronic('col','mephess',$mem);
          }
        } elsif (($iter % $hessstep)  !=0) {
          if ($mixmod eq "update") {
            chdir "$wdir/calcopt";
            cp ("$wdir/calcopt/curr_geom", "$wdir/calcopt/geom");
            runprog('hessian');
            chdir "$wdir";
          }
        }
      }
     

      chdir "$wdir";
    } else {
      print("\nStep not accepted\n");
      system("rm -rf $wdir/calcgrad/*");
      chdir "calcopt";
      system("rm -f new_*");
      system("rm -rf reject");
      chdir "$wdir";
    }
  } #end of loop on hypersphere


  &defaults::writerestart("2",$wdir);


  system("rm -rf $wdir/calcgrad/*");
  system("rm -rf $wdir/calcopt/*");

  chdir "$wdir";

} # end of main loop
 
close(DIS);

      # write molden file
      if ($mld eq 1) {
        molden("collectmold");
      }


$datetime=date;
print LOG ("MEP calculation finished on $datetime\n\n");


close (LOG);

  open(STE,">curr_step") or die "Cannot write curr_step";
  $step=$step+1;
  print STE "$s\n";
  close(STE);
print STDOUT ("\nMEP calculation finished on $datetime\n\n");



###################################################################
#
# Subroutines
#
###################################################################


###################################################################
# Independent
###################################################################

sub runprog{
  my ($prog)=@_;
  close (LOG);
  system("\$MEP/$prog.x");
  open(LOG,">>$wdir/irc.log") or die "Cannot reopen logfile";
  open(FLOG,"$prog.log") or die "Cannot open $prog.log file";
  while($_=<FLOG>) {
    chomp($_);
    if ($_ eq 'ERROR'){
      die "\nERROR in fortran program $prog \n terminating.\n"
    }
    print LOG ("$_\n");
  }
  print LOG ("\n\n");
  
}

sub check {
  -e "$wdir/ircpar" or die "\nMissing ircpar\n\n";
  -e "$wdir/geninp" or die "\nMissing geninp\n\n";
  -e "$wdir/geom.start" or die "\nMissing geom.start\n\n";

  if (abs($inidirect == 1)) {
    -e "$wdir/suscalls" or die "\nMissing suscalls\n\n";
  }

  if ($elprog eq 'col') {
    -e "$wdir/mocoef.start" or die "\nMissing mocoef.start\n\n";
    -e "$wdir/gradjob/control.run" or die "\nProblem with COLUMBUS gradient job\n\n";
  } elsif ($elprog eq 'om2') {
    -e "$wdir/gradjob.inp" or die "\nMissing gradjob.inp\n\n";
  }
  
  if (($hessmod eq 'calc')or($inihess eq 'calc')) {
    -e "$wdir/hessjob/DISPLACEMENT/displfl" or die "\nProblem with COLUMBUS hessian job\n\n";
  }
     
  print LOG ("\n Successfully checked key files\n");
}





###################################################################
# Interface to electronic structure programs
###################################################################


sub electronic{
  my ($prog,$type,$mem)=@_;
  my (@icoord,@temp,$hdisp,$i,$j,$k);


  if ($prog eq 'col') {

    if ($type eq 'initstep') {


      if ($s == 1) {
        if (abs($inidirect) == 1) {
          # along normal mode
          cp ("$wdir/suscalls", "$wdir/temp/suscalls");
        } elsif (abs($inidirect) == 2) {
          # along gradient
          cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/cartgrd", "$wdir/temp/cartgrd");
        } elsif (abs($inidirect) == 3) {
          cp ("$wdir/pointinggeom", "$wdir/temp/");
        }
      } else {
        # extrapolation using old data
        cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/mw_gradE_q", "$wdir/temp/mw_gradE_q");
        cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/intgeom", "$wdir/temp/intgeom");
        cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/bmatrix", "$wdir/temp/bmatrix");
        cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/cartgrd", "$wdir/temp/cartgrd");
        cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$prev/geom", "$wdir/temp/geom_prev");
      }

    } elsif ($type eq 'checkerr') {
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        if (!(/creating symbolic link/)) {
          $i=$i+1;
        }
      }
      if ($i != 0) {
        print LOG ("\nError in $cbus/runc -m $mem >runls 2>>error.log\n\n");
        die "Error in gradient calculation\n\n"
      } else {
        print LOG ("$cbus/runc -m $mem >runls 2>>error.log: SUCCESS \n");
      }


    } elsif ($type eq 'colhess') {
      chdir "$wdir/calcgrad";
      print LOG ("Starting COLUMBUS hessian calculation\n\n");
      system ("cp -r $wdir/hessjob/* $wdir/calcgrad/");
      cp ("$wdir/calcopt/curr_intgeom", "$wdir/calcgrad/");
      cp ("$wdir/calcopt/curr_gradE_q", "$wdir/calcgrad/");
      cp ("$wdir/calcopt/ref_intgeom", "$wdir/calcgrad/");
      cp ("$wdir/calcopt/curr_iter", "$wdir/calcgrad/");
      if ($iter >= 1) {
      cp ("$wdir/calcopt/lambda", "$wdir/calcgrad/");
      }
      cp ("$wdir/calcopt/curr_geom", "$wdir/calcgrad/geom.start");
      cp ("$wdir/calcopt/curr_geom", "$wdir/calcgrad/geom");
      cp ("$wdir/calcopt/curr_mocoef", "$wdir/calcgrad/mocoef");
      if ($iter != 0) { 
        cp ("$wdir/calcopt/curr_bmatrix", "$wdir/calcgrad/");
        cp ("$wdir/calcopt/curr_bmatrix", "$wdir/calcgrad/");
        cp ("$wdir/calcopt/prev_bmatrix", "$wdir/calcgrad/");
        cp ("$wdir/calcopt/ref_bmatrix", "$wdir/calcgrad/");
      } else {
        cp ("$wdir/calcopt/curr_bmatrix", "$wdir/calcgrad/");
      }
      cp ("$wdir/calcopt/curr_intcfl", "$wdir/calcgrad/intcfl");
      cp ("$wdir/geninp", "$wdir/calcgrad/");
      cp ("$wdir/ircpar", "$wdir/calcgrad/");
      open(DIS3,"<$wdir/calcgrad/DISPLACEMENT/displfl") or die "Cannot read dispfl for hessian";
      chomp ($_=<DIS3>);
      chomp ($_=<DIS3>);
      chomp ($_=<DIS3>);
      cp ("$wdir/calcgrad/geom", "$wdir/calcgrad/DISPLACEMENT/REFPOINT/");
      cp ("$wdir/calcgrad/mocoef", "$wdir/calcgrad/DISPLACEMENT/REFPOINT/");
      while($_=<DIS3>){
        ($i,$j,$k)=split(/\s+/,$_);
        if ($k eq 'doit') {
          chdir "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j";
          cp ("$wdir/calcgrad/geom", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j/");
          cp ("$wdir/calcgrad/mocoef", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j/");
          cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j/geom","$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j/geom.start");
          system("$cbus/bmat.x < bmatin > bmatls");
          cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j/geom","$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j/geom.displ");
          cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$j/bmatls", "$wdir/calcgrad/curr_bmatls");
        }
      }
      close (DIS3);
      chdir "$wdir/calcgrad";
      system("nohup $cbus/calc.pl -m $mem >& runls");
      system("grep -i 'Error' runls | grep -v 'runc.error' >error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) {
        print LOG ("\nError in nohup $cbus/calc.pl -m $mem >& runls\n\n");
        die "Error in hessian calculation\n\n"
      } else {
        print LOG ("nohup $cbus/calc.pl -m $mem >& runls: SUCCESS \n\n");
      }

      system("$cbus/forceconst.pl -$mem >& runls");
      system("grep -i 'Error' runls | grep -v 'runc.error' >error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) {
        print LOG ("\nError in $cbus/forceconst.pl -$mem >& runls\n\n");
        die "Error in hessian calculation\n\n"
      } else {
        print LOG ("$cbus/forceconst.pl -$mem >& runls: SUCCESS \n\n");
      }

      if ($iter >= 2) {
      }

      runprog('hessian');
      cp ("$wdir/calcgrad/mwhessian", "$wdir/calcopt/");
      cp ("$wdir/calcgrad/hessian", "$wdir/calcopt/");
      system("rm -rf $wdir/calcgrad/*");

      print LOG ("Calculation of hessian with COLUMBUS: SUCCESS\n");

      chdir "$wdir";


    } elsif ($type eq 'mephess') {
      chdir "$wdir/calcgrad";
      print LOG ("Starting hessian calculation\n\n");
      # curr data will be used. Pay attention that call is after newtocurr
      # get geom.start (REFPOINT)
      cp ("$wdir/calcopt/curr_intgeom", "$wdir/calcgrad/");
      cp ("$wdir/calcopt/curr_gradE_q", "$wdir/calcgrad/");
      cp ("$wdir/calcopt/ref_intgeom", "$wdir/calcgrad/");
      cp ("$wdir/calcopt/curr_iter", "$wdir/calcgrad/");
      if ($iter >= 1) {
      cp ("$wdir/calcopt/lambda", "$wdir/calcgrad/");
      cp ("$wdir/calcopt/prev_bmatrix", "$wdir/calcgrad/");
      }
      cp ("$wdir/calcopt/curr_geom", "$wdir/calcgrad/geom.start");
      cp ("$wdir/calcopt/curr_geom", "$wdir/calcgrad/geom");
      cp ("$wdir/calcopt/curr_mocoef", "$wdir/calcgrad/mocoef");
      cp ("$wdir/calcopt/curr_intcfl", "$wdir/calcgrad/intcfl");
      cp ("$wdir/calcopt/curr_bmatrix", "$wdir/calcgrad/");
      cp ("$wdir/geninp", "$wdir/calcgrad/");
      cp ("$wdir/ircpar", "$wdir/calcgrad/");
      cp ("$wdir/gradjob/control.run", "$wdir/calcgrad/");
   


      $hdisp=0.001;  # default displacement for hessian
      mkdir "$wdir/calcgrad/DISPLACEMENT";

      open(DIS3,">$wdir/calcgrad/DISPLACEMENT/displfl") or die "Cannot write dispfl for hessian";
      print DIS3 ("$nintc /number of internal coordinates\n");
      print DIS3 ("no  /calculate dip.mom.derivatives\n");
      print DIS3 ("yes  /reference point calculation\n");

      #run fortran program for mass weigthed displacements
      print LOG ("Preparing mass weighted displacements");
      runprog('mwdisp');

      # get internal coordinates
      open(IC,"<$wdir/calcgrad/digs") or die "Cannot open digs to read";
      @icoord=0;
      for ($i=1;$i<=$nintc;$i++) {
        for ($k=1;$k<=2;$k++) {
          for ($j=1;$j<=$nintc;$j++) {
            chomp ($_=<IC>);
            $icoord[$i][$j][$k]=$_;
          }
          chomp ($_=<IC>);
        }
      }
      close(IC);

      for ($i=1;$i<=$nintc;$i++) {
        print DIS3 ("$i   -$hdisp    doit\n");
        print DIS3 ("$i   $hdisp   doit\n");
        mkdir "DISPLACEMENT/CALC.c$i.d-$hdisp";
        mkdir "DISPLACEMENT/CALC.c$i.d$hdisp";
        cp ("$wdir/gradjob/*", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d-$hdisp/");
        cp ("$wdir/gradjob/*", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$hdisp/");
        cp ("$wdir/calcgrad/geom.start", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d-$hdisp/");
        cp ("$wdir/calcgrad/geom.start", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$hdisp/");
        cp ("$wdir/calcgrad/mocoef", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d-$hdisp/");
        cp ("$wdir/calcgrad/mocoef", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$hdisp/");

        #get coordinates for -hdisp
        open(ICC,">$wdir/temp/intgeomch") or die "Cannot write $wdir/temp/intgeomch";
        for ($j=1;$j<=$nintc;$j++) {
          print ICC ("$icoord[$i][$j][1]\n");
        }
        close (ICC);
        chdir "$wdir/temp";      
        coordsystem($coordprog,'i2c_hess','');
        chdir "$wdir/calcgrad";
        mv ("$wdir/calcgrad/geom.new", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d-$hdisp/geom");      

        #get coordinates for hdisp
        open(ICC,">$wdir/temp/intgeomch") or die "Cannot write $wdir/temp/intgeomch";
        for ($j=1;$j<=$nintc;$j++) {
          print ICC ("$icoord[$i][$j][2]\n");
        }
        close (ICC);
        chdir "$wdir/temp";      
        coordsystem($coordprog,'i2c_hess','');
        chdir "$wdir/calcgrad";
        mv ("$wdir/calcgrad/geom.new", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$hdisp/geom");      
        if (-e "$wdir/calcgrad/cart2intls"){
          mv ("$wdir/calcgrad/cart2intls", "$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$hdisp/");      
        }
      }
      close (DIS3);

      # get things for refpoint
      chdir "$wdir/calcgrad";
      mkdir "$wdir/calcgrad/DISPLACEMENT/REFPOINT";
      cp ("$wdir/gradjob/*", "$wdir/calcgrad/DISPLACEMENT/REFPOINT");
      cp ("$wdir/calcgrad/geom.start", "$wdir/calcgrad/DISPLACEMENT/REFPOINT/geom");
      cp ("$wdir/calcgrad/mocoef", "$wdir/calcgrad/DISPLACEMENT/REFPOINT/");


      print LOG ("\nPreparation successfully completed\n\n");
      # execute hessian calculation
      print LOG ("Executing COLUMBUS calculation:\n\n");
      system("nohup $cbus/calc.pl -m $mem >& runls");
      system("grep -i 'Error' runls | grep -v 'runc.error' >error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) { 
        print LOG ("\nError in nohup $cbus/calc.pl -m $mem >& runls\n\n");
        die "Error in hessian calculation\n\n"
      } else {
        print LOG ("nohup $cbus/calc.pl -m $mem >& runls: SUCCESS \n\n");
      }
    

      print LOG ("Preparing mass weighted gradients\n\n");
      # collect gradients
      open(GOD,">$wdir/calcgrad/grad_digs") or die "Cannot write grad_digs\n";
      for ($i=1;$i<=$nintc;$i++) {
        open(ICC,"<$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d-$hdisp/GRADIENTS/intgrd.sp") or die "Cannot read intgrd\n";
        for ($j=1;$j<=$nintc;$j++) {
          chomp ($_=<ICC>);
          print GOD ("$_\n");
        }
        close(ICC);
        print GOD ("   \n");
        open(ICC,"<$wdir/calcgrad/DISPLACEMENT/CALC.c$i.d$hdisp/GRADIENTS/intgrd.sp") or die "Cannot read intgrd\n";
        for ($j=1;$j<=$nintc;$j++) {
          chomp ($_=<ICC>);
          print GOD ("$_\n");
        }
        print GOD ("   \n");
        close(ICC);
      }
      close(GOD); 

      print LOG ("Constructing the mass weigthed hessian\n\n");
      cp ("$wdir/calcgrad/grad_digs", "$wdir/calcopt/");
      runprog('hessian');
      cp ("$wdir/calcgrad/mwhessian", "$wdir/calcopt/");
      system("rm -rf $wdir/calcgrad/*");

      print LOG ("Construction of mass weighted hessian: SUCCESS\n\n"); 
      print LOG ("Calculation of hessian: SUCCESS\n\n"); 
 
    } elsif ($type eq'ini') {

      print LOG ("Calculation of $type geometry\n");
      mkdir ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d0");
      cp ("$wdir/geom.start", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/geom");
      cp ("$wdir/mocoef.start", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/mocoef");

      chdir "calcgrad";
      cp ("$wdir/gradjob/*", "$wdir/calcgrad/");
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/geom", "$wdir/calcgrad/geom");
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/mocoef", "$wdir/calcgrad/mocoef");
      cp ("$wdir/geninp", "$wdir/calcgrad/");
      print LOG ("Starting calculation for $type geometry\n");
      system("$cbus/runc -m $mem >runls 2>>error.log");
      electronic('col','checkerr',$mem);

      cp ("./WORK/cartgrd", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/cartgrd");
      cp ("./WORK/intcfl", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/intcfl");
      cp ("./WORK/intgeom", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/intgeom");
      cp ("./WORK/intgrd", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/intgrd");
      cp ("./WORK/bmatrix", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/bmatrix");
      mv ("$wdir/calcgrad/LISTINGS", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/");
      system("rm -rf $wdir/calcgrad/*");
      chdir "$wdir";
      print LOG ("Gradient calculation: SUCCESS\n\n");

    } elsif ($type eq 'ref') {

      print LOG ("Calculation of $type geometry\n");
      chdir "calcgrad";
      cp ("$wdir/calcopt/ref_geom", "$wdir/calcgrad/geom");
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/mocoef", "$wdir/calcgrad/mocoef");
      cp ("$wdir/gradjob/*", "$wdir/calcgrad/");
      print LOG ("Starting calculation for $type geometry\n");
      system("$cbus/runc -m $mem >runls 2>>error.log");
      electronic('col','checkerr',$mem);

      cp ("$wdir/calcgrad/WORK/cartgrd", "$wdir/calcopt/ref_cartgrd");
      cp ("$wdir/calcgrad/WORK/intcfl", "$wdir/calcopt/ref_intcfl");
      cp ("$wdir/calcgrad/WORK/intgeom", "$wdir/calcopt/ref_intgeom");
      cp ("$wdir/calcgrad/WORK/bmatrix", "$wdir/calcopt/ref_bmatrix");
      system("rm -rf $wdir/calcgrad/*");
      chdir "$wdir";
      print LOG ("Gradient calculation: SUCCESS\n\n");


    } elsif (($type eq 'curr') or ($type eq 'new')) {
 
      print LOG ("Calculation of $type geometry\n");

      chdir "calcgrad";
      cp ("$wdir/calcopt/$type\_geom", "$wdir/calcgrad/geom");
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/mocoef", "$wdir/calcgrad/mocoef");
      cp ("$wdir/gradjob/*", "$wdir/calcgrad/");
      print LOG ("Starting calculation for $type geometry\n");
      system("$cbus/runc -m $mem >runls 2>>error.log");
      electronic('col','checkerr',$mem);

      cp ("$wdir/calcgrad/WORK/cartgrd", "$wdir/calcopt/$type\_cartgrd");
      cp ("$wdir/calcgrad/WORK/intcfl", "$wdir/calcopt/$type\_intcfl");
      cp ("$wdir/calcgrad/WORK/energy", "$wdir/calcopt/$type\_energy");
      cp ("$wdir/calcgrad/WORK/mocoef", "$wdir/calcopt/$type\_mocoef");
       cp ("$wdir/calcgrad/WORK/bmatrix", "$wdir/calcopt/$type\_bmatrix");
      if ($type eq 'curr') {
        cp ("$wdir/calcgrad/WORK/bmatrix", "$wdir/calcopt/bmatrix.all");
        cp ("$wdir/calcgrad/WORK/intgeom", "$wdir/calcopt/intgeom.all");
        cp ("$wdir/calcgrad/WORK/bmatrix", "$wdir/calcopt/$type\_bmatrix");
        cp ("$wdir/calcgrad/WORK/intgeom", "$wdir/calcopt/$type\_intgeom");
      }
      system("rm -rf $wdir/calcopt/LISTINGS");
      mv ("$wdir/calcgrad/LISTINGS", "$wdir/calcopt/");
      system("rm -rf $wdir/calcgrad/*");
      chdir "$wdir";
      print LOG ("Gradient calculation: SUCCESS\n\n");

    } elsif ($type eq 'copyresults') {

      print LOG ("Copying results ...");
      cp ("$wdir/calcopt/mw_gradE_q", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/mw_gradE_q");
      cp ("$wdir/calcopt/new_bmatrix", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/bmatrix");
      cp ("$wdir/calcopt/new_geom", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/geom");
      cp ("$wdir/calcopt/new_intgeom", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/intgeom");
      cp ("$wdir/calcopt/new_cartgrd", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/cartgrd");
      cp ("$wdir/calcopt/new_intcfl", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/intcfl");
      cp ("$wdir/calcopt/new_mocoef", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/mocoef");
      cp ("$wdir/calcopt/mwhessian", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/mwhessian");
      mv ("$wdir/calcopt/LISTINGS", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s");
      cp ("$wdir/calcopt/dyn.$s.mld", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d$s/dyn.mld");
      print LOG ("... finished\n");

    } elsif ($type eq 'copydebug') {

      print LOG ("Copying debug information ...");
      cp ("$wdir/calcopt/new_geom", "$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter/geom");
      cp ("$wdir/calcopt/new_cartgrd", "$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter/cartgrd");
      cp ("$wdir/calcopt/new_intgeom", "$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter/intgeom");
      cp ("$wdir/calcopt/new_intcfl", "$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter/intcfl");
      cp ("$wdir/calcopt/new_mocoef", "$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter/mocoef");
      cp ("$wdir/calcopt/mwhessian", "$wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter/mwhessian");
      cp ("$wdir/calcopt/dyn.$s.mld", "$wdir/DEBUG/$s/DISPLACEMENT/");
      # needs no removal of files in case of new job
      system("cp -r $wdir/calcopt/LISTINGS $wdir/DEBUG/$s/DISPLACEMENT/CALC.c$nmode.d$iter/");
      print LOG ("... finished\n\n");

    } elsif ($type eq 'newtocurr') {
      chdir "$wdir/calcopt";

      open(ADQ,">>$wdir/calcopt/delta_q.all") or die "Cannot append to delta_q.all";
      open(NDQ,"<$wdir/calcopt/delta_q") or die "Cannot read from delta_q";
      while($_=<NDQ>) {
        chomp($_);
        print ADQ ("$_\n");
      }
      print ADQ ("\n");
      close(NDQ);
      close(ADQ);

      open(ADX,">>$wdir/calcopt/delta_x.all") or die "Cannot append to delta_x.all";
      open(NDX,"<$wdir/calcopt/delta_x") or die "Cannot read from delta_x";
      while($_=<NDX>) {
        chomp($_);
        print ADX ("$_\n");
      }
      print ADX ("\n");
      close(NDX);
      close(ADX);

      open(ADY,">>$wdir/calcopt/delta_y.all") or die "Cannot append to delta_y.all";
      open(NDY,"<$wdir/calcopt/delta_y") or die "Cannot read from delta_y";
      while($_=<NDY>) {
        chomp($_);
        print ADY ("$_\n");
      }
      print ADY ("\n");
      close(NDY);
      close(ADY);

      open(AGE,">>$wdir/calcopt/gradE_q.all") or die "Cannot append to gradE_q.all";
      open(NGE,"<$wdir/calcopt/new_gradE_q") or die "Cannot read from new_gradE_q";
      while($_=<NGE>) {
        chomp($_);
        print AGE ("$_\n");
      }
      print AGE ("\n");
      close(NGE);
      close(AGE);

      open(ACA,">>$wdir/calcopt/C_angs.all") or die "Cannot append to C_angs.all";
      open(NCA,"<$wdir/calcopt/curr_C_angs") or die "Cannot read from curr_C_angs";
      while($_=<NCA>) {
        chomp($_);
        print ACA ("$_\n");
      }
      print ACA ("\n");
      close(NCA);
      close(ACA);

      open(AGC,">>$wdir/calcopt/gradC_q.all") or die "Cannot append to gradC_q.all";
      open(NGC,"<$wdir/calcopt/curr_gradC_q") or die "Cannot read from curr_gradC_q";
      while($_=<NGC>) {
        chomp($_);
        print AGC ("$_\n");
      }
      print AGC ("\n");
      close(NGC);
      close(AGC);

      open(ABM,">>$wdir/calcopt/bmatrix.all") or die "Cannot append to bmatrix.all";
      open(NBM,"<$wdir/calcopt/new_bmatrix") or die "Cannot read from new_bmatrix";
      while($_=<NBM>) {
        chomp($_);
        print ABM ("$_\n");
      }
      print ABM ("\n");
      close(NBM);
      close(ABM);

      open(AIG,">>$wdir/calcopt/intgeom.all") or die "Cannot append to intgeom.all";
      open(NIG,"<$wdir/calcopt/new_intgeom") or die "Cannot read from new_intgeom";
      while($_=<NIG>) {
        chomp($_);
        print AIG ("$_\n");
      }
      print AIG ("\n");
      close(NIG);
      close(AIG);

      open(ATM,">>$wdir/calcopt/tmti.all") or die "Cannot append to tmti.all";
      open(CTM,"<$wdir/calcopt/curr_tmti") or die "Cannot read from curr_tmti";
      while($_=<CTM>) {
        chomp($_);
        print ATM ("$_\n");
      }
      print ATM ("\n");
      close(CTM);
      close(ATM);

      open(ATC,">>$wdir/calcopt/tcb.all") or die "Cannot append to tcb.all";
      open(CTC,"<$wdir/calcopt/curr_tcb") or die "Cannot read from curr_tcb";
      while($_=<CTC>) {
        chomp($_);
        print ATC ("$_\n");
      }
      print ATC ("\n");
      close(CTC);
      close(ATC);


      mv ("curr_bmatrix", "prev_bmatrix");
      mv ("new_bmatrix", "curr_bmatrix");
      mv ("curr_C_angs", "prev_C_angs");
      mv ("new_C_angs", "curr_C_angs");
      mv ("new_cartgrd", "curr_cartgrd");
      mv ("new_energy", "curr_energy");
      mv ("new_geom", "curr_geom");
      mv ("curr_gradC_q", "prev_gradC_q");
      mv ("new_gradC_q", "curr_gradC_q");
      mv ("curr_gradE_q", "prev_gradE_q");
      mv ("new_gradE_q", "curr_gradE_q");
      mv ("new_intcfl", "curr_intcfl");
      mv ("curr_intgeom", "prev_intgeom");
      mv ("new_intgeom", "curr_intgeom");
      chdir "$wdir";
    } elsif ($type eq 'cleanup') {

      system ("rm -f $wdir/gradjob/geom* $wdir/gradjob/mocoef*");

    }

  } #if prog

}





###################################################################
# Interface to coordinate system transformtation programs
###################################################################

sub runpulay{
  system("$cbus/cart2int.x 2>> error.log");
  open(BU,"<bummer") or die "Cannot open bummer";
  chomp ($_=<BU>);
  close(BU);
  if (!($_ eq ' normally terminated')){
    print LOG ("\nError: Problem with $cbus/cart2int.x\n");
    die "Problem with $cbus/cart2int.x";
  } elsif ($printl >= 2) {
    print LOG ("\nFinished $cbus/cart2int.x: SUCCESS\n\n");
  }
}

sub molden{
  my ($kind)=@_;
  my ($x,$y,$z,$trash1,$trash2,$trash3,$trash4,$j);

  if ($kind eq 'ini'){
    chdir("$wdir/temp");
    cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/geom", "$wdir/temp/new_geom");
    open(NGO,"<$wdir/temp/new_geom") or die "Cannot open $wdir/temp/new_geom\n";
    open(DSM,">>$wdir/temp/dyn.$s.mld") or die "Cannot open $wdir/temp/dyn.$s.mld\n";
    print DSM (" $nat\n\n");
    for ($i=1;$i<=$nat;$i++){
      $_=<NGO>;
      chomp($_);
      ($trash4,$trash1,$trash2,$x,$y,$z,$trash3)=split(/\s+/,$_);
      $x=$x*0.529177249;
      $y=$y*0.529177249;
      $z=$z*0.529177249;
      printf DSM ("%3s %11.8f %11.8f %11.8f \n",$trash1,$x,$y,$z);
    }
    close(NGO);
    close(DSM);
    cp ("$wdir/temp/dyn.$s.mld", "$wdir/results/DISPLACEMENT/CALC.c$nmode.d0/dyn.mld");
    chdir("$wdir");
  
  } elsif ($kind eq 'next'){
    chdir("$wdir/temp");
    if (-e "$wdir/calcopt/dyn.$s.mld") {
      cp ("$wdir/calcopt/dyn.$s.mld", "$wdir/temp");
    }
    cp ("$wdir/calcopt/new_geom", "$wdir/temp");
    open(NGO,"<$wdir/temp/new_geom") or die "Cannot open $wdir/temp/new_geom\n";
    open(DSM,">>$wdir/temp/dyn.$s.mld") or die "Cannot open $wdir/temp/dyn.$s.mld\n";
    print DSM (" $nat\n\n");
    for ($i=1;$i<=$nat;$i++){
      $_=<NGO>;
      chomp($_);
      ($trash4,$trash1,$trash2,$x,$y,$z,$trash3)=split(/\s+/,$_);
      $x=$x*0.529177249;
      $y=$y*0.529177249;
      $z=$z*0.529177249;
      printf DSM ("%3s %11.8f %11.8f %11.8f \n",$trash1,$x,$y,$z);
    }
    close(NGO);
    close(DSM);
    cp ("$wdir/temp/dyn.$s.mld", "$wdir/calcopt/dyn.$s.mld");
    chdir("$wdir");

  } elsif ($kind eq 'collectmold') {
    chdir("$wdir/temp");
    for ($i=0;$i<$s;$i++) {
      $j=$nat+2;
      system ("tail -n $j $wdir/results/DISPLACEMENT/CALC.c$nmode.d$i/dyn.mld >>$wdir/results/dyn_irc.mld");
    }
    chdir("$wdir");
  }

}

sub coordsystem{
  my ($prog,$kind,$type)=@_;

  if ($prog eq 'zmat') {

    if ($kind eq 'i2c_hess') {
      # convert new_intgeom to cartesian using curr_ data
      print LOG ("convert new_intgeom to cartesian with zmat i2c_hess\n");
      chdir "temp";
      mv ("$wdir/temp/intgeomch", "$wdir/temp/intgeom");
      cp ("$wdir/calcopt/geninp", "$wdir/temp/");
      cp ("$wdir/calcopt/curr_intcfl", "$wdir/temp/intcfl");
      cp ("$wdir/calcopt/ref_geom", "$wdir/temp/geom");

      # create inputfile for int -> cart
      open(C2I,">zmatin") or die "Cannot write to zmatin ";
        print C2I " &zmatin\n";
        print C2I " geom_i2c     = 1\n";
        print C2I " bmat     = 1\n";
        print C2I " &end \n";
      close(C2I);

      # transform int ->cart
      runprog('zmat');

      cp ("$wdir/temp/zmat_geom", "$wdir/calcgrad/geom.new");
      system("rm -f $wdir/temp/*");
      chdir "$wdir";




    } elsif ($kind eq 'i2c') {
      print LOG ("Convert new intgeom to cartesian with zmat\n");
      chdir "temp";
      cp ("$wdir/calcopt/geninp", "$wdir/temp/");
      cp ("$wdir/calcopt/new_intgeom", "$wdir/temp/intgeom");
      cp ("$wdir/calcopt/ref_intcfl", "$wdir/temp/intcfl");
      cp ("$wdir/calcopt/ref_geom", "$wdir/temp/geom");

      # create inputfile for int -> cart
      open(C2I,">zmatin") or die "Cannot write to zmatin ";
        print C2I " &zmatin\n";
        print C2I " geom_i2c     = 1\n";
        print C2I " bmat     = 1\n";
        print C2I " &end \n";
      close(C2I);

      # transform int ->cart
      runprog('zmat');

      cp ("$wdir/temp/zmat_geom", "$wdir/calcopt/new_geom");
      system("rm -f $wdir/temp/*");
      chdir "$wdir"; 



    } elsif ($kind eq 'grad2int') {
      print LOG ("Convert new cartesian gradient to internals with zmat\n");

      # convert gradients to internal
      chdir "temp";
      cp ("$wdir/calcopt/$type\_geom", "$wdir/temp/geom");
      cp ("$wdir/calcopt/$type\_cartgrd", "$wdir/temp/cartgrd");
      cp ("$wdir/calcopt/$type\_intcfl", "$wdir/temp/intcfl");
      cp ("$wdir/geninp", "$wdir/temp/");

      open(C2I,">zmatin") or die "Cannot write to zmatin ";
        print C2I " &zmatin\n";
        print C2I " geom_c2i  = 1\n";
        print C2I " grd_c2i  = 1\n";
        print C2I " bmat     = 1\n";
        print C2I " &end \n";
      close(C2I);

      # transform int ->cart
      runprog('zmat');

      cp ("$wdir/temp/gradE_q", "$wdir/calcopt/$type\_gradE_q");
      system("rm -f $wdir/temp/*");
      chdir "$wdir";




    }


  } # if prog



  if ($prog eq 'pulay') {

    if ($kind eq 'i2c_hess') {
      # convert new_intgeom to cartesian using curr_ data
      chdir "temp";
      cp ("$wdir/calcopt/curr_intcfl", "$wdir/temp/intcfl");
      cp ("$wdir/calcopt/curr_cartgrd", "$wdir/temp/cartgrd");
      # it turns out that curr failed in it1 on 2nd hypsh in C2H5F
      cp ("$wdir/calcopt/ref_geom", "$wdir/temp/geom");

      # create inputfile for int -> cart
      open(C2I,">cart2intin") or die "Cannot write to car2intin ";
        print C2I (" &input\n");
        print C2I (" calctype='int2cart',\n");
        print C2I (" linallow=$linconv,\n");
        print C2I (" maxiter=200,\n");
        print C2I (" geomch=0,\n");
        print C2I (" maxstep=0.1,\n");
        print C2I (" /&end \n");
      close(C2I);

      # transform int ->cart
      runpulay();

      $see=(!(-e "geom.new"));

     -e "geom.new" or die "New geometry cannot be transformed";

      cp ("$wdir/temp/geom.new", "$wdir/calcgrad/");
      cp ("$wdir/temp/cart2intls", "$wdir/calcgrad/");
      system("rm -f $wdir/temp/*");
      chdir "$wdir";




    } elsif ($kind eq 'i2c') {
      print LOG ("Convert new intgeom to cartesian\n");
      # convert new_intgeom to cartesian using curr_ data
      chdir "temp";
      cp ("$wdir/calcopt/new_intgeom", "$wdir/temp/intgeomch");
      cp ("$wdir/calcopt/ref_intcfl", "$wdir/temp/intcfl");
      cp ("$wdir/calcopt/ref_cartgrd", "$wdir/temp/cartgrd");
      cp ("$wdir/calcopt/ref_geom", "$wdir/temp/geom");

      # create inputfile for int -> cart
      open(C2I,">cart2intin") or die "Cannot write to car2intin ";
        print C2I (" &input\n");
        print C2I (" calctype='int2cart',\n");
        print C2I (" linallow=$linconv,\n");
        print C2I (" maxiter=200,\n");
        print C2I (" geomch=0,\n");
        print C2I (" maxstep=0.1,\n");
        print C2I (" /&end \n");
      close(C2I);

      # transform int ->cart
      system("$cbus/cart2int.x 2>> error.log");
      open(BU,"<bummer") or die "Cannot open bummer";
      chomp ($_=<BU>);
      close(BU);
      if (!($_ eq ' normally terminated')){
        print LOG ("\nError: Problem with $cbus/cart2int.x\n");
      }
      if ($printl >= 2) {
        print LOG ("\nFinished $cbus/cart2int.x: SUCCESS\n\n");
      }

      $see=(!(-e "geom.new"));
      if ($see eq "1"){
        print ("Transforming geometry with current data\n");
        # convert new_intgeom to cartesian  using ref_ data
        chdir "temp";
        cp ("$wdir/calcopt/new_intgeom", "$wdir/temp/intgeomch");
        cp ("$wdir/calcopt/curr_intcfl", "$wdir/temp/intcfl");
        cp ("$wdir/calcopt/curr_cartgrd", "$wdir/temp/cartgrd");
        cp ("$wdir/calcopt/curr_geom", "$wdir/temp/geom");

        # create inputfile for int -> cart
        open(C2I,">cart2intin") or die "Cannot write to car2intin ";
          print C2I (" &input\n");
          print C2I (" calctype='int2cart',\n");
          print C2I (" linallow=$linconv,\n");
          print C2I (" maxiter=200,\n");
          print C2I (" geomch=0,\n");
          print C2I (" maxstep=0.1,\n");
          print C2I (" /&end \n");
        close(C2I);

        # transform int ->cart
        runpulay();
     }


     -e "geom.new" or die "New geometry cannot be transformed";


      cp ("$wdir/temp/geom.new", "$wdir/calcopt/new_geom");
      system("rm -f $wdir/temp/*");
      chdir "$wdir"; 

    } elsif ($kind eq 'getintref') {

      # get internal referencgeom
      chdir "temp";

      # copy needed files
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/ref_geom", "$wdir/temp/geom");
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/ref_cartgrd", "$wdir/temp/cartgrd");
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/ref_intcfl", "$wdir/temp/intcfl");

      # create inputfile for cart -> int
      open(C2I,">cart2intin") or die "Cannot write to car2intin ";
        print C2I " &input\n";
        print C2I " calctype='cart2int',\n";
        print C2I " /&end \n";
      close(C2I);

      # transform cart ->int
      runpulay();
      system('rm -f cart2int* bummer bmatrix geom');
      cp ("$wdir/temp/intgeom", "$wdir/calcopt/ref_intgeom");



    } elsif ($kind eq 'grad2int') {

      # convert gradients to internal
      chdir "temp";
      cp ("$wdir/calcopt/$type\_geom", "$wdir/temp/geom");
      cp ("$wdir/calcopt/$type\_cartgrd", "$wdir/temp/cartgrd");
      cp ("$wdir/calcopt/$type\_intcfl", "$wdir/temp/intcfl");
      cp ("$wdir/geninp", "$wdir/temp");
      open (NIN,">cart2intin") or die "Cannot open file: cart2intin\n";
      print NIN " &input\n calctype='cart2int',\n intgradout=1,\n";
      print NIN " intgradfl='gradE_q',\n gradfl='cartgrd',\n";
      print NIN " \/end\n";
      close NIN;
      runpulay();     

      cp ("$wdir/temp/gradE_q", "$wdir/calcopt/$type\_gradE_q");
      system("rm -f $wdir/temp/*");
      chdir "$wdir";







    } elsif ($kind eq 'getrefgeom') {
 
      # get good referencgeom: rgeom
      chdir "temp";

      # copy needed files
      cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/ref_intgeom", "$wdir/temp/intgeomch");
      if ($type eq 'ref') {
        cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/ref_cartgrd", "$wdir/temp/cartgrd");
        cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$ref/ref_intcfl", "$wdir/temp/intcfl");
      } elsif ($type eq 'curr'){
        cp ("$wdir/calcopt/curr_cartgrd", "$wdir/temp/cartgrd");
        cp ("$wdir/calcopt/curr_intcfl", "$wdir/temp/intcfl");
      }

      # get the current geometry to use as reference
      cp ("$wdir/calcopt/curr_geom", "$wdir/temp/geom");

      # create inputfile for int -> cart
      open(C2I,">cart2intin") or die "Cannot write to car2intin ";
        print C2I (" &input\n");
        print C2I (" calctype='int2cart',\n");
        print C2I (" linallow=$linconv,\n");
        print C2I (" maxiter=200,\n");
        print C2I (" geomch=0,\n");
        print C2I (" maxstep=0.1,\n");
        print C2I (" /&end \n");
      close(C2I);

      # transform int ->cart
      runpulay();      

      # copy finally the referencegeom
      cp ("$wdir/temp/geom.new", "$wdir/calcopt/curr_rgeom");
      -e "geom.new" or die "Cannot transform reference geometry";
      system("rm -f $wdir/temp/*");
      chdir "$wdir";





    } elsif ($kind eq 'aligngeom') {


      chdir "temp";
      # copy needed files
      if ($type eq 'ref') {
        cp ("$wdir/calcopt/ref_geom", "$wdir/temp/geom");
        cp ("$wdir/calcopt/ref_cartgrd", "$wdir/temp/cartgrd");
        cp ("$wdir/calcopt/ref_intcfl", "$wdir/temp/intcfl");
      } elsif ($type eq 'curr') {
        cp ("$wdir/calcopt/$type\_geom", "$wdir/temp/geom");
        cp ("$wdir/calcopt/$type\_cartgrd", "$wdir/temp/cartgrd");
        cp ("$wdir/calcopt/$type\_intcfl", "$wdir/temp/intcfl");
      } 
      # get the current internal geometry 
      cp ("$wdir/calcopt/curr_intgeom", "$wdir/temp/intgeomch");
      # create inputfile for int -> cart
      open(C2I,">cart2intin") or die "Cannot write to car2intin ";
        print C2I (" &input\n");
        print C2I (" calctype='int2cart',\n");
        print C2I (" linallow=$linconv,\n");
        print C2I (" maxiter=200,\n");
        print C2I (" geomch=0,\n");
        print C2I (" maxstep=0.1,\n");
        print C2I (" /&end \n");
      close(C2I);
      # transform int ->cart
      runpulay();
      -e "geom.new" or die "Cannot transform reference geometry";
      cp ("$wdir/temp/geom.new", "$wdir/calcopt/curr_geom");
      system("rm -f $wdir/temp/*");
      chdir "$wdir"; 
    
    } # if kind

  } # if prog

}


