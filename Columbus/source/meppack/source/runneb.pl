#! /usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use strict;
use warnings;
use Shell qw(pwd cd cp mkdir date mv cat);
use lib $ENV{"MEP"};
use defaults qw(test writerestart getkeyw definit prepdef);

# (c) Bernhard Sellner 2011
# part of MEPPACK
my ($wdir,$step,$prev,$iter,$see,$s,$ref,$inidirect,$elprog);
my ($coordprog,$printl,$nintc,$nat,$inihess,$hessmod,$trash);
my ($nmode,$rpsteps,$maxhypstep,$datetime,$consttyp,$version);
my ($mem,$cbus,@geni,$i,$inihmod,$mixmod,$hessstep,$chess);
my ($nproc,$nprocneb,@images,$maxit,$inistep,$lines,$fst,$lst,$it);
my ($j,$optdim,@crd,$trash1,$trash2,$var,$rem,$prtstep);
my ($restart,$ii,$linconv,$initneb,$licneb,$doneb,$adjneb);
my ($nim,@genp,@nenp,$meppath,$mld,$fixed_seed,$iseed,$hdisp);
my ($matfact,$tangtype,$nebtype,$startci,$startdiis,$maxgdiis);
my ($maxddiis,$bmupd,$stepres,$scmin,$scmax,$conv_grad,$conv_disp);
my ($nofidiis,$cipoints);


$meppath=$ENV{"MEP"};
$cbus=$ENV{"COLUMBUS"};

$wdir=pwd;
chomp($wdir);

###################################################################
#
# Start of program
#
###################################################################

$version='2.3';



open(LOG,">>$wdir/neb.log") or die "Cannot write logfile";
print LOG ("********************************************************\n");
print LOG ("***                                                  ***\n");
print LOG ("***     MINIMUM ENERGY PATH PROGRAM PACKAGE          ***\n");
print LOG ("***                  MEPPACK                         ***\n");
print LOG ("***             NUDGED ELASTIC BAND                  ***\n");
print LOG ("***                                                  ***\n");
print LOG ("***        INTRINSIC REACTION COORDINATE             ***\n");
print LOG ("***                                                  ***\n");
print LOG ("***               version $version                        ***\n");
print LOG ("***                                                  ***\n");
print LOG ("***          University of Vienna                    ***\n");
print LOG ("***          Institute for Theoretical Chemistry     ***\n");
print LOG ("***          Bernhard Sellner, Hans Lischka          ***\n");
print LOG ("***                                                  ***\n");
print LOG ("***       bernhard.sellner\@univie.ac.at              ***\n");
print LOG ("***       hans.lischka\@univie.ac.at                  ***\n");
print LOG ("***                                                  ***\n");
print LOG ("********************************************************\n\n\n");

$datetime=date;

print ("\n\nNEB calculation started on $datetime\n\n");

print LOG ("\n\nNEB calculation started on $datetime\n\n");

print LOG ("Reading input\n\n");

# Initialize variables
$genp[0][0]=("mem");
$genp[1][0]=("nproc");
$genp[2][0]=("elprog");
$genp[3][0]=("coordprog");
$genp[4][0]=("printl");
$genp[5][0]=("prtstep");
$genp[6][0]=("mld");
$genp[7][0]=("consttyp");
$genp[8][0]=("fixed_seed");
$genp[9][0]=("iseed");
$genp[10][0]=("restart");
$genp[11][0]=("linconv");
$genp[12][0]=("hdisp");
$genp[13][0]=("inihess");
$genp[14][0]=("inihmod");
$genp[15][0]=("hessmod");
$genp[16][0]=("mixmod");
$genp[17][0]=("hessstep");
$genp[18][0]=("chess");
$genp[19][0]=("nintc");
$genp[20][0]=("nat");

$nenp[0][0]=("licneb");
$nenp[1][0]=("adjneb");
$nenp[2][0]=("initneb");
$nenp[3][0]=("doneb");
$nenp[4][0]=("maxit");
$nenp[5][0]=("nprocneb");
$nenp[6][0]=("matfact");
$nenp[7][0]=("tangtype");
$nenp[8][0]=("nebtype");
$nenp[9][0]=("startci");
$nenp[10][0]=("startdiis");
$nenp[11][0]=("maxgdiis");
$nenp[12][0]=("maxddiis");
$nenp[13][0]=("bmupd");
$nenp[14][0]=("stepres");
$nenp[15][0]=("scmin");
$nenp[16][0]=("scmax");
$nenp[17][0]=("conv_grad");
$nenp[18][0]=("conv_disp");  
$nenp[19][0]=("nim");
$nenp[20][0]=("nofidiis");
$nenp[21][0]=("cipoints");

&defaults::prepdef($meppath,$wdir,'neb');

&defaults::definit($wdir,@genp);
&defaults::definit($wdir,@nenp);

system("rm -f $wdir/def");

# read the input files
for($i=0;$i<@genp;$i++){
  $genp[$i][1]=&defaults::getkeyw('geninp',$genp[$i][0],$genp[$i][1],$wdir);
}
for($i=0;$i<@nenp;$i++){
  $nenp[$i][1]=&defaults::getkeyw('nebpar',$nenp[$i][0],$nenp[$i][1],$wdir);
}

# copy final variables
$licneb=$nenp[0][1];
$adjneb=$nenp[1][1];
$initneb=$nenp[2][1];
$doneb=$nenp[3][1];
$maxit=$nenp[4][1];
$nprocneb=$nenp[5][1];
$matfact=$nenp[6][1];
$tangtype=$nenp[7][1];
$nebtype=$nenp[8][1];
$startci=$nenp[9][1];
$startdiis=$nenp[10][1];
$maxgdiis=$nenp[11][1];
$maxddiis=$nenp[12][1];
$bmupd=$nenp[13][1];
$stepres=$nenp[14][1];
$scmin=$nenp[15][1];
$scmax=$nenp[16][1];
$conv_grad=$nenp[17][1];
$conv_disp=$nenp[18][1];
$nim=$nenp[19][1];
$nofidiis=$nenp[20][1];
$cipoints=$nenp[21][1];

$mem=$genp[0][1];
$nproc=$genp[1][1];
$elprog=$genp[2][1];
$coordprog=$genp[3][1];
$printl=$genp[4][1];
$prtstep=$genp[5][1];
$mld=$genp[6][1];
$consttyp=$genp[7][1];
$fixed_seed=$genp[8][1];
$iseed=$genp[9][1];
$restart=$genp[10][1];
$linconv=$genp[11][1];
$hdisp=$genp[12][1];
$inihess=$genp[13][1];
$inihmod=$genp[14][1];
$hessmod=$genp[15][1];
$mixmod=$genp[16][1];
$hessstep=$genp[17][1];
$chess=$genp[18][1];
$nintc=$genp[19][1];
$nat=$genp[20][1];

print LOG (" Input setting for geninp:\n\n");
for ($i=0;$i<@genp;$i++){
  print LOG (" $genp[$i][0] = $genp[$i][1]\n");
}
print LOG ("\n Input setting for nebpar:\n\n");
for ($i=0;$i<@nenp;$i++){
  print LOG (" $nenp[$i][0] = $nenp[$i][1]\n");
}

print LOG ("\nRelaxing nugged elastic band\n");

if ($consttyp == 0) { 
  print LOG ("using cartesian coordiantes\n");

} elsif ($consttyp == 1) {

  print LOG ("using mass weighted cartesian coordiantes\n"); 

}


if ($licneb ==1) {
#make linear interpolation with mass weighted internal coordinates

  if (-e "$wdir/DISPLACEMENT/displfl") {
    die "$wdir/DISPLACEMENT/displfl already exists\n\n";
  }

  mkdir ("$wdir/temp");
  mkdir ("$wdir/DISPLACEMENT");
  open(DIS,">$wdir/DISPLACEMENT/displfl") or die "Cannot write dispfl";
  print DIS ("$nintc   /number of internal coordinates\n");
  print DIS ("no  /calculate dip.mom.derivatives\n");
  print DIS ("no  /calculate reference point\n");
  for ($i=0;$i<=$nim-1;$i++) {
    print DIS ("1   $i\n");
  }
  close (DIS);
  cp ("$wdir/DISPLACEMENT/displfl", "$wdir/temp");
  coordsystem($coordprog,'lic','',$nim);
  for ($i=0;$i<=$nim-1;$i++) {
    mkdir ("$wdir/DISPLACEMENT/CALC.c1.d$i");
    mv ("$wdir/temp/geom.$i", "$wdir/DISPLACEMENT/CALC.c1.d$i/geom");
  }

  print LOG ("\n\nLinear interpolation of geometries finished\n");
  print LOG ("Please check the geometries\n\n");
  system ("rm -rf temp");

}


# read first and last point from dispfl
$lines=0;
open (DFL,"$wdir/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
while (<DFL>) {
  $lines++;
}
close (DFL);
open (DFL,"$wdir/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
chomp($_=<DFL>);
chomp($_=<DFL>);
chomp($_=<DFL>);
for ($i=0;$i<=$lines-4;$i++) {
  chomp($_=<DFL>);
  ($crd[$i],$images[$i])=split(/\s+/,$_);
}
$optdim=$lines-5;
$fst=$images[0];
$lst=$images[$optdim+1];

close (DFL);


if ($adjneb == 1) {
#only initialize the geometries

  mkdir ("$wdir/temp");
  if (($coordprog eq 'zmat')or($coordprog eq 'cart')) {
    coordsystem($coordprog,'initialize','',$linconv);
  } elsif ($coordprog eq 'pulay') {
    print LOG ("Nothing done for coordprog pulay\n");
  }
  print LOG ("\n\nInitialization of geometries finished\n");
  print LOG ("Please check the geometries\n\n");
  system ("rm -rf temp");

}

if ($initneb == 1) {
#only initialize the neb calculation

  electronic('col','initialize',$mem);

  print LOG ("\n\nInitialization run finished\n");
  print LOG ("Please check the mocoefs and submit with initneb=0\n");
  print LOG ("for running the neb calculation\n\n");
 

}

if ($doneb ==1) {
#run standard neb calculation
check();

if (!($restart == 1)) {


if ($printl >= 1) {
  # write input for program to log file
  print LOG ("\nContent of input files:\n");

  print LOG ("\nContent of geninp:\n\n");
  open(GEN,"$wdir/geninp") or die "Cannot read geninp\n";
  while($_=<GEN>) {
    print LOG ("$_");
  }
  close(GEN);

  print LOG ("\nContent of nebpar:\n\n");
  open(GEN,"$wdir/nebpar") or die "Cannot read nebpar\n";
  while($_=<GEN>) {
    print LOG ("$_");
  }


}
print LOG ("\n\n Start calculating NEB \n\n");


$it=0;


#initialize things in case of first step 
-e "$wdir/calcopt/curr_iter" or $it=0 ;
if ($it == 0){
  mkdir ("$wdir/temp");
  mkdir ("$wdir/results");
  mkdir ("$wdir/calcgrad");
  mkdir ("$wdir/calchess");
  mkdir ("$wdir/calcopt");
  mkdir ("$wdir/restart");
  mkdir ("$wdir/results/DISPLACEMENT");
  mkdir ("$wdir/calchess/DISPLACEMENT");
  if ($hessmod eq "calc") { 
    system ("cp -r $wdir/hessjob $wdir/restart");
  }
  for ($i=0;$i<=$optdim+1;$i++) {
    mkdir ("$wdir/results/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]");
    mkdir ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]");
  }
  open(ITE,">$wdir/calcopt/curr_iter") or die "Cannot write curr_iter";
  print ITE "$it\n";
  close(ITE);
  if ($printl >= 1) {
    system("mkdir $wdir/DEBUG");
  }
  system("cp -r $wdir/DISPLACEMENT $wdir/calcgrad");
} else {
  open(ITE,"<$wdir/calcopt/curr_iter") or die "Cannot read from curr_step";
  chop($_=<ITE>);
  $it=$_;
  close(ITE);
}

&defaults::writerestart("1",$wdir);

  system ("cp -r $wdir/DISPLACEMENT $wdir/restart/");
  system ("cp -r $wdir/calcopt $wdir/restart/");


cp ("$wdir/geninp", "$wdir/calcopt/");
cp ("$wdir/nebpar", "$wdir/calcopt/");
cp ("$wdir/DISPLACEMENT/displfl", "$wdir/calcopt/");
  
# copy files
for ($i=0;$i<=$optdim+1;$i++) {
  cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom", "$wdir/calcopt/curr_geom.c$crd[$i].d$images[$i]");
  cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/energy", "$wdir/calcopt/curr_energy.c$crd[$i].d$images[$i]");
  cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GRADIENTS/intgrd.sp", "$wdir/calcopt/curr_intgrd.c$crd[$i].d$images[$i]");
  cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GEOMS/intgeom.sp", "$wdir/calcopt/curr_intgeom.c$crd[$i].d$images[$i]");
  cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/intcfl", "$wdir/calcopt/intcfl.c$crd[$i].d$images[$i]");
  open(CG,"<$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GRADIENTS/cartgrd.all") or die "Cannot read cartgrd.all !\n";
  for ($j=1;$j<=3;$j++) {
    chomp($_=<CG>);
  }
  open(CCG,">$wdir/calcopt/curr_cartgrd.c$crd[$i].d$images[$i]") or die "Cannot write curr_cartgrd.c$crd[$i].d$images[$i] ! \n";
    $_=<CG>;
    $_=substr($_,1);
    print CCG ("$_");

  for ($j=2;$j<=$nat;$j++) {
    $_=<CG>;
    print CCG ("$_");
  }
  close(CG);
  close(CCG);
  if (($i==0) or ($i==$optdim+1)) {
    cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom", "$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]");
    cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/energy", "$wdir/calcopt/new_energy.c$crd[$i].d$images[$i]");
    cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GRADIENTS/intgrd.sp", "$wdir/calcopt/new_intgrd.c$crd[$i].d$images[$i]");
    cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GEOMS/intgeom.sp", "$wdir/calcopt/new_intgeom.c$crd[$i].d$images[$i]");
    cp ("$wdir/calcopt/curr_cartgrd.c$crd[$i].d$images[$i]", "$wdir/calcopt/new_cartgrd.c$crd[$i].d$images[$i]");
  } 
}

# get internal geometries and bmatrices

coordsystem($coordprog,'nebini','',$linconv);


# get first bmatrix in a separate file

electronic('col','first','');


# make initial hessian
if ($inihmod eq "newh") {
  if ($inihess eq "calc")  {
  electronic('col','nebcolhess',$mem);

  } elsif ($inihess eq "calcmw")  {
  } elsif ($inihess eq "intcfl")   {
    chdir "$wdir/calcopt";
    runprog("nebhess");
    chdir "$wdir";
  } elsif ($inihess eq "unitma") {
    chdir "$wdir/calcopt";
    runprog("nebhess");
    chdir "$wdir";
  }
} 
  

if ($it ==0) {
  $it=1;
}

} else {   # restart

  -e "$wdir/restart/calcopt/curr_iter" or die "\n\n Restarting failed \n\n";

  print LOG ("\n Restarting NEB job \n");

  open(ITE,"<$wdir/restart/calcopt/curr_iter") or die "Cannot read from curr_iter";
  chop($_=<ITE>);
  $it=$_;
  close(ITE);
  $it++;

  system("rm -rf $wdir/calcgrad/*");
  system("cp -r $wdir/restart/DISPLACEMENT $wdir/calcgrad/");

  if ($hessmod eq "calc") {
    for ($ii=1;$ii<=$optdim;$ii++) {
      system("rm -rf $wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/*");
    }
  }

  system ("rm -rf $wdir/calcopt");
  system ("rm -rf $wdir/calchess/*");
  system ("rm -rf $wdir/temp/*");
  system ("cp -r $wdir/restart/calcopt $wdir/"); 
}

##################################################################
#
#  main loop
#
##################################################################

for($iter=$it;$iter<=$maxit;$iter++){

  print LOG ("\n§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§\n");
  print LOG ("                                 Iteration $iter                                                   \n");
  print LOG ("§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§\n\n");

  if ($iter >=2) {
    system ("mv $wdir/restart/DISPLACEMENT $wdir/restart/bk_DISPLACEMENT");
    system ("mv $wdir/restart/calcopt $wdir/restart/bk_calcopt");
#  }
  $ii=$iter-1;
  system ("cp -r $wdir/DEBUG/$ii/DISPLACEMENT $wdir/restart/");
  system ("cp -r $wdir/calcopt $wdir/restart/");
#  if ($iter >=2) {
    system ("rm -rf $wdir/restart/bk_calcopt");
    system ("rm -rf $wdir/restart/bk_DISPLACEMENT");
  }

  open(ITE,">$wdir/calcopt/curr_iter") or die "Cannot write curr_iter";
  print ITE "$iter\n";
  close(ITE);




  # execute NEB calculation     
  chdir "$wdir/calcopt";

  runprog('NEB');

  chdir "$wdir";
  # convert new_intgeom to cartesian
  coordsystem($coordprog,'i2c','',$linconv);
    

  # check convergence
  chdir "$wdir/calcopt";
  open(CON,"$wdir/calcopt/convergence") or die "Cannot open convergence\n";
  $_=<CON>;
  chop($_);
  close(CON);
  if ($_ eq 'T'){
    if ($printl >= 2) {
    }
    chdir "$wdir";
    $datetime=date;
    print LOG ("\n\nNEB converged within $iter iterations on $datetime\n\n");
    #  exit this loop
    if ($printl >= 0) {
      cp ("$wdir/calcopt/cartmwdist.dat", "$wdir/results/");
      cp ("$wdir/calcopt/energy.dat", "$wdir/results/");
    }
    if ($printl >= 1) {
      cp ("$wdir/calcopt/see_*", "$wdir/results/");
      cp ("$wdir/calcopt/see_conv*", "$wdir/results/");
      cp ("$wdir/calcopt/see_energy.dat", "$wdir/results/");
      cp ("$wdir/calcopt/see_angles.dat", "$wdir/results/");
      cp ("$wdir/calcopt/dyn_$iter.mld", "$wdir/results/dyn.mld");
      #cp ("$wdir/calcopt/dyn_*", "$wdir/results/");
    }
    if ($printl >= 2) {
      cp ("$wdir/calcopt/*", "$wdir/results/");
    }
    for ($i=0;$i<=$optdim+1;$i++) {
      system("cp -r $wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/* $wdir/results/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]");
    }
    cp ("$wdir/calcgrad/DISPLACEMENT/displfl", "$wdir/results/DISPLACEMENT/");
    system("rm -rf $wdir/calcgrad/*");
    system("rm -rf $wdir/DEBUG");
    system("rm -rf $wdir/restart");
    last;
  }
  if (($_ eq 'F') and ($iter == $maxit)) {
    print LOG ("Failed to converge NEB within $maxit iterations \n");
    print LOG ("PROGRAM TERMINATION WITH ERROR\n\n\n");
    die 'ERROR';
  }
  chdir "$wdir";

  # execute gradient calculations
  electronic('col','nebgrads',$mem);

  # convert gradients to internal
  coordsystem($coordprog,'grad2int','',$linconv);

  mkdir ("$wdir/DEBUG/$iter");
  system("rm -rf $wdir/calcgrad/DISPLACEMENT/CALC*/WORK");
  system("cp -r $wdir/calcgrad/DISPLACEMENT $wdir/DEBUG/$iter/");

  # to save disk space: delete old files
  if (($iter >= 2) && ((($iter-1) % $prtstep) != 0)) {
    $rem=$iter-1;
    system("rm -rf $wdir/DEBUG/$rem");
  }

  if (-e "$wdir/stop") {
    die "\nStopped by user\n\n";
  }

  #  get new hessian
  if ($hessmod eq "update") {
    chdir "$wdir/calcopt";
    runprog("nebhess");
    chdir "$wdir";

  } elsif (($hessmod eq "calc") or ($hessmod eq "calcmw")) {
    if (( $iter % $hessstep)  ==0) {
      if ($hessmod eq "calc") {
        electronic('col','nebcolhess',$mem);
      } elsif ($hessmod eq "calcmw") {
      }
    } elsif (($iter % $hessstep)  !=0) {
      if ($mixmod eq "update") {
      }
    }
  }
     
  electronic('col','newtocurr_neb','');

  chdir "$wdir";


} # end of main loop
  system("rm -rf $wdir/calcgrad/*");
  system("rm -rf $wdir/calcopt/*");

  chdir "$wdir";
 


close (LOG);

} #elsif ($initneb ==0)

print STDOUT ("\nMEP calculation finished on $datetime\n\n");



###################################################################
#
# Subroutines
#
###################################################################


###################################################################
# Independent
###################################################################

sub runprog{
  my ($prog)=@_;
  close (LOG);
  system("\$MEP/$prog.x");
  open(LOG,">>$wdir/neb.log") or die "Cannot reopen logfile";
  open(FLOG,"$prog.log") or die "Cannot open $prog.log file";
  while($_=<FLOG>) {
    chomp($_);
    if ($_ eq 'ERROR'){
      die "\nERROR in fortran program $prog \n terminating.\n"
    }
    print LOG ("$_\n");
  }
  print LOG ("\n\n");
  
}

sub check {
  -e "$wdir/geninp" or die "\nMissing geninp\n\n";
  -e "$wdir/nebpar" or die "\nMissing nebpar\n\n";



  if ($elprog eq 'col') {
    for ($i=0;$i<=$optdim+1;$i++) {
      if ($initneb ==0) {
      -e "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/MOCOEFS/mocoef_mc.sp" or die "\nMissing mocoef\n\n";
      -e "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/energy" or die "\nProblem with energy\n\n";
      -e "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GEOMS/intgeom.sp" or die "\nProblem with intgeom.sp\n\n";
      -e "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GRADIENTS/cartgrd.all" or die "\nProblem with cartgrd.all\n\n";
      }
      -e "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/control.run" or die "\nProblem with control.run\n\n";
      -e "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/intcfl" or die "\nProblem with intcfl\n\n";
    }
  } elsif ($elprog eq 'om2') {
    -e "$wdir/gradjob.inp" or die "\nMissing gradjob.inp\n\n";
  }
  
  if (($hessmod eq 'calc')or($inihess eq 'calc')) {
    -e "$wdir/hessjob/DISPLACEMENT/displfl" or die "\nProblem with COLUMBUS hessian job\n\n";
  }
     
  print LOG ("\n Successfully checked key files\n");
}





###################################################################
# Interface to electronic structure programs
###################################################################


sub electronic{
  my ($prog,$type,$mem)=@_;
  my (@icoord,@temp,$hdisp,$i,$j,$k);
  my ($next,$runs);
  my ($ii,$allf,$rest);
  my ($lstd,$lstc,@chkstat);

  if ($prog eq 'col') {

    if ($type eq 'runc') {

      system("$cbus/runc -m $mem >runls 2>>error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) {
        print LOG ("\nError in $cbus/runc -m $mem >runls 2>>error.log\n\n");
        die "Error in gradient calculation\n\n"
      } else {
        print LOG ("$cbus/runc -m $mem >runls 2>>error.log: SUCCESS \n");
      }

    } elsif ($type eq 'checkerr') {
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        if (!(/creating symbolic link/)) {
          $i=$i+1;
        }
      }
      if ($i != 0) {
        print LOG ("\nError in $cbus/runc -m $mem >runls 2>>error.log\n\n");
        die "Error in gradient calculation\n\n"
      } else {
        print LOG ("$cbus/runc -m $mem >runls 2>>error.log: SUCCESS \n");
      }

    } elsif ($type eq 'initialize') {

      for ($i=0;$i<=$optdim+1;$i++) {
        chdir "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]";
        system("\$MEP/apaga");
        chdir "$wdir";
      }

      cp ("$wdir/DISPLACEMENT/CALC.c$crd[0].d$images[0]/control.run", "$wdir");
      print LOG ("Running in sequential mode using calc.pl\n");
      chdir("$wdir");
      system("$cbus/calc.pl -restart -m $mem >& runcalcls");
      system("grep -i 'Error' runcalcls | grep -v 'runc.error' >error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) {
        print LOG ("\nError in $cbus/calc.pl -restart -m $mem >runcalcls\n\n");
        die "Error in initial COLUMBUS calculation\n\n"
      } else {
        print LOG ("$cbus/calc.pl -restart -m $mem >runcalcls: SUCCESS \n\n");
      }

      system("rm -rf $wdir/control.run $wdir/MOCOEFS $wdir/mocoef_prev ");

    }  elsif ($type eq 'calcpl') {
 
      system("nohup $cbus/calc.pl -m $mem >& runls");
      system("grep -i 'Error' runls | grep -v 'runc.error'  >error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) {
        print LOG ("\nError in nohup $cbus/calc.pl -m $mem >& runls\n\n");
        die "Error in hessian calculation\n\n"
      } else {
        print LOG ("nohup $cbus/calc.pl -m $mem >& runls: SUCCESS \n\n");
      }

    } elsif ($type eq 'nebgrads') {
      for ($i=1;$i<=$optdim;$i++) {
        chdir "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]";
        cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/MOCOEFS/mocoef_mc.sp", "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/mocoef");
        cp ("$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]", "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom");
        system("\$MEP/apaga"); 
        chdir "$wdir";
      }
      # second verion of parallelisation
      $rest=($optdim)%($nprocneb);
      $runs=($optdim-$rest)/$nprocneb;
      $next=1;
      for ($j=1;$j<=$runs;$j++) {
        $datetime=date;
     	print ("Run number $j started on $datetime\n");
        for ($i=$next;$i<$next+$nprocneb;$i++) {
          chdir "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]";
          system("$cbus/runc -m $mem >runls 2>>error.log &");
        print ("Calculating gradient of image $i\n");
        }

        # check loop if all calculations are finished
        $allf=0; 
        for($i=1;$i<=$optdim;$i++){
          $chkstat[$crd[$i]][$images[$i]]=0;
        }
        until($allf == 1){
          $allf=1;
          for ($i=$next;$i<$next+$nprocneb;$i++) {
            if ($chkstat[$crd[$i]][$images[$i]] == 0){
              if ((-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/energy") and 
                  (-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GRADIENTS/intgrd.sp") and
                  (-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/WORK/cartgrd") and
                  (-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/WORK/bmatrix") ) {
                $chkstat[$crd[$i]][$images[$i]]=1;
                print ("Gradient of CALC.c$crd[$i].d$images[$i] finished\n");
              }
            }
            for ($k=$next;$k<$next+$nprocneb;$k++){ 
              $allf=$allf*$chkstat[$crd[$k]][$images[$k]]
            }
          }
          system("sleep 0.5");
        }       
        for ($i=$next;$i<$next+$nprocneb;$i++) {
          chdir "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]";
          electronic('col','checkerr','');
        }
        $next=$next+$nprocneb;
        chdir "$wdir";
      }
      $datetime=date;
      print ("\nLast run started on $datetime\n");
      for ($i=($optdim-$rest+1);$i<=$optdim;$i++) {
        chdir "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]";
        system("$cbus/runc -m $mem >runls 2>>error.log &");
        print ("Calculating gradient of image $i\n");

      }
      # check loop if all calculations are finished
      $allf=0; 
      until($allf == $rest){
        $allf=0;
        for ($i=($optdim-$rest+1);$i<=$optdim;$i++) {
          if ((-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/energy") and 
              (-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GRADIENTS/intgrd.sp") and
              (-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/WORK/cartgrd") and
              (-e "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/WORK/bmatrix") ) {
            $allf=$allf+1;
          }


        }
        system("sleep 0.5");
      }


      for ($i=($optdim-$rest+1);$i<=$optdim;$i++) {
        chdir "$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]";
        electronic('col','checkerr','');
      }

      for ($i=1;$i<=$optdim;$i++) {
        cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/energy", "$wdir/calcopt/new_energy.c$crd[$i].d$images[$i]");
        cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/GRADIENTS/intgrd.sp", "$wdir/calcopt/new_intgrd.c$crd[$i].d$images[$i]");
        cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/WORK/cartgrd", "$wdir/calcopt/new_cartgrd.c$crd[$i].d$images[$i]");
        cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/WORK/bmatrix", "$wdir/calcopt/new_bmatrix.c$crd[$i].d$images[$i]");
      }

    } elsif ($type eq 'nebcolhess') {
    for ($ii=1;$ii<=$optdim;$ii++) {

      system("rm -rf $wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/*");

      chdir "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/";
      print LOG ("Starting COLUMBUS hessian calculation\n\n");
      system ("cp -r $wdir/hessjob/* $wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/");
      cp ("$wdir/calcopt/curr_geom.c$crd[$ii].d$images[$ii]", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/geom.start");
      cp ("$wdir/calcopt/curr_geom.c$crd[$ii].d$images[$ii]", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/geom");
      cp ("$wdir/calcgrad/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/mocoef", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/mocoef");
      cp ("$wdir/calcopt/curr_bmatrix.c$crd[$ii].d$images[$ii]", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/");
      cp ("$wdir/calcopt/intcfl.c$crd[$ii].d$images[$ii]", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/intcfl");


      open(DIS3,"<$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/displfl") or die "Cannot read dispfl for hessian";
      chomp ($_=<DIS3>);
      chomp ($_=<DIS3>);
      chomp ($_=<DIS3>);
      cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/geom", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/REFPOINT/");
      cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/mocoef", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/REFPOINT/");
      while($_=<DIS3>){
        ($i,$j,$k)=split(/\s+/,$_);
        if ($k eq 'doit') {
          $lstc=$i;
          $lstd=$j;
          chdir "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j";
          cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/geom", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j/");
          cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/mocoef", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j/");
          cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j/geom","$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j/geom.start");
          system("$cbus/bmat.x < bmatin > bmatls");
          cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j/geom","$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j/geom.displ");
          cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/DISPLACEMENT/CALC.c$i.d$j/bmatls", "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/curr_bmatls");
        }
      }
      close (DIS3);
    }

# Parallel execution of hessian calculations
      $rest=($optdim)%($nprocneb);
      $runs=($optdim-$rest)/$nprocneb;
      $next=1;
      for ($j=1;$j<=$runs;$j++) {
        $datetime=date;
     	print ("Run number $j started on $datetime\n");
        for ($i=$next;$i<$next+$nprocneb;$i++) {
          chdir "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/";
          system("nohup $cbus/calc.pl -m $mem >& runls &");
          print ("Calculating hessian of image $i\n");
        }

        # check loop if all calculations are finished
        $allf=0; 
        until($allf == $nprocneb){
          $allf=0;
          for ($i=$next;$i<$next+$nprocneb;$i++) {
            if (-e "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/DISPLACEMENT/CALC.c$lstc.d$lstd/LISTINGS/energy") {
              $allf=$allf+1;
            }
          }
          system("sleep 0.1");
        }       
        system("sleep 3.1");
        $next=$next+$nprocneb;
      }
      $datetime=date;
      print ("\nLast run started on $datetime\n");
      for ($i=($optdim-$rest+1);$i<=$optdim;$i++) {
        chdir "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/";
        system("nohup $cbus/calc.pl -m $mem >& runls & ");
        print ("Calculating hessian of image $i\n");
      }
      # check loop if all calculations are finished
      $allf=0; 
      until($allf == $rest){
        $allf=0;
        for ($i=($optdim-$rest+1);$i<=$optdim;$i++) {
          if (-e "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/DISPLACEMENT/CALC.c$lstc.d$lstd/LISTINGS/energy") {
            $allf=$allf+1;
          }
        }
        system("sleep 0.1");
      }
      system("sleep 3.1");

    # check for errors and get hessians
    for ($ii=1;$ii<=$optdim;$ii++) {
      chdir "$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/";
      system("grep -i 'Error' runls | grep -v 'runc.error' >error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) {
        print LOG ("\nError in nohup $cbus/calc.pl -m $mem >& runls\n\n");
        die "Error in hessian calculation\n\n"
      } else {
        print LOG ("nohup $cbus/calc.pl -m $mem >& runls: SUCCESS \n\n");
      }
      system("$cbus/forceconst.pl 2> error");
      system("grep -i 'Error' runls | grep -v 'runc.error'  >error.log");
      $i=0;
      open(ERR,"<error.log") or die "Cannot open error.log\n";
      while ($_=<ERR>){
        print LOG ("$_");
        $i=$i+1;
      }
      if ($i != 0) {
        print LOG ("\nError in $cbus/forceconst.pl -$mem >& runls\n\n");
        die "Error in hessian calculation\n\n"
      } else {
        print LOG ("$cbus/forceconst.pl -$mem >& runls: SUCCESS \n\n");
      }

      cp ("$wdir/calchess/DISPLACEMENT/CALC.c$crd[$ii].d$images[$ii]/hessian", "$wdir/calcopt/hessian.c$crd[$ii].d$images[$ii]");


      print LOG ("Calculation of hessian with COLUMBUS: SUCCESS\n");


    }

      chdir ("$wdir/calcopt");
      runprog("nebhess");
      chdir "$wdir";


    } elsif ($type eq 'mephess') {
 
    } elsif ($type eq'ini') {

    } elsif ($type eq 'ref') {


    } elsif (($type eq 'curr') or ($type eq 'new')) {
 
    } elsif ($type eq 'copyresults') {

      print LOG ("Copying results ...");
      print LOG ("... finished\n");

    } elsif ($type eq 'copydebug') {

      print LOG ("Copying debug information ...");
      print LOG ("... finished\n\n");

    } elsif ($type eq 'first') {

      for ($i=1;$i<=$optdim;$i++) {
        open(AIG,">>$wdir/calcopt/intgeom_all.c$crd[$i].d$images[$i]") or die "Cannot append to intgeom_all.c$crd[$i].d$images[$i]";
        open(NIG,"<$wdir/calcopt/curr_intgeom.c$crd[$i].d$images[$i]") or die "Cannot read from curr_intgeom.c$crd[$i].d$images[$i]";
        while($_=<NIG>) {
          chomp($_);
          print AIG ("$_\n");
        }
        print AIG ("\n");
        close(NIG);
        close(AIG);
      }

      if ($coordprog eq 'cart') {
      for ($i=1;$i<=$optdim;$i++) {
        open(AIG,">>$wdir/calcopt/geom_all.c$crd[$i].d$images[$i]") or die "Cannot append to geom_all.c$crd[$i].d$images[$i]";
        open(NIG,"<$wdir/calcopt/curr_geom.c$crd[$i].d$images[$i]") or die "Cannot read from curr_geom.c$crd[$i].d$images[$i]";
        while($_=<NIG>) {
          chomp($_);
          print AIG ("$_\n");
        }
        print AIG ("\n");
        close(NIG);
        close(AIG);
      }
      }

      for ($i=0;$i<=$optdim+1;$i++) {

        if ($coordprog ne 'cart') {

        cp ("$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]", "$wdir/calcopt/first_bmatrix.c$crd[$i].d$images[$i]");


        open(ABM,">>$wdir/calcopt/bmatrix_all.c$crd[$i].d$images[$i]") or die "Cannot append to bmatrix_all.c$crd[$i].d$images[$i]";
        open(NBM,"<$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]") or die "Cannot read from curr_bmatrix.c$crd[$i].d$images[$i]";
        while($_=<NBM>) {
          chomp($_);
          print ABM ("$_\n");
        }
        print ABM ("\n");
        close(NBM);
        close(ABM);

        }

        cp ("$wdir/calcopt/curr_geom.c$crd[$i].d$images[$i]", "$wdir/calcopt/first_geom.c$crd[$i].d$images[$i]");
        cp ("$wdir/calcopt/curr_cartgrd.c$crd[$i].d$images[$i]", "$wdir/calcopt/first_cartgrd.c$crd[$i].d$images[$i]");
      }

    } elsif ($type eq 'newtocurr_neb') {
      if ($coordprog ne 'cart') { 
      for ($i=0;$i<=$optdim+1;$i++) { 

        open(ABM,">>$wdir/calcopt/bmatrix_all.c$crd[$i].d$images[$i]") or die "Cannot append to bmatrix_all.c$crd[$i].d$images[$i]";
        open(NBM,"<$wdir/calcopt/new_bmatrix.c$crd[$i].d$images[$i]") or die "Cannot read from new_bmatrix.c$crd[$i].d$images[$i]";
        while($_=<NBM>) {
          chomp($_);
          print ABM ("$_\n");
        }
        print ABM ("\n");
        close(NBM);
        close(ABM);
      }
      } # if coorprog
      for ($i=1;$i<=$optdim;$i++) { 
        if ($coordprog ne 'cart') {
        open(AIG,">>$wdir/calcopt/intgeom_all.c$crd[$i].d$images[$i]") or die "Cannot append to intgeom_all.c$crd[$i].d$images[$i]";
        open(NIG,"<$wdir/calcopt/new_intgeom.c$crd[$i].d$images[$i]") or die "Cannot read from new_intgeom.c$crd[$i].d$images[$i]";
        while($_=<NIG>) {
          chomp($_);
          print AIG ("$_\n");
        }
        print AIG ("\n");
        close(NIG);
        close(AIG);
        mv ("$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]", "$wdir/calcopt/prev_bmatrix.c$crd[$i].d$images[$i]");
        mv ("$wdir/calcopt/curr_intgeom.c$crd[$i].d$images[$i]", "$wdir/calcopt/prev_intgeom.c$crd[$i].d$images[$i]");

        mv ("$wdir/calcopt/new_bmatrix.c$crd[$i].d$images[$i]", "$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]");
        mv ("$wdir/calcopt/new_intgeom.c$crd[$i].d$images[$i]", "$wdir/calcopt/curr_intgeom.c$crd[$i].d$images[$i]");
        mv ("$wdir/calcopt/new_intgrd.c$crd[$i].d$images[$i]", "$wdir/calcopt/curr_intgrd.c$crd[$i].d$images[$i]");

        } elsif ($coordprog eq 'cart') {
        open(AIG,">>$wdir/calcopt/geom_all.c$crd[$i].d$images[$i]") or die "Cannot append to geom_all.c$crd[$i].d$images[$i]";
        open(NIG,"<$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]") or die "Cannot read from new_geom.c$crd[$i].d$images[$i]";
        while($_=<NIG>) {
          chomp($_);
          print AIG ("$_\n");
        }
        print AIG ("\n");
        close(NIG);
        close(AIG);
        } # if coorprog
        open(AIG,">>$wdir/calcopt/uwdq_all.c$crd[$i].d$images[$i]") or die "Cannot append to uwdq_all.c$crd[$i].d$images[$i]";
        open(NIG,"<$wdir/calcopt/uwdq.c$crd[$i].d$images[$i]") or die "Cannot read from uwdq.c$crd[$i].d$images[$i]";
        while($_=<NIG>) {
          chomp($_);
          print AIG ("$_\n");
        }
        print AIG ("\n");
        close(NIG);
        close(AIG);

        mv ("$wdir/calcopt/new_cartgrd.c$crd[$i].d$images[$i]", "$wdir/calcopt/curr_cartgrd.c$crd[$i].d$images[$i]");
        mv ("$wdir/calcopt/new_energy.c$crd[$i].d$images[$i]", "$wdir/calcopt/curr_energy.c$crd[$i].d$images[$i]");
        mv ("$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]", "$wdir/calcopt/curr_geom.c$crd[$i].d$images[$i]");
      }
    } elsif ($type eq 'cleanup') {

      system ("rm -f $wdir/gradjob/geom* $wdir/gradjob/mocoef*");

    }

  } #if prog

}




###################################################################
# Interface to coordinate system transformtation programs
###################################################################

sub runpulay{
  system("$cbus/cart2int.x 2>> error.log");
  open(BU,"<bummer") or die "Cannot open bummer";
  chomp ($_=<BU>);
  close(BU);
  if (!($_ eq ' normally terminated')){
    print LOG ("\nError: Problem with $cbus/cart2int.x\n");
    die "Problem with $cbus/cart2int.x";
  } 
  if ($printl >= 2) {
    print LOG ("\nFinished $cbus/cart2int.x: SUCCESS\n\n");
  }
}


sub coordsystem{
  my ($prog,$kind,$type,$lc)=@_;
  my ($i,$j);

  if ($prog eq 'cart') {

  if ($kind eq 'initialize') {
    chdir ("$wdir/temp");
    cp ("$wdir/geninp", "$wdir/temp");
    for ($i=1;$i<=$optdim+1;$i++) {
      cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom","$wdir/temp/geom");
      cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i-1].d$images[$i-1]/geom","$wdir/temp/ref_geom");
      runprog('orientgeom');
      cp ("$wdir/temp/new_geom", "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom");
      system("rm -f geom new_geom ref_geom orientgeom*");
    }

  } elsif ($kind eq 'oldalign') {
    chdir ("$wdir/temp");
    cp ("$wdir/geninp", "$wdir/temp");
    for ($i=1;$i<=$optdim;$i++) {
      cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom","$wdir/temp/geom");
      cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i-1].d$images[$i-1]/geom","$wdir/temp/ref_geom");
      runprog('orientgeom');
      cp ("$wdir/temp/new_geom", "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom");
      system("rm -f geom new_geom ref_geom orientgeom*");
    }

  } elsif ($kind eq 'align') {
    chdir ("$wdir/temp");
    cp ("$wdir/geninp", "$wdir/temp");
    for ($i=1;$i<=$optdim;$i++) {
      cp ("$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]","$wdir/temp/geom");
      cp ("$wdir/calcopt/new_geom.c$crd[$i-1].d$images[$i-1]","$wdir/temp/ref_geom");
      runprog('orientgeom');
      cp ("$wdir/temp/new_geom", "$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]/");
      system("rm -f geom new_geom ref_geom orientgeom*");
    }

  } elsif ($kind eq 'lic') {

    chdir ("$wdir/temp");
    cp ("$wdir/geninp", "$wdir/temp");
    cp ("$wdir/nebpar", "$wdir/temp");
    cp ("$wdir/geom.ini", "$wdir/temp/ref_geom");
    cp ("$wdir/geom.last", "$wdir/temp/geom");
    runprog('orientgeom');
    cp ("$wdir/geom.ini", "$wdir/temp/");
    cp ("$wdir/temp/new_geom", "$wdir/temp/geom.last");
    runprog("lic");



  }


  } elsif ($prog eq 'zmat') {

    if ($kind eq 'nebini') {
      # convert initial geometries to internal: get bmatrix !!!!!
      cp ("$wdir/calcopt/geninp", "$wdir/temp/"); 
      for ($i=0;$i<=$optdim+1;$i++) {
        chdir "$wdir/temp";
        cp ("$wdir/calcopt/curr_geom.c$crd[$i].d$images[$i]", "$wdir/temp/geom");

        cp ("$wdir/calcopt/intcfl.c$crd[$i].d$images[$i]", "$wdir/temp/intcfl");

        # create inputfile for cart -> int
        open(C2I,">zmatin") or die "Cannot write to zmatin ";
          print C2I " &zmatin\n";
          print C2I " geom_c2i = 1\n";
          print C2I " bmat     = 1\n";
          print C2I " &end \n";
        close(C2I);
         
        # transform cart ->int
        runprog('zmat');
        cp ("$wdir/temp/bmatrix", "$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]");
        system('rm -f bmatrix zmat.log');
        
        if (($i==0)or($i==($optdim+1))) {
          cp ("$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]", "$wdir/calcopt/new_bmatrix.c$crd[$i].d$images[$i]");
          cp ("$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]", "$wdir/calcopt/prev_bmatrix.c$crd[$i].d$images[$i]");

          cp ("$wdir/calcopt/curr_intgeom.c$crd[$i].d$images[$i]", "$wdir/calcopt/prev_intgeom.c$crd[$i].d$images[$i]");
        }
      }

    }  elsif ($kind eq 'lic') {

      chdir ("$wdir/temp");
      cp ("$wdir/geninp", "$wdir/temp/");
      cp ("$wdir/nebpar", "$wdir/temp");
        chdir "$wdir/temp";
        # align geometry for bmatrix 
        cp ("$wdir/geninp", "$wdir/temp");
        cp ("$wdir/geom.ini", "$wdir/temp/ref_geom");
        cp ("$wdir/geom.last", "$wdir/temp/geom");
        runprog('orientgeom');
        cp ("$wdir/temp/new_geom", "$wdir/temp/geom.last");

        open(C2I,">zmatin") or die "Cannot write to zmatin ";
          print C2I " &zmatin\n";
          print C2I " geom_c2i = 1\n";
          print C2I " intcfl = 1\n";
          print C2I " bmat     = 1\n";
          print C2I " &end \n";
        close(C2I);

        cp ("$wdir/temp/geom.last", "$wdir/temp/geom");
        runprog('zmat');
        cp ("$wdir/temp/intgeom", "$wdir/temp/intgeom.last");
        cp ("$wdir/temp/bmatrix", "$wdir/temp/bmatrix.last");

        cp ("$wdir/geom.ini", "$wdir/temp/geom");
        runprog('zmat');
        cp ("$wdir/temp/intgeom", "$wdir/temp/intgeom.ini");
        cp ("$wdir/temp/bmatrix", "$wdir/temp/bmatrix.ini");

        # use geom.ini intcfl
        system("rm -f bmatrix zmat_geom intgeom zmat.log"); 
        runprog("lic");

        #backconvert geometries
        for ($i=0;$i<=$lc-1;$i++) {
          cp ("$wdir/temp/intgeom.$i", "$wdir/temp/intgeom");
          open(C2I,">zmatin") or die "Cannot write to zmatin ";
          print C2I " &zmatin\n";
          print C2I " geom_i2c     = 1\n";
          print C2I " bmat     = 1\n";
          print C2I " &end \n";
          close(C2I);
          runprog('zmat');
          cp ("$wdir/temp/zmat_geom", "$wdir/temp/geom.$i");
          system("rm -f bmatrix zmat_geom intgeom zmat.log");
        }
      


    }  elsif ($kind eq 'i2c') {

      for ($i=1;$i<=$optdim;$i++) {
        print LOG ("Converting geometry c$crd[$i].d$images[$i]\n");
        chdir "$wdir/temp";
        cp ("$wdir/calcopt/geninp", "$wdir/temp/"); 
        cp ("$wdir/calcopt/curr_geom.c$crd[0].d$images[0]", "$wdir/temp/geom");
        cp ("$wdir/calcopt/new_intgeom.c$crd[$i].d$images[$i]", "$wdir/temp/intgeom");
        cp ("$wdir/calcopt/intcfl.c$crd[$i].d$images[$i]", "$wdir/temp/intcfl");

        # create inputfile for cart -> int
        open(C2I,">zmatin") or die "Cannot write to zmatin ";
          print C2I " &zmatin\n";
          print C2I " geom_i2c     = 1\n";
          print C2I " bmat     = 1\n";
          print C2I " &end \n";
        close(C2I);
 
        # transform int ->cart
        runprog('zmat');

        cp ("$wdir/temp/zmat_geom", "$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]");
        system("rm -f bmatrix zmat_geom intgeom zmat.log");


      }

    } elsif ($kind eq 'initialize') {
        chdir ("$wdir/temp");
        cp ("$wdir/geninp", "$wdir/temp");
        cp ("$wdir/DISPLACEMENT/CALC.c$crd[0].d$images[0]/geom","$wdir/temp");
        open(C2I,">zmatin") or die "Cannot write to zmatin ";
          print C2I " &zmatin\n";
          print C2I " geom_c2i     = 1\n";
          print C2I " geom_i2c     = 1\n";
          print C2I " intcfl     = 1\n";
          print C2I " &end \n";
        close(C2I);
        runprog('zmat');
        for ($i=0;$i<=$optdim+1;$i++) {
          cp ("$wdir/temp/intcfl", "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/intcfl");
        } 
        open(C2I,">zmatin") or die "Cannot write to zmatin ";
          print C2I " &zmatin\n";
          print C2I " geom_c2i     = 1\n";
          print C2I " geom_i2c     = 1\n";
          print C2I " &end \n";
        close(C2I);
        for ($i=0;$i<=$optdim+1;$i++) {
          cp ("$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom", "$wdir/temp");
          runprog('zmat');
          cp ("$wdir/temp/zmat_geom", "$wdir/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom");
          system("rm -rf intgeom zmat_geom zmat.log");
        }
        


 
    } elsif ($kind eq 'grad2int') {

      system("rm -rf $wdir/temp/*");
      for ($i=1;$i<=$optdim;$i++) {
        print LOG ("Converting gradient c$crd[$i].d$images[$i]\n");
        chdir "$wdir/temp";
        cp ("$wdir/geninp", "$wdir/temp");
        cp ("$wdir/calcopt/new_cartgrd.c$crd[$i].d$images[$i]", "$wdir/temp/cartgrd");
        cp ("$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]", "$wdir/temp/geom");
        cp ("$wdir/calcopt/intcfl.c$crd[$i].d$images[$i]", "$wdir/temp/intcfl");

        # create inputfile for cart -> int
        open(C2I,">zmatin") or die "Cannot write to zmatin ";
          print C2I " &zmatin\n";
          print C2I " geom_c2i  = 1\n";
          print C2I " grd_c2i  = 1\n";
          print C2I " bmat     = 1\n";
          print C2I " &end \n";
        close(C2I);

        # transform int ->cart
        runprog('zmat');

        cp ("$wdir/temp/gradE_q", "$wdir/calcopt/new_intgrd.c$crd[$i].d$images[$i]");
        cp ("$wdir/temp/bmatrix", "$wdir/calcopt/new_bmatrix.c$crd[$i].d$images[$i]");
        system("rm -rf $wdir/temp/*");

      }
   }

  }  elsif ($prog eq 'pulay') {

    if ($kind eq 'nebini') {
      # convert initial geometries to internal: get bmatrix !!!!!
      
      for ($i=0;$i<=$optdim+1;$i++) {
        chdir "$wdir/temp";
        cp ("$wdir/calcopt/curr_geom.c$crd[$i].d$images[$i]", "$wdir/temp/geom");
        open(CG,">$wdir/temp/cartgrd") or die "Cannot write cartgrd";
        for ($j=1;$j<=$nat;$j++) {
        print CG ("   0.0            0.0            0.0\n");
        }
        close (CG);

        cp ("$wdir/calcopt/intcfl.c$crd[$i].d$images[$i]", "$wdir/temp/intcfl");

        # create inputfile for cart -> int
        open(C2I,">cart2intin") or die "Cannot write to car2intin ";
          print C2I " &input\n";
          print C2I " calctype='cart2int',\n";
          print C2I " /&end \n";
        close(C2I);
      
        # transform cart ->int
        runpulay();
        cp ("$wdir/temp/bmatrix", "$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]");
        system('rm -f cart2int* bummer bmatrix geom');
        
        if (($i==0)or($i==($optdim+1))) {
          cp ("$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]", "$wdir/calcopt/new_bmatrix.c$crd[$i].d$images[$i]");
          cp ("$wdir/calcopt/curr_bmatrix.c$crd[$i].d$images[$i]", "$wdir/calcopt/prev_bmatrix.c$crd[$i].d$images[$i]");

          cp ("$wdir/calcopt/curr_intgeom.c$crd[$i].d$images[$i]", "$wdir/calcopt/prev_intgeom.c$crd[$i].d$images[$i]");
        }
      }

    } elsif ($kind eq 'lic') {

      chdir "$wdir/temp";
        # align geometry for bmatrix 
        cp ("$wdir/geninp", "$wdir/temp");
        cp ("$wdir/nebpar", "$wdir/temp");
        cp ("$wdir/geom.ini", "$wdir/temp/ref_geom");
        cp ("$wdir/geom.last", "$wdir/temp/geom");
        runprog('orientgeom');
        cp ("$wdir/temp/new_geom", "$wdir/temp/geom.last");

        cp ("$wdir/temp/geom.last", "$wdir/temp/geom");
        open(CG,">$wdir/temp/cartgrd") or die "Cannot write cartgrd";
        for ($j=1;$j<=$nat;$j++) {
        print CG ("   0.0            0.0            0.0\n");
        }
        close (CG);
        cp ("$wdir/intcfl", "$wdir/temp/");
        # create inputfile for cart -> int
        open(C2I,">cart2intin") or die "Cannot write to car2intin ";
          print C2I " &input\n";
          print C2I " calctype='cart2int',\n";
          print C2I " /&end \n";
        close(C2I);

        runpulay();
        cp ("$wdir/temp/bmatrix", "$wdir/temp/bmatrix.last");
        cp ("$wdir/temp/intgeom", "$wdir/temp/intgeom.last");
        system('rm -f cart2intls bummer bmatrix geom');
        cp ("$wdir/geom.ini", "$wdir/temp/geom");
        runpulay();
        cp ("$wdir/temp/bmatrix", "$wdir/temp/bmatrix.ini");
        cp ("$wdir/temp/intgeom", "$wdir/temp/intgeom.ini");
        runprog("lic");

        # backconvert geometries

        cp ("$wdir/geom.ini", "$wdir/temp/geom");
        for ($i=0;$i<=$lc-1;$i++) {
          if ($i >= 1) {
            $j=$i-1;
            cp ("$wdir/temp/geom.$j", "$wdir/temp/geom");
          }
          cp ("$wdir/temp/intgeom.$i", "$wdir/temp/intgeomch");
          open(C2I,">cart2intin") or die "Cannot write to car2intin ";
          print C2I (" &input\n");
          print C2I (" calctype='int2cart',\n");
          print C2I (" linonly=0,\n");
          print C2I (" maxiter=200,\n");
          print C2I (" geomch=0,\n");
          print C2I (" maxstep=0.1,\n");
          print C2I (" /&end \n");
          close(C2I);

          system("$cbus/cart2int.x 2>> error.log");
          open(BU,"<bummer") or die "Cannot open bummer";
          chomp ($_=<BU>);
          close(BU);
          if (!($_ eq ' normally terminated')){
            print LOG ("\nError: Problem with $cbus/cart2int.x\n");
          }
          if ($printl >= 2) {
            print LOG ("\nFinished $cbus/cart2int.x: SUCCESS\n\n");
          }

          $see=(!(-e "geom.new"));
          if ($see eq "1"){
            cp ("$wdir/geom.last", "$wdir/temp/geom");            
            runpulay();
          }
          cp ("$wdir/temp/geom.new", "$wdir/temp/geom.$i");
          system('rm -f cart2intls bummer bmatrix geom');
        }
        

    }  elsif ($kind eq 'i2c') {

      for ($i=1;$i<=$optdim;$i++) {
        print LOG ("Converting geometry c$crd[$i].d$images[$i]\n");
        chdir "$wdir/temp";
        cp ("$wdir/calcopt/new_intgeom.c$crd[$i].d$images[$i]", "$wdir/temp/intgeomch");
        cp ("$wdir/calcopt/intcfl.c$crd[$i].d$images[$i]", "$wdir/temp/intcfl");



        print LOG (" ... using first data\n");
        cp ("$wdir/calcopt/first_cartgrd.c$crd[$i].d$images[$i]", "$wdir/temp/cartgrd");
        cp ("$wdir/calcopt/first_geom.c$crd[$i].d$images[$i]", "$wdir/temp/geom");

        # create inputfile for int -> cart
        open(C2I,">cart2intin") or die "Cannot write to car2intin ";
          print C2I (" &input\n");
          print C2I (" calctype='int2cart',\n");
          print C2I (" linonly=0,\n");
          print C2I (" maxiter=200,\n");
          print C2I (" geomch=0,\n");
          print C2I (" maxstep=0.1,\n");
          print C2I (" /&end \n");
        close(C2I);

        # transform int ->cart
        system("$cbus/cart2int.x 2>> error.log");
        open(BU,"<bummer") or die "Cannot open bummer";
        chomp ($_=<BU>);
        close(BU);
        if (!($_ eq ' normally terminated')){
          print LOG ("\nError: Problem with $cbus/cart2int.x\n");
        }
        if ($printl >= 2) {
          print LOG ("\nFinished $cbus/cart2int.x: SUCCESS\n\n");
        }

        $see=(!(-e "geom.new"));
        if ($see eq "1"){
          print LOG (" ... using curr data\n");
          cp ("$wdir/calcopt/curr_cartgrd.c$crd[$i].d$images[$i]", "$wdir/temp/cartgrd");
          cp ("$wdir/calcopt/curr_geom.c$crd[$i].d$images[$i]", "$wdir/temp/geom");

 
          # transform int ->cart
          runpulay();
        }

        $see=(!(-e "geom.new"));
        if (($lc==1)and($see eq "1")) {
          print LOG ("Switching to linconv=1\n");
          open(C2I,">cart2intin") or die "Cannot write to car2intin ";
          print C2I (" &input\n");
          print C2I (" calctype='int2cart',\n");
          print C2I (" linonly=1,\n");
          print C2I (" maxiter=200,\n");
          print C2I (" geomch=0,\n");
          print C2I (" maxstep=0.1,\n");
          print C2I (" /&end \n");
          close(C2I);
          # transform int ->cart
          runpulay();

        }

        cp ("$wdir/temp/geom.new", "$wdir/calcopt/new_geom.c$crd[$i].d$images[$i]");
        system("rm -rf $wdir/temp/*");


      }

    } elsif ($kind eq 'grad2int') {


    } elsif ($kind eq 'getrefgeom') {
 


    } elsif ($kind eq 'aligngeom') {

   
    } # if kind

  } # if prog

}


