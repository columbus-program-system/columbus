package defaults;
require Exporter;

# (c) Bernhard Sellner 2011
# part of MEPPACK

use strict;
use warnings;

sub max{
  my(@en)=@_;
  my($wsr1,$i);

  $wsr1=$en[0][1];
  for($i=1;$i<@en;$i++){
    if ($en[$i][1] <= $wsr1){
      $wsr1=$en[$i][1]
    }
  }
  return $wsr1;
}

sub prepdef{
  my($bdir,$wdir,$typ)=@_;
  my($i,$mni,$mii,$mli,$mfi);

  open(WDM,"<$bdir/def_mod.f90") or die "Cannot open $bdir/def_mod.f90\n";
  $i=1;
  while(<WDM>){
    $i++;
  }
  $mfi=$i;
  close(WDM);
  open(WDM,"<$bdir/def_mod.f90") or die "Cannot open $bdir/def_mod.f90\n";
  $i=0;
  while(<WDM>){
    $i++;
    if (/typ.eq.'neb'/){
      $mni=$i;
      last;
    }
  }
  close(WDM);
  open(WDM,"<$bdir/def_mod.f90") or die "Cannot open $bdir/def_mod.f90\n";
  $i=0;
  while(<WDM>){
    $i++;
    if (/typ.eq.'irc'/){
      $mii=$i;
      last;
    }
  }
  close(WDM);
  open(WDM,"<$bdir/def_mod.f90") or die "Cannot open $bdir/def_mod.f90\n";
  $i=0;
  while(<WDM>){
    $i++;
    if (/flag for perl/){
      $mli=$i;
      last;
    }
  }
  close(WDM);

  if ($typ eq 'neb'){

    open(WDM,"<$bdir/def_mod.f90") or die "Cannot open $bdir/def_mod.f90\n";
    open(TDM,">$wdir/def") or die "Cannot write $wdir/def\n";
    for ($i=1;$i<=$mii;$i++) {
      $_=<WDM>;
      print TDM ("$_");
    } 
    for ($i=1;$i<=($mli-$mii);$i++) {
      $_=<WDM>;
    }
    for ($i=1;$i<($mfi-$mli);$i++) {
      $_=<WDM>;
      print TDM ("$_");
    } 

    close(WDM);
    close(TDM);

  } elsif ($typ eq 'irc') {


    open(WDM,"<$bdir/def_mod.f90") or die "Cannot open $bdir/def_mod.f90\n";
    open(TDM,">$wdir/def") or die "Cannot write $wdir/def\n";
    for ($i=1;$i<$mni;$i++) {
      $_=<WDM>;
      print TDM ("$_");
    } 
    for ($i=1;$i<=($mii-$mni);$i++) {
      $_=<WDM>;
    }
    for ($i=1;$i<=($mfi-$mii);$i++) {
      $_=<WDM>;
      print TDM ("$_");
    } 

    close(WDM);
    close(TDM);
  }
}

sub definit{
  my($wdir,@akw)=@_;
  my($i,$mni,$mii);

  for($i=0;$i<@akw;$i++){

     $akw[$i][1]=getkeyw('def',$akw[$i][0],$akw[$i][1],$wdir);

  }
}

sub writerestart {
  my ($value,$wdir)=@_;
  my($ws1,$ws2,$fname,$kwname,$i);

  $fname='geninp';
  $kwname='restart';

  $ws1=lookkw($fname,$kwname,$wdir);
  if ($ws1 == 0) {

  $ws1=countkw($fname,$kwname,$wdir);
  $ws2=countkw($fname,'totallinesnot',$wdir);


  open(NNI,">$wdir/restart/geninp") or die "Cannot write $wdir/restart/geninp\n";
  open(NI,"<$wdir/geninp") or die "Cannot open $wdir/geninp !\n";
  for ($i=1;$i<=($ws1-1);$i++) {
    $_=<NI>;
    print NNI ("$_");
  }
  print NNI ("  restart    =   $value\n");
  $_=<NI>;
  for ($i=($ws1+1);$i<=$ws2;$i++) {
    $_=<NI>;
    print NNI ("$_");
  }
  close (NNI);
  close (NI);

  } else {

  open(NNI,">$wdir/restart/geninp") or die "Cannot write $wdir/restart/geninp\n";
  open(NI,"<$wdir/geninp") or die "Cannot open $wdir/geninp !\n";
  $_=<NI>;
  print NNI ("$_");
  print NNI ("  restart    =   $value\n");
  while ($_=<NI>) {
    print NNI ("$_");
  }
  close (NNI);
  close (NI);



  }

}

sub lookkw{
  my($fname,$kwname,$wdir)=@_;
  my($ws1);

  $ws1=1;

  open(FN,"<$wdir/$fname") or die "Cannot open $wdir/$fname !\n";
  while ($_=<FN>) {
    if (/$kwname/i) {
     if (/=/ && !(/=dpr/)) {
      $ws1=$ws1*0;
     }
    }
  }
  return $ws1;
  close(FN);

}

sub countkw{
  my($fname,$kwname,$wdir)=@_;
  my($ws1,$ws2);

  $ws1=0;
  $ws2=-1;

  open(FN,"<$wdir/$fname") or die "Cannot open $wdir/$fname !\n";
  while ($_=<FN>) {
    $ws1++;
    if (/$kwname/i) {
     if (/=/ && !(/=dpr/)) {
      $ws2=$ws1;
    }
    }
  }
  if ($ws2==-1) {
    $ws2=$ws1;
  }
  return $ws2;
  close(FN);

}

sub getkeyw{
  my($fname,$kwname,$kw,$wdir)=@_;
  my($ws1,$ws2);

  $ws1=lookkw($fname,$kwname,$wdir);

  if ($ws1 == 0) {

  open(FN,"<$wdir/$fname") or die "Cannot open $wdir/$fname !\n";
  while ($_=<FN>) {
    if (/$kwname/i) {
     if (/=/ && !(/=dpr/)) {
      chomp($_);
      ($ws1,$ws2)=split(/=\s+/,$_);
      $ws2 =~ s/\s+//g;

     if ($ws2 eq '.false.') {
       $ws2 ='F';
     }
     if ($ws2 eq '.true.') {
       $ws2 ='T';
     }
     if ($ws2 =~ /^'/) {
       $ws2 =~ s/'//g;
     }
     if ($ws2 =~ /_dpr/) {
       $ws2 =~ s/_dpr//g;
     }
      return $ws2;

    }
    }
  }
  close(FN);

  } else {
    return $kw;
  }
 
}


sub test {
  my ($check)=@_;

  system("echo world,$check\n");

}

sub angles{
  my($wdir)=@_;
  my($lin,$i,@type,@work,$pi);
  $pi=3.1415926536;

  open(INT,"<$wdir/a_intcfl") or die "Cannot open $wdir/a_intcfl \n";
  while($_=<INT>) {
    if(/K/){
      $lin++
    }
  }
  close(INT);

  for ($i=1;$i<=$lin;$i++) {
    $type[$i]=0;
  }

  open(INT,"<$wdir/a_intcfl") or die "Cannot open $wdir/a_intcfl \n";
  $i=0;
  while($_=<INT>) {
    if(/K/ ){
      $i++;
      if (/BEND/ or /TORS/ ){
        $type[$i]=1;
      }
    }
  }

  open(AIC,"<$wdir/ANALYSIS/intc.dat") or die "Cannot open $wdir/ANALYSIS/intc.dat \n";

  open(NAIC,">$wdir/ANALYSIS/intc_deg.dat") or die "Cannot write $wdir/ANALYSIS/intc_deg.dat \n";
  while($_=<AIC>){
    @work=split(/\s+/,$_);
    for ($i=1;$i<=$lin;$i++) {
      if ($type[$i] ==1) {
        $work[$i]=(360*$work[$i])/(2*$pi);
      }
    }
    for ($i=1;$i<=$lin;$i++) {
      printf NAIC ("%14.8f", $work[$i]);
    }
    print NAIC "\n";

  }
}
