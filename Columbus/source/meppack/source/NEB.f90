!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program NEB ! written by Bernhard Sellner
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! It is the main program for relaxation of a nudged elastic
! band to a minimum energy pathway. The relaxation in 
! internal coordinates is in the main program, the 
! relaxation in cartesian coordinates is in the 
! subroutine cartneb()


use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) Gn12(:,:),Gp12(:,:),bm(:,:,:),wmr1(:,:),hess(:,:)
real(kind=dpr) M(:,:),geom(:,:,:),c_E(:),intgeom(:,:),gradE_q(:,:)
real(kind=dpr) scons(:),tang(:,:),gradE(:,:),dqE(:,:),wvr1(:)
real(kind=dpr) wsr1,wsr2,wbm1(:,:),obm(:,:,:),wvr2(:)
real(kind=dpr) conhess(:,:),gradspr(:,:),dqspr(:,:)
real(kind=dpr) dq(:,:),bkdq(:,:),wdq(:,:),bkintgeom(:,:),wintgeom(:,:)
real(kind=dpr) allbm(:,:,:,:),wgeom(:,:),distg(:),Z(:,:)
integer i,j,k,l,ii,it,lines,filestat,wi1,wi2,wi3,wi4,du1
integer optdim,jj,mapdiis(:,:)
character*1 n,t,S(:,:)
character*5 version
character*40 fname1,fname2,fl,img1(:),img2(:),wimg1(:),wimg2(:)
allocatable Gn12,Gp12,bm,wmr1,hess,M,geom,Z,S,c_E,intgeom
allocatable gradE_q,scons,gradE,tang,dq,wvr1,wbm1
allocatable obm,wvr2,conhess,gradspr,dqE,dqspr,allbm,wgeom
allocatable distg,img1,img2,wimg1,wimg2,bkdq,wdq,bkintgeom,wintgeom
allocatable mapdiis

! Unit .......................................... input ..................... working               
! Gp12 ...... =G^(+1/2) ......................... [(A|rad/(sqrt(amu)*A)] .... [(A|rad/(sqrt(amu)*A)]
! Gn12 ...... =G^(-1/2) ......................... [(sqrt(amu)*A)/(A|rad)] ... [(sqrt(amu)*A)/(A|rad)]
! bm ........ array of bmatrices ................ [A|rad/A] ................. [A|rad/A]
! hess ...... hessian mw ........................ [aJ/(A|rad)^2] ............ [aJ/(A^2*amu)]
! geom ...... cart. geometry .................... [a.u.] .................... [A*sqrt(amu)]
! M ......... atomic mass ....................... [amu] ..................... [amu]
! c_E ....... array of energies ................. [a.u.] .................... [aJ]
! intgeom ... internal geometry ................. [A|rad] ................... [A|rad]
! gradE_q ... energy gradient (also gradE) ...... [aJ/(A|rad)] .............. [aJ/(A*sqrt(amu))]
! scons ..... array of spring constants ......... [aJ/(A^2*amu)] ............ [aJ/(A^2*amu)]
! allbm ..... all bmatrices ..................... [A|rad/A] ................. [A|rad/A]

! mapdiis ... selected img. if nofidiis.ne.optdim 
! img1 ...... dir name "CALC.c""img1"".d""img2"
! img2 ...... dir name "CALC.c""img1"".d""img2"
! wimg1 ..... like img1
! wimg2 ..... like img2

! tang ...... array of tangents ................. [1]
! dqE ....... relaxation from energy gradient ... [A*sqrt(amu)]
! gradspr ... projected spring gradient ......... [aJ/(A*sqrt(amu))] 
! dqspr ..... relaxation from spring gradient ... [A*sqrt(amu)]
! dq ........ =dqE+dqspr; total relaxation ...... [A*sqrt(amu)] 
! distg ..... cartesian dist. of images ......... [A*sqrt(amu)] 

version='2.3.0'
filestat=0

open(unit=1,file='NEB.log',form='formatted',iostat=filestat)
write(1,fmt='(A)',iostat=filestat)''
if (filestat > 0) call error_sub("Problem writing NEB.log in program NEB.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: NEB.x version ',version
write(1,fmt='(A)')''

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Read Input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program NEB.x ",2)
  read(20, NML= geninp)
close(20)

call def_nebpar(optdim,licneb,adjneb,initneb,doneb,maxit,nprocneb,&
               &matfact,tangtype,nebtype,startci,startdiis,maxgdiis,maxddiis,&
               &nofidiis,bmupd,stepres,scmin,scmax,conv_grad,conv_disp,cipoints)

open(unit=20,file='nebpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No nebpar file for program NEB.x ",2)
  read(20, NML= nebpar)
close(20)

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No curr_iter file for program NEB.x ",2)
  read(9,fmt=*)it
close(9)

if (coordprog.eq.'cart') then
  call cartneb()
else

allocate(img1(0:optdim+1))
allocate(img2(0:optdim+1))
allocate(wimg1(nofidiis))
allocate(wimg2(nofidiis))
allocate(hess(optdim*nintc,optdim*nintc))
allocate(conhess(optdim*nintc,optdim*nintc))
allocate(wmr1(nintc,nintc))
allocate(Gn12(nintc,nintc))
allocate(Gp12(nintc,nintc))
allocate(bm(0:optdim+1,3*nat,nintc))
allocate(c_E(0:optdim+1))
allocate(M(0:optdim+1,nat))
allocate(S(0:optdim+1,nat))
allocate(Z(0:optdim+1,nat))
allocate(geom(0:optdim+1,nat,3))
allocate(intgeom(0:optdim+1,nintc))
allocate(bkintgeom(0:optdim+1,nintc))
allocate(wintgeom(nofidiis,nintc))
allocate(gradE_q(0:optdim+1,nintc))
allocate(scons(optdim))
allocate(gradE(optdim,nintc))
allocate(gradspr(optdim,nintc))
allocate(tang(optdim,nintc))
allocate(dq(optdim,nintc))
allocate(bkdq(optdim,nintc))
allocate(wdq(nofidiis,nintc))
allocate(dqE(optdim,nintc))
allocate(dqspr(optdim,nintc))
allocate(wbm1(3*nat,nintc))
allocate(obm(0:optdim+1,3*nat,nintc))
allocate(allbm(it,0:optdim+1,3*nat,nintc))
allocate(wgeom(nat,3))
allocate(distg(optdim+1))
allocate(mapdiis(optdim,nofidiis))

open(unit=30,file='displfl',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No displfl file for program NEB.x ",2)
do i=1,3
  read(30,*,iostat=filestat)
enddo

do i=0,(optdim+1)
  read(30,fmt=*,iostat=filestat)img1(i),img2(i)
enddo
close(30)

do i=0,(optdim+1)
  fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))

  fname1='curr_bmatrix.'//fname2
  open(unit=30,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_bmatrix.* file for program NEB.x ",2)
    do j=1,nat
      do ii=1,3 ! iteration over x,y,z
        du1=nintc/3
        do l=1,du1
          read(30,fmt=*)(bm(i,(j-1)*3+ii,((l-1)*3+k)),k=1,3,1)
        enddo
      enddo
    enddo
  close(30)

  fname1='curr_energy.'//fname2
  open(unit=11,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_energy.* file for program NEB.x ",2)
  read(11,fmt=*)c_E(i)
  close(11)

  fname1='curr_geom.'//fname2
  open(unit=10,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_geom.* file for program NEB.x ",2)
  do j=1,nat
    read(10,fmt=*)S(i,j),Z(i,j),geom(i,j,1),geom(i,j,2),geom(i,j,3),M(i,j)
  enddo
  close(10)

  fname1='curr_intgeom.'//fname2
  open(unit=15,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_intgeom.* file for program NEB.x ",2)
  do j=1,nintc
    read(15,fmt=*)intgeom(i,j)
  enddo
  close(15)


  fname1='curr_intgrd.'//fname2
  open(unit=14,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_intgrd.* file for program NEB.x ",2)
  do j=1,nintc
    read(14,fmt=*)gradE_q(i,j)
  enddo
  close(14)

  if (it.gt.1) then
    fname1='first_bmatrix.'//fname2
    open(unit=30,file=fname1,status='old',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("No first_bmatrix.* file for program NEB.x ",2)
      do j=1,nat
        do ii=1,3 ! iteration over x,y,z
          du1=nintc/3
          do l=1,du1
            read(30,fmt=*)(obm(i,(j-1)*3+ii,((l-1)*3+k)),k=1,3,1)
          enddo
        enddo
      enddo
    close(30)

    fname1='bmatrix_all.'//fname2
    open(unit=30,file=fname1,status='old',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("No bmatrix_all.* file for program NEB.x ",2)
    do jj=1,it
        do j=1,nat
          do ii=1,3 ! iteration over x,y,z
            du1=nintc/3
            do l=1,du1
              read(30,fmt=*)(allbm(jj,i,(j-1)*3+ii,((l-1)*3+k)),k=1,3,1)
            enddo
          enddo
        enddo
      read(30,fmt=*) 
    enddo
    close(30)
  endif
enddo

if (it.gt.1) then
  do i=0,optdim+1
    wsr1=0.0_dpr
    wbm1=bm(i,:,:)-obm(i,:,:)
    do j=1,3*nat
      do k=1,nintc
        wsr1=wsr1+wbm1(j,k)**2
      enddo
    enddo
  enddo

  i=int(it/bmupd)
  i=i*bmupd
  write(1,fmt='(A,I10)')'Using bmatrix of step ',i
  write(1,fmt='(A)')''
  bm=allbm(i,:,:,:)
else
  obm=bm
endif

open(unit=27,file='see_energy3D.dat',status='unknown',position='append',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing see_energy3D.dat in program NEB.x ",2)
do i=0,(optdim+1)
  write(27,fmt='(I8,X,I8,X,E23.14E3,X)')it,i,c_E(i)
enddo
close(27)

! convert energies to aJ
do i=0,(optdim+1)
  c_E(i)=c_E(i)*au2aj
enddo

open(unit=13,file='mwhessian',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No mwhessian file for program NEB.x ",2)
do i=1,optdim*nintc
  read(13,fmt=*)(hess(i,j),j=1,optdim*nintc)
enddo
close(13)

if (chess.eqv..true.)  then
  open(unit=13,file='mwconhess',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No mwconhess file for program NEB.x ",2)
  do i=1,optdim*nintc
    read(13,fmt=*)(conhess(i,j),j=1,optdim*nintc)
  enddo
  close(13)
endif

if (printl .ge. 1) then
  open(unit=27,file='see_energy.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_energy.dat in program NEB.x ",2)
  wsr1=min(c_E(0),c_E(optdim+1))
  do i=0,(optdim)
    write(27,fmt='(F8.4,X)',advance='no')((c_E(i)-wsr1)*au2ev)/au2aj
  enddo
  i=optdim+1
  write(27,fmt='(F8.4,X)',advance='yes')((c_E(i)-wsr1)*au2ev)/au2aj
  close(27)

  open(unit=27,file='see_energy3D_eV.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_energy3D_eV.dat in program NEB.x ",2)
  do i=0,(optdim+1)
    write(27,fmt='(I8,X,I8,X,E23.14E3,X)')it,i,(((c_E(i)-wsr1)*au2ev)/au2aj)
  enddo
  close(27)
endif

open(unit=27,file='energy.dat',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing energy.dat in program NEB.x ",2)
do i=0,(optdim)
  write(27,fmt='(F12.6,X)',advance='no')c_E(i)
enddo
write(27,fmt='(F12.6,X)',advance='yes')c_E(optdim+1)
close(27)

if (printl.ge.1) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Input data to NEB.x:'
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian geometry [a.u. resp. amu]:'
    do i=1,nat
      write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(ii,i),Z(ii,i),&
           &geom(ii,i,1),geom(ii,i,2),geom(ii,i,3),M(ii,i)
    enddo
    write(1,fmt='(A)')''

    write(1,fmt='(A)')''
    if (printl.ge.2) then
      write(1,fmt='(A)')'Energy / aJ '
      write(1,fmt=outfmt)c_E(ii)
      write(1,fmt='(A)')''
      write(1,fmt='(A)')'Internal geometry / A|rad'
      do i=1,(nintc-1)
        write(1,fmt=outfmt)intgeom(ii,i)
      enddo
      write(1,fmt=outfmt)intgeom(ii,nintc)
      write(1,fmt='(A)')''
      write(1,fmt='(A)')'Internal gradient / aJ*A|rad^-1'
      do i=1,(nintc-1)
        write(1,fmt=outfmt)gradE_q(ii,i)
      enddo
      write(1,fmt=outfmt)gradE_q(ii,nintc)
      write(1,fmt='(A)')''
    endif
  enddo
endif

!from a.u. to A
geom=geom*au2ang

if (printl.ge.1) then
  ! create molden files:
  ! of each point
  do i=1,optdim
    fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))
    fname1='dyn_'//trim(adjustl(fname2))//'.mld'
    open(unit=15,file=fname1,status='unknown',position='append',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("Problem writing dyn_* in program NEB.x ",2)
    write(15,fmt='(I6)')nat
    write(15,fmt='(A)')''
    do j=1,nat
      write(15,fmt='(A2)',advance='no')S(i,j)
      write(15,fmt='(3(3X,F11.8))')(geom(i,j,k),k=1,3)
    enddo
    close(15)
  enddo

  ! of each chain (iteration)
  write(fl,fmt='(I8)')it
  read(fl,fmt='(A)')fname2
  fname1='dyn_'//trim(adjustl(fname2))//'.mld'
  open(unit=15,file=fname1,status='new',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing dyn_* in program NEB.x ",2)
  do i=0,optdim+1
    write(15,fmt='(I6)')nat
    write(15,fmt='(A)')''
    do j=1,nat
      write(15,fmt='(A2)',advance='no')S(i,j)
      write(15,fmt='(3(3X,F11.8))')(geom(i,j,k),k=1,3)
    enddo
  enddo
  close(15)
endif

! check distances of images
do i=0,optdim+1
  do j=1,nat
    ! geometries: from [A] to [sqrt(amu)*A]
    geom(i,j,:)=geom(i,j,:)*dsqrt(M(i,j))
  enddo
enddo 

do i=1,optdim+1
 wgeom=geom(i,:,:)-geom(i-1,:,:)
 distg(i)=col_norm(wgeom,nat)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_cartmwdist.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_cartmwdist.dat in program NEB.x ",2)
  do i=1,optdim
    write(27,fmt='(F10.7,X)',advance='no')distg(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')distg(optdim+1)
  close(27)
endif

open(unit=27,file='cartmwdist.dat',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing cartmwdist.dat in program NEB.x ",2)
do i=1,optdim
  write(27,fmt='(F10.7,X)',advance='no')distg(i)
enddo
write(27,fmt='(F10.7,X)',advance='yes')distg(optdim+1)
close(27)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! NEB
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

allocate(wvr1(nintc*optdim))
do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=gradE_q(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_trueEgrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_trueEgrad.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)
endif

do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=intgeom(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_intgeom.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_intgeom.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)
endif

deallocate(wvr1)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Mass weighting
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (consttyp .eq. 1) then
  ! mass weight things ...
  do i=0,(optdim+1)
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    call mwgrad(gradE_q(i,:),Gp12,nintc)
  enddo

  do i=1,optdim
    do ii=1,nintc
      do j=1,nintc
        wmr1(ii,j)=hess((i-1)*nintc+ii,(i-1)*nintc+j)
      enddo
    enddo
    wbm1=bm(i,:,:)
    call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
    call mwhess(wmr1,Gp12,nintc)
    do ii=1,nintc
      do j=1,nintc
        hess((i-1)*nintc+ii,(i-1)*nintc+j)=wmr1(ii,j)
      enddo
    enddo
  enddo
endif

if ((consttyp .eq. 1).and.(printl.ge.2)) then
  write(1,fmt='(A)')'DEBUG print: after massweigthing'
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Energy / aJ'
    write(1,fmt=outfmt)c_E(ii)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Internal geometry A|rad'
    do i=1,(nintc-1)
      write(1,fmt=outfmt)intgeom(ii,i)
    enddo
    write(1,fmt=outfmt)intgeom(ii,nintc)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Internal gradient aJ*(A*sqrt(amu)^-1)'
    do i=1,(nintc-1)
      write(1,fmt=outfmt)gradE_q(ii,i)
    enddo
    write(1,fmt=outfmt)gradE_q(ii,nintc)
    write(1,fmt='(A)')''
  enddo
  write(1,fmt='(A)')'Hessian :'
    do i=1,optdim*nintc
      do j=1,(optdim*nintc-1)
        write(1,fmt='(F10.4)',advance='no')hess(i,j)
      enddo
      write(1,fmt='(F10.4)',advance='yes')hess(i,optdim*nintc)
    enddo
  write(1,fmt='(A)')''
endif

allocate(wvr1(nintc*optdim))

do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=gradE_q(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_Egrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_Egrad.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)  
endif

deallocate(wvr1)

! get the tangents and spring constants
call tangcons(c_E,intgeom,optdim,scons,tang,bm,M,scmin,scmax,nintc,nat,tangtype,printl,consttyp)

if (printl.ge.2) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Tangents and spring constants:'
  write(1,fmt='(A)')''
  do ii=1,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Spring constant / aJ*(A^-2*amu^-1) '
    write(1,fmt=outfmt)scons(ii)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Tangent '
    do i=1,(nintc-1)
      write(1,fmt=outfmt)tang(ii,i)
    enddo
    write(1,fmt=outfmt)tang(ii,nintc)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_springcons.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_springcons.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')scons(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')scons(optdim)
  close(27)
endif

!copy the tangent for DEBUG print
allocate(wvr1(optdim*nintc))
do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=tang(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_tang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_tang.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)
endif

deallocate(wvr1)

! adjust the gradient for NEB
call adjgrd(intgeom,scons,gradE_q,tang,gradE,gradspr,c_E,optdim,bm,M,nintc,nat,tangtype,printl,startci,nebtype,consttyp,cipoints)

if (printl.ge.2) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adjusted gradients'
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Energy gradient / aJ*A^-1*sqrt(amu)^-1'
    do i=1,(nintc-1)
      write(1,fmt=outfmt)gradE_q(ii,i)
    enddo
    write(1,fmt=outfmt)gradE_q(ii,nintc)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Spring gradient'
    do i=1,(nintc-1)
      write(1,fmt=outfmt)gradspr(ii,i)
    enddo
    write(1,fmt=outfmt)gradspr(ii,nintc)
    write(1,fmt='(A)')''

  enddo
endif

if (chess.eqv..true.)  then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adding hessian of springs to hessian'
  write(1,fmt='(A)')''
  hess=hess+conhess
endif

! calculate the step

! ELECTRONIC PART
!Standard relaxation
call relax(hess,gradE,dqE,optdim,nintc)

!SPRING PART
call relax(hess,gradspr,dqspr,optdim,nintc)

allocate(wvr1(optdim))

do i=1,optdim
  wvr1(i)=sp(gradE_q(i,:),tang(i,:),nintc)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_spgradEtang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spgradEtang.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim)
  close(27)
endif

do i=1,optdim
  wvr1(i)=sp(dqspr(i,:),tang(i,:),nintc)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_spdqsprtang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spdqsprtang.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim)
  close(27)
endif

do i=1,optdim
  wvr1(i)=sp(dqE(i,:),tang(i,:),nintc)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_spdqEtang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spdqEtang.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim)
  close(27)
endif

do i=1,optdim
  wvr1(i)=norm(dqE(i,:),nintc)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_normdqE.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_normdqE.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim)
  close(27)
endif

do i=1,optdim
  wvr1(i)=norm(dqspr(i,:),nintc)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_normdqspr.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_normdqspr.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim)
  close(27)
endif

deallocate(wvr1)

dq=dqE+dqspr

! write out things for hessian update
! copy gradient
allocate(wvr1(nintc*optdim))

do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=dqE(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_dqE.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_dqE.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)  
endif

do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=dqspr(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_dqspr.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_dqspr.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)  
endif

do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=gradE(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_projEgrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_projEgrad.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)  
endif

do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=gradE(i,j)+gradspr(i,j)
  enddo
enddo

open(unit=40,file='grad',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing grad in program NEB.x ",2)
do i=1,optdim*nintc
  write(40,fmt=outfmt)wvr1(i)
enddo
close(40)

if (printl .ge. 2) then
  open(unit=27,file='see_grad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_grad.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)  
endif

do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=dqE(i,j)+dqspr(i,j)
  enddo
enddo

open(unit=40,file='dq',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing dq in program NEB.x ",2)
do i=1,optdim*nintc
  write(40,fmt=outfmt)wvr1(i)
enddo
close(40)

if (printl .ge. 2) then
  open(unit=27,file='see_displ.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_displ.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)
endif

deallocate(wvr1)

! check for convergence
call check_nebconv(gradE_q,conv_grad,conv_disp,M,optdim,dq,img1,img2,nintc,nat)

! unweight displacement
allocate(wvr1(nintc))
do i=1,optdim
  wbm1=bm(i,:,:)
  call getGmatrices(wbm1,M(i,:),Gn12,Gp12,nintc,nat)
  if (consttyp .eq. 1) then
    call uwdisp(wvr1,dq(i,:),Gp12,nintc)
    dq(i,:)=wvr1
  endif
enddo
deallocate(wvr1)

! step restriction
write(1,fmt='(A)')''
write(1,fmt='(A)')'Checking for step restriction on components individually'
write(1,fmt='(A)')''

do i=1,optdim
  do j=1,nintc
    if (dabs(dq(i,j)).gt.stepres) then
      write(1,fmt='(A,I10,A,I10)')'Restriction on image ',i,' component ',j
      dq(i,j)=(stepres/dabs(dq(i,j)))*dq(i,j)
    endif
  enddo
enddo

allocate(wvr1(nintc*optdim))
do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=dq(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_uwdispl.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_uwdispl.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)
endif

deallocate(wvr1)

do i=1,optdim
  fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))
  fname1='uwdq.'//fname2
  open(unit=15,file=fname1,status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing uwdq.* in program NEB.x ",2)
  do j=1,nintc
    write(15,fmt=outfmt)dq(i,j)
  enddo
  close(15)
enddo

if ((it.lt.startdiis).or.(maxgdiis.le.1)) then
  ! displace geometries
  do i=1,optdim
    intgeom(i,:)=intgeom(i,:)+dq(i,:)
  enddo
elseif (nofidiis.eq.optdim) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Performing gdiis global'
  write(1,fmt='(A)')''
  call gdiisdq_neb_total(intgeom,dq,optdim,img1,img2,nintc)
else
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Performing gdiis local'
  write(1,fmt='(A)')''
  !backup step
  bkdq=dq
  bkintgeom=intgeom
  do i=1,optdim
    wi1=(nofidiis-1)/2
    write(1,fmt='(A)')'start creating mapping vector'
    do j=1,nofidiis
      !create mapping vector
      mapdiis(i,j)=i-wi1-1+j
    enddo
    !check mapping vector and move it if necessary
    ! shift
    wi3=0
    if ((mapdiis(i,1).lt.1).and.(mapdiis(i,nofidiis).gt.optdim)) then
      call error_sub("More images for diis procedure requested than available",2)
    elseif (mapdiis(i,1).lt.1) then
      ! beginning of the chain
      wi3=1-mapdiis(i,1)
      do j=1,nofidiis
        mapdiis(i,j)=mapdiis(i,j)+wi3
      enddo
    elseif (mapdiis(i,nofidiis).gt.optdim) then
      wi3=-(mapdiis(i,nofidiis)-optdim)
      do j=1,nofidiis
        mapdiis(i,j)=mapdiis(i,j)+wi3
      enddo
    endif
    wi2=1
    ! copy now the requested images
    do j=1,nofidiis
      wdq(j,:)=bkdq(mapdiis(i,j),:)
      wimg1(j)=img1(mapdiis(i,j))
      wimg2(j)=img2(mapdiis(i,j))
      wintgeom(j,:)=bkintgeom(mapdiis(i,j),:)
    enddo 
    write(1,fmt='(A)')'Starting diis'
    ! do diis 
    call gdiisdq_neb_flex(wintgeom,wdq,nofidiis,wimg1,wimg2,nintc)
    intgeom(i,:)=wintgeom(wi1-wi3+1,:)
  enddo
endif

allocate(wvr1(nintc*optdim))
do i=1,optdim
  do j=1,nintc
    wvr1((i-1)*nintc+j)=dq(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_gdiisuwdispl.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_gdiisuwdispl.dat in program NEB.x ",2)
  do i=1,(optdim*nintc-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*nintc)
  close(27)
endif

deallocate(wvr1)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write results
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do i=1,optdim
  fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))
  fname1='new_intgeom.'//fname2
  open(unit=15,file=fname1,status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing new_intgeom.* in program NEB.x ",2)
  do j=1,nintc
    write(15,fmt=outfmt)intgeom(i,j)
  enddo
  close(15)
enddo

deallocate(img1)
deallocate(img2)
deallocate(wimg1)
deallocate(wimg2)
deallocate(hess)
deallocate(conhess)
deallocate(wmr1)
deallocate(Gn12)
deallocate(Gp12)
deallocate(bm)
deallocate(c_E)
deallocate(M)
deallocate(S)
deallocate(Z)
deallocate(geom)
deallocate(intgeom)
deallocate(bkintgeom)
deallocate(wintgeom)
deallocate(gradE_q)
deallocate(scons)
deallocate(gradE)
deallocate(gradspr)
deallocate(tang)
deallocate(dq)
deallocate(bkdq)
deallocate(wdq)
deallocate(dqE)
deallocate(dqspr)
deallocate(wbm1)
deallocate(obm)
deallocate(allbm)
deallocate(wgeom)
deallocate(distg)
deallocate(mapdiis)

write(1,fmt='(A)')'Program finished: NEB.x with SUCCESS'

close(1)

endif ! if coordprog.eq.'cart'

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Subroutines
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine steepes_desc(gradient,displ,optdim)
! steepest descent relaxation
! dx=-a*grad
! with a=1 and [a]=[(A^2*amu)/aJ]

use prec_mod
use units_mod
use general_mod

implicit none
integer optdim
real(kind=dpr) gradient(optdim,nintc),displ(optdim,nintc)

! gradient ... gradient ... [aJ/(A*sqrt(amu)]
! displ ...... displacement ... [(A*sqrt(amu)]

do i=1,optdim
  displ(i,:)=-gradient(i,:)
enddo

end subroutine

subroutine gdiisdq_neb_total_cart(newintg,dq,optdim,img1,img2,imdim,maxgdiis,maxddiis)
! makes the diis interpolation with all dq at once 
! for relaxation in cartesian coordinates

use prec_mod
use units_mod
use general_mod

implicit none
integer optdim,imdim,maxgdiis
real(kind=dpr) newintg(0:optdim+1,imdim)
real(kind=dpr) dq(optdim,imdim),maxddiis
real(kind=dpr) alldq(:,:),allintg(:,:)
real(kind=dpr) ws1,wv2(imdim),wvr2(imdim)
real(kind=dpr) wvr1(:,:),amat(:,:),usedq(:,:),useig(:,:),work(:),zd,md
integer i,ii,j,k,l,filestat,points,du1,wvi1(:),dimgd
integer ipiv(:),lwork,info,jj
allocatable allintg,wvi1,alldq,wvr1,amat,usedq
allocatable useig,ipiv,work
character*1 U,sd
character*40 img1(0:optdim+1),img2(0:optdim+1)

! Unit ............................................... input ... working 
! alldq ...... all displacements ..................... [A] ..... [A]
! allintg .... geometries ............................ [a.u.] .. [A]
! dq ......... current step .......................... [A]
!
! optdim ..... N° of images to opt.
! imdim ...... dimension of 1 image
! img1 ....... dir name "CALC.c""img1"".d""img2"
! img2 ....... dir name "CALC.c""img1"".d""img2"
! maxgdiis ... max N° of images
! maxddiis ... max allowed displacement .............. [A] 
!
! output
! dq      ... interpolated step; overwritten ......... [A]
! newintg ... new internal coordinate; overwritten ... [A]

fname2='c'//trim(adjustl(img1(1)))//'.d'//trim(adjustl(img2(1)))

points=0
filestat = 0

fname1='geom_all.'//fname2

open(unit=103,file=fname1,status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No geom_all.* file in sub gdiisdq_neb_total_cart",2)
do while (filestat == 0)
  read(103,*,iostat=filestat)
  if (filestat /= 0) exit
    points=points+1
  enddo
close(103)

points=points/(imdim/3+1)

allocate(allintg(points,optdim*imdim))
allocate(alldq(points,optdim*imdim))
allocate(wvi1(points))

do jj=1,optdim
  fname2='c'//trim(adjustl(img1(jj)))//'.d'//trim(adjustl(img2(jj)))
  ! read data
  fname1='geom_all.'//fname2
  open(unit=103,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No geom_all.* file in sub gdiisdq_neb_total_cart",2)

  do i=1,points
    do j=1,imdim/3
      read(103,fmt=*)sd,zd,allintg(i,(jj-1)*imdim+(j-1)*3+1),allintg(i,(jj-1)*imdim+(j-1)*3+2),&
                     &allintg(i,(jj-1)*imdim+(j-1)*3+3),md
    enddo
    read(103,fmt=*)
  enddo
  close(103)
  fname1='uwdq_all.'//fname2
  open(unit=104,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No uwdq_all.* file in sub gdiisdq_neb_total_cart",2)
  do i=1,(points-1)
    do j=1,imdim
      read(104,fmt=*)alldq(i,(jj-1)*imdim+j)
    enddo
    read(104,fmt=*)
  enddo
  close(104)
  do j=1,imdim
    alldq(points,(jj-1)*imdim+j)=dq(jj,j)
  enddo
enddo !images

!convert geometry from a.u. to A
allintg=allintg*au2ang

if (printl .ge. 2) then
  do jj=1,optdim
    write(1,fmt='(A,I8)')'optdim ',jj
    do j=1,points
      write(1,fmt='(A,I8)')'points ',j
      do i=1,imdim/3-1
        write(1,fmt='(3(X,F14.8))')allintg(j,(jj-1)*imdim+(i-1)*3+1),allintg(j,(jj-1)*imdim+(i-1)*3+2),&
        &allintg(j,(jj-1)*imdim+(i-1)*3+3)
      enddo
      write(1,fmt='(3(X,F14.8))')allintg(j,(optdim-1)*imdim+(imdim/3-1)*3+1),&
      &allintg(j,(optdim-1)*imdim+(imdim/3-1)*3+2),allintg(j,(optdim-1)*imdim+(imdim/3-1)*3+3)
      write(1,fmt='(A)')'' 
      write(1,fmt='(A,I8)')'alldq ',j
      do i=1,imdim/3-1
        write(1,fmt='(3(X,F14.8))')alldq(j,(jj-1)*imdim+(i-1)*3+1),&
        &alldq(j,(jj-1)*imdim+(i-1)*3+2),alldq(j,(jj-1)*imdim+(i-1)*3+3)
      enddo
      write(1,fmt='(3(X,F14.8))')alldq(j,(optdim-1)*imdim+(imdim/3-1)*3+1),&
      &alldq(j,(optdim-1)*imdim+(imdim/3-1)*3+2),alldq(j,(optdim-1)*imdim+(imdim/3-1)*3+3)
      write(1,fmt='(A)')'' 
    enddo
  enddo
endif

! make selection based on norm of displacements
wvi1=0
do i=1,points
  ws1=maxval(alldq(i,:))
  if (ws1.lt.maxddiis) then
    wvi1(i)=1
  endif
enddo

! Error check
if (wvi1(points).ne.1) then
  call error_sub("Current step rejected in the diis procedure. Check step restriction",2)
endif

!restrict selection to a maximum number
if (sum(wvi1).gt.maxgdiis) then
  j=0
  do i=points,1,-1
    if (wvi1(i).eq.1) then
      j=j+1
      if (j.gt.maxgdiis) then
        wvi1(i)=0
      endif
    endif
  enddo
endif

dimgd=sum(wvi1)

write(1,fmt='(A)')''
write(1,fmt='(A,I4)')'Dimension in gdiis : ',dimgd
write(1,fmt='(A)')''

allocate(usedq(dimgd,optdim*imdim))
allocate(useig(dimgd,optdim*imdim))

j=1
do i=1,points
  if (wvi1(i).eq.1) then
    usedq(j,:)=alldq(i,:)
    useig(j,:)=allintg(i,:)
    j=j+1
  endif
enddo

! Perform now the gdiis procedure
allocate(amat(dimgd+1,dimgd+1))
allocate(wvr1(dimgd+1,1))

! construct the A matrix:
do i=1,dimgd
  do j=i,dimgd
    amat(i,j)=sp(usedq(i,:),usedq(j,:),optdim*imdim)
    amat(j,i)=amat(i,j)
  enddo
enddo
amat(:,dimgd+1)=1.0_dpr
amat(dimgd+1,:)=1.0_dpr
amat(dimgd+1,dimgd+1)=0.0_dpr

! construct the work vector
wvr1=0.0_dpr
wvr1(dimgd+1,1)=1.0_dpr

info=0
U='U'
! Solve the linear equation system
!get the factorization
allocate(ipiv(dimgd+1))
allocate(work(dimgd+1))
call DSYTRF('U',dimgd+1,amat,dimgd+1,ipiv,work,-1,info)
if (info.ne.0) call error_sub("Problem with DSYTRF in sub gdiisdq_neb_total_cart",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DSYTRF('U',dimgd+1,amat,dimgd+1,ipiv,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DSYTRF in sub gdiisdq_neb_total_cart",2)
deallocate(work)
! get the solution
call DSYTRS('U',dimgd+1,1,amat,dimgd+1,ipiv,wvr1,dimgd+1,info)
if (info.ne.0) call error_sub("Problem with DSYTRS in sub gdiisdq_neb_total_cart",2)
deallocate(ipiv)
!wvr1 contains the coefficients and as a last entry the langrangian multiplier

do jj=1,optdim
  ! now overwrite newintg and dq with the linear combinations
  newintg(jj,:)=0.0_dpr
  dq(jj,:)=0.0_dpr

  do i=1,dimgd
    do j=1,imdim
      dq(jj,j)=dq(jj,j)+wvr1(i,1)*usedq(i,(jj-1)*imdim+j)
      newintg(jj,j)=newintg(jj,j)+wvr1(i,1)*useig(i,(jj-1)*imdim+j)
    enddo
  enddo

  ! Make the relaxation
  newintg(jj,:)=newintg(jj,:)+dq(jj,:)
enddo !images

deallocate(allintg)
deallocate(alldq)
deallocate(wvi1)
deallocate(amat)
deallocate(wvr1)
deallocate(usedq)
deallocate(useig)

end subroutine


subroutine gdiisdq_neb_total(newintg,dq,optdim,img1,img2,nintc)
! Makes the diis interpolation. To be used with
! internal coordinates if the whole band is used. 

! maxdiis should be changed because of unit !! XXX

use prec_mod
use units_mod
use general_mod

implicit none
integer optdim,nintc
real(kind=dpr) newintg(0:optdim+1,nintc)
real(kind=dpr) dq(optdim,nintc)
real(kind=dpr) alldq(:,:),allintg(:,:)
real(kind=dpr) ws1,wv2(nintc),wvr2(nintc)
real(kind=dpr) wvr1(:,:),amat(:,:),usedq(:,:),useig(:,:),work(:)
integer i,ii,j,k,l,filestat,points,du1,wvi1(:),dimgd
integer ipiv(:),lwork,info,jj
allocatable allintg,wvi1,alldq,wvr1,amat,usedq
allocatable useig,ipiv,work
character*1 U
character*40 img1(0:optdim+1),img2(0:optdim+1)

! Unit ............................................... input ... working 
! alldq ...... all displacements ..................... [A|rad] ..... [A|rad]
! allintg .... geometries ............................ [A|rad] ..... [A|rad]
! dq ......... current step .......................... [A|rad]
!
! optdim ..... N° of images to opt.
! nintc ...... dimension of 1 image
! img1 ....... dir name "CALC.c""img1"".d""img2"
! img2 ....... dir name "CALC.c""img1"".d""img2"
! maxgdiis ... max N° of images
! maxddiis ... max allowed displacement............... [A|rad] 
!
! output
! dq      ... interpolated step; overwritten ......... [A|rad]
! newintg ... new internal coordinate; overwritten ... [A|rad]

fname2='c'//trim(adjustl(img1(1)))//'.d'//trim(adjustl(img2(1)))

points=0
filestat = 0

fname1='intgeom_all.'//fname2

open(unit=103,file=fname1,status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No intgeom_all.* file in sub gdiisdq_neb_total",2)
do while (filestat == 0)
  read(103,*,iostat=filestat)
  if (filestat /= 0) exit
    points=points+1
  enddo
close(103)

points=points/(nintc+1)

allocate(allintg(points,optdim*nintc))
allocate(alldq(points,optdim*nintc))
allocate(wvi1(points))

do jj=1,optdim
  fname2='c'//trim(adjustl(img1(jj)))//'.d'//trim(adjustl(img2(jj)))

  ! read data
  fname1='intgeom_all.'//fname2
  open(unit=103,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No intgeom_all.* file in sub gdiisdq_neb_total",2)
  do i=1,points
    do j=1,nintc
      read(103,fmt=*)allintg(i,(jj-1)*nintc+j)
    enddo
    read(103,fmt=*)
  enddo
  close(103)

  fname1='uwdq_all.'//fname2
  open(unit=104,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No uwdq_all.* file in sub gdiisdq_neb_total",2)
  do i=1,(points-1)
    do j=1,nintc
      read(104,fmt=*)alldq(i,(jj-1)*nintc+j)
    enddo
    read(104,fmt=*)
  enddo
  close(104)

  do j=1,nintc
    alldq(points,(jj-1)*nintc+j)=dq(jj,j)
  enddo
enddo !images

! make selection based on norm of displacements
wvi1=0
do i=1,points
  ws1=maxval(alldq(i,:))
  if (ws1.lt.maxddiis) then
    wvi1(i)=1
  endif
enddo

! Error check
if (wvi1(points).ne.1) then
  call error_sub("Current step rejected in the diis procedure. Check step restriction",2)
endif


!restrict selection to a maximum number
if (sum(wvi1).gt.maxgdiis) then
  j=0
  do i=points,1,-1
    if (wvi1(i).eq.1) then
      j=j+1
      if (j.gt.maxgdiis) then
        wvi1(i)=0
      endif
    endif
  enddo
endif

dimgd=sum(wvi1)

write(1,fmt='(A)')''
write(1,fmt='(A,I4)')'Dimension in gdiis : ',dimgd
write(1,fmt='(A)')''

allocate(usedq(dimgd,optdim*nintc))
allocate(useig(dimgd,optdim*nintc))

j=1
do i=1,points
  if (wvi1(i).eq.1) then
    usedq(j,:)=alldq(i,:)
    useig(j,:)=allintg(i,:)
    j=j+1
  endif
enddo

! Perform now the gdiis procedure
allocate(amat(dimgd+1,dimgd+1))
allocate(wvr1(dimgd+1,1))

! construct the A matrix:
do i=1,dimgd
  do j=i,dimgd
    amat(i,j)=sp(usedq(i,:),usedq(j,:),optdim*nintc)
    amat(j,i)=amat(i,j)
  enddo
enddo
amat(:,dimgd+1)=1.0_dpr
amat(dimgd+1,:)=1.0_dpr
amat(dimgd+1,dimgd+1)=0.0_dpr

! construct the work vector
wvr1=0.0_dpr
wvr1(dimgd+1,1)=1.0_dpr

info=0
U='U'
! Solve the linear equation system
!get the factorization
allocate(ipiv(dimgd+1))
allocate(work(dimgd+1))
call DSYTRF('U',dimgd+1,amat,dimgd+1,ipiv,work,-1,info)
if (info.ne.0) call error_sub("Problem with DSYTRF in sub gdiisdq_neb_total",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DSYTRF('U',dimgd+1,amat,dimgd+1,ipiv,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DSYTRF in sub gdiisdq_neb_total",2)
deallocate(work)
! get the solution
call DSYTRS('U',dimgd+1,1,amat,dimgd+1,ipiv,wvr1,dimgd+1,info)
if (info.ne.0) call error_sub("Problem with DSYTRS in sub gdiisdq_neb_total",2)
deallocate(ipiv)
!wvr1 contains the coefficients and as a last entry the langrangian multiplier

do jj=1,optdim
  ! now overwrite newintg and dq with the linear combinations
  newintg(jj,:)=0.0_dpr
  dq(jj,:)=0.0_dpr

  do i=1,dimgd
    do j=1,nintc
      dq(jj,j)=dq(jj,j)+wvr1(i,1)*usedq(i,(jj-1)*nintc+j)
      newintg(jj,j)=newintg(jj,j)+wvr1(i,1)*useig(i,(jj-1)*nintc+j)
    enddo
  enddo

  ! Make the relaxation
  newintg(jj,:)=newintg(jj,:)+dq(jj,:)

enddo !images

deallocate(allintg)
deallocate(alldq)
deallocate(wvi1)
deallocate(amat)
deallocate(wvr1)
deallocate(usedq)
deallocate(useig)

end subroutine


subroutine gdiisdq_neb_flex(newintg,dq,optdim,img1,img2,imdim)
! Makes the diis interpolation. To be used with
! internal coordinates if a part the whole band is used.

! maxdiis should be changed because of unit !! XXX

use prec_mod
use units_mod
use general_mod

implicit none
integer optdim,imdim
real(kind=dpr) newintg(optdim,imdim)
real(kind=dpr) dq(optdim,imdim)
real(kind=dpr) alldq(:,:),allintg(:,:)
real(kind=dpr) ws1,wv2(imdim),wvr2(imdim)
real(kind=dpr) wvr1(:,:),amat(:,:),usedq(:,:),useig(:,:),work(:)
integer i,ii,j,k,l,filestat,points,du1,wvi1(:),dimgd
integer ipiv(:),lwork,info,jj
allocatable allintg,wvi1,alldq,wvr1,amat,usedq
allocatable useig,ipiv,work
character*1 U
character*40 img1(optdim),img2(optdim)

! Unit ............................................... input ... working 
! alldq ...... all displacements ..................... [A|rad] ..... [A|rad]
! allintg .... geometries ............................ [A|rad] ..... [A|rad]
! dq ......... current step .......................... [A|rad]
!
! optdim ..... N° of images to opt.
! imdim ...... dimension of 1 image
! img1 ....... dir name "CALC.c""img1"".d""img2"
! img2 ....... dir name "CALC.c""img1"".d""img2"
! maxgdiis ... max N° of images
! maxddiis ... max allowed displacement............... [A|rad] 
!
! output
! dq      ... interpolated step; overwritten ......... [A|rad]
! newintg ... new internal coordinate; overwritten ... [A|rad]

fname2='c'//trim(adjustl(img1(1)))//'.d'//trim(adjustl(img2(1)))

points=0
filestat = 0

fname1='intgeom_all.'//fname2

open(unit=103,file=fname1,status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No intgeom_all.* file in sub gdiisdq_neb_flex",2)
do while (filestat == 0)
  read(103,*,iostat=filestat)
  if (filestat /= 0) exit
    points=points+1
  enddo
close(103)

points=points/(imdim+1)

allocate(allintg(points,optdim*imdim))
allocate(alldq(points,optdim*imdim))
allocate(wvi1(points))

do jj=1,optdim
  fname2='c'//trim(adjustl(img1(jj)))//'.d'//trim(adjustl(img2(jj)))

  ! read data
  fname1='intgeom_all.'//fname2
  open(unit=103,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No intgeom_all.* file in sub gdiisdq_neb_flex",2)

  do i=1,points
    do j=1,imdim
      read(103,fmt=*)allintg(i,(jj-1)*imdim+j)
    enddo
    read(103,fmt=*)
  enddo
  close(103)
  fname1='uwdq_all.'//fname2
  open(unit=104,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No uwdq_all.* file in sub gdiisdq_neb_flex",2)
  do i=1,(points-1)
    do j=1,imdim
      read(104,fmt=*)alldq(i,(jj-1)*imdim+j)
    enddo
    read(104,fmt=*)
  enddo
  close(104)
  do j=1,imdim
    alldq(points,(jj-1)*imdim+j)=dq(jj,j)
  enddo
enddo !images

! make selection based on norm of displacements
wvi1=0
do i=1,points
  ws1=maxval(alldq(i,:))
  if (ws1.lt.maxddiis) then
    wvi1(i)=1
  endif
enddo

! Error check
if (wvi1(points).ne.1) then
  call error_sub("Current step rejected in the diis procedure. Check step restriction",2)
endif

!restrict selection to a maximum number
if (sum(wvi1).gt.maxgdiis) then
  j=0
  do i=points,1,-1
    if (wvi1(i).eq.1) then
      j=j+1
      if (j.gt.maxgdiis) then
        wvi1(i)=0
      endif
    endif
  enddo
endif

dimgd=sum(wvi1)

write(1,fmt='(A)')''
write(1,fmt='(A,I4)')'Dimension in gdiis : ',dimgd
write(1,fmt='(A)')''

allocate(usedq(dimgd,optdim*imdim))
allocate(useig(dimgd,optdim*imdim))

j=1
do i=1,points
  if (wvi1(i).eq.1) then
    usedq(j,:)=alldq(i,:)
    useig(j,:)=allintg(i,:)
    j=j+1
  endif
enddo

! Perform now the gdiis procedure
allocate(amat(dimgd+1,dimgd+1))
allocate(wvr1(dimgd+1,1))

! construct the A matrix:
do i=1,dimgd
  do j=i,dimgd
    amat(i,j)=sp(usedq(i,:),usedq(j,:),optdim*imdim)
    amat(j,i)=amat(i,j)
  enddo
enddo
amat(:,dimgd+1)=1.0_dpr
amat(dimgd+1,:)=1.0_dpr
amat(dimgd+1,dimgd+1)=0.0_dpr

! construct the work vector
wvr1=0.0_dpr
wvr1(dimgd+1,1)=1.0_dpr

info=0
U='U'
! Solve the linear equation system
!get the factorization
allocate(ipiv(dimgd+1))
allocate(work(dimgd+1))
call DSYTRF('U',dimgd+1,amat,dimgd+1,ipiv,work,-1,info)
if (info.ne.0) call error_sub("Problem with DSYTRF in sub gdiisdq_neb_flex",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DSYTRF('U',dimgd+1,amat,dimgd+1,ipiv,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DSYTRF in sub gdiisdq_neb_flex",2)
deallocate(work)
! get the solution
call DSYTRS('U',dimgd+1,1,amat,dimgd+1,ipiv,wvr1,dimgd+1,info)
if (info.ne.0) call error_sub("Problem with DSYTRS in sub gdiisdq_neb_flex",2)
deallocate(ipiv)
!wvr1 contains the coefficients and as a last entry the langrangian multiplier

do jj=1,optdim
  ! now overwrite newintg and dq with the linear combinations
  newintg(jj,:)=0.0_dpr
  dq(jj,:)=0.0_dpr

  do i=1,dimgd
    do j=1,imdim
      dq(jj,j)=dq(jj,j)+wvr1(i,1)*usedq(i,(jj-1)*imdim+j)
      newintg(jj,j)=newintg(jj,j)+wvr1(i,1)*useig(i,(jj-1)*imdim+j)
    enddo
  enddo

  ! Make the relaxation
  newintg(jj,:)=newintg(jj,:)+dq(jj,:)
enddo !images

deallocate(allintg)
deallocate(alldq)
deallocate(wvi1)
deallocate(amat)
deallocate(wvr1)
deallocate(usedq)
deallocate(useig)

end subroutine

subroutine cartneb()
! This subroutine makes the cartesian NEB relaxation

use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) carthess(optdim*3*nat,optdim*3*nat),c_E(0:optdim+1)
real(kind=dpr) M(0:optdim+1,nat),Z(0:optdim+1,nat),workg(0:optdim+1,nat,3)
real(kind=dpr) geom(0:optdim+1,nat,3),gradE_c(0:optdim+1,3*nat),scons(optdim)
real(kind=dpr) gradE(optdim,3*nat),gradspr(optdim,3*nat),tang(optdim,3*nat)
real(kind=dpr) dq(optdim,3*nat),wgeom(nat,3),wvr1(3*nat*optdim),distg(optdim+1)
real(kind=dpr) wmr1(3*nat,3*nat),wmr2(3*nat,3*nat),wmr3(3*nat,3*nat)
real(kind=dpr) wvr2(3*nat),vgeom(0:optdim+1,3*nat),dqE(optdim,3*nat),dqspr(optdim,3*nat)
real(kind=dpr) wsr1,wsr2
integer i,j,k,lines,filestat
character*40 img1(0:optdim+1),img2(0:optdim+1)
character*1 S(0:optdim+1,nat)

! Unit .............................................. input ............ working               
! carthess ... hessian mw ........................... [aJ/(A^2)] ....... [aJ/(A^2*amu)]
! c_E ........ array of energies .................... [a.u.] ........... [aJ]
! M .......... atomic mass .......................... [amu] ............ [amu]
! geom ....... array of cart. geometry .............. [a.u.] ........... [A*sqrt(amu)]
! gradE_c .... cart. energy gradient (also gradE) ... [a.u.] ........... [aJ/(A*sqrt(amu))]
! scons ...... array of spring constants ............ [aJ/(A^2*amu)] ... [aJ/(A^2*amu)]

! mapdiis .... selected img. if nofidiis.ne.optdim 
! img1 ....... dir name "CALC.c""img1"".d""img2"
! img2 ....... dir name "CALC.c""img1"".d""img2"

! tang ....... array of tangents .................... [1]
! dqE ........ relaxation from energy gradient ...... [A*sqrt(amu)]
! gradspr .... projected spring gradient ............ [aJ/(A*sqrt(amu))] 
! dqspr ...... relaxation from spring gradient ...... [A*sqrt(amu)]
! dq ......... =dqE+dqspr; total relaxation ......... [A*sqrt(amu)] 
! distg ...... cartesian dist. of images ............ [A*sqrt(amu)]

filestat=0

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program NEB.x ",2)
  read(20, NML= geninp)
close(20)

call def_nebpar(optdim,licneb,adjneb,initneb,doneb,maxit,nprocneb,&
               &matfact,tangtype,nebtype,startci,startdiis,maxgdiis,maxddiis,&
               &nofidiis,bmupd,stepres,scmin,scmax,conv_grad,conv_disp,cipoints)

open(unit=20,file='nebpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No nebpar file for program NEB.x ",2)
  read(20, NML= nebpar)
close(20)

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No curr_iter file for program NEB.x ",2)
  read(9,fmt=*)it
close(9)

open(unit=30,file='displfl',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No displfl file for program NEB.x ",2)
do i=1,3
  read(30,*,iostat=filestat)
enddo

do i=0,(optdim+1)
  read(30,fmt=*,iostat=filestat)img1(i),img2(i)
enddo
close(30)

do i=0,(optdim+1)
  fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))

  fname1='curr_energy.'//fname2
  open(unit=11,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_energy.* file for program NEB.x ",2)
  read(11,fmt=*)c_E(i)
  close(11)

  fname1='curr_cartgrd.'//fname2
  open(unit=14,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_cartgrd.* file for program NEB.x ",2)
  do j=1,nat
    read(14,fmt=*)gradE_c(i,(j-1)*3+1),gradE_c(i,(j-1)*3+2),gradE_c(i,(j-1)*3+3)
  enddo
  close(14)

  fname1='curr_geom.'//fname2
  open(unit=16,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_geom.* file for program NEB.x ",2)
  do j=1,nat
    read(16,fmt=*)S(i,j),Z(i,j),geom(i,j,1),geom(i,j,2),geom(i,j,3),M(i,j)
  enddo
  close(16)
enddo

open(unit=27,file='see_energy3D.dat',status='unknown',position='append',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing see_energy3D.dat in program NEB.x ",2)
do i=0,(optdim+1)
  write(27,fmt='(I8,X,I8,X,E23.14E3,X)')it,i,c_E(i)
enddo
close(27)

! convert energies to aJ
do i=0,(optdim+1)
  c_E(i)=c_E(i)*au2aj
enddo

open(unit=13,file='mwhessian',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No mwhessian file for program NEB.x ",2)
do i=1,optdim*3*nat
  read(13,fmt=*)(carthess(i,j),j=1,optdim*3*nat)
enddo
close(13)

if (printl.ge.2) then
  write(1,fmt='(A)')' hess'
  do ii=1,optdim*3*nat
    do j=1,(optdim*3*nat-1)
      write(1,fmt='(F10.4)',advance='no')carthess(ii,j)
    enddo
    write(1,fmt='(F10.4)',advance='yes')carthess(ii,optdim*3*nat)
  enddo
endif

if (printl .ge. 1) then
  open(unit=27,file='see_energy.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_energy.dat in program NEB.x ",2)
  wsr1=min(c_E(0),c_E(optdim+1))
  do i=0,(optdim)
    write(27,fmt='(F8.4,X)',advance='no')((c_E(i)-wsr1)*au2ev)/au2aj
  enddo
  i=optdim+1
  write(27,fmt='(F8.4,X)',advance='yes')((c_E(i)-wsr1)*au2ev)/au2aj
  close(27)

  open(unit=27,file='see_energy3D_eV.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_energy3D_eV.dat in program NEB.x ",2)
  do i=0,(optdim+1)
    write(27,fmt='(I8,X,I8,X,E23.14E3,X)')it,i,(((c_E(i)-wsr1)*au2ev)/au2aj)
  enddo
  close(27)
endif

open(unit=27,file='energy.dat',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing energy.dat in program NEB.x ",2)
do i=0,(optdim)
  write(27,fmt='(F12.6,X)',advance='no')c_E(i)
enddo
write(27,fmt='(F12.6,X)',advance='yes')c_E(optdim+1)
close(27)

if (printl.ge.1) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Input data to NEB.x:'
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian geometry [a.u. resp. amu]:'
    do i=1,nat
      write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(ii,i),Z(ii,i),&
           &geom(ii,i,1),geom(ii,i,2),geom(ii,i,3),M(ii,i)
    enddo
    write(1,fmt='(A)')''

    write(1,fmt='(A)')''
    if (printl.ge.2) then
      write(1,fmt='(A)')'Energy / aJ '
      write(1,fmt=outfmt)c_E(ii)
      write(1,fmt='(A)')''
      write(1,fmt='(A)')'Cartesian gradient / a.u.'
      do i=1,nat-1
        write(1,fmt='(3(X,F14.8))')gradE_c(ii,(i-1)*3+1),gradE_c(ii,(i-1)*3+2),gradE_c(ii,(i-1)*3+3)
      enddo
      write(1,fmt='(3(X,F14.8))')gradE_c(ii,(nat-1)*3+1),gradE_c(ii,(nat-1)*3+2),gradE_c(ii,(nat-1)*3+3)
      write(1,fmt='(A)')''
    endif
  enddo
endif

!from a.u. to A
geom=geom*au2ang

!gradient from a.u. to aJ/A
gradE_c=gradE_c*(au2aj/au2ang)

if (printl.ge.2) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Input data after conversion NEB.x:'
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian geometry [A resp. amu]:'
    do i=1,nat
      write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(ii,i),Z(ii,i),&
           &geom(ii,i,1),geom(ii,i,2),geom(ii,i,3),M(ii,i)
    enddo
    write(1,fmt='(A)')''
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Energy / aJ '
    write(1,fmt=outfmt)c_E(ii)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian gradient / aJ/A'
    do i=1,nat-1
      write(1,fmt='(3(X,F14.8))')gradE_c(ii,(i-1)*3+1),gradE_c(ii,(i-1)*3+2),gradE_c(ii,(i-1)*3+3)
    enddo
    write(1,fmt='(3(X,F14.8))')gradE_c(ii,(nat-1)*3+1),gradE_c(ii,(nat-1)*3+2),gradE_c(ii,(nat-1)*3+3)
    write(1,fmt='(A)')''
  enddo
endif

if (mld.ge.1) then
  ! create molden files:
  ! of each point
  do i=1,optdim
    fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))
    fname1='dyn_'//trim(adjustl(fname2))//'.mld'
    open(unit=15,file=fname1,status='unknown',position='append',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("Problem writing dyn_* in program NEB.x ",2)
    write(15,fmt='(I6)')nat
    write(15,fmt='(A)')''
    do j=1,nat
      write(15,fmt='(A2)',advance='no')S(i,j)
      write(15,fmt='(3(3X,F11.8))')(geom(i,j,k),k=1,3)
    enddo
    close(15)
  enddo

  ! of each chain (iteration)
  write(fl,fmt='(I8)')it
  read(fl,fmt='(A)')fname2
  fname1='dyn_'//trim(adjustl(fname2))//'.mld'
  open(unit=15,file=fname1,status='new',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing dyn_* in program NEB.x ",2)
  do i=0,optdim+1
    write(15,fmt='(I6)')nat
    write(15,fmt='(A)')''
    do j=1,nat
      write(15,fmt='(A2)',advance='no')S(i,j)
      write(15,fmt='(3(3X,F11.8))')(geom(i,j,k),k=1,3)
    enddo
  enddo
  close(15)
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! NEB
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do i=1,optdim
  do j=1,3*nat
    wvr1((i-1)*3*nat+j)=gradE_c(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_trueEgrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_trueEgrad.dat in program NEB.x ",2)
  do i=1,(optdim*3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*3*nat)
  close(27)
endif

!copy geom to vector format
do i=0,optdim+1
  do j=1,nat
    vgeom(i,(j-1)*3+1)=geom(i,j,1)
    vgeom(i,(j-1)*3+2)=geom(i,j,2)
    vgeom(i,(j-1)*3+3)=geom(i,j,3)
  enddo
enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Mass weighting
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (consttyp .eq. 1) then
  wmr1=0.0_dpr
  wmr2=0.0_dpr
!  do ii=1,optdim
    do i=1,nat
      do j=1,3
        wmr2((i-1)*3+j,(i-1)*3+j)=1.0_dpr/M(1,i)
      enddo
    enddo
!  enddo

  if (printl.ge.2) then
    write(1,fmt='(A)')' Masses for hessian'
    do i=1,3*nat
      do j=1,(3*nat-1)
        write(1,fmt='(F10.4)',advance='no')wmr2(i,j)
      enddo
      write(1,fmt='(F10.4)',advance='yes')wmr2(i,3*nat)
    enddo
    write(1,fmt='(A)')' Hess'
    do ii=1,optdim*3*nat
      do j=1,(optdim*3*nat-1)
        write(1,fmt='(F10.4)',advance='no')carthess(ii,j)
      enddo
      write(1,fmt='(F10.4)',advance='yes')carthess(ii,optdim*3*nat)
    enddo
  endif

  ! mass weight things ...
  do i=1,optdim
    do ii=1,3*nat
      do j=1,3*nat
        wmr1(ii,j)=carthess((i-1)*3*nat+ii,(i-1)*3*nat+j)
      enddo
    enddo
    if (printl.ge.2) then
    write(1,fmt='(A)')'copied hess'
      do ii=1,3*nat
        do j=1,(3*nat-1)
          write(1,fmt='(F10.4)',advance='no')wmr1(ii,j)
        enddo
        write(1,fmt='(F10.4)',advance='yes')wmr1(ii,3*nat)
      enddo
    endif

    call DGEMM('n','n',3*nat,3*nat,3*nat,1.0_dpr,wmr1,3*nat,wmr2,3*nat,0.0_dpr,wmr3,3*nat)
    if (printl.ge.2) then
      write(1,fmt='(A)')'mult. hess'
      do ii=1,3*nat
        do j=1,(3*nat-1)
          write(1,fmt='(F10.4)',advance='no')wmr3(ii,j)
        enddo
        write(1,fmt='(F10.4)',advance='yes')wmr3(ii,3*nat)
      enddo
    endif

    do ii=1,nintc
      do j=1,nintc
        carthess((i-1)*3*nat+ii,(i-1)*3*nat+j)=wmr3(ii,j)
      enddo
    enddo
  enddo

  do i=0,optdim+1
    do j=1,nat
      vgeom(i,(j-1)*3+1)=vgeom(i,(j-1)*3+1)*dsqrt(M(i,j))
      vgeom(i,(j-1)*3+2)=vgeom(i,(j-1)*3+2)*dsqrt(M(i,j))
      vgeom(i,(j-1)*3+3)=vgeom(i,(j-1)*3+3)*dsqrt(M(i,j))
    enddo
  enddo
  do i=1,optdim
    do j=1,nat
      gradE_c(i,(j-1)*3+1)=gradE_c(i,(j-1)*3+1)/dsqrt(M(i,j))
      gradE_c(i,(j-1)*3+2)=gradE_c(i,(j-1)*3+2)/dsqrt(M(i,j))
      gradE_c(i,(j-1)*3+3)=gradE_c(i,(j-1)*3+3)/dsqrt(M(i,j))
    enddo
  enddo
endif

if ((consttyp .eq. 1).and.(printl.ge.2)) then
  write(1,fmt='(A)')'DEBUG print: after massweigthing'
  write(1,fmt='(A)')''
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian geometry [A*sqrt(amu)]'
    do i=1,nat-1
      write(1,fmt='(3(X,F14.8))')vgeom(ii,(i-1)*3+1),vgeom(ii,(i-1)*3+2),vgeom(ii,(i-1)*3+3)
    enddo
    write(1,fmt='(3(X,F14.8))')vgeom(ii,(nat-1)*3+1),vgeom(ii,(nat-1)*3+2),vgeom(ii,(nat-1)*3+3)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Energy / aJ '
    write(1,fmt=outfmt)c_E(ii)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian gradient / aJ/(A*sqrt(amu))'
    do i=1,nat-1
      write(1,fmt='(3(X,F14.8))')gradE_c(ii,(i-1)*3+1),gradE_c(ii,(i-1)*3+2),gradE_c(ii,(i-1)*3+3)
    enddo
    write(1,fmt='(3(X,F14.8))')gradE_c(ii,(nat-1)*3+1),gradE_c(ii,(nat-1)*3+2),gradE_c(ii,(nat-1)*3+3)
    write(1,fmt='(A)')''
  enddo
endif

! check distances of images
do i=1,optdim+1
  distg(i)=norm(vgeom(i,:)-vgeom(i-1,:),3*nat)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_cartmwdist.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_cartmwdist.dat in program NEB.x ",2)
  do i=1,optdim
    write(27,fmt='(F10.7,X)',advance='no')distg(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')distg(optdim+1)
  close(27)
endif

open(unit=27,file='cartmwdist.dat',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing cartmwdist.dat in program NEB.x ",2)
do i=1,optdim
  write(27,fmt='(F10.7,X)',advance='no')distg(i)
enddo
write(27,fmt='(F10.7,X)',advance='yes')distg(optdim+1)
close(27)

do i=1,optdim
  do j=1,3*nat
    wvr1((i-1)*3*nat+j)=gradE_c(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_Egrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_Egrad.dat in program NEB.x ",2)
  do i=1,(optdim*3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*3*nat)
  close(27)
endif

call tangcons_cart(c_E,vgeom,optdim,scons,tang,M,scmin,scmax,3*nat,tangtype,printl,nat)

if (printl.ge.2) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Tangents and spring constants:'
  write(1,fmt='(A)')''
  do ii=1,optdim
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Spring constant / aJ*(A^-2*amu^-1) '
    write(1,fmt=outfmt)scons(ii)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Tangent '
    do i=1,(3*nat-1)
      write(1,fmt=outfmt)tang(ii,i)
    enddo
    write(1,fmt=outfmt)tang(ii,3*nat)
    write(1,fmt='(A)')''
  enddo
endif

if (printl .ge. 2) then
  open(unit=27,file='see_springcons.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_springcons.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F14.8,X)',advance='no')scons(i)
  enddo
  write(27,fmt='(F14.8,X)',advance='yes')scons(optdim)
  close(27)
endif

!copy the tangent for DEBUG print
do i=1,optdim
  do j=1,3*nat
    wvr1((i-1)*3*nat+j)=tang(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_tang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_tang.dat in program NEB.x ",2)
  do i=1,(optdim*3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*3*nat)
  close(27)
endif

! adjust the gradient for NEB
call adjgrd_cart(vgeom,scons,gradE_c,tang,gradE,gradspr,c_E,optdim,M,3*nat,tangtype,printl,startci,nebtype,cipoints)

if (printl.ge.2) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adjusted gradients'
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Energy gradient / aJ*A^-1*sqrt(amu)^-1'
    do i=1,(3*nat-1)
      write(1,fmt=outfmt)gradE(ii,i)
    enddo
    write(1,fmt=outfmt)gradE(ii,3*nat)
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Spring gradient'
    do i=1,(3*nat-1)
      write(1,fmt=outfmt)gradspr(ii,i)
    enddo
    write(1,fmt=outfmt)gradspr(ii,3*nat)
    write(1,fmt='(A)')''
  enddo
endif

! calculate the step
call relax(carthess,gradspr,dqspr,optdim,3*nat)

call relax(carthess,gradE,dqE,optdim,3*nat)

do i=1,3*nat
  wvr1(i)=sp(gradE_q(i,:),tang(i,:),3*nat)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_spgradEtang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spgradEtang.dat in program NEB.x ",2)
  do i=1,(3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(3*nat)
  close(27)
endif

do i=1,3*nat
  wvr1(i)=sp(dqspr(i,:),tang(i,:),3*nat)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_spdqsprtang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spdqsprtang.dat in program NEB.x ",2)
  do i=1,(3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(3*nat)
  close(27)
endif

do i=1,3*nat
  wvr1(i)=sp(dqE(i,:),tang(i,:),3*nat)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_spdqEtang.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_spdqEtang.dat in program NEB.x ",2)
  do i=1,(3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(3*nat)
  close(27)
endif

do i=1,optdim
  wvr1(i)=norm(dqE(i,:),3*nat)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_normdqE.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_normdqE.dat in program NEB.x ",2)
  do i=1,(3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(3*nat)
  close(27)
endif

do i=1,optdim
  wvr1(i)=norm(dqspr(i,:),3*nat)
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_normdqspr.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_normdqspr.dat in program NEB.x ",2)
  do i=1,(3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(3*nat)
  close(27)
endif

dq=dqE+dqspr

if (printl.ge.2) then
  write(1,fmt='(A)')'DEBUG print: after relax'
  write(1,fmt='(A)')''
  write(1,fmt='(A)')''
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian displacement [A*sqrt(amu)]'
    do i=1,nat-1
      write(1,fmt='(3(X,F14.8))')dq(ii,(i-1)*3+1),dq(ii,(i-1)*3+2),dq(ii,(i-1)*3+3)
    enddo
    write(1,fmt='(3(X,F14.8))')dq(ii,(nat-1)*3+1),dq(ii,(nat-1)*3+2),dq(ii,(nat-1)*3+3)
    write(1,fmt='(A)')''
  enddo
endif

do i=1,optdim
  do j=1,3*nat
    wvr1((i-1)*3*nat+j)=dq(i,j)
  enddo
enddo

open(unit=40,file='dq',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing dq in program NEB.x ",2)
do i=1,optdim*3*nat
  write(40,fmt=outfmt)wvr1(i)
enddo
close(40)

if (printl .ge. 2) then
  open(unit=27,file='see_displ.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_displ.dat in program NEB.x ",2)
  do i=1,(optdim*3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*3*nat)
  close(27)
endif

! check for convergence
call check_nebconv(gradE_c,conv_grad,conv_disp,M,optdim,dq,img1,img2,3*nat,nat)

! unweight displacement
do i=1,optdim
  if (consttyp .eq. 1) then
    do j=1,nat
      dq(i,(j-1)*3+1)=dq(i,(j-1)*3+1)/dsqrt(M(i,j))
      dq(i,(j-1)*3+2)=dq(i,(j-1)*3+2)/dsqrt(M(i,j))
      dq(i,(j-1)*3+3)=dq(i,(j-1)*3+3)/dsqrt(M(i,j))
    enddo
  endif
enddo

! step restriction
write(1,fmt='(A)')''
write(1,fmt='(A)')'Checking for step restriction on components individually'
write(1,fmt='(A)')''

do i=1,optdim
  do j=1,3*nat
    if (dabs(dq(i,j)).gt.stepres) then
      write(1,fmt='(A,I10,A,I10)')'Restriction on image ',i,' component ',j
      dq(i,j)=(stepres/dabs(dq(i,j)))*dq(i,j)
    endif
  enddo
enddo

do i=1,optdim
  do j=1,3*nat
    wvr1((i-1)*3*nat+j)=dq(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_uwdispl.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_uwdispl.dat in program NEB.x ",2)
  do i=1,(optdim*3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*3*nat)
  close(27)
endif

do i=1,optdim
  fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))
  fname1='uwdq.'//fname2
  open(unit=15,file=fname1,status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing uwdq.* in program NEB.x ",2)
  do j=1,3*nat
    write(15,fmt=outfmt)dq(i,j)
  enddo
  close(15)
enddo

if (printl .ge. 2) then
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian geometry before relaxation:'
    do i=1,nat
      write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(ii,i),Z(ii,i),&
           &geom(ii,i,1),geom(ii,i,2),geom(ii,i,3),M(ii,i)
    enddo
    write(1,fmt='(A)')''
  enddo
endif

if ((it.lt.startdiis).or.(maxgdiis.le.1)) then
  ! displace geometries
  do i=1,optdim
    do j=1,nat
      do ii=1,3
        geom(i,j,ii)=geom(i,j,ii)+dq(i,(j-1)*3+ii)
      enddo
    enddo
  enddo
else
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Performing gdiis global'
  write(1,fmt='(A)')''
  !unweight geometries 
  do i=0,optdim+1
    do j=1,nat
      vgeom(i,(j-1)*3+1)=vgeom(i,(j-1)*3+1)/dsqrt(M(i,j))
      vgeom(i,(j-1)*3+2)=vgeom(i,(j-1)*3+2)/dsqrt(M(i,j))
      vgeom(i,(j-1)*3+3)=vgeom(i,(j-1)*3+3)/dsqrt(M(i,j))
    enddo
  enddo

  call gdiisdq_neb_total_cart(vgeom,dq,optdim,img1,img2,3*nat,maxgdiis,maxddiis)
  !copy geom back
  do i=0,optdim+1
    do j=1,nat
      geom(i,j,1)=vgeom(i,(j-1)*3+1)
      geom(i,j,2)=vgeom(i,(j-1)*3+2)
      geom(i,j,3)=vgeom(i,(j-1)*3+3)
    enddo
  enddo
endif

if (printl .ge. 2) then
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian geometry after relaxation:'
    do i=1,nat
      write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(ii,i),Z(ii,i),&
           &geom(ii,i,1),geom(ii,i,2),geom(ii,i,3),M(ii,i)
    enddo
    write(1,fmt='(A)')''
  enddo
endif

!back to  a.u.
geom=geom/au2ang

if (printl .ge. 2) then
  do ii=0,optdim+1
    write(1,fmt='(A,I8)')'Image ',ii
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian geometry after relaxation in a.u.'
    do i=1,nat
      write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(ii,i),Z(ii,i),&
           &geom(ii,i,1),geom(ii,i,2),geom(ii,i,3),M(ii,i)
    enddo
    write(1,fmt='(A)')''
  enddo
endif

do i=1,optdim
  do j=1,3*nat
    wvr1((i-1)*3*nat+j)=dq(i,j)
  enddo
enddo

if (printl .ge. 2) then
  open(unit=27,file='see_gdiisuwdispl.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_gdiisuwdispl.dat in program NEB.x ",2)
  do i=1,(optdim*3*nat-1)
    write(27,fmt='(F10.7,X)',advance='no')wvr1(i)
  enddo
  write(27,fmt='(F10.7,X)',advance='yes')wvr1(optdim*3*nat)
  close(27)
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write results
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do i=1,optdim
  fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))
  fname1='new_geom.'//fname2
  open(unit=15,file=fname1,status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing new_geom.* in program NEB.x ",2)
  do ii=1,nat
    write(unit=15,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i,ii),Z(i,ii),&
         &geom(i,ii,1),geom(i,ii,2),geom(i,ii,3),M(i,ii)
  enddo
  close(15)
enddo

write(1,fmt='(A)')'Program finished: NEB.x with SUCCESS'

close(1)

end subroutine

subroutine check_nebconv(gradE_q,conv_grad,conv_disp,M,optdim,dq,img1,img2,imdim,wnat)
! This subroutine checks the convergence of the NEB relaxation 
! by two criteria: the projected gradient and the displacement 

use prec_mod
use units_mod
use general_mod

implicit none
integer optdim,imdim,wnat
real(kind=dpr) gradE_q(0:optdim+1,imdim),conv_grad,M(0:optdim+1,wnat)
real(kind=dpr) mtot,curr_grad(optdim),dq(optdim,imdim),wvr1(imdim)
real(kind=dpr) curr_disp(optdim),conv_disp
integer i,j,k,co(2*optdim),test
character*40 img1(0:optdim+1),img2(0:optdim+1)

! gradE_q ..... energy gradient ............................ [aJ/(A*sqrt(amu))] 
! curr_grad ... ||proj. Egrad||/sqrt(N) .................... [aJ/(A*sqrt(amu))]
! curr_disp ... ||dq||/sqrt(N) ............................. [A*sqrt(amu)]
! conv_grad ... threshold for norm of projected gradient ... [aJ/(A*sqrt(amu))]
! conv_disp ... threshold for norm of displacement ......... [A*sqrt(amu)]
! M ........... masses ..................................... [amu]
! dq .......... step ....................................... [A*sqrt(amu)]

co=1

write(1,fmt='(A)')'Average norm                  Image                Currently        Converged'
write(1,fmt='(A)')''
do i=1,optdim
  mtot=0.0_dpr
  do j=1,wnat
    mtot=mtot+M(i,j)
  enddo

  curr_grad(i)=(norm(gradE_q(i,:),imdim)/dsqrt(dble(imdim)))

  if (curr_grad(i).lt.conv_grad) then
    co(i)=0
    write(1,fmt='(A)',advance='no')'Gradient(aJ/(A*sqrt(amu))) '
    write(1,fmt='(A4,X)',advance='no')img1(i)
    write(1,fmt='(A4,X)',advance='no')img2(i)
    write(1,fmt=outfmt,advance='no')curr_grad(i)
    write(1,fmt='(A)',advance='yes')'              YES '
  else
    co(i)=1
    write(1,fmt='(A)',advance='no')'Gradient(aJ/(A*sqrt(amu))) '
    write(1,fmt='(A4,X)',advance='no')img1(i)
    write(1,fmt='(A4,X)',advance='no')img2(i)
    write(1,fmt=outfmt,advance='no')curr_grad(i)
    write(1,fmt='(A)',advance='yes')'               NO '
  endif
enddo

write(1,fmt='(A)')''
do i=1,optdim
  curr_disp(i)=norm(dq(i,:),imdim)/dsqrt(dble(imdim))
  if (curr_disp(i).lt.conv_disp) then
    co(i+optdim)=0
    write(1,fmt='(A)',advance='no')'Displ(sqrt(amu)*A)         '
    write(1,fmt='(A4,X)',advance='no')img1(i)
    write(1,fmt='(A4,X)',advance='no')img2(i)
    write(1,fmt=outfmt,advance='no')curr_disp(i)
    write(1,fmt='(A)',advance='yes')'              YES '
  else
    write(1,fmt='(A)',advance='no')'Displ(sqrt(amu)*A)         '
    write(1,fmt='(A4,X)',advance='no')img1(i)
    write(1,fmt='(A4,X)',advance='no')img2(i)
    write(1,fmt=outfmt,advance='no')curr_disp(i)
    write(1,fmt='(A)',advance='yes')'               NO '
  endif
enddo

write(1,fmt='(A)')''
write(1,fmt='(A)')''

test=sum(co)
open(unit=47,file='convergence',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing file convergence in sub check_nebconv",2)
if (test.eq.0) then
  write(47,fmt='(A)')'T'
else
  write(47,fmt='(A)')'F'
endif

if (printl .ge. 1) then 
  open(unit=27,file='see_convgrad.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_convgrad.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F6.4,X)',advance='no')curr_grad(i)
  enddo
  write(27,fmt='(F6.4,X)',advance='yes')curr_grad(optdim)
  close(27)
endif

if (printl .ge. 1) then
  open(unit=27,file='see_convdisp.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_convdisp.dat in program NEB.x ",2)
  do i=1,(optdim-1)
    write(27,fmt='(F6.4,X)',advance='no')curr_disp(i)
  enddo
  write(27,fmt='(F6.4,X)',advance='yes')curr_disp(optdim)
  close(27)
endif

end subroutine

subroutine relax(hess,grad,dq,optdim,imdim)
! relaxes the nuged elastic band
! dq=-H^-1*g

use prec_mod
use units_mod
use general_mod

implicit none
integer optdim,imdim
real(kind=dpr) hess(optdim*imdim,optdim*imdim),grad(optdim,imdim),wvr1(optdim*imdim)
real(kind=dpr) invhess(optdim*imdim,optdim*imdim),dq(optdim,imdim),work(:),wvr2(optdim*imdim)
integer i,j,k,l,ii,wi1,ipiv(:),lwork,info
allocatable ipiv,work

! hess ..... hessian mw ....... [aJ/(A^2*amu)]
! grad ..... cart. gradient ... [aJ/(A*sqrt(amu))]
! dq ....... step ............. [A*sqrt(amu)]
! optdim ... N° of images to opt.
! imdim .... dimension of 1 image

write(1,fmt='(A)')''
write(1,fmt='(A)')'Newton Raphson relaxation'
write(1,fmt='(A)')''

if (printl .ge. 2) then
  write(1,fmt='(A)')'Input Hessian :'
    do i=1,imdim*optdim
      do j=1,(imdim*optdim-1)
        write(1,fmt='(F10.4)',advance='no')hess(i,j)
      enddo
      write(1,fmt='(F10.4)',advance='yes')hess(i,optdim*imdim)
    enddo
  write(1,fmt='(A)')''
endif

! copy gradient
do i=1,optdim
  do j=1,imdim
    wvr1((i-1)*imdim+j)=grad(i,j)
  enddo
enddo

wi1=optdim*imdim

!invredhess=redhess^-1
allocate(ipiv(optdim*imdim))
allocate(work(optdim*imdim))

info=0
invhess=hess
call dgetrf(wi1,wi1,invhess,wi1,ipiv,info) !PLU fact. necessary first
if (info.ne.0) call error_sub("Problem with dgetrf in sub relax",2)
call dgetri(wi1,invhess,wi1,ipiv,work,-1,info)
if (info.ne.0) call error_sub("Problem with dgetri in sub relax",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call dgetri(wi1,invhess,wi1,ipiv,work,lwork,info)
if (info.ne.0) call error_sub("Problem with dgetri in sub relax",2)

if (printl .ge. 2) then
  write(1,fmt='(A)')'Inverse Hessian :'
    do i=1,optdim*imdim
      do j=1,(optdim*imdim-1)
        write(1,fmt='(F10.5)',advance='no')invhess(i,j)
      enddo
      write(1,fmt='(F10.5)',advance='yes')invhess(i,optdim*imdim)
    enddo
  write(1,fmt='(A)')''
endif

call DGEMV('n',wi1,wi1,-1.0_dpr,invhess,wi1,wvr1,1,0.0_dpr,wvr2,1)

!copy the displacement
do i=1,optdim
  do j=1,imdim
    dq(i,j)=wvr2((i-1)*imdim+j)
  enddo
enddo

end subroutine

end program
