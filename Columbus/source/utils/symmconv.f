!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program symconv
      implicit none
      integer maxatoms,maxelements
      parameter (maxatoms=100,maxelements=36) 
      integer natoms
      real*8 xyz(3,maxatoms),cofmass(3),totmass,thresh
      real*8 xyznew(3,maxatoms),inertia(3,3) 
      real*8 work(maxatoms),eigenvalues(3),rin(3),rout(3,maxatoms)
      real*8 symmcoords(3,maxatoms),uniquecoords(3,maxatoms)
      real*8 scrcoords(3,maxatoms)
      integer iwork(maxatoms),info ,ipiv(maxatoms)
      integer equivatoms(7,maxatoms)
      character*2 label(maxatoms)
       integer i,oz(maxatoms),uoz(maxatoms),nunique,j,isum,iaxis 

      character*2 alabel(maxelements)
      real*8 mass(maxelements)
      common /elements/mass, alabel 
      data alabel /'H ','HE',
     .             'LI','BE','B ','C ','N ','O ','F ','NE',
     .             'NA','MG','AL','SI','P ','S ','CL','AR',
     .             'K ','CA',
     .             'SC','TI','V ','CR','MN','FE','CO','NI','CU','ZN',
     .             'GA','GE','AS','SE','BR','KR'/
       data mass /1.00794d0,4.002602d0,6.941d0,9.012182d0,10.811d0,
     .           12.0107d0,14.0067d0,15.9994d0,18.9984032d0,20.1797d0,
     .           22.989770d0,24.3050d0,26.981538d0,28.0855d0,
     .           30.973761d0,32.065d0,35.453d0,39.948d0,
     .   39.0983d0,40.078d0,44.955910d0,47.867d0,50.9415d0,51.9961d0,
     .   54.938049d0,55.845d0,58.933200d0,58.6934d0,63.546d0,65.409d0,
     .   69.723d0,72.64d0,74.92160d0,78.96d0,79.904d0,83.798d0/



c     open(unit=6, file="symconvls", form="formatted")

      write(6,*) '*************************************************'
      write(6,*) '**        program symconv                      **' 
      write(6,*) '** - placing arbitrarily positioned molecules  **'
      write(6,*) '**   correctly into symmetry-adjusted frame    **'
      write(6,*) '** - threshold-driven symmetrization of        **'
      write(6,*) '**    nearly symmetric coordinates             **'
      write(6,*) '** - determination of point group symmetry     **'
      write(6,*) '**    of abelian point groups                  **'
      write(6,*) '**                                             **'
      write(6,*) '** Thomas Mueller,                             **'
      write(6,*) '** Juelich Supercomputer Centre                **'
      write(6,*) '** Institute of Advanced Simulation            **'
      write(6,*) '** Research Centre Juelich, Germany            **'
      write(6,*) '** version 0.1 / september 2011                **'
      write(6,*) '** input format: molden (moldenin)             **'
      write(6,*) '**               xmol   (xmoldenin)            **'
      write(6,*) '*************************************************'
       
      call readmolden(natoms,label,xyz,oz) 
      write(6,*) '----- input data -----'
      write(6,99)
      do i=1,natoms
       write(6,100) label(i),xyz(1,i),xyz(2,i),xyz(3,i),oz(i)
      enddo
      write(6,101) natoms
  99  format('symbol    x(ang)       y(ang)        z(ang)       OZ') 
 100  format(a2,3x,3(f12.6,2x),2x,i4)
 101  format('total: ',i6,' atoms')
      write(6,*) '----------------------'

       write(6,*) 'enter symmetrization threshold'
       read(5,*) thresh
 
c
c   compute totalmass and center of mass
c
       call centerofmass(cofmass,totmass,natoms,xyz,oz)
 
       write(6,*) 'Shifting orign to center of mass '
       write(6,*) 'new coordinates '
       do i=1,natoms
         do j=1,3
         xyznew(j,i)=xyz(j,i)-cofmass(j)/totmass
         enddo 
       enddo
      do i=1,natoms
       write(6,100) label(i),xyznew(1:3,i),oz(i)
      enddo

c
c   compute tensor of intertia
c
       call tensorofinertia(inertia,natoms,xyznew,oz)
c
c   eigenvectors of moment of inertia
c
c       lwork=1+5*n+3*n*n + 2*n*n
c       liwork=2+6*n
        
        CALL dsyevd('V','U',3,inertia,3,eigenvalues,
     .       work,100,iwork,100,info)

        write(6,106) eigenvalues
 106    format(' eigenvalues: ' /, 3(f12.6,2x))
        write(6,107) inertia(1,1:3),inertia(2,1:3),inertia(3,1:3) 
 107    format(' inertia: ' /, 3(3(f12.6,2x)/))


 108    format(' inv inertia: ' /, 3(3(f12.6,2x)/))
c
c   rotation into the axis of inertia
c
        
c       do i=1,natoms
c        call dgemv('T',3,3,1.0d0,inertia,3,xyznew(1,i),1,0.0d0,
c    .              rout(1,i),1)
c       enddo 
        call dgemm('T','N',3,natoms,3,1.0d0,inertia,3,xyznew,3,
     .               0.0d0,rout,3)

        write(6,*) 'rotated coordinates '
        do i=1,natoms
          write(6,200) '#',i,rout(1:3,i),oz(i)
 200     format(a,i3,3x,3(f12.6,3x),i4)
        enddo

       write(6,*) ' TESTING: center of mass ' 
       call centerofmass(cofmass,totmass,natoms,rout,oz)
 
       write(6,*) ' TESTING: tensor of inertia ' 
       call tensorofinertia(inertia,natoms,rout,oz)

c
c      Symmetry analysis 
c

       call analyse(rout,equivatoms,natoms,symmcoords,thresh)

c
c      should add a test, if the algorithm fails due to high
c      symmetry
c
        write(6,*) 'symmetrized coordinates '
        do i=1,natoms
          write(6,200) '#',i,symmcoords(1:3,i),oz(i)
        enddo
        write(6,*) 'symmetry unique coordinates '
        nunique=0
        do i=1,natoms
           isum=0
          do j=1,7
           if (equivatoms(j,i).eq.-1 .or. equivatoms(j,i).eq.i) 
     .               isum=isum-1 
          enddo      
          if (isum.eq.-7) then 
          nunique=nunique+1
          uniquecoords(1:3,nunique)=symmcoords(1:3,i)
          uoz(nunique)=oz(i)
          write(6,200) '#',i,symmcoords(1:3,i),oz(i)
          endif
        enddo

       write(6,*) 'interchange coordinate axis(312 means xyz -> yzx)'
       read(5,*) iaxis
       i=iaxis/100 
       do j=1,natoms
         scrcoords(i,j)=symmcoords(1,j)
       enddo 

       i=mod(iaxis,100)/10 
       do j=1,natoms
         scrcoords(i,j)=symmcoords(2,j)
       enddo 
          

       i=mod(iaxis,10) 
       do j=1,natoms
         scrcoords(i,j)=symmcoords(3,j)
       enddo 

       symmcoords(1:3,1:natoms)=scrcoords(1:3,1:natoms)


       i=iaxis/100
       do j=1,nunique
         scrcoords(i,j)=uniquecoords(1,j)
       enddo

       i=mod(iaxis,100)/10
       do j=1,nunique
         scrcoords(i,j)=uniquecoords(2,j)
       enddo


       i=mod(iaxis,10)
       do j=1,nunique
         scrcoords(i,j)=uniquecoords(3,j)
       enddo

       uniquecoords(1:3,1:nunique)=scrcoords(1:3,1:nunique)

          

c  diese symmetrisierten Koordinaten sind nur paarweise
c  symmetrisch - besser sie aus den uniquecoords zu regenerieren

       write(6,*) 'symmetrized, symmetry-redundant coordinates:',
     .            ' on  molden.out ' 
       call outmolden(symmcoords,natoms,oz,'molden.out')
       write(6,*) 'symmetrized, symmetry-unique coordinates:',
     .            ' on  molden_unique.out ' 
       call outmolden(uniquecoords,nunique,uoz,'molden_unique.out')
       call dscal(natoms*3,1.8897518d0,symmcoords,1)
       write(6,*) 'symmetrized, symmetry-redundant coordinates:',
     .            ' on  geom.out (COLUMBUS format) ' 
       call outcolumbus(symmcoords,natoms,oz,'geom.out')
       call dscal(nunique*3,1.8897518d0,uniquecoords,1)
       write(6,*) 'symmetrized, symmetry-unique coordinates:',
     .            ' on  geom_unique.out (COLUMBUS format) ' 
       call outcolumbus(uniquecoords,nunique,uoz,'geom_unique.out')
       close(6)

      end 


      subroutine readmolden(natoms,label,xyz,oz)
      implicit none
      integer maxatoms,maxelements
      parameter (maxatoms=100,maxelements=36)
      integer iatom,natoms,j,k,jstart,oz(maxatoms)
      character*80 line,nline
      real*8 xyz(3,maxatoms) 
      character*2 label(maxatoms)
      integer capa,capz,lita,litz
      logical lexist1,lexist2

      character*2 alabel(maxelements)
      real*8 mass(maxelements)
      common /elements/mass, alabel 

      inquire(file="moldenin", exist=lexist1)
      inquire(file="xmoldenin", exist=lexist2)
      if (.not. (lexist1 .or. lexist2) ) 
     .  call bummer('could not read input geometry from file moldenin'// 
     .  '(molden format) or xmoldenin (xmol format)',0,2)
      if (lexist1) then
      open(unit=10,file='moldenin',form='formatted',status='old')
      else 
      open(unit=10,file='xmoldenin',form='formatted',status='old')
      endif
      read(10,*) natoms
      if (natoms.gt.maxatoms) 
     .   call bummer('too many atoms, increase maxatoms',maxatoms,2)
      read(10,*)
      do iatom=1,natoms
       read (10,'(a)') line
        do j=1,len_trim(line) 
            if (line(j:j).eq.',') line(j:j)='.'
        enddo 
c      write(6,*) line(1:len_trim(line))
c  eliminate multiple spaces 
       k=0
       do j=1,len_trim(line)
        if (line(j:j).ne.' ' ) then 
          jstart=j
          exit
        endif
       enddo
       k=1
       nline(k:k)=line(jstart:jstart) 
       do j=jstart+1,len_trim(line)
       if (line(j:j).ne.' ') then
          k=k+1
          nline(k:k)=line(j:j) 
       elseif (nline(k:k).ne.' ') then
          k=k+1
          nline(k:k)=line(j:j)
       endif 
       enddo 
       k=k+1
       nline(k:k)=' '
c  get label 
       j=scan(nline,' ')
c      write(6,*) 'label=',nline(1:j-1)
       if (j-1.eq.1) then 
          label(iatom)(1:1)=nline(1:1)
          label(iatom)(2:2)=' '
       else
          label(iatom)(1:2)=nline(1:2)
       endif
c      write(6,'(3a)') 'remainder=',nline(j+1:len_trim(nline)),':'
       read (nline(j+1:),*) xyz(1:3,iatom)
c      write(6,'(3f10.6)') x(iatom),y(iatom),z(iatom)
c      capital A 
       capa=iachar('A')
       capz=iachar('Z')
       lita=iachar('a')
       litz=iachar('z')
c      convert to upper case
       if (iachar(label(iatom)(1:1)).ge.lita .and. 
     .             iachar(label(iatom)(1:1)).le.litz)   then 
        label(iatom)(1:1)=achar(iachar(label(iatom)(1:1))+capa-lita)
       endif 
       if (iachar(label(iatom)(2:2)).ge.lita .and. 
     .             iachar(label(iatom)(2:2)).le.litz)   then 
        label(iatom)(2:2)=achar(iachar(label(iatom)(2:2))+capa-lita)
       endif 
c     find OZ
       oz(iatom)=-99 
       do k=1,maxelements
        if (label(iatom)(1:2).eq.alabel(k)(1:2)) then 
           oz(iatom)=k
           exit
        endif
       enddo




       enddo
       return
       end 
       
 
      

       subroutine  centerofmass(cofmass,totmass,natoms,xyz,oz)
       implicit none
       integer maxelements 
       parameter (maxelements=36)
       real*8 cofmass(3),totmass,xyz(3,*)
       integer oz(*),i,natoms 
      character*2 alabel(maxelements)
      real*8 mass(maxelements)
      common /elements/mass, alabel

       cofmass=0.0d0
       totmass=0.0d0
       do i=1,natoms
         cofmass(1)=cofmass(1)+xyz(1,i)*mass(oz(i))
         cofmass(2)=cofmass(2)+xyz(2,i)*mass(oz(i))
         cofmass(3)=cofmass(3)+xyz(3,i)*mass(oz(i))
         totmass   = totmass + mass(oz(i))
       enddo

       write(6,102) cofmass(1:3),totmass
102    format(' center of mass: ',3(f12.6,2x),' total mass=',f12.6)
        return
        end

       subroutine tensorofinertia(inertia,natoms,xyznew,oz)
       implicit none
       integer maxelements 
       parameter (maxelements=36)
        real*8 inertia(3,3)
       integer i,natoms
       integer oz(*)
        real*8 xyznew(3,*) 
      character*2 alabel(maxelements)
      real*8 mass(maxelements)
      common /elements/mass, alabel

       inertia=0.0d0
       do i=1,natoms
       Inertia(1,1)=Inertia(1,1)+mass(oz(i))*
     .           (xyznew(2,i)*xyznew(2,i)+xyznew(3,i)*xyznew(3,i))
        Inertia(1,2)=Inertia(1,2)-mass(oz(i))*xyznew(1,i)*xyznew(2,i)
        Inertia(1,3)=Inertia(1,3)-mass(oz(i))*xyznew(1,i)*xyznew(3,i)
        Inertia(2,1)=Inertia(2,1)-mass(oz(i))*xyznew(1,i)*xyznew(2,i)
       Inertia(2,2)=Inertia(2,2)+mass(oz(i))*
     .             (xyznew(1,i)*xyznew(1,i)+xyznew(3,i)*xyznew(3,i))
        Inertia(2,3)=Inertia(2,3)-mass(oz(i))*(xyznew(2,i)*xyznew(3,i))
        Inertia(3,1)=Inertia(3,1)-mass(oz(i))*(xyznew(1,i)*xyznew(3,i))
        Inertia(3,2)=Inertia(3,2)-mass(oz(i))*(xyznew(2,i)*xyznew(3,i))
       Inertia(3,3)=Inertia(3,3)+mass(oz(i))*
     .     (xyznew(1,i)*xyznew(1,i)+xyznew(2,i)*xyznew(2,i))
       enddo
       write(6,105) Inertia
 105   format('tensor of inertia' /    3( 3(f12.6,2x)/))

        return
         end

        subroutine analyse(xyz,equivatoms,natoms,symmcoords,thresh)
        implicit none
        integer maxatoms
        parameter (maxatoms=100)
        real*8 thresh
        integer natoms,k,m(3),z(3)
        real*8 rvals(maxatoms),xyz(3,natoms),symmcoords(3,natoms)
c
c       operations: C2X,C2Y,C2Z,SXY,SXZ,SYZ,I
c
        integer  equivatoms(7,natoms)
        integer i,j

         equivatoms=-1
         do i=1,natoms
          rvals(i)=xyz(1,i)*xyz(1,i)+xyz(2,i)*xyz(2,i)+xyz(3,i)*xyz(3,i)
         enddo 
           do i=1,natoms
c           do j=1,i-1
            do j=1,i
             if (abs(rvals(i)-rvals(j)).lt.thresh) then 
c
c       C2X * (x,y,z) = (x,-y,-z)
c       C2Y * (x,y,z) = (-x,y,-z)
c       C2Z * (x,y,z) = (-x,-y,z)
c       SXY * (x,y,z) = (x,y,-z)
c       SXZ * (x,y,z) = (x,-y,z)
c       SYZ * (x,y,z) = (-x, y,z)
c
             Z=0
             do k=1,3
             if ((abs(xyz(k,i))-abs(xyz(k,j))).lt.thresh)  then 
                    if (abs(xyz(k,i)-xyz(k,j)).lt.thresh) then 
                           m(k)=1
                      symmcoords(k,i)=(xyz(k,i)+xyz(k,j))*0.5d0
                      symmcoords(k,j)=symmcoords(k,i)
                    elseif (abs(xyz(k,i)+xyz(k,j)).lt.thresh) then 
                           m(k)=-1
                      symmcoords(k,i)=(xyz(k,i)-xyz(k,j))*0.5d0
                      symmcoords(k,i)=sign(symmcoords(k,i),xyz(k,i))
                      symmcoords(k,j)=-symmcoords(k,i)
                    else
                      m(k)=99
                    endif
                    if (abs(xyz(k,i)).lt.thresh) then 
                       z(k)=1
                       symmcoords(k,i)=0.0d0
                       symmcoords(k,j)=0.0d0
                    endif
             else
                  m(k)=99
             endif 
             enddo
c             write(6,102) 'pair=',i,j,'m(*)=',m(1:3),' z(*)=',z(1:3)
  102    format(a,2i4,a,3i4,a,3i4)
c any zero coordinate can be used either with +-1 
              
             if (m(1)+m(2)+m(3).eq.1) then 
c sigma plane
             if (m(3).eq.-1) equivatoms(4,i)=j
             if (m(2).eq.-1) equivatoms(5,i)=j
             if (m(1).eq.-1) equivatoms(6,i)=j
             elseif (m(1)+m(2)+m(3).eq.-1) then
c  c2 axis
               if (m(1).eq.1) equivatoms(1,i)=j
               if (m(2).eq.1) equivatoms(2,i)=j
               if (m(3).eq.1) equivatoms(3,i)=j
             elseif (m(1)+m(2)+m(3).eq.-3) then
c inversion
              equivatoms(7,i)=j
             endif

             if (z(1).eq.1) then 

             if (-z(1)+m(2)+m(3).eq.1) then 
c sigma plane
              equivatoms(6,i)=j
             elseif (-z(1)+m(2)+m(3).eq.-1) then
c  c2 axis
               if (m(2).eq.1) equivatoms(2,i)=j
               if (m(3).eq.1) equivatoms(3,i)=j
             elseif (-z(1)+m(2)+m(3).eq.-3) then
c inversion
              equivatoms(7,i)=j
             endif
             endif

             if (z(2).eq.1) then

             if (-z(2)+m(1)+m(3).eq.1) then
c sigma plane
              equivatoms(5,i)=j
             elseif (-z(2)+m(1)+m(3).eq.-1) then
c  c2 axis
               if (m(1).eq.1) equivatoms(1,i)=j
               if (m(3).eq.1) equivatoms(3,i)=j
             elseif (-z(2)+m(1)+m(3).eq.-3) then
c inversion
              equivatoms(7,i)=j
             endif
             endif

             if (z(3).eq.1) then

             if (-z(3)+m(1)+m(2).eq.1) then
c sigma plane
              equivatoms(4,i)=j
             elseif (-z(3)+m(1)+m(2).eq.-1) then
c  c2 axis
               if (m(2).eq.1) equivatoms(2,i)=j
               if (m(1).eq.1) equivatoms(1,i)=j
             elseif (-z(3)+m(1)+m(2).eq.-3) then
c inversion
              equivatoms(7,i)=j
             endif
             endif
             endif





            enddo
           enddo
          write(6,100) 
          do i=1,natoms
           write(6,101) i, equivatoms(1:7,i)
          enddo
 100      format(' iatom    C2X   C2Y   C2Z  SXY   SXZ   SYZ    I ')
 101      format(2x,i4,7(3x,i3))
          return
          end
  


           subroutine outmolden(xyz,natoms,oz,name)
           implicit none
           integer maxelements
           parameter (maxelements=36)
           integer i,natoms,oz(natoms)
           real*8  xyz(3,natoms)
           character*(*) name
      character*2 alabel(maxelements)
      real*8 mass(maxelements)
      common /elements/mass, alabel

            open(unit=7,file=name(1:len_trim(name)),form='formatted') 
            write(7,*) natoms
            write(7,*) 'generated by symmconversion'
            do i=1,natoms
             write(7,100) alabel(oz(i)),xyz(1:3,i)
            enddo
  100       format(a,3x,3(f12.7,3x))
            close(7)
            return
            end

           subroutine outcolumbus(xyz,natoms,oz,name)
           implicit none
           integer maxelements 
           parameter (maxelements=36)
           integer i,natoms,oz(natoms)
           real*8  xyz(3,natoms)
           character*(*) name
      character*2 alabel(maxelements)
      real*8 mass(maxelements)
      common /elements/mass, alabel


c C     6.0    0.00000000    0.00000000    1.25306000   12.00000000
c C     6.0    0.00000000    0.00000000   -1.25306000   12.00000000
c H     1.0    1.74646000    0.00000000    2.37500000    1.00782504
c H     1.0   -1.74646000    0.00000000    2.37500000    1.00782504
c H     1.0    1.74646000    0.00000000   -2.37500000    1.00782504
c H     1.0   -1.74646000    0.00000000   -2.37500000    1.00782504
c12123456781123456789012312345678901234              12345678901234


            open(unit=7,file=name(1:len_trim(name)),form='formatted')
            do i=1,natoms
             write(7,100) alabel(oz(i)),dble(oz(i)),
     .                     xyz(1:3,i),mass(oz(i))
            enddo
  100       format(a2,f8.1,4(1x,f13.8))
            close(7)
            return
            end

