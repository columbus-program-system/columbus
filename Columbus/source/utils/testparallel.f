!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program testmpi

      include 'mpif.h'

      integer ierr,irank
      character*80 hostname
      character*9 filename

      
      call mpi_init(ierr)
      if (ierr.ne.0) call mpi_abort(MPI_COMM_WORLD,ierr)
      call mpi_comm_rank(MPI_COMM_WORLD,irank,ierr)
      if (ierr.ne.0) call mpi_abort(MPI_COMM_WORLD,ierr)
      ierr=hostnm(hostname)
      write(0,'(a,i3,a,a)') 'My process id is ',irank,' hostname=',
     . hostname

       if (irank.lt.10) then
         write(filename,'(a7,i1)') 'output.',irank
       elseif (irank.lt.100) then
         write(filename,'(a7,i2)') 'output.',irank
       else
         write(0,*) 'Too many processes ...'
       endif
      open (unit=7,file=filename(1:len_trim(filename)),
     .         form='formatted')
       write(7,*) ' MPI-TESTPROGRAM ' 
       write(7,'(a,i3,a,a)') 'My process id is ',irank,' hostname=',
     . hostname
       close(7)
      call mpi_finalize(ierr)
      stop
      end

