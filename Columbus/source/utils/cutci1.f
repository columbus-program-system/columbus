!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cutci1.f
cutci part=1 of 2.  workspace allocation routine
Created by filepp Feb 12 2002
c deck cutci
      program cutci
       implicit none
      include "../colib/getlcl.h" 
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer ifirst, lcore
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c
c
c     # use standard f90 memory allocation.
c
      integer mem1
      parameter ( lcored = 2 500 000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('argos: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore) )
      ifirst = 1
      mem1   = 0

c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
c
c     # return from driver() implies successful execution.
c
      call bummer('normal termination',0,3)
      stop 'end of cutci'
      end
