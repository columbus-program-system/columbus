!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program mo_mult
       implicit none
c
c  multiply different MO-coefficient matrices. This is for example useful in the case
c    of frozen core MCSCF.
c
c  possible add-on: include mulliken population analysis
c
c  version log:
c    20-oct-2011 file created -felix plasser
c
c
c  
c
  
      integer mxtitle, ntitlel, ntitler, filerr, syserr
      integer maxbfn,ierr,lvprt,formatl
      parameter (mxtitle=20, maxbfn=1023)
      character*80 titlesl(mxtitle),afmtl
      character*80 titlesr(mxtitle),afmtr
      character*4  labelsl(8),labelsr(8)
      integer i,j,offsetocc,offsetc
      integer occlenl,nsyml,nbfpsyl(8),nmopsyl(8),clenl
      integer occlenr,nsymr,nbfpsyr(8),nmopsyr(8),clenr
      integer nfrzcr(8),nfrzvr(8)
      integer cposl, cposr, cposrb
      integer occposr, occposn
      real*8 cl(maxbfn*maxbfn),cr(maxbfn*maxbfn)
      real*8 cout(maxbfn*maxbfn),crbig(maxbfn*maxbfn)
      real*8 occnew(maxbfn),occl(maxbfn),occr(maxbfn)
      character*80 mo_left,mo_right,mo_out
      integer newmo(maxbfn,8)
      logical occvec
      namelist /input/ mo_left,mo_right,mo_out,lvprt,nfrzcr,nfrzvr,
     . formatl
      
      open(6,file='mo_multls',status='unknown')
      
      write(6,*) '===== Program mo_mult ====='
      formatl = 0
      mo_left = 'mocoef_left'
      mo_right= 'mocoef_right'
      mo_out  = 'mocoef_mult'
      do i = 1,8
        nfrzcr(i)  = 0
        nfrzvr(i)  = 0
      enddo
      lvprt = 1
      
      open(unit=10,file='mo_multin',status='old',err=400)
      goto 401
      
400   call bummer('mo_multin missing, using default values',0,1)
      goto 402
     
401   call echoin(10,6,ierr)
      rewind(10)
      read(10,input,end=100)
 100  continue
      close(10)      

402   write(6,*) 'opening left mocoefficient file:',mo_left
      open(unit=11,file=mo_left,status='old')
      if (formatl.eq.0) then
c input in Columbus format
      clenl=maxbfn*maxbfn
      call moread(11,10,filerr,syserr,mxtitle,ntitlel,titlesl,
     . afmtl,nsyml,nbfpsyl,nmopsyl,labelsl,clenl,cl)

      write(6,*) 'Titles from mocoef file:'
      write(6,*) (titlesl(i),i=1,ntitlel)
      write(6,900) nsyml
      write(6,901) (nbfpsyl(i),i=1,nsyml)
      write(6,902) (nmopsyl(i),i=1,nsyml)

 900  format(' number of irreducible representations:',i4)
 901  format(' number of basis functions per irrep:',8i4)
 902  format(' number of molecular orbitals per irrep:',8i4)

      occlenl=0
      clenl=0
      do i=1,nsyml
       clenl=clenl+nmopsyl(i)*nbfpsyl(i)
       occlenl=occlenl+nmopsyl(i)
      enddo

      write(6,*) ' reading mocoefficients ... '
      call moread(11,20,filerr,syserr,mxtitle,ntitlel,titlesl,
     . afmtl,nsyml,nbfpsyl,nmopsyl,labelsl,clenl,cl)
      else
c input in Molcas format
       write(6,*) ' reading MOLCAS mocoefficients ...'
       call rdmof_molcas(11,nsyml, nmopsyl, nbfpsyl, cl,'mos')
       
       ntitlel=1
       titlesl(1) = 'converted from MOLCAS format'
       
       write(6,900) nsyml
       write(6,901) (nbfpsyl(i),i=1,nsyml)
       write(6,902) (nmopsyl(i),i=1,nsyml)
      endif
      if (lvprt.ge.2) then
      call prblks('Left MO coefficients',cl,nsyml,nbfpsyl,nmopsyl,
     .            'SAO','MO',1,6)
      endif
      
      close(11)
     
      write(6,*) 'opening right mocoefficient file:',mo_right
      open(unit=11,file=mo_right,status='old')
      clenr=maxbfn*maxbfn
      call moread(11,10,filerr,syserr,mxtitle,ntitler,titlesr,
     . afmtr,nsymr,nbfpsyr,nmopsyr,labelsr,clenr,cr)
      
      if (nsyml.ne.nsymr) call bummer('number of irreps not
     .  the same nsymr',nsymr,2)

      write(6,*) 'Titles from mocoef file:'
      write(6,*) (titlesr(i),i=1,ntitler)
      write(6,900) nsymr
      write(6,901) (nbfpsyr(i),i=1,nsymr)
      write(6,902) (nmopsyr(i),i=1,nsymr)

      occlenr=0
      clenr=0
      do i=1,nsymr
       clenr=clenr+nmopsyr(i)*nbfpsyr(i)
       occlenr=occlenr+nmopsyr(i)
      enddo

      write(6,*) ' reading mocoefficients ... '
      call moread(11,20,filerr,syserr,mxtitle,ntitler,titlesr,
     . afmtr,nsymr,nbfpsyr,nmopsyr,labelsr,clenr,cr)

      if (lvprt.ge.2) then
      call prblks('Right MO coefficients',cr,nsymr,nbfpsyr,nmopsyr,
     .            'Bas1','MO',1,6)
      endif
     

      write(6,*) 'reading occupation numbers ... '
      call wzero(maxbfn,occr,1)
      call moread(11,40,filerr,syserr,mxtitle,ntitler,titlesr,
     . afmtr,nsymr,nbfpsyr,nmopsyr,labelsr,occlenr,occr)
      if (filerr.ne.0)  then
          occvec=.false.
          else
          occvec=.true.
      endif
      close(11)
      
      write(6,*) 'Starting matrix multiplication ...'
      occposr = 1
      occposn = 1
      cposl = 1
      cposr = 1
      call wzero(maxbfn,occnew,1)
c      cposout = 1
      do i=1,nsyml
c embed the current block of the right MO-coefficient matrix
c   in a unit matrix
        if (nmopsyl(i).ne.(nfrzcr(i)+nbfpsyr(i)+nfrzvr(i)))then
         call bummer('MO matrices are not compatible in irrep',i,2)
        endif
        
        call wzero(maxbfn*maxbfn,crbig,1)
        cposrb = 1
        do j=1,nmopsyl(i)
          if (j.le.nfrzcr(i))then
            crbig(cposrb+j-1) = 1.d0
            occnew(occposn) = 2.d0
          elseif(j.gt.nfrzcr(i)+nbfpsyr(i))then
            crbig(cposrb+j-1) = 1.d0
            occnew(occposn) = 0.d0
          else
            call dcopy_wr(nbfpsyr(i),cr(cposr),1,
     .       crbig(cposrb+nfrzcr(i)),1)
            occnew(occposn) = occr(occposr)
            cposr = cposr + nbfpsyr(i)
            occposr = occposr + 1
          endif
          cposrb= cposrb + nmopsyl(i)
          occposn = occposn + 1
        enddo
        
c perform the actual matrix multiplication in this irrep
        call mxm(cl(cposl),nbfpsyl(i),crbig,nmopsyl(i),
     .    cout(cposl),nmopsyl(i))
        cposl=cposl + nmopsyl(i)*nbfpsyl(i)
      enddo
      
      if (lvprt.ge.2) then
      call prblks('Output MO coefficients',cout,nsyml,nbfpsyl,
     .       nmopsyl,'SAO','MO',1,6)
      endif
      
      ntitler = ntitler + 1
      titlesr(ntitler) = 'transformed using mo_mult.x'
      
      open(unit=12,file=mo_out,status='unknown')
       call mowrit(12,10,filerr,syserr,ntitler,titlesr,afmtr,
     .  nsyml,nbfpsyl,nmopsyl,labelsr,cout)
       write(6,*) '  writing transformed MOs ... '
       call mowrit(12,20,filerr,syserr,ntitler,titlesr,afmtr,
     .  nsyml,nbfpsyl,nmopsyl,labelsr,cout)

       if (occvec) then
       write(6,*) '  writing occupation numbers ... '
       call mowrit(12,40,filerr,syserr,ntitler,titlesr,afmtr,
     .  nsyml,nbfpsyl,nmopsyl,labelsr,occnew)
       endif

       close(12)
       close(6)      
      write(*,*) 'Finished ...'
      call bummer('normal termination',0,3)
      stop
      end
