!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccutci2.f
ccutci part=2 of 2.
cCreated by filepp Feb 12 2002
c deck driver
      subroutine driver( core, lcore, mem1, ifirst)
c
c   This program allows to copy all valid ci vectors (as defined in the
c   ci info file civout) from a given ci (or sigma) vector file.
c   Namelist type input
c
c   vectorin : name of the input vector file
c   vectorout: name of the output vector file
c   infoin   : name of the input vector info file
c   infoout  : name of the output vector info file
c   lvlprt   : printlevel (>=0)
c   delsrc   : if set to 1 the input vector and input vector info
c              files are deleted after the copying operation
c
c  09/20/00 (tm)
c
      implicit none 
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer maxvec
      parameter (maxvec=16)

      integer lcore,mem1,ifirst
      real*8 core(*)

      integer nslist,tape6
      equivalence (nunits(1),tape6)
      equivalence (nunits(3),nslist)
      integer civfl
      equivalence(nunits(7),civfl)
      integer civfln
      equivalence(nunits(8),civfln)

      integer delsrc,lvlprt
      integer convold2new,convnew2old,inputold
      character*60 vectorin,vectorout,infoin,infoout
      common /indata/delsrc,
     .   lvlprt,vectorin,vectorout,infoin,infoout
     .   , convold2new,convnew2old,inputold


      integer bperacc,recpervec,iroot,nel1st,nel,isweep
      integer nroot,roots(maxvec)
      integer i,inroot,outroot,ncsf
      integer*8 offsetin,offsetout
c
c     open the listing files.
c
      open( unit = tape6, file = fname(1), status = 'unknown')
      open( unit = nslist, file= fname(3), status = 'unknown')

      call who2c( 'cutci', tape6 )
      call datain
c     # open the necessary info containing files
c
      call cpcivout(nroot,roots,ncsf,delsrc)

c
      call ibummr( tape6 )
c
*@ifdef direct
*      call headwr(tape6,'cutci','5.4.2')
*@else
      call headwr(tape6,'cutci','5.4.2')
*@endif
c
      if (inputold.eq.0) then 
      call getputvec(fname(8)(1:len_trim(fname(8))),core,
     .                   2,int(1,8),'new')
      call getputvec(fname(8)(1:len_trim(fname(8))),core,
     .                   2,int(1,8),'close')
      else
       call openda(civfl,fname(7),8*4096,'keep','random')
       call openda(civfln,fname(8),8*4096,'keep','random')
      endif 

      bperacc= lcore/(8*4096)
      recpervec = (ncsf-1)/(8*4096) +1

      write(tape6,*) 'Core space of', lcore,' words sufficient for '
     . ,bperacc,' records'
      write(tape6,*) ncsf,' configurations = ', recpervec,
     . ' records per vector'
      write(tape6,*) 'Writing ',nroot,' vectors to new vector file'

      do i=1,nroot
         outroot=i
         inroot=roots(i)
         nel1st=1
         write(tape6,501) inroot,outroot
501      format(/1x,'Copying vector #',i2,' to record #',i2,
     .    ' on the output file')
         do isweep = 1, (recpervec-1)/bperacc
            nel=bperacc*8*4096
            write(tape6,100) inroot,isweep,nel1st,nel
100         format('Processing: root(in)=',i3,2x,' isweep=',i3,
     .       'nel1st=',i10,2x,'nel=',i10)
            if (inputold.eq.0) then 
            offsetin=(inroot-1)*ncsf+nel1st
           call getputvec(fname(7)(1:len_trim(fname(7))),core,nel,
     .       offsetin,'openrd')
            offsetout=(outroot-1)*ncsf+nel1st
           call getputvec(fname(8)(1:len_trim(fname(8))),core,nel,
     .       offsetout,'openwt')
           else
            call getvec(civfl,8*4096,inroot,ncsf,core(1),nel1st,nel)
            call putvec(civfln,8*4096,outroot,ncsf,core(1),nel1st,nel,0)
           endif
            nel1st=nel1st+nel
         enddo
         nel=ncsf-nel1st+1
         write(tape6,101) inroot,nel1st,nel
101      format('final sweep: root(in)=',i3,2x,
     .    'nel1st=',i10,2x,'nel=',i10)
            if (inputold.eq.0) then 
            offsetin=(inroot-1)*ncsf+nel1st
           call getputvec(fname(7)(1:len_trim(fname(7))),core,nel,
     .       offsetin,'openrd')
            offsetout=(outroot-1)*ncsf+nel1st
           call getputvec(fname(8)(1:len_trim(fname(8))),core,nel,
     .       offsetout,'openwt')
         else
         call getvec(civfl,8*4096,inroot,ncsf,core(1),nel1st,nel)
         call putvec(civfln,8*4096,outroot,ncsf,core(1),nel1st,nel,0)
         endif
      enddo
      if (inputold.eq.0) then 
      if (delsrc.eq.1) then
         open(unit=civfl,file=fname(7)(1:len_trim(fname(7))))
         close(unit=civfl,status='delete')
      else
         call getputvec(fname(7)(1:len_trim(fname(7))),core,
     .                   2,int(1,8),'close')
      endif
         call getputvec(fname(8)(1:len_trim(fname(8))),core,
     .                   2,int(1,8),'close')
      else
        if (delsrc.eq.1) then
          call closda(civfl,'delete',1)
        else
          call closda(civfl,'nodel',1)
        endif
        call closda(civfln,'nodel',1)
      endif
      call bummer('normal termination',0,3)
      return
      end



      block data xx
       implicit none 
c     da file record parameters
C     Common variables
C
      INTEGER             vecrln,indxln, ssym1,ssym2
C
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C

      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)

c------------------------------------------------------------------
c     # tape6 = standard listing file.
      integer      tape6
      equivalence (tape6,nunits(1))
      data tape6/6/, fname(1)/'cutcils'/
c------------------------------------------------------------------
c     # tape5 = user input file.
      integer      tape5
      equivalence (tape5,nunits(2))
      data tape5/5/, fname(2)/'cutciin'/
c------------------------------------------------------------------
c     # nslist = summary listing file.
      integer      nslist
      equivalence (nslist,nunits(3))
      data nslist/7/, fname(3)/'cutcism'/
c------------------------------------------------------------------
c     # filev1 = input vector file.
      integer      filev1
      equivalence (filev1,nunits(7))
      data filev1/10/
c------------------------------------------------------------------
c     # filev2 = output vector file.
      integer      filev2
      equivalence (filev2,nunits(8))
      data filev2/11/
c------------------------------------------------------------------
c     # civout1   input info file
      integer     civout1
      equivalence (civout1,nunits(16))
      data civout1/20/
c------------------------------------------------------------------
c     # civout2   output info file
      integer     civout2
      equivalence (civout2,nunits(17))
      data civout2/21/
c------------------------------------------------------------------
c include(data.common)
      integer maxsym
      parameter (maxsym=8)
      integer symtab,sym12,sym21,nbas,lmda,lmdb,nmbpr,strtbw,strtbx
      integer blst1e,nmsym,mnbas,cnfx,cnfw,scrlen
      common/data/symtab(maxsym,maxsym),sym12(maxsym),sym21(maxsym),
     + nbas(maxsym),lmda(maxsym,maxsym),lmdb(maxsym,maxsym),
     + nmbpr(maxsym),strtbw(maxsym,maxsym),strtbx(maxsym,maxsym),
     + blst1e(maxsym),nmsym,mnbas,cnfx(maxsym),cnfw(maxsym),scrlen
c
      data symtab/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      data sym12/1,2,3,4,5,6,7,8/

c     # da record lengths
c     # record lengths for ci created direct acc. files
c     #
c     # indxln : da record length for index file
c     # vecrln : da record length for vector files
c
c     # should be tuned for your machine....
c

*@if defined ibm && defined vector
*C      # vax maximum da record length is 4095
*       data indxln/4095/, vecrln/4095/
*@else
      data indxln/4096/, vecrln/32768/
*@endif

      end


      subroutine datain
c
c  read user input.
c
       implicit none
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer      tape6
      equivalence (tape6,nunits(1))
      integer      tape5
      equivalence (tape5,nunits(2))
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      character*60 fnamex
      equivalence (fname(nfilmx),fnamex)
c

c
      integer delsrc,lvlprt
      integer convold2new,convnew2old,inputold
      character*60 vectorin,vectorout,infoin,infoout
      common /indata/delsrc,
     .   lvlprt,vectorin,vectorout,infoin,infoout
     .   , convold2new,convnew2old,inputold
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

       integer ierr,i,j
c
*@ifndef nonamelist
      namelist /input/ lvlprt,vectorin,vectorout,infoin,infoout
     .                 ,delsrc,convold2new,convnew2old,inputold
*@endif
c
      ierr = 0
c
c
      open( unit = tape5, file = fname(2), status = 'unknown')
c
c     read and echo standard input until eof is reached.
c
      write(tape6,6010)
6010  format(' echo of the input for program cutci:')
      call echoin( tape5, tape6, ierr )
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind tape5

c defaults
       lvlprt=0
       vectorin='x'
       vectorout='x'
       infoin='x'
       infoout='x'
       convold2new=0
       convnew2old=0
       inputold=0
       delsrc =0
c
c     # read namelist input and keep processing if eof is reached.
c
c
*@ifdef nonamelist
*@elif defined  nml_required
*      read( tape5, nml=input, end=3 )
*@else
      read( tape5, input, end=3 )
*@endif
c
3     continue
c
      if ( vectorin.eq.'x') then
       call bummer('enter filename for input vector file (vectorin)',
     .               0,0)
      ierr=ierr+1
      endif

      if ( vectorout.eq.'x') then
       call bummer('enter filename for output vector file (vectorout)',
     .               0,0)
      ierr=ierr+1
      endif

      if ( infoin.eq.'x') then
       call bummer('enter filename for input info file (infoin)',
     .               0,0)
      ierr=ierr+1
      endif

      if ( infoout.eq.'x') then
       call bummer('enter filename for output info file (infoout)',
     .               0,0)
      ierr=ierr+1
      endif

      if (ierr.ne.0)
     . call bummer('input file errors, exiting',ierr,2)

      fname(7) = vectorin
      fname(8) = vectorout
      fname(16)= infoin
      fname(17)= infoout



      write(tape6,*)'units and filenames:'
c
      do 110 i = 1, (nfilmx - 1)
         if ( nunits(i) .eq. 0 ) goto 110
111      continue
         write(tape6,6050) i, nunits(i), fname(i)
110   continue
6050  format(1x,i4,': (',i2,')',4x,a)
6051  format(1x,i4,': (',i2,')',4x,a,t35,a)
c
      write(tape6,6020)
      close(unit=tape5)

       if (inputold.eq.1) then
         write(6,*) 'assuming initial v-vector in old format'
       else
         write(6,*) 'assuming initial v-vector in new format'
       endif

      if (convold2new.eq.1 .and. convnew2old.eq.1) 
     .  call bummer('invalid input convold2new OR convnew2old',0,2)

      if (convold2new.eq.1) then
        write(6,*) 'Converting old format (getvec/putvec) '
     .  ,'to new format (getputvec)'
        if (inputold.ne.1) 
     .  call bummer('... but input format is not old',0,2)
      endif
      if (convnew2old.eq.1) then
        write(6,*) 'Converting new format (getputvec) to old format',
     .  ' (getvec/putvec)'
        if (inputold.eq.1) 
     .  call bummer('... but input format is not new',0,2)
      endif
c
      return
6020  format(1x,72('-'))
      end



      subroutine cpcivout (nroot,roots,ncsf,delsrc)

c     copies and modifies the
c     vector info file written by ciudg
c
      implicit none 
c
c     output:
c       nroot   = number of valid roots on civout
c       roots(i)= contains the old position of ith valid vector
c                 on civout
c----------------------------------------------------
C
C  ##  parameter & common block section
C
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 20)
      INTEGER             CURVER
      PARAMETER           (CURVER = 1)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 10)
      INTEGER             MXINFO
      PARAMETER           (MXINFO = 10)
      INTEGER             MXNRGY
      PARAMETER           (MXNRGY = 10)
      integer maxvec
      parameter (maxvec=16)
C
      integer nunits,nfilmx
      parameter(nfilmx=55)
      character*60 fname
      common/cfname/fname(nfilmx)
      common/cfiles/nunits(nfilmx)
      integer nlist, civoutin,civoutout
      equivalence (nunits(1),nlist)
      equivalence (nunits(16),civoutin)
      equivalence (nunits(17),civoutout)
c
c ## arguments

       integer nroot,ncsf,delsrc
       integer roots(maxvec)



c  ##  integer section
c
      integer cist, civcst
      integer i, indxst, iseg , ietype(mxnrgy), info(mxinfo),
     &        idum1, idum2
      integer lenci, lroot
      integer mxread, mxsegl
      integer ninfo, nenrgy
      integer nsegw, ntitle
      integer segci, segel
      integer versn,blksiz,iroot
c
c  ##  real*8 section
c
      REAL*8              energy(mxnrgy)
c
c  ##  character section
c
      CHARACTER*80        TITLE(MXTITL)
c
c-------------------------------------------------------------

      open( unit = civoutin, file = fname(16), status = 'unknown' ,
     +       form = 'unformatted' )
      rewind(civoutin)
c
      open( unit = civoutout, file = fname(17), status = 'unknown' ,
     +       form = 'unformatted' )
c

      call izero_wr(maxvec,roots,1)
c
      nroot=0
      iroot=0
 90   read (civoutin,end=99) versn,blksiz,ncsf
      write(civoutout) versn,blksiz,ncsf
      iroot=iroot+1
      if ( versn .ne. curver ) then
         call bummer('rdvhd:  wrong version number', versn, faterr)
      endif
c
c     # general information
      read (civoutin) ninfo, nenrgy, ntitle
      write (civoutout) ninfo, nenrgy, ntitle
c
c     info(1) : maximum overlap with reference vector # info(1)
c     info(2) : corresponding ci vector is record # info(2)
c     info(3) : method CI(0),AQCC(3),AQCC-LRT(30)
c     info(4) : if set to one this is the last record of civout
c     info(5) : overlap of reference vector and ci vector in percent
c
      mxread = min (ninfo, mxinfo)
      read (civoutin) (info(i), i=1,mxread)

      write(nlist,*) 'copying information from info file: '
      write(nlist,*) 'old CI record: ',info(2),' new CI record:',
     .                nroot+1
      nroot=nroot+1
      roots(nroot)=info(2)
      info(2)=nroot
      write (civoutout) (info(i), i=1,mxread)

c     # read titles:
      mxread = min (ntitle,mxtitl)
      read (civoutin) (title(i), i=1,mxread)
      write(civoutout) (title(i), i=1,mxread)
c
c     # read energies
      mxread = min(nenrgy,mxnrgy)
      read(civoutin) (ietype(i),i=1,mxread),(energy(i),i=1,mxread)
      write(civoutout) (ietype(i),i=1,mxread),(energy(i),i=1,mxread)

      write(nlist,15)
15    format(20('==='))
      write(nlist,*) 'ci vector:',info(2),' itype=',info(3),
     .       ' reference vector',info(1)
      write(nlist,*) 'titles:'
      do i=1, min(ntitle,mxtitl)
       write(nlist,*) title(i)
      enddo
      write(nlist,15)
      if (info(4).eq.0) goto 90
 99   continue
      write(nlist,500) (roots(i),i=1,nroot)
      write(nlist,501)
500   format(1x,40('==')/2x,'root mapping:',16i4/)
501   format(1x,40('=='))
      if (delsrc.eq.1) then
       close(civoutin,status='delete')
      else
      close(civoutin)
      endif
      close(civoutout)
      return
      end
