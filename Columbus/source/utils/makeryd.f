!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       program makeryd
c=============================================================
c  experimental version 0.1 Februar 15th 2000
c  written by: tm
c=============================================================
c things to do
c  1. extract Rydberg information from Aoinfo header
c  2. include averaging of several set of NOs
c  3. better output
c =============================================================
c     Based on a set of n (max. 10) sets of
c     MCSCF NO coefficient files
c     a set of Rydberg contractions is generated
c
c     procedure
c    * carry out a MCSCF calculations for the Cation
c          with Rydberg primitives present
c    * select the lowest virtual orbitals corresponding to
c          the desired Rydberg states
c      ======================================================
c    * read in MO coefficient file and extract those subblocks
c      that belong the Rydberg primitives
c    * read in the overlap matrix from the aoints file and
c      extract only those parts that belong to the Rydberg
c      primitives
c    * renormalize/orthogonalize the MO-subset
c    * generate an orthonormal basis covering the complete
c      Rydberg basis
c    * calculate the AO-density (the n selected Rydberg MOs
c      have their occupation set to one)
c    * set all lm/l'm' subblocks to zero
c    * average over the lm/lm' subblocks
c    * transform back to the new orthogonal basis
c    * diagonalize the averaged density
c    * transform the eigenvectors to the AO basis

c      all off-diagonal elements of the submatrices for S,P,D vanish
c      as well as all off-diagonal submatrix blocks
c      i.e. set all these elements to zero and all of the diagonal
c      elements are set to the corresponding D(s),D(p) and D(d) value
c      averaging is only done for m-components of the same CGTO !!
c        3s : no averaging at all :== D(s)
c        3p : D(ij)= 0 if i<>j
c             D(ii)= sum_j ( D(jj)*1/3 ) :== D(p)
c        3d : D(d) = .2*D(xy)+.2*D(xz)+.2*D(yz)+.4*D(xx)+.4*D(yy)+.4*D(z
c
c    input variables:
c     fnames = filename of the MOs to be averaged
c     factor = corresponding weight factors
c     nentry = number of densities
c     lrydmo = identifies the Rydberg states in the MO basis
c              in the order of irreps as pairs : # of first MO, # of Ryd
c     lrydao = identifies the Rydberg AOs  in terms of triples
c              in the order of irreps as pairs : # of first AO, # of Ryd
c     rydtype= identifies the type of Rydberg AOs in the same order
c              as lrydao with
c              type 1(s),2(px),3(py),4(pz),5(d-2),6(d-1),7(d0),8(d1),
c                   9(d2)
c
c e.g.
c $input
c  factor(1) = 1.0
c  fnames(1) = "mocoef"
c  lrydmo = 11 2 3 1 0 0 0 0 0 0 0 0 0 0 0 0
c  lrydao =  61 16 22 8 0  0 0 0  0 0 0 0
c  rydtype = 2 3 2 3 2 3 2 3
c            2 3 2 3 2 3 2 3
c            4 4 4 4 4 4 4 4
c  nentry=1
c $end
c
c  input MOs on mocoef
c  Rydberg MOs: #11,#12 of irrep 1, #3 of irrep 2
c  Rydberg AOs: #61 to #76 of irrep 1, #22 to #29 of irrep 2
c  Rydberg type: px and py to irrep 1 in alternating order followed
c                by those of pz type in irrep 2
c
c-----------------------------------------------------------------------



         implicit none 
         integer nmax
        integer maxryd
        parameter ( maxryd=80,nmax = 20)
        character*80 fmoc,fn,fnames(nmax)
        real*8  factor(nmax)
        integer nentry
        integer nryd,lrydmo(8*2),lrydao(8*2),rydtype,plotmo(8*2)
        common /datain/ factor,fnames,fmoc,nentry,lrydmo,lrydao
     .   ,nryd,rydtype(maxryd),plotmo
         integer nbfpsy(8),nrydpsy(8),mrydpsy(8),nsym,ntot
        common /info1/ nbfpsy,nrydpsy,mrydpsy,nsym,ntot
        real*8 rydov(maxryd*maxryd),rydoldmo(maxryd*maxryd),
     .                   ryddensao(maxryd*maxryd)
     .           ,rydnewmo(maxryd*maxryd),ryddensmo(maxryd*maxryd)
     .                   , eigenvalues(maxryd)

        integer nmomx
        parameter (nmomx=510)
        double precision
     .   scr1(nmomx*nmomx),scr2(nmomx*nmomx)

        integer off2,offmo,offtype,off,offov,i,ierr,k,j,ij
        real*8 tfact,sum
c
c       read namelist input
c
        open (unit=5,file='makerydin',status='old')
        open (unit=6,file='makerydls',status='unknown')

c ======================================================================
c read overlap matrix and extract rydberg part
c

       call wzero(nmomx*nmomx,scr1,1)
       call getoverlap(scr1,nmomx*nmomx)

       call plblks('origov',scr1,nsym,nbfpsy,'AO',3,6)

       call readinput

       call getrydov (scr1,rydov,nrydpsy,lrydao,nsym,nbfpsy,rydtype)

c       remove coupling blocks in density
c
c       call decrydov (scr1,rydov,nrydpsy,lrydao,nsym,nbfpsy,rydtype)
       call plblks('Rydov',rydov,nsym,nrydpsy,'AO',3,6)
c ======================================================================
c read MO coefficient file and extract rydberg part
c
       call readmo(20,fmoc,scr1,scr2,1,1.0d0)

       call getrydmo (scr1,rydoldmo,nrydpsy,lrydmo,lrydao,nsym,nbfpsy)

       open (unit=15,file='gnu.xxx',status='unknown')
       call  makeplotmo(scr1,scr2,nrydpsy,plotmo,lrydao,nsym,nbfpsy)

       call prblks('rydoldmo',rydoldmo,nsym,nrydpsy,mrydpsy,
     .  'ao   ','no   ',3,6)

c=======================================================================
c orthonormalize the rydberg part of MOs
c

c       goto 111
       off=1
       offov=1
       do i=1,nsym
       if (nrydpsy(i)*mrydpsy(i).gt.0) then
       call  ortmgs( nrydpsy(i), mrydpsy(i), rydoldmo(off),
     .               1, rydov(offov), scr1, tfact, ierr )
       if (ierr.lt.0) call bummer( 'ortmgs not successful ierr=',ierr,2)
       if (ierr.gt.0)
     .   call bummer( 'ortmgs vector not normalized=',ierr,0)
       endif
       off=off+nrydpsy(i)*mrydpsy(i)
       offov=offov + nrydpsy(i)*(nrydpsy(i)+1)/2
       enddo

       call prblks('rydoldmo (ren)',rydoldmo,nsym,nrydpsy,mrydpsy,
     .  'ao   ','mo   ',3,6)

 111   continue

c ======================================================================
c      transform MO-Density to AO basis
c      and set lm/l'm' offdiagonal blocks to zero
c
c      D[AO,AO] = C[AO,MO] * D[MO,MO] * CT[AO,MO] * S[AO,AO]
c

        off=1
        offov=1
        offtype=1
        do i=1,nsym
         call calcdens(rydoldmo(off),rydov(offov),nrydpsy(i),
     .                 mrydpsy(i),ryddensao(offov),rydtype(offtype))
        off=off+nrydpsy(i)*mrydpsy(i)
        offov=offov + nrydpsy(i)*(nrydpsy(i)+1)/2
        offtype=offtype+nrydpsy(i)
        enddo

       call plblks('ryddensao',ryddensao,nsym,nrydpsy,'AO',3,6)
       write(*,*) 'Ao density trace:'
       call trace (ryddensao,nsym,nrydpsy)


c ======================================================================
c  produce an orthogonal set of MOs filling the rydberg space
c
c

        off=1
        offov=1
        call wzero(maxryd*maxryd,rydnewmo,1)
        do i=1,nsym
        if (nrydpsy(i).gt.0 ) then
         k=-1
         do j=1,nrydpsy(i)
          k=k+1
          rydnewmo(off+k)=1.0d0
          k=k+nrydpsy(i)
         enddo
         call  ortmgs( nrydpsy(i), nrydpsy(i), rydnewmo(off),
     .               1, rydov(offov), scr1, tfact, ierr )
       if (ierr.lt.0) call bummer( 'ortmgs not successful ierr=',ierr,2)
       if (ierr.gt.0)
     .   call bummer( 'ortmgs vector not normalized=',ierr,0)
        endif
       off=off+nrydpsy(i)*nrydpsy(i)
       offov=offov + nrydpsy(i)*(nrydpsy(i)+1)/2
       enddo
       call prblks('rydnewmo',rydnewmo,nsym,nrydpsy,nrydpsy,
     .  'ao   ','mo   ',3,6)


c ======================================================================
c      spherical averaging of the AO-density
c

         call averp(ryddensao,nrydpsy,nsym,rydtype)

         call plblks('densav',ryddensao,nsym,nrydpsy,'AO',3,6)

         write(*,*) 'averaged Ao density trace:'
         call trace (ryddensao,nsym,nrydpsy)
c ======================================================================
c  transform averaged AO density to newnos' basis
c
c  D[MO,MO] = CT[AO,MO]*S[AO,AO]*D[AO,AO]*C[AO,MO]
c
        off=1
        offov=1
        do i=1,nsym
         call calcmod(rydnewmo(off),rydov(offov),nrydpsy(i),
     .                 nrydpsy(i),ryddensao(offov),ryddensmo(offov))
        off=off+nrydpsy(i)*nrydpsy(i)
        offov=offov + nrydpsy(i)*(nrydpsy(i)+1)/2
        enddo

        call plblks('modensity',ryddensmo,nsym,nrydpsy,'AO',3,6)
        call trace (ryddensmo,nsym,nrydpsy)

c=======================================================================
c  Diagonalize the density in the orthonormal MO basis
c

         offov=1
         off =1
         do i=1,nsym
         call wzero(nrydpsy(i)*nrydpsy(i),scr1(off),1)
         call dcopy_wr(nrydpsy(i)*(nrydpsy(i)+1)/2,
     .                 ryddensmo(offov),1,scr2(offov),1)
         if (nrydpsy(i).gt.0) then
         k=0
         do j=1,nrydpsy(i)
          scr1(k+off)=1.0d0
          k=k+nrydpsy(i)+1
         enddo
c       call plblks('imodens',scr2(offov),1,nrydpsy(i),'MO',3,6)
c        call prblks('icoef',scr1(off),1,nrydpsy(i),nrydpsy(i),
c    .               'ao  ','no  ',3,6)
         call erduw(scr2(offov),scr1(off),nrydpsy(i),1.d-8)
         endif
         offov=offov + nrydpsy(i)*(nrydpsy(i)+1)/2
         off=off + nrydpsy(i)*nrydpsy(i)
         enddo

c        call prblks('eigenvectors',scr1,nsym,nrydpsy,nrydpsy,
c    .               'ao  ','no  ',3,6)
c        call plblks('eigval',scr2,nsym,nrydpsy,'AO',3,6)

          ij=0
           k=0
           off=1
           call wzero(maxryd,eigenvalues,1)
          do i=1,nsym
            do j=1,nrydpsy(i)
            ij=ij+1
            k=k+j
            if (scr2(k).gt.1.d-6) eigenvalues(ij)=scr2(k)
            if (scr2(k).lt.-1.d-6) then
             write(*,*) 'Warning significant negative eigenvalue',
     .     'for a positive semidefinit matrix!'
             write(*,*) 'sym=',i,'bfn=',j,'value=',scr2(k)
            endif
            enddo
             call bubble(scr1(off),eigenvalues(ij-nrydpsy(i)+1),
     .          nrydpsy(i),nrydpsy(i))
           off=off+nrydpsy(i)*nrydpsy(i)
           enddo

          ij=0
          do i=1,nsym
           write(*,*) 'Eigenvalues (irrep#',i,')=',
     .                 (eigenvalues(j),j=ij+1,ij+nrydpsy(i))
           ij=ij+nrydpsy(i)
          enddo
           sum=0.0d0
           do i=1,ij
           sum=sum+eigenvalues(i)
           enddo
           write(*,*) 'sum of positive eigenvalues:',sum
           write(*,*) 'diagonalized density trace:'
           call trace (scr2,nsym,nrydpsy)

            call prblks('eigenvectors',scr1,nsym,nrydpsy,nrydpsy,
     .               'mo  ','no  ',3,6)
c ======================================================================
c    select the eigenvectors with the highest eigenvalues
c    transform them to the AO basis and print out the
c    coefficients
c
c
c         c[ao,no]= c[ao,mo]*c[mo,no]
c           scr2  =  rydnewmo  * scr1

         off=1
         off2=1
         do i=1,nsym
          call calccoef(scr2(off2),rydnewmo(off),scr1(off),
     .          nrydpsy(i),mrydpsy(i))
         off=off+nrydpsy(i)*nrydpsy(i)
         off2=off2+nrydpsy(i)*mrydpsy(i)
         enddo
c
c
         write(*,*) '================================================='
         write(*,*) '=  final contraction coefficients               ='
         write(*,*) '================================================='

         call prblks('coefficients',scr2,nsym,nrydpsy,mrydpsy,
     .               'ao  ','no  ',3,6)

        off=0
        do i=1,nsym
          do j=1,mrydpsy(i)
          write(15,*) '# Irrep', i,' : final coeff no  ',j
          off=off+1
          write(15,100) 'a= ',scr2(off)
100       format (a,3x,f10.6)
          off=off+1
          write(15,100) 'b= ',scr2(off)
          off=off+1
          write(15,100) 'c= ',scr2(off)
          off=off+1
          write(15,100) 'd= ',scr2(off)
          off=off+1
          write(15,100) 'e= ',scr2(off)
          off=off+1
          write(15,100) 'f= ',scr2(off)
          off=off+1
          write(15,100) 'g= ',scr2(off)
          off=off+1
          write(15,100) 'h= ',scr2(off)
         enddo
         enddo
         close(15)

c=======================================================================
c
c      write coefficients in a format compatible with daltaoin
c
c        call writedalt(scr2,nsym,nrydpsy,mrydpsy,rydtype)
c


       call bummer('normal termination',0,3)
       stop 999


        end


        subroutine readinput

         implicit none 
         integer nmax
        integer maxryd
        parameter ( nmax = 20,maxryd=80)
        character*80 fmoc,fnames(nmax)
        real*8  f2,factor(nmax)
        integer nentry,lrydmo(8*2),lrydao(8*2),nryd,rydtype(maxryd)
        integer plotmo(8*2)
        namelist /input/factor,fnames,fmoc,nentry,lrydmo,lrydao,
     .     rydtype,plotmo
        common /datain/ factor,fnames,fmoc,nentry,lrydmo,lrydao
     .   ,nryd,rydtype,plotmo
         integer nmomx,nsymmx
         parameter (nmomx=510,nsymmx=8)
         integer snsym,snbfpsy(nsymmx),snmopsy(nsymmx),itype(nmomx)
         common /check/ snsym,snbfpsy,snmopsy,itype

         integer nbfpsy(8),nrydpsy(8),mrydpsy(8),nsym,ntot
        common /info1/ nbfpsy,nrydpsy,mrydpsy,nsym,ntot

        character*2 map(1:10)
        integer ierr,i,nryd2,j
        integer faterr
        parameter (faterr=2)
        data map / 's','x ','y ','z ', 'xx','yy','zz','xy','xz','yz' /
        call header ( 'readinput',1)

        call echoin (5,6,ierr)
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind 5
          do i=1,nmax
            fnames(i)=" "
            factor(i)=0.0d0
            fmoc=" "
            nentry=0
          enddo
        call izero_wr(8*2,lrydmo,1)
        call izero_wr(8*2,lrydao,1)

         read (5,input,end=3)

   3     continue

         close(5)
c        renormalization
         f2=0.0d0
         do i=1,nentry
           f2=factor(i)+f2
         enddo

         do i=1,nentry
           factor(i) = factor(i)/f2
         enddo
         fmoc=fnames(1)
         write(6,*) 'Standard MO basis file :',fmoc
         write(6,*) 'NO coefficient file   averaging factor '
         do i=1,nentry
          write(6,'(a20,3x,f6.3)') fnames(i)(1:20), factor(i)
         enddo

         write(*,*) 'lrydmo=',lrydmo
         write(*,*) 'lrydao=',lrydao

         write(6,*) 'List of Rydberg MOs in MO basis:'
         nryd=0
         do i=1,8*2,2
         if (lrydmo(i+1).ne.0) then
              nryd=nryd+lrydmo(i+1)
              mrydpsy((i+1)/2)=lrydmo(i+1)
             write(*,*) 'irrep:',i,
     .      ' MO numbers:', (j,j=lrydmo(i),lrydmo(i)+lrydmo(i+1)-1)
         endif
         enddo
 1000    continue

         write(6,*) 'List of Rydberg AOs in MO basis:'
         nryd=0
         do i=1,8*2,2
         if (lrydao(i+1).ne.0) then
             write(*,*) 'irrep:',(i+1)/2,
     .      ' AO numbers:', (j,j=lrydao(i),lrydao(i)+lrydao(i+1)-1)
             nrydpsy((i+1)/2)=lrydao(i+1)
             write(*,*) 'type:',
     .        (map(rydtype(j)),j=nryd+1,nryd+lrydao(i+1))
             write(*,*) 'type:',
     .        ((rydtype(j)),j=nryd+1,nryd+lrydao(i+1))
              nryd=nryd+lrydao(i+1)
         endif
         enddo
 1001    continue


         write(*,*) 'nrydpsy(*)=',(nrydpsy(i),i=1,nsym)
         write(*,*) 'mrydpsy(*)=',(mrydpsy(i),i=1,nsym)
         write(*,*) 'plotmo(*)=',(plotmo(i),i=1,nsym)


        call header ( 'readinput',0)
         return
         end



         subroutine readmo(unitno,fname,nocoef,occ,noocc,fac)
          implicit none
c        noocc=0   set occupation vector
c        noocc=1   don't set occupation vector
         integer nmomx
         parameter ( nmomx=510)
         real*8 nocoef(nmomx*nmomx),occ(nmomx),fac
         character*80 fname
         integer unitno,noocc
         integer mxtitle,maxryd
         parameter (mxtitle = 10,maxryd=80)
         character*80 titles(mxtitle)
         integer filerr,syserr,ntitle
         integer nsymmx
         parameter (nsymmx = 8)
         integer nsym,nbfpsy(nsymmx),nmopsy(nsymmx)
         character*4 labels(nmomx)
         character*80 afmt

         integer snsym,snbfpsy(nsymmx),snmopsy(nsymmx),itype(nmomx)
         common /check/ snsym,snbfpsy,snmopsy,itype

        integer nmax
        parameter ( nmax=20)
        character*80 fmoc,fn,fnames(nmax)
        real*8  factor(nmax)
        integer plotmo,nentry
        integer lrydmo(8*2),lrydao(8*2),nryd,rydtype(maxryd)
        common /datain/ factor,fnames,fmoc,nentry,lrydmo,lrydao
     .   ,nryd,rydtype,plotmo(8*2)

         integer nrydpsy(8),mrydpsy(8)
        common /info1/ nbfpsy,nrydpsy,mrydpsy,nsym,ntot


          integer imap(510),off,j,cnt,cnt2,i,ntot,cntryd
         logical first
         data first /.true./
         save first


          call header ('readmo',1)
         write(*,*) 'readmo: opening ',fname
c  header information
         open (unit=unitno,file=fname,status='old',err=99)
         call moread(unitno,10,filerr,syserr,mxtitle,ntitle,
     .    titles,afmt,nsym,nbfpsy,nmopsy,labels,nmomx*nmomx,
     .     nocoef )

          ntot=0
          if (first) then
           snsym=nsym
           do i=1,nsym
             snbfpsy(i)=nbfpsy(i)
             snmopsy(i)=nmopsy(i)
             ntot=ntot+nmopsy(i)
           enddo
          else
           if (snsym.ne.nsym)
     .      call bummer ('readmo: snsym.ne.nsym',snsym,2)
           do i=1,nsym
             if (snbfpsy(i).ne.nbfpsy(i))
     .       call bummer ('readmo: snbfpsy(i).ne.nbfpsy(i),i=',i,2)
             if (snmopsy(i).ne.nmopsy(i))
     .       call bummer ('readmo: snmopsy(i).ne.nmopsy(i),i=',i,2)
           enddo
          endif

c mo coefficients
         call moread(unitno,20,filerr,syserr,mxtitle,ntitle,
     .    titles,afmt,nsym,nbfpsy,nmopsy,labels,nmomx*nmomx,
     .     nocoef )

         return
  99     continue
         call bummer ('couldnt open nocoef file',0,2)
         return
         end


      subroutine bubble ( c, e, n, m )
c
c  sort n energies and n vectors (of length m)
c  decreasing order
c
      implicit real*8 (a-h,o-z)
c
      dimension c(*), e(*)
c
      if ( n .eq. 1 ) return
      ipt = 0
c
      do 40 i= 1, n-1
      low = i
      elow = e( i )
c
      do 10 j= i+1 , n
      if ( e(j) .lt. elow ) go to 10
      low = j
      elow = e( j )
   10 continue
c
      if ( low .eq. i ) go to 30
c
c  swap low vector and energy
c
      tmp = e( i )
      e( i ) = e( low )
      e( low ) = tmp
      jpt = ( low-1 ) * m
      do 20 k= 1, m
      tmp = c( ipt+k )
      c( ipt+k ) = c( jpt+k )
      c( jpt+k ) = tmp
   20 continue
c
   30 continue
      ipt = ipt + m
   40 continue
c
      return
      end


      subroutine erduw(a,b,na,epslon)
c taken mainly from version 4, aug., 1971, of jacscf, u. of wa.
c   matrix diagonalization by the jacobi method.
c     a = real symmetric matrix to be diagonalized. it is stored
c       by columns with all sub-diagonal elements omitted, so a(i,j) is
c       stored as a((j*(j-1))/2+i).
c     b = matrix to be multiplied by the matrix of eigenvectors.
c     na = dimension of the matrices.
c      epslon is the convergence criterion for off-diagonal elements.
c     modified nov 82 r.a.
c     programm keeps track of non-diagonal norm and adjusts thresh
c     dynamically. the eventually quadratic convergence of jacobi is
c     exploited to reduce thresh faster at the end.
c
      implicit real*8  (a-h,o-z)
      dimension a(1), b(1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      data a0, a1s2 /0.0d0, 0.5d0/
c
      if(na.eq.1) return
      loopc=64
      sumnd=a0
      sum=a0
      ij=1
      do 24 i=1,na
      do 16 j=1,i
      term=a(ij)*a(ij)
      sum=sum+term
      if(i.eq.j) go to 16
      sumnd=sumnd+term
   16 ij=ij+1
   24 continue
      thrshg= sqrt((sum+sumnd)/na)*epslon
      small=sumnd*epslon*na*na
   32 if (sumnd.lt.small) go to 35
      thresh= sqrt(sumnd+sumnd)/na
      go to 37
   35 thresh=thresh*0.03d0
   37 thresh=dmax1(thresh,thrshg)
      n=0
      ij=2
      jj=1
      do 112 j=2,na
      jj=jj+j
      jm1=j-1
      ii=0
      do 104 i=1,jm1
      ii=ii+i
      if( abs(a(ij)).lt.thresh) go to 104
      n=n+1
      sumnd=sumnd-a(ij)*a(ij)
      sum=a1s2*(a(jj)+a(ii))
      term=a1s2*(a(jj)-a(ii))
      amax= sign( sqrt(term*term+a(ij)*a(ij)),term)
      c= sqrt((amax+term)/(amax+amax))
      s=a(ij)/(c*(amax+amax))
      a(ii)=sum-amax
      a(jj)=sum+amax
      a(ij)=a0
      im1=i-1
      if(im1) 40,56,40
   40 ki=ii-i
      kj=jj-j
      do 48 k=1,im1
      ki=ki+1
      kj=kj+1
      term=c*a(ki)-s*a(kj)
      a(kj)=s*a(ki)+c*a(kj)
   48 a(ki)=term
   56 if(jm1.eq.i) go to 72
      ip1=i+1
      ik=ii+i
      kj=ij
      do 64 k=ip1,jm1
      kj=kj+1
      term=c*a(ik)-s*a(kj)
      a(kj)=s*a(ik)+c*a(kj)
      a(ik)=term
   64 ik=ik+k
   72 if(j.eq.na) go to 88
      jp1=j+1
      ik=jj+i
      jk=jj+j
      do 80 k=jp1,na
      term=c*a(ik)-s*a(jk)
      a(jk)=s*a(ik)+c*a(jk)
      a(ik)=term
      ik=ik+k
   80 jk=jk+k
   88 ki=im1*na
      kj=jm1*na
      do 96 k=1,na
      ki=ki+1
      kj=kj+1
      term=c*b(ki)-s*b(kj)
      b(kj)=s*b(ki)+c*b(kj)
   96 b(ki)=term
  104 ij=ij+1
  112 ij=ij+1
      loopc=loopc-1
      if(loopc) 120,1024,120
  120 if(thresh.le.thrshg.and.n.eq.0) return
      go to 32
 1024 write (*,2048)
      call bummer('erduw: loopc=',loopc,faterr)
 2048 format(12h error erduw)
      return
      end



*@ifdef debug
*       subroutine calcinv (matrix,invmatrix)
*       implicit 
*       integer nmomx
*       parameter (nmomx=510)
*       real*8 matrix(nmomx*nmomx),invmatrix(nmomx*nmomx)
*       real*8 overlap(nmomx*nmomx)
*         integer nsymmx
*         parameter (nsymmx = 8)
*         integer naux
*        parameter (naux=2000)
*        real*8 aux(naux)
*         integer nsym,nbfpsy(nsymmx),nmopsy(nsymmx),itype(nmomx)
*         common /check/ nsym,nbfpsy,nmopsy,itype
*        integer i,off,ierr,ipiv(nmomx)
*       off=1
*       do i=1,nsym
*        call dcopy(nmopsy(i)*nmopsy(i),matrix(off),1,
*     .   invmatrix(off),1)
*       call dgetrf_wr(nmopsy(i),nmopsy(i),invmatrix(off),nmopsy(i),
*     .       ipiv, ierr)
*        if (ierr.ne.0)
*     .  call bummer('dgetrf, ierr=',ierr,2)
*          ierr=0
*        call dgetri_wr(nmopsy(i),invmatrix(off),nmopsy(i),ipiv,aux,
*     .    naux,ierr)
*        if (ierr.ne.0)
*     .  call bummer('dgetri, ierr=',ierr,2)
*        call mxmov(matrix(off),invmatrix(off),overlap(off),
*     .      nmopsy(i))
*        off=off+nmopsy(i)*nmopsy(i)
*       enddo
*         call prblks('stdmo',matrix,nsym,nmopsy,
*     .    nmopsy,'no  ','ao  ',3,6)
*         call prblks('invstdmo',invmatrix,nsym,nmopsy,
*     .    nmopsy,'no  ','ao  ',3,6)
*         call prblks('overlap',overlap,nsym,nmopsy,
*     .    nmopsy,'ao  ','ao  ',3,6)
*       return
*       end
*@endif


         subroutine header( str,in)
         character*(*) str
         integer in

         write(*,*) '=================================================='
         if (in.eq.1) then
          write(*,* ) '        entering ', str
         else
          write(*,* ) '        exiting  ', str
         endif
         write(*,*) '================================================='
         return
         end


       subroutine getoverlap(ov,idim)

      implicit integer(a-z)
c
      integer   lenbuf,       n1eabf,       n2eabf,       nbfmxp
      parameter(lenbuf=16384, n1eabf=13104, n2eabf=10922, nbfmxp=510)
      integer idem
      real*8 ov(idim),fcore
c
c     # bitvector length must be rounded up to a multiple of 64.
      integer   lenbv
      parameter(lenbv=((n2eabf+63)/64)*64)
      integer kntin(8*9/2),symb(nbfmxp),mapin(nbfmxp)
      integer lasta,lastb,last
c
      character*80 title(20)
      real*8 buffer(lenbuf)
      integer labels(2,n1eabf)
      real*8 values(n1eabf)
      integer ibitv(lenbv)
      integer ietype(20),info(10)
      integer imtype(1),map(1)
      real*8 energy(20)
      character*4 slabel(8)
      character*8 bfnlab(nbfmxp)
      character*60 fname
c
      integer nin, nlist, aoints, ntitle, ierr, nmap, nenrgy,
     & ninfo, nbft,  i, idummy
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

      integer nbpsy(8),nrydpsy(8),mrydpsy(8),nsym,ntot
      common /info1/ nbpsy,nrydpsy,mrydpsy,nsym,ntot
c
      real*8   sifsce
      external sifsce
c
      nin   = 5
      nlist = 6
      aoints= 10
c
      fname = 'aoints'
      open(unit=aoints,file=fname,form='unformatted',status='old')
c
c     # read the header info.
c
      call sifrh1( aoints, ntitle, nsym, nbft,
     & ninfo, nenrgy, nmap, ierr )
c
      if(ierr.ne.0)then
         call bummer('istat: ierr=',ierr,faterr)
      elseif(ntitle.gt.20)then
         call bummer('istat: ntitle=',ntitle,faterr)
      elseif(nbft.gt.nbfmxp)then
         call bummer('istat: nbft=',nbft,faterr)
      elseif(ninfo.gt.10)then
         call bummer('istat: ninfo=',ninfo,faterr)
      elseif(nenrgy.gt.10)then
         call bummer('istat: nenrgy=',nenrgy,faterr)
      endif
c
c     # ignore map vectors, if any...
      nmap = 0
      call sifrh2( aoints, ntitle, nsym, nbft,
     & ninfo, nenrgy, nmap, title,
     & nbpsy, slabel, info, bfnlab,
     & ietype, energy, imtype, map,    ierr )
      if ( ierr.ne.0 ) call bummer('istat: ierr=',ierr,faterr)
c
      write(nlist,6020)(title(i),i=1,ntitle)
6020  format(/' file header information:'/(1x,a))
c
      write(nlist,6030)
6030  format(/' core energies:')
      call sifpre( nlist, nenrgy, energy, ietype )
c
      write(nlist,6032) sifsce(nenrgy, energy, ietype )
6032  format(/' total core energy =',1pe20.12)
c
      write(nlist,6040) nsym, nbft
6040  format(/' nsym =',i2,' nbft=',i4/)
      write(nlist,6050)'symmetry  =',(i,i=1,nsym)
      write(nlist,6060)'slabel(*) =',(slabel(i),i=1,nsym)
      write(nlist,6050)'nbpsy(*)  =',(nbpsy(i),i=1,nsym)
6050  format(1x,a,t15,8i5)
6060  format(1x,a,t15,8(1x,a4))
      write(nlist,6070)(info(i),i=1,ninfo)
6070  format(/' info(*)=',10i6)
      write(nlist,6080)(i,bfnlab(i),i=1,nbft)
6080  format(/' i:bfnlab(i) ='/ (5(i4,':',a8)) )
c
c     # check record length parameters...
c
      if(info(1).ne.1 .and. info(1).ne.2 )then
         call bummer('istat: fsplit=',info(1),faterr)
      elseif(info(2).gt.lenbuf)then
         call bummer('istat: l1rec=',info(2),faterr)
      elseif(info(3).gt.n1eabf)then
         call bummer('istat: n1max=',info(3),faterr)
      elseif(info(4).gt.lenbuf)then
         call bummer('istat: l2rec=',info(4),faterr)
      elseif(info(5).gt.n2eabf)then
         call bummer('istat: n2max=',info(5),faterr)
      endif
c
       fcore=0.0d0
       do i=1,nbfmxp
        mapin(i)=i
       enddo
       call izero_wr(8*9/2,kntin,1)

       call  sifr1x(
     & aoints,  info,    0,  0,
     & nsym,    nbpsy,   idum,  buffer,
     & values,  labels,  mapin,   symb,
     & idim,    ov,   fcore,   kntin,
     & lasta,   lastb,   last,    nrec,
     & ierr )
       if (ierr.ne.0)
     &  call bummer('sifr1x ierr=',ierr,2)
       return
       end


       subroutine getrydov (ov,rydov,nrydpsy,lrydao,nsym,nbfpsy,type)
        implicit none 
        real*8 ov(*),rydov(*)
       integer nsym,nrydpsy(nsym),lrydao(2*nsym),nbfpsy(nsym)
       integer type(*)
       integer dimao,mu,nu,i,j,k,offov,offrydov

        offov=0
        offrydov=1
        do i=1,nsym
          do j=1,lrydao(i*2-1)-1
           offov = offov + j
          enddo
          do j=lrydao(i*2-1), lrydao(i*2-1)+lrydao(i*2)-1
           do k = lrydao(i*2-1),j
            rydov(offrydov)=ov(offov+k)
            offrydov=offrydov+1
           enddo
           offov=offov+j
          enddo
          do j=max(lrydao(i*2-1)+lrydao(i*2),1), nbfpsy(i)
           offov=offov+j
          enddo
         enddo

         return
         end



       subroutine getrydmo (stdmo,rydstdmo,nrydpsy,lrydmo,lrydao,
     .                      nsym,nbfpsy)
        implicit none 
        real*8 stdmo(*),rydstdmo(*)
       integer nsym,nrydpsy(nsym),lrydao(2*nsym),lrydmo(2*nsym),
     .         nbfpsy(nsym)

       integer i,j,k,off,offryd

        off=0
        offryd=1
        do i=1,nsym
         do j=1,lrydmo(i*2-1)-1
           off=off+nbfpsy(i)
         enddo
         do j=lrydmo(i*2-1), lrydmo(i*2-1)+lrydmo(i*2)-1
           off = off + lrydao(i*2-1)-1
           do k= lrydao(i*2-1),lrydao(i*2-1)+lrydao(i*2)-1
           off=off+1
           rydstdmo(offryd)=stdmo(off)
           offryd=offryd+1
           enddo
           off = off + nbfpsy(i)-lrydao(i*2-1)-lrydao(i*2)+1
         enddo
         do j=max(lrydmo(i*2-1)+lrydmo(i*2),1),nbfpsy(i)
          off=off+nbfpsy(i)
         enddo
        enddo
        return
        end


         subroutine calcdens(mo,ov,dimao,dimmo,d,type)
          implicit none 
          integer maxryd
         parameter (maxryd=80)
         integer dimao ,dimmo
         real*8 mo(dimao,dimmo),ov(*),d(dimao*dimao)
         integer k,mu,nu,i,lamda,j,type(dimao)
         real*8 s(maxryd,maxryd)

c
c        D(i,j) =  C(AO,MO) * D(MO,MO)*CT(AO,MO) * S(AO,AO)
c
c        D(i,j) =  sum(a,b) C(i,a)*D(a,b)*CT(c,b) *S(b,j)
c               =  sum(a,b) C(i,a)*D(a,a)o

         call wzero(dimao*dimao,s,1)
         k=0
         do i=1,dimao
          do j=1,i
           k=k+1
           s(i,j)=ov(k)
           s(j,i)=ov(k)
          enddo
         enddo
c         do i=1,dimao
c         s(i,i)=1.0d0
c         enddo

c        write(*,*) 'dimao,dimmo=',dimao,dimmo
c        write(*,*) 'mo(1,1),mo(2,1),mo(3,1)=',mo(1,1),mo(2,1),mo(3,1)
         k=0
         do mu=1,dimao
          do nu=1,mu
            k=k+1
            d(k) =0.0d0
             do i=1,dimmo
               do lamda=1,dimao
               d(k) = mo(mu,i)*mo(lamda,i)*s(lamda,nu) +d(k)
               enddo
             enddo
           enddo
          enddo

c
c         return

           k=0
           do mu=1,dimao
             do nu=1,mu
              k=k+1
              if (type(mu).ne.type(nu)) then
               d(k)=0.0d0
              endif
             enddo
            enddo
          return
          end




         subroutine averp(d,npsy,nsym,rydtype)

          implicit none 
           real*8 d(*)
          integer nsym, npsy(nsym),rydtype(*)

          integer i,j,k,l
          real*8 scr(6)
          integer px(8),py(8),pz(8)
          integer isym(50)
          integer ipx,ipy,ipz,offsym(8),offbfn(8)
          integer offxsym,offysym,offzsym,offxbf,offybf,offzbf
          integer  xsym,ysym,zsym
          integer xxsym,yysym,zzsym,xysym,xzsym,yzsym
          integer offxxsym,offyysym,offzzsym,offxysym,offxzsym,
     .            offyzsym,offxxbf,offyybf,offzzbf,offxybf,
     .            offxzbf,offyzbf
          integer dxx(8),dyy(8),dzz(8),dxy(8),dxz(8),dyz(8)
          integer idxx,idyy,idzz,idxy,idxz,idyz

c initialisation

          call izero_wr(50,isym,1)
          call izero_wr(8,offsym,1)
          call izero_wr(8,offbfn,1)
          call izero_wr(8,px,1)
          call izero_wr(8,py,1)
          call izero_wr(8,pz,1)
          call izero_wr(8,dxx,1)
          call izero_wr(8,dyy,1)
          call izero_wr(8,dzz,1)
          call izero_wr(8,dxy,1)
          call izero_wr(8,dxz,1)
          call izero_wr(8,dyz,1)

c         1 = s
c         2,3,4  px,py,pz
c         5,6,7,8,9,10 dxx,dyy,dzz,dxy,dxz,dyz
c

          k=0
          ipx=0
          ipy=0
          ipz=0
          idxx=0
          idyy=0
          idzz=0
          idxy=0
          idxz=0
          idyz=0

          do i=1,nsym
           do j=1,npsy(i)
            k=k+1
            isym(k)=i
            if (rydtype(k) .eq. 2) then
                ipx=ipx+1
                px(ipx)=k
            endif
            if (rydtype(k) .eq. 3) then
                ipy=ipy+1
                py(ipy)=k
            endif
            if (rydtype(k) .eq. 4) then
                ipz=ipz+1
                pz(ipz)=k
            endif
            if (rydtype(k) .eq. 5) then
                idxx=idxx+1
                dxx(idxx)=k
            endif
            if (rydtype(k) .eq. 6) then
                idyy=idyy+1
                dyy(idyy)=k
            endif
            if (rydtype(k) .eq. 7) then
                idzz=idzz+1
                dzz(idzz)=k
            endif
            if (rydtype(k) .eq. 8) then
                idxy=idxy+1
                dxy(idxy)=k
            endif
            if (rydtype(k) .eq. 9) then
                idxz=idxz+1
                dxz(idxz)=k
            endif

            if (rydtype(k) .eq. 10) then
                idyz=idyz+1
                dyz(idyz)=k
            endif
           enddo
          enddo

           offbfn(1)=0
           offsym(1)=0
            do i=2,nsym
             offbfn(i)=offbfn(i-1)+npsy(i-1)
             offsym(i)=offsym(i-1)+(npsy(i-1)+1)*npsy(i-1)/2
            enddo
            write(*,*) 'offbfn:',offbfn
            write(*,*) 'offsym:',offsym
            write(*,*) 'isym:',isym
            write(*,*) 'px:',px
            write(*,*) 'py:',py
            write(*,*) 'pz:',pz
            write(*,*) 'dxx:',dxx
            write(*,*) 'dyy:',dyy
            write(*,*) 'dzz:',dzz
            write(*,*) 'dxy:',dxy
            write(*,*) 'dxz:',dxz
            write(*,*) 'dyz:',dyz

c
c           arrays pz,py,px enthalten die basisfunktionsnummern
c           der px,py,pz Orbitale
c

            if ( (ipx.ne.ipy).or.(ipx.ne.ipz))
     .      call bummer ( ' ipx.ne.ipy.ne.ipz ',0,2)

            xsym=isym(px(1))
            ysym=isym(py(1))
            zsym=isym(pz(1))
            offxsym=offsym(xsym)
            offysym=offsym(ysym)
            offzsym=offsym(zsym)
            offxbf=offbfn(xsym)
            offybf=offbfn(ysym)
            offzbf=offbfn(zsym)

c=======================================================================
c                      p-Orbital averaging
c  type 1 2 3
c
            do i=1,ipx
             do j=1,i
c
c            i>=j
c          El(i,j) = offsym(isym(i)) + (i-offbfn(isym(i))*(i-1-offbfn(is
c                                    + (j-offbfn(isym(j))
c
*@ifdef debug
*             write(*,*) 'i,j,xind,yind,zind=',
*     .  (offxsym + (px(i)-offxbf)*(px(i)-1-offxbf)/2 + px(j)-offxbf),
*     .  (offysym + (py(i)-offybf)*(py(i)-1-offybf)/2 + py(j)-offybf),
*     .  (offzsym + (pz(i)-offzbf)*(pz(i)-1-offzbf)/2 + pz(j)-offzbf)
*@endif
       scr(1) =
     .  d(offxsym + (px(i)-offxbf)*(px(i)-1-offxbf)/2 + px(j)-offxbf)
       scr(2) =
     .  d(offysym + (py(i)-offybf)*(py(i)-1-offybf)/2 + py(j)-offybf)
       scr(3) =
     .  d(offzsym + (pz(i)-offzbf)*(pz(i)-1-offzbf)/2 + pz(j)-offzbf)

*@ifdef debug
*             write(*,*) 'ipx: i,j,scr(*)=',i,j,scr
*@endif
             scr(1) = (scr(1)+scr(2)+scr(3))/3.0d0
      d(offxsym + (px(i)-offxbf)*(px(i)-1-offxbf)/2 + px(j)-offxbf)
     .            = scr(1)
      d(offysym + (py(i)-offybf)*(py(i)-1-offybf)/2 + py(j)-offybf)
     .            = scr(1)
      d(offzsym + (pz(i)-offzbf)*(pz(i)-1-offzbf)/2 + pz(j)-offzbf)
     .            = scr(1)
             enddo
            enddo

c=====================================================================
c              d-Orbital averaging

            xxsym=isym(dxx(1))
            yysym=isym(dyy(1))
            zzsym=isym(dzz(1))
            xysym=isym(dxy(1))
            xzsym=isym(dxz(1))
            yzsym=isym(dyz(1))

            offxxsym=offsym(xxsym)
            offyysym=offsym(yysym)
            offzzsym=offsym(zzsym)
            offxysym=offsym(xysym)
            offxzsym=offsym(xzsym)
            offyzsym=offsym(yzsym)

            offxxbf=offbfn(xxsym)
            offyybf=offbfn(yysym)
            offzzbf=offbfn(zzsym)
            offxybf=offbfn(xysym)
            offxzbf=offbfn(xzsym)
            offyzbf=offbfn(yzsym)

c           if ( (idxx.ne.idyy).or.(idxx.ne.idzz) .or. (idxx.ne.idxy)
c    .           .or.(idxx.ne.idxz).or.(idxx.ne.idyz))
c    .       call bummer('idxx.ne.idyy...',0,2)

            do i=1,idyy
              do j=1,i
               scr(1) = d(offxxsym + (dxx(i)-offxxbf)*
     .                   (dxx(i)-1-offxxbf)/2 + dxx(j)-offxxbf)
               scr(2) = d(offyysym + (dyy(i)-offyybf)*
     .                   (dyy(i)-1-offyybf)/2 + dyy(j)-offyybf)
               scr(3) = d(offzzsym + (dzz(i)-offzzbf)*
     .                   (dzz(i)-1-offzzbf)/2 + dzz(j)-offzzbf)
               scr(4) = d(offxysym + (dxy(i)-offxybf)*
     .                   (dxy(i)-1-offxybf)/2 + dxy(j)-offxybf)
               scr(5) = d(offxzsym + (dxz(i)-offxzbf)*
     .                   (dxz(i)-1-offxzbf)/2 + dxz(j)-offxzbf)
c              scr(6) = d(offyzsym + (dyz(i)-offyzbf)*
c    .                   (dyz(i)-1-offyzbf)/2 + dyz(j)-offyzbf)
               write(*,*) 'adding: idyy=',i,(scr(k),k=1,5)
               scr(6) = 0.2d0*(scr(1)+scr(2)+scr(3)+scr(4)+
     .                         scr(5))
               d(offxxsym + (dxx(i)-offxxbf)*
     .           (dxx(i)-1-offxxbf)/2 + dxx(j)-offxxbf)=scr(6)
               d(offyysym + (dyy(i)-offyybf)*
     .           (dyy(i)-1-offyybf)/2 + dyy(j)-offyybf)=scr(6)
               d(offzzsym + (dzz(i)-offzzbf)*
     .           (dzz(i)-1-offzzbf)/2 + dzz(j)-offzzbf)=scr(6)
               d(offxysym + (dxy(i)-offxybf)*
     .           (dxy(i)-1-offxybf)/2 + dxy(j)-offxybf)=scr(6)
               d(offxzsym + (dxz(i)-offxzbf)*
     .           (dxz(i)-1-offxzbf)/2 + dxz(j)-offxzbf)=scr(6)
c              d(offyzsym + (dyz(i)-offyzbf)*
c    .           (dyz(i)-1-offyzbf)/2 + dyz(j)-offyzbf)=scr(6)
              enddo
            enddo


c=======================================================================

c
            return
            end

         subroutine calcmod(mo,ov,dimao,dimmo,d,newd)
          implicit none 
          integer maxryd
         parameter(maxryd=80)
         integer dimao ,dimmo
         real*8 mo(dimao,dimmo),ov(*),d(dimao*dimao)
         real*8 newd(*)
         integer k,mu,nu,i,lamda,j
         real*8 s(maxryd,maxryd),dao(maxryd,maxryd)

c
c        D(mo,mo) =  CT(AO,MO) * S(AO,AO) * D(AO,AO)*C(AO,MO)
c
c        D(i,j) =  sum(a,b,c) C(i,a)*D(a,b)*CT(c,b) *S(c,j)

         call wzero(maxryd*maxryd,s,1)
         call wzero(maxryd*maxryd,dao,1)
         k=0
         do i=1,dimao
          do j=1,i
           k=k+1
           s(i,j)=ov(k)
           s(j,i)=ov(k)
           dao(i,j)=d(k)
           dao(j,i)=d(k)
          enddo
         enddo

         k=0
         do i=1,dimmo
          do j=1,i
            k=k+1
            newd(k) =0.0d0
             do mu=1,dimao
               do lamda=1,dimao
                 do nu = 1,dimao
c              newd(k) = mo(mu,i)*dao(mu,nu)*mo(nu,j)
               newd(k) = mo(mu,i)*s(mu,lamda)*dao(lamda,nu)*mo(nu,j)
     .                    + newd(k)
                 enddo
               enddo
             enddo
           enddo
          enddo

          return
          end



         subroutine trimxm(s,c,dim)

c        B= A*B

          implicit none 
          integer dim
         real*8 s(*),c(*)
         real*8 ss(20,20),cc(20,20)
         integer i,j,k,l

         if (dim.gt.20) call bummer('dim gt.20',dim,2)

         k=0
         do i=1,dim
         do j=1,i
           k=k+1
           cc(i,j)=c(k)
           cc(j,i)=c(k)
           ss(i,j)=s(k)
           ss(j,i)=s(k)
         enddo
         enddo
         k=0
         do i=1,dim
          do j=1,i
           k=k+1
            c(k)=0.0d0
            do l=1,dim
            c(k) =c(k)+ss(i,l)*cc(l,j)
            enddo
           enddo
          enddo
          return
          end


          subroutine trace(d,idim,dim)
            implicit none 
            real*8 d(*),t
           integer idim,dim(idim)
           integer sym,i,j
            t=0.0d0
            j=0
            do sym=1,idim
            do i=1,dim(sym)
               j=j+i
              t=t+d(j)
            enddo
            write(*,*) 'trace(dim=',dim,sym,')=',t
            enddo
            return
            end



          subroutine calccoef(aono,aomo,mono,nryd,mryd)
           implicit none 
           integer nryd,mryd,i,j,k,l,mu
          real*8 aono(nryd,mryd), aomo(nryd,nryd),
     .                     mono(nryd,nryd),scale

          do mu=1,nryd
           do j=1,mryd
             aono(mu,j)=0.0d0
                do k=1,nryd
               aono(mu,j) = aono(mu,j)+aomo(mu,k)*mono(k,j)
                enddo
             if (abs(aono(mu,j)).lt.1.d-7) aono(mu,j)=0.0d0
            enddo
           enddo

           do j=1,mryd
             do mu=nryd,1,-1
              if (abs(aono(mu,j)).gt.1.d-5) then
                  scale= aono(mu,j)
                  goto 101
              endif
             enddo
101          continue
             do mu=1,nryd
              aono(mu,j)=aono(mu,j)/scale
             enddo
           enddo

           return
           end



         subroutine writedalt(c,nsym,nryd,mryd,type)

           implicit none 
           real*8 c(*)
          integer nsym,nryd(*),mryd(*),type(*)
          integer n,i,j,k
          character*1 map(4)
          data map/'s','x','y','z'/

          n=0
          do i=1,nsym
           if (mryd(i).gt.0) then
            write(*,*) 'Coefficients for irrep block ', i
            write(*,*) 'Rydberg type AOs:',
     .               (map(type(j)),j=n+1,n+nryd(i))
           endif
          enddo
          return
          end

       subroutine decrydov (ov,rydov,nrydpsy,lrydao,nsym,nbfpsy,type)
        implicit none 
        real*8 ov(*),rydov(*)
       integer nsym,nrydpsy(nsym),lrydao(2*nsym),nbfpsy(nsym)
       integer type(*)
       integer dimao,mu,nu,i,j,k,offov,offrydov

           k=0
           offov=0
           do i=1,nsym
           do mu=1,nrydpsy(i)
             do nu=1,mu
              k=k+1
              if (type(mu+offov).ne.type(nu+offov)) then
               rydov(k)=0.0d0
              endif
             enddo
            enddo
           offov=offov+nrydpsy(i)
           enddo

         return
         end

       subroutine makeplotmo (stdmo,plotstdmo,nrydpsy,plotmo,lrydao,
     .                      nsym,nbfpsy)
        implicit none 
        real*8 stdmo(*),plotstdmo(*)
       integer nsym,nrydpsy(nsym),lrydao(2*nsym),plotmo(2*nsym),
     .         nbfpsy(nsym)

       integer i,j,k,off,offryd

        off=0
        offryd=1
        do i=1,nsym
         do j=1,plotmo(i*2-1)-1
           off=off+nbfpsy(i)
         enddo
         do j=plotmo(i*2-1), plotmo(i*2-1)+plotmo(i*2)-1
           off = off + lrydao(i*2-1)-1
           do k= lrydao(i*2-1),lrydao(i*2-1)+lrydao(i*2)-1
           off=off+1
           plotstdmo(offryd)=stdmo(off)
           offryd=offryd+1
           enddo
           off = off + nbfpsy(i)-lrydao(i*2-1)-lrydao(i*2)+1
         enddo
         do j=max(plotmo(i*2-1)+plotmo(i*2),1),nbfpsy(i)
          off=off+nbfpsy(i)
         enddo
        enddo

        off=0
        do i=1,nsym
          do j=1,plotmo(i*2)
          write(15,*) '# Irrep', i,' : MO ',j+plotmo(i*2-1)-1
          off=off+1
          write(15,100) 'a= ',plotstdmo(off)
100       format (a,3x,f10.6)
          off=off+1
          write(15,100) 'b= ',plotstdmo(off)
          off=off+1
          write(15,100) 'c= ',plotstdmo(off)
          off=off+1
          write(15,100) 'd= ',plotstdmo(off)
          off=off+1
          write(15,100) 'e= ',plotstdmo(off)
          off=off+1
          write(15,100) 'f= ',plotstdmo(off)
          off=off+1
          write(15,100) 'g= ',plotstdmo(off)
          off=off+1
          write(15,100) 'h= ',plotstdmo(off)
         enddo
         enddo
        return
        end


