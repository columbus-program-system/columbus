!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       program mkciovinp

       implicit none

!     input filename mkciovinpin
!       &input
!         seward= 1 or 0     ( seward integrals)
!         runfile='name_of_runfile'  
!         mo_t1  ='name_of_mocoef_file_timestep1'
!         mo_t2  ='name_of_mocoef_file_timestep2'
!         cidrtfl='name of the cidrtfl file'
!       &end

!
!       compile via 
!       ifort -c -i8  -o mkciovinp.o mkciovinp.F90
!       ifort -z muldefs -o mkciovinp.x mkciovinp.o $COLUMBUS/colib.a $COLUMBUS/libmolcas_col.a $COLUMBUS/blaswrapper.a -L$ACMLDIR -lacml -lacml_mv $COLUMBUS/libma.a 
!  z.B. 
!       ifort -z muldefs -o mkciovinp.x mkciovinp.o $COLUMBUS/colib.a $COLUMBUS/libmolcas_col.a  $COLUMBUS/blaswrapper.a  -L/software/ACML/acml4.4.0/ifort64_int64/lib -lacml -lacml_mv  $COLUMBUS/libma.a 
!
      

        integer seward
        character*40 runfile,mo_t1,mo_t2,cidrtfl

        namelist /input/ seward,runfile,mo_t1,mo_t2 ,cidrtfl      

        integer  nsym,nbpsy(8),isym,ntot,ntotq,nvalid,irc
        integer  filerr,syserr,mxtitl,ntitle
        integer  i,ierror,mc1,mc6,io,j,ibas,jbas,x,y,l,n,k
        integer  nbpsy1(8),nsym1
        integer  nbpsy2(8),nsym2
        parameter (mxtitl=10)
        character*80 afmt, titles(mxtitl)
        character*4 labels(8)
	character*255 :: cioverlapinfile
        logical lex

        double precision, allocatable :: overlap(:)
        double precision, allocatable :: mocoef_t1(:),mocoef_t2(:)

        integer nfzct,nfzvt,nelec,nbfrzc(8),nbfrzv(8),nmpsy(8)
        integer ,parameter :: nbuf=1000
        integer ibuf(nbuf)







!       open input file and read namelist input
!       first set defaults

        seward=1
        runfile='RUNFILE'
        cidrtfl='cidrtfl'

        open(unit=7,file='mkciovinpin',form='formatted')
        read(7,input)
        close(7)
        

        write(6,*) '   Input File Contents    '
        write(6,'(a,i4)')  '  seward=',seward
        write(6,'(a,a//)')   '  runfile=',runfile

!       initialization of molcas RTE

        if (seward.eq.1) then 
           call link_it
           call getenvinit
           call fioinit
           call NameRun(runfile(1:len_trim(runfile)))
        else
          call bummer('not yet adapted to non-seward integral files',0,2)
        endif


!       in the molcas case extract some basic infos from RUNFILE
 
        if (seward.eq.1) then
            call mcget_iscalar('nsym',nsym)
           call mcget_iarray('nbas',nbpsy,nsym)
           write(6,'(a,i4,a)') 'basis set info: ', nsym , ' irred. representations'
           write(6,'(a,8i4)') 'dimension:',nbpsy(1:nsym)
        endif 

!       allocate memory
        ntot=0
        do isym=1,nsym
           ntot=ntot+nbpsy(isym)*(nbpsy(isym)+1)/2
        enddo
        allocate(overlap(ntot))

        mc1=1
        mc6=6

        if (seward.eq.1) then 
        call rdonexx(irc,mc6,'Mltpl  0',mc1,overlap,mc1,nvalid,'ONEINT')
        if (irc.ne.0 .or. nvalid.gt.ntot) then
            write(6,*) 'irc,nvalid,nnbt=',irc,nvalid,ntot
            stop 'error1'
        endif
        endif 
!       print integrals for inspection
        call plblks( 'SMAT AO',overlap, nsym, nbpsy, 'AO',3,6)
      

!       read mo coefficients for mo_t1

        inquire(file=mo_t1(1:len_trim(mo_t1)),exist=lex)
        if (.not. lex) then
           write(6,*) 'the file ',mo_t1,' is missing'
           stop 'error2'
        endif 
         open(file=mo_t1(1:len_trim(mo_t1)),unit=11)
         call moread(11,10,filerr,syserr,mxtitl,ntitle,titles,afmt,nsym1, &
        &            nbpsy1,nbpsy1,labels,0,mocoef_t1)

!  compute size of mocoef
          ntotq=0
          do i=1,nsym1
            ntotq=ntotq+nbpsy1(i)*nbpsy1(i)
          enddo
         allocate(mocoef_t1(ntotq))
          if (nsym.ne.nsym1) then
             write(6,*) 'inconsistent symmetry',nsym,' vs.',nsym1
             if (ierror.gt.0) stop 'error3' 
          endif
 
         call moread(11,20,filerr,syserr,mxtitl,ntitle,titles,afmt,nsym1, &
        &            nbpsy1,nbpsy1,labels,ntotq,mocoef_t1)
         close (11)
         call prblks('mo_t1',mocoef_t1,nsym1,nbpsy1, nbpsy1,'AO ','MO ',3,6)

 
!       read mo coefficients for mo_t2

        inquire(file=mo_t1(1:len_trim(mo_t2)),exist=lex)
        if (.not. lex) then
           write(6,*) 'the file ',mo_t2,' is missing'
           stop 'error2'
        endif
         open(file=mo_t2(1:len_trim(mo_t2)),unit=11)
         call moread(11,10,filerr,syserr,mxtitl,ntitle,titles,afmt,nsym2, &
        &            nbpsy2,nbpsy2,labels,0,mocoef_t1)
!  compute size of mocoef
          ntotq=0
          do i=1,nsym2
            ntotq=ntotq+nbpsy2(i)*nbpsy2(i)
          enddo
         allocate(mocoef_t2(ntotq))
          if (nsym.ne.nsym2) then
             write(6,*) 'inconsistent symmetry',nsym,' vs.',nsym2
             if (ierror.gt.0) stop 'error4'
          endif

         call moread(11,20,filerr,syserr,mxtitl,ntitle,titles,afmt,nsym2, &
        &            nbpsy2,nbpsy2,labels,ntotq,mocoef_t2)
         close (11)
         call prblks('mo_t2',mocoef_t2,nsym2,nbpsy2, nbpsy2,'AO ','MO ',3,6)


          call rddrt( 6, cidrtfl, nsym1, nbpsy1, &
     &    nfzct,nfzvt,nelec, nbfrzc,nbfrzv,nmpsy, ibuf,nbuf )

         write(6,'(a,i4)') 'cidrtfl: #electrons=',nelec
         write(6,'(a,i4)') 'cidrtfl: #frozen core orbitals=',nfzct
         write(6,'(a,i4)') 'cidrtfl: #frozen virtual orbitals=',nfzvt


!        some additional minimalistic check on the consistency of MOs and Runfile
         ierror=0
         do i=1,nsym
          if (nbpsy(i).ne.nbpsy1(i)+nbpsy2(i)) ierror=ierror+1
         enddo
         if (ierror.ne.0) then
           write(6,*) 'Runfile does not correspond to the supermolecule'
           stop 'error 4'
         endif

 

	write(6,'(A)') 'Writing cioverlap.in...'

	cioverlapinfile='cioverlap.in'
	open(42,file=cioverlapinfile,status='replace',action='write',iostat=io)
	if (io/=0) then
	  write(6,*) 'Can not open output file ',cioverlapinfile
	  stop
	endif
	n=sum(nbpsy(1:nsym))/2
	write(42,'(4(3X,I6))') n,sum(nbfrzc(1:nsym)),sum(nbfrzv(1:nsym)),nelec

	write(42,*) 2*n,2*n
	do i=1,2*n
	  do j=1,2*n
	    call sym(i,nsym,nbpsy,ibas)
	    call sym(j,nsym,nbpsy,jbas)
	    if (ibas==jbas) then
	      x=j-sum(nbpsy(1:jbas-1))
	      y=i-sum(nbpsy(1:ibas-1))
	      call indices(x,y,l)
	      do k=1,ibas-1
		l=l+(nbpsy(k)**2+nbpsy(k))/2
	      enddo
	      write(42,'(1X,F25.18)',advance='no') overlap(l)
	    else
	      write(42,'(1X,F25.18)',advance='no') 0.d0
	    endif
	  enddo
	  write(42,'(X)')
	enddo

	write(42,*) n,n
	do i=1,n
	  do j=1,n
	    call sym(i,nsym1,nbpsy1,ibas)
	    call sym(j,nsym1,nbpsy1,jbas)
	    if (ibas==jbas) then
	      x=j-sum(nbpsy1(1:jbas-1))
	      y=i-sum(nbpsy1(1:ibas-1))
	      l=y+(x-1)*nbpsy1(ibas)
	      do k=1,ibas-1
		l=l+nbpsy1(k)**2
	      enddo
	      write(42,'(1X,F25.18)',advance='no') mocoef_t1(l)
	    else
	      write(42,'(1X,F25.18)',advance='no') 0.d0
	    endif
	  enddo
	  write(42,'(X)')
	enddo

	write(42,*) n,n
	do i=1,n
	  do j=1,n
	    call sym(i,nsym2,nbpsy2,ibas)
	    call sym(j,nsym2,nbpsy2,jbas)
	    if (ibas==jbas) then
	      x=j-sum(nbpsy2(1:jbas-1))
	      y=i-sum(nbpsy2(1:ibas-1))
	      l=y+(x-1)*nbpsy2(ibas)
	      do k=1,ibas-1
		l=l+nbpsy2(k)**2
	      enddo
	      write(42,'(1X,F25.18)',advance='no') mocoef_t2(l)
	    else
	      write(42,'(1X,F25.18)',advance='no') 0.d0
	    endif
	  enddo
	  write(42,'(X)')
	enddo


        endprogram
 
subroutine entries(j,x,y)
implicit none
integer, intent(in) :: j
integer, intent(out) :: x,y
integer :: i

i=j
x=1
y=1
do
  i=i-1
  x=x+1
  if (x>y) then
    x=1
    y=y+1
  endif
  if (i==0) exit
enddo

endsubroutine

subroutine indices(x,y,i)
implicit none
integer, intent(in) :: x,y
integer, intent(out) :: i
integer :: x1,y1

if (x<=y) then
  i=y*(y-1)/2+x
else
  i=x*(x-1)/2+y
endif

endsubroutine

subroutine sym(i,nsym,nbpsy,ibas)
implicit none
integer, intent(in) :: i,nsym
integer, intent(in) :: nbpsy(1:nsym)
integer, intent(out) :: ibas
integer :: j,tot

ibas=1
tot=0
do j=1,nsym
  tot=tot+nbpsy(j)
  if (i>tot) then
    ibas=ibas+1
  else
    exit
  endif
enddo

endsubroutine





          subroutine rddrt( nlist, cidrtfl, nsym, nbpsy, &
     &    nfzct,nfzvt,nelec, nbfrzc,nbfrzv,nmpsy, ibuf,nbuf )
!
!  read some of the ci drt file info for the integral transformation..
!
!  input:
!  nlist = list file unit number.
!  cidrtfl = drt file name   
!  nsym = number of symmetry blocks (checked for consistency).
!  nbpsy = number of basis functions per irrep
!  ibuf(*) = scratch buffer space of at least size = max(nsym,lidrt).
!  nbuf   = buffer length
!
!  output:
!   nfzct = total number of frozen core orbitals
!   nfzvt = total number of discarded virtual orbitals
!   nelec    = total number of correlated electrons 
!   nbfrzc   = frozen core orbitals per irrep
!   nbfrzv   = frozen virtual orbitals per irrep
!   nmpsy    = orbitals per irrep  in MO basis     
!
       implicit none 
!====>Begin Module RDDRT                  File tran7.f                  
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
!     Argument variables
!
!
      INTEGER             NBUF, IBUF(NBUF), NBFT,    NDRT
      INTEGER             NLIST,       NSYM, NFZCT, NFZVT, NELEC
      INTEGER             NBFRZC(8),NBFRZV(8),NMPSY(8)
      CHARACTER*(*)       cidrtfl
!
!     Local variables
!
      INTEGER             I,           IBF,         IOERR,       LENBUF
      INTEGER             LIDRT,       LXYZIR(3),   NERROR,      NFCT
      INTEGER             NFCTX,       NFVT,        NFVTX,       NMOTD
      INTEGER             NSYMD,       VRSION , NIOT, NEXO(8),nbpsy(8)
      INTEGER             nl,icd(10),nlevel,isym,nrow
      CHARACTER*80        title
!
      LOGICAL             SPNODD,      SPNORB
!
!====>End Module   RDDRT                  File tran7.f                  
      nerror = 0
      nl=0
      do i=1,len_trim(cidrtfl)
        if (cidrtfl(i:i).ne.' ') nl=nl+1
      enddo  
!
      ndrt=15
      open (file=cidrtfl(1:nl),unit=ndrt,form='formatted')
      rewind ndrt
      read(ndrt,'(a)',iostat=ioerr)title
      if (ioerr .ne. 0 ) then
         call bummer('rddrt: title ioerr=',ioerr,2)
      endif
      read(ndrt,'(2L4,3I4)',iostat=ioerr)spnorb, spnodd, lxyzir
      if(ioerr.ne.0)call bummer('rddrt: ioerr=',ioerr,2)
      if (nbuf.lt.3) call bummer('insufficient buffer length',nbuf,2)
!
      call rddbl( ndrt, 3, ibuf, 3 )
!
      vrsion = ibuf(1)
      lenbuf = ibuf(2)
      lidrt  = ibuf(3)

      icd(1)=1
      icd(2)=icd(1)+lenbuf
!
      if ( vrsion .ne. 6 ) then
         call bummer & 
     &     ('DRT version 6 required (index vector compression)' & 
     &     // ' cstar(*) deleted' ,vrsion,2)
      endif
!
!     # read the integer parameter array.
!
      if (nbuf.lt.lidrt)  call bummer('insufficient buffer length',lidrt,2)
      call rddbl( ndrt, lenbuf, ibuf, lidrt )
!     buf( 1) = nmot
!     buf( 2) = niot
!     buf( 3) = nfct
!     buf( 4) = nfvt
!     buf( 5) = nrow
!     buf( 6) = nsym
!     buf( 7) = ssym
!     buf( 8) = xbar(1,1)
!     buf( 9) = xbar(2,2)
!     buf(10) = xbar(3,3)
!     buf(11) = xbar(4,3)
!     buf(12) = nvalw(1)
!     buf(13) = nvalw(2)
!     buf(14) = nvalw(3)
!     buf(15) = nvalw(4)
!     buf(16) = ncsft

!
!     # check some variables for consistency.
!
!     # in this version the total number of orbitals must equal the
!     # number of basis functions (rls).
!
      nmotd = ibuf(1)
      niot  = ibuf(2)
      nlevel = niot+1
      nbft = sum(nbpsy(1:nsym))
      if ( nbft .ne. nmotd ) then
         write(nlist,6010)'nbft',nbft,nmotd
         nerror = nerror + 1
      endif
!
      nrow = ibuf(5)
      nsymd = ibuf(6)
      if ( nsym .ne. nsymd ) then
         write(nlist,6010)'nsym',nsym,nsymd
         nerror = nerror + 1
      endif
!
      nfzct = ibuf(3)
      nfzvt = ibuf(4)
!
!     # skip over the symmetry labels.
!
      call rddbl( ndrt, lenbuf, ibuf, -nsym )
!
!     # read the orbital-to-level mapping vector, map(*).
!
      call rddbl( ndrt, lenbuf, ibuf(icd(2)), nmotd )
!
!     # check that map(*) is consistent with nfzct and nfzvt.
!
      nfctx = 0
      nfvtx = 0
      ibf=-1
      do isym=1,nsym
       do i=1,nbpsy(isym)
         ibf=ibf+1 
         if ( ibuf(icd(2)+ibf) .eq. -1 ) then
            nfctx=nfctx+1
            nbfrzc(isym)=nbfrzc(isym)+1
         elseif ( ibuf(icd(2)+ibf) .eq. 0 ) then
            nfvtx=nfvtx+1
            nbfrzv(isym)=nbfrzv(isym)+1
         endif
       enddo
      enddo
!
      if ( nfzvt .ne. nfvtx ) then
         write(nlist,6010)'nfvt',nfvt,nfvtx
         nerror = nerror + 1
      endif
      if ( nfzct .ne. nfctx ) then
         write(nlist,6010)'nfct',nfct,nfctx
         nerror = nerror + 1
      endif
!
!     # write out the drt info.
!
!     write(nlist,6030)title
!     write(nlist,6040) nmotd, nfzct, nfzvt, nbft
!     write(nlist,*)
!
      if ( nerror .ne. 0 ) then
         write(nlist,6020)
         call bummer('rddrt: nerror=', nerror, 2)
      endif
!
!     we need additionally the number of external orbitals
!
!    skip mu entry
      call rddbl( ndrt, lenbuf, ibuf, -niot )
!    read nbpsy entry
      call rddbl( ndrt, lenbuf, nmpsy, nsym )
!    read external orbitals nexo
      call rddbl( ndrt, lenbuf, nexo, -nsym )

       write(nlist,6060) nbpsy(1:nsym)
       write(nlist,6061) nmpsy(1:nsym)
       write(nlist,6062) nbfrzc(1:nsym)
       write(nlist,6063) nbfrzv(1:nsym)

 6060  format(' # basis functions per irrep:     ',8i6)
 6061  format(' # molecular orbitals per irrep:  ',8i6) 
 6062  format(' # frozen core orbit. per irrep:  ',8i6) 
 6063  format(' # frozen virt. orbit. per irrep: ',8i6) 

! additional check
       do isym=1,nsym
         if (nbpsy(isym).ne. nmpsy(isym)) nerror=nerror+1
!          if (nbpsy(isym).ne. nmpsy(isym)+nbfrzv(isym)+nbfrzc(isym)) nerror=nerror+1
       enddo
      if ( nerror .ne. 0 ) then
         write(nlist,6021)
         call bummer('rddrt: nerror=', nerror, 2)
      endif
!
!
!     # nviwsm(*)... number of valid internal walks per symmetry and walk type
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),-4*nsym)
!
!     # ncsfsm(*)... number of configurations per symmetry and walk type
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),-4*nsym)
!
!     # modrt(*)...
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),-niot)
!
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),-niot)
!
!     # nj(*)...
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),-nlevel)
!
!     # njskp(*)...
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),-nlevel)
!
!     # a(*)...
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),nrow)
!
        nelec = 2*ibuf(icd(2)+nrow-1)
!
!     # b(*)...
        CALL rddbl(ndrt,lenbuf,ibuf(icd(2)),nrow)
        nelec =nelec+ibuf(icd(2)+nrow-1)
      close(ndrt)
      return
!
6010  format(' *** rddrt error *** ',a,3i8)
6020  format(/' program stopping due to inconsistencies on the', & 
     &  ' drt file.')
6021  format(/' inconsistent number of orbitals per irrep'/)
6030  format(/' input ci drt file information:'/(1x,a))
6040  format(' nmotd =',i4,' nfct  =',i4,' nfvt  =',i4,' nbft  =',i4)
6050  format(1x,a/(1x,20i4))
      end subroutine rddrt
         






