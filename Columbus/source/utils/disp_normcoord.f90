!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
!##########################################++##########################
!####                                                               ###
!#### program disp_normcoord written by Bernhard Sellner 04.12.2007 ###
!####                                                               ###
!######################################################################

program disp_normcoord  ! version 2.1 24.09.2012
implicit none
real*8 Z(:),M(:),geom(:,:),lq(:,:,:),freq(:),rm(:),f(:),newg(:,:)
real*8 hbar,c,lo,up,tauinSI,Mauinamu,MauinSI,lauinSI,MamuinSI,inc,dis
real*8 idf,a,u,lAinau,pi
integer h,i,j,k,l,nat,steps,df,du1,du2,d_un,nmod,lofm(:)
integer wi1,wi2,filestat,ii,jj
character*1 S(:)
character trash1,trash2
character*3 version_number,prg
allocatable Z,M,geom,S,lq,freq,rm,f,newg,lofm
character*20 wc1

version_number='2.1'

print*,'program disp_normcoord version ',version_number
print*,''

! constants #########################################################

hbar = 1.05457266D-34            !Js
c = 299792458D0                  !m/s
tauinSI = 2.41889D-17            !s
Mauinamu = (1D0/1822.888515D0)   !amu
MauinSI = 9.1093897D31           !kg
lauinSI = 5.29177249D-11         !m
lAinau= (1D-10/lauinSI)          !a.u.
MamuinSI = MauinSI/Mauinamu
pi=dacos(-1D0)

! read input ########################################################


open(unit=10,file='inp',form='formatted')
  read(10,fmt='(A3)')prg
  read(10,fmt=*)nmod
  allocate(lofm(nmod))
  read(10,fmt=*)(lofm(i),i=1,nmod,1)
  read(10,fmt=*)d_un
  read(10,fmt=*)lo
  read(10,fmt=*)up
  read(10,fmt=*)steps
  read(10,fmt=*)nat
close(10)


lo=dble(lo)
up=dble(up)
steps=int(steps)

df=3*nat
idf=df-6

allocate(Z(nat))
allocate(geom(nat,3))
allocate(M(nat))
allocate(S(nat))
allocate(lq(df,nat,3))
allocate(freq(df))
allocate(rm(df))
allocate(f(df))
allocate(newg(nat,3))


if (prg.eq.'col') then
  open(unit=20,file='geom',form='formatted')
  do i=1,nat
    read(20,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
  enddo
  close(20)
  
    open(unit=34,file='suscalls',form='formatted',status='old',iostat=filestat)
    if (filestat > 0) STOP "No suscalls file for program guessgeom.x "
    do while (wc1.ne.'cartesian')
      read(34,fmt=*)wc1
      wc1=trim(adjustl(wc1))
    enddo

    wi1=int((3*nat)/8)
    if (mod((3*nat),8).ne.0) wi1=wi1+1

    do i=1,wi1-1
      read(34,fmt=*)
      read(34,fmt=*)
      read(34,fmt=*)
      read(34,fmt=*)trash1,(freq(j),j=(i-1)*8+1,(i-1)*8+8,1)
      read(34,fmt=*)
      do ii=1,nat
        do jj=1,3
          read(34,fmt='(6X,A1,X,A1)',advance='no')trash1,trash2
          if (((jj.eq.1).and.(trash2.eq.'x')).or.&
             &((jj.eq.2).and.(trash2.eq.'y')).or.&
             &((jj.eq.3).and.(trash2.eq.'z'))) then
            read(34,fmt=*)(lq(k,ii,jj),k=(i-1)*8+1,(i-1)*8+8,1)
          else
            do k=(i-1)*8+1,(i-1)*8+8
              lq(k,ii,jj)=0.0D0
            enddo
            backspace(34)
          endif
        enddo
      enddo
    enddo
    i=wi1
    if (mod((3*nat),8).eq.0) then
      wi2=8
    else
      wi2=mod((3*nat),8)
    endif
    read(34,fmt=*)
    read(34,fmt=*)
    read(34,fmt=*)
    read(34,fmt=*)trash1,(freq(j),j=(i-1)*8+1,(i-1)*8+wi2,1)
    read(34,fmt=*)
    do ii=1,nat
      do jj=1,3
        filestat=0
        read(34,fmt='(A7)',advance='no',iostat=filestat)trash1
        if (filestat.eq.0) then
          backspace(34)
          read(34,fmt='(6X,A1,X,A1)',advance='no')trash1,trash2
          if (((jj.eq.1).and.(trash2.eq.'x')).or.&
             &((jj.eq.2).and.(trash2.eq.'y')).or.&
             &((jj.eq.3).and.(trash2.eq.'z'))) then
            read(34,fmt=*)(lq(k,ii,jj),k=(i-1)*8+1,(i-1)*8+wi2,1)
          else
            do k=(i-1)*8+1,(i-1)*8+wi2
              lq(k,ii,jj)=0.0D0
            enddo
            backspace(34)
          endif
        else
          do k=(i-1)*8+1,(i-1)*8+wi2
            lq(k,ii,jj)=0.0D0
          enddo
          backspace(34)
        endif
      enddo
    enddo
    close(34)

  rm=0.0D0
  do i=1,df
    do ii=1,nat
      do jj=1,3
        rm(i)=rm(i)+(lq(i,ii,jj)**2)/M(ii)
      enddo
    enddo
    rm(i)=1D0/rm(i)
  enddo


else

  open(unit=20,file='coord',form='formatted')
  read(20,fmt=*)
  do i=1,nat
    read(20,fmt=*)geom(i,1),geom(i,2),geom(i,3),S(i)
  enddo
  close(20)

  open(unit=20,file='frequencies',form='formatted')
  read(20,fmt=*)
  read(20,fmt=*)(freq(i),i=1,df,1)
  read(20,fmt=*)(rm(i),i=1,df,1)
  do i=1,nat
    do j=1,3
      read(20,fmt=*)(lq(k,i,j),k=1,df,1)
    enddo
  enddo
  close(20)
  
endif


! write input in log file ###########################################

open(unit=30,file='displacels',form='formatted')

write(unit=30,fmt='(A,A)')'program disp_normcoord.x version ',version_number

call print_inputdata(S,Z,geom,M,nat,lq,freq,rm,d_un,lo,up,steps,30,prg,version_number) 

write(unit=30,fmt='(A)')
write(unit=30,fmt='(A)')'constants and conversion factors'
write(unit=30,fmt='(A,E25.16)')'c[m/s]=        ',c
write(unit=30,fmt='(A,E25.16)')'tauinSI[s]=    ',tauinSI
write(unit=30,fmt='(A,F25.16)')'Mauinamu[amu]= ',Mauinamu
write(unit=30,fmt='(A,E25.16)')'lAinau[a.u.]=  ',lAinau
write(unit=30,fmt='(A)')
write(unit=30,fmt='(A)')'all calculations are performed in a.u.' 
write(unit=30,fmt='(A)')


! convert to a.u. ####################################################

if (d_un .eq. 2) then
  lo=dble(lo)*lAinau
  up=dble(up)*lAinau
else 
endif

do i=1,nmod
  freq(lofm(i))=((100D0*freq(lofm(i)))*c)*tauinSI   !convert to a.u.
  freq(lofm(i))=2D0*pi*freq(lofm(i))                !calculate angular frequency
  rm(lofm(i))=rm(lofm(i))/Mauinamu
enddo

M=M/Mauinamu


! convert normal coordinates from mwc coordinates to cartesian ######


if (prg.eq.'col') then
  call norm(lq,nat)                
  call massweighting(lq,nat,M)       ! transform from mwc to cartc
endif

call norm(lq,nat)               


! create displaced geometries #######################################

write(unit=30,fmt=*)''
write(unit=30,fmt=*)'displaced geometries'

if (prg.eq.'col') then
  M=M*Mauinamu  !  backconversion for COLUMBUS output
endif

do i=1,nmod
  do j=0,(steps-1)
    write(unit=30,fmt=*)lofm(i)
    a=(dble(lo)+((dble(up)-dble(lo))*dble(j))/dble(steps-1))
    if (d_un .eq. 1) then
      if (freq(lofm(i)).le.0D0) then
        print*,'Negative or  zero frequencies cannot be used with dimensionless units'
        stop
      endif
      f(lofm(i))=dsqrt((rm(lofm(i))*freq(lofm(i))))
      a=a/f(lofm(i))
    else
    endif
    write(unit=30,fmt='(F25.16)')a
    do h=1,nat
      do l=1,3
        newg(h,l)=geom(h,l)+lq(lofm(i),h,l)*a
      enddo
    enddo
    if (prg.eq.'col') then
      do k=1,nat
        write(unit=30,fmt='(A2,3X,F5.1,4F14.8)',advance='yes')S(k),Z(k),newg(k,1),newg(k,2),newg(k,3),M(k)
      enddo
    else
      do k=1,nat
        write(unit=30,fmt='(F20.14,2F22.14,6X,A1)',advance='yes')newg(k,1),newg(k,2),newg(k,3),S(k)
      enddo
    endif 
  enddo 
enddo   
  

!####################################################################

close(30)

print*,'Job done'

end program

!####################################################################
!####################################################################
!####################################################################


subroutine norm(vecs,atoms)
implicit none
integer i,j,k,atoms
real*8 vecs(atoms*3,atoms,3),n(atoms*3)

n=0

do i=1,(atoms*3)
  do j=1,3
    do k=1,atoms
      n(i)=n(i)+vecs(i,k,j)**2
    enddo
  enddo
  n(i)=dsqrt(n(i))
enddo



do i=1,(atoms*3)
  do j=1,3
    do k=1,atoms
      vecs(i,k,j)=vecs(i,k,j)/n(i)
    enddo
  enddo
enddo

end subroutine


subroutine massweighting(vecs,atoms,mass)
implicit none
integer i,j,k,atoms
real*8 vecs(atoms*3,atoms,3),mass(atoms)

do i=1,(atoms*3)
  do j=1,3
    do k=1,atoms
      vecs(i,k,j)=vecs(i,k,j)/dsqrt(mass(k))
    enddo
  enddo
enddo

end subroutine


subroutine print_freqs(lq,freq,atoms,un,S)
implicit none
integer i,j,k,atoms,un,df
real*8 lq(atoms*3,atoms,3),freq(atoms*3)
character*1 S(atoms)


df=3*atoms

write(unit=un,fmt='(A9)',advance='no')'         '

do i=1,(df-1)
  write(unit=un,fmt='(I10)',advance='no')i
enddo

write(unit=un,fmt='(I10)',advance='yes')i

write(unit=un,fmt='(A9)',advance='no')'    freq '

do i=1,(df-1)
  write(unit=un,fmt='(F10.3)',advance='no')freq(i)
enddo

write(unit=un,fmt='(F10.3)',advance='yes')freq(i)

do j=1,atoms
  do k=1,3
    write(unit=un,fmt='(A7)',advance='no')S(j)
    if (k .eq. 1) then
      write(unit=un,fmt='(A2)',advance='no')' x'
    elseif (k .eq. 2) then
      write(unit=un,fmt='(A2)',advance='no')' y'
    else
      write(unit=un,fmt='(A2)',advance='no')' z'
    endif
    do i=1,(df-1)
      write(unit=un,fmt='(F10.5)',advance='no')lq(i,j,k)
    enddo
    write(unit=un,fmt='(F10.5)',advance='yes')lq(df,j,k)
  enddo
enddo

end subroutine


subroutine print_inputdata(S,Z,geom,M,atoms,lq,freq,rm,d_un,lo,up,steps,u,prg,version_number)
implicit none
integer i,j,k,l,atoms,u,du1,du2,df,un1,un2,un3,un4,steps,d_un
real*8 Z(atoms),geom(atoms,3),M(atoms),lq(atoms*3,atoms,3),freq(3*atoms),rm(3*atoms),lo,up
character*1 S(atoms)
character*3 prg,version_number

un1=u
un2=u
un3=u
un4=u

df=3*atoms

write(unit=un3,fmt='(A,A)')'data taken from ',prg

write(unit=un3,fmt='(A,A)')'input data for disp_normcoord.x ',version_number


if (d_un .eq. 2) then
  write(unit=un3,fmt='(A)')'bounds in Angstrom'
else
  write(unit=un3,fmt='(A)')'bounds in dimensionless units'
endif

write(unit=un3,fmt='(A,F25.16)')'lower bound',lo
write(unit=un3,fmt='(A,F25.16)')'upper bound',up
write(unit=un3,fmt='(A,I12)')'displacement steps',steps

write(unit=un1,fmt='(A)')''
write(unit=un1,fmt='(A)')'geom file'


if (prg.eq.'col') then
  do i=1,atoms
    write(unit=un1,fmt='(A2,3X,F5.1,4F14.8)',advance='yes')S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
  enddo
else
  do i=1,atoms
        write(unit=un1,fmt='(F20.14,2F22.14,6X,A1)',advance='yes')geom(i,1),geom(i,2),geom(i,3),S(i)
  enddo
endif


du1=int(dble(df)/8D0)

du2=mod(df,8)

write(unit=un2,fmt='(A)',advance='yes')''
if (prg.eq.'col') then
  write(unit=un2,fmt='(A)',advance='yes')'          lq'
else
  write(unit=un2,fmt='(A)',advance='yes')'          lx'
endif
write(unit=un2,fmt='(A)',advance='yes')''

do i=1,du1
  write(unit=un2,fmt='(A8)',advance='no')''
  do j=(1+8*(i-1)),(7+8*(i-1))
    write(unit=un2,fmt='(I9,X)',advance='no')j
  enddo  
  write(unit=un2,fmt='(I9,X)',advance='yes')(8+8*(i-1))
  
  write(unit=un2,fmt='(A9)',advance='yes')''

  write(unit=un2,fmt='(A9)',advance='no')'freq'
  do j=(1+8*(i-1)),(7+8*(i-1))
    write(unit=un2,fmt='(2X,F8.2)',advance='no')freq(j)
  enddo
  write(unit=un2,fmt='(2X,F8.2)',advance='yes')freq((8+8*(i-1)))

  write(unit=un2,fmt='(A)',advance='yes')''
  
  do k=1,atoms
    do l=1,3
      if (l.eq.1) then
        write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' x'
      elseif (l.eq.2) then
        write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' y'
      else
        write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' z'
      endif
      do j=(1+8*(i-1)),(7+8*(i-1))
        write(unit=un2,fmt='(2X,F8.5)',advance='no')lq(j,k,l)
      enddo
      write(unit=un2,fmt='(2X,F8.5)',advance='yes')lq((8+8*(i-1)),k,l)
    enddo
  enddo
  write(unit=un2,fmt='(A)',advance='yes')''
enddo

if (mod(df,8).ne.0) then
  i=du1+1
  if ((1+8*(i-1)).lt.(df)) then
    write(unit=un2,fmt='(A9)',advance='no')''
    do j=(1+8*(i-1)),(df-1)
      write(unit=un2,fmt='(I8,2X)',advance='no')j
    enddo
    write(unit=un2,fmt='(I8)',advance='yes')df

    write(unit=un2,fmt='(A)',advance='yes')''

    write(unit=un2,fmt='(A9)',advance='no')'freq'
    do j=(1+8*(i-1)),(df-1)
      write(unit=un2,fmt='(2X,F8.2)',advance='no')freq(j)
    enddo
    write(unit=un2,fmt='(2X,F8.2)',advance='yes')freq(df)

    write(unit=un2,fmt='(A)',advance='yes')''

    do k=1,atoms
      do l=1,3
        if (l.eq.1) then
          write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' x'
        elseif (l.eq.2) then
          write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' y'
        else
          write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' z'
        endif
        do j=(1+8*(i-1)),(df-1)
          write(unit=un2,fmt='(2X,F8.5)',advance='no')lq(j,k,l)
        enddo
        write(unit=un2,fmt='(2X,F8.5)',advance='yes')lq(df,k,l)
      enddo
    enddo

  else
 
    write(unit=un2,fmt='(A9)',advance='no')''
    write(unit=un2,fmt='(I9,X)',advance='yes')df
    write(unit=un2,fmt='(A)',advance='yes')''
    write(unit=un2,fmt='(A9)',advance='no')'freq'
    write(unit=un2,fmt='(2X,F8.2)',advance='yes')freq(df)
    write(unit=un2,fmt='(A)',advance='yes')''
 
    do k=1,atoms
      do l=1,3
        if (l.eq.1) then
          write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' x'
        elseif (l.eq.2) then
          write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' y'
        else
          write(unit=un2,fmt='(A7,A2)',advance='no')S(k),' z'
        endif
        write(unit=un2,fmt='(2X,F8.5)',advance='yes')lq(df,k,l)
      enddo
    enddo
  endif
else
endif

write(unit=un4,fmt=*)''
write(unit=un4,fmt='(A)')'frequency     reduced mass'
write(unit=un4,fmt='(A)')' [cm**-1]         [amu]   '

do i=1,df
  write(unit=un4,fmt='(F8.2,8X,F10.5)')freq(i),rm(i)
enddo

write(unit=un3,fmt=*)''
write(unit=un3,fmt=*)'end input data'


end subroutine
