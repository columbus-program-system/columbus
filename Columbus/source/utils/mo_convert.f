!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program mo_convert
       implicit none
c
c  convert MO-coefficient files between different formats
c    currently implemented:
c     Columbus "mocoef": format[in/out]=0
c     Molcas   "lumorb": format[in/out]=1
c
c  version log:
c    17-may-2013 file created -felix plasser
c
c 
      integer mxtitle, ntitlel, ntitler, filerr, syserr
      integer maxbfn,ierr,lvprt,formatin,formatout
      parameter (mxtitle=20, maxbfn=1023)
      character*80 titles(mxtitle),afmt
      character*4  labels(8)
      integer occlen,nsym,nbfpsy(8),nmopsy(8),clen
      real*8 c(maxbfn*maxbfn),occ(maxbfn)
      character*80 mo_in,mo_out
      integer i,ntitle
      
      namelist /input/ mo_in,mo_out,lvprt,
     . formatin, formatout
      
      open(6,file='mo_convertls',status='unknown')
      
      write(6,*) '===== Program mo_convert ====='
      formatin = 0
      formatout = 0
      mo_in = 'mo_in'
      mo_out  = 'mo_out'
      lvprt = 1
      
      open(unit=10,file='mo_convertin',status='old',err=400)
      goto 401
      
400   call bummer('mo_convertin missing, using default values',0,1)
      goto 402
     
401   call echoin(10,6,ierr)
      rewind(10)
      read(10,input,end=100)
 100  continue
      close(10)      

402   write(6,*) 'opening input mocoefficient file:',mo_in
      open(unit=11,file=mo_in,status='old')
      if (formatin.eq.0) then
c input in Columbus format
      clen=maxbfn*maxbfn
      call moread(11,10,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbfpsy,nmopsy,labels,clen,c)

      write(6,*) 'Titles from mocoef file:'
      write(6,*) (titles(i),i=1,ntitle)
      write(6,900) nsym
      write(6,901) (nbfpsy(i),i=1,nsym)
      write(6,902) (nmopsy(i),i=1,nsym)

 900  format(' number of irreducible representations:',i4)
 901  format(' number of basis functions per irrep:',8i4)
 902  format(' number of molecular orbitals per irrep:',8i4)

      occlen=0
      clen=0
      do i=1,nsym
       clen=clen+nmopsy(i)*nbfpsy(i)
       occlen=occlen+nmopsy(i)
      enddo

      write(6,*) ' reading mocoefficients ... '
      call moread(11,20,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbfpsy,nmopsy,labels,clen,c)
      write(6,*) ' reading occupation numbers ... ' 
      call wzero(maxbfn,occ,1)
      call moread(11,40,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbfpsy,nmopsy,labels,occlen,occ)     
      elseif (formatin.eq.1) then
c input in Molcas format
       write(6,*) ' reading MOLCAS mocoefficients ...'
       call rdmof_molcas(11,nsym, nmopsy, nbfpsy, c,'mos')
       
       ntitle=1
       titles(1) = 'from MOLCAS format'
       afmt = '(1p3d25.15)'
       labels(1)='SYM1'
       labels(2)='SYM2'
       labels(3)='SYM3'
       labels(4)='SYM4'
       labels(5)='SYM5'
       labels(6)='SYM6'
       labels(7)='SYM7'
       labels(8)='SYM8'
       
       write(6,900) nsym
       write(6,901) (nbfpsy(i),i=1,nsym)
       write(6,902) (nmopsy(i),i=1,nsym)
      else
       call bummer('Input format no supported',formatin,2)
      endif
      if (lvprt.ge.2) then
      call prblks('Input MO coefficients',c,nsym,nbfpsy,nmopsy,
     .            'SAO','MO',1,6)
      endif
      
      close(11)    
      
      open(unit=12,file=mo_out,status='unknown')
      if (formatout.eq.0) then
      write(6,*) ' Output in Columbus format'
      ntitle = ntitle + 1
      titles(ntitle) = 'converted using mo_convert.x'
      
      
       call mowrit(12,10,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsy,labels,c)
       write(6,*) '  writing transformed MOs ... '
       call mowrit(12,20,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsy,labels,c)

c       if (occvec) then
       write(6,*) '  writing occupation numbers ... '
       call mowrit(12,40,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsy,labels,occ)
c       endif

      elseif (formatout.eq.1) then
       write(6,*) ' Output in Molcas format'
       call wrtmof_molcas(12, nsym,nmopsy,nbfpsy,c,'mos',
     .      titles(1))
       call wrtmof_molcas(12, nsym,nmopsy,nbfpsy,occ,'occ',
     .      titles(1))
      else
       call bummer('Output format not supported',formatout,2)
      endif
      close(12)
      
      close(6)            
      call bummer('normal termination',0,3)
      stop
      end
