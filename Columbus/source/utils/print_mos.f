!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program print_mos
      IMPLICIT NONE
      include "../colib/getlcl.h"
      integer ifirst, lcore
      integer lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c
c
c     # use standard f90 memory allocation.
c
      integer mem1
      parameter ( lcored = 2 500 000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('exptvl: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore),stat=ierr )
      if ( ierr .ne. 0 ) then
         call bummer('exptvl: allocate failed ierr=',ierr,faterr)
      endif

      ifirst = 1
      mem1   = 0
      call driver( a(ifirst), (lcore) )
c
      call bummer('normal termination',0,3)
      stop 'end of print_mos'
      end

       subroutine driver(core,lcore)

       implicit none
       integer lcore
       integer, parameter :: mxtitle=20
       integer, parameter :: faterr=2
       integer, parameter :: nlist=6
       real*8 core(lcore) 
       character*40 filename,afmt
       character*80 title(mxtitle)
       integer nofile,fileerr,syserr,ntit,nsym,nbpsy(8),nmpsy(8)
       character*4 labels(8) 
       integer isym,nnbt,mxvect,mxorb,i,filerr,ioff,koff,j
       integer locnos,lococc,loctop,nsb(8),nsm(8),impt,ivpt
      integer nndxf
      nndxf(i) = (i*(i-1))/2


       write(6,*) ' Input MOCOEF or NOCOEF file'
       read(5,*) filename
       nofile=10
       open(unit=nofile,file=filename(1:len_trim(filename)),
     .           form='formatted',err=90)
c     #    read in the title and scalar variables.
c
      call moread(nofile, 10, filerr, syserr, mxtitle, ntit, title,
     & afmt, nsym, nbpsy, nmpsy, labels, 0, core )
c
c     #    read in the NO coeficients
c
      nnbt   = 0
      mxvect = 0
      mxorb  = 0
      do i = 1,nsym
         mxvect = mxvect + nbpsy(i)**2
         mxorb  = mxorb + nbpsy(i)
         nnbt   = nnbt + nndxf( nbpsy(i)+1 )
      enddo
c
      locnos   = 1
      lococc   = locnos + mxvect
      loctop   = lococc + mxorb 
      if (loctop .gt. lcore) call bummer(
     & 'insufficient core memory, need ',loctop,faterr)
c
      call moread(nofile, 20, filerr, syserr, mxtitle, ntit, title,
     & afmt, nsym, nbpsy, nmpsy, labels, mxvect,core(locnos) )
c
      call prblks('MO coefficients',
     & core(locnos),nsym,nbpsy,nmpsy,' bfn','  mo',1,nlist)
*@ifdef obsolete
*c print in scilab format (full matrix)
*c   expand to full matrix 
*         if (loctop+mxorb*mxorb.gt.lcore) 
*     .     call bummer('insufficient memory for expansion; need',
*     .     loctop+mxorb*mxorb,faterr)
*         core(loctop+1:loctop+mxorb*mxorb)=0.0d0
*         ioff=0
*         koff=0
*         do i=1,nsym
*          do j=1,nbpsy(i)
*           call dcopy(nbpsy(i),core(locnos+koff),1,core(loctop+ioff),1)
*           koff=koff+nbpsy(i)
*           ioff=ioff+mxorb
*          enddo
*           ioff=ioff+nbpsy(i)
*         enddo
**
*         do i=1,mxorb
*            write(nlist,55) core(loctop+(i-1)*mxorb : loctop+i*mxorb)
*         enddo
*  55     format(10(f12.9," ,"))
*@endif 


c
c     #    read in the occupations
c
      call moread(nofile, 40, filerr, syserr, mxtitle, ntit, title,
     & afmt, nsym, nbpsy, nmpsy, labels, mxorb,core(lococc))
      if (filerr.ne.0) return 
c
c
      nsb(1)=0
      nsm(1)=0
      do isym=2,nsym
         nsb(isym)=nsb(isym)+nbpsy(isym-1)
         nsm(isym)=nsm(isym)+nmpsy(isym-1)
      enddo
c
         impt=1
         ivpt=1
         do isym=1,nsym
            if(nmpsy(isym).ne.0)then
               write(nlist,'(/10x,2a)')
     &          'Natural orbitals, block: ',labels(isym)
               call prvblk(core(locnos-1+impt),core(lococc-1+ivpt),
     &          nbpsy(isym),nbpsy(isym),nmpsy(isym),
     &          nsb(isym),nsm(isym),' bfn','  no',' occ(*)=',1,nlist)
               impt=impt+nbpsy(isym)*nmpsy(isym)
               ivpt=ivpt+nmpsy(isym)
            endif
         enddo
c
      return

 90   write(6,*) 'could not open ',filename
      call bummer('print_mo: could not open file',0,2)


      end
