!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program argnew
cversion 5.4.0.0
c  ********************************************************************
c  *   written by Hoechtl Peter, University of Vienna, May. 14. 1996  *
c  ********************************************************************
c  input files:
c               argosin
c               geom
c  output file:
c               argosin.new
c
c  this program copies the coordinates for the atoms from
c  the geom-file in the argosin.new. This new file remains
c  the same as the argosin, only the coordinates are changed.
c  The format is taken from iargos.
c
c  read title-line; only 79 characters because some machines
c  will produce an additional line
          character*79 title(5)
c  declarations of the variables
          integer msup,mstup,mrcrup,mconsp,msfup,mnsfup,mccup,mgup
     +        mcup,mconp,mrup,mctup,mcsup,mgcsup,mcrup
c
      parameter (msup=12,mstup=8,mrcrup=8,mconsp=200,msfup=52,
     +  mnsfup=((msfup*(msfup+1))/2),mccup=182,mgup=49,mcup=12,
     +  mconup=24,mrup=40,mctup=40,mcsup=40,mgcsup=10,mcrup=77)
c
      integer nf(msup),nc(msup),ncon(mconsp),nrcr(mconsp),
     +        lmnp1(mconsp),mcons(msfup),iaords(mgcsup),
     +        mcrs(msup),irep(mrup),nd(mstup),ncr(mcrup),
     +        isocoef(mctup,mcsup),igen(mcup,mgup)
c
          real*8 zet(mconup,mconsp),ccr(mcrup),zcr(mcrup),
     +       eta(mrcrup,mconup,mconsp),x(mcup),y(mcup),z(mcup),
     +       chg(msup)
c
      character*3 ityp(mstup),mtype(msup)
      integer i,j,k,l
      integer ngen,naords,ncons,ngcs,itol,icut,aoints,only1e,
     +        inrm,ncrs,l1rec,l2rec,aoint2,fsplit,nst,ndpt,
     +        icsu,ictu,nbfcr,llsu,lcru,ns,nrep,p1,p2,p3,iconu,
     +        igcs,natom
c  declarations of variables for geom
      character*2 atombez(99)
      real*8 ord(99),gx(99),gy(99),gz(99),n
c     read in coordinates from geom file
      open(6,file='geom',status='old')
      do 170 i=1,99
        read(6,180,end=190) atombez(i),ord(i),gx(i),gy(i),gz(i),n
 180  format (1x,a2,4x,f3.1,4f14.8)
 170  continue
 190  i=i-1
          number=i
      close(6)
      open(5,file='argosin',status='old')
      open(7,file='argosin.new',status='unknown')
c
      read(5,'(a70)') title(1)
      ngen=0
      ns=0
      naords=0 
      ncons=0
      ngcs=0
      itol=0
      icut=0 
      aoints=0
      only1e=0
      inrm=0
      ncrs=0
      l1rec=0
      l2rec=0 
      aoint2=0 
      fsplit=0
      read(5,*) ngen,ns,naords,ncons,ngcs,itol,icut,aoints,
     +          only1e,inrm,ncrs,l1rec,l2rec,aoint2,fsplit
      read(5,10) nst, (nd(i),ityp(i),i=1,nst)
      write(7,'(a79)') title(1)
      write(7,5) ngen,ns,naords,ncons,ngcs,itol,icut,aoints,
     +          only1e,inrm,ncrs,l1rec,l2rec,aoint2,fsplit
      write(7,10) nst, (nd(i),ityp(i),i=1,nst)
   5  format(26i4)
  10  format(i3,12(i3,a3))
      read(5,*) ndpt
      write(7,'(i3)') ndpt
      do 20 i=1,ndpt
        read(5,*) p1,p2,p3
        write(7,5) p1,p2,p3
  20  continue
      do 30 i=1, naords
        read(5,*) nrep,(irep(j),j=1,nrep)
        write(7,25) nrep,(irep(j),j=1,nrep)
  25  format(15(1x,i4))
  30  continue
      do 40 i=1,ngcs
        read(5,*) icsu,ictu,iaords(i)
        write(7,25) icsu,ictu,iaords(i)
        do 40 j=1,icsu
          read(5,*) (isocoef(k,j),k=1,ictu)
          write(7,25) (isocoef(k,j),k=1,ictu)
  40  continue
      do 50 i=1,ncons
        read(5,*) iconu,lmnp1(i),nrcr(i)
        write(7,25) iconu,lmnp1(i),nrcr(i)
        do 50 j=1,iconu
          read(5,*) zet(j,i), (eta(k,j,i),k=1,nrcr(i))
          write(7,55) zet(j,i), (eta(k,j,i),k=1,nrcr(i))
  55  format(f15.7,(t21,4f12.7))
  50  continue
      if (ncrs.ne.0) then
        do 60 ircs=1,ncrs
          read(5,*) lcru,llsu
          write(7,25) lcru,llsu
          if (lcru.ge.0) then
            do 70 l=0,lcru
              read(5,*) nbfcr
              write(7,25) nbfcr
              do 80 k=1,nbfcr
                read(5,*) ncr(k),zcr(k),ccr(k)
                write(7,85) ncr(k),zcr(k),ccr(k)
  85  format(i4,2f15.7)
  80          continue
  70        continue
          endif
          if (llsu.ge.1) then
            do 90 l=1,llsu
              read(5,*) nbfcr
              write(7,25) nbfcr
              do 100 k=1,nbfcr
                read(5,*) ncr(k),zcr(k),ccr(k)
                write(7,95) ncr(k),zcr(k),ccr(k)
  95  format(i4,2f16.8)
 100          continue
  90        continue
          endif
  60    continue
      endif
      natom=0
      do 110 is=1,ns
        read(5,120) mtype(is),nf(is),nc(is),chg(is)
        write(7,120) mtype(is),nf(is),nc(is),chg(is)
 120    format(a3,2i3,f3.0)
        do 130 j=1,nc(is)
          natom=natom+1
          read(5,*) x(j),y(j),z(j)
          write(7,'(3f14.8)') gx(natom),gy(natom),gz(natom)
 130    continue
        if (nc(is).ne.1) then
          do 140 j=1,ngen
            read(5,*) (igen(k,j),k=1,nc(is))
            write(7,5) (igen(k,j),k=1,nc(is))
 140      continue
        endif
        do 150 j=1,nf(is)
          read(5,*) mcons(j),igcs
          write(7,5) mcons(j),igcs
 150    continue
        if (ncrs.ne.0) then
          read(5,*) mcrs(is)
          write(7,5) mcrs(is)
        endif
 110  continue
      do 160 i=2,4
        read(5,'(a79)',end=200) title(i)
c       if (title(i).eq.' ') exit
        write(7,'(a79)') title(i)
 160  continue
 200  continue
      close(5)
      close(7)
      call bummer('normal termination',0,3)
      stop 'end of argnew'
      end
