!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c 2011-Jan-25: all arrays consistently shaped with parameter natmax.
c some arrays may have become unneccessarily big. but memory requirements
c should still be very low compared to the other programs (-felix plasser)
      PROGRAM SYMCOE
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL BASDEF,ECP
      CHARACTER*8 BLKTYP, BASIS, BLANK, BLOCKS,BLOCKI, WRDKEY
      CHARACTER*4 IBASIS, IECP, IBLANK, KEYECP, KEYEFC, KEYFLD
      CHARACTER*4 IEFC, IFLD
      integer natmax
      parameter (natmax=255)
      COMMON/TIMES/TIMLIM,TI,TX,TIM
      COMMON/OUTPUT/NPRINT
      COMMON/RESTAR/NREST
      COMMON/INFOA/NAT,ICH,MUL,NUM,NX,NE,NA,NB,ZAN(NATMAX),C(3,NATMAX)
      COMMON/NSHEL/KMIN(2*natmax),KMAX(2*natmax)
      COMMON/BASIS/NORMF,NORMP
      COMMON/INTOPT/BLKTYP
      COMMON/ECPOPT/IECP
      COMMON/FLDPAR/FLDX,FLDY,FLDZ,IFLD
      COMMON/SYMTRY/INVT(2*natmax),NT,NTMAX,NTWD,NOSYM
      COMMON/SYMMOL/IGROUP,NAXIS,LINEAR
      character*5 group
      COMMON/SYMNAM/GROUP
      COMMON/SYMSHL/MAPCTR(natmax,2*natmax)
      COMMON/IOFILE/IR,IR2,IW,IP,IJK,IPK,IDAF,NAV,IODA(99)
C     COMMON/RUNLAB/ANAM(50),BNAM(50)
      DATA BASIS /' $BASIS '/
      DATA BLANK,BLOCKS,BLOCKI /'        ','  BLOCKS','  BLOCKI'/
      DATA IBLANK /'    '/
      DATA KEYECP /' ECP'/
      DATA KEYEFC /' EFC'/
      DATA KEYFLD /' FLD'/
      DATA ZERO /0.0D+00/
c      DATA NATMAX /255/
C
C     READ SYMMETRY POINT GROUP OF THE MOLECULE,
C     AND GENERATE THE TRANSFORMATION MATRICES.
C
      NPRINT=1
      IR=30
      IR2=31
      IR3=32
      IW=6
C
C.....symmetry information input
      open(unit=IR,file='infofl',status='old')
C
C.....oriented geometry file
      open(unit=IR2,file='geom',status='old')
C
C.....rotation matrix
      open(unit=IR3,file='rotmax',status='old')
C
C.....conversion table
      open(unit=9,file='atable',status='unknown')
C
      CALL PTGRP
C
C     ----- TRANSFORMATION OF ATOMS -----
C
      CALL ATOMT
C
C     ----- GET SYMMETRY UNIQUE CENTERS -----
C
      CALL UNIK
C
      call bummer('normal termination',0,3)
      STOP
      END
C
C---------------------------------------------------------------------
C
      SUBROUTINE ATOMT
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL OUT
      LOGICAL POL
      LOGICAL BASLIB,BASSTO,BASGEN,BASDEF
      CHARACTER*4 LABEL, ITYPE
      CHARACTER*8 DOTS, DASH, ENDWRD, A
      CHARACTER*2 ALAB
      integer natmax
      parameter (natmax=255)
      COMMON/MACHIN/ISINGL
      COMMON/RESTAR/NREST
      COMMON/OUTPUT/NPRINT
      COMMON/IOFILE/IR,IR2,IW,IP,IJK,IPK,IDAF,NAV,IODA(99)
      COMMON/INFOA/NAT,ICH,MUL,NUM,NX,NE,NA,NB,ZAN(NATMAX),C(3,50)
      COMMON/NSHEL/KMIN(2*natmax),KMAX(2*natmax)
      COMMON/BASIS/NORMF,NORMP
      COMMON/SYMTRY/INVT(2*natmax),NT,NTMAX,NTWD,NOSYM
      COMMON/SYMMOL/IGROUP,NAXIS,LINEAR
      COMMON/SYMSHL/MAPCTR(natmax,2*natmax)
      COMMON/SYMTRF/XSMAL,YSMAL,ZSMAL,XNEW,YNEW,ZNEW,XP,YP,ZP
      COMMON/FRAME/U1,U2,U3,V1,V2,V3,W1,W2,W3,X0,Y0,Z0
      COMMON/ISOPAC/INDIN(48),INDOUT(12)
C     COMMON/RUNLAB/A(50),B(50)
      COMMON/INTCIN/CCO(7,20,20),INT(7,20,20,4),NTY(7),NIC(7,20),
     1 LAN(7,20)
      COMMON/ATM/ATM(NATMAX),ATMS(NATMAX)
cvp
      character*4 tipus, wort
      DIMENSION tipus(7),wort(3),AT(4),IA(4)
      DIMENSION BOND(NATMAX),ALPHA(NATMAX),BETA(NATMAX),
     1 SIGN(NATMAX),ICONX(NATMAX),
     1 IATCON(3,NATMAX),INATOM(NATMAX)
      DIMENSION NBFS(23),MINF(23),MAXF(23),LABEL(23),NANGM(23)
      DIMENSION EXX(6),CSS(6),CPP(6),CDD(6)
      DIMENSION STOSF(36,6),RLEESF(36,2),SCC(3)
      DIMENSION STOSF1(36),STOSF2(36),STOSF3(36),
     1          STOSF4(36),STOSF5(36),STOSF6(36)
      DIMENSION NS(NATMAX),KS(NATMAX)
      DIMENSION INTYP(2*natmax)
      DIMENSION CSINP(440),CPINP(440),CDINP(440),CFINP(440),CGINP(440)
      DIMENSION RMAX(3,3),I1(3),NI1(3),OC(3,NATMAX)
c     DATA NSHMAX,NGSMAX /120,440/
      DATA RLEESF/1.20D+00,0.00D+00,1.03D+00,1.03D+00,1.03D+00,1.00D+00,
     1            0.99D+00,0.99D+00,1.00D+00,1.00D+00,1.00D+00,1.00D+00,
     2            1.00D+00,1.00D+00,1.00D+00,1.00D+00,1.00D+00,1.00D+00,
     3         18*0.00D+00,
     4            1.15D+00,0.00D+00,1.12D+00,1.12D+00,1.12D+00,1.04D+00,
     5            0.98D+00,0.98D+00,1.00D+00,1.00D+00,1.00D+00,1.00D+00,
     6            1.00D+00,1.00D+00,1.02D+00,1.01D+00,1.01D+00,1.00D+00,
     7         18*0.00D+00/
      DATA PI32 /5.56832799683170D+00/
      DATA TOL /1.0D-8/
      DATA TOL2 /1.0D-5/
      DATA DOTS,DASH /' . . . .','       -'/
      DATA ZERO,ONE,PT5,PT75 /0.0D+00,1.0D+00,0.5D+00,0.75D+00/
      DATA PT187,PT6562 /1.875D+00,6.5625D+00/
      DATA BOHR /0.52917715D+00/
      DATA ENDWRD /' $END   '/
      DATA tipus/'stre','bend','out ','tors','lin1','lin2','tor2'/
      DATA wort/'k   ','    ','stop'/
C
      OUT=NPRINT.EQ.1
      UNITS=ONE/BOHR
      IR3=32
      REWIND IR
      NAT=0
C
C     ----- READ IN CARTESIAN COORDINATES FROM GEOM.SDR -----
C
  100 READ(IR2,1000,END=200)ALAB,ATMNU,X,Y,Z,ATMAS
c     changed by ph
c     TOL <-> TOL2
      IF (ABS(X).LT.TOL) X=ZERO
      IF (ABS(Y).LT.TOL) Y=ZERO
      IF (ABS(Z).LT.TOL) Z=ZERO
      WRITE(6,1000)ALAB,ATMNU,X,Y,Z,ATMAS
      NAT=NAT+1
      OC(1,NAT)=X
      OC(2,NAT)=Y
      OC(3,NAT)=Z
      ATM(NAT)=ATMNU
      ATMS(NAT)=ATMAS
      GO TO 100
  200 CONTINUE
      REWIND IR2
C
C     ----- READ IN ROTATION MATRIX FOR GEOM.SDR -----
C           AND PRINT THE CONVERSION FOR X,Y,Z
C
      DO 500 I=1,3
      READ(IR3,*) (RMAX(I,J),J=1,3)
      write(6,*) (RMAX(I,J),J=1,3)
  500 CONTINUE
      REWIND IR3
C
      DO 300 I=1,3
      write(6,*) 'I',I
      IONE=0
      DO 400 J=1,3
      write(6,*) 'J',J
      ISIGN=1
      IF (RMAX(I,J).LT.ZERO) ISIGN=-1
      AR=ABS(RMAX(I,J))
      CHECK=ABS(AR-ONE)
      IF ((AR.GT.TOL2).AND.(CHECK.GT.TOL2)) STOP
     1 'error in file rotmax'
      IF (CHECK.LE.TOL) THEN
         IONE=IONE+1
         IF (IONE.EQ.2) STOP 'error in file rotmax'
         I1(I)=ISIGN*J
C        I1(J)=ISIGN*I
      ENDIF
  400 CONTINUE
      IF (IONE.EQ.0) STOP 'error in file rotmax'
  300 CONTINUE
C
C     If D2h, or C2v, then re-orient the molecule,
C
C      IF (IGROUP.EQ.9) GO TO 90
C      IF (IGROUP.EQ.7) GO TO 70
       DO IAT=1,NAT
         C(1,IAT)=OC(1,IAT)
         C(2,IAT)=OC(2,IAT)
         C(3,IAT)=OC(3,IAT)
      ENDDO
      NI1(1)=I1(1)
      NI1(2)=I1(2)
      NI1(3)=I1(3)
      GO TO 600
C
  600 CONTINUE
      DO IAT=1,NAT
         write(6,'(i3,2x,3f14.8)') IAT,(C(I,IAT),I=1,3)
      ENDDO
C     WRITE(9,'(3I5)') (I1(I),I=1,3)
      WRITE(9,'(3I5)') (NI1(I),I=1,3)
C
C     FORM TRANSFORMATION TABLES FOR ATOMS
C
      DO 3050 IAT=1,NAT
      X=C(1,IAT)
      Y=C(2,IAT)
      Z=C(3,IAT)
C     NS1=KS(IAT)-1
C     NS2=NS(IAT)
      CALL LCLFRM(X,Y,Z,XS,YS,ZS)
      XSMAL=XS
      YSMAL=YS
      ZSMAL=ZS
      DO 3040 IT=1,NT
      NN=9*(IT-1)
      CALL TRFSYM(NN)
      CALL MSTFRM
C     WRITE(IW,*) IT,XP,YP,ZP
      DO 3010 I=1,NAT
      TEST=(XP-C(1,I))**2+(YP-C(2,I))**2+(ZP-C(3,I))**2
      IF(TEST.GT.TOL) GO TO 3010
      ICTR=I
      GO TO 3020
 3010 CONTINUE
 3020 MAPCTR(IAT,IT)=ICTR
 3040 CONTINUE
 3050 CONTINUE
C
C     PRINT THE TRANSFORMATION TABLE OF ATOMS
C
      WRITE(IW,9997)
      DO 4000 IT=1,NT
      WRITE(IW,9998) IT
      WRITE(IW,9995) (MAPCTR(IAT,IT),IAT=1,NAT)
 4000 CONTINUE
  920 FORMAT(a3,3x,i3)
  930 FORMAT(3f14.8)
 9992 FORMAT(20x)
 9993 FORMAT(10x,f10.5)
 9994 FORMAT(a4,6x,f10.4,a4,i4,2x,5f10.4)
 9995 FORMAT(30I4)
 9996 FORMAT(1X,'ATOM',1X,A8,1X,'X=',F14.8,1X,'Y=',F14.8,1X,'Z=',F14.8)
 9997 FORMAT(//,20X,20(1H-),/,20X,'TRANSFORMATION TABLE',/,
     120X,20(1H-))
 9998 FORMAT(/,1X,'TRANSFORMATION',5X,I3,/)
 9999 FORMAT(A8,3F14.11)
 1000 FORMAT(1X,A2,2x,F5.1,4F14.8)
      RETURN
      END
C
C--------------------------------------------------------------------
C
      SUBROUTINE PTGRP
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*5 GRP, GROUP
      CHARACTER*8 DRC, DIRECT
      CHARACTER*3 GROUPA(8),GRPA
      LOGICAL OUT
      LOGICAL ALIGN
      integer natmax
      parameter (natmax=255)
      COMMON/OUTPUT/NPRINT
      COMMON/PRINT/DPRINT,FPRINT,GPRINT
      COMMON/IOFILE/IR,IR2,IW,IP,IJK,IPK,IDAF,NAV,IODA(99)
      COMMON/SYMTRY/INVT(2*natmax),NT,NTMAX,NTWD,NOSYM
      COMMON/SYMTRF/XSMAL,YSMAL,ZSMAL,XNEW,YNEW,ZNEW,XP,YP,ZP
      COMMON/FRAME/U1,U2,U3,V1,V2,V3,W1,W2,W3,X0,Y0,Z0
      COMMON/SYMMOL/IGROUP,NAXIS,LINEAR
      COMMON/SYMNAM/GROUP
      COMMON/SYMMAT/T(5000)
      COMMON/ORIENT/ALIGN
      COMMON/IRREPS/GAM(2*natmax,2*natmax),TRACEP(2*natmax),
     1 TRACED(2*natmax),CHARR(2*natmax,14),TRACEF(2*natmax)
      COMMON/CIPAS /IRPNAM(14),
     1  LAMBDA(14),IADDR(14),IPA(14)
      DIMENSION GRP(19),DRC(2)
      DATA GRP /'C1   ','CS   ','CI   ','CN   ','S2N  ','CNH  ',
     1  'CNV  ','DN   ','DNH  ','DND  ','CINFV','DINFH','T    ',
     1  'TH   ','TD   ','O    ','OH   ','I    ','IH   '/
      DATA DRC /'NORMAL  ','PARALLEL'/
      DATA ZERO,PT5,ONE,THREE /0.0D+00,0.5D+00,1.0D+00,3.0D+00/
      DATA TOL /1.0D-10/
      DATA PI2 /6.28318530717958D+00/
      DATA GROUPA/'C1 ','CS ','C2 ','CI ','D2 ','C2V','C2H','D2H'/
 9999 FORMAT(A5,2I5,3F3.1)
 9998 FORMAT(' ARE YOU KIDDING... YOU SHOULD GIVE UP...')
 9997 FORMAT(9F10.5)
 9996 FORMAT(3F10.5,A8)
 9995 FORMAT(' LINEAR MOLECULE , POINT GROUP IS CINFV OR DINFH ',/,
     1 ' PLEASE USE GROUP C4V OR D4H...')
 9994 FORMAT(' ILLEGAL POINT GROUP INPUT. STOP..')
 9991 FORMAT(/,' THE POINT GROUP OF THE MOLECULE IS ...',A8,/,
     1 ' THE ORDER OF THE PRINCIPAL AXIS IS ...',I5)
 9990 FORMAT(/,' THE ORIGIN OF THE LOCAL FRAME IS AT X =  ',F10.5,
     1 ' Y = ',F10.5,' Z = ',F10.5,/,
     1 ' DIRECTOR COSINES OF THE NEW AXES ',/,34X,3(5X,F10.5),/,34X,
     1 3(5X,F10.5),/,34X,3(5X,F10.5))
 9989 FORMAT(' ROTATIONS ABOUT PRINCIPAL AXIS')
 9988 FORMAT(' SIGMA-H FOLLOWED BY ROTATIONS')
 9987 FORMAT(' C2 FOLLOWED BY ROTATIONS ')
 9986 FORMAT(' SIGMA-D FOLLOWED BY ROTATIONS')
 9985 FORMAT(' SIGMA-V FOLLOWED BY ROTATIONS')
 9984 FORMAT(/,10X,' CENTER OF SYMMETRY AT X = ',F10.5,' Y = ',F10.5,
     1 ' Z = ',F10.5)
 9983 FORMAT(/,' PLANE OF SYMMETRY DEFINED BY ITS NORMAL U = ',F10.5,
     1 ' V = ',F10.5,' W = ',F10.5)
 9982 FORMAT(/,10X,3F15.9,/,10X,3F15.9,/,10X,3F15.9)
 9981 FORMAT(' C2 FOLLOWED BY SIGMA-H FOLLOWED BY ROTATIONS')
 9980 FORMAT(' SIGMA-D FOLLOWED BY C2 FOLLOWED BY ROTATIONS')
 9979 FORMAT(' S2N ROTATION FOLLOWED BY ROTATIONS')
 9978 FORMAT(' THE MOLECULE IS LINEAR ')
C
      READ(IR,'(a3)') GRPA
       call allcap(grpa)
      do i=1,8
       if (GRPA.eq.GROUPA(i)) IGROUPA=i
      enddo
      go to (11,12,13,14,15,16,17,18), IGROUPA
   11 GROUP='C1   '
      NAXIS=1
      go to 2000
   12 GROUP='CS   '
      NAXIS=1
      go to 2000
   13 GROUP='CN   '
      NAXIS=2
      go to 2000
   14 GROUP='CI   '
      NAXIS=1
      go to 2000
   15 GROUP='DN   '
      NAXIS=2
      go to 2000
   16 GROUP='CNV  '
      NAXIS=2
      go to 2000
   17 GROUP='CNH  '
      NAXIS=2
      go to 2000
   18 GROUP='DNH  '
      NAXIS=2
      go to 2000
C
 2000 continue
      OUT=NPRINT.EQ.1
      LINEAR=0
      DPRINT=0
      FPRINT=0
      GPRINT=0
C      READ(IR,9999) GROUP,NAXIS,LINEAR,DPRINT,FPRINT,GPRINT
      IF(NAXIS.LE.0) NAXIS=1
      IGROUP=20
      DO 2 I=1,19
    2 IF(GROUP.EQ.GRP(I)) IGROUP=I
      WRITE(IW,9991) GROUP,NAXIS
      IF(LINEAR.NE.0) WRITE(IW,9978)
      IF(IGROUP.LE.19) GO TO 5
      WRITE(IW,9994)
      STOP
    5 IF(IGROUP.LE.3) GO TO 200
      IF(IGROUP.EQ.11.OR.IGROUP.EQ.12) GO TO 200
C
C     DEFINE LOCAL FRAME
C     READ IN PRINCIPAL AXIS   ( 1 CARD )
C     READ IN X-LOCAL AXIS   ( 1 CARD )
C     DEFAULT OPTION _ LOCAL FRAME IDENTICAL TO MASTER FRAME
C
      X0=ZERO
      Y0=ZERO
      Z0=ZERO
      X1=ZERO
      Y1=ZERO
      Z1=ZERO
C      READ(IR,9997) X0,Y0,Z0,X1,Y1,Z1
      RHO= DSQRT((X1-X0)**2+(Y1-Y0)**2+(Z1-Z0)**2)
      IF(RHO.GT.TOL) GO TO 6
      X0=ZERO
      Y0=ZERO
      Z0=ZERO
      X1=ZERO
      Y1=ZERO
      Y2=ZERO
      Z2=ZERO
      Z1=ONE
      X2=ONE
      DIRECT=DRC(2)
      RHO=ONE
      GO TO 7
    6 READ(IR,9996) X2,Y2,Z2,DIRECT
    7 CONTINUE
      IF(DIRECT.NE.DRC(1)) DIRECT=DRC(2)
      ALIGN=X0.EQ.ZERO.AND.Y0.EQ.ZERO.AND.Z0.EQ.ZERO.AND.
     1      X1.EQ.ZERO.AND.Y1.EQ.ZERO               .AND.
     2                     Y2.EQ.ZERO.AND.Z2.EQ.ZERO.AND.
     3      DIRECT.EQ.DRC(2)
      W1=(X1-X0)/RHO
      W2=(Y1-Y0)/RHO
      W3=(Z1-Z0)/RHO
      WW=W1*W1+W2*W2+W3*W3
      X02=X2-X0
      Y02=Y2-Y0
      Z02=Z2-Z0
      RHO=(W1*X02+W2*Y02+W3*Z02)/WW
      DUM=RHO*W1
      X0=X0+DUM
      X02=X02-DUM
      DUM=RHO*W2
      Y0=Y0+DUM
      Y02=Y02-DUM
      DUM=RHO*W3
      Z0=Z0+DUM
      Z02=Z02-DUM
      UU=(X02*X02+Y02*Y02+Z02*Z02)
      U= DSQRT(UU)
      U1=X02/U
      U2=Y02/U
      U3=Z02/U
      V3=W1*U2-W2*U1
      V2=W3*U1-W1*U3
      V1=W2*U3-W3*U2
      IF(DIRECT.EQ.DRC(2)) GO TO 8
      DUM=U1
      U1=V1
      V1=-DUM
      DUM=U2
      U2=V2
      V2=-DUM
      DUM=U3
      U3=V3
      V3=-DUM
    8 CONTINUE
      IF(.NOT.OUT) GO TO 9
      WRITE(IW,9990) X0,Y0,Z0,U1,V1,W1,U2,V2,W2,U3,V3,W3
    9 CONTINUE
      IF((IGROUP.GE.13).AND.(IGROUP.LE.17)) GO TO 200
      IF((IGROUP.EQ.18).OR.(IGROUP.EQ.19)) GO TO 999
C
C     ROTATION ABOUT PRINCIPAL AXIS
C
      NN=0
      N=NAXIS
      ALPHA=ZERO
      ALPH=PI2/ DFLOAT(N)
   10 NN=NN+1
      IF(NN.GT.N) GO TO 20
      COSA=DCOS(ALPHA)
      SINA=DSIN(ALPHA)
      I=9*(NN-1)
      T(I+1)=COSA
      T(I+5)=COSA
      T(I+2)=-SINA
      T(I+4)=SINA
      T(I+3)=ZERO
      T(I+6)=ZERO
      T(I+7)=ZERO
      T(I+8)=ZERO
      T(I+9)=ONE
      ALPHA=ALPHA+ALPH
      GO TO 10
C
C     END OF GROUP 4
C
   20 NT=N
      II=9*NT
      IF(.NOT.OUT) GO TO 24
      WRITE(IW,9989)
      N1=1
      N2=NAXIS
      CALL PRTSYM(N1,N2)
   24 CONTINUE
      IF(IGROUP.EQ.4) GO TO 1000
      IF(IGROUP.EQ.5) GO TO 500
      IF(IGROUP.EQ.7) GO TO 115
      IF(IGROUP.NE.6.AND.IGROUP.NE.9) GO TO 55
C
C     SIGMA-H PLANE  EQUATION (Z=0) IN LOCAL FRAME
C
      NN=0
   30 NN=NN+1
      IF(NN.GT.NT) GO TO 50
C
C     GROUP 6 0R 9
C
      I=II+9*(NN-1)
      DO 40 J=1,8
   40 T(I+J)=T(I+J-II)
      T(I+9)=-T(I+9-II)
      GO TO 30
   50 NT=NT+NT
      II=9*NT
      IF(.NOT.OUT) GO TO 54
      WRITE(IW,9988)
      N1=N2+1
      N2=N2+NAXIS
      CALL PRTSYM(N1,N2)
   54 CONTINUE
C
C     END OF GROUP 6
C
      IF(IGROUP.EQ.6) GO TO 1000
C
C     ONE CP2 AXIS IS THE X-AXIS OF THE LOCAL FRAME
C     GROUP 8 , 9 ,10
C
   55 CONTINUE
      NN=0
   60 NN=NN+1
      IF(NN.GT.NT) GO TO 70
      I=II+9*(NN-1)
      T(I+1)=T(I+1-II)
      T(I+2)=-T(I+2-II)
      T(I+3)=-T(I+3-II)
      T(I+4)=T(I+4-II)
      T(I+5)=-T(I+5-II)
      T(I+6)=-T(I+6-II)
      T(I+7)=T(I+7-II)
      T(I+8)=-T(I+8-II)
      T(I+9)=-T(I+9-II)
      GO TO 60
   70 NT=NT+NT
      II=9*NT
      IF(.NOT.OUT) GO TO 99
      WRITE(IW,9987)
      N1=N2+1
      N2=N2+NAXIS
      CALL PRTSYM(N1,N2)
      IF(IGROUP.NE.9) GO TO 99
      WRITE(IW,9981)
      N1=N2+1
      N2=N2+NAXIS
      CALL PRTSYM(N1,N2)
   99 CONTINUE
C
C     END OF GROUP 8 AND 9
C
      IF(IGROUP.EQ.8.OR.IGROUP.EQ.9) GO TO 1000
C
C     DND GROUP . EQUATION OF PLANE SIGMA-D IS _
C     DSIN(ALPH/4)*X-DCOS(ALPH/4)*Y=0
C     THE X-AXIS IS THE CP2 AXIS.
C
C     GROUP 10
C
      BETA=PT5*ALPH
      COSA=DCOS(BETA)
      SINA=DSIN(BETA)
      NN=0
  100 NN=NN+1
      IF(NN.GT.NT) GO TO 110
      I=II+9*(NN-1)
      T(I+1)=COSA*T(I+1-II) + SINA*T(I+2-II)
      T(I+2)=SINA*T(I+1-II) - COSA*T(I+2-II)
      T(I+3)=     T(I+3-II)
      T(I+4)=COSA*T(I+4-II) + SINA*T(I+5-II)
      T(I+5)=SINA*T(I+4-II) - COSA*T(I+5-II)
      T(I+6)=     T(I+6-II)
      T(I+7)=COSA*T(I+7-II) + SINA*T(I+8-II)
      T(I+8)=SINA*T(I+7-II) - COSA*T(I+8-II)
      T(I+9)=     T(I+9-II)
      GO TO 100
  110 NT=NT+NT
      II=9*NT
      IF(.NOT.OUT) GO TO 114
      WRITE(IW,9986)
      N1=N2+1
      N2=N2+NAXIS
      CALL PRTSYM(N1,N2)
      WRITE(IW,9980)
      N1=N2+1
      N2=N2+NAXIS
      CALL PRTSYM(N1,N2)
  114 CONTINUE
C
C     END OF GROUP 10
C
      GO TO 1000
C
C     GROUP 7
C     SIGMA-V IS THE (X-Z) PLANE OF LOCAL FRAME
C
  115 CONTINUE
      NN=0
  120 NN=NN+1
      IF(NN.GT.NT) GO TO 130
      I=II+9*(NN-1)
      T(I+1)=T(I+1-II)
      T(I+2)=-T(I+2-II)
      T(I+3)=T(I+3-II)
      T(I+4)=T(I+4-II)
      T(I+5)=-T(I+5-II)
      T(I+6)=T(I+6-II)
      T(I+7)=T(I+7-II)
      T(I+8)=-T(I+8-II)
      T(I+9)=T(I+9-II)
      GO TO 120
  130 NT=NT+NT
      II=9*NT
C
C     END OF GROUP 7
C
      IF(.NOT.OUT) GO TO 1000
      WRITE(IW,9985)
      N1=N2+1
      N2=N2+NAXIS
      CALL PRTSYM(N1,N2)
      GO TO 1000
  200 CONTINUE
      T(1)=ONE
      T(5)=ONE
      T(9)=ONE
      T(2)=ZERO
      T(3)=ZERO
      T(4)=ZERO
      T(6)=ZERO
      T(7)=ZERO
      T(8)=ZERO
      IF(IGROUP.EQ.1) GO TO 210
      IF(IGROUP.EQ.2) GO TO 250
      IF(IGROUP.EQ.3) GO TO 300
      IF(IGROUP.EQ.11.OR.IGROUP.EQ.12) GO TO 400
      GO TO 600
  210 NT=1
      ALIGN=.TRUE.
      X0=ZERO
      Y0=ZERO
      Z0=ZERO
      U1=ONE
      V2=ONE
      W3=ONE
      U2=ZERO
      U3=ZERO
      V1=ZERO
      V3=ZERO
      W1=ZERO
      W2=ZERO
      GO TO 1000
C
C     CS SYMMETRY GROUP
C     THE 3 POINTS 1,2,3 DEFINE SIGMA-H PLANE
C
Ca  250 READ(IR,9997) X1,Y1,Z1,X2,Y2,Z2
ct    RHO=(X2-X1)**2+(Y2-Y1)**2+(Z2-Z1)**2
ct    IF(RHO.GT.TOL) GO TO 251
Ca
  250 CONTINUE
C     DEFAULT OPTION _ PLANE IS THE (X,Y) PLANE
      X1=ZERO
      Y1=ZERO
      Z1=ZERO
      Y2=ZERO
      Z2=ZERO
      X3=ZERO
      Z3=ZERO
      X2=ONE
      Y3=ONE
      GO TO 252
  251 READ(IR,9997) X3,Y3,Z3
  252 CONTINUE
      ALIGN=X1.EQ.ZERO.AND.Y1.EQ.ZERO.AND.Z1.EQ.ZERO.AND.
     1                     Y2.EQ.ZERO.AND.Z2.EQ.ZERO.AND.
     2      X3.EQ.ZERO.AND.               Z3.EQ.ZERO
      NT=2
      W1=(Y2-Y1)*(Z3-Z1)-(Y3-Y1)*(Z2-Z1)
      W2=(Z2-Z1)*(X3-X1)-(Z3-Z1)*(X2-X1)
      W3=(X2-X1)*(Y3-Y1)-(X3-X1)*(Y2-Y1)
      RHO= DSQRT(W1*W1+W2*W2+W3*W3)
      W1=W1/RHO
      W2=W2/RHO
      W3=W3/RHO
      U1=X2-X1
      U2=Y2-Y1
      U3=Z2-Z1
      RHO= DSQRT(U1*U1+U2*U2+U3*U3)
      U1=U1/RHO
      U2=U2/RHO
      U3=U3/RHO
      V1=W2*U3-W3*U2
      V2=W3*U1-W1*U3
      V3=W1*U2-W2*U1
      X0=X1
      Y0=Y1
      Z0=Z1
      T(10)=ONE
      T(14)=ONE
      T(18)=-ONE
      T(11)=ZERO
      T(12)=ZERO
      T(13)=ZERO
      T(15)=ZERO
      T(16)=ZERO
      T(17)=ZERO
      IF(.NOT.OUT) GO TO 1000
      WRITE(IW,9983) W1,W2,W3
      WRITE(IW,9982) U1,V1,W1,U2,V2,W2,U3,V3,W3
      GO TO 1000
C
C     CI SYMMETRY GROUP
C     CENTER OF INVERSION IS (X0,Y0,Z0)
C
ca  300 READ(IR,9997) X0,Y0,Z0
  300 X0=ZERO
      Y0=ZERO
      Z0=ZERO
      ALIGN=X1.EQ.ZERO.AND.Y1.EQ.ZERO.AND.Z1.EQ.ZERO
      IF(.NOT.OUT) WRITE(IW,9984) X0,Y0,Z0
      T(10)=-ONE
      T(14)=-ONE
      T(18)=-ONE
      T(11)=ZERO
      T(12)=ZERO
      T(13)=ZERO
      T(15)=ZERO
      T(16)=ZERO
      T(17)=ZERO
      NT=2
      U1=ONE
      V2=ONE
      W3=ONE
      U2=ZERO
      U3=ZERO
      V1=ZERO
      V3=ZERO
      W1=ZERO
      W2=ZERO
      GO TO 1000
  400 WRITE(IW,9995)
      STOP
  500 NN=0
      BETA=PT5*ALPH
      COSB=DCOS(BETA)
      SINB=DSIN(BETA)
  510 NN=NN+1
      IF(NN.GT.NT) GO TO 520
C
C     S2N GROUP
C     THE PLANE OF SYMMETRY FOR THE IMPROPER ROTATION
C     IS THE (X,Y) PLANE OF THE LOCAL FRAME
C
      I=II+9*(NN-1)
      T(I+1)= COSB*T(I+1-II)+SINB*T(I+2-II)
      T(I+2)=-SINB*T(I+1-II)+COSB*T(I+2-II)
      T(I+3)=     -T(I+3-II)
      T(I+4)= COSB*T(I+4-II)+SINB*T(I+5-II)
      T(I+5)=-SINB*T(I+4-II)+COSB*T(I+5-II)
      T(I+6)=     -T(I+6-II)
      T(I+7)= COSB*T(I+7-II)+SINB*T(I+8-II)
      T(I+8)=-SINB*T(I+7-II)+COSB*T(I+8-II)
      T(I+9)=     -T(I+9-II)
      GO TO 510
  520 NT=NT+NT
      II=9*NT
      IF(.NOT.OUT) GO TO 1000
      WRITE(IW,9979)
      N1=N2+1
      N2=N2+NAXIS
      CALL PRTSYM(N1,N2)
      GO TO 1000
C
C     T GROUP AND OTHERS CONTAINING A SUBGROUP T _
C     LOCAL X,Y,Z ARE THE C2 AXES
C
  600 DO 610 I=10,36
  610 T(I)=ZERO
      T(10)=ONE
      T(23)=ONE
      T(36)=ONE
      T(14)=-ONE
      T(18)=-ONE
      T(19)=-ONE
      T(27)=-ONE
      T(28)=-ONE
      T(32)=-ONE
      DO 620 II=5,12
      I=9*(II-1)
      J=9*(II-5)
      T(I+1)=T(J+7)
      T(I+2)=T(J+8)
      T(I+3)=T(J+9)
      T(I+4)=T(J+1)
      T(I+5)=T(J+2)
      T(I+6)=T(J+3)
      T(I+7)=T(J+4)
      T(I+8)=T(J+5)
  620 T(I+9)=T(J+6)
      NT=12
      IF(IGROUP.EQ.13) GO TO 1000
      IF(IGROUP.EQ.14) GO TO 650
      IF(IGROUP.EQ.15) GO TO 680
      GO TO 670
C
C     TH GROUP
C     EXPAND GROUP BY TAKING DIRECT PRODUCT WITH CI
C
  650 I=9*NT
      DO 660 J=1,I
  660 T(J+I)=-T(J)
      NT=NT+NT
      GO TO 1000
C
C     OCTAHEDRAL GROUP IS DIRECT PRODUCT OF T AND A C4 ROTATION
C     ABOUT Z AXIS
C
  670 SIGN=-ONE
      GO TO 685
C
C     TD GROUP IS DIRECT PRODUCT OF T AND A REFLECTION IN A
C     PLANE ( EQUATION OF THE PLANE   X=Y  )
C
  680 SIGN=ONE
  685 DO 690 II=13,24
      I=9*(II-1)
      J=9*(II-13)
      T(I+1)=T(J+4)*SIGN
      T(I+2)=T(J+5)*SIGN
      T(I+3)=T(J+6)*SIGN
      T(I+4)=T(J+1)
      T(I+5)=T(J+2)
      T(I+6)=T(J+3)
      T(I+7)=T(J+7)
      T(I+8)=T(J+8)
  690 T(I+9)=T(J+9)
      NT=24
      IF(IGROUP.NE.17) GO TO 1000
C
C     OH GROUP IS DIRECT PRODUCT OF O AND CI
C
      I=9*NT
      DO 700 J=1,I
  700 T(J+I)=-T(J)
      NT=48
      GO TO 1000
C
C     IH GROUP
C
  999 NT=120
      CALL ICOS(LAMBDA)
      DO 900 I=1,NT
         II=9*(I-1)
         DO 900 J=1,9
  900       T(II+J)=GAM(I,J+61)
C
C     FIND THE INVERSE TRANSFORMATIONS
C
 1000 CONTINUE
      DO 1002 ITR=1,NT
      NN=9*(ITR-1)
      DO 1001 IT=1,NT
      II=9*(IT-1)
      TEST= T(NN+1)*T(II+1)+T(NN+2)*T(II+4)+T(NN+3)*T(II+7)
     1     +T(NN+4)*T(II+2)+T(NN+5)*T(II+5)+T(NN+6)*T(II+8)
     1     +T(NN+7)*T(II+3)+T(NN+8)*T(II+6)+T(NN+9)*T(II+9)
     1     -THREE
      IF( DABS(TEST).GT.TOL) GO TO 1001
      INVT(ITR)=IT
      GO TO 1002
 1001 CONTINUE
 1002 CONTINUE
C
C     ----- GENERATE TRANSFORMATION MATRICES FOR P, D, BASIS FUNCTIONS -
C
      CALL MATSYM
C
      NTMAX=NT
      IF(NTMAX.EQ.1) NOSYM=1
      RETURN
      END
C
C--------------------------------------------------------------------
C
      SUBROUTINE LCLFRM(X,Y,Z,XS,YS,ZS)
      IMPLICIT REAL*8 (A-H,O-Z)
C
C     CALCULATE THE COORDINATES (XS,YS,ZS) OF A POINT IN THE LOCAL
C     FRAME GIVEN THE COORDINATES (X,Y,Z) IN THE MASTER FRAME
C
      COMMON/FRAME/U1,U2,U3,V1,V2,V3,W1,W2,W3,X0,Y0,Z0
      XS=U1*(X-X0)+U2*(Y-Y0)+U3*(Z-Z0)
      YS=V1*(X-X0)+V2*(Y-Y0)+V3*(Z-Z0)
      ZS=W1*(X-X0)+W2*(Y-Y0)+W3*(Z-Z0)
      RETURN
      END
C
C--------------------------------------------------------------------
C
      SUBROUTINE TRFSYM(NN)
      IMPLICIT REAL*8 (A-H,O-Z)
C
C      CALCULATE THE COORDINATES (XNEW,YNEW,ZNEW) OF THE TRANSFORM
C      OF THE POINT (XOLD,YOLD,ZOLD) UNDER THE TRANSFORMATION T
C
      COMMON/SYMTRF/XOLD,YOLD,ZOLD,XNEW,YNEW,ZNEW,XP,YP,ZP
      COMMON/SYMMAT/T(5000)
      XNEW=XOLD*T(NN+1)+YOLD*T(NN+2)+ZOLD*T(NN+3)
      YNEW=XOLD*T(NN+4)+YOLD*T(NN+5)+ZOLD*T(NN+6)
      ZNEW=XOLD*T(NN+7)+YOLD*T(NN+8)+ZOLD*T(NN+9)
      RETURN
      END
C
C-------------------------------------------------------------------
C
      SUBROUTINE MSTFRM
      IMPLICIT REAL*8 (A-H,O-Z)
C
C     CALCULATE THE COORDINATES (XP,YP,ZP) OF A POINT IN THE MASTER
C     FRAME GIVEN THE COORDINATES (XNEW,YNEW,ZNEW) IN THE LOCAL FRAME
C
      COMMON/SYMTRF/XOLD,YOLD,ZOLD,XNEW,YNEW,ZNEW,XP,YP,ZP
      COMMON/FRAME/U1,U2,U3,V1,V2,V3,W1,W2,W3,X0,Y0,Z0
      XP=X0+U1*XNEW+V1*YNEW+W1*ZNEW
      YP=Y0+U2*XNEW+V2*YNEW+W2*ZNEW
      ZP=Z0+U3*XNEW+V3*YNEW+W3*ZNEW
      RETURN
      END
C
C--------------------------------------------------------------------
C
      SUBROUTINE PRTSYM(N1,N2)
      IMPLICIT REAL*8 (A-H,O-Z)
      integer natmax
      parameter (natmax=255)
      COMMON/IOFILE/IR,IR2,IW,IP,IJK,IPK,IDAF,NAV,IODA(99)
      COMMON/SYMTRY/INVT(2*natmax),NT,NTMAX,NTWD,NOSYM
      COMMON/SYMMAT/T(5000)
      DIMENSION NN(2*natmax)
      IMAX=N1-1
  100 IMIN=IMAX+1
      IMAX=IMAX+4
      IF(IMAX.GT.N2) IMAX=N2
      NJ=9*N1-8
      DO 200 J=1,3
      NI=0
      DO 150 I=IMIN,IMAX
      NN(I)=NJ+NI
  150 NI=NI+9
      WRITE(IW,1000) (T(NN(I)),T(NN(I)+1),T(NN(I)+2),I=IMIN,IMAX)
  200 NJ=NJ+3
      WRITE(IW,1001)
      IF(IMAX.LT.N2) GO TO 100
 1000 FORMAT(4X,4(3F10.5,2H '))
 1001 FORMAT(/)
      RETURN
      END
C
C--------------------------------------------------------------------
C
      SUBROUTINE MATSYM
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL OUT
c     REAL*4 PNAME,DNAME,FNAME,GNAME
      character*4 PNAME,DNAME,FNAME,GNAME
      integer natmax
      parameter (natmax=255)
      COMMON/IOFILE/IR,IR2,IW,IP,IJK,IPK,IDAF,NAV,IODA(99)
      COMMON/OUTPUT/NPRINT
      COMMON/PRINT/DPRINT,FPRINT,GPRINT
      COMMON/RESTAR/NREST
      COMMON/SYMSPD/PTR(3,360),DTR(6,720),FTR(10,1200),GTR(15,1800)
      COMMON/SYMTRY/INVT(2*natmax),NT,NTMAX,NTWD,NOSYM
      COMMON/SYMTRF/XSMAL,YSMAL,ZSMAL,XNEW,YNEW,ZNEW,XP,YP,ZP
      COMMON/FRAME/U1,U2,U3,V1,V2,V3,W1,W2,W3,X0,Y0,Z0
      COMMON/BASIS/NORMF,NORMP
      DIMENSION PNAME(3),DNAME(6),FNAME(10),GNAME(15)
      DIMENSION TRACEP(2*natmax),TRACED(2*natmax),TRACEF(2*natmax)
     1 ,TRACEG(2*natmax)
      DATA PNAME /4HX   ,4HY   ,4HZ   /
      DATA DNAME /4HXX  ,4HYY  ,4HZZ  ,4HXY  ,4HXZ  ,4HYZ  /
      DATA FNAME / 4HXXX ,4HYYY ,4HZZZ ,4HXXY ,4HXXZ ,4HYYX ,4HYYZ ,
     2             4HZZX ,4HZZY ,4HXYZ /
      DATA GNAME / 4HXXXX,4HYYYY,4HZZZZ,4HXXXY,4HXXXZ,4HYYYX,4HYYYZ,
     1             4HZZZX,4HZZZY,4HXXYY,4HXXZZ,4HYYZZ,4HXXYZ,4HYYXZ,
     2             4HZZXY/
      DATA SQRT3 /1.73205080756888D+00/
      DATA SQRT5 /2.23606797749979D+00/
      DATA SQRT7 /2.64575131106459D+00/
      DATA ONE /1.0D+00/
 8869 FORMAT(//)
 8863 FORMAT(/,' TRANSFORMATION OF THE BASIS FUNCTIONS',/)
 8862 FORMAT(8X,10(3X,A4,3X))
 8861 FORMAT(2X,A4,2X,10F10.6)
 8860 FORMAT(1H1)
 8858 FORMAT(/,21X,'TRANSFORMATION NUMBER',I4,/)
 8855 FORMAT(/)
C
      OUT=NPRINT.EQ.1
C
C     ----- CALCULATE TRANSFORMS OF P D F AND G FUNCTIONS
C           FOR ALL SYMETRY OPERATIONS.
C
      X=X0+ONE
      Y=Y0
      Z=Z0
      CALL LCLFRM(X,Y,Z,XS,YS,ZS)
      XSMAL=XS
      YSMAL=YS
      ZSMAL=ZS
      DO 60 IT=1,NT
      NN=9*(IT-1)
      CALL TRFSYM(NN)
      CALL MSTFRM
      N=3*(IT-1)
      PTR(1,N+1)=XP-X0
      PTR(2,N+1)=YP-Y0
      PTR(3,N+1)=ZP-Z0
   60 CONTINUE
      X=X0
      Y=Y0+ONE
      Z=Z0
      CALL LCLFRM(X,Y,Z,XS,YS,ZS)
      XSMAL=XS
      YSMAL=YS
      ZSMAL=ZS
      DO 70 IT=1,NT
      NN=9*(IT-1)
      CALL TRFSYM(NN)
      CALL MSTFRM
      N=3*(IT-1)
      PTR(1,N+2)=XP-X0
      PTR(2,N+2)=YP-Y0
      PTR(3,N+2)=ZP-Z0
   70 CONTINUE
      X=X0
      Y=Y0
      Z=Z0+ONE
      CALL LCLFRM(X,Y,Z,XS,YS,ZS)
      XSMAL=XS
      YSMAL=YS
      ZSMAL=ZS
      DO 80 IT=1,NT
      NN=9*(IT-1)
      CALL TRFSYM(NN)
      CALL MSTFRM
      N=3*(IT-1)
      PTR(1,N+3)=XP-X0
      PTR(2,N+3)=YP-Y0
      PTR(3,N+3)=ZP-Z0
   80 CONTINUE
      DO 120 IT=1,NT
      NP=3*(IT-1)
      ND=6*(IT-1)
      NF=10*(IT-1)
      NG=15*(IT-1)
C
C     -D-
C
      DO 88 I=1,6
      GO TO (81,82,83,84,85,86),I
   81 J=1
      K=1
      GO TO 87
   82 J=2
      K=2
      GO TO 87
   83 J=3
      K=3
      GO TO 87
   84 J=1
      K=2
      GO TO 87
   85 J=1
      K=3
      GO TO 87
   86 J=2
      K=3
   87 DTR(1,ND+I)=PTR(1,NP+J)*PTR(1,NP+K)
      DTR(2,ND+I)=PTR(2,NP+J)*PTR(2,NP+K)
      DTR(3,ND+I)=PTR(3,NP+J)*PTR(3,NP+K)
      DTR(4,ND+I)=PTR(1,NP+J)*PTR(2,NP+K)
     1           +PTR(2,NP+J)*PTR(1,NP+K)
      DTR(5,ND+I)=PTR(1,NP+J)*PTR(3,NP+K)
     1           +PTR(3,NP+J)*PTR(1,NP+K)
      DTR(6,ND+I)=PTR(2,NP+J)*PTR(3,NP+K)
     1           +PTR(3,NP+J)*PTR(2,NP+K)
   88 CONTINUE
C
C     -F-
C
      DO 101 I=1,10
      GO TO (90,91,92,93,94,95,96,97,98,99),I
   90 J=1
      K=1
      GO TO 100
   91 J=2
      K=2
      GO TO 100
   92 J=3
      K=3
      GO TO 100
   93 J=1
      K=2
      GO TO 100
   94 J=1
      K=3
      GO TO 100
   95 J=2
      K=1
      GO TO 100
   96 J=2
      K=3
      GO TO 100
   97 J=3
      K=1
      GO TO 100
   98 J=3
      K=2
      GO TO 100
   99 J=4
      K=3
  100 FTR( 1,NF+I)=DTR(1,ND+J)*PTR(1,NP+K)
      FTR( 2,NF+I)=DTR(2,ND+J)*PTR(2,NP+K)
      FTR( 3,NF+I)=DTR(3,ND+J)*PTR(3,NP+K)
      FTR( 4,NF+I)=DTR(1,ND+J)*PTR(2,NP+K)
     1            +DTR(4,ND+J)*PTR(1,NP+K)
      FTR( 5,NF+I)=DTR(1,ND+J)*PTR(3,NP+K)
     1            +DTR(5,ND+J)*PTR(1,NP+K)
      FTR( 6,NF+I)=DTR(2,ND+J)*PTR(1,NP+K)
     1            +DTR(4,ND+J)*PTR(2,NP+K)
      FTR( 7,NF+I)=DTR(2,ND+J)*PTR(3,NP+K)
     1            +DTR(6,ND+J)*PTR(2,NP+K)
      FTR( 8,NF+I)=DTR(3,ND+J)*PTR(1,NP+K)
     1            +DTR(5,ND+J)*PTR(3,NP+K)
      FTR( 9,NF+I)=DTR(3,ND+J)*PTR(2,NP+K)
     1            +DTR(6,ND+J)*PTR(3,NP+K)
      FTR(10,NF+I)=DTR(4,ND+J)*PTR(3,NP+K)
     1            +DTR(5,ND+J)*PTR(2,NP+K)
     2            +DTR(6,ND+J)*PTR(1,NP+K)
  101 CONTINUE
C
C     -G-
C
      DO 119 I=1,15
      GO TO (103,104,105,106,107,108,109,110,111,112,
     1       113,114,115,116,117),I
  103 J=1
      K=1
      GO TO 118
  104 J=2
      K=2
      GO TO 118
  105 J=3
      K=3
      GO TO 118
  106 J=1
      K=2
      GO TO 118
  107 J=1
      K=3
      GO TO 118
  108 J=2
      K=1
      GO TO 118
  109 J=2
      K=3
      GO TO 118
  110 J=3
      K=1
      GO TO 118
C..... P,D ORBITALS
C
  111 J=3
      K=2
      GO TO 118
  112 J=4
      K=2
      GO TO 118
  113 J=5
      K=3
      GO TO 118
  114 J=7
      K=3
      GO TO 118
  115 J=4
      K=3
      GO TO 118
  116 J=6
      K=3
      GO TO 118
  117 J=8
      K=2
  118 GTR( 1,NG+I)=FTR( 1,NF+J)*PTR(1,NP+K)
      GTR( 2,NG+I)=FTR( 2,NF+J)*PTR(2,NP+K)
      GTR( 3,NG+I)=FTR( 3,NF+J)*PTR(3,NP+K)
      GTR( 4,NG+I)=FTR( 1,NF+J)*PTR(2,NP+K)
     1            +FTR( 4,NF+J)*PTR(1,NP+K)
      GTR( 5,NG+I)=FTR( 1,NF+J)*PTR(3,NP+K)
     1            +FTR( 5,NF+J)*PTR(1,NP+K)
      GTR( 6,NG+I)=FTR( 2,NF+J)*PTR(1,NP+K)
     1            +FTR( 6,NF+J)*PTR(2,NP+K)
      GTR( 7,NG+I)=FTR( 2,NF+J)*PTR(3,NP+K)
     1            +FTR( 7,NF+J)*PTR(2,NP+K)
      GTR( 8,NG+I)=FTR( 3,NF+J)*PTR(1,NP+K)
     1            +FTR( 8,NF+J)*PTR(3,NP+K)
      GTR( 9,NG+I)=FTR( 3,NF+J)*PTR(2,NP+K)
     1            +FTR( 9,NF+J)*PTR(3,NP+K)
      GTR(10,NG+I)=FTR( 4,NF+J)*PTR(2,NP+K)
     1            +FTR( 6,NF+J)*PTR(1,NP+K)
      GTR(11,NG+I)=FTR( 5,NF+J)*PTR(3,NP+K)
     1            +FTR( 8,NF+J)*PTR(1,NP+K)
      GTR(12,NG+I)=FTR( 7,NF+J)*PTR(3,NP+K)
     1            +FTR( 9,NF+J)*PTR(2,NP+K)
      GTR(13,NG+I)=FTR( 4,NF+J)*PTR(3,NP+K)
     1            +FTR( 5,NF+J)*PTR(2,NP+K)
     2            +FTR(10,NF+J)*PTR(1,NP+K)
      GTR(14,NG+I)=FTR( 6,NF+J)*PTR(3,NP+K)
     1            +FTR( 7,NF+J)*PTR(1,NP+K)
     2            +FTR(10,NF+J)*PTR(2,NP+K)
      GTR(15,NG+I)=FTR( 8,NF+J)*PTR(2,NP+K)
     1            +FTR( 9,NF+J)*PTR(1,NP+K)
     2            +FTR(10,NF+J)*PTR(3,NP+K)
  119 CONTINUE
  120 CONTINUE
C
C     ----- SKIP NORMALIZATION -----
C
      NORMF=1
      NORMP=1
      IF(NORMF.EQ.1.AND.NORMP.EQ.1) GO TO 131
C
C     ----- NORMALIZATION -----
C
      DO 130 IT=1,NT
      ND=6*(IT-1)
      NF=10*(IT-1)
      NG=15*(IT-1)
C
C     -D-
C
      DO 122 I=1,6
      IF(I.GT.3) GO TO  121
      DTR(4,ND+I)=DTR(4,ND+I)/SQRT3
      DTR(5,ND+I)=DTR(5,ND+I)/SQRT3
      DTR(6,ND+I)=DTR(6,ND+I)/SQRT3
      GO TO 122
  121 DTR(1,ND+I)=DTR(1,ND+I)*SQRT3
      DTR(2,ND+I)=DTR(2,ND+I)*SQRT3
      DTR(3,ND+I)=DTR(3,ND+I)*SQRT3
  122 CONTINUE
C
C     -F-
C
      DO 125 I=1,10
      IF(I.GT.3) GO TO 123
      FTR( 4,NF+I)=FTR( 4,NF+I)/SQRT5
      FTR( 5,NF+I)=FTR( 5,NF+I)/SQRT5
      FTR( 6,NF+I)=FTR( 6,NF+I)/SQRT5
      FTR( 7,NF+I)=FTR( 7,NF+I)/SQRT5
      FTR( 8,NF+I)=FTR( 8,NF+I)/SQRT5
      FTR( 9,NF+I)=FTR( 9,NF+I)/SQRT5
      FTR(10,NF+I)=FTR(10,NF+I)/(SQRT5*SQRT3)
      GO TO 125
  123 IF(I.GT.9) GO TO 124
      FTR( 1,NF+I)=FTR( 1,NF+I)*SQRT5
      FTR( 2,NF+I)=FTR( 2,NF+I)*SQRT5
      FTR( 3,NF+I)=FTR( 3,NF+I)*SQRT5
      FTR(10,NF+I)=FTR(10,NF+I)/SQRT3
      GO TO 125
  124 FTR( 1,NF+I)=FTR( 1,NF+I)*SQRT5*SQRT3
      FTR( 2,NF+I)=FTR( 2,NF+I)*SQRT5*SQRT3
      FTR( 3,NF+I)=FTR( 3,NF+I)*SQRT5*SQRT3
      FTR( 4,NF+I)=FTR( 4,NF+I)*SQRT3
      FTR( 5,NF+I)=FTR( 5,NF+I)*SQRT3
      FTR( 6,NF+I)=FTR( 6,NF+I)*SQRT3
      FTR( 7,NF+I)=FTR( 7,NF+I)*SQRT3
      FTR( 8,NF+I)=FTR( 8,NF+I)*SQRT3
      FTR( 9,NF+I)=FTR( 9,NF+I)*SQRT3
  125 CONTINUE
C
C     -G-
C
      DO 129 I=1,15
      IF(I.GT.3) GO TO 126
      GTR( 4,NG+I)=GTR( 4,NG+I)/SQRT7
      GTR( 5,NG+I)=GTR( 5,NG+I)/SQRT7
      GTR( 6,NG+I)=GTR( 6,NG+I)/SQRT7
      GTR( 7,NG+I)=GTR( 7,NG+I)/SQRT7
      GTR( 8,NG+I)=GTR( 8,NG+I)/SQRT7
      GTR( 9,NG+I)=GTR( 9,NG+I)/SQRT7
      GTR(10,NG+I)=GTR(10,NG+I)*SQRT3/(SQRT5*SQRT7)
      GTR(11,NG+I)=GTR(11,NG+I)*SQRT3/(SQRT5*SQRT7)
      GTR(12,NG+I)=GTR(12,NG+I)*SQRT3/(SQRT5*SQRT7)
      GTR(13,NG+I)=GTR(13,NG+I)/(SQRT5*SQRT7)
      GTR(14,NG+I)=GTR(14,NG+I)/(SQRT5*SQRT7)
      GTR(15,NG+I)=GTR(15,NG+I)/(SQRT5*SQRT7)
      GO TO 129
  126 IF(I.GT.9) GO TO 127
      GTR( 1,NG+I)=GTR( 1,NG+I)*SQRT7
      GTR( 2,NG+I)=GTR( 2,NG+I)*SQRT7
      GTR( 3,NG+I)=GTR( 3,NG+I)*SQRT7
      GTR(10,NG+I)=GTR(10,NG+I)*SQRT3/SQRT5
      GTR(11,NG+I)=GTR(11,NG+I)*SQRT3/SQRT5
      GTR(12,NG+I)=GTR(12,NG+I)*SQRT3/SQRT5
      GTR(13,NG+I)=GTR(13,NG+I)/SQRT5
      GTR(14,NG+I)=GTR(14,NG+I)/SQRT5
      GTR(15,NG+I)=GTR(15,NG+I)/SQRT5
      GO TO 129
  127 IF(I.GT.12) GO TO 128
      GTR( 1,NG+I)=GTR( 1,NG+I)*SQRT7*SQRT5/SQRT3
      GTR( 2,NG+I)=GTR( 2,NG+I)*SQRT7*SQRT5/SQRT3
      GTR( 3,NG+I)=GTR( 3,NG+I)*SQRT7*SQRT5/SQRT3
      GTR( 4,NG+I)=GTR( 4,NG+I)*SQRT5/SQRT3
      GTR( 5,NG+I)=GTR( 5,NG+I)*SQRT5/SQRT3
      GTR( 6,NG+I)=GTR( 6,NG+I)*SQRT5/SQRT3
      GTR( 7,NG+I)=GTR( 7,NG+I)*SQRT5/SQRT3
      GTR( 8,NG+I)=GTR( 8,NG+I)*SQRT5/SQRT3
      GTR( 9,NG+I)=GTR( 9,NG+I)*SQRT5/SQRT3
      GTR(13,NG+I)=GTR(13,NG+I)/SQRT3
      GTR(14,NG+I)=GTR(14,NG+I)/SQRT3
      GTR(15,NG+I)=GTR(15,NG+I)/SQRT3
      GO TO 129
  128 GTR( 1,NG+I)=GTR( 1,NG+I)*SQRT7*SQRT5
      GTR( 2,NG+I)=GTR( 2,NG+I)*SQRT7*SQRT5
      GTR( 3,NG+I)=GTR( 3,NG+I)*SQRT7*SQRT5
      GTR( 4,NG+I)=GTR( 4,NG+I)*SQRT5
      GTR( 5,NG+I)=GTR( 5,NG+I)*SQRT5
      GTR( 6,NG+I)=GTR( 6,NG+I)*SQRT5
      GTR( 7,NG+I)=GTR( 7,NG+I)*SQRT5
      GTR( 8,NG+I)=GTR( 8,NG+I)*SQRT5
      GTR( 9,NG+I)=GTR( 9,NG+I)*SQRT5
      GTR(10,NG+I)=GTR(10,NG+I)*SQRT3
      GTR(11,NG+I)=GTR(11,NG+I)*SQRT3
      GTR(12,NG+I)=GTR(12,NG+I)*SQRT3
  129 CONTINUE
  130 CONTINUE
  131 CONTINUE
C
C     ----- PRINT MATRICES -----
C
      IF(.NOT.OUT) GO TO 300
      WRITE(IW,8863)
      DO 255 IT=1,NT
      WRITE(IW,8860)
      WRITE(IW,8858) IT
C
      NP=3*(IT-1)
      WRITE(IW,8862) (PNAME(J),J=1,3)
      WRITE(IW,8855)
      DO 250 I=1,3
  250 WRITE(IW,8861) PNAME(I),(PTR(I,NP+J),J=1,3)
      WRITE(IW,8869)
      IF(DPRINT.EQ.0.) GO TO 255
C
      ND=6*(IT-1)
      WRITE(IW,8862) (DNAME(J),J=1,6)
      WRITE(IW,8855)
      DO 251 I=1,6
  251 WRITE(IW,8861) DNAME(I),(DTR(I,ND+J),J=1,6)
      WRITE(IW,8869)
      WRITE(IW,8869)
      IF(FPRINT.EQ.0.) GO TO 255
C
      NF=10*(IT-1)
      WRITE(IW,8862) (FNAME(J),J=1,10)
      WRITE(IW,8855)
      DO 252 I=1,10
  252 WRITE(IW,8861) FNAME(I),(FTR(I,NF+J),J=1,10)
      WRITE(IW,8869)
      IF(GPRINT.EQ.0.) GO TO 255
C
      NG=15*(IT-1)
      JMAX=0
  253 JMIN=JMAX+1
      JMAX=JMAX+10
      IF(JMAX.GT.15) JMAX=15
      WRITE(IW,8862) (GNAME(J),J=JMIN,JMAX)
      WRITE(IW,8855)
      DO 254 I=1,15
  254 WRITE(IW,8861) GNAME(I),(GTR(I,NG+J),J=JMIN,JMAX)
      WRITE(IW,8855)
      IF(JMAX.LT.15) GO TO 253
      WRITE(IW,8869)
  255 CONTINUE
  300 CONTINUE
C
C     --WRITE -PTR-,-DTR-,-FTR- AND -GTR- ON IDAF(7,8,9 AND 10) -----
C
      NPF=432
      NDF=1728
      NFF=4800
      NGF=10800
C     IF(NREST.EQ.0) CALL DAWRIT(IDAF,IODA,PTR,NPF, 7,NAV)
C     IF(NREST.EQ.0) CALL DAWRIT(IDAF,IODA,DTR,NDF, 8,NAV)
C     IF(NREST.EQ.0) CALL DAWRIT(IDAF,IODA,FTR,NFF, 9,NAV)
C     IF(NREST.EQ.0) CALL DAWRIT(IDAF,IODA,GTR,NGF,10,NAV)
      RETURN
      END
C
C--------------------------------------------------------------------
C
      SUBROUTINE UNIK
      IMPLICIT REAL*8 (A-H,O-Z)
      INTEGER NONZERO
      PARAMETER (MIRP=10,MNAT=255,MI=80,MGN=1)
      PARAMETER (MSYM=4,MCOM=10)
      integer natmax
      parameter (natmax=255)
      COMMON/IOFILE/IR,IR2,IW,IP,IJK,IPK,IDAF,NAV,IODA(99)
      COMMON/INFOA/NAT,ICH,MUL,NUM,NX,NE,NA,NB,ZAN(NATMAX),C(3,NATMAX)
      COMMON/SYMTRY/INVT(2*natmax),NT,NTMAX,NTWD,NOSYM
      COMMON/SYMBLK/NIRRED,L1SYM(14),L2SYM,L3SYM
      COMMON/SYMSPD/PTR(3,360),DTR(6,720),FTR(10,1200),GTR(15,1800)
      COMMON/IRREPS/GAM(2*natmax,2*natmax),TRACEP(2*natmax),
     1 TRACED(2*natmax),CHARR(2*natmax,14),TRACEF(2*natmax)
      COMMON/SYMSHL/MAPCTR(natmax,2*natmax)
      COMMON/CIPAS /IRPNAM(14),
     1  LAMBDA(14),IADDR(14),IPA(14)
      COMMON/INTCIN/CCO(7,20,20),INT(7,20,20,4),NTY(7),NIC(7,20),
     1 LAN(7,20)
      COMMON/ATM/ATM(NATMAX),ATMS(NATMAX)
      COMMON/SYMMOL/IGROUP,NAXIS,LINEAR
      DIMENSION MAPINT(3*MNAT-6,2*natmax),MEQ(MNAT,MNAT),IJ(MNAT)
      INTEGER CC
      CHARACTER*3 GROUP(8)
      DATA ZERO,ONE,TOL,TOL2/0.0D+00,1.0D+00,1.0D-4,1.0D-8/
      DATA GROUP/'C1 ','CS ','C2 ','CI ','D2 ','C2V','C2H','D2H'/
C
C
C     ----- CHECK NO. OF ATOMS -----
C
      IF (NAT.GT.MNAT) THEN
         WRITE (6,*) 'MNAT TOO SMALL|' , NAT
         STOP
      ENDIF
C
C     ----- GET SYMMETRY EQUIVALENT GROUPS -----
C
      MEQ(1,1)=1
      I=0
      DO 50 IAT=1,NAT
        IF (I.NE.0) THEN
         DO 51 II=1,I
            DO 51 JJ=1,IJ(II)
   51           IF (MEQ(II,JJ).EQ.IAT) GO TO 50
        ENDIF
        write(6,*)'IAT',IAT
        I=I+1
        IJ(I)=1
        MEQ(I,1)=IAT
        DO 55 IT=1,NT
                 DO 52 JJ=1,IJ(I)
   52             IF (MAPCTR(IAT,IT).EQ.MEQ(I,JJ)) GO TO 55
              IJ(I)=IJ(I)+1
              MEQ(I,IJ(I))=MAPCTR(IAT,IT)
              write(6,*)'MEQ',MEQ(I,IJ(I))
   55   CONTINUE
   50 CONTINUE
      NEQ=I
C
C     ----- PRINT THE CONVERSION OF ATOMS -----
C
      write(6,*)'number of equivalent groups',NEQ
      write(9,'(8i5)') ((MEQ(II,JJ),JJ=1,IJ(II)),
     1 II=1,NEQ)
      do 56 II=1,NEQ
        write(6,*)'atom',(MEQ(II,JJ),JJ=1,IJ(II))
       do 49 JJ=1,IJ(II)
         write(6,*)(C(IC,MEQ(II,JJ)),IC=1,3)
   49  CONTINUE
   56 CONTINUE
C
C     ----- PRINT THE UNIQUE CENTERS -----
C
      IF (IGROUP.EQ.1) IGR=1
      IF (IGROUP.EQ.2) IGR=2
      IF (IGROUP.EQ.3) IGR=4
      IF (IGROUP.EQ.4) IGR=3
      IF (IGROUP.EQ.6) IGR=7
      IF (IGROUP.EQ.7) IGR=6
      IF (IGROUP.EQ.8) IGR=5
      IF (IGROUP.EQ.9) IGR=8
C
      write(6,*)'the unique centers:'
      do 57 II=1,NEQ
         write(6,*)'atom',MEQ(II,1),(C(IC,MEQ(II,1)),IC=1,3)
   57 CONTINUE
      OPEN(UNIT=8,FILE='geom.unik',STATUS='unknown')
      WRITE(8,'(A3)') GROUP(IGR)
      WRITE(8,'(I3)') NEQ
      DO 58 II=1,NEQ
       IATM=ATM(MEQ(II,1))
       WRITE(8,'(I5,3F14.8)')IATM,
     1 (C(IC,MEQ(II,1)),IC=1,3)
   58 CONTINUE
      RETURN
      END
C
C-----------------------------------------------------------------------
C
      SUBROUTINE ICOS(LDIM)
      IMPLICIT REAL*8 (A-H,O-Z)
C
C     ----- IRREDUCIBLE REPRESENTATIONS OF THE -I- AND -IH- GROUPS -----
C
      integer natmax
      parameter (natmax=255)
      COMMON/IRREPS/GAM(2*natmax,2*natmax),TRACEP(2*natmax),
     1 TRACED(2*natmax),CHARR(2*natmax,14),TRACEF(2*natmax)
      COMMON/SYMBLK/NIRRED,L1SYM(14),L2SYM,L3SYM
      COMMON/SYMMOL/IGROUP,NAXIS,LINEAR
      DIMENSION D(natmax,9),F1(natmax,3,3),T(3,3),U(9),V(3,3),W(9)
      DIMENSION IBIAS(11),IDIM(10),LDIM(14)
C
      EQUIVALENCE (F1(1,1,1),D(1,1)),(T(1,1),U(1)),(V(1,1),W(1))
C
      DATA IBIAS  /0,1,10,19,35,60,61,70,79,95,120/
      DATA IDIM   /1,3,3,4,5,1,3,3,4,5/
      DATA TOL    /1.0D-10/
      DATA ZERO   /0.0D+00/
      DATA ONE    /1.0D+00/
      DATA TWO    /2.0D+00/
      DATA THREE  /3.0D+00/
      DATA FOUR   /4.0D+00/
      DATA SIX    /6.0D+00/
      DATA EIGHT  /8.0D+00/
      DATA TWELVE /12.0D+00/
      DATA TWENT4 /24.0D+00/
C
      SQRT2 = DSQRT(2.0D+00)
      SQRT3 = DSQRT(3.0D+00)
      SQRT5 = DSQRT(5.0D+00)
      SQRT6 = DSQRT(6.0D+00)
      SQRT10= DSQRT(10.0D+00)
      SQRT15= DSQRT(15.0D+00)
      SQRT30= DSQRT(30.0D+00)
C
      NIRRED=5
      IF(IGROUP.EQ.19) NIRRED=10
      DO 5 IQW=1,NIRRED
    5 LDIM(IQW)=IDIM(IQW)
C
      NT=3
      DO 10 I=1,60
      GAM(   1,I+60)= ZERO
      GAM(I   ,   1)= ONE
      GAM(I   ,  61)= ONE
      GAM(I+60,   1)= ONE
      GAM(I+60,  61)=-ONE
      DO 10 J=2,60
   10 GAM(I   ,J   )= ZERO
      DO 12 I=1,60
      DO 12 J=1,9
   12 D(I,J)=ZERO
      DO 14 J=1,9,4
   14 D(1,J)=ONE
      DO 16 IRP=2,NIRRED
      IBEGIN=IBIAS(IRP)+1
      ISTEP =IDIM(IRP)+1
      IEND  =IBIAS(IRP+1)
      DO 16 J=IBEGIN,IEND,ISTEP
   16 GAM(1,J)=ONE
      F1(2,1,1)= ONE
      F1(2,2,2)=-ONE/TWO
      F1(2,3,2)= SQRT3/TWO
      F1(2,2,3)=-SQRT3/TWO
      F1(2,3,3)=F1(2,2,2)
      F1(3,1,1)=ONE/THREE
      F1(3,2,1)=-(SQRT2 +THREE*SQRT10)/TWELVE
      F1(3,3,1)=-(SQRT30-      SQRT6 )/TWELVE
      F1(3,1,2)=  SQRT2 *TWO/THREE
      F1(3,2,2)= (ONE   +THREE*SQRT5 )/TWENT4
      F1(3,3,2)= (SQRT15-      SQRT3 )/TWENT4
      F1(3,2,3)=- THREE*F1(3,3,2)
      F1(3,3,3)=  THREE*F1(3,2,2)
      GAM(2,11)=F1(2,2,2)
      GAM(2,12)=F1(2,2,3)
      GAM(2,14)=F1(2,3,2)
      GAM(2,15)=F1(2,2,2)
      GAM(2,19)=ONE
      GAM(3,11)=(ONE-THREE*SQRT5)/EIGHT
      GAM(3,12)=(SQRT15+SQRT3 )/TWENT4
      GAM(3,13)=(SQRT6 +SQRT30)/TWELVE
      GAM(3,14)=-GAM(3,12)*THREE
      GAM(3,15)= GAM(3,11)/THREE
      GAM(3,16)=(SQRT2-THREE*SQRT10)/TWELVE
      GAM(3,18)=-SQRT2*TWO/THREE
      GAM(3,19)=ONE/THREE
      GAM(2,20)=ONE
      GAM(2,25)=ONE
      GAM(2,30)=-ONE/TWO
      GAM(2,31)=F1(2,3,2)
      GAM(2,34)=-GAM(2,31)
      GAM(2,35)=-ONE/TWO
      GAM(3,20)=-ONE/FOUR
      GAM(3,21)=-SQRT15/TWELVE
      GAM(3,22)= SQRT30/SIX
      GAM(3,24)= SQRT15/FOUR
      GAM(3,25)=-ONE/TWELVE
      GAM(3,26)= SQRT2/SIX
      GAM(3,29)=-SQRT2/THREE
      GAM(3,30)=-ONE/SIX
      GAM(3,31)= SQRT3/TWO
      GAM(3,33)=-SQRT6/THREE
      GAM(3,34)=-SQRT3/SIX
      GAM(3,35)=-ONE/TWO
      GAM(2,36)=ONE
      GAM(2,42)=-ONE/TWO
      GAM(2,43)= SQRT3/TWO
      GAM(2,47)=-SQRT3/TWO
      GAM(2,48)=-ONE/TWO
      GAM(2,54)=-ONE/TWO
      GAM(2,55)=GAM(2,43)
      GAM(2,60)=-ONE/TWO
      GAM(3,36)=-ONE/THREE
      GAM(3,37)= SQRT2*TWO/THREE
      GAM(3,41)= SQRT2/SIX
      GAM(3,42)= ONE/TWELVE
      GAM(3,43)=-SQRT3/FOUR
      GAM(3,44)=-SQRT3/FOUR
      GAM(3,45)=-THREE/FOUR
      GAM(3,46)= SQRT6/SIX
      GAM(3,47)= SQRT3/TWELVE
      GAM(3,48)= ONE/FOUR
      GAM(3,49)=-THREE/FOUR
      GAM(3,50)= SQRT3/FOUR
      GAM(3,51)=-SQRT6/SIX
      GAM(3,52)=-SQRT3/TWELVE
      GAM(3,53)= THREE/FOUR
      GAM(3,54)=-ONE/FOUR
      GAM(3,55)=-SQRT3/FOUR
      GAM(3,56)=-SQRT2/TWO
      GAM(3,57)=-ONE/FOUR
      GAM(3,58)=-SQRT3/FOUR
      GAM(3,59)=-SQRT3/FOUR
      GAM(3,60)= ONE/FOUR
C
      KMIN=2
  100 IN=2
      NTOLD=NT
  110 DO 115 I=1,9
  115 U(I)=D(IN,I)
      KN=KMIN
  120 DO 130 I=1,3
      DO 130 K=1,3
      SUM=ZERO
      DO 125 J=1,3
  125 SUM=SUM+F1(KN,I,J)*T(J,K)
  130 V(I,K)=SUM
      J1=1
  135 DO 140 K=1,9
      Q= ABS(D(J1,K)-W(K))
      IF(Q.GT.TOL) GO TO 145
  140 CONTINUE
      GO TO 175
  145 IF(J1.EQ.NT) GO TO 150
      J1=J1+1
      GO TO 135
  150 NT=NT+1
      DO 155 I=1,9
  155 D(NT,I)=W(I)
      DO 170 IRP=3,5
      IFIRST=IBIAS(IRP)+1
      ISTEP =IDIM(IRP)
      ILAST =IBIAS(IRP+1)-ISTEP+1
      IEND  =IFIRST+ISTEP-1
      KX=IBIAS(IRP)
      DO 170 I=IFIRST,ILAST,ISTEP
      DO 170 J=IFIRST,IEND
      SUM=ZERO
      IX=I
      JX=J
      DO 165 L=1,ISTEP
      SUM=SUM+GAM(KN,IX)*GAM(IN,JX)
      IX=IX+1
  165 JX=JX+ISTEP
      KX=KX+1
  170 GAM(NT,KX)=SUM
  175 KN=KN+1
      IF(KN.LE.NT) GO TO 120
      IN=IN+1
      IF(IN.LE.3) GO TO 110
      IF(NT.EQ.NTOLD) GO TO 180
      KMIN=NTOLD+1
      GO TO 100
  180 CONTINUE
      DO 200 I=2,60
      IS=2
      DO 190 J=1,3
      DO 190 K=1,3
      GAM(I,IS)=F1(I,J,K)
  190 IS=IS+1
      DO 200 L=2,60
  200 GAM(I,L+60)=GAM(I,L)
      DO 210 I=1,60
      DO 210 L=1,60
      GAM(I+60,L   )= GAM(I,L)
  210 GAM(I+60,L+60)=-GAM(I,L)
      NT=12*NIRRED
      RETURN
      END
      SUBROUTINE MATRIX(IRP,H)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION H(5,5,120)
      external product
C
      ND=5
      DO 1 I=1,ND
         DO 1 J=1,ND
            H(I,J,1)=0.0D+00
            H(I,J,2)=0.0D+00
    1 CONTINUE
C
      DO 2 I=1,ND
         H(I,I,1)=1.0D+00
    2 CONTINUE
C
      IF (IRP.EQ.3) THEN
         GO TO 100
      ENDIF
C
C    ---------- HG ----------
C     H2 MATRICES
C
      H(1,1,2)=-0.125D0
      H(1,2,2)=-0.649519052838328947D0
      H(1,4,2)=-0.75D0
      H(2,1,2)=-0.649519052838328989D0
      H(2,2,2)=0.625D0
      H(2,4,2)=-0.433012701892219298D0
      H(3,3,2)=-0.5D0
      H(3,5,2)=-0.866025403784438597D0
      H(4,1,2)=0.75D0
      H(4,2,2)=0.433012701892219298D0
      H(4,4,2)=-0.5D0
      H(5,3,2)=0.866025403784438597D0
      H(5,5,2)=-0.5D0
C
C     H3
C
      H(1,1,3)=0.892572059335907664D0
      H(1,2,3)=0.0482405392473671986D0
      H(1,3,3)=-0.0389842427770390187D0
      H(1,4,3)=0.148872875703131569D0
      H(1,5,3)=-0.421076079777705364D0
      H(2,1,3)=-0.0620235504609008533D0
      H(2,2,3)=-0.749926089457457715D0
      H(2,3,3)=0.606031797993231716D0
      H(2,4,3)=0.0859517948622367106D0
      H(2,5,3)=-0.243108388008970527D0
      H(3,1,3)=0.D0
      H(3,2,3)=-0.605613696826568515D0
      H(3,3,3)=-0.749408715659660926D0
      H(3,4,3)=-0.252311319355706928D0
      H(3,5,3)=-0.0892055224432724697D0
      H(4,1,3)=-0.446618627109394636D0
      H(4,2,3)=0.200554188011885640D0
      H(4,3,3)=-0.162072258672647018D0
      H(4,4,3)=0.285587581927070178D0
      H(4,5,3)=-0.807763663213200145D0
      H(5,1,3)=0.D0
      H(5,2,3)=-0.168207546237137934D0
      H(5,3,3)=-0.208146219034302457D0
      H(5,4,3)=0.908420545239852814D0
      H(5,5,3)=0.321175163854140369D0
      GO TO 300
C
C     ----------- T1U -----------
C     H2  MATRIX
C
  100 CONTINUE
      H(1,1,2)=1.
      H(2,2,2)=-0.5
      H(2,3,2)=(-1)*SQRT(3.0D+00)/(2.0D+00)
      H(3,2,2)=SQRT(3.0D+00)/(2.0D+00)
      H(3,3,2)=-0.5
C
C     H3  MATRIX
C
      H(1,1,3)=0.333333
      H(1,2,3)=0.942809
      H(1,3,3)=0.000000
      H(2,1,3)=-0.908421
      H(2,2,3)=0.321175
      H(2,3,3)=-0.267617
      H(3,1,3)=-0.252311
      H(3,2,3)=0.089206
      H(3,3,3)=0.963525
      GO TO 300
C
C ------ GENERATING HG OR HU MATRICES BY H2 & H3, OR C3 & C5 ------
C
  300 CONTINUE
      DO 3 I=1,ND
         DO 3 J=1,ND
            H(I,J,61)=-H(I,J,1)
            H(I,J,62)=-H(I,J,2)
            H(I,J,63)=-H(I,J,3)
    3 CONTINUE
C
C     H4=H2*H2, H4=-H64
C
      CALL PRODUCT(5,H(1,1,2),H(1,1,2),H(1,1,4),H(1,1,64))
C
C     H5=H3*H2,H5=-H65
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,2),H(1,1,5),H(1,1,65))
C
C     H6=H3*H4, H6=-H66
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,4),H(1,1,6),H(1,1,66))
C
C     H7=H2*H3, H7=-H67
C
      CALL PRODUCT(5,H(1,1,2),H(1,1,3),H(1,1,7),H(1,1,67))
C
C     H8=H3*H3, H8=-H68
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,3),H(1,1,8),H(1,1,68))
C
C     H9=H4*H3, H9=-H69
C
      CALL PRODUCT(5,H(1,1,4),H(1,1,3),H(1,1,9),H(1,1,69))
C
C     H10=H3*H7, H10=-H70
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,7),H(1,1,10),H(1,1,70))
C
C     H11=H3*H9, H11=-H71
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,9),H(1,1,11),H(1,1,71))
C
C     H12=H2*H8, H12=-H72
C
      CALL PRODUCT(5,H(1,1,2),H(1,1,8),H(1,1,12),H(1,1,72))
C
C     H13=H8*H3, H13=-H73
C
      CALL PRODUCT(5,H(1,1,8),H(1,1,3),H(1,1,13),H(1,1,73))
C
C     H14=H9*H3, H14=-H74
C
      CALL PRODUCT(5,H(1,1,9),H(1,1,3),H(1,1,14),H(1,1,74))
C
C     H15=H5*H8, H15=-H75
C
      CALL PRODUCT(5,H(1,1,5),H(1,1,8),H(1,1,15),H(1,1,75))
C
C     H16=H3*H14, H16=-H76
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,14),H(1,1,16),H(1,1,76))
C
C     H17=H7*H8, H17=-H77
C
      CALL PRODUCT(5,H(1,1,7),H(1,1,8),H(1,1,17),H(1,1,77))
C
C     H18=H8*H8, H18=-H78
C
      CALL PRODUCT(5,H(1,1,8),H(1,1,8),H(1,1,18),H(1,1,78))
C
C     H19=H8*H2 H19=-H79
C
      CALL PRODUCT(5,H(1,1,8),H(1,1,2),H(1,1,19),H(1,1,79))
C
C     H20=H3*H17 H20=-H80
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,17),H(1,1,20),H(1,1,80))
C
C     H21=H7*H6 H21=-H81
C
      CALL PRODUCT(5,H(1,1,7),H(1,1,6),H(1,1,21),H(1,1,81))
C
C     H22=H6*H6 H22=-H82
C
      CALL PRODUCT(5,H(1,1,6),H(1,1,6),H(1,1,22),H(1,1,82))
C
C     H23=H8*H7 H23=-H83
C
      CALL PRODUCT(5,H(1,1,8),H(1,1,7),H(1,1,23),H(1,1,83))
C
C     H24=H11*H10 H24=-H84
C
      CALL PRODUCT(5,H(1,1,11),H(1,1,10),H(1,1,24),H(1,1,84))
C
C     H25=H21*H3 H25=-H85
C
      CALL PRODUCT(5,H(1,1,21),H(1,1,3),H(1,1,25),H(1,1,85))
C
C     H26=H2*H5 H26=-H86
C
      CALL PRODUCT(5,H(1,1,2),H(1,1,5),H(1,1,26),H(1,1,86))
C
C     H27=H9*H2 H27=-H87
C
      CALL PRODUCT(5,H(1,1,9),H(1,1,2),H(1,1,27),H(1,1,87))
C
C     H28=H5*H5 H28=-H88
C
      CALL PRODUCT(5,H(1,1,5),H(1,1,5),H(1,1,28),H(1,1,88))
C
C     H29=H6*H5 H29=-H89
C
      CALL PRODUCT(5,H(1,1,6),H(1,1,5),H(1,1,29),H(1,1,89))
C
C     H30=H17*H2 H30=-H90
C
      CALL PRODUCT(5,H(1,1,17),H(1,1,2),H(1,1,30),H(1,1,90))
C
C     H31=H9*H9 H31=-H91
C
      CALL PRODUCT(5,H(1,1,9),H(1,1,9),H(1,1,31),H(1,1,91))
C
C     H32=H3*H6 H32=-H92
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,6),H(1,1,32),H(1,1,92))
C
C     H33=H3*H30 H33=-H93
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,30),H(1,1,33),H(1,1,93))
C
C     H34=H8*H26 H34=-H94
C
      CALL PRODUCT(5,H(1,1,8),H(1,1,26),H(1,1,34),H(1,1,94))
C
C     H35=H8*H9 H35=-H95
C
      CALL PRODUCT(5,H(1,1,8),H(1,1,9),H(1,1,35),H(1,1,95))
C
C     H36=H3*H34 H36=-H96
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,34),H(1,1,36),H(1,1,96))
C
C     H37=H2*H6 H37=-H97
C
      CALL PRODUCT(5,H(1,1,2),H(1,1,6),H(1,1,37),H(1,1,97))
C
C     H38=H14*H15 H38=-H98
C
      CALL PRODUCT(5,H(1,1,14),H(1,1,15),H(1,1,38),H(1,1,98))
C
C     H39=H5*H6 H39=-H99
C
      CALL PRODUCT(5,H(1,1,5),H(1,1,6),H(1,1,39),H(1,1,99))
C
C     H40=H26*H26 H40=-H100
C
      CALL PRODUCT(5,H(1,1,26),H(1,1,26),H(1,1,40),H(1,1,100))
C
C     H41=H10*H12 H41=-H101
C
      CALL PRODUCT(5,H(1,1,10),H(1,1,12),H(1,1,41),H(1,1,101))
C
C     H42=H3*H40 H42=-H102
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,40),H(1,1,42),H(1,1,102))
C
C     H43=H7*H7 H43=-H103
C
      CALL PRODUCT(5,H(1,1,7),H(1,1,7),H(1,1,43),H(1,1,103))
C
C     H44=H27*H3 H44=-H104
C
      CALL PRODUCT(5,H(1,1,27),H(1,1,3),H(1,1,44),H(1,1,104))
C
C     H45=H3*H43 H45=-H105
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,43),H(1,1,45),H(1,1,105))
C
C     H46=H29*H3 H46=-H106
C
      CALL PRODUCT(5,H(1,1,29),H(1,1,3),H(1,1,46),H(1,1,106))
C
C     H48=H10*H11 H48=-H108
C
      CALL PRODUCT(5,H(1,1,10),H(1,1,11),H(1,1,48),H(1,1,108))
C
C     H49=H7*H26 H49=-H109
C
      CALL PRODUCT(5,H(1,1,7),H(1,1,26),H(1,1,49),H(1,1,109))
C
C     H50=H34*H3 H50=-H110
C
      CALL PRODUCT(5,H(1,1,34),H(1,1,3),H(1,1,50),H(1,1,110))
C
C     H51=H3*H50 H51=-H111
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,50),H(1,1,51),H(1,1,111))
C
C     H52=H40*H3 H52=-H112
C
      CALL PRODUCT(5,H(1,1,40),H(1,1,3),H(1,1,52),H(1,1,112))
C
C     H53=H3*H46 H53=-H113
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,46),H(1,1,53),H(1,1,113))
C
C     H47=H3*H53 H47=-H107
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,53),H(1,1,47),H(1,1,107))
C
C     H54=H2*H42 H54=-H114
C
      CALL PRODUCT(5,H(1,1,2),H(1,1,42),H(1,1,54),H(1,1,114))
C
C     H55=H3*H42 H55=-H115
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,42),H(1,1,55),H(1,1,115))
C
C     H56=H9*H26 H56=-H116
C
      CALL PRODUCT(5,H(1,1,9),H(1,1,26),H(1,1,56),H(1,1,116))
C
C     H57=H3*H56 H57=-H117
C
      CALL PRODUCT(5,H(1,1,3),H(1,1,56),H(1,1,57),H(1,1,117))
C
C     H59=H26*H6 H59=-H119
C
      CALL PRODUCT(5,H(1,1,26),H(1,1,6),H(1,1,59),H(1,1,119))
C
C     H58=H59*H3 H58=-H118
C
      CALL PRODUCT(5,H(1,1,59),H(1,1,3),H(1,1,58),H(1,1,118))
C
C     H60=H58*H3 H60=-H120
C
      CALL PRODUCT(5,H(1,1,58),H(1,1,3),H(1,1,60),H(1,1,120))
C
C          ------- HG -------
C
      IF (IRP.EQ.5) THEN
         DO 10 IT=1,60
            DO 10 I=1,ND
               DO 10 J=1,ND
                  H(I,J,60+IT)=H(I,J,IT)
   10    CONTINUE
      ENDIF
C
      RETURN
      END
C
C--------------------------------------------------------------------
C
      SUBROUTINE PRODUCT(ND,A,B,C,D)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION A(ND,ND),B(ND,ND),C(ND,ND),D(ND,ND)
C
C     C=A*B,D=-C
C
      DO 4 I=1,ND
         DO 4 J=1,ND
            SUM=0.0D+00
            DO 3 K=1,ND
               SUM=SUM+A(I,K)*B(K,J)
    3       CONTINUE
            C(I,J)=SUM
            D(I,J)=-SUM
    4 CONTINUE
      RETURN
      END
