!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program trci2den

       implicit none
      integer   nfilmx
      parameter(nfilmx=55)
c
      integer   ninfmx
      parameter(ninfmx=10)
c
      integer   maxsym
      parameter(maxsym=10)
c
      INTEGER  nmotx,nrowmx
      PARAMETER (nmotx = 510,nrowmx=1023)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c   SIF related
      integer nbfd2,lend1e, n1edbf
      parameter(nbfd2=4096,lend1e=2047,n1edbf=1636)
c
      integer d1zero,d2zero
      integer ind(4),info(ninfmx),iitype,ibvtyp,ifmt1,ierr,iout,ios,
     &        ietype,i,idummy,itypea,itypeb,irm1(nmotx),irm2(nmotx)
      integer fildrt
      integer l1rcal,l1rc,l2rc,lenbuf,lrow,labbuf(2*n1edbf)
      integer map(nmotx)
      integer nmot,n1mx,n2mx,ntitld,niot,nfct,nfvt,nrow,nsym,nvwalk,
     &        ncsfv,nwalk,nenrgy,nmap,nbfnsm(maxsym),nmpsy(maxsym),
     &        nbft
      integer ssym
      integer xbar(4)
c
      real*8 energy
      real*8 val(lend1e),d2
c
      logical lastcall
c
      real*8 buf(lend1e),valbuf(lend1e),d1(nmotx*(nmotx+1)/2)
c      default lenbuf size for drtfl is 1600 (integer*8!)!
      integer ibuf(1600)
c
      character*80 title
      character*8 bfnlab(nmotx)
      character*4 labels(maxsym)
c
c-----------------------------------------------------------------------
c
c  ##  read the DRT
c
      fildrt=9
      open( unit = fildrt, file = 'cidrtfl.1', status = 'old' ,
     +       form = 'formatted' ,iostat=ios)
      if (ios.ne.0)
     . call bummer ('can not open unit ',fildrt,2)
c
c     call rdhdrtn(
c    & title,          nmot,       niot,   nfct,
c    & nfvt,           nrow,       nsym,   ssym,
c    & xbar,           nvwalk,     ncsfv,  lenbuf,
c    & nwalk,          mxinor,     nmotx,  nrowmx,
c    & buf,            6,  fildrt )
c
ctm  nbft is unknown at this point!
      call rddrt(
     & 6,     fildrt,   title,   nsym,
     & map,      ibuf,    nmpsy,
     & niot,  nmot,    ssym )
c
      d1zero =10
      open( unit = d1zero,file ='dzero', status = 'unknown' ,
     & form = 'unformatted' )
c
      d2zero=11
      open( unit = d2zero,file ='dzero2', status = 'unknown' ,
     & form = 'unformatted' )
c
      ibvtyp = 0
      call izero_wr(ninfmx,info,1)
c info(1) is the fsplit parameter
      info(1) = 2
c  info(2) and info(3) are the record length parameters for the cid1fl
      l1rcal = nbfd2
      iitype = 1
      call sifcfg(iitype,l1rcal,nmot,ibvtyp,ifmt1,l1rc,n1mx,ierr)
      if (ierr.ne.0) call bummer(' densi: from sifcfg:1, ierr=',ierr,
     +                           2)
      info(2) = nbfd2
      info(3) = n1mx
c info(4) and info(5) are the record length parameters for cid2fl
      l1rcal = nbfd2
      iitype = 2
      call sifcfg(iitype,l1rcal,nmot,ibvtyp,ifmt1,l2rc,n2mx,ierr)
      if (ierr.ne.0) call bummer(' densi: from sifcfg:1, ierr=',ierr,
     +                           2)
      info(4) = nbfd2
      info(5) = n2mx
      info(6) = ifmt1 
c
      ntitld=1
      title="symmetric and antisymmetric 1-e density matrix"
c
      nenrgy=1
      nmap=0
      ietype=-1
      energy=10.0
      do i = 1,nmot
       write (bfnlab(i),fmt='(a5,i3.3)') 'tout:',i
       map(i) = i
      enddo
c
      call sifwh(
     & d1zero,     ntitld,    nsym,     nmot,
     & ninfmx,     nenrgy,    nmap,     title,
     & nmpsy,      labels,    info,     bfnlab,
     & ietype,     energy,    idummy,   idummy,
     & ierr)
      if (ierr.gt.0) call bummer("error in sifwh for d1zero",
     & d1zero,faterr)
c
      call wzero(nmotx*(nmotx+1)/2,d1,1)
      irm1(1) = 1
      irm2(1) = nmpsy(1)
      do i = 2,nsym
         irm1(i) = irm1(i-1) + nmpsy(i-1)
         irm2(i) = irm2(i-1) + nmpsy(i)
      enddo
c
      itypea=0
      itypeb=7
      call wtden1(
     & d1zero,        info,          lend1e,        n1edbf,
     & val,           labbuf,        valbuf,        d1,
     & nsym,          irm1,          irm2,          map,
     & itypea,        itypeb,        2)
c
      ind(1)=1
      ind(2)=1
      ind(3)=1
      ind(4)=1
      d2=0.0d+00
      lastcall=.true.
      iout=1
      call d2tr_out(
     & d2,    ind,   labbuf,  valbuf,
     & info,  iout,  lastcall, d2zero)
c
      call bummer('normal termination',0,3)
      stop 'end of dzero'
      end
c
c***********************************************************************
c
      subroutine d2tr_out(
     & d2tr,  ind,   labbuf,  valbuf,
     & info,  iout,  lastcall, file)
c
       implicit none
c  ##  paramter section
c
      integer nrec
      save  nrec
      data  nrec /0/
c
      integer   ninfmx
      parameter(ninfmx=10)
c
c     # sif parameters
      integer   msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
      integer itypea2, itypeb2, nipv2
      parameter(itypea2 = 3, itypeb2 = 1, nipv2 = 4)
c
c     # sifs buffer length
      integer nsifbf
      parameter(nsifbf=4096)
c
      integer ifmt, ibvtyp, ibitv(1)
      data ifmt, ibvtyp, ibitv /0, 0, 0/
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  ##  integer section
c
      integer ind(4),iout,info(ninfmx),i,ierr
      integer j
      integer file
      integer labbuf(4,*),last
      integer outmx
      integer reqnum
c
c  ##  real*8 section
c
      real*8 buffer(nsifbf)
      real*8 d2tr
      real*8 valbuf(*)
c
c  ##  logical section
c
      logical lastcall
c
      save reqnum
c-----------------------------------------------------------
c
      iout=1
        do i=1,4
        labbuf(i,iout)=ind(i)
        enddo
      valbuf(iout)=d2tr
       last=nomore
c
        call sif2w8( file, info, reqnum, ierr )
         if ( ierr .ne. 0 )
     &    call bummer('3:d2tr_out: sif2w8, ierr=', ierr, faterr )
c
        call sifew2(
     &   file,   info,     nipv2,   iout,
     &   last,      itypea2,  itypeb2, 
     &   ibvtyp,    valbuf,   labbuf,  ibitv,
     &   buffer,    0,         nrec ,  reqnum,
     &   ierr   )
        if (ierr.ne.0)
     &   call bummer('4:d2tr_out: sifew2 ierr=', ierr, faterr)
c
c
      return
      end

c
c***********************************************************************
c

      subroutine rdhdrtn(
     & title,  nmot,   niot,   nfct,
     & nfvt,   nrow,   nsym,   ssym,
     & xbar,   nvalw,  ncsft,  lenbuf,
     & nwalk,  niomx,  nmomx,  nrowmx,
     & buf,    nlist,  ndrt )
c
c  read the header info from the drt file.
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine rdhdrtn
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*80        TITLE
C
      INTEGER             BUF(*),      LENBUF,      NCSFT,       NDRT
      INTEGER             NFCT,        NFVT,        NIOMX,       NIOT
      INTEGER             NLIST,       NMOMX,       NMOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVALW(4),    NWALK
      INTEGER             SSYM,        XBAR(4)
C
C     Local variables
C
      INTEGER             IERR,        NHDINT,      NVALWT,      VRSION
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif

C
C    END OF VARIABLE DECLARATIONS FOR subroutine rdhdrtn


c     # bummer error types.
c     # title card...
      rewind ndrt
      ierr=0
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
*@ifdef spinorbit
      read(ndrt,'(2L4,3I4)',iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if(spnorb) call bummer('rdhead: no SO-CI calcs',0,faterr)
*@endif


c     write(nlist,6010)title
c
c     # number of integer drt parameters, buffer length, and version.
c
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 4 ) then
         write(nlist,*)
     &    '*** warning, rddrt: possible drt inconsistency. vrsion=',
     &    vrsion,' .ne. 4 ***'
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning, rddrt: possible drt inconsistency. vrsion=',
*     &    vrsion,' .ne. 3 ***'
*      endif
*@endif
c
c     # header info...
c
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      nmot     = buf(1)
      niot     = buf(2)
      nfct     = buf(3)
      nfvt     = buf(4)
      nrow     = buf(5)
      nsym     = buf(6)
      ssym     = buf(7)
      xbar(1)  = buf(8)
      xbar(2)  = buf(9)
      xbar(3)  = buf(10)
      xbar(4)  = buf(11)
      nvalw(1) = buf(12)
      nvalw(2) = buf(13)
      nvalw(3) = buf(14)
      nvalw(4) = buf(15)
      ncsft    = buf(16)

c
      nwalk  = xbar(1)  + xbar(2)  + xbar(3)  + xbar(4)
      nvalwt = nvalw(1) + nvalw(2) + nvalw(3) + nvalw(4)
c
      write(nlist,6020)nmot,niot,nfct,nfvt,nrow,nsym,ssym,lenbuf
      write(nlist,6100)'nwalk,xbar',nwalk,xbar
      write(nlist,6100)'nvalwt,nvalw',nvalwt,nvalw
      write(nlist,6100)'ncsft',ncsft
c
      if(niot.gt.niomx)then
         write(nlist,6900)'niot,niomx',niot,niomx
         call bummer('rdhead: niomx=',niomx,faterr)
      endif
      if(nmot.gt.nmomx)then
         write(nlist,6900)'nmot,nmomx',nmot,nmomx
         call bummer('rdhead: nmomx=',nmomx,faterr)
      endif
      if(nrow.gt.nrowmx)then
         write(nlist,6900)'nrow,nrowmx',nrow,nrowmx
         call bummer('rdhead: nrowmx=',nrowmx,faterr)
      endif
ctm     if(nwalk.gt.nwlkmx)then
ctm         write(nlist,6900)'nwalk,nwlkmx',nwalk,nwlkmx
ctm         call bummer('rdhead: nwlkmx=',nwlkmx,faterr)
ctm      endif
c
      return
c
6010  format(/' drt header information:'/1x,a)
6020  format(
     & ' nmot  =',i6,' niot  =',i6,' nfct  =',i6,' nfvt  =',i6/
     & ' nrow  =',i6,' nsym  =',i6,' ssym  =',i6,' lenbuf=',i6)
6100  format(1x,a,':',t15,5i9)
6900  format(/' error: ',a,3i10)
      end

c
c***********************************************************************
c
c deck wtden1
      subroutine wtden1(
     & ntape,  info,   lbuf,   nipbuf,
     & buf,    ilab,   val,    d1,
     & nsym,   irx1,   irx2,   map,
     & itypea, itypeb, endmark)
c
c  write the 1-e d1(*) matrix elements to the sifs file
c
c  input:
c  ntape = output unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  d1(*) = 1-e density matrix
c  nsym,irx1(*),irx2(*) = symmetry range info.
c  map(*) = orbital index mapping vector.
c
c  written by gary kedziora
c
      implicit integer(a-z)
c
      integer info(10)
      integer    ifmt,     ibvtyp
      parameter( ifmt = 0, ibvtyp = 0)
      real*8 fcore
c
      integer   nipv
      parameter(nipv=2)
c
c     # sifs continuaton flag values
      integer   msame,   nmsame,  nomore
      parameter(msame=0, nmsame=1,nomore=2)
c
      real*8 buf(lbuf),d1(*),val(nipbuf)
      integer ilab(nipv,nipbuf),irx1(nsym),irx2(nsym),map(*)
c
      real*8    small
      parameter(small=1d-15)
c
c     # bummer error types
      integer wrnerr, nfterr, faterr
      parameter(wrnerr=0, nfterr=1, faterr=2)
c
      integer endmark
c
c specification of the various ietypes
      numtot = 0
c
c fcore contribution gets computed in tran for now.
c
      fcore   = 0.0
c
c  write 1-e density matrix elements and labels to sifs file
c
      last = msame
      pq=0
      iknt=0
      do 130 isym=1,nsym
         do 120 p=irx1(isym),irx2(isym)
            do 110 q=irx1(isym),p
               pq=pq+1
               if(iknt.eq.nipbuf)then
c                 # if the rare event happens that this is the last time
c                 # through the nested loops and iknt.eq.nipbuf then
c                 # set last=nomore
                  if ((isym .eq. nsym) .and. (p .eq. irx2(nsym)) .and.
     +                (q .eq. p)) then
                     last = endmark
                  else
                     last = msame
                  endif
                  numtot = numtot + iknt
                  call sifew1(
     &              ntape,   info,    nipv,    iknt,
     &              last,    itypea,  itypeb,  
     &              ibvtyp,  val,     ilab,    fcore,
     &              ibitv,   buf,     nrec,    ierr)
                  if ( ierr .ne. 0 )
     &               call bummer('wtden1:  sifew1, ierr=', ierr, faterr)
                  numtot = numtot - iknt
                  iknt = 0
               endif
               if (last .eq. endmark) return
c              if (abs(d1(pq)).gt.small) then
                iknt=iknt+1
                val(iknt)=d1(pq)
                ilab(1,iknt)=map(p)
                ilab(2,iknt)=map(q)
c              endif
110         continue
120      continue
130   continue
c
c dump the rest of the d1 matrix elements
c
      last = endmark
      numtot = iknt + numtot
      call sifew1(
     & ntape,  info,   nipv,     iknt,
     & last,   itypea, itypeb,   
     & ibvtyp, val,    ilab,     fcore,
     & ibitv,  buf,    nrec,     ierr)
      return
c
      end
c
c********************************************************************+
c
c deck rddrt
      subroutine rddrt(
     & nlist,  ndrt,  title,  nsym,
     & map,   ibuf,   nmpsy,
     & niot,   nmot,   ssym )
c
c  read some of the ci drt file info for the integral transformation..
c
c  input:
c  nlist = list file unit number.
c  ndrt = drt unit number.
c  nsym = number of symmetry blocks (checked for consistency).
c  ibuf(*) = scratch buffer space of at least size = max(nsym,lidrt).
c
c  output:
c  title = ci drt title.
c  map(*) = mo-to-drt_level mapping vector. frozen core orbitals are
c           indicated with -1 entries and frozen virtual orbitals
c           are indicated with 0 entries.  other entries are >0.
c
       implicit none
      integer nlist, ndrt, nsym, nmpsy(*)
      character*80 title
      integer map(*), ibuf(*)
c
      integer ioerr, lidrt, lenbuf, vrsion, nsymd,
     & nfct, nfvt, nfctx, nfvtx, ibf, i, nmot, ssym,nhdint,nrow,ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer niot
c
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif
      rewind ndrt
      ierr=0
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
      write(nlist,*)title
*@ifdef spinorbit
      read(ndrt,'(2L4,3I4)',iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if(spnorb) call bummer('rdhead: no SO-CI calcs',0,faterr)
*@endif
c
c     # number of integer drt parameters, buffer length, and version.
c
      call rddbl( ndrt, 3, ibuf, 3 )
c
      vrsion = ibuf(1)
      lenbuf = ibuf(2)
      nhdint = ibuf(3)
c
c
*@ifdef spinorbit
      if ( vrsion .ne. 4 ) then
         write(nlist,*)
     &    '*** warning, rddrt: possible drt inconsistency. vrsion=',
     &    vrsion,' .ne. 4 ***'
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning, rddrt: possible drt inconsistency. vrsion=',
*     &    vrsion,' .ne. 3 ***'
*      endif
*@endif
c
c     # header info...
c
      call rddbl( ndrt, lenbuf, ibuf, nhdint )
c
      nmot     = ibuf(1)
      niot     = ibuf(2)
      nrow     = ibuf(5)
      nsym     = ibuf(6)
      ssym     = ibuf(7)

c
c     # skip over the symmetry labels.
c
      call rddbl( ndrt, lenbuf, ibuf, nsym )
c
c     # read the orbital-to-level mapping vector, map(*).
c
      call rddbl( ndrt, lenbuf, map, nmot )
c
c     # check that map(*) is consistent with nfct and nfvt.
c
      nfctx = 0
      nfct = 0
      nfvtx = 0
      nfvt = 0
      do 50 ibf = 1,nmot 
         if ( map(ibf) .eq. -1 ) then
            nfctx=nfctx+1
         elseif ( map(ibf) .eq. 0 ) then
            nfvtx=nfvtx+1
         endif
50    continue
c
c     # write out the drt info.
c
      write(nlist,6030)title
      write(nlist,6040) nmot, nfct, nfvt
      write(nlist,6050)'mo-to-level map(*)',(map(i),i=1,nmot)
      write(nlist,*)
c
c     # mu(*)...
c
      call rddbl( ndrt, lenbuf, ibuf, -niot)
c
c     # nmpsy(*)...
c
      call rddbl( ndrt, lenbuf, nmpsy, nsym )
      return
c
6010  format(' *** rddrt error *** ',a,3i8)
6020  format(/' program stopping due to inconsistencies on the',
     + ' drt file.')
6030  format(/' input ci drt file information:'/(1x,a))
6040  format(' nmot =',i4,' nfct  =',i4,' nfvt  =',i4)
6050  format(1x,a/(1x,20i4))
      end
c
c***********************************************************************
c
