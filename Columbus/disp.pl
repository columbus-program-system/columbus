#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#**************************************************************************
# colinp emulation
#**************************************************************************
use Config;
use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;


use Shell qw( date pwd );

$scriptversion="Version 0.5, III/13/2000 md";
$debug = 0;
if ( $debug ) { open XX, ">XX";}
# Perl5+Curses ONLY!
# Comment these lines for use with Perl4/curseperl
BEGIN { $Curses::OldCurses = 1; }
use Curses;                     # PerlMenu needs "Curses"
use perlmenu;                   # Main menu package (Perl5 only)
require "menuutil.pl";        # For "pause" and "print_nl" routines.
use colib_perl;

$NOHUP=" "; 


# Perl4/curseperl ONLY!
# Uncomment these lines for use with Perl4/curseperl
# (Did you remember to run "create_menu.pl"?)
#require "./menu.pl";           # Main menu package (Perl4 only)
#require "./menuutil.pl";       # For "pause" and "print_nl" routines.    
#
# Required global variables are $window, $row, and $col.
# These variables are used by the menuutil.pl routines.
#
   $window=initscr();
   &menu_curses_application($window);
# Default prefs active at start
  $numbered_flag = 1;       # Numbered menus
  $num_pref = "numbered";
  $gopher_pref = "default"; # Non-gopherlike arrows/scrolling
  $gopher_flag = 0;
  $mult_pref = "single";    # Single column menus
  $mult_flag = 0;
  $arrow_pref = "arrow";    # Arrow selection indicator menus
  $arrow_flag = 0;     


  $menu_default_top = 0;    # Storage for mainline top item number.
  $menu_default_row = 0;    # Storage for mainline arrow location.
  $menu_default_col = 0;    # Storage for mainline arrow location.
  $row = $col = 0;      # Storage for row/col for menuutil.pl
  $title_cnt = 0;       # To trigger different subtitles/bottom titles       
#
     &readdisplfl;
#
     while (1) {  # always true

     &menu_init($numbered_flag, "COLUMBUS INPUT FACILITY ",0,
                ,"\n-menu 1: COLUMBUS calculations based on the displfl file ",
                $scriptversion,"helpmenu4");
     &menu_item ("copy input files for force constant calculation","input1");
     &menu_item ("copy input files for potential curve calculation","input2");
     &menu_item ("perform force constant calculations (no CI restart)", "freqcalc");
     &menu_item ("perform force constant calculations (using CI restart)", "freqcalc_rest");
     &menu_item ("perform potential curve calculations", "potcur");
     &menu_item ("quit", "");

     $sel = &menu_display("");
    if ( $sel eq "input1" ) { # copy input files for force constant calculation
       &endwin(); 
       $it_flag=1;
       &input();
       }
    elsif( $sel eq "input2" ){ # copy input files for potential curve calculation
       &endwin(); 
       $it_flag=0;
       &input();
       }
    elsif($sel eq "freqcalc"){ # frequency and dip.mom. derivative calculation
       &inpcoremem();
       $cbus = $ENV{"COLUMBUS"}; 
       print " Requested calculations started with parameters:\n";
       print " COLUMBUS dir: $cbus\n";
       print " core memory (MB): $coremem\n";
       print "  executing:\n";
       print "$NOHUP $cbus/calc.pl -m $coremem  -nproc $nproc 2>&1 runls &\n";
       system("$NOHUP $cbus/calc.pl -m $coremem -nproc $nproc 2>&1 runls &");
       last;}
    elsif($sel eq "freqcalc_rest"){ # frequency and dip.mom. derivative calculation
       &inpcoremem();
       $cbus = $ENV{"COLUMBUS"}; 
       print " Requested calculations started with parameters:\n";
       print " COLUMBUS dir: $cbus\n";
       print " core memory (MB): $coremem\n";
       print "  executing:\n";
       print "$NOHUP $cbus/calc.pl -cirestart -m $coremem -nproc $nproc 2>&1 runls &\n";
       system("$NOHUP $cbus/calc.pl -cirestart -m $coremem -nproc $nproc 2>&1 runls &");
       last;}
   elsif($sel eq "potcur"){ # potential curve calculation
       &inpcoremem();
       $cbus = $ENV{"COLUMBUS"};
       print " Requested calculations started with parameters:\n";
       print " COLUMBUS dir: $cbus\n";
       print " core memory (MB): $coremem\n";
       print "  executing:\n";
       print "$NOHUP $cbus/calc.pl -restart -m $coremem -nproc $nproc 2>&1 runls &\n";
       system("$NOHUP $cbus/calc.pl -restart -m $coremem -nproc $nproc 2>&1 runls &");
       last;}          
    else { 
       last;}
    }
  &endwin(); 
#
################################################################################
  sub readdisplfl
   {

  local $line,$i;
    open(DISPLFL,"DISPLACEMENT/displfl") or  die "DISPLACEMENT/displfl do not exists!!!\n\n";
    $line=<DISPLFL>; $line=<DISPLFL>; #  skip the first two lines
    $refcalc=<DISPLFL>;
    chomp ($refcalc);
    $refcalc=~s/\W.*//;  
    $i=1;
       while(<DISPLFL>)
        {
         ($coord[$i],$displ[$i],$flag[$i])=split(/\s+/),$_,3;        
         $i++;
        }
    $npoint=$i-1;
#
         print "refcalc:$refcalc\n";
   }
   
#
################################################################################
#
  sub input{
  my %inpfiles,@field;
   %inpfiles = (    argos   => "intprogram:argosin:control.run:infofl" ,
                    hermit  => "daltcomm:daltaoin:control.run:infofl",
                    scf     => "scfin" ,
                    mcscf   => "mcscfin:mcdrtin*",
                    ciudg   => "cidrtin:tranin:cisrtin:ciudgin",
                    sciudg  => "cidrtin:tranin:cisrtin:ciudgin",
                    pciudg  => "cidrtin:tranin:cisrtin:ciudgin",
                    scfprop => "propin" ,
                    mcscfprop =>  "propin" ,
                    mcscfgrad => "tranmcdenin:abacusin:gdiisin:intcfl",
                    samcgrad => "tranmcdenin:abacusin:gdiisin:intcfl:cigrdin:transmomin",
                    cigrad    => "trancidenin:abacusin:gdiisin:intcfl:cidenin:cigrdin",
                    nadcoupl => "trancidenin:abacusin*:intcfl:cidenin:cigrdin:transmomin:polyhesin",
                    polyhes => "polyhesin",
                    gdiis   => "gdiisin:intcfl",
                    ciprop  => "propin" ,
                    mcscfmom => "transmomin" ,
                    ciudgav => "cidrtmsin:tranin:cisrtin:ciudgin.drt*",
                    ciudgmom => "cidrtmsin:transmomin",
                    turbocol => "control.basis:control.run:infofl"  ); 
#
   if ($it_flag == 1)
  {
#  save the control file
   rename "control.run","control.run.save"; 
   &changekeyword("control.run.save","control.run","niter",1);

 }


  open(CONTROL,"control.run") or die "contron.run do not exists!!!\n\n";
  $line=<CONTROL>;
     while (<CONTROL>){
       $w=$_;
       chop($w);
       @v = split /:/, $inpfiles{$w};
       #print "$w\n";
       #print "@v\n";
       foreach $x ( @v ) 
        {  
##       if (/^ciudg$/ && $it_flag == 1) 
#         {
#          &changekeyword("ciudgin","ciudgin_new","rtolci",1e-5);
#          rename "ciudgin_new","ciudgin";
#         }
#
#   copy input files for reference point calculation
#
         if ($refcalc eq "yes")
          {
           $dir="DISPLACEMENT/REFPOINT";
             if (! -e $dir )
              {
               rename "control.run.save","control.run";
               print "\n\n Directory: $dir\n";
               print "     make input first\n";
               die "input error!!!\n";
              } # if (! -e $dir ) 
           $error=system("cp $x ${dir}/.");
           if (!$error eq 0) {print "Error in file copy for file: $x, error code: $error"; die }
          } # end of: if ($refcalc eq "yes")
  

#   copy input files for displacement calculations
#  
         for ($i=1; $i<=$npoint; $i++)
          {
	    if (($flag[$i] ne "skip") && ($flag[$i] ne "fixc"))
	     {
	      $dir="DISPLACEMENT/CALC.c$coord[$i].d$displ[$i]";
	         if (! -e $dir )
                  {
                   rename "control.run.save","control.run";
                   print "\n\n Directory: $dir\n";
                   print "     make input first\n";
                   die "input error!!!\n";
                  } # if (! -e $dir )
              $error=system("cp $x ${dir}/.");
	      if (!$error eq 0) {print "Error in file copy for file: $x, error code: $error"; die }
	     } # end of: if ($flag[$i] ne "skip")
          } # end of: for ($i=1; $i<=$npoint; $i++)
         close(DISPLFL);
       } # end of: foreach $x ( @v )
     } # end of: while (<CONTROL>)
     
 close(CONTROL);
#
#   return the original control.run file
     rename "control.run.save","control.run";
#
#  if present copy mocoef file
    if ( -s "mocoef")
     {
#
         if ($refcalc eq "yes")
          {
           $dir="DISPLACEMENT/REFPOINT";
             if (! -e $dir )
              {
               rename "control.run.save","control.run";
               print "\n\n Directory: $dir\n";
               print "     make input first\n";
               die "input error!!!\n";
              } # if (! -e $dir )
           system("cp mocoef ${dir}/.");
          } # end of: if ($refcalc eq "yes")  
#
         for ($i=1; $i<=$npoint; $i++)
          {
	     if (($flag[$i] ne "skip") && ($flag[$i] ne "fixc"))
	      {
               $dir="DISPLACEMENT/CALC.c$coord[$i].d$displ[$i]";
                  if (! -e $dir )
                   {
                    rename "control.run.save","control.run";
                    print "\n\n Directory: $dir\n";
                    print "     make input first\n";
                    die "input error!!!\n";
                   } # if (! -e -e $dir )
               system("cp mocoef ${dir}/.");
	      } # end of: if ($flag[$i] ne "skip") 
          } # end of: for ($i=1; $i<=$npoint; $i++)
      close(DISPLFL);  
     } # end of: if ( -s mocoef)
  } # end of: sub input
# 
#
################################################################################
  sub inpcoremem{
#
   @input_data = ();       # Place to put data entered on screen
   @defaults = ();         # Default data
   @protect = ();          # Protected markers
   @required = ();         # Required field markers
   $bell = "\007";         # Ascii bell character
   $row = $col = 0;        # Storage for row/col used by menuutil.pl

#
  $window=initscr();
# Activate left and right markers, both in standout rendition.
#
   &menu_template_prefs("*"," ",1,"*"," ",1);

&menu_load_template_array(split("\n", <<'END_OF_TEMPLATE'));


   Input the necessary core memory for the calculations (MB):



       
       core memory/process  [_________]
       #processes           [_________]




   Indicate completion by pressing "Return".

END_OF_TEMPLATE

#
   $defaults[0]=1700;
   $defaults[1]=1;
   &menu_display_template(*input_data,*defaults);
   &endwin();  
#
   $input_data[0] =~ s/ //g;
   $input_data[1] =~ s/ //g;
   $coremem=$input_data[0];   
   $nproc=$input_data[1];   
#
   return;
  } # end of: sub inpcoremem
