/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _lbinom_h
#define _lbinom_h
#include "la.h"
#include "basic.h"

using namespace LA;
extern unsigned long lfact(const unsigned int n);
extern unsigned long lbinom(unsigned long n, unsigned long k);
extern int ibinom(int n, int k);
extern REAL ifact(int n);
const REAL pi=3.141592653589793;
inline REAL fsqr(REAL x) {return x*x;}
extern REAL dipow(REAL x, int i);
extern REAL smartpow(REAL x, REAL y);
extern REAL gammp(REAL x, REAL y);
extern REAL mygamma(REAL x);
extern REAL ifact2(int n);
#endif
