/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _abstring_h
#define _abstring_h

#include "lbinom.h"
#include "la.h"
#include "basic.h"
#include "gelfand.h"

using namespace LA;

typedef NRVec<orbindex> abstring;
typedef NRMat<orbindex> abstringset;

typedef int Uindex; //MUST NOT BE UNSIGNED
struct Ugenerators_reorganized
	{
	NRMat<Uindex *> begins;
	NRVec<Uindex> bigstore;
	};

extern NRMat<lexindex> abstring_fci_addr(unsigned int nelec, unsigned int nbas);
extern abstringset generate_abstr(unsigned int nelec, unsigned int nbas);
extern lexindex abstr_address(const orbindex *str, const NRMat<lexindex> &adr);
extern Ugenerators calcLambda1(const abstringset &basis, const NRMat<lexindex> &adr);
extern Ugenerators joinLambda1(const Ugenerators (&Lambda1)[2], const unsigned int (&nelec)[2], int ispinbeg=0, int ispinend=1);

//NOTE to Ugenerators_resorted: in multireference case - the excitation can become deescitation with respect to vacuum 0 i.e. indices i>j here and a transposed Ugenerators_resorted must be employed
//THIS here is a symmetric matrix in i,j, since in this form it cannot be transposed directly
//ALSO NOTE: in string basis, this representation is too general, since one excitation from a string pair yiels only one other string pair, not a linear combination
//NRVec<NRSMat_from1<SparseMat<REAL> > > Ugenerators_resorted has in the string basis at most one element in the SparseMat<REAL> and the representation could be simplified
//but the gain in efficiency is probably not big, since the bottleneck is in the gemm step

extern Ugenerators_resorted resortUgenerators(const Ugenerators &E, bool symmetrize=true, bool transpose=false, bool antisymmetrize=false);
extern Ugenerators_resorted resortjoinLambda1(const Ugenerators (&Lambda1)[2], const unsigned int (&nelec)[2]);
extern Ugenerators_reorganized reorganizeUgenerators(const Ugenerators_resorted&E);

#endif
