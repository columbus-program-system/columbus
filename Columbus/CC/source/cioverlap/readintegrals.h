/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _readintegrals_included
#define _readintegrals_included

//routine to read integrals ... the same aces2old interface like for masik's program to keep it simple
//note, aces interface writes the fock elements, not the one-electron integrals
//but this can be circumvented by restarting a cationic job removing all electrons

typedef index4type packedindex[sizeof(int)/sizeof(unsigned char)]; //check for 64-bit architecture

typedef enum interface {aces2old,ascii,binary} interface;

extern int readintegrals(OneElInt &onelint, fourindex<index4type,REAL> *twoelint, SparseMat<REAL> *h, const Ugenerators &E, REAL &ecore, interface source=binary, const bool incl2el=1, const bool inverseorb=false);

#endif
