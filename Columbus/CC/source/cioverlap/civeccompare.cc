/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#ifdef USE_TRACEBACK
#include "traceback.h"
#endif

#include "options.h"

//LA
#include "vec.h"
#include "mat.h"
#include "smat.h"
#include "nonclass.h"
#include "qsort.h"


#include "basic.h"
#include "gelfand.h"
#include "drt.h"

static spinorbindex *spinorb_begin;

static int spinorb_compare(const int i, const int j)
{
int xx=spinorb_begin[i]<0? -2*spinorb_begin[i]-1:2*spinorb_begin[i]-2;
int yy=spinorb_begin[j]<0? -2*spinorb_begin[j]-1:2*spinorb_begin[j]-2;
return xx-yy;
}

static void spinorb_swap(const int i, const int j)
{
spinorbindex t;
t=spinorb_begin[i];
spinorb_begin[i]=spinorb_begin[j];
spinorb_begin[j]=t;
}


void canonicalize(slaterbasis &sl, NRMat<REAL> &ci)
{
sl.copyonwrite();
ci.copyonwrite();
if(sl.nrows()!=ci.ncols()) laerror("inconsistent ci size in canonicalize");
lexindex cisize=ci.ncols();
int nelec=sl.ncols();
int nci=ci.nrows();

//put the spinorbitals in the order 1 -1 2 -2 ...
for(lexindex l=0; l<cisize; ++l)
	{
//	cerr <<" before ";for(int i=0; i<nelec; ++i) cout <<sl[l][i]<<" "; cout <<endl;
	spinorb_begin=sl[l];
	int sig;
	if(sig=genqsort(0,nelec-1,spinorb_compare,spinorb_swap)) //odd permutation
		{
		for(int i=0; i<nci; ++i) ci[i][l] *= -1.;
		}
//	cerr <<"  after ";for(int i=0; i<nelec; ++i) cout <<sl[l][i]<<" "; cout << " : " <<(sig?'-':'+')<<endl;
	}
}

int main(int argc, char **argv)
{
#ifdef USE_TRACEBACK
sigtraceback(SIGSEGV,1);
sigtraceback(SIGABRT,1);
sigtraceback(SIGBUS,1);
sigtraceback(SIGFPE,1);
#endif

int f;

cout.setf(ios::fixed);
//cout.setf(ios::scientific);
    cout.precision(12);

if(argc<5)  laerror("civeccompare [-f] [-g] civec1 slaterfile1 civec2 slaterfile2 <freeze1 freeze2 nelec thres");

bool flip1=0;
bool flip2=0;

if(!strcmp(argv[1],"-f"))
	{
	flip1=1;
	++argv;
	--argc;
	}

if(!strcmp(argv[1],"-g"))
	{
	flip2=1;
	++argv;
	--argc;
	}

if(argc<5)  laerror("civeccompare [-f] [-g] civec1 slaterfile1 civec2 slaterfile2 <freeze1 freeze2 nelec thres");

int freeze1,freeze2,nelec;
REAL thres;

cin >>freeze1 >>freeze2 >>nelec >>thres;

//read ci vectors
NRMat<REAL> civec1,civec2;
f=open(argv[1],O_RDONLY|O_LARGEFILE); if(f<0) perror("cannot open file");
civec1.get(f,true);
close(f);
f=open(argv[3],O_RDONLY|O_LARGEFILE); if(f<0) perror("cannot open file");
civec2.get(f,true);
close(f);
int ncivec=min(civec1.nrows(),civec2.nrows());
if(civec1.ncols()!=civec2.ncols()) laerror("ci vector length mismatch");
lexindex cisize=civec1.ncols();

//read slaterfiles
int nelec1=2*freeze1+nelec;
int nelec2=2*freeze2+nelec;
slaterbasis sl1(cisize,nelec1),sl2(cisize,nelec2);
f=open(argv[2],O_RDONLY|O_LARGEFILE); if(f<0) perror("cannot open file");
sl1.get(f,false);
if(sl1.checkzero())  laerror("malformed slaterfile encountered, perhaps cipc/mcpc was compiled without --assume byterecl");
if(flip1) sl1 *= -1;
close(f);
f=open(argv[4],O_RDONLY|O_LARGEFILE); if(f<0) perror("cannot open file");
sl2.get(f,false);
if(sl2.checkzero())  laerror("malformed slaterfile encountered, perhaps cipc/mcpc was compiled without --assume byterecl");
if(flip2) sl2 *= -1;
close(f);

//perform freezing first
canonicalize(sl1,civec1);
canonicalize(sl2,civec2);
slaterbasis sl1f(cisize,nelec),sl2f(cisize,nelec);
for(lexindex l=0; l<cisize; ++l)
	{
	for(int i=0; i<freeze1; ++i) if(sl1(l,2*i)!=i+1 || sl1(l,2*i+1)!= -i-1) 
		{
		cerr <<"L= "<<l<< " i="<<i<<" sl1 "<<sl1(l,2*i)<<" "<< sl1(l,2*i+1)<<endl;
		laerror("canonicalization assertion failed in freeze1");
		}
	for(int i=0; i<freeze2; ++i) if(sl2(l,2*i)!=i+1 || sl2(l,2*i+1)!= -i-1) laerror("canonicalization assertion failed in freeze2");
	for(int i=0; i<nelec;++i) 
		{
		spinorbindex t;
		t=sl1(l,2*freeze1+i);
		sl1f(l,i)=t>0?t-freeze1:t+freeze1;
		t=sl2(l,2*freeze2+i);
		sl2f(l,i)=t>0?t-freeze2:t+freeze2;
		}
	}
//free memory
sl1.resize(0,0); sl2.resize(0,0);

//reorder slater basis and ci vectors to common order
canonicalize(sl1f,civec1);
canonicalize(sl2f,civec2);
NRVec<SPMatindex> slperm1(cisize);
for(lexindex i=0; i<cisize; ++i) slperm1[i]=i;
slsort_sldetbase=&sl1f;
slsort_permbase=&slperm1[0];
slsort_nelectrons=nelec;
genqsort(0,(int)cisize-1,slsort_slbascmp,slsort_slbasswap);
NRVec<SPMatindex> slperm2(cisize);
for(lexindex i=0; i<cisize; ++i) slperm2[i]=i;
slsort_sldetbase=&sl2f;
slsort_permbase=&slperm2[0];
slsort_nelectrons=nelec;
genqsort(0,(int)cisize-1,slsort_slbascmp,slsort_slbasswap);
NRMat<REAL> civec1s(ncivec,cisize);
NRMat<REAL> civec2s(ncivec,cisize);

//apply permutation
slaterbasis sl1fs(cisize,nelec),sl2fs(cisize,nelec);
for(lexindex i=0; i<cisize; ++i)
	{
	memcpy(sl1fs[i],sl1f[slperm1[i]],nelec*sizeof(spinorbindex));
	memcpy(sl2fs[i],sl2f[slperm2[i]],nelec*sizeof(spinorbindex));
	for(int j=0; j<ncivec; ++j) civec1s(j,i)=civec1(j,slperm1[i]);
	for(int j=0; j<ncivec; ++j) civec2s(j,i)=civec2(j,slperm2[i]);
	}

//make the phases unique according to the largest coefficient
for(int i=0; i<ncivec; ++i)
        {
	//find largest element in first CI vector
	lexindex lm;
	REAL t, amax=-1;
	for(lexindex l=0; l<cisize; ++l) 
		if((t=abs(civec1s(i,l))) > amax)
			{
			lm=l; amax=t;
			}
	if(civec1s(i,lm)*civec2s(i,lm)<0)
		{
		for(lexindex l=0; l<cisize; ++l) civec2s(i,l) *= -1;
		}
	}

//finally perform the elementwise comparison of all coefficients above thres and print resume
for(int i=0; i<ncivec; ++i)
        {
	int ndev=0;
	REAL t,u=0.,maxdev=-1;
	for(lexindex l=0; l<cisize; ++l)
		{
		if(abs(civec1s(i,l)) > thres && abs(civec2s(i,l)) > thres)
			{
			if(civec1s(i,l)*civec2s(i,l)<0) ++ndev; 
			}
		if((t=abs(civec1s(i,l)-civec2s(i,l))) >maxdev) maxdev=t;
		u+=t;
		}
	if(ndev) cout <<"number of deviations>thr in CI vector "<<i<<" = "<<ndev <<endl; else cout <<"CI vector "<<i<<" OK\n";
	cout <<"Maximum deviation for CI vector "<<i<<" = "<<maxdev<<endl;
	cout <<"Sum of |deviations| for CI vector "<<i<<" = "<<u<<endl;
	}

//print detailed test output
for(int i=0; i<ncivec; ++i)
	{
	cout <<"Comparing CI vector "<<i<<endl;
	for(lexindex l=0; l<cisize; ++l)
		{
		cout << "VEC1: "; for(int j=0; j<nelec; ++j) {int x=sl1fs[l][j]; cout <<(x<0?x-freeze1:x+freeze1)<<" ";} cout <<": " <<civec1s(i,l)<<endl;
		cout << "VEC2: "; for(int j=0; j<nelec; ++j) {int y=sl2fs[l][j]; cout <<(y<0?y-freeze2:y+freeze2)<<" ";} cout <<": " <<civec2s(i,l)<<endl;
		if(abs(civec1s(i,l)-civec2s(i,l))>thres) cout<< " DIFFERENCE "<<civec1s(i,l)-civec2s(i,l);
		cout <<endl;
		}
	}



}


