/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#define EPSPRINT .05

#include "la.h"
#include "fourindex.h"
#include "nonclass.h"
#include "basic.h"

#ifdef USE_TRACEBACK
#include "traceback.h"
#endif

#include "sifs.h"

using namespace LA;
int main(int argc, char **argv)
{
#ifdef USE_TRACEBACK
sigtraceback(SIGSEGV,1);
sigtraceback(SIGABRT,1);
sigtraceback(SIGBUS,1);
sigtraceback(SIGFPE,1);
#endif

cout.setf(ios::fixed);
    cout.precision(12);

if(argc<2) 
	{
	cout <<"Usage: readsifs [-1] file\n";
	cout <<"Stores one-electron as NRSMat (with dimensions) in file.itypea.itypeb and S,H without dimensions for tinyscf\n";
	cout <<"and converts the two-electron integrals to fourindex_ext format\n";
	exit(1);
	}
--argc; ++argv;

bool oneonly=false;
if(!strcmp(*argv,"-1"))
        {
        ++argv;
        --argc;
        oneonly=true;
        }


int f=open(*argv,O_RDONLY|O_LARGEFILE);
if(f<=0) {perror("open:"); laerror("cannot open file for reading");}

sifs sif(f);

cout <<"SIFS headers\n";
cout <<sif<<endl;

{
char name[256];
sprintf(name,"%s0",*argv);
int g=open(name,O_WRONLY|O_LARGEFILE|O_CREAT,0666);
write(g,sif.energies,sizeof(REAL));
write(g,&sif.header.nbas,sizeof(int));
write(g,&sif.header.nbas,sizeof(int));
close(g);
}

NRSMat<REAL> x;
NRSMat<REAL> s;
NRSMat<REAL> h(0.,sif.header.nbas);
int itypea,itypeb,last;
REAL fcore;
int res;
do{
res=read_sifs_onel(sif,x,itypea,itypeb,last,fcore);
//cout <<x;
cout <<"read number of integral blocks "<<res<<" last "<<last<<" itypea "<<itypea<<" itypeb "<<itypeb<<" fcore "<<fcore<<"\n";
if(res>0)
	{
	char name[256];
	sprintf(name,"%s.%d.%d",*argv,itypea,itypeb);
	int g=open(name,O_WRONLY|O_LARGEFILE|O_CREAT,0666);
	x.put(g,true);
	close(g);
	if(itypea==0 && itypeb==0) s=x;
	if(itypea==0 && (itypeb==1 || itypeb==2)) h+=x;
	}
} while (res>0);

{
char name[256];
sprintf(name,"%s1S",*argv);
int g=open(name,O_WRONLY|O_LARGEFILE|O_CREAT,0666);
s.put(g,false);
close(g);
sprintf(name,"%s1H",*argv);
g=open(name,O_WRONLY|O_LARGEFILE|O_CREAT,0666);
h.put(g,false);
close(g);
}

if(oneonly) exit(0);

//read 2-el integrals
if(sif.infos[0]==2) 
	{
	close(f);
	char name[256];
        sprintf(name,"%s2",*argv);
	f=open(name,O_RDONLY|O_LARGEFILE);
	if(f<=0) {perror("open:"); laerror("cannot open file for reading");}
	cout << "continuing from file "<<name<<endl;
	sif.fd=f;
	}
else
	{
	if(res!= -3) laerror("2-electron integrals not found while fsplit==1");
	}

fourindex_sifs<TWOEL_INDEX, REAL> twoel(sif);
//cout <<twoel;
char name[256];
sprintf(name,"%s2E",*argv);
int g=open(name,O_WRONLY|O_LARGEFILE|O_CREAT,0666);
fourindex_ext<TWOEL_INDEX, REAL> twoel2(g,twoelectronrealmullikan,twoel.size());
fourindex_sifs<TWOEL_INDEX, REAL>::iterator it=twoel.begin();
while(it!=twoel.end())
	{
	twoel2.put(*it);
	++it;
	}
close(g);
close(f);
}
