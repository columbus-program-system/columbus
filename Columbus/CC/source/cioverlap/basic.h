/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _basic_h
#define _basic_h
typedef double REAL;
typedef unsigned int lexindex;
typedef unsigned char index4type;
typedef index4type orbindex;
typedef short spinorbindex;
typedef /*signed*/ char occnum; //signed type is crucial to ensure correct processing of tests >=0 in loops
typedef unsigned char index4type; //must be able to hold up to number of orbitals ... possibly enlarge to unsigned short

#include "la.h"
using namespace LA;

typedef NRSMat_from1<REAL> OneElInt;
typedef NRSMat_from1<SparseMat<REAL> > Ugenerators;
typedef NRVec<NRSMat_from1<SparseMat<REAL> > > Ugenerators_resorted;


#endif
