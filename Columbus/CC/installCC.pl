#!/usr/bin/perl -w
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#================================================================
# Installation of Jiri Pittner's wave function analysis programs
#   in connection with Columbus.
# -----------------------------
# This script was adapted from the Newton-X installation script installnx.pl
#
# 2011-05-11
# - Felix Plasser
#
##================================================================
## SET YOUR CHOICES
  use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
  use colib_perl;

print_STDOUT(" ================================================\n");
print_STDOUT("   Installation of Columbus CC package           \n");
print_STDOUT(" ================================================\n");

print " *** Configuration *** \n";

# command line arguments
$ciov_config=""; 
$i = 0;
while ( $i <= $#ARGV )
  { $_=$ARGV[$i];
    if (/-forcestatic/) {$ciov_config="--enable-static";}
  $i++;}
  
if ($ciov_config eq "")
{
    print "Dynamic linking of the executables.\n";
}
else
{
    print "Static linking of the executables.\n";
}


# variables from configuration files
$cbus=$ENV{'COLUMBUS'};
$instd="$cbus/CC";
$bin="$cbus";

$machine_id=$ENV{'MACHINE'};
$machine_file="$cbus/machine.cfg/$machine_id";
print "using machine file information from\n $machine_file\n";
  
$idgsl=keyinfile($machine_file,"IDGSL"); #"/usr/include/gsl"; # Headers
$ldgsl=keyinfile($machine_file,"LDGSL"); #"/usr/lib64"; # Libraries
print "IDGSL: $idgsl\n";
print "LDGSL: $ldgsl\n";
  
$fortran=keyinfile($machine_file,FORTRAN);
print "FORTRAN: $fortran";
$_=$fortran;
if (/byterecl/){print "\n - contains byterecl, seems correct\n";}
else {print "\n\n *** Warning the Fortran compiler does not include '-assume byterecl'! ***\n";
      print " Reconsider the machine configuration if civecconsolidate does not work\n\n";}
      
# parsing the machine file is kind of problematic
print "Assuming 64-bit integer compilation.\n";
print "If this is not ok, change status_record32 in \$COLUMBUS/CC/installCC.pl\n\n";
$status_record32 = "on";      # ON: if DALTON was compiled with 64 bits
 
$skip_error = 0; # if set to 1 -> keep going if there is an error

##================================================================


print " *** Checking libraries ***\n";
check_cioverlap();
parse_sifs();
print "\n *** Starting compilation ***\n";
compile_CC();

end_install();

#

#================================================================
#
sub check_cioverlap{

  # Libraries
  $lib="blas";
  ($rc{$lib},$plib{$lib})=check_lib($lib);
  $lib="lapack";
  ($rc{$lib},$plib{$lib})=check_lib($lib);
  $lib="gslcblas";
  ($rc{$lib},$plib{$lib})=check_lib($lib);
  $lib="gsl";
  ($rc{$lib},$plib{$lib})=check_lib($lib);

}
#
#================================================================
#
sub parse_sifs{

  if ($status_record32 eq "off"){
    $sentence="// #define FORCE_FORTRAN_INT_64_RECORD32";
    write_sifs();
    $sifsbits="32 bits";
  }else{
    $sentence=" #define FORCE_FORTRAN_INT_64_RECORD32";
    write_sifs();
    $sifsbits="64 bits";
  }

}
#
#================================================================
#
sub write_sifs{

  my $sifsdir="$instd/source/cioverlap";
  system("cp -f $sifsdir/sifs.h $sifsdir/sifs.h.bk"); # backup file
  open(OLD,"<$sifsdir/sifs.h.bk") or die "Cannot read $sifsdir/sifs.h.bk";
  open(NEW,">$sifsdir/sifs.h") or die "Cannot write to $sifsdir/sifs.h";
  while(<OLD>){
    if (/#define FORCE_FORTRAN_INT_64_RECORD32/){
      print NEW "$sentence\n";
    }else{    
      print NEW "$_";
    }
  }
  close(NEW);
  close(OLD);

}
#
#================================================================
#
#
sub compile_CC{
 

      print_STDOUT(" Compiling LA  ");
      $basedir="$instd/source";
      $targdir=$basedir;

      $prog="LA";
      $path=export_paths($prog);
      configure($prog,$path);

      $path="";
      $prog="LA";
      $dir_prog="LA";
      $dir_final="$basedir";
      $clean = 0;
      make_files($prog,$dir_prog,$dir_final,$clean,$path);

      print STDOUT "DONE\n";

    print_STDOUT(" Compiling CIOVERLAP  ");
    $basedir="$instd/source";
    $targdir="$basedir";

    $prog="cioverlap";
    $path=export_paths($prog);
    configure($prog,$path);

    $prog="civecconsolidate";
    $dir_prog="cioverlap";
    $dir_final="$bin";
    $clean = 1;
    make_files($prog,$dir_prog,$dir_final,$clean,$path);

    $prog="readsifs";
    $dir_prog="cioverlap";
    $dir_final="$bin";
    $clean = 1;
    make_files($prog,$dir_prog,$dir_final,$clean,$path);

    $prog="cioverlap";
    $dir_prog="cioverlap";
    $dir_final="$bin";
    $clean = 1;
    make_files($prog,$dir_prog,$dir_final,$clean,$path);

    print STDOUT "DONE\n";


}
#
#================================================================
#
sub make_files{
# Usage: make_files(Program, Directory, Final directory, Delete files flag)
#
  local($prog,$dir_prog,$dir_final,$result,$clean,$path,$label);
  ($prog,$dir_prog,$dir_final,$clean,$path)=@_;
  $label=$prog;
  if ($prog eq "LA"){
   $prog="";
  }
  print STDOUT " Starting MAKEFILE for $label program.\n";
  print "changing to $targdir/$dir_prog\n";
  chdir("$targdir/$dir_prog");
  print STDOUT "....";
  if ($label eq "LA"){
   print STDOUT "(please, wait)";
  }
  $result=`$path echo \$LD_LIBRARY_PATH; make $prog 2>&1`;
  print STDOUT "....";
  print STDOUT "\n$result\n";
  print STDOUT "\n Compilation of $label ended with value $?\n";
  $status{$prog}=$?;
  if (($? ne 0) && ($skip_error eq 0)){exit($?);}
  if (!-e $dir_final){
    system("mkdir $dir_final");
  }
  if ($label ne "LA"){
    system("cp -f $prog $dir_final/.");
  }
  if ($label eq "LA"){
    system("rm -f $targdir/$dir_prog/.libs/libla.so*"); # static link with LA
  }
}
#
#================================================================
#
sub configure{
# Run ./configure inside $prog.
  my ($prog,$path);
  ($prog,$path)=@_;
  print STDOUT " Starting CONFIGURE for $prog.\n";
  print "changing to $targdir/$prog\n";
  chdir("$targdir/$prog");
  print STDOUT "....";
#fp: make sure it is executable
  $result=`$path echo \$LD_LIBRARY_PATH; echo \$LDFLAGS; chmod +x \./configure; \./configure $ciov_config 2>&1`;
  print STDOUT "....";
  print STDOUT "\n$result\n";
  print STDOUT "\n Configuration of $prog ended with value $?\n";
  $status_conf{$prog}=$?;
  if (($? ne 0) && ($skip_error eq 0)){exit($?);}
  chdir("$bin");
}
#
#================================================================
#
sub export_paths{
  my ($prog,@g,$path);
  ($prog)=@_;
  if ($prog eq "LA"){
    $path=      " export CXXFLAGS=\'-I$idgsl\';";
    $path=$path." export LDFLAGS=\'-L$ldgsl -lgsl -lgslcblas\';";
  }elsif($prog eq "cioverlap"){
    $path=      " export CXXFLAGS=\'-I$idgsl -I$instd/source/LA\';";
    $path=      " export CXXFLAGS=\'-I$instd/source/LA\';";
    $path=$path." export LDFLAGS=\'-L$ldgsl -lgsl -lgslcblas -L$instd/source/LA/.libs -lla\';";
#    $path=$path." export LDFLAGS=\'-L$ldgsl -lgsl -lgslcblas -latlas -L$instd/source/LA/.libs -lla\';";
    $path=$path." export LD_LIBRARY_PATH=$instd/source/LA/.libs:\$LD_LIBRARY_PATH;";
    $path=$path." export CPLUS_INCLUDE_PATH=$instd/source/LA; ";
  }
  (@g)=split(/;/,$path);
  print STDOUT "\n";
  foreach(@g){
    print STDOUT "$_\n";
  }
  print STDOUT "\n";
  return $path;
}
#
#================================================================
#
sub end_install{
  print_STDOUT(" Setting permissions ");
  print STDOUT "....";
  print STDOUT "....DONE\n";

  report();

  print_STDOUT("\n Installation completed!\n");
}
#
#================================================================
#
sub report{
  my ($key,$value,$final_status,$final_status_conf,$txt1,$txt2);

  print STDOUT " Installation status report";

  $final_status_conf=0;
  while (($key, $value) = each(%status_conf)){
     if ($value == 0){
       $value = "success";
     }else{
       $value = "fail ($value)";
       $final_status_conf++;
     }
     $key=sprintf("%-20s",$key);
     print STDOUT "  Configure: ".$key." :: status: ".$value."\n";
  }

  $final_status=0;
  while (($key, $value) = each(%status)){
     if ($value == 0){
       $value = "success";
     }else{
       $value = "fail ($value)";
       $final_status++;
     }
     $key=sprintf("%-20s",$key);
     print STDOUT "  Program:   ".$key." :: status: ".$value."\n";
  }
  if ($final_status_conf <= 1){
    $txt1="program";
  }else{
    $txt1="programs";
  }
  if ($final_status <= 1){
    $txt2="program";
  }else{
    $txt2="programs";
  }
  print_STDOUT(" $final_status_conf $txt1 ended with configuration problem.\n");  
  print_STDOUT(" $final_status $txt2 ended with compilation problem.\n");
}
#
#================================================================
#

#================================================================
# This routine (check_lib) checks the existence of a specific
# library in a Linux system. It looks for lib<name>.so.* at 
# $LD_LIBRARY_PATH and at /etc/ld.so.conf file and returns (1)
# the number of paths where the library was found (0 if it is
# not found) and (2) a string with the paths.
# Example of usage:
#   $lib="blas";
#   ($rc,$plib)=check_lib($lib);
#   if ($rc != 0){
#     print "-l$lib was found in $plib\n";
#   }else{
#     print "-l$lib was not found in your system.\n"
#   }
#================================================================
#
sub check_lib{
  my ($plib,$lib,$libname,$rc,@file,$dir);
  ($lib)=@_;
  $rc=0;
  undef(@paths);

  read_ldlibpath();
  read_ldsoconf();

  $libname="lib".$lib.".so";

  $plib="";
  foreach(@paths){
    $dir=$_;
    opendir(DIR,$dir) or warn "cannot open dir $dir: $!";
    @file= readdir DIR;
    closedir DIR;

    $n=0;
    foreach(@file){
      if ($_ =~m/^$libname/){
        $n++;
      }
    }
    if ($n > 0){
       $plib=$plib.$dir." ";
       $rc++;
    }
  }

  if ($rc == 0){
    $plib = "<<NOT FOUND>>";
  }
  print "$lib present in: $plib\n";

  return $rc,$plib;

}
#
#================================================================
#
sub read_ldlibpath{
  my ($LDLP,@g);
  $LDLP = $ENV{"LD_LIBRARY_PATH"};
  if ($LDLP){
    @g=split(/:/,$LDLP);
  }
  foreach(@g){
    if (-e $_){
      push(@paths,$_);
    }
  }
}
#
#================================================================
#
sub read_ldsoconf{
  my ($fl,$ind);
  $fl="/etc/ld.so.conf";
  $ind=0;
  if (-s $fl){
    open(FL,$fl) or warn "$fl does not exist or is empty.";
    while(<FL>){
      chomp;$_ =~ s/^\s*//;$_ =~ s/\s*$//;
      if (/^\//){
        if (-e $_){
          push(@paths,$_);
        }
      }
      if ($_ =~ m/\/usr\/local\/lib/){
        $ind=1;
      }
    }
    close(FL);
    if ($ind==0){
      push (@paths,"/usr/local/lib");
    }
  }
}
#
#================================================================
#
sub print_STDOUT{
# Print unformated outputs to STDOUT.
# Usage:
# print_STDOUT($output);
#
  my ($text);
  ($text)=@_;
  print STDOUT "$text";
#  print LOG "$text";
}
#
#================================================================
#

