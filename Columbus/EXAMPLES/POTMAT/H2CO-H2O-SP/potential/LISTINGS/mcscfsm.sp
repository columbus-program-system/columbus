 total ao core energy =   28.629706965
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          3             0.333 0.333 0.333

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336

 Number of active-double rotations:     16
 Number of active-active rotations:      0
 Number of double-virtual rotations:    44
 Number of active-virtual rotations:   176
 
 iter=    1 emc= -114.0794673619 demc= 1.1408E+02 wnorm= 4.5783E-01 knorm= 2.0788E-01 apxde= 2.3050E-02    *not converged* 
 iter=    2 emc= -114.1070054801 demc= 2.7538E-02 wnorm= 8.8010E-02 knorm= 4.0819E-01 apxde= 5.3500E-03    *not converged* 
 iter=    3 emc= -114.1147788716 demc= 7.7734E-03 wnorm= 2.6955E-02 knorm= 2.6924E-01 apxde= 1.6436E-03    *not converged* 
 iter=    4 emc= -114.1178100042 demc= 3.0311E-03 wnorm= 1.5814E-02 knorm= 2.6324E-01 apxde= 1.5923E-03    *not converged* 
 iter=    5 emc= -114.1207204553 demc= 2.9105E-03 wnorm= 1.3116E-02 knorm= 3.3270E-02 apxde= 1.1146E-02    *not converged* 
 iter=    6 emc= -114.1209347076 demc= 2.1425E-04 wnorm= 1.3408E-02 knorm= 3.7516E-02 apxde= 1.3039E-02    *not converged* 
 iter=    7 emc= -114.1212078778 demc= 2.7317E-04 wnorm= 1.3869E-02 knorm= 1.7788E-01 apxde= 1.5000E-02    *not converged* 
 iter=    8 emc= -114.1231106500 demc= 1.9028E-03 wnorm= 1.7937E-02 knorm= 2.0205E-01 apxde= 1.8421E-02    *not converged* 
 iter=    9 emc= -114.1264622316 demc= 3.3516E-03 wnorm= 2.1811E-02 knorm= 2.4594E-01 apxde= 9.9976E-03    *not converged* 
 iter=   10 emc= -114.1306715203 demc= 4.2093E-03 wnorm= 1.7422E-02 knorm= 2.0956E-01 apxde= 1.7810E-03    *not converged* 
 iter=   11 emc= -114.1323046425 demc= 1.6331E-03 wnorm= 5.8220E-03 knorm= 1.4716E-02 apxde= 1.1950E-05    *not converged* 
 iter=   12 emc= -114.1323166988 demc= 1.2056E-05 wnorm= 4.4392E-05 knorm= 1.9840E-04 apxde= 1.6424E-09    *not converged* 

 final mcscf convergence values:
 iter=   13 emc= -114.1323167004 demc= 1.6430E-09 wnorm= 6.4656E-07 knorm= 3.8012E-08 apxde= 1.2639E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=   -114.270090851
   DRT #1 state # 2 weight 0.333333 total energy=   -114.153358471
   DRT #1 state # 3 weight 0.333333 total energy=   -113.973500780
   ------------------------------------------------------------


