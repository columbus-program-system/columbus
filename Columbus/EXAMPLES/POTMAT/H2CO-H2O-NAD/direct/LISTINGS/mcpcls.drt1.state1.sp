

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:58:32.237 04-Feb-08
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a  

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -114.0710757175    nuclear repulsion=    30.9052930207
 demc=             0.0000000000    wnorm=                 0.0000002653
 knorm=            0.0000000379    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:

In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:

 The CSFs for the state No  1 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
 csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1  0.9471695149  0.8971300899  33333300
     21 -0.2385257299  0.0568945238  33313320
     41 -0.1593087258  0.0253792701  33303330
     26  0.0645626065  0.0041683302  33313122
     24 -0.0545411281  0.0029747347  33313212
      3 -0.0540025717  0.0029162778  33333102
      6 -0.0448888327  0.0020150073  33333003
    127  0.0414114544  0.0017149086  31323312
     72 -0.0273812290  0.0007497317  33123312
    110  0.0268927021  0.0007232174  31333203
     52 -0.0223252522  0.0004984169  33133302
     91 -0.0216167376  0.0004672833  33033330
     81 -0.0212877356  0.0004531677  33113322
    176 -0.0211202064  0.0004460631  30333330
     55 -0.0203280540  0.0004132298  33133203
     93 -0.0180686293  0.0003264754  33033303
    178 -0.0159781019  0.0002552997  30333303
     43 -0.0158770563  0.0002520809  33303303
    146 -0.0152045042  0.0002311769  31233330
     46  0.0149030337  0.0002221004  33303033
      4 -0.0147697303  0.0002181449  33333030
     15 -0.0134880085  0.0001819264  33330330
     53  0.0132654363  0.0001759718  33133230
    148  0.0126554259  0.0001601598  31233303
     28 -0.0122893954  0.0001510292  33313023
     17 -0.0122620793  0.0001503586  33330303
     44 -0.0087118882  0.0000758970  33303132
     83  0.0085442542  0.0000730043  33113223
    107  0.0080439068  0.0000647044  31333302
    144  0.0076714661  0.0000588514  31303233
     89 -0.0075884962  0.0000575853  33103233
    311 -0.0061575609  0.0000379156  03333303
    309 -0.0060467702  0.0000365634  03333330
    138 -0.0051522371  0.0000265455  31313223
    187  0.0048488245  0.0000235111  30313323
    136  0.0048346871  0.0000233742  31313322
     87  0.0047558646  0.0000226182  33103332
    102  0.0045661538  0.0000208498  33013323
    105  0.0043822551  0.0000192042  33003333
     75 -0.0043463063  0.0000188904  33123123
    130  0.0040425827  0.0000163425  31323123
    179 -0.0038769589  0.0000150308  30333132
    190  0.0038684700  0.0000149651  30303333
    149 -0.0036833015  0.0000135667  31233132
     57 -0.0035552112  0.0000126395  33133032
    205  0.0031838102  0.0000101366  13332330
    160 -0.0031636920  0.0000100089  31203333
    207 -0.0030967474  0.0000095898  13332303
    218  0.0025547804  0.0000065269  13323312
    320  0.0024392958  0.0000059502  03313323
    267 -0.0023811828  0.0000056700  12333330
    157 -0.0023354858  0.0000054545  31213323
     50  0.0021402143  0.0000045805  33300333
     94  0.0020966949  0.0000043961  33033132
     18  0.0019687806  0.0000038761  33330132
    237 -0.0019398885  0.0000037632  13233330
    191 -0.0019264886  0.0000037114  30133332
    201  0.0015492874  0.0000024003  13333203
    142 -0.0014477937  0.0000020961  31303332
     96  0.0012979494  0.0000016847  33033033
    208  0.0012929113  0.0000016716  13332132
    312 -0.0011859321  0.0000014064  03333132
    108  0.0011132258  0.0000012393  31333230
     39 -0.0011106900  0.0000012336  33310323
    323  0.0010472302  0.0000010967  03303333
    181  0.0010314762  0.0000010639  30333033
    196  0.0010104336  0.0000010210  30033333

 input the coefficient threshold (end with 0.) [ 0.0000]:

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
