

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:58:32.237 04-Feb-08
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a  

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -114.0710757175    nuclear repulsion=    30.9052930207
 demc=             0.0000000000    wnorm=                 0.0000002653
 knorm=            0.0000000379    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:

In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:

 The CSFs for the state No  2 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
 csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      7  0.9787848349  0.9580197531  33331320
     29  0.1399882956  0.0195967229  33312330
    217  0.0833733363  0.0069511132  13323330
     32  0.0514465549  0.0026467480  33312132
     14  0.0485574177  0.0023578228  33331023
    115 -0.0394825999  0.0015588757  31332312
      2 -0.0358023168  0.0012818059  33333120
    202 -0.0346037670  0.0011974207  13333122
     10  0.0257658671  0.0006638799  33331212
    122  0.0236146420  0.0005576513  31331223
    132  0.0223363798  0.0004989139  31321332
     67 -0.0216971253  0.0004707652  33131223
    200  0.0213875157  0.0004574258  13333212
     98 -0.0190303092  0.0003621527  33031323
    183 -0.0177616996  0.0003154780  30331323
    252 -0.0173938654  0.0003025466  13133322
     77 -0.0170676584  0.0002913050  33121332
    118 -0.0152714778  0.0002332180  31332123
     48 -0.0149222946  0.0002226749  33301323
    238  0.0148319873  0.0002199878  13233312
     34 -0.0143136299  0.0002048800  33312033
     84 -0.0133279635  0.0001776346  33112332
    197 -0.0128952010  0.0001662862  13333320
     60  0.0124224782  0.0001543180  33132312
     63  0.0122677987  0.0001504989  33132123
    153  0.0121615829  0.0001479041  31231323
    120  0.0114077355  0.0001301364  31331322
     36 -0.0092519438  0.0000855985  33311232
    316 -0.0076041484  0.0000578231  03331323
    222 -0.0074873651  0.0000560606  13323033
    141 -0.0070572698  0.0000498051  31312233
    126 -0.0069651109  0.0000485128  31323330
    220 -0.0068499412  0.0000469217  13323132
     23 -0.0065239106  0.0000425614  33313230
    268 -0.0053497450  0.0000286198  12333312
     12 -0.0051333847  0.0000263516  33331122
     86  0.0046445626  0.0000215720  33112233
    260 -0.0040664708  0.0000165362  13123233
    104 -0.0038747245  0.0000150135  33012333
    159  0.0035467911  0.0000125797  31212333
     79  0.0032815639  0.0000107687  33121233
    296  0.0032124321  0.0000103197  11323233
    288  0.0030033201  0.0000090199  11333322
    189 -0.0029764978  0.0000088595  30312333
    294  0.0027864426  0.0000077643  11323332
    134 -0.0027765421  0.0000077092  31321233
    215 -0.0027304422  0.0000074553  13330323
    139  0.0027246788  0.0000074239  31312332
     31  0.0026708407  0.0000071334  33312303
    206 -0.0026507915  0.0000070267  13332312
    111  0.0025921570  0.0000067193  31333122
    249  0.0024220941  0.0000058665  13213233
    258 -0.0023424962  0.0000054873  13123332
    266  0.0022337651  0.0000049897  13023333
    277 -0.0020652116  0.0000042651  12313332
      5 -0.0020311732  0.0000041257  33333012
     65  0.0019398181  0.0000037629  33131322
    254  0.0019029998  0.0000036214  13133223
    302 -0.0015637361  0.0000024453  11223333
    307  0.0015566935  0.0000024233  10323333
    279 -0.0014459839  0.0000020909  12313233
    271  0.0013408172  0.0000017978  12333123
    161  0.0012938029  0.0000016739  31133322
    213  0.0012594033  0.0000015861  13331223
    223  0.0012047036  0.0000014513  13321332
    147 -0.0011466764  0.0000013149  31233312
    241  0.0010045227  0.0000010091  13233123

 input the coefficient threshold (end with 0.) [ 0.0000]:

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
