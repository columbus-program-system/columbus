

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       100000000 of real*8 words (  762.94 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   FCIORB=  1,3,20,1,4,20,1,5,20,1,6,20,1,7,20,1,8,20,1,9,20,1,10,20
   NAVST(1) = 3,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   WAVST(1,3)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /scratch/matruc/potential-examples/potmat/H2CO-H2O-NAD/po
 ten

 Integral file header information:
 Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:58:27.585 04-Feb-08

 Core type energy values:
 energy( 1)=  3.090529302074E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   30.905293021


   ******  Basis set informations:  ******

 Number of irreps:                  1
 Total number of basis functions:  32

 irrep no.              1
 irrep label           A  
 no. of bas.fcions.    32


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          3             0.333 0.333 0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
             4

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       3(  3)    20
       1       4(  4)    20
       1       5(  5)    20
       1       6(  6)    20
       1       7(  7)    20
       1       8(  8)    20
       1       9(  9)    20
       1      10( 10)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336
 
 !timer: initialization                  cpu_time=     0.001 walltime=     0.002

 faar:   0 active-active rotations allowed out of:  28 possible.


 Number of active-double rotations:     16
 Number of active-active rotations:      0
 Number of double-virtual rotations:    44
 Number of active-virtual rotations:   176

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is   0
 number of blocks to be transformed out of core is    1

 in-core ao list requires    0 segments.
 out of core ao list requires    1 segments.
 each segment has length  99991847 working precision words.

               ao integral sort statistics
 length of records:      4096
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  3040

 twoao processed     130078 two electron ao integrals.

     259628 ao integrals were written into   96 records

 Source of the initial MO coeficients:

 Input MO coefficient file: /scratch/matruc/potential-examples/potmat/H2CO-H2O-NAD/poten
 

               starting mcscf iteration...   1
 !timer:                                 cpu_time=     0.029 walltime=     0.029

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51862
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.032 walltime=     0.032

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.008 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.010 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 ciiter=  28 noldhv=  4 noldv=  4
 !timer: hmc(*) diagonalization          cpu_time=     0.156 walltime=     0.157

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2003027833     -145.1055958040        0.0000002033        0.0000010000
    2      -114.0320973099     -144.9373903307        0.0000003786        0.0000010000
    3      -113.8411431655     -144.7464361862        0.0000005989        0.0000010000
    4      -113.7969666680     -144.7022596887        0.0035534467        0.0100000000
 !timer: hmcvec                          cpu_time=     0.157 walltime=     0.157
 !timer: rdft                            cpu_time=     0.013 walltime=     0.013
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.002 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.019 walltime=     0.018
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.502787357461672E-002
 Total number of micro iterations:   11

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.97665804 pnorm= 0.0000E+00 rznorm= 2.3275E-06 rpnorm= 0.0000E+00 noldr= 11 nnewr= 11 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.003 walltime=     0.003

 fdd(*) eigenvalues. symmetry block  1
   -41.531158  -22.666244

 qvv(*) eigenvalues. symmetry block  1
     0.672774    0.721568    1.481326    1.643163    1.667475    1.800680    2.137010    2.178989    2.324352    2.346584
     2.494718    3.462675    3.487124    3.520962    3.850380    4.237158    4.341127    4.855919    5.192582    5.629263
     5.866891    6.364293
 *** warning *** large active-orbital occupation. i=  8 nocc= 1.9994E+00
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.223 walltime=     0.224

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc= -114.0245144196 demc= 1.1402E+02 wnorm= 4.4022E-01 knorm= 2.1480E-01 apxde= 2.1776E-02    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     0.252 walltime=     0.253

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51862
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.030 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.007 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.009 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 ciiter=  21 noldhv= 19 noldv= 19
 !timer: hmc(*) diagonalization          cpu_time=     0.118 walltime=     0.118

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2098404466     -145.1151334673        0.0000049439        0.0000100000
    2      -114.0657137324     -144.9710067531        0.0000073164        0.0000100000
    3      -113.8761207226     -144.7814137434        0.0000090054        0.0000100000
    4      -113.8173252765     -144.7226182972        0.0042657858        0.0100000000
 !timer: hmcvec                          cpu_time=     0.119 walltime=     0.119
 !timer: rdft                            cpu_time=     0.014 walltime=     0.013
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.001 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.001 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.019 walltime=     0.018
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.043884250113396E-002
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.91278554 pnorm= 0.0000E+00 rznorm= 4.8572E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.002 walltime=     0.002

 fdd(*) eigenvalues. symmetry block  1
   -41.310873  -22.608037

 qvv(*) eigenvalues. symmetry block  1
     0.684113    0.737270    1.479355    1.668902    1.680406    1.851103    2.181404    2.202458    2.307879    2.400601
     2.551205    3.513873    3.534499    3.560414    3.905167    4.283926    4.388361    4.887778    5.239864    5.673335
     5.906630    6.424186
 *** warning *** large active-orbital occupation. i=  8 nocc= 1.9991E+00
 !timer: motran                          cpu_time=     0.002 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.182 walltime=     0.182

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc= -114.0505583005 demc= 2.6044E-02 wnorm= 8.3511E-02 knorm= 4.0844E-01 apxde= 5.2187E-03    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     0.434 walltime=     0.434

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51862
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.030 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.008 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.010 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 ciiter=  19 noldhv=  4 noldv=  4
 !timer: hmc(*) diagonalization          cpu_time=     0.112 walltime=     0.112

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2150218721     -145.1203148928        0.0000034585        0.0000060797
    2      -114.0739117638     -144.9792047846        0.0000037624        0.0000060797
    3      -113.8849729204     -144.7902659411        0.0000019828        0.0000060797
    4      -113.8229673525     -144.7282603732        0.0044279998        0.0100000000
 !timer: hmcvec                          cpu_time=     0.112 walltime=     0.112
 !timer: rdft                            cpu_time=     0.013 walltime=     0.013
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.001 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.019 walltime=     0.018
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.062545397076788E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.97075823 pnorm= 0.0000E+00 rznorm= 4.5850E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.002 walltime=     0.002

 fdd(*) eigenvalues. symmetry block  1
   -41.270502  -22.596182

 qvv(*) eigenvalues. symmetry block  1
     0.685692    0.735957    1.476007    1.565588    1.673061    1.859669    1.949099    2.206166    2.252829    2.411071
     2.561047    3.521746    3.543161    3.567498    3.915095    4.291072    4.397335    4.889527    5.246563    5.682065
     5.913520    6.434998
 !timer: motran                          cpu_time=     0.002 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.175 walltime=     0.175

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc= -114.0579688521 demc= 7.4106E-03 wnorm= 2.4500E-02 knorm= 2.4006E-01 apxde= 1.3035E-03    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 cpu_time=     0.609 walltime=     0.609

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51863
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.030 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.008 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.010 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 ciiter=  20 noldhv=  6 noldv=  6
 !timer: hmc(*) diagonalization          cpu_time=     0.116 walltime=     0.117

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2172284170     -145.1225214377        0.0000007629        0.0000010000
    2      -114.0760292118     -144.9813222325        0.0000005459        0.0000010000
    3      -113.8879919091     -144.7932849299        0.0000006314        0.0000010000
    4      -113.8242802855     -144.7295733062        0.0055960662        0.0100000000
 !timer: hmcvec                          cpu_time=     0.118 walltime=     0.118
 !timer: rdft                            cpu_time=     0.013 walltime=     0.013
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.002 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.018 walltime=     0.018
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.902989752759947E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.96701818 pnorm= 0.0000E+00 rznorm= 4.9211E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.002 walltime=     0.002

 fdd(*) eigenvalues. symmetry block  1
   -41.259465  -22.595621

 qvv(*) eigenvalues. symmetry block  1
     0.677386    0.726491    1.412220    1.475422    1.674024    1.858220    1.907868    2.207006    2.269050    2.414253
     2.563203    3.525666    3.545262    3.569185    3.917847    4.288050    4.399750    4.890207    5.246426    5.684117
     5.914571    6.437776
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.180 walltime=     0.180

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc= -114.0604165126 demc= 2.4477E-03 wnorm= 1.5224E-02 knorm= 2.5471E-01 apxde= 1.4718E-03    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 cpu_time=     0.789 walltime=     0.790

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.029 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.007 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.009 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 ciiter=  20 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     0.121 walltime=     0.121

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2207092155     -145.1260022362        0.0000005227        0.0000010000
    2      -114.0781890806     -144.9834821013        0.0000004379        0.0000010000
    3      -113.8908408756     -144.7961338964        0.0000008052        0.0000010000
    4      -113.8242628052     -144.7295558260        0.0049225310        0.0100000000
 !timer: hmcvec                          cpu_time=     0.122 walltime=     0.122
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.083 walltime=     0.083
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.001 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.002 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.096 walltime=     0.096
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.695062873920759E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 Total number of micro iterations:   19

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.91514190 pnorm= 1.1032E+00 rznorm= 6.6355E-06 rpnorm= 3.8732E-06 noldr= 16 nnewr= 16 nolds= 18 nnews= 18
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.245 walltime=     0.248
 !timer: solvek total                    cpu_time=     0.308 walltime=     0.308

 fdd(*) eigenvalues. symmetry block  1
   -41.255988  -22.593489

 qvv(*) eigenvalues. symmetry block  1
     0.644825    0.723946    1.386496    1.476240    1.674475    1.825995    1.915121    2.207145    2.295068    2.415814
     2.562627    3.527768    3.546546    3.570171    3.919320    4.278384    4.401091    4.893986    5.242935    5.685129
     5.914562    6.439501
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.568 walltime=     0.568

 all mcscf convergence criteria are not satisfied.
 iter=    5 emc= -114.0632463906 demc= 2.8299E-03 wnorm= 1.3561E-02 knorm= 2.7074E-01 apxde= 1.9594E-03    *not converged* 

               starting mcscf iteration...   6
 !timer:                                 cpu_time=     1.357 walltime=     1.358

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51863
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.030 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.007 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.009 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 ciiter=  24 noldhv= 15 noldv= 15
 !timer: hmc(*) diagonalization          cpu_time=     0.138 walltime=     0.139

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2242694628     -145.1295624835        0.0000005277        0.0000010000
    2      -114.0789998990     -144.9842929197        0.0000003029        0.0000010000
    3      -113.8918250796     -144.7971181004        0.0000004590        0.0000010000
    4      -113.8249396573     -144.7302326780        0.0042362393        0.0100000000
 !timer: hmcvec                          cpu_time=     0.140 walltime=     0.139
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.082 walltime=     0.082
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.002 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.097 walltime=     0.098
 
  tol(10)=  0.000000000000000E+000  eshsci=  9.016140599398179E-004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 Total number of micro iterations:   20

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.29354641 pnorm= 4.9513E+00 rznorm= 1.6718E-06 rpnorm= 3.0739E-06 noldr= 19 nnewr= 19 nolds= 19 nnews= 19
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.259 walltime=     0.262
 !timer: solvek total                    cpu_time=     0.326 walltime=     0.326

 fdd(*) eigenvalues. symmetry block  1
   -41.253066  -22.591967

 qvv(*) eigenvalues. symmetry block  1
     0.603531    0.719990    1.373296    1.477212    1.675157    1.775627    1.966355    2.207192    2.321389    2.417313
     2.560175    3.527344    3.547986    3.571098    3.920837    4.287870    4.402520    4.897756    5.245268    5.686061
     5.917101    6.440886
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.605 walltime=     0.605

 all mcscf convergence criteria are not satisfied.
 iter=    6 emc= -114.0650314805 demc= 1.7851E-03 wnorm= 7.2129E-03 knorm= 1.8925E-01 apxde= 5.0282E-03    *not converged* 

               starting mcscf iteration...   7
 !timer:                                 cpu_time=     1.962 walltime=     1.963

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51864
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.030 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.007 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.009 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 ciiter=  22 noldhv=  4 noldv=  4
 !timer: hmc(*) diagonalization          cpu_time=     0.137 walltime=     0.137

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2271606233     -145.1324536441        0.0000002192        0.0000010000
    2      -114.0792594317     -144.9845524525        0.0000004283        0.0000010000
    3      -113.8912143444     -144.7965073651        0.0000005095        0.0000010000
    4      -113.8267435996     -144.7320366204        0.0081444023        0.0100000000
 !timer: hmcvec                          cpu_time=     0.138 walltime=     0.137
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.001 walltime=     0.000
 !timer: rdft                            cpu_time=     0.082 walltime=     0.082
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.002 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.097 walltime=     0.097
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.284370771482987E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 Total number of micro iterations:   22

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.27969402 pnorm= 2.4809E+01 rznorm= 3.4776E-06 rpnorm= 7.3869E-06 noldr= 21 nnewr= 21 nolds= 21 nnews= 21
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.291 walltime=     0.290
 !timer: solvek total                    cpu_time=     0.360 walltime=     0.361

 fdd(*) eigenvalues. symmetry block  1
   -41.252190  -22.592334

 qvv(*) eigenvalues. symmetry block  1
     0.590992    0.711124    1.308003    1.477428    1.675523    1.756441    2.019035    2.206973    2.331860    2.417523
     2.560473    3.525655    3.548506    3.571339    3.921410    4.295721    4.402902    4.896863    5.251258    5.686302
     5.916109    6.439865
 !timer: motran                          cpu_time=     0.002 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.637 walltime=     0.638

 all mcscf convergence criteria are not satisfied.
 iter=    7 emc= -114.0658781331 demc= 8.4665E-04 wnorm= 1.0275E-02 knorm= 3.8667E-02 apxde= 1.0563E-02    *not converged* 

               starting mcscf iteration...   8
 !timer:                                 cpu_time=     2.599 walltime=     2.600

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.030 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.008 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.010 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 ciiter=  26 noldhv=  8 noldv=  8
 !timer: hmc(*) diagonalization          cpu_time=     0.153 walltime=     0.153

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2279161177     -145.1332091384        0.0000005226        0.0000010000
    2      -114.0794355376     -144.9847285583        0.0000007581        0.0000010000
    3      -113.8910429790     -144.7963359997        0.0000005011        0.0000010000
    4      -113.8272052893     -144.7324983101        0.0045436540        0.0100000000
 !timer: hmcvec                          cpu_time=     0.154 walltime=     0.154
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.082 walltime=     0.082
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.002 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.097 walltime=     0.097
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.339625320035073E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 Total number of micro iterations:   21

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.30741237 pnorm= 2.0175E+02 rznorm= 4.2229E-06 rpnorm= 8.3227E-06 noldr= 20 nnewr= 20 nolds= 20 nnews= 20
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.277 walltime=     0.275
 !timer: solvek total                    cpu_time=     0.342 walltime=     0.342

 fdd(*) eigenvalues. symmetry block  1
   -41.251982  -22.592150

 qvv(*) eigenvalues. symmetry block  1
     0.588889    0.709386    1.294226    1.477554    1.675594    1.752817    2.028578    2.206946    2.333613    2.417493
     2.560586    3.525067    3.548625    3.571413    3.921383    4.296434    4.402956    4.896778    5.251711    5.686356
     5.915789    6.439401
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.636 walltime=     0.636

 all mcscf convergence criteria are not satisfied.
 iter=    8 emc= -114.0661315448 demc= 2.5341E-04 wnorm= 1.0717E-02 knorm= 4.7165E-03 apxde= 1.0716E-02    *not converged* 

               starting mcscf iteration...   9
 !timer:                                 cpu_time=     3.235 walltime=     3.236

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51864
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.030 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.008 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.010 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 ciiter=  24 noldhv=  4 noldv=  4
 !timer: hmc(*) diagonalization          cpu_time=     0.144 walltime=     0.144

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2280112818     -145.1333043026        0.0000003830        0.0000010000
    2      -114.0794604908     -144.9847535115        0.0000005638        0.0000010000
    3      -113.8910215176     -144.7963145383        0.0000004783        0.0000010000
    4      -113.8272359333     -144.7325289540        0.0060364066        0.0100000000
 !timer: hmcvec                          cpu_time=     0.145 walltime=     0.145
 !timer: cadu                            cpu_time=     0.001 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.083 walltime=     0.083
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.002 walltime=     0.002
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.098 walltime=     0.098
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.346287825529606E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 Total number of micro iterations:   19

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.31132279 pnorm= 4.7567E+00 rznorm= 9.8336E-06 rpnorm= 8.2506E-06 noldr= 17 nnewr= 17 nolds= 18 nnews= 18
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.248 walltime=     0.248
 !timer: solvek total                    cpu_time=     0.309 walltime=     0.309

 fdd(*) eigenvalues. symmetry block  1
   -41.251957  -22.592123

 qvv(*) eigenvalues. symmetry block  1
     0.588641    0.709175    1.292530    1.477571    1.675603    1.752385    2.029709    2.206944    2.333816    2.417489
     2.560605    3.524988    3.548640    3.571423    3.921377    4.296501    4.402963    4.896774    5.251721    5.686363
     5.915755    6.439338
 !timer: motran                          cpu_time=     0.002 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.594 walltime=     0.594

 all mcscf convergence criteria are not satisfied.
 iter=    9 emc= -114.0661644301 demc= 3.2885E-05 wnorm= 1.0770E-02 knorm= 1.9551E-01 apxde= 1.0708E-02    *not converged* 

               starting mcscf iteration...  10
 !timer:                                 cpu_time=     3.828 walltime=     3.830

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.031 walltime=     0.030

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.007 walltime=     0.008
 !timer: mosrt2                          cpu_time=     0.002 walltime=     0.002
 !timer: mosort                          cpu_time=     0.009 walltime=     0.010
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.004
 ciiter=  23 noldhv=  8 noldv=  8
 !timer: hmc(*) diagonalization          cpu_time=     0.142 walltime=     0.143

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2323191814     -145.1376122022        0.0000002545        0.0000010000
    2      -114.0810669168     -144.9863599375        0.0000005843        0.0000010000
    3      -113.8901797566     -144.7954727774        0.0000007086        0.0000010000
    4      -113.8295734630     -144.7348664837        0.0042466765        0.0100000000
 !timer: hmcvec                          cpu_time=     0.144 walltime=     0.143
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.001 walltime=     0.000
 !timer: rdft                            cpu_time=     0.112 walltime=     0.114
 !timer: mqva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.001 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.136 walltime=     0.138
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.533599295852507E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.019
 !timer: hmcxv                           cpu_time=     0.006 walltime=     0.006
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.004
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.005 walltime=     0.005
 !timer: hmcxv                           cpu_time=     0.004 walltime=     0.005
 performing all-state projection
 Total number of micro iterations:   20

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.62466003 pnorm= 3.5609E+00 rznorm= 2.1130E-06 rpnorm= 4.0089E-06 noldr= 19 nnewr= 19 nolds= 19 nnews= 19
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.372 walltime=     0.389
 !timer: solvek total                    cpu_time=     0.463 walltime=     0.660

 fdd(*) eigenvalues. symmetry block  1
   -41.250925  -22.590260

 qvv(*) eigenvalues. symmetry block  1
     0.576482    0.700795    1.220653    1.478483    1.676037    1.737024    2.069406    2.207131    2.340870    2.417167
     2.561575    3.521215    3.549321    3.571922    3.920618    4.296763    4.403184    4.896722    5.242643    5.686688
     5.914156    6.436207
 !timer: motran                          cpu_time=     0.003 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.787 walltime=     0.984

 all mcscf convergence criteria are not satisfied.
 iter=   10 emc= -114.0678552850 demc= 1.6909E-03 wnorm= 1.2269E-02 knorm= 2.1113E-01 apxde= 5.9232E-03    *not converged* 

               starting mcscf iteration...  11
 !timer:                                 cpu_time=     4.615 walltime=     4.815

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51863
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.047 walltime=     0.053

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.013 walltime=     0.013
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.016 walltime=     0.016
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 ciiter=  21 noldhv=  6 noldv=  6
 !timer: hmc(*) diagonalization          cpu_time=     0.250 walltime=     0.251

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2366081016     -145.1419011224        0.0000003310        0.0000010000
    2      -114.0834467062     -144.9887397269        0.0000003077        0.0000010000
    3      -113.8893356347     -144.7946286554        0.0000004683        0.0000010000
    4      -113.8316631874     -144.7369562082        0.0064593408        0.0100000000
 !timer: hmcvec                          cpu_time=     0.251 walltime=     0.252
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.158 walltime=     0.158
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.001 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.004 walltime=     0.004
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.181 walltime=     0.181
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.365529821511476E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 Total number of micro iterations:   17

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.93412763 pnorm= 1.0166E+00 rznorm= 5.0846E-06 rpnorm= 9.6421E-06 noldr= 17 nnewr= 17 nolds= 16 nnews= 16
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.435 walltime=     0.437
 !timer: solvek total                    cpu_time=     0.521 walltime=     0.521

 fdd(*) eigenvalues. symmetry block  1
   -41.248991  -22.586870

 qvv(*) eigenvalues. symmetry block  1
     0.562357    0.692738    1.144965    1.479950    1.676522    1.724774    2.098669    2.208195    2.344347    2.417104
     2.564636    3.515641    3.550422    3.572879    3.919923    4.295990    4.403711    4.898626    5.210069    5.687412
     5.915525    6.431507
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     1.020 walltime=     1.027

 all mcscf convergence criteria are not satisfied.
 iter=   11 emc= -114.0697968142 demc= 1.9415E-03 wnorm= 1.0924E-02 knorm= 2.5031E-01 apxde= 1.4860E-03    *not converged* 

               starting mcscf iteration...  12
 !timer:                                 cpu_time=     5.635 walltime=     5.842

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51862
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.049 walltime=     0.049

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.013 walltime=     0.013
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.016 walltime=     0.016
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.015
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 ciiter=  24 noldhv= 13 noldv= 13
 !timer: hmc(*) diagonalization          cpu_time=     0.299 walltime=     0.299

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2394044557     -145.1446974765        0.0000003094        0.0000010000
    2      -114.0852951426     -144.9905881633        0.0000007841        0.0000010000
    3      -113.8883822471     -144.7936752678        0.0000007896        0.0000010000
    4      -113.8330892872     -144.7383823079        0.0077756767        0.0100000000
 !timer: hmcvec                          cpu_time=     0.299 walltime=     0.300
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.157 walltime=     0.157
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.002 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.183 walltime=     0.182
 
  tol(10)=  0.000000000000000E+000  eshsci=  9.531150415594758E-004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 Total number of micro iterations:   16

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99871590 pnorm= 1.2590E-01 rznorm= 5.1946E-06 rpnorm= 3.2714E-06 noldr= 14 nnewr= 14 nolds= 15 nnews= 15
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.406 walltime=     0.407
 !timer: solvek total                    cpu_time=     0.481 walltime=     0.481

 fdd(*) eigenvalues. symmetry block  1
   -41.247258  -22.583270

 qvv(*) eigenvalues. symmetry block  1
     0.543187    0.687342    1.073825    1.481756    1.676753    1.715344    2.118256    2.209606    2.344634    2.417188
     2.567626    3.507853    3.551468    3.573827    3.920318    4.298492    4.404273    4.901210    5.156445    5.688194
     5.918905    6.425806
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     1.033 walltime=     1.032

 all mcscf convergence criteria are not satisfied.
 iter=   12 emc= -114.0710272818 demc= 1.2305E-03 wnorm= 7.6249E-03 knorm= 5.0264E-02 apxde= 4.7023E-05    *not converged* 

               starting mcscf iteration...  13
 !timer:                                 cpu_time=     6.668 walltime=     6.874

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51864
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.049 walltime=     0.049

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.013 walltime=     0.013
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.016 walltime=     0.016
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.015
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.015
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 ciiter=  21 noldhv=  5 noldv=  5
 !timer: hmc(*) diagonalization          cpu_time=     0.276 walltime=     0.276

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2395246093     -145.1448176301        0.0000008606        0.0000010000
    2      -114.0854014035     -144.9906944242        0.0000004428        0.0000010000
    3      -113.8883008753     -144.7935938960        0.0000004015        0.0000010000
    4      -113.8332923435     -144.7385853642        0.0066187796        0.0100000000
 !timer: hmcvec                          cpu_time=     0.277 walltime=     0.277
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.156 walltime=     0.156
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.001 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.004 walltime=     0.004
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.180 walltime=     0.180
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.226744550395971E-005
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   15

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999705 pnorm= 5.7228E-03 rznorm= 1.5271E-07 rpnorm= 7.4791E-07 noldr= 13 nnewr= 13 nolds= 13 nnews= 13
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.354 walltime=     0.353
 !timer: solvek total                    cpu_time=     0.427 walltime=     0.427

 fdd(*) eigenvalues. symmetry block  1
   -41.247499  -22.582739

 qvv(*) eigenvalues. symmetry block  1
     0.539778    0.687139    1.064691    1.482102    1.676579    1.714321    2.120107    2.209693    2.344651    2.417111
     2.567249    3.506355    3.551345    3.573746    3.920736    4.298893    4.404166    4.900988    5.151800    5.688114
     5.919174    6.425769
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.953 walltime=     0.953

 all mcscf convergence criteria are not satisfied.
 iter=   13 emc= -114.0710756294 demc= 4.8348E-05 wnorm= 3.3814E-04 knorm= 2.4293E-03 apxde= 8.7925E-08    *not converged* 

               starting mcscf iteration...  14
 !timer:                                 cpu_time=     7.621 walltime=     7.827

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51863
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.049 walltime=     0.049

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.013 walltime=     0.013
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.016 walltime=     0.016
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.015
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  21 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     0.259 walltime=     0.260

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2395235932     -145.1448166140        0.0000003601        0.0000010000
    2      -114.0854014965     -144.9906945172        0.0000006252        0.0000010000
    3      -113.8883020627     -144.7935950834        0.0000008789        0.0000010000
    4      -113.8333503769     -144.7386433976        0.0073067029        0.0100000000
 !timer: hmcvec                          cpu_time=     0.260 walltime=     0.261
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.001 walltime=     0.000
 !timer: rdft                            cpu_time=     0.183 walltime=     0.183
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.002 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.206 walltime=     0.206
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.084804278027902E-007
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 1.5940E-07 rpnorm= 4.6352E-07 noldr=  2 nnewr=  2 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.007 walltime=     0.007

 fdd(*) eigenvalues. symmetry block  1
   -41.247501  -22.582652

 qvv(*) eigenvalues. symmetry block  1
     0.539711    0.687136    1.064572    1.482113    1.676572    1.714309    2.120126    2.209697    2.344659    2.417114
     2.567216    3.506338    3.551346    3.573749    3.920753    4.298893    4.404167    4.900979    5.151834    5.688116
     5.919179    6.425840
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.541 walltime=     0.543

 all mcscf convergence criteria are not satisfied.
 iter=   14 emc= -114.0710757175 demc= 8.8104E-08 wnorm= 8.6784E-07 knorm= 2.5606E-06 apxde= 3.2083E-13    *not converged* 

               starting mcscf iteration...  15
 !timer:                                 cpu_time=     8.162 walltime=     8.370

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.047 walltime=     0.048

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.012 walltime=     0.013
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.015 walltime=     0.016
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.001

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  23 noldhv= 16 noldv= 16
 !timer: hmc(*) diagonalization          cpu_time=     0.272 walltime=     0.271

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2395235871     -145.1448166078        0.0000004189        0.0000010000
    2      -114.0854014958     -144.9906945165        0.0000004562        0.0000010000
    3      -113.8883020695     -144.7935950903        0.0000008470        0.0000010000
    4      -113.8333039829     -144.7385970036        0.0073615950        0.0100000000
 !timer: hmcvec                          cpu_time=     0.273 walltime=     0.272
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.153 walltime=     0.153
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.175 walltime=     0.175
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.316507399847888E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 3.9181E-07 rpnorm= 1.1900E-08 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.007 walltime=     0.007

 fdd(*) eigenvalues. symmetry block  1
   -41.247501  -22.582652

 qvv(*) eigenvalues. symmetry block  1
     0.539711    0.687136    1.064572    1.482113    1.676572    1.714309    2.120126    2.209697    2.344659    2.417114
     2.567216    3.506338    3.551346    3.573749    3.920753    4.298893    4.404167    4.900979    5.151834    5.688116
     5.919179    6.425841
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.523 walltime=     0.522

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=   15 emc= -114.0710757175 demc= 3.1264E-13 wnorm= 2.6532E-07 knorm= 3.7942E-08 apxde= 5.0070E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=   -114.239523587
   DRT #1 state # 2 weight 0.333333 total energy=   -114.085401496
   DRT #1 state # 3 weight 0.333333 total energy=   -113.888302070
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  A   block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1O1s       0.99671607    -0.00116684    -0.00182944     0.07743545     0.19969747    -0.00000112     0.00057380     0.06815124
   2O1s       0.02050311     0.00261190     0.00257566    -0.17166495    -0.46125358     0.00000287    -0.00391133    -0.15786454
   3O1s      -0.01216454     0.00033632     0.00090157    -0.10573539    -0.47596612     0.00000466    -0.00054809    -0.28630796
   4O1px     -0.00006382     0.00021743    -0.18223210    -0.00847265     0.00880599     0.11418996    -0.61748060    -0.04170321
   5O1py     -0.00105333     0.00151787     0.01835471     0.13116518     0.02885976     0.24497039     0.09354771    -0.48035568
   6O1pz     -0.00054171     0.00080992    -0.03330797     0.06363450     0.01648587    -0.48975620    -0.09717857    -0.24999285
   7O1px      0.00021663     0.00019183    -0.07912802    -0.00364663     0.00771369     0.07513381    -0.46669843    -0.01966036
   8O1py      0.00289687     0.00284542     0.00925233     0.04934403     0.00740218     0.16118155     0.07503485    -0.26643325
   9O1pz      0.00149943     0.00146791    -0.01382116     0.02383296     0.00549975    -0.32224455    -0.07128205    -0.13785361
  10O1d2-    -0.00001369    -0.00007890     0.01604917     0.00026322    -0.00012325    -0.00674946     0.01572146     0.00406190
  11O1d1-    -0.00012757    -0.00077499     0.00202148    -0.00851441    -0.00377217     0.01738098     0.00096779     0.01464507
  12O1d0      0.00001048     0.00007435     0.00053356     0.00068782     0.00042901     0.00618680     0.00036042    -0.00069571
  13O1d1+    -0.00000892    -0.00008476     0.00876037     0.00024543    -0.00044494    -0.00037784     0.00935519     0.00113191
  14O1d2+     0.00005994     0.00032960     0.00164601     0.00440828     0.00145999     0.00560236     0.00294831    -0.00781148
  15C1s       0.00027184     0.99884073    -0.01553325     0.17734105    -0.02289670     0.00000312     0.00099195    -0.03642852
  16C1s      -0.00015170     0.01871247     0.03516078    -0.37942087     0.07897132    -0.00000727    -0.00122121     0.09081208
  17C1s       0.00519155    -0.01355259     0.03651168    -0.29220478     0.18586282    -0.00000494    -0.00045944    -0.03117876
  18C1px     -0.00000253     0.00067280    -0.43288977    -0.03538653    -0.00274307     0.05770109     0.10693007     0.01652855
  19C1py     -0.00095036     0.00460547     0.04833927     0.04719749    -0.22585683     0.12378068    -0.02060584     0.30626485
  20C1pz     -0.00047594     0.00246042    -0.07675260     0.01536286    -0.11360946    -0.24748313     0.01462437     0.15703651
  21C1px      0.00013597     0.00033605    -0.17644895    -0.01149811    -0.00049718     0.03173989     0.05785204    -0.00114830
  22C1py      0.00248533     0.00298801     0.01568456     0.06563094    -0.03093958     0.06809174    -0.00204076     0.05586851
  23C1pz      0.00127478     0.00157297    -0.03329560     0.03014950    -0.01559259    -0.13613474     0.01246765     0.02767565
  24C1d2-    -0.00008694     0.00011511     0.01801469     0.00517293    -0.00625566     0.00607271    -0.04244624     0.00348127
  25C1d1-    -0.00044120     0.00083347     0.00259048    -0.01591471    -0.01589749    -0.01708962    -0.00391704     0.01647266
  26C1d0      0.00001223    -0.00004326     0.00035503     0.00448612    -0.00116131    -0.00591952    -0.00180647     0.00014168
  27C1d1+     0.00001751    -0.00000339     0.01051637    -0.00638983     0.00352690     0.00134541    -0.02230235    -0.00237226
  28C1d2+     0.00027579    -0.00046855     0.00241783    -0.00106849     0.01435392    -0.00558960    -0.00413759    -0.01223899
  29H1s      -0.00005587    -0.00371640     0.24038803    -0.13841769     0.09762558    -0.00000027    -0.12588018    -0.07376147
  30H1s       0.00016094     0.00119441     0.16606775    -0.03768538     0.04593462     0.00000022    -0.14664986    -0.06809974
  31H2s       0.00000251    -0.00335488    -0.20681621    -0.17847724     0.10896774    -0.00000030     0.12954027    -0.07191937
  32H2s       0.00022807     0.00143798    -0.16067933    -0.06876322     0.05785701     0.00000050     0.15909458    -0.06099526

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
   1O1s       0.00000011    -0.08170074    -0.02166807     0.00201559     0.05765703     0.00000009    -0.00037767     0.02755324
   2O1s      -0.00000042     0.37018619     0.02523771    -0.00351494     0.03817394    -0.00000087    -0.00137845    -0.16378656
   3O1s      -0.00000003     0.38142905     0.49530945    -0.03818686    -1.88490358     0.00000145     0.01342679    -0.24358633
   4O1px      0.08125274    -0.04110050    -0.00884586    -0.11160882    -0.00070631     0.01146723     0.34300753     0.00609386
   5O1py      0.17431003    -0.67802875    -0.00632158     0.01336891    -0.16435520     0.02460205    -0.04800900    -0.09943845
   6O1pz     -0.34849285    -0.34871529    -0.00522169    -0.01933568    -0.08237074    -0.04918541     0.05595987    -0.04831027
   7O1px      0.07741145    -0.00368057    -0.02178800    -0.38222814     0.03863552     0.03106179     0.20205455     0.01404307
   8O1py      0.16606716    -0.09341660    -0.11558547     0.06869929     0.74617887     0.06663749    -0.03574022     0.28003990
   9O1pz     -0.33201791    -0.04758170    -0.06288417    -0.05475661     0.38222465    -0.13323115     0.02923347     0.14334358
  10O1d2-    -0.00020231    -0.00184497     0.00187488    -0.00893932    -0.01112168    -0.01742200    -0.02040714     0.00070013
  11O1d1-    -0.00069201    -0.04451755     0.00164124    -0.00160569    -0.09908728     0.04986652    -0.00074711    -0.05574595
  12O1d0     -0.00010974     0.00542581     0.00105225    -0.00047869     0.00762175     0.01718674    -0.00109282     0.00898149
  13O1d1+     0.00082957    -0.00665964    -0.00245109    -0.00418403    -0.00562890    -0.00444117    -0.01031601    -0.01314654
  14O1d2+    -0.00029128     0.01588444    -0.00423550     0.00006835     0.04815993     0.01635370    -0.00212016     0.01347399
  15C1s      -0.00000254     0.12323479    -0.10232819     0.00401719    -0.02249517     0.00000087     0.00102329    -0.03394754
  16C1s       0.00000583    -0.53855262     0.10597565    -0.00807576    -0.24853360    -0.00000031     0.02260549    -1.08381901
  17C1s       0.00001440    -0.39868758     1.59827459    -0.04229749     1.96696140    -0.00001518    -0.05786447     2.38827779
  18C1px     -0.11791395    -0.03611174     0.00598734     0.41120640    -0.02635855     0.20579807     0.72091565     0.02450925
  19C1py     -0.25295625    -0.62037868    -0.22504301    -0.04909775    -0.36793037     0.44149394    -0.10506368     0.12613255
  20C1pz      0.50573095    -0.31871168    -0.11117189     0.07131762    -0.19017460    -0.88268005     0.11553108     0.06879461
  21C1px     -0.11051501    -0.00796568     0.00858604     1.59923016     0.09146790    -0.24288954    -2.21152991    -0.06879409
  22C1py     -0.23708480    -0.16739036    -1.02312630    -0.16996901     1.70083275    -0.52107017     0.32066891    -0.41918484
  23C1pz      0.47399919    -0.08557476    -0.50976239     0.28785337     0.87202691     1.04179624    -0.35523234    -0.22571728
  24C1d2-     0.00413045    -0.01232582     0.00015621     0.00051901     0.00248370     0.00493968    -0.05228033     0.03909711
  25C1d1-    -0.01211050     0.03152334     0.00356785     0.00071494     0.08051366    -0.01386871    -0.00403958    -0.02483900
  26C1d0     -0.00414455    -0.01229063     0.00007185    -0.00015972    -0.01078459    -0.00480752    -0.00275151     0.02673844
  27C1d1+     0.00125215     0.02192492    -0.00105085     0.00066890     0.01451050     0.00107238    -0.02602645    -0.05073750
  28C1d2+    -0.00398598     0.01292727    -0.00297542     0.00014215    -0.02572682    -0.00453392    -0.00393666    -0.05840077
  29H1s      -0.00000253     0.08553950    -0.03493470    -0.02044245     0.09167294     0.00000664    -0.35723684    -0.60144747
  30H1s      -0.00000509     0.09965609    -1.38409649     1.86614133    -0.22143182     0.00001121    -0.86973712    -0.34397777
  31H2s      -0.00000283     0.08469033    -0.02644967     0.02687632     0.08857285     0.00000387     0.38485662    -0.59181373
  32H2s      -0.00000499     0.09689539    -1.54251818    -1.75078856    -0.19316754     0.00000474     0.88603457    -0.30355951

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
   1O1s      -0.07199885    -0.00028522    -0.01513677    -0.00000039     0.00234230    -0.03345863    -0.00000040     0.00000024
   2O1s       0.30988374    -0.00128695     0.12065705     0.00000178     0.00372897     1.62400339     0.00001638    -0.00001207
   3O1s       0.45092830     0.01591058    -0.20713555     0.00000082    -0.05794705    -3.59060678    -0.00003534     0.00002662
   4O1px      0.00276044    -0.11284747    -0.01470123     0.19110381    -0.84499214     0.00526029    -0.02957021    -0.00105015
   5O1py      0.50942983     0.01730748     0.15813831     0.40995209     0.09960528    -0.05146608    -0.06343546    -0.00225897
   6O1pz      0.25545008    -0.01765500     0.07568226    -0.81961475    -0.14720200    -0.02451576     0.12682489     0.00451670
   7O1px     -0.03553418     0.05947804     0.02510734    -0.25684547     1.45812875     0.04951135    -0.01716716    -0.00130824
   8O1py     -1.36709543    -0.02039241    -0.19542515    -0.55097605    -0.13997125     1.13858184    -0.03681814    -0.00282860
   9O1pz     -0.69206984     0.00366899    -0.09191044     1.10155855     0.26997018     0.58102550     0.07364058     0.00563425
  10O1d2-     0.00246041    -0.13882539     0.00009364    -0.00783343     0.09652542     0.04314447    -0.10512540    -0.22336072
  11O1d1-    -0.02159834    -0.01190982    -0.01703973     0.02514530     0.00903850     0.10236222     0.33184312    -0.09049336
  12O1d0      0.00176216    -0.00614151     0.00343949     0.00839084     0.00481496     0.01156261     0.11123633     0.04271422
  13O1d1+     0.00179403    -0.07244344    -0.00628230    -0.00388701     0.04845964    -0.03357076    -0.04825064     0.44896012
  14O1d2+     0.01168648    -0.01344984     0.00174015     0.00838449     0.00692740    -0.10527878     0.11039516    -0.06660237
  15C1s      -0.02717941    -0.00157444     0.03458463    -0.00000009    -0.00010438    -0.00064635     0.00000012     0.00000002
  16C1s      -0.29526130     0.04553544    -1.50798885    -0.00002757     0.03807271     0.01527568    -0.00000409    -0.00000012
  17C1s      -1.11486940    -0.09662556     2.65193031     0.00004661     0.01173822     2.07706444     0.00003012    -0.00001509
  18C1px     -0.02909983     0.77529016     0.05578671    -0.00946735     0.32228105     0.01217147    -0.04572846    -0.00178364
  19C1py     -0.57942590    -0.12995874     0.45554729    -0.02029674    -0.03790862     0.21163676    -0.09809487    -0.00385734
  20C1pz     -0.29659999     0.11575994     0.24086850     0.04059591     0.05617984     0.10868912     0.19613005     0.00770661
  21C1px     -0.03405056    -1.07726003    -0.10944290     0.09812825    -1.62344888     0.07988244     0.02629639     0.00126647
  22C1py      0.07578019     0.16976339    -0.63484119     0.21046286     0.26747871     1.20840334     0.05641946     0.00275347
  23C1pz      0.02997354    -0.16625622    -0.34305896    -0.42082355    -0.24473151     0.62303070    -0.11277830    -0.00552519
  24C1d2-    -0.00654325     0.19030368    -0.03055480     0.00930570    -0.13493968     0.05632955     0.14428018    -0.30459567
  25C1d1-    -0.10992903     0.01634792    -0.01704707    -0.02582656    -0.00793627     0.07506402    -0.36876512    -0.16353475
  26C1d0      0.01536834     0.00847379    -0.01714845    -0.00898220    -0.00555384     0.02218136    -0.13157825     0.04846886
  27C1d1+    -0.02398599     0.09913481     0.03741637     0.00181249    -0.07268100    -0.05075637     0.00614884     0.63996830
  28C1d2+     0.03197348     0.01823582     0.06146798    -0.00842772    -0.01731333    -0.11559437    -0.11870089    -0.10601327
  29H1s      -0.19194228     0.69406582     0.57402024     0.00000565    -0.11682304    -0.04126531     0.00000124     0.00000493
  30H1s       0.35900243    -1.21244243    -1.34353143    -0.00001639    -0.71333220     0.02210503    -0.00000505    -0.00001509
  31H2s      -0.16819135    -0.72686727     0.53890232     0.00000430     0.09627926    -0.04359661     0.00000179    -0.00000420
  32H2s       0.37156202     1.29147161    -1.23216335    -0.00002938     0.75480345     0.01392404    -0.00000571     0.00001457

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
   1O1s       0.00265804     0.00016642    -0.00000002    -0.01294366    -0.03090679    -0.00000021     0.00011829     0.05587953
   2O1s      -0.30923102    -0.00074348    -0.00000092     0.05588427     0.50533409     0.00000341     0.00321746     0.71551718
   3O1s       0.76287629    -0.00117997     0.00000236     0.35172825     0.06573709     0.00000019    -0.01058162    -2.89238351
   4O1px      0.00210533    -0.26613302     0.00007033    -0.00997436    -0.02399010    -0.00136819    -0.04781069     0.01147764
   5O1py     -0.00225309     0.03758605     0.00004904    -0.17106391    -0.42938344    -0.00293780     0.00765908     0.20361309
   6O1pz     -0.00063221    -0.04325050    -0.00010318    -0.08788323    -0.22035801     0.00586673    -0.00731632     0.10451639
   7O1px     -0.02267045     0.50163793    -0.00070286     0.00044975     0.01150176     0.09415271    -0.35631153     0.09226955
   8O1py     -0.25608554    -0.07187128    -0.00129659    -0.03018860     0.21146747     0.20197950     0.05667178     1.54834288
   9O1pz     -0.13337584     0.08101125     0.00260223    -0.01500223     0.10845756    -0.40382232    -0.05473026     0.79594451
  10O1d2-     0.18794753    -0.63275630    -0.33809629    -0.11977840     0.03088585    -0.20587425     0.77374732    -0.11282623
  11O1d1-    -0.35014396    -0.05713852    -0.16026825     0.08485661    -0.11099042     0.58301337     0.06708625    -1.02287784
  12O1d0      0.16153919    -0.02661418     0.05901252    -0.08403727     0.03466262     0.20157436     0.03273148     0.08180086
  13O1d1+    -0.28719024    -0.33357818     0.69591970     0.15981359    -0.05871472    -0.04815218     0.40827961    -0.06654750
  14O1d2+    -0.21181917    -0.06370428    -0.10963927     0.17923020    -0.02044979     0.19088367     0.07945732     0.48727253
  15C1s       0.02427448    -0.00000229    -0.00000003     0.04720359     0.00920461    -0.00000031     0.00018667     0.03201891
  16C1s       0.17752948     0.00011323    -0.00000027     0.23966466     0.21029774     0.00000035     0.00526480     1.23846434
  17C1s      -1.04414799    -0.00390448    -0.00000002    -0.83486411    -0.07164032    -0.00000110     0.00407400     1.45474252
  18C1px     -0.00214662    -0.32404856    -0.00001736     0.00328567    -0.00202040    -0.04982239     0.07426764     0.06541040
  19C1py     -0.04099196     0.04559814    -0.00015982     0.04500783    -0.02431445    -0.10688297    -0.00578202     1.13686798
  20C1pz     -0.02099939    -0.05274618     0.00031260     0.02328219    -0.01263676     0.21368758     0.01442401     0.58387565
  21C1px      0.00549541    -0.85978321     0.00039761    -0.00243061    -0.02425063    -0.05474329     0.22894277     0.03590304
  22C1py     -0.06926540     0.12535931     0.00048826    -0.04467722    -0.40440916    -0.11743858    -0.02983838     0.68480539
  23C1pz     -0.03335462    -0.13776037    -0.00099871    -0.02290212    -0.20793502     0.23479349     0.03845482     0.35088803
  24C1d2-     0.08806434     0.41579309     0.25691421     0.17012705    -0.16979928    -0.19803054     0.97900135     0.03832102
  25C1d1-    -0.19853743     0.03574751     0.12057648    -0.59867631    -0.59223285     0.56195337     0.09006626     0.53852801
  26C1d0      0.08071382     0.01819804    -0.04513069     0.18837226    -0.01577831     0.19417275     0.04120072    -0.05294216
  27C1d1+    -0.14133578     0.21747286    -0.52793043    -0.31865551     0.08830118    -0.04712085     0.51592072     0.05163811
  28C1d2+    -0.08928281     0.04087813     0.08285400    -0.11385718     0.46197345     0.18405468     0.09721103    -0.22988938
  29H1s       0.26786682    -0.60599497     0.00010661     0.43057164    -0.32073747    -0.00000851    -0.39794996    -0.05762178
  30H1s       0.15549023    -0.15836179     0.00003152     0.03704644     0.01705914     0.00000408     0.27810206    -0.00393758
  31H2s       0.26327525     0.60933615    -0.00010762     0.42700337    -0.31759886    -0.00000934     0.39960635    -0.05945439
  32H2s       0.14812513     0.16239051    -0.00003232     0.03852814     0.01621349     0.00000451    -0.27823747     0.00046327

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     1.99663748     1.99543604     1.99304362     1.94064717     1.66950589     1.65929620
 
   1O1s       0.99671607    -0.00116684    -0.00182840     0.07743566     0.19969738    -0.00000110     0.00057373     0.06815129
   2O1s       0.02050311     0.00261190     0.00257335    -0.17166540    -0.46125337     0.00000282    -0.00391117    -0.15786466
   3O1s      -0.01216454     0.00033632     0.00089991    -0.10573584    -0.47596592     0.00000461    -0.00054776    -0.28630809
   4O1px     -0.00006382     0.00021743    -0.18223228    -0.00847063     0.00880615     0.11418960    -0.61748059    -0.04170390
   5O1py     -0.00105333     0.00151787     0.01835595     0.13116501     0.02885973     0.24497046     0.09354818    -0.48035561
   6O1pz     -0.00054171     0.00080992    -0.03330734     0.06363479     0.01648600    -0.48975624    -0.09717791    -0.24999299
   7O1px      0.00021663     0.00019183    -0.07912812    -0.00364575     0.00771376     0.07513354    -0.46669845    -0.01966091
   8O1py      0.00289687     0.00284542     0.00925275     0.04934394     0.00740218     0.16118160     0.07503508    -0.26643317
   9O1pz      0.00149943     0.00146791    -0.01382094     0.02383306     0.00549984    -0.32224458    -0.07128165    -0.13785370
  10O1d2-    -0.00001369    -0.00007890     0.01604918     0.00026304    -0.00012326    -0.00674945     0.01572146     0.00406191
  11O1d1-    -0.00012757    -0.00077499     0.00202139    -0.00851443    -0.00377217     0.01738098     0.00096776     0.01464507
  12O1d0      0.00001048     0.00007435     0.00053356     0.00068781     0.00042901     0.00618680     0.00036041    -0.00069570
  13O1d1+    -0.00000892    -0.00008476     0.00876038     0.00024534    -0.00044495    -0.00037784     0.00935518     0.00113192
  14O1d2+     0.00005994     0.00032960     0.00164606     0.00440827     0.00145999     0.00560236     0.00294832    -0.00781148
  15C1s       0.00027183     0.99884073    -0.01553132     0.17734120    -0.02289683     0.00000309     0.00099199    -0.03642853
  16C1s      -0.00015170     0.01871247     0.03515668    -0.37942118     0.07897159    -0.00000719    -0.00122130     0.09081209
  17C1s       0.00519155    -0.01355259     0.03650856    -0.29220502     0.18586305    -0.00000485    -0.00045938    -0.03117872
  18C1px     -0.00000253     0.00067280    -0.43289015    -0.03538172    -0.00274275     0.05770112     0.10693009     0.01652891
  19C1py     -0.00095036     0.00460547     0.04833977     0.04719682    -0.22585704     0.12378061    -0.02060626     0.30626471
  20C1pz     -0.00047594     0.00246042    -0.07675240     0.01536357    -0.11360944    -0.24748317     0.01462425     0.15703646
  21C1px      0.00013597     0.00033605    -0.17644908    -0.01149615    -0.00049706     0.03173991     0.05785206    -0.00114813
  22C1py      0.00248533     0.00298801     0.01568528     0.06563076    -0.03093969     0.06809172    -0.00204083     0.05586849
  23C1pz      0.00127478     0.00157297    -0.03329524     0.03014982    -0.01559259    -0.13613475     0.01246763     0.02767563
  24C1d2-    -0.00008694     0.00011511     0.01801474     0.00517273    -0.00625568     0.00607268    -0.04244625     0.00348121
  25C1d1-    -0.00044120     0.00083347     0.00259030    -0.01591475    -0.01589748    -0.01708962    -0.00391705     0.01647265
  26C1d0      0.00001223    -0.00004326     0.00035508     0.00448611    -0.00116132    -0.00591952    -0.00180646     0.00014168
  27C1d1+     0.00001751    -0.00000339     0.01051630    -0.00638995     0.00352690     0.00134540    -0.02230235    -0.00237229
  28C1d2+     0.00027579    -0.00046855     0.00241782    -0.00106850     0.01435393    -0.00558960    -0.00413757    -0.01223899
  29H1s      -0.00005587    -0.00371640     0.24038651    -0.13842028     0.09762556    -0.00000027    -0.12588011    -0.07376170
  30H1s       0.00016094     0.00119441     0.16606732    -0.03768719     0.04593457     0.00000017    -0.14664980    -0.06809998
  31H2s       0.00000251    -0.00335488    -0.20681813    -0.17847486     0.10896806    -0.00000019     0.12954039    -0.07191905
  32H2s       0.00022807     0.00143798    -0.16068007    -0.06876139     0.05785720     0.00000060     0.15909468    -0.06099494

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.72672062     0.01871298     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 
   1O1s       0.00000012    -0.08170075    -0.02166806     0.00201573     0.05765703     0.00000009    -0.00037769     0.02755325
   2O1s      -0.00000043     0.37018621     0.02523773    -0.00351474     0.03817399    -0.00000087    -0.00137827    -0.16378656
   3O1s      -0.00000004     0.38142908     0.49530922    -0.03819138    -1.88490374     0.00000159     0.01342648    -0.24358641
   4O1px      0.08125266    -0.04110053    -0.00884602    -0.11160883    -0.00070656     0.01146722     0.34300755     0.00609425
   5O1py      0.17431000    -0.67802872    -0.00632159     0.01336838    -0.16435514     0.02460205    -0.04800878    -0.09943851
   6O1pz     -0.34849289    -0.34871527    -0.00522173    -0.01933594    -0.08237079    -0.04918543     0.05595998    -0.04831021
   7O1px      0.07741138    -0.00368059    -0.02178829    -0.38222802     0.03863667     0.03106180     0.20205450     0.01404353
   8O1py      0.16606715    -0.09341658    -0.11558532     0.06870124     0.74617880     0.06663746    -0.03574060     0.28003986
   9O1pz     -0.33201793    -0.04758169    -0.06288416    -0.05475562     0.38222489    -0.13323115     0.02923327     0.14334369
  10O1d2-    -0.00020230    -0.00184497     0.00187486    -0.00893933    -0.01112169    -0.01742200    -0.02040715     0.00070009
  11O1d1-    -0.00069201    -0.04451756     0.00164122    -0.00160583    -0.09908733     0.04986653    -0.00074712    -0.05574597
  12O1d0     -0.00010974     0.00542581     0.00105225    -0.00047867     0.00762176     0.01718674    -0.00109283     0.00898149
  13O1d1+     0.00082957    -0.00665964    -0.00245110    -0.00418404    -0.00562892    -0.00444117    -0.01031600    -0.01314656
  14O1d2+    -0.00029128     0.01588444    -0.00423550     0.00006841     0.04815995     0.01635370    -0.00212014     0.01347399
  15C1s      -0.00000253     0.12323479    -0.10232819     0.00401724    -0.02249516     0.00000088     0.00102332    -0.03394754
  16C1s       0.00000581    -0.53855263     0.10597559    -0.00807645    -0.24853373    -0.00000034     0.02260680    -1.08381906
  17C1s       0.00001438    -0.39868758     1.59827482    -0.04229444     1.96696183    -0.00001522    -0.05786705     2.38827795
  18C1px     -0.11791393    -0.03611173     0.00598774     0.41120635    -0.02635931     0.20579807     0.72091560     0.02451031
  19C1py     -0.25295625    -0.62037868    -0.22504306    -0.04909822    -0.36793005     0.44149399    -0.10506413     0.12613241
  20C1pz      0.50573095    -0.31871170    -0.11117183     0.07131735    -0.19017472    -0.88268002     0.11553082     0.06879485
  21C1px     -0.11051500    -0.00796567     0.00858747     1.59923033     0.09146479    -0.24288957    -2.21152975    -0.06879759
  22C1py     -0.23708480    -0.16739036    -1.02312632    -0.16996473     1.70083307    -0.52107034     0.32067024    -0.41918428
  23C1pz      0.47399919    -0.08557477    -0.50976206     0.28785557     0.87202646     1.04179615    -0.35523163    -0.22571790
  24C1d2-     0.00413044    -0.01232582     0.00015623     0.00051899     0.00248382     0.00493968    -0.05228036     0.03909706
  25C1d1-    -0.01211050     0.03152334     0.00356785     0.00071497     0.08051366    -0.01386872    -0.00403947    -0.02483900
  26C1d0     -0.00414456    -0.01229063     0.00007185    -0.00015974    -0.01078457    -0.00480751    -0.00275155     0.02673844
  27C1d1+     0.00125215     0.02192491    -0.00105084     0.00066894     0.01451053     0.00107237    -0.02602638    -0.05073752
  28C1d2+    -0.00398598     0.01292727    -0.00297542     0.00014218    -0.02572684    -0.00453392    -0.00393663    -0.05840078
  29H1s      -0.00000256     0.08553950    -0.03493466    -0.02044235     0.09167353     0.00000661    -0.35723598    -0.60144782
  30H1s      -0.00000511     0.09965609    -1.38409489     1.86614207    -0.22143594     0.00001119    -0.86973672    -0.34397952
  31H2s      -0.00000281     0.08469033    -0.02644966     0.02687640     0.08857260     0.00000382     0.38485746    -0.59181324
  32H2s      -0.00000497     0.09689539    -1.54251978    -1.75078762    -0.19316386     0.00000479     0.88603488    -0.30355793

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 
   1O1s      -0.07199884    -0.00028537    -0.01513681    -0.00000040     0.00234224    -0.03345864    -0.00000040     0.00000024
   2O1s       0.30988364    -0.00128621     0.12065732     0.00000179     0.00372945     1.62400339     0.00001628    -0.00001220
   3O1s       0.45092836     0.01591169    -0.20713525     0.00000090    -0.05794683    -3.59060663    -0.00003511     0.00002694
   4O1px      0.00276083    -0.11284748    -0.01470121     0.19110382    -0.84499213     0.00526047    -0.02957021    -0.00105015
   5O1py      0.50942972     0.01730824     0.15813846     0.40995211     0.09960542    -0.05146612    -0.06343546    -0.00225898
   6O1pz      0.25545015    -0.01765460     0.07568234    -0.81961474    -0.14720195    -0.02451573     0.12682490     0.00451671
   7O1px     -0.03553490     0.05947800     0.02510726    -0.25684550     1.45812873     0.04951116    -0.01716716    -0.00130824
   8O1py     -1.36709524    -0.02039473    -0.19542569    -0.55097612    -0.13997183     1.13858181    -0.03681821    -0.00282870
   9O1pz     -0.69206997     0.00366780    -0.09191073     1.10155851     0.26996992     0.58102545     0.07364053     0.00563420
  10O1d2-     0.00246060    -0.13882538     0.00009363    -0.00783343     0.09652544     0.04314442    -0.10512540    -0.22336074
  11O1d1-    -0.02159832    -0.01190975    -0.01703968     0.02514530     0.00903860     0.10236217     0.33184312    -0.09049335
  12O1d0      0.00176216    -0.00614151     0.00343949     0.00839084     0.00481495     0.01156263     0.11123633     0.04271422
  13O1d1+     0.00179414    -0.07244344    -0.00628231    -0.00388701     0.04845964    -0.03357077    -0.04825066     0.44896013
  14O1d2+     0.01168650    -0.01344987     0.00174012     0.00838448     0.00692735    -0.10527878     0.11039517    -0.06660235
  15C1s      -0.02717943    -0.00157447     0.03458462    -0.00000009    -0.00010437    -0.00064634     0.00000012     0.00000002
  16C1s      -0.29526094     0.04553536    -1.50798885    -0.00002758     0.03807267     0.01527586    -0.00000409    -0.00000013
  17C1s      -1.11486998    -0.09662854     2.65192948     0.00004652     0.01173740     2.07706398     0.00002999    -0.00001528
  18C1px     -0.02910125     0.77529011     0.05578678    -0.00946735     0.32228105     0.01217128    -0.04572846    -0.00178364
  19C1py     -0.57942585    -0.12995960     0.45554713    -0.02029676    -0.03790873     0.21163675    -0.09809489    -0.00385736
  20C1pz     -0.29660032     0.11575950     0.24086843     0.04059591     0.05617980     0.10868909     0.19613004     0.00770660
  21C1px     -0.03404749    -1.07726011    -0.10944293     0.09812827    -1.62344896     0.07988252     0.02629639     0.00126647
  22C1py      0.07577996     0.16976312    -0.63484139     0.21046283     0.26747839     1.20840335     0.05641937     0.00275335
  23C1pz      0.02997418    -0.16625638    -0.34305907    -0.42082358    -0.24473171     0.62303070    -0.11277834    -0.00552525
  24C1d2-    -0.00654351     0.19030364    -0.03055479     0.00930570    -0.13493971     0.05632961     0.14428019    -0.30459567
  25C1d1-    -0.10992902     0.01634761    -0.01704718    -0.02582657    -0.00793646     0.07506391    -0.36876512    -0.16353478
  26C1d0      0.01536832     0.00847382    -0.01714844    -0.00898220    -0.00555383     0.02218139    -0.13157825     0.04846885
  27C1d1+    -0.02398613     0.09913478     0.03741638     0.00181249    -0.07268100    -0.05075632     0.00614881     0.63996830
  28C1d2+     0.03197343     0.01823595     0.06146805    -0.00842772    -0.01731322    -0.11559439    -0.11870087    -0.10601326
  29H1s      -0.19194344     0.69406544     0.57402025     0.00000566    -0.11682315    -0.04126545     0.00000124     0.00000493
  30H1s       0.35900549    -1.21244152    -1.34353128    -0.00001638    -0.71333200     0.02210523    -0.00000505    -0.00001509
  31H2s      -0.16819054    -0.72686761     0.53890216     0.00000428     0.09627921    -0.04359645     0.00000179    -0.00000420
  32H2s       0.37155960     1.29147253    -1.23216305    -0.00002936     0.75480368     0.01392413    -0.00000571     0.00001457

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 
   1O1s       0.00265803     0.00016640    -0.00000002    -0.01294370    -0.03090676    -0.00000021     0.00011829     0.05587953
   2O1s      -0.30923116    -0.00074354    -0.00000092     0.05588470     0.50533417     0.00000340     0.00321725     0.71551704
   3O1s       0.76287693    -0.00117939     0.00000230     0.35172904     0.06573605     0.00000021    -0.01058117    -2.89238333
   4O1px      0.00210531    -0.26613302     0.00007033    -0.00997437    -0.02398980    -0.00136819    -0.04781069     0.01147755
   5O1py     -0.00225311     0.03758597     0.00004905    -0.17106442    -0.42938323    -0.00293779     0.00765914     0.20361319
   6O1pz     -0.00063223    -0.04325053    -0.00010317    -0.08788349    -0.22035783     0.00586673    -0.00731629     0.10451642
   7O1px     -0.02267045     0.50163791    -0.00070286     0.00044972     0.01150140     0.09415270    -0.35631154     0.09226958
   8O1py     -0.25608578    -0.07187148    -0.00129658    -0.03018867     0.21146789     0.20197949     0.05667150     1.54834275
   9O1pz     -0.13337596     0.08101115     0.00260224    -0.01500228     0.10845767    -0.40382232    -0.05473040     0.79594445
  10O1d2-     0.18794756    -0.63275629    -0.33809628    -0.11977834     0.03088622    -0.20587425     0.77374733    -0.11282618
  11O1d1-    -0.35014395    -0.05713852    -0.16026825     0.08485655    -0.11099066     0.58301337     0.06708639    -1.02287781
  12O1d0      0.16153919    -0.02661417     0.05901251    -0.08403723     0.03466274     0.20157436     0.03273147     0.08180086
  13O1d1+    -0.28719022    -0.33357820     0.69591970     0.15981352    -0.05871472    -0.04815218     0.40827963    -0.06654745
  14O1d2+    -0.21181918    -0.06370431    -0.10963927     0.17923014    -0.02044984     0.19088366     0.07945727     0.48727255
  15C1s       0.02427448    -0.00000229    -0.00000003     0.04720360     0.00920455    -0.00000031     0.00018666     0.03201892
  16C1s       0.17752945     0.00011325    -0.00000027     0.23966484     0.21029780     0.00000034     0.00526462     1.23846430
  17C1s      -1.04414839    -0.00390496     0.00000003    -0.83486475    -0.07163912    -0.00000111     0.00407372     1.45474235
  18C1px     -0.00214657    -0.32404857    -0.00001736     0.00328566    -0.00202052    -0.04982238     0.07426763     0.06541047
  19C1py     -0.04099203     0.04559808    -0.00015981     0.04500769    -0.02431432    -0.10688298    -0.00578215     1.13686800
  20C1pz     -0.02099941    -0.05274622     0.00031260     0.02328212    -0.01263671     0.21368758     0.01442395     0.58387568
  21C1px      0.00549542    -0.85978322     0.00039761    -0.00243063    -0.02425040    -0.05474329     0.22894276     0.03590305
  22C1py     -0.06926571     0.12535902     0.00048829    -0.04467798    -0.40440893    -0.11743858    -0.02983845     0.68480536
  23C1pz     -0.03335478    -0.13776051    -0.00099869    -0.02290250    -0.20793485     0.23479348     0.03845478     0.35088802
  24C1d2-     0.08806427     0.41579305     0.25691422     0.17012683    -0.16979941    -0.19803054     0.97900138     0.03832120
  25C1d1-    -0.19853753     0.03574733     0.12057650    -0.59867710    -0.59223196     0.56195338     0.09006630     0.53852809
  26C1d0      0.08071381     0.01819804    -0.04513069     0.18837225    -0.01577855     0.19417275     0.04120073    -0.05294214
  27C1d1+    -0.14133577     0.21747287    -0.52793044    -0.31865542     0.08830162    -0.04712086     0.51592070     0.05163814
  28C1d2+    -0.08928272     0.04087825     0.08285398    -0.11385659     0.46197357     0.18405467     0.09721098    -0.22988947
  29H1s       0.26786683    -0.60599501     0.00010662     0.43057126    -0.32073816    -0.00000850    -0.39794990    -0.05762168
  30H1s       0.15549025    -0.15836175     0.00003151     0.03704651     0.01705921     0.00000408     0.27810207    -0.00393753
  31H2s       0.26327515     0.60933611    -0.00010762     0.42700299    -0.31759942    -0.00000934     0.39960642    -0.05945423
  32H2s       0.14812513     0.16239056    -0.00003233     0.03852822     0.01621353     0.00000451    -0.27823745     0.00046319
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        810 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files reqcpu_time=     0.001 walltime=     0.001


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s       1.999098   0.000113   0.000103   0.222435   1.440334   0.000000
    O1_ p       0.000001  -0.001008   0.186153   0.138382   0.003381   1.448084
    O1_ d       0.000000  -0.000196   0.006517   0.003298   0.000295   0.008165
    C1_ s       0.000532   2.000883   0.011329   1.172230   0.012914   0.000000
    C1_ p       0.000441   0.000134   1.021641   0.018892   0.360384   0.471410
    C1_ d      -0.000082   0.000004   0.004293   0.006570   0.017245   0.012988
    H1_ s       0.000004   0.000006   0.446340   0.158899   0.069599   0.000000
    H2_ s       0.000006   0.000064   0.320261   0.274729   0.088891   0.000000
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       0.000013   0.185303   0.000000   0.000942   0.000000   0.000000
    O1_ p       1.444832   1.050441   0.170673   0.006373   0.000000   0.000000
    O1_ d      -0.000569   0.004579   0.000146   0.000421   0.000000   0.000000
    C1_ s       0.000005   0.020829   0.000000   0.004128   0.000000   0.000000
    C1_ p       0.022253   0.318035   0.553156   0.006263   0.000000   0.000000
    C1_ d       0.030047   0.006158   0.002745   0.000218   0.000000   0.000000
    H1_ s       0.082333   0.038053   0.000000   0.000186   0.000000   0.000000
    H2_ s       0.090591   0.035899   0.000000   0.000182   0.000000   0.000000
 
   ao class      13A        14A        15A        16A        17A        18A  
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A  


                        gross atomic populations
     ao           O1_        C1_        H1_        H2_
      s         3.848342   3.222850   0.795421   0.810623
      p         4.447311   2.772609   0.000000   0.000000
      d         0.022657   0.080187   0.000000   0.000000
    total       8.318309   6.075646   0.795421   0.810623
 

 Total number of electrons:   16.00000000

 !timer: mcscf                           cpu_time=     8.694 walltime=     8.902
