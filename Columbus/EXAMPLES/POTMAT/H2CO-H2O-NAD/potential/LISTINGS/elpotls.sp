****************************************************
******     Electrostatic Potential             *****
******         -------------                   *****
******     COLUMBUS 5.9.x                      *****
******     exptl. Version (Dalton, only)       *****
******         -------------                   *****
******        Thomas Mueller                   *****
******    Central Institute of Applied Math.   *****
******    Research Centre Juelich              *****
******    D-52425 Juelich, Germany             *****
****************************************************
 Input files:  daltaoin (basis set info)            
               daltcomm (dalton control file)       
               geom     (geometry data file)        
               elpotin  (namelist input)            
              nocoef   (natural orbitals either from
                         SCF, MCSCF or CI)          
 Output files: elpotls  (listing)                   
               elpot.cube  (potential, cube format) 
               elpotpts    (potential, debug format)
 Namelist input:                                    
 idgb [0]: Debug level                              
 specification of the coordinate frame for the      
 generation of the grid points                      
 initpt [0.0,0.0,0.0]: x,y,z coordinate of orign    
 incdir1 [0.0,0.0,0.0]: definition of axis 1        
 workmem adjustment via export WRKMEM !
 output of control variables:
 variable:        idbg =               0
 variable:    property =      hmat      
 variable:      output =      cubeformat
 variable:   blocksize =            1000
 Reading pointscharges from file potential.xyz
  #   charge       x          y            z
     1   -0.8000 -3.843193  2.877979  0.384996
     2    0.4000 -4.113380  4.501439  1.134059
     3    0.4000 -2.083945  2.464496  0.587911


  Atoms and basis sets
  --------------------

  Number of atom types:     3
  Total number of atoms:    4

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      27      14      [10s4p1d|3s2p1d]                       
  C  1        1       6      27      14      [10s4p1d|3s2p1d]                       
  H  1        1       1       4       2      [4s|2s]                                
  H  2        1       1       4       2      [4s|2s]                                
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      4      16      62      32

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 12


   1   O  1     x      1.1934168500
   2            y      0.8120720100
   3            z      0.5251322100

   4   C  1     x      1.0744907200
   5            y     -1.2509741300
   6            z     -0.5344655700

   7   H  1     x     -0.7075777100
   8            y     -1.9485835300
   9            z     -1.2989164800

  10   H  2     x      2.7446665700
  11            y     -2.4386235300
  12            z     -0.7391172400

  Nuclear repulsion energy :    0.000000000000


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1     5484.671700    0.0018  0.0000  0.0000
   seg. cont.  2      825.234950    0.0140  0.0000  0.0000
               3      188.046960    0.0684  0.0000  0.0000
               4       52.964500    0.2327  0.0000  0.0000
               5       16.897570    0.4702  0.0000  0.0000
               6        5.799635    0.3585  0.0000  0.0000
               7       15.539616    0.0000 -0.1108  0.0000
               8        3.599934    0.0000 -0.1480  0.0000
               9        1.013762    0.0000  1.1308  0.0000
              10        0.270006    0.0000  0.0000  1.0000

  O  1   2px  11       15.539616    0.0709  0.0000
   seg. cont. 12        3.599934    0.3398  0.0000
              13        1.013762    0.7272  0.0000
              14        0.270006    0.0000  1.0000

  O  1   2py  15       15.539616    0.0709  0.0000
   seg. cont. 16        3.599934    0.3398  0.0000
              17        1.013762    0.7272  0.0000
              18        0.270006    0.0000  1.0000

  O  1   2pz  19       15.539616    0.0709  0.0000
   seg. cont. 20        3.599934    0.3398  0.0000
              21        1.013762    0.7272  0.0000
              22        0.270006    0.0000  1.0000

  O  1   3d2- 23        0.800000    1.0000

  O  1   3d1- 24        0.800000    1.0000

  O  1   3d0  25        0.800000    1.0000

  O  1   3d1+ 26        0.800000    1.0000

  O  1   3d2+ 27        0.800000    1.0000

  C  1   1s   28     3047.524900    0.0018  0.0000  0.0000
   seg. cont. 29      457.369510    0.0140  0.0000  0.0000
              30      103.948690    0.0688  0.0000  0.0000
              31       29.210155    0.2322  0.0000  0.0000
              32        9.286663    0.4679  0.0000  0.0000
              33        3.163927    0.3623  0.0000  0.0000
              34        7.868272    0.0000 -0.1193  0.0000
              35        1.881288    0.0000 -0.1609  0.0000
              36        0.544249    0.0000  1.1435  0.0000
              37        0.168714    0.0000  0.0000  1.0000

  C  1   2px  38        7.868272    0.0690  0.0000
   seg. cont. 39        1.881288    0.3164  0.0000
              40        0.544249    0.7443  0.0000
              41        0.168714    0.0000  1.0000

  C  1   2py  42        7.868272    0.0690  0.0000
   seg. cont. 43        1.881288    0.3164  0.0000
              44        0.544249    0.7443  0.0000
              45        0.168714    0.0000  1.0000

  C  1   2pz  46        7.868272    0.0690  0.0000
   seg. cont. 47        1.881288    0.3164  0.0000
              48        0.544249    0.7443  0.0000
              49        0.168714    0.0000  1.0000

  C  1   3d2- 50        0.800000    1.0000

  C  1   3d1- 51        0.800000    1.0000

  C  1   3d0  52        0.800000    1.0000

  C  1   3d1+ 53        0.800000    1.0000

  C  1   3d2+ 54        0.800000    1.0000

  H  1   1s   55       18.731137    0.0335  0.0000
   seg. cont. 56        2.825394    0.2347  0.0000
              57        0.640122    0.8138  0.0000
              58        0.161278    0.0000  1.0000

  H  2   1s   59       18.731137    0.0335  0.0000
   seg. cont. 60        2.825394    0.2347  0.0000
              61        0.640122    0.8138  0.0000
              62        0.161278    0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6
   2  O  1    1s       7     8     9
   3  O  1    1s      10
   4  O  1    2px     11    12    13
   5  O  1    2py     15    16    17
   6  O  1    2pz     19    20    21
   7  O  1    2px     14
   8  O  1    2py     18
   9  O  1    2pz     22
  10  O  1    3d2-    23
  11  O  1    3d1-    24
  12  O  1    3d0     25
  13  O  1    3d1+    26
  14  O  1    3d2+    27
  15  C  1    1s      28    29    30    31    32    33
  16  C  1    1s      34    35    36
  17  C  1    1s      37
  18  C  1    2px     38    39    40
  19  C  1    2py     42    43    44
  20  C  1    2pz     46    47    48
  21  C  1    2px     41
  22  C  1    2py     45
  23  C  1    2pz     49
  24  C  1    3d2-    50
  25  C  1    3d1-    51
  26  C  1    3d0     52
  27  C  1    3d1+    53
  28  C  1    3d2+    54
  29  H  1    1s      55    56    57
  30  H  1    1s      58
  31  H  2    1s      59    60    61
  32  H  2    1s      62




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        32


  Symmetry  A  ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     2px        4
    5     O  1     2py        5
    6     O  1     2pz        6
    7     O  1     2px        7
    8     O  1     2py        8
    9     O  1     2pz        9
   10     O  1     3d2-      10
   11     O  1     3d1-      11
   12     O  1     3d0       12
   13     O  1     3d1+      13
   14     O  1     3d2+      14
   15     C  1     1s        15
   16     C  1     1s        16
   17     C  1     1s        17
   18     C  1     2px       18
   19     C  1     2py       19
   20     C  1     2pz       20
   21     C  1     2px       21
   22     C  1     2py       22
   23     C  1     2pz       23
   24     C  1     3d2-      24
   25     C  1     3d1-      25
   26     C  1     3d0       26
   27     C  1     3d1+      27
   28     C  1     3d2+      28
   29     H  1     1s        29
   30     H  1     1s        30
   31     H  2     1s        31
   32     H  2     1s        32

  Symmetries of electric field:  A  (1)  A  (1)  A  (1)

  Symmetries of magnetic field:  A  (1)  A  (1)  A  (1)
           3  points done ....
 >>>> Total CPU  time used in HERMIT:   0.00 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds
vnn betw. point chrgs. and core nuclei      0.2343048183
vnn betw. point charges:           -0.2982654701


** Adding      -0.0639606517 to the old repulsion energy term(     30.9692536725) => new value      30.9052930207 **


