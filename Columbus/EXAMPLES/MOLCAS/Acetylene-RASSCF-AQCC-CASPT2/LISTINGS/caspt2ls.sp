License is going to expire in 85 days on Saturday December 18th, 2010
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.5 patchlevel 565          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                                               
                              ^^^^^^^       ^^^^ ^^^                                               
                              ^^^ ^^^       ^^^^ ^^^                                               
                              ^^^ ^^^^      ^^^  ^^^                                               
                              ^^^  ^^ ^^^^^  ^^  ^^^^                                              
                             ^^^^      ^^^^   ^   ^^^                                              
                             ^   ^^^   ^^^^   ^^^^  ^                                              
                            ^   ^^^^    ^^    ^^^^   ^                                             
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                                         
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                                      
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^                                  
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^                                
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^                                
               ^^^^^^^^^^^^^                          ^^^      ^^^^                                
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^                                
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



   -------------------------------------------------------------------
  |                                                                   
  |   Project         = molcas
  |   Submitted from  = /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK
  |   Scratch area    = /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK
  |   Save outputs to = WORKDIR
  |                                                                   
  |   Scratch area is NOT empty
  |                                                                   
  |                                                                   
   -------------------------------------------------------------------
 
 
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module CASPT2 with 250 MB of memory                                  
                                              at 12:33:32 Fri Sep 24 2010                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
 
 
 ###############################################################################
 ###############################################################################
 ###                                                                         ###
 ###                                                                         ###
 ###    User changerd nr of frozen orbitals.                                 ###
 ###                                                                         ###
 ###                                                                         ###
 ###############################################################################
 ###############################################################################
 
 ****************** WARNING ********************
  Default frozen orbitals as max of non valence 
  orbitals and orbitals frozen in the CASSCF is 
  overwritten by user input. This if of course  
  O.K., but may give problems to inexperienced  
  users, hence this warning.                    
Default values:   1   0   0   0   1   0   0   0
 ***********************************************
 
 
      Wave function specifications:
      -----------------------------
 
      Number of closed shell electrons           0
      Number of electrons in active shells      10
      Max number of holes in RAS1 space          0
      Max number of electrons in RAS3 space      0
      Number of inactive orbitals                0
      Number of active orbitals                  8
      Number of secondary orbitals              78
      Spin quantum number                      0.0
      State symmetry                             1
      Number of configuration state fnc.       176
      Number of root(s) available                1
      Root passed to geometry opt.               1
      A file JOBMIX will be created.
      This is a CASSCF reference function
 
 
      Orbital specifications:
      -----------------------
 
      Symmetry species                           1   2   3   4   5   6   7   8
                                                ag b3u b2u b1g b1u b2g b3g  au
      Frozen orbitals                            1   0   0   0   1   0   0   0
      Inactive orbitals                          0   0   0   0   0   0   0   0
      Active orbitals                            2   1   1   0   2   1   1   0
      Secondary orbitals                        17   9   9   4  17   9   9   4
      Deleted orbitals                           0   0   0   0   0   0   0   0
      Number of basis functions                 20  10  10   4  20  10  10   4
 
 
      Type of  Fock operator to use: STANDARD
      Type of HZERO operator to use: STANDARD IPEA           
      Extra imaginary denominator shift SHIFTI=      0.05000000
      The CANONICAL keyword was used in the RASSCF program.
      Therefore, input orbitals should not need to be transformed.
      The input orbitals and the CI vector will be used as they are.
 
--------------------------------------------------------------------------------
 Estimated memory requirements:
  POLY3 :                   121302
  MKRHS :                   110897
  SIGMA :                   181963
  DIADNS:                     5780
  PRPCTL:                   191903
 Available workspace:     32733047
 
********************************************************************************
 Single-state initialization phase begins for state  1
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  Single-state initialization phase finished.
 
********************************************************************************
  CASPT2 EQUATION SOLUTION
--------------------------------------------------------------------------------
  Total nr of CASPT2 parameters:
   Before reduction:       31408
   After  reduction:       31252
 
 The contributions to the second order correlation energy in atomic units.
-----------------------------------------------------------------------------------------------------------------------------
  IT.      VJTU        VJTI        ATVX        AIVX        VJAI        BVAT        BJAT        BJAI        TOTAL       RNORM  
-----------------------------------------------------------------------------------------------------------------------------
   1     0.000000    0.000000   -0.052513    0.000000    0.000000   -0.145963    0.000000    0.000000   -0.198476    0.005143
   2     0.000000    0.000000   -0.052949    0.000000    0.000000   -0.146110    0.000000    0.000000   -0.199059    0.000411
   3     0.000000    0.000000   -0.052950    0.000000    0.000000   -0.146111    0.000000    0.000000   -0.199060    0.000054
   4     0.000000    0.000000   -0.052947    0.000000    0.000000   -0.146110    0.000000    0.000000   -0.199057    0.000005
-------------------------------------------------------------------------------------------------------------------
 
  FINAL CASPT2 RESULT:
 
      Reference energy:         -75.4981734021
      E2 (Non-variational):      -0.1990568414
      Shift correction:          -0.0000372087
      E2 (Variational):          -0.1990940501
      Total energy:             -75.6972674523
      Residual norm:              0.0000006531
      Reference weight:           0.95417
 
      Contributions to the CASPT2 correlation energy
      Active & Virtual Only:         -0.1990568414
      One Inactive Excited:           0.0000000000
      Two Inactive Excited:           0.0000000000
 
 
----------------------------------------------------------------------------------------------------
 Report on small energy denominators, large coefficients, and large energy contributions.
++ Denominators, etc.
CASE  SYMM ACTIVE-MIX  NON-ACTIVE INDICES          DENOMINATOR     RHS VALUE       COEFFICIENT     CONTRIBUTION
ATVX     1  Mu1.0004  Se1.007                       2.43695053     -0.06294495      0.02653970     -0.00167054
ATVX     4  Mu4.0004  Se4.001                       2.43448919     -0.06274054      0.02648040     -0.00166139
BVATM    4  Mu4.0001  Se3.002 Se2.002               1.57152417     -0.04783192      0.03062823     -0.00146501
BVATM    4  Mu4.0001  Se4.001 Se1.007               2.26594511     -0.08689721      0.03833055     -0.00333082
--
 
********************************************************************************
  CASPT2 PROPERTY SECTION
--------------------------------------------------------------------------------
 
 
      Mulliken population Analysis:
      -----------------------------
 
 
++       Molecular Charges:
      ------------------
 
 
 
      Mulliken charges per center and basis function type
      ---------------------------------------------------
 
               H       C   
      1s     2.2754  1.9915
      2s     0.0000  1.7674
      2px    0.0000  1.0741
      2pz    0.0000  1.6744
      2py    0.0000  1.0741
      *s    -1.5148 -0.2144
      *px    0.0315 -0.1276
      *pz   -0.0031 -0.9996
      *py    0.0315 -0.1276
      *d2+   0.0002  0.0039
      *d1+   0.0035  0.0167
      *d0   -0.0023  0.0140
      *d1-   0.0035  0.0167
      *d2-   0.0002  0.0039
      *f3+   0.0000  0.0003
      *f2+   0.0000  0.0005
      *f1+   0.0000  0.0030
      *f0    0.0000 -0.0005
      *f1-   0.0000  0.0030
      *f2-   0.0000  0.0005
      *f3-   0.0000  0.0003
      Total  0.8256  6.1744
 
      N-E    0.1744 -0.1744
 
      Total electronic charge=   14.000000
 
      Total            charge=    0.000000
--
 
      Expectation values of various properties:
      -----------------------------------------
 
 
++       Moleculer Properties:
      ---------------------
 
 
      Dipole Moment (Debye):                                                          
      Origin of the operator (Ang)=    0.0000    0.0000    0.0000
                     X=    0.0000               Y=    0.0000               Z=    0.0000           Total=    0.0000
      Quadrupole Moment (Debye*Ang):                                                  
      Origin of the operator (Ang)=    0.0000    0.0000    0.0000
                    XX=  -11.9487              XY=    0.0000              XZ=    0.0000              YY=  -11.9487
                    YZ=    0.0000              ZZ=   -8.6830
      In traceless form (Debye*Ang)
                    XX=   -1.6328              XY=    0.0000              XZ=    0.0000              YY=   -1.6328
                    YZ=    0.0000              ZZ=    3.2657
--
 
  Total CASPT2 energies:
::    CASPT2 Root  1     Total energy:    -75.69726745                                                                  
 
      ********************************************************************************
      CASPT2 TIMING INFORMATION: CPU(s), I/O(s).
        Inizialization             0.06    0.00
        CASPT2 equations           0.13    0.00
        Properties                 0.02    0.00
        Gradient/MS coupling       0.00    0.00
       Total time                  0.21    0.00
 
  A NEW JOBIPH FILE NAMED 'JOBMIX' IS PREPARED.
********************************************************************************
 
  The CI coefficients for the MIXED state nr.   1
--------------------------------------------------------------------------------
 CI COEFFICIENTS LARGER THAN 0.50D-01
  Occupation of active orbitals, and spin coupling
  of open shells. (u,d: Spin up or down).
   Conf  Occupation         Coef       Weight                                   
  
      1  22 2 2 20 0 0         0.962325         0.926070
     10  22 2 0 20 0 2        -0.128493         0.016510
     15  22 0 2 20 2 0        -0.128493         0.016511
     46  2u d 2 2u d 0         0.055668         0.003099
     54  2u 2 d 2u 0 d        -0.055668         0.003099
     62  22 u d 20 u d         0.115025         0.013231
     85  22 u u 20 d d        -0.083022         0.006893
 
--- Stop Module: caspt2 at Fri Sep 24 12:33:33 2010 /rc=0 ---

     Happy landing!

--- Stop Module: auto at Fr Sep 24 12:33:33 2010 /rc=0 ---
