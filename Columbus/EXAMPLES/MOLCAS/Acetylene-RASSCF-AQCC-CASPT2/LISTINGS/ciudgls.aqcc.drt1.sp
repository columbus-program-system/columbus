1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       176       23296      807302      659464     1490238
      internal walks       595        2352        2352        1764        7063
valid internal walks       176        2352        2139        1674        6341
 lcore1,lcore2=              32693347              32676673
 lencor,maxblo              32768000                 60000

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32622060
 minimum size of srtscr:   1605583 WP (    49 records)
 maximum size of srtscr:   1736651 WP (    51 records)
========================================
 current settings:
 minbl3        1485
 minbl4        1515
 locmaxbl3    29652
 locmaxbuf    14826
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:    25 records of integral w-combinations 
                                 24 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=   84.00   83.33)

 sorted 3-external integrals:    12 records of integral w-combinations 
                                 12 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=   91.67   91.67)
 Orig.  diagonal integrals:  1electron:        88
                             0ext.    :       110
                             2ext.    :      1560
                             4ext.    :      6162


 Orig. off-diag. integrals:  4ext.    :    649056
                             3ext.    :    325674
                             2ext.    :     70491
                             1ext.    :      7788
                             0ext.    :       381
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       506


 Sorted integrals            3ext.  w :    315048 x :    304422
                             4ext.  w :    610094 x :    573707


 compressed index vector length=                  1522
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
    NTYPE=3
    GSET=3
   DAVCOR =10,
  NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  RTOLBK = 1e-3
  NITER = 20
  NVCIMN = 1
  RTOLCI = 1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
    MOLCAS=1
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 bummer (warning):2:changed keyword: nbkitr=         0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  20      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   1
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    1      maxseg =   4      nrfitr =  30
 ncorel =   10      nvrfmx =    6      nvrfmn =   1      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           32767999 DP per process
 Allocating               32767999  for integral sort step ...

********** Integral sort section *************


 workspace allocation information: lencor=  32767999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  thresh=1.d-10
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title                                                                    
 SIFS file created by program tran.      zam489            12:32:16.498 24-Sep-10

 input energy(*) values:
 energy( 1)=  3.170536871154E+01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   3.170536871154E+01

 nsym = 8 nmot=  88

 symmetry  =    1    2    3    4    5    6    7    8
 slabel(*) = ag   b3u  b2u  b1g  b1u  b2g  b3g  au  
 nmpsy(*)  =   20   10   10    4   20   10   10    4

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044  45:tout:045  46:tout:046  47:tout:047  48:tout:048  49:tout:049  50:tout:050
  51:tout:051  52:tout:052  53:tout:053  54:tout:054  55:tout:055  56:tout:056  57:tout:057  58:tout:058  59:tout:059  60:tout:060
  61:tout:061  62:tout:062  63:tout:063  64:tout:064  65:tout:065  66:tout:066  67:tout:067  68:tout:068  69:tout:069  70:tout:070
  71:tout:071  72:tout:072  73:tout:073  74:tout:074  75:tout:075  76:tout:076  77:tout:077  78:tout:078  79:tout:079  80:tout:080
  81:tout:081  82:tout:082  83:tout:083  84:tout:084  85:tout:085  86:tout:086  87:tout:087  88:tout:088

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=     766

 drt information:
  cidrt_title                                                                    
 nmotd =  88 nfctd =   0 nfvtc =   0 nmot  =  88
 nlevel =  88 niot  =  10 lowinl=  79
 orbital-to-level map(*)
   79  81  82   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17
   83  18  19  20  21  22  23  24  25  26  84  27  28  29  30  31  32  33  34  35
   36  37  38  39  80  85  86  40  41  42  43  44  45  46  47  48  49  50  51  52
   53  54  55  56  87  57  58  59  60  61  62  63  64  65  88  66  67  68  69  70
   71  72  73  74  75  76  77  78
 compressed map(*)
   79  81  82   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17
   83  18  19  20  21  22  23  24  25  26  84  27  28  29  30  31  32  33  34  35
   36  37  38  39  80  85  86  40  41  42  43  44  45  46  47  48  49  50  51  52
   53  54  55  56  87  57  58  59  60  61  62  63  64  65  88  66  67  68  69  70
   71  72  73  74  75  76  77  78
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   2   2   2
    2   2   2   2   2   2   3   3   3   3   3   3   3   3   3   4   4   4   4   5
    5   5   5   5   5   5   5   5   5   5   5   5   5   5   5   5   6   6   6   6
    6   6   6   6   6   7   7   7   7   7   7   7   7   7   8   8   8   8   1   5
    1   1   2   3   5   5   6   7
 repartitioning mu(*)=
   2.  2.  0.  0.  0.  0.  0.  0.  0.  0.

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      7832
 number with all external indices:      6162
 number with half external - half internal indices:      1560
 number with all internal indices:       110

 indxof: off-diagonal integral statistics.
    4-external integrals: num=     649056 strt=          1
    3-external integrals: num=     325674 strt=     649057
    2-external integrals: num=      70491 strt=     974731
    1-external integrals: num=       7788 strt=    1045222
    0-external integrals: num=        381 strt=    1053010

 total number of off-diagonal integrals:     1053390



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32589459
 !timer: setup required                  cpu_time=     0.004 walltime=     0.003
 pro2e        1    3917    7833   11749   15665   15720   15775   19691   63379  107067
   139834  148026  153486  175325

 pro2e:    855147 integrals read in   157 records.
 pro1e        1    3917    7833   11749   15665   15720   15775   19691   63379  107067
   139834  148026  153486  175325
 pro1e: eref =   -7.855557658556766E+01
 total size of srtscr:                    44  records of                  32767 
 WP =              11533984 Bytes
 !timer: first half-sort required        cpu_time=     0.056 walltime=     0.056

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -7.855557658557E+01
 putdg        1    3917    7833   11749   12515   45282   67127   19691   63379  107067
   139834  148026  153486  175325

 putf:      14 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:    25 records of integral w-combinations and
            24 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=   84.00   83.33
 prep4e:    76 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals    1183801
 number of original 4-external integrals   649056


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=    25 nrecx=    24 lbufp= 30006

 putd34:    12 records of integral w-combinations and
            12 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=   91.67   91.67
 prep3e:    57 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals     619470
 number of original 3-external integrals   325674


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=    12 nrecx=    12 lbufp= 30006

 putf:     106 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       cpu_time=     0.052 walltime=     0.054
 !timer: cisrt complete                  cpu_time=     0.112 walltime=     0.113
 deleting integral sort shared memory
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:    6162 2ext:    1560 0ext:     110
 fil4w,fil4x  :  649056 fil3w,fil3x :  325674
 ofdgint  2ext:   70491 1ext:    7788 0ext:     381so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1515 minbl3    1485 maxbl2    1518nbas:  17   9   9   4  17   9   9   4 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  32767999

 core energy values from the integral file:
 energy( 1)=  3.170536871154E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.855557658557E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.685020787403E+01
 nmot  =    88 niot  =    10 nfct  =     0 nfvt  =     0
 nrow  =    87 nsym  =     8 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       7063      595     2352     2352     1764
 nvalwt,nvalw:     6341      176     2352     2139     1674
 ncsft:         1490238
 total number of valid internal walks:    6341
 nvalz,nvaly,nvalx,nvalw =      176    2352    2139    1674

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   6894
 nd4ext,nd2ext,nd0ext  6162  1560   110
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int   649056   325674    70491     7788      381        0        0        0
 minbl4,minbl3,maxbl2  1515  1485  1518
 maxbuf 30006
 number of external orbitals per symmetry block:  17   9   9   4  17   9   9   4
 nmsym   8 number of internal orbitals  10
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    10                  4480
 block size     0
 pthz,pthy,pthx,pthw:   595  2352  2352  1764 total internal walks:    7063
 maxlp3,n2lp,n1lp,n0lp  4480     0     0     0
 orbsym(*)= 1 5 1 1 2 3 5 5 6 7

 setref:      176 references kept,
                0 references were marked as invalid, out of
              176 total.
 nmb.of records onel     1
 nmb.of records 2-ext    93
 nmb.of records 1-ext    11
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61573
    threx             86893
    twoex              7777
    onex               1413
    allin               766
    diagon             7571
               =======
   maximum            86893
 
  __ static summary __ 
   reflst               176
   hrfspc               176
               -------
   static->             352
 
  __ core required  __ 
   totstc               352
   max n-ex           86893
               -------
   totnec->           87245
 
  __ core available __ 
   totspc          32767999
   totnec -           87245
               -------
   totvec->        32680754

 number of external paths / symmetry
 vertex x     428     378     378     298     467     378     378     298
 vertex w     506     378     378     298     467     378     378     298
segment: free space=    32680754
 reducing frespc by                 15713 to               32665041 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         595|       176|         0|       176|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        2352|     23296|       176|      2352|       176|         2|
 -------------------------------------------------------------------------------
  X 3        2352|    807302|     23472|      2139|      2528|         3|
 -------------------------------------------------------------------------------
  W 4        1763|    659464|    830774|      1674|      4667|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=         812DP  conft+indsym=        9408DP  drtbuffer=        5493 DP

dimension of the ci-matrix ->>>   1490238

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1    2352     595     807302        176    2139     176
     2  4   1    25      two-ext wz   2X  4 1    1763     595     659464        176    1674     176
     3  4   3    26      two-ext wx*  WX  4 3    1763    2352     659464     807302    1674    2139
     4  4   3    27      two-ext wx+  WX  4 3    1763    2352     659464     807302    1674    2139
     5  2   1    11      one-ext yz   1X  2 1    2352     595      23296        176    2352     176
     6  3   2    15      1ex3ex yx    3X  3 2    2352    2352     807302      23296    2139    2352
     7  4   2    16      1ex3ex yw    3X  4 2    1763    2352     659464      23296    1674    2352
     8  1   1     1      allint zz    OX  1 1     595     595        176        176     176     176
     9  2   2     5      0ex2ex yy    OX  2 2    2352    2352      23296      23296    2352    2352
    10  3   3     6      0ex2ex xx*   OX  3 3    2352    2352     807302     807302    2139    2139
    11  3   3    18      0ex2ex xx+   OX  3 3    2352    2352     807302     807302    2139    2139
    12  4   4     7      0ex2ex ww*   OX  4 4    1763    1763     659464     659464    1674    1674
    13  4   4    19      0ex2ex ww+   OX  4 4    1763    1763     659464     659464    1674    1674
    14  2   2    42      four-ext y   4X  2 2    2352    2352      23296      23296    2352    2352
    15  3   3    43      four-ext x   4X  3 3    2352    2352     807302     807302    2139    2139
    16  4   4    44      four-ext w   4X  4 4    1763    1763     659464     659464    1674    1674
    17  1   1    75      dg-024ext z  OX  1 1     595     595        176        176     176     176
    18  2   2    76      dg-024ext y  OX  2 2    2352    2352      23296      23296    2352    2352
    19  3   3    77      dg-024ext x  OX  3 3    2352    2352     807302     807302    2139    2139
    20  4   4    78      dg-024ext w  OX  4 4    1763    1763     659464     659464    1674    1674
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:               1490238

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        4070 2x:           0 4x:           0
All internal counts: zz :        4283 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:        3764
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:        2841    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension     176
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -75.4981734022
       2         -74.8863402235

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   176

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                    10                    14
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.377777777777778      ncorel=
                    10

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -75.4981734022

  ### active excitation selection ###

 Inactive orbitals:                     1                     2
    there are      176 all-active excitations of which    176 are references.

    the    176 reference all-active excitation csfs

    ------------------------------------------------------------------------------------------
         1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
    ------------------------------------------------------------------------------------------
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:     2     2     2     2     2     2     2     2     2     2     2     2     2     1     1
  6:     2     2     2     2     2     0     0     0     0     0     0     0     0    -1    -1
  7:     2     1     0     0     0     2     2     2     1     1     0     0     0     2     1
  8:     0    -1     2     0     0     2     0     0    -1    -1     2     2     0     0    -1
  9:     0     0     0     2     0     0     2     0     2     0     2     0     2     1     1
 10:     0     0     0     0     2     0     0     2     0     2     0     2     2    -1    -1
    ------------------------------------------------------------------------------------------
        16    17    18    19    20    21    22    23    24    25    26    27    28    29    30
    ------------------------------------------------------------------------------------------
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:     1     1     1     1     1     1     1     0     0     0     0     0     0     0     0
  6:    -1    -1     1     1     1     1     1     2     2     2     2     2     2     2     2
  7:     1     0     2    -1    -1     1     0     2     2     2     1     1     0     0     0
  8:     1     2     0    -1     1    -1     2     2     0     0    -1    -1     2     2     0
  9:    -1     1    -1     1    -1    -1    -1     0     2     0     2     0     2     0     2
 10:    -1    -1    -1    -1    -1    -1    -1     0     0     2     0     2     0     2     2
    ------------------------------------------------------------------------------------------
        31    32    33    34    35    36    37    38    39    40    41    42    43    44    45
    ------------------------------------------------------------------------------------------
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     2     2     2     2     2     1     1     1     1     1     1     1     1     1     1
  5:     0     0     0     0     0     2     2     2     2     2     2     2     2    -1    -1
  6:     0     0     0     0     0    -1    -1    -1    -1     1     1     1     1     2     2
  7:     2     2     2     1     0     2     1     1     0     2    -1    -1     0     2     1
  8:     2     2     0    -1     2     1     2     0     1    -1     2     0    -1     1     2
  9:     2     0     2     2     2     0     0     2     2     0     0     2     2    -1    -1
 10:     0     2     2     2     2    -1    -1    -1    -1    -1    -1    -1    -1     0     0
    ------------------------------------------------------------------------------------------
        46    47    48    49    50    51    52    53    54    55    56    57    58    59    60
    ------------------------------------------------------------------------------------------
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     0
  5:    -1    -1    -1    -1     1     1     1     1     1     1     0     0     0     0     2
  6:     2     2     0     0     2     2     2     2     0     0    -1    -1     1     1     2
  7:     1     0     2     1     2    -1    -1     0     2    -1     2     1     2    -1     2
  8:     0     1     1     2    -1     2     0    -1    -1     2     1     2    -1     2     2
  9:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     2     2     2     2     0
 10:     2     2     2     2     0     0     2     2     2     2    -1    -1    -1    -1     0
    ------------------------------------------------------------------------------------------
        61    62    63    64    65    66    67    68    69    70    71    72    73    74    75
    ------------------------------------------------------------------------------------------
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  5:     2     2     2     2     2     2     2     2     2     2     2     2     1     1     0
  6:     2     2     2     2     2     2     2     0     0     0     0     0    -1     1     2
  7:     2     2     1     1     0     0     0     2     2     2     1     0     2     2     2
  8:     0     0    -1    -1     2     2     0     2     2     0    -1     2     2     2     2
  9:     2     0     2     0     2     0     2     2     0     2     2     2     1    -1     2
 10:     0     2     0     2     0     2     2     0     2     2     2     2    -1    -1     0
    ------------------------------------------------------------------------------------------
        76    77    78    79    80    81    82    83    84    85    86    87    88    89    90
    ------------------------------------------------------------------------------------------
  3:     2     2     2     2     2     1     1     1     1     1     1     1     1     1     1
  4:     0     0     0     0     0     2     2     2     2     2     2     2     2     2     2
  5:     0     0     0     0     0     2     2     2     2     2     2     2     2    -1    -1
  6:     2     2     2     2     0    -1    -1    -1    -1     1     1     1     1     2     2
  7:     2     2     1     0     2     2     1     1     0     2    -1    -1     0     2     1
  8:     2     0    -1     2     2     1     2     0     1    -1     2     0    -1     1     2
  9:     0     2     2     2     2     0     0     2     2     0     0     2     2    -1    -1
 10:     2     2     2     2     2    -1    -1    -1    -1    -1    -1    -1    -1     0     0
    ------------------------------------------------------------------------------------------
        91    92    93    94    95    96    97    98    99   100   101   102   103   104   105
    ------------------------------------------------------------------------------------------
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2     2    -1
  5:    -1    -1    -1    -1     1     1     1     1     1     1     0     0     0     0     2
  6:     2     2     0     0     2     2     2     2     0     0    -1    -1     1     1     2
  7:     1     0     2     1     2    -1    -1     0     2    -1     2     1     2    -1     2
  8:     0     1     1     2    -1     2     0    -1    -1     2     1     2    -1     2     2
  9:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     2     2     2     2     0
 10:     2     2     2     2     0     0     2     2     2     2    -1    -1    -1    -1     0
    ------------------------------------------------------------------------------------------
       106   107   108   109   110   111   112   113   114   115   116   117   118   119   120
    ------------------------------------------------------------------------------------------
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  5:     2     2     2     2     2     2     2     2     2     2     2     2     1     1     0
  6:     2     2     2     2     2     2     2     0     0     0     0     0    -1     1     2
  7:     2     2     1     1     0     0     0     2     2     2     1     0     2     2     2
  8:     0     0    -1    -1     2     2     0     2     2     0    -1     2     2     2     2
  9:     2     0     2     0     2     0     2     2     0     2     2     2     1    -1     2
 10:     0     2     0     2     0     2     2     0     2     2     2     2    -1    -1     0
    ------------------------------------------------------------------------------------------
       121   122   123   124   125   126   127   128   129   130   131   132   133   134   135
    ------------------------------------------------------------------------------------------
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     0     0     0
  5:     0     0     0     0     0     2     2     2    -1    -1     1     0     2     2     2
  6:     2     2     2     2     0     2     2     0    -1     1    -1     2    -1    -1     1
  7:     2     2     1     0     2    -1    -1    -1     2     2     2    -1     2     1     2
  8:     2     0    -1     2     2    -1    -1    -1     2     2     2    -1     1     2    -1
  9:     0     2     2     2     2     2     0     2     1    -1    -1     2     2     2     2
 10:     2     2     2     2     2     0     2     2    -1    -1    -1     2    -1    -1    -1
    ------------------------------------------------------------------------------------------
       136   137   138   139   140   141   142   143   144   145   146   147   148   149   150
    ------------------------------------------------------------------------------------------
  3:     1     1     1     1     1     0     0     0     0     0     0     0     0     0     0
  4:     0     0     0     0     0     2     2     2     2     2     2     2     2     2     2
  5:     2    -1    -1     1     1     2     2     2     2     2     2     2     2     2     2
  6:     1     2     2     2     2     2     2     2     2     2     2     2     2     0     0
  7:    -1     2     1     2    -1     2     2     2     1     1     0     0     0     2     2
  8:     2     1     2    -1     2     2     0     0    -1    -1     2     2     0     2     2
  9:     2    -1    -1    -1    -1     0     2     0     2     0     2     0     2     2     0
 10:    -1     2     2     2     2     0     0     2     0     2     0     2     2     0     2
    ------------------------------------------------------------------------------------------
       151   152   153   154   155   156   157   158   159   160   161   162   163   164   165
    ------------------------------------------------------------------------------------------
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     2     2     2     2     2     2     2     2     1     1     1     1
  5:     2     2     2     1     1     0     0     0     0     0     0     2     2     2     2
  6:     0     0     0    -1     1     2     2     2     2     2     0    -1    -1     1     1
  7:     2     1     0     2     2     2     2     2     1     0     2     2     1     2    -1
  8:     0    -1     2     2     2     2     2     0    -1     2     2     1     2    -1     2
  9:     2     2     2     1    -1     2     0     2     2     2     2     2     2     2     2
 10:     2     2     2    -1    -1     0     2     2     2     2     2    -1    -1    -1    -1
    ------------------------------------------------------------------
       166   167   168   169   170   171   172   173   174   175   176
    ------------------------------------------------------------------
  3:     0     0     0     0     0     0     0     0     0     0     0
  4:     1     1     1     1     0     0     0     0     0     0     0
  5:    -1    -1     1     1     2     2     2     2     2     2     0
  6:     2     2     2     2     2     2     2     2     2     0     2
  7:     2     1     2    -1     2     2     2     1     0     2     2
  8:     1     2    -1     2     2     2     0    -1     2     2     2
  9:    -1    -1    -1    -1     2     0     2     2     2     2     2
 10:     2     2     2     2     0     2     2     2     2     2     2

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:           1490238
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    2.194507
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    1.000000
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

              civs   1
 civs   1    1.00000    

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -75.4981734022  1.0658E-14  2.2080E-01  1.0475E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0096
    2   25    0     0.01     0.00     0.01     0.01         0.    0.0070
    3   26    0     1.23     0.04     0.01     1.22         0.    0.1478
    4   27    0     1.22     0.03     0.00     1.22         0.    0.0739
    5   11    0     0.05     0.04     0.00     0.05         0.    0.0409
    6   15    0     0.98     0.20     0.00     0.98         0.    0.4817
    7   16    0     0.77     0.16     0.01     0.77         0.    0.3588
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0038
    9    5    0     0.19     0.07     0.00     0.19         0.    0.1453
   10    6    0     1.26     0.07     0.01     1.26         0.    0.1342
   11   18    0     1.12     0.08     0.00     1.12         0.    0.1342
   12    7    0     0.58     0.04     0.00     0.58         0.    0.0495
   13   19    0     0.48     0.03     0.00     0.48         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.48     0.00     0.00     0.48         0.    0.0247
   16   44    0     0.38     0.00     0.00     0.38         0.    0.0247
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0028
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0409
   19   77    0     0.11     0.01     0.00     0.11         0.    0.0350
   20   78    0     0.08     0.05     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.210000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     8.9700s 
time spent in multnx:                   8.9600s 
integral transfer time:                 0.0400s 
time spent for loop construction:       0.8400s 
time for vector access in mult:         0.0300s 
total time per CI iteration:            9.2200s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000000
ci vector #   2dasum_wr=    4.498995
ci vector #   3dasum_wr=    7.407562
ci vector #   4dasum_wr=   12.672719


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    0.061008    1.000000
calca4: root=   1 anorm**2=  0.05015032 scale:  1.02476842
calca4: root=   2 anorm**2=  0.94984968 scale:  1.39637018

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -5.963610E-14

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1  -0.974602       0.223943    
 civs   2  -0.906657       -3.94579    

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -75.7035839686  2.0541E-01  7.6597E-03  1.9358E-01  1.0000E-03
 mraqcc  #  2  2    -71.6076862726  2.4757E+01  0.0000E+00  2.3097E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0096
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0070
    3   26    0     1.23     0.02     0.01     1.23         0.    0.1478
    4   27    0     1.22     0.02     0.00     1.22         0.    0.0739
    5   11    0     0.04     0.03     0.00     0.04         0.    0.0409
    6   15    0     1.09     0.23     0.00     1.09         0.    0.4817
    7   16    0     0.86     0.14     0.01     0.86         0.    0.3588
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0038
    9    5    0     0.19     0.06     0.00     0.19         0.    0.1453
   10    6    0     1.24     0.07     0.00     1.24         0.    0.1342
   11   18    0     1.12     0.08     0.00     1.12         0.    0.1342
   12    7    0     0.58     0.03     0.00     0.58         0.    0.0495
   13   19    0     0.48     0.04     0.00     0.48         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.48     0.00     0.01     0.48         0.    0.0247
   16   44    0     0.40     0.00     0.01     0.39         0.    0.0247
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0028
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0409
   19   77    0     0.11     0.02     0.00     0.11         0.    0.0350
   20   78    0     0.08     0.05     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.020000
time for cinew                         0.219999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     9.1800s 
time spent in multnx:                   9.1700s 
integral transfer time:                 0.0400s 
time spent for loop construction:       0.8300s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            9.4400s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.109381
ci vector #   2dasum_wr=    1.157452
ci vector #   3dasum_wr=    4.606440
ci vector #   4dasum_wr=    4.830627


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    0.002115    0.061008    1.000069
calca4: root=   1 anorm**2=  0.05459764 scale:  1.02693605
calca4: root=   2 anorm**2=  0.94622324 scale:  1.39507105
calca4: root=   3 anorm**2=  0.87512633 scale:  1.36935252

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -5.963610E-14  -8.324089E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   0.964952       0.236318       0.213962    
 civs   2   0.935054       -3.93793       0.103905    
 civs   3  -0.872563       0.551631        21.7187    

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -75.7168890870  1.3305E-02  7.8494E-04  5.7430E-02  1.0000E-03
 mraqcc  #  3  2    -71.7290032743  1.2132E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  3  3    -71.5565179044  2.4706E+01  0.0000E+00  4.6865E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0096
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0070
    3   26    0     1.22     0.04     0.00     1.22         0.    0.1478
    4   27    0     1.22     0.03     0.00     1.22         0.    0.0739
    5   11    0     0.04     0.03     0.00     0.04         0.    0.0409
    6   15    0     1.10     0.26     0.01     1.10         0.    0.4817
    7   16    0     0.85     0.15     0.01     0.85         0.    0.3588
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0038
    9    5    0     0.19     0.06     0.00     0.19         0.    0.1453
   10    6    0     1.25     0.07     0.00     1.25         0.    0.1342
   11   18    0     1.12     0.06     0.00     1.12         0.    0.1342
   12    7    0     0.58     0.03     0.01     0.58         0.    0.0495
   13   19    0     0.48     0.03     0.00     0.48         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.49     0.00     0.00     0.49         0.    0.0247
   16   44    0     0.40     0.00     0.00     0.40         0.    0.0247
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0028
   18   76    0     0.02     0.00     0.00     0.02         0.    0.0409
   19   77    0     0.11     0.00     0.00     0.11         0.    0.0350
   20   78    0     0.08     0.03     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.019999
time for cinew                         0.240000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     9.1800s 
time spent in multnx:                   9.1800s 
integral transfer time:                 0.0300s 
time spent for loop construction:       0.7900s 
time for vector access in mult:         0.0400s 
total time per CI iteration:            9.4800s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.057140
ci vector #   2dasum_wr=    0.460040
ci vector #   3dasum_wr=    1.555445
ci vector #   4dasum_wr=    1.480668


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    0.000268    0.002122    0.061014    1.000107
calca4: root=   1 anorm**2=  0.05587412 scale:  1.02755736
calca4: root=   2 anorm**2=  0.82042554 scale:  1.34923146
calca4: root=   3 anorm**2=  0.93682956 scale:  1.39170024
calca4: root=   4 anorm**2=  0.76495060 scale:  1.32851443

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -5.963610E-14  -8.324089E-03  -6.128172E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1  -0.968681       7.198135E-02   9.294772E-02   0.448215    
 civs   2  -0.934925        1.18586       -3.22073       -2.02684    
 civs   3   0.958761       -11.8475       -13.1889        12.9677    
 civs   4  -0.832101        48.6486        6.89536        36.1827    

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -75.7179953773  1.1063E-03  9.9905E-05  2.0981E-02  1.0000E-03
 mraqcc  #  4  2    -72.6268680897  8.9786E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  3    -71.6664176945  1.0990E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  4    -71.0769918365  2.4227E+01  0.0000E+00  1.4540E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0096
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0070
    3   26    0     1.23     0.03     0.01     1.23         0.    0.1478
    4   27    0     1.22     0.02     0.00     1.22         0.    0.0739
    5   11    0     0.05     0.04     0.00     0.05         0.    0.0409
    6   15    0     1.09     0.21     0.00     1.09         0.    0.4817
    7   16    0     0.85     0.15     0.00     0.85         0.    0.3588
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0038
    9    5    0     0.19     0.06     0.00     0.19         0.    0.1453
   10    6    0     1.26     0.08     0.00     1.26         0.    0.1342
   11   18    0     1.12     0.06     0.00     1.12         0.    0.1342
   12    7    0     0.58     0.03     0.00     0.58         0.    0.0495
   13   19    0     0.47     0.03     0.00     0.47         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.48     0.00     0.00     0.48         0.    0.0247
   16   44    0     0.40     0.00     0.01     0.40         0.    0.0247
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0028
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0409
   19   77    0     0.11     0.01     0.00     0.11         0.    0.0350
   20   78    0     0.08     0.03     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.040001
time for cinew                         0.250000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     9.1800s 
time spent in multnx:                   9.1800s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.7800s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            9.4900s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.014558
ci vector #   2dasum_wr=    0.203066
ci vector #   3dasum_wr=    0.546436
ci vector #   4dasum_wr=    0.517643


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    0.000030    0.000268    0.002122    0.061015    1.000107
calca4: root=   1 anorm**2=  0.05643465 scale:  1.02783007
calca4: root=   2 anorm**2=  0.84413725 scale:  1.35799015
calca4: root=   3 anorm**2=  0.93837355 scale:  1.39225485
calca4: root=   4 anorm**2=  0.75795288 scale:  1.32587815
calca4: root=   5 anorm**2=  0.87803486 scale:  1.37041412

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -5.963610E-14  -8.324089E-03  -6.128172E-03  -4.141919E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1   0.968602      -3.134337E-02   9.898729E-02   0.450175       5.406822E-02
 civs   2   0.934514      -0.709864       -3.12355       -1.97409        1.48632    
 civs   3  -0.970219        8.36254       -13.8880        12.7046       -8.02671    
 civs   4   0.943784       -42.0245        9.48147        37.0707        22.3533    
 civs   5  -0.878150        97.7533        7.21790        4.34864        152.621    

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -75.7181216217  1.2624E-04  1.6432E-05  8.2844E-03  1.0000E-03
 mraqcc  #  5  2    -73.4680913795  8.4122E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -71.6703422590  3.9246E-03  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  4    -71.0779744076  9.8257E-04  0.0000E+00  1.4830E+00  1.0000E-04
 mraqcc  #  5  5    -70.5661942544  2.3716E+01  0.0000E+00  1.4889E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.00     0.01     0.01         0.    0.0096
    2   25    0     0.02     0.00     0.00     0.02         0.    0.0070
    3   26    0     1.22     0.02     0.00     1.22         0.    0.1478
    4   27    0     1.22     0.03     0.01     1.21         0.    0.0739
    5   11    0     0.05     0.04     0.00     0.05         0.    0.0409
    6   15    0     1.10     0.20     0.00     1.10         0.    0.4817
    7   16    0     0.84     0.15     0.01     0.84         0.    0.3588
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0038
    9    5    0     0.19     0.07     0.00     0.19         0.    0.1453
   10    6    0     1.24     0.07     0.00     1.24         0.    0.1342
   11   18    0     1.12     0.08     0.00     1.12         0.    0.1342
   12    7    0     0.58     0.04     0.01     0.58         0.    0.0495
   13   19    0     0.48     0.03     0.00     0.48         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.49     0.00     0.00     0.49         0.    0.0247
   16   44    0     0.40     0.00     0.00     0.40         0.    0.0247
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0028
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0409
   19   77    0     0.10     0.02     0.00     0.10         0.    0.0350
   20   78    0     0.08     0.04     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.049999
time for cinew                         0.280003
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     9.1800s 
time spent in multnx:                   9.1600s 
integral transfer time:                 0.0400s 
time spent for loop construction:       0.8200s 
time for vector access in mult:         0.0300s 
total time per CI iteration:            9.5400s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.007804
ci vector #   2dasum_wr=    0.075753
ci vector #   3dasum_wr=    0.244647
ci vector #   4dasum_wr=    0.215836


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    0.000005    0.000030    0.000268    0.002123    0.061015    1.000107
calca4: root=   1 anorm**2=  0.05655303 scale:  1.02788765
calca4: root=   2 anorm**2=  0.74771387 scale:  1.32201130
calca4: root=   3 anorm**2=  0.91557438 scale:  1.38404277
calca4: root=   4 anorm**2=  0.79715403 scale:  1.34057974
calca4: root=   5 anorm**2=  0.87512468 scale:  1.36935192
calca4: root=   6 anorm**2=  0.88733693 scale:  1.37380382

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -5.963610E-14  -8.324089E-03  -6.128172E-03  -4.141919E-04  -2.284952E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1  -0.968746       5.487288E-02   9.935466E-02   0.419990       0.168473       3.534385E-02
 civs   2  -0.934405       0.317115       -3.25023       -1.85997      -0.338552        1.46004    
 civs   3   0.971935       -6.26897       -8.36884        16.3917       -11.2154        1.48043    
 civs   4  -0.960619        32.0627       -6.87025        25.8142        46.0123       -6.46648    
 civs   5    1.01091       -98.7219        7.01418       -14.6283        85.5746        124.850    
 civs   6  -0.807212        177.734        127.242        64.1798       -222.525        291.928    

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -75.7181393221  1.7700E-05  3.3485E-06  3.7056E-03  1.0000E-03
 mraqcc  #  6  2    -74.0328392890  5.6475E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -71.8083474244  1.3801E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  4    -71.0844881441  6.5137E-03  0.0000E+00  1.1610E+00  1.0000E-04
 mraqcc  #  6  5    -70.9945798303  4.2839E-01  0.0000E+00  1.3095E+00  1.0000E-04
 mraqcc  #  6  6    -69.9813691053  2.3131E+01  0.0000E+00  1.8680E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0096
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0070
    3   26    0     1.22     0.03     0.00     1.22         0.    0.1478
    4   27    0     1.22     0.04     0.00     1.22         0.    0.0739
    5   11    0     0.05     0.04     0.01     0.04         0.    0.0409
    6   15    0     1.10     0.26     0.00     1.10         0.    0.4817
    7   16    0     0.85     0.15     0.00     0.85         0.    0.3588
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0038
    9    5    0     0.19     0.06     0.00     0.19         0.    0.1453
   10    6    0     1.25     0.08     0.00     1.25         0.    0.1342
   11   18    0     1.13     0.06     0.00     1.13         0.    0.1342
   12    7    0     0.58     0.03     0.00     0.58         0.    0.0495
   13   19    0     0.48     0.03     0.00     0.48         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.49     0.00     0.00     0.49         0.    0.0247
   16   44    0     0.39     0.00     0.00     0.39         0.    0.0247
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0028
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0409
   19   77    0     0.11     0.01     0.00     0.11         0.    0.0350
   20   78    0     0.08     0.06     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.049999
time for cinew                         0.310001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     9.1800s 
time spent in multnx:                   9.1700s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.8700s 
time for vector access in mult:         0.0400s 
total time per CI iteration:            9.5800s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.003310
ci vector #   2dasum_wr=    0.030574
ci vector #   3dasum_wr=    0.104079
ci vector #   4dasum_wr=    0.092635


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    0.000001    1.000000
calca4: root=   1 anorm**2=  0.05653103 scale:  1.02787695
calca4: root=   2 anorm**2=  0.76115238 scale:  1.32708417

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1  -0.971184      -9.387368E-05

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1  -0.999945       7.479763E-02
 civs   2  -0.673193       -909.045    

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -75.7181421991  2.8770E-06  9.2627E-07  1.7304E-03  1.0000E-03
 mraqcc  #  7  2    -71.6078464766 -2.4250E+00  0.0000E+00  2.3181E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0096
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0070
    3   26    0     1.23     0.03     0.00     1.23         0.    0.1478
    4   27    0     1.21     0.03     0.00     1.21         0.    0.0739
    5   11    0     0.05     0.04     0.00     0.05         0.    0.0409
    6   15    0     1.10     0.20     0.00     1.10         0.    0.4817
    7   16    0     0.85     0.16     0.00     0.85         0.    0.3588
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0038
    9    5    0     0.19     0.08     0.00     0.19         0.    0.1453
   10    6    0     1.25     0.07     0.00     1.25         0.    0.1342
   11   18    0     1.12     0.08     0.00     1.12         0.    0.1342
   12    7    0     0.58     0.04     0.00     0.58         0.    0.0495
   13   19    0     0.48     0.05     0.00     0.48         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.49     0.00     0.00     0.49         0.    0.0247
   16   44    0     0.40     0.00     0.00     0.40         0.    0.0247
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0028
   18   76    0     0.02     0.01     0.00     0.02         0.    0.0409
   19   77    0     0.11     0.01     0.00     0.11         0.    0.0350
   20   78    0     0.08     0.05     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.019997
time for cinew                         0.220001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     9.1900s 
time spent in multnx:                   9.1900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.8600s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            9.4400s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.002246
ci vector #   2dasum_wr=    0.016156
ci vector #   3dasum_wr=    0.039802
ci vector #   4dasum_wr=    0.040020


====================================================================================================
Diagonal     counts:  0x:      137589 2x:       37318 4x:        6165
All internal counts: zz :        4283 yy:       95923 xx:      176986 ww:      104136
One-external counts: yz :       46114 yx:      609399 yw:      462901
Two-external counts: yy :      107097 ww:       83302 xx:      194372 xz:        5574 wz:        4523 wx:      210948
Three-ext.   counts: yx :       85663 yw:       70268

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        9551    task #     2:        6957    task #     3:      147841    task #     4:       73920
task #     5:       40854    task #     6:      481715    task #     7:      358807    task #     8:        3764
task #     9:      145313    task #    10:      134207    task #    11:      134207    task #    12:       49451
task #    13:       49451    task #    14:       24725    task #    15:       24727    task #    16:       24727
task #    17:        2841    task #    18:       40918    task #    19:       34991    task #    20:       24830
Spectrum of overlapmatrix:    0.000000    0.000001    1.000000
calca4: root=   1 anorm**2=  0.05654549 scale:  1.02788399
calca4: root=   2 anorm**2=  0.58145038 scale:  1.25755731
calca4: root=   3 anorm**2=  0.69689155 scale:  1.30264790

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1  -0.971184      -9.387368E-05  -1.999622E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   -1.00010      -0.186458      -0.217472    
 civs   2  -0.926841       -565.730        714.622    
 civs   3   0.917051        1237.98        841.529    

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -75.7181431498  9.5064E-07  2.5080E-07  9.8860E-04  1.0000E-03
 mraqcc  #  8  2    -73.9220880562  2.3142E+00  0.0000E+00  7.0375E-01  1.0000E-04
 mraqcc  #  8  3    -70.5385012690 -1.2698E+00  0.0000E+00  2.1559E+00  1.0000E-04
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0096
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0070
    3   26    0     1.22     0.03     0.01     1.22         0.    0.1478
    4   27    0     1.22     0.05     0.00     1.22         0.    0.0739
    5   11    0     0.04     0.03     0.00     0.04         0.    0.0409
    6   15    0     1.10     0.26     0.00     1.10         0.    0.4817
    7   16    0     0.85     0.14     0.00     0.85         0.    0.3588
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0038
    9    5    0     0.19     0.07     0.00     0.19         0.    0.1453
   10    6    0     1.25     0.08     0.01     1.25         0.    0.1342
   11   18    0     1.13     0.08     0.01     1.13         0.    0.1342
   12    7    0     0.57     0.03     0.00     0.57         0.    0.0495
   13   19    0     0.48     0.04     0.00     0.48         0.    0.0495
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0247
   15   43    0     0.49     0.00     0.01     0.49         0.    0.0247
   16   44    0     0.40     0.00     0.00     0.40         0.    0.0247
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0028
   18   76    0     0.02     0.02     0.00     0.02         0.    0.0409
   19   77    0     0.10     0.01     0.00     0.10         0.    0.0350
   20   78    0     0.08     0.06     0.00     0.08         0.    0.0035
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.030006
time for cinew                         0.229996
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     9.1700s 
time spent in multnx:                   9.1700s 
integral transfer time:                 0.0400s 
time spent for loop construction:       0.9100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            9.4300s 

 mraqcc   convergence criteria satisfied after  8 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -75.7181431498  9.5064E-07  2.5080E-07  9.8860E-04  1.0000E-03
 mraqcc  #  8  2    -73.9220880562  2.3142E+00  0.0000E+00  7.0375E-01  1.0000E-04
 mraqcc  #  8  3    -70.5385012690 -1.2698E+00  0.0000E+00  2.1559E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc vector at position   1 energy=  -75.718143149750

################END OF CIUDGINFO################

 
 a4den factor =  1.036466909 for root   1
diagon:itrnv=   0
    1 of the   4 expansion vectors are transformed.
    1 of the   3 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.97118)
reference energy =                         -75.4981734022
total aqcc energy =                        -75.7181431498
diagonal element shift (lrtshift) =         -0.2199697476

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -75.7181431498

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10

                                          orbital     1   45    2    3   21   31   46   47   65   75

                                         symmetry   ag   b1u  ag   ag   b3u  b2u  b1u  b1u  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.934562                        +-   +-   +-   +-   +-   +-   +-                
 z*  1  1       3 -0.010068                        +-   +-   +-   +-   +-   +-        +-           
 z*  1  1       4 -0.022761                        +-   +-   +-   +-   +-   +-             +-      
 z*  1  1       5 -0.022760                        +-   +-   +-   +-   +-   +-                  +- 
 z*  1  1       6 -0.016980                        +-   +-   +-   +-   +-        +-   +-           
 z*  1  1       7 -0.015258                        +-   +-   +-   +-   +-        +-        +-      
 z*  1  1       8 -0.125921                        +-   +-   +-   +-   +-        +-             +- 
 z*  1  1      14  0.110666                        +-   +-   +-   +-   +     -   +-        +     - 
 z*  1  1      18 -0.088216                        +-   +-   +-   +-   +    +    +-         -    - 
 z*  1  1      23 -0.016981                        +-   +-   +-   +-        +-   +-   +-           
 z*  1  1      24 -0.125923                        +-   +-   +-   +-        +-   +-        +-      
 z*  1  1      25 -0.015258                        +-   +-   +-   +-        +-   +-             +- 
 z*  1  1      33  0.032241                        +-   +-   +-   +-             +-        +-   +- 
 z*  1  1      36 -0.059596                        +-   +-   +-   +    +-    -   +-   +          - 
 z*  1  1      40  0.037948                        +-   +-   +-   +    +-   +    +-    -         - 
 z*  1  1      44  0.059597                        +-   +-   +-   +     -   +-   +-   +     -      
 z*  1  1      48 -0.014461                        +-   +-   +-   +     -        +-   +     -   +- 
 z*  1  1      50 -0.037949                        +-   +-   +-   +    +    +-   +-    -    -      
 z*  1  1      54  0.013978                        +-   +-   +-   +    +         +-    -    -   +- 
 z*  1  1      56  0.014461                        +-   +-   +-   +          -   +-   +    +-    - 
 z*  1  1      58 -0.013979                        +-   +-   +-   +         +    +-    -   +-    - 
 z*  1  1      60 -0.035680                        +-   +-   +-        +-   +-   +-   +-           
 z*  1  1      81  0.012485                        +-   +-   +    +-   +-    -   +-   +          - 
 z*  1  1      85  0.024558                        +-   +-   +    +-   +-   +    +-    -         - 
 z*  1  1      89 -0.012486                        +-   +-   +    +-    -   +-   +-   +     -      
 z*  1  1      95 -0.024558                        +-   +-   +    +-   +    +-   +-    -    -      
 z*  1  1     141 -0.010218                        +-   +-        +-   +-   +-   +-   +-           
 z*  1  1     142 -0.017743                        +-   +-        +-   +-   +-   +-        +-      
 z*  1  1     143 -0.017743                        +-   +-        +-   +-   +-   +-             +- 
 y   1  1     239  0.013658              2( b2u)   +-   +-   +-   +-   +-    -   +     -           
 y   1  1     247  0.024046              1( b1g)   +-   +-   +-   +-   +-    -   +          -      
 y   1  1     252 -0.011564              2( ag )   +-   +-   +-   +-   +-    -   +               - 
 y   1  1     254 -0.024046              4( ag )   +-   +-   +-   +-   +-    -   +               - 
 y   1  1     255  0.012024              5( ag )   +-   +-   +-   +-   +-    -   +               - 
 y   1  1     338  0.015118              1( ag )   +-   +-   +-   +-   +-   +     -              - 
 y   1  1     340 -0.014769              3( ag )   +-   +-   +-   +-   +-   +     -              - 
 y   1  1     603 -0.013658              2( b3u)   +-   +-   +-   +-    -   +-   +     -           
 y   1  1     612  0.011564              2( ag )   +-   +-   +-   +-    -   +-   +          -      
 y   1  1     614 -0.024046              4( ag )   +-   +-   +-   +-    -   +-   +          -      
 y   1  1     615 -0.012025              5( ag )   +-   +-   +-   +-    -   +-   +          -      
 y   1  1     628 -0.024046              1( b1g)   +-   +-   +-   +-    -   +-   +               - 
 y   1  1    1072 -0.015119              1( ag )   +-   +-   +-   +-   +    +-    -         -      
 y   1  1    1074  0.014769              3( ag )   +-   +-   +-   +-   +    +-    -         -      
 y   1  1    1123  0.011990              1( au )   +-   +-   +-   +-   +     -   +-    -           
 y   1  1    2294  0.013057              2( b3u)   +-   +-   +-    -   +-   +-   +          -      
 y   1  1    2303  0.013058              2( b2u)   +-   +-   +-    -   +-   +-   +               - 
 y   1  1    4076 -0.011094              3( ag )   +-   +-   +-   +    +-   +-    -    -           
 y   1  1    8012  0.011445              2( b3u)   +-   +-    -   +-   +-   +-   +          -      
 y   1  1    8021  0.011445              2( b2u)   +-   +-    -   +-   +-   +-   +               - 
 y   1  1    8115  0.011544              1( b1u)   +-   +-    -   +-   +-   +    +-              - 
 y   1  1    8117  0.013713              3( b1u)   +-   +-    -   +-   +-   +    +-              - 
 y   1  1    8530 -0.011544              1( b1u)   +-   +-    -   +-   +    +-   +-         -      
 y   1  1    8532 -0.013713              3( b1u)   +-   +-    -   +-   +    +-   +-         -      
 y   1  1   12969  0.010181              7( ag )   +-   +-   +    +-   +-   +-    -    -           
 y   1  1   12971 -0.010236              9( ag )   +-   +-   +    +-   +-   +-    -    -           
 y   1  1   12981  0.018729              2( b3u)   +-   +-   +    +-   +-   +-    -         -      
 y   1  1   12985 -0.012491              6( b3u)   +-   +-   +    +-   +-   +-    -         -      
 y   1  1   12990  0.018726              2( b2u)   +-   +-   +    +-   +-   +-    -              - 
 y   1  1   12994 -0.012490              6( b2u)   +-   +-   +    +-   +-   +-    -              - 
 y   1  1   13029  0.016200              1( au )   +-   +-   +    +-   +-    -   +-         -      
 y   1  1   13030  0.010668              2( au )   +-   +-   +    +-   +-    -   +-         -      
 y   1  1   13036 -0.016190              4( b1u)   +-   +-   +    +-   +-    -   +-              - 
 y   1  1   13041 -0.010665              9( b1u)   +-   +-   +    +-   +-    -   +-              - 
 y   1  1   13392 -0.016191              4( b1u)   +-   +-   +    +-    -   +-   +-         -      
 y   1  1   13397 -0.010666              9( b1u)   +-   +-   +    +-    -   +-   +-         -      
 y   1  1   13406 -0.016199              1( au )   +-   +-   +    +-    -   +-   +-              - 
 y   1  1   13407 -0.010668              2( au )   +-   +-   +    +-    -   +-   +-              - 
 x   1  1   28753 -0.027599    1( b3u)   1( b2u)   +-   +-   +-   +-    -    -   +-                
 x   1  1   28763 -0.010082    2( b3u)   2( b2u)   +-   +-   +-   +-    -    -   +-                
 x   1  1   28837 -0.035140    4( ag )   1( b1g)   +-   +-   +-   +-    -    -   +-                
 x   1  1   64210 -0.017021    2( ag )   1( b2u)   +-   +-   +-    -   +-    -   +-                
 x   1  1   77912  0.017021    2( ag )   1( b3u)   +-   +-   +-    -    -   +-   +-                
 w   1  1  832809 -0.011541    2( ag )   4( ag )   +-   +-   +-   +-   +-        +-                
 w   1  1  832811 -0.017145    4( ag )   4( ag )   +-   +-   +-   +-   +-        +-                
 w   1  1  833000 -0.023781    1( b2u)   1( b2u)   +-   +-   +-   +-   +-        +-                
 w   1  1  833002 -0.011601    2( b2u)   2( b2u)   +-   +-   +-   +-   +-        +-                
 w   1  1  833045 -0.017145    1( b1g)   1( b1g)   +-   +-   +-   +-   +-        +-                
 w   1  1  838663  0.021961    1( b3u)   1( b2u)   +-   +-   +-   +-   +     -   +-                
 w   1  1  838745 -0.016321    2( ag )   1( b1g)   +-   +-   +-   +-   +     -   +-                
 w   1  1  838749  0.010714    6( ag )   1( b1g)   +-   +-   +-   +-   +     -   +-                
 w   1  1  852099  0.011541    2( ag )   4( ag )   +-   +-   +-   +-        +-   +-                
 w   1  1  852101 -0.017145    4( ag )   4( ag )   +-   +-   +-   +-        +-   +-                
 w   1  1  852245 -0.023781    1( b3u)   1( b3u)   +-   +-   +-   +-        +-   +-                
 w   1  1  852247 -0.011601    2( b3u)   2( b3u)   +-   +-   +-   +-        +-   +-                
 w   1  1  852335 -0.017145    1( b1g)   1( b1g)   +-   +-   +-   +-        +-   +-                
 w   1  1  873992  0.014179    2( ag )   1( b2u)   +-   +-   +-   +    +-    -   +-                
 w   1  1  874011  0.010312    4( ag )   2( b2u)   +-   +-   +-   +    +-    -   +-                
 w   1  1  874145 -0.010309    2( b3u)   1( b1g)   +-   +-   +-   +    +-    -   +-                
 w   1  1  888006 -0.014179    2( ag )   1( b3u)   +-   +-   +-   +     -   +-   +-                
 w   1  1  888025  0.010312    4( ag )   2( b3u)   +-   +-   +-   +     -   +-   +-                
 w   1  1  888159  0.010309    2( b2u)   1( b1g)   +-   +-   +-   +     -   +-   +-                
 w   1  1  952829 -0.012309    2( ag )   2( ag )   +-   +-   +-        +-   +-   +-                
 w   1  1  952838  0.011523    2( ag )   5( ag )   +-   +-   +-        +-   +-   +-                
 w   1  1  952843  0.012920    2( ag )   6( ag )   +-   +-   +-        +-   +-   +-                
 w   1  1 1020881  0.011106    3( ag )   3( b1u)   +-   +-   +    +-   +-   +-    -                
 w   1  1 1020917 -0.010739    5( ag )   5( b1u)   +-   +-   +    +-   +-   +-    -                
 w   1  1 1020953 -0.011653    7( ag )   7( b1u)   +-   +-   +    +-   +-   +-    -                
 w   1  1 1020968  0.010611    5( ag )   8( b1u)   +-   +-   +    +-   +-   +-    -                
 w   1  1 1020972  0.011688    9( ag )   8( b1u)   +-   +-   +    +-   +-   +-    -                

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              95
     0.01> rq > 0.001           3939
    0.001> rq > 0.0001         43080
   0.0001> rq > 0.00001       255256
  0.00001> rq > 0.000001      548787
 0.000001> rq                 639077
           all               1490238
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.934562075845
     2     2      0.005160419310
     3     3     -0.010067802985
     4     4     -0.022760873015
     5     5     -0.022760017509
     6     6     -0.016980379472
     7     7     -0.015258097108
     8     8     -0.125920957670
     9     9     -0.000624970297
    10    10     -0.002052848995
    11    11      0.000606340498
    12    12      0.001832401392
    13    13      0.003201852261
    14    14      0.110666198394
    15    15      0.001432812617
    16    16     -0.000000706572
    17    17     -0.001221762388
    18    18     -0.088215709370
    19    19     -0.000000688051
    20    20     -0.001155485225
    21    21     -0.000095172824
    22    22      0.000901297433
    23    23     -0.016980816842
    24    24     -0.125922517657
    25    25     -0.015258491383
    26    26     -0.002052693783
    27    27     -0.000624823650
    28    28      0.001832806634
    29    29      0.000606370309
    30    30      0.003201850298
    31    31      0.002599552842
    32    32      0.002599543121
    33    33      0.032241090822
    34    34      0.000770882419
    35    35     -0.000472930343
    36    36     -0.059596494752
    37    37     -0.000379639867
    38    38     -0.000421917137
    39    39     -0.001347691965
    40    40      0.037948367592
    41    41     -0.000506448021
    42    42      0.001442226280
    43    43      0.000705679528
    44    44      0.059597076227
    45    45      0.000379485290
    46    46     -0.000421868853
    47    47     -0.001347695293
    48    48     -0.014460784240
    49    49     -0.000109717094
    50    50     -0.037948673084
    51    51      0.000506372095
    52    52      0.001442464925
    53    53      0.000705390746
    54    54      0.013978434986
    55    55      0.000003780355
    56    56      0.014460915217
    57    57      0.000109660334
    58    58     -0.013978510600
    59    59     -0.000003753335
    60    60     -0.035679518121
    61    61     -0.006943041726
    62    62     -0.006943035996
    63    63      0.000066561679
    64    64      0.000066536746
    65    65      0.000885002843
    66    66      0.000884985638
    67    67      0.000393533960
    68    68      0.000724383043
    69    69      0.007862102311
    70    70      0.001031811383
    71    71     -0.000084775824
    72    72     -0.000183510609
    73    73     -0.007137599537
    74    74      0.006913738517
    75    75      0.007862225386
    76    76      0.000724403776
    77    77      0.001031860667
    78    78     -0.000084777832
    79    79     -0.000183494284
    80    80     -0.003119204577
    81    81      0.012485420908
    82    82      0.001041276922
    83    83      0.002515444707
    84    84      0.000368932495
    85    85      0.024557569463
    86    86     -0.000819686915
    87    87     -0.004991452422
    88    88      0.000523325549
    89    89     -0.012485549386
    90    90     -0.001041203840
    91    91      0.002515450128
    92    92      0.000368854885
    93    93      0.000921679238
    94    94      0.000354989968
    95    95     -0.024558025659
    96    96      0.000819568269
    97    97     -0.004991615832
    98    98      0.000523645839
    99    99      0.006247817575
   100   100     -0.000380656570
   101   101     -0.000921588088
   102   102     -0.000354902836
   103   103     -0.006247853859
   104   104      0.000380777256
   105   105     -0.003341311396
   106   106      0.007776731380
   107   107      0.007776845755
   108   108     -0.001223200277
   109   109     -0.001223240403
   110   110     -0.000019004810
   111   111     -0.000018972759
   112   112     -0.000499525469
   113   113     -0.000018038509
   114   114      0.000796571525
   115   115     -0.001064194771
   116   116      0.000376688592
   117   117      0.000012986483
   118   118     -0.000814375922
   119   119      0.002106074449
   120   120      0.000796658546
   121   121     -0.000018007860
   122   122     -0.001064208748
   123   123      0.000376666875
   124   124      0.000012977847
   125   125     -0.000616748854
   126   126      0.002420317427
   127   127      0.002420225176
   128   128     -0.000790605197
   129   129      0.000000059957
   130   130      0.001956804184
   131   131     -0.001383605289
   132   132     -0.000790662318
   133   133      0.000233970089
   134   134     -0.000194159614
   135   135     -0.000431977849
   136   136      0.000373990532
   137   137      0.000233964348
   138   138     -0.000194143290
   139   139     -0.000432014838
   140   140      0.000373990097
   141   141     -0.010217716863
   142   142     -0.017743087345
   143   143     -0.017742939437
   144   144     -0.000764554295
   145   145     -0.000764626225
   146   146      0.000507112684
   147   147      0.000507131140
   148   148      0.001155699506
   149   149      0.000519956869
   150   150      0.002142806961
   151   151      0.002316815609
   152   152      0.000245316287
   153   153     -0.000113237215
   154   154     -0.001623165206
   155   155      0.000956303663
   156   156      0.002143020589
   157   157      0.000520007276
   158   158      0.002316866488
   159   159      0.000245377177
   160   160     -0.000113272361
   161   161     -0.000638984977
   162   162     -0.000901820999
   163   163     -0.000095435868
   164   164      0.000570719564
   165   165      0.000040114982
   166   166     -0.000901860472
   167   167     -0.000095467885
   168   168      0.000570667766
   169   169      0.000040114698
   170   170      0.000717097460
   171   171      0.000717097529
   172   172      0.000222740672
   173   173      0.000037289105
   174   174     -0.000058103213
   175   175     -0.000132636537
   176   176     -0.000132639042

 number of reference csfs (nref) is   176.  root number (iroot) is  1.
 c0**2 =   0.94345451c0**2(scaled) =   0.99680261  c**2 (all zwalks) =   0.94345451
 passed aftci ... 
 readint2: molcas,dalton2=                     1                     0
 files%faoints=aoints              
molcasbasis:  20  10  10   4  20  10  10   4

 Integral file header information:
 Using SEWARD integrals


   ******  Basis set information:  ******

 Number of irreps:                  8
 Total number of basis functions:  88

 irrep no.              1    2    3    4    5    6    7    8
 irrep label           ag   b3u  b2u  b1g  b1u  b2g  b3g  au 
 no. of bas.fcions.    20   10   10    4   20   10   10    4
 input basis function labels, i:bfnlab(i)=
H   1s     H   *s     H   *s     H   *pz    H   *pz    H   *d0    H   *d2+   C   1s     C   2s     C   *s  
C   *s     C   2pz    C   *pz    C   *pz    C   *d0    C   *d0    C   *d2+   C   *d2+   C   *f0    C   *f2+
H   *px    H   *px    H   *d1+   C   2px    C   *px    C   *px    C   *d1+   C   *d1+   C   *f1+   C   *f3+
H   *py    H   *py    H   *d1-   C   2py    C   *py    C   *py    C   *d1-   C   *d1-   C   *f3-   C   *f1-
H   *d2-   C   *d2-   C   *d2-   C   *f2-   H   1s     H   *s     H   *s     H   *pz    H   *pz    H   *d0 
H   *d2+   C   1s     C   2s     C   *s     C   *s     C   2pz    C   *pz    C   *pz    C   *d0    C   *d0 
C   *d2+   C   *d2+   C   *f0    C   *f2+   H   *px    H   *px    H   *d1+   C   2px    C   *px    C   *px 
C   *d1+   C   *d1+   C   *f1+   C   *f3+   H   *py    H   *py    H   *d1-   C   2py    C   *py    C   *py 
C   *d1-   C   *d1-   C   *f3-   C   *f1-   H   *d2-   C   *d2-   C   *d2-   C   *f2-
 map-vector 1 , imtype=                     3
   1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2   2   2   2   2
  1   1   1   2   2   2   2   2   2   2   1   1   1   2   2   2   2   2   2   2
  1   2   2   2   1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2
  2   2   2   2   1   1   1   2   2   2   2   2   2   2   1   1   1   2   2   2
  2   2   2   2   1   2   2   2
 map-vector 2 , imtype=                     4
   1   1   1   2   2   4   4   1   1   1   1   2   2   2   4   4   4   4   6   6
  2   2   4   2   2   2   4   4   6   6   2   2   4   2   2   2   4   4   6   6
  4   4   4   6   1   1   1   2   2   4   4   1   1   1   1   2   2   2   4   4
  4   4   6   6   2   2   4   2   2   2   4   4   6   6   2   2   4   2   2   2
  4   4   6   6   4   4   4   6
lodens (list->root)=  1
                       Size (real*8) of d2temp for two-external contributions      64963
 bummer (warning):inf%bufszi too small ... resetting to       1440
 
                       Size (real*8) of d2temp for all-internal contributions        381
                       Size (real*8) of d2temp for one-external contributions       7788
                       Size (real*8) of d2temp for two-external contributions      64963
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   18     2856
                1    1    2    1   10     2295
                1    1    3    1   10     2295
                1    1    4    1    5     1482
                1    1    5    1   18     7803
                1    1    6    1   10     6426
                1    1    7    1   10     7398
                1    1    8    1    5     3270
                5    2    5    1   18    15759
                5    2    6    1   10     7398
                5    2    7    1   10     7398
                5    2    8    1    5     3270
                1    3    1    1   18     2856
                1    3    2    1   10     2295
                1    3    3    1   10     2295
                1    3    4    1    5     1482
                1    3    5    1   18     7803
                1    3    6    1   10     6426
                1    3    7    1   10     7398
                1    3    8    1    5     3270
                1    4    1    1   18     2856
                1    4    2    1   10     2295
                1    4    3    1   10     2295
                1    4    4    1    5     1482
                1    4    5    1   18     7803
                1    4    6    1   10     6426
                1    4    7    1   10     7398
                1    4    8    1    5     3270
                2    5    2    1   10     4599
                2    5    3    1   10     1215
                2    5    4    1    5     2106
                2    5    5    1   18     4131
                2    5    6    1   10     9018
                2    5    7    1   10     5238
                2    5    8    1    5     4374
                3    6    3    1   10     5814
                3    6    4    1    5     2106
                3    6    5    1   18     4131
                3    6    6    1   10     3051
                3    6    7    1   10    11205
                3    6    8    1    5     4374
                5    7    5    1   18    15759
                5    7    6    1   10     7398
                5    7    7    1   10     7398
                5    7    8    1    5     3270
                5    8    5    1   18    15759
                5    8    6    1   10     7398
                5    8    7    1   10     7398
                5    8    8    1    5     3270
                6    9    5    1   18     9639
                6    9    6    1   10    11430
                6    9    7    1   10     5238
                6    9    8    1    5     4374
                7   10    5    1   18     9639
                7   10    6    1   10     4023
                7   10    7    1   10    12645
                7   10    8    1    5     4374
                       Size (real*8) of d2temp for three-external contributions     325674
                       Size (real*8) of d2temp for four-external contributions     650574
 enough memory for temporary d2 elements on vdisk ... 
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=   77761
 files%d4ext =     unit=  24  vdsk=   1  filestart=  437761
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                  1441      2881     11521         1         1
d2rec                     1         6        46         6        11
recsize                1440      1440      1440     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                1097761  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  3 last record  1max overlap with ref# 97% root-following 0
 MR-AQCC energy:   -75.71814315   -28.86793528
 residuum:     0.00098860
 deltae:     0.00000095
 apxde:     0.00000025
 a4den:     1.03646691
 reference energy:   -75.49817340   -28.64796553

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97118204    -0.01335753    -0.02415261     0.11674477    -0.00473906    -0.05576772     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97118204     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=   176 #zcsf=     176)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=     150  D0Y=       0  D0X=       0  D0W=       0
  DDZI=    1100 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 2.00to den1(    1)=   0.0000000
adding (  2) 2.00to den1(  331)=   0.0000000
adding (  3) 0.00to den1(    3)=   1.9970880
adding (  4) 0.00to den1(    6)=   1.9823727
adding (  5) 0.00to den1(  211)=   1.9345446
adding (  6) 0.00to den1(  266)=   1.9345446
adding (  7) 0.00to den1(  333)=   1.9978004
adding (  8) 0.00to den1(  336)=   0.0201364
adding (  9) 0.00to den1(  541)=   0.0667566
adding ( 10) 0.00to den1(  596)=   0.0667566
 root #                      1 : Scaling(2) with    1.03646690916473      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    1926  DYX=   23308  DYW=   18840
   D0Z=     150  D0Y=    2660  D0X=    2321  D0W=    1655
  DDZI=    1100 DDYI=   14896 DDXI=   12790 DDWI=    9632
  DDZE=       0 DDYE=    2352 DDXE=    2139 DDWE=    1674
================================================================================
adding (  1) 2.00to den1(    1)=   0.0000000
adding (  2) 2.00to den1(  331)=   0.0000000
adding (  3) 0.00to den1(    3)=   1.9771874
adding (  4) 0.00to den1(    6)=   1.9668617
adding (  5) 0.00to den1(  211)=   1.9076587
adding (  6) 0.00to den1(  266)=   1.9076598
adding (  7) 0.00to den1(  333)=   1.9741433
adding (  8) 0.00to den1(  336)=   0.0230343
adding (  9) 0.00to den1(  541)=   0.0751725
adding ( 10) 0.00to den1(  596)=   0.0751705
Trace of MO density:    14.000000
   14  correlated and     0  frozen core electrons


 total number of electrons =   14.0000000000

 test slabel:                     8
  ag  b3u b2u b1g b1u b2g b3g au 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        ag  partial gross atomic populations
   ao class       1ag        2ag        3ag        4ag        5ag        6ag 
    H _ s       0.006480   0.004748   0.345190   0.050246  -0.000208  -0.032882
    H _ p       0.004374  -0.023916   0.002547  -0.000170   0.000951  -0.000581
    H _ d       0.002569  -0.000704  -0.004701   0.000001  -0.000130  -0.000004
    C _ s       1.984263   1.994934   0.126776  -0.033547   0.012001   0.030395
    C _ p       0.001472  -0.041858   1.494463  -0.005896   0.000769   0.009572
    C _ d       0.000104   0.044496   0.002987   0.000498  -0.006251  -0.000115
    C _ f       0.000739   0.000300  -0.001157   0.000007  -0.000039   0.000012
 
   ao class       7ag        8ag        9ag       10ag       11ag       12ag 
    H _ s       0.000000  -0.000084   0.001073   0.000477   0.000000  -0.000042
    H _ p       0.000000   0.000436   0.000381  -0.000082   0.000000   0.000029
    H _ d      -0.000045  -0.000010  -0.000021  -0.000053  -0.000021  -0.000002
    C _ s       0.000000   0.000007  -0.001580  -0.000104   0.000000   0.000004
    C _ p       0.000000  -0.000046   0.000230   0.000178   0.000000   0.000008
    C _ d       0.002208   0.000393   0.000513  -0.000003   0.000044   0.000202
    C _ f      -0.000019  -0.000028  -0.000002   0.000048   0.000326   0.000020
 
   ao class      13ag       14ag       15ag       16ag       17ag       18ag 
    H _ s      -0.000001   0.000000  -0.000002   0.000000   0.000000   0.000000
    H _ p      -0.000045   0.000000   0.000007   0.000000   0.000002   0.000005
    H _ d      -0.000003   0.000022   0.000030   0.000018   0.000007   0.000000
    C _ s       0.000094   0.000000   0.000001   0.000000  -0.000001  -0.000001
    C _ p      -0.000016   0.000000   0.000007   0.000000  -0.000001   0.000001
    C _ d       0.000032   0.000031   0.000005   0.000008   0.000001   0.000000
    C _ f       0.000046   0.000004   0.000002   0.000000   0.000001   0.000002
 
   ao class      19ag       20ag 
    H _ s       0.000001   0.000001
    H _ p       0.000000   0.000000
    H _ d       0.000000   0.000000
    C _ s       0.000001   0.000000
    C _ p       0.000000   0.000000
    C _ d       0.000000   0.000000
    C _ f       0.000000   0.000000

                        b3u partial gross atomic populations
   ao class       1b3u       2b3u       3b3u       4b3u       5b3u       6b3u
    H _ p       0.048065  -0.000172   0.005154   0.000613  -0.000295   0.000000
    H _ d       0.005145  -0.000013  -0.000183  -0.000108   0.000031   0.000000
    C _ p       1.811737   0.007484  -0.000072  -0.000360   0.000734   0.000000
    C _ d       0.037105   0.000552   0.000875   0.000516   0.000044   0.000000
    C _ f       0.005636  -0.000025  -0.000039  -0.000046  -0.000010   0.000358
 
   ao class       7b3u       8b3u       9b3u      10b3u
    H _ p       0.000025  -0.000026   0.000000   0.000005
    H _ d      -0.000007   0.000028   0.000021   0.000000
    C _ p       0.000036   0.000005   0.000000   0.000000
    C _ d       0.000183  -0.000003   0.000002   0.000000
    C _ f       0.000016   0.000057   0.000005   0.000000

                        b2u partial gross atomic populations
   ao class       1b2u       2b2u       3b2u       4b2u       5b2u       6b2u
    H _ p       0.048065  -0.000172   0.005154   0.000613  -0.000295   0.000000
    H _ d       0.005145  -0.000013  -0.000183  -0.000108   0.000031   0.000000
    C _ p       1.811738   0.007484  -0.000072  -0.000360   0.000734   0.000000
    C _ d       0.037105   0.000552   0.000875   0.000516   0.000044   0.000000
    C _ f       0.005636  -0.000025  -0.000039  -0.000046  -0.000010   0.000358
 
   ao class       7b2u       8b2u       9b2u      10b2u
    H _ p       0.000025  -0.000026   0.000000   0.000005
    H _ d      -0.000007   0.000028   0.000021   0.000000
    C _ p       0.000036   0.000005   0.000000   0.000000
    C _ d       0.000183  -0.000003   0.000002   0.000000
    C _ f       0.000016   0.000057   0.000005   0.000000

                        b1g partial gross atomic populations
   ao class       1b1g       2b1g       3b1g       4b1g
    H _ d      -0.000147  -0.000035   0.000135   0.000035
    C _ d       0.007304   0.000075   0.000190   0.000015
    C _ f      -0.000063   0.000555   0.000024   0.000000

                        b1u partial gross atomic populations
   ao class       1b1u       2b1u       3b1u       4b1u       5b1u       6b1u
    H _ s       0.017726   1.116327   0.135321   0.003966  -0.018487  -0.000848
    H _ p       0.008862  -0.001310  -0.000445   0.003096   0.000066  -0.001795
    H _ d       0.002473  -0.004594   0.000000  -0.000007   0.000000  -0.000004
    C _ s       1.988708   0.969862   0.006402   0.016266  -0.004161  -0.001078
    C _ p      -0.013961  -0.083172  -0.118102  -0.009764   0.025111   0.001240
    C _ d      -0.003654  -0.021408   0.000104  -0.007175   0.000051   0.004893
    C _ f      -0.000154  -0.001501   0.000014   0.000073  -0.000021   0.000047
 
   ao class       7b1u       8b1u       9b1u      10b1u      11b1u      12b1u
    H _ s       0.000000   0.000953   0.000098   0.000197  -0.000043   0.000000
    H _ p       0.000000   0.000246   0.000136  -0.000073   0.000000   0.000000
    H _ d      -0.000004  -0.000026   0.000007  -0.000057   0.000005   0.000010
    C _ s       0.000000   0.000319  -0.000202   0.000124   0.000001   0.000000
    C _ p       0.000000  -0.001114   0.000009   0.000017   0.000100   0.000000
    C _ d       0.000475  -0.000003   0.000188   0.000015  -0.000024   0.000034
    C _ f       0.000001  -0.000046  -0.000009  -0.000010   0.000059   0.000006
 
   ao class      13b1u      14b1u      15b1u      16b1u      17b1u      18b1u
    H _ s       0.000000  -0.000011  -0.000008   0.000000  -0.000001   0.000000
    H _ p       0.000000  -0.000006   0.000001   0.000000   0.000001   0.000002
    H _ d       0.000005   0.000006   0.000008   0.000007   0.000002   0.000000
    C _ s       0.000000   0.000015   0.000000   0.000000  -0.000001  -0.000001
    C _ p       0.000000   0.000018   0.000010   0.000000   0.000002   0.000000
    C _ d       0.000009  -0.000006   0.000005   0.000000   0.000001   0.000000
    C _ f       0.000018   0.000005  -0.000001   0.000003   0.000002   0.000000
 
   ao class      19b1u      20b1u
    H _ s       0.000001   0.000000
    H _ p       0.000000   0.000000
    H _ d       0.000000   0.000000
    C _ s       0.000000   0.000000
    C _ p       0.000000   0.000000
    C _ d       0.000000   0.000000
    C _ f       0.000000   0.000000

                        b2g partial gross atomic populations
   ao class       1b2g       2b2g       3b2g       4b2g       5b2g       6b2g
    H _ p       0.025326  -0.000919   0.001295  -0.000219   0.000097   0.000027
    H _ d       0.000256  -0.000052  -0.000128   0.000006   0.000037   0.000000
    C _ p       0.068531   0.002599   0.000104   0.000089   0.000112  -0.000026
    C _ d      -0.018861   0.000599   0.000562   0.000367  -0.000109   0.000030
    C _ f       0.000033   0.000021  -0.000479  -0.000005   0.000058   0.000113
 
   ao class       7b2g       8b2g       9b2g      10b2g
    H _ p       0.000000  -0.000001   0.000009   0.000001
    H _ d       0.000000   0.000011   0.000002   0.000001
    C _ p       0.000000   0.000000   0.000000   0.000000
    C _ d       0.000000   0.000007  -0.000002   0.000001
    C _ f       0.000053   0.000005   0.000000   0.000000

                        b3g partial gross atomic populations
   ao class       1b3g       2b3g       3b3g       4b3g       5b3g       6b3g
    H _ p       0.025325  -0.000919   0.001295  -0.000219   0.000097   0.000027
    H _ d       0.000256  -0.000052  -0.000128   0.000006   0.000037   0.000000
    C _ p       0.068530   0.002599   0.000104   0.000089   0.000112  -0.000026
    C _ d      -0.018861   0.000599   0.000562   0.000367  -0.000109   0.000030
    C _ f       0.000033   0.000021  -0.000479  -0.000005   0.000058   0.000114
 
   ao class       7b3g       8b3g       9b3g      10b3g
    H _ p       0.000000  -0.000001   0.000009   0.000001
    H _ d       0.000000   0.000011   0.000002   0.000001
    C _ p       0.000000   0.000000   0.000000   0.000000
    C _ d       0.000000   0.000007  -0.000002   0.000001
    C _ f       0.000053   0.000005   0.000000   0.000000

                        au  partial gross atomic populations
   ao class       1au        2au        3au        4au 
    H _ d      -0.000021   0.000041   0.000016   0.000085
    C _ d       0.002472   0.000146   0.000026  -0.000007
    C _ f       0.000007   0.000025   0.000056   0.000000


                        gross atomic populations
     ao           H _        C _
      s         1.630191   7.089500
      p         0.150694   5.041226
      d         0.004984   0.072613
      f         0.000000   0.010854
    total       1.785868  12.214193
 

 Total number of electrons:   14.00006157

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.1       0.0       0.0      75.7      75.7       0.0       0.0      75.9
--------------------------------------------------------------------------------
       1         0.1       0.0       0.0      75.7      75.7       0.0       0.0      75.9
 DA ...
