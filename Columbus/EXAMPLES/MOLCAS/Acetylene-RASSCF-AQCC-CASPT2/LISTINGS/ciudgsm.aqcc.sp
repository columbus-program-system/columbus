1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -75.4981734022  1.0658E-14  2.2080E-01  1.0475E+00  1.0000E-03
 mraqcc  #  2  1    -75.7035839686  2.0541E-01  7.6597E-03  1.9358E-01  1.0000E-03
 mraqcc  #  3  1    -75.7168890870  1.3305E-02  7.8494E-04  5.7430E-02  1.0000E-03
 mraqcc  #  4  1    -75.7179953773  1.1063E-03  9.9905E-05  2.0981E-02  1.0000E-03
 mraqcc  #  5  1    -75.7181216217  1.2624E-04  1.6432E-05  8.2844E-03  1.0000E-03
 mraqcc  #  6  1    -75.7181393221  1.7700E-05  3.3485E-06  3.7056E-03  1.0000E-03
 mraqcc  #  7  1    -75.7181421991  2.8770E-06  9.2627E-07  1.7304E-03  1.0000E-03
 mraqcc  #  8  1    -75.7181431498  9.5064E-07  2.5080E-07  9.8860E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  8 iterations.

 final mraqcc   convergence information:
 mraqcc  #  8  1    -75.7181431498  9.5064E-07  2.5080E-07  9.8860E-04  1.0000E-03

 number of reference csfs (nref) is   176.  root number (iroot) is  1.
 c0**2 =   0.94345451c0**2(scaled) =   0.99680261  c**2 (all zwalks) =   0.94345451
