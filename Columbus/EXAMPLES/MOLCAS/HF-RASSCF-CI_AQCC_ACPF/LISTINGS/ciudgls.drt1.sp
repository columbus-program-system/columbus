
======= Computing sorting integral file structure =========

 nmpsy=                       20                       10 
                        4                       10
                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         8         428        7407        9314       17157
      internal walks         8          40          45          50         143
valid internal walks         8          40          42          47         137
 found l2rec=                     4096 n2max=                     2730

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32583894
 minimum size of srtscr:                   229369  WP(  
                        7  records)
 maximum size of srtscr:                   360437  WP(  
                        9  records)

sorted 4-external integrals:     3 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00

sorted 3-external integrals:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 Orig.  diagonal integrals:  1electron:        44
                             0ext.    :        42
                             2ext.    :       456
                             4ext.    :      1482


 Orig. off-diag. integrals:  4ext.    :     79122
                             3ext.    :     47394
                             2ext.    :     13251
                             1ext.    :      2040
                             0ext.    :       138
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       236


 Sorted integrals            3ext.  w :     44360 x :     41326
                             4ext.  w :     70390 x :     62163


1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      -varseg3    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 compressed index vector length=                       25
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 8
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-4
  NITER = 30
  NVCIMN = 1
  NVCIMX = 6
  RTOLCI = 1e-4
  IDEN  = 1
  CSFPRN = 10,
    MOLCAS=1
 /&end
 ------------------------------------------------------------------------
 Ame=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        0
 me=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        0
 ntotseg(*)=                        4                        4 
                        4                        4                        4 
                        4
 iexplseg(*)=                        0                        0 
                        0                        0                        0 
                        0
 resetting explseg from                         0  to                         0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    1      maxseg =   4      nrfitr =  30
 ncorel =    8      nvrfmx =    6      nvrfmn =   1      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           32767999 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  32767999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------

 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title                                                                    
 SIFS file created by program tran.      zam216            14:03:29.306 23-Dec-08

 input energy(*) values:
 energy( 1)=  5.220720459423E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   5.220720459423E+00

 nsym = 4 nmot=  44

 symmetry  =    1    2    3    4
 slabel(*) = a1   b1   a2   b2  
 nmpsy(*)  =   20   10    4   10

 info(*) =          1      4096      3272      4096      2730

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   32767 maxbl3=   60000 maxbl4=   60000 intmxo=     766

 drt information:
  cidrt_title                                                                    
 nmotd =  44 nfctd =   0 nfvtc =   0 nmot  =  44
 nlevel =  44 niot  =   6 lowinl=  39
 orbital-to-level map(*)
   39  40  41  42   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16
   43  17  18  19  20  21  22  23  24  25  26  27  28  29  44  30  31  32  33  34
   35  36  37  38
 compressed map(*)
   39  40  41  42   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16
   43  17  18  19  20  21  22  23  24  25  26  27  28  29  44  30  31  32  33  34
   35  36  37  38
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   2   2   2   2
    2   2   2   2   2   3   3   3   3   4   4   4   4   4   4   4   4   4   1   1
    1   1   2   4
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1980
 number with all external indices:      1482
 number with half external - half internal indices:       456
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      79122 strt=          1
    3-external integrals: num=      47394 strt=      79123
    2-external integrals: num=      13251 strt=     126517
    1-external integrals: num=       2040 strt=     139768
    0-external integrals: num=        138 strt=     141808

 total number of off-diagonal integrals:      141945



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32586861
 !timer: setup required                  user+sys=     0.000 walltime=     0.000
 pro2e        1     991    1981    2971    3961    3982    4003    4993   48681   92369
   125136  129232  131962  142881

 pro2e:    108786 integrals read in    40 records.
 pro1e        1     991    1981    2971    3961    3982    4003    4993   48681   92369
   125136  129232  131962  142881
 pro1e: eref =   -7.631761899820656E+01
 total size of srtscr:                        8  records of  
                    32767 WP =                  2097088 Bytes
 !timer: first half-sort required        user+sys=     0.010 walltime=     0.000

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -7.631761899821E+01
 putdg        1     991    1981    2971    3737   36504   58349    4993   48681   92369
   125136  129232  131962  142881

 putf:       5 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    21 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     132553
 number of original 4-external integrals    79122


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     2 lbufp= 32767

 putd34:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    21 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      85686
 number of original 3-external integrals    47394


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     2 nrecx=     2 lbufp= 32767

 putf:      23 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       user+sys=     0.020 walltime=     0.000
 !timer: cisrt complete                  user+sys=     0.030 walltime=     0.000
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:    1482 2ext:     456 0ext:      42
 fil4w,fil4x  :   79122 fil3w,fil3x :   47394
 ofdgint  2ext:   13251 1ext:    2040 0ext:     138so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     705 minbl3     705 maxbl2     708nbas:  16   9   4   9   0   0   0   0 maxbuf 32767
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  32767999

 core energy values from the integral file:
 energy( 1)=  5.220720459423E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.631761899821E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.109689853878E+01
 nmot  =    44 niot  =     6 nfct  =     0 nfvt  =     0
 nrow  =    34 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        143        8       40       45       50
 nvalwt,nvalw:      137        8       40       42       47
 ncsft:           17157
 total number of valid internal walks:     137
 nvalz,nvaly,nvalx,nvalw =        8      40      42      47

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   1532
 nd4ext,nd2ext,nd0ext  1482   456    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    79122    47394    13251     2040      138        0        0        0
 minbl4,minbl3,maxbl2   705   705   708
 maxbuf 32767
 number of external orbitals per symmetry block:  16   9   4   9
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                        6                       39
 block size     0
 pthz,pthy,pthx,pthw:     8    40    45    50 total internal walks:     143
 maxlp3,n2lp,n1lp,n0lp    39     0     0     0
 orbsym(*)= 1 1 1 1 2 4

 setref:        8 references kept,
                0 references were marked as invalid, out of
                8 total.
 limcnvrt: found                        8  valid internal walksout of  
                        8  walks (skipping trailing invalids)
  ... adding                         8  segmentation marks segtype= 
                        1
 limcnvrt: found                       40  valid internal walksout of  
                       40  walks (skipping trailing invalids)
  ... adding                        40  segmentation marks segtype= 
                        2
 limcnvrt: found                       42  valid internal walksout of  
                       45  walks (skipping trailing invalids)
  ... adding                        42  segmentation marks segtype= 
                        3
 limcnvrt: found                       47  valid internal walksout of  
                       50  walks (skipping trailing invalids)
  ... adding                        47  segmentation marks segtype= 
                        4

 norbsm(*)=  20  10   4  10   0   0   0   0
 noxt(*)=  16   9   4   9   0   0   0   0
 intorb=   6 nmsym=   4 lowdoc=   5
 ivout(*)=   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  22  23  24  25  26
  27  28  29  30  31  32  33  34  36  37  38  39  40  41  42  43  44   1   2   3
   4  21  35   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0
 post-tapin: reflst,limvec,indsym=                        1 
                        9                      152
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            66792
    threx             33002
    twoex              4642
    onex               1343
    allin               766
    diagon             2125
               =======
   maximum            66792

  __ static summary __ 
   reflst                 8
   hrfspc                 8
               -------
   static->               8

  __ core required  __ 
   totstc                 8
   max n-ex           66792
               -------
   totnec->           66800

  __ core available __ 
   totspc          32767999
   totnec -           66800
               -------
   totvec->        32701199

 number of external paths / symmetry
 vertex x     198     180     145     180
 vertex w     236     180     145     180
segment: free space=    32701199
 reducing frespc by                      598 to                  32700601 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           8|         8|         0|         8|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          40|       428|         8|        40|         8|         2|
 -------------------------------------------------------------------------------
  X 3          42|      7407|       436|        42|        48|         3|
 -------------------------------------------------------------------------------
  W 4          47|      9314|      7843|        47|        90|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         188DP  drtbuffer=         406 DP

dimension of the ci-matrix ->>>     17157

 executing brd_struct for civct
 gentasklist: ntask=                       20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      42       8       7407          8      42       8
     2  4   1    25      two-ext wz   2X  4 1      47       8       9314          8      47       8
     3  4   3    26      two-ext wx*  WX  4 3      47      42       9314       7407      47      42
     4  4   3    27      two-ext wx+  WX  4 3      47      42       9314       7407      47      42
     5  2   1    11      one-ext yz   1X  2 1      40       8        428          8      40       8
     6  3   2    15      1ex3ex yx    3X  3 2      42      40       7407        428      42      40
     7  4   2    16      1ex3ex yw    3X  4 2      47      40       9314        428      47      40
     8  1   1     1      allint zz    OX  1 1       8       8          8          8       8       8
     9  2   2     5      0ex2ex yy    OX  2 2      40      40        428        428      40      40
    10  3   3     6      0ex2ex xx*   OX  3 3      42      42       7407       7407      42      42
    11  3   3    18      0ex2ex xx+   OX  3 3      42      42       7407       7407      42      42
    12  4   4     7      0ex2ex ww*   OX  4 4      47      47       9314       9314      47      47
    13  4   4    19      0ex2ex ww+   OX  4 4      47      47       9314       9314      47      47
    14  2   2    42      four-ext y   4X  2 2      40      40        428        428      40      40
    15  3   3    43      four-ext x   4X  3 3      42      42       7407       7407      42      42
    16  4   4    44      four-ext w   4X  4 4      47      47       9314       9314      47      47
    17  1   1    75      dg-024ext z  OX  1 1       8       8          8          8       8       8
    18  2   2    76      dg-024ext y  OX  2 2      40      40        428        428      40      40
    19  3   3    77      dg-024ext x  OX  3 3      42      42       7407       7407      42      42
    20  4   4    78      dg-024ext w  OX  4 4      47      47       9314       9314      47      47
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=   1 indiv tasks=   1
REDTASK #   2 TIME=  18.000 N=   1 indiv tasks=   2
REDTASK #   3 TIME=  17.000 N=   1 indiv tasks=   3
REDTASK #   4 TIME=  16.000 N=   1 indiv tasks=   4
REDTASK #   5 TIME=  15.000 N=   1 indiv tasks=   5
REDTASK #   6 TIME=  14.000 N=   1 indiv tasks=   6
REDTASK #   7 TIME=  13.000 N=   1 indiv tasks=   7
REDTASK #   8 TIME=  12.000 N=   1 indiv tasks=   8
REDTASK #   9 TIME=  11.000 N=   1 indiv tasks=   9
REDTASK #  10 TIME=  10.000 N=   1 indiv tasks=  10
REDTASK #  11 TIME=   9.000 N=   1 indiv tasks=  11
REDTASK #  12 TIME=   8.000 N=   1 indiv tasks=  12
REDTASK #  13 TIME=   7.000 N=   1 indiv tasks=  13
REDTASK #  14 TIME=   6.000 N=   1 indiv tasks=  14
REDTASK #  15 TIME=   5.000 N=   1 indiv tasks=  15
REDTASK #  16 TIME=   4.000 N=   1 indiv tasks=  16
REDTASK #  17 TIME=   3.000 N=   1 indiv tasks=  17
REDTASK #  18 TIME=   2.000 N=   1 indiv tasks=  18
REDTASK #  19 TIME=   1.000 N=   1 indiv tasks=  19
REDTASK #  20 TIME=   0.000 N=   1 indiv tasks=  20
 initializing v-file: 1:                    17157

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          95 2x:           0 4x:           0
All internal counts: zz :          55 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:          54
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:          82    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension       8

    root           eigenvalues
    ----           ------------
       1        -100.0823305449

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                        8

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     8)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             17157
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    1.124793
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:           0 xx:           0 ww:           0
One-external counts: yz :         349 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          68 wz:          93 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:           0    task #     4:           0
task #     5:         332    task #     6:           0    task #     7:           0    task #     8:          54
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:          56    task #    16:          56
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

              civs   1
 civs   1    1.00000    
 eci,repnuc=   -28.98543200610800        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.0823305449 -2.4869E-14  2.8280E-01  1.4291E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0000
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0000
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0000
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0000
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0000
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0000
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0000
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0000
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0000
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0000
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0001
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0001
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.01     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0200s 
time spent in multnx:                   0.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0300s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.0823305449 -2.4869E-14  2.8280E-01  1.4291E+00  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             17157
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000000
ci vector #   2dasum_wr=    0.915615
ci vector #   3dasum_wr=    2.200899
ci vector #   4dasum_wr=    4.165199


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -2.882599E-13

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1   0.980979      -0.194112    
 civs   2   0.809451        4.09070    
 eci,repnuc=   -29.21877908724286        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.3156776260  2.3335E-01  1.2824E-02  2.7741E-01  1.0000E-04
 mr-sdci #  1  2    -94.1227199041  2.3026E+01  0.0000E+00  3.7122E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.01     0.00     0.01     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.051924
ci vector #   2dasum_wr=    0.296305
ci vector #   3dasum_wr=    0.682810
ci vector #   4dasum_wr=    0.980938


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -2.882599E-13   1.704962E-02

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   0.967844      -0.236463       0.321133    
 civs   2   0.840330      -0.871241       -4.10596    
 civs   3   0.744406        16.4591       -7.61455    
 eci,repnuc=   -29.22833315677901        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -100.3252316956  9.5541E-03  6.4856E-04  6.3800E-02  1.0000E-04
 mr-sdci #  2  2    -94.7692660005  6.4655E-01  0.0000E+00  3.1661E+00  1.0000E-04
 mr-sdci #  2  3    -93.9843682534  2.2887E+01  0.0000E+00  4.0866E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.02     0.01     0.01     0.02         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.02     0.00     0.01     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0700s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.012520
ci vector #   2dasum_wr=    0.066533
ci vector #   3dasum_wr=    0.179867
ci vector #   4dasum_wr=    0.239029


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -2.882599E-13   1.704962E-02  -3.532643E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1  -0.970018      -6.175871E-02  -0.233070      -0.395376    
 civs   2  -0.842309       -1.41406      -0.994665        3.83925    
 civs   3  -0.791524       -6.79732        16.0918        5.78727    
 civs   4  -0.936390       -75.9853       -2.66995       -36.2985    
 eci,repnuc=   -29.22894050480033        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -100.3258390436  6.0735E-04  4.8451E-05  1.6614E-02  1.0000E-04
 mr-sdci #  3  2    -95.6937476226  9.2448E-01  0.0000E+00  3.4447E+00  1.0000E-04
 mr-sdci #  3  3    -94.7678745604  7.8351E-01  0.0000E+00  3.1382E+00  1.0000E-04
 mr-sdci #  3  4    -93.5953498769  2.2498E+01  0.0000E+00  4.0132E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.01     0.00     0.01     0.00         0.    0.0003
    6   15    0     0.01     0.01     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.01     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.0900s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.004179
ci vector #   2dasum_wr=    0.021954
ci vector #   3dasum_wr=    0.040918
ci vector #   4dasum_wr=    0.062209


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -2.882599E-13   1.704962E-02  -3.532643E-03  -1.315717E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1   0.968725      -0.292955      -0.356730      -0.288570      -0.126397    
 civs   2   0.842447       0.846871       -1.06644        1.99022        3.58773    
 civs   3   0.795242        5.58577        10.1345       -8.73954        12.1502    
 civs   4    1.01004        43.5638       -45.3457       -56.0698      -0.748467    
 civs   5  -0.986299       -200.139       -61.3666       -109.697        169.451    
 eci,repnuc=   -29.22898829266967        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -100.3258868315  4.7788E-05  3.8881E-06  4.7806E-03  1.0000E-04
 mr-sdci #  4  2    -97.4473735328  1.7536E+00  0.0000E+00  2.0697E+00  1.0000E-04
 mr-sdci #  4  3    -94.9455818673  1.7771E-01  0.0000E+00  3.2078E+00  1.0000E-04
 mr-sdci #  4  4    -94.2263040064  6.3095E-01  0.0000E+00  3.4287E+00  1.0000E-04
 mr-sdci #  4  5    -92.7052651423  2.1608E+01  0.0000E+00  4.2495E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.01     0.01         0.    0.0005
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.00     0.01     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.01     0.00     0.01     0.01         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.01     0.00     0.00     0.01         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0300s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001288
ci vector #   2dasum_wr=    0.006228
ci vector #   3dasum_wr=    0.012950
ci vector #   4dasum_wr=    0.018754


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -2.882599E-13   1.704962E-02  -3.532643E-03  -1.315717E-03  -1.225650E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1   0.968731      -0.196601       0.362659       0.319425      -0.183750      -0.110750    
 civs   2   0.842450       0.564239       -1.17509        1.22647        2.18823        3.27601    
 civs   3   0.795524        4.63571       0.143804       -10.5569       -6.62869        14.0019    
 civs   4    1.01568        36.9290        3.82113        42.7945       -64.3689        14.3807    
 civs   5   -1.06183       -173.650        146.098        46.5365       -32.3180        175.447    
 civs   6   0.941229        440.791        719.047       -59.9232        550.619       -224.114    

 trial vector basis is being transformed.  new dimension:   1
 eci,repnuc=   -29.22899195228861        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -100.3258904911  3.6596E-06  3.3638E-07  1.3890E-03  1.0000E-04
 mr-sdci #  5  2    -97.9982890834  5.5092E-01  0.0000E+00  1.4711E+00  1.0000E-04
 mr-sdci #  5  3    -95.4846637437  5.3908E-01  0.0000E+00  3.1297E+00  1.0000E-04
 mr-sdci #  5  4    -94.9410180051  7.1471E-01  0.0000E+00  3.1757E+00  1.0000E-04
 mr-sdci #  5  5    -93.5311476605  8.2588E-01  0.0000E+00  3.6290E+00  1.0000E-04
 mr-sdci #  5  6    -92.6203408828  2.1523E+01  0.0000E+00  4.2594E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.00     0.01     0.00         0.    0.0014
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.01     0.00     0.01     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000223
ci vector #   2dasum_wr=    0.002433
ci vector #   3dasum_wr=    0.003915
ci vector #   4dasum_wr=    0.005376


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1   0.979988       2.515894E-05

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1   0.999981      -6.959779E-02
 civs   2   0.917433        3275.91    
 eci,repnuc=   -29.22899226089750        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -100.3258907997  3.0861E-07  6.2669E-08  4.9498E-04  1.0000E-04
 mr-sdci #  6  2    -96.3910676721 -1.6072E+00  0.0000E+00  3.2862E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.01     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.01     0.00     0.01     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000230
ci vector #   2dasum_wr=    0.000798
ci vector #   3dasum_wr=    0.001285
ci vector #   4dasum_wr=    0.001889


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1   0.979988       2.515894E-05  -7.426858E-06

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   0.999982       1.062119E-02   7.481643E-02
 civs   2    1.11973       -2044.66       -2593.87    
 civs   3    1.08586       -5770.12        3456.81    
 eci,repnuc=   -29.22899232894773        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -100.3258908677  6.8050E-08  9.2095E-09  2.3140E-04  1.0000E-04
 mr-sdci #  7  2    -98.0595948518  1.6685E+00  0.0000E+00  2.1314E+00  1.0000E-04
 mr-sdci #  7  3    -95.7922208974  3.0756E-01  0.0000E+00  3.2647E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.01     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.01     0.00     0.00     0.01         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000074
ci vector #   2dasum_wr=    0.000337
ci vector #   3dasum_wr=    0.000662
ci vector #   4dasum_wr=    0.000874


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1   0.979988       2.515894E-05  -7.426858E-06   1.709343E-05

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1   0.999966      -0.149709       0.184398      -0.253734    
 civs   2    1.14668        1692.23       -1650.50       -2449.67    
 civs   3    1.23050        5109.42        4343.78       -525.277    
 civs   4   0.984243        8194.04       -7149.34        17377.5    
 eci,repnuc=   -29.22899233801213        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -100.3258908768  9.0644E-09  1.4226E-09  8.2504E-05  1.0000E-04
 mr-sdci #  8  2    -98.6164761792  5.5688E-01  0.0000E+00  1.2865E+00  1.0000E-04
 mr-sdci #  8  3    -95.9497278322  1.5751E-01  0.0000E+00  3.0431E+00  1.0000E-04
 mr-sdci #  8  4    -94.9251829100 -1.5835E-02  0.0000E+00  3.8264E+00  1.0000E-04

======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.02     0.02     0.00     0.02         0.    0.0015
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0014
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0006
   13   19    0     0.02     0.00     0.01     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.01     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.01     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -100.3258908768  9.0644E-09  1.4226E-09  8.2504E-05  1.0000E-04
 mr-sdci #  8  2    -98.6164761792  5.5688E-01  0.0000E+00  1.2865E+00  1.0000E-04
 mr-sdci #  8  3    -95.9497278322  1.5751E-01  0.0000E+00  3.0431E+00  1.0000E-04
 mr-sdci #  8  4    -94.9251829100 -1.5835E-02  0.0000E+00  3.8264E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -100.325890876795

################END OF CIUDGINFO################


    1 of the   5 expansion vectors are transformed.
    1 of the   4 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference                         1 (overlap= 
   0.9799911530975521      )

information on vector: 1from unit 11 written to unit 48filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -100.3258908768

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     1    2    3    4   21   35

                                         symmetry   a1   a1   a1   a1   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.012600                        +-   +-   +-   +-   +-      
 z*  1  1       2  0.012600                        +-   +-   +-   +-        +- 
 z*  1  1       3 -0.976408                        +-   +-   +-        +-   +- 
 z*  1  1       5  0.082412                        +-   +-        +-   +-   +- 
 z*  1  1       8  0.011029                        +-        +-   +-   +-   +- 
 y   1  1     121  0.015639              1( b2 )   +-   +-    -   +    +-    - 
 y   1  1     130 -0.015639              1( b1 )   +-   +-    -   +     -   +- 
 y   1  1     159  0.047194              1( b2 )   +-   +-   +     -   +-    - 
 y   1  1     160 -0.028746              2( b2 )   +-   +-   +     -   +-    - 
 y   1  1     163 -0.011331              5( b2 )   +-   +-   +     -   +-    - 
 y   1  1     164  0.012596              6( b2 )   +-   +-   +     -   +-    - 
 y   1  1     168 -0.047194              1( b1 )   +-   +-   +     -    -   +- 
 y   1  1     169  0.028746              2( b1 )   +-   +-   +     -    -   +- 
 y   1  1     172  0.011331              5( b1 )   +-   +-   +     -    -   +- 
 y   1  1     173 -0.012596              6( b1 )   +-   +-   +     -    -   +- 
 y   1  1     300  0.014444              2( a1 )   +-    -   +     -   +-   +- 
 y   1  1     301  0.016203              3( a1 )   +-    -   +     -   +-   +- 
 y   1  1     302 -0.019475              4( a1 )   +-    -   +     -   +-   +- 
 y   1  1     337  0.015154              3( b2 )   +-   +    +-    -   +-    - 
 y   1  1     346 -0.015154              3( b1 )   +-   +    +-    -    -   +- 
 y   1  1     374 -0.014598              4( a1 )   +-   +     -    -   +-   +- 
 y   1  1     377 -0.011382              7( a1 )   +-   +     -    -   +-   +- 
 x   1  1     801  0.030115    5( a1 )   1( a2 )   +-   +-   +-         -    - 
 x   1  1     861  0.020723    1( b1 )   1( b2 )   +-   +-   +-         -    - 
 x   1  1     862 -0.014210    2( b1 )   1( b2 )   +-   +-   +-         -    - 
 x   1  1     870 -0.014210    1( b1 )   2( b2 )   +-   +-   +-         -    - 
 x   1  1     871  0.022038    2( b1 )   2( b2 )   +-   +-   +-         -    - 
 x   1  1     881  0.011880    3( b1 )   3( b2 )   +-   +-   +-         -    - 
 x   1  1    1845  0.011262    3( b1 )   1( a2 )   +-   +-    -        +-    - 
 x   1  1    1896 -0.014716    2( a1 )   2( b2 )   +-   +-    -        +-    - 
 x   1  1    1915  0.011262    5( a1 )   3( b2 )   +-   +-    -        +-    - 
 x   1  1    1917  0.011147    7( a1 )   3( b2 )   +-   +-    -        +-    - 
 x   1  1    2040  0.014716    2( a1 )   2( b1 )   +-   +-    -         -   +- 
 x   1  1    2059  0.011262    5( a1 )   3( b1 )   +-   +-    -         -   +- 
 x   1  1    2061 -0.011147    7( a1 )   3( b1 )   +-   +-    -         -   +- 
 x   1  1    2175  0.011262    1( a2 )   3( b2 )   +-   +-    -         -   +- 
 x   1  1    3793  0.010991    4( a1 )   1( b2 )   +-    -   +-        +-    - 
 x   1  1    3809 -0.010140    4( a1 )   2( b2 )   +-    -   +-        +-    - 
 x   1  1    3937 -0.010991    4( a1 )   1( b1 )   +-    -   +-         -   +- 
 x   1  1    3953  0.010140    4( a1 )   2( b1 )   +-    -   +-         -   +- 
 w   1  1    8454  0.015781    5( a1 )   5( a1 )   +-   +-   +-        +-      
 w   1  1    8621  0.015780    1( a2 )   1( a2 )   +-   +-   +-        +-      
 w   1  1    8631  0.021854    1( b2 )   1( b2 )   +-   +-   +-        +-      
 w   1  1    8632 -0.022772    1( b2 )   2( b2 )   +-   +-   +-        +-      
 w   1  1    8633  0.023864    2( b2 )   2( b2 )   +-   +-   +-        +-      
 w   1  1    8636  0.013119    3( b2 )   3( b2 )   +-   +-   +-        +-      
 w   1  1    8647 -0.010147    2( b2 )   6( b2 )   +-   +-   +-        +-      
 w   1  1    8651  0.010202    6( b2 )   6( b2 )   +-   +-   +-        +-      
 w   1  1    8740 -0.019686    1( b1 )   1( b2 )   +-   +-   +-        +     - 
 w   1  1    8741  0.014194    2( b1 )   1( b2 )   +-   +-   +-        +     - 
 w   1  1    8749  0.014194    1( b1 )   2( b2 )   +-   +-   +-        +     - 
 w   1  1    8750 -0.021330    2( b1 )   2( b2 )   +-   +-   +-        +     - 
 w   1  1    8760 -0.011481    3( b1 )   3( b2 )   +-   +-   +-        +     - 
 w   1  1    8835  0.015781    5( a1 )   5( a1 )   +-   +-   +-             +- 
 w   1  1    8957  0.021854    1( b1 )   1( b1 )   +-   +-   +-             +- 
 w   1  1    8958 -0.022772    1( b1 )   2( b1 )   +-   +-   +-             +- 
 w   1  1    8959  0.023864    2( b1 )   2( b1 )   +-   +-   +-             +- 
 w   1  1    8962  0.013119    3( b1 )   3( b1 )   +-   +-   +-             +- 
 w   1  1    8973 -0.010147    2( b1 )   6( b1 )   +-   +-   +-             +- 
 w   1  1    8977  0.010202    6( b1 )   6( b1 )   +-   +-   +-             +- 
 w   1  1    9002  0.015780    1( a2 )   1( a2 )   +-   +-   +-             +- 
 w   1  1   10036 -0.010971    3( b1 )   1( a2 )   +-   +-   +         +-    - 
 w   1  1   10087  0.010159    2( a1 )   2( b2 )   +-   +-   +         +-    - 
 w   1  1   10106  0.010971    5( a1 )   3( b2 )   +-   +-   +         +-    - 
 w   1  1   10231 -0.010159    2( a1 )   2( b1 )   +-   +-   +          -   +- 
 w   1  1   10250  0.010971    5( a1 )   3( b1 )   +-   +-   +          -   +- 
 w   1  1   10366  0.010971    1( a2 )   3( b2 )   +-   +-   +          -   +- 
 w   1  1   11373  0.010646    2( a1 )   2( a1 )   +-   +-             +-   +- 
 w   1  1   11391  0.010689    6( a1 )   6( a1 )   +-   +-             +-   +- 
 w   1  1   11512  0.010228    3( b1 )   3( b1 )   +-   +-             +-   +- 
 w   1  1   11567  0.010228    3( b2 )   3( b2 )   +-   +-             +-   +- 
 w   1  1   12623 -0.013427    4( a1 )   1( b2 )   +-   +    +-        +-    - 
 w   1  1   12637 -0.011201    2( a1 )   2( b2 )   +-   +    +-        +-    - 
 w   1  1   12639  0.017617    4( a1 )   2( b2 )   +-   +    +-        +-    - 
 w   1  1   12703 -0.010413    4( a1 )   6( b2 )   +-   +    +-        +-    - 
 w   1  1   12767  0.013427    4( a1 )   1( b1 )   +-   +    +-         -   +- 
 w   1  1   12781  0.011201    2( a1 )   2( b1 )   +-   +    +-         -   +- 
 w   1  1   12783 -0.017617    4( a1 )   2( b1 )   +-   +    +-         -   +- 
 w   1  1   12847  0.010413    4( a1 )   6( b1 )   +-   +    +-         -   +- 
 w   1  1   13928  0.010549    2( a1 )   4( a1 )   +-   +     -        +-   +- 
 w   1  1   16099  0.012248    4( a1 )   4( a1 )   +-        +-        +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              80
     0.01> rq > 0.001           1615
    0.001> rq > 0.0001          4039
   0.0001> rq > 0.00001         7041
  0.00001> rq > 0.000001        2149
 0.000001> rq                   2232
           all                 17157
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          95 2x:           0 4x:           0
All internal counts: zz :          55 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         100
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.012600092839     -1.256648451263
     2     2      0.012600092839     -1.256648451264
     3     3     -0.976407812832     97.717972323704
     4     4     -0.006000237663      0.592955238228
     5     5      0.082412026418     -8.281602523517
     6     6     -0.000420207871      0.039380317689
     7     7     -0.000743125648      0.067328606553
     8     8      0.011028849667     -1.099155864334

 number of reference csfs (nref) is     8.  root number (iroot) is  1.
 c0**2 =   0.96063985  c**2 (all zwalks) =   0.96063985

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =   -100.081742223553   "relaxed" cnot**2         =   0.960639850925
 eci       =   -100.325890876795   deltae = eci - eref       =  -0.244148653242
 eci+dv1   =   -100.335500604183   dv1 = (1-cnot**2)*deltae  =  -0.009609727388
 eci+dv2   =   -100.335894342068   dv2 = dv1 / cnot**2       =  -0.010003465273
 eci+dv3   =   -100.336321723550   dv3 = dv1 / (2*cnot**2-1) =  -0.010430846755
 eci+pople =   -100.334093030341   ( 10e- scaled deltae )    =  -0.252350806788
 passed aftci ... 
 readint2: molcas,dalton2=                        1                        0
 files%faoints=aoints              
molcasbasis:  20  10   4  10

 Integral file header information:
 Using SEWARD integrals


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  44

 irrep no.              1    2    3    4
 irrep label           a1   b1   a2   b2 
 no. of bas.fcions.    20   10    4   10
 input basis function labels, i:bfnlab(i)=
H   1s     H   *s     H   *s     H   *pz    H   *pz    H   *d0    H   *d2+   F   1s     F   2s     F   *s  
F   *s     F   2pz    F   *pz    F   *pz    F   *d0    F   *d0    F   *d2+   F   *d2+   F   *f0    F   *f2+
H   *px    H   *px    H   *d1+   F   2px    F   *px    F   *px    F   *d1+   F   *d1+   F   *f1+   F   *f3+
H   *d2-   F   *d2-   F   *d2-   F   *f2-   H   *py    H   *py    H   *d1-   F   2py    F   *py    F   *py 
F   *d1-   F   *d1-   F   *f3-   F   *f1-
 map-vector 1 , imtype=                        3
   1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2   2   2   2   2
  1   1   1   2   2   2   2   2   2   2   1   2   2   2   1   1   1   2   2   2
  2   2   2   2
 map-vector 2 , imtype=                        4
   1   1   1   2   2   4   4   1   1   1   1   2   2   2   4   4   4   4   6   6
  2   2   4   2   2   2   4   4   6   6   4   4   4   6   2   2   4   2   2   2
  4   4   6   6
 <<< Entering RdOne >>>
 rc on entry:            0
 Label on entry:  MLTPL  0
 Comp on entry:          1
 SymLab on entry:        1
 Option on entry:        6
 picked LabelMLTPL  0 Component                        1 currop= 
                        1
 Computed Symlab=                        1
  I will close the file for you!

          overlap matrix SAO basis block   1

               sao   1        sao   2        sao   3        sao   4        sao   5        sao   6        sao   7        sao   8
  sao   1     1.00000000
  sao   2     0.97447731     1.00000000
  sao   3     0.86984181     0.78887809     1.00000000
  sao   4     0.00000000     0.00000000     0.00000000     1.00000000
  sao   5     0.00000000     0.00000000     0.00000000     0.61492356     1.00000000
  sao   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao   8     0.05911140     0.06743507     0.05561031     0.04104207     0.13108980     0.12746029     0.00000000     1.00000000
  sao   9     0.49084273     0.49592760     0.47131392     0.28485591     0.58719620     0.27993617     0.00000000     0.00000029
  sao  10     0.21122260     0.23217693     0.19467288     0.18392315     0.40778364     0.35651489     0.00000000     0.55834817
  sao  11     0.64136164     0.62605044     0.64465485     0.23110630     0.56227706     0.13648451     0.00000000     0.16285663
  sao  12    -0.37254426    -0.42058338    -0.18140325    -0.31000965    -0.13871174    -0.16898404     0.00000000     0.00000000
  sao  13    -0.31114088    -0.35063667    -0.11851283    -0.41707795    -0.22046505    -0.20036660     0.00000000     0.00000000
  sao  14    -0.56255659    -0.62833307    -0.33645097    -0.07884573     0.03561921     0.09260922     0.00000000     0.00000000
  sao  15     0.04234456     0.03588607     0.00339274     0.19083866     0.00219189     0.12247687     0.00000000     0.00000000
  sao  16     0.20723129     0.18729841     0.02501434     0.25343197    -0.08891297    -0.02767215     0.00000000     0.00000000
  sao  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.05903711     0.00000000
  sao  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.24067146     0.00000000
  sao  19    -0.05225636    -0.02226505    -0.00080503    -0.16729315     0.03349404    -0.03157112     0.00000000     0.00000000
  sao  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.19224851     0.00000000

               sao   9        sao  10        sao  11        sao  12        sao  13        sao  14        sao  15        sao  16
  sao   9     1.00000000
  sao  10     0.66714794     1.00000000
  sao  11     0.89221651     0.52037470     1.00000000
  sao  12     0.00000000     0.00000000     0.00000000     1.00000000
  sao  13     0.00000000     0.00000000     0.00000000     0.93861578     1.00000000
  sao  14     0.00000000     0.00000000     0.00000000     0.72541428     0.64084785     1.00000000
  sao  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  17        sao  18        sao  19        sao  20
  sao  17     1.00000000
  sao  18     0.50517544     1.00000000
  sao  19     0.00000000     0.00000000     1.00000000
  sao  20     0.00000000     0.00000000     0.00000000     1.00000000

          overlap matrix SAO basis block   2

               sao  21        sao  22        sao  23        sao  24        sao  25        sao  26        sao  27        sao  28
  sao  21     1.00000000
  sao  22     0.61492356     1.00000000
  sao  23     0.00000000     0.00000000     1.00000000
  sao  24     0.18446324     0.41505806     0.32427560     1.00000000
  sao  25     0.18201399     0.35646190     0.38055058     0.93861578     1.00000000
  sao  26     0.23558499     0.59841627     0.21903729     0.72541428     0.64084785     1.00000000
  sao  27    -0.08800157    -0.07574290    -0.21771107     0.00000000     0.00000000     0.00000000     1.00000000
  sao  28    -0.37809183    -0.37224003    -0.43546125     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  29     0.22479575     0.07582204     0.24920595     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  29        sao  30
  sao  29     1.00000000
  sao  30     0.00000000     1.00000000

          overlap matrix SAO basis block   3

               sao  31        sao  32        sao  33        sao  34
  sao  31     1.00000000
  sao  32     0.05903711     1.00000000
  sao  33     0.24067146     0.50517544     1.00000000
  sao  34    -0.19224851     0.00000000     0.00000000     1.00000000

          overlap matrix SAO basis block   4

               sao  35        sao  36        sao  37        sao  38        sao  39        sao  40        sao  41        sao  42
  sao  35     1.00000000
  sao  36     0.61492356     1.00000000
  sao  37     0.00000000     0.00000000     1.00000000
  sao  38     0.18446324     0.41505806     0.32427560     1.00000000
  sao  39     0.18201399     0.35646190     0.38055058     0.93861578     1.00000000
  sao  40     0.23558499     0.59841627     0.21903729     0.72541428     0.64084785     1.00000000
  sao  41    -0.08800157    -0.07574290    -0.21771107     0.00000000     0.00000000     0.00000000     1.00000000
  sao  42    -0.37809183    -0.37224003    -0.43546125     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  44     0.22479575     0.07582204     0.24920595     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  43        sao  44
  sao  43     1.00000000
  sao  44     0.00000000     1.00000000
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=       32766638
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
 1-DENSITY: executing task                        11                        1
 1-DENSITY: executing task                        13                        3
 1-DENSITY: executing task                        14                        4
 1-DENSITY: executing task                         1                        1
 1-DENSITY: executing task                         2                        2
 1-DENSITY: executing task                         3                        3
 1-DENSITY: executing task                         4                        4
 1-DENSITY: executing task                        71                        1
 1-DENSITY: executing task                        52                        2
 1-DENSITY: executing task                        72                        2
 1-DENSITY: executing task                        53                        3
 1-DENSITY: executing task                        73                        3
 1-DENSITY: executing task                        54                        4
 1-DENSITY: executing task                        74                        4
================================================================================
   DYZ=      43  DYX=     167  DYW=     172
   D0Z=       9  D0Y=      68  D0X=      75  D0W=      74
  DDZI=      35 DDYI=     180 DDXI=     180 DDWI=     185
  DDZE=       0 DDYE=      40 DDXE=      42 DDWE=      47
================================================================================
 Trace of MO density:     9.999999999999998     
                       10 correlated and                         0 
 frozen core electrons


*****   symmetry block  a1    *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.98812        1.96606       2.636706E-02   7.699722E-03   4.776210E-03   4.441120E-03   1.104390E-03

  mo    1    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  mo    2    0.00000       0.988298       0.152505       2.166106E-04   1.251248E-03  -9.665431E-04    0.00000      -4.631503E-04
  mo    3    0.00000      -0.152506       0.988283      -5.395727E-03   2.371284E-04  -1.410116E-03    0.00000      -9.658450E-04
  mo    4    0.00000      -9.691462E-04   5.308155E-03   0.994081      -7.122715E-02  -3.374635E-02    0.00000      -5.483686E-02
  mo    5    0.00000       1.285761E-03  -1.301859E-03  -5.338355E-03  -0.214735      -0.234537        0.00000       1.125250E-02
  mo    6    0.00000       1.674051E-03   1.695912E-03  -5.275468E-02  -0.503371       0.459235        0.00000      -0.303013    
  mo    7    0.00000       8.935868E-04   9.819464E-04  -1.460783E-02  -0.352369      -8.033552E-02    0.00000       0.495798    
  mo    8    0.00000      -1.610374E-04   8.990422E-04   6.099972E-02   0.678570       0.411186        0.00000       3.572773E-02
  mo    9    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.926888        0.00000    
  mo   10    0.00000      -2.597226E-04  -6.550534E-04   7.545024E-03   8.626301E-02  -0.496968        0.00000       6.110272E-02
  mo   11    0.00000       3.853036E-04  -1.003408E-04   4.818360E-02  -0.153040       0.479644        0.00000       0.273085    
  mo   12    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.152835        0.00000    
  mo   13    0.00000       1.419135E-04   7.515586E-04  -1.299036E-03   6.374855E-02  -9.521462E-02    0.00000       0.251255    
  mo   14    0.00000      -9.607350E-05   1.123899E-03   2.497440E-02  -6.447556E-02   8.464620E-02    0.00000       0.577112    
  mo   15    0.00000       7.841967E-04   1.279460E-03   1.283285E-02   0.204951      -7.584697E-02    0.00000       0.294149    
  mo   16    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000      -5.271472E-02    0.00000    
  mo   17    0.00000       7.020022E-04   7.366227E-04   3.362483E-02   0.121279       0.144338        0.00000       0.153930    
  mo   18    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.338735        0.00000    
  mo   19    0.00000       1.659801E-04   1.921962E-04   2.542285E-02  -2.514701E-02   0.193271        0.00000       0.217392    
  mo   20    0.00000       8.162771E-04  -1.505330E-04  -9.068737E-03   0.119918       1.481137E-02    0.00000      -0.161196    

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   7.444845E-04   3.624199E-04   3.518478E-04   3.001584E-04   2.187522E-04   1.296002E-04   8.075784E-05   4.725081E-05

  mo    2   3.926539E-04    0.00000      -1.146295E-03    0.00000      -1.859799E-03   3.788963E-04  -3.778403E-04    0.00000    
  mo    3   2.090414E-03    0.00000      -1.032578E-03    0.00000      -2.136369E-04  -3.354483E-04   3.146383E-04    0.00000    
  mo    4  -4.397116E-02    0.00000       1.932777E-02    0.00000      -1.809691E-03   1.076470E-02   1.096434E-02    0.00000    
  mo    5   0.310456        0.00000      -1.337990E-02    0.00000       0.375279      -0.116409      -3.582404E-02    0.00000    
  mo    6  -0.261220        0.00000       0.362085        0.00000       0.180765      -4.551952E-02  -0.205697        0.00000    
  mo    7  -0.194200        0.00000       3.772507E-02    0.00000      -6.126416E-02  -0.250152       0.566939        0.00000    
  mo    8  -6.472521E-02    0.00000       9.125560E-02    0.00000      -0.105899      -0.224355       0.106310        0.00000    
  mo    9    0.00000      -0.244735        0.00000      -0.269888        0.00000        0.00000        0.00000      -9.024327E-02
  mo   10   0.311496        0.00000       0.209851        0.00000      -1.753937E-02  -0.297490      -0.191479        0.00000    
  mo   11   0.531715        0.00000       4.540120E-02    0.00000      -0.102533      -0.436386      -3.485041E-02    0.00000    
  mo   12    0.00000       0.403521        0.00000      -0.139063        0.00000        0.00000        0.00000       0.891333    
  mo   13  -0.474049        0.00000      -0.123407        0.00000       0.123413      -8.548954E-02  -5.557455E-02    0.00000    
  mo   14  -0.185489        0.00000      -1.661076E-02    0.00000      -0.137137       0.175396      -0.294847        0.00000    
  mo   15  -2.360543E-02    0.00000       0.749927        0.00000       0.295788       0.148729      -0.167950        0.00000    
  mo   16    0.00000       0.655464        0.00000      -0.645539        0.00000        0.00000        0.00000      -0.388416    
  mo   17  -2.822862E-02    0.00000      -0.432135        0.00000       0.732144      -0.151242      -0.194138        0.00000    
  mo   18    0.00000       0.589612        0.00000       0.700786        0.00000        0.00000        0.00000      -0.215675    
  mo   19   0.388733        0.00000      -7.695961E-02    0.00000       9.688798E-02   0.713562       0.183406        0.00000    
  mo   20  -1.390119E-02    0.00000       0.204769        0.00000       0.365711      -1.341164E-02   0.629386        0.00000    

               no   17        no   18        no   19        no   20

      occ   3.240640E-05   1.111029E-05   7.268795E-06   3.693530E-06

  mo    2   1.005920E-04  -2.929452E-04  -6.330717E-04   4.266396E-05
  mo    3   9.419665E-04   2.522539E-04   1.539748E-04   2.453245E-04
  mo    4  -1.988842E-03  -1.697945E-03   2.599085E-04   1.148033E-03
  mo    5   0.415455       0.272664       0.627946       7.230520E-02
  mo    6  -0.283873       0.232162       0.115044      -0.130369    
  mo    7  -0.287146      -0.225464       0.250351      -2.244274E-02
  mo    8  -0.136545       0.185742       0.467470      -9.181060E-02
  mo   10  -0.525309       0.341875      -0.133930      -0.253941    
  mo   11   0.159956       5.915444E-02  -0.318934       0.227764    
  mo   13  -7.302834E-03   0.550218      -0.178869       0.568658    
  mo   14   0.307000       0.217091      -4.172697E-02  -0.583686    
  mo   15   7.870132E-02  -0.326298      -3.096666E-02   0.226192    
  mo   17  -0.231663      -0.265331      -9.315532E-02  -0.150830    
  mo   19  -0.352093       0.246975       6.203320E-02   0.126738    
  mo   20   0.256986       0.279829      -0.379958      -0.313757    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.98812        1.96606       2.636706E-02   7.699722E-03   4.776210E-03   4.441120E-03   1.104390E-03

  ao    1   2.656011E-03   0.468305      -0.619475      -6.661602E-02   5.996101E-02   0.765398       2.700423E-16   -1.69025    
  ao    2  -7.244295E-04  -0.143281       0.104895       0.148801        1.07997       0.226967       3.525436E-16  -0.176218    
  ao    3  -1.536984E-03  -0.170900       0.224935        1.39514       8.497622E-02  -0.154179      -9.888465E-17   0.240029    
  ao    4   6.668593E-04   1.751488E-02  -1.466754E-02  -1.765363E-02  -8.317138E-02   7.149042E-02   1.467380E-16   0.126182    
  ao    5   2.803823E-04   1.755396E-02  -1.624419E-02   0.114302       0.568574       0.479681       1.921592E-16  -0.502041    
  ao    6   1.548623E-04   3.319370E-03  -2.635290E-03   1.429965E-02   3.446881E-03   5.650062E-02   6.717379E-17   7.785097E-03
  ao    7   4.433440E-19   4.450845E-20  -7.329246E-20  -2.379696E-19   7.989037E-18   2.081261E-18   2.843632E-02  -3.885815E-18
  ao    8   0.999444      -7.424475E-03  -2.057960E-03   4.610221E-02  -7.365640E-02  -6.794869E-02   5.220870E-17  -2.269505E-02
  ao    9  -1.173097E-03   0.879140       0.366706      -2.428718E-02   0.913700      -9.237290E-02   6.798450E-17  -0.648341    
  ao   10   6.796289E-04  -6.495954E-05   5.876962E-03  -1.636774E-02   0.583521       0.150798      -5.464827E-17  -0.295225    
  ao   11   4.268120E-04  -3.020050E-02   0.118565      -0.836813       -2.48371      -0.867514      -5.220073E-16    2.12527    
  ao   12  -3.300168E-03  -0.210311       0.797474       0.353603       0.935160      -0.347659      -8.287791E-17    2.14769    
  ao   13   2.863689E-03  -8.519197E-03  -8.353354E-03  -1.924147E-02  -0.209461       0.361097       2.238735E-16   -1.65241    
  ao   14   9.269381E-04   2.640822E-02  -3.187778E-02   0.121328      -0.199631       0.774292       2.564919E-16   -1.73926    
  ao   15  -7.957979E-06   4.289208E-03  -6.691612E-03   4.673369E-02  -2.856557E-02   0.312280       2.078073E-17   0.395539    
  ao   16  -5.320522E-04   1.084968E-02  -1.926825E-02   2.858303E-02  -2.408674E-02   0.584480      -1.607029E-16   0.170444    
  ao   17  -2.487475E-21  -2.079211E-19   6.682328E-19   1.626351E-19   3.492913E-19   2.406677E-20   0.388234      -1.035501E-18
  ao   18  -1.353278E-21  -1.510554E-19   4.757882E-19   8.814383E-20   2.342760E-19  -8.061080E-20   0.739196      -6.646477E-19
  ao   19   1.497144E-04  -2.239014E-03   3.429539E-03  -9.919075E-03  -3.815910E-03  -3.788712E-02   3.078662E-17   2.293261E-02
  ao   20   1.063864E-31  -3.575007E-31  -1.291018E-31   1.284979E-32  -1.343244E-30  -7.735613E-31  -2.119629E-02   9.630217E-31

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   7.444845E-04   3.624199E-04   3.518478E-04   3.001584E-04   2.187522E-04   1.296002E-04   8.075784E-05   4.725081E-05

  ao    1   -3.76528      -1.290119E-15   -1.24379       1.238142E-15   -1.04988        5.09877        4.53268       7.151725E-16
  ao    2    2.13833      -3.727938E-15    1.41282       3.392143E-15   -1.09859       -3.63172       -2.05785       1.952304E-15
  ao    3    1.40163       4.558678E-16   0.493427      -3.778252E-16   0.568840       -1.81467       -1.94370      -2.538135E-16
  ao    4  -0.187283      -7.072965E-16  -0.289809       5.864885E-16  -0.400349       0.147863       0.756741       3.920063E-16
  ao    5   0.152978      -2.187447E-15   0.374113       1.985738E-15  -0.490391      -0.759401       0.744440       1.141623E-15
  ao    6   2.279912E-02  -6.639233E-16  -7.582708E-02   5.350970E-16  -0.304478       0.104124       0.246358       3.490737E-16
  ao    7  -8.698814E-19  -0.226492       5.615158E-18   5.624269E-03   3.367632E-17  -3.272950E-18   2.620834E-17   -1.02914    
  ao    8   8.284391E-03   4.339767E-16  -0.356131      -3.266507E-17   -1.64731       0.210806       -1.71643      -2.033075E-16
  ao    9   0.265882       1.110376E-15  -0.726380      -2.363025E-16   -3.30206       0.649104       -4.42269      -5.373416E-16
  ao   10   4.542148E-03  -5.268472E-16   0.520286       8.325605E-17    2.36524      -0.144056        2.09644       2.490309E-16
  ao   11  -0.159308       3.564246E-15  -6.612977E-02  -3.826463E-15    3.38167      -3.517070E-02    2.23764      -1.914504E-15
  ao   12   -1.55409       7.013119E-16    1.05896      -6.081404E-16   0.567658       6.697358E-02   -1.36252      -3.718053E-16
  ao   13    1.29600      -1.793833E-15   -1.38402       1.531386E-15   -1.39710      -0.132417        2.59232       9.542736E-16
  ao   14  -0.104865      -2.231313E-15   0.552257       2.119606E-15  -0.459797       0.115727      -0.219597       1.180473E-15
  ao   15   0.387300      -6.297769E-17  -0.354717       1.122723E-16   0.286836       0.742858      -5.255890E-02   3.679870E-17
  ao   16   0.158964       9.322128E-16   0.377713      -9.243598E-16   0.121400      -0.979985      -0.556144      -5.074425E-16
  ao   17   3.427143E-18   0.629516       4.548876E-18   0.870508      -5.789610E-18   4.591842E-18   8.093862E-18  -0.213735    
  ao   18   3.050551E-18  -0.433033       4.912346E-18  -0.723537      -5.088420E-18   4.546058E-18   6.911647E-18   0.417458    
  ao   19   0.254952      -3.423906E-16   0.674120       2.855532E-16  -0.405424       0.520005       0.488249       1.789572E-16
  ao   20  -1.817234E-30   0.738658      -1.950536E-30  -0.595932      -2.497917E-30  -3.309017E-30  -5.220600E-30  -0.373996    

               no   17        no   18        no   19        no   20

      occ   3.240640E-05   1.111029E-05   7.268795E-06   3.693530E-06

  ao    1    1.70888        2.39158       0.888453       0.549101    
  ao    2   -5.64300        4.07766      -0.130178       -7.63115    
  ao    3  -0.215262      -0.697226        1.09265      -8.957711E-02
  ao    4   0.487655        1.95343      -0.789602      -0.617443    
  ao    5   -1.97747        2.08362        2.00654       -4.28327    
  ao    6   0.411059       0.555135      -6.743222E-02   -2.04457    
  ao    7   6.454056E-18   5.960597E-18  -2.008955E-17  -1.640157E-17
  ao    8  -0.518973      -0.322986        1.20096        1.36665    
  ao    9   -1.25349      -0.743412        2.93418        3.84268    
  ao   10   0.514361       0.596268       -1.51323       -1.31894    
  ao   11    4.35990       -4.98452       -4.19109        4.32470    
  ao   12   0.215131      -0.924061       5.611095E-02   0.462050    
  ao   13  -0.297786        2.31364      -0.171006       -2.75931    
  ao   14   -2.52846        2.97585       0.734914       -2.79920    
  ao   15  -0.542920       0.199279       0.103621       0.111654    
  ao   16   0.761434       -1.75862       1.393453E-02    1.01162    
  ao   17   1.475769E-18   8.169859E-18  -1.385939E-18  -5.508101E-18
  ao   18   1.545469E-18   7.029883E-18  -1.280077E-18  -4.090931E-18
  ao   19   0.199950       0.513576      -0.121158      -9.403085E-02
  ao   20  -4.517181E-31  -3.179187E-30   2.070016E-30   5.889189E-31


*****   symmetry block  b1    *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395763E-02   4.789085E-03   7.458448E-04   5.838618E-04   3.250540E-04   3.167194E-04   9.886930E-05

  mo    1   0.999974       4.003757E-03  -1.673961E-03  -5.177728E-03   2.162731E-03   9.259681E-05   5.510082E-07   6.863631E-04
  mo    2  -6.178832E-03   0.699844      -0.302365      -0.483520      -8.105583E-02   0.156008       6.620877E-04   0.158279    
  mo    3   1.411056E-03  -0.608004      -0.448968      -0.143702      -0.463520       0.209447       8.768004E-04   0.193883    
  mo    4   9.953331E-05  -0.122703       0.783838      -0.212509      -7.388706E-02  -1.548615E-02  -1.003527E-04   0.334998    
  mo    5   8.253157E-06  -6.738273E-02  -9.278887E-03   0.115137       0.351479       0.209625       9.517379E-04  -0.453010    
  mo    6  -1.728571E-03  -0.217536      -9.346045E-02  -0.147130       0.608740       0.309864       1.308129E-03  -8.394084E-02
  mo    7   2.997146E-03   0.270255       7.012453E-02   0.719529      -0.294700       0.430884       1.788030E-03  -1.660970E-04
  mo    8  -4.349609E-08   2.618829E-06   2.787392E-07   4.760217E-05   9.186780E-06  -4.247344E-03   0.999991       1.229791E-04
  mo    9  -2.489040E-04  -2.364746E-02   0.175719      -0.206262       1.202473E-02   0.774047       3.289198E-03   0.160157    
  mo   10  -1.517529E-04   4.255121E-03  -0.219049       0.324347       0.438202      -8.226275E-02  -4.563367E-04   0.766302    

               no    9        no   10

      occ   4.533568E-05   7.945470E-06

  mo    1  -1.170763E-03  -2.569014E-04
  mo    2  -0.359137       4.906191E-03
  mo    3  -0.331747       4.172384E-02
  mo    4  -0.455431      -6.395639E-03
  mo    5  -0.457787       0.632336    
  mo    6  -0.195636      -0.640619    
  mo    7  -0.251143      -0.262161    
  mo    8  -8.983473E-06  -2.896185E-05
  mo    9   0.491142       0.244629    
  mo   10  -3.754712E-02   0.243682    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395763E-02   4.789085E-03   7.458448E-04   5.838618E-04   3.250540E-04   3.167194E-04   9.886930E-05

  ao    1   1.133765E-02  -3.852458E-02   6.473132E-02   0.207554       0.329226       0.178482       8.442566E-04  -0.811640    
  ao    2   3.699716E-02  -0.161729       0.172341       0.578999       0.331951      -0.315423      -1.328314E-03  -0.503314    
  ao    3   6.481147E-03  -3.581490E-02   4.461645E-02   0.156463       0.190705       0.119935       4.981774E-04  -0.362390    
  ao    4   0.967804        1.66605       0.296104        2.03090       -1.60877       0.409420       1.652615E-03  -0.115223    
  ao    5  -1.383108E-02  -0.603559      -0.171262       -2.18097        1.76102      -0.529980      -2.157099E-03   0.204521    
  ao    6   3.264463E-02   -1.33187      -0.364445      -0.464454      -0.428449       0.204125       8.414672E-04   0.559113    
  ao    7  -3.698942E-03   1.816257E-02  -0.339559       0.434953       0.426494      -0.534707      -2.383775E-03   0.729412    
  ao    8  -1.289423E-02   5.471128E-02  -0.657943       0.116614       7.490555E-02   0.444973       2.008281E-03   -1.24179    
  ao    9   2.803967E-03   2.998894E-03   2.985233E-02   0.104167       0.280811       0.766758       3.191841E-03   0.593253    
  ao   10  -4.349609E-08   2.618829E-06   2.787392E-07   4.760217E-05   9.186780E-06  -4.247344E-03   0.999991       1.229791E-04

               no    9        no   10

      occ   4.533568E-05   7.945470E-06

  ao    1   -1.00005       0.281887    
  ao    2    1.21678       -1.07687    
  ao    3  -0.468619       -1.46647    
  ao    4  -0.475267      -0.343499    
  ao    5   0.761425       0.793189    
  ao    6  -0.568915       0.645835    
  ao    7  -0.307774       0.151024    
  ao    8   0.147852       -1.01973    
  ao    9   0.371643       0.424681    
  ao   10  -8.983473E-06  -2.896185E-05


*****   symmetry block  a2    *****

               no    1        no    2        no    3        no    4

      occ   4.439955E-03   3.623199E-04   3.000222E-04   4.724495E-05

  mo    1   0.926889      -0.244977      -0.269667      -9.023797E-02
  mo    2   0.152834       0.403486      -0.139477       0.891284    
  mo    3  -5.268068E-02   0.654922      -0.646014      -0.388545    
  mo    4   0.338738       0.590138       0.700351      -0.215644    

               no    1        no    2        no    3        no    4

      occ   4.439955E-03   3.623199E-04   3.000222E-04   4.724495E-05

  ao    1   2.844598E-02  -0.226579       5.917054E-03   -1.02912    
  ao    2   0.388235       0.630174       0.870043      -0.213688    
  ao    3   0.739194      -0.433584      -0.723227       0.417427    
  ao    4  -2.116150E-02   0.738155      -0.596473      -0.374127    


*****   symmetry block  b2    *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395763E-02   4.789085E-03   7.458448E-04   5.838618E-04   3.250540E-04   3.167194E-04   9.886930E-05

  mo    1   0.999974       4.003757E-03  -1.673961E-03  -5.177728E-03   2.162731E-03   9.259681E-05  -5.510082E-07   6.863631E-04
  mo    2  -6.178832E-03   0.699844      -0.302365      -0.483520      -8.105583E-02   0.156008      -6.620877E-04   0.158279    
  mo    3   1.411056E-03  -0.608004      -0.448968      -0.143702      -0.463520       0.209447      -8.768004E-04   0.193883    
  mo    4   9.953331E-05  -0.122703       0.783838      -0.212509      -7.388706E-02  -1.548615E-02   1.003527E-04   0.334998    
  mo    5   8.253157E-06  -6.738273E-02  -9.278887E-03   0.115137       0.351479       0.209625      -9.517379E-04  -0.453010    
  mo    6  -1.728571E-03  -0.217536      -9.346045E-02  -0.147130       0.608740       0.309864      -1.308129E-03  -8.394084E-02
  mo    7   2.997146E-03   0.270255       7.012453E-02   0.719529      -0.294700       0.430884      -1.788030E-03  -1.660970E-04
  mo    8   4.349609E-08  -2.618829E-06  -2.787392E-07  -4.760217E-05  -9.186783E-06   4.247344E-03   0.999991      -1.229791E-04
  mo    9  -2.489040E-04  -2.364746E-02   0.175719      -0.206262       1.202473E-02   0.774047      -3.289198E-03   0.160157    
  mo   10  -1.517529E-04   4.255121E-03  -0.219049       0.324347       0.438202      -8.226275E-02   4.563367E-04   0.766302    

               no    9        no   10

      occ   4.533568E-05   7.945470E-06

  mo    1  -1.170763E-03  -2.569014E-04
  mo    2  -0.359137       4.906191E-03
  mo    3  -0.331747       4.172384E-02
  mo    4  -0.455431      -6.395639E-03
  mo    5  -0.457787       0.632336    
  mo    6  -0.195636      -0.640619    
  mo    7  -0.251143      -0.262161    
  mo    8   8.983473E-06   2.896185E-05
  mo    9   0.491142       0.244629    
  mo   10  -3.754712E-02   0.243682    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395763E-02   4.789085E-03   7.458448E-04   5.838618E-04   3.250540E-04   3.167194E-04   9.886930E-05

  ao    1   1.133765E-02  -3.852458E-02   6.473132E-02   0.207554       0.329226       0.178482      -8.442566E-04  -0.811640    
  ao    2   3.699716E-02  -0.161729       0.172341       0.578999       0.331951      -0.315423       1.328314E-03  -0.503314    
  ao    3   6.481147E-03  -3.581490E-02   4.461645E-02   0.156463       0.190705       0.119935      -4.981774E-04  -0.362390    
  ao    4   0.967804        1.66605       0.296104        2.03090       -1.60877       0.409420      -1.652615E-03  -0.115223    
  ao    5  -1.383108E-02  -0.603559      -0.171262       -2.18097        1.76102      -0.529980       2.157099E-03   0.204521    
  ao    6   3.264463E-02   -1.33187      -0.364445      -0.464454      -0.428449       0.204125      -8.414672E-04   0.559113    
  ao    7  -3.698942E-03   1.816257E-02  -0.339559       0.434953       0.426494      -0.534707       2.383775E-03   0.729412    
  ao    8  -1.289423E-02   5.471128E-02  -0.657943       0.116614       7.490555E-02   0.444973      -2.008281E-03   -1.24179    
  ao    9   4.349609E-08  -2.618829E-06  -2.787392E-07  -4.760217E-05  -9.186783E-06   4.247344E-03   0.999991      -1.229791E-04
  ao   10   2.803967E-03   2.998894E-03   2.985233E-02   0.104167       0.280811       0.766758      -3.191841E-03   0.593253    

               no    9        no   10

      occ   4.533568E-05   7.945470E-06

  ao    1   -1.00005       0.281887    
  ao    2    1.21678       -1.07687    
  ao    3  -0.468619       -1.46647    
  ao    4  -0.475267      -0.343499    
  ao    5   0.761425       0.793189    
  ao    6  -0.568915       0.645835    
  ao    7  -0.307774       0.151024    
  ao    8   0.147852       -1.01973    
  ao    9   8.983473E-06   2.896185E-05
  ao   10   0.371643       0.424681    


 total number of electrons =   10.0000000000

 test slabel:                        4
  a1  b1  a2  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
    H _ s       0.000046   0.237136   0.291742   0.030211   0.000179   0.000012
    H _ p       0.000129   0.031368  -0.000784  -0.001169  -0.000799   0.000043
    H _ d       0.000040   0.001876   0.000095  -0.000055   0.000000   0.000010
    F _ s       1.999784   1.602387   0.288556  -0.003309   0.007490   0.000251
    F _ p       0.000001   0.113237   1.380828   0.000540   0.000847   0.000897
    F _ d       0.000000   0.001999   0.005390   0.000148  -0.000017   0.003548
    F _ f       0.000000   0.000115   0.000238   0.000001   0.000000   0.000015

   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
    H _ s       0.000000  -0.000147   0.000319   0.000000   0.000011   0.000000
    H _ p       0.000000  -0.000095   0.000008   0.000000   0.000017   0.000000
    H _ d       0.000029  -0.000001   0.000002   0.000036   0.000002   0.000000
    F _ s       0.000000   0.000290   0.000014   0.000000   0.000006   0.000000
    F _ p       0.000000   0.000856   0.000221   0.000000   0.000090   0.000000
    F _ d       0.004409   0.000200   0.000097   0.000117   0.000043   0.000193
    F _ f       0.000003   0.000002   0.000083   0.000209   0.000183   0.000107

   ao class      13a1       14a1       15a1       16a1       17a1       18a1 
    H _ s       0.000073   0.000033   0.000023   0.000000   0.000034  -0.000010
    H _ p      -0.000027   0.000014   0.000005   0.000000   0.000001   0.000016
    H _ d      -0.000003   0.000005  -0.000009   0.000042   0.000006  -0.000001
    F _ s       0.000117  -0.000001   0.000021   0.000000  -0.000012   0.000003
    F _ p       0.000031   0.000001   0.000035   0.000000  -0.000001   0.000000
    F _ d       0.000004   0.000057  -0.000002   0.000002   0.000004   0.000002
    F _ f       0.000024   0.000019   0.000007   0.000003   0.000000   0.000000

   ao class      19a1       20a1 
    H _ s      -0.000001   0.000001
    H _ p       0.000006  -0.000001
    H _ d       0.000000   0.000004
    F _ s       0.000002   0.000000
    F _ p       0.000000   0.000000
    F _ d       0.000000   0.000000
    F _ f       0.000000   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
    H _ p       0.039048   0.001339   0.000393   0.000242   0.000126   0.000021
    H _ d       0.004218   0.000022   0.000078  -0.000028   0.000021   0.000008
    F _ p       1.931784   0.012462   0.000215   0.000387   0.000277   0.000011
    F _ d       0.001021   0.000135   0.004093   0.000127   0.000090   0.000082
    F _ f       0.000054  -0.000001   0.000010   0.000018   0.000070   0.000203

   ao class       7b1        8b1        9b1       10b1 
    H _ p       0.000000   0.000044   0.000031   0.000001
    H _ d       0.000000  -0.000012   0.000008   0.000007
    F _ p       0.000000   0.000002   0.000002   0.000000
    F _ d       0.000000   0.000048   0.000003   0.000000
    F _ f       0.000317   0.000017   0.000002   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
    H _ d       0.000029   0.000036   0.000000   0.000042
    F _ d       0.004408   0.000117   0.000193   0.000002
    F _ f       0.000003   0.000209   0.000107   0.000003

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
    H _ p       0.039048   0.001339   0.000393   0.000242   0.000126   0.000021
    H _ d       0.004218   0.000022   0.000078  -0.000028   0.000021   0.000008
    F _ p       1.931784   0.012462   0.000215   0.000387   0.000277   0.000011
    F _ d       0.001021   0.000135   0.004093   0.000127   0.000090   0.000082
    F _ f       0.000054  -0.000001   0.000010   0.000018   0.000070   0.000203

   ao class       7b2        8b2        9b2       10b2 
    H _ p       0.000000   0.000044   0.000031   0.000001
    H _ d       0.000000  -0.000012   0.000008   0.000007
    F _ p       0.000000   0.000002   0.000002   0.000000
    F _ d       0.000000   0.000048   0.000003   0.000000
    F _ f       0.000317   0.000017   0.000002   0.000000


                        gross atomic populations
     ao           H _        F _
      s         0.559662   3.895598
      p         0.111222   5.387864
      d         0.010829   0.032115
      f         0.000000   0.002709
    total       0.681713   9.318287


 Total number of electrons:   10.00000000

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0       0.7       0.7       0.0       0.0       1.6
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0       0.7       0.7       0.0       0.0       1.6
