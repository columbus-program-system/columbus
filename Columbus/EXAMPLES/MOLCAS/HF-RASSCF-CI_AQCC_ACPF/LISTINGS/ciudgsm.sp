1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     8)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.0823305449 -2.4869E-14  2.8280E-01  1.4291E+00  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -100.0823305449 -2.4869E-14  2.8280E-01  1.4291E+00  1.0000E-04

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.3156776260  2.3335E-01  1.2824E-02  2.7741E-01  1.0000E-04
 mr-sdci #  2  1   -100.3252316956  9.5541E-03  6.4856E-04  6.3800E-02  1.0000E-04
 mr-sdci #  3  1   -100.3258390436  6.0735E-04  4.8451E-05  1.6614E-02  1.0000E-04
 mr-sdci #  4  1   -100.3258868315  4.7788E-05  3.8881E-06  4.7806E-03  1.0000E-04
 mr-sdci #  5  1   -100.3258904911  3.6596E-06  3.3638E-07  1.3890E-03  1.0000E-04
 mr-sdci #  6  1   -100.3258907997  3.0861E-07  6.2669E-08  4.9498E-04  1.0000E-04
 mr-sdci #  7  1   -100.3258908677  6.8050E-08  9.2095E-09  2.3140E-04  1.0000E-04
 mr-sdci #  8  1   -100.3258908768  9.0644E-09  1.4226E-09  8.2504E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  8  1   -100.3258908768  9.0644E-09  1.4226E-09  8.2504E-05  1.0000E-04

 number of reference csfs (nref) is     8.  root number (iroot) is  1.
 c0**2 =   0.96063985  c**2 (all zwalks) =   0.96063985

 eref      =   -100.081742223553   "relaxed" cnot**2         =   0.960639850925
 eci       =   -100.325890876795   deltae = eci - eref       =  -0.244148653242
 eci+dv1   =   -100.335500604183   dv1 = (1-cnot**2)*deltae  =  -0.009609727388
 eci+dv2   =   -100.335894342068   dv2 = dv1 / cnot**2       =  -0.010003465273
 eci+dv3   =   -100.336321723550   dv3 = dv1 / (2*cnot**2-1) =  -0.010430846755
 eci+pople =   -100.334093030341   ( 10e- scaled deltae )    =  -0.252350806788
