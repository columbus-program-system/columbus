1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   115)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7042693745 -3.9080E-14  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -25.7042693745 -3.9080E-14  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7560331435  5.1764E-02  2.6026E-03  7.2622E-02  1.0000E-05
 mr-sdci #  1  2    -25.6559723480  3.5527E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  2  1    -25.7581747841  2.1416E-03  2.2698E-04  2.0055E-02  1.0000E-05
 mr-sdci #  2  2    -25.6559723480  0.0000E+00  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  3  1    -25.7583626944  1.8791E-04  2.9481E-05  7.5543E-03  1.0000E-05
 mr-sdci #  3  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  4  1    -25.7583892866  2.6592E-05  3.8323E-06  2.6993E-03  1.0000E-05
 mr-sdci #  4  2    -25.6559723480  7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  5  1    -25.7583929726  3.6860E-06  6.2756E-07  1.0565E-03  1.0000E-05
 mr-sdci #  5  2    -25.6559723480  7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  6  1    -25.7583935068  5.3419E-07  1.5451E-07  5.2101E-04  1.0000E-05
 mr-sdci #  6  2    -25.6559723480 -3.5527E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  7  1    -25.7583936502  1.4342E-07  2.5360E-08  2.1071E-04  1.0000E-05
 mr-sdci #  7  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  8  1    -25.7583936716  2.1421E-08  4.3006E-09  8.6648E-05  1.0000E-05
 mr-sdci #  8  2    -25.6559723480  1.4211E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  9  1    -25.7583936757  4.0516E-09  1.1811E-09  4.5509E-05  1.0000E-05
 mr-sdci #  9  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci # 10  1    -25.7583936769  1.1602E-09  1.6466E-10  1.6900E-05  1.0000E-05
 mr-sdci # 10  2    -25.6559723480  1.0658E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci # 11  1    -25.7583936770  1.4296E-10  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 11  2    -25.6559723480  7.1054E-15  5.9626E-02  3.3458E-01  1.0000E-05
 mr-sdci # 12  1    -25.7583936770 -3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 12  2    -25.7131138163  5.7141E-02  3.4924E-03  7.8109E-02  1.0000E-05
 mr-sdci # 13  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 13  2    -25.7158825107  2.7687E-03  4.3482E-04  2.6143E-02  1.0000E-05
 mr-sdci # 14  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 14  2    -25.7162555665  3.7306E-04  7.4632E-05  1.1041E-02  1.0000E-05
 mr-sdci # 15  1    -25.7583936770  7.1054E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 15  2    -25.7163224793  6.6913E-05  2.5044E-05  5.5673E-03  1.0000E-05
 mr-sdci # 16  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 16  2    -25.7163495755  2.7096E-05  8.2335E-06  3.4782E-03  1.0000E-05
 mr-sdci # 17  1    -25.7583936770 -1.0658E-14  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 17  2    -25.7163562628  6.6873E-06  8.1351E-07  1.2511E-03  1.0000E-05
 mr-sdci # 18  1    -25.7583936770 -3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 18  2    -25.7163569904  7.2762E-07  1.6741E-07  4.8429E-04  1.0000E-05
 mr-sdci # 19  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 19  2    -25.7163571431  1.5265E-07  2.9620E-08  2.0926E-04  1.0000E-05
 mr-sdci # 20  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 20  2    -25.7163571675  2.4449E-08  6.4160E-09  1.0540E-04  1.0000E-05
 mr-sdci # 21  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 21  2    -25.7163571724  4.8740E-09  1.6761E-09  4.4270E-05  1.0000E-05
 mr-sdci # 22  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 22  2    -25.7163571742  1.7743E-09  4.7192E-10  2.6429E-05  1.0000E-05
 mr-sdci # 23  1    -25.7583936770 -7.1054E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 23  2    -25.7163571746  4.4491E-10  1.0720E-10  1.4175E-05  1.0000E-05
 mr-sdci # 24  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.9333E-11  2.5237E-11  5.5303E-06  1.0000E-05

 mr-sdci  convergence criteria satisfied after 24 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 24  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.9333E-11  2.5237E-11  5.5303E-06  1.0000E-05

 number of reference csfs (nref) is   115.  root number (iroot) is  1.
 c0**2 =   0.96965664  c**2 (all zwalks) =   0.96965664

 eref      =    -25.703701602666   "relaxed" cnot**2         =   0.969656643738
 eci       =    -25.758393676995   deltae = eci - eref       =  -0.054692074329
 eci+dv1   =    -25.760053218091   dv1 = (1-cnot**2)*deltae  =  -0.001659541096
 eci+dv2   =    -25.760105149924   dv2 = dv1 / cnot**2       =  -0.001711472929
 eci+dv3   =    -25.760160436944   dv3 = dv1 / (2*cnot**2-1) =  -0.001766759949
 eci+pople =    -25.759632521201   (  7e- scaled deltae )    =  -0.055930918534

 number of reference csfs (nref) is   115.  root number (iroot) is  2.
 c0**2 =   0.96246688  c**2 (all zwalks) =   0.96246688

 eref      =    -25.654790727398   "relaxed" cnot**2         =   0.962466880261
 eci       =    -25.716357174689   deltae = eci - eref       =  -0.061566447291
 eci+dv1   =    -25.718667955527   dv1 = (1-cnot**2)*deltae  =  -0.002310780838
 eci+dv2   =    -25.718758068565   dv2 = dv1 / cnot**2       =  -0.002400893875
 eci+dv3   =    -25.718855495041   dv3 = dv1 / (2*cnot**2-1) =  -0.002498320352
 eci+pople =    -25.718100687934   (  7e- scaled deltae )    =  -0.063309960536
