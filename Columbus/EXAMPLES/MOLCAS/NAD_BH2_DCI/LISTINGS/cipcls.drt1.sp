
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title DRT#1                                                              
 nmot  =    24 niot  =     7 nfct  =     0 nfvt  =     0
 nrow  =    47 nsym  =     2 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:        580      210      210       90       70
 nvalwt,nvalw:      485      115      210       90       70
 ncsft:           13970
 map(*)=    18 19 20 21 22 23  1  2  3  4  5  6  7  8  9 10 11 12 24 13
            14 15 16 17
 mu(*)=      0  0  0  0  0  0  0
 syml(*) =   1  1  1  1  1  1  2
 rmo(*)=     1  2  3  4  5  6  1

 indx01:   485 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title DRT#1                                                              
 energy computed by program ciudg.       zam216            13:32:57.603 10-Oct-08

 lenrec =   32768 lenci =     13970 ninfo =  6 nenrgy =  3 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       98% overlap
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -2.575839367700E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  7.929578883964E-06, ietype=-2055, cnvginf energy of type: CI-Resid
==================================================================================
===================================ROOT # 2===================================

 rdhciv: CI vector file information:
  cidrt_title DRT#1                                                              
 energy computed by program ciudg.       zam216            13:32:57.603 10-Oct-08

 lenrec =   32768 lenci =     13970 ninfo =  6 nenrgy =  7 ntitle =  2

 Max. overlap with ref vector #        2
 Valid ci vector #        2
 Method:        0       98% overlap
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -2.575839367700E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  7.929578883964E-06, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)= -2.571635717469E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 5)=  5.530291038744E-06, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 6)=  8.933298545344E-11, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 7)=  2.523673868980E-11, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   4357506 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      86 coefficients were selected.
 workspace: ncsfmx=   13970
 ncsfmx=                    13970

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   13970 ncsft =   13970 ncsf =      86
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       7 *******
 3.1250E-02 <= |c| < 6.2500E-02      10 **********
 1.5625E-02 <= |c| < 3.1250E-02      30 ******************************
 7.8125E-03 <= |c| < 1.5625E-02      38 **************************************
 3.9063E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9063E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       86 total stored =      86

 from the selected csfs,
 min(|csfvec(:)|) = 1.0060E-02    max(|csfvec(:)|) = 9.4732E-01
 norm=   0.9999999999999970     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   13970 ncsft =   13970 ncsf =      86

 i:slabel(i) =  1: a'   2: a" 

 internal level =    1    2    3    4    5    6    7
 syml(*)        =    1    1    1    1    1    1    2
 label          =  a'   a'   a'   a'   a'   a'   a" 
 rmo(*)         =    1    2    3    4    5    6    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.94732 0.89742 z*                    3331000
        5  0.11943 0.01426 z*                    3312100
        7 -0.10193 0.01039 z*                    3311200
       16  0.09567 0.00915 z*                    3301300
        3  0.07401 0.00548 z*                    3330010
       25  0.07329 0.00537 z*                    3133000
       39  0.07138 0.00510 z*                    3121120
       35 -0.06748 0.00455 z*                    3123100
       34 -0.05748 0.00330 z*                    3130003
       81  0.04773 0.00228 z*                    3031030
       38  0.04380 0.00192 z*                    3121210
       82  0.04064 0.00165 z*                    3031003
      145  0.03855 0.00149 y           a' : 12  13320100
       19  0.03685 0.00136 z*                    3301030
       15 -0.03615 0.00131 z*                    3303010
      836  0.03572 0.00128 y           a" :  4  13121002
       78  0.03378 0.00114 z*                    3031300
      120  0.03178 0.00101 y           a' : 11  13330000
       20  0.02935 0.00086 z*                    3301003
       29 -0.02915 0.00085 z*                    3131020
     8399 -0.02776 0.00077 w           a' : 12  33301000
     8815  0.02764 0.00076 w  a' :  8  a' :  9 123121000
     8381 -0.02751 0.00076 w           a' :  8  33301000
      166  0.02716 0.00074 y           a" :  4  13320001
      176 -0.02500 0.00063 y           a' : 14  13312000
       10  0.02448 0.00060 z*                    3310210
       11  0.02444 0.00060 z*                    3310120
     8819  0.02435 0.00059 w  a' :  9  a' : 10 123121000
     1240 -0.02323 0.00054 y           a" :  3  13031002
      823  0.02319 0.00054 y           a' :  8  13121020
      122  0.02308 0.00053 y           a' : 13  13330000
    10784 -0.02242 0.00050 w           a' :  9  33031000
     2006 -0.02232 0.00050 x  a' :  7  a' :  8 113320000
      807 -0.02096 0.00044 y           a" :  4  13122001
      765 -0.02043 0.00042 y           a" :  3  13130002
       77 -0.01974 0.00039 z*                    3033010
      185  0.01965 0.00039 y           a' : 11  13310200
     8893  0.01916 0.00037 w  a" :  3  a" :  4 123121000
     8462 -0.01905 0.00036 w           a" :  4  33301000
    10859 -0.01820 0.00033 w           a" :  3  33031000
     8386 -0.01804 0.00033 w  a' :  8  a' : 10 123301000
      829  0.01747 0.00031 y           a' : 14  13121020
     2448  0.01746 0.00030 x  a' :  8  a' :  9 113221000
       46 -0.01701 0.00029 z*                    3113200
     8287 -0.01688 0.00028 w  a' :  7  a' :  8 123310000
      129 -0.01639 0.00027 y           a' :  8  13321000
       49 -0.01613 0.00026 z*                    3112210
      181  0.01592 0.00025 y           a' :  7  13310200
      732 -0.01541 0.00024 y           a' : 11  13132000
     8297 -0.01531 0.00023 w  a' :  8  a' : 11 123310000
      447  0.01462 0.00021 y           a' : 12  13211200
     2013  0.01430 0.00020 x  a' :  8  a' : 11 113320000
     8388 -0.01429 0.00020 w           a' : 10  33301000
     8896  0.01415 0.00020 w  a" :  3  a" :  5 123121000
      207 -0.01408 0.00020 y           a" :  4  13310002
     8384 -0.01397 0.00020 w           a' :  9  33301000
     1211  0.01397 0.00020 y           a" :  3  13032001
     1229 -0.01366 0.00019 y           a' :  9  13031020
    10799 -0.01363 0.00019 w           a' : 12  33031000
      747  0.01344 0.00018 y           a' : 14  13130200
     2032  0.01344 0.00018 x  a' : 12  a' : 14 113320000
      172  0.01297 0.00017 y           a' : 10  13312000
      752  0.01286 0.00017 y           a' :  7  13130020
      418 -0.01284 0.00016 y           a' : 12  13212100
     8861  0.01269 0.00016 w  a' : 12  a' : 16 123121000
     2074  0.01244 0.00015 x  a" :  3  a" :  4 113320000
      800 -0.01237 0.00015 y           a' : 14  13122010
       27 -0.01219 0.00015 z*                    3132010
       33  0.01218 0.00015 z*                    3130030
      124 -0.01197 0.00014 y           a' : 15  13330000
      794 -0.01187 0.00014 y           a' :  8  13122010
     8902  0.01185 0.00014 w  a" :  5  a" :  6 123121000
      121  0.01169 0.00014 y           a' : 12  13330000
     8879 -0.01145 0.00013 w  a' :  9  a' : 18 123121000
      764 -0.01141 0.00013 y           a" :  2  13130002
      365  0.01124 0.00013 y           a' : 12  13231000
     1176 -0.01096 0.00012 y           a' :  9  13033000
     8844 -0.01092 0.00012 w  a' : 12  a' : 14 123121000
      397 -0.01082 0.00012 y           a" :  3  13230001
      362  0.01077 0.00012 y           a' :  9  13231000
      373  0.01075 0.00012 y           a' :  8  13230100
     2457  0.01067 0.00011 x  a' :  8  a' : 12 113221000
     8414 -0.01048 0.00011 w           a' : 14  33301000
      109  0.01045 0.00011 z*                    3001330
      215 -0.01026 0.00011 y           a' : 12  13303000
      455 -0.01006 0.00010 y           a' :  8  13211020
           86 csfs were printed in this range.

================================================================================
===================================VECTOR # 2===================================
================================================================================


 rdcivnew:     100 coefficients were selected.
 workspace: ncsfmx=   13970
 ncsfmx=                    13970

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   13970 ncsft =   13970 ncsf =     100
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       3 **
 3.1250E-02 <= |c| < 6.2500E-02      16 ********
 1.5625E-02 <= |c| < 3.1250E-02      29 ***************
 7.8125E-03 <= |c| < 1.5625E-02      50 *************************
 3.9063E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9063E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      100 total stored =     100

 from the selected csfs,
 min(|csfvec(:)|) = 1.0037E-02    max(|csfvec(:)|) = 9.3810E-01
 norm=   0.9999999999999938     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   13970 ncsft =   13970 ncsf =     100

 i:slabel(i) =  1: a'   2: a" 

 internal level =    1    2    3    4    5    6    7
 syml(*)        =    1    1    1    1    1    1    2
 label          =  a'   a'   a'   a'   a'   a'   a" 
 rmo(*)         =    1    2    3    4    5    6    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        4 -0.93810 0.88004 z*                    3313000
       13  0.17786 0.03163 z*                    3310003
       26  0.09242 0.00854 z*                    3132100
        6  0.09053 0.00820 z*                    3312010
        9  0.08317 0.00692 z*                    3310300
       52  0.06027 0.00363 z*                    3112003
       60 -0.05264 0.00277 z*                    3103120
       18  0.05052 0.00255 z*                    3301120
        8  0.04812 0.00232 z*                    3311020
       36  0.04527 0.00205 z*                    3123010
      774 -0.04496 0.00202 y           a' : 12  13123000
       28  0.04460 0.00199 z*                    3131200
       90 -0.04396 0.00193 z*                    3013030
       12  0.03923 0.00154 z*                    3310030
      206  0.03738 0.00140 y           a" :  3  13310002
       91 -0.03509 0.00123 z*                    3013003
      173  0.03431 0.00118 y           a' : 11  13312000
        2 -0.03431 0.00118 z*                    3330100
      174  0.03427 0.00117 y           a' : 12  13312000
       14  0.03359 0.00113 z*                    3303100
     8296 -0.03284 0.00108 w  a' :  7  a' : 11 123310000
       87 -0.03121 0.00097 z*                    3013300
     8286 -0.02973 0.00088 w           a' :  7  33310000
      256 -0.02884 0.00083 y           a' : 12  13301200
       76  0.02803 0.00079 z*                    3033100
      188 -0.02800 0.00078 y           a' : 14  13310200
       41 -0.02788 0.00078 z*                    3121003
      226 -0.02784 0.00077 y           a' : 11  13302100
      277 -0.02638 0.00070 y           a" :  4  13301002
      996 -0.02585 0.00067 y           a" :  4  13103002
     8300 -0.02538 0.00064 w           a' : 11  33310000
      165 -0.02528 0.00064 y           a" :  3  13320001
      197  0.02475 0.00061 y           a' : 11  13310020
      132 -0.02411 0.00058 y           a' : 11  13321000
     1526  0.02297 0.00053 y           a" :  3  13013002
     1177 -0.02199 0.00048 y           a' : 10  13033000
    11123  0.02192 0.00048 w           a' :  9  33013000
      123  0.02166 0.00047 y           a' : 14  13330000
     9497 -0.01948 0.00038 w  a' :  9  a' : 10 123103000
     1175 -0.01881 0.00035 y           a' :  8  13033000
      147  0.01854 0.00034 y           a' : 14  13320100
      989 -0.01836 0.00034 y           a' : 14  13103020
       51 -0.01835 0.00034 z*                    3112030
     9493 -0.01826 0.00033 w  a' :  8  a' :  9 123103000
    11198  0.01807 0.00033 w           a" :  3  33013000
       47  0.01765 0.00031 z*                    3113020
      175  0.01747 0.00031 y           a' : 13  13312000
      771  0.01709 0.00029 y           a' :  9  13123000
      944  0.01600 0.00026 y           a" :  3  13112002
      943  0.01571 0.00025 y           a" :  2  13112002
      775 -0.01550 0.00024 y           a' : 13  13123000
      198  0.01527 0.00023 y           a' : 12  13310020
      134 -0.01506 0.00023 y           a' : 13  13321000
      128 -0.01482 0.00022 y           a' :  7  13321000
    11138  0.01471 0.00022 w           a' : 12  33013000
      199  0.01460 0.00021 y           a' : 13  13310020
      935 -0.01457 0.00021 y           a' : 11  13112020
      217 -0.01435 0.00021 y           a' : 14  13303000
     8390 -0.01425 0.00020 w  a' :  8  a' : 11 123301000
      983 -0.01413 0.00020 y           a' :  8  13103020
     8321 -0.01387 0.00019 w           a' : 14  33310000
     1515  0.01382 0.00019 y           a' :  9  13013020
      156 -0.01376 0.00019 y           a' : 11  13320010
     9571 -0.01374 0.00019 w  a" :  3  a" :  4 123103000
       64 -0.01351 0.00018 z*                    3102130
     8366 -0.01344 0.00018 w           a" :  3  33310000
     2082  0.01341 0.00018 x  a' :  7  a' :  8 113302000
       59 -0.01340 0.00018 z*                    3103210
     1349 -0.01327 0.00018 y           a" :  3  13023001
     8380 -0.01321 0.00017 w  a' :  7  a' :  8 123301000
      211 -0.01311 0.00017 y           a' :  8  13303000
       74  0.01308 0.00017 z*                    3100123
      926 -0.01304 0.00017 y           a' : 14  13112200
     2089 -0.01250 0.00016 x  a' :  8  a' : 11 113302000
      227 -0.01247 0.00016 y           a' : 12  13302100
      467  0.01239 0.00015 y           a" :  3  13211002
      458 -0.01235 0.00015 y           a' : 11  13211020
       45  0.01233 0.00015 z*                    3120013
      205  0.01214 0.00015 y           a" :  2  13310002
     9574 -0.01192 0.00014 w  a" :  3  a" :  5 123103000
      403  0.01177 0.00014 y           a' :  9  13213000
      177 -0.01152 0.00013 y           a' : 15  13312000
      104  0.01145 0.00013 z*                    3010033
       85  0.01140 0.00013 z*                    3030103
      454 -0.01106 0.00012 y           a' :  7  13211020
      222 -0.01105 0.00012 y           a' :  7  13302100
      931 -0.01103 0.00012 y           a' :  7  13112020
      443 -0.01082 0.00012 y           a' :  8  13211200
      788 -0.01074 0.00012 y           a' : 14  13122100
      230 -0.01068 0.00011 y           a' : 15  13302100
      556  0.01061 0.00011 y           a' : 12  13203100
      806  0.01055 0.00011 y           a" :  3  13122001
      406  0.01043 0.00011 y           a' : 12  13213000
     8387  0.01039 0.00011 w  a' :  9  a' : 10 123301000
      157 -0.01027 0.00011 y           a' : 12  13320010
     8306 -0.01009 0.00010 w           a' : 12  33310000
     9162 -0.01008 0.00010 w  a' :  9  a' : 11 123112000
       40  0.01008 0.00010 z*                    3121030
     3024 -0.01007 0.00010 x  a' :  8  a' :  9 113203000
      773 -0.01004 0.00010 y           a' : 11  13123000
          100 csfs were printed in this range.
