License is going to expire in 253 days on Thursday April 30th, 2009
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.0 patchlevel 266          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                                               
                              ^^^^^^^       ^^^^ ^^^                                               
                              ^^^ ^^^       ^^^^ ^^^                                               
                              ^^^ ^^^^      ^^^  ^^^                                               
                              ^^^  ^^ ^^^^^  ^^  ^^^^                                              
                             ^^^^      ^^^^   ^   ^^^                                              
                             ^   ^^^   ^^^^   ^^^^  ^                                              
                            ^   ^^^^    ^^    ^^^^   ^                                             
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                                         
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                                      
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^                                  
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^                                
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^                                
               ^^^^^^^^^^^^^                          ^^^      ^^^^                                
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^                                
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2007.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  

 COLUMBUS MR-CISD gradients using ALASKA 


()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module ALASKA with 200 MB of memory                                  
                                              at 14:11:07 Wed Aug 20 2008                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()



                     Threshold for contributions to the gradient: .100E-06


                    ********************************************
                    * Symmetry Adapted Cartesian Displacements *
                    ********************************************


           Irreducible representation : ag 
           Basis function(s) of irrep:                                                                                 

 Basis Label        Type   Center Phase
   1   C1           x         1     1      2    -1
   2   C2           x         3     1      4    -1      5     1      6    -1
   3   C2           z         3     1      4     1      5    -1      6    -1
   4   H1           x         7     1      8    -1
   5   H2           x         9     1     10    -1     11     1     12    -1
   6   H2           z         9     1     10     1     11    -1     12    -1

           Irreducible representation : b3u
           Basis function(s) of irrep: x                                                                               

 Basis Label        Type   Center Phase
   7   C1           x         1     1      2     1
   8   C2           x         3     1      4     1      5     1      6     1
   9   C2           z         3     1      4    -1      5    -1      6     1
  10   H1           x         7     1      8     1
  11   H2           x         9     1     10     1     11     1     12     1
  12   H2           z         9     1     10    -1     11    -1     12     1

           Irreducible representation : b2u
           Basis function(s) of irrep: y                                                                               

 Basis Label        Type   Center Phase
  13   C1           y         1     1      2     1
  14   C2           y         3     1      4     1      5     1      6     1
  15   H1           y         7     1      8     1
  16   H2           y         9     1     10     1     11     1     12     1

           Irreducible representation : b1g
           Basis function(s) of irrep: xy, Rz                                                                          

 Basis Label        Type   Center Phase
  17   C1           y         1     1      2    -1
  18   C2           y         3     1      4    -1      5     1      6    -1
  19   H1           y         7     1      8    -1
  20   H2           y         9     1     10    -1     11     1     12    -1

           Irreducible representation : b1u
           Basis function(s) of irrep: z                                                                               

 Basis Label        Type   Center Phase
  21   C1           z         1     1      2     1
  22   C2           x         3     1      4    -1      5    -1      6     1
  23   C2           z         3     1      4     1      5     1      6     1
  24   H1           z         7     1      8     1
  25   H2           x         9     1     10    -1     11    -1     12     1
  26   H2           z         9     1     10     1     11     1     12     1

           Irreducible representation : b2g
           Basis function(s) of irrep: xz, Ry                                                                          

 Basis Label        Type   Center Phase
  27   C1           z         1     1      2    -1
  28   C2           x         3     1      4     1      5    -1      6    -1
  29   C2           z         3     1      4    -1      5     1      6    -1
  30   H1           z         7     1      8    -1
  31   H2           x         9     1     10     1     11    -1     12    -1
  32   H2           z         9     1     10    -1     11     1     12    -1

           Irreducible representation : b3g
           Basis function(s) of irrep: yz, Rx                                                                          

 Basis Label        Type   Center Phase
  33   C2           y         3     1      4     1      5    -1      6    -1
  34   H2           y         9     1     10     1     11    -1     12    -1

           Irreducible representation : au 
           Basis function(s) of irrep: I                                                                               

 Basis Label        Type   Center Phase
  35   C2           y         3     1      4    -1      5    -1      6     1
  36   H2           y         9     1     10    -1     11    -1     12     1

                     No automatic utilization of translational and rotational invariance of the energy is employed.


 ******************************************************************
 *                                                                *
 *              The Nuclear repulsion  contribution               *
 *                                                                *
 ******************************************************************

                Irreducible representation: ag 

                C1       x                 0.8346118E+01
                C2       x                 0.4173220E+01
                C2       z                -0.7227640E+01
                H1       x                 0.2478783E+01
                H2       x                 0.1239553E+01
                H2       z                -0.2146714E+01


 *********************************************************
 *                                                       *
 *              Total Nuclear contribution               *
 *                                                       *
 *********************************************************

                Irreducible representation: ag 

                C1       x                 0.8346118E+01
                C2       x                 0.4173220E+01
                C2       z                -0.7227640E+01
                H1       x                 0.2478783E+01
                H2       x                 0.1239553E+01
                H2       z                -0.2146714E+01


 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Renormalization Contribution                                      *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: ag 

                C1       x                -0.4237496E-01
                C2       x                -0.2120739E-01
                C2       z                 0.3665152E-01
                H1       x                -0.2225329E+00
                H2       x                -0.1113250E+00
                H2       z                 0.1927658E+00


 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Kinetic Energy Contribution                                       *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: ag 

                C1       x                 0.2357164E+00
                C2       x                 0.1178387E+00
                C2       z                -0.2040051E+00
                H1       x                 0.4577417E+00
                H2       x                 0.2289535E+00
                H2       z                -0.3964452E+00


 **************************************************************************************************************
 *                                                                                                            *
 *                                    The Nuclear Attraction Contribution                                     *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: ag 

                C1       x                -0.1703215E+02
                C2       x                -0.8514672E+01
                C2       z                 0.1474845E+02
                H1       x                -0.7803605E+01
                H2       x                -0.3902891E+01
                H2       z                 0.6758130E+01

 Conventional ERI gradients!
 A total of 56133732. entities were prescreened and 34380157. were kept.

 ********************************************************
 *                                                      *
 *              Two-electron contribution               *
 *                                                      *
 ********************************************************

                Irreducible representation: ag 

                C1       x                 0.8407942E+01
                C2       x                 0.4202406E+01
                C2       z                -0.7280145E+01
                H1       x                 0.4644545E+01
                H2       x                 0.2323049E+01
                H2       z                -0.4022211E+01


 **************************************************
 *                                                *
 *              Molecular gradients               *
 *                                                *
 **************************************************

                Irreducible representation: ag 

                C1       x                -0.1263481E-05
                C2       x                -0.2142454E-07
                C2       z                 0.4923178E-05
                H1       x                -0.1911958E-05
                H2       x                -0.1023993E-04
                H2       z                -0.7338754E-05


  CGrad                                                       
  mat. size =     3x   12
   -0.00000126  0.00000126 -0.00000002  0.00000002 -0.00000002  0.00000002 -0.00000191  0.00000191 -0.00001024  0.00001024
   -0.00001024  0.00001024
    0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000
    0.00000000  0.00000000
    0.00000000  0.00000000  0.00000492  0.00000492 -0.00000492 -0.00000492  0.00000000  0.00000000 -0.00000734 -0.00000734
    0.00000734  0.00000734
--- Stop Module: alaska at Wed Aug 20 14:11:13 2008 /rc=                        0 ---

     Happy landing!

--- Stop Module: auto at Mi Aug 20 14:11:13 2008 /rc=0 ---
