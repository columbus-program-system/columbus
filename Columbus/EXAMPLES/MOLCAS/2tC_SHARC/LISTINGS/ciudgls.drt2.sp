1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       945      128979           0           0      129924
      internal walks       945        2691           0           0        3636
valid internal walks       945        2529           0           0        3474
  MR-CIS calculation - skip 3- and 4-external 2e integrals
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  5551 ci%nnlev=                  2775  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              52384982              52366026
 lencor,maxblo              52428800                 60000
========================================
 current settings:
 minbl3           0
 minbl4           0
 locmaxbl3   135154
 locmaxbuf    67577
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================
 Orig.  diagonal integrals:  1electron:        74
                             0ext.    :       552
                             2ext.    :      2346
                             4ext.    :      2652


 Orig. off-diag. integrals:  4ext.    :         0
                             3ext.    :         0
                             2ext.    :   1097928
                             1ext.    :    351900
                             0ext.    :     44022
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:      1326


 Sorted integrals            3ext.  w :         0 x :         0
                             4ext.  w :         0 x :         0


Cycle #  1 sortfile size=   3670016(       7 records of   524288) #buckets=   2
distributed memory consumption per node=         0 available core  52384982
Cycle #  2 sortfile size=   3670016(       7 records of   524288) #buckets=   2
distributed memory consumption per node=         0 available core  52384982
 minimum size of srtscr:   2621440 WP (     5 records)
 maximum size of srtscr:   3670016 WP (     7 records)
diagi   file:      4 records  of   6144 WP each=>      24576 WP total
ofdgi   file:    246 records  of   6144 WP each=>    1511424 WP total
fil3w   file:      0 records  of  30006 WP each=>          0 WP total
fil3x   file:      0 records  of  30006 WP each=>          0 WP total
fil4w   file:      0 records  of  30006 WP each=>          0 WP total
fil4x   file:      0 records  of  30006 WP each=>          0 WP total
 compressed index vector length=                   689
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 42
   nroot=2
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 5
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 150
  NVCIMN = 9
   rtolci=1e-3,1e-3
  NVCIMX = 20
  NVRFMX = 20
  NVBKMX = 10
   iden=2
  CSFPRN = 10,
  update_mode=1
  nvrfmn=3
   MOLCAS=1
 /&end
 transition
   1  1  1  2
 ------------------------------------------------------------------------
transition densities: resetting nroot to    2
lodens (list->root)=  1  2
invlodens (root->list)=  1  2
 bummer (warning):resetting fileloc for seriel operation0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    2      noldv  =   0      noldhv =   0
 nunitv =    9      nbkitr =    1      niter  = 150      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   10      ibktv  =  -1      ibkthv =  -1
 nvcimx =   20      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    5      nvcimn =    9      maxseg = 300      nrfitr =  30
 ncorel =   42      nvrfmx =   20      nvrfmn =   3      iden   =   2
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-03
    2        1.000E-04    1.000E-03
 Computing density:                    .drt1.state1
 Computing density:                    .drt1.state2
 Computing transition density:          drt1.state1-> drt1.state2 (.trd1to2)
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           52428799 DP per process
 gapointer%node_offset(*)=                     0                     0
 gapointer%node_width(*)=                     0                     0

********** Integral sort section *************


 workspace allocation information: lencor=  52428799

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =  412.532574920                                          
 MCSCF energy =    -711.472179149                                                
 SIFS file created by program tran.      LIN-CMFP2-1.LUNET 17:56:49.247 21-Jan-21

 input energy(*) values:
 energy( 1)=  4.125325749203E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   4.125325749203E+02

 nsym = 1 nmot=  74

 symmetry  =    1
 slabel(*) = a   
 nmpsy(*)  =   74

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044  45:tout:045  46:tout:046  47:tout:047  48:tout:048  49:tout:049  50:tout:050
  51:tout:051  52:tout:052  53:tout:053  54:tout:054  55:tout:055  56:tout:056  57:tout:057  58:tout:058  59:tout:059  60:tout:060
  61:tout:061  62:tout:062  63:tout:063  64:tout:064  65:tout:065  66:tout:066  67:tout:067  68:tout:068  69:tout:069  70:tout:070
  71:tout:071  72:tout:072  73:tout:073  74:tout:074

 input parameters:
 prnopt=  0
 ldamin=   32767 ldamax=  524288 ldainc=    4096
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    6144
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  86 nfctd =  12 nfvtc =   0 nmot  =  74
 nlevel =  74 niot  =  23 lowinl=  52
 orbital-to-level map(*)
   -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  52  53  54  55  56  57  58  59
   60  61  62  63  64  65  66  67  68  69  70  71  72  73  74   1   2   3   4   5
    6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25
   26  27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45
   46  47  48  49  50  51
 compressed map(*)
   52  53  54  55  56  57  58  59  60  61  62  63  64  65  66  67  68  69  70  71
   72  73  74   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17
   18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37
   38  39  40  41  42  43  44  45  46  47  48  49  50  51
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  0.  0.
   0.  0.  0.
  ... transition density matrix calculation requested 
  ... setting rmu(*) to zero 

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -8.812792647683E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      5550
 number with all external indices:      2652
 number with half external - half internal indices:      2346
 number with all internal indices:       552

 indxof: off-diagonal integral statistics.
    4-external integrals: num=     944775 strt=          1
    3-external integrals: num=    1612875 strt=     944776
    2-external integrals: num=    1097928 strt=    2557651
    1-external integrals: num=     351900 strt=    3655579
    0-external integrals: num=      44022 strt=    4007479

 total number of off-diagonal integrals:     4051500


 indxof(2nd)  ittp=   3 numx(ittp)=     1097928
 indxof(2nd)  ittp=   4 numx(ittp)=      351900
 indxof(2nd)  ittp=   5 numx(ittp)=       44022

 intermediate da file sorting parameters:
 nbuk=   2 lendar=  524288 nipbk=  349524 nipsg=  51432198
 pro2e        1    2776    5551    8326   11101   11377   11653   14428  713476 1412524
  1936812 1945004 1950464 1972303

 pro2e:   3851700 integrals read in   706 records.

 pro2e:   2435199 integrals 34-ext integrals skipped.
 pro1e        1    2776    5551    8326   11101   11377   11653   14428  713476 1412524
  1936812 1945004 1950464 1972303
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     7  records of                 524288 
 WP =              29360128 Bytes
 putdg        1    2776    5551    8326   14470  538758  888283   14428  713476 1412524
  1936812 1945004 1950464 1972303

 putf:       4 buffers of length    6144 written to file 12
 diagonal integral file completed.
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd   1097928         3   2557651   2557651   1097928   4051500
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd    351900         4   3655579   3655579    351900   4051500
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     44022         5   4007479   4007479     44022   4051500

 putf:     246 buffers of length    6144 written to file 13
 off-diagonal files sort completed.
 !timer: cisrt complete                  cpu_time=     0.286 walltime=     0.565
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  6144
 diagfile 4ext:    2652 2ext:    2346 0ext:     552
 fil4w,fil4x  :  944775 fil3w,fil3x : 1612875
 ofdgint  2ext: 1097928 1ext:  351900 0ext:   44022so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    3975 minbl3    3975 maxbl2    3978nbas:  51   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  52428799

 core energy values from the integral file:
 energy( 1)=  4.125325749203E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.812792647683E+02, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.687466898479E+02
 nmot  =    86 niot  =    23 nfct  =    12 nfvt  =     0
 nrow  =   224 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       3636      945     2691        0        0
 nvalwt,nvalw:     3474      945     2529        0        0
 ncsft:          129924
 total number of valid internal walks:    3474
 nvalz,nvaly,nvalx,nvalw =      945    2529       0       0

 cisrt info file parameters:
 file number  12 blocksize   6144
 mxbld   6144
 nd4ext,nd2ext,nd0ext  2652  2346   552
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int   944775  1612875  1097928   351900    44022        0        0        0
 minbl4,minbl3,maxbl2  3975  3975  3978
 maxbuf 30006
 number of external orbitals per symmetry block:  51
 nmsym   1 number of internal orbitals  23
 bummer (warning):transition densities: resetting ref occupation number to 00
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    23                     1
 block size     0
 pthz,pthy,pthx,pthw:   945  2691     0     0 total internal walks:    3636
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
setref: retained number of references =    45
 setref: total/valid number of walks=                   945
                   945
 nmb.of records onel     1
 nmb.of records 2-ext   179
 nmb.of records 1-ext    58
 nmb.of records 0-ext     8
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            70083
    threx             60019
    twoex             33735
    onex              11551
    allin              6144
    diagon             7292
               =======
   maximum            70083
 
  __ static summary __ 
   reflst               945
   hrfspc               945
               -------
   static->             945
 
  __ core required  __ 
   totstc               945
   max n-ex           70083
               -------
   totnec->           71028
 
  __ core available __ 
   totspc          52428799
   totnec -           71028
               -------
   totvec->        52357771

 number of external paths / symmetry
 vertex x    1275
 vertex w    1326
segment: free space=    52357771
 reducing frespc by                 18172 to               52339599 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         945|       945|         0|       945|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        2691|    128979|       945|      2529|       945|         2|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=         652DP  conft+indsym=       10116DP  drtbuffer=        7404 DP

dimension of the ci-matrix ->>>    129924

 executing brd_struct for civct
 gentasklist: ntask=                     6
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1    2691     945     128979        945    2529     945
     2  1   1     1      allint zz    OX  1 1     945     945        945        945     945     945
     3  2   2     5      0ex2ex yy    OX  2 2    2691    2691     128979     128979    2529    2529
     4  2   2    42      four-ext y   4X  2 2    2691    2691     128979     128979    2529    2529
     5  1   1    75      dg-024ext z  OX  1 1     945     945        945        945     945     945
     6  2   2    76      dg-024ext y  OX  2 2    2691    2691     128979     128979    2529    2529
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=   5.000 N=  1 (task/type/sgbra)=(   1/11/0) (
REDTASK #   2 TIME=   4.000 N=  1 (task/type/sgbra)=(   2/ 1/0) (
REDTASK #   3 TIME=   3.000 N=  1 (task/type/sgbra)=(   3/ 5/0) (
REDTASK #   4 TIME=   2.000 N=  1 (task/type/sgbra)=(   4/42/1) (
REDTASK #   5 TIME=   1.000 N=  1 (task/type/sgbra)=(   5/75/1) (
REDTASK #   6 TIME=   0.000 N=  1 (task/type/sgbra)=(   6/76/1) (
 initializing v-file: 1:                129924

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      9 vectors will be written to unit 11 beginning with logical record   1

            9 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      251850 2x:           0 4x:           0
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:           0    task #
 reference space has dimension      45
 dsyevx: computed roots 1 to   18(converged:  18)

    root           eigenvalues
    ----           ------------
       1        -711.4153745371
       2        -711.4131804909
       3        -711.3854867596
       4        -711.3687140487
       5        -711.2833910953
       6        -711.2202746353
       7        -711.2090678146
       8        -711.1417028971
       9        -711.1368853598
      10        -711.1116179167
      11        -711.0879186769
      12        -711.0764795042
      13        -711.0454895547
      14        -711.0412137449
      15        -711.0272537794
      16        -711.0136581994
      17        -711.0125930371
      18        -711.0026271264

 strefv generated    9 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   945

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   945

         vector  2 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   945)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            129924
 number of initial trial vectors:                         9
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              10
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:           0    task #     4:           0
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.00000000     0.00000000  -242.63879691
   ht   4    -0.00000000    -0.00000000    -0.00000000  -242.62202420
   ht   5    -0.00000000    -0.00000000     0.00000000    -0.00000000  -242.53670125
   ht   6     0.00000000    -0.00000000    -0.00000000     0.00000000    -0.00000000  -242.47358479
   ht   7    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000  -242.46237797
   ht   8    -0.00000000    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000  -242.39501305
   ht   9    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000

                ht   9
   ht   9  -242.39019551

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1    1.00000      -1.037201E-11  -5.104722E-12   7.524432E-13   6.184914E-13   2.361833E-13   2.838146E-13  -1.979042E-13
 ref    2   1.037209E-11    1.00000      -4.388365E-13   4.351103E-13   8.996710E-14  -2.223326E-13  -3.813703E-14   7.406272E-14

              v      9
 ref    1  -1.682857E-13
 ref    2  -8.967139E-14

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1    1.00000        1.00000       2.625077E-23   7.554918E-25   3.906257E-25   1.052143E-25   8.200518E-26   4.465136E-26

              v      9
 ref    1   3.636102E-26

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     1.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000
 ref:   2     0.00000000     1.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000

                ci   9
 ref:   1    -0.00000000
 ref:   2    -0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -711.4153745371 -2.5580E-13  9.6468E-02  3.4326E-01  1.0000E-04   
 mr-sdci #  1  2   -711.4131804909  3.4106E-13  0.0000E+00  3.4666E-01  1.0000E-04   
 mr-sdci #  1  3   -711.3854867596  3.4106E-13  0.0000E+00  3.5038E-01  1.0000E-04   
 mr-sdci #  1  4   -711.3687140487  3.9790E-13  0.0000E+00  3.6143E-01  1.0000E-04   
 mr-sdci #  1  5   -711.2833910953  0.0000E+00  0.0000E+00  3.1932E-01  1.0000E-04   
 mr-sdci #  1  6   -711.2202746353 -1.1369E-13  0.0000E+00  2.8980E-01  1.0000E-04   
 mr-sdci #  1  7   -711.2090678146 -3.9790E-13  0.0000E+00  4.0560E-01  1.0000E-04   
 mr-sdci #  1  8   -711.1417028971  8.5265E-14  0.0000E+00  3.8176E-01  1.0000E-04   
 mr-sdci #  1  9   -711.1368853598 -2.8422E-14  0.0000E+00  4.0185E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009385
time for cinew                         0.085827
time for eigenvalue solver             0.000169
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -711.4153745371 -2.5580E-13  9.6468E-02  3.4326E-01  1.0000E-04   
 mr-sdci #  1  2   -711.4131804909  3.4106E-13  0.0000E+00  3.4666E-01  1.0000E-04   
 mr-sdci #  1  3   -711.3854867596  3.4106E-13  0.0000E+00  3.5038E-01  1.0000E-04   
 mr-sdci #  1  4   -711.3687140487  3.9790E-13  0.0000E+00  3.6143E-01  1.0000E-04   
 mr-sdci #  1  5   -711.2833910953  0.0000E+00  0.0000E+00  3.1932E-01  1.0000E-04   
 mr-sdci #  1  6   -711.2202746353 -1.1369E-13  0.0000E+00  2.8980E-01  1.0000E-04   
 mr-sdci #  1  7   -711.2090678146 -3.9790E-13  0.0000E+00  4.0560E-01  1.0000E-04   
 mr-sdci #  1  8   -711.1417028971  8.5265E-14  0.0000E+00  3.8176E-01  1.0000E-04   
 mr-sdci #  1  9   -711.1368853598 -2.8422E-14  0.0000E+00  4.0185E-01  1.0000E-04   
 
    2 of the  10 expansion vectors are transformed.
    2 of the   9 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            129924
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:              20
 number of roots to converge:                             2
 number of iterations:                                  150
 residual norm convergence criteria:               0.001000  0.001000

          starting ci iteration   1

 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2
 ref    1    1.00000      -1.037209E-11
 ref    2   1.037209E-11    1.00000    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2
 ref    1    1.00000        1.00000    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000    -0.00000000
 ref:   2     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -711.4153745371  1.1369E-13  9.6468E-02  3.4326E-01  1.0000E-03   
 mr-sdci #  1  2   -711.4131804909  2.8422E-14  0.0000E+00  3.4666E-01  1.0000E-03   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001016
time for cinew                         0.074678
time for eigenvalue solver             0.000053
time for vector access                 0.000001

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3
 ref    1  -0.970453      -9.236069E-04  -0.241288    
 ref    2  -8.947400E-04    1.00000      -2.292004E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3
 ref    1   0.941780        1.00000       5.822006E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.97045306    -0.00092361    -0.24128822
 ref:   2    -0.00089474     0.99999957    -0.00022920

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -711.4924379529  7.7063E-02  6.3017E-03  8.4754E-02  1.0000E-03   
 mr-sdci #  2  2   -711.4131804928  1.9214E-09  0.0000E+00  3.4666E-01  1.0000E-03   
 mr-sdci #  2  3   -710.1687804037 -1.2167E+00  0.0000E+00  6.1733E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001124
time for cinew                         0.076358
time for eigenvalue solver             0.000094
time for vector access                 0.000001

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.964557      -2.157556E-03   0.142162       0.222293    
 ref    2  -1.987057E-03   0.999997       1.687871E-03   4.346240E-06

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1   0.930375       0.999998       2.021296E-02   4.941426E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96455739    -0.00215756     0.14216228     0.22229319
 ref:   2    -0.00198706     0.99999660     0.00168787     0.00000435

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -711.4982070133  5.7691E-03  9.1149E-04  3.3445E-02  1.0000E-03   
 mr-sdci #  3  2   -711.4131820753  1.5825E-06  0.0000E+00  3.4666E-01  1.0000E-03   
 mr-sdci #  3  3   -710.7391963383  5.7042E-01  0.0000E+00  6.4791E-01  1.0000E-04   
 mr-sdci #  3  4   -710.1323587251 -1.2364E+00  0.0000E+00  6.2934E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001304
time for cinew                         0.075856
time for eigenvalue solver             0.000085
time for vector access                 0.000000

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.962678      -3.405041E-03  -0.155604       3.127380E-02  -0.219202    
 ref    2  -2.969238E-03   0.999986      -3.969337E-03   1.641525E-03   5.584258E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.926757       0.999984       2.422851E-02   9.807454E-04   4.804989E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96267752    -0.00340504    -0.15560450     0.03127380    -0.21920214
 ref:   2    -0.00296924     0.99998621    -0.00396934     0.00164153     0.00055843

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -711.4990106256  8.0361E-04  1.9395E-04  1.2927E-02  1.0000E-03   
 mr-sdci #  4  2   -711.4131920018  9.9265E-06  0.0000E+00  3.4663E-01  1.0000E-03   
 mr-sdci #  4  3   -710.8497745194  1.1058E-01  0.0000E+00  5.7302E-01  1.0000E-04   
 mr-sdci #  4  4   -710.3057695528  1.7341E-01  0.0000E+00  7.0836E-01  1.0000E-04   
 mr-sdci #  4  5   -710.1098570598 -1.1735E+00  0.0000E+00  6.1709E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001769
time for cinew                         0.078325
time for eigenvalue solver             0.000106
time for vector access                 0.000001

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.960677      -6.766835E-03   0.121575      -0.137215       6.626858E-02  -0.197621    
 ref    2  -4.290218E-03   0.999723       2.300107E-02   2.276452E-04  -2.362675E-03  -1.766152E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.922919       0.999493       1.530953E-02   1.882797E-02   4.397107E-03   3.905393E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96067705    -0.00676684     0.12157499    -0.13721486     0.06626858    -0.19762060
 ref:   2    -0.00429022     0.99972340     0.02300107     0.00022765    -0.00236267    -0.00017662

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -711.4992177489  2.0712E-04  5.8385E-05  8.1214E-03  1.0000E-03   
 mr-sdci #  5  2   -711.4132841638  9.2162E-05  0.0000E+00  3.4634E-01  1.0000E-03   
 mr-sdci #  5  3   -711.2268979363  3.7712E-01  0.0000E+00  3.9984E-01  1.0000E-04   
 mr-sdci #  5  4   -710.4760846516  1.7032E-01  0.0000E+00  6.6572E-01  1.0000E-04   
 mr-sdci #  5  5   -710.2386183622  1.2876E-01  0.0000E+00  6.6779E-01  1.0000E-04   
 mr-sdci #  5  6   -710.0905277745 -1.1297E+00  0.0000E+00  6.3245E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001916
time for cinew                         0.076626
time for eigenvalue solver             0.000097
time for vector access                 0.000000

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.959723      -1.313622E-02  -0.115374       0.158525      -4.332102E-02  -2.976801E-02  -0.193794    
 ref    2  -5.760742E-03   0.997465      -7.048432E-02  -6.479901E-03  -4.340734E-03   3.782792E-04  -1.509876E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.921101       0.995108       1.827912E-02   2.517209E-02   1.895553E-03   8.862777E-04   3.755821E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.95972261    -0.01313622    -0.11537365     0.15852478    -0.04332102    -0.02976801    -0.19379352
 ref:   2    -0.00576074     0.99746455    -0.07048432    -0.00647990    -0.00434073     0.00037828    -0.00150988

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -711.4992839554  6.6206E-05  2.0959E-05  4.5557E-03  1.0000E-03   
 mr-sdci #  6  2   -711.4137264495  4.4229E-04  0.0000E+00  3.4443E-01  1.0000E-03   
 mr-sdci #  6  3   -711.3149906774  8.8093E-02  0.0000E+00  2.6995E-01  1.0000E-04   
 mr-sdci #  6  4   -710.5411541498  6.5069E-02  0.0000E+00  5.7001E-01  1.0000E-04   
 mr-sdci #  6  5   -710.4391405528  2.0052E-01  0.0000E+00  7.4475E-01  1.0000E-04   
 mr-sdci #  6  6   -710.1673318534  7.6804E-02  0.0000E+00  6.9926E-01  1.0000E-04   
 mr-sdci #  6  7   -710.0562697255 -1.1528E+00  0.0000E+00  6.0406E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002125
time for cinew                         0.079948
time for eigenvalue solver             0.000000
time for vector access                 0.000001

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.959048       3.932827E-02   9.197801E-02   0.111596      -9.702292E-02  -0.115868      -9.951939E-02  -0.158187    
 ref    2   7.425256E-03  -0.940183       0.340227      -1.353894E-02   6.207840E-03  -3.628752E-03  -3.379352E-03   5.206130E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.919829       0.885491       0.124214       1.263697E-02   9.451984E-03   1.343859E-02   9.915530E-03   2.502344E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95904823     0.03932827     0.09197801     0.11159601    -0.09702292    -0.11586814    -0.09951939    -0.15818713
 ref:   2     0.00742526    -0.94018294     0.34022670    -0.01353894     0.00620784    -0.00362875    -0.00337935     0.00052061

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -711.4993093147  2.5359E-05  9.3508E-06  3.2613E-03  1.0000E-03   
 mr-sdci #  7  2   -711.4171057521  3.3793E-03  0.0000E+00  3.2036E-01  1.0000E-03   
 mr-sdci #  7  3   -711.3844464043  6.9456E-02  0.0000E+00  2.4143E-01  1.0000E-04   
 mr-sdci #  7  4   -710.9618555876  4.2070E-01  0.0000E+00  5.7921E-01  1.0000E-04   
 mr-sdci #  7  5   -710.4950381171  5.5898E-02  0.0000E+00  6.9025E-01  1.0000E-04   
 mr-sdci #  7  6   -710.2847049263  1.1737E-01  0.0000E+00  6.5976E-01  1.0000E-04   
 mr-sdci #  7  7   -710.0975243488  4.1255E-02  0.0000E+00  6.4436E-01  1.0000E-04   
 mr-sdci #  7  8   -710.0407026670 -1.1010E+00  0.0000E+00  6.1895E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003319
time for cinew                         0.080156
time for eigenvalue solver             0.000135
time for vector access                 0.000001

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543

                ht   9
   ht   9    -0.00293737

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.958646      -7.132015E-02   5.920564E-02  -0.108485      -0.108882      -4.183611E-02  -0.133803      -2.433449E-03
 ref    2  -9.400553E-03   0.682830       0.729303       3.869844E-02   3.294374E-03  -1.456504E-02  -6.667591E-03   3.134533E-04

              v      9
 ref    1  -0.170652    
 ref    2   2.548555E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.919091       0.471344       0.535388       1.326659E-02   1.186612E-02   1.962400E-03   1.794770E-02   6.019926E-06

              v      9
 ref    1   2.912856E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95864610    -0.07132015     0.05920564    -0.10848511    -0.10888191    -0.04183611    -0.13380301    -0.00243345
 ref:   2    -0.00940055     0.68283030     0.72930295     0.03869844     0.00329437    -0.01456504    -0.00666759     0.00031345

                ci   9
 ref:   1    -0.17065188
 ref:   2     0.00254856

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -711.4993214852  1.2171E-05  6.0416E-06  2.2616E-03  1.0000E-03   
 mr-sdci #  8  2   -711.4300485728  1.2943E-02  0.0000E+00  2.2731E-01  1.0000E-03   
 mr-sdci #  8  3   -711.3998075699  1.5361E-02  0.0000E+00  3.0579E-01  1.0000E-04   
 mr-sdci #  8  4   -711.0973356743  1.3548E-01  0.0000E+00  4.6913E-01  1.0000E-04   
 mr-sdci #  8  5   -710.4990436734  4.0056E-03  0.0000E+00  6.5120E-01  1.0000E-04   
 mr-sdci #  8  6   -710.3892948484  1.0459E-01  0.0000E+00  7.5640E-01  1.0000E-04   
 mr-sdci #  8  7   -710.2797572926  1.8223E-01  0.0000E+00  6.6039E-01  1.0000E-04   
 mr-sdci #  8  8   -710.0653410152  2.4638E-02  0.0000E+00  6.8402E-01  1.0000E-04   
 mr-sdci #  8  9   -710.0272165137 -1.1097E+00  0.0000E+00  5.9665E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002775
time for cinew                         0.080898
time for eigenvalue solver             0.000130
time for vector access                 0.000000

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201

                ht   9         ht  10
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.958088      -7.215615E-02   2.675600E-02  -9.995363E-02  -0.130753       5.030949E-02  -6.667717E-02   0.115903    
 ref    2  -1.259455E-02   0.407974       0.909744       7.389063E-02   3.047458E-03   9.133729E-03  -1.416016E-02  -1.338193E-03

              v      9       v     10
 ref    1   8.985282E-02  -0.143438    
 ref    2  -2.720534E-03   1.324325E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.918091       0.171650       0.828350       1.545055E-02   1.710559E-02   2.614470E-03   4.646355E-03   1.343532E-02

              v      9       v     10
 ref    1   8.080931E-03   2.057620E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95808777    -0.07215615     0.02675600    -0.09995363    -0.13075283     0.05030949    -0.06667717     0.11590310
 ref:   2    -0.01259455     0.40797446     0.90974404     0.07389063     0.00304746     0.00913373    -0.01416016    -0.00133819

                ci   9         ci  10
 ref:   1     0.08985282    -0.14343796
 ref:   2    -0.00272053     0.00132433

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -711.4993320040  1.0519E-05  4.8021E-06  2.4484E-03  1.0000E-03   
 mr-sdci #  9  2   -711.4601871345  3.0139E-02  0.0000E+00  1.3541E-01  1.0000E-03   
 mr-sdci #  9  3   -711.4049376705  5.1301E-03  0.0000E+00  3.4745E-01  1.0000E-04   
 mr-sdci #  9  4   -711.2858600307  1.8852E-01  0.0000E+00  2.6882E-01  1.0000E-04   
 mr-sdci #  9  5   -710.6860912621  1.8705E-01  0.0000E+00  5.7641E-01  1.0000E-04   
 mr-sdci #  9  6   -710.4029554930  1.3661E-02  0.0000E+00  6.8965E-01  1.0000E-04   
 mr-sdci #  9  7   -710.3429725130  6.3215E-02  0.0000E+00  7.1451E-01  1.0000E-04   
 mr-sdci #  9  8   -710.1895578655  1.2422E-01  0.0000E+00  7.0181E-01  1.0000E-04   
 mr-sdci #  9  9   -710.0395355234  1.2319E-02  0.0000E+00  6.5298E-01  1.0000E-04   
 mr-sdci #  9 10   -710.0240379683  2.4128E+02  0.0000E+00  6.1857E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003113
time for cinew                         0.079220
time for eigenvalue solver             0.000000
time for vector access                 0.000002

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458

                ht   9         ht  10         ht  11
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.957567      -7.224819E-02   2.721614E-02  -9.782148E-02  -0.123682      -1.001551E-02   5.238660E-02   9.356078E-02
 ref    2   1.621848E-02   0.395667       0.908001       0.133123      -1.462192E-02  -1.816317E-02   1.565382E-02  -1.239515E-02

              v      9       v     10       v     11
 ref    1  -0.132264       0.139490       6.073221E-02
 ref    2   3.717772E-03  -1.275061E-03   6.335481E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.917198       0.161772       0.825206       2.729068E-02   1.551094E-02   4.302114E-04   2.989398E-03   8.907259E-03

              v      9       v     10       v     11
 ref    1   1.750760E-02   1.945910E-02   3.728540E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95756742    -0.07224819     0.02721614    -0.09782148    -0.12368159    -0.01001551     0.05238660     0.09356078
 ref:   2     0.01621848     0.39566681     0.90800062     0.13312263    -0.01462192    -0.01816317     0.01565382    -0.01239515

                ci   9         ci  10         ci  11
 ref:   1    -0.13226406     0.13949004     0.06073221
 ref:   2     0.00371777    -0.00127506     0.00633548

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -711.4993395431  7.5391E-06  2.1574E-06  1.5311E-03  1.0000E-03   
 mr-sdci # 10  2   -711.4730179118  1.2831E-02  0.0000E+00  8.0272E-02  1.0000E-03   
 mr-sdci # 10  3   -711.4049410009  3.3304E-06  0.0000E+00  3.4698E-01  1.0000E-04   
 mr-sdci # 10  4   -711.3214130445  3.5553E-02  0.0000E+00  1.9660E-01  1.0000E-04   
 mr-sdci # 10  5   -710.7872170953  1.0113E-01  0.0000E+00  5.0396E-01  1.0000E-04   
 mr-sdci # 10  6   -710.4242757262  2.1320E-02  0.0000E+00  7.3390E-01  1.0000E-04   
 mr-sdci # 10  7   -710.3437319274  7.5941E-04  0.0000E+00  7.1398E-01  1.0000E-04   
 mr-sdci # 10  8   -710.3038006241  1.1424E-01  0.0000E+00  6.5576E-01  1.0000E-04   
 mr-sdci # 10  9   -710.1867672952  1.4723E-01  0.0000E+00  6.8201E-01  1.0000E-04   
 mr-sdci # 10 10   -710.0240657449  2.7777E-05  0.0000E+00  6.2146E-01  1.0000E-04   
 mr-sdci # 10 11   -709.9403479574  2.4119E+02  0.0000E+00  6.3572E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000429
time for cinew                         0.080772
time for eigenvalue solver             0.000174
time for vector access                 0.000000

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.957264      -7.353842E-02   3.320003E-02  -8.961558E-02  -9.392574E-02  -0.112844       8.427100E-02  -2.012498E-02
 ref    2  -1.857313E-02   0.416745       0.878911       0.222990      -5.165520E-02  -1.768321E-03  -1.642222E-02   2.427228E-02

              v      9       v     10       v     11       v     12
 ref    1   2.684030E-02   0.161207       0.113975      -1.852780E-02
 ref    2  -6.919510E-03   4.631762E-03  -2.671641E-03  -1.271429E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916700       0.179085       0.773588       5.775558E-02   1.149030E-02   1.273691E-02   7.371291E-03   9.941585E-04

              v      9       v     10       v     11       v     12
 ref    1   7.682814E-04   2.600911E-02   1.299741E-02   5.049325E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95726425    -0.07353842     0.03320003    -0.08961558    -0.09392574    -0.11284406     0.08427100    -0.02012498
 ref:   2    -0.01857313     0.41674544     0.87891143     0.22299021    -0.05165520    -0.00176832    -0.01642222     0.02427228

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.02684030     0.16120687     0.11397489    -0.01852780
 ref:   2    -0.00691951     0.00463176    -0.00267164    -0.01271429

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -711.4993422981  2.7550E-06  0.0000E+00  8.8685E-04  1.0000E-03   
 mr-sdci # 11  2   -711.4780673646  5.0495E-03  2.6469E-03  5.4824E-02  1.0000E-03   
 mr-sdci # 11  3   -711.4064122506  1.4712E-03  0.0000E+00  3.3435E-01  1.0000E-04   
 mr-sdci # 11  4   -711.3378868132  1.6474E-02  0.0000E+00  1.8619E-01  1.0000E-04   
 mr-sdci # 11  5   -711.0113700313  2.2415E-01  0.0000E+00  4.4250E-01  1.0000E-04   
 mr-sdci # 11  6   -710.5045411161  8.0265E-02  0.0000E+00  5.9407E-01  1.0000E-04   
 mr-sdci # 11  7   -710.3873526971  4.3621E-02  0.0000E+00  6.5827E-01  1.0000E-04   
 mr-sdci # 11  8   -710.3329649846  2.9164E-02  0.0000E+00  7.1309E-01  1.0000E-04   
 mr-sdci # 11  9   -710.2322088846  4.5442E-02  0.0000E+00  7.3118E-01  1.0000E-04   
 mr-sdci # 11 10   -710.1178166097  9.3751E-02  0.0000E+00  6.4831E-01  1.0000E-04   
 mr-sdci # 11 11   -709.9791015367  3.8754E-02  0.0000E+00  6.2437E-01  1.0000E-04   
 mr-sdci # 11 12   -709.9028809124  2.4116E+02  0.0000E+00  6.3316E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000109
time for cinew                         0.169704
time for eigenvalue solver             0.000000
time for vector access                 0.000002

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.957078       7.015152E-02  -4.026969E-02   9.152039E-02  -6.772332E-02  -0.115476      -5.029447E-02  -8.123254E-02
 ref    2  -2.223063E-02  -0.597287      -0.737826      -0.158861      -0.221235       0.120171       2.884167E-02  -1.142264E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -3.010450E-02   1.682277E-02   0.179710      -9.008825E-02   2.077973E-02
 ref    2  -5.290287E-02  -6.381948E-02   3.638264E-02   2.420698E-02  -4.189622E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916493       0.361673       0.546009       3.361281E-02   5.353155E-02   2.777581E-02   3.361376E-03   6.729202E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.704994E-03   4.355932E-03   3.361954E-02   8.701871E-03   4.319726E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95707832     0.07015152    -0.04026969     0.09152039    -0.06772332    -0.11547595    -0.05029447    -0.08123254
 ref:   2    -0.02223063    -0.59728659    -0.73782632    -0.15886103    -0.22123540     0.12017120     0.02884167    -0.01142264

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.03010450     0.01682277     0.17971045    -0.09008825     0.02077973
 ref:   2    -0.05290287    -0.06381948     0.03638264     0.02420698    -0.00041896

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -711.4993432319  9.3378E-07  0.0000E+00  8.2397E-04  1.0000E-03   
 mr-sdci # 12  2   -711.4827646049  4.6972E-03  3.3437E-03  6.1170E-02  1.0000E-03   
 mr-sdci # 12  3   -711.4407529242  3.4341E-02  0.0000E+00  2.2658E-01  1.0000E-04   
 mr-sdci # 12  4   -711.3386681021  7.8129E-04  0.0000E+00  1.6528E-01  1.0000E-04   
 mr-sdci # 12  5   -711.0810892822  6.9719E-02  0.0000E+00  4.7413E-01  1.0000E-04   
 mr-sdci # 12  6   -710.7183477507  2.1381E-01  0.0000E+00  5.7348E-01  1.0000E-04   
 mr-sdci # 12  7   -710.3915670497  4.2144E-03  0.0000E+00  6.6732E-01  1.0000E-04   
 mr-sdci # 12  8   -710.3629290540  2.9964E-02  0.0000E+00  6.3958E-01  1.0000E-04   
 mr-sdci # 12  9   -710.2445390553  1.2330E-02  0.0000E+00  7.1384E-01  1.0000E-04   
 mr-sdci # 12 10   -710.2187742269  1.0096E-01  0.0000E+00  7.0524E-01  1.0000E-04   
 mr-sdci # 12 11   -710.1003944373  1.2129E-01  0.0000E+00  6.4928E-01  1.0000E-04   
 mr-sdci # 12 12   -709.9664233029  6.3542E-02  0.0000E+00  6.2622E-01  1.0000E-04   
 mr-sdci # 12 13   -709.8973341571  2.4115E+02  0.0000E+00  6.3757E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000149
time for cinew                         0.078115
time for eigenvalue solver             0.000000
time for vector access                 0.000002

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357
   ht  14     3.68244141     2.26087687     0.06159528    -0.03516856    -0.03872663     0.11086025    -0.00278001     0.05557899

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354
   ht  14    -0.01028113     0.03704294     0.00742108    -0.00731018    -0.35460193    -1.56206422

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.956839       5.672167E-02   5.821056E-02  -9.319083E-02  -6.784963E-02  -0.115697      -4.017315E-02  -3.043459E-02
 ref    2   2.897005E-02  -0.862026       0.402156       7.016007E-02  -0.180265       0.117150      -0.158018       6.233496E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   8.147573E-02   3.538718E-02   1.045018E-02  -0.181008       8.730291E-02  -1.592128E-02
 ref    2  -2.957185E-02   7.425094E-02  -8.291090E-02  -1.843733E-02  -1.294665E-02   2.400459E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916380       0.746306       0.165118       1.360697E-02   3.709916E-02   2.711011E-02   2.658343E-02   4.811911E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   7.512788E-03   6.765454E-03   6.983424E-03   3.310395E-02   7.789414E-03   8.297075E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95683906     0.05672167     0.05821056    -0.09319083    -0.06784963    -0.11569739    -0.04017315    -0.03043459
 ref:   2     0.02897005    -0.86202569     0.40215584     0.07016007    -0.18026532     0.11715044    -0.15801756     0.06233496

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.08147573     0.03538718     0.01045018    -0.18100833     0.08730291    -0.01592128
 ref:   2    -0.02957185     0.07425094    -0.08291090    -0.01843733    -0.01294665     0.02400459

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -711.4993441429  9.1103E-07  0.0000E+00  8.1013E-04  1.0000E-03   
 mr-sdci # 13  2   -711.4895293545  6.7647E-03  4.2436E-03  7.1908E-02  1.0000E-03   
 mr-sdci # 13  3   -711.4673953123  2.6642E-02  0.0000E+00  1.0686E-01  1.0000E-04   
 mr-sdci # 13  4   -711.3429755990  4.3075E-03  0.0000E+00  1.3952E-01  1.0000E-04   
 mr-sdci # 13  5   -711.0862475796  5.1583E-03  0.0000E+00  4.6496E-01  1.0000E-04   
 mr-sdci # 13  6   -710.7183864100  3.8659E-05  0.0000E+00  5.7382E-01  1.0000E-04   
 mr-sdci # 13  7   -710.4571054559  6.5538E-02  0.0000E+00  7.3349E-01  1.0000E-04   
 mr-sdci # 13  8   -710.3876542370  2.4725E-02  0.0000E+00  6.5737E-01  1.0000E-04   
 mr-sdci # 13  9   -710.3569666115  1.1243E-01  0.0000E+00  6.6412E-01  1.0000E-04   
 mr-sdci # 13 10   -710.2356938718  1.6920E-02  0.0000E+00  7.1853E-01  1.0000E-04   
 mr-sdci # 13 11   -710.2177703103  1.1738E-01  0.0000E+00  6.9581E-01  1.0000E-04   
 mr-sdci # 13 12   -710.0963758232  1.2995E-01  0.0000E+00  6.5616E-01  1.0000E-04   
 mr-sdci # 13 13   -709.9617399916  6.4406E-02  0.0000E+00  6.2570E-01  1.0000E-04   
 mr-sdci # 13 14   -709.8720512650  2.4113E+02  0.0000E+00  6.2393E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.004745
time for cinew                         0.082865
time for eigenvalue solver             0.000225
time for vector access                 0.000002

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357
   ht  14     3.68244141     2.26087687     0.06159528    -0.03516856    -0.03872663     0.11086025    -0.00278001     0.05557899
   ht  15    -0.65239253    -1.01225768    -0.09644134     0.15829073    -0.02820914     0.06391743    -0.01078406     0.02139350

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354
   ht  14    -0.01028113     0.03704294     0.00742108    -0.00731018    -0.35460193    -1.56206422
   ht  15     0.00686190     0.01205915     0.00234039    -0.00722090    -0.04720741     0.17214342    -1.33838907

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.956451      -4.952620E-02   6.926067E-02   9.101489E-02   6.252161E-02   0.121000       3.979661E-02  -3.494650E-02
 ref    2  -3.972960E-02   0.946950       0.146215      -3.609220E-02   9.276138E-02  -6.825699E-02   0.144320       4.619200E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -7.149479E-02   2.943563E-02   3.811951E-02   5.184237E-03   0.185565      -7.763104E-02  -1.986438E-02
 ref    2  -5.456311E-02  -0.197343       2.420374E-02   1.391129E-02   5.962648E-03  -4.238766E-03   6.772176E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916376       0.899167       2.617573E-02   9.586357E-03   1.251363E-02   1.930009E-02   2.241190E-02   3.354959E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   8.088639E-03   3.981084E-02   2.038918E-03   2.204002E-04   3.446991E-02   6.044546E-03   4.404561E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95645067    -0.04952620     0.06926067     0.09101489     0.06252161     0.12100031     0.03979661    -0.03494650
 ref:   2    -0.03972960     0.94695009     0.14621452    -0.03609220     0.09276138    -0.06825699     0.14431954     0.04619200

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.07149479     0.02943563     0.03811951     0.00518424     0.18556497    -0.07763104    -0.01986438
 ref:   2    -0.05456311    -0.19734331     0.02420374     0.01391129     0.00596265    -0.00423877     0.00677218

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -711.4993450251  8.8220E-07  0.0000E+00  8.2983E-04  1.0000E-03   
 mr-sdci # 14  2   -711.4950967253  5.5674E-03  1.9643E-03  4.5312E-02  1.0000E-03   
 mr-sdci # 14  3   -711.4741402362  6.7449E-03  0.0000E+00  5.1105E-02  1.0000E-04   
 mr-sdci # 14  4   -711.3470702700  4.0947E-03  0.0000E+00  1.3126E-01  1.0000E-04   
 mr-sdci # 14  5   -711.1596736343  7.3426E-02  0.0000E+00  3.8965E-01  1.0000E-04   
 mr-sdci # 14  6   -710.7366854898  1.8299E-02  0.0000E+00  5.3150E-01  1.0000E-04   
 mr-sdci # 14  7   -710.4578115032  7.0605E-04  0.0000E+00  7.4735E-01  1.0000E-04   
 mr-sdci # 14  8   -710.3880812018  4.2696E-04  0.0000E+00  6.6388E-01  1.0000E-04   
 mr-sdci # 14  9   -710.3661422092  9.1756E-03  0.0000E+00  6.9187E-01  1.0000E-04   
 mr-sdci # 14 10   -710.3040434566  6.8350E-02  0.0000E+00  6.9219E-01  1.0000E-04   
 mr-sdci # 14 11   -710.2331070761  1.5337E-02  0.0000E+00  7.3371E-01  1.0000E-04   
 mr-sdci # 14 12   -710.1628468008  6.6471E-02  0.0000E+00  6.7581E-01  1.0000E-04   
 mr-sdci # 14 13   -710.0935402261  1.3180E-01  0.0000E+00  6.5020E-01  1.0000E-04   
 mr-sdci # 14 14   -709.9533643315  8.1313E-02  0.0000E+00  6.3399E-01  1.0000E-04   
 mr-sdci # 14 15   -709.8564912450  2.4111E+02  0.0000E+00  6.3219E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.006472
time for cinew                         0.083820
time for eigenvalue solver             0.000250
time for vector access                 0.000000

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357
   ht  14     3.68244141     2.26087687     0.06159528    -0.03516856    -0.03872663     0.11086025    -0.00278001     0.05557899
   ht  15    -0.65239253    -1.01225768    -0.09644134     0.15829073    -0.02820914     0.06391743    -0.01078406     0.02139350
   ht  16     1.37300873    -1.65237486    -0.06240527    -0.13854449     0.04322058    -0.03694217     0.01514720    -0.01429881

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354
   ht  14    -0.01028113     0.03704294     0.00742108    -0.00731018    -0.35460193    -1.56206422
   ht  15     0.00686190     0.01205915     0.00234039    -0.00722090    -0.04720741     0.17214342    -1.33838907
   ht  16    -0.00670938    -0.00824613    -0.00089843     0.00523092     0.23103182     0.38882529    -0.17601112    -1.03121609

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.955833      -5.663905E-02  -7.173877E-02   8.463921E-02   6.001883E-02   0.102091      -7.291238E-02  -8.368898E-02
 ref    2  -5.307340E-02   0.957148      -5.622194E-02  -1.346245E-02   1.860431E-02   3.558543E-02   0.140520      -0.114981    

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -3.061600E-02   1.877840E-02   4.142605E-02  -2.485976E-02  -1.392972E-03  -0.186001       7.806544E-02  -1.427176E-02
 ref    2   4.970228E-02  -0.198906      -9.297418E-03  -2.778197E-02  -1.365541E-02  -6.761267E-03   1.970618E-03   1.180052E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916433       0.919341       8.307358E-03   7.345034E-03   3.948381E-03   1.168889E-02   2.506211E-02   2.022440E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   3.407657E-03   3.991616E-02   1.802560E-03   1.389846E-03   1.884105E-04   3.464211E-02   6.098096E-03   2.050756E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95583291    -0.05663905    -0.07173877     0.08463921     0.06001883     0.10209099    -0.07291238    -0.08368898
 ref:   2    -0.05307340     0.95714816    -0.05622194    -0.01346245     0.01860431     0.03558543     0.14052009    -0.11498065

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.03061600     0.01877840     0.04142605    -0.02485976    -0.00139297    -0.18600105     0.07806544    -0.01427176
 ref:   2     0.04970228    -0.19890583    -0.00929742    -0.02778197    -0.01365541    -0.00676127     0.00197062     0.00118005

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -711.4993458903  8.6515E-07  0.0000E+00  9.1147E-04  1.0000E-03   
 mr-sdci # 15  2   -711.4971494363  2.0527E-03  7.3921E-04  2.8331E-02  1.0000E-03   
 mr-sdci # 15  3   -711.4761057084  1.9655E-03  0.0000E+00  3.4692E-02  1.0000E-04   
 mr-sdci # 15  4   -711.3538670648  6.7968E-03  0.0000E+00  1.2598E-01  1.0000E-04   
 mr-sdci # 15  5   -711.2499126683  9.0239E-02  0.0000E+00  3.0683E-01  1.0000E-04   
 mr-sdci # 15  6   -710.8449382872  1.0825E-01  0.0000E+00  5.7277E-01  1.0000E-04   
 mr-sdci # 15  7   -710.6122337747  1.5442E-01  0.0000E+00  7.5455E-01  1.0000E-04   
 mr-sdci # 15  8   -710.4218010313  3.3720E-02  0.0000E+00  6.2558E-01  1.0000E-04   
 mr-sdci # 15  9   -710.3880366564  2.1894E-02  0.0000E+00  6.6070E-01  1.0000E-04   
 mr-sdci # 15 10   -710.3059343972  1.8909E-03  0.0000E+00  7.0287E-01  1.0000E-04   
 mr-sdci # 15 11   -710.2535270679  2.0420E-02  0.0000E+00  6.8330E-01  1.0000E-04   
 mr-sdci # 15 12   -710.2254040551  6.2557E-02  0.0000E+00  7.4415E-01  1.0000E-04   
 mr-sdci # 15 13   -710.1517557578  5.8216E-02  0.0000E+00  6.8629E-01  1.0000E-04   
 mr-sdci # 15 14   -710.0932124428  1.3985E-01  0.0000E+00  6.5224E-01  1.0000E-04   
 mr-sdci # 15 15   -709.9502514549  9.3760E-02  0.0000E+00  6.3400E-01  1.0000E-04   
 mr-sdci # 15 16   -709.8187954132  2.4107E+02  0.0000E+00  6.1841E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000549
time for cinew                         0.091208
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357
   ht  14     3.68244141     2.26087687     0.06159528    -0.03516856    -0.03872663     0.11086025    -0.00278001     0.05557899
   ht  15    -0.65239253    -1.01225768    -0.09644134     0.15829073    -0.02820914     0.06391743    -0.01078406     0.02139350
   ht  16     1.37300873    -1.65237486    -0.06240527    -0.13854449     0.04322058    -0.03694217     0.01514720    -0.01429881
   ht  17     0.11331108     0.80095808    -0.06407809    -0.05310801     0.01327850    -0.02775354     0.00011367    -0.01011079

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354
   ht  14    -0.01028113     0.03704294     0.00742108    -0.00731018    -0.35460193    -1.56206422
   ht  15     0.00686190     0.01205915     0.00234039    -0.00722090    -0.04720741     0.17214342    -1.33838907
   ht  16    -0.00670938    -0.00824613    -0.00089843     0.00523092     0.23103182     0.38882529    -0.17601112    -1.03121609
   ht  17    -0.00100700    -0.00525102    -0.00005705     0.00281855     0.15212022     0.05531413     0.16603859     0.06623876

                ht  17
   ht  17    -0.29450094

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.955029      -6.769397E-02  -7.243924E-02   7.343160E-02   6.676159E-02   0.106310      -1.883771E-02   0.101122    
 ref    2  -6.670308E-02   0.958520      -1.802110E-02   9.793874E-04  -6.339070E-03   1.319826E-02   0.141311       4.252975E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   5.598764E-02  -1.213973E-02  -3.846739E-02   2.932412E-02   2.343076E-02  -0.131617      -0.141384       5.056680E-02
 ref    2   2.516061E-02   5.657456E-03   0.218978       5.730897E-02  -9.607067E-03   1.173558E-02  -4.067319E-02  -2.325429E-02

              v     17
 ref    1  -1.783822E-02
 ref    2   1.206745E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916529       0.923344       5.572203E-03   5.393160E-03   4.497293E-03   1.147608E-02   2.032373E-02   1.203453E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   3.767672E-03   1.793799E-04   4.943128E-02   4.144222E-03   6.412961E-04   1.746070E-02   2.164371E-02   3.097763E-03

              v     17
 ref    1   4.638254E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95502883    -0.06769397    -0.07243924     0.07343160     0.06676159     0.10631031    -0.01883771     0.10112246
 ref:   2    -0.06670308     0.95852038    -0.01802110     0.00097939    -0.00633907     0.01319826     0.14131126     0.04252975

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05598764    -0.01213973    -0.03846739     0.02932412     0.02343076    -0.13161680    -0.14138389     0.05056680
 ref:   2     0.02516061     0.00565746     0.21897841     0.05730897    -0.00960707     0.01173558    -0.04067319    -0.02325429

                ci  17
 ref:   1    -0.01783822
 ref:   2     0.01206745

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -711.4993467187  8.2844E-07  0.0000E+00  6.9355E-04  1.0000E-03   
 mr-sdci # 16  2   -711.4979117370  7.6230E-04  1.9026E-04  1.4417E-02  1.0000E-03   
 mr-sdci # 16  3   -711.4770582263  9.5252E-04  0.0000E+00  2.3692E-02  1.0000E-04   
 mr-sdci # 16  4   -711.3616057852  7.7387E-03  0.0000E+00  1.2208E-01  1.0000E-04   
 mr-sdci # 16  5   -711.3017264487  5.1814E-02  0.0000E+00  2.1189E-01  1.0000E-04   
 mr-sdci # 16  6   -710.9037968264  5.8859E-02  0.0000E+00  4.3437E-01  1.0000E-04   
 mr-sdci # 16  7   -710.6762238124  6.3990E-02  0.0000E+00  8.6964E-01  1.0000E-04   
 mr-sdci # 16  8   -710.4758500729  5.4049E-02  0.0000E+00  5.2251E-01  1.0000E-04   
 mr-sdci # 16  9   -710.3982345751  1.0198E-02  0.0000E+00  6.8072E-01  1.0000E-04   
 mr-sdci # 16 10   -710.3349320032  2.8998E-02  0.0000E+00  7.1368E-01  1.0000E-04   
 mr-sdci # 16 11   -710.2946834744  4.1156E-02  0.0000E+00  6.8293E-01  1.0000E-04   
 mr-sdci # 16 12   -710.2392665425  1.3862E-02  0.0000E+00  7.0850E-01  1.0000E-04   
 mr-sdci # 16 13   -710.2204961269  6.8740E-02  0.0000E+00  7.4491E-01  1.0000E-04   
 mr-sdci # 16 14   -710.1097708552  1.6558E-02  0.0000E+00  6.6880E-01  1.0000E-04   
 mr-sdci # 16 15   -710.0542453057  1.0399E-01  0.0000E+00  6.0056E-01  1.0000E-04   
 mr-sdci # 16 16   -709.9159207474  9.7125E-02  0.0000E+00  6.4873E-01  1.0000E-04   
 mr-sdci # 16 17   -709.8097592529  2.4106E+02  0.0000E+00  6.1960E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.007141
time for cinew                         0.083603
time for eigenvalue solver             0.000313
time for vector access                 0.000002

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357
   ht  14     3.68244141     2.26087687     0.06159528    -0.03516856    -0.03872663     0.11086025    -0.00278001     0.05557899
   ht  15    -0.65239253    -1.01225768    -0.09644134     0.15829073    -0.02820914     0.06391743    -0.01078406     0.02139350
   ht  16     1.37300873    -1.65237486    -0.06240527    -0.13854449     0.04322058    -0.03694217     0.01514720    -0.01429881
   ht  17     0.11331108     0.80095808    -0.06407809    -0.05310801     0.01327850    -0.02775354     0.00011367    -0.01011079
   ht  18     0.38509180    -0.29593624     0.02578759    -0.01528440     0.00047566    -0.00167058    -0.00086301     0.00093036

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354
   ht  14    -0.01028113     0.03704294     0.00742108    -0.00731018    -0.35460193    -1.56206422
   ht  15     0.00686190     0.01205915     0.00234039    -0.00722090    -0.04720741     0.17214342    -1.33838907
   ht  16    -0.00670938    -0.00824613    -0.00089843     0.00523092     0.23103182     0.38882529    -0.17601112    -1.03121609
   ht  17    -0.00100700    -0.00525102    -0.00005705     0.00281855     0.15212022     0.05531413     0.16603859     0.06623876
   ht  18    -0.00083629     0.00102444     0.00038921    -0.00076516     0.04636236     0.03418393    -0.03367687    -0.13597493

                ht  17         ht  18
   ht  17    -0.29450094
   ht  18     0.02517632    -0.08649539

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.954627       7.269560E-02   7.279686E-02   5.217642E-02  -8.133691E-02  -6.792181E-02  -9.266722E-02  -8.200992E-02
 ref    2   7.260605E-02  -0.957580       4.469792E-03   2.129062E-02   2.159212E-02  -3.748372E-02   7.479671E-02  -0.134722    

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.392168E-02   7.242737E-02   3.977010E-02  -1.395655E-02   4.003573E-02   0.108567      -8.397805E-02  -0.131926    
 ref    2   5.304569E-02  -3.847845E-02  -0.140639       0.153932       1.980863E-02  -2.153448E-02   1.369616E-03  -4.962532E-02

              v     17       v     18
 ref    1   6.142366E-02  -1.780923E-03
 ref    2  -1.900118E-02  -1.512559E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916585       0.922244       5.319362E-03   3.175669E-03   7.081913E-03   6.018402E-03   1.418176E-02   2.487576E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   3.007658E-03   6.726315E-03   2.136095E-02   2.388993E-02   1.995242E-03   1.225061E-02   7.054189E-03   1.986723E-02

              v     17       v     18
 ref    1   4.133911E-03   2.319553E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95462718     0.07269560     0.07279686     0.05217642    -0.08133691    -0.06792181    -0.09266722    -0.08200992
 ref:   2     0.07260605    -0.95758018     0.00446979     0.02129062     0.02159212    -0.03748372     0.07479671    -0.13472243

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.01392168     0.07242737     0.03977010    -0.01395655     0.04003573     0.10856738    -0.08397805    -0.13192635
 ref:   2     0.05304569    -0.03847845    -0.14063887     0.15393227     0.01980863    -0.02153448     0.00136962    -0.04962532

                ci  17         ci  18
 ref:   1     0.06142366    -0.00178092
 ref:   2    -0.01900118    -0.01512559

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -711.4993470300  3.1129E-07  0.0000E+00  5.6777E-04  1.0000E-03   
 mr-sdci # 17  2   -711.4981267155  2.1498E-04  4.5326E-05  7.4863E-03  1.0000E-03   
 mr-sdci # 17  3   -711.4774056715  3.4745E-04  0.0000E+00  1.7902E-02  1.0000E-04   
 mr-sdci # 17  4   -711.3729989735  1.1393E-02  0.0000E+00  1.2094E-01  1.0000E-04   
 mr-sdci # 17  5   -711.3280933266  2.6367E-02  0.0000E+00  1.5897E-01  1.0000E-04   
 mr-sdci # 17  6   -711.0414311577  1.3763E-01  0.0000E+00  4.6518E-01  1.0000E-04   
 mr-sdci # 17  7   -710.7696818395  9.3458E-02  0.0000E+00  6.8530E-01  1.0000E-04   
 mr-sdci # 17  8   -710.5619161559  8.6066E-02  0.0000E+00  6.5800E-01  1.0000E-04   
 mr-sdci # 17  9   -710.4010228061  2.7882E-03  0.0000E+00  6.4715E-01  1.0000E-04   
 mr-sdci # 17 10   -710.3923415714  5.7410E-02  0.0000E+00  6.7377E-01  1.0000E-04   
 mr-sdci # 17 11   -710.3158480118  2.1165E-02  0.0000E+00  6.7090E-01  1.0000E-04   
 mr-sdci # 17 12   -710.2616012376  2.2335E-02  0.0000E+00  7.2874E-01  1.0000E-04   
 mr-sdci # 17 13   -710.2347180159  1.4222E-02  0.0000E+00  7.3220E-01  1.0000E-04   
 mr-sdci # 17 14   -710.1133271005  3.5562E-03  0.0000E+00  6.6678E-01  1.0000E-04   
 mr-sdci # 17 15   -710.1083355520  5.4090E-02  0.0000E+00  6.7491E-01  1.0000E-04   
 mr-sdci # 17 16   -710.0424177386  1.2650E-01  0.0000E+00  6.0447E-01  1.0000E-04   
 mr-sdci # 17 17   -709.9036493123  9.3890E-02  0.0000E+00  6.5613E-01  1.0000E-04   
 mr-sdci # 17 18   -709.7619985912  2.4102E+02  0.0000E+00  6.0313E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.006390
time for cinew                         0.083332
time for eigenvalue solver             0.000286
time for vector access                 0.000000

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357
   ht  14     3.68244141     2.26087687     0.06159528    -0.03516856    -0.03872663     0.11086025    -0.00278001     0.05557899
   ht  15    -0.65239253    -1.01225768    -0.09644134     0.15829073    -0.02820914     0.06391743    -0.01078406     0.02139350
   ht  16     1.37300873    -1.65237486    -0.06240527    -0.13854449     0.04322058    -0.03694217     0.01514720    -0.01429881
   ht  17     0.11331108     0.80095808    -0.06407809    -0.05310801     0.01327850    -0.02775354     0.00011367    -0.01011079
   ht  18     0.38509180    -0.29593624     0.02578759    -0.01528440     0.00047566    -0.00167058    -0.00086301     0.00093036
   ht  19    -0.03909116    -0.06768336     0.01798977    -0.00341430    -0.00241700    -0.00096197     0.00011382     0.00036182

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354
   ht  14    -0.01028113     0.03704294     0.00742108    -0.00731018    -0.35460193    -1.56206422
   ht  15     0.00686190     0.01205915     0.00234039    -0.00722090    -0.04720741     0.17214342    -1.33838907
   ht  16    -0.00670938    -0.00824613    -0.00089843     0.00523092     0.23103182     0.38882529    -0.17601112    -1.03121609
   ht  17    -0.00100700    -0.00525102    -0.00005705     0.00281855     0.15212022     0.05531413     0.16603859     0.06623876
   ht  18    -0.00083629     0.00102444     0.00038921    -0.00076516     0.04636236     0.03418393    -0.03367687    -0.13597493
   ht  19    -0.00002262     0.00055879     0.00016446     0.00014052    -0.00927245    -0.01548567     0.02072800     0.02619600

                ht  17         ht  18         ht  19
   ht  17    -0.29450094
   ht  18     0.02517632    -0.08649539
   ht  19     0.00344782     0.00484454    -0.01300878

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.954491      -7.440723E-02  -7.265548E-02  -4.254985E-02   8.480038E-02   5.417772E-02   0.103800      -6.017581E-02
 ref    2  -7.441034E-02   0.957073      -4.490414E-04  -3.353741E-02  -2.365884E-02   3.519016E-02  -4.544889E-02  -0.153172    

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   7.285530E-02   6.001607E-02  -2.863531E-02   4.099155E-02  -4.142420E-02   5.496040E-02   9.771087E-02  -6.060453E-02
 ref    2  -5.131169E-02   1.754937E-02   0.110333      -0.142690      -4.695863E-02   9.987626E-02  -2.326534E-02   3.152951E-02

              v     17       v     18       v     19
 ref    1   0.147083       2.819416E-02   1.788895E-03
 ref    2   2.097355E-02  -1.207106E-02   1.577015E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916591       0.921524       5.279021E-03   2.935248E-03   7.750846E-03   4.173573E-03   1.283994E-02   2.708294E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   7.940783E-03   3.909909E-03   1.299343E-02   2.204068E-02   3.921077E-03   1.299591E-02   1.008869E-02   4.667019E-03

              v     17       v     18       v     19
 ref    1   2.207343E-02   9.406214E-04   2.518977E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18        ev   19

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18       v     19

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95449136    -0.07440723    -0.07265548    -0.04254985     0.08480038     0.05417772     0.10379953    -0.06017581
 ref:   2    -0.07441034     0.95707256    -0.00044904    -0.03353741    -0.02365884     0.03519016    -0.04544889    -0.15317248

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.07285530     0.06001607    -0.02863531     0.04099155    -0.04142420     0.05496040     0.09771087    -0.06060453
 ref:   2    -0.05131169     0.01754937     0.11033333    -0.14268979    -0.04695863     0.09987626    -0.02326534     0.03152951

                ci  17         ci  18         ci  19
 ref:   1     0.14708346     0.02819416     0.00178889
 ref:   2     0.02097355    -0.01207106     0.01577015

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -711.4993471315  1.0144E-07  0.0000E+00  4.9000E-04  1.0000E-03   
 mr-sdci # 18  2   -711.4981776000  5.0884E-05  1.2837E-05  3.5187E-03  1.0000E-03   
 mr-sdci # 18  3   -711.4775112009  1.0553E-04  0.0000E+00  1.5109E-02  1.0000E-04   
 mr-sdci # 18  4   -711.3819087316  8.9098E-03  0.0000E+00  1.1059E-01  1.0000E-04   
 mr-sdci # 18  5   -711.3347602882  6.6670E-03  0.0000E+00  1.4588E-01  1.0000E-04   
 mr-sdci # 18  6   -711.1431931146  1.0176E-01  0.0000E+00  3.9991E-01  1.0000E-04   
 mr-sdci # 18  7   -710.8090909382  3.9409E-02  0.0000E+00  5.6150E-01  1.0000E-04   
 mr-sdci # 18  8   -710.5804081984  1.8492E-02  0.0000E+00  7.1715E-01  1.0000E-04   
 mr-sdci # 18  9   -710.4285510859  2.7528E-02  0.0000E+00  5.9991E-01  1.0000E-04   
 mr-sdci # 18 10   -710.3980704226  5.7289E-03  0.0000E+00  6.8112E-01  1.0000E-04   
 mr-sdci # 18 11   -710.3208729093  5.0249E-03  0.0000E+00  6.6796E-01  1.0000E-04   
 mr-sdci # 18 12   -710.2953898861  3.3789E-02  0.0000E+00  7.0557E-01  1.0000E-04   
 mr-sdci # 18 13   -710.2356253904  9.0737E-04  0.0000E+00  7.2349E-01  1.0000E-04   
 mr-sdci # 18 14   -710.1788059237  6.5479E-02  0.0000E+00  7.3630E-01  1.0000E-04   
 mr-sdci # 18 15   -710.1132462046  4.9107E-03  0.0000E+00  6.6702E-01  1.0000E-04   
 mr-sdci # 18 16   -710.1009502502  5.8533E-02  0.0000E+00  6.6594E-01  1.0000E-04   
 mr-sdci # 18 17   -710.0124762198  1.0883E-01  0.0000E+00  5.9706E-01  1.0000E-04   
 mr-sdci # 18 18   -709.8566867363  9.4688E-02  0.0000E+00  6.6255E-01  1.0000E-04   
 mr-sdci # 18 19   -709.7561462513  2.4101E+02  0.0000E+00  6.1089E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.007370
time for cinew                         0.085865
time for eigenvalue solver             0.000320
time for vector access                 0.000000

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.66868469
   ht   2    -0.00000000  -242.66649064
   ht   3    -0.09646758    -0.00009147   -23.39404955
   ht   4     8.11029498    -0.10996833    -0.62881791    -2.44015823
   ht   5    -0.69707303     0.00271098     0.16787111     0.13873317    -0.25021521
   ht   6     0.94416716    -0.02264136     0.05626321    -0.26071975     0.03622710    -0.12637107
   ht   7     0.08961306    -0.00908617    -0.02222349    -0.03536731    -0.00857230    -0.00370678    -0.01821572
   ht   8     0.10951086     0.00853784    -0.00701079    -0.02770616     0.00536432    -0.02162024     0.00105361    -0.01420274
   ht   9     0.04714197     0.00121795     0.00218301    -0.00014205    -0.00342511     0.00287398     0.00057217     0.00064543
   ht  10    -0.07251039     0.01899662     0.00198636     0.00206398     0.00452750    -0.00563347     0.00074228    -0.00500201
   ht  11    -0.09222063     0.00100131     0.00303672     0.00569610     0.00065887    -0.00003677     0.00093861    -0.00041458
   ht  12     0.03184229    -0.01416487    -0.00597125    -0.00007351    -0.00119811     0.00186441    -0.00074699     0.00093632
   ht  13     0.98889433     0.33080419     0.12493562     0.09841932    -0.02436241     0.06935655    -0.00273032     0.02299357
   ht  14     3.68244141     2.26087687     0.06159528    -0.03516856    -0.03872663     0.11086025    -0.00278001     0.05557899
   ht  15    -0.65239253    -1.01225768    -0.09644134     0.15829073    -0.02820914     0.06391743    -0.01078406     0.02139350
   ht  16     1.37300873    -1.65237486    -0.06240527    -0.13854449     0.04322058    -0.03694217     0.01514720    -0.01429881
   ht  17     0.11331108     0.80095808    -0.06407809    -0.05310801     0.01327850    -0.02775354     0.00011367    -0.01011079
   ht  18     0.38509180    -0.29593624     0.02578759    -0.01528440     0.00047566    -0.00167058    -0.00086301     0.00093036
   ht  19    -0.03909116    -0.06768336     0.01798977    -0.00341430    -0.00241700    -0.00096197     0.00011382     0.00036182
   ht  20    -0.07239777    -0.05018672     0.00968164     0.00476962    -0.00176311     0.00036574    -0.00040489     0.00055278

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00293737
   ht  10     0.00031332    -0.00481090
   ht  11     0.00033632    -0.00020574    -0.00133226
   ht  12    -0.00004306     0.00101680     0.00007577    -0.00092671
   ht  13     0.00598644     0.01244361     0.00366189    -0.00409056    -0.94976354
   ht  14    -0.01028113     0.03704294     0.00742108    -0.00731018    -0.35460193    -1.56206422
   ht  15     0.00686190     0.01205915     0.00234039    -0.00722090    -0.04720741     0.17214342    -1.33838907
   ht  16    -0.00670938    -0.00824613    -0.00089843     0.00523092     0.23103182     0.38882529    -0.17601112    -1.03121609
   ht  17    -0.00100700    -0.00525102    -0.00005705     0.00281855     0.15212022     0.05531413     0.16603859     0.06623876
   ht  18    -0.00083629     0.00102444     0.00038921    -0.00076516     0.04636236     0.03418393    -0.03367687    -0.13597493
   ht  19    -0.00002262     0.00055879     0.00016446     0.00014052    -0.00927245    -0.01548567     0.02072800     0.02619600
   ht  20     0.00004573     0.00043053    -0.00002947    -0.00023197     0.00562607     0.02153110    -0.02697300    -0.03359322

                ht  17         ht  18         ht  19         ht  20
   ht  17    -0.29450094
   ht  18     0.02517632    -0.08649539
   ht  19     0.00344782     0.00484454    -0.01300878
   ht  20     0.01045324    -0.01335369     0.00148784    -0.00775913

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.954443      -7.499511E-02  -7.266018E-02   2.230749E-02  -8.316311E-02  -5.120894E-02  -8.878617E-02  -9.676917E-02
 ref    2  -7.504158E-02   0.956456       1.482406E-03   6.003130E-02   2.665521E-02  -2.983307E-02   3.078979E-02  -8.725261E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   6.244247E-02  -5.104613E-02   4.946800E-03  -9.161794E-03  -6.931176E-02  -9.016971E-02   5.544017E-02  -2.912531E-02
 ref    2  -0.127065      -1.243431E-02  -0.160438       0.109041      -3.832957E-02  -3.700548E-02  -8.647814E-02   2.641285E-02

              v     17       v     18       v     19       v     20
 ref    1   5.338742E-02   0.155312      -2.020986E-02  -1.004240E-03
 ref    2  -7.066827E-03   2.004825E-02   1.166497E-02  -1.612543E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916593       0.920433       5.281699E-03   4.101381E-03   7.626602E-03   3.512367E-03   8.830995E-03   1.697729E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.004455E-02   2.760319E-03   2.576482E-02   1.197384E-02   6.273275E-03   9.499982E-03   1.055208E-02   1.545922E-03

              v     17       v     18       v     19       v     20
 ref    1   2.900156E-03   2.452375E-02   5.445101E-04   2.610380E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18        ev   19        ev   20

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18       v     19       v     20

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95444311    -0.07499511    -0.07266018     0.02230749    -0.08316311    -0.05120894    -0.08878617    -0.09676917
 ref:   2    -0.07504158     0.95645610     0.00148241     0.06003130     0.02665521    -0.02983307     0.03078979    -0.08725261

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.06244247    -0.05104613     0.00494680    -0.00916179    -0.06931176    -0.09016971     0.05544017    -0.02912531
 ref:   2    -0.12706490    -0.01243431    -0.16043801     0.10904083    -0.03832957    -0.03700548    -0.08647814     0.02641285

                ci  17         ci  18         ci  19         ci  20
 ref:   1     0.05338742     0.15531199    -0.02020986    -0.00100424
 ref:   2    -0.00706683     0.02004825     0.01166497    -0.01612543

 trial vector basis is being transformed.  new dimension:   9

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -711.4993471701  3.8652E-08  0.0000E+00  4.9531E-04  1.0000E-03   
 mr-sdci # 19  2   -711.4981941197  1.6520E-05  6.2320E-06  2.6665E-03  1.0000E-03   
 mr-sdci # 19  3   -711.4775416313  3.0430E-05  0.0000E+00  1.4335E-02  1.0000E-04   
 mr-sdci # 19  4   -711.3999895787  1.8081E-02  0.0000E+00  1.4511E-01  1.0000E-04   
 mr-sdci # 19  5   -711.3496819870  1.4922E-02  0.0000E+00  1.5008E-01  1.0000E-04   
 mr-sdci # 19  6   -711.2746504014  1.3146E-01  0.0000E+00  3.0206E-01  1.0000E-04   
 mr-sdci # 19  7   -710.9443556602  1.3526E-01  0.0000E+00  4.3706E-01  1.0000E-04   
 mr-sdci # 19  8   -710.6340667681  5.3659E-02  0.0000E+00  5.4991E-01  1.0000E-04   
 mr-sdci # 19  9   -710.4563698662  2.7819E-02  0.0000E+00  6.4288E-01  1.0000E-04   
 mr-sdci # 19 10   -710.3986962305  6.2581E-04  0.0000E+00  6.8166E-01  1.0000E-04   
 mr-sdci # 19 11   -710.3322534733  1.1381E-02  0.0000E+00  6.6044E-01  1.0000E-04   
 mr-sdci # 19 12   -710.3020836465  6.6938E-03  0.0000E+00  7.0566E-01  1.0000E-04   
 mr-sdci # 19 13   -710.2388323379  3.2069E-03  0.0000E+00  7.1013E-01  1.0000E-04   
 mr-sdci # 19 14   -710.2034404574  2.4635E-02  0.0000E+00  7.1870E-01  1.0000E-04   
 mr-sdci # 19 15   -710.1494692045  3.6223E-02  0.0000E+00  7.5514E-01  1.0000E-04   
 mr-sdci # 19 16   -710.1020536172  1.1034E-03  0.0000E+00  6.7049E-01  1.0000E-04   
 mr-sdci # 19 17   -710.0493992708  3.6923E-02  0.0000E+00  6.8915E-01  1.0000E-04   
 mr-sdci # 19 18   -710.0111414216  1.5445E-01  0.0000E+00  6.0182E-01  1.0000E-04   
 mr-sdci # 19 19   -709.8423407024  8.6194E-02  0.0000E+00  6.8161E-01  1.0000E-04   
 mr-sdci # 19 20   -709.7487308837  2.4100E+02  0.0000E+00  5.9771E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002968
time for cinew                         0.123913
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.75265732
   ht   2     0.00000000  -242.75150427
   ht   3     0.00000000    -0.00000000  -242.73085178
   ht   4     0.00000000    -0.00000000     0.00000000  -242.65329973
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.60299214
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.52796055
   ht   7    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.19766581
   ht   8    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000  -241.88737692
   ht   9     0.00000000     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   ht  10     0.04435918     0.01099718    -0.00400938     0.10814461     0.06061386    -0.24084499     0.12304149    -0.04985555

                ht   9         ht  10
   ht   9  -241.70968002
   ht  10    -0.06549642    -0.00224071

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.954405      -7.547546E-02   7.262416E-02  -1.218923E-02  -6.596524E-02  -7.085254E-02  -7.846354E-02  -0.104668    
 ref    2   7.547525E-02   0.955909      -2.302847E-03  -8.004957E-02   1.667333E-02  -7.971508E-03   2.662592E-02  -7.169286E-02

              v      9       v     10
 ref    1  -6.745195E-02   3.423693E-03
 ref    2   0.113414       7.474652E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916586       0.919458       5.279572E-03   6.556511E-03   4.629413E-03   5.083627E-03   6.865466E-03   1.609517E-02

              v      9       v     10
 ref    1   1.741251E-02   5.598764E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95440536    -0.07547546     0.07262416    -0.01218923    -0.06596524    -0.07085254    -0.07846354    -0.10466758
 ref:   2     0.07547525     0.95590854    -0.00230285    -0.08004957     0.01667333    -0.00797151     0.02662592    -0.07169286

                ci   9         ci  10
 ref:   1    -0.06745195     0.00342369
 ref:   2     0.11341404     0.07474652

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -711.4993472079  3.7802E-08  0.0000E+00  4.9423E-04  1.0000E-03   
 mr-sdci # 20  2   -711.4982024716  8.3519E-06  5.0143E-06  2.2676E-03  1.0000E-03   
 mr-sdci # 20  3   -711.4775468582  5.2269E-06  0.0000E+00  1.4241E-02  1.0000E-04   
 mr-sdci # 20  4   -711.4234615110  2.3472E-02  0.0000E+00  1.7415E-01  1.0000E-04   
 mr-sdci # 20  5   -711.3625817367  1.2900E-02  0.0000E+00  1.4188E-01  1.0000E-04   
 mr-sdci # 20  6   -711.3183155601  4.3665E-02  0.0000E+00  1.8835E-01  1.0000E-04   
 mr-sdci # 20  7   -711.0100759522  6.5720E-02  0.0000E+00  3.8437E-01  1.0000E-04   
 mr-sdci # 20  8   -710.6819355187  4.7869E-02  0.0000E+00  5.0654E-01  1.0000E-04   
 mr-sdci # 20  9   -710.4605009187  4.1311E-03  0.0000E+00  6.3076E-01  1.0000E-04   
 mr-sdci # 20 10   -710.1801503640 -2.1855E-01  0.0000E+00  7.5921E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002651
time for cinew                         0.075558
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.75265732
   ht   2     0.00000000  -242.75150427
   ht   3     0.00000000    -0.00000000  -242.73085178
   ht   4     0.00000000    -0.00000000     0.00000000  -242.65329973
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.60299214
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.52796055
   ht   7    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.19766581
   ht   8    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000  -241.88737692
   ht   9     0.00000000     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   ht  10     0.04435918     0.01099718    -0.00400938     0.10814461     0.06061386    -0.24084499     0.12304149    -0.04985555
   ht  11    -0.03556518    -0.03568651     0.01728400     0.14572235     0.11396764    -0.11692846     0.13588377    -0.09708516

                ht   9         ht  10         ht  11
   ht   9  -241.70968002
   ht  10    -0.06549642    -0.00224071
   ht  11    -0.03448461    -0.00030148    -0.00196395

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.954369      -7.593627E-02   7.257017E-02  -7.965733E-03  -5.354524E-02   7.976822E-02  -6.325319E-02  -0.102053    
 ref    2   7.586082E-02   0.955198      -3.360071E-03  -9.201546E-02   1.533080E-03  -2.901161E-03   2.510541E-02  -5.639372E-02

              v      9       v     10       v     11
 ref    1  -8.599248E-02  -6.372585E-03  -9.975825E-03
 ref    2   5.845684E-02   0.126348       3.340428E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916575       0.918170       5.277720E-03   8.530298E-03   2.869443E-03   6.371386E-03   4.631248E-03   1.359510E-02

              v      9       v     10       v     11
 ref    1   1.081191E-02   1.600445E-02   1.215363E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95436878    -0.07593627     0.07257017    -0.00796573    -0.05354524     0.07976822    -0.06325319    -0.10205315
 ref:   2     0.07586082     0.95519800    -0.00336007    -0.09201546     0.00153308    -0.00290116     0.02510541    -0.05639372

                ci   9         ci  10         ci  11
 ref:   1    -0.08599248    -0.00637259    -0.00997582
 ref:   2     0.05845684     0.12634811     0.03340428

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -711.4993472500  4.2126E-08  0.0000E+00  4.8735E-04  1.0000E-03   
 mr-sdci # 21  2   -711.4982089957  6.5241E-06  2.1635E-06  1.5794E-03  1.0000E-03   
 mr-sdci # 21  3   -711.4775508671  4.0089E-06  0.0000E+00  1.4196E-02  1.0000E-04   
 mr-sdci # 21  4   -711.4514689648  2.8007E-02  0.0000E+00  1.0613E-01  1.0000E-04   
 mr-sdci # 21  5   -711.3714219587  8.8402E-03  0.0000E+00  1.0285E-01  1.0000E-04   
 mr-sdci # 21  6   -711.3285985011  1.0283E-02  0.0000E+00  1.3426E-01  1.0000E-04   
 mr-sdci # 21  7   -711.0779983630  6.7922E-02  0.0000E+00  3.3139E-01  1.0000E-04   
 mr-sdci # 21  8   -710.7667230731  8.4788E-02  0.0000E+00  4.5106E-01  1.0000E-04   
 mr-sdci # 21  9   -710.5082838611  4.7783E-02  0.0000E+00  5.4574E-01  1.0000E-04   
 mr-sdci # 21 10   -710.2369645298  5.6814E-02  0.0000E+00  7.2946E-01  1.0000E-04   
 mr-sdci # 21 11   -710.0303650417 -3.0189E-01  0.0000E+00  8.9358E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003906
time for cinew                         0.079838
time for eigenvalue solver             0.000160
time for vector access                 0.000000

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      909864 2x:       56427 4x:        2529
All internal counts: zz :      547012 yy:     2111106 xx:           0 ww:           0
One-external counts: yz :      525174 yx:           0 yw:           0
Two-external counts: yy :      138726 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:      143612    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.75265732
   ht   2     0.00000000  -242.75150427
   ht   3     0.00000000    -0.00000000  -242.73085178
   ht   4     0.00000000    -0.00000000     0.00000000  -242.65329973
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.60299214
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.52796055
   ht   7    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.19766581
   ht   8    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000  -241.88737692
   ht   9     0.00000000     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   ht  10     0.04435918     0.01099718    -0.00400938     0.10814461     0.06061386    -0.24084499     0.12304149    -0.04985555
   ht  11    -0.03556518    -0.03568651     0.01728400     0.14572235     0.11396764    -0.11692846     0.13588377    -0.09708516
   ht  12     0.07224886    -0.08516098    -0.03966513     0.09535384     0.03521734    -0.07969176     0.08312475    -0.07666957

                ht   9         ht  10         ht  11         ht  12
   ht   9  -241.70968002
   ht  10    -0.06549642    -0.00224071
   ht  11    -0.03448461    -0.00030148    -0.00196395
   ht  12    -0.03261887    -0.00038248    -0.00022577    -0.00083196

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.954358      -7.607986E-02   7.258370E-02  -3.539766E-03  -5.294786E-02   7.973749E-02  -5.811567E-02  -8.941355E-02
 ref    2   7.603121E-02   0.954935      -3.608832E-03  -9.239823E-02  -4.504821E-03  -5.763396E-03   1.238292E-02  -2.766607E-02

              v      9       v     10       v     11       v     12
 ref    1  -0.104615       7.510176E-03  -6.371757E-03  -1.795015E-02
 ref    2   6.454019E-03  -0.154725       4.339474E-02   8.009060E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.916581       0.917689       5.281418E-03   8.549963E-03   2.823769E-03   6.391284E-03   3.530768E-03   8.760194E-03

              v      9       v     10       v     11       v     12
 ref    1   1.098594E-02   2.399613E-02   1.923703E-03   3.863529E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95435843    -0.07607986     0.07258370    -0.00353977    -0.05294786     0.07973749    -0.05811567    -0.08941355
 ref:   2     0.07603121     0.95493500    -0.00360883    -0.09239823    -0.00450482    -0.00576340     0.01238292    -0.02766607

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.10461492     0.00751018    -0.00637176    -0.01795015
 ref:   2     0.00645402    -0.15472469     0.04339474     0.00800906

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -711.4993472734  2.3407E-08  0.0000E+00  4.6948E-04  1.0000E-03   
 mr-sdci # 22  2   -711.4982112895  2.2938E-06  5.8320E-07  8.2351E-04  1.0000E-03   
 mr-sdci # 22  3   -711.4775512177  3.5060E-07  0.0000E+00  1.4222E-02  1.0000E-04   
 mr-sdci # 22  4   -711.4594743102  8.0053E-03  0.0000E+00  6.6317E-02  1.0000E-04   
 mr-sdci # 22  5   -711.3741373014  2.7153E-03  0.0000E+00  8.8257E-02  1.0000E-04   
 mr-sdci # 22  6   -711.3300311491  1.4326E-03  0.0000E+00  1.2867E-01  1.0000E-04   
 mr-sdci # 22  7   -711.1341858684  5.6188E-02  0.0000E+00  2.6942E-01  1.0000E-04   
 mr-sdci # 22  8   -710.8316145866  6.4892E-02  0.0000E+00  4.0872E-01  1.0000E-04   
 mr-sdci # 22  9   -710.5516189919  4.3335E-02  0.0000E+00  4.8634E-01  1.0000E-04   
 mr-sdci # 22 10   -710.3058454347  6.8881E-02  0.0000E+00  6.8028E-01  1.0000E-04   
 mr-sdci # 22 11   -710.1969874800  1.6662E-01  0.0000E+00  7.3541E-01  1.0000E-04   
 mr-sdci # 22 12   -709.9616264002 -3.4046E-01  0.0000E+00  9.4692E-01  1.0000E-04   
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000576
time for cinew                         0.080265
time for eigenvalue solver             0.000168
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after 22 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -711.4993472734  2.3407E-08  0.0000E+00  4.6948E-04  1.0000E-03   
 mr-sdci # 22  2   -711.4982112895  2.2938E-06  5.8320E-07  8.2351E-04  1.0000E-03   
 mr-sdci # 22  3   -711.4775512177  3.5060E-07  0.0000E+00  1.4222E-02  1.0000E-04   
 mr-sdci # 22  4   -711.4594743102  8.0053E-03  0.0000E+00  6.6317E-02  1.0000E-04   
 mr-sdci # 22  5   -711.3741373014  2.7153E-03  0.0000E+00  8.8257E-02  1.0000E-04   
 mr-sdci # 22  6   -711.3300311491  1.4326E-03  0.0000E+00  1.2867E-01  1.0000E-04   
 mr-sdci # 22  7   -711.1341858684  5.6188E-02  0.0000E+00  2.6942E-01  1.0000E-04   
 mr-sdci # 22  8   -710.8316145866  6.4892E-02  0.0000E+00  4.0872E-01  1.0000E-04   
 mr-sdci # 22  9   -710.5516189919  4.3335E-02  0.0000E+00  4.8634E-01  1.0000E-04   
 mr-sdci # 22 10   -710.3058454347  6.8881E-02  0.0000E+00  6.8028E-01  1.0000E-04   
 mr-sdci # 22 11   -710.1969874800  1.6662E-01  0.0000E+00  7.3541E-01  1.0000E-04   
 mr-sdci # 22 12   -709.9616264002 -3.4046E-01  0.0000E+00  9.4692E-01  1.0000E-04   

####################CIUDGINFO####################

   ci vector at position   1 energy= -711.499347273445
   ci vector at position   2 energy= -711.498211289505

################END OF CIUDGINFO################

 
    2 of the  13 expansion vectors are transformed.
    2 of the  12 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -711.4993472734

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18   19   20   21   22   23

                                          orbital    13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29
                                               30   31   32   33   34   35

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1 -0.129080                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +    +       
 z*  3  1       2 -0.021241                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +         +  
 z*  3  1       4 -0.931993                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-   +       
 z*  3  1       5 -0.076140                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-        +  
 z*  3  1      13 -0.036214                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-   +    +  
 z*  3  1      16 -0.046736                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-   +       
 z*  3  1      17 -0.072914                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-        +  
 z*  3  1      20 -0.012947                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -   +  
 z*  3  1      21 -0.010083                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +    +     - 
 z*  3  1      25 -0.047818                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-   +    +  
 z*  3  1      29 -0.110373                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-    -   +  
 z*  3  1      30 -0.031455                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-   +     - 
 z*  3  1      31  0.012848                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-        +- 
 z*  3  1      37  0.010296                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +         +-   +-   +  
 z*  3  1      38 -0.041899                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +         +-   +    +- 
 z*  3  1      40  0.032107                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-   +    +  
 z*  3  1      42  0.013137                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +    +    +- 
 z*  3  1      44  0.097850                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-   +    +- 
 z   3  1      46 -0.015589                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +-   +       
 z   3  1      55 -0.012658                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-   +    +  
 z   3  1      58 -0.010045                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +    +-   +-      
 z   3  1      59  0.011815                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +    +-    -   +  
 z   3  1      60 -0.020258                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +    +-   +     - 
 z   3  1      70 -0.015390                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +-   +    +  
 z   3  1      76 -0.017932                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-   +-   +-      
 z   3  1      78  0.017211                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-   +-   +     - 
 z   3  1      85 -0.010899                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +     -   +-   +-   +  
 z   3  1      88  0.032873                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +    +-   +-    - 
 z   3  1     108  0.021687                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-   +-      
 z   3  1     110 -0.013796                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-   +     - 
 z   3  1     111 -0.038737                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-        +- 
 z   3  1     123  0.024345                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +    +-   +-   +  
 z   3  1     128  0.011781                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +    +-   +-   +     - 
 z   3  1     135 -0.010556                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +     -   +-   +-   +  
 z   3  1     138  0.024099                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +    +    +-   +-    - 
 y   3  1    1252 -0.011786              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-           
 y   3  1    1762 -0.015988              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-        +  
 y   3  1    2323 -0.010164              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-    -   +    +-   +       
 y   3  1    2935 -0.026184              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-           
 y   3  1    3003 -0.012182             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +       
 y   3  1    3409  0.016916             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-   +       
 y   3  1    3445  0.023824              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-        +  
 y   3  1    3853 -0.038114              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-    -      
 y   3  1    3904 -0.042067              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-         - 
 y   3  1    4771  0.014316              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-   +       
 y   3  1    5383  0.011471              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-   +-      
 y   3  1    5485  0.012326              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-   +     - 
 y   3  1    8392  0.012814              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +     -      
 y   3  1    8596 -0.012337              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-   +       
 y   3  1    8647  0.012892              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-        +  
 y   3  1    9055  0.096856              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +    +-    -      
 y   3  1    9106 -0.036085              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +    +-         - 
 y   3  1    9514 -0.014927              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-        +-    -   +  
 y   3  1    9565  0.035144              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-        +-   +     - 
 y   3  1    9820 -0.027014              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +-   +       
 y   3  1   10279 -0.043207              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -    -   +-   +    +  
 y   3  1   10432  0.010102              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +    +-   +-      
 y   3  1   10483  0.021152              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +    +-    -   +  
 y   3  1   10534 -0.068015              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +    +-   +     - 
 y   3  1   11554 -0.012287              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +     -   +-    -   +  
 y   3  1   12778 -0.018710              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +         +    +-    -   +- 
 y   3  1   15481 -0.017597              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-   +       
 y   3  1   15496  0.010344             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-   +       
 y   3  1   15940  0.013632              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-    -      
 y   3  1   15958 -0.013450             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-    -      
 y   3  1   15963  0.013067             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-    -      
 y   3  1   15972  0.011245             33( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-    -      
 y   3  1   15973  0.015493             34( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-    -      
 y   3  1   16013 -0.010793             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-         - 
 y   3  1   17164  0.013188              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -    -   +-   +    +  
 y   3  1   17419  0.020662              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +    +-   +     - 
 y   3  1   17437  0.011850             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +    +-   +     - 
 y   3  1   21252 -0.010124              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +    +-   +     - 
 y   3  1   21254  0.013445             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +    +-   +     - 
 y   3  1   34302 -0.012262              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-   +       
 y   3  1   35023  0.012463             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +    +-   +     - 
 y   3  1   35024  0.010773             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +    +-   +     - 
 y   3  1   41907  0.011187              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +    +-   +     - 
 y   3  1   54401  0.011179              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-        +  
 y   3  1   55574 -0.010462              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-   +-      
 y   3  1   68171  0.012823              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-        +  
 y   3  1   69344 -0.011811              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-   +-      
 y   3  1   69447 -0.011644              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-   +     - 
 y   3  1   81942  0.010638              9( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-        +  
 y   3  1   83219  0.010035             11( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-   +     - 
 y   3  1   84333 -0.010920              3( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-   +       
 y   3  1   98103 -0.012512              3( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-   +       
 y   3  1  110757 -0.010448              9( a  )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-   +     - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              85
     0.01> rq > 0.001           3957
    0.001> rq > 0.0001         22978
   0.0001> rq > 0.00001        46969
  0.00001> rq > 0.000001       41440
 0.000001> rq                  14492
           all                129924
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      251850 2x:           0 4x:           0
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:        6247    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.129079914104     91.828629900351
     2     2     -0.021241218047     15.111978888101
     3     3     -0.007928977730      5.640372046193
     4     4     -0.931993140040    663.029003928607
     5     5     -0.076140290970     54.169614382326
     6     6     -0.006686156384      4.757023020416
     7     7      0.000244442655     -0.172699910817
     8     8      0.008552204537     -6.084210396174
     9     9      0.000626852753     -0.444450957403
    10    10     -0.001499927391      1.066922502508
    11    11     -0.001653424798      1.176452762097
    12    12     -0.002512345055      1.786685024629
    13    13     -0.036214196366     25.767489862954
    14    14      0.000218017281     -0.155023133140
    15    15      0.004485150106     -3.190589896784
    16    16     -0.046735529654     33.259610506432
    17    17     -0.072914334649     51.878889644547
    18    18      0.002397139572     -1.705393740474
    19    19     -0.003351658346      2.385198330760
    20    20     -0.012946832091      9.211271391132
    21    21     -0.010083086236      7.174472092017
    22    22      0.001624379588     -1.154594449883
    23    23      0.001084980816     -0.772029730760
    24    24      0.000722694541     -0.514013867149
    25    25     -0.047817943400     34.022074866216
    26    26      0.001648175909     -1.172634702625
    27    27     -0.008905707882      6.335962466337
    28    28      0.001205118430     -0.854876604501
    29    29     -0.110373492503     78.529761551801
    30    30     -0.031454846858     22.381294513758
    31    31      0.012847583070     -9.134344353685
    32    32     -0.000021234381      0.015068757086
    33    33     -0.000729521014      0.519140738479
    34    34      0.000910094064     -0.647469575950
    35    35      0.000746520769     -0.531431545450
    36    36     -0.000064450142      0.045905948608
    37    37      0.010296425295     -7.327645178604
    38    38     -0.041899466826     29.809029600799
    39    39     -0.000007532366      0.005376671564
    40    40      0.032107135152    -22.842944161509
    41    41     -0.001366337407      0.971611728117
    42    42      0.013136620799     -9.346245508434
    43    43     -0.002951427873      2.095737213942
    44    44      0.097850000680    -69.618015748420
    45    45      0.000289355434     -0.206031170297

 number of reference csfs (nref) is    45.  root number (iroot) is  1.
 c0**2 =   0.92919582  c**2 (all zwalks) =   0.93675367

 pople ci energy extrapolation is computed with 42 correlated electrons.

 eref      =   -711.413450035199   "relaxed" cnot**2         =   0.929195820405
 eci       =   -711.499347273445   deltae = eci - eref       =  -0.085897238247
 eci+dv1   =   -711.505429156929   dv1 = (1-cnot**2)*deltae  =  -0.006081883484
 eci+dv2   =   -711.505892592903   dv2 = dv1 / cnot**2       =  -0.006545319458
 eci+dv3   =   -711.506432481635   dv3 = dv1 / (2*cnot**2-1) =  -0.007085208190
 eci+pople =   -711.506040462652   ( 42e- scaled deltae )    =  -0.092590427453


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -711.4982112895

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18   19   20   21   22   23

                                          orbital    13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29
                                               30   31   32   33   34   35

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1 -0.920539                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +    +       
 z*  3  1       2 -0.167071                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +         +  
 z*  3  1       4  0.130126                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-   +       
 z*  3  1       5  0.012035                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-        +  
 z*  3  1       6 -0.049625                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -   +    +  
 z*  3  1       8  0.065853                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +     -   +  
 z*  3  1      10 -0.012167                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +         +- 
 z*  3  1      15  0.034094                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +    +    +- 
 z*  3  1      18  0.020232                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +    +  
 z*  3  1      19 -0.026773                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +    +-      
 z*  3  1      20 -0.091869                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -   +  
 z*  3  1      21 -0.071513                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +    +     - 
 z*  3  1      22  0.012964                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +         +- 
 z*  3  1      26  0.012049                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +    +-   +  
 z*  3  1      27 -0.066344                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +    +    +- 
 z*  3  1      29  0.015357                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-    -   +  
 z*  3  1      41 -0.011022                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +    +-   +  
 z*  3  1      42  0.093555                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +    +    +- 
 z*  3  1      44 -0.013760                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-   +    +- 
 z   3  1      48 -0.012185                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -   +    +  
 z   3  1      49 -0.025680                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +    +-      
 z   3  1      50  0.014066                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +     -   +  
 z   3  1      51 -0.012179                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +    +     - 
 z   3  1      64 -0.010773                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +    +    +-    - 
 z   3  1      80 -0.011037                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-    -   +-   +  
 z   3  1      82  0.030201                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-   +    +-    - 
 z   3  1     100 -0.014792                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +     -   +  
 z   3  1     101 -0.020690                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +    +     - 
 z   3  1     102 -0.036283                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +         +- 
 z   3  1     121  0.021304                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +    +-   +  
 z   3  1     132  0.022518                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +    +-   +    +-    - 
 y   3  1     946 -0.023677              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +            
 y   3  1    1011 -0.012849             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-        +       
 y   3  1    1014 -0.015883             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-        +       
 y   3  1    1456  0.017433              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +          - 
 y   3  1    1981 -0.010801             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +    +     - 
 y   3  1    2986  0.012613              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +       
 y   3  1    3037  0.014205              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -        +  
 y   3  1    3088 -0.040074              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -      
 y   3  1    3139 -0.040054              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +          - 
 y   3  1    5026  0.011152              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +    +     - 
 y   3  1    8290 -0.032971              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -   +       
 y   3  1    8341  0.014173              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -        +  
 y   3  1    8392  0.090571              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +     -      
 y   3  1    8443 -0.040689              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +          - 
 y   3  1    8698  0.029012              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -    -   +    +  
 y   3  1    8749 -0.010802              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +    +-      
 y   3  1    8800 -0.022070              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +     -   +  
 y   3  1    8851  0.039868              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +    +     - 
 y   3  1    9055 -0.013566              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +    +-    -      
 y   3  1    9922 -0.043664              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-    -   +    +  
 y   3  1    9973  0.012862              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +    +-      
 y   3  1   10024  0.027301              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +     -   +  
 y   3  1   10075 -0.067538              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +    +     - 
 y   3  1   10126 -0.011247              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +         +- 
 y   3  1   12523 -0.016680              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +         +-   +     -   +- 
 y   3  1   15295 -0.013130             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +     -      
 y   3  1   15300  0.012843             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +     -      
 y   3  1   15309  0.010716             33( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +     -      
 y   3  1   15310  0.014812             34( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +     -      
 y   3  1   15350 -0.010821             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +          - 
 y   3  1   16807  0.011801              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-    -   +    +  
 y   3  1   16960  0.019461              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +    +     - 
 y   3  1   16978  0.012140             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +    +     - 
 y   3  1   20795  0.013335             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +    +     - 
 y   3  1   33536  0.010120              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +    +       
 y   3  1   33537 -0.011712              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +    +       
 y   3  1   33551  0.010367             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +    +       
 y   3  1   33842 -0.010207              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +    +-      
 y   3  1   33843  0.012088              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +    +-      
 y   3  1   33857 -0.011033             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +    +-      
 y   3  1   33944 -0.010106              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +    +     - 
 y   3  1   33945  0.013934              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +    +     - 
 y   3  1   34564  0.012003             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +    +     - 
 y   3  1   34565  0.010408             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +    +     - 
 y   3  1   41448  0.010919              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +-   +    +     - 
 y   3  1   68018  0.010953              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +         +  
 y   3  1   68885 -0.010925              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +    +-      
 y   3  1   68988 -0.011155              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +    +     - 
 y   3  1   84026  0.010422              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -   +       
 y   3  1   84027 -0.012811              3( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -   +       
 y   3  1   97796  0.013995              2( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -   +       
 y   3  1   97797 -0.014566              3( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -   +       

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              80
     0.01> rq > 0.001           4164
    0.001> rq > 0.0001         24893
   0.0001> rq > 0.00001        50307
  0.00001> rq > 0.000001       37313
 0.000001> rq                  13164
           all                129924
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      251850 2x:           0 4x:           0
All internal counts: zz :      547012 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      378885    task #     2:      348587    task #     3:     1524165    task #     4:       57186
task #     5:       55268    task #     6:        6247    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.920538935196    654.879009758794
     2     2     -0.167071259479    118.861305010560
     3     3      0.001090282247     -0.775584949931
     4     4      0.130125603601    -92.572533282584
     5     5      0.012034915733     -8.562126326471
     6     6     -0.049624869738     35.306743730583
     7     7      0.003739995182     -2.651458491043
     8     8      0.065852874069    -46.848547815456
     9     9      0.008453302926     -6.002770704861
    10    10     -0.012166716681      8.654510131669
    11    11      0.000149005407     -0.106028723175
    12    12      0.000544670348     -0.387408666639
    13    13      0.005457079938     -3.882851685882
    14    14      0.001238611389     -0.880546233449
    15    15      0.034093824475    -24.253049695171
    16    16      0.007061439239     -5.025035206525
    17    17      0.009484491989     -6.748302493870
    18    18      0.020231840539    -14.393445743140
    19    19     -0.026773082139     19.052713364733
    20    20     -0.091869277030     65.362194778577
    21    21     -0.071513389168     50.884606365380
    22    22      0.012963976153     -9.215181312552
    23    23     -0.000042780403      0.030418083857
    24    24     -0.000501061960      0.356459787106
    25    25      0.005973939932     -4.250385648376
    26    26      0.012049453457     -8.572720493947
    27    27     -0.066343652596     47.199902009612
    28    28      0.000008968808     -0.006835764225
    29    29      0.015356815398    -10.926164833259
    30    30      0.004880466165     -3.472588062480
    31    31     -0.001637881707      1.164476821177
    32    32     -0.000938495130      0.667542123647
    33    33     -0.001219741324      0.868807962192
    34    34      0.005995387608     -4.265139410384
    35    35      0.004726368228     -3.365082971265
    36    36      0.000004521081     -0.003204669700
    37    37     -0.001288571571      0.917122451800
    38    38      0.006072332053     -4.320058863756
    39    39      0.000034847476     -0.024533149940
    40    40     -0.004311812983      3.067656110556
    41    41     -0.011021879614      7.838112947272
    42    42      0.093555219228    -66.561015507938
    43    43      0.000509046451     -0.361616990179
    44    44     -0.013760207838      9.789982600172
    45    45      0.001926408565     -1.371944133532

 number of reference csfs (nref) is    45.  root number (iroot) is  2.
 c0**2 =   0.92962840  c**2 (all zwalks) =   0.93621644

 pople ci energy extrapolation is computed with 42 correlated electrons.

 eref      =   -711.411344463164   "relaxed" cnot**2         =   0.929628397816
 eci       =   -711.498211289505   deltae = eci - eref       =  -0.086866826341
 eci+dv1   =   -711.504324247252   dv1 = (1-cnot**2)*deltae  =  -0.006112957746
 eci+dv2   =   -711.504786989818   dv2 = dv1 / cnot**2       =  -0.006575700313
 eci+dv3   =   -711.505325527970   dv3 = dv1 / (2*cnot**2-1) =  -0.007114238464
 eci+pople =   -711.504932303923   ( 42e- scaled deltae )    =  -0.093587840758
maximum overlap with reference    1(overlap= 0.95436)
weight of reference states=  0.9166

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.95494)
weight of reference states=  0.9177

 information on vector: 2 from unit 11 written to unit 48 filename civout              
 passed aftci ... 
                       Size (real*8) of d2temp for two-external contributions    1054527
 
                       Size (real*8) of d2temp for all-internal contributions      44022
                       Size (real*8) of d2temp for one-external contributions     351900
                       Size (real*8) of d2temp for two-external contributions    1054527
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   52    70125
                1    2    1    1   52    70125
                1    3    1    1   52    70125
                1    4    1    1   52    70125
                1    5    1    1   52    70125
                1    6    1    1   52    70125
                1    7    1    1   52    70125
                1    8    1    1   52    70125
                1    9    1    1   52    70125
                1   10    1    1   52    70125
                1   11    1    1   52    70125
                1   12    1    1   52    70125
                1   13    1    1   52    70125
                1   14    1    1   52    70125
                1   15    1    1   52    70125
                1   16    1    1   52    70125
                1   17    1    1   52    70125
                1   18    1    1   52    70125
                1   19    1    1   52    70125
                1   20    1    1   52    70125
                1   21    1    1   52    70125
                1   22    1    1   52    70125
                1   23    1    1   52    70125
                       Size (real*8) of d2temp for three-external contributions    1612875
                       Size (real*8) of d2temp for four-external contributions     948753
 serial operation: forcing vdisk for temporary dd012 matrices
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart= 1468417
 files%d4ext =     unit=  24  vdsk=   1  filestart= 3088417
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                  6145     55297    411649         1         1
d2rec                     8        58       172        27        16
recsize                6144      6144      6144     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                4048417  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -468.746689847934     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.49934727  -242.75265743
 residuum:     0.00046948
 deltae:     0.00000002

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95435843    -0.07607986     0.07258370    -0.00353977    -0.05294786     0.07973749    -0.05811567    -0.08941355
 ref:   2     0.07603121     0.95493500    -0.00360883    -0.09239823    -0.00450482    -0.00576340     0.01238292    -0.02766607

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.10461492     0.00751018    -0.00637176    -0.01795015    -0.06931176    -0.09016971     0.05544017    -0.02912531
 ref:   2     0.00645402    -0.15472469     0.04339474     0.00800906    -0.03832957    -0.03700548    -0.08647814     0.02641285

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24
 ref:   1     0.05338742     0.15531199    -0.02020986    -0.00100424     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00706683     0.02004825     0.01166497    -0.01612543     0.00000000     0.00000000     0.00000000     0.00000000

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95435843    -0.07607986     0.07258370    -0.00353977    -0.05294786     0.07973749    -0.05811567    -0.08941355
 ref:   2     0.07603121     0.95493500    -0.00360883    -0.09239823    -0.00450482    -0.00576340     0.01238292    -0.02766607

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.10461492     0.00751018    -0.00637176    -0.01795015     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2     0.00645402    -0.15472469     0.04339474     0.00800906     0.00000000     0.00000000     0.00000000     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=   11036  DYX=       0  DYW=       0
   D0Z=   17456  D0Y=   53891  D0X=       0  D0W=       0
  DDZI=   21345 DDYI=   56427 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    2529 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=   11036  DYX=       0  DYW=       0
   D0Z=   17456  D0Y= 2111106  D0X=       0  D0W=       0
  DDZI=  251850 DDYI=  658014 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    42.000000
   42  correlated and    24  frozen core electrons
   7396 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000    
  a     6    0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000    
  a     7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000    
  a     8    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000    

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     9    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    10    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    11    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    12    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a    13    0.00000        0.00000        0.00000        0.00000        1.99904       2.021741E-04   5.991612E-04   6.601640E-05
  a    14    0.00000        0.00000        0.00000        0.00000       2.021741E-04    1.99934      -5.989861E-04   1.392014E-04
  a    15    0.00000        0.00000        0.00000        0.00000       5.991612E-04  -5.989861E-04    1.99872      -5.214568E-04
  a    16    0.00000        0.00000        0.00000        0.00000       6.601640E-05   1.392014E-04  -5.214568E-04    1.99852    
  a    17    0.00000        0.00000        0.00000        0.00000      -1.753673E-04  -2.204297E-04  -5.190516E-05   3.302710E-04
  a    18    0.00000        0.00000        0.00000        0.00000      -2.765990E-04  -1.445374E-04   3.388680E-04  -2.574951E-04
  a    19    0.00000        0.00000        0.00000        0.00000       4.043835E-04   4.978309E-06  -7.412576E-04  -3.592572E-04
  a    20    0.00000        0.00000        0.00000        0.00000       1.181091E-04  -2.420767E-04  -5.245142E-04  -2.105314E-05
  a    21    0.00000        0.00000        0.00000        0.00000       2.501013E-04  -2.235414E-04  -5.595730E-04  -1.680603E-04
  a    22    0.00000        0.00000        0.00000        0.00000       2.870713E-04   1.652058E-04  -9.051716E-05  -3.370952E-04
  a    23    0.00000        0.00000        0.00000        0.00000       1.447232E-05   2.789442E-06  -1.753241E-04  -1.330616E-04
  a    24    0.00000        0.00000        0.00000        0.00000       2.246839E-04  -2.202698E-05  -2.575412E-04  -1.164524E-04
  a    25    0.00000        0.00000        0.00000        0.00000      -1.744861E-04   1.352309E-04   1.696072E-04   3.043791E-04
  a    26    0.00000        0.00000        0.00000        0.00000       3.882289E-05   2.753873E-04   3.772307E-04  -5.385731E-04
  a    27    0.00000        0.00000        0.00000        0.00000       1.770873E-05  -1.198907E-05  -3.240526E-06  -8.675412E-05
  a    28    0.00000        0.00000        0.00000        0.00000      -4.287731E-04   2.899881E-04   8.057944E-04   3.661652E-04
  a    29    0.00000        0.00000        0.00000        0.00000       9.845426E-06   1.219705E-05   2.191269E-05  -2.663220E-06
  a    30    0.00000        0.00000        0.00000        0.00000      -8.173033E-06  -2.014142E-05   4.818853E-06  -1.345242E-05
  a    31    0.00000        0.00000        0.00000        0.00000       8.321172E-07  -9.799196E-06   1.120622E-05   4.338026E-05
  a    32    0.00000        0.00000        0.00000        0.00000       1.692478E-04   1.298831E-04   1.328054E-04  -9.285553E-05
  a    33    0.00000        0.00000        0.00000        0.00000       1.854223E-04  -1.200816E-04  -3.136272E-04   1.338152E-04
  a    34    0.00000        0.00000        0.00000        0.00000      -1.586007E-04   1.290530E-04  -4.491903E-04   1.594110E-04
  a    35    0.00000        0.00000        0.00000        0.00000      -2.085009E-04   1.049814E-06   1.287362E-04   2.438939E-04
  a    36    0.00000        0.00000        0.00000        0.00000       2.254704E-06   2.889749E-05  -6.386403E-05  -9.235863E-05
  a    37    0.00000        0.00000        0.00000        0.00000      -1.510324E-03  -1.228383E-03   8.736434E-05   3.023860E-03
  a    38    0.00000        0.00000        0.00000        0.00000       7.481103E-04  -9.382708E-04  -2.963770E-03  -6.232296E-04
  a    39    0.00000        0.00000        0.00000        0.00000       3.580452E-04  -8.202456E-04   5.382136E-04  -1.361287E-03
  a    40    0.00000        0.00000        0.00000        0.00000       1.050978E-03   7.907901E-04   2.096790E-03   1.429161E-03
  a    41    0.00000        0.00000        0.00000        0.00000      -8.762932E-04  -1.147600E-03  -1.883786E-04  -7.856103E-04
  a    42    0.00000        0.00000        0.00000        0.00000       7.019377E-04   1.779422E-04  -9.322656E-04  -1.951706E-04
  a    43    0.00000        0.00000        0.00000        0.00000       3.512824E-03  -1.389255E-03  -1.638774E-03   3.457313E-03
  a    44    0.00000        0.00000        0.00000        0.00000       1.823946E-03  -2.324944E-03  -3.016940E-03   1.034815E-03
  a    45    0.00000        0.00000        0.00000        0.00000      -3.098752E-04   1.314365E-03   3.743294E-03   1.605657E-03
  a    46    0.00000        0.00000        0.00000        0.00000      -3.129721E-04  -1.246362E-03   5.849625E-05  -3.481549E-04
  a    47    0.00000        0.00000        0.00000        0.00000       3.683937E-04   7.362014E-04   1.105626E-04  -1.242784E-04
  a    48    0.00000        0.00000        0.00000        0.00000       5.715082E-05   9.456926E-04   6.545746E-04   6.993278E-04
  a    49    0.00000        0.00000        0.00000        0.00000       5.397263E-04  -1.922850E-03  -1.621084E-03   1.042210E-03
  a    50    0.00000        0.00000        0.00000        0.00000       6.430971E-04   3.124404E-04  -3.111625E-04   3.571412E-05
  a    51    0.00000        0.00000        0.00000        0.00000      -7.706725E-05  -1.079518E-04  -7.191576E-05   7.775258E-05
  a    52    0.00000        0.00000        0.00000        0.00000      -5.174742E-04  -9.962024E-04  -1.528514E-04   1.332700E-03
  a    53    0.00000        0.00000        0.00000        0.00000      -2.200411E-04  -1.391980E-04  -5.865740E-04   7.631697E-05
  a    54    0.00000        0.00000        0.00000        0.00000      -5.824761E-05  -7.020835E-05  -1.329529E-04  -1.135523E-04
  a    55    0.00000        0.00000        0.00000        0.00000       1.392099E-03   8.659799E-04   1.482721E-03   2.065930E-03
  a    56    0.00000        0.00000        0.00000        0.00000       1.107459E-03  -1.215563E-03  -1.815398E-03   4.716140E-04
  a    57    0.00000        0.00000        0.00000        0.00000       1.558541E-03  -8.342586E-04  -2.224937E-03   9.619029E-04
  a    58    0.00000        0.00000        0.00000        0.00000      -3.627925E-05  -5.813377E-05  -5.108664E-05  -2.388373E-05
  a    59    0.00000        0.00000        0.00000        0.00000       1.403347E-04  -9.317153E-05  -1.138950E-04  -4.184592E-05
  a    60    0.00000        0.00000        0.00000        0.00000       8.345363E-04  -1.410267E-03  -2.029994E-03  -2.936607E-04
  a    61    0.00000        0.00000        0.00000        0.00000      -4.147706E-04   7.224398E-04   1.083517E-03  -4.831626E-04
  a    62    0.00000        0.00000        0.00000        0.00000      -2.726726E-04   2.538023E-04   2.470026E-04  -2.271791E-04
  a    63    0.00000        0.00000        0.00000        0.00000       1.301727E-03  -5.518093E-04  -2.050924E-04   2.952155E-04
  a    64    0.00000        0.00000        0.00000        0.00000      -2.869620E-04  -6.457763E-05   1.616057E-05   1.333281E-04
  a    65    0.00000        0.00000        0.00000        0.00000       1.718502E-04  -5.536850E-05  -4.563312E-05   3.260489E-04
  a    66    0.00000        0.00000        0.00000        0.00000      -8.143881E-05  -3.520721E-04  -2.992187E-04  -1.735941E-04
  a    67    0.00000        0.00000        0.00000        0.00000       3.187533E-04   2.102759E-04   7.913335E-05  -2.863023E-04
  a    68    0.00000        0.00000        0.00000        0.00000       2.264097E-04  -2.660703E-04  -1.595755E-05   1.704651E-04
  a    69    0.00000        0.00000        0.00000        0.00000       3.146679E-05   3.245054E-04  -1.232618E-04  -3.358355E-04
  a    70    0.00000        0.00000        0.00000        0.00000      -8.507790E-05   2.572448E-04  -1.748911E-04  -4.642666E-05
  a    71    0.00000        0.00000        0.00000        0.00000      -3.879041E-04   8.423858E-04   1.175127E-04   8.724554E-06
  a    72    0.00000        0.00000        0.00000        0.00000      -2.894200E-04  -8.497893E-04  -5.926114E-04  -7.232603E-04
  a    73    0.00000        0.00000        0.00000        0.00000       1.791547E-05   1.075625E-05  -1.310285E-04  -1.096476E-04
  a    74    0.00000        0.00000        0.00000        0.00000      -4.760464E-04  -7.589123E-04   4.861556E-04   3.261345E-04
  a    75    0.00000        0.00000        0.00000        0.00000      -1.358345E-03  -6.883339E-04  -5.292465E-04  -5.686861E-04
  a    76    0.00000        0.00000        0.00000        0.00000       9.098950E-05   6.228142E-04   2.211301E-05  -3.308678E-04
  a    77    0.00000        0.00000        0.00000        0.00000      -1.203246E-03   4.032829E-04   1.025674E-03   8.609787E-04
  a    78    0.00000        0.00000        0.00000        0.00000      -1.957231E-04   6.969226E-05  -8.743121E-04  -1.263548E-03
  a    79    0.00000        0.00000        0.00000        0.00000       6.114558E-04  -2.944879E-04  -2.961108E-04   2.256381E-04
  a    80    0.00000        0.00000        0.00000        0.00000       1.952911E-04   2.645138E-04  -4.225538E-04  -7.378538E-04
  a    81    0.00000        0.00000        0.00000        0.00000      -2.171589E-04   5.484717E-04   8.365687E-04   1.680893E-04
  a    82    0.00000        0.00000        0.00000        0.00000      -3.498987E-04  -1.340623E-04  -1.321182E-04   4.157286E-05
  a    83    0.00000        0.00000        0.00000        0.00000      -2.352459E-04   3.707826E-04  -5.731768E-05  -4.509650E-04
  a    84    0.00000        0.00000        0.00000        0.00000       1.114295E-03  -2.521676E-04  -1.062573E-03   1.542431E-04
  a    85    0.00000        0.00000        0.00000        0.00000      -6.669500E-04  -3.063594E-04  -3.859968E-04   8.728307E-05
  a    86    0.00000        0.00000        0.00000        0.00000       8.037560E-04  -1.335527E-03  -1.705528E-03   2.601886E-04

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a    13  -1.753673E-04  -2.765990E-04   4.043835E-04   1.181091E-04   2.501013E-04   2.870713E-04   1.447232E-05   2.246839E-04
  a    14  -2.204297E-04  -1.445374E-04   4.978309E-06  -2.420767E-04  -2.235414E-04   1.652058E-04   2.789442E-06  -2.202698E-05
  a    15  -5.190516E-05   3.388680E-04  -7.412576E-04  -5.245142E-04  -5.595730E-04  -9.051716E-05  -1.753241E-04  -2.575412E-04
  a    16   3.302710E-04  -2.574951E-04  -3.592572E-04  -2.105314E-05  -1.680603E-04  -3.370952E-04  -1.330616E-04  -1.164524E-04
  a    17    1.99831      -1.825042E-04  -3.550391E-05  -3.441432E-05  -2.393130E-04   2.740696E-04   2.202871E-05   6.606282E-04
  a    18  -1.825042E-04    1.99812       2.828024E-04   2.780323E-04   7.058853E-04   2.064532E-04   3.105152E-04   3.203491E-04
  a    19  -3.550391E-05   2.828024E-04    1.99761      -7.032323E-04  -4.731909E-04  -7.008017E-04  -4.998951E-04  -7.663470E-04
  a    20  -3.441432E-05   2.780323E-04  -7.032323E-04    1.99866      -2.320288E-04  -2.415358E-04  -1.103541E-04  -5.048451E-04
  a    21  -2.393130E-04   7.058853E-04  -4.731909E-04  -2.320288E-04    1.99774      -2.864724E-06  -1.257734E-03  -3.733199E-04
  a    22   2.740696E-04   2.064532E-04  -7.008017E-04  -2.415358E-04  -2.864724E-06    1.99884       1.643063E-04  -5.068807E-04
  a    23   2.202871E-05   3.105152E-04  -4.998951E-04  -1.103541E-04  -1.257734E-03   1.643063E-04    1.99872      -2.060220E-04
  a    24   6.606282E-04   3.203491E-04  -7.663470E-04  -5.048451E-04  -3.733199E-04  -5.068807E-04  -2.060220E-04    1.99833    
  a    25   2.307514E-04   3.575695E-04  -7.492830E-05  -7.737258E-05   4.282422E-04   3.844586E-04  -3.556274E-04  -2.532194E-04
  a    26   6.307598E-04  -4.646624E-04   1.046031E-03   5.488342E-04   6.566614E-04  -4.078008E-04   2.385649E-04  -3.059724E-04
  a    27   3.799994E-05  -3.480673E-05   2.075668E-04   8.975571E-05   1.510142E-04   2.632703E-05   8.911144E-05   3.705822E-05
  a    28  -2.681895E-04  -5.324847E-04   6.316457E-04   2.618847E-04   7.668405E-04   4.907300E-05   3.641315E-04   5.222500E-04
  a    29  -3.103327E-05  -5.490832E-05  -1.176281E-05  -2.017310E-05   5.223269E-05  -2.463713E-06   3.664279E-05  -2.036390E-06
  a    30   1.717997E-05  -6.936467E-06   6.528643E-05   3.193725E-05   5.467285E-05   2.092165E-05   4.997402E-05   2.609053E-05
  a    31   3.145266E-05   9.142365E-06   7.057813E-05   4.837673E-05  -1.723488E-07   1.354138E-06   1.299013E-05  -1.973122E-05
  a    32  -6.505954E-05  -7.025822E-04   2.484989E-04   1.216127E-04   6.650946E-04  -1.805537E-04   4.175449E-04  -1.227800E-04
  a    33  -1.249484E-04   5.928712E-04  -3.642492E-04  -1.058144E-04  -6.690378E-04  -2.205996E-06  -3.715145E-04  -4.606235E-05
  a    34   1.675823E-04   5.091286E-04   1.924886E-04  -5.012809E-05  -8.345890E-04  -2.202820E-04  -8.141356E-04  -1.745379E-04
  a    35   5.669962E-05   5.702221E-04  -3.293128E-04   1.307678E-04  -8.390341E-04  -4.328702E-05  -6.986162E-04  -3.433648E-04
  a    36  -7.185247E-05   1.282331E-05   4.610748E-05   3.187098E-05   2.536953E-04   5.702569E-05   2.760566E-04   1.960066E-04
  a    37  -3.945703E-04   1.665703E-03  -1.606019E-04  -9.917692E-04   5.698087E-04  -1.762655E-04   8.024182E-05   2.298380E-03
  a    38   1.605407E-03   1.432202E-04  -4.089160E-04  -4.866098E-03   1.217780E-03  -3.566573E-03  -1.053104E-03  -1.990143E-03
  a    39   8.380621E-04   2.211395E-03  -9.183430E-04  -1.120213E-03   1.170928E-03  -4.157556E-04   2.446509E-03  -2.506112E-03
  a    40   1.322063E-05  -5.343246E-04   3.973012E-03  -1.375630E-03  -8.977331E-04   1.241653E-03  -1.821711E-03   2.719771E-03
  a    41   2.115221E-04  -3.468893E-03  -1.607498E-03  -1.710499E-03   5.752152E-03   9.109645E-04   1.272018E-04   1.224162E-03
  a    42  -2.526529E-03   4.354087E-03   9.686806E-04   1.741700E-03   2.396800E-04  -6.565654E-04  -2.280243E-03   6.023772E-05
  a    43  -8.666283E-04   2.503825E-03  -7.514721E-04   4.646605E-03  -8.968137E-03   3.358727E-03  -5.887434E-03  -8.263640E-05
  a    44  -1.558472E-03   2.526822E-03  -1.510394E-03   5.549333E-04  -4.058951E-03   4.810039E-05   5.711536E-04   4.885956E-04
  a    45   1.735897E-03  -3.492822E-03   2.787154E-03   7.444082E-03   3.118999E-04  -7.801972E-04   5.063350E-04   1.403803E-04
  a    46  -1.940585E-04  -4.448019E-03   1.974243E-03  -6.765481E-06   6.818786E-03  -4.868091E-04   5.883731E-03  -2.260184E-03
  a    47  -5.970947E-04   2.413108E-03  -1.236278E-03   2.731483E-03  -1.401854E-03  -1.494588E-03  -1.060814E-03   1.722821E-03
  a    48  -8.963616E-05   2.409500E-03   8.462586E-04   1.496386E-03  -2.830801E-03   8.665763E-04  -3.230935E-03  -1.320346E-03
  a    49  -3.749510E-04  -5.788944E-04   2.344755E-03  -1.408208E-03   6.982897E-04   3.605956E-03   8.343455E-04  -2.639214E-03
  a    50  -2.153900E-03   3.518563E-03   1.487371E-04   1.569658E-03   5.580547E-05  -1.038930E-03  -8.709728E-04   4.335948E-03
  a    51   1.240412E-04   8.885757E-05  -7.799219E-05  -8.322576E-05  -3.594582E-04   2.294244E-05  -2.691510E-04  -2.894740E-04
  a    52   1.135211E-04  -1.387554E-03  -8.703113E-04  -1.135542E-03  -1.346991E-03   1.165380E-03  -8.834314E-05  -2.547060E-03
  a    53   1.387889E-03   5.875208E-04  -2.894447E-03  -9.876366E-04  -3.791604E-03  -1.445499E-03  -2.935299E-03  -2.302454E-03
  a    54  -1.577420E-05   6.979451E-05  -2.201720E-04  -3.437599E-04   6.588254E-05   4.372896E-06   5.897286E-05  -6.232461E-05
  a    55  -3.313661E-04   8.884144E-04   2.736406E-03   5.262841E-03  -2.647962E-03   6.637005E-04  -1.508077E-03   2.937477E-03
  a    56  -1.718445E-03  -2.026403E-03  -2.502655E-04  -9.093169E-04   2.617043E-03   9.628199E-04   2.846142E-03   1.448538E-03
  a    57  -1.209827E-04   1.167174E-03  -1.975637E-03  -6.379673E-05  -3.322767E-03   8.442488E-04  -6.708537E-04  -1.275355E-03
  a    58  -4.527650E-06   1.184340E-04  -8.981080E-05  -1.134218E-04  -2.310123E-05   1.197669E-04  -4.557602E-05  -8.066943E-06
  a    59  -9.996804E-05  -8.892299E-05  -1.467589E-04  -1.439216E-04   2.062461E-05   1.851773E-04   1.713698E-04   6.010190E-05
  a    60   3.124762E-04  -1.759932E-03  -2.118557E-03  -2.329645E-03  -2.206781E-03   1.322267E-03  -5.791407E-04  -3.193657E-03
  a    61   8.977928E-04  -9.062245E-04  -5.362824E-04  -4.072664E-04  -2.265969E-03  -9.808925E-04  -7.681076E-04  -2.089504E-03
  a    62   3.344371E-04  -2.047336E-04  -3.649473E-04  -2.767271E-04  -2.220600E-04   8.319101E-05  -9.790349E-05  -5.616177E-04
  a    63  -1.720335E-03   1.947617E-03   1.029579E-03   1.195042E-03   3.408693E-04  -1.021323E-03   1.714184E-04   1.877721E-03
  a    64   2.540104E-04  -8.852790E-04   1.800894E-04  -3.225618E-04   3.642139E-04  -3.305131E-04   5.346242E-05   1.966031E-04
  a    65   1.474067E-04   4.000143E-04   1.642779E-03   1.220311E-03   4.855941E-04   7.901688E-04   1.813003E-03  -1.690752E-03
  a    66  -2.662527E-04   2.331694E-04  -6.722277E-04  -2.751343E-03  -4.309034E-05  -8.759042E-04   4.358922E-04  -1.135379E-03
  a    67  -3.172247E-04  -8.368792E-04  -1.216849E-03   6.530244E-04  -1.763439E-03   6.740812E-04  -1.206425E-03  -2.322669E-03
  a    68   1.273629E-04  -3.037893E-04   1.012976E-04  -1.492264E-04   2.385109E-04  -1.886197E-04  -7.180933E-04  -5.587133E-05
  a    69  -1.183230E-04   2.118376E-04  -3.536781E-04  -2.101978E-04  -1.858388E-04  -6.594518E-05   4.659349E-04  -1.293854E-04
  a    70   6.561245E-05   3.248706E-04  -2.869135E-04  -1.866967E-04  -1.498025E-04  -2.530685E-04   4.206488E-05   8.023950E-05
  a    71   4.088232E-04  -1.785969E-04   1.032356E-04  -7.152331E-04   5.473397E-04   8.273035E-04  -4.597154E-04  -2.813385E-05
  a    72   6.818755E-04  -1.688839E-03   8.062930E-04   2.286904E-04   2.066830E-03   4.691213E-04   2.405635E-03  -3.133064E-04
  a    73   5.767573E-05   1.000045E-04  -1.412475E-04  -6.796597E-05  -2.841934E-05  -3.366661E-05  -6.741199E-05  -5.835669E-05
  a    74  -4.753588E-04  -1.340717E-03   2.327029E-03   6.540877E-04   2.089261E-03   1.322494E-03   1.486681E-03   9.462786E-04
  a    75  -7.648615E-04  -1.032281E-03   4.686323E-04  -4.009040E-04   1.744122E-03  -1.695349E-04   6.848901E-04   7.426120E-04
  a    76   2.866103E-04   1.408260E-03  -6.115430E-04  -2.586913E-04  -1.793718E-04  -8.463692E-04  -1.046040E-03  -6.411864E-04
  a    77  -5.040036E-04  -2.084291E-04   5.551010E-04   1.103150E-03   1.148418E-03   5.870613E-04   1.133970E-03   5.639741E-04
  a    78  -1.704703E-04   8.865544E-04  -1.821305E-03  -1.034370E-03  -1.141390E-04  -1.295707E-04  -9.132215E-04  -2.891576E-04
  a    79  -1.137679E-03   4.385258E-04   1.318759E-03   6.162630E-04   4.118946E-04   1.241447E-04   3.748059E-04  -1.353503E-04
  a    80  -7.680202E-04   1.128702E-03  -1.088379E-03  -1.084027E-03  -1.433122E-03  -4.125805E-04  -6.178163E-04  -6.249955E-04
  a    81  -4.548335E-04  -3.512971E-04   1.925385E-03  -5.612374E-04   2.695028E-04   9.836330E-04  -4.159276E-05   3.491627E-04
  a    82   3.743896E-04   1.027911E-03  -8.154198E-04  -5.193515E-04  -1.125208E-03  -6.550245E-04  -6.335981E-04  -9.347376E-04
  a    83  -2.394392E-04   9.474547E-04   4.279457E-04   4.910086E-04   1.177879E-03  -2.017735E-04   9.384977E-04   5.640595E-04
  a    84  -7.879786E-04   4.802187E-04   3.089972E-04  -2.753294E-04  -2.748608E-04   1.919200E-04  -6.643057E-05   3.860169E-04
  a    85   5.831070E-04  -1.966947E-04  -2.656285E-04  -1.380049E-04   2.265009E-04  -3.482129E-04   1.366349E-04  -4.550041E-04
  a    86  -5.588736E-04   7.812094E-04  -1.500350E-04  -8.737871E-04  -1.817080E-04   3.634506E-04   4.124286E-05   2.588886E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a    13  -1.744861E-04   3.882289E-05   1.770873E-05  -4.287731E-04   9.845426E-06  -8.173033E-06   8.321172E-07   1.692478E-04
  a    14   1.352309E-04   2.753873E-04  -1.198907E-05   2.899881E-04   1.219705E-05  -2.014142E-05  -9.799196E-06   1.298831E-04
  a    15   1.696072E-04   3.772307E-04  -3.240526E-06   8.057944E-04   2.191269E-05   4.818853E-06   1.120622E-05   1.328054E-04
  a    16   3.043791E-04  -5.385731E-04  -8.675412E-05   3.661652E-04  -2.663220E-06  -1.345242E-05   4.338026E-05  -9.285553E-05
  a    17   2.307514E-04   6.307598E-04   3.799994E-05  -2.681895E-04  -3.103327E-05   1.717997E-05   3.145266E-05  -6.505954E-05
  a    18   3.575695E-04  -4.646624E-04  -3.480673E-05  -5.324847E-04  -5.490832E-05  -6.936467E-06   9.142365E-06  -7.025822E-04
  a    19  -7.492830E-05   1.046031E-03   2.075668E-04   6.316457E-04  -1.176281E-05   6.528643E-05   7.057813E-05   2.484989E-04
  a    20  -7.737258E-05   5.488342E-04   8.975571E-05   2.618847E-04  -2.017310E-05   3.193725E-05   4.837673E-05   1.216127E-04
  a    21   4.282422E-04   6.566614E-04   1.510142E-04   7.668405E-04   5.223269E-05   5.467285E-05  -1.723488E-07   6.650946E-04
  a    22   3.844586E-04  -4.078008E-04   2.632703E-05   4.907300E-05  -2.463713E-06   2.092165E-05   1.354138E-06  -1.805537E-04
  a    23  -3.556274E-04   2.385649E-04   8.911144E-05   3.641315E-04   3.664279E-05   4.997402E-05   1.299013E-05   4.175449E-04
  a    24  -2.532194E-04  -3.059724E-04   3.705822E-05   5.222500E-04  -2.036390E-06   2.609053E-05  -1.973122E-05  -1.227800E-04
  a    25    1.99830       2.007714E-06   5.573672E-05  -4.282529E-04   8.624689E-06   1.203591E-05   9.938627E-06   2.787773E-04
  a    26   2.007714E-06    1.99611      -4.215697E-04  -2.250043E-04   3.332494E-05  -2.626816E-05   1.054266E-05   6.518796E-04
  a    27   5.573672E-05  -4.215697E-04    1.99928       1.698811E-04  -1.833219E-04  -1.804081E-04  -5.989671E-04  -6.913055E-03
  a    28  -4.282529E-04  -2.250043E-04   1.698811E-04    1.99680      -2.369056E-05   1.428302E-05  -1.872662E-05   3.308541E-04
  a    29   8.624689E-06   3.332494E-05  -1.833219E-04  -2.369056E-05    1.99195       1.127936E-03  -2.179563E-03  -4.578047E-03
  a    30   1.203591E-05  -2.626816E-05  -1.804081E-04   1.428302E-05   1.127936E-03    1.97376      -2.985590E-03  -1.682324E-02
  a    31   9.938627E-06   1.054266E-05  -5.989671E-04  -1.872662E-05  -2.179563E-03  -2.985590E-03    1.91310       4.077109E-02
  a    32   2.787773E-04   6.518796E-04  -6.913055E-03   3.308541E-04  -4.578047E-03  -1.682324E-02   4.077109E-02    1.02760    
  a    33  -5.670377E-05   2.044097E-04   9.378425E-04   5.923977E-05   3.780960E-04   2.236246E-03  -1.122318E-02   0.133721    
  a    34  -4.152887E-04  -8.053838E-05   1.553847E-03   5.455172E-04   1.489137E-02   6.278021E-03  -4.345563E-03  -1.396557E-02
  a    35   2.270926E-04   1.403072E-04   1.157711E-03   8.551367E-04  -1.764218E-02   3.593950E-02  -2.048157E-02   3.000550E-02
  a    36   1.990911E-05  -2.583440E-04   7.785350E-04  -4.896052E-04   5.289009E-03   3.527619E-03   1.429546E-02   9.400147E-03
  a    37  -6.306357E-04   6.324675E-03   6.740179E-04  -1.565202E-04   4.078367E-04  -4.073976E-04  -6.136288E-05   7.205064E-04
  a    38   1.450351E-04  -4.664510E-03  -1.687527E-03   4.829407E-03  -1.363942E-04  -1.236092E-04   1.562371E-04  -1.478954E-04
  a    39   4.558086E-04  -4.345146E-03   1.142288E-04   2.951531E-03   3.633945E-05   7.948179E-05   1.004368E-04  -1.607734E-04
  a    40   1.103280E-03   8.217133E-05  -5.465566E-05  -7.517543E-04  -6.395426E-04   3.809438E-04  -5.570693E-04  -9.799748E-04
  a    41   6.315265E-05   2.544718E-03   1.283217E-04   9.780682E-06   2.984433E-04  -2.779073E-04   5.086424E-04   7.778917E-04
  a    42  -5.030293E-04   2.196524E-03  -4.569089E-04   2.110993E-03  -4.100757E-04  -3.139353E-04  -1.491365E-04  -2.554190E-04
  a    43   1.815991E-03  -2.856751E-03  -9.429913E-05   6.410432E-04  -8.086949E-04   2.001065E-04  -1.138500E-03  -1.053948E-03
  a    44   1.444192E-03   4.933545E-03   2.889140E-04   1.673877E-03  -5.166442E-05  -1.986055E-04  -4.033553E-04  -9.076505E-04
  a    45   4.800173E-03  -7.015505E-03  -9.391189E-04  -4.291507E-03   6.014743E-04   3.431333E-04   9.336996E-04   6.448022E-04
  a    46  -1.275627E-03  -2.802174E-03  -7.791898E-04  -5.327065E-03  -1.025924E-04  -8.620727E-05  -2.808790E-04   1.848893E-04
  a    47  -1.177808E-03   1.369254E-03  -3.739117E-04   2.353226E-03  -4.980991E-04   2.367130E-04  -1.255091E-04   2.103634E-05
  a    48  -2.460333E-03  -1.278192E-03  -9.705894E-04   3.159207E-03   8.691601E-05  -1.214110E-04  -2.658421E-04  -3.017540E-04
  a    49  -1.946883E-03  -5.058141E-04  -6.322173E-04   1.774444E-03  -2.791873E-04   2.262067E-04  -2.270259E-04  -8.902631E-04
  a    50  -3.137515E-03  -2.374309E-03  -2.233900E-04   1.418182E-04  -2.764605E-04  -9.840642E-05  -4.063162E-04  -1.032290E-03
  a    51   3.212033E-05   7.012210E-05   5.173011E-04  -8.151282E-05   4.409014E-03  -1.742267E-03   3.950690E-03   1.111257E-02
  a    52   1.732660E-03   3.420084E-03   6.154048E-04   4.710146E-04  -3.908906E-04   3.189760E-04  -7.718274E-05  -2.827316E-04
  a    53   2.058669E-03   2.656418E-03   9.501992E-04  -1.505224E-03  -9.252254E-04   2.990313E-04  -1.095631E-03  -1.805857E-03
  a    54  -1.909402E-04   3.677892E-04  -2.180639E-04   4.006693E-05   3.888590E-03   6.441164E-03   6.708697E-04   5.389134E-04
  a    55   3.506819E-03  -6.113817E-03  -7.755175E-04  -1.089174E-03   3.838812E-04   3.315931E-04  -1.050904E-06   1.269397E-04
  a    56   2.826836E-03   1.466518E-03  -2.881322E-04  -1.264377E-03  -1.686537E-04  -2.056285E-04  -1.729837E-04  -5.678868E-05
  a    57   2.880614E-03   1.981742E-03  -2.636232E-04   2.101292E-03   3.068018E-04   1.491007E-05   2.121861E-05   1.563937E-04
  a    58  -1.111385E-04   1.431637E-04  -3.158055E-04   2.048855E-04  -1.897716E-04   1.989135E-03  -6.998207E-03  -8.301554E-04
  a    59   1.419808E-04   1.617899E-04   3.319865E-04   4.186841E-04  -4.105800E-03   6.622886E-03   3.723010E-03   6.213478E-04
  a    60   1.875789E-03   4.421070E-03   3.229471E-04   4.258468E-03   3.467056E-04  -4.742759E-04   2.911510E-04  -9.327732E-06
  a    61   6.121106E-04   6.146397E-04   4.943112E-04  -3.844752E-03  -8.489010E-06   7.422186E-05  -4.945417E-04   8.852478E-05
  a    62   8.413547E-05   1.368391E-04   9.153702E-05  -1.045251E-03  -1.288412E-03  -4.885064E-05   1.503555E-03  -1.991596E-03
  a    63  -9.993806E-04   3.210415E-04  -5.949821E-04   5.465490E-03   8.242625E-05  -1.965293E-04   1.285382E-04  -2.375151E-04
  a    64   3.204896E-04  -2.719416E-04  -5.352175E-04  -1.782973E-03  -2.015178E-04   2.689215E-04  -1.910483E-04  -3.857243E-04
  a    65   7.795074E-04  -4.360209E-04  -5.901311E-04   2.213287E-03  -8.390836E-05   1.475132E-04   7.674205E-05  -1.241778E-06
  a    66   3.249152E-04   1.523605E-03   5.175823E-04  -1.342563E-03   6.325513E-05  -9.629206E-05  -1.030358E-04  -2.232431E-05
  a    67  -6.074132E-04   1.210566E-03   4.100230E-04  -8.823579E-05  -6.029713E-05   2.465949E-04  -2.794289E-04  -1.809718E-04
  a    68   9.277872E-04   1.388464E-04   1.024975E-04   3.220060E-04  -1.274868E-03   2.269191E-03  -2.591608E-03   2.720739E-04
  a    69  -7.520845E-04   3.668407E-05   6.557369E-05  -3.184976E-04  -1.751797E-03   2.228208E-03  -2.886886E-03   1.116266E-03
  a    70  -6.959711E-05   2.196778E-04   6.704240E-04   5.753785E-04   1.089846E-04   1.168003E-03  -4.383400E-04  -1.286670E-03
  a    71  -3.536477E-04  -2.415107E-04  -4.561208E-04   2.202773E-03  -1.593146E-04  -1.016287E-04   1.125991E-04   2.545867E-04
  a    72   1.859941E-03   1.216754E-04  -4.611158E-04   3.930256E-03  -3.003339E-04   1.872569E-04   1.851920E-04   1.787834E-04
  a    73  -5.738198E-05   6.778954E-05  -2.799075E-04   1.006538E-04   1.902427E-03   8.939607E-04  -3.315588E-03  -3.749236E-03
  a    74  -8.336000E-04  -2.030639E-03  -1.657475E-04  -2.237767E-03  -2.944514E-05   2.867006E-04  -1.595952E-04  -1.126531E-04
  a    75  -9.769408E-04   5.210106E-04   1.726583E-04  -2.392959E-03   3.923486E-06  -9.129142E-05   3.384405E-05   1.273306E-05
  a    76  -1.306616E-03  -1.636008E-04   5.168992E-04  -3.747904E-03   5.566998E-05   3.615728E-05   3.626787E-05  -5.842962E-05
  a    77   8.729579E-04   7.838507E-04  -1.970666E-04   1.570973E-03   1.928149E-05  -5.289319E-06  -1.434705E-04  -2.959023E-05
  a    78  -3.033042E-04   1.313921E-03   1.557469E-04   1.253057E-03  -8.543687E-05  -8.598351E-05   1.815928E-04   1.021642E-04
  a    79   2.547099E-04  -5.227965E-04  -5.525965E-05   1.155963E-03   7.634700E-05  -8.182548E-06   3.093283E-05   9.168913E-05
  a    80  -2.636663E-04   1.053667E-03   3.134003E-05   1.164211E-03   9.576338E-05  -1.391090E-04  -1.164945E-04  -7.664794E-05
  a    81  -4.789037E-04  -9.765293E-04  -2.075571E-04  -3.110924E-04  -1.980467E-05   2.986870E-05   8.415658E-05   2.473252E-05
  a    82  -2.495548E-04   1.307272E-03   4.328406E-04  -5.336791E-04  -6.745932E-05   2.685335E-05   7.060995E-06  -1.320181E-04
  a    83  -2.724142E-04  -9.300573E-04  -3.982029E-04   3.576280E-04  -8.515936E-06  -2.407039E-05   3.185873E-05   6.194243E-05
  a    84  -2.411569E-04   1.508254E-04   6.991143E-05  -1.150786E-05  -6.525477E-05   2.324639E-05  -5.150686E-05  -5.928887E-05
  a    85   1.016910E-04  -2.756367E-04   1.082977E-04  -1.347712E-04   1.013585E-04   2.015701E-05  -2.170621E-05  -3.889075E-05
  a    86  -2.958330E-04   5.827976E-05  -1.221183E-04  -3.310372E-04   2.622205E-05  -9.983415E-05  -4.784264E-05  -4.866479E-05

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a    13   1.854223E-04  -1.586007E-04  -2.085009E-04   2.254704E-06  -1.510324E-03   7.481103E-04   3.580452E-04   1.050978E-03
  a    14  -1.200816E-04   1.290530E-04   1.049814E-06   2.889749E-05  -1.228383E-03  -9.382708E-04  -8.202456E-04   7.907901E-04
  a    15  -3.136272E-04  -4.491903E-04   1.287362E-04  -6.386403E-05   8.736434E-05  -2.963770E-03   5.382136E-04   2.096790E-03
  a    16   1.338152E-04   1.594110E-04   2.438939E-04  -9.235863E-05   3.023860E-03  -6.232296E-04  -1.361287E-03   1.429161E-03
  a    17  -1.249484E-04   1.675823E-04   5.669962E-05  -7.185247E-05  -3.945703E-04   1.605407E-03   8.380621E-04   1.322063E-05
  a    18   5.928712E-04   5.091286E-04   5.702221E-04   1.282331E-05   1.665703E-03   1.432202E-04   2.211395E-03  -5.343246E-04
  a    19  -3.642492E-04   1.924886E-04  -3.293128E-04   4.610748E-05  -1.606019E-04  -4.089160E-04  -9.183430E-04   3.973012E-03
  a    20  -1.058144E-04  -5.012809E-05   1.307678E-04   3.187098E-05  -9.917692E-04  -4.866098E-03  -1.120213E-03  -1.375630E-03
  a    21  -6.690378E-04  -8.345890E-04  -8.390341E-04   2.536953E-04   5.698087E-04   1.217780E-03   1.170928E-03  -8.977331E-04
  a    22  -2.205996E-06  -2.202820E-04  -4.328702E-05   5.702569E-05  -1.762655E-04  -3.566573E-03  -4.157556E-04   1.241653E-03
  a    23  -3.715145E-04  -8.141356E-04  -6.986162E-04   2.760566E-04   8.024182E-05  -1.053104E-03   2.446509E-03  -1.821711E-03
  a    24  -4.606235E-05  -1.745379E-04  -3.433648E-04   1.960066E-04   2.298380E-03  -1.990143E-03  -2.506112E-03   2.719771E-03
  a    25  -5.670377E-05  -4.152887E-04   2.270926E-04   1.990911E-05  -6.306357E-04   1.450351E-04   4.558086E-04   1.103280E-03
  a    26   2.044097E-04  -8.053838E-05   1.403072E-04  -2.583440E-04   6.324675E-03  -4.664510E-03  -4.345146E-03   8.217133E-05
  a    27   9.378425E-04   1.553847E-03   1.157711E-03   7.785350E-04   6.740179E-04  -1.687527E-03   1.142288E-04  -5.465566E-05
  a    28   5.923977E-05   5.455172E-04   8.551367E-04  -4.896052E-04  -1.565202E-04   4.829407E-03   2.951531E-03  -7.517543E-04
  a    29   3.780960E-04   1.489137E-02  -1.764218E-02   5.289009E-03   4.078367E-04  -1.363942E-04   3.633945E-05  -6.395426E-04
  a    30   2.236246E-03   6.278021E-03   3.593950E-02   3.527619E-03  -4.073976E-04  -1.236092E-04   7.948179E-05   3.809438E-04
  a    31  -1.122318E-02  -4.345563E-03  -2.048157E-02   1.429546E-02  -6.136288E-05   1.562371E-04   1.004368E-04  -5.570693E-04
  a    32   0.133721      -1.396557E-02   3.000550E-02   9.400147E-03   7.205064E-04  -1.478954E-04  -1.607734E-04  -9.799748E-04
  a    33    1.97792       1.031273E-03  -9.183772E-04   1.554733E-03   2.185980E-03  -9.078623E-04   1.023682E-04  -4.344319E-03
  a    34   1.031273E-03   0.983262       7.531639E-02  -1.604507E-02   8.313773E-04   2.943906E-03  -2.442624E-03  -5.042348E-04
  a    35  -9.183772E-04   7.531639E-02   9.671250E-02  -9.144539E-03   9.405608E-05   1.073832E-04  -1.152566E-04  -1.978415E-04
  a    36   1.554733E-03  -1.604507E-02  -9.144539E-03   2.897981E-02  -9.356955E-06   8.588511E-05  -1.533560E-04   7.712674E-05
  a    37   2.185980E-03   8.313773E-04   9.405608E-05  -9.356955E-06   1.603477E-03  -1.257488E-03  -6.172296E-04   5.094222E-04
  a    38  -9.078623E-04   2.943906E-03   1.073832E-04   8.588511E-05  -1.257488E-03   2.401516E-03   1.138150E-03  -1.045014E-03
  a    39   1.023682E-04  -2.442624E-03  -1.152566E-04  -1.533560E-04  -6.172296E-04   1.138150E-03   1.054390E-03  -4.317013E-04
  a    40  -4.344319E-03  -5.042348E-04  -1.978415E-04   7.712674E-05   5.094222E-04  -1.045014E-03  -4.317013E-04   1.561186E-03
  a    41   3.895896E-03   6.564133E-04  -4.849198E-05   1.637497E-05   9.011779E-05  -2.750527E-04  -2.695071E-04  -2.887605E-04
  a    42  -3.284091E-03   1.059340E-03  -2.531387E-04   8.910402E-05   2.177486E-04  -1.219664E-04  -6.030431E-05   2.085649E-04
  a    43  -7.872513E-03  -1.915679E-03  -2.359503E-04   3.614946E-06  -1.376863E-04   3.579685E-04  -5.417990E-05   4.472248E-04
  a    44  -4.885233E-03   9.121986E-05   8.616895E-05  -6.953052E-05   7.573397E-05   8.310670E-04   3.821898E-04  -6.198899E-04
  a    45   3.284074E-03   1.119032E-03   6.537804E-05   5.801006E-05   7.087647E-05  -6.135423E-04   1.306152E-04   5.027646E-04
  a    46   1.927190E-03   1.261991E-04  -5.652506E-05   4.738688E-05  -1.381380E-04  -4.849231E-04  -6.302135E-05   8.278438E-04
  a    47   3.041338E-04   4.753782E-04  -3.570660E-05   4.622710E-05   2.188218E-04  -3.726420E-04   4.482433E-05   2.089599E-04
  a    48  -2.088241E-03   6.720920E-04   5.942262E-05   3.187812E-05  -6.004321E-05   1.929275E-04   2.221111E-04  -2.307620E-04
  a    49  -2.428586E-03   8.143166E-05   5.390232E-05   5.124545E-06  -2.635584E-04   4.172425E-04   4.078071E-05  -1.626785E-05
  a    50  -5.694401E-03  -5.727783E-04  -2.898152E-05  -4.226062E-06   1.208402E-04  -4.812153E-04  -2.727981E-04   6.950880E-04
  a    51  -1.432743E-03  -3.130295E-03  -3.872518E-06   5.841797E-05   1.824780E-06   1.081319E-05   2.464597E-05  -4.285887E-05
  a    52  -4.468401E-04  -2.512066E-04  -3.963663E-05  -2.990233E-06   6.218060E-04  -6.526724E-04  -4.218501E-04   2.103600E-04
  a    53  -6.224475E-03  -5.548041E-04  -1.192647E-04  -1.350115E-05  -3.869383E-04   4.625181E-04   1.766348E-04  -1.317764E-04
  a    54   1.189277E-05   5.660217E-03   8.341296E-04   8.212342E-04   2.710768E-05   4.926086E-05  -4.188925E-05  -3.516184E-05
  a    55  -2.665193E-04   2.316045E-05   4.568629E-05   3.796853E-05  -6.584789E-05  -1.587801E-04   7.370374E-06   4.434317E-04
  a    56   9.213709E-04  -2.635407E-05   1.773531E-05  -2.309187E-05   1.842011E-04  -4.884780E-05  -1.369313E-04  -1.272364E-04
  a    57  -1.305852E-03   7.384265E-05  -5.592080E-05   3.022961E-05   1.929132E-04   1.469308E-04  -1.368573E-04  -9.388879E-05
  a    58  -1.485403E-04  -2.218835E-03  -2.943893E-03   5.992133E-04   3.421485E-06  -1.291856E-05   4.491439E-05  -2.345608E-06
  a    59   8.300805E-05  -3.542226E-03  -5.825761E-06   6.865356E-04   1.797768E-06  -2.537618E-05   5.112827E-05  -2.963379E-06
  a    60  -4.787813E-04   3.304671E-04   1.745257E-04  -9.571960E-05   2.391851E-04   2.963132E-04   2.322193E-04  -2.884858E-04
  a    61  -2.533859E-04   5.653033E-04  -2.931186E-04   3.236362E-05  -1.038083E-04   4.199400E-05  -9.626698E-05  -8.335119E-05
  a    62   1.392515E-04  -5.689262E-03   1.699900E-03  -1.877779E-05  -5.944636E-05  -7.381171E-05   8.579576E-05  -2.174988E-05
  a    63  -1.687791E-04  -6.485022E-04   2.404767E-04  -3.515058E-06   1.845150E-04   2.500944E-05  -5.127766E-05  -5.192838E-05
  a    64   1.304155E-04  -5.516387E-04   1.909045E-04   8.975681E-05  -3.997877E-05  -3.949663E-05  -2.406513E-05   8.661459E-05
  a    65   6.258222E-04   4.926089E-05   2.610136E-05   2.421115E-05   5.208833E-05   4.737892E-05   5.983670E-05  -1.068118E-05
  a    66  -9.350096E-04  -1.515100E-04  -8.030897E-06  -1.558114E-05   4.661360E-05   4.948559E-05   2.275964E-05   6.602491E-05
  a    67  -7.227329E-04  -2.102276E-04   3.421273E-05   6.145566E-05  -4.531206E-05  -5.666530E-05  -6.379991E-05  -1.526672E-04
  a    68  -1.078093E-04  -2.257399E-03   3.750282E-04   6.976927E-04   2.172659E-05   3.332815E-05   1.292442E-05   1.500278E-05
  a    69  -3.443573E-04  -2.806608E-03   3.133142E-04   7.326084E-04  -2.684366E-05  -8.647559E-06  -3.247943E-05   7.884791E-07
  a    70   7.558156E-05   2.296858E-04   2.145501E-04   2.500328E-04   1.540614E-05   5.349335E-05  -3.945950E-05  -8.475208E-06
  a    71   1.215239E-04   1.938086E-04  -6.426147E-05  -1.515732E-05  -1.045156E-05   1.657921E-05   6.586942E-05   4.134532E-05
  a    72   1.104432E-03   7.593187E-06  -4.477467E-05   1.073235E-05  -8.434775E-05   9.540380E-05   1.545558E-04   2.277980E-05
  a    73   3.696008E-04  -1.177048E-03   6.697931E-04   2.913439E-04  -3.115733E-06   1.042051E-05   2.850205E-06  -1.163387E-05
  a    74   9.044727E-04  -2.729347E-04   5.209867E-05   7.658852E-05  -6.753701E-05  -9.331724E-05  -6.672351E-05   1.583434E-04
  a    75   2.054913E-04   9.797282E-05  -5.726540E-06  -7.643968E-07  -5.533073E-05  -7.096252E-05  -1.044565E-04   5.351997E-05
  a    76  -6.065009E-04   1.594943E-04   1.583144E-06  -1.420497E-05  -9.873440E-05   7.368156E-05  -2.582354E-05  -3.506457E-05
  a    77   5.014493E-04  -8.872224E-05  -1.705281E-05   1.038214E-05   1.858608E-04  -2.087523E-04  -5.898829E-06   1.349289E-04
  a    78  -5.920044E-04   1.051753E-04  -1.283153E-05  -3.402733E-05  -2.061252E-06   6.055797E-05  -2.885976E-05  -1.163743E-04
  a    79   2.500940E-04   5.874386E-05  -4.138205E-05  -2.316964E-05   2.302745E-05  -2.476348E-05   4.014060E-05   1.985705E-05
  a    80  -4.999466E-04  -6.420239E-05   3.447859E-05   1.173240E-05   3.538660E-05   2.987547E-05   1.795199E-05  -9.859468E-05
  a    81   4.178550E-04   6.283095E-05  -2.585049E-06   2.782163E-06   3.288396E-06  -9.228530E-05  -4.568603E-05   8.663799E-05
  a    82  -1.050134E-03   6.841877E-05   1.419785E-05  -1.018257E-05   8.674042E-06   3.025130E-05   5.789732E-05  -1.387210E-05
  a    83   4.950449E-04   1.802100E-05  -9.365427E-06   1.336310E-05  -1.124664E-05   1.511010E-05   4.276944E-05   2.728081E-05
  a    84  -6.055875E-04   7.844431E-05   1.363977E-05   1.497741E-05  -2.124246E-05   6.281323E-05  -8.803763E-06  -1.320052E-05
  a    85   3.250164E-04   3.221312E-05  -5.976931E-07   9.313105E-06   4.581288E-05  -3.017730E-05   7.759141E-06   6.801846E-06
  a    86  -6.142725E-04  -1.492278E-05   9.785814E-06  -1.227258E-05   3.862862E-05   1.267145E-05  -2.470130E-05   3.214735E-05

               a    41        a    42        a    43        a    44        a    45        a    46        a    47        a    48
  a    13  -8.762932E-04   7.019377E-04   3.512824E-03   1.823946E-03  -3.098752E-04  -3.129721E-04   3.683937E-04   5.715082E-05
  a    14  -1.147600E-03   1.779422E-04  -1.389255E-03  -2.324944E-03   1.314365E-03  -1.246362E-03   7.362014E-04   9.456926E-04
  a    15  -1.883786E-04  -9.322656E-04  -1.638774E-03  -3.016940E-03   3.743294E-03   5.849625E-05   1.105626E-04   6.545746E-04
  a    16  -7.856103E-04  -1.951706E-04   3.457313E-03   1.034815E-03   1.605657E-03  -3.481549E-04  -1.242784E-04   6.993278E-04
  a    17   2.115221E-04  -2.526529E-03  -8.666283E-04  -1.558472E-03   1.735897E-03  -1.940585E-04  -5.970947E-04  -8.963616E-05
  a    18  -3.468893E-03   4.354087E-03   2.503825E-03   2.526822E-03  -3.492822E-03  -4.448019E-03   2.413108E-03   2.409500E-03
  a    19  -1.607498E-03   9.686806E-04  -7.514721E-04  -1.510394E-03   2.787154E-03   1.974243E-03  -1.236278E-03   8.462586E-04
  a    20  -1.710499E-03   1.741700E-03   4.646605E-03   5.549333E-04   7.444082E-03  -6.765481E-06   2.731483E-03   1.496386E-03
  a    21   5.752152E-03   2.396800E-04  -8.968137E-03  -4.058951E-03   3.118999E-04   6.818786E-03  -1.401854E-03  -2.830801E-03
  a    22   9.109645E-04  -6.565654E-04   3.358727E-03   4.810039E-05  -7.801972E-04  -4.868091E-04  -1.494588E-03   8.665763E-04
  a    23   1.272018E-04  -2.280243E-03  -5.887434E-03   5.711536E-04   5.063350E-04   5.883731E-03  -1.060814E-03  -3.230935E-03
  a    24   1.224162E-03   6.023772E-05  -8.263640E-05   4.885956E-04   1.403803E-04  -2.260184E-03   1.722821E-03  -1.320346E-03
  a    25   6.315265E-05  -5.030293E-04   1.815991E-03   1.444192E-03   4.800173E-03  -1.275627E-03  -1.177808E-03  -2.460333E-03
  a    26   2.544718E-03   2.196524E-03  -2.856751E-03   4.933545E-03  -7.015505E-03  -2.802174E-03   1.369254E-03  -1.278192E-03
  a    27   1.283217E-04  -4.569089E-04  -9.429913E-05   2.889140E-04  -9.391189E-04  -7.791898E-04  -3.739117E-04  -9.705894E-04
  a    28   9.780682E-06   2.110993E-03   6.410432E-04   1.673877E-03  -4.291507E-03  -5.327065E-03   2.353226E-03   3.159207E-03
  a    29   2.984433E-04  -4.100757E-04  -8.086949E-04  -5.166442E-05   6.014743E-04  -1.025924E-04  -4.980991E-04   8.691601E-05
  a    30  -2.779073E-04  -3.139353E-04   2.001065E-04  -1.986055E-04   3.431333E-04  -8.620727E-05   2.367130E-04  -1.214110E-04
  a    31   5.086424E-04  -1.491365E-04  -1.138500E-03  -4.033553E-04   9.336996E-04  -2.808790E-04  -1.255091E-04  -2.658421E-04
  a    32   7.778917E-04  -2.554190E-04  -1.053948E-03  -9.076505E-04   6.448022E-04   1.848893E-04   2.103634E-05  -3.017540E-04
  a    33   3.895896E-03  -3.284091E-03  -7.872513E-03  -4.885233E-03   3.284074E-03   1.927190E-03   3.041338E-04  -2.088241E-03
  a    34   6.564133E-04   1.059340E-03  -1.915679E-03   9.121986E-05   1.119032E-03   1.261991E-04   4.753782E-04   6.720920E-04
  a    35  -4.849198E-05  -2.531387E-04  -2.359503E-04   8.616895E-05   6.537804E-05  -5.652506E-05  -3.570660E-05   5.942262E-05
  a    36   1.637497E-05   8.910402E-05   3.614946E-06  -6.953052E-05   5.801006E-05   4.738688E-05   4.622710E-05   3.187812E-05
  a    37   9.011779E-05   2.177486E-04  -1.376863E-04   7.573397E-05   7.087647E-05  -1.381380E-04   2.188218E-04  -6.004321E-05
  a    38  -2.750527E-04  -1.219664E-04   3.579685E-04   8.310670E-04  -6.135423E-04  -4.849231E-04  -3.726420E-04   1.929275E-04
  a    39  -2.695071E-04  -6.030431E-05  -5.417990E-05   3.821898E-04   1.306152E-04  -6.302135E-05   4.482433E-05   2.221111E-04
  a    40  -2.887605E-04   2.085649E-04   4.472248E-04  -6.198899E-04   5.027646E-04   8.278438E-04   2.089599E-04  -2.307620E-04
  a    41   9.055010E-04  -3.702708E-04  -5.130467E-04  -1.885912E-05   1.880741E-04   1.994630E-04  -9.146184E-05  -1.056183E-04
  a    42  -3.702708E-04   5.703575E-04   3.992438E-04   4.199710E-04  -5.029514E-04  -1.448430E-04   2.526916E-04   1.887467E-05
  a    43  -5.130467E-04   3.992438E-04   2.369244E-03   7.358042E-04  -8.235396E-04  -2.468560E-04   7.078701E-05   1.525368E-04
  a    44  -1.885912E-05   4.199710E-04   7.358042E-04   2.833639E-03  -6.404084E-04  -1.135703E-03   2.895859E-04   1.830788E-04
  a    45   1.880741E-04  -5.029514E-04  -8.235396E-04  -6.404084E-04   2.347548E-03   7.934809E-04  -5.733766E-05  -2.360475E-04
  a    46   1.994630E-04  -1.448430E-04  -2.468560E-04  -1.135703E-03   7.934809E-04   2.332346E-03  -2.878936E-04  -5.478416E-04
  a    47  -9.146184E-05   2.526916E-04   7.078701E-05   2.895859E-04  -5.733766E-05  -2.878936E-04   9.960419E-04   2.121516E-04
  a    48  -1.056183E-04   1.887467E-05   1.525368E-04   1.830788E-04  -2.360475E-04  -5.478416E-04   2.121516E-04   6.993456E-04
  a    49   1.578976E-04  -9.957737E-05   4.303946E-04   6.438961E-05  -1.722820E-04   1.150549E-04  -3.678668E-04  -8.477975E-05
  a    50  -2.470812E-04   3.926701E-04   3.891179E-04  -1.185195E-04  -6.361950E-05   4.768062E-04   2.462687E-04  -7.009211E-05
  a    51   1.880008E-05  -1.836073E-05  -1.629888E-05   9.368458E-06   3.223022E-06  -3.654840E-05  -1.530627E-05   3.515569E-06
  a    52   1.570534E-04  -1.980063E-05   1.359976E-04  -4.899111E-06  -4.321796E-06  -3.059127E-04   1.359401E-04  -5.286051E-05
  a    53  -3.450768E-04   1.533273E-04   5.871425E-04   3.625244E-04  -8.614211E-04  -5.364171E-04   2.188182E-05   2.832091E-04
  a    54   1.557874E-05   9.376833E-06  -4.419943E-05   3.004823E-05  -3.711289E-05  -9.433356E-06   2.018775E-05   2.737748E-05
  a    55  -1.890004E-04  -4.117517E-05   3.549930E-04  -2.746702E-04   6.496598E-04   3.032561E-04   5.062255E-06   4.543743E-05
  a    56   2.966358E-04  -7.432987E-05  -1.980505E-04   3.969517E-04   1.192559E-04   1.916504E-05  -2.809731E-04  -2.675811E-04
  a    57  -9.884385E-05   1.760712E-04   7.142060E-04   3.841630E-04  -7.417600E-04  -7.212758E-04   2.150179E-05   7.207492E-05
  a    58   5.120928E-06   1.516664E-05   8.772541E-06   4.981758E-05  -1.373494E-05   4.564197E-06   3.330150E-05   7.835517E-06
  a    59   2.965994E-06   9.492276E-06   4.751750E-05   8.087405E-05  -2.178915E-05  -4.464786E-05   1.022128E-05  -2.243167E-05
  a    60  -1.888430E-05   2.111401E-04   3.191008E-04   1.163839E-03  -4.024793E-04  -4.898622E-04   3.316425E-04   1.325857E-05
  a    61  -1.034819E-04  -1.324949E-04  -1.334159E-04  -4.001509E-04  -1.602188E-05  -1.892008E-04  -2.623849E-04   1.611586E-05
  a    62  -2.937603E-05  -6.198438E-05  -4.012003E-05  -9.290828E-05  -2.676299E-05  -5.490083E-06  -3.933338E-05  -6.013912E-06
  a    63   9.279446E-05   3.249233E-05   2.727538E-04   2.364705E-04  -1.829039E-04  -4.541173E-04   1.016365E-05   8.738804E-05
  a    64  -2.567657E-05  -2.770129E-06  -4.061858E-05  -3.713278E-05   1.210712E-04   2.413009E-04  -2.005351E-05  -1.074095E-04
  a    65  -6.723816E-05  -3.643522E-06  -1.524610E-05  -1.970282E-04   7.654099E-05  -1.241529E-04  -1.566650E-04   2.189231E-05
  a    66  -1.816339E-04   8.496211E-05  -8.100597E-06   3.755865E-05  -1.685861E-04  -4.981131E-05  -7.493004E-05  -4.497829E-05
  a    67   9.883532E-05   9.698171E-06   4.353392E-05   1.524222E-04  -1.532046E-04  -1.333432E-04   1.056166E-04   6.127034E-05
  a    68  -2.137907E-05   1.550890E-05   4.110889E-05   7.131316E-05   1.236898E-05  -1.680937E-05   1.996050E-05   3.069959E-06
  a    69  -1.254149E-05  -1.470102E-05  -9.407862E-06  -5.197288E-05  -2.819479E-05  -3.417964E-06  -9.334550E-06   3.222977E-06
  a    70  -1.943910E-05   8.641292E-06   1.946768E-05   1.866747E-05  -1.544341E-05  -6.254950E-05   1.935738E-06   1.491178E-05
  a    71   4.053516E-05  -4.139956E-05  -6.132899E-05  -1.048650E-05   9.561227E-05  -6.442741E-05   8.016376E-05   5.418118E-05
  a    72   1.342164E-04  -8.359885E-05  -7.888513E-05   1.255325E-04   2.306817E-04   5.193382E-05   6.840326E-05  -3.923591E-05
  a    73  -3.144055E-06  -2.013108E-05  -8.223767E-06   1.096312E-05  -1.016486E-05  -2.009093E-05   1.630570E-05   2.148345E-05
  a    74   5.446759E-05  -4.102688E-05  -1.368157E-04  -2.584526E-04   2.149335E-04   4.268397E-04  -1.065874E-04  -1.176152E-04
  a    75   3.829907E-05  -2.644455E-05  -8.044202E-05  -2.037634E-04  -3.035917E-05   2.529482E-04  -1.343889E-04  -6.732900E-05
  a    76  -1.029597E-04   5.155291E-05  -7.044553E-06  -4.926765E-05  -1.797808E-04   6.293583E-05  -4.174132E-05  -2.099416E-05
  a    77   4.607318E-05  -1.868288E-05  -1.067955E-04  -8.323108E-05   2.139687E-04   7.593454E-05   7.501887E-05   2.882553E-05
  a    78   2.179393E-05  -4.460921E-06   8.734044E-05   2.089065E-05  -2.212562E-04  -2.535811E-04  -3.104933E-05   6.860246E-05
  a    79   2.202624E-05   3.480254E-05  -8.007976E-06   1.143386E-04   1.039952E-04   9.199995E-05   5.384387E-05  -1.655366E-05
  a    80  -1.474099E-06   2.307133E-05   6.168206E-05   1.563702E-04  -1.322072E-04  -1.359611E-04   5.009858E-05   6.543177E-05
  a    81   3.212401E-05  -3.915693E-05  -3.971112E-05  -1.796219E-04   1.387384E-04   1.707126E-04  -2.747581E-05  -1.455814E-05
  a    82  -9.029642E-05   6.186747E-05   6.099988E-05   1.306596E-04  -8.310059E-05  -6.484983E-05   8.450553E-05   3.821386E-05
  a    83  -7.518365E-06   5.255336E-06  -2.130968E-05   4.286722E-05   1.230778E-04   8.394650E-05   3.761127E-05  -3.778559E-05
  a    84  -1.693879E-05   3.609194E-05   6.411421E-05   9.981278E-05  -1.118984E-04  -4.085664E-05  -1.127077E-06   3.209742E-05
  a    85   3.700155E-06  -1.059843E-05  -3.671794E-05  -4.934923E-05   4.665942E-05   4.662220E-05   1.912636E-05  -2.035575E-07
  a    86  -2.026346E-05   4.280214E-05   8.455853E-05   8.328724E-05  -7.528354E-05   5.552629E-05  -2.529958E-05  -9.502174E-06

               a    49        a    50        a    51        a    52        a    53        a    54        a    55        a    56
  a    13   5.397263E-04   6.430971E-04  -7.706725E-05  -5.174742E-04  -2.200411E-04  -5.824761E-05   1.392099E-03   1.107459E-03
  a    14  -1.922850E-03   3.124404E-04  -1.079518E-04  -9.962024E-04  -1.391980E-04  -7.020835E-05   8.659799E-04  -1.215563E-03
  a    15  -1.621084E-03  -3.111625E-04  -7.191576E-05  -1.528514E-04  -5.865740E-04  -1.329529E-04   1.482721E-03  -1.815398E-03
  a    16   1.042210E-03   3.571412E-05   7.775258E-05   1.332700E-03   7.631697E-05  -1.135523E-04   2.065930E-03   4.716140E-04
  a    17  -3.749510E-04  -2.153900E-03   1.240412E-04   1.135211E-04   1.387889E-03  -1.577420E-05  -3.313661E-04  -1.718445E-03
  a    18  -5.788944E-04   3.518563E-03   8.885757E-05  -1.387554E-03   5.875208E-04   6.979451E-05   8.884144E-04  -2.026403E-03
  a    19   2.344755E-03   1.487371E-04  -7.799219E-05  -8.703113E-04  -2.894447E-03  -2.201720E-04   2.736406E-03  -2.502655E-04
  a    20  -1.408208E-03   1.569658E-03  -8.322576E-05  -1.135542E-03  -9.876366E-04  -3.437599E-04   5.262841E-03  -9.093169E-04
  a    21   6.982897E-04   5.580547E-05  -3.594582E-04  -1.346991E-03  -3.791604E-03   6.588254E-05  -2.647962E-03   2.617043E-03
  a    22   3.605956E-03  -1.038930E-03   2.294244E-05   1.165380E-03  -1.445499E-03   4.372896E-06   6.637005E-04   9.628199E-04
  a    23   8.343455E-04  -8.709728E-04  -2.691510E-04  -8.834314E-05  -2.935299E-03   5.897286E-05  -1.508077E-03   2.846142E-03
  a    24  -2.639214E-03   4.335948E-03  -2.894740E-04  -2.547060E-03  -2.302454E-03  -6.232461E-05   2.937477E-03   1.448538E-03
  a    25  -1.946883E-03  -3.137515E-03   3.212033E-05   1.732660E-03   2.058669E-03  -1.909402E-04   3.506819E-03   2.826836E-03
  a    26  -5.058141E-04  -2.374309E-03   7.012210E-05   3.420084E-03   2.656418E-03   3.677892E-04  -6.113817E-03   1.466518E-03
  a    27  -6.322173E-04  -2.233900E-04   5.173011E-04   6.154048E-04   9.501992E-04  -2.180639E-04  -7.755175E-04  -2.881322E-04
  a    28   1.774444E-03   1.418182E-04  -8.151282E-05   4.710146E-04  -1.505224E-03   4.006693E-05  -1.089174E-03  -1.264377E-03
  a    29  -2.791873E-04  -2.764605E-04   4.409014E-03  -3.908906E-04  -9.252254E-04   3.888590E-03   3.838812E-04  -1.686537E-04
  a    30   2.262067E-04  -9.840642E-05  -1.742267E-03   3.189760E-04   2.990313E-04   6.441164E-03   3.315931E-04  -2.056285E-04
  a    31  -2.270259E-04  -4.063162E-04   3.950690E-03  -7.718274E-05  -1.095631E-03   6.708697E-04  -1.050904E-06  -1.729837E-04
  a    32  -8.902631E-04  -1.032290E-03   1.111257E-02  -2.827316E-04  -1.805857E-03   5.389134E-04   1.269397E-04  -5.678868E-05
  a    33  -2.428586E-03  -5.694401E-03  -1.432743E-03  -4.468401E-04  -6.224475E-03   1.189277E-05  -2.665193E-04   9.213709E-04
  a    34   8.143166E-05  -5.727783E-04  -3.130295E-03  -2.512066E-04  -5.548041E-04   5.660217E-03   2.316045E-05  -2.635407E-05
  a    35   5.390232E-05  -2.898152E-05  -3.872518E-06  -3.963663E-05  -1.192647E-04   8.341296E-04   4.568629E-05   1.773531E-05
  a    36   5.124545E-06  -4.226062E-06   5.841797E-05  -2.990233E-06  -1.350115E-05   8.212342E-04   3.796853E-05  -2.309187E-05
  a    37  -2.635584E-04   1.208402E-04   1.824780E-06   6.218060E-04  -3.869383E-04   2.710768E-05  -6.584789E-05   1.842011E-04
  a    38   4.172425E-04  -4.812153E-04   1.081319E-05  -6.526724E-04   4.625181E-04   4.926086E-05  -1.587801E-04  -4.884780E-05
  a    39   4.078071E-05  -2.727981E-04   2.464597E-05  -4.218501E-04   1.766348E-04  -4.188925E-05   7.370374E-06  -1.369313E-04
  a    40  -1.626785E-05   6.950880E-04  -4.285887E-05   2.103600E-04  -1.317764E-04  -3.516184E-05   4.434317E-04  -1.272364E-04
  a    41   1.578976E-04  -2.470812E-04   1.880008E-05   1.570534E-04  -3.450768E-04   1.557874E-05  -1.890004E-04   2.966358E-04
  a    42  -9.957737E-05   3.926701E-04  -1.836073E-05  -1.980063E-05   1.533273E-04   9.376833E-06  -4.117517E-05  -7.432987E-05
  a    43   4.303946E-04   3.891179E-04  -1.629888E-05   1.359976E-04   5.871425E-04  -4.419943E-05   3.549930E-04  -1.980505E-04
  a    44   6.438961E-05  -1.185195E-04   9.368458E-06  -4.899111E-06   3.625244E-04   3.004823E-05  -2.746702E-04   3.969517E-04
  a    45  -1.722820E-04  -6.361950E-05   3.223022E-06  -4.321796E-06  -8.614211E-04  -3.711289E-05   6.496598E-04   1.192559E-04
  a    46   1.150549E-04   4.768062E-04  -3.654840E-05  -3.059127E-04  -5.364171E-04  -9.433356E-06   3.032561E-04   1.916504E-05
  a    47  -3.678668E-04   2.462687E-04  -1.530627E-05   1.359401E-04   2.188182E-05   2.018775E-05   5.062255E-06  -2.809731E-04
  a    48  -8.477975E-05  -7.009211E-05   3.515569E-06  -5.286051E-05   2.832091E-04   2.737748E-05   4.543743E-05  -2.675811E-04
  a    49   8.016298E-04  -4.886501E-05  -6.506489E-06   6.407297E-05   7.414515E-05   6.763807E-06   4.345039E-05   1.942878E-04
  a    50  -4.886501E-05   9.573953E-04  -2.516083E-05  -1.574817E-06   1.730152E-05  -2.208661E-05   1.919850E-04  -1.232059E-04
  a    51  -6.506489E-06  -2.516083E-05   7.013886E-04  -1.653596E-06  -8.338093E-06   1.535917E-04  -3.537523E-07   5.094629E-06
  a    52   6.407297E-05  -1.574817E-06  -1.653596E-06   8.037825E-04  -3.987933E-05   6.047844E-06  -1.035933E-04   1.565259E-04
  a    53   7.414515E-05   1.730152E-05  -8.338093E-06  -3.987933E-05   1.050093E-03   8.224524E-06  -2.372765E-04  -1.634696E-04
  a    54   6.763807E-06  -2.208661E-05   1.535917E-04   6.047844E-06   8.224524E-06   1.083199E-03   7.601294E-06   1.687419E-05
  a    55   4.345039E-05   1.919850E-04  -3.537523E-07  -1.035933E-04  -2.372765E-04   7.601294E-06   7.486492E-04  -1.429094E-04
  a    56   1.942878E-04  -1.232059E-04   5.094629E-06   1.565259E-04  -1.634696E-04   1.687419E-05  -1.429094E-04   7.342535E-04
  a    57   2.180911E-04  -7.915373E-05   8.998829E-06   2.947150E-04   2.313157E-04   1.846356E-05  -1.960733E-04   1.463551E-05
  a    58  -1.347230E-05   1.266579E-05   1.319787E-04  -3.184141E-06   6.643975E-06   2.188595E-04  -7.185906E-07  -6.931094E-06
  a    59   1.639245E-07   1.476431E-06  -1.775926E-04   1.707284E-05   1.002472E-06  -3.959700E-04  -2.967640E-05   5.080706E-06
  a    60  -1.065139E-04  -1.102362E-04   1.180942E-05   1.462629E-04   1.300744E-04   3.284694E-05  -3.028683E-04   2.145012E-06
  a    61  -4.278515E-05  -1.619058E-04   2.514539E-05  -1.061680E-05   1.982221E-04   2.683291E-05   4.590179E-05  -6.887051E-06
  a    62  -3.231031E-05  -1.854714E-05  -1.107438E-04  -2.123010E-05   5.035129E-05  -2.038923E-04  -2.117869E-05  -3.425105E-05
  a    63   2.138538E-04  -3.635045E-05  -7.803731E-06   1.929462E-04  -4.701177E-05  -7.367118E-06   4.138240E-05   1.659020E-04
  a    64  -6.163032E-05   6.850463E-05  -2.339127E-05  -5.216378E-05  -5.650129E-05  -4.020434E-06   1.625051E-05  -2.850484E-05
  a    65   2.256215E-04  -1.252462E-04   4.740588E-06   8.505508E-05  -5.823774E-05  -2.325470E-06   7.463789E-05   7.106938E-05
  a    66  -3.829113E-05  -1.217239E-05   4.203774E-07   2.404137E-05   1.113953E-04   6.886625E-06  -9.642379E-05   5.565282E-05
  a    67  -4.156125E-05  -2.961286E-05  -3.433016E-06   9.334774E-05   9.371636E-05   3.194869E-06  -6.125726E-05  -1.263266E-05
  a    68  -4.788492E-06  -1.187678E-05  -9.609976E-05   7.394822E-06   1.511655E-05  -1.045886E-04  -7.978154E-06   2.728598E-06
  a    69   6.958011E-06   5.660095E-06  -1.174105E-04  -2.714259E-06   8.762082E-06  -1.813919E-04  -1.479496E-05  -1.171561E-05
  a    70   4.626218E-06  -2.670583E-05  -6.070059E-06   1.545166E-05   2.326712E-05   6.267646E-05  -7.621188E-06  -4.560706E-06
  a    71   3.058660E-07  -4.065528E-05   4.958030E-06   1.687218E-05  -2.318128E-05  -2.734428E-05   2.779192E-05  -3.128845E-05
  a    72   1.101609E-04  -1.191084E-04   6.139936E-06   4.741925E-05  -1.102893E-04  -9.536666E-06  -8.116398E-07  -4.212438E-06
  a    73   1.073954E-05  -1.081110E-05  -1.711586E-05   3.328837E-06   4.968403E-06   3.183800E-04   1.265708E-05  -2.049763E-06
  a    74   5.131348E-05   1.320870E-04  -1.634012E-05  -1.076476E-04  -1.893685E-04  -2.620503E-07   1.112613E-04   5.135598E-05
  a    75   3.966664E-05   5.660317E-05  -3.329409E-06  -6.423846E-05  -2.618059E-05   1.162627E-06   7.892354E-06   4.326288E-05
  a    76  -8.207622E-05   7.957971E-05  -3.045191E-06  -1.355684E-04   1.066245E-04   4.367808E-06  -3.526539E-05  -4.234482E-05
  a    77  -4.094433E-05  -2.761769E-06  -4.264082E-07   1.133952E-04  -9.578602E-05  -2.222461E-06   3.496985E-05  -8.689377E-06
  a    78   6.745405E-05  -7.393672E-05   6.177039E-06   7.194131E-05   1.006810E-04  -4.658717E-06  -5.917837E-05   5.088561E-06
  a    79  -5.710139E-06   3.041709E-05   2.974775E-06  -2.466986E-05  -8.587578E-05  -8.603144E-07   2.218327E-05   8.915432E-06
  a    80  -2.849275E-05  -5.319030E-05   3.203062E-06   1.156773E-05   8.923670E-05   3.249820E-06  -5.857292E-05  -2.464490E-05
  a    81   2.165694E-05   5.715477E-05  -5.165059E-06   3.657624E-06  -9.956137E-05  -5.665933E-06   6.937955E-05  -1.916472E-05
  a    82  -8.263483E-05   2.043362E-05  -1.646360E-06  -1.790518E-06   1.144010E-04   1.506486E-07  -4.137516E-05  -7.153943E-05
  a    83  -1.974427E-05   3.677516E-05  -3.210698E-06  -1.723996E-05  -8.686479E-05  -3.786200E-06   4.308734E-05   1.074154E-05
  a    84   4.582098E-05   2.556846E-05  -2.080665E-06  -7.771200E-06   2.021578E-05   2.027999E-06  -1.832721E-05   4.283834E-06
  a    85  -2.422835E-05   4.325193E-06  -7.339736E-07   6.394271E-06  -2.392752E-05   1.639343E-06   9.727299E-06   1.624863E-06
  a    86   4.258896E-05   5.575190E-05  -2.964055E-06  -5.989093E-06  -1.505049E-05   4.615681E-07   7.153754E-06   1.587299E-05

               a    57        a    58        a    59        a    60        a    61        a    62        a    63        a    64
  a    13   1.558541E-03  -3.627925E-05   1.403347E-04   8.345363E-04  -4.147706E-04  -2.726726E-04   1.301727E-03  -2.869620E-04
  a    14  -8.342586E-04  -5.813377E-05  -9.317153E-05  -1.410267E-03   7.224398E-04   2.538023E-04  -5.518093E-04  -6.457763E-05
  a    15  -2.224937E-03  -5.108664E-05  -1.138950E-04  -2.029994E-03   1.083517E-03   2.470026E-04  -2.050924E-04   1.616057E-05
  a    16   9.619029E-04  -2.388373E-05  -4.184592E-05  -2.936607E-04  -4.831626E-04  -2.271791E-04   2.952155E-04   1.333281E-04
  a    17  -1.209827E-04  -4.527650E-06  -9.996804E-05   3.124762E-04   8.977928E-04   3.344371E-04  -1.720335E-03   2.540104E-04
  a    18   1.167174E-03   1.184340E-04  -8.892299E-05  -1.759932E-03  -9.062245E-04  -2.047336E-04   1.947617E-03  -8.852790E-04
  a    19  -1.975637E-03  -8.981080E-05  -1.467589E-04  -2.118557E-03  -5.362824E-04  -3.649473E-04   1.029579E-03   1.800894E-04
  a    20  -6.379673E-05  -1.134218E-04  -1.439216E-04  -2.329645E-03  -4.072664E-04  -2.767271E-04   1.195042E-03  -3.225618E-04
  a    21  -3.322767E-03  -2.310123E-05   2.062461E-05  -2.206781E-03  -2.265969E-03  -2.220600E-04   3.408693E-04   3.642139E-04
  a    22   8.442488E-04   1.197669E-04   1.851773E-04   1.322267E-03  -9.808925E-04   8.319101E-05  -1.021323E-03  -3.305131E-04
  a    23  -6.708537E-04  -4.557602E-05   1.713698E-04  -5.791407E-04  -7.681076E-04  -9.790349E-05   1.714184E-04   5.346242E-05
  a    24  -1.275355E-03  -8.066943E-06   6.010190E-05  -3.193657E-03  -2.089504E-03  -5.616177E-04   1.877721E-03   1.966031E-04
  a    25   2.880614E-03  -1.111385E-04   1.419808E-04   1.875789E-03   6.121106E-04   8.413547E-05  -9.993806E-04   3.204896E-04
  a    26   1.981742E-03   1.431637E-04   1.617899E-04   4.421070E-03   6.146397E-04   1.368391E-04   3.210415E-04  -2.719416E-04
  a    27  -2.636232E-04  -3.158055E-04   3.319865E-04   3.229471E-04   4.943112E-04   9.153702E-05  -5.949821E-04  -5.352175E-04
  a    28   2.101292E-03   2.048855E-04   4.186841E-04   4.258468E-03  -3.844752E-03  -1.045251E-03   5.465490E-03  -1.782973E-03
  a    29   3.068018E-04  -1.897716E-04  -4.105800E-03   3.467056E-04  -8.489010E-06  -1.288412E-03   8.242625E-05  -2.015178E-04
  a    30   1.491007E-05   1.989135E-03   6.622886E-03  -4.742759E-04   7.422186E-05  -4.885064E-05  -1.965293E-04   2.689215E-04
  a    31   2.121861E-05  -6.998207E-03   3.723010E-03   2.911510E-04  -4.945417E-04   1.503555E-03   1.285382E-04  -1.910483E-04
  a    32   1.563937E-04  -8.301554E-04   6.213478E-04  -9.327732E-06   8.852478E-05  -1.991596E-03  -2.375151E-04  -3.857243E-04
  a    33  -1.305852E-03  -1.485403E-04   8.300805E-05  -4.787813E-04  -2.533859E-04   1.392515E-04  -1.687791E-04   1.304155E-04
  a    34   7.384265E-05  -2.218835E-03  -3.542226E-03   3.304671E-04   5.653033E-04  -5.689262E-03  -6.485022E-04  -5.516387E-04
  a    35  -5.592080E-05  -2.943893E-03  -5.825761E-06   1.745257E-04  -2.931186E-04   1.699900E-03   2.404767E-04   1.909045E-04
  a    36   3.022961E-05   5.992133E-04   6.865356E-04  -9.571960E-05   3.236362E-05  -1.877779E-05  -3.515058E-06   8.975681E-05
  a    37   1.929132E-04   3.421485E-06   1.797768E-06   2.391851E-04  -1.038083E-04  -5.944636E-05   1.845150E-04  -3.997877E-05
  a    38   1.469308E-04  -1.291856E-05  -2.537618E-05   2.963132E-04   4.199400E-05  -7.381171E-05   2.500944E-05  -3.949663E-05
  a    39  -1.368573E-04   4.491439E-05   5.112827E-05   2.322193E-04  -9.626698E-05   8.579576E-05  -5.127766E-05  -2.406513E-05
  a    40  -9.388879E-05  -2.345608E-06  -2.963379E-06  -2.884858E-04  -8.335119E-05  -2.174988E-05  -5.192838E-05   8.661459E-05
  a    41  -9.884385E-05   5.120928E-06   2.965994E-06  -1.888430E-05  -1.034819E-04  -2.937603E-05   9.279446E-05  -2.567657E-05
  a    42   1.760712E-04   1.516664E-05   9.492276E-06   2.111401E-04  -1.324949E-04  -6.198438E-05   3.249233E-05  -2.770129E-06
  a    43   7.142060E-04   8.772541E-06   4.751750E-05   3.191008E-04  -1.334159E-04  -4.012003E-05   2.727538E-04  -4.061858E-05
  a    44   3.841630E-04   4.981758E-05   8.087405E-05   1.163839E-03  -4.001509E-04  -9.290828E-05   2.364705E-04  -3.713278E-05
  a    45  -7.417600E-04  -1.373494E-05  -2.178915E-05  -4.024793E-04  -1.602188E-05  -2.676299E-05  -1.829039E-04   1.210712E-04
  a    46  -7.212758E-04   4.564197E-06  -4.464786E-05  -4.898622E-04  -1.892008E-04  -5.490083E-06  -4.541173E-04   2.413009E-04
  a    47   2.150179E-05   3.330150E-05   1.022128E-05   3.316425E-04  -2.623849E-04  -3.933338E-05   1.016365E-05  -2.005351E-05
  a    48   7.207492E-05   7.835517E-06  -2.243167E-05   1.325857E-05   1.611586E-05  -6.013912E-06   8.738804E-05  -1.074095E-04
  a    49   2.180911E-04  -1.347230E-05   1.639245E-07  -1.065139E-04  -4.278515E-05  -3.231031E-05   2.138538E-04  -6.163032E-05
  a    50  -7.915373E-05   1.266579E-05   1.476431E-06  -1.102362E-04  -1.619058E-04  -1.854714E-05  -3.635045E-05   6.850463E-05
  a    51   8.998829E-06   1.319787E-04  -1.775926E-04   1.180942E-05   2.514539E-05  -1.107438E-04  -7.803731E-06  -2.339127E-05
  a    52   2.947150E-04  -3.184141E-06   1.707284E-05   1.462629E-04  -1.061680E-05  -2.123010E-05   1.929462E-04  -5.216378E-05
  a    53   2.313157E-04   6.643975E-06   1.002472E-06   1.300744E-04   1.982221E-04   5.035129E-05  -4.701177E-05  -5.650129E-05
  a    54   1.846356E-05   2.188595E-04  -3.959700E-04   3.284694E-05   2.683291E-05  -2.038923E-04  -7.367118E-06  -4.020434E-06
  a    55  -1.960733E-04  -7.185906E-07  -2.967640E-05  -3.028683E-04   4.590179E-05  -2.117869E-05   4.138240E-05   1.625051E-05
  a    56   1.463551E-05  -6.931094E-06   5.080706E-06   2.145012E-06  -6.887051E-06  -3.425105E-05   1.659020E-04  -2.850484E-05
  a    57   8.279318E-04   4.219651E-06   3.009325E-05   3.613375E-04  -5.213644E-05  -4.510940E-05   3.277674E-04  -1.199262E-04
  a    58   4.219651E-06   5.608300E-04  -3.981367E-05   1.600043E-05   9.558909E-06  -1.203239E-04  -1.156742E-05  -5.364454E-07
  a    59   3.009325E-05  -3.981367E-05   6.863324E-04   2.902009E-05  -5.956470E-05   2.943218E-04   4.462492E-05   2.767317E-05
  a    60   3.613375E-04   1.600043E-05   2.902009E-05   9.896691E-04  -2.411617E-04  -5.464234E-05   7.143298E-05  -1.632640E-05
  a    61  -5.213644E-05   9.558909E-06  -5.956470E-05  -2.411617E-04   4.166849E-04   1.241331E-05  -1.030671E-04  -1.355379E-05
  a    62  -4.510940E-05  -1.203239E-04   2.943218E-04  -5.464234E-05   1.241331E-05   4.528619E-04  -9.098780E-07   4.271001E-05
  a    63   3.277674E-04  -1.156742E-05   4.462492E-05   7.143298E-05  -1.030671E-04  -9.098780E-07   4.216139E-04  -1.222366E-04
  a    64  -1.199262E-04  -5.364454E-07   2.767317E-05  -1.632640E-05  -1.355379E-05   4.271001E-05  -1.222366E-04   7.421888E-05
  a    65   1.266535E-04  -1.217454E-05  -2.906421E-07  -1.228919E-04   2.433545E-05  -1.352216E-05   1.713390E-04  -7.432581E-05
  a    66   5.449193E-05   3.838750E-07   1.120574E-06   2.961040E-05   7.784876E-05   1.943839E-05  -3.973353E-05   1.148160E-05
  a    67   4.282398E-05   5.696061E-06   1.891188E-05   1.273509E-04  -7.638987E-06  -2.548743E-06   2.017679E-05  -1.265811E-05
  a    68   2.243830E-05   4.990702E-05   2.431899E-04   3.853032E-05  -2.548906E-05   2.362506E-05   7.861795E-06   2.077699E-05
  a    69   2.038531E-06   5.846164E-05   3.173860E-04  -6.204372E-05   1.928553E-05   4.784666E-05  -4.296813E-06   2.236520E-05
  a    70   3.140948E-05  -3.317274E-05  -3.267672E-05   1.693962E-05   1.347426E-05  -5.246012E-05   6.581033E-06   1.053910E-06
  a    71  -3.057689E-05  -6.239053E-06   4.071125E-06   1.583020E-05  -2.626609E-05  -1.833232E-05   3.938902E-05  -2.415724E-05
  a    72  -3.961459E-06   8.923250E-06   1.795272E-05   1.412635E-04  -1.302536E-04  -2.236336E-05   3.762349E-05  -5.786460E-06
  a    73   8.682104E-06   8.655312E-05  -1.330438E-04   1.175472E-05  -2.274423E-06   3.373692E-05   2.088054E-05   1.633582E-05
  a    74  -1.994286E-04  -2.235779E-06   2.096850E-06  -2.009293E-04  -1.940929E-06   6.745105E-06  -7.812032E-05   5.095482E-05
  a    75  -1.009274E-04  -5.607067E-06  -1.439675E-05  -1.221992E-04   8.676801E-05   1.613864E-05  -8.729113E-05   3.179559E-05
  a    76  -4.282049E-05  -4.470323E-06  -1.256540E-05  -7.364591E-05   6.446259E-05   1.578456E-05  -1.029419E-04   2.672555E-05
  a    77  -6.533768E-05   3.103570E-06  -3.129313E-06  -6.450347E-06  -4.675110E-05  -1.087781E-05   1.646403E-05   5.628729E-06
  a    78   1.510019E-04  -9.477063E-06   2.151724E-06   4.727738E-05   6.803265E-05  -7.606720E-07   8.660297E-05  -5.000542E-05
  a    79  -5.286801E-05   8.928056E-06   2.743549E-07   3.471173E-05  -8.076676E-05  -1.349945E-05  -9.461653E-06   1.590981E-05
  a    80   5.844521E-05   1.526654E-06   4.050957E-06   1.154467E-04  -2.853814E-07   5.378067E-07   1.073742E-05  -1.165650E-05
  a    81  -7.904719E-05  -3.395355E-06  -6.367003E-06  -1.019052E-04  -3.664839E-06  -5.342698E-07  -2.687509E-05   1.862069E-05
  a    82   2.104545E-05   3.954554E-06   4.887089E-06   1.046909E-04  -1.311325E-05   4.473251E-06  -3.529691E-05   7.793376E-06
  a    83  -5.024292E-05  -7.786583E-09   7.557884E-07   2.217542E-05  -2.949124E-05  -7.979498E-06  -2.118140E-05   1.804996E-05
  a    84   4.800177E-05  -8.346385E-07   2.875256E-06   6.367891E-06  -1.771351E-05  -4.821698E-06   1.001152E-05  -1.609547E-06
  a    85  -2.705947E-05   1.158534E-06  -5.700803E-07   2.103531E-06   7.911053E-07  -1.583454E-06  -3.737061E-06  -3.600309E-07
  a    86   1.431986E-05   7.315646E-07   8.725272E-08  -1.986426E-06  -1.942115E-05  -7.655961E-07  -1.125048E-05   1.220970E-05

               a    65        a    66        a    67        a    68        a    69        a    70        a    71        a    72
  a    13   1.718502E-04  -8.143881E-05   3.187533E-04   2.264097E-04   3.146679E-05  -8.507790E-05  -3.879041E-04  -2.894200E-04
  a    14  -5.536850E-05  -3.520721E-04   2.102759E-04  -2.660703E-04   3.245054E-04   2.572448E-04   8.423858E-04  -8.497893E-04
  a    15  -4.563312E-05  -2.992187E-04   7.913335E-05  -1.595755E-05  -1.232618E-04  -1.748911E-04   1.175127E-04  -5.926114E-04
  a    16   3.260489E-04  -1.735941E-04  -2.863023E-04   1.704651E-04  -3.358355E-04  -4.642666E-05   8.724554E-06  -7.232603E-04
  a    17   1.474067E-04  -2.662527E-04  -3.172247E-04   1.273629E-04  -1.183230E-04   6.561245E-05   4.088232E-04   6.818755E-04
  a    18   4.000143E-04   2.331694E-04  -8.368792E-04  -3.037893E-04   2.118376E-04   3.248706E-04  -1.785969E-04  -1.688839E-03
  a    19   1.642779E-03  -6.722277E-04  -1.216849E-03   1.012976E-04  -3.536781E-04  -2.869135E-04   1.032356E-04   8.062930E-04
  a    20   1.220311E-03  -2.751343E-03   6.530244E-04  -1.492264E-04  -2.101978E-04  -1.866967E-04  -7.152331E-04   2.286904E-04
  a    21   4.855941E-04  -4.309034E-05  -1.763439E-03   2.385109E-04  -1.858388E-04  -1.498025E-04   5.473397E-04   2.066830E-03
  a    22   7.901688E-04  -8.759042E-04   6.740812E-04  -1.886197E-04  -6.594518E-05  -2.530685E-04   8.273035E-04   4.691213E-04
  a    23   1.813003E-03   4.358922E-04  -1.206425E-03  -7.180933E-04   4.659349E-04   4.206488E-05  -4.597154E-04   2.405635E-03
  a    24  -1.690752E-03  -1.135379E-03  -2.322669E-03  -5.587133E-05  -1.293854E-04   8.023950E-05  -2.813385E-05  -3.133064E-04
  a    25   7.795074E-04   3.249152E-04  -6.074132E-04   9.277872E-04  -7.520845E-04  -6.959711E-05  -3.536477E-04   1.859941E-03
  a    26  -4.360209E-04   1.523605E-03   1.210566E-03   1.388464E-04   3.668407E-05   2.196778E-04  -2.415107E-04   1.216754E-04
  a    27  -5.901311E-04   5.175823E-04   4.100230E-04   1.024975E-04   6.557369E-05   6.704240E-04  -4.561208E-04  -4.611158E-04
  a    28   2.213287E-03  -1.342563E-03  -8.823579E-05   3.220060E-04  -3.184976E-04   5.753785E-04   2.202773E-03   3.930256E-03
  a    29  -8.390836E-05   6.325513E-05  -6.029713E-05  -1.274868E-03  -1.751797E-03   1.089846E-04  -1.593146E-04  -3.003339E-04
  a    30   1.475132E-04  -9.629206E-05   2.465949E-04   2.269191E-03   2.228208E-03   1.168003E-03  -1.016287E-04   1.872569E-04
  a    31   7.674205E-05  -1.030358E-04  -2.794289E-04  -2.591608E-03  -2.886886E-03  -4.383400E-04   1.125991E-04   1.851920E-04
  a    32  -1.241778E-06  -2.232431E-05  -1.809718E-04   2.720739E-04   1.116266E-03  -1.286670E-03   2.545867E-04   1.787834E-04
  a    33   6.258222E-04  -9.350096E-04  -7.227329E-04  -1.078093E-04  -3.443573E-04   7.558156E-05   1.215239E-04   1.104432E-03
  a    34   4.926089E-05  -1.515100E-04  -2.102276E-04  -2.257399E-03  -2.806608E-03   2.296858E-04   1.938086E-04   7.593187E-06
  a    35   2.610136E-05  -8.030897E-06   3.421273E-05   3.750282E-04   3.133142E-04   2.145501E-04  -6.426147E-05  -4.477467E-05
  a    36   2.421115E-05  -1.558114E-05   6.145566E-05   6.976927E-04   7.326084E-04   2.500328E-04  -1.515732E-05   1.073235E-05
  a    37   5.208833E-05   4.661360E-05  -4.531206E-05   2.172659E-05  -2.684366E-05   1.540614E-05  -1.045156E-05  -8.434775E-05
  a    38   4.737892E-05   4.948559E-05  -5.666530E-05   3.332815E-05  -8.647559E-06   5.349335E-05   1.657921E-05   9.540380E-05
  a    39   5.983670E-05   2.275964E-05  -6.379991E-05   1.292442E-05  -3.247943E-05  -3.945950E-05   6.586942E-05   1.545558E-04
  a    40  -1.068118E-05   6.602491E-05  -1.526672E-04   1.500278E-05   7.884791E-07  -8.475208E-06   4.134532E-05   2.277980E-05
  a    41  -6.723816E-05  -1.816339E-04   9.883532E-05  -2.137907E-05  -1.254149E-05  -1.943910E-05   4.053516E-05   1.342164E-04
  a    42  -3.643522E-06   8.496211E-05   9.698171E-06   1.550890E-05  -1.470102E-05   8.641292E-06  -4.139956E-05  -8.359885E-05
  a    43  -1.524610E-05  -8.100597E-06   4.353392E-05   4.110889E-05  -9.407862E-06   1.946768E-05  -6.132899E-05  -7.888513E-05
  a    44  -1.970282E-04   3.755865E-05   1.524222E-04   7.131316E-05  -5.197288E-05   1.866747E-05  -1.048650E-05   1.255325E-04
  a    45   7.654099E-05  -1.685861E-04  -1.532046E-04   1.236898E-05  -2.819479E-05  -1.544341E-05   9.561227E-05   2.306817E-04
  a    46  -1.241529E-04  -4.981131E-05  -1.333432E-04  -1.680937E-05  -3.417964E-06  -6.254950E-05  -6.442741E-05   5.193382E-05
  a    47  -1.566650E-04  -7.493004E-05   1.056166E-04   1.996050E-05  -9.334550E-06   1.935738E-06   8.016376E-05   6.840326E-05
  a    48   2.189231E-05  -4.497829E-05   6.127034E-05   3.069959E-06   3.222977E-06   1.491178E-05   5.418118E-05  -3.923591E-05
  a    49   2.256215E-04  -3.829113E-05  -4.156125E-05  -4.788492E-06   6.958011E-06   4.626218E-06   3.058660E-07   1.101609E-04
  a    50  -1.252462E-04  -1.217239E-05  -2.961286E-05  -1.187678E-05   5.660095E-06  -2.670583E-05  -4.065528E-05  -1.191084E-04
  a    51   4.740588E-06   4.203774E-07  -3.433016E-06  -9.609976E-05  -1.174105E-04  -6.070059E-06   4.958030E-06   6.139936E-06
  a    52   8.505508E-05   2.404137E-05   9.334774E-05   7.394822E-06  -2.714259E-06   1.545166E-05   1.687218E-05   4.741925E-05
  a    53  -5.823774E-05   1.113953E-04   9.371636E-05   1.511655E-05   8.762082E-06   2.326712E-05  -2.318128E-05  -1.102893E-04
  a    54  -2.325470E-06   6.886625E-06   3.194869E-06  -1.045886E-04  -1.813919E-04   6.267646E-05  -2.734428E-05  -9.536666E-06
  a    55   7.463789E-05  -9.642379E-05  -6.125726E-05  -7.978154E-06  -1.479496E-05  -7.621188E-06   2.779192E-05  -8.116398E-07
  a    56   7.106938E-05   5.565282E-05  -1.263266E-05   2.728598E-06  -1.171561E-05  -4.560706E-06  -3.128845E-05  -4.212438E-06
  a    57   1.266535E-04   5.449193E-05   4.282398E-05   2.243830E-05   2.038531E-06   3.140948E-05  -3.057689E-05  -3.961459E-06
  a    58  -1.217454E-05   3.838750E-07   5.696061E-06   4.990702E-05   5.846164E-05  -3.317274E-05  -6.239053E-06   8.923250E-06
  a    59  -2.906421E-07   1.120574E-06   1.891188E-05   2.431899E-04   3.173860E-04  -3.267672E-05   4.071125E-06   1.795272E-05
  a    60  -1.228919E-04   2.961040E-05   1.273509E-04   3.853032E-05  -6.204372E-05   1.693962E-05   1.583020E-05   1.412635E-04
  a    61   2.433545E-05   7.784876E-05  -7.638987E-06  -2.548906E-05   1.928553E-05   1.347426E-05  -2.626609E-05  -1.302536E-04
  a    62  -1.352216E-05   1.943839E-05  -2.548743E-06   2.362506E-05   4.784666E-05  -5.246012E-05  -1.833232E-05  -2.236336E-05
  a    63   1.713390E-04  -3.973353E-05   2.017679E-05   7.861795E-06  -4.296813E-06   6.581033E-06   3.938902E-05   3.762349E-05
  a    64  -7.432581E-05   1.148160E-05  -1.265811E-05   2.077699E-05   2.236520E-05   1.053910E-06  -2.415724E-05  -5.786460E-06
  a    65   2.752920E-04  -8.517676E-06  -6.174193E-05   3.101092E-06   5.614888E-06   1.416992E-05   3.626083E-05   8.684486E-05
  a    66  -8.517676E-06   1.526767E-04  -1.353240E-05   2.349943E-06   2.827664E-06   8.395064E-07  -2.986803E-05  -7.757658E-05
  a    67  -6.174193E-05  -1.353240E-05   1.326741E-04   1.729626E-05   2.197937E-05   9.394492E-06   4.936355E-06  -2.846245E-05
  a    68   3.101092E-06   2.349943E-06   1.729626E-05   3.376589E-04   3.925766E-04   2.016389E-05  -1.256967E-07   1.001802E-05
  a    69   5.614888E-06   2.827664E-06   2.197937E-05   3.925766E-04   5.163585E-04  -1.289890E-05   2.099857E-06  -6.097365E-06
  a    70   1.416992E-05   8.395064E-07   9.394492E-06   2.016389E-05  -1.289890E-05   1.072068E-04   4.550427E-06   3.990415E-06
  a    71   3.626083E-05  -2.986803E-05   4.936355E-06  -1.256967E-07   2.099857E-06   4.550427E-06   7.379732E-05   7.430941E-05
  a    72   8.684486E-05  -7.757658E-05  -2.846245E-05   1.001802E-05  -6.097365E-06   3.990415E-06   7.430941E-05   2.930225E-04
  a    73   9.597646E-06  -3.734938E-06   9.879291E-06   3.206748E-05  -5.212084E-06   6.320347E-05  -1.217769E-05   4.361977E-06
  a    74  -6.774393E-06  -2.501992E-05  -5.420956E-05   8.338281E-06   1.961135E-05  -1.276222E-05  -1.667909E-05  -1.005576E-05
  a    75  -3.741023E-05   3.579047E-05  -2.826908E-05  -1.503464E-05   1.597609E-06  -1.084948E-05  -5.045842E-05  -4.947847E-05
  a    76  -8.455902E-05   3.776020E-05  -1.912361E-06  -9.458669E-06   7.482494E-07  -5.605441E-06  -4.595068E-05  -1.271243E-04
  a    77   1.046493E-05  -1.028115E-05  -1.990687E-06   5.688281E-06  -4.867041E-06   2.445857E-06   1.330175E-05   3.493296E-05
  a    78   4.414280E-05   2.458240E-05   2.624442E-05  -1.049752E-05   2.766950E-06   1.253782E-05   5.564843E-06  -2.807216E-06
  a    79  -1.656441E-05  -2.423382E-05  -4.600107E-06  -3.701659E-06  -1.107725E-05  -7.221335E-06   2.783934E-06   3.916466E-05
  a    80  -3.793400E-05   1.751835E-05   3.895264E-05   8.270469E-06   3.624125E-06   1.078484E-05  -6.941035E-06  -1.540731E-05
  a    81  -3.275006E-06  -2.316602E-05  -1.542998E-05  -6.306274E-06   6.322046E-07  -7.366655E-06  -7.682686E-06  -7.266718E-06
  a    82  -5.132925E-05   2.312037E-05   2.491227E-05   9.105650E-06  -3.816128E-06   3.329783E-06  -1.211308E-05  -3.587675E-05
  a    83  -1.273635E-05  -1.618332E-05  -1.381156E-05  -1.107005E-06  -1.375757E-07   3.099472E-07   8.579871E-06   4.391394E-05
  a    84  -1.043014E-05   1.682913E-05  -2.419596E-06   1.809325E-06   5.841126E-06   3.262006E-06  -1.792388E-05  -1.206947E-05
  a    85   2.160573E-06   1.934084E-07   2.251330E-06   1.549127E-06  -1.113299E-06   1.199552E-06   7.718435E-06  -5.023290E-06
  a    86  -2.392530E-05   2.892760E-05  -9.685117E-06  -3.132979E-06   1.308117E-06  -2.984275E-06  -2.366203E-05  -3.415146E-05

               a    73        a    74        a    75        a    76        a    77        a    78        a    79        a    80
  a    13   1.791547E-05  -4.760464E-04  -1.358345E-03   9.098950E-05  -1.203246E-03  -1.957231E-04   6.114558E-04   1.952911E-04
  a    14   1.075625E-05  -7.589123E-04  -6.883339E-04   6.228142E-04   4.032829E-04   6.969226E-05  -2.944879E-04   2.645138E-04
  a    15  -1.310285E-04   4.861556E-04  -5.292465E-04   2.211301E-05   1.025674E-03  -8.743121E-04  -2.961108E-04  -4.225538E-04
  a    16  -1.096476E-04   3.261345E-04  -5.686861E-04  -3.308678E-04   8.609787E-04  -1.263548E-03   2.256381E-04  -7.378538E-04
  a    17   5.767573E-05  -4.753588E-04  -7.648615E-04   2.866103E-04  -5.040036E-04  -1.704703E-04  -1.137679E-03  -7.680202E-04
  a    18   1.000045E-04  -1.340717E-03  -1.032281E-03   1.408260E-03  -2.084291E-04   8.865544E-04   4.385258E-04   1.128702E-03
  a    19  -1.412475E-04   2.327029E-03   4.686323E-04  -6.115430E-04   5.551010E-04  -1.821305E-03   1.318759E-03  -1.088379E-03
  a    20  -6.796597E-05   6.540877E-04  -4.009040E-04  -2.586913E-04   1.103150E-03  -1.034370E-03   6.162630E-04  -1.084027E-03
  a    21  -2.841934E-05   2.089261E-03   1.744122E-03  -1.793718E-04   1.148418E-03  -1.141390E-04   4.118946E-04  -1.433122E-03
  a    22  -3.366661E-05   1.322494E-03  -1.695349E-04  -8.463692E-04   5.870613E-04  -1.295707E-04   1.241447E-04  -4.125805E-04
  a    23  -6.741199E-05   1.486681E-03   6.848901E-04  -1.046040E-03   1.133970E-03  -9.132215E-04   3.748059E-04  -6.178163E-04
  a    24  -5.835669E-05   9.462786E-04   7.426120E-04  -6.411864E-04   5.639741E-04  -2.891576E-04  -1.353503E-04  -6.249955E-04
  a    25  -5.738198E-05  -8.336000E-04  -9.769408E-04  -1.306616E-03   8.729579E-04  -3.033042E-04   2.547099E-04  -2.636663E-04
  a    26   6.778954E-05  -2.030639E-03   5.210106E-04  -1.636008E-04   7.838507E-04   1.313921E-03  -5.227965E-04   1.053667E-03
  a    27  -2.799075E-04  -1.657475E-04   1.726583E-04   5.168992E-04  -1.970666E-04   1.557469E-04  -5.525965E-05   3.134003E-05
  a    28   1.006538E-04  -2.237767E-03  -2.392959E-03  -3.747904E-03   1.570973E-03   1.253057E-03   1.155963E-03   1.164211E-03
  a    29   1.902427E-03  -2.944514E-05   3.923486E-06   5.566998E-05   1.928149E-05  -8.543687E-05   7.634700E-05   9.576338E-05
  a    30   8.939607E-04   2.867006E-04  -9.129142E-05   3.615728E-05  -5.289319E-06  -8.598351E-05  -8.182548E-06  -1.391090E-04
  a    31  -3.315588E-03  -1.595952E-04   3.384405E-05   3.626787E-05  -1.434705E-04   1.815928E-04   3.093283E-05  -1.164945E-04
  a    32  -3.749236E-03  -1.126531E-04   1.273306E-05  -5.842962E-05  -2.959023E-05   1.021642E-04   9.168913E-05  -7.664794E-05
  a    33   3.696008E-04   9.044727E-04   2.054913E-04  -6.065009E-04   5.014493E-04  -5.920044E-04   2.500940E-04  -4.999466E-04
  a    34  -1.177048E-03  -2.729347E-04   9.797282E-05   1.594943E-04  -8.872224E-05   1.051753E-04   5.874386E-05  -6.420239E-05
  a    35   6.697931E-04   5.209867E-05  -5.726540E-06   1.583144E-06  -1.705281E-05  -1.283153E-05  -4.138205E-05   3.447859E-05
  a    36   2.913439E-04   7.658852E-05  -7.643968E-07  -1.420497E-05   1.038214E-05  -3.402733E-05  -2.316964E-05   1.173240E-05
  a    37  -3.115733E-06  -6.753701E-05  -5.533073E-05  -9.873440E-05   1.858608E-04  -2.061252E-06   2.302745E-05   3.538660E-05
  a    38   1.042051E-05  -9.331724E-05  -7.096252E-05   7.368156E-05  -2.087523E-04   6.055797E-05  -2.476348E-05   2.987547E-05
  a    39   2.850205E-06  -6.672351E-05  -1.044565E-04  -2.582354E-05  -5.898829E-06  -2.885976E-05   4.014060E-05   1.795199E-05
  a    40  -1.163387E-05   1.583434E-04   5.351997E-05  -3.506457E-05   1.349289E-04  -1.163743E-04   1.985705E-05  -9.859468E-05
  a    41  -3.144055E-06   5.446759E-05   3.829907E-05  -1.029597E-04   4.607318E-05   2.179393E-05   2.202624E-05  -1.474099E-06
  a    42  -2.013108E-05  -4.102688E-05  -2.644455E-05   5.155291E-05  -1.868288E-05  -4.460921E-06   3.480254E-05   2.307133E-05
  a    43  -8.223767E-06  -1.368157E-04  -8.044202E-05  -7.044553E-06  -1.067955E-04   8.734044E-05  -8.007976E-06   6.168206E-05
  a    44   1.096312E-05  -2.584526E-04  -2.037634E-04  -4.926765E-05  -8.323108E-05   2.089065E-05   1.143386E-04   1.563702E-04
  a    45  -1.016486E-05   2.149335E-04  -3.035917E-05  -1.797808E-04   2.139687E-04  -2.212562E-04   1.039952E-04  -1.322072E-04
  a    46  -2.009093E-05   4.268397E-04   2.529482E-04   6.293583E-05   7.593454E-05  -2.535811E-04   9.199995E-05  -1.359611E-04
  a    47   1.630570E-05  -1.065874E-04  -1.343889E-04  -4.174132E-05   7.501887E-05  -3.104933E-05   5.384387E-05   5.009858E-05
  a    48   2.148345E-05  -1.176152E-04  -6.732900E-05  -2.099416E-05   2.882553E-05   6.860246E-05  -1.655366E-05   6.543177E-05
  a    49   1.073954E-05   5.131348E-05   3.966664E-05  -8.207622E-05  -4.094433E-05   6.745405E-05  -5.710139E-06  -2.849275E-05
  a    50  -1.081110E-05   1.320870E-04   5.660317E-05   7.957971E-05  -2.761769E-06  -7.393672E-05   3.041709E-05  -5.319030E-05
  a    51  -1.711586E-05  -1.634012E-05  -3.329409E-06  -3.045191E-06  -4.264082E-07   6.177039E-06   2.974775E-06   3.203062E-06
  a    52   3.328837E-06  -1.076476E-04  -6.423846E-05  -1.355684E-04   1.133952E-04   7.194131E-05  -2.466986E-05   1.156773E-05
  a    53   4.968403E-06  -1.893685E-04  -2.618059E-05   1.066245E-04  -9.578602E-05   1.006810E-04  -8.587578E-05   8.923670E-05
  a    54   3.183800E-04  -2.620503E-07   1.162627E-06   4.367808E-06  -2.222461E-06  -4.658717E-06  -8.603144E-07   3.249820E-06
  a    55   1.265708E-05   1.112613E-04   7.892354E-06  -3.526539E-05   3.496985E-05  -5.917837E-05   2.218327E-05  -5.857292E-05
  a    56  -2.049763E-06   5.135598E-05   4.326288E-05  -4.234482E-05  -8.689377E-06   5.088561E-06   8.915432E-06  -2.464490E-05
  a    57   8.682104E-06  -1.994286E-04  -1.009274E-04  -4.282049E-05  -6.533768E-05   1.510019E-04  -5.286801E-05   5.844521E-05
  a    58   8.655312E-05  -2.235779E-06  -5.607067E-06  -4.470323E-06   3.103570E-06  -9.477063E-06   8.928056E-06   1.526654E-06
  a    59  -1.330438E-04   2.096850E-06  -1.439675E-05  -1.256540E-05  -3.129313E-06   2.151724E-06   2.743549E-07   4.050957E-06
  a    60   1.175472E-05  -2.009293E-04  -1.221992E-04  -7.364591E-05  -6.450347E-06   4.727738E-05   3.471173E-05   1.154467E-04
  a    61  -2.274423E-06  -1.940929E-06   8.676801E-05   6.446259E-05  -4.675110E-05   6.803265E-05  -8.076676E-05  -2.853814E-07
  a    62   3.373692E-05   6.745105E-06   1.613864E-05   1.578456E-05  -1.087781E-05  -7.606720E-07  -1.349945E-05   5.378067E-07
  a    63   2.088054E-05  -7.812032E-05  -8.729113E-05  -1.029419E-04   1.646403E-05   8.660297E-05  -9.461653E-06   1.073742E-05
  a    64   1.633582E-05   5.095482E-05   3.179559E-05   2.672555E-05   5.628729E-06  -5.000542E-05   1.590981E-05  -1.165650E-05
  a    65   9.597646E-06  -6.774393E-06  -3.741023E-05  -8.455902E-05   1.046493E-05   4.414280E-05  -1.656441E-05  -3.793400E-05
  a    66  -3.734938E-06  -2.501992E-05   3.579047E-05   3.776020E-05  -1.028115E-05   2.458240E-05  -2.423382E-05   1.751835E-05
  a    67   9.879291E-06  -5.420956E-05  -2.826908E-05  -1.912361E-06  -1.990687E-06   2.624442E-05  -4.600107E-06   3.895264E-05
  a    68   3.206748E-05   8.338281E-06  -1.503464E-05  -9.458669E-06   5.688281E-06  -1.049752E-05  -3.701659E-06   8.270469E-06
  a    69  -5.212084E-06   1.961135E-05   1.597609E-06   7.482494E-07  -4.867041E-06   2.766950E-06  -1.107725E-05   3.624125E-06
  a    70   6.320347E-05  -1.276222E-05  -1.084948E-05  -5.605441E-06   2.445857E-06   1.253782E-05  -7.221335E-06   1.078484E-05
  a    71  -1.217769E-05  -1.667909E-05  -5.045842E-05  -4.595068E-05   1.330175E-05   5.564843E-06   2.783934E-06  -6.941035E-06
  a    72   4.361977E-06  -1.005576E-05  -4.947847E-05  -1.271243E-04   3.493296E-05  -2.807216E-06   3.916466E-05  -1.540731E-05
  a    73   2.653434E-04   5.917186E-06  -5.868016E-06  -7.338719E-06   9.863663E-07  -3.443367E-06  -4.959633E-06   5.369492E-06
  a    74   5.917186E-06   1.620430E-04   8.989368E-05   2.510076E-05   4.092138E-06  -6.985833E-05   1.321495E-05  -6.438820E-05
  a    75  -5.868016E-06   8.989368E-05   1.516138E-04   2.898944E-05   3.549966E-06   1.288136E-05  -2.208714E-05  -1.224523E-05
  a    76  -7.338719E-06   2.510076E-05   2.898944E-05   1.483428E-04  -8.958253E-05  -2.947811E-05  -2.173108E-05   2.606636E-06
  a    77   9.863663E-07   4.092138E-06   3.549966E-06  -8.958253E-05   1.642226E-04  -2.572328E-05   1.939130E-05  -1.084322E-05
  a    78  -3.443367E-06  -6.985833E-05   1.288136E-05  -2.947811E-05  -2.572328E-05   1.060398E-04  -3.895688E-05   3.400929E-05
  a    79  -4.959633E-06   1.321495E-05  -2.208714E-05  -2.173108E-05   1.939130E-05  -3.895688E-05   5.378877E-05   7.156430E-06
  a    80   5.369492E-06  -6.438820E-05  -1.224523E-05   2.606636E-06  -1.084322E-05   3.400929E-05   7.156430E-06   6.729891E-05
  a    81  -4.126679E-06   5.332443E-05   3.138498E-05  -1.024107E-05   3.852234E-05  -2.484808E-05   6.529017E-06  -2.243663E-05
  a    82  -1.291721E-06  -4.283090E-05  -1.975539E-05   3.472402E-05  -1.181728E-06  -6.106846E-06  -1.267977E-06   2.325539E-05
  a    83  -1.235967E-07   1.844603E-05  -1.036359E-05  -1.012290E-05   3.537128E-06  -2.085047E-05   2.210177E-05  -1.285734E-05
  a    84   8.777340E-07  -4.183741E-06   1.173996E-05   7.624382E-06  -1.194662E-05   1.154898E-05   4.089709E-06   5.975081E-06
  a    85   1.628524E-06   3.184853E-06  -4.328769E-06  -3.591844E-06   6.903776E-07  -3.390566E-07  -4.408154E-07   4.401715E-06
  a    86  -1.899103E-06   5.020463E-06   2.008944E-05   1.090550E-05  -1.348829E-05   7.263093E-06   1.199526E-05   1.214777E-05

               a    81        a    82        a    83        a    84        a    85        a    86
  a    13  -2.171589E-04  -3.498987E-04  -2.352459E-04   1.114295E-03  -6.669500E-04   8.037560E-04
  a    14   5.484717E-04  -1.340623E-04   3.707826E-04  -2.521676E-04  -3.063594E-04  -1.335527E-03
  a    15   8.365687E-04  -1.321182E-04  -5.731768E-05  -1.062573E-03  -3.859968E-04  -1.705528E-03
  a    16   1.680893E-04   4.157286E-05  -4.509650E-04   1.542431E-04   8.728307E-05   2.601886E-04
  a    17  -4.548335E-04   3.743896E-04  -2.394392E-04  -7.879786E-04   5.831070E-04  -5.588736E-04
  a    18  -3.512971E-04   1.027911E-03   9.474547E-04   4.802187E-04  -1.966947E-04   7.812094E-04
  a    19   1.925385E-03  -8.154198E-04   4.279457E-04   3.089972E-04  -2.656285E-04  -1.500350E-04
  a    20  -5.612374E-04  -5.193515E-04   4.910086E-04  -2.753294E-04  -1.380049E-04  -8.737871E-04
  a    21   2.695028E-04  -1.125208E-03   1.177879E-03  -2.748608E-04   2.265009E-04  -1.817080E-04
  a    22   9.836330E-04  -6.550245E-04  -2.017735E-04   1.919200E-04  -3.482129E-04   3.634506E-04
  a    23  -4.159276E-05  -6.335981E-04   9.384977E-04  -6.643057E-05   1.366349E-04   4.124286E-05
  a    24   3.491627E-04  -9.347376E-04   5.640595E-04   3.860169E-04  -4.550041E-04   2.588886E-04
  a    25  -4.789037E-04  -2.495548E-04  -2.724142E-04  -2.411569E-04   1.016910E-04  -2.958330E-04
  a    26  -9.765293E-04   1.307272E-03  -9.300573E-04   1.508254E-04  -2.756367E-04   5.827976E-05
  a    27  -2.075571E-04   4.328406E-04  -3.982029E-04   6.991143E-05   1.082977E-04  -1.221183E-04
  a    28  -3.110924E-04  -5.336791E-04   3.576280E-04  -1.150786E-05  -1.347712E-04  -3.310372E-04
  a    29  -1.980467E-05  -6.745932E-05  -8.515936E-06  -6.525477E-05   1.013585E-04   2.622205E-05
  a    30   2.986870E-05   2.685335E-05  -2.407039E-05   2.324639E-05   2.015701E-05  -9.983415E-05
  a    31   8.415658E-05   7.060995E-06   3.185873E-05  -5.150686E-05  -2.170621E-05  -4.784264E-05
  a    32   2.473252E-05  -1.320181E-04   6.194243E-05  -5.928887E-05  -3.889075E-05  -4.866479E-05
  a    33   4.178550E-04  -1.050134E-03   4.950449E-04  -6.055875E-04   3.250164E-04  -6.142725E-04
  a    34   6.283095E-05   6.841877E-05   1.802100E-05   7.844431E-05   3.221312E-05  -1.492278E-05
  a    35  -2.585049E-06   1.419785E-05  -9.365427E-06   1.363977E-05  -5.976931E-07   9.785814E-06
  a    36   2.782163E-06  -1.018257E-05   1.336310E-05   1.497741E-05   9.313105E-06  -1.227258E-05
  a    37   3.288396E-06   8.674042E-06  -1.124664E-05  -2.124246E-05   4.581288E-05   3.862862E-05
  a    38  -9.228530E-05   3.025130E-05   1.511010E-05   6.281323E-05  -3.017730E-05   1.267145E-05
  a    39  -4.568603E-05   5.789732E-05   4.276944E-05  -8.803763E-06   7.759141E-06  -2.470130E-05
  a    40   8.663799E-05  -1.387210E-05   2.728081E-05  -1.320052E-05   6.801846E-06   3.214735E-05
  a    41   3.212401E-05  -9.029642E-05  -7.518365E-06  -1.693879E-05   3.700155E-06  -2.026346E-05
  a    42  -3.915693E-05   6.186747E-05   5.255336E-06   3.609194E-05  -1.059843E-05   4.280214E-05
  a    43  -3.971112E-05   6.099988E-05  -2.130968E-05   6.411421E-05  -3.671794E-05   8.455853E-05
  a    44  -1.796219E-04   1.306596E-04   4.286722E-05   9.981278E-05  -4.934923E-05   8.328724E-05
  a    45   1.387384E-04  -8.310059E-05   1.230778E-04  -1.118984E-04   4.665942E-05  -7.528354E-05
  a    46   1.707126E-04  -6.484983E-05   8.394650E-05  -4.085664E-05   4.662220E-05   5.552629E-05
  a    47  -2.747581E-05   8.450553E-05   3.761127E-05  -1.127077E-06   1.912636E-05  -2.529958E-05
  a    48  -1.455814E-05   3.821386E-05  -3.778559E-05   3.209742E-05  -2.035575E-07  -9.502174E-06
  a    49   2.165694E-05  -8.263483E-05  -1.974427E-05   4.582098E-05  -2.422835E-05   4.258896E-05
  a    50   5.715477E-05   2.043362E-05   3.677516E-05   2.556846E-05   4.325193E-06   5.575190E-05
  a    51  -5.165059E-06  -1.646360E-06  -3.210698E-06  -2.080665E-06  -7.339736E-07  -2.964055E-06
  a    52   3.657624E-06  -1.790518E-06  -1.723996E-05  -7.771200E-06   6.394271E-06  -5.989093E-06
  a    53  -9.956137E-05   1.144010E-04  -8.686479E-05   2.021578E-05  -2.392752E-05  -1.505049E-05
  a    54  -5.665933E-06   1.506486E-07  -3.786200E-06   2.027999E-06   1.639343E-06   4.615681E-07
  a    55   6.937955E-05  -4.137516E-05   4.308734E-05  -1.832721E-05   9.727299E-06   7.153754E-06
  a    56  -1.916472E-05  -7.153943E-05   1.074154E-05   4.283834E-06   1.624863E-06   1.587299E-05
  a    57  -7.904719E-05   2.104545E-05  -5.024292E-05   4.800177E-05  -2.705947E-05   1.431986E-05
  a    58  -3.395355E-06   3.954554E-06  -7.786583E-09  -8.346385E-07   1.158534E-06   7.315646E-07
  a    59  -6.367003E-06   4.887089E-06   7.557884E-07   2.875256E-06  -5.700803E-07   8.725272E-08
  a    60  -1.019052E-04   1.046909E-04   2.217542E-05   6.367891E-06   2.103531E-06  -1.986426E-06
  a    61  -3.664839E-06  -1.311325E-05  -2.949124E-05  -1.771351E-05   7.911053E-07  -1.942115E-05
  a    62  -5.342698E-07   4.473251E-06  -7.979498E-06  -4.821698E-06  -1.583454E-06  -7.655961E-07
  a    63  -2.687509E-05  -3.529691E-05  -2.118140E-05   1.001152E-05  -3.737061E-06  -1.125048E-05
  a    64   1.862069E-05   7.793376E-06   1.804996E-05  -1.609547E-06  -3.600309E-07   1.220970E-05
  a    65  -3.275006E-06  -5.132925E-05  -1.273635E-05  -1.043014E-05   2.160573E-06  -2.392530E-05
  a    66  -2.316602E-05   2.312037E-05  -1.618332E-05   1.682913E-05   1.934084E-07   2.892760E-05
  a    67  -1.542998E-05   2.491227E-05  -1.381156E-05  -2.419596E-06   2.251330E-06  -9.685117E-06
  a    68  -6.306274E-06   9.105650E-06  -1.107005E-06   1.809325E-06   1.549127E-06  -3.132979E-06
  a    69   6.322046E-07  -3.816128E-06  -1.375757E-07   5.841126E-06  -1.113299E-06   1.308117E-06
  a    70  -7.366655E-06   3.329783E-06   3.099472E-07   3.262006E-06   1.199552E-06  -2.984275E-06
  a    71  -7.682686E-06  -1.211308E-05   8.579871E-06  -1.792388E-05   7.718435E-06  -2.366203E-05
  a    72  -7.266718E-06  -3.587675E-05   4.391394E-05  -1.206947E-05  -5.023290E-06  -3.415146E-05
  a    73  -4.126679E-06  -1.291721E-06  -1.235967E-07   8.777340E-07   1.628524E-06  -1.899103E-06
  a    74   5.332443E-05  -4.283090E-05   1.844603E-05  -4.183741E-06   3.184853E-06   5.020463E-06
  a    75   3.138498E-05  -1.975539E-05  -1.036359E-05   1.173996E-05  -4.328769E-06   2.008944E-05
  a    76  -1.024107E-05   3.472402E-05  -1.012290E-05   7.624382E-06  -3.591844E-06   1.090550E-05
  a    77   3.852234E-05  -1.181728E-06   3.537128E-06  -1.194662E-05   6.903776E-07  -1.348829E-05
  a    78  -2.484808E-05  -6.106846E-06  -2.085047E-05   1.154898E-05  -3.390566E-07   7.263093E-06
  a    79   6.529017E-06  -1.267977E-06   2.210177E-05   4.089709E-06  -4.408154E-07   1.199526E-05
  a    80  -2.243663E-05   2.325539E-05  -1.285734E-05   5.975081E-06   4.401715E-06   1.214777E-05
  a    81   4.960143E-05  -1.879332E-05   9.263320E-06  -5.201820E-06   3.287578E-06  -7.275386E-07
  a    82  -1.879332E-05   6.704648E-05  -6.594491E-06   1.188728E-06  -2.157648E-06   1.983404E-06
  a    83   9.263320E-06  -6.594491E-06   4.877012E-05  -5.060004E-06   6.153159E-06  -1.894795E-06
  a    84  -5.201820E-06   1.188728E-06  -5.060004E-06   3.499584E-05  -1.384568E-05   2.915843E-05
  a    85   3.287578E-06  -2.157648E-06   6.153159E-06  -1.384568E-05   2.070144E-05  -2.716942E-07
  a    86  -7.275386E-07   1.983404E-06  -1.894795E-06   2.915843E-05  -2.716942E-07   5.290406E-05

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99998026     1.99991255     1.99968212     1.99963947
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.99943898     1.99941337     1.99910542     1.99881820     1.99871605     1.99850904     1.99842177     1.99791978
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     1.99755908     1.99715013     1.99688524     1.99561688     1.99357943     1.99251714     1.97505241     1.91471858
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     1.01334991     0.98455276     0.08939583     0.02755319     0.00701921     0.00445150     0.00340386     0.00266778
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00209715     0.00180512     0.00172390     0.00109868     0.00095949     0.00077600     0.00075420     0.00071706
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00062016     0.00054425     0.00047383     0.00038752     0.00034915     0.00031322     0.00028701     0.00025144
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62       MO    63       MO    64
  occ(*)=     0.00023564     0.00021593     0.00018634     0.00017221     0.00016411     0.00013975     0.00012943     0.00009406
              MO    65       MO    66       MO    67       MO    68       MO    69       MO    70       MO    71       MO    72
  occ(*)=     0.00008619     0.00007653     0.00005500     0.00003755     0.00003646     0.00003105     0.00002697     0.00002388
              MO    73       MO    74       MO    75       MO    76       MO    77       MO    78       MO    79       MO    80
  occ(*)=     0.00002101     0.00001657     0.00001389     0.00001281     0.00001012     0.00000693     0.00000502     0.00000433
              MO    81       MO    82       MO    83       MO    84       MO    85       MO    86
  occ(*)=     0.00000325     0.00000263     0.00000200     0.00000113     0.00000081     0.00000027


 total number of electrons =   66.0000000000

 item #                     2 suffix=:.drt1.state2:
 read_civout: repnuc=  -468.746689847934     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.49934727  -242.75265743
 residuum:     0.00046948
 deltae:     0.00000002
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  1max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.49821129  -242.75152144
 residuum:     0.00082351
 deltae:     0.00000229
 apxde:     0.00000058

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95435843    -0.07607986     0.07258370    -0.00353977    -0.05294786     0.07973749    -0.05811567    -0.08941355
 ref:   2     0.07603121     0.95493500    -0.00360883    -0.09239823    -0.00450482    -0.00576340     0.01238292    -0.02766607

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.10461492     0.00751018    -0.00637176    -0.01795015    -0.06931176    -0.09016971     0.05544017    -0.02912531
 ref:   2     0.00645402    -0.15472469     0.04339474     0.00800906    -0.03832957    -0.03700548    -0.08647814     0.02641285

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24
 ref:   1     0.05338742     0.15531199    -0.02020986    -0.00100424     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00706683     0.02004825     0.01166497    -0.01612543     0.00000000     0.00000000     0.00000000     0.00000000

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95435843    -0.07607986     0.07258370    -0.00353977    -0.05294786     0.07973749    -0.05811567    -0.08941355
 ref:   2     0.07603121     0.95493500    -0.00360883    -0.09239823    -0.00450482    -0.00576340     0.01238292    -0.02766607

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.10461492     0.00751018    -0.00637176    -0.01795015     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2     0.00645402    -0.15472469     0.04339474     0.00800906     0.00000000     0.00000000     0.00000000     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=   11036  DYX=       0  DYW=       0
   D0Z=   17456  D0Y=   53891  D0X=       0  D0W=       0
  DDZI=   21345 DDYI=   56427 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    2529 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=   11036  DYX=       0  DYW=       0
   D0Z=   17456  D0Y= 2111106  D0X=       0  D0W=       0
  DDZI=  251850 DDYI=  658014 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    42.000000
   42  correlated and    24  frozen core electrons
   7396 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000    
  a     6    0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000    
  a     7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000    
  a     8    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000    

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     9    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    10    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    11    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    12    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a    13    0.00000        0.00000        0.00000        0.00000        1.99900       2.006287E-04   6.041517E-04   9.270915E-05
  a    14    0.00000        0.00000        0.00000        0.00000       2.006287E-04    1.99931      -6.100037E-04   1.625781E-04
  a    15    0.00000        0.00000        0.00000        0.00000       6.041517E-04  -6.100037E-04    1.99870      -5.626197E-04
  a    16    0.00000        0.00000        0.00000        0.00000       9.270915E-05   1.625781E-04  -5.626197E-04    1.99839    
  a    17    0.00000        0.00000        0.00000        0.00000      -1.989005E-04  -2.206109E-04  -9.930101E-06   4.312652E-04
  a    18    0.00000        0.00000        0.00000        0.00000      -1.812975E-04  -1.416548E-04   3.287464E-04  -2.293985E-04
  a    19    0.00000        0.00000        0.00000        0.00000       4.231636E-04   3.801035E-05  -7.181376E-04  -2.967875E-04
  a    20    0.00000        0.00000        0.00000        0.00000       1.663591E-04  -1.976147E-04  -4.947226E-04  -2.384392E-05
  a    21    0.00000        0.00000        0.00000        0.00000       1.449584E-04  -2.356099E-04  -5.412710E-04  -1.976357E-04
  a    22    0.00000        0.00000        0.00000        0.00000       3.094445E-04   1.639944E-04  -1.345783E-04  -3.395729E-04
  a    23    0.00000        0.00000        0.00000        0.00000      -5.247346E-05  -5.113683E-06  -1.705545E-04  -1.456982E-04
  a    24    0.00000        0.00000        0.00000        0.00000       2.607933E-04   1.270818E-05  -2.636965E-04  -1.733245E-04
  a    25    0.00000        0.00000        0.00000        0.00000      -1.860881E-04   1.558031E-04   1.707266E-04   3.095054E-04
  a    26    0.00000        0.00000        0.00000        0.00000       9.052352E-05   2.220379E-04   2.393827E-04  -6.977180E-04
  a    27    0.00000        0.00000        0.00000        0.00000       2.364219E-05  -1.450405E-05  -1.404893E-05  -1.183291E-04
  a    28    0.00000        0.00000        0.00000        0.00000      -3.990286E-04   2.563099E-04   7.426929E-04   4.736829E-04
  a    29    0.00000        0.00000        0.00000        0.00000      -1.851360E-05  -1.084462E-05  -4.044348E-06   8.505008E-06
  a    30    0.00000        0.00000        0.00000        0.00000       2.028597E-06  -7.216366E-06   2.087618E-05  -7.694303E-06
  a    31    0.00000        0.00000        0.00000        0.00000       1.102336E-05   1.056776E-05   3.241595E-05  -1.717680E-06
  a    32    0.00000        0.00000        0.00000        0.00000      -1.626584E-04  -1.379752E-04  -1.450082E-04   1.432217E-04
  a    33    0.00000        0.00000        0.00000        0.00000      -9.270560E-04  -1.066519E-03  -1.422161E-03   1.004758E-03
  a    34    0.00000        0.00000        0.00000        0.00000      -2.882113E-05   2.633641E-04  -1.364956E-04   6.462560E-05
  a    35    0.00000        0.00000        0.00000        0.00000       3.116407E-04  -3.321294E-05  -6.287180E-05  -1.988395E-04
  a    36    0.00000        0.00000        0.00000        0.00000      -6.498832E-05   9.877322E-05  -3.686383E-05   5.870090E-06
  a    37    0.00000        0.00000        0.00000        0.00000      -2.105110E-03  -1.929922E-03  -3.909516E-04   4.728326E-03
  a    38    0.00000        0.00000        0.00000        0.00000       1.064611E-04  -7.255903E-04  -2.919873E-03  -3.148651E-03
  a    39    0.00000        0.00000        0.00000        0.00000       2.046028E-04  -9.766317E-04   3.546632E-04  -1.970957E-03
  a    40    0.00000        0.00000        0.00000        0.00000       1.730499E-03   2.370865E-03   3.670930E-03   1.843449E-03
  a    41    0.00000        0.00000        0.00000        0.00000      -1.723716E-03  -1.931680E-03  -8.933779E-04  -9.297491E-04
  a    42    0.00000        0.00000        0.00000        0.00000       1.265586E-03   7.366788E-04   6.172747E-04   3.177604E-04
  a    43    0.00000        0.00000        0.00000        0.00000       3.399110E-03   1.211222E-03   1.355107E-03   1.104086E-03
  a    44    0.00000        0.00000        0.00000        0.00000       1.972814E-03  -7.298319E-04  -1.358692E-03  -5.303010E-04
  a    45    0.00000        0.00000        0.00000        0.00000      -4.699464E-04   3.802955E-04   1.691411E-03   2.581460E-03
  a    46    0.00000        0.00000        0.00000        0.00000      -1.879559E-03  -9.479984E-04   8.988403E-04   5.040389E-04
  a    47    0.00000        0.00000        0.00000        0.00000       1.162591E-03   1.010308E-03   8.388678E-04   2.586389E-03
  a    48    0.00000        0.00000        0.00000        0.00000       1.070402E-03   1.229127E-03  -2.728181E-05  -9.239352E-04
  a    49    0.00000        0.00000        0.00000        0.00000      -6.292165E-04  -1.180074E-03  -4.655811E-04   7.701344E-04
  a    50    0.00000        0.00000        0.00000        0.00000       9.749855E-04   8.138086E-04   9.707960E-04   6.935453E-04
  a    51    0.00000        0.00000        0.00000        0.00000      -1.430672E-05   4.882096E-05   4.058028E-05  -1.140976E-04
  a    52    0.00000        0.00000        0.00000        0.00000      -5.749226E-04  -5.340581E-04   4.373728E-04   2.257917E-03
  a    53    0.00000        0.00000        0.00000        0.00000       4.779581E-04   5.825309E-04  -2.369295E-04  -1.544588E-03
  a    54    0.00000        0.00000        0.00000        0.00000      -5.750496E-05  -7.138509E-05  -7.832816E-05  -7.338130E-05
  a    55    0.00000        0.00000        0.00000        0.00000       1.698643E-03   1.498484E-03   1.722818E-03   1.463469E-03
  a    56    0.00000        0.00000        0.00000        0.00000       7.246163E-05  -7.605912E-04  -5.311417E-04   9.180199E-04
  a    57    0.00000        0.00000        0.00000        0.00000       8.616627E-04  -7.294134E-04  -1.842527E-03   6.924489E-04
  a    58    0.00000        0.00000        0.00000        0.00000      -1.796440E-06  -3.754091E-05  -2.933761E-05  -3.906837E-05
  a    59    0.00000        0.00000        0.00000        0.00000       6.004638E-05  -1.507574E-04  -1.157761E-04   9.058432E-05
  a    60    0.00000        0.00000        0.00000        0.00000       3.229999E-04  -1.710069E-03  -1.904461E-03   9.388906E-04
  a    61    0.00000        0.00000        0.00000        0.00000      -2.357255E-04   6.757025E-04   5.682825E-04  -8.870276E-04
  a    62    0.00000        0.00000        0.00000        0.00000      -2.170820E-04   2.054816E-04   6.157033E-05  -2.961298E-04
  a    63    0.00000        0.00000        0.00000        0.00000       1.108475E-03  -6.226464E-04   1.020774E-04   5.948507E-04
  a    64    0.00000        0.00000        0.00000        0.00000      -3.014023E-04  -8.004438E-06   8.637584E-05   9.522401E-05
  a    65    0.00000        0.00000        0.00000        0.00000       4.479954E-04  -2.584719E-05  -3.739651E-04  -5.409336E-04
  a    66    0.00000        0.00000        0.00000        0.00000      -1.448441E-04   1.715898E-04   4.261641E-04  -2.733835E-04
  a    67    0.00000        0.00000        0.00000        0.00000       2.267973E-04   3.457074E-04   4.874694E-04   9.998066E-05
  a    68    0.00000        0.00000        0.00000        0.00000      -3.484679E-05  -4.120221E-04  -1.790119E-05   3.694798E-04
  a    69    0.00000        0.00000        0.00000        0.00000      -1.962895E-04   2.662634E-04   2.161149E-05  -1.184131E-04
  a    70    0.00000        0.00000        0.00000        0.00000      -4.222646E-05   2.625738E-04  -1.970846E-04  -8.379376E-05
  a    71    0.00000        0.00000        0.00000        0.00000       2.333680E-04   9.784843E-04   2.588271E-05  -6.632512E-04
  a    72    0.00000        0.00000        0.00000        0.00000       1.079361E-03  -3.817054E-04  -1.113264E-03  -2.003485E-03
  a    73    0.00000        0.00000        0.00000        0.00000       3.787635E-05   1.096877E-04   2.795065E-05  -1.554693E-04
  a    74    0.00000        0.00000        0.00000        0.00000      -9.213000E-05  -5.374989E-04   4.208577E-04  -2.479330E-05
  a    75    0.00000        0.00000        0.00000        0.00000      -1.110063E-03  -4.553656E-04  -3.808527E-04  -4.016886E-04
  a    76    0.00000        0.00000        0.00000        0.00000       1.432994E-04   6.476813E-04   3.573918E-04   1.762747E-06
  a    77    0.00000        0.00000        0.00000        0.00000      -1.041268E-03  -4.159864E-05   6.389516E-04   1.173596E-03
  a    78    0.00000        0.00000        0.00000        0.00000      -3.034543E-04  -2.638833E-05  -5.208194E-04  -1.289192E-03
  a    79    0.00000        0.00000        0.00000        0.00000       2.981771E-04  -3.088316E-04  -1.315773E-04   8.397019E-04
  a    80    0.00000        0.00000        0.00000        0.00000      -4.109202E-04  -1.722755E-04  -1.248098E-05   4.985161E-04
  a    81    0.00000        0.00000        0.00000        0.00000       2.278163E-04   7.885719E-04   8.651466E-04   6.790498E-05
  a    82    0.00000        0.00000        0.00000        0.00000       1.360181E-04   3.371125E-04   3.905088E-04  -1.676838E-05
  a    83    0.00000        0.00000        0.00000        0.00000      -7.541299E-05   2.125668E-04  -1.019198E-04   6.986141E-05
  a    84    0.00000        0.00000        0.00000        0.00000       1.055742E-03   1.333633E-04  -6.516579E-04   7.132202E-05
  a    85    0.00000        0.00000        0.00000        0.00000      -1.124659E-03  -6.519462E-04  -4.541735E-04   3.336476E-04
  a    86    0.00000        0.00000        0.00000        0.00000       9.055852E-05  -1.015960E-03  -1.125764E-03   2.817704E-04

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a    13  -1.989005E-04  -1.812975E-04   4.231636E-04   1.663591E-04   1.449584E-04   3.094445E-04  -5.247346E-05   2.607933E-04
  a    14  -2.206109E-04  -1.416548E-04   3.801035E-05  -1.976147E-04  -2.356099E-04   1.639944E-04  -5.113683E-06   1.270818E-05
  a    15  -9.930101E-06   3.287464E-04  -7.181376E-04  -4.947226E-04  -5.412710E-04  -1.345783E-04  -1.705545E-04  -2.636965E-04
  a    16   4.312652E-04  -2.293985E-04  -2.967875E-04  -2.384392E-05  -1.976357E-04  -3.395729E-04  -1.456982E-04  -1.733245E-04
  a    17    1.99804      -2.965703E-04  -1.937448E-04  -1.296295E-04  -1.618128E-04   2.542680E-04   7.367871E-05   7.271182E-04
  a    18  -2.965703E-04    1.99803       2.414880E-04   2.707012E-04   7.319945E-04   2.321714E-04   3.845513E-04   3.592047E-04
  a    19  -1.937448E-04   2.414880E-04    1.99729      -7.131275E-04  -4.213274E-04  -8.716188E-04  -5.056718E-04  -7.603863E-04
  a    20  -1.296295E-04   2.707012E-04  -7.131275E-04    1.99858      -3.292360E-04  -2.532104E-04  -1.844233E-04  -6.069243E-04
  a    21  -1.618128E-04   7.319945E-04  -4.213274E-04  -3.292360E-04    1.99767      -3.509135E-05  -1.338777E-03  -4.302207E-04
  a    22   2.542680E-04   2.321714E-04  -8.716188E-04  -2.532104E-04  -3.509135E-05    1.99875       1.061641E-04  -5.674823E-04
  a    23   7.367871E-05   3.845513E-04  -5.056718E-04  -1.844233E-04  -1.338777E-03   1.061641E-04    1.99863      -2.803480E-04
  a    24   7.271182E-04   3.592047E-04  -7.603863E-04  -6.069243E-04  -4.302207E-04  -5.674823E-04  -2.803480E-04    1.99816    
  a    25   2.578128E-04   4.565099E-04  -8.632485E-05  -1.158438E-04   3.356970E-04   3.509280E-04  -4.241504E-04  -3.099892E-04
  a    26   8.772917E-04  -3.940148E-04   1.106103E-03   4.343445E-04   6.135524E-04  -5.246507E-04   2.304788E-04  -6.926248E-04
  a    27   7.843591E-05  -4.441867E-05   2.496418E-04   8.506712E-05   1.698706E-04   2.748156E-05   1.150587E-04  -1.304066E-05
  a    28  -4.249648E-04  -4.176061E-04   3.907699E-04   2.051548E-04   5.653660E-04   2.506926E-05   2.042882E-04   6.325002E-04
  a    29   2.661529E-05   4.659348E-05   3.379389E-05   3.082466E-05  -4.176800E-05   2.153229E-06  -4.470887E-05   1.056958E-05
  a    30  -2.494608E-05   1.928331E-05  -4.853954E-05  -4.406840E-05  -6.963936E-06   4.086053E-06  -1.069211E-06  -1.990687E-05
  a    31   6.580338E-06  -5.091876E-05  -6.022998E-05  -1.356476E-05  -2.367707E-05  -6.734540E-06  -2.568278E-05  -4.494008E-05
  a    32   1.051931E-04   6.786921E-04  -1.783047E-04  -1.201179E-04  -6.628338E-04  -1.538970E-04  -6.829196E-04  -8.899275E-05
  a    33   5.322227E-04   5.002963E-03  -1.352449E-03  -9.987911E-04  -4.568650E-03  -1.230006E-03  -4.628965E-03  -5.203309E-04
  a    34  -1.721924E-04  -8.044928E-04   5.746525E-05   2.732713E-05   8.967566E-04  -1.957521E-04   7.536655E-04   1.877497E-04
  a    35  -1.892726E-05  -5.747510E-04   4.119467E-04   3.187724E-05   1.026246E-03   1.543207E-04   6.001474E-04   3.527813E-04
  a    36  -4.330119E-05   2.431782E-04  -8.269067E-05  -1.052116E-04  -2.989170E-04  -4.630488E-05  -2.819715E-04  -2.224926E-04
  a    37  -1.358811E-03   2.751045E-03  -5.669913E-03   9.121510E-03   1.365205E-03  -2.001983E-03   7.513977E-04   9.531984E-03
  a    38   6.407990E-03   1.001824E-03   3.146035E-03  -6.474406E-04   1.465754E-03  -3.879062E-03  -8.101330E-04  -6.735513E-03
  a    39   3.980082E-03   2.338973E-03   7.844234E-04   4.091325E-04   1.858057E-03   9.090068E-04   8.909286E-04  -3.024847E-03
  a    40  -5.177788E-03  -3.619218E-03   3.563810E-03  -5.498924E-03   2.130708E-04   4.719113E-03  -2.508360E-04  -2.577815E-03
  a    41   7.484935E-04  -1.163261E-03  -1.499889E-03   6.577895E-04   3.708297E-03   7.587458E-04  -5.339204E-04  -9.441608E-04
  a    42  -2.323532E-03   9.358987E-04   3.827800E-04  -1.738238E-05   2.572720E-03  -5.826726E-04  -3.243051E-03   1.878571E-03
  a    43  -6.040313E-04  -1.733461E-03   5.787778E-04   7.797208E-03  -1.340182E-03   6.647151E-04   1.631949E-04   3.011170E-03
  a    44   1.726939E-03  -2.954129E-04   2.171960E-04  -1.265446E-03  -8.373861E-04   9.095613E-04   1.180277E-03  -2.557778E-03
  a    45   9.465817E-04   1.320454E-03   9.168597E-04   3.985431E-03  -3.681897E-03  -8.327494E-04  -2.576764E-03  -7.283083E-04
  a    46   2.343219E-03  -4.510026E-03   2.470787E-03   2.763965E-03   7.258156E-03   1.111845E-03   5.882031E-03   8.896951E-04
  a    47  -5.380953E-03  -1.272587E-03  -6.180897E-03  -6.717665E-04   1.115112E-03  -1.349375E-03  -3.815954E-04   4.447626E-04
  a    48   2.392524E-03   2.280559E-03   3.484769E-03   2.922771E-03  -2.738516E-03   3.108156E-03  -1.895824E-03   4.184813E-04
  a    49  -1.060921E-03  -1.157190E-03   5.208524E-04  -7.097360E-04   2.607440E-04   9.576103E-04   9.648770E-04  -3.078241E-03
  a    50  -2.207273E-03   5.512500E-04  -4.590366E-05   2.391852E-03   1.209275E-03   5.554473E-04   2.325811E-04   4.669806E-03
  a    51   1.122934E-04  -3.163568E-04   6.539114E-05  -1.427667E-04   2.804034E-04   2.928100E-05   3.151276E-04  -2.444548E-04
  a    52  -4.002509E-03  -2.354040E-03  -3.095048E-03  -2.270084E-03   6.237139E-05   1.472988E-03   7.247934E-04   1.658019E-03
  a    53   1.808277E-03  -2.143707E-03  -7.075251E-04  -5.150469E-04   1.943045E-03  -1.038798E-03   1.723819E-03  -1.583097E-03
  a    54  -7.633178E-05  -4.742290E-05  -1.664769E-04  -2.099195E-04   1.325472E-04  -2.350547E-06   1.723347E-04  -1.255923E-05
  a    55   3.321646E-04   9.901710E-04   2.388741E-03   3.921691E-03  -1.998493E-03   1.034487E-03  -1.611294E-03   2.108099E-03
  a    56  -2.612352E-03  -2.585006E-03   1.561927E-04  -9.725209E-04   2.940034E-03   1.358334E-03   3.525401E-03   1.768224E-03
  a    57  -8.132631E-04   1.898972E-03  -1.313864E-03  -5.692582E-04  -3.546210E-03   5.080908E-04  -1.230894E-03  -4.203156E-04
  a    58   2.331639E-05  -8.598887E-05  -1.179418E-04  -6.903527E-05   2.180858E-04   1.359988E-04   9.287932E-05  -1.314428E-05
  a    59  -3.487637E-05   1.211097E-04  -1.317978E-04  -4.316572E-05  -2.002997E-04   1.813871E-04  -8.514318E-05   5.456445E-05
  a    60   8.115465E-04  -4.810704E-04  -2.273253E-03  -1.413619E-03  -8.980085E-04   1.095694E-03  -1.147911E-03  -1.407589E-03
  a    61   1.036343E-03  -3.601388E-04  -3.524193E-04  -6.470288E-04  -1.791923E-03  -8.613770E-04  -1.090725E-03  -9.100277E-04
  a    62   3.907141E-04   8.732617E-05  -4.373541E-04  -3.263515E-04  -1.712005E-04   5.890009E-05  -2.348741E-04  -3.712945E-04
  a    63  -2.042599E-03   9.807401E-04   2.153908E-03   1.447110E-03  -1.035800E-03  -6.299104E-04  -3.991938E-05   1.602211E-03
  a    64   3.583956E-04  -4.862556E-04  -1.340643E-04  -4.664323E-04   4.773939E-04  -2.642598E-04   2.152856E-04  -2.712837E-04
  a    65   6.482588E-05   1.663653E-04   1.167691E-03  -1.096241E-04  -3.328281E-04  -2.525727E-04   6.149172E-04  -2.098211E-04
  a    66  -8.169531E-04  -6.223190E-04   7.922129E-04  -3.900056E-03  -2.928820E-05   4.861278E-04   6.867485E-04  -1.404871E-03
  a    67  -3.639096E-04  -8.399055E-04   2.198897E-04  -3.168155E-04  -1.128541E-03   1.358923E-03  -1.186587E-03  -9.010791E-04
  a    68   5.075831E-05   5.328073E-05   1.691498E-04   4.224866E-04   4.770541E-04  -1.192116E-04  -5.175928E-04   2.276393E-05
  a    69   4.235768E-05   1.539625E-04  -1.099705E-05  -3.966200E-05  -4.325374E-04   3.970772E-05   3.824610E-04   1.428367E-04
  a    70   6.321563E-05   1.535342E-04  -1.943731E-04  -1.448581E-04  -2.354164E-04  -2.440139E-04   1.121747E-04   3.382788E-06
  a    71  -7.101741E-05  -4.834478E-04   7.989102E-04  -1.584436E-03   2.475528E-04   1.152038E-03  -6.014202E-04  -8.126514E-04
  a    72   1.086802E-03  -2.094780E-03  -1.911922E-04  -2.465856E-03   2.209781E-03  -2.198732E-04   1.711356E-03  -1.884565E-03
  a    73   1.332734E-05  -1.131930E-04   6.659882E-05  -1.590342E-04   7.185528E-05   8.086963E-05  -1.677608E-05  -1.208372E-04
  a    74  -4.630389E-04  -1.346795E-03   1.335776E-03  -2.622015E-04   1.580571E-03   1.176085E-03   1.007817E-03   2.883440E-04
  a    75  -4.698122E-04  -2.152293E-03  -3.629732E-04  -1.000472E-03   1.978283E-03  -1.734824E-04   9.128117E-04   2.590805E-04
  a    76   3.534863E-04   7.486394E-04  -4.302739E-04   1.126291E-04   1.557002E-04  -5.236703E-04  -3.658898E-04  -4.633109E-04
  a    77  -7.761927E-04   1.839528E-04   1.304919E-04   7.723312E-04   8.900999E-04   3.238877E-04   6.811522E-04   7.925497E-04
  a    78   4.994443E-06  -5.315329E-05  -1.311984E-03  -8.006777E-04  -1.618544E-04   1.517412E-04  -6.886177E-04  -9.457226E-05
  a    79  -3.949633E-04   1.045819E-03   1.009032E-03   6.905784E-04   1.040001E-04   1.376778E-04  -1.976614E-04   1.625416E-04
  a    80  -1.115550E-04   1.216064E-03  -4.451850E-04   1.024049E-04  -1.171550E-03   7.132358E-05  -4.453260E-04   1.686793E-04
  a    81  -5.844198E-04  -3.827643E-04   1.615362E-03  -6.962094E-04  -1.036797E-04   9.590695E-04  -2.945742E-04   1.377551E-04
  a    82   3.607977E-04  -2.358583E-04  -3.693659E-04  -8.990270E-04   4.824989E-04  -1.934889E-04   3.863479E-04  -9.649822E-04
  a    83   6.125497E-04   9.701374E-04  -1.352414E-04   6.912114E-05   7.518552E-04  -2.012395E-04   4.277712E-04   9.692804E-05
  a    84  -6.556303E-04   3.168716E-04   2.072826E-04  -1.768744E-04  -4.128533E-04   2.603019E-04  -1.470609E-04   1.956462E-04
  a    85   7.350097E-04  -7.781441E-05  -9.765256E-05   2.132216E-04   3.288181E-04  -2.220308E-04   3.153811E-04  -2.181893E-04
  a    86  -1.554530E-04   7.174524E-04  -3.242887E-05  -3.713686E-04  -1.187796E-04   3.440327E-04   1.264588E-04   1.573588E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a    13  -1.860881E-04   9.052352E-05   2.364219E-05  -3.990286E-04  -1.851360E-05   2.028597E-06   1.102336E-05  -1.626584E-04
  a    14   1.558031E-04   2.220379E-04  -1.450405E-05   2.563099E-04  -1.084462E-05  -7.216366E-06   1.056776E-05  -1.379752E-04
  a    15   1.707266E-04   2.393827E-04  -1.404893E-05   7.426929E-04  -4.044348E-06   2.087618E-05   3.241595E-05  -1.450082E-04
  a    16   3.095054E-04  -6.977180E-04  -1.183291E-04   4.736829E-04   8.505008E-06  -7.694303E-06  -1.717680E-06   1.432217E-04
  a    17   2.578128E-04   8.772917E-04   7.843591E-05  -4.249648E-04   2.661529E-05  -2.494608E-05   6.580338E-06   1.051931E-04
  a    18   4.565099E-04  -3.940148E-04  -4.441867E-05  -4.176061E-04   4.659348E-05   1.928331E-05  -5.091876E-05   6.786921E-04
  a    19  -8.632485E-05   1.106103E-03   2.496418E-04   3.907699E-04   3.379389E-05  -4.853954E-05  -6.022998E-05  -1.783047E-04
  a    20  -1.158438E-04   4.343445E-04   8.506712E-05   2.051548E-04   3.082466E-05  -4.406840E-05  -1.356476E-05  -1.201179E-04
  a    21   3.356970E-04   6.135524E-04   1.698706E-04   5.653660E-04  -4.176800E-05  -6.963936E-06  -2.367707E-05  -6.628338E-04
  a    22   3.509280E-04  -5.246507E-04   2.748156E-05   2.506926E-05   2.153229E-06   4.086053E-06  -6.734540E-06  -1.538970E-04
  a    23  -4.241504E-04   2.304788E-04   1.150587E-04   2.042882E-04  -4.470887E-05  -1.069211E-06  -2.568278E-05  -6.829196E-04
  a    24  -3.099892E-04  -6.926248E-04  -1.304066E-05   6.325002E-04   1.056958E-05  -1.990687E-05  -4.494008E-05  -8.899275E-05
  a    25    1.99825       1.453201E-05   7.053279E-05  -4.939039E-04  -3.275794E-05   2.972843E-07   1.034946E-05  -4.830989E-04
  a    26   1.453201E-05    1.99529      -5.815497E-04   3.124314E-04  -1.001592E-05   1.928821E-05   7.067845E-05   5.787135E-05
  a    27   7.053279E-05  -5.815497E-04    1.99939       2.806633E-04  -9.504990E-05   1.486016E-04  -1.891535E-04   1.363309E-04
  a    28  -4.939039E-04   3.124314E-04   2.806633E-04    1.99657      -2.341917E-05   3.040479E-06  -5.603430E-05  -9.444227E-04
  a    29  -3.275794E-05  -1.001592E-05  -9.504990E-05  -2.341917E-05    1.99401       3.001856E-04  -1.964873E-03  -6.629189E-04
  a    30   2.972843E-07   1.928821E-05   1.486016E-04   3.040479E-06   3.001856E-04    1.97384      -2.739073E-03  -1.654318E-03
  a    31   1.034946E-05   7.067845E-05  -1.891535E-04  -5.603430E-05  -1.964873E-03  -2.739073E-03    1.92955      -3.095337E-02
  a    32  -4.830989E-04   5.787135E-05   1.363309E-04  -9.444227E-04  -6.629189E-04  -1.654318E-03  -3.095337E-02    1.95498    
  a    33  -3.305192E-03   3.746796E-04   1.100402E-03  -6.722340E-03  -2.954245E-04  -1.764020E-03   1.219015E-02  -0.133603    
  a    34   2.597458E-04  -3.842248E-05   2.333566E-04  -2.050971E-05  -9.731359E-03   1.775291E-02  -3.971198E-02  -1.889924E-02
  a    35   7.840258E-05  -5.597941E-04   8.423421E-05  -1.088535E-03  -2.158668E-02   2.629063E-02  -2.960390E-02   5.212747E-02
  a    36  -4.556495E-05   4.513231E-04  -2.219021E-03   2.088801E-04   9.732046E-03   5.849237E-03   9.475647E-03   1.307407E-02
  a    37   2.021104E-03   1.425714E-02   2.330364E-03  -6.404645E-03  -3.296687E-04   3.001379E-04   5.427610E-05  -7.613483E-04
  a    38  -1.346323E-03  -1.605438E-02  -3.443010E-03   6.439465E-03   1.434413E-04  -5.436260E-04  -2.832576E-04   6.075844E-05
  a    39   8.970039E-05  -1.134689E-02  -8.187098E-04   3.036322E-03   1.977205E-05   1.639553E-04   8.654296E-05   2.822534E-04
  a    40  -5.261071E-04   6.579143E-03   4.611166E-04   7.066874E-04   5.595265E-04  -3.735527E-04   4.537886E-04   9.400512E-04
  a    41   2.323122E-03   3.106068E-03   4.815409E-04  -1.212806E-03  -4.200507E-04   1.130513E-04  -3.763489E-04  -6.263403E-04
  a    42   4.892379E-04   2.595747E-03  -2.646357E-04   8.335863E-04   1.667491E-04  -3.174184E-04   4.053730E-04   5.715850E-04
  a    43   4.721333E-03  -4.213087E-03  -5.312827E-04  -5.192760E-04   7.196645E-04   7.263026E-05   1.255963E-03   1.164609E-03
  a    44   7.739076E-05   1.700135E-03  -2.047280E-04   4.571395E-03   2.089743E-04   3.147047E-05   4.931815E-04   8.649152E-04
  a    45   2.385758E-03  -3.037360E-03  -3.875102E-04  -1.593773E-03  -7.508311E-04  -1.455592E-04  -9.808217E-04  -5.917847E-04
  a    46   6.976632E-04  -1.061693E-03  -1.453036E-03   1.522749E-03   1.590145E-05  -1.330115E-04   1.488671E-04  -2.609441E-04
  a    47  -1.738948E-03   3.585056E-03   3.223522E-04  -4.538482E-03   5.877096E-04  -5.154538E-04   7.900406E-05  -4.103134E-05
  a    48   3.104920E-04  -1.227158E-03  -1.532488E-03   3.001919E-03   1.394924E-05  -6.275128E-05   1.347572E-04   3.080038E-04
  a    49   1.119934E-03  -4.975669E-03  -1.388239E-03   4.820094E-03   3.631839E-04  -2.410191E-04   1.979594E-04   8.248744E-04
  a    50  -1.463564E-03   3.411675E-03   6.048369E-04  -5.813756E-04   4.589122E-04  -3.804444E-04   2.828400E-04   1.985533E-03
  a    51   2.265609E-04  -1.936411E-04  -2.413381E-04   1.829814E-04  -1.837431E-03   8.883537E-04  -5.963168E-03   5.779763E-03
  a    52   1.302735E-03   1.370095E-02   2.141919E-03  -3.627590E-03   3.945382E-04  -1.004124E-05   1.480378E-04   3.652986E-04
  a    53   1.959876E-03  -1.975964E-03  -5.142148E-04   8.699555E-04   8.959456E-04   5.538733E-05   1.165064E-03   1.991251E-03
  a    54  -4.904444E-05   3.546295E-04  -1.176271E-04  -1.124119E-04   1.902493E-03   5.993217E-03  -1.838488E-03  -1.283727E-04
  a    55   1.560315E-03  -5.636533E-03  -8.926163E-04   1.313043E-03  -9.233506E-05   2.394714E-04  -1.625312E-04  -2.338037E-04
  a    56   3.469403E-03   3.393556E-03  -2.563150E-04   2.841648E-04   2.285988E-04  -2.201479E-04   3.260218E-05  -1.642616E-05
  a    57   2.406408E-03   1.863261E-03   2.721332E-05  -8.495889E-04  -4.235718E-04   1.405358E-05  -3.754061E-04  -2.030811E-04
  a    58  -1.615671E-04   9.544094E-05  -3.473957E-04   1.198418E-04  -1.116304E-03   1.561433E-03  -8.677924E-03  -2.147810E-03
  a    59  -9.476163E-05   1.615618E-04  -4.200195E-05   2.333997E-04  -2.173260E-03   6.601840E-03   4.118457E-03   1.058672E-03
  a    60  -1.010081E-03   3.077272E-03   3.855357E-04   2.194350E-03   1.218863E-04  -5.325358E-04   6.477308E-06  -3.846480E-06
  a    61   5.658704E-04  -2.803280E-04   6.213211E-05  -2.472758E-03   9.958964E-05   2.425351E-04  -6.724728E-04   5.666039E-05
  a    62   3.699487E-05  -9.530584E-05   5.027163E-04  -8.273041E-04  -1.699349E-04  -8.683881E-04   3.580984E-03  -1.057849E-04
  a    63   2.209040E-04   5.669145E-05  -2.868689E-04   3.780566E-03  -1.028989E-04  -2.965491E-04   3.347116E-04  -1.094593E-04
  a    64  -3.169424E-04   3.334195E-04  -4.818174E-04  -9.636335E-04   1.727984E-05  -2.005552E-06   1.147796E-04  -2.513916E-04
  a    65   1.149443E-03  -1.557976E-03  -5.081709E-04   8.901646E-04   4.481416E-05  -9.802981E-05  -8.418108E-05  -7.392453E-05
  a    66   3.372544E-04   2.659616E-03   4.058709E-04   5.065167E-04  -3.881326E-05   1.312854E-04   9.871016E-05   4.047150E-05
  a    67  -8.822039E-04   1.154299E-03   2.711282E-04   4.944968E-04  -3.862615E-05   3.218539E-04   2.413046E-05  -1.754263E-05
  a    68   7.762301E-04   7.708706E-05   1.735443E-04   6.862806E-04  -9.406751E-04   2.200075E-03  -1.936019E-03  -8.695319E-04
  a    69  -4.598848E-04  -7.986802E-05   7.654839E-05  -3.458691E-04  -1.516690E-03   2.309348E-03  -2.261449E-03  -6.639883E-04
  a    70  -5.059399E-05   5.141029E-05   8.705210E-04   1.291909E-04   4.733985E-04   7.269875E-04  -2.021824E-04  -6.756155E-04
  a    71  -9.583766E-04  -2.262426E-04  -3.238223E-04   6.427969E-04  -1.451885E-04  -1.274826E-04   9.375717E-05   1.553432E-04
  a    72   2.579823E-04  -4.078289E-04  -6.278843E-05   5.192244E-04   1.637734E-04  -1.644613E-04  -6.528806E-05  -5.003116E-06
  a    73  -3.981011E-05   3.219537E-05  -2.761699E-04   1.982622E-04   2.909547E-03   1.150262E-03  -1.502975E-03  -2.418986E-03
  a    74  -7.607185E-04  -1.274883E-03  -6.016365E-05  -8.198535E-04   3.940657E-05   5.177209E-05  -1.614117E-04  -2.475604E-04
  a    75  -3.892437E-04   1.037654E-03   1.384509E-04  -8.819083E-04   5.095986E-05   3.458276E-05  -6.920694E-06  -2.932085E-05
  a    76  -3.265222E-04  -1.505572E-05   1.777862E-04  -1.128111E-03   1.470271E-05   3.283706E-05  -3.267127E-05   1.032501E-05
  a    77   3.869265E-04   7.458739E-04   1.409391E-05  -4.691841E-04  -1.457309E-05   7.815575E-05   4.064191E-05  -2.047219E-05
  a    78   4.429380E-04   8.535779E-04  -1.286616E-05   7.445059E-04  -3.266903E-05  -2.610285E-05   7.805664E-05   1.668790E-04
  a    79  -1.623350E-04  -3.613109E-04   2.365354E-05   6.868336E-04  -1.009973E-04   1.063837E-04   7.469171E-05   3.825722E-05
  a    80   7.109492E-05   1.008763E-03  -7.408311E-05   1.607118E-03  -7.987779E-05   7.485964E-05   4.771330E-05  -1.055148E-05
  a    81  -4.644320E-04  -6.887676E-04  -1.616251E-04  -1.328848E-04   8.226116E-07  -6.494656E-05  -8.212963E-05  -1.764291E-05
  a    82  -2.859741E-04   1.101439E-03   1.887425E-04   1.600320E-04   9.571485E-05  -1.269051E-05   1.637708E-05   1.338206E-04
  a    83  -4.729847E-04  -7.414144E-04  -2.816381E-04  -8.932137E-05  -3.054349E-05  -1.006136E-04  -2.353202E-05  -4.583266E-05
  a    84  -4.089851E-05   1.899872E-04   1.691464E-05   5.674294E-04   7.930515E-05   2.701150E-05  -2.649634E-05  -1.285865E-05
  a    85   3.460371E-04  -1.660520E-04   8.136553E-05  -6.180318E-06  -9.824581E-05   6.815978E-05  -2.838134E-05  -1.612871E-05
  a    86  -1.070345E-04   1.585926E-04  -1.399561E-04   2.560418E-04  -3.332639E-05   3.559942E-05   6.284653E-05   4.347615E-05

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a    13  -9.270560E-04  -2.882113E-05   3.116407E-04  -6.498832E-05  -2.105110E-03   1.064611E-04   2.046028E-04   1.730499E-03
  a    14  -1.066519E-03   2.633641E-04  -3.321294E-05   9.877322E-05  -1.929922E-03  -7.255903E-04  -9.766317E-04   2.370865E-03
  a    15  -1.422161E-03  -1.364956E-04  -6.287180E-05  -3.686383E-05  -3.909516E-04  -2.919873E-03   3.546632E-04   3.670930E-03
  a    16   1.004758E-03   6.462560E-05  -1.988395E-04   5.870090E-06   4.728326E-03  -3.148651E-03  -1.970957E-03   1.843449E-03
  a    17   5.322227E-04  -1.721924E-04  -1.892726E-05  -4.330119E-05  -1.358811E-03   6.407990E-03   3.980082E-03  -5.177788E-03
  a    18   5.002963E-03  -8.044928E-04  -5.747510E-04   2.431782E-04   2.751045E-03   1.001824E-03   2.338973E-03  -3.619218E-03
  a    19  -1.352449E-03   5.746525E-05   4.119467E-04  -8.269067E-05  -5.669913E-03   3.146035E-03   7.844234E-04   3.563810E-03
  a    20  -9.987911E-04   2.732713E-05   3.187724E-05  -1.052116E-04   9.121510E-03  -6.474406E-04   4.091325E-04  -5.498924E-03
  a    21  -4.568650E-03   8.967566E-04   1.026246E-03  -2.989170E-04   1.365205E-03   1.465754E-03   1.858057E-03   2.130708E-04
  a    22  -1.230006E-03  -1.957521E-04   1.543207E-04  -4.630488E-05  -2.001983E-03  -3.879062E-03   9.090068E-04   4.719113E-03
  a    23  -4.628965E-03   7.536655E-04   6.001474E-04  -2.819715E-04   7.513977E-04  -8.101330E-04   8.909286E-04  -2.508360E-04
  a    24  -5.203309E-04   1.877497E-04   3.527813E-04  -2.224926E-04   9.531984E-03  -6.735513E-03  -3.024847E-03  -2.577815E-03
  a    25  -3.305192E-03   2.597458E-04   7.840258E-05  -4.556495E-05   2.021104E-03  -1.346323E-03   8.970039E-05  -5.261071E-04
  a    26   3.746796E-04  -3.842248E-05  -5.597941E-04   4.513231E-04   1.425714E-02  -1.605438E-02  -1.134689E-02   6.579143E-03
  a    27   1.100402E-03   2.333566E-04   8.423421E-05  -2.219021E-03   2.330364E-03  -3.443010E-03  -8.187098E-04   4.611166E-04
  a    28  -6.722340E-03  -2.050971E-05  -1.088535E-03   2.088801E-04  -6.404645E-03   6.439465E-03   3.036322E-03   7.066874E-04
  a    29  -2.954245E-04  -9.731359E-03  -2.158668E-02   9.732046E-03  -3.296687E-04   1.434413E-04   1.977205E-05   5.595265E-04
  a    30  -1.764020E-03   1.775291E-02   2.629063E-02   5.849237E-03   3.001379E-04  -5.436260E-04   1.639553E-04  -3.735527E-04
  a    31   1.219015E-02  -3.971198E-02  -2.960390E-02   9.475647E-03   5.427610E-05  -2.832576E-04   8.654296E-05   4.537886E-04
  a    32  -0.133603      -1.889924E-02   5.212747E-02   1.307407E-02  -7.613483E-04   6.075844E-05   2.822534E-04   9.400512E-04
  a    33    1.01912      -6.355813E-04   1.476300E-03  -1.542336E-03  -4.038253E-03   1.411764E-03   7.167855E-04   5.310294E-03
  a    34  -6.355813E-04   0.969266       0.159926      -2.435412E-02   1.714543E-04   3.017242E-03  -2.736557E-03   1.217752E-03
  a    35   1.476300E-03   0.159926       0.125398      -1.052075E-02   4.444337E-06   3.186844E-04  -3.716055E-04   1.776009E-04
  a    36  -1.542336E-03  -2.435412E-02  -1.052075E-02   2.939428E-02   3.560002E-05   1.077820E-04  -1.204179E-04  -2.904079E-05
  a    37  -4.038253E-03   1.714543E-04   4.444337E-06   3.560002E-05   2.249496E-03  -1.948463E-03  -9.785666E-04   7.247079E-04
  a    38   1.411764E-03   3.017242E-03   3.186844E-04   1.077820E-04  -1.948463E-03   3.003388E-03   1.489355E-03  -1.182281E-03
  a    39   7.167855E-04  -2.736557E-03  -3.716055E-04  -1.204179E-04  -9.785666E-04   1.489355E-03   1.266676E-03  -5.266489E-04
  a    40   5.310294E-03   1.217752E-03   1.776009E-04  -2.904079E-05   7.247079E-04  -1.182281E-03  -5.266489E-04   1.498191E-03
  a    41  -2.457593E-03   7.646072E-05  -1.556843E-04   4.777661E-05   1.634260E-04  -4.005026E-04  -3.217202E-04  -1.818615E-04
  a    42   2.731584E-03   2.634507E-03   7.543598E-05   8.144481E-05   3.047905E-04  -1.927930E-04  -1.028456E-04   1.931044E-04
  a    43   5.098067E-03   8.119386E-04   6.779579E-05  -1.395134E-05  -4.934810E-05   3.834392E-04  -6.033970E-06   3.193579E-04
  a    44   2.867108E-03   2.498781E-05  -3.978768E-05   3.046525E-05  -1.026303E-04   9.049571E-04   4.649768E-04  -5.847700E-04
  a    45  -3.466788E-03  -6.475433E-04  -1.363827E-04   3.171211E-05   1.142353E-04  -4.810877E-04   1.790035E-04   4.433929E-04
  a    46   1.186871E-03   9.444903E-04   1.806635E-04  -3.426502E-05   2.638198E-05  -5.715577E-04  -1.251094E-04   8.711180E-04
  a    47   5.895100E-04   7.991977E-04   1.706944E-04  -1.241118E-05   3.660889E-04  -4.539584E-04  -1.111928E-05   1.506820E-04
  a    48   5.352753E-04   7.001892E-04   9.907998E-05   5.789977E-05  -1.558227E-04   2.692782E-04   2.750624E-04  -2.485669E-04
  a    49   6.404365E-03  -8.493058E-05   3.519595E-05   1.400967E-05  -4.360882E-04   6.040775E-04   1.506642E-04  -8.841818E-05
  a    50   1.200658E-02  -4.065720E-04  -7.144379E-06  -1.019102E-05   3.222259E-04  -6.087258E-04  -3.478205E-04   6.300131E-04
  a    51   9.671082E-04  -6.275652E-03  -1.231648E-03   2.682167E-04  -3.249207E-05   2.275240E-05   4.579650E-05  -4.099067E-05
  a    52   2.821833E-03  -4.897962E-05  -1.104251E-05  -5.356844E-06   9.177513E-04  -1.019018E-03  -6.461108E-04   3.494961E-04
  a    53   1.466024E-02   9.384915E-04   4.810035E-05   1.787840E-05  -4.818886E-04   5.756905E-04   2.536255E-04  -2.195033E-04
  a    54   5.641937E-05   2.547966E-03   5.469170E-04   9.509987E-04   1.442020E-05   2.296568E-05  -3.992156E-05  -2.092409E-06
  a    55  -2.397322E-03  -1.456163E-04   4.312373E-05   3.733550E-05  -6.244214E-05  -4.284801E-05   6.713899E-05   3.529139E-04
  a    56   6.096081E-04  -4.426444E-06   3.398327E-06   3.352610E-06   2.318236E-04  -1.489010E-04  -1.846790E-04  -2.401670E-05
  a    57  -1.405007E-03  -8.656338E-05  -1.123877E-04   2.364298E-05   2.517828E-04   4.546455E-05  -1.856698E-04  -7.270063E-05
  a    58   1.899449E-04  -2.891240E-03  -3.394725E-03   7.455533E-04   3.469899E-06  -1.216180E-05   4.458282E-05  -1.228448E-05
  a    59  -2.537456E-04   1.450782E-03   1.321317E-04   3.527375E-04   2.548570E-05  -1.482918E-05   4.459585E-05  -2.898853E-05
  a    60  -4.387305E-04  -2.053653E-04   9.052405E-05  -6.194474E-05   2.200342E-04   2.298663E-04   2.257980E-04  -2.289961E-04
  a    61   5.834042E-04   5.440468E-04  -2.621848E-04   4.616381E-05  -1.381898E-04   7.607273E-05  -9.218239E-05  -8.532120E-05
  a    62  -4.246851E-05  -4.822601E-03   1.421871E-03   1.842479E-05  -4.923253E-05  -7.189961E-05   8.553247E-05  -3.978679E-05
  a    63  -8.003388E-04  -7.390822E-04   1.935026E-04   1.602016E-05   1.618301E-04   2.403560E-05  -4.431237E-05  -3.583319E-05
  a    64   1.383385E-04  -4.406253E-04   2.038081E-04   7.217915E-05  -2.772164E-05  -4.405206E-05  -2.841331E-05   8.262864E-05
  a    65  -1.680463E-04  -5.088349E-05   3.015971E-05   1.279301E-05   2.398906E-05   9.132887E-05   7.352452E-05  -2.702774E-05
  a    66   4.952604E-04   2.695558E-05  -2.564087E-06  -1.087083E-05   4.934791E-05  -2.996004E-05  -2.608735E-05   1.274782E-04
  a    67   5.204980E-04  -1.324322E-05   3.521054E-05   6.069337E-05  -6.998231E-05  -6.564850E-05  -6.461727E-05  -1.129936E-04
  a    68   1.284165E-04  -1.840942E-03   6.196465E-04   4.533436E-04   3.539907E-05   3.058876E-05   2.017624E-05  -4.772712E-06
  a    69   3.052956E-04  -2.037229E-03   6.793850E-04   4.097168E-04  -5.600078E-06  -1.846076E-05  -3.841252E-05  -1.749361E-05
  a    70  -1.618479E-04   4.075917E-05   1.033303E-04   2.735873E-04   2.281143E-05   4.095824E-05  -4.215124E-05  -8.991598E-06
  a    71   7.300547E-04   4.257405E-04  -2.380115E-05  -2.170921E-05  -2.005509E-05   1.522371E-06   5.264183E-05   6.001787E-05
  a    72   9.549555E-04   9.774040E-05  -4.785301E-06  -2.024815E-05  -8.910276E-05   1.218541E-04   1.612587E-04  -4.827555E-07
  a    73  -2.587445E-04  -3.890203E-03   6.063129E-04   2.612157E-04  -2.881718E-06  -7.354951E-06   7.360414E-06  -9.677154E-06
  a    74   4.170907E-04  -2.442397E-04   7.780519E-05   4.759950E-05  -8.976547E-05  -4.319113E-05  -4.781756E-05   1.466056E-04
  a    75  -1.046195E-04   9.485360E-05  -3.130229E-06  -7.461839E-07  -4.091873E-05  -7.199908E-05  -1.098954E-04   5.140349E-05
  a    76  -6.799472E-04   3.761260E-05  -3.262116E-05  -2.921733E-06  -6.673534E-05   5.177797E-05  -3.850602E-05  -3.696408E-05
  a    77   6.081028E-04   6.765493E-05   2.082519E-05  -1.505982E-06   1.356990E-04  -1.419999E-04   3.256175E-05   1.238741E-04
  a    78  -4.275505E-04   1.138290E-04  -3.780537E-05  -2.009526E-05   1.843274E-07   3.411937E-05  -3.837327E-05  -1.085448E-04
  a    79   2.404868E-04   1.131281E-04  -4.903947E-05  -2.095386E-05   4.490355E-05  -5.265304E-05   2.258380E-05   4.199896E-05
  a    80  -2.466640E-04  -4.318887E-05   9.892761E-06   3.485767E-05   9.224378E-05  -6.754665E-05  -2.891243E-05  -5.253808E-05
  a    81   2.570749E-04  -3.565617E-05   1.450537E-06  -5.233418E-06  -6.841192E-05  -3.408450E-05  -1.557090E-05   8.015794E-05
  a    82  -8.462014E-04  -7.183025E-05  -2.413178E-05   8.956247E-06  -8.379149E-06   3.711232E-05   6.722233E-05  -1.537483E-05
  a    83   4.543587E-04   4.372549E-05   2.793907E-05   1.238402E-07  -3.963469E-05   5.939301E-05   6.232642E-05   1.667732E-05
  a    84  -4.000038E-04   3.452325E-05  -6.382240E-06   2.155989E-05  -2.778524E-05   7.017495E-05  -2.586354E-06  -2.497835E-05
  a    85   3.038031E-04   5.922832E-05   1.064294E-05   8.616219E-06   3.638017E-05  -3.077109E-05   8.156855E-06   2.051994E-05
  a    86  -3.641650E-04  -2.408007E-05  -1.232264E-05   5.691991E-06   3.300587E-05   2.939839E-06  -2.466282E-05   3.966649E-05

               a    41        a    42        a    43        a    44        a    45        a    46        a    47        a    48
  a    13  -1.723716E-03   1.265586E-03   3.399110E-03   1.972814E-03  -4.699464E-04  -1.879559E-03   1.162591E-03   1.070402E-03
  a    14  -1.931680E-03   7.366788E-04   1.211222E-03  -7.298319E-04   3.802955E-04  -9.479984E-04   1.010308E-03   1.229127E-03
  a    15  -8.933779E-04   6.172747E-04   1.355107E-03  -1.358692E-03   1.691411E-03   8.988403E-04   8.388678E-04  -2.728181E-05
  a    16  -9.297491E-04   3.177604E-04   1.104086E-03  -5.303010E-04   2.581460E-03   5.040389E-04   2.586389E-03  -9.239352E-04
  a    17   7.484935E-04  -2.323532E-03  -6.040313E-04   1.726939E-03   9.465817E-04   2.343219E-03  -5.380953E-03   2.392524E-03
  a    18  -1.163261E-03   9.358987E-04  -1.733461E-03  -2.954129E-04   1.320454E-03  -4.510026E-03  -1.272587E-03   2.280559E-03
  a    19  -1.499889E-03   3.827800E-04   5.787778E-04   2.171960E-04   9.168597E-04   2.470787E-03  -6.180897E-03   3.484769E-03
  a    20   6.577895E-04  -1.738238E-05   7.797208E-03  -1.265446E-03   3.985431E-03   2.763965E-03  -6.717665E-04   2.922771E-03
  a    21   3.708297E-03   2.572720E-03  -1.340182E-03  -8.373861E-04  -3.681897E-03   7.258156E-03   1.115112E-03  -2.738516E-03
  a    22   7.587458E-04  -5.826726E-04   6.647151E-04   9.095613E-04  -8.327494E-04   1.111845E-03  -1.349375E-03   3.108156E-03
  a    23  -5.339204E-04  -3.243051E-03   1.631949E-04   1.180277E-03  -2.576764E-03   5.882031E-03  -3.815954E-04  -1.895824E-03
  a    24  -9.441608E-04   1.878571E-03   3.011170E-03  -2.557778E-03  -7.283083E-04   8.896951E-04   4.447626E-04   4.184813E-04
  a    25   2.323122E-03   4.892379E-04   4.721333E-03   7.739076E-05   2.385758E-03   6.976632E-04  -1.738948E-03   3.104920E-04
  a    26   3.106068E-03   2.595747E-03  -4.213087E-03   1.700135E-03  -3.037360E-03  -1.061693E-03   3.585056E-03  -1.227158E-03
  a    27   4.815409E-04  -2.646357E-04  -5.312827E-04  -2.047280E-04  -3.875102E-04  -1.453036E-03   3.223522E-04  -1.532488E-03
  a    28  -1.212806E-03   8.335863E-04  -5.192760E-04   4.571395E-03  -1.593773E-03   1.522749E-03  -4.538482E-03   3.001919E-03
  a    29  -4.200507E-04   1.667491E-04   7.196645E-04   2.089743E-04  -7.508311E-04   1.590145E-05   5.877096E-04   1.394924E-05
  a    30   1.130513E-04  -3.174184E-04   7.263026E-05   3.147047E-05  -1.455592E-04  -1.330115E-04  -5.154538E-04  -6.275128E-05
  a    31  -3.763489E-04   4.053730E-04   1.255963E-03   4.931815E-04  -9.808217E-04   1.488671E-04   7.900406E-05   1.347572E-04
  a    32  -6.263403E-04   5.715850E-04   1.164609E-03   8.649152E-04  -5.917847E-04  -2.609441E-04  -4.103134E-05   3.080038E-04
  a    33  -2.457593E-03   2.731584E-03   5.098067E-03   2.867108E-03  -3.466788E-03   1.186871E-03   5.895100E-04   5.352753E-04
  a    34   7.646072E-05   2.634507E-03   8.119386E-04   2.498781E-05  -6.475433E-04   9.444903E-04   7.991977E-04   7.001892E-04
  a    35  -1.556843E-04   7.543598E-05   6.779579E-05  -3.978768E-05  -1.363827E-04   1.806635E-04   1.706944E-04   9.907998E-05
  a    36   4.777661E-05   8.144481E-05  -1.395134E-05   3.046525E-05   3.171211E-05  -3.426502E-05  -1.241118E-05   5.789977E-05
  a    37   1.634260E-04   3.047905E-04  -4.934810E-05  -1.026303E-04   1.142353E-04   2.638198E-05   3.660889E-04  -1.558227E-04
  a    38  -4.005026E-04  -1.927930E-04   3.834392E-04   9.049571E-04  -4.810877E-04  -5.715577E-04  -4.539584E-04   2.692782E-04
  a    39  -3.217202E-04  -1.028456E-04  -6.033970E-06   4.649768E-04   1.790035E-04  -1.251094E-04  -1.111928E-05   2.750624E-04
  a    40  -1.818615E-04   1.931044E-04   3.193579E-04  -5.847700E-04   4.433929E-04   8.711180E-04   1.506820E-04  -2.485669E-04
  a    41   8.541421E-04  -2.989618E-04  -3.522724E-04   7.518186E-06   1.675547E-04   1.964420E-04  -2.166341E-05  -8.932213E-05
  a    42  -2.989618E-04   5.409585E-04   2.566181E-04   3.175734E-04  -4.411476E-04  -6.496007E-05   2.433267E-04  -1.512304E-05
  a    43  -3.522724E-04   2.566181E-04   2.017703E-03   5.532765E-04  -6.046339E-04  -3.241661E-05  -2.922247E-05   4.704034E-05
  a    44   7.518186E-06   3.175734E-04   5.532765E-04   2.675507E-03  -4.222611E-04  -1.022971E-03   3.334721E-04   1.689973E-04
  a    45   1.675547E-04  -4.411476E-04  -6.046339E-04  -4.222611E-04   2.012601E-03   6.893181E-04  -8.049917E-05  -1.819097E-04
  a    46   1.964420E-04  -6.496007E-05  -3.241661E-05  -1.022971E-03   6.893181E-04   2.297164E-03  -2.627971E-04  -5.137978E-04
  a    47  -2.166341E-05   2.433267E-04  -2.922247E-05   3.334721E-04  -8.049917E-05  -2.627971E-04   9.644359E-04   1.741049E-04
  a    48  -8.932213E-05  -1.512304E-05   4.704034E-05   1.689973E-04  -1.819097E-04  -5.137978E-04   1.741049E-04   6.647522E-04
  a    49   1.514191E-04  -1.450561E-04   4.285255E-04   1.172612E-04  -1.156556E-04   9.498499E-05  -3.909322E-04  -9.309783E-05
  a    50  -1.635024E-04   3.830051E-04   2.723153E-04  -1.700827E-04  -1.486691E-04   5.762997E-04   2.263090E-04  -1.086038E-04
  a    51   1.191848E-05  -3.218975E-05  -1.639106E-05   1.165501E-05   8.007955E-06  -3.589008E-05  -1.915831E-05   2.893200E-06
  a    52   2.225474E-04   1.444107E-05   1.061155E-04  -3.154563E-06  -2.328048E-05  -2.922701E-04   1.783124E-04  -6.459466E-05
  a    53  -2.902909E-04   6.991805E-05   4.253896E-04   2.275250E-04  -7.095901E-04  -4.777828E-04   1.174565E-05   2.553546E-04
  a    54   1.398610E-06   1.994456E-05  -1.421914E-05   2.870702E-05  -4.082054E-05  -5.293291E-06   2.526428E-05   2.246667E-05
  a    55  -1.474713E-04  -5.655634E-05   3.325724E-04  -2.309559E-04   5.272465E-04   3.183388E-04  -5.854412E-05   3.921015E-05
  a    56   2.713422E-04  -4.321569E-05  -1.192021E-04   3.762651E-04   1.255418E-04   2.329406E-05  -2.476390E-04  -2.379686E-04
  a    57  -5.728817E-05   1.294533E-04   6.224285E-04   2.738276E-04  -6.453950E-04  -6.422629E-04  -2.207164E-06   5.349303E-05
  a    58   1.299224E-05   7.512951E-06  -1.579216E-05   5.142964E-05   7.228114E-06   4.793169E-06   2.777393E-05   5.674158E-06
  a    59   1.650388E-05   9.544739E-06   2.988691E-05   8.453234E-05  -2.003296E-05  -4.177029E-05   2.421543E-06  -1.687609E-05
  a    60  -1.782827E-05   1.849205E-04   2.622981E-04   1.096031E-03  -2.664503E-04  -4.143705E-04   3.573705E-04   2.093598E-05
  a    61  -1.221201E-04  -1.399138E-04  -1.182702E-04  -4.313548E-04  -1.219895E-05  -2.385276E-04  -2.606256E-04   1.184610E-05
  a    62  -2.869021E-05  -7.408431E-05  -5.610811E-05  -9.821484E-05  -1.632303E-05  -2.506620E-05  -3.412130E-05  -8.362046E-06
  a    63   9.847848E-05   7.393526E-06   2.459825E-04   2.094512E-04  -1.509815E-04  -3.918364E-04  -2.832610E-05   8.470788E-05
  a    64  -2.910661E-05   6.158226E-06  -2.884898E-05  -2.476707E-05   1.006954E-04   2.273624E-04  -6.607068E-06  -1.047995E-04
  a    65  -6.269205E-05  -1.214497E-05   1.272840E-05  -1.760971E-04   5.735219E-05  -1.313029E-04  -1.822738E-04   3.470899E-05
  a    66  -1.801361E-04   9.039606E-05  -3.736392E-05   3.297166E-05  -1.326073E-04  -8.283953E-05  -5.252055E-05  -4.352964E-05
  a    67   1.017363E-04   1.365943E-06  -7.560162E-06   1.403856E-04  -1.220338E-04  -1.295500E-04   1.076867E-04   5.661950E-05
  a    68  -7.854975E-06   8.835060E-06   2.331319E-05   7.048289E-05   3.207317E-05  -1.336763E-05   1.234677E-05   4.997171E-06
  a    69  -2.412793E-06  -2.560770E-05  -3.281407E-05  -5.291700E-05  -8.297201E-06  -1.001748E-05  -1.937962E-05  -2.263838E-06
  a    70  -1.557182E-05   5.894274E-06   1.300228E-05   1.032711E-05  -4.692548E-06  -5.937866E-05   1.920528E-07   1.096863E-05
  a    71   4.180301E-05  -3.438939E-05  -7.157155E-05   2.841229E-05   9.082642E-05  -6.981779E-05   6.851650E-05   5.565680E-05
  a    72   1.387385E-04  -8.011559E-05  -3.756106E-05   1.859496E-04   2.024764E-04   5.872237E-05   4.245891E-05  -2.102832E-05
  a    73  -2.501423E-06  -3.099251E-05  -1.640320E-05   8.053765E-06   2.075145E-06  -1.825201E-05   1.308617E-05   1.610467E-05
  a    74   3.890419E-05  -2.047377E-05  -8.043637E-05  -1.967099E-04   1.423329E-04   3.891756E-04  -1.026756E-04  -1.092967E-04
  a    75   3.024132E-05  -1.404784E-05  -5.046649E-05  -1.995825E-04  -5.028964E-05   2.255611E-04  -1.054608E-04  -6.927795E-05
  a    76  -9.243590E-05   5.226072E-05  -3.486882E-05  -5.742109E-05  -1.753354E-04   3.417496E-05  -2.729044E-05  -2.255345E-05
  a    77   2.706404E-05  -1.823316E-05  -6.232918E-05  -9.238353E-05   1.870452E-04   9.829336E-05   8.067651E-05   2.926975E-05
  a    78   2.850210E-05  -1.667956E-05   6.001634E-05  -1.436962E-05  -1.761625E-04  -2.393796E-04  -3.222820E-05   6.439369E-05
  a    79   2.371709E-05   3.988506E-05   3.836884E-06   1.264133E-04   1.075693E-04   1.009338E-04   5.212035E-05  -2.739529E-05
  a    80   1.207583E-05   2.016140E-05   2.296459E-05   1.264018E-04  -7.294619E-05  -1.228920E-04   6.811951E-05   4.932108E-05
  a    81   1.748287E-05  -3.402065E-05  -2.784254E-05  -1.468829E-04   1.071682E-04   1.538553E-04  -3.523198E-05  -7.405846E-06
  a    82  -7.589992E-05   4.250527E-05   2.543769E-05   1.118632E-04  -5.262064E-05  -4.970467E-05   8.547261E-05   3.349090E-05
  a    83  -1.693237E-05   4.125421E-06   5.001147E-06   6.193832E-05   1.000241E-04   8.394102E-05   2.106370E-05  -3.168864E-05
  a    84  -1.409474E-05   2.801171E-05   5.259599E-05   9.312495E-05  -9.541267E-05  -4.054445E-05  -5.565539E-06   3.142478E-05
  a    85   6.704244E-07  -6.713620E-06  -3.000687E-05  -4.802153E-05   4.347849E-05   4.967062E-05   2.344878E-05  -1.059671E-06
  a    86  -1.754256E-05   3.546489E-05   6.851036E-05   6.263530E-05  -4.977186E-05   6.440730E-05  -1.628528E-05  -1.709948E-05

               a    49        a    50        a    51        a    52        a    53        a    54        a    55        a    56
  a    13  -6.292165E-04   9.749855E-04  -1.430672E-05  -5.749226E-04   4.779581E-04  -5.750496E-05   1.698643E-03   7.246163E-05
  a    14  -1.180074E-03   8.138086E-04   4.882096E-05  -5.340581E-04   5.825309E-04  -7.138509E-05   1.498484E-03  -7.605912E-04
  a    15  -4.655811E-04   9.707960E-04   4.058028E-05   4.373728E-04  -2.369295E-04  -7.832816E-05   1.722818E-03  -5.311417E-04
  a    16   7.701344E-04   6.935453E-04  -1.140976E-04   2.257917E-03  -1.544588E-03  -7.338130E-05   1.463469E-03   9.180199E-04
  a    17  -1.060921E-03  -2.207273E-03   1.122934E-04  -4.002509E-03   1.808277E-03  -7.633178E-05   3.321646E-04  -2.612352E-03
  a    18  -1.157190E-03   5.512500E-04  -3.163568E-04  -2.354040E-03  -2.143707E-03  -4.742290E-05   9.901710E-04  -2.585006E-03
  a    19   5.208524E-04  -4.590366E-05   6.539114E-05  -3.095048E-03  -7.075251E-04  -1.664769E-04   2.388741E-03   1.561927E-04
  a    20  -7.097360E-04   2.391852E-03  -1.427667E-04  -2.270084E-03  -5.150469E-04  -2.099195E-04   3.921691E-03  -9.725209E-04
  a    21   2.607440E-04   1.209275E-03   2.804034E-04   6.237139E-05   1.943045E-03   1.325472E-04  -1.998493E-03   2.940034E-03
  a    22   9.576103E-04   5.554473E-04   2.928100E-05   1.472988E-03  -1.038798E-03  -2.350547E-06   1.034487E-03   1.358334E-03
  a    23   9.648770E-04   2.325811E-04   3.151276E-04   7.247934E-04   1.723819E-03   1.723347E-04  -1.611294E-03   3.525401E-03
  a    24  -3.078241E-03   4.669806E-03  -2.444548E-04   1.658019E-03  -1.583097E-03  -1.255923E-05   2.108099E-03   1.768224E-03
  a    25   1.119934E-03  -1.463564E-03   2.265609E-04   1.302735E-03   1.959876E-03  -4.904444E-05   1.560315E-03   3.469403E-03
  a    26  -4.975669E-03   3.411675E-03  -1.936411E-04   1.370095E-02  -1.975964E-03   3.546295E-04  -5.636533E-03   3.393556E-03
  a    27  -1.388239E-03   6.048369E-04  -2.413381E-04   2.141919E-03  -5.142148E-04  -1.176271E-04  -8.926163E-04  -2.563150E-04
  a    28   4.820094E-03  -5.813756E-04   1.829814E-04  -3.627590E-03   8.699555E-04  -1.124119E-04   1.313043E-03   2.841648E-04
  a    29   3.631839E-04   4.589122E-04  -1.837431E-03   3.945382E-04   8.959456E-04   1.902493E-03  -9.233506E-05   2.285988E-04
  a    30  -2.410191E-04  -3.804444E-04   8.883537E-04  -1.004124E-05   5.538733E-05   5.993217E-03   2.394714E-04  -2.201479E-04
  a    31   1.979594E-04   2.828400E-04  -5.963168E-03   1.480378E-04   1.165064E-03  -1.838488E-03  -1.625312E-04   3.260218E-05
  a    32   8.248744E-04   1.985533E-03   5.779763E-03   3.652986E-04   1.991251E-03  -1.283727E-04  -2.338037E-04  -1.642616E-05
  a    33   6.404365E-03   1.200658E-02   9.671082E-04   2.821833E-03   1.466024E-02   5.641937E-05  -2.397322E-03   6.096081E-04
  a    34  -8.493058E-05  -4.065720E-04  -6.275652E-03  -4.897962E-05   9.384915E-04   2.547966E-03  -1.456163E-04  -4.426444E-06
  a    35   3.519595E-05  -7.144379E-06  -1.231648E-03  -1.104251E-05   4.810035E-05   5.469170E-04   4.312373E-05   3.398327E-06
  a    36   1.400967E-05  -1.019102E-05   2.682167E-04  -5.356844E-06   1.787840E-05   9.509987E-04   3.733550E-05   3.352610E-06
  a    37  -4.360882E-04   3.222259E-04  -3.249207E-05   9.177513E-04  -4.818886E-04   1.442020E-05  -6.244214E-05   2.318236E-04
  a    38   6.040775E-04  -6.087258E-04   2.275240E-05  -1.019018E-03   5.756905E-04   2.296568E-05  -4.284801E-05  -1.489010E-04
  a    39   1.506642E-04  -3.478205E-04   4.579650E-05  -6.461108E-04   2.536255E-04  -3.992156E-05   6.713899E-05  -1.846790E-04
  a    40  -8.841818E-05   6.300131E-04  -4.099067E-05   3.494961E-04  -2.195033E-04  -2.092409E-06   3.529139E-04  -2.401670E-05
  a    41   1.514191E-04  -1.635024E-04   1.191848E-05   2.225474E-04  -2.902909E-04   1.398610E-06  -1.474713E-04   2.713422E-04
  a    42  -1.450561E-04   3.830051E-04  -3.218975E-05   1.444107E-05   6.991805E-05   1.994456E-05  -5.655634E-05  -4.321569E-05
  a    43   4.285255E-04   2.723153E-04  -1.639106E-05   1.061155E-04   4.253896E-04  -1.421914E-05   3.325724E-04  -1.192021E-04
  a    44   1.172612E-04  -1.700827E-04   1.165501E-05  -3.154563E-06   2.275250E-04   2.870702E-05  -2.309559E-04   3.762651E-04
  a    45  -1.156556E-04  -1.486691E-04   8.007955E-06  -2.328048E-05  -7.095901E-04  -4.082054E-05   5.272465E-04   1.255418E-04
  a    46   9.498499E-05   5.762997E-04  -3.589008E-05  -2.922701E-04  -4.777828E-04  -5.293291E-06   3.183388E-04   2.329406E-05
  a    47  -3.909322E-04   2.263090E-04  -1.915831E-05   1.783124E-04   1.174565E-05   2.526428E-05  -5.854412E-05  -2.476390E-04
  a    48  -9.309783E-05  -1.086038E-04   2.893200E-06  -6.459466E-05   2.553546E-04   2.246667E-05   3.921015E-05  -2.379686E-04
  a    49   8.604028E-04  -1.149432E-04   1.193802E-05  -4.362296E-05   1.274372E-04   9.314152E-06   1.093194E-04   2.266204E-04
  a    50  -1.149432E-04   9.561518E-04  -1.184569E-05   9.799581E-05   2.237680E-05   4.880369E-06   1.045053E-04  -5.287589E-05
  a    51   1.193802E-05  -1.184569E-05   6.270876E-04  -2.359268E-06   9.973542E-06   1.050573E-04  -2.979213E-06   5.383251E-06
  a    52  -4.362296E-05   9.799581E-05  -2.359268E-06   1.052566E-03  -8.098474E-05   1.382898E-05  -1.390466E-04   2.378665E-04
  a    53   1.274372E-04   2.237680E-05   9.973542E-06  -8.098474E-05   1.028778E-03   2.245191E-05  -2.204937E-04  -1.650941E-04
  a    54   9.314152E-06   4.880369E-06   1.050573E-04   1.382898E-05   2.245191E-05   9.706651E-04   1.032293E-05   1.700813E-05
  a    55   1.093194E-04   1.045053E-04  -2.979213E-06  -1.390466E-04  -2.204937E-04   1.032293E-05   6.614940E-04  -1.225816E-04
  a    56   2.266204E-04  -5.287589E-05   5.383251E-06   2.378665E-04  -1.650941E-04   1.700813E-05  -1.225816E-04   7.305988E-04
  a    57   1.861731E-04  -1.190460E-04   6.747167E-06   3.177010E-04   1.488073E-04   1.733768E-05  -1.665540E-04   3.202571E-05
  a    58  -1.233619E-05   1.333813E-05   1.789108E-04  -6.394270E-06  -9.703343E-06   2.619712E-04   1.683233E-06  -3.608252E-06
  a    59  -1.006573E-05  -1.524918E-05  -1.285284E-04   1.174193E-05  -1.284040E-05  -4.075236E-04  -3.152949E-05   8.179516E-06
  a    60  -1.363539E-04  -9.456766E-05   4.584527E-06   1.409602E-04   6.583250E-05   2.508294E-05  -2.385421E-04  -7.373797E-06
  a    61  -2.910373E-05  -1.605784E-04   2.583300E-05  -8.399979E-06   2.093350E-04   2.758756E-05   4.836931E-05  -3.292712E-05
  a    62  -3.871081E-05  -2.151498E-05  -9.142565E-05  -2.284574E-05   3.490744E-05  -2.122060E-04  -1.944063E-05  -4.094496E-05
  a    63   2.375146E-04  -6.777669E-05  -8.081294E-06   1.908039E-04  -5.602293E-05  -9.230559E-06   5.774241E-05   1.856489E-04
  a    64  -6.659488E-05   7.532811E-05  -1.761933E-05  -4.874698E-05  -5.317221E-05  -6.234950E-06   5.599819E-06  -3.422282E-05
  a    65   2.336304E-04  -1.322906E-04   5.876275E-06   5.819612E-05  -3.871961E-05  -3.515191E-06   8.485051E-05   8.917718E-05
  a    66  -4.448597E-05  -6.811700E-08   4.221498E-07   5.497905E-05   8.491870E-05   7.444270E-06  -8.293342E-05   5.539180E-05
  a    67  -4.069330E-05  -2.710330E-05  -1.498786E-06   9.006803E-05   8.072361E-05   1.145923E-06  -5.238505E-05  -1.883639E-05
  a    68  -4.626100E-06  -1.913725E-05  -1.757260E-05   2.775391E-06  -7.636776E-06  -8.590377E-05  -3.635311E-06   6.832684E-06
  a    69   6.712142E-07  -1.598320E-06  -1.660838E-05  -6.204908E-06  -1.572173E-05  -1.467869E-04  -7.762584E-06  -8.044902E-06
  a    70   1.542403E-06  -2.919487E-05  -1.229995E-05   1.805327E-05   1.478552E-05   4.144927E-05  -5.395633E-06  -4.181759E-06
  a    71   3.628328E-06  -3.637986E-05   2.850081E-06   1.549923E-05  -6.230513E-06  -2.395634E-05   2.841980E-05  -1.837013E-05
  a    72   1.189995E-04  -1.139727E-04   9.406810E-06   2.773449E-05  -7.648608E-05  -6.515140E-06  -7.281863E-07   1.325565E-05
  a    73   1.001538E-05  -1.099987E-05  -5.722747E-06   2.873427E-06  -7.770079E-06   2.908679E-04   1.832422E-05   1.296697E-06
  a    74   5.778371E-05   1.414687E-04  -1.086207E-05  -1.064406E-04  -1.439258E-04   3.498022E-07   9.387887E-05   5.599823E-05
  a    75   3.231309E-05   6.886774E-05  -1.796384E-06  -3.309075E-05  -2.683020E-05   2.920687E-06   5.421560E-06   4.271545E-05
  a    76  -1.020115E-04   7.014375E-05  -3.925931E-06  -1.245376E-04   7.948339E-05   4.246072E-06  -4.088404E-05  -5.020500E-05
  a    77  -2.888613E-05   1.195780E-05  -1.957906E-07   1.066754E-04  -6.519730E-05  -1.784565E-06   2.845104E-05  -1.187125E-05
  a    78   6.195854E-05  -8.546206E-05   5.985754E-06   7.275201E-05   7.343719E-05  -5.283469E-06  -3.823204E-05   3.972975E-06
  a    79   4.383097E-07   4.223580E-05  -1.224197E-06  -1.522179E-05  -7.894136E-05  -1.316035E-06   2.239572E-05   1.560087E-05
  a    80  -3.520898E-05  -4.227133E-05  -1.161294E-07   4.000938E-05   5.069809E-05  -3.844183E-09  -4.391944E-05  -1.918026E-05
  a    81   2.798213E-05   5.230614E-05  -1.615395E-06  -1.813643E-05  -6.957465E-05  -5.086291E-06   6.107406E-05  -1.738336E-05
  a    82  -9.974975E-05  -1.360447E-05   1.751633E-06  -7.296876E-06   7.294122E-05   8.419142E-07  -2.828042E-05  -7.186038E-05
  a    83  -2.958734E-06   3.824273E-05  -2.522756E-06  -3.464613E-05  -5.233630E-05  -3.720978E-06   3.848808E-05   3.807073E-06
  a    84   4.007545E-05   6.504011E-06  -9.265259E-07  -1.252666E-05   2.885118E-07   2.682005E-06  -1.319840E-05   6.461102E-06
  a    85  -1.620865E-05   1.746545E-05  -1.548429E-06   3.251796E-06  -7.588137E-06  -1.504775E-06   1.138909E-05  -4.857379E-06
  a    86   3.852720E-05   4.790278E-05  -3.509647E-06  -4.891263E-06  -3.465793E-05  -1.195595E-07   1.638737E-05   1.252317E-05

               a    57        a    58        a    59        a    60        a    61        a    62        a    63        a    64
  a    13   8.616627E-04  -1.796440E-06   6.004638E-05   3.229999E-04  -2.357255E-04  -2.170820E-04   1.108475E-03  -3.014023E-04
  a    14  -7.294134E-04  -3.754091E-05  -1.507574E-04  -1.710069E-03   6.757025E-04   2.054816E-04  -6.226464E-04  -8.004438E-06
  a    15  -1.842527E-03  -2.933761E-05  -1.157761E-04  -1.904461E-03   5.682825E-04   6.157033E-05   1.020774E-04   8.637584E-05
  a    16   6.924489E-04  -3.906837E-05   9.058432E-05   9.388906E-04  -8.870276E-04  -2.961298E-04   5.948507E-04   9.522401E-05
  a    17  -8.132631E-04   2.331639E-05  -3.487637E-05   8.115465E-04   1.036343E-03   3.907141E-04  -2.042599E-03   3.583956E-04
  a    18   1.898972E-03  -8.598887E-05   1.211097E-04  -4.810704E-04  -3.601388E-04   8.732617E-05   9.807401E-04  -4.862556E-04
  a    19  -1.313864E-03  -1.179418E-04  -1.317978E-04  -2.273253E-03  -3.524193E-04  -4.373541E-04   2.153908E-03  -1.340643E-04
  a    20  -5.692582E-04  -6.903527E-05  -4.316572E-05  -1.413619E-03  -6.470288E-04  -3.263515E-04   1.447110E-03  -4.664323E-04
  a    21  -3.546210E-03   2.180858E-04  -2.002997E-04  -8.980085E-04  -1.791923E-03  -1.712005E-04  -1.035800E-03   4.773939E-04
  a    22   5.080908E-04   1.359988E-04   1.813871E-04   1.095694E-03  -8.613770E-04   5.890009E-05  -6.299104E-04  -2.642598E-04
  a    23  -1.230894E-03   9.287932E-05  -8.514318E-05  -1.147911E-03  -1.090725E-03  -2.348741E-04  -3.991938E-05   2.152856E-04
  a    24  -4.203156E-04  -1.314428E-05   5.456445E-05  -1.407589E-03  -9.100277E-04  -3.712945E-04   1.602211E-03  -2.712837E-04
  a    25   2.406408E-03  -1.615671E-04  -9.476163E-05  -1.010081E-03   5.658704E-04   3.699487E-05   2.209040E-04  -3.169424E-04
  a    26   1.863261E-03   9.544094E-05   1.615618E-04   3.077272E-03  -2.803280E-04  -9.530584E-05   5.669145E-05   3.334195E-04
  a    27   2.721332E-05  -3.473957E-04  -4.200195E-05   3.855357E-04   6.213211E-05   5.027163E-04  -2.868689E-04  -4.818174E-04
  a    28  -8.495889E-04   1.198418E-04   2.333997E-04   2.194350E-03  -2.472758E-03  -8.273041E-04   3.780566E-03  -9.636335E-04
  a    29  -4.235718E-04  -1.116304E-03  -2.173260E-03   1.218863E-04   9.958964E-05  -1.699349E-04  -1.028989E-04   1.727984E-05
  a    30   1.405358E-05   1.561433E-03   6.601840E-03  -5.325358E-04   2.425351E-04  -8.683881E-04  -2.965491E-04  -2.005552E-06
  a    31  -3.754061E-04  -8.677924E-03   4.118457E-03   6.477308E-06  -6.724728E-04   3.580984E-03   3.347116E-04   1.147796E-04
  a    32  -2.030811E-04  -2.147810E-03   1.058672E-03  -3.846480E-06   5.666039E-05  -1.057849E-04  -1.094593E-04  -2.513916E-04
  a    33  -1.405007E-03   1.899449E-04  -2.537456E-04  -4.387305E-04   5.834042E-04  -4.246851E-05  -8.003388E-04   1.383385E-04
  a    34  -8.656338E-05  -2.891240E-03   1.450782E-03  -2.053653E-04   5.440468E-04  -4.822601E-03  -7.390822E-04  -4.406253E-04
  a    35  -1.123877E-04  -3.394725E-03   1.321317E-04   9.052405E-05  -2.621848E-04   1.421871E-03   1.935026E-04   2.038081E-04
  a    36   2.364298E-05   7.455533E-04   3.527375E-04  -6.194474E-05   4.616381E-05   1.842479E-05   1.602016E-05   7.217915E-05
  a    37   2.517828E-04   3.469899E-06   2.548570E-05   2.200342E-04  -1.381898E-04  -4.923253E-05   1.618301E-04  -2.772164E-05
  a    38   4.546455E-05  -1.216180E-05  -1.482918E-05   2.298663E-04   7.607273E-05  -7.189961E-05   2.403560E-05  -4.405206E-05
  a    39  -1.856698E-04   4.458282E-05   4.459585E-05   2.257980E-04  -9.218239E-05   8.553247E-05  -4.431237E-05  -2.841331E-05
  a    40  -7.270063E-05  -1.228448E-05  -2.898853E-05  -2.289961E-04  -8.532120E-05  -3.978679E-05  -3.583319E-05   8.262864E-05
  a    41  -5.728817E-05   1.299224E-05   1.650388E-05  -1.782827E-05  -1.221201E-04  -2.869021E-05   9.847848E-05  -2.910661E-05
  a    42   1.294533E-04   7.512951E-06   9.544739E-06   1.849205E-04  -1.399138E-04  -7.408431E-05   7.393526E-06   6.158226E-06
  a    43   6.224285E-04  -1.579216E-05   2.988691E-05   2.622981E-04  -1.182702E-04  -5.610811E-05   2.459825E-04  -2.884898E-05
  a    44   2.738276E-04   5.142964E-05   8.453234E-05   1.096031E-03  -4.313548E-04  -9.821484E-05   2.094512E-04  -2.476707E-05
  a    45  -6.453950E-04   7.228114E-06  -2.003296E-05  -2.664503E-04  -1.219895E-05  -1.632303E-05  -1.509815E-04   1.006954E-04
  a    46  -6.422629E-04   4.793169E-06  -4.177029E-05  -4.143705E-04  -2.385276E-04  -2.506620E-05  -3.918364E-04   2.273624E-04
  a    47  -2.207164E-06   2.777393E-05   2.421543E-06   3.573705E-04  -2.606256E-04  -3.412130E-05  -2.832610E-05  -6.607068E-06
  a    48   5.349303E-05   5.674158E-06  -1.687609E-05   2.093598E-05   1.184610E-05  -8.362046E-06   8.470788E-05  -1.047995E-04
  a    49   1.861731E-04  -1.233619E-05  -1.006573E-05  -1.363539E-04  -2.910373E-05  -3.871081E-05   2.375146E-04  -6.659488E-05
  a    50  -1.190460E-04   1.333813E-05  -1.524918E-05  -9.456766E-05  -1.605784E-04  -2.151498E-05  -6.777669E-05   7.532811E-05
  a    51   6.747167E-06   1.789108E-04  -1.285284E-04   4.584527E-06   2.583300E-05  -9.142565E-05  -8.081294E-06  -1.761933E-05
  a    52   3.177010E-04  -6.394270E-06   1.174193E-05   1.409602E-04  -8.399979E-06  -2.284574E-05   1.908039E-04  -4.874698E-05
  a    53   1.488073E-04  -9.703343E-06  -1.284040E-05   6.583250E-05   2.093350E-04   3.490744E-05  -5.602293E-05  -5.317221E-05
  a    54   1.733768E-05   2.619712E-04  -4.075236E-04   2.508294E-05   2.758756E-05  -2.122060E-04  -9.230559E-06  -6.234950E-06
  a    55  -1.665540E-04   1.683233E-06  -3.152949E-05  -2.385421E-04   4.836931E-05  -1.944063E-05   5.774241E-05   5.599819E-06
  a    56   3.202571E-05  -3.608252E-06   8.179516E-06  -7.373797E-06  -3.292712E-05  -4.094496E-05   1.856489E-04  -3.422282E-05
  a    57   7.656696E-04   5.356355E-06   2.294036E-05   2.830369E-04  -2.570818E-05  -3.891461E-05   2.958562E-04  -1.095457E-04
  a    58   5.356355E-06   6.023282E-04  -1.012694E-04   1.931561E-05   1.300699E-05  -1.724867E-04  -1.625278E-05  -6.077287E-06
  a    59   2.294036E-05  -1.012694E-04   6.513844E-04   3.087047E-05  -5.823326E-05   2.683614E-04   4.071402E-05   2.010827E-05
  a    60   2.830369E-04   1.931561E-05   3.087047E-05   9.094303E-04  -2.439477E-04  -4.795177E-05   4.808548E-05  -3.462737E-06
  a    61  -2.570818E-05   1.300699E-05  -5.823326E-05  -2.439477E-04   4.406318E-04   1.087673E-05  -1.016519E-04  -1.620971E-05
  a    62  -3.891461E-05  -1.724867E-04   2.683614E-04  -4.795177E-05   1.087673E-05   4.674344E-04   2.938016E-06   4.327925E-05
  a    63   2.958562E-04  -1.625278E-05   4.071402E-05   4.808548E-05  -1.016519E-04   2.938016E-06   4.158109E-04  -1.198140E-04
  a    64  -1.095457E-04  -6.077287E-06   2.010827E-05  -3.462737E-06  -1.620971E-05   4.327925E-05  -1.198140E-04   7.291747E-05
  a    65   1.296481E-04  -1.252049E-05  -3.239040E-06  -1.318172E-04   2.895274E-05  -1.337674E-05   1.824453E-04  -7.629850E-05
  a    66   5.031966E-05  -2.079885E-07   1.096286E-06   1.769500E-05   7.537487E-05   1.644989E-05  -3.395343E-05   9.887702E-06
  a    67   2.412653E-05   2.532373E-06   1.602172E-05   1.133868E-04  -8.102586E-06  -4.168585E-06   1.572442E-05  -1.089777E-05
  a    68   1.744131E-05   5.228856E-05   1.743957E-04   4.095140E-05  -2.648904E-05   2.848896E-06   1.082542E-05   1.511005E-05
  a    69   2.583830E-06   6.633493E-05   2.336035E-04  -5.755400E-05   2.373096E-05   1.933610E-05  -5.165535E-06   1.750960E-05
  a    70   2.748516E-05  -4.389064E-05  -3.593808E-05   1.227294E-05   1.483751E-05  -4.635948E-05   5.138734E-06   1.271521E-06
  a    71  -3.261095E-05  -6.279311E-06   5.960731E-06   1.532918E-05  -2.215019E-05  -1.965841E-05   3.778916E-05  -2.338537E-05
  a    72  -6.234923E-06   9.497643E-06   1.113114E-05   1.395242E-04  -1.286493E-04  -2.410404E-05   4.012359E-05  -5.068006E-06
  a    73   3.511932E-06   8.086069E-05  -1.532423E-04   9.508052E-06  -6.194502E-06   6.247223E-05   2.535763E-05   2.094143E-05
  a    74  -1.681337E-04  -2.097569E-06  -3.157428E-06  -1.643912E-04  -6.994130E-06   4.736637E-06  -6.407251E-05   4.565319E-05
  a    75  -7.845088E-05  -3.576285E-06  -1.304465E-05  -1.078330E-04   7.848930E-05   1.251582E-05  -7.465083E-05   2.768575E-05
  a    76  -3.818511E-05  -2.898296E-06  -1.031464E-05  -6.437303E-05   6.193470E-05   1.543473E-05  -9.907078E-05   2.560432E-05
  a    77  -6.020090E-05   1.373584E-06  -2.946409E-06  -6.556894E-07  -4.923773E-05  -1.082194E-05   1.184492E-05   5.426665E-06
  a    78   1.364893E-04  -9.352212E-06   2.410057E-06   2.141136E-05   7.197908E-05  -1.156176E-06   8.225517E-05  -4.858792E-05
  a    79  -5.107623E-05   9.009770E-06   4.745124E-06   4.254018E-05  -8.319540E-05  -1.372456E-05  -1.261708E-05   1.829020E-05
  a    80   4.697743E-05   6.016208E-08   7.497817E-06   9.978317E-05  -5.326213E-06   2.618573E-07   8.742273E-06  -9.236178E-06
  a    81  -7.038538E-05  -1.944101E-06  -6.981819E-06  -9.048922E-05  -2.769769E-06  -1.368552E-07  -2.124003E-05   1.589909E-05
  a    82   2.571565E-06   4.977179E-06   2.155242E-06   9.646356E-05  -1.448223E-05   3.072914E-06  -3.834636E-05   1.002346E-05
  a    83  -4.619262E-05  -9.228309E-08   6.335217E-07   3.134723E-05  -2.467447E-05  -5.722290E-06  -2.631055E-05   1.910416E-05
  a    84   4.245132E-05  -4.765958E-07   1.008557E-06   6.069922E-06  -1.901006E-05  -6.206564E-06   1.153771E-05  -2.167492E-06
  a    85  -2.556347E-05   3.090646E-07   2.223155E-06   9.783234E-07  -6.553545E-07  -8.227492E-07  -4.516258E-06   3.827879E-08
  a    86   9.815945E-06   8.706251E-07   1.966319E-06  -6.567941E-06  -2.248394E-05  -1.518044E-06  -1.143850E-05   1.280170E-05

               a    65        a    66        a    67        a    68        a    69        a    70        a    71        a    72
  a    13   4.479954E-04  -1.448441E-04   2.267973E-04  -3.484679E-05  -1.962895E-04  -4.222646E-05   2.333680E-04   1.079361E-03
  a    14  -2.584719E-05   1.715898E-04   3.457074E-04  -4.120221E-04   2.662634E-04   2.625738E-04   9.784843E-04  -3.817054E-04
  a    15  -3.739651E-04   4.261641E-04   4.874694E-04  -1.790119E-05   2.161149E-05  -1.970846E-04   2.588271E-05  -1.113264E-03
  a    16  -5.409336E-04  -2.733835E-04   9.998066E-05   3.694798E-04  -1.184131E-04  -8.379376E-05  -6.632512E-04  -2.003485E-03
  a    17   6.482588E-05  -8.169531E-04  -3.639096E-04   5.075831E-05   4.235768E-05   6.321563E-05  -7.101741E-05   1.086802E-03
  a    18   1.663653E-04  -6.223190E-04  -8.399055E-04   5.328073E-05   1.539625E-04   1.535342E-04  -4.834478E-04  -2.094780E-03
  a    19   1.167691E-03   7.922129E-04   2.198897E-04   1.691498E-04  -1.099705E-05  -1.943731E-04   7.989102E-04  -1.911922E-04
  a    20  -1.096241E-04  -3.900056E-03  -3.168155E-04   4.224866E-04  -3.966200E-05  -1.448581E-04  -1.584436E-03  -2.465856E-03
  a    21  -3.328281E-04  -2.928820E-05  -1.128541E-03   4.770541E-04  -4.325374E-04  -2.354164E-04   2.475528E-04   2.209781E-03
  a    22  -2.525727E-04   4.861278E-04   1.358923E-03  -1.192116E-04   3.970772E-05  -2.440139E-04   1.152038E-03  -2.198732E-04
  a    23   6.149172E-04   6.867485E-04  -1.186587E-03  -5.175928E-04   3.824610E-04   1.121747E-04  -6.014202E-04   1.711356E-03
  a    24  -2.098211E-04  -1.404871E-03  -9.010791E-04   2.276393E-05   1.428367E-04   3.382788E-06  -8.126514E-04  -1.884565E-03
  a    25   1.149443E-03   3.372544E-04  -8.822039E-04   7.762301E-04  -4.598848E-04  -5.059399E-05  -9.583766E-04   2.579823E-04
  a    26  -1.557976E-03   2.659616E-03   1.154299E-03   7.708706E-05  -7.986802E-05   5.141029E-05  -2.262426E-04  -4.078289E-04
  a    27  -5.081709E-04   4.058709E-04   2.711282E-04   1.735443E-04   7.654839E-05   8.705210E-04  -3.238223E-04  -6.278843E-05
  a    28   8.901646E-04   5.065167E-04   4.944968E-04   6.862806E-04  -3.458691E-04   1.291909E-04   6.427969E-04   5.192244E-04
  a    29   4.481416E-05  -3.881326E-05  -3.862615E-05  -9.406751E-04  -1.516690E-03   4.733985E-04  -1.451885E-04   1.637734E-04
  a    30  -9.802981E-05   1.312854E-04   3.218539E-04   2.200075E-03   2.309348E-03   7.269875E-04  -1.274826E-04  -1.644613E-04
  a    31  -8.418108E-05   9.871016E-05   2.413046E-05  -1.936019E-03  -2.261449E-03  -2.021824E-04   9.375717E-05  -6.528806E-05
  a    32  -7.392453E-05   4.047150E-05  -1.754263E-05  -8.695319E-04  -6.639883E-04  -6.756155E-04   1.553432E-04  -5.003116E-06
  a    33  -1.680463E-04   4.952604E-04   5.204980E-04   1.284165E-04   3.052956E-04  -1.618479E-04   7.300547E-04   9.549555E-04
  a    34  -5.088349E-05   2.695558E-05  -1.324322E-05  -1.840942E-03  -2.037229E-03   4.075917E-05   4.257405E-04   9.774040E-05
  a    35   3.015971E-05  -2.564087E-06   3.521054E-05   6.196465E-04   6.793850E-04   1.033303E-04  -2.380115E-05  -4.785301E-06
  a    36   1.279301E-05  -1.087083E-05   6.069337E-05   4.533436E-04   4.097168E-04   2.735873E-04  -2.170921E-05  -2.024815E-05
  a    37   2.398906E-05   4.934791E-05  -6.998231E-05   3.539907E-05  -5.600078E-06   2.281143E-05  -2.005509E-05  -8.910276E-05
  a    38   9.132887E-05  -2.996004E-05  -6.564850E-05   3.058876E-05  -1.846076E-05   4.095824E-05   1.522371E-06   1.218541E-04
  a    39   7.352452E-05  -2.608735E-05  -6.461727E-05   2.017624E-05  -3.841252E-05  -4.215124E-05   5.264183E-05   1.612587E-04
  a    40  -2.702774E-05   1.274782E-04  -1.129936E-04  -4.772712E-06  -1.749361E-05  -8.991598E-06   6.001787E-05  -4.827555E-07
  a    41  -6.269205E-05  -1.801361E-04   1.017363E-04  -7.854975E-06  -2.412793E-06  -1.557182E-05   4.180301E-05   1.387385E-04
  a    42  -1.214497E-05   9.039606E-05   1.365943E-06   8.835060E-06  -2.560770E-05   5.894274E-06  -3.438939E-05  -8.011559E-05
  a    43   1.272840E-05  -3.736392E-05  -7.560162E-06   2.331319E-05  -3.281407E-05   1.300228E-05  -7.157155E-05  -3.756106E-05
  a    44  -1.760971E-04   3.297166E-05   1.403856E-04   7.048289E-05  -5.291700E-05   1.032711E-05   2.841229E-05   1.859496E-04
  a    45   5.735219E-05  -1.326073E-04  -1.220338E-04   3.207317E-05  -8.297201E-06  -4.692548E-06   9.082642E-05   2.024764E-04
  a    46  -1.313029E-04  -8.283953E-05  -1.295500E-04  -1.336763E-05  -1.001748E-05  -5.937866E-05  -6.981779E-05   5.872237E-05
  a    47  -1.822738E-04  -5.252055E-05   1.076867E-04   1.234677E-05  -1.937962E-05   1.920528E-07   6.851650E-05   4.245891E-05
  a    48   3.470899E-05  -4.352964E-05   5.661950E-05   4.997171E-06  -2.263838E-06   1.096863E-05   5.565680E-05  -2.102832E-05
  a    49   2.336304E-04  -4.448597E-05  -4.069330E-05  -4.626100E-06   6.712142E-07   1.542403E-06   3.628328E-06   1.189995E-04
  a    50  -1.322906E-04  -6.811700E-08  -2.710330E-05  -1.913725E-05  -1.598320E-06  -2.919487E-05  -3.637986E-05  -1.139727E-04
  a    51   5.876275E-06   4.221498E-07  -1.498786E-06  -1.757260E-05  -1.660838E-05  -1.229995E-05   2.850081E-06   9.406810E-06
  a    52   5.819612E-05   5.497905E-05   9.006803E-05   2.775391E-06  -6.204908E-06   1.805327E-05   1.549923E-05   2.773449E-05
  a    53  -3.871961E-05   8.491870E-05   8.072361E-05  -7.636776E-06  -1.572173E-05   1.478552E-05  -6.230513E-06  -7.648608E-05
  a    54  -3.515191E-06   7.444270E-06   1.145923E-06  -8.590377E-05  -1.467869E-04   4.144927E-05  -2.395634E-05  -6.515140E-06
  a    55   8.485051E-05  -8.293342E-05  -5.238505E-05  -3.635311E-06  -7.762584E-06  -5.395633E-06   2.841980E-05  -7.281863E-07
  a    56   8.917718E-05   5.539180E-05  -1.883639E-05   6.832684E-06  -8.044902E-06  -4.181759E-06  -1.837013E-05   1.325565E-05
  a    57   1.296481E-04   5.031966E-05   2.412653E-05   1.744131E-05   2.583830E-06   2.748516E-05  -3.261095E-05  -6.234923E-06
  a    58  -1.252049E-05  -2.079885E-07   2.532373E-06   5.228856E-05   6.633493E-05  -4.389064E-05  -6.279311E-06   9.497643E-06
  a    59  -3.239040E-06   1.096286E-06   1.602172E-05   1.743957E-04   2.336035E-04  -3.593808E-05   5.960731E-06   1.113114E-05
  a    60  -1.318172E-04   1.769500E-05   1.133868E-04   4.095140E-05  -5.755400E-05   1.227294E-05   1.532918E-05   1.395242E-04
  a    61   2.895274E-05   7.537487E-05  -8.102586E-06  -2.648904E-05   2.373096E-05   1.483751E-05  -2.215019E-05  -1.286493E-04
  a    62  -1.337674E-05   1.644989E-05  -4.168585E-06   2.848896E-06   1.933610E-05  -4.635948E-05  -1.965841E-05  -2.410404E-05
  a    63   1.824453E-04  -3.395343E-05   1.572442E-05   1.082542E-05  -5.165535E-06   5.138734E-06   3.778916E-05   4.012359E-05
  a    64  -7.629850E-05   9.887702E-06  -1.089777E-05   1.511005E-05   1.750960E-05   1.271521E-06  -2.338537E-05  -5.068006E-06
  a    65   2.665616E-04  -4.988069E-07  -5.776970E-05   4.420462E-06   5.543941E-06   1.360474E-05   3.104056E-05   6.989596E-05
  a    66  -4.988069E-07   1.502306E-04  -1.529244E-05  -1.439549E-06   3.397334E-08   1.162494E-06  -1.721054E-05  -5.993832E-05
  a    67  -5.776970E-05  -1.529244E-05   1.234193E-04   1.265012E-05   1.601345E-05   7.920987E-06   1.200050E-05  -1.434190E-05
  a    68   4.420462E-06  -1.439549E-06   1.265012E-05   3.135314E-04   3.649648E-04   8.877323E-06  -1.251814E-06   7.810104E-06
  a    69   5.543941E-06   3.397334E-08   1.601345E-05   3.649648E-04   4.871312E-04  -2.547022E-05   1.494430E-06  -9.090975E-06
  a    70   1.360474E-05   1.162494E-06   7.920987E-06   8.877323E-06  -2.547022E-05   1.026755E-04   3.896171E-06   2.843969E-06
  a    71   3.104056E-05  -1.721054E-05   1.200050E-05  -1.251814E-06   1.494430E-06   3.896171E-06   7.500610E-05   6.841638E-05
  a    72   6.989596E-05  -5.993832E-05  -1.434190E-05   7.810104E-06  -9.090975E-06   2.843969E-06   6.841638E-05   2.626041E-04
  a    73   1.134865E-05  -4.118881E-06   8.265294E-06   3.373263E-05  -5.752562E-06   6.412158E-05  -1.391226E-05   6.185653E-06
  a    74  -1.016966E-05  -1.799871E-05  -4.424686E-05   7.996818E-06   1.845931E-05  -1.104471E-05  -1.426448E-05  -1.033908E-05
  a    75  -3.303624E-05   3.318509E-05  -2.633889E-05  -1.440201E-05   1.572911E-06  -9.365922E-06  -4.588636E-05  -4.358389E-05
  a    76  -8.152527E-05   3.198641E-05  -4.837140E-06  -1.071442E-05   4.630422E-07  -4.313783E-06  -3.885623E-05  -1.107234E-04
  a    77   5.888512E-06  -1.259697E-05  -2.285632E-06   7.787570E-06  -5.021428E-06   1.664860E-06   6.908819E-06   2.522608E-05
  a    78   5.006780E-05   2.023440E-05   2.093781E-05  -1.144767E-05   1.209609E-06   1.108490E-05   5.780073E-06  -1.260777E-06
  a    79  -2.126571E-05  -2.022204E-05  -3.564184E-06  -6.132054E-07  -9.411660E-06  -6.899786E-06   2.177384E-06   3.650093E-05
  a    80  -3.373197E-05   1.378573E-05   3.189423E-05   8.114618E-06   3.575341E-06   1.013961E-05  -3.369011E-06  -7.394443E-06
  a    81  -3.350512E-06  -1.749099E-05  -7.279655E-06  -5.410580E-06   2.234842E-06  -6.773308E-06  -5.434960E-06  -7.426730E-06
  a    82  -4.854989E-05   1.982390E-05   2.229969E-05   6.797020E-06  -6.852622E-06   2.058854E-06  -6.749721E-06  -2.263081E-05
  a    83  -1.737107E-05  -1.440091E-05  -1.107004E-05   7.065498E-07   9.102944E-07   5.489184E-07   4.902361E-06   3.717054E-05
  a    84  -6.372232E-06   1.365203E-05  -2.623925E-06  -2.547324E-07   2.453172E-06   3.035214E-06  -1.530814E-05  -9.095088E-06
  a    85   2.175811E-06  -1.871560E-06   1.202079E-07   3.197546E-06   5.810779E-07   1.355820E-06   6.039923E-06  -2.193164E-06
  a    86  -1.931441E-05   2.350412E-05  -1.295951E-05  -2.995243E-06   1.185858E-06  -3.143592E-06  -2.095631E-05  -2.542652E-05

               a    73        a    74        a    75        a    76        a    77        a    78        a    79        a    80
  a    13   3.787635E-05  -9.213000E-05  -1.110063E-03   1.432994E-04  -1.041268E-03  -3.034543E-04   2.981771E-04  -4.109202E-04
  a    14   1.096877E-04  -5.374989E-04  -4.553656E-04   6.476813E-04  -4.159864E-05  -2.638833E-05  -3.088316E-04  -1.722755E-04
  a    15   2.795065E-05   4.208577E-04  -3.808527E-04   3.573918E-04   6.389516E-04  -5.208194E-04  -1.315773E-04  -1.248098E-05
  a    16  -1.554693E-04  -2.479330E-05  -4.016886E-04   1.762747E-06   1.173596E-03  -1.289192E-03   8.397019E-04   4.985161E-04
  a    17   1.332734E-05  -4.630389E-04  -4.698122E-04   3.534863E-04  -7.761927E-04   4.994443E-06  -3.949633E-04  -1.115550E-04
  a    18  -1.131930E-04  -1.346795E-03  -2.152293E-03   7.486394E-04   1.839528E-04  -5.315329E-05   1.045819E-03   1.216064E-03
  a    19   6.659882E-05   1.335776E-03  -3.629732E-04  -4.302739E-04   1.304919E-04  -1.311984E-03   1.009032E-03  -4.451850E-04
  a    20  -1.590342E-04  -2.622015E-04  -1.000472E-03   1.126291E-04   7.723312E-04  -8.006777E-04   6.905784E-04   1.024049E-04
  a    21   7.185528E-05   1.580571E-03   1.978283E-03   1.557002E-04   8.900999E-04  -1.618544E-04   1.040001E-04  -1.171550E-03
  a    22   8.086963E-05   1.176085E-03  -1.734824E-04  -5.236703E-04   3.238877E-04   1.517412E-04   1.376778E-04   7.132358E-05
  a    23  -1.677608E-05   1.007817E-03   9.128117E-04  -3.658898E-04   6.811522E-04  -6.886177E-04  -1.976614E-04  -4.453260E-04
  a    24  -1.208372E-04   2.883440E-04   2.590805E-04  -4.633109E-04   7.925497E-04  -9.457226E-05   1.625416E-04   1.686793E-04
  a    25  -3.981011E-05  -7.607185E-04  -3.892437E-04  -3.265222E-04   3.869265E-04   4.429380E-04  -1.623350E-04   7.109492E-05
  a    26   3.219537E-05  -1.274883E-03   1.037654E-03  -1.505572E-05   7.458739E-04   8.535779E-04  -3.613109E-04   1.008763E-03
  a    27  -2.761699E-04  -6.016365E-05   1.384509E-04   1.777862E-04   1.409391E-05  -1.286616E-05   2.365354E-05  -7.408311E-05
  a    28   1.982622E-04  -8.198535E-04  -8.819083E-04  -1.128111E-03  -4.691841E-04   7.445059E-04   6.868336E-04   1.607118E-03
  a    29   2.909547E-03   3.940657E-05   5.095986E-05   1.470271E-05  -1.457309E-05  -3.266903E-05  -1.009973E-04  -7.987779E-05
  a    30   1.150262E-03   5.177209E-05   3.458276E-05   3.283706E-05   7.815575E-05  -2.610285E-05   1.063837E-04   7.485964E-05
  a    31  -1.502975E-03  -1.614117E-04  -6.920694E-06  -3.267127E-05   4.064191E-05   7.805664E-05   7.469171E-05   4.771330E-05
  a    32  -2.418986E-03  -2.475604E-04  -2.932085E-05   1.032501E-05  -2.047219E-05   1.668790E-04   3.825722E-05  -1.055148E-05
  a    33  -2.587445E-04   4.170907E-04  -1.046195E-04  -6.799472E-04   6.081028E-04  -4.275505E-04   2.404868E-04  -2.466640E-04
  a    34  -3.890203E-03  -2.442397E-04   9.485360E-05   3.761260E-05   6.765493E-05   1.138290E-04   1.131281E-04  -4.318887E-05
  a    35   6.063129E-04   7.780519E-05  -3.130229E-06  -3.262116E-05   2.082519E-05  -3.780537E-05  -4.903947E-05   9.892761E-06
  a    36   2.612157E-04   4.759950E-05  -7.461839E-07  -2.921733E-06  -1.505982E-06  -2.009526E-05  -2.095386E-05   3.485767E-05
  a    37  -2.881718E-06  -8.976547E-05  -4.091873E-05  -6.673534E-05   1.356990E-04   1.843274E-07   4.490355E-05   9.224378E-05
  a    38  -7.354951E-06  -4.319113E-05  -7.199908E-05   5.177797E-05  -1.419999E-04   3.411937E-05  -5.265304E-05  -6.754665E-05
  a    39   7.360414E-06  -4.781756E-05  -1.098954E-04  -3.850602E-05   3.256175E-05  -3.837327E-05   2.258380E-05  -2.891243E-05
  a    40  -9.677154E-06   1.466056E-04   5.140349E-05  -3.696408E-05   1.238741E-04  -1.085448E-04   4.199896E-05  -5.253808E-05
  a    41  -2.501423E-06   3.890419E-05   3.024132E-05  -9.243590E-05   2.706404E-05   2.850210E-05   2.371709E-05   1.207583E-05
  a    42  -3.099251E-05  -2.047377E-05  -1.404784E-05   5.226072E-05  -1.823316E-05  -1.667956E-05   3.988506E-05   2.016140E-05
  a    43  -1.640320E-05  -8.043637E-05  -5.046649E-05  -3.486882E-05  -6.232918E-05   6.001634E-05   3.836884E-06   2.296459E-05
  a    44   8.053765E-06  -1.967099E-04  -1.995825E-04  -5.742109E-05  -9.238353E-05  -1.436962E-05   1.264133E-04   1.264018E-04
  a    45   2.075145E-06   1.423329E-04  -5.028964E-05  -1.753354E-04   1.870452E-04  -1.761625E-04   1.075693E-04  -7.294619E-05
  a    46  -1.825201E-05   3.891756E-04   2.255611E-04   3.417496E-05   9.829336E-05  -2.393796E-04   1.009338E-04  -1.228920E-04
  a    47   1.308617E-05  -1.026756E-04  -1.054608E-04  -2.729044E-05   8.067651E-05  -3.222820E-05   5.212035E-05   6.811951E-05
  a    48   1.610467E-05  -1.092967E-04  -6.927795E-05  -2.255345E-05   2.926975E-05   6.439369E-05  -2.739529E-05   4.932108E-05
  a    49   1.001538E-05   5.778371E-05   3.231309E-05  -1.020115E-04  -2.888613E-05   6.195854E-05   4.383097E-07  -3.520898E-05
  a    50  -1.099987E-05   1.414687E-04   6.886774E-05   7.014375E-05   1.195780E-05  -8.546206E-05   4.223580E-05  -4.227133E-05
  a    51  -5.722747E-06  -1.086207E-05  -1.796384E-06  -3.925931E-06  -1.957906E-07   5.985754E-06  -1.224197E-06  -1.161294E-07
  a    52   2.873427E-06  -1.064406E-04  -3.309075E-05  -1.245376E-04   1.066754E-04   7.275201E-05  -1.522179E-05   4.000938E-05
  a    53  -7.770079E-06  -1.439258E-04  -2.683020E-05   7.948339E-05  -6.519730E-05   7.343719E-05  -7.894136E-05   5.069809E-05
  a    54   2.908679E-04   3.498022E-07   2.920687E-06   4.246072E-06  -1.784565E-06  -5.283469E-06  -1.316035E-06  -3.844183E-09
  a    55   1.832422E-05   9.387887E-05   5.421560E-06  -4.088404E-05   2.845104E-05  -3.823204E-05   2.239572E-05  -4.391944E-05
  a    56   1.296697E-06   5.599823E-05   4.271545E-05  -5.020500E-05  -1.187125E-05   3.972975E-06   1.560087E-05  -1.918026E-05
  a    57   3.511932E-06  -1.681337E-04  -7.845088E-05  -3.818511E-05  -6.020090E-05   1.364893E-04  -5.107623E-05   4.697743E-05
  a    58   8.086069E-05  -2.097569E-06  -3.576285E-06  -2.898296E-06   1.373584E-06  -9.352212E-06   9.009770E-06   6.016208E-08
  a    59  -1.532423E-04  -3.157428E-06  -1.304465E-05  -1.031464E-05  -2.946409E-06   2.410057E-06   4.745124E-06   7.497817E-06
  a    60   9.508052E-06  -1.643912E-04  -1.078330E-04  -6.437303E-05  -6.556894E-07   2.141136E-05   4.254018E-05   9.978317E-05
  a    61  -6.194502E-06  -6.994130E-06   7.848930E-05   6.193470E-05  -4.923773E-05   7.197908E-05  -8.319540E-05  -5.326213E-06
  a    62   6.247223E-05   4.736637E-06   1.251582E-05   1.543473E-05  -1.082194E-05  -1.156176E-06  -1.372456E-05   2.618573E-07
  a    63   2.535763E-05  -6.407251E-05  -7.465083E-05  -9.907078E-05   1.184492E-05   8.225517E-05  -1.261708E-05   8.742273E-06
  a    64   2.094143E-05   4.565319E-05   2.768575E-05   2.560432E-05   5.426665E-06  -4.858792E-05   1.829020E-05  -9.236178E-06
  a    65   1.134865E-05  -1.016966E-05  -3.303624E-05  -8.152527E-05   5.888512E-06   5.006780E-05  -2.126571E-05  -3.373197E-05
  a    66  -4.118881E-06  -1.799871E-05   3.318509E-05   3.198641E-05  -1.259697E-05   2.023440E-05  -2.022204E-05   1.378573E-05
  a    67   8.265294E-06  -4.424686E-05  -2.633889E-05  -4.837140E-06  -2.285632E-06   2.093781E-05  -3.564184E-06   3.189423E-05
  a    68   3.373263E-05   7.996818E-06  -1.440201E-05  -1.071442E-05   7.787570E-06  -1.144767E-05  -6.132054E-07   8.114618E-06
  a    69  -5.752562E-06   1.845931E-05   1.572911E-06   4.630422E-07  -5.021428E-06   1.209609E-06  -9.411660E-06   3.575341E-06
  a    70   6.412158E-05  -1.104471E-05  -9.365922E-06  -4.313783E-06   1.664860E-06   1.108490E-05  -6.899786E-06   1.013961E-05
  a    71  -1.391226E-05  -1.426448E-05  -4.588636E-05  -3.885623E-05   6.908819E-06   5.780073E-06   2.177384E-06  -3.369011E-06
  a    72   6.185653E-06  -1.033908E-05  -4.358389E-05  -1.107234E-04   2.522608E-05  -1.260777E-06   3.650093E-05  -7.394443E-06
  a    73   2.997721E-04   1.088329E-05  -5.087972E-06  -8.072745E-06   8.560633E-07  -6.230578E-06  -5.944319E-06   3.482244E-06
  a    74   1.088329E-05   1.439854E-04   8.055238E-05   2.128260E-05   1.752404E-06  -5.980813E-05   1.207821E-05  -5.621017E-05
  a    75  -5.087972E-06   8.055238E-05   1.417357E-04   2.277708E-05   2.500592E-06   1.649638E-05  -2.083531E-05  -1.029053E-05
  a    76  -8.072745E-06   2.128260E-05   2.277708E-05   1.347807E-04  -7.696662E-05  -2.969120E-05  -1.822177E-05   3.183233E-07
  a    77   8.560633E-07   1.752404E-06   2.500592E-06  -7.696662E-05   1.526886E-04  -2.429944E-05   1.427085E-05  -1.002229E-05
  a    78  -6.230578E-06  -5.980813E-05   1.649638E-05  -2.969120E-05  -2.429944E-05   1.010184E-04  -3.926238E-05   2.773937E-05
  a    79  -5.944319E-06   1.207821E-05  -2.083531E-05  -1.822177E-05   1.427085E-05  -3.926238E-05   5.371153E-05   1.044223E-05
  a    80   3.482244E-06  -5.621017E-05  -1.029053E-05   3.183233E-07  -1.002229E-05   2.773937E-05   1.044223E-05   6.354056E-05
  a    81  -1.858100E-06   4.675186E-05   2.598205E-05  -8.660735E-06   3.383806E-05  -1.994466E-05   3.478119E-06  -2.226208E-05
  a    82  -2.449188E-06  -3.484492E-05  -1.940476E-05   2.811572E-05   3.437719E-06  -1.070250E-05   7.178985E-07   1.719982E-05
  a    83   1.050845E-06   1.566613E-05  -9.024352E-06  -6.908021E-06   3.858345E-06  -1.924007E-05   1.966453E-05  -1.116071E-05
  a    84   4.579473E-07  -2.904400E-06   8.671542E-06   5.430319E-06  -1.084892E-05   9.303148E-06   4.164554E-06   4.038421E-06
  a    85   5.550222E-07   2.932190E-06  -1.442923E-06  -5.222923E-06   4.369014E-06   7.549814E-07  -1.534645E-06   2.878593E-06
  a    86  -2.978832E-06   8.369619E-06   1.898253E-05   6.794783E-06  -1.028055E-05   4.748955E-06   1.307452E-05   8.650640E-06

               a    81        a    82        a    83        a    84        a    85        a    86
  a    13   2.278163E-04   1.360181E-04  -7.541299E-05   1.055742E-03  -1.124659E-03   9.055852E-05
  a    14   7.885719E-04   3.371125E-04   2.125668E-04   1.333633E-04  -6.519462E-04  -1.015960E-03
  a    15   8.651466E-04   3.905088E-04  -1.019198E-04  -6.516579E-04  -4.541735E-04  -1.125764E-03
  a    16   6.790498E-05  -1.676838E-05   6.986141E-05   7.132202E-05   3.336476E-04   2.817704E-04
  a    17  -5.844198E-04   3.607977E-04   6.125497E-04  -6.556303E-04   7.350097E-04  -1.554530E-04
  a    18  -3.827643E-04  -2.358583E-04   9.701374E-04   3.168716E-04  -7.781441E-05   7.174524E-04
  a    19   1.615362E-03  -3.693659E-04  -1.352414E-04   2.072826E-04  -9.765256E-05  -3.242887E-05
  a    20  -6.962094E-04  -8.990270E-04   6.912114E-05  -1.768744E-04   2.132216E-04  -3.713686E-04
  a    21  -1.036797E-04   4.824989E-04   7.518552E-04  -4.128533E-04   3.288181E-04  -1.187796E-04
  a    22   9.590695E-04  -1.934889E-04  -2.012395E-04   2.603019E-04  -2.220308E-04   3.440327E-04
  a    23  -2.945742E-04   3.863479E-04   4.277712E-04  -1.470609E-04   3.153811E-04   1.264588E-04
  a    24   1.377551E-04  -9.649822E-04   9.692804E-05   1.956462E-04  -2.181893E-04   1.573588E-04
  a    25  -4.644320E-04  -2.859741E-04  -4.729847E-04  -4.089851E-05   3.460371E-04  -1.070345E-04
  a    26  -6.887676E-04   1.101439E-03  -7.414144E-04   1.899872E-04  -1.660520E-04   1.585926E-04
  a    27  -1.616251E-04   1.887425E-04  -2.816381E-04   1.691464E-05   8.136553E-05  -1.399561E-04
  a    28  -1.328848E-04   1.600320E-04  -8.932137E-05   5.674294E-04  -6.180318E-06   2.560418E-04
  a    29   8.226116E-07   9.571485E-05  -3.054349E-05   7.930515E-05  -9.824581E-05  -3.332639E-05
  a    30  -6.494656E-05  -1.269051E-05  -1.006136E-04   2.701150E-05   6.815978E-05   3.559942E-05
  a    31  -8.212963E-05   1.637708E-05  -2.353202E-05  -2.649634E-05  -2.838134E-05   6.284653E-05
  a    32  -1.764291E-05   1.338206E-04  -4.583266E-05  -1.285865E-05  -1.612871E-05   4.347615E-05
  a    33   2.570749E-04  -8.462014E-04   4.543587E-04  -4.000038E-04   3.038031E-04  -3.641650E-04
  a    34  -3.565617E-05  -7.183025E-05   4.372549E-05   3.452325E-05   5.922832E-05  -2.408007E-05
  a    35   1.450537E-06  -2.413178E-05   2.793907E-05  -6.382240E-06   1.064294E-05  -1.232264E-05
  a    36  -5.233418E-06   8.956247E-06   1.238402E-07   2.155989E-05   8.616219E-06   5.691991E-06
  a    37  -6.841192E-05  -8.379149E-06  -3.963469E-05  -2.778524E-05   3.638017E-05   3.300587E-05
  a    38  -3.408450E-05   3.711232E-05   5.939301E-05   7.017495E-05  -3.077109E-05   2.939839E-06
  a    39  -1.557090E-05   6.722233E-05   6.232642E-05  -2.586354E-06   8.156855E-06  -2.466282E-05
  a    40   8.015794E-05  -1.537483E-05   1.667732E-05  -2.497835E-05   2.051994E-05   3.966649E-05
  a    41   1.748287E-05  -7.589992E-05  -1.693237E-05  -1.409474E-05   6.704244E-07  -1.754256E-05
  a    42  -3.402065E-05   4.250527E-05   4.125421E-06   2.801171E-05  -6.713620E-06   3.546489E-05
  a    43  -2.784254E-05   2.543769E-05   5.001147E-06   5.259599E-05  -3.000687E-05   6.851036E-05
  a    44  -1.468829E-04   1.118632E-04   6.193832E-05   9.312495E-05  -4.802153E-05   6.263530E-05
  a    45   1.071682E-04  -5.262064E-05   1.000241E-04  -9.541267E-05   4.347849E-05  -4.977186E-05
  a    46   1.538553E-04  -4.970467E-05   8.394102E-05  -4.054445E-05   4.967062E-05   6.440730E-05
  a    47  -3.523198E-05   8.547261E-05   2.106370E-05  -5.565539E-06   2.344878E-05  -1.628528E-05
  a    48  -7.405846E-06   3.349090E-05  -3.168864E-05   3.142478E-05  -1.059671E-06  -1.709948E-05
  a    49   2.798213E-05  -9.974975E-05  -2.958734E-06   4.007545E-05  -1.620865E-05   3.852720E-05
  a    50   5.230614E-05  -1.360447E-05   3.824273E-05   6.504011E-06   1.746545E-05   4.790278E-05
  a    51  -1.615395E-06   1.751633E-06  -2.522756E-06  -9.265259E-07  -1.548429E-06  -3.509647E-06
  a    52  -1.813643E-05  -7.296876E-06  -3.464613E-05  -1.252666E-05   3.251796E-06  -4.891263E-06
  a    53  -6.957465E-05   7.294122E-05  -5.233630E-05   2.885118E-07  -7.588137E-06  -3.465793E-05
  a    54  -5.086291E-06   8.419142E-07  -3.720978E-06   2.682005E-06  -1.504775E-06  -1.195595E-07
  a    55   6.107406E-05  -2.828042E-05   3.848808E-05  -1.319840E-05   1.138909E-05   1.638737E-05
  a    56  -1.738336E-05  -7.186038E-05   3.807073E-06   6.461102E-06  -4.857379E-06   1.252317E-05
  a    57  -7.038538E-05   2.571565E-06  -4.619262E-05   4.245132E-05  -2.556347E-05   9.815945E-06
  a    58  -1.944101E-06   4.977179E-06  -9.228309E-08  -4.765958E-07   3.090646E-07   8.706251E-07
  a    59  -6.981819E-06   2.155242E-06   6.335217E-07   1.008557E-06   2.223155E-06   1.966319E-06
  a    60  -9.048922E-05   9.646356E-05   3.134723E-05   6.069922E-06   9.783234E-07  -6.567941E-06
  a    61  -2.769769E-06  -1.448223E-05  -2.467447E-05  -1.901006E-05  -6.553545E-07  -2.248394E-05
  a    62  -1.368552E-07   3.072914E-06  -5.722290E-06  -6.206564E-06  -8.227492E-07  -1.518044E-06
  a    63  -2.124003E-05  -3.834636E-05  -2.631055E-05   1.153771E-05  -4.516258E-06  -1.143850E-05
  a    64   1.589909E-05   1.002346E-05   1.910416E-05  -2.167492E-06   3.827879E-08   1.280170E-05
  a    65  -3.350512E-06  -4.854989E-05  -1.737107E-05  -6.372232E-06   2.175811E-06  -1.931441E-05
  a    66  -1.749099E-05   1.982390E-05  -1.440091E-05   1.365203E-05  -1.871560E-06   2.350412E-05
  a    67  -7.279655E-06   2.229969E-05  -1.107004E-05  -2.623925E-06   1.202079E-07  -1.295951E-05
  a    68  -5.410580E-06   6.797020E-06   7.065498E-07  -2.547324E-07   3.197546E-06  -2.995243E-06
  a    69   2.234842E-06  -6.852622E-06   9.102944E-07   2.453172E-06   5.810779E-07   1.185858E-06
  a    70  -6.773308E-06   2.058854E-06   5.489184E-07   3.035214E-06   1.355820E-06  -3.143592E-06
  a    71  -5.434960E-06  -6.749721E-06   4.902361E-06  -1.530814E-05   6.039923E-06  -2.095631E-05
  a    72  -7.426730E-06  -2.263081E-05   3.717054E-05  -9.095088E-06  -2.193164E-06  -2.542652E-05
  a    73  -1.858100E-06  -2.449188E-06   1.050845E-06   4.579473E-07   5.550222E-07  -2.978832E-06
  a    74   4.675186E-05  -3.484492E-05   1.566613E-05  -2.904400E-06   2.932190E-06   8.369619E-06
  a    75   2.598205E-05  -1.940476E-05  -9.024352E-06   8.671542E-06  -1.442923E-06   1.898253E-05
  a    76  -8.660735E-06   2.811572E-05  -6.908021E-06   5.430319E-06  -5.222923E-06   6.794783E-06
  a    77   3.383806E-05   3.437719E-06   3.858345E-06  -1.084892E-05   4.369014E-06  -1.028055E-05
  a    78  -1.994466E-05  -1.070250E-05  -1.924007E-05   9.303148E-06   7.549814E-07   4.748955E-06
  a    79   3.478119E-06   7.178985E-07   1.966453E-05   4.164554E-06  -1.534645E-06   1.307452E-05
  a    80  -2.226208E-05   1.719982E-05  -1.116071E-05   4.038421E-06   2.878593E-06   8.650640E-06
  a    81   4.800432E-05  -1.279686E-05   8.332726E-06  -2.685923E-06   2.615991E-06   1.236746E-06
  a    82  -1.279686E-05   6.211021E-05  -7.982832E-07  -2.587528E-06   6.263158E-07  -1.142915E-06
  a    83   8.332726E-06  -7.982832E-07   4.653266E-05  -3.365720E-06   6.163023E-06   1.579356E-06
  a    84  -2.685923E-06  -2.587528E-06  -3.365720E-06   3.212956E-05  -1.289171E-05   2.572950E-05
  a    85   2.615991E-06   6.263158E-07   6.163023E-06  -1.289171E-05   2.039403E-05   1.200639E-06
  a    86   1.236746E-06  -1.142915E-06   1.579356E-06   2.572950E-05   1.200639E-06   4.934510E-05

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99997856     1.99990520     1.99968150     1.99959829
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.99952179     1.99941331     1.99907252     1.99874782     1.99853235     1.99844424     1.99832439     1.99769315
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     1.99715681     1.99688336     1.99481423     1.99455578     1.99385820     1.99275087     1.97488363     1.91450082
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     1.00115693     0.99614430     0.09453107     0.02800331     0.00718668     0.00476223     0.00292939     0.00274313
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00217220     0.00166256     0.00161846     0.00106010     0.00084658     0.00078080     0.00076576     0.00057362
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00051977     0.00048908     0.00045293     0.00036690     0.00031050     0.00028441     0.00027676     0.00027051
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62       MO    63       MO    64
  occ(*)=     0.00022990     0.00021599     0.00018266     0.00017875     0.00014235     0.00013628     0.00013041     0.00008826
              MO    65       MO    66       MO    67       MO    68       MO    69       MO    70       MO    71       MO    72
  occ(*)=     0.00008739     0.00007004     0.00005593     0.00003893     0.00003816     0.00003115     0.00002544     0.00002082
              MO    73       MO    74       MO    75       MO    76       MO    77       MO    78       MO    79       MO    80
  occ(*)=     0.00002045     0.00001710     0.00001594     0.00001231     0.00001060     0.00000697     0.00000507     0.00000426
              MO    81       MO    82       MO    83       MO    84       MO    85       MO    86
  occ(*)=     0.00000325     0.00000256     0.00000201     0.00000114     0.00000081     0.00000027


 total number of electrons =   66.0000000000

 accstate=                     3
 accpdens=                     3
logrecs(*)=   1   2logvrecs(*)=   1   2
 item #                     3 suffix=:.trd1to2:
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   2)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=   11036  DYX=       0  DYW=       0
   D0Z=   17456  D0Y=   53891  D0X=       0  D0W=       0
  DDZI=   21345 DDYI=   56427 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    2529 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   2)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=   11036  DYX=       0  DYW=       0
   D0Z=   17456  D0Y= 2111106  D0X=       0  D0W=       0
  DDZI=  251850 DDYI=  658014 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  0.132368183701112     
