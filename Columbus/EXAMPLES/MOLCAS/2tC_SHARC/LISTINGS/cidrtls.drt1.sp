 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt2.F9 Revision: 1.1.6.6           Date: 2015/02/26 17:04:32   * 
* cidrt3.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt4.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lencor=  52428800 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /home/lunet/cmfp2/programs/Columbus/Test/2tC/RUN3/WORK/cidrtky                  
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    66
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     1
 enter symmetry labels:(y,[n]) enter 1 labels (a4):
 enter symmetry label, default=   1
 symmetry labels: (symmetry, slabel)
 ( 1,  a  ) 
 input nmpsy(*):
 nmpsy(*)=        86
 
   symmetry block summary
 block(*)=         1
 slabel(*)=      a  
 nmpsy(*)=        86
 
 total molecular orbitals            :    86
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a  
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :    12
 
 fcorb(*)=         1   2   3   4   5   6   7   8   9  10  11  12
 slabel(*)=      a   a   a   a   a   a   a   a   a   a   a   a  
 
 number of frozen core orbitals      :    12
 number of frozen core electrons     :    24
 number of internal electrons        :    42
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    23
 
 modrt(*)=        13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28  29  30  31  32  33  34  35
 slabel(*)=      a   a   a   a   a   a   a   a   a   a   a   a   a   a   a  
                 a   a   a   a   a   a   a   a  
 
 total number of orbitals            :    86
 number of frozen core orbitals      :    12
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    23
 number of external orbitals         :    51
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  52  53  54
                  55  56  57  58  59  60  61  62  63  64  65  66  67  68  69
                  70  71  72  73  74   1   2   3   4   5   6   7   8   9  10
                  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25
                  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40
                  41  42  43  44  45  46  47  48  49  50  51
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :    18
 
 no. of internal orbitals            :    23
 no. of doubly-occ. (ref) orbitals   :    18
 no. active (ref) orbitals           :     5
 no. of active electrons             :     6
 
 input the active-orbital, active-electron occmnr(*):
  31 32 33 34 35
 input the active-orbital, active-electron occmxr(*):
  31 32 33 34 35
 
 actmo(*) =       31  32  33  34  35
 occmnr(*)=        0   0   0   0   6
 occmxr(*)=        6   6   6   6   6
 reference csf cumulative electron occupations:
 modrt(*)=        13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28  29  30  31  32  33  34  35
 occmnr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  36  36  36  36  42
 occmxr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  42  42  42  42  42
 
 input the active-orbital bminr(*):
  31 32 33 34 35
 input the active-orbital bmaxr(*):
  31 32 33 34 35
 reference csf b-value constraints:
 modrt(*)=        13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28  29  30  31  32  33  34  35
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   6   6   6   6   6
 input the active orbital smaskr(*):
  31 32 33 34 35
 modrt:smaskr=
  13:1000  14:1000  15:1000  16:1000  17:1000  18:1000  19:1000  20:1000
  21:1000  22:1000  23:1000  24:1000  25:1000  26:1000  27:1000  28:1000
  29:1000  30:1000  31:1111  32:1111  33:1111  34:1111  35:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     1
 number of internal electrons:       :    42
 
 input the internal-orbital mrsdci occmin(*):
  13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
  33 34 35
 input the internal-orbital mrsdci occmax(*):
  13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
  33 34 35
 mrsdci csf cumulative electron occupations:
 modrt(*)=        13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28  29  30  31  32  33  34  35
 occmin(*)=        1   3   5   7   9  11  13  15  17  19  21  23  25  27  29
                  31  33  35  35  35  35  35  41
 occmax(*)=       42  42  42  42  42  42  42  42  42  42  42  42  42  42  42
                  42  42  42  42  42  42  42  42
 
 input the internal-orbital mrsdci bmin(*):
  13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
  33 34 35
 input the internal-orbital mrsdci bmax(*):
  13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
  33 34 35
 mrsdci b-value constraints:
 modrt(*)=        13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28  29  30  31  32  33  34  35
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0
 bmax(*)=         42  42  42  42  42  42  42  42  42  42  42  42  42  42  42
                  42  42  42  42  42  42  42  42
 
 input the internal-orbital smask(*):
  13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
  33 34 35
 modrt:smask=
  13:1111  14:1111  15:1111  16:1111  17:1111  18:1111  19:1111  20:1111
  21:1111  22:1111  23:1111  24:1111  25:1111  26:1111  27:1111  28:1111
  29:1111  30:1111  31:1111  32:1111  33:1111  34:1111  35:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
                   1   1   1   1   1   1   1   1
 slabel(*)=      a   a   a   a   a   a   a   a   a   a   a   a   a   a   a  
                 a   a   a   a   a   a   a   a  
 rmo(*)=          13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28  29  30  31  32  33  34  35
 modrt(*)=        13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28  29  30  31  32  33  34  35
 
 reference csf info:
 occmnr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  36  36  36  36  42
 occmxr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  42  42  42  42  42
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   6   6   6   6   6
 
 
 mrsdci csf info:
 occmin(*)=        1   3   5   7   9  11  13  15  17  19  21  23  25  27  29
                  31  33  35  35  35  35  35  41
 occmax(*)=       42  42  42  42  42  42  42  42  42  42  42  42  42  42  42
                  42  42  42  42  42  42  42  42
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0
 bmax(*)=         42  42  42  42  42  42  42  42  42  42  42  42  42  42  42
                  42  42  42  42  42  42  42  42
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.
 multp(*)=
  hmult                     0
 lxyzir   0   0   0
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt : 196

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=         770
 xbary=       16275
 xbarx=      162522
 xbarw=      102515
        --------
 nwalk=      282082
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=     770

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a  
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
       50 csfs initially selected from     770 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
  13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
  33 34 35

 step 2 reference csf selection complete.
       50 csfs currently selected from     770 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
       50 reference csfs selected from     770 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   2   2   2   2   2   2   2   2   2   2   2   2   2
                   2   2   2   0   0   0   0   0
 
 number of step vectors saved:     50

 exlimw: beginning excitation-based walk selection...
 exlimw: nref=                    50

  number of valid internal walks of each symmetry:

       a  
      ----
 z             770
 y            1785
 x               0
 w               0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z             770
 y           91035
 x               0
 w               0

 total csf counts:
 z-vertex:             770
 y-vertex:           91035
 x-vertex:               0
 w-vertex:               0
           --------
 total:           91805
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=     770    1785       0       0 nvalwt=    2555

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     770    1785       0       0 nvalwt=    2555

 lprune input numv1,nwalk=                  2555                282082
 lprune input xbar(1,1),nref=                   770                    50

 lprune: l(*,*,*) pruned with nwalk=  282082 nvalwt=    2555= 7701785   0   0
 lprune:  z-drt, nprune=   412
 lprune:  y-drt, nprune=   360
 lprune: wx-drt, nprune=   514

 xbarz=         770
 xbary=        1785
 xbarx=           0
 xbarw=           0
        --------
 nwalk=        2555
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  33333333333333333333300
        2       2  33333333333333333333120
        3       3  33333333333333333333102
        4       4  33333333333333333333030
        5       5  33333333333333333333012
        6       6  33333333333333333333003
        7       7  33333333333333333331320
        8       8  33333333333333333331302
        9       9  33333333333333333331230
       10      10  33333333333333333331212
       11      11  33333333333333333331203
       12      12  33333333333333333331122
       13      13  33333333333333333331032
       14      14  33333333333333333331023
       15      15  33333333333333333330330
       16      16  33333333333333333330312
       17      17  33333333333333333330303
       18      18  33333333333333333330132
       19      19  33333333333333333330123
       20      20  33333333333333333330033
       21      21  33333333333333333313320
       22      22  33333333333333333313302
       23      23  33333333333333333313230
       24      24  33333333333333333313212
       25      25  33333333333333333313203
       26      26  33333333333333333313122
       27      27  33333333333333333313032
       28      28  33333333333333333313023
       29      29  33333333333333333312330
       30      30  33333333333333333312312
       31      31  33333333333333333312303
       32      32  33333333333333333312132
       33      33  33333333333333333312123
       34      34  33333333333333333312033
       35      35  33333333333333333311322
       36      36  33333333333333333311232
       37      37  33333333333333333311223
       38      38  33333333333333333310332
       39      39  33333333333333333310323
       40      40  33333333333333333310233
       41      41  33333333333333333303330
       42      42  33333333333333333303312
       43      43  33333333333333333303303
       44      44  33333333333333333303132
       45      45  33333333333333333303123
       46      46  33333333333333333303033
       47      47  33333333333333333301332
       48      48  33333333333333333301323
       49      49  33333333333333333301233
       50      50  33333333333333333300333
 indx01:    50 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:  2555 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a  
      ----
 z             770
 y            1785
 x               0
 w               0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z             770
 y           91035
 x               0
 w               0

 total csf counts:
 z-vertex:             770
 y-vertex:           91035
 x-vertex:               0
 w-vertex:               0
           --------
 total:           91805
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /home/lunet/cmfp2/programs/Columbus/Test/2tC/RUN3/WORK/cidrtfl                  
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  a  
nwalk=    2555 cpos=     320 maxval=    9 cmprfactor=   87.48 %.
nwalk=    2555 cpos=      27 maxval=   99 cmprfactor=   97.89 %.
nwalk=    2555 cpos=       3 maxval=  999 cmprfactor=   99.65 %.
nwalk=    2555 cpos=       1 maxval= 9999 cmprfactor=   99.84 %.
 compressed with: nwalk=    2555 cpos=       3 maxval=  999 cmprfactor=   99.65 %.
initial index vector length:      2555
compressed index vector length:         3reduction:  99.88%
nwalk=     770 cpos=      97 maxval=    9 cmprfactor=   87.40 %.
nwalk=     770 cpos=       9 maxval=   99 cmprfactor=   97.66 %.
nwalk=     770 cpos=       2 maxval=  999 cmprfactor=   99.22 %.
nwalk=     770 cpos=       2 maxval= 9999 cmprfactor=   98.96 %.
 compressed with: nwalk=     770 cpos=       2 maxval=  999 cmprfactor=   99.22 %.
initial ref vector length:       770
compressed ref vector length:         2reduction:  99.74%
