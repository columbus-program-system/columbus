

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        52428800 of real*8 words (  400.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-6,
   tol(2)=1.e-6,
   tol(1)=1.e-10,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,11,12, 2,26
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,29,20,1,30,20,1,31,20,1,32,20,1,33,20,1,34,20,1,35,20,1,36,20
   NAVST(1) = 4,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   WAVST(1,3)=1 ,
   WAVST(1,4)=1 ,
   NAVST(2) = 2,
   WAVST(2,1)=1 ,
   WAVST(2,2)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***



 Integral file header information:
 SEWARD INTEGRALS                                                                
 total ao core energy =  412.532574920


   ******  Basis set information:  ******

 Number of irreps:                  1
 Total number of basis functions:  86

 irrep no.              1
 irrep label           a  
 no. of bas.fcions.    86
 map-vector 1 , imtype=                     3
  1   1   1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2   3   3
  3   3   3   3   3   3   3   4   4   4   4   4   4   4   4   4   5   5   5   5
  5   5   5   5   5   6   6   6   6   6   6   6   6   6   7   7   7   7   7   7
  7   7   7   7   7   7   7   8   8   8   8   8   8   8   8   8   9   9  10  10
 11  11  12  12  13  13
 map-vector 2 , imtype=                     4
  1   1   1   2   2   2   2   2   2   1   1   1   2   2   2   2   2   2   1   1
  1   2   2   2   2   2   2   1   1   1   2   2   2   2   2   2   1   1   1   2
  2   2   2   2   2   1   1   1   2   2   2   2   2   2   1   1   1   1   2   2
  2   2   2   2   2   2   2   1   1   1   2   2   2   2   2   2   1   1   1   1
  1   1   1   1   1   1
 using sifcfg values:                  4096                  3272
                  4096                  2730                     0
 warning: resetting tol(6) due to possible inconsistency.


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

AO integrals are read from Seward integral files 
 ... RUNFILE, ONEINT , ORDINT 

 tol(1)=  1.0000E-10. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-06. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-06. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.2500E-07. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          4             0.167 0.167 0.167 0.167
  2   ground state          2             0.167 0.167

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        5
    2        3

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1      29( 29)    20
       1      30( 30)    20
       1      31( 31)    20
       1      32( 32)    20
       1      33( 33)    20
       1      34( 34)    20
       1      35( 35)    20
       1      36( 36)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 11:  construct the hmc(*) matrix explicitly.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 26:  Read AO integrals in MOLCAS native format 


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   66
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  10
 Total number of CSFs:      1176

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   66
 Spin multiplicity:            3
 Number of active orbitals:    8
 Number of active electrons:  10
 Total number of CSFs:      1512
 

 faar:   0 active-active rotations allowed out of:  28 possible.


 Number of active-double rotations:       224
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1400
 Number of active-virtual rotations:      400
 lenbfsdef=                131071  lenbfs=                  2500
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         666
 class  2 (pq|ri):         #        8064
 class  3 (pq|ia):         #       50400
 class  4 (pi|qa):         #       89600
 class  5 (pq|ra):         #       14400
 class  6 (pq|ij)/(pi|qj): #       42840
 class  7 (pq|ab):         #       45900
 class  8 (pa|qb):         #       90000
 class  9 p(bp,ai)         #      560000
 class 10p(ai,jp):        #      313600
 class 11p(ai,bj):        #     1015000

 Size of orbital-Hessian matrix B:                  2096424
 Size of the orbital-state Hessian matrix C:       15641472
 Total size of the state Hessian matrix M:         15639960
 Size of HESSIAN-matrix for quadratic conv.:       33377856


 Source of the initial MO coeficients:

 Input MO coefficient file: /home/lunet/cmfp2/programs/Columbus/Test/2tC/RUN3/WORK/mocoe
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    86, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     1, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  52391255

 input file header information:
 SEWARD INTEGRALS                                                                

 total ao core energy =   4.125325749203E+02

 nsym = 1 nbft=  86

 symmetry  =    1
 SLABEL(*) = a   
 NBPSY(*)  =   86
 INFOAO(*) = unset

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  52203575
 address segment size,           sizesg =  52052089
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 processing ORDINT  
 twoaofs: molcasbuflen determined to               13995082
 twoaofs: remaining lcore               52214698
 total read=  13995081
 total abs(val)>1d-15 =  11746961
 total sym1cmp =   5875081
 total sym2cmp =   5875081

    5875081 array elements were read by twoaof,
    5875081 array elements were written into   136 da records of length   64957.
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:x) array: ITYPEA= 3 ITYPEB=   0 nummot= 3652 nrecmo=  1 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3652
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:y) array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  2 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:z) array: ITYPEA= 3 ITYPEB=   0 nummot= 3650 nrecmo=  3 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3650
 L**2             component                      1  not on file ... skipping
 L**2             component                      2  not on file ... skipping
 L**2             component                      3  not on file ... skipping
 L**2             component                      4  not on file ... skipping
 L**2             component                      5  not on file ... skipping
 L**2             component                      6  not on file ... skipping
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lx)   array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  4 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(ly)   array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  5 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lz)   array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  6 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=   97.7260820000000       fcore=  0.000000000000000E+000

 tran1e: X(*)     array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo=  7 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.977260820000E+02
         kntmo(*)  3741
 fcoreao=   15.9333700000000       fcore=  0.000000000000000E+000

 tran1e: Y(*)     array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo=  8 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.159333700000E+02
         kntmo(*)  3741
 fcoreao=   26.9631040000000       fcore=  0.000000000000000E+000

 tran1e: Z(*)     array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo=  9 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.269631040000E+02
         kntmo(*)  3741
 fcoreao=   576.517543914757       fcore=  0.000000000000000E+000

 tran1e: XX(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 10 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.576517543915E+03
         kntmo(*)  3741
 fcoreao=   110.312102774948       fcore=  0.000000000000000E+000

 tran1e: XY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 11 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.110312102775E+03
         kntmo(*)  3741
 fcoreao=   202.028126333868       fcore=  0.000000000000000E+000

 tran1e: XZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 12 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.202028126334E+03
         kntmo(*)  3741
 fcoreao=   22.6906898390624       fcore=  0.000000000000000E+000

 tran1e: YY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 13 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.226906898391E+02
         kntmo(*)  3741
 fcoreao=   20.5729841315529       fcore=  0.000000000000000E+000

 tran1e: YZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 14 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.205729841316E+02
         kntmo(*)  3741
 fcoreao=   422.894527152504       fcore=  0.000000000000000E+000

 tran1e: ZZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 15 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.422894527153E+03
         kntmo(*)  3741
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: S1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=   86 nrecmo= 16 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)    86
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: T1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 17 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3741
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: V1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 18 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3741

 tran1e: transformation of the input 1-e arrays is complete.
 trmain:    3890460 transformed 1/r12    array elements were written in     713 records.

 !timer: 2-e transformation              cpu_time=     1.358 walltime=     1.358

 mosort: allocated sort2 space, avc2is=    52253108 available sort2 space, avcisx=    52253360

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=  42 noldhv= 13 noldv= 13

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -711.5722470209    -1124.1048219412        0.0000000707        0.0000001250
    2      -711.4622495104    -1123.9948244307        0.0000000566        0.0000001250
    3      -711.4396928664    -1123.9722677867        0.0000000530        0.0000001250
    4      -711.4250875926    -1123.9576625130        0.0000000849        0.0000001250
    5      -711.3716689810    -1123.9042439013        0.0063339408        0.0100000000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     7
 ciiter=  30 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -711.4685050199    -1124.0010799403        0.0000000333        0.0000001250
    2      -711.4652928846    -1123.9978678049        0.0000001178        0.0000001250
    3      -711.4512834682    -1123.9838583885        0.0010552131        0.0100000000
 !timer: rdft                            cpu_time=     1.118 walltime=     1.118
 !timer: rdft                            cpu_time=     0.669 walltime=     0.669
 !timer: hbcon                           cpu_time=     2.058 walltime=     2.058
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.637313605236834E-008
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 2.0648E-08 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
  -182.651119  -31.090142  -30.960396  -30.931547  -22.542305  -22.466866  -22.436240  -22.341558  -17.849725  -13.198969
   -13.192611  -13.191413   -2.662629   -2.490133   -2.322148   -2.163070   -1.991701   -1.752528   -1.692417   -1.508516
    -1.460893   -1.356314   -1.274993   -1.201332   -1.154999   -1.065711   -0.938089   -0.825014

 qvv(*) eigenvalues. symmetry block  1
     0.461601    0.551685    0.587408    0.622670    0.691784    0.721585    0.857109    0.960849    1.021703    1.110272
     1.173695    1.234182    1.361551    1.426610    1.435338    1.530258    1.547919    1.867752    1.874237    2.015953
     2.059537    2.080198    2.102953    2.139115    2.144492    2.213013    2.306546    2.463326    2.584939    2.691831
     2.809292    2.865164    2.876022    2.909008    3.009340    3.058600    3.138008    3.185516    3.269916    3.405705
     3.494335    3.581531    3.711853    3.871941    4.193662    4.275532    4.619888    5.828956    6.026241    6.625265

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     4.161 walltime=     4.161

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -711.4721791491 demc= 7.1147E+02 wnorm= 1.3099E-07 knorm= 8.6827E-08 apxde= 6.9473E-15    *not conv.*     

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     4.167 walltime=     4.168

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    86, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     1, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  52391255

 input file header information:
 SEWARD INTEGRALS                                                                

 total ao core energy =   4.125325749203E+02

 nsym = 1 nbft=  86

 symmetry  =    1
 SLABEL(*) = a   
 NBPSY(*)  =   86
 INFOAO(*) = unset

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  52203575
 address segment size,           sizesg =  52052089
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:x) array: ITYPEA= 3 ITYPEB=   0 nummot= 3652 nrecmo=  1 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3652
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:y) array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  2 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:z) array: ITYPEA= 3 ITYPEB=   0 nummot= 3650 nrecmo=  3 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3650
 L**2             component                      1  not on file ... skipping
 L**2             component                      2  not on file ... skipping
 L**2             component                      3  not on file ... skipping
 L**2             component                      4  not on file ... skipping
 L**2             component                      5  not on file ... skipping
 L**2             component                      6  not on file ... skipping
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lx)   array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  4 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(ly)   array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  5 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lz)   array: ITYPEA= 3 ITYPEB=   0 nummot= 3655 nrecmo=  6 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3655
 fcoreao=   97.7260820000000       fcore=  0.000000000000000E+000

 tran1e: X(*)     array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo=  7 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.977260820000E+02
         kntmo(*)  3741
 fcoreao=   15.9333700000000       fcore=  0.000000000000000E+000

 tran1e: Y(*)     array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo=  8 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.159333700000E+02
         kntmo(*)  3741
 fcoreao=   26.9631040000000       fcore=  0.000000000000000E+000

 tran1e: Z(*)     array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo=  9 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.269631040000E+02
         kntmo(*)  3741
 fcoreao=   576.517543914757       fcore=  0.000000000000000E+000

 tran1e: XX(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 10 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.576517543915E+03
         kntmo(*)  3741
 fcoreao=   110.312102774948       fcore=  0.000000000000000E+000

 tran1e: XY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 11 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.110312102775E+03
         kntmo(*)  3741
 fcoreao=   202.028126333868       fcore=  0.000000000000000E+000

 tran1e: XZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 12 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.202028126334E+03
         kntmo(*)  3741
 fcoreao=   22.6906898390624       fcore=  0.000000000000000E+000

 tran1e: YY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 13 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.226906898391E+02
         kntmo(*)  3741
 fcoreao=   20.5729841315529       fcore=  0.000000000000000E+000

 tran1e: YZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 14 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.205729841316E+02
         kntmo(*)  3741
 fcoreao=   422.894527152504       fcore=  0.000000000000000E+000

 tran1e: ZZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 15 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.422894527153E+03
         kntmo(*)  3741
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: S1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=   86 nrecmo= 16 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)    86
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: T1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 17 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3741
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: V1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot= 3741 nrecmo= 18 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)  3741

 tran1e: transformation of the input 1-e arrays is complete.
 trmain:    3890461 transformed 1/r12    array elements were written in     713 records.

 !timer: 2-e transformation              cpu_time=     0.666 walltime=     0.667

 mosort: allocated sort2 space, avc2is=    52253108 available sort2 space, avcisx=    52253360
 !timer: mosrt1                          cpu_time=     0.398 walltime=     0.697
 !timer: mosort                          cpu_time=     0.483 walltime=     0.782

   5 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  5 noldv=  5

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -711.5722470214    -1124.1048219417        0.0000000825        0.0000001250
    2      -711.4622495089    -1123.9948244292        0.0000000764        0.0000001250
    3      -711.4396928678    -1123.9722677882        0.0000000701        0.0000001250
    4      -711.4250875943    -1123.9576625146        0.0000001013        0.0000001250
    5      -711.3716689833    -1123.9042439036        0.0063339378        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   2 noldhv=  4 noldv=  4

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -711.4685050192    -1124.0010799395        0.0000000557        0.0000001250
    2      -711.4652928832    -1123.9978678035        0.0000000667        0.0000001250
    3      -711.4512834698    -1123.9838583901        0.0010546385        0.0100000000
 !timer: rdft                            cpu_time=     1.072 walltime=     1.073
 !timer: rdft                            cpu_time=     0.727 walltime=     0.727
 !timer: hbcon                           cpu_time=     2.079 walltime=     2.171
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.641766970225752E-009
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 7.1957E-08 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
  -182.651119  -31.090142  -30.960396  -30.931547  -22.542305  -22.466866  -22.436240  -22.341558  -17.849725  -13.198969
   -13.192611  -13.191413   -2.662629   -2.490133   -2.322148   -2.163070   -1.991701   -1.752528   -1.692417   -1.508516
    -1.460893   -1.356314   -1.274993   -1.201332   -1.154999   -1.065711   -0.938089   -0.825014

 qvv(*) eigenvalues. symmetry block  1
     0.461601    0.551685    0.587408    0.622670    0.691784    0.721585    0.857109    0.960849    1.021703    1.110272
     1.173695    1.234182    1.361551    1.426610    1.435338    1.530258    1.547919    1.867752    1.874237    2.015953
     2.059537    2.080198    2.102953    2.139115    2.144492    2.213013    2.306546    2.463326    2.584939    2.691831
     2.809292    2.865164    2.876022    2.909008    3.009340    3.058600    3.138008    3.185516    3.269916    3.405705
     3.494335    3.581531    3.711853    3.871941    4.193662    4.275532    4.619888    5.828956    6.026241    6.625265

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     3.421 walltime=     3.999

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    2 emc=   -711.4721791491 demc= 1.0232E-12 wnorm= 2.1134E-08 knorm= 1.2020E-09 apxde=-5.2655E-16    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.167 total energy=     -711.572247021, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.167 total energy=     -711.462249509, rel. (eV)=   2.993186
   DRT #1 state # 3 wt 0.167 total energy=     -711.439692868, rel. (eV)=   3.606984
   DRT #1 state # 4 wt 0.167 total energy=     -711.425087594, rel. (eV)=   4.004413
   DRT #2 state # 1 wt 0.167 total energy=     -711.468505019, rel. (eV)=   2.822965
   DRT #2 state # 2 wt 0.167 total energy=     -711.465292883, rel. (eV)=   2.910371
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  a  
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.98559491     1.92949328     1.90305084     1.61800202
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     1.49993004     0.66144410     0.33874967     0.06373513     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69        MO   70        MO   71        MO   72
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   73        MO   74        MO   75        MO   76        MO   77        MO   78        MO   79        MO   80
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   81        MO   82        MO   83        MO   84        MO   85        MO   86
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
       3466 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
    N1_ s       0.000000   1.986707  -0.000004   0.000000  -0.000134  -0.000002
    N1_ p      -0.000001  -0.000001  -0.000070  -0.000000  -0.000328  -0.000022
    C2_ s      -0.000003   0.001906   0.001863   0.000000   1.996048   0.000065
    C2_ p       0.000131   0.003503   0.003855   0.000008  -0.000000   0.000422
    N3_ s       0.000000  -0.000012   1.982678   0.000404  -0.000549  -0.000465
    N3_ p      -0.000001  -0.000054  -0.000007  -0.000038  -0.000536  -0.001665
    C4_ s      -0.000000   0.000049   0.002968   0.002016   0.000170   1.992714
    C4_ p      -0.000000   0.000064   0.008544   0.007317   0.000674   0.000037
    C5_ s      -0.000000  -0.000177  -0.000080   0.000049  -0.000055   0.003170
    C5_ p      -0.000000  -0.000433  -0.000276  -0.000128  -0.000210   0.005621
    C6_ s      -0.000000   0.002874   0.000038  -0.000000   0.000171   0.000127
    C6_ p      -0.000000   0.004517   0.000056   0.000000   0.000448   0.000151
    S7_ s       1.999874   0.000019   0.000029   0.000000   0.001677   0.000003
    S7_ p       0.000000   0.000005   0.000029   0.000001   0.002672   0.000006
    N8_ s      -0.000000   0.000000   0.000372   1.987809  -0.000003  -0.000001
    N8_ p      -0.000000  -0.000000  -0.000001  -0.000003  -0.000003  -0.000248
    H9_ s      -0.000000   0.000909   0.000000   0.000000  -0.000043  -0.000000
    H1_ s      -0.000000   0.000124  -0.000000   0.000000   0.000000  -0.000002
    H1_ s       0.000000  -0.000002  -0.000001   0.000015  -0.000001   0.000033
    H1_ s      -0.000000   0.000000   0.000001   0.001259  -0.000000   0.000002
    H1_ s      -0.000000  -0.000000   0.000007   0.001292  -0.000000   0.000054
 
   ao class       7a         8a         9a        10a        11a        12a  
    N1_ s       0.000320  -0.000034  -0.000013  -0.000011   0.000004   0.000000
    N1_ p      -0.000763  -0.000080   0.000074   0.000060   0.000033   0.000001
    C2_ s       0.000040   0.000006   0.000743  -0.000051  -0.000000   0.000000
    C2_ p       0.000260   0.000028  -0.004316  -0.002581  -0.000492   0.000112
    N3_ s      -0.000011  -0.000021  -0.000018  -0.000016   0.000002  -0.000000
    N3_ p      -0.000010  -0.000042   0.000039   0.000034   0.000036   0.000001
    C4_ s       0.000109   0.003903   0.000000   0.000000   0.000000  -0.000000
    C4_ p       0.000247   0.005415   0.000000  -0.000001   0.000001  -0.000000
    C5_ s       0.003196   1.979141   0.000000  -0.000000   0.000000  -0.000000
    C5_ p       0.004323  -0.000024  -0.000000  -0.000000  -0.000000   0.000000
    C6_ s       1.990287   0.005200   0.000000   0.000000   0.000000  -0.000000
    C6_ p       0.000002   0.005575   0.000001   0.000001   0.000000  -0.000000
    S7_ s       0.000002  -0.000000   2.003165   0.000235   0.000000   0.000000
    S7_ p       0.000003  -0.000000   0.000306   2.002336   2.000392   1.999887
    N8_ s       0.000002   0.000009   0.000000  -0.000000   0.000000   0.000000
    N8_ p       0.000004   0.000006   0.000000   0.000000  -0.000000  -0.000000
    H9_ s       0.000001   0.000000   0.000019  -0.000005   0.000025   0.000000
    H1_ s       0.001981   0.000122   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000006   0.000793   0.000000   0.000000  -0.000000   0.000000
    H1_ s       0.000000   0.000003   0.000000   0.000000  -0.000000  -0.000000
    H1_ s       0.000000   0.000001  -0.000000  -0.000000  -0.000000  -0.000000
 
   ao class      13a        14a        15a        16a        17a        18a  
    N1_ s       0.909050   0.453763   0.139347   0.026874   0.110161   0.029843
    N1_ p       0.043147   0.001003   0.030951   0.139913   0.016694   0.342333
    C2_ s       0.247273   0.004000   0.134017   0.260420   0.140871   0.000752
    C2_ p      -0.049047   0.043215   0.041188   0.089597   0.213310   0.127648
    N3_ s       0.426345   0.353084   0.582975   0.005357   0.082589   0.071928
    N3_ p       0.073010   0.055480   0.053311   0.033231   0.028549   0.066921
    C4_ s       0.063116   0.334293   0.004519   0.063813  -0.000993   0.241325
    C4_ p      -0.009593  -0.031030   0.002198   0.056716   0.082394   0.126629
    C5_ s       0.098338   0.000381   0.013909   0.424162   0.414297   0.100114
    C5_ p       0.066941  -0.018905   0.004448   0.042309   0.002636   0.115887
    C6_ s       0.073498   0.053528   0.095622   0.310775   0.066949   0.357253
    C6_ p      -0.028847   0.001479   0.000065   0.045303   0.106027   0.029640
    S7_ s       0.000525   0.002201   0.022581   0.230640   0.631288   0.023376
    S7_ p      -0.012150   0.001830   0.002845   0.018680   0.030911   0.004158
    N8_ s       0.045537   0.607579   0.738674   0.159594   0.000030   0.050391
    N8_ p       0.007925   0.045117   0.013587   0.003579   0.008055   0.105521
    H9_ s       0.044307   0.032266   0.013918   0.013491   0.021170   0.028883
    H1_ s      -0.008806   0.000160   0.001281   0.016525   0.003528   0.103944
    H1_ s       0.006204  -0.000789  -0.002707   0.021361   0.039456   0.015181
    H1_ s       0.001402   0.031941   0.061379   0.017339   0.000852   0.013682
    H1_ s       0.001827   0.029404   0.045893   0.020322   0.001225   0.044589
 
   ao class      19a        20a        21a        22a        23a        24a  
    N1_ s       0.006465  -0.001693   0.001093   0.003468   0.006565   0.012874
    N1_ p       0.393521   0.705867   0.310186   0.116100   0.107038   0.148129
    C2_ s       0.155495   0.003270   0.000705   0.077532   0.000679   0.045804
    C2_ p       0.169944   0.030645   0.208396   0.005546   0.090373   0.069712
    N3_ s       0.007932   0.062966   0.002524   0.003900  -0.001240   0.020274
    N3_ p       0.296001   0.062891   0.226838   0.039782   0.149365   0.068052
    C4_ s       0.131640   0.033011   0.008018   0.019557   0.003720   0.020272
    C4_ p       0.039621   0.008584   0.240298   0.333818   0.049922   0.088348
    C5_ s       0.044285   0.106999   0.000922   0.016738   0.057745   0.014830
    C5_ p       0.002061   0.201335   0.251623   0.051018   0.056329   0.671521
    C6_ s       0.012102   0.004562   0.018256   0.042891   0.049553   0.015410
    C6_ p       0.069657   0.232137   0.198724   0.179844   0.178151   0.042190
    S7_ s       0.339202   0.078417   0.001058   0.088949   0.000687   0.111878
    S7_ p       0.001547   0.010046   0.005892   0.024472   0.004702   0.100426
    N8_ s       0.035683  -0.000304   0.000792   0.002490   0.000143   0.001157
    N8_ p       0.126343   0.021635   0.348991   0.641817   0.623321   0.060916
    H9_ s       0.073514   0.226474   0.002680   0.056776   0.001309   0.050360
    H1_ s       0.025299   0.064922   0.001858   0.114861   0.143653   0.011255
    H1_ s       0.010168   0.140801   0.000280   0.003391   0.060545   0.396457
    H1_ s       0.050966   0.007014   0.060348   0.111147   0.247521   0.034477
    H1_ s       0.008555   0.000422   0.110515   0.065903   0.169919   0.015657
 
   ao class      25a        26a        27a        28a        29a        30a  
    N1_ s       0.003115   0.000075   0.000096   0.004314  -0.000000   0.000000
    N1_ p       0.020936   0.075688   0.011599   0.093470   1.350150   0.087872
    C2_ s       0.005806   0.051052   0.000207   0.020692   0.000007   0.000004
    C2_ p       0.027992   0.478270   0.006603   0.050494   0.344907   0.001051
    N3_ s       0.041186   0.002229  -0.000197   0.180326   0.000000  -0.000001
    N3_ p       0.132583   0.179619   0.015577   1.187606   0.005774   0.385886
    C4_ s       0.020292   0.001457   0.000739   0.001364   0.000002   0.000116
    C4_ p       0.216114   0.050254   0.247678   0.093113  -0.001112   0.493509
    C5_ s       0.000325   0.000662  -0.000214   0.007820  -0.000001   0.000007
    C5_ p       0.307316   0.041072  -0.001153   0.151119   0.003429   0.697097
    C6_ s      -0.002836   0.000065  -0.000068   0.000630   0.000002   0.000001
    C6_ p       0.527159   0.061706   0.000513   0.020596   0.241451   0.193727
    S7_ s       0.001283   0.322657   0.003042   0.008388  -0.000000  -0.000000
    S7_ p       0.001844   0.558942   0.010296   0.038397   0.035702   0.004576
    N8_ s       0.000561   0.006049   0.065543  -0.000597   0.000170   0.003027
    N8_ p       0.289085   0.089983   1.629946   0.066486   0.005094   0.061178
    H9_ s       0.018444   0.020995   0.000022   0.015681  -0.000000   0.000000
    H1_ s       0.232183   0.008027   0.000235   0.017869   0.000000   0.000000
    H1_ s       0.016615   0.023064   0.002535   0.025917   0.000000   0.000001
    H1_ s       0.012894   0.027309   0.001123   0.001304   0.000013   0.001122
    H1_ s       0.127101   0.000827   0.005881   0.015013   0.000007   0.000321
 
   ao class      31a        32a        33a        34a        35a        36a  
    N1_ s       0.000000   0.000001   0.004715   0.000000   0.000000  -0.000000
    N1_ p       0.032621   0.064447   0.007237   0.105485   0.000307   0.002837
    C2_ s       0.000004   0.000000   0.000019  -0.000001   0.000000   0.000000
    C2_ p       0.507106  -0.004886   0.040427   0.193032   0.069446   0.004585
    N3_ s       0.000006   0.000000   0.006381   0.000001   0.000000  -0.000000
    N3_ p       0.544729   0.140707   0.008024   0.001827   0.084397   0.011327
    C4_ s       0.000000   0.000002   0.000936   0.000099   0.000004   0.000007
    C4_ p       0.036465   0.008033   0.002021   0.196128   0.008786   0.017502
    C5_ s       0.000005   0.000001   0.000002   0.000012   0.000000   0.000001
    C5_ p       0.224869   0.090596   0.000271   0.006522   0.070472   0.016041
    C6_ s       0.000001   0.000000   0.000616  -0.000000   0.000000   0.000000
    C6_ p       0.392832   0.020306   0.002523   0.095064   0.088846   0.009904
    S7_ s      -0.000001   0.000001   0.000224   0.000000   0.000000  -0.000000
    S7_ p       0.160063   1.295369   1.424630   0.029939   0.015220   0.000193
    N8_ s       0.000277   0.000161   0.000021   0.001521   0.000039   0.000073
    N8_ p       0.003793   0.003101   0.000320   0.030116   0.001155   0.001165
    H9_ s       0.000000   0.000000   0.000901   0.000000   0.000000   0.000000
    H1_ s       0.000000  -0.000000   0.000449   0.000000   0.000000  -0.000000
    H1_ s       0.000005   0.000003   0.000001  -0.000000   0.000003   0.000001
    H1_ s       0.000252   0.000145   0.000186   0.001366   0.000026   0.000085
    H1_ s       0.000023   0.000015   0.000026   0.000332   0.000047   0.000016
 
   ao class      37a        38a        39a        40a        41a        42a  
 
   ao class      43a        44a        45a        46a        47a        48a  
 
   ao class      49a        50a        51a        52a        53a        54a  
 
   ao class      55a        56a        57a        58a        59a        60a  
 
   ao class      61a        62a        63a        64a        65a        66a  
 
   ao class      67a        68a        69a        70a        71a        72a  
 
   ao class      73a        74a        75a        76a        77a        78a  
 
   ao class      79a        80a        81a        82a        83a        84a  
 
   ao class      85a        86a  


                        gross atomic populations
     ao           N1_        C2_        N3_        C4_        C5_        C6_
      s         3.696960   3.149228   3.830558   2.949241   3.286584   3.097507
      p         4.206434   2.760483   3.849243   2.378691   3.063727   2.719747
    total       7.903395   5.909712   7.679802   5.327933   6.350311   5.817254
 
 
     ao           S7_        N8_        H9_        H1_        H1_        H1_
      s         5.871400   3.706798   0.622098   0.739465   0.759336   0.685157
      p         9.774165   4.187983   0.000000   0.000000   0.000000   0.000000
    total      15.645565   7.894781   0.622098   0.739465   0.759336   0.685157
 
 
     ao           H1_
      s         0.665192
    total       0.665192
 

 Total number of electrons:   66.00000000

 !timer: mcscf                           cpu_time=     7.614 walltime=     8.192
