 total ao core energy =  412.532574920
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          4             0.167 0.167 0.167 0.167
  2   ground state          2             0.167 0.167

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    66
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  10
 Total number of CSFs:      1176

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    66
 Spin multiplicity:            3
 Number of active orbitals:    8
 Number of active electrons:  10
 Total number of CSFs:      1512

 Number of active-double rotations:       224
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1400
 Number of active-virtual rotations:      400
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -711.4721791491  7.115E+02  1.310E-07  8.683E-08  6.947E-15  F   *not conv.*     

 final mcscf convergence values:
    2   -711.4721791491  1.023E-12  2.113E-08  1.202E-09 -5.265E-16  F   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.167 total energy=     -711.572247021, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.167 total energy=     -711.462249509, rel. (eV)=   2.993186
   DRT #1 state # 3 wt 0.167 total energy=     -711.439692868, rel. (eV)=   3.606984
   DRT #1 state # 4 wt 0.167 total energy=     -711.425087594, rel. (eV)=   4.004413
   DRT #2 state # 1 wt 0.167 total energy=     -711.468505019, rel. (eV)=   2.822965
   DRT #2 state # 2 wt 0.167 total energy=     -711.465292883, rel. (eV)=   2.910371
   ------------------------------------------------------------


