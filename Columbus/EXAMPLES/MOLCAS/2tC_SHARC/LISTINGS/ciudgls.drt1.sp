1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       770       91035           0           0       91805
      internal walks       770        1785           0           0        2555
valid internal walks       770        1785           0           0        2555
  MR-CIS calculation - skip 3- and 4-external 2e integrals
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  5551 ci%nnlev=                  2775  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              52384982              52366026
 lencor,maxblo              52428800                 60000
========================================
 current settings:
 minbl3           0
 minbl4           0
 locmaxbl3   135154
 locmaxbuf    67577
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================
 Orig.  diagonal integrals:  1electron:        74
                             0ext.    :       552
                             2ext.    :      2346
                             4ext.    :      2652


 Orig. off-diag. integrals:  4ext.    :         0
                             3ext.    :         0
                             2ext.    :   1097928
                             1ext.    :    351900
                             0ext.    :     44022
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:      1326


 Sorted integrals            3ext.  w :         0 x :         0
                             4ext.  w :         0 x :         0


Cycle #  1 sortfile size=   3670016(       7 records of   524288) #buckets=   2
distributed memory consumption per node=         0 available core  52384982
Cycle #  2 sortfile size=   3670016(       7 records of   524288) #buckets=   2
distributed memory consumption per node=         0 available core  52384982
 minimum size of srtscr:   2621440 WP (     5 records)
 maximum size of srtscr:   3670016 WP (     7 records)
diagi   file:      4 records  of   6144 WP each=>      24576 WP total
ofdgi   file:    246 records  of   6144 WP each=>    1511424 WP total
fil3w   file:      0 records  of  30006 WP each=>          0 WP total
fil3x   file:      0 records  of  30006 WP each=>          0 WP total
fil4w   file:      0 records  of  30006 WP each=>          0 WP total
fil4x   file:      0 records  of  30006 WP each=>          0 WP total
 compressed index vector length=                     3
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 42
   nroot=4
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 5
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 150
  NVCIMN = 9
   rtolci=1e-3,1e-3,1e-3,1e-3
  NVCIMX = 20
  NVRFMX = 20
  NVBKMX = 10
   iden=2
  CSFPRN = 10,
  update_mode=1
  nvrfmn=5
   MOLCAS=1
 /&end
 transition
   1  1  1  2
   1  2  1  3
   1  2  1  4
 ------------------------------------------------------------------------
transition densities: resetting nroot to    4
lodens (list->root)=  1  2  3  4
invlodens (root->list)=  1  2  3  4
 bummer (warning):resetting fileloc for seriel operation0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    4      noldv  =   0      noldhv =   0
 nunitv =    9      nbkitr =    1      niter  = 150      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   10      ibktv  =  -1      ibkthv =  -1
 nvcimx =   20      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    5      nvcimn =    9      maxseg = 300      nrfitr =  30
 ncorel =   42      nvrfmx =   20      nvrfmn =   5      iden   =   2
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-03
    2        1.000E-04    1.000E-03
    3        1.000E-04    1.000E-03
    4        1.000E-04    1.000E-03
 Computing density:                    .drt1.state1
 Computing density:                    .drt1.state2
 Computing density:                    .drt1.state3
 Computing density:                    .drt1.state4
 Computing transition density:          drt1.state1-> drt1.state2 (.trd1to2)
 Computing transition density:          drt1.state2-> drt1.state3 (.trd2to3)
 Computing transition density:          drt1.state2-> drt1.state4 (.trd2to4)
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           52428799 DP per process
 gapointer%node_offset(*)=                     0                     0
 gapointer%node_width(*)=                     0                     0

********** Integral sort section *************


 workspace allocation information: lencor=  52428799

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =  412.532574920                                          
 MCSCF energy =    -711.472179149                                                
 SIFS file created by program tran.      LIN-CMFP2-1.LUNET 17:56:49.247 21-Jan-21

 input energy(*) values:
 energy( 1)=  4.125325749203E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   4.125325749203E+02

 nsym = 1 nmot=  74

 symmetry  =    1
 slabel(*) = a   
 nmpsy(*)  =   74

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044  45:tout:045  46:tout:046  47:tout:047  48:tout:048  49:tout:049  50:tout:050
  51:tout:051  52:tout:052  53:tout:053  54:tout:054  55:tout:055  56:tout:056  57:tout:057  58:tout:058  59:tout:059  60:tout:060
  61:tout:061  62:tout:062  63:tout:063  64:tout:064  65:tout:065  66:tout:066  67:tout:067  68:tout:068  69:tout:069  70:tout:070
  71:tout:071  72:tout:072  73:tout:073  74:tout:074

 input parameters:
 prnopt=  0
 ldamin=   32767 ldamax=  524288 ldainc=    4096
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    6144
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  86 nfctd =  12 nfvtc =   0 nmot  =  74
 nlevel =  74 niot  =  23 lowinl=  52
 orbital-to-level map(*)
   -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  52  53  54  55  56  57  58  59
   60  61  62  63  64  65  66  67  68  69  70  71  72  73  74   1   2   3   4   5
    6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25
   26  27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45
   46  47  48  49  50  51
 compressed map(*)
   52  53  54  55  56  57  58  59  60  61  62  63  64  65  66  67  68  69  70  71
   72  73  74   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17
   18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37
   38  39  40  41  42  43  44  45  46  47  48  49  50  51
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  0.  0.
   0.  0.  0.
  ... transition density matrix calculation requested 
  ... setting rmu(*) to zero 

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -8.812792647683E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      5550
 number with all external indices:      2652
 number with half external - half internal indices:      2346
 number with all internal indices:       552

 indxof: off-diagonal integral statistics.
    4-external integrals: num=     944775 strt=          1
    3-external integrals: num=    1612875 strt=     944776
    2-external integrals: num=    1097928 strt=    2557651
    1-external integrals: num=     351900 strt=    3655579
    0-external integrals: num=      44022 strt=    4007479

 total number of off-diagonal integrals:     4051500


 indxof(2nd)  ittp=   3 numx(ittp)=     1097928
 indxof(2nd)  ittp=   4 numx(ittp)=      351900
 indxof(2nd)  ittp=   5 numx(ittp)=       44022

 intermediate da file sorting parameters:
 nbuk=   2 lendar=  524288 nipbk=  349524 nipsg=  51432198
 pro2e        1    2776    5551    8326   11101   11377   11653   14428  713476 1412524
  1936812 1945004 1950464 1972303

 pro2e:   3851700 integrals read in   706 records.

 pro2e:   2435199 integrals 34-ext integrals skipped.
 pro1e        1    2776    5551    8326   11101   11377   11653   14428  713476 1412524
  1936812 1945004 1950464 1972303
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     7  records of                 524288 
 WP =              29360128 Bytes
 putdg        1    2776    5551    8326   14470  538758  888283   14428  713476 1412524
  1936812 1945004 1950464 1972303

 putf:       4 buffers of length    6144 written to file 12
 diagonal integral file completed.
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd   1097928         3   2557651   2557651   1097928   4051500
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd    351900         4   3655579   3655579    351900   4051500
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     44022         5   4007479   4007479     44022   4051500

 putf:     246 buffers of length    6144 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  6144
 diagfile 4ext:    2652 2ext:    2346 0ext:     552
 fil4w,fil4x  :  944775 fil3w,fil3x : 1612875
 ofdgint  2ext: 1097928 1ext:  351900 0ext:   44022so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    3975 minbl3    3975 maxbl2    3978nbas:  51   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  52428799

 core energy values from the integral file:
 energy( 1)=  4.125325749203E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.812792647683E+02, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.687466898479E+02
 nmot  =    86 niot  =    23 nfct  =    12 nfvt  =     0
 nrow  =   196 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       2555      770     1785        0        0
 nvalwt,nvalw:     2555      770     1785        0        0
 ncsft:           91805
 total number of valid internal walks:    2555
 nvalz,nvaly,nvalx,nvalw =      770    1785       0       0

 cisrt info file parameters:
 file number  12 blocksize   6144
 mxbld   6144
 nd4ext,nd2ext,nd0ext  2652  2346   552
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int   944775  1612875  1097928   351900    44022        0        0        0
 minbl4,minbl3,maxbl2  3975  3975  3978
 maxbuf 30006
 number of external orbitals per symmetry block:  51
 nmsym   1 number of internal orbitals  23
 bummer (warning):transition densities: resetting ref occupation number to 00
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    23                     1
 block size     0
 pthz,pthy,pthx,pthw:   770  1785     0     0 total internal walks:    2555
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
setref: retained number of references =    50
 setref: total/valid number of walks=                   770
                   770
 nmb.of records onel     1
 nmb.of records 2-ext   179
 nmb.of records 1-ext    58
 nmb.of records 0-ext     8
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            70083
    threx             60019
    twoex             33735
    onex              11551
    allin              6144
    diagon             7292
               =======
   maximum            70083
 
  __ static summary __ 
   reflst               770
   hrfspc               770
               -------
   static->             770
 
  __ core required  __ 
   totstc               770
   max n-ex           70083
               -------
   totnec->           70853
 
  __ core available __ 
   totspc          52428799
   totnec -           70853
               -------
   totvec->        52357946

 number of external paths / symmetry
 vertex x    1275
 vertex w    1326
segment: free space=    52357946
 reducing frespc by                 12484 to               52345462 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         770|       770|         0|       770|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1785|     91035|       770|      1785|       770|         2|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=        7140DP  drtbuffer=        5340 DP

dimension of the ci-matrix ->>>     91805

 executing brd_struct for civct
 gentasklist: ntask=                     6
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1    1785     770      91035        770    1785     770
     2  1   1     1      allint zz    OX  1 1     770     770        770        770     770     770
     3  2   2     5      0ex2ex yy    OX  2 2    1785    1785      91035      91035    1785    1785
     4  2   2    42      four-ext y   4X  2 2    1785    1785      91035      91035    1785    1785
     5  1   1    75      dg-024ext z  OX  1 1     770     770        770        770     770     770
     6  2   2    76      dg-024ext y  OX  2 2    1785    1785      91035      91035    1785    1785
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=   5.000 N=  1 (task/type/sgbra)=(   1/11/0) (
REDTASK #   2 TIME=   4.000 N=  1 (task/type/sgbra)=(   2/ 1/0) (
REDTASK #   3 TIME=   3.000 N=  1 (task/type/sgbra)=(   3/ 5/0) (
REDTASK #   4 TIME=   2.000 N=  1 (task/type/sgbra)=(   4/42/1) (
REDTASK #   5 TIME=   1.000 N=  1 (task/type/sgbra)=(   5/75/1) (
REDTASK #   6 TIME=   0.000 N=  1 (task/type/sgbra)=(   6/76/1) (
 initializing v-file: 1:                 91805

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      9 vectors will be written to unit 11 beginning with logical record   1

            9 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      203100 2x:           0 4x:           0
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:           0    task #
 reference space has dimension      50
 dsyevx: computed roots 1 to   18(converged:  18)

    root           eigenvalues
    ----           ------------
       1        -711.5231997929
       2        -711.4098999536
       3        -711.3787528665
       4        -711.3676168670
       5        -711.3027125187
       6        -711.1914476476
       7        -711.1591865208
       8        -711.1279498745
       9        -711.1047440543
      10        -711.0912045108
      11        -711.0694705528
      12        -711.0507875712
      13        -711.0300201650
      14        -711.0235628180
      15        -710.9975401017
      16        -710.9963451724
      17        -710.9774849400
      18        -710.9553784516

 strefv generated    9 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   770

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   770

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   770

         vector  3 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   770

         vector  4 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   770)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             91805
 number of initial trial vectors:                         9
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              10
 number of roots to converge:                             4
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:           0    task #     4:           0
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000    -0.00000000    -0.00000000  -242.62092702
   ht   5    -0.00000000    -0.00000000     0.00000000     0.00000000  -242.55602267
   ht   6     0.00000000     0.00000000     0.00000000    -0.00000000     0.00000000  -242.44475780
   ht   7    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000  -242.41249667
   ht   8    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000  -242.38126003
   ht   9     0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000    -0.00000000

                ht   9
   ht   9  -242.35805421

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   -1.00000      -2.630279E-13   8.863271E-13  -1.251681E-13  -9.371518E-14  -8.056099E-14  -2.320444E-13  -3.652639E-14
 ref    2  -2.633276E-13    1.00000      -3.358711E-13  -5.912104E-12   3.084150E-13   3.933199E-13   6.263997E-14  -2.750924E-13
 ref    3   8.862806E-13   3.358251E-13    1.00000      -2.039285E-13  -8.508551E-13  -1.407121E-13  -2.837943E-13  -7.766140E-16
 ref    4  -1.252991E-13   5.912021E-12   2.038023E-13    1.00000       3.336827E-14   1.785510E-13   6.860110E-14   4.768000E-13

              v      9
 ref    1   1.180228E-14
 ref    2   1.302033E-13
 ref    3   1.489786E-14
 ref    4  -5.029289E-13

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1    1.00000        1.00000        1.00000        1.00000       8.289701E-25   2.128710E-25   1.430136E-25   3.043489E-25

              v      9
 ref    1   2.702516E-25

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -1.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000
 ref:   2    -0.00000000     1.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000
 ref:   4    -0.00000000     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9
 ref:   1     0.00000000
 ref:   2     0.00000000
 ref:   3     0.00000000
 ref:   4    -0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -711.5231997929  1.1369E-13  1.1595E-01  3.8581E-01  1.0000E-04   
 mr-sdci #  1  2   -711.4098999536  3.9790E-13  0.0000E+00  3.4757E-01  1.0000E-04   
 mr-sdci #  1  3   -711.3787528665  5.6843E-14  0.0000E+00  3.6252E-01  1.0000E-04   
 mr-sdci #  1  4   -711.3676168670 -3.6948E-13  0.0000E+00  3.6161E-01  1.0000E-04   
 mr-sdci #  1  5   -711.3027125187 -8.5265E-14  0.0000E+00  4.1642E-01  1.0000E-04   
 mr-sdci #  1  6   -711.1914476476 -1.1369E-13  0.0000E+00  4.1416E-01  1.0000E-04   
 mr-sdci #  1  7   -711.1591865208 -8.5265E-14  0.0000E+00  4.2423E-01  1.0000E-04   
 mr-sdci #  1  8   -711.1279498745  2.5580E-13  0.0000E+00  3.8904E-01  1.0000E-04   
 mr-sdci #  1  9   -711.1047440543 -2.5580E-13  0.0000E+00  4.2685E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.006225
time for cinew                         0.065001
time for eigenvalue solver             0.000143
time for vector access                 0.000001

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -711.5231997929  1.1369E-13  1.1595E-01  3.8581E-01  1.0000E-04   
 mr-sdci #  1  2   -711.4098999536  3.9790E-13  0.0000E+00  3.4757E-01  1.0000E-04   
 mr-sdci #  1  3   -711.3787528665  5.6843E-14  0.0000E+00  3.6252E-01  1.0000E-04   
 mr-sdci #  1  4   -711.3676168670 -3.6948E-13  0.0000E+00  3.6161E-01  1.0000E-04   
 mr-sdci #  1  5   -711.3027125187 -8.5265E-14  0.0000E+00  4.1642E-01  1.0000E-04   
 mr-sdci #  1  6   -711.1914476476 -1.1369E-13  0.0000E+00  4.1416E-01  1.0000E-04   
 mr-sdci #  1  7   -711.1591865208 -8.5265E-14  0.0000E+00  4.2423E-01  1.0000E-04   
 mr-sdci #  1  8   -711.1279498745  2.5580E-13  0.0000E+00  3.8904E-01  1.0000E-04   
 mr-sdci #  1  9   -711.1047440543 -2.5580E-13  0.0000E+00  4.2685E-01  1.0000E-04   
 
    4 of the  10 expansion vectors are transformed.
    4 of the   9 matrix-vector products are transformed.

    4 expansion eigenvectors written to unit nvfile (= 11)
    4 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             91805
 number of initial trial vectors:                         4
 number of initial matrix-vector products:                4
 maximum dimension of the subspace vectors:              20
 number of roots to converge:                             4
 number of iterations:                                  150
 residual norm convergence criteria:               0.001000  0.001000  0.001000  0.001000

          starting ci iteration   1

 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1   -1.00000      -1.734272E-13   5.844771E-14   1.380271E-14
 ref    2   1.733787E-13   -1.00000       3.228650E-13  -5.036816E-12
 ref    3   5.838732E-14   3.227592E-13    1.00000      -9.746856E-14
 ref    4   1.380337E-14  -5.036782E-12   9.747017E-14    1.00000    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1    1.00000        1.00000        1.00000        1.00000    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -1.00000000    -0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000    -1.00000000     0.00000000    -0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000    -0.00000000
 ref:   4     0.00000000    -0.00000000     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -711.5231997929  2.8422E-14  1.1595E-01  3.8581E-01  1.0000E-03   
 mr-sdci #  1  2   -711.4098999536 -2.8422E-14  0.0000E+00  3.4757E-01  1.0000E-03   
 mr-sdci #  1  3   -711.3787528665  1.1369E-13  0.0000E+00  3.6252E-01  1.0000E-03   
 mr-sdci #  1  4   -711.3676168670 -2.8422E-14  0.0000E+00  3.6161E-01  1.0000E-03   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001693
time for cinew                         0.061776
time for eigenvalue solver             0.000076
time for vector access                 0.000001

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.965776      -2.527202E-04   3.106720E-02  -6.192295E-04   0.257511    
 ref    2   2.215039E-04    1.00000       7.317076E-05  -1.147995E-06   1.418318E-04
 ref    3   2.675882E-02   6.433416E-05  -0.999437      -6.054518E-04   2.021794E-02
 ref    4  -5.441013E-04  -1.008490E-06   5.824385E-04   -1.00000      -4.343290E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.933439        1.00000       0.999840        1.00000       6.672093E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96577555    -0.00025272     0.03106720    -0.00061923     0.25751109
 ref:   2     0.00022150     0.99999996     0.00007317    -0.00000115     0.00014183
 ref:   3     0.02675882     0.00006433    -0.99943726    -0.00060545     0.02021794
 ref:   4    -0.00054410    -0.00000101     0.00058244    -0.99999959    -0.00043433

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -711.6178693168  9.4670E-02  8.2388E-03  8.8099E-02  1.0000E-03   
 mr-sdci #  2  2   -711.4098999680  1.4426E-08  0.0000E+00  3.4757E-01  1.0000E-03   
 mr-sdci #  2  3   -711.3790664124  3.1355E-04  0.0000E+00  3.6130E-01  1.0000E-03   
 mr-sdci #  2  4   -711.3676170105  1.4348E-07  0.0000E+00  3.6161E-01  1.0000E-03   
 mr-sdci #  2  5   -710.1937079779 -1.1090E+00  0.0000E+00  5.9291E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001146
time for cinew                         0.058523
time for eigenvalue solver             0.000086
time for vector access                 0.000001

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.957555      -2.928670E-04   3.972209E-02  -7.970451E-04  -0.148097       0.244084    
 ref    2   2.419084E-04    1.00000       1.232465E-04  -1.978386E-06  -1.671058E-04   1.293870E-04
 ref    3   3.121650E-02   1.070814E-04  -0.998675      -1.330234E-03  -3.685048E-02   1.769667E-02
 ref    4  -6.498437E-04  -1.778330E-06   1.271673E-03  -0.999999       9.046400E-04  -3.741422E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.917887        1.00000       0.998932       0.999999       2.329163E-02   5.989018E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.95755509    -0.00029287     0.03972209    -0.00079705    -0.14809736     0.24408369
 ref:   2     0.00024191     0.99999994     0.00012325    -0.00000198    -0.00016711     0.00012939
 ref:   3     0.03121650     0.00010708    -0.99867543    -0.00133023    -0.03685048     0.01769667
 ref:   4    -0.00064984    -0.00000178     0.00127167    -0.99999850     0.00090464    -0.00037414

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -711.6244414626  6.5721E-03  1.7507E-03  4.7070E-02  1.0000E-03   
 mr-sdci #  3  2   -711.4098999738  5.7883E-09  0.0000E+00  3.4757E-01  1.0000E-03   
 mr-sdci #  3  3   -711.3794084891  3.4208E-04  0.0000E+00  3.6086E-01  1.0000E-03   
 mr-sdci #  3  4   -711.3676172078  1.9729E-07  0.0000E+00  3.6161E-01  1.0000E-03   
 mr-sdci #  3  5   -711.0018227209  8.0811E-01  0.0000E+00  6.6009E-01  1.0000E-04   
 mr-sdci #  3  6   -710.1608011802 -1.0306E+00  0.0000E+00  5.7738E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001324
time for cinew                         0.057799
time for eigenvalue solver             0.000091
time for vector access                 0.000001

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.954283       4.314631E-04  -4.946963E-02   9.347411E-04   0.169008      -0.131782      -0.202402    
 ref    2   2.997655E-04   -1.00000      -6.016247E-04   9.824876E-06   5.045762E-04  -3.021455E-04   4.675960E-05
 ref    3   3.523171E-02  -5.472116E-04   0.996714       3.801041E-03   6.822232E-02  -2.517738E-02  -4.124029E-03
 ref    4  -7.574183E-04   1.068298E-05  -3.646666E-03   0.999991      -1.803756E-03   6.577744E-04   4.020882E-06

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.911897        1.00000       0.995900       0.999998       3.322144E-02   1.800101E-02   4.098338E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.95428250     0.00043146    -0.04946963     0.00093474     0.16900782    -0.13178236    -0.20240151
 ref:   2     0.00029977    -0.99999960    -0.00060162     0.00000982     0.00050458    -0.00030215     0.00004676
 ref:   3     0.03523171    -0.00054721     0.99671399     0.00380104     0.06822232    -0.02517738    -0.00412403
 ref:   4    -0.00075742     0.00001068    -0.00364667     0.99999122    -0.00180376     0.00065777     0.00000402

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -711.6258421708  1.4007E-03  3.2706E-04  1.9403E-02  1.0000E-03   
 mr-sdci #  4  2   -711.4099001358  1.6202E-07  0.0000E+00  3.4757E-01  1.0000E-03   
 mr-sdci #  4  3   -711.3805905545  1.1821E-03  0.0000E+00  3.5870E-01  1.0000E-03   
 mr-sdci #  4  4   -711.3676179691  7.6126E-07  0.0000E+00  3.6161E-01  1.0000E-03   
 mr-sdci #  4  5   -711.0743390527  7.2516E-02  0.0000E+00  5.6838E-01  1.0000E-04   
 mr-sdci #  4  6   -710.2848859487  1.2408E-01  0.0000E+00  7.5720E-01  1.0000E-04   
 mr-sdci #  4  7   -710.0879674146 -1.0712E+00  0.0000E+00  6.3364E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001605
time for cinew                         0.063365
time for eigenvalue solver             0.000108
time for vector access                 0.000001

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.951448      -6.854144E-04   7.302867E-02   1.322045E-03  -0.162439      -0.116623       0.129122      -0.180974    
 ref    2  -3.192776E-04   0.999998       1.641552E-03   2.468717E-05  -1.428228E-03  -3.003906E-04   2.318272E-05   4.587209E-05
 ref    3  -3.674363E-02   1.306403E-03  -0.979260       9.412691E-03  -0.197336      -2.404166E-02   9.059910E-03  -2.841037E-03
 ref    4   8.086539E-04  -2.836210E-05   8.239118E-03   0.999944       6.576941E-03   6.521240E-04  -6.724853E-05   6.658080E-06

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.906604       0.999997       0.964354       0.999978       6.537322E-02   1.417941E-02   1.675470E-02   3.275963E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95144795    -0.00068541     0.07302867     0.00132205    -0.16243868    -0.11662288     0.12912249    -0.18097391
 ref:   2    -0.00031928     0.99999754     0.00164155     0.00002469    -0.00142823    -0.00030039     0.00002318     0.00004587
 ref:   3    -0.03674363     0.00130640    -0.97925984     0.00941269    -0.19733627    -0.02404166     0.00905991    -0.00284104
 ref:   4     0.00080865    -0.00002836     0.00823912     0.99994389     0.00657694     0.00065212    -0.00006725     0.00000666

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -711.6261691281  3.2696E-04  6.0454E-05  9.1214E-03  1.0000E-03   
 mr-sdci #  5  2   -711.4099003771  2.4124E-07  0.0000E+00  3.4757E-01  1.0000E-03   
 mr-sdci #  5  3   -711.3832633812  2.6728E-03  0.0000E+00  3.5494E-01  1.0000E-03   
 mr-sdci #  5  4   -711.3676199744  2.0054E-06  0.0000E+00  3.6161E-01  1.0000E-03   
 mr-sdci #  5  5   -711.2780760345  2.0374E-01  0.0000E+00  4.1594E-01  1.0000E-04   
 mr-sdci #  5  6   -710.2864032269  1.5173E-03  0.0000E+00  7.5167E-01  1.0000E-04   
 mr-sdci #  5  7   -710.2081720493  1.2020E-01  0.0000E+00  7.0315E-01  1.0000E-04   
 mr-sdci #  5  8   -710.0804507448 -1.0475E+00  0.0000E+00  6.4647E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002241
time for cinew                         0.066417
time for eigenvalue solver             0.000000
time for vector access                 0.000001

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489

                ht   9
   ht   9    -0.01444323

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950420       1.810442E-03   0.131628       2.456278E-03  -0.108380      -0.142190      -0.119936       0.162530    
 ref    2   3.299122E-04  -0.999958       8.680442E-03   8.822765E-05  -3.016104E-03   3.122129E-05  -3.003596E-04  -5.234783E-05
 ref    3   3.736503E-02  -5.121083E-03  -0.800048       3.023343E-02  -0.597389      -1.002257E-02  -2.426202E-02   1.683983E-03
 ref    4  -8.279334E-04   1.146306E-04   1.409034E-02   0.999400       3.162731E-02   9.879104E-05   6.537963E-04   6.150184E-07

              v      9
 ref    1  -8.125292E-02
 ref    2  -4.974693E-05
 ref    3  -4.648432E-03
 ref    4   3.310985E-05

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904695       0.999945       0.657676       0.999720       0.369630       2.031840E-02   1.497372E-02   2.641885E-02

              v      9
 ref    1   6.623648E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95041991     0.00181044     0.13162815     0.00245628    -0.10837981    -0.14218979    -0.11993564     0.16253002
 ref:   2     0.00032991    -0.99995767     0.00868044     0.00008823    -0.00301610     0.00003122    -0.00030036    -0.00005235
 ref:   3     0.03736503    -0.00512108    -0.80004755     0.03023343    -0.59738927    -0.01002257    -0.02426202     0.00168398
 ref:   4    -0.00082793     0.00011463     0.01409034     0.99939984     0.03162731     0.00009879     0.00065380     0.00000062

                ci   9
 ref:   1    -0.08125292
 ref:   2    -0.00004975
 ref:   3    -0.00464843
 ref:   4     0.00003311

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -711.6262318153  6.2687E-05  8.1761E-06  3.4162E-03  1.0000E-03   
 mr-sdci #  6  2   -711.4099017495  1.3724E-06  0.0000E+00  3.4756E-01  1.0000E-03   
 mr-sdci #  6  3   -711.3941657383  1.0902E-02  0.0000E+00  3.0173E-01  1.0000E-03   
 mr-sdci #  6  4   -711.3676271875  7.2131E-06  0.0000E+00  3.6158E-01  1.0000E-03   
 mr-sdci #  6  5   -711.3523373314  7.4261E-02  0.0000E+00  3.1700E-01  1.0000E-04   
 mr-sdci #  6  6   -710.4289040337  1.4250E-01  0.0000E+00  5.7789E-01  1.0000E-04   
 mr-sdci #  6  7   -710.2863727215  7.8201E-02  0.0000E+00  7.5164E-01  1.0000E-04   
 mr-sdci #  6  8   -710.0830855741  2.6348E-03  0.0000E+00  6.6818E-01  1.0000E-04   
 mr-sdci #  6  9   -709.8771914456 -1.2276E+00  0.0000E+00  7.4738E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001876
time for cinew                         0.068761
time for eigenvalue solver             0.000000
time for vector access                 0.000001

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597

                ht   9         ht  10
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950263       5.857687E-03  -0.147743       3.166748E-03  -7.387115E-02  -0.132256       9.030432E-02   0.192249    
 ref    2   3.346901E-04  -0.999356      -3.572507E-02   1.571791E-04  -3.288602E-03   1.770765E-04   2.817636E-04   4.522271E-05
 ref    3   3.761037E-02  -2.076423E-02   0.651207       5.043232E-02  -0.755508      -3.522900E-03   2.123848E-02   9.813219E-03
 ref    4  -8.307966E-04   4.213493E-04  -1.253912E-02   0.998365       5.576126E-02   2.667891E-04  -6.339497E-04  -6.792581E-05

              v      9       v     10
 ref    1  -1.326667E-02   8.299745E-02
 ref    2   1.834099E-04   7.245861E-05
 ref    3   1.220495E-02   6.204655E-03
 ref    4  -7.179389E-05  -4.032559E-05

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904415       0.999178       0.447333       0.999286       0.579370       1.750412E-02   8.606425E-03   3.705605E-02

              v      9       v     10
 ref    1   3.250041E-04   6.927082E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95026302     0.00585769    -0.14774333     0.00316675    -0.07387115    -0.13225585     0.09030432     0.19224916
 ref:   2     0.00033469    -0.99935610    -0.03572507     0.00015718    -0.00328860     0.00017708     0.00028176     0.00004522
 ref:   3     0.03761037    -0.02076423     0.65120742     0.05043232    -0.75550813    -0.00352290     0.02123848     0.00981322
 ref:   4    -0.00083080     0.00042135    -0.01253912     0.99836471     0.05576126     0.00026679    -0.00063395    -0.00006793

                ci   9         ci  10
 ref:   1    -0.01326667     0.08299745
 ref:   2     0.00018341     0.00007246
 ref:   3     0.01220495     0.00620466
 ref:   4    -0.00007179    -0.00004033

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -711.6262393939  7.5787E-06  1.0779E-06  1.1950E-03  1.0000E-03   
 mr-sdci #  7  2   -711.4099077913  6.0418E-06  0.0000E+00  3.4743E-01  1.0000E-03   
 mr-sdci #  7  3   -711.4043002315  1.0134E-02  0.0000E+00  2.4989E-01  1.0000E-03   
 mr-sdci #  7  4   -711.3676328379  5.6503E-06  0.0000E+00  3.6155E-01  1.0000E-03   
 mr-sdci #  7  5   -711.3607433893  8.4061E-03  0.0000E+00  3.3131E-01  1.0000E-04   
 mr-sdci #  7  6   -710.6300242856  2.0112E-01  0.0000E+00  5.2232E-01  1.0000E-04   
 mr-sdci #  7  7   -710.2911752110  4.8025E-03  0.0000E+00  7.6304E-01  1.0000E-04   
 mr-sdci #  7  8   -710.1091074771  2.6022E-02  0.0000E+00  6.6234E-01  1.0000E-04   
 mr-sdci #  7  9   -709.9726906366  9.5499E-02  0.0000E+00  6.6963E-01  1.0000E-04   
 mr-sdci #  7 10   -709.8755498349  2.4113E+02  0.0000E+00  7.4001E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001730
time for cinew                         0.062617
time for eigenvalue solver             0.000000
time for vector access                 0.000001

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453

                ht   9         ht  10         ht  11
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950233       0.131651       7.163559E-02   3.553491E-03   5.984351E-02   0.117305      -4.897473E-03  -0.210067    
 ref    2  -3.348523E-04  -0.480996       0.876717       1.916856E-04   3.034187E-03  -5.624151E-05  -3.093666E-04  -9.514704E-05
 ref    3  -3.768403E-02  -0.515634      -0.285713       6.462081E-02   0.803784      -3.510325E-03  -1.139303E-02  -2.292828E-02
 ref    4   8.311732E-04   9.790365E-03   5.402694E-03   0.997347      -7.192607E-02  -4.933948E-04   5.042280E-04   3.464099E-04

              v      9       v     10       v     11
 ref    1  -7.883404E-02  -1.406315E-02   8.170214E-02
 ref    2   8.157713E-06   1.807043E-04   9.113289E-05
 ref    3   6.019313E-03   1.001923E-02   4.488335E-03
 ref    4  -1.040266E-04  -4.654360E-05  -1.989764E-05

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904363       0.514664       0.855426       0.998889       0.654832       1.377313E-02   1.541364E-04   4.465377E-02

              v      9       v     10       v     11
 ref    1   6.251049E-03   2.981920E-04   6.695394E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95023265     0.13165139     0.07163559     0.00355349     0.05984351     0.11730540    -0.00489747    -0.21006651
 ref:   2    -0.00033485    -0.48099589     0.87671743     0.00019169     0.00303419    -0.00005624    -0.00030937    -0.00009515
 ref:   3    -0.03768403    -0.51563432    -0.28571349     0.06462081     0.80378388    -0.00351033    -0.01139303    -0.02292828
 ref:   4     0.00083117     0.00979036     0.00540269     0.99734662    -0.07192607    -0.00049339     0.00050423     0.00034641

                ci   9         ci  10         ci  11
 ref:   1    -0.07883404    -0.01406315     0.08170214
 ref:   2     0.00000816     0.00018070     0.00009113
 ref:   3     0.00601931     0.01001923     0.00448833
 ref:   4    -0.00010403    -0.00004654    -0.00001990

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -711.6262403610  9.6704E-07  0.0000E+00  4.1669E-04  1.0000E-03   
 mr-sdci #  8  2   -711.4102785902  3.7080E-04  5.7080E-02  2.5597E-01  1.0000E-03   
 mr-sdci #  8  3   -711.4097867469  5.4865E-03  0.0000E+00  3.2325E-01  1.0000E-03   
 mr-sdci #  8  4   -711.3676363105  3.4727E-06  0.0000E+00  3.6153E-01  1.0000E-03   
 mr-sdci #  8  5   -711.3629241087  2.1807E-03  0.0000E+00  3.3749E-01  1.0000E-04   
 mr-sdci #  8  6   -710.7982662570  1.6824E-01  0.0000E+00  4.9740E-01  1.0000E-04   
 mr-sdci #  8  7   -710.3345180876  4.3343E-02  0.0000E+00  7.1988E-01  1.0000E-04   
 mr-sdci #  8  8   -710.1788505670  6.9743E-02  0.0000E+00  6.1713E-01  1.0000E-04   
 mr-sdci #  8  9   -710.0437864737  7.1096E-02  0.0000E+00  7.5771E-01  1.0000E-04   
 mr-sdci #  8 10   -709.9690008649  9.3451E-02  0.0000E+00  6.7671E-01  1.0000E-04   
 mr-sdci #  8 11   -709.8610043556  2.4111E+02  0.0000E+00  7.3528E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002613
time for cinew                         0.136336
time for eigenvalue solver             0.000213
time for vector access                 0.000001

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950227      -8.254889E-02  -0.111799      -2.450637E-02   7.472750E-02  -0.119583       1.869432E-02  -0.126102    
 ref    2  -3.936854E-04   0.715401      -0.664021       5.431942E-02  -0.134642       1.960562E-02  -0.130211       5.499148E-02
 ref    3  -3.773338E-02   0.521139       0.441451      -0.217417       0.680179       2.127459E-02  -0.109101       3.915126E-02
 ref    4   8.301696E-04   6.264224E-03  -8.476571E-03  -0.952442      -0.304511       9.777627E-04  -3.400709E-03   1.234428E-03

              v      9       v     10       v     11       v     12
 ref    1  -0.177078      -5.709198E-02  -2.045785E-02  -7.736309E-02
 ref    2  -5.355684E-02   5.180997E-02   7.224966E-03  -1.515489E-02
 ref    3  -6.714349E-02   5.571002E-02   1.747796E-02  -1.978945E-02
 ref    4  -8.061614E-04   9.384401E-04   8.416073E-05  -2.772303E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904356       0.790239       0.648374       0.957967       0.579083       1.513812E-02   2.921893E-02   2.046002E-02

              v      9       v     10       v     11       v     12
 ref    1   3.873383E-02   9.048254E-03   7.762100E-04   6.606418E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022680    -0.08254889    -0.11179904    -0.02450637     0.07472750    -0.11958334     0.01869432    -0.12610159
 ref:   2    -0.00039369     0.71540135    -0.66402101     0.05431942    -0.13464171     0.01960562    -0.13021114     0.05499148
 ref:   3    -0.03773338     0.52113902     0.44145086    -0.21741734     0.68017926     0.02127459    -0.10910063     0.03915126
 ref:   4     0.00083017     0.00626422    -0.00847657    -0.95244190    -0.30451107     0.00097776    -0.00340071     0.00123443

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.17707793    -0.05709198    -0.02045785    -0.07736309
 ref:   2    -0.05355684     0.05180997     0.00722497    -0.01515489
 ref:   3    -0.06714349     0.05571002     0.01747796    -0.01978945
 ref:   4    -0.00080616     0.00093844     0.00008416    -0.00027723

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -711.6262403661  5.1116E-09  0.0000E+00  4.1050E-04  1.0000E-03   
 mr-sdci #  9  2   -711.4642415584  5.3963E-02  2.9216E-02  1.5517E-01  1.0000E-03   
 mr-sdci #  9  3   -711.4098427089  5.5962E-05  0.0000E+00  2.8457E-01  1.0000E-03   
 mr-sdci #  9  4   -711.3677953471  1.5904E-04  0.0000E+00  3.5709E-01  1.0000E-03   
 mr-sdci #  9  5   -711.3659492289  3.0251E-03  0.0000E+00  3.2054E-01  1.0000E-04   
 mr-sdci #  9  6   -710.8003667432  2.1005E-03  0.0000E+00  4.9733E-01  1.0000E-04   
 mr-sdci #  9  7   -710.5175436193  1.8303E-01  0.0000E+00  7.0054E-01  1.0000E-04   
 mr-sdci #  9  8   -710.2056653514  2.6815E-02  0.0000E+00  6.3043E-01  1.0000E-04   
 mr-sdci #  9  9   -710.1529946866  1.0921E-01  0.0000E+00  6.1386E-01  1.0000E-04   
 mr-sdci #  9 10   -709.9858871389  1.6886E-02  0.0000E+00  7.8103E-01  1.0000E-04   
 mr-sdci #  9 11   -709.9687342636  1.0773E-01  0.0000E+00  6.7135E-01  1.0000E-04   
 mr-sdci #  9 12   -709.8552023937  2.4111E+02  0.0000E+00  7.4793E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000295
time for cinew                         0.070694
time for eigenvalue solver             0.000177
time for vector access                 0.000002

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950224      -3.734268E-02   0.147895       4.135208E-02   5.407264E-03  -0.113762      -4.741228E-02  -7.015264E-05
 ref    2   3.587958E-04   0.810624       0.336162      -0.437100      -1.416627E-02   2.890389E-02  -3.808545E-02  -0.188679    
 ref    3   3.771076E-02   0.512733      -0.345740       0.761599       6.775797E-02   3.004192E-02  -3.718540E-02  -0.165643    
 ref    4  -8.319003E-04   1.733157E-02   2.247339E-02   8.534511E-02  -0.995901       2.046102E-03  -5.856070E-03  -6.844473E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   0.170915       0.143864       2.695645E-02  -5.771360E-04  -7.900429E-02
 ref    2  -1.201754E-03  -7.695698E-03  -1.333281E-02  -1.195515E-02  -1.897179E-02
 ref    3   1.411441E-02   7.195028E-03  -2.441771E-02  -2.502170E-02  -1.804692E-02
 ref    4   4.084470E-04  -1.607804E-03  -1.365868E-04   1.392152E-03  -1.145584E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904349       0.921701       0.254920       0.780083       0.996640       1.468384E-02   5.115473E-03   6.308394E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.941275E-02   2.081038E-02   1.500657E-03   7.712826E-04   6.928610E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022428    -0.03734268     0.14789550     0.04135208     0.00540726    -0.11376162    -0.04741228    -0.00007015
 ref:   2     0.00035880     0.81062356     0.33616239    -0.43710026    -0.01416627     0.02890389    -0.03808545    -0.18867861
 ref:   3     0.03771076     0.51273350    -0.34574040     0.76159896     0.06775797     0.03004192    -0.03718540    -0.16564259
 ref:   4    -0.00083190     0.01733157     0.02247339     0.08534511    -0.99590097     0.00204610    -0.00585607    -0.00684447

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.17091496     0.14386385     0.02695645    -0.00057714    -0.07900429
 ref:   2    -0.00120175    -0.00769570    -0.01333281    -0.01195515    -0.01897179
 ref:   3     0.01411441     0.00719503    -0.02441771    -0.02502170    -0.01804692
 ref:   4     0.00040845    -0.00160780    -0.00013659     0.00139215    -0.00114558

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -711.6262403684  2.3484E-09  0.0000E+00  4.1285E-04  1.0000E-03   
 mr-sdci # 10  2   -711.4784673737  1.4226E-02  1.3684E-02  1.0693E-01  1.0000E-03   
 mr-sdci # 10  3   -711.4259814628  1.6139E-02  0.0000E+00  1.6877E-01  1.0000E-03   
 mr-sdci # 10  4   -711.3798746131  1.2079E-02  0.0000E+00  3.3648E-01  1.0000E-03   
 mr-sdci # 10  5   -711.3675443962  1.5952E-03  0.0000E+00  3.6146E-01  1.0000E-04   
 mr-sdci # 10  6   -710.8013288904  9.6215E-04  0.0000E+00  4.9173E-01  1.0000E-04   
 mr-sdci # 10  7   -710.7573962467  2.3985E-01  0.0000E+00  6.3384E-01  1.0000E-04   
 mr-sdci # 10  8   -710.3267166478  1.2105E-01  0.0000E+00  6.7093E-01  1.0000E-04   
 mr-sdci # 10  9   -710.1953711885  4.2377E-02  0.0000E+00  6.2256E-01  1.0000E-04   
 mr-sdci # 10 10   -710.1146247534  1.2874E-01  0.0000E+00  6.0097E-01  1.0000E-04   
 mr-sdci # 10 11   -709.9690248189  2.9056E-04  0.0000E+00  6.6951E-01  1.0000E-04   
 mr-sdci # 10 12   -709.9013835302  4.6181E-02  0.0000E+00  8.4020E-01  1.0000E-04   
 mr-sdci # 10 13   -709.8466434238  2.4110E+02  0.0000E+00  7.0720E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003252
time for cinew                         0.074381
time for eigenvalue solver             0.000198
time for vector access                 0.000001

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694
   ht  14    81.97342744    -2.98784114   -17.17741214    -0.88396546     0.23925574    -6.97141174     1.16824278    -0.68253045

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386
   ht  14     0.10605040    -0.01716468    -0.00624847   -22.97705862   132.45417116   -39.08092871

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950224      -2.944431E-02   0.147269      -4.905280E-02  -3.836212E-03   5.889956E-02   0.109022       2.037408E-02
 ref    2   3.483885E-04   0.852864       0.288312       0.371099      -3.914594E-02   3.196010E-02  -1.090980E-02   4.518291E-02
 ref    3   3.770936E-02   0.442074      -0.425687      -0.767559      -7.362410E-03  -6.163749E-02   1.228629E-02  -4.478817E-02
 ref    4  -8.331133E-04   3.345922E-02   3.284116E-02  -7.895493E-03   0.998568       1.101218E-02  -1.266560E-03   1.699633E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.580078E-02  -0.172806       0.137312      -2.481500E-02  -5.960430E-05  -7.955642E-02
 ref    2  -0.211498       4.520930E-02  -5.810724E-04   4.034593E-03  -3.650405E-03  -1.629473E-02
 ref    3  -0.159154       5.441069E-03   9.522959E-03   2.103901E-02  -2.365628E-02  -1.817793E-02
 ref    4  -1.267996E-02   4.454099E-03  -7.944811E-04  -9.477243E-04   2.545442E-03  -6.939229E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904349       0.924792       0.287100       0.729329       0.998739       8.411055E-03   1.215727E-02   4.751454E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   7.088791E-02   3.195528E-02   1.894638E-02   1.075600E-03   5.794276E-04   6.925660E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022447    -0.02944431     0.14726942    -0.04905280    -0.00383621     0.05889956     0.10902152     0.02037408
 ref:   2     0.00034839     0.85286356     0.28831221     0.37109893    -0.03914594     0.03196010    -0.01090980     0.04518291
 ref:   3     0.03770936     0.44207373    -0.42568672    -0.76755876    -0.00736241    -0.06163749     0.01228629    -0.04478817
 ref:   4    -0.00083311     0.03345922     0.03284116    -0.00789549     0.99856800     0.01101218    -0.00126656     0.01699633

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02580078    -0.17280611     0.13731249    -0.02481500    -0.00005960    -0.07955642
 ref:   2    -0.21149840     0.04520930    -0.00058107     0.00403459    -0.00365040    -0.01629473
 ref:   3    -0.15915363     0.00544107     0.00952296     0.02103901    -0.02365628    -0.01817793
 ref:   4    -0.01267996     0.00445410    -0.00079448    -0.00094772     0.00254544    -0.00069392

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -711.6262403687  2.1876E-10  0.0000E+00  4.1093E-04  1.0000E-03   
 mr-sdci # 11  2   -711.4885607991  1.0093E-02  2.7507E-03  6.0534E-02  1.0000E-03   
 mr-sdci # 11  3   -711.4280506578  2.0692E-03  0.0000E+00  1.6509E-01  1.0000E-03   
 mr-sdci # 11  4   -711.3828939097  3.0193E-03  0.0000E+00  3.2110E-01  1.0000E-03   
 mr-sdci # 11  5   -711.3679457806  4.0138E-04  0.0000E+00  3.6087E-01  1.0000E-04   
 mr-sdci # 11  6   -710.8619791701  6.0650E-02  0.0000E+00  4.9380E-01  1.0000E-04   
 mr-sdci # 11  7   -710.7823730276  2.4977E-02  0.0000E+00  5.8636E-01  1.0000E-04   
 mr-sdci # 11  8   -710.4648667931  1.3815E-01  0.0000E+00  7.1117E-01  1.0000E-04   
 mr-sdci # 11  9   -710.3054332063  1.1006E-01  0.0000E+00  6.4378E-01  1.0000E-04   
 mr-sdci # 11 10   -710.1799142223  6.5289E-02  0.0000E+00  6.1954E-01  1.0000E-04   
 mr-sdci # 11 11   -710.1140405115  1.4502E-01  0.0000E+00  5.9918E-01  1.0000E-04   
 mr-sdci # 11 12   -709.9664256636  6.5042E-02  0.0000E+00  6.6297E-01  1.0000E-04   
 mr-sdci # 11 13   -709.8982675310  5.1624E-02  0.0000E+00  8.3841E-01  1.0000E-04   
 mr-sdci # 11 14   -709.8461569655  2.4110E+02  0.0000E+00  7.0822E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002859
time for cinew                         0.078066
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694
   ht  14    81.97342744    -2.98784114   -17.17741214    -0.88396546     0.23925574    -6.97141174     1.16824278    -0.68253045
   ht  15    -3.09214664    -0.59030861    -0.03777813    -0.53593629    -0.31037172     0.11382691    -0.00797571     0.01081256

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386
   ht  14     0.10605040    -0.01716468    -0.00624847   -22.97705862   132.45417116   -39.08092871
   ht  15     0.00307214    -0.00099132     0.00058193     1.17138176    -3.87711034     1.26590525    -0.68397985

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950225       1.933651E-02   0.125933       9.391471E-02  -1.276164E-03  -4.989806E-02  -0.113941       7.010118E-03
 ref    2   3.513752E-04  -0.901460       0.272641      -0.174866       0.111718      -6.874686E-02   2.904115E-02  -0.139584    
 ref    3   3.770706E-02  -0.326951      -0.697616       0.582281      -0.124542       9.535871E-02  -2.545061E-02   9.082820E-02
 ref    4  -8.322707E-04  -5.798062E-02   9.460314E-02  -0.119199      -0.985061      -2.427745E-02   7.619988E-03  -4.407150E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -2.103396E-02   0.166652      -7.721638E-02  -0.123679      -4.452992E-02   6.385849E-02  -3.101213E-02
 ref    2   0.209737       2.103246E-02  -1.488402E-02   7.049649E-03   4.342660E-03   1.259132E-02  -6.556157E-03
 ref    3   0.133983       7.928724E-02  -2.828853E-02   6.572324E-02   3.301254E-02   5.661271E-03  -4.450682E-02
 ref    4   1.621983E-02  -7.418178E-03   2.245807E-03  -1.237188E-02  -3.273445E-03   2.996550E-03   6.928014E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904349       0.923264       0.585810       0.392658       0.998339       1.689862E-02   1.453178E-02   2.972492E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   6.264643E-02   3.455690E-02   6.989188E-03   1.981889E-02   3.102316E-03   4.277478E-03   3.033590E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022463     0.01933651     0.12593290     0.09391471    -0.00127616    -0.04989806    -0.11394117     0.00701012
 ref:   2     0.00035138    -0.90146046     0.27264120    -0.17486584     0.11171775    -0.06874686     0.02904115    -0.13958409
 ref:   3     0.03770706    -0.32695123    -0.69761605     0.58228089    -0.12454217     0.09535871    -0.02545061     0.09082820
 ref:   4    -0.00083227    -0.05798062     0.09460314    -0.11919921    -0.98506126    -0.02427745     0.00761999    -0.04407150

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.02103396     0.16665246    -0.07721638    -0.12367938    -0.04452992     0.06385849    -0.03101213
 ref:   2     0.20973687     0.02103246    -0.01488402     0.00704965     0.00434266     0.01259132    -0.00655616
 ref:   3     0.13398270     0.07928724    -0.02828853     0.06572324     0.03301254     0.00566127    -0.04450682
 ref:   4     0.01621983    -0.00741818     0.00224581    -0.01237188    -0.00327344     0.00299655     0.00692801

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -711.6262403687  2.3164E-11  0.0000E+00  4.1127E-04  1.0000E-03   
 mr-sdci # 12  2   -711.4920580948  3.4973E-03  1.1516E-03  3.8054E-02  1.0000E-03   
 mr-sdci # 12  3   -711.4394266243  1.1376E-02  0.0000E+00  1.7342E-01  1.0000E-03   
 mr-sdci # 12  4   -711.4008854091  1.7991E-02  0.0000E+00  2.2278E-01  1.0000E-03   
 mr-sdci # 12  5   -711.3686099843  6.6420E-04  0.0000E+00  3.6044E-01  1.0000E-04   
 mr-sdci # 12  6   -710.8709236973  8.9445E-03  0.0000E+00  4.9702E-01  1.0000E-04   
 mr-sdci # 12  7   -710.7852119877  2.8390E-03  0.0000E+00  5.7869E-01  1.0000E-04   
 mr-sdci # 12  8   -710.5878991129  1.2303E-01  0.0000E+00  6.4530E-01  1.0000E-04   
 mr-sdci # 12  9   -710.3217833312  1.6350E-02  0.0000E+00  6.5345E-01  1.0000E-04   
 mr-sdci # 12 10   -710.2386014867  5.8687E-02  0.0000E+00  6.1948E-01  1.0000E-04   
 mr-sdci # 12 11   -710.1216917717  7.6513E-03  0.0000E+00  5.9650E-01  1.0000E-04   
 mr-sdci # 12 12   -709.9961184327  2.9693E-02  0.0000E+00  7.2229E-01  1.0000E-04   
 mr-sdci # 12 13   -709.9654714057  6.7204E-02  0.0000E+00  6.6746E-01  1.0000E-04   
 mr-sdci # 12 14   -709.8780108424  3.1854E-02  0.0000E+00  8.2408E-01  1.0000E-04   
 mr-sdci # 12 15   -709.8118748500  2.4107E+02  0.0000E+00  7.3497E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.004906
time for cinew                         0.065506
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694
   ht  14    81.97342744    -2.98784114   -17.17741214    -0.88396546     0.23925574    -6.97141174     1.16824278    -0.68253045
   ht  15    -3.09214664    -0.59030861    -0.03777813    -0.53593629    -0.31037172     0.11382691    -0.00797571     0.01081256
   ht  16    -0.27376100    -0.12997713    -1.62978286     0.18506893    -0.14508938    -0.00462163     0.03774719     0.00866432

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386
   ht  14     0.10605040    -0.01716468    -0.00624847   -22.97705862   132.45417116   -39.08092871
   ht  15     0.00307214    -0.00099132     0.00058193     1.17138176    -3.87711034     1.26590525    -0.68397985
   ht  16    -0.00221367     0.00059808     0.00001265     0.16499599     0.21455517     0.01141016    -0.03577251    -0.30950962

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950225      -1.091384E-02   8.435417E-02   0.133329       2.637177E-03  -3.539947E-02  -0.119800      -9.275311E-03
 ref    2   3.497573E-04   0.939051       0.166285      -3.109759E-02  -0.123901      -4.670150E-02   1.557199E-05   0.150087    
 ref    3   3.770929E-02   0.186198      -0.882243       0.290338       0.155118       4.301119E-02   3.138656E-02  -0.169816    
 ref    4  -8.329106E-04   8.564661E-02   0.141795      -0.100049       0.978473      -9.764685E-03  -7.864759E-03   6.010694E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -1.321271E-02   6.984406E-02  -0.162029      -6.347077E-02   8.943464E-02   8.822876E-02   7.149291E-02  -1.161579E-02
 ref    2   0.109493      -0.161724      -9.800650E-02  -1.946209E-03  -1.689581E-02  -2.867298E-02   8.448628E-03  -2.399903E-02
 ref    3   0.191660       9.070112E-03  -5.841346E-02  -3.778812E-02  -4.623447E-02  -1.923211E-02   9.253618E-03  -3.571049E-02
 ref    4  -1.316849E-02  -3.169367E-02  -1.106329E-02   6.682791E-03   4.069695E-03   1.369475E-04   1.617921E-03   2.296964E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904350       0.923940       0.833226       0.113050       0.996829       5.379464E-03   1.539910E-02   5.506247E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   4.907030E-02   3.211970E-02   3.939315E-02   5.504928E-03   1.043821E-02   8.976347E-03   5.270863E-03   1.991395E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022459    -0.01091384     0.08435417     0.13332879     0.00263718    -0.03539947    -0.11980039    -0.00927531
 ref:   2     0.00034976     0.93905051     0.16628532    -0.03109759    -0.12390119    -0.04670150     0.00001557     0.15008681
 ref:   3     0.03770929     0.18619828    -0.88224340     0.29033815     0.15511845     0.04301119     0.03138656    -0.16981621
 ref:   4    -0.00083291     0.08564661     0.14179486    -0.10004886     0.97847276    -0.00976468    -0.00786476     0.06010694

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.01321271     0.06984406    -0.16202885    -0.06347077     0.08943464     0.08822876     0.07149291    -0.01161579
 ref:   2     0.10949346    -0.16172430    -0.09800650    -0.00194621    -0.01689581    -0.02867298     0.00844863    -0.02399903
 ref:   3     0.19165983     0.00907011    -0.05841346    -0.03778812    -0.04623447    -0.01923211     0.00925362    -0.03571049
 ref:   4    -0.01316849    -0.03169367    -0.01106329     0.00668279     0.00406970     0.00013695     0.00161792     0.00229696

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -711.6262403687  1.0033E-11  0.0000E+00  4.1147E-04  1.0000E-03   
 mr-sdci # 13  2   -711.4939463319  1.8882E-03  7.5234E-04  3.0987E-02  1.0000E-03   
 mr-sdci # 13  3   -711.4602974035  2.0871E-02  0.0000E+00  1.3091E-01  1.0000E-03   
 mr-sdci # 13  4   -711.4122839378  1.1399E-02  0.0000E+00  1.3941E-01  1.0000E-03   
 mr-sdci # 13  5   -711.3687358055  1.2582E-04  0.0000E+00  3.6024E-01  1.0000E-04   
 mr-sdci # 13  6   -710.8821097630  1.1186E-02  0.0000E+00  5.0310E-01  1.0000E-04   
 mr-sdci # 13  7   -710.7941485129  8.9365E-03  0.0000E+00  5.6618E-01  1.0000E-04   
 mr-sdci # 13  8   -710.6128102835  2.4911E-02  0.0000E+00  6.4435E-01  1.0000E-04   
 mr-sdci # 13  9   -710.4178668478  9.6084E-02  0.0000E+00  6.2775E-01  1.0000E-04   
 mr-sdci # 13 10   -710.2682737646  2.9672E-02  0.0000E+00  6.6862E-01  1.0000E-04   
 mr-sdci # 13 11   -710.2263216558  1.0463E-01  0.0000E+00  6.2061E-01  1.0000E-04   
 mr-sdci # 13 12   -710.1192524119  1.2313E-01  0.0000E+00  5.9645E-01  1.0000E-04   
 mr-sdci # 13 13   -709.9659801667  5.0876E-04  0.0000E+00  6.8352E-01  1.0000E-04   
 mr-sdci # 13 14   -709.9627612193  8.4750E-02  0.0000E+00  7.1002E-01  1.0000E-04   
 mr-sdci # 13 15   -709.8769008124  6.5026E-02  0.0000E+00  8.1615E-01  1.0000E-04   
 mr-sdci # 13 16   -709.7885028107  2.4104E+02  0.0000E+00  7.5673E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000633
time for cinew                         0.069984
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694
   ht  14    81.97342744    -2.98784114   -17.17741214    -0.88396546     0.23925574    -6.97141174     1.16824278    -0.68253045
   ht  15    -3.09214664    -0.59030861    -0.03777813    -0.53593629    -0.31037172     0.11382691    -0.00797571     0.01081256
   ht  16    -0.27376100    -0.12997713    -1.62978286     0.18506893    -0.14508938    -0.00462163     0.03774719     0.00866432
   ht  17    -1.51204930    -0.29649728    -0.82386990    -0.10931905    -0.01541931     0.00753509     0.01954138    -0.00950987

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386
   ht  14     0.10605040    -0.01716468    -0.00624847   -22.97705862   132.45417116   -39.08092871
   ht  15     0.00307214    -0.00099132     0.00058193     1.17138176    -3.87711034     1.26590525    -0.68397985
   ht  16    -0.00221367     0.00059808     0.00001265     0.16499599     0.21455517     0.01141016    -0.03577251    -0.30950962
   ht  17     0.00231980    -0.00056869    -0.00042864     0.70775528    -1.02166185     0.44866851    -0.07088197    -0.04286350

                ht  17
   ht  17    -0.20697019

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950225       4.308642E-03  -7.086384E-02   0.141288       2.246563E-03  -4.031902E-02  -0.111233       3.744735E-02
 ref    2   3.548118E-04  -0.953289      -5.361153E-02   7.658780E-04  -0.122240      -4.068574E-02   2.327516E-03  -0.108542    
 ref    3   3.769992E-02  -7.153712E-02   0.920647       0.193184       0.149425      -3.511000E-03   1.549757E-02   5.313721E-02
 ref    4  -8.310410E-04  -0.103348      -0.134901      -7.755488E-02   0.979730      -5.575398E-03  -6.735248E-03  -3.629545E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.493226E-02  -1.068911E-02  -0.173058       5.605831E-02   6.945767E-02  -7.049396E-02  -8.071799E-02   8.076668E-02
 ref    2   2.217685E-02   0.233469      -4.135239E-02   3.104889E-02  -2.138645E-03   2.467838E-02   2.805318E-02   1.035939E-03
 ref    3  -0.269376       1.988486E-02  -4.800726E-02   6.922793E-02  -2.726297E-02   1.076502E-02   3.257447E-02   2.186323E-02
 ref    4   4.860349E-02   3.828323E-02  -9.072192E-04  -2.899975E-03   2.612319E-04   5.283562E-04  -1.236147E-03  -7.692707E-04

              v     17
 ref    1  -2.150921E-03
 ref    2   3.007056E-02
 ref    3   2.630957E-02
 ref    4  -7.284171E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904349       0.924578       0.873685       6.329774E-02   0.997147       3.324365E-03   1.266364E-02   1.732454E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   7.603898E-02   5.648311E-02   3.396452E-02   8.907484E-03   5.572280E-03   5.694586E-03   8.364999E-03   7.002922E-03

              v     17
 ref    1   1.601589E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022487     0.00430864    -0.07086384     0.14128819     0.00224656    -0.04031902    -0.11123258     0.03744735
 ref:   2     0.00035481    -0.95328942    -0.05361153     0.00076588    -0.12224040    -0.04068574     0.00232752    -0.10854177
 ref:   3     0.03769992    -0.07153712     0.92064700     0.19318397     0.14942505    -0.00351100     0.01549757     0.05313721
 ref:   4    -0.00083104    -0.10334840    -0.13490092    -0.07755488     0.97973030    -0.00557540    -0.00673525    -0.03629545

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.02493226    -0.01068911    -0.17305772     0.05605831     0.06945767    -0.07049396    -0.08071799     0.08076668
 ref:   2     0.02217685     0.23346914    -0.04135239     0.03104889    -0.00213865     0.02467838     0.02805318     0.00103594
 ref:   3    -0.26937567     0.01988486    -0.04800726     0.06922793    -0.02726297     0.01076502     0.03257447     0.02186323
 ref:   4     0.04860349     0.03828323    -0.00090722    -0.00289998     0.00026123     0.00052836    -0.00123615    -0.00076927

                ci  17
 ref:   1    -0.00215092
 ref:   2     0.03007056
 ref:   3     0.02630957
 ref:   4    -0.00072842

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -711.6262403690  2.5221E-10  0.0000E+00  4.1033E-04  1.0000E-03   
 mr-sdci # 14  2   -711.4949861446  1.0398E-03  2.9165E-04  1.8762E-02  1.0000E-03   
 mr-sdci # 14  3   -711.4715844846  1.1287E-02  0.0000E+00  6.9730E-02  1.0000E-03   
 mr-sdci # 14  4   -711.4151331705  2.8492E-03  0.0000E+00  1.2271E-01  1.0000E-03   
 mr-sdci # 14  5   -711.3687553871  1.9582E-05  0.0000E+00  3.6015E-01  1.0000E-04   
 mr-sdci # 14  6   -710.9181487747  3.6039E-02  0.0000E+00  4.5327E-01  1.0000E-04   
 mr-sdci # 14  7   -710.7980964980  3.9480E-03  0.0000E+00  5.8710E-01  1.0000E-04   
 mr-sdci # 14  8   -710.6647007320  5.1890E-02  0.0000E+00  6.0811E-01  1.0000E-04   
 mr-sdci # 14  9   -710.4803909654  6.2524E-02  0.0000E+00  6.0283E-01  1.0000E-04   
 mr-sdci # 14 10   -710.3072823041  3.9009E-02  0.0000E+00  6.6173E-01  1.0000E-04   
 mr-sdci # 14 11   -710.2369647258  1.0643E-02  0.0000E+00  6.0767E-01  1.0000E-04   
 mr-sdci # 14 12   -710.1523088263  3.3056E-02  0.0000E+00  6.5496E-01  1.0000E-04   
 mr-sdci # 14 13   -710.0745108077  1.0853E-01  0.0000E+00  6.7225E-01  1.0000E-04   
 mr-sdci # 14 14   -709.9628536785  9.2459E-05  0.0000E+00  6.9968E-01  1.0000E-04   
 mr-sdci # 14 15   -709.9511407293  7.4240E-02  0.0000E+00  7.2929E-01  1.0000E-04   
 mr-sdci # 14 16   -709.8597432642  7.1240E-02  0.0000E+00  7.7675E-01  1.0000E-04   
 mr-sdci # 14 17   -709.7785559687  2.4103E+02  0.0000E+00  7.5041E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.005869
time for cinew                         0.068499
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694
   ht  14    81.97342744    -2.98784114   -17.17741214    -0.88396546     0.23925574    -6.97141174     1.16824278    -0.68253045
   ht  15    -3.09214664    -0.59030861    -0.03777813    -0.53593629    -0.31037172     0.11382691    -0.00797571     0.01081256
   ht  16    -0.27376100    -0.12997713    -1.62978286     0.18506893    -0.14508938    -0.00462163     0.03774719     0.00866432
   ht  17    -1.51204930    -0.29649728    -0.82386990    -0.10931905    -0.01541931     0.00753509     0.01954138    -0.00950987
   ht  18     0.82352911     0.05419649    -0.72639842     0.01408960    -0.00126546     0.01141429    -0.00208623     0.00442831

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386
   ht  14     0.10605040    -0.01716468    -0.00624847   -22.97705862   132.45417116   -39.08092871
   ht  15     0.00307214    -0.00099132     0.00058193     1.17138176    -3.87711034     1.26590525    -0.68397985
   ht  16    -0.00221367     0.00059808     0.00001265     0.16499599     0.21455517     0.01141016    -0.03577251    -0.30950962
   ht  17     0.00231980    -0.00056869    -0.00042864     0.70775528    -1.02166185     0.44866851    -0.07088197    -0.04286350
   ht  18    -0.00187514    -0.00011058     0.00017494    -0.32358891     0.90349906    -0.40203668     0.00335738    -0.03312692

                ht  17         ht  18
   ht  17    -0.20697019
   ht  18    -0.00916845    -0.09233858

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950224       1.847917E-03   6.732565E-02  -0.143065      -1.546267E-03   1.696368E-02  -0.121041       7.370870E-03
 ref    2  -3.445532E-04  -0.955559       1.071444E-02  -5.265239E-03   0.120323       4.385661E-02   1.556298E-02   4.590005E-02
 ref    3  -3.771993E-02  -2.700222E-02  -0.926031      -0.169373      -0.138380       6.015191E-02   2.367401E-02   4.515745E-03
 ref    4   8.321400E-04  -0.107809       0.123401       6.488858E-02  -0.981573       1.816375E-02   2.288245E-03   2.411605E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -1.029705E-02  -2.903531E-02   0.175617       1.124531E-02  -4.727816E-02   7.070182E-02  -9.454167E-02  -5.039504E-02
 ref    2  -6.185780E-02   0.240881       4.769903E-02   1.621810E-02  -3.823947E-02   1.564131E-02   2.703424E-02   2.843840E-02
 ref    3   0.277542       2.976973E-02   3.824232E-02   2.039985E-02  -7.586331E-02  -1.708820E-02   1.879660E-02   2.974896E-02
 ref    4  -5.591659E-02   3.628590E-02   5.924228E-03  -6.041490E-03   4.090518E-03  -8.481528E-04   3.341460E-04  -1.633078E-03

              v     17       v     18
 ref    1   8.025784E-02  -2.080933E-03
 ref    2  -1.684072E-03   1.940547E-02
 ref    3   2.058619E-02   2.785259E-02
 ref    4  -6.803911E-04  -5.115027E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904350       0.925449       0.877409       5.339298E-02   0.997115       6.159343E-03   1.545879E-02   2.763120E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   8.408864E-02   6.106938E-02   3.461427E-02   8.421372E-04   9.469456E-03   5.536124E-03   1.002240E-02   4.236070E-03

              v     17       v     18
 ref    1   6.868412E-03   1.156931E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022440     0.00184792     0.06732565    -0.14306503    -0.00154627     0.01696368    -0.12104085     0.00737087
 ref:   2    -0.00034455    -0.95555920     0.01071444    -0.00526524     0.12032288     0.04385661     0.01556298     0.04590005
 ref:   3    -0.03771993    -0.02700222    -0.92603087    -0.16937276    -0.13838043     0.06015191     0.02367401     0.00451575
 ref:   4     0.00083214    -0.10780927     0.12340132     0.06488858    -0.98157294     0.01816375     0.00228824     0.02411605

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.01029705    -0.02903531     0.17561749     0.01124531    -0.04727816     0.07070182    -0.09454167    -0.05039504
 ref:   2    -0.06185780     0.24088052     0.04769903     0.01621810    -0.03823947     0.01564131     0.02703424     0.02843840
 ref:   3     0.27754199     0.02976973     0.03824232     0.02039985    -0.07586331    -0.01708820     0.01879660     0.02974896
 ref:   4    -0.05591659     0.03628590     0.00592423    -0.00604149     0.00409052    -0.00084815     0.00033415    -0.00163308

                ci  17         ci  18
 ref:   1     0.08025784    -0.00208093
 ref:   2    -0.00168407     0.01940547
 ref:   3     0.02058619     0.02785259
 ref:   4    -0.00068039    -0.00051150

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -711.6262403726  3.6569E-09  0.0000E+00  4.0925E-04  1.0000E-03   
 mr-sdci # 15  2   -711.4953098312  3.2369E-04  7.3204E-05  9.5355E-03  1.0000E-03   
 mr-sdci # 15  3   -711.4749314194  3.3469E-03  0.0000E+00  4.2235E-02  1.0000E-03   
 mr-sdci # 15  4   -711.4158049982  6.7183E-04  0.0000E+00  1.1769E-01  1.0000E-03   
 mr-sdci # 15  5   -711.3690300240  2.7464E-04  0.0000E+00  3.5952E-01  1.0000E-04   
 mr-sdci # 15  6   -711.0138136077  9.5665E-02  0.0000E+00  4.0801E-01  1.0000E-04   
 mr-sdci # 15  7   -710.8046660279  6.5695E-03  0.0000E+00  5.4120E-01  1.0000E-04   
 mr-sdci # 15  8   -710.7735864092  1.0889E-01  0.0000E+00  5.8919E-01  1.0000E-04   
 mr-sdci # 15  9   -710.4974210607  1.7030E-02  0.0000E+00  6.3030E-01  1.0000E-04   
 mr-sdci # 15 10   -710.3131540880  5.8718E-03  0.0000E+00  6.3918E-01  1.0000E-04   
 mr-sdci # 15 11   -710.2430344058  6.0697E-03  0.0000E+00  6.2069E-01  1.0000E-04   
 mr-sdci # 15 12   -710.1968744426  4.4566E-02  0.0000E+00  7.3443E-01  1.0000E-04   
 mr-sdci # 15 13   -710.1482520226  7.3741E-02  0.0000E+00  6.9393E-01  1.0000E-04   
 mr-sdci # 15 14   -710.0520947561  8.9241E-02  0.0000E+00  6.1775E-01  1.0000E-04   
 mr-sdci # 15 15   -709.9603100627  9.1693E-03  0.0000E+00  7.1109E-01  1.0000E-04   
 mr-sdci # 15 16   -709.9460680601  8.6325E-02  0.0000E+00  7.1571E-01  1.0000E-04   
 mr-sdci # 15 17   -709.8592545970  8.0699E-02  0.0000E+00  7.7615E-01  1.0000E-04   
 mr-sdci # 15 18   -709.7447811257  2.4100E+02  0.0000E+00  7.4860E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002789
time for cinew                         0.069868
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694
   ht  14    81.97342744    -2.98784114   -17.17741214    -0.88396546     0.23925574    -6.97141174     1.16824278    -0.68253045
   ht  15    -3.09214664    -0.59030861    -0.03777813    -0.53593629    -0.31037172     0.11382691    -0.00797571     0.01081256
   ht  16    -0.27376100    -0.12997713    -1.62978286     0.18506893    -0.14508938    -0.00462163     0.03774719     0.00866432
   ht  17    -1.51204930    -0.29649728    -0.82386990    -0.10931905    -0.01541931     0.00753509     0.01954138    -0.00950987
   ht  18     0.82352911     0.05419649    -0.72639842     0.01408960    -0.00126546     0.01141429    -0.00208623     0.00442831
   ht  19     0.24113973     0.01195178     0.14692194    -0.07113801     0.00255687     0.02085831    -0.00716506     0.00399543

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386
   ht  14     0.10605040    -0.01716468    -0.00624847   -22.97705862   132.45417116   -39.08092871
   ht  15     0.00307214    -0.00099132     0.00058193     1.17138176    -3.87711034     1.26590525    -0.68397985
   ht  16    -0.00221367     0.00059808     0.00001265     0.16499599     0.21455517     0.01141016    -0.03577251    -0.30950962
   ht  17     0.00231980    -0.00056869    -0.00042864     0.70775528    -1.02166185     0.44866851    -0.07088197    -0.04286350
   ht  18    -0.00187514    -0.00011058     0.00017494    -0.32358891     0.90349906    -0.40203668     0.00335738    -0.03312692
   ht  19    -0.00098787    -0.00011034    -0.00006860    -0.14231273     0.05916809    -0.06169318    -0.00857388     0.00208111

                ht  17         ht  18         ht  19
   ht  17    -0.20697019
   ht  18    -0.00916845    -0.09233858
   ht  19     0.01005828    -0.00127861    -0.01977225

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950224       1.135128E-03   6.544113E-02  -0.143909       6.325559E-04   8.700887E-03  -5.463605E-02   0.106840    
 ref    2  -3.408180E-04  -0.956093      -2.904789E-03  -5.136375E-03   0.118761       3.172735E-02   1.092820E-02   6.044676E-03
 ref    3  -3.772880E-02  -1.129189E-02  -0.929093      -0.157833      -0.121844       7.898999E-02  -1.658929E-02  -2.403485E-02
 ref    4   8.296613E-04  -0.107614       0.112018       4.505364E-02  -0.981892       6.592906E-02   2.928272E-02   1.329498E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -2.917285E-02  -4.701516E-02  -3.909338E-03  -0.169230       6.270305E-02  -3.166038E-02  -0.120826      -2.666079E-03
 ref    2  -8.355137E-03   0.146267      -0.210718      -4.415399E-02   3.143532E-02   8.966963E-03   2.253248E-02   5.153314E-03
 ref    3   0.247712      -0.111549      -4.970021E-02  -3.443594E-02   4.637733E-02   6.979781E-02   1.071685E-02   6.029642E-03
 ref    4  -4.783371E-02   3.052733E-02  -3.311329E-02  -1.330548E-03  -7.515341E-04  -4.582269E-03   1.146536E-03  -8.504193E-04

              v     17       v     18       v     19
 ref    1  -1.185912E-02   7.808495E-02   1.051070E-02
 ref    2   2.683657E-02  -3.385023E-03  -2.082020E-02
 ref    3   4.506564E-02   1.061988E-02  -4.010254E-02
 ref    4  -2.267410E-03  -2.405189E-04   5.572342E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904351       0.925824       0.880052       4.767751E-02   0.993063       1.166839E-02   4.237206E-03   1.220573E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   6.457030E-02   3.697975E-02   4.798416E-02   3.177595E-02   7.071274E-03   5.975518E-03   1.522280E-02   7.074441E-05

              v     17       v     18       v     19
 ref    1   2.896893E-03   6.221557E-03   2.152480E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18        ev   19

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18       v     19

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022435     0.00113513     0.06544113    -0.14390935     0.00063256     0.00870089    -0.05463605     0.10683991
 ref:   2    -0.00034082    -0.95609336    -0.00290479    -0.00513637     0.11876093     0.03172735     0.01092820     0.00604468
 ref:   3    -0.03772880    -0.01129189    -0.92909265    -0.15783344    -0.12184378     0.07898999    -0.01658929    -0.02403485
 ref:   4     0.00082966    -0.10761413     0.11201834     0.04505364    -0.98189223     0.06592906     0.02928272     0.01329498

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.02917285    -0.04701516    -0.00390934    -0.16922993     0.06270305    -0.03166038    -0.12082602    -0.00266608
 ref:   2    -0.00835514     0.14626740    -0.21071848    -0.04415399     0.03143532     0.00896696     0.02253248     0.00515331
 ref:   3     0.24771229    -0.11154935    -0.04970021    -0.03443594     0.04637733     0.06979781     0.01071685     0.00602964
 ref:   4    -0.04783371     0.03052733    -0.03311329    -0.00133055    -0.00075153    -0.00458227     0.00114654    -0.00085042

                ci  17         ci  18         ci  19
 ref:   1    -0.01185912     0.07808495     0.01051070
 ref:   2     0.02683657    -0.00338502    -0.02082020
 ref:   3     0.04506564     0.01061988    -0.04010254
 ref:   4    -0.00226741    -0.00024052     0.00055723

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -711.6262403741  1.5179E-09  0.0000E+00  4.1139E-04  1.0000E-03   
 mr-sdci # 16  2   -711.4953961668  8.6336E-05  2.1474E-05  5.0296E-03  1.0000E-03   
 mr-sdci # 16  3   -711.4761485558  1.2171E-03  0.0000E+00  3.0483E-02  1.0000E-03   
 mr-sdci # 16  4   -711.4163888812  5.8388E-04  0.0000E+00  1.1411E-01  1.0000E-03   
 mr-sdci # 16  5   -711.3704506435  1.4206E-03  0.0000E+00  3.5614E-01  1.0000E-04   
 mr-sdci # 16  6   -711.1128148077  9.9001E-02  0.0000E+00  3.7111E-01  1.0000E-04   
 mr-sdci # 16  7   -710.8612704529  5.6604E-02  0.0000E+00  4.9687E-01  1.0000E-04   
 mr-sdci # 16  8   -710.7979298076  2.4343E-02  0.0000E+00  5.8786E-01  1.0000E-04   
 mr-sdci # 16  9   -710.5311306380  3.3710E-02  0.0000E+00  5.8535E-01  1.0000E-04   
 mr-sdci # 16 10   -710.3910330399  7.7879E-02  0.0000E+00  6.5316E-01  1.0000E-04   
 mr-sdci # 16 11   -710.3036430535  6.0609E-02  0.0000E+00  6.7879E-01  1.0000E-04   
 mr-sdci # 16 12   -710.2342487985  3.7374E-02  0.0000E+00  6.0490E-01  1.0000E-04   
 mr-sdci # 16 13   -710.1561069547  7.8549E-03  0.0000E+00  6.4158E-01  1.0000E-04   
 mr-sdci # 16 14   -710.1182169048  6.6122E-02  0.0000E+00  7.5700E-01  1.0000E-04   
 mr-sdci # 16 15   -709.9727056336  1.2396E-02  0.0000E+00  7.0375E-01  1.0000E-04   
 mr-sdci # 16 16   -709.9517377076  5.6696E-03  0.0000E+00  6.7767E-01  1.0000E-04   
 mr-sdci # 16 17   -709.9186594258  5.9405E-02  0.0000E+00  7.6898E-01  1.0000E-04   
 mr-sdci # 16 18   -709.8497676839  1.0499E-01  0.0000E+00  7.4374E-01  1.0000E-04   
 mr-sdci # 16 19   -709.7116490229  2.4096E+02  0.0000E+00  7.0857E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002260
time for cinew                         0.070728
time for eigenvalue solver             0.000313
time for vector access                 0.000000

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.77650994
   ht   2     0.00000000  -242.66321011
   ht   3     0.00000000     0.00000000  -242.63206302
   ht   4     0.00000000     0.00000000     0.00000000  -242.62092702
   ht   5    -0.11594928     0.00005842     0.00811443    -0.00017268   -25.85693794
   ht   6     9.12016889    -0.02356417    -2.71477631     0.02939474    -0.50458036    -4.35797369
   ht   7    -1.41901089     0.01341509     0.80445891    -0.00700318     0.22644723     0.42033132    -0.47188427
   ht   8     0.69040450    -0.00721837    -0.43491600     0.00472213     0.14736377    -0.36379342     0.06526706    -0.12332294
   ht   9    -0.10697782    -0.00008082     0.03256474    -0.00063457    -0.00434663     0.07915716     0.00214241     0.00837489
   ht  10     0.01959873    -0.00038161    -0.01125544     0.00042862    -0.00482276    -0.00750998     0.00131776    -0.00316597
   ht  11     0.00559236    -0.00018822     0.00186520     0.00012471    -0.00096673    -0.00289367     0.00093796    -0.00085453
   ht  12    79.93899814     0.23322456     0.30298243     0.30973872    -0.78207984     0.23324911     0.05173184     0.38466935
   ht  13  -299.24001531    -3.87873090    73.45051763    -2.05948497    -0.40581676    32.03693045    -5.10829019     3.82975694
   ht  14    81.97342744    -2.98784114   -17.17741214    -0.88396546     0.23925574    -6.97141174     1.16824278    -0.68253045
   ht  15    -3.09214664    -0.59030861    -0.03777813    -0.53593629    -0.31037172     0.11382691    -0.00797571     0.01081256
   ht  16    -0.27376100    -0.12997713    -1.62978286     0.18506893    -0.14508938    -0.00462163     0.03774719     0.00866432
   ht  17    -1.51204930    -0.29649728    -0.82386990    -0.10931905    -0.01541931     0.00753509     0.01954138    -0.00950987
   ht  18     0.82352911     0.05419649    -0.72639842     0.01408960    -0.00126546     0.01141429    -0.00208623     0.00442831
   ht  19     0.24113973     0.01195178     0.14692194    -0.07113801     0.00255687     0.02085831    -0.00716506     0.00399543
   ht  20     0.07015668     0.00785612    -0.15094418    -0.05017124     0.00203326     0.00039535     0.00244683    -0.00182993

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01444323
   ht  10    -0.00021013    -0.00180183
   ht  11     0.00028124     0.00004936    -0.00025825
   ht  12    -0.06922168     0.02286573     0.00322748   -47.05637405
   ht  13    -0.66070428     0.12417474     0.03343278    69.17488688  -540.25020386
   ht  14     0.10605040    -0.01716468    -0.00624847   -22.97705862   132.45417116   -39.08092871
   ht  15     0.00307214    -0.00099132     0.00058193     1.17138176    -3.87711034     1.26590525    -0.68397985
   ht  16    -0.00221367     0.00059808     0.00001265     0.16499599     0.21455517     0.01141016    -0.03577251    -0.30950962
   ht  17     0.00231980    -0.00056869    -0.00042864     0.70775528    -1.02166185     0.44866851    -0.07088197    -0.04286350
   ht  18    -0.00187514    -0.00011058     0.00017494    -0.32358891     0.90349906    -0.40203668     0.00335738    -0.03312692
   ht  19    -0.00098787    -0.00011034    -0.00006860    -0.14231273     0.05916809    -0.06169318    -0.00857388     0.00208111
   ht  20    -0.00067267    -0.00032214    -0.00007629    -0.03252942     0.13227814    -0.04931941    -0.00335243    -0.00050731

                ht  17         ht  18         ht  19         ht  20
   ht  17    -0.20697019
   ht  18    -0.00916845    -0.09233858
   ht  19     0.01005828    -0.00127861    -0.01977225
   ht  20     0.00008996    -0.00720944    -0.00064493    -0.00668524

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950224       7.997380E-04  -6.505021E-02  -0.144068       2.543098E-04  -2.026345E-03  -5.289457E-02  -9.313670E-02
 ref    2  -3.352265E-04  -0.956401       7.124881E-03  -4.464440E-03   0.116258       1.712404E-03  -1.647080E-02  -1.181179E-02
 ref    3  -3.773901E-02  -5.507930E-03   0.929186      -0.156362      -9.636148E-02   0.105412      -3.703400E-02   2.240817E-02
 ref    4   8.177201E-04  -0.106158      -9.998709E-02   3.768583E-02  -0.966693       0.193085       3.068381E-02  -1.599253E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   7.601910E-02  -2.736187E-02   7.970990E-04  -0.150595       5.966827E-02   4.771360E-02   7.816373E-02  -0.119653    
 ref    2  -4.359825E-02   7.612990E-02   0.244593      -2.844907E-02   3.070018E-02  -5.170797E-03   1.899169E-02   2.298458E-02
 ref    3  -0.114089      -0.249571       2.781424E-02  -1.631430E-02   4.509930E-02  -6.033326E-02   5.500278E-02   1.175754E-02
 ref    4   2.666373E-02   4.498984E-02   3.932643E-02  -1.179769E-04  -6.690313E-04   3.639760E-03  -5.948034E-03   1.010792E-03

              v     17       v     18       v     19       v     20
 ref    1   4.613059E-03  -4.149308E-02  -5.688566E-02  -1.267042E-02
 ref    2  -3.602347E-03   2.572021E-02  -3.708321E-03   2.046000E-02
 ref    3  -2.887401E-03   3.686874E-02  -1.972290E-02   3.877717E-02
 ref    4   5.255305E-04  -6.980887E-04  -2.112940E-03  -1.038153E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904351       0.926004       0.877666       4.664496E-02   0.957297       4.840043E-02   5.382137E-03   9.571851E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.140701E-02   7.085418E-02   6.214637E-02   2.375447E-02   6.537198E-03   5.956674E-03   9.530938E-03   1.498428E-02

              v     17       v     18       v     19       v     20
 ref    1   4.287049E-05   3.742996E-03   3.643187E-03   2.083898E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18        ev   19        ev   20

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18       v     19       v     20

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022411     0.00079974    -0.06505021    -0.14406838     0.00025431    -0.00202634    -0.05289457    -0.09313670
 ref:   2    -0.00033523    -0.95640109     0.00712488    -0.00446444     0.11625782     0.00171240    -0.01647080    -0.01181179
 ref:   3    -0.03773901    -0.00550793     0.92918555    -0.15636212    -0.09636148     0.10541186    -0.03703400     0.02240817
 ref:   4     0.00081772    -0.10615821    -0.09998709     0.03768583    -0.96669287     0.19308478     0.03068381    -0.01599253

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.07601910    -0.02736187     0.00079710    -0.15059531     0.05966827     0.04771360     0.07816373    -0.11965251
 ref:   2    -0.04359825     0.07612990     0.24459258    -0.02844907     0.03070018    -0.00517080     0.01899169     0.02298458
 ref:   3    -0.11408918    -0.24957095     0.02781424    -0.01631430     0.04509930    -0.06033326     0.05500278     0.01175754
 ref:   4     0.02666373     0.04498984     0.03932643    -0.00011798    -0.00066903     0.00363976    -0.00594803     0.00101079

                ci  17         ci  18         ci  19         ci  20
 ref:   1     0.00461306    -0.04149308    -0.05688566    -0.01267042
 ref:   2    -0.00360235     0.02572021    -0.00370832     0.02046000
 ref:   3    -0.00288740     0.03686874    -0.01972290     0.03877717
 ref:   4     0.00052553    -0.00069809    -0.00211294    -0.00103815

 trial vector basis is being transformed.  new dimension:   9

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -711.6262403814  7.3015E-09  0.0000E+00  4.0605E-04  1.0000E-03   
 mr-sdci # 17  2   -711.4954234734  2.7307E-05  6.3745E-06  2.8599E-03  1.0000E-03   
 mr-sdci # 17  3   -711.4766677613  5.1921E-04  0.0000E+00  2.4505E-02  1.0000E-03   
 mr-sdci # 17  4   -711.4164154533  2.6572E-05  0.0000E+00  1.1379E-01  1.0000E-03   
 mr-sdci # 17  5   -711.3754332029  4.9826E-03  0.0000E+00  3.3787E-01  1.0000E-04   
 mr-sdci # 17  6   -711.2249802200  1.1217E-01  0.0000E+00  3.0932E-01  1.0000E-04   
 mr-sdci # 17  7   -710.9288711252  6.7601E-02  0.0000E+00  4.2530E-01  1.0000E-04   
 mr-sdci # 17  8   -710.8007813076  2.8515E-03  0.0000E+00  5.9595E-01  1.0000E-04   
 mr-sdci # 17  9   -710.6153637162  8.4233E-02  0.0000E+00  5.0189E-01  1.0000E-04   
 mr-sdci # 17 10   -710.4822193213  9.1186E-02  0.0000E+00  6.6464E-01  1.0000E-04   
 mr-sdci # 17 11   -710.3146632273  1.1020E-02  0.0000E+00  6.4242E-01  1.0000E-04   
 mr-sdci # 17 12   -710.2417808662  7.5321E-03  0.0000E+00  6.2793E-01  1.0000E-04   
 mr-sdci # 17 13   -710.1561446850  3.7730E-05  0.0000E+00  6.4519E-01  1.0000E-04   
 mr-sdci # 17 14   -710.1187699592  5.5305E-04  0.0000E+00  7.5429E-01  1.0000E-04   
 mr-sdci # 17 15   -710.1018422997  1.2914E-01  0.0000E+00  7.3757E-01  1.0000E-04   
 mr-sdci # 17 16   -709.9726636946  2.0926E-02  0.0000E+00  7.0473E-01  1.0000E-04   
 mr-sdci # 17 17   -709.9514858602  3.2826E-02  0.0000E+00  6.8013E-01  1.0000E-04   
 mr-sdci # 17 18   -709.9055503058  5.5783E-02  0.0000E+00  7.0802E-01  1.0000E-04   
 mr-sdci # 17 19   -709.8081302377  9.6481E-02  0.0000E+00  7.6162E-01  1.0000E-04   
 mr-sdci # 17 20   -709.7102371645  2.4096E+02  0.0000E+00  7.0923E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.006413
time for cinew                         0.104147
time for eigenvalue solver             0.000383
time for vector access                 0.000002

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610

                ht   9         ht  10
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950225       7.293043E-04   6.483484E-02  -0.144067      -2.527608E-03  -7.489984E-03  -5.388346E-02  -7.102553E-02
 ref    2  -3.326520E-04  -0.956540      -7.806954E-03  -4.471707E-03   0.111255      -2.001670E-02  -2.828051E-02  -1.676648E-02
 ref    3  -3.774427E-02  -3.892616E-03  -0.929408      -0.156365      -7.525868E-02   0.116141      -5.190294E-02   1.212501E-02
 ref    4   8.051549E-04  -0.105038       9.183305E-02   3.775456E-02  -0.934168       0.315759       4.064970E-02  -1.115429E-02

              v      9       v     10
 ref    1  -9.604852E-02   7.558810E-03
 ref    2   4.006917E-02  -9.510758E-03
 ref    3   0.106060      -4.062773E-02
 ref    4  -3.431231E-02  -1.239802E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904352       0.926018       0.876496       4.665071E-02   0.890719       0.113649       8.049528E-03   5.597175E-03

              v      9       v     10
 ref    1   2.325683E-02   1.951914E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022456     0.00072930     0.06483484    -0.14406712    -0.00252761    -0.00748998    -0.05388346    -0.07102553
 ref:   2    -0.00033265    -0.95654004    -0.00780695    -0.00447171     0.11125508    -0.02001670    -0.02828051    -0.01676648
 ref:   3    -0.03774427    -0.00389262    -0.92940750    -0.15636485    -0.07525868     0.11614061    -0.05190294     0.01212501
 ref:   4     0.00080515    -0.10503805     0.09183305     0.03775456    -0.93416843     0.31575867     0.04064970    -0.01115429

                ci   9         ci  10
 ref:   1    -0.09604852     0.00755881
 ref:   2     0.04006917    -0.00951076
 ref:   3     0.10605958    -0.04062773
 ref:   4    -0.03431231    -0.01239802

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -711.6262403861  4.6583E-09  0.0000E+00  3.9914E-04  1.0000E-03   
 mr-sdci # 18  2   -711.4954298613  6.3879E-06  1.9231E-06  1.4798E-03  1.0000E-03   
 mr-sdci # 18  3   -711.4768275931  1.5983E-04  0.0000E+00  2.1682E-02  1.0000E-03   
 mr-sdci # 18  4   -711.4164154545  1.1955E-09  0.0000E+00  1.1379E-01  1.0000E-03   
 mr-sdci # 18  5   -711.3832043142  7.7711E-03  0.0000E+00  3.0355E-01  1.0000E-04   
 mr-sdci # 18  6   -711.2605013545  3.5521E-02  0.0000E+00  2.9142E-01  1.0000E-04   
 mr-sdci # 18  7   -710.9639346021  3.5063E-02  0.0000E+00  3.7209E-01  1.0000E-04   
 mr-sdci # 18  8   -710.8140666521  1.3285E-02  0.0000E+00  5.9394E-01  1.0000E-04   
 mr-sdci # 18  9   -710.6692956688  5.3932E-02  0.0000E+00  4.7416E-01  1.0000E-04   
 mr-sdci # 18 10   -710.0970021691 -3.8522E-01  0.0000E+00  7.5666E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002317
time for cinew                         0.068047
time for eigenvalue solver             0.000139
time for vector access                 0.000000

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694

                ht   9         ht  10         ht  11
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950224       6.757103E-04   6.494318E-02   0.143771      -8.074293E-03  -9.545755E-03  -5.104756E-02   3.515710E-02
 ref    2   3.308168E-04  -0.956636      -7.642771E-03   7.170876E-03   0.106919      -3.165313E-02  -3.414610E-02   1.401167E-02
 ref    3   3.774348E-02  -3.417050E-03  -0.928835       0.156890      -5.738242E-02   0.129261      -3.907325E-02  -2.665198E-02
 ref    4  -7.953557E-04  -0.104295       8.576860E-02  -6.126465E-02  -0.900849       0.396541       5.912876E-02   8.237063E-04

              v      9       v     10       v     11
 ref    1   0.115666       4.770417E-03  -1.463607E-02
 ref    2  -2.945655E-02   2.835984E-02   5.785017E-04
 ref    3  -7.758809E-02   0.111182      -3.419483E-03
 ref    4   4.259588E-02   1.775855E-02   4.356485E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904351       0.926043       0.874367       4.908932E-02   0.826319       0.175046       8.794739E-03   2.143355E-03

              v      9       v     10       v     11
 ref    1   2.208067E-02   1.350378E-02   2.452210E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022413     0.00067571     0.06494318     0.14377090    -0.00807429    -0.00954576    -0.05104756     0.03515710
 ref:   2     0.00033082    -0.95663648    -0.00764277     0.00717088     0.10691938    -0.03165313    -0.03414610     0.01401167
 ref:   3     0.03774348    -0.00341705    -0.92883497     0.15688999    -0.05738242     0.12926056    -0.03907325    -0.02665198
 ref:   4    -0.00079536    -0.10429538     0.08576860    -0.06126465    -0.90084927     0.39654065     0.05912876     0.00082371

                ci   9         ci  10         ci  11
 ref:   1     0.11566618     0.00477042    -0.01463607
 ref:   2    -0.02945655     0.02835984     0.00057850
 ref:   3    -0.07758809     0.11118174    -0.00341948
 ref:   4     0.04259588     0.01775855     0.00435649

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -711.6262403882  2.1509E-09  0.0000E+00  3.9751E-04  1.0000E-03   
 mr-sdci # 19  2   -711.4954317685  1.9071E-06  0.0000E+00  6.9201E-04  1.0000E-03   
 mr-sdci # 19  3   -711.4768962951  6.8702E-05  3.2715E-04  1.9946E-02  1.0000E-03   
 mr-sdci # 19  4   -711.4164963163  8.0862E-05  0.0000E+00  1.1481E-01  1.0000E-03   
 mr-sdci # 19  5   -711.3914964654  8.2922E-03  0.0000E+00  2.6900E-01  1.0000E-04   
 mr-sdci # 19  6   -711.2723415227  1.1840E-02  0.0000E+00  3.0599E-01  1.0000E-04   
 mr-sdci # 19  7   -710.9997912619  3.5857E-02  0.0000E+00  3.4072E-01  1.0000E-04   
 mr-sdci # 19  8   -710.8449170355  3.0850E-02  0.0000E+00  5.5927E-01  1.0000E-04   
 mr-sdci # 19  9   -710.6957021208  2.6406E-02  0.0000E+00  4.9097E-01  1.0000E-04   
 mr-sdci # 19 10   -710.4094196481  3.1242E-01  0.0000E+00  6.6866E-01  1.0000E-04   
 mr-sdci # 19 11   -710.0114558751 -3.0321E-01  0.0000E+00  8.1781E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003056
time for cinew                         0.135323
time for eigenvalue solver             0.000164
time for vector access                 0.000002

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474

                ht   9         ht  10         ht  11         ht  12
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950225       6.725533E-04   6.466371E-02   0.139996       2.132201E-02   5.219331E-03   4.610939E-02   5.596372E-02
 ref    2   3.304951E-04  -0.956641      -6.819425E-03   1.335450E-02  -0.105919       2.929092E-02   3.233676E-02   2.110049E-02
 ref    3   3.774680E-02  -3.315508E-03  -0.928929       0.158986       6.124605E-02  -0.127751       3.980800E-02  -2.117846E-02
 ref    4  -7.900425E-04  -0.104243       7.579288E-02  -0.118366       0.900451      -0.381240      -5.191154E-02  -2.940004E-02

              v      9       v     10       v     11       v     12
 ref    1  -0.116052      -4.556305E-02   5.252074E-02   1.482616E-02
 ref    2   2.934326E-02  -3.180066E-02  -5.264887E-03  -5.478677E-04
 ref    3   7.724851E-02  -0.104650      -4.634257E-02   3.464011E-03
 ref    4  -4.198864E-02   2.695872E-02  -5.977409E-02  -4.485487E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904354       0.926040       0.872882       5.906435E-02   0.826236       0.162549       7.451227E-03   4.890059E-03

              v      9       v     10       v     11       v     12
 ref    1   2.205949E-02   1.476561E-02   8.506723E-03   2.522342E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022538     0.00067255     0.06466371     0.13999584     0.02132201     0.00521933     0.04610939     0.05596372
 ref:   2     0.00033050    -0.95664109    -0.00681942     0.01335450    -0.10591907     0.02929092     0.03233676     0.02110049
 ref:   3     0.03774680    -0.00331551    -0.92892929     0.15898636     0.06124605    -0.12775141     0.03980800    -0.02117846
 ref:   4    -0.00079004    -0.10424294     0.07579288    -0.11836599     0.90045096    -0.38123955    -0.05191154    -0.02940004

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.11605206    -0.04556305     0.05252074     0.01482616
 ref:   2     0.02934326    -0.03180066    -0.00526489    -0.00054787
 ref:   3     0.07724851    -0.10464972    -0.04634257     0.00346401
 ref:   4    -0.04198864     0.02695872    -0.05977409    -0.00448549

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -711.6262403889  6.5751E-10  0.0000E+00  3.9604E-04  1.0000E-03   
 mr-sdci # 20  2   -711.4954317832  1.4775E-08  0.0000E+00  6.7591E-04  1.0000E-03   
 mr-sdci # 20  3   -711.4771600196  2.6372E-04  3.5097E-04  1.2394E-02  1.0000E-03   
 mr-sdci # 20  4   -711.4173622166  8.6590E-04  0.0000E+00  1.1472E-01  1.0000E-03   
 mr-sdci # 20  5   -711.3939653809  2.4689E-03  0.0000E+00  2.6494E-01  1.0000E-04   
 mr-sdci # 20  6   -711.2736648013  1.3233E-03  0.0000E+00  3.0130E-01  1.0000E-04   
 mr-sdci # 20  7   -711.0007574997  9.6624E-04  0.0000E+00  3.4258E-01  1.0000E-04   
 mr-sdci # 20  8   -710.8710765309  2.6159E-02  0.0000E+00  5.4293E-01  1.0000E-04   
 mr-sdci # 20  9   -710.6957173518  1.5231E-05  0.0000E+00  4.9081E-01  1.0000E-04   
 mr-sdci # 20 10   -710.4416616256  3.2242E-02  0.0000E+00  7.2099E-01  1.0000E-04   
 mr-sdci # 20 11   -710.3552841186  3.4383E-01  0.0000E+00  6.8476E-01  1.0000E-04   
 mr-sdci # 20 12   -710.0114533008 -2.3033E-01  0.0000E+00  8.1781E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002993
time for cinew                         0.060196
time for eigenvalue solver             0.000175
time for vector access                 0.000002

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950226       6.792535E-04  -6.604437E-02  -0.140784      -1.707801E-02  -1.425839E-02   4.907936E-02  -4.826264E-02
 ref    2   3.306371E-04  -0.956638       6.009949E-03  -1.487981E-02   0.107636      -2.939473E-02   3.264074E-02  -2.007192E-02
 ref    3   3.774671E-02  -3.330794E-03   0.928898      -0.159203      -5.971054E-02   0.121814       4.169547E-02   2.447547E-02
 ref    4  -7.905189E-04  -0.104257      -7.146729E-02   0.129428      -0.904847       0.362805      -4.792394E-02   3.690173E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   0.116250      -1.498733E-02   1.822295E-02   0.194476      -3.703736E-03
 ref    2  -2.931078E-02   1.151198E-02   2.730044E-02   4.053591E-02   2.910521E-03
 ref    3  -7.719147E-02   4.947306E-02   0.101290       6.584663E-02   3.514427E-04
 ref    4   4.214009E-02  -6.847545E-02   5.501196E-02   1.167531E-02   5.455579E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904355       0.926038       0.872356       6.213861E-02   0.834191       0.147533       7.509418E-03   4.692951E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.210741E-02   7.493617E-03   1.436347E-02   4.393603E-02   5.207565E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022578     0.00067925    -0.06604437    -0.14078392    -0.01707801    -0.01425839     0.04907936    -0.04826264
 ref:   2     0.00033064    -0.95663827     0.00600995    -0.01487981     0.10763563    -0.02939473     0.03264074    -0.02007192
 ref:   3     0.03774671    -0.00333079     0.92889758    -0.15920273    -0.05971054     0.12181391     0.04169547     0.02447547
 ref:   4    -0.00079052    -0.10425656    -0.07146729     0.12942791    -0.90484704     0.36280489    -0.04792394     0.03690173

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.11624966    -0.01498733     0.01822295     0.19447564    -0.00370374
 ref:   2    -0.02931078     0.01151198     0.02730044     0.04053591     0.00291052
 ref:   3    -0.07719147     0.04947306     0.10129048     0.06584663     0.00035144
 ref:   4     0.04214009    -0.06847545     0.05501196     0.01167531     0.00545558

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -711.6262403889  6.9065E-12  0.0000E+00  3.9580E-04  1.0000E-03   
 mr-sdci # 21  2   -711.4954317845  1.2643E-09  0.0000E+00  6.7368E-04  1.0000E-03   
 mr-sdci # 21  3   -711.4772336102  7.3591E-05  1.1605E-04  9.2510E-03  1.0000E-03   
 mr-sdci # 21  4   -711.4174077356  4.5519E-05  0.0000E+00  1.1572E-01  1.0000E-03   
 mr-sdci # 21  5   -711.3954088498  1.4435E-03  0.0000E+00  2.6502E-01  1.0000E-04   
 mr-sdci # 21  6   -711.2768792832  3.2145E-03  0.0000E+00  2.9077E-01  1.0000E-04   
 mr-sdci # 21  7   -711.0010749766  3.1748E-04  0.0000E+00  3.4214E-01  1.0000E-04   
 mr-sdci # 21  8   -710.8727054473  1.6289E-03  0.0000E+00  5.4658E-01  1.0000E-04   
 mr-sdci # 21  9   -710.6957180997  7.4785E-07  0.0000E+00  4.9071E-01  1.0000E-04   
 mr-sdci # 21 10   -710.4864799709  4.4818E-02  0.0000E+00  7.3023E-01  1.0000E-04   
 mr-sdci # 21 11   -710.3853440528  3.0060E-02  0.0000E+00  6.5247E-01  1.0000E-04   
 mr-sdci # 21 12   -710.0996586795  8.8205E-02  0.0000E+00  6.6392E-01  1.0000E-04   
 mr-sdci # 21 13   -710.0111552785 -1.4499E-01  0.0000E+00  8.1742E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.004242
time for cinew                         0.070099
time for eigenvalue solver             0.000214
time for vector access                 0.000000

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064
   ht  14     8.20627337    -2.53022710    -3.13157608    -1.63402854     0.00199043     0.18884505    -1.09554096     2.15387864

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320
   ht  14     0.26859395    -0.00045955     0.00408052     0.22653162    -1.05674394    -0.45254959

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950226       6.779969E-04   6.605800E-02  -0.131998      -4.941279E-02  -6.217065E-03   3.113200E-02   7.101181E-02
 ref    2   3.281539E-04  -0.956655      -2.965798E-03  -4.514913E-02   0.120440      -3.817582E-02   4.946799E-02  -2.595442E-02
 ref    3   3.774923E-02  -3.293820E-03  -0.930636      -0.139930      -0.101748       0.128591       3.126004E-02   2.352868E-02
 ref    4  -7.862451E-04  -0.104203       6.020624E-02   0.310524      -0.871054       0.258259       2.803812E-02  -0.177971    

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -3.960678E-02  -0.108592       1.333274E-02   0.127242       0.153409      -3.521559E-03
 ref    2   8.338227E-02  -6.081502E-03   4.227711E-02   9.957690E-02  -6.811005E-02   2.748073E-03
 ref    3  -4.288261E-02   0.106584       0.100301      -5.463201E-03   0.117916       5.451190E-04
 ref    4   0.119117      -0.106948       4.613094E-02   3.242725E-02  -3.025215E-02   5.382784E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904355       0.926059       0.874081       0.135467       0.786035       8.472944E-02   5.179610E-03   3.794342E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   2.454899E-02   3.462748E-02   1.415348E-02   2.718735E-02   4.299283E-02   4.922480E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022610     0.00067800     0.06605800    -0.13199780    -0.04941279    -0.00621707     0.03113200     0.07101181
 ref:   2     0.00032815    -0.95665531    -0.00296580    -0.04514913     0.12043955    -0.03817582     0.04946799    -0.02595442
 ref:   3     0.03774923    -0.00329382    -0.93063614    -0.13992973    -0.10174792     0.12859134     0.03126004     0.02352868
 ref:   4    -0.00078625    -0.10420260     0.06020624     0.31052408    -0.87105403     0.25825891     0.02803812    -0.17797053

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.03960678    -0.10859239     0.01333274     0.12724155     0.15340927    -0.00352156
 ref:   2     0.08338227    -0.00608150     0.04227711     0.09957690    -0.06811005     0.00274807
 ref:   3    -0.04288261     0.10658432     0.10030104    -0.00546320     0.11791632     0.00054512
 ref:   4     0.11911665    -0.10694845     0.04613094     0.03242725    -0.03025215     0.00538278

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -711.6262403890  1.3810E-10  0.0000E+00  3.9532E-04  1.0000E-03   
 mr-sdci # 22  2   -711.4954317877  3.2032E-09  0.0000E+00  6.7091E-04  1.0000E-03   
 mr-sdci # 22  3   -711.4773162193  8.2609E-05  3.2663E-05  5.8590E-03  1.0000E-03   
 mr-sdci # 22  4   -711.4187956864  1.3880E-03  0.0000E+00  1.2506E-01  1.0000E-03   
 mr-sdci # 22  5   -711.4043999445  8.9911E-03  0.0000E+00  2.3675E-01  1.0000E-04   
 mr-sdci # 22  6   -711.2880299524  1.1151E-02  0.0000E+00  2.4268E-01  1.0000E-04   
 mr-sdci # 22  7   -711.0086317073  7.5567E-03  0.0000E+00  3.5261E-01  1.0000E-04   
 mr-sdci # 22  8   -710.9342281065  6.1523E-02  0.0000E+00  4.9970E-01  1.0000E-04   
 mr-sdci # 22  9   -710.7332820566  3.7564E-02  0.0000E+00  5.4405E-01  1.0000E-04   
 mr-sdci # 22 10   -710.6869145733  2.0043E-01  0.0000E+00  5.2030E-01  1.0000E-04   
 mr-sdci # 22 11   -710.3939832385  8.6392E-03  0.0000E+00  6.5695E-01  1.0000E-04   
 mr-sdci # 22 12   -710.1248295288  2.5171E-02  0.0000E+00  7.2455E-01  1.0000E-04   
 mr-sdci # 22 13   -710.0563385858  4.5183E-02  0.0000E+00  6.6239E-01  1.0000E-04   
 mr-sdci # 22 14   -710.0111551378 -1.0761E-01  0.0000E+00  8.1748E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002773
time for cinew                         0.065933
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064
   ht  14     8.20627337    -2.53022710    -3.13157608    -1.63402854     0.00199043     0.18884505    -1.09554096     2.15387864
   ht  15     0.59205004    -1.67968770    -0.45691776     0.06667060    -0.25065161     0.19839805     0.00055937     0.08527799

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320
   ht  14     0.26859395    -0.00045955     0.00408052     0.22653162    -1.05674394    -0.45254959
   ht  15    -0.04743852    -0.00017635     0.00009233     0.00068118    -0.05324268    -0.04925493    -0.02561361

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950226       6.744655E-04   6.642295E-02   5.480144E-02  -0.129750      -6.360064E-03   1.856790E-02   7.315795E-02
 ref    2   3.283690E-04  -0.956666      -1.251487E-03   0.112831       4.736094E-02  -1.048680E-02   3.484202E-02   9.952841E-04
 ref    3   3.774855E-02  -3.268151E-03  -0.931586       1.999422E-02  -0.174447       0.115631       3.527940E-02   2.347017E-02
 ref    4  -7.901441E-04  -0.104106       4.510490E-02  -0.852693      -0.371511       9.050579E-02   0.135448      -0.233411    

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -8.360323E-03  -0.115861       2.909265E-02   1.081648E-02  -0.150298      -0.125317      -3.072310E-02
 ref    2   3.425869E-02   1.689540E-02  -0.141837       5.473521E-02  -7.436227E-02   5.391261E-02   9.803197E-03
 ref    3  -3.681562E-02   9.428104E-02   7.941748E-02   9.385134E-02  -1.620954E-02  -0.105864      -2.492272E-02
 ref    4   0.157293      -9.301636E-02   3.480606E-02   4.318669E-02  -3.303703E-02   4.021928E-02   2.244376E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904356       0.926059       0.874301       0.743220       0.187530       2.171220E-02   2.114958E-02   6.038472E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.734001E-02   3.125020E-02   2.848259E-02   1.378610E-02   2.947336E-02   3.143555E-02   2.164876E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022622     0.00067447     0.06642295     0.05480144    -0.12975029    -0.00636006     0.01856790     0.07315795
 ref:   2     0.00032837    -0.95666586    -0.00125149     0.11283061     0.04736094    -0.01048680     0.03484202     0.00099528
 ref:   3     0.03774855    -0.00326815    -0.93158611     0.01999422    -0.17444669     0.11563078     0.03527940     0.02347017
 ref:   4    -0.00079014    -0.10410553     0.04510490    -0.85269333    -0.37151101     0.09050579     0.13544818    -0.23341122

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.00836032    -0.11586106     0.02909265     0.01081648    -0.15029775    -0.12531670    -0.03072310
 ref:   2     0.03425869     0.01689540    -0.14183655     0.05473521    -0.07436227     0.05391261     0.00980320
 ref:   3    -0.03681562     0.09428104     0.07941748     0.09385134    -0.01620954    -0.10586364    -0.02492272
 ref:   4     0.15729294    -0.09301636     0.03480606     0.04318669    -0.03303703     0.04021928     0.02244376

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -711.6262403891  4.0615E-11  0.0000E+00  3.9522E-04  1.0000E-03   
 mr-sdci # 23  2   -711.4954317908  3.0930E-09  0.0000E+00  6.7343E-04  1.0000E-03   
 mr-sdci # 23  3   -711.4773580546  4.1835E-05  1.9627E-05  4.7786E-03  1.0000E-03   
 mr-sdci # 23  4   -711.4305213422  1.1726E-02  0.0000E+00  1.7092E-01  1.0000E-03   
 mr-sdci # 23  5   -711.4147277759  1.0328E-02  0.0000E+00  1.4031E-01  1.0000E-04   
 mr-sdci # 23  6   -711.3038339578  1.5804E-02  0.0000E+00  1.7800E-01  1.0000E-04   
 mr-sdci # 23  7   -711.0213162999  1.2685E-02  0.0000E+00  3.8369E-01  1.0000E-04   
 mr-sdci # 23  8   -710.9514094658  1.7181E-02  0.0000E+00  4.8846E-01  1.0000E-04   
 mr-sdci # 23  9   -710.7692587092  3.5977E-02  0.0000E+00  5.5294E-01  1.0000E-04   
 mr-sdci # 23 10   -710.6893368306  2.4223E-03  0.0000E+00  5.0885E-01  1.0000E-04   
 mr-sdci # 23 11   -710.4194368031  2.5454E-02  0.0000E+00  6.8800E-01  1.0000E-04   
 mr-sdci # 23 12   -710.3937915577  2.6896E-01  0.0000E+00  6.4879E-01  1.0000E-04   
 mr-sdci # 23 13   -710.1189252798  6.2587E-02  0.0000E+00  7.0555E-01  1.0000E-04   
 mr-sdci # 23 14   -710.0393705423  2.8215E-02  0.0000E+00  6.8668E-01  1.0000E-04   
 mr-sdci # 23 15   -710.0045288687 -9.7313E-02  0.0000E+00  7.9285E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003029
time for cinew                         0.069828
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064
   ht  14     8.20627337    -2.53022710    -3.13157608    -1.63402854     0.00199043     0.18884505    -1.09554096     2.15387864
   ht  15     0.59205004    -1.67968770    -0.45691776     0.06667060    -0.25065161     0.19839805     0.00055937     0.08527799
   ht  16    -0.11027834     0.54752908    -0.38280370     0.00511263     0.10457420    -0.04042400     0.05759094    -0.00878583

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320
   ht  14     0.26859395    -0.00045955     0.00408052     0.22653162    -1.05674394    -0.45254959
   ht  15    -0.04743852    -0.00017635     0.00009233     0.00068118    -0.05324268    -0.04925493    -0.02561361
   ht  16     0.09584443    -0.00020956     0.00010042     0.00161277    -0.00238020     0.00472781     0.00478046    -0.00769364

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950226      -6.716675E-04   6.651312E-02  -1.897121E-02   0.139469       7.525740E-03  -1.860659E-02   7.144616E-02
 ref    2  -3.286981E-04   0.956700       7.298939E-04  -0.116032      -1.619078E-02  -1.505021E-03  -3.477523E-02  -9.878042E-03
 ref    3  -3.774865E-02   3.232622E-03  -0.931755      -6.221136E-04   0.170704      -0.100235      -3.542221E-02   4.330288E-02
 ref    4   7.938664E-04   0.103808       2.802113E-02   0.921054       0.144285      -2.148281E-02  -0.135511      -0.205037    

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -1.783284E-04  -0.116048      -2.228301E-02  -1.519532E-02   2.764545E-02  -0.174887      -8.822905E-02  -3.114756E-02
 ref    2   4.870310E-02   7.840152E-03   0.117356      -3.179795E-02   0.119007      -2.088212E-02   4.870134E-02   1.157624E-02
 ref    3  -6.256686E-02   0.105196      -9.705706E-02  -0.103291       2.126706E-02  -1.735382E-02  -0.111792      -3.338462E-02
 ref    4   0.112870      -6.008761E-02  -0.169310      -4.434504E-02   9.438910E-02   2.345920E-02   1.992855E-02   2.010342E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904356       0.926062       0.873377       0.862164       6.967167E-02   1.056751E-02   2.117339E-02   4.911751E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.902624E-02   2.820533E-02   5.235474E-02   1.387759E-02   2.428846E-02   3.187297E-02   2.305081E-02   2.622860E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022619    -0.00067167     0.06651312    -0.01897121     0.13946928     0.00752574    -0.01860659     0.07144616
 ref:   2    -0.00032870     0.95669975     0.00072989    -0.11603225    -0.01619078    -0.00150502    -0.03477523    -0.00987804
 ref:   3    -0.03774865     0.00323262    -0.93175507    -0.00062211     0.17070361    -0.10023521    -0.03542221     0.04330288
 ref:   4     0.00079387     0.10380846     0.02802113     0.92105403     0.14428488    -0.02148281    -0.13551065    -0.20503718

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00017833    -0.11604845    -0.02228301    -0.01519532     0.02764545    -0.17488686    -0.08822905    -0.03114756
 ref:   2     0.04870310     0.00784015     0.11735598    -0.03179795     0.11900670    -0.02088212     0.04870134     0.01157624
 ref:   3    -0.06256686     0.10519550    -0.09705706    -0.10329135     0.02126706    -0.01735382    -0.11179213    -0.03338462
 ref:   4     0.11286984    -0.06008761    -0.16930950    -0.04434504     0.09438910     0.02345920     0.01992855     0.02010342

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -711.6262403891  3.0553E-11  0.0000E+00  3.9524E-04  1.0000E-03   
 mr-sdci # 24  2   -711.4954318082  1.7380E-08  0.0000E+00  6.6820E-04  1.0000E-03   
 mr-sdci # 24  3   -711.4773838543  2.5800E-05  1.0036E-05  3.5347E-03  1.0000E-03   
 mr-sdci # 24  4   -711.4478915098  1.7370E-02  0.0000E+00  1.1648E-01  1.0000E-03   
 mr-sdci # 24  5   -711.4165524817  1.8247E-03  0.0000E+00  1.1518E-01  1.0000E-04   
 mr-sdci # 24  6   -711.3086270844  4.7931E-03  0.0000E+00  1.5771E-01  1.0000E-04   
 mr-sdci # 24  7   -711.0213165856  2.8575E-07  0.0000E+00  3.8361E-01  1.0000E-04   
 mr-sdci # 24  8   -710.9583669523  6.9575E-03  0.0000E+00  4.6936E-01  1.0000E-04   
 mr-sdci # 24  9   -710.7777099307  8.4512E-03  0.0000E+00  5.5329E-01  1.0000E-04   
 mr-sdci # 24 10   -710.6914664845  2.1297E-03  0.0000E+00  5.0188E-01  1.0000E-04   
 mr-sdci # 24 11   -710.5686662472  1.4923E-01  0.0000E+00  5.9106E-01  1.0000E-04   
 mr-sdci # 24 12   -710.3944445553  6.5300E-04  0.0000E+00  6.6422E-01  1.0000E-04   
 mr-sdci # 24 13   -710.1972746620  7.8349E-02  0.0000E+00  7.8917E-01  1.0000E-04   
 mr-sdci # 24 14   -710.0919086735  5.2538E-02  0.0000E+00  6.8087E-01  1.0000E-04   
 mr-sdci # 24 15   -710.0316628995  2.7134E-02  0.0000E+00  7.1454E-01  1.0000E-04   
 mr-sdci # 24 16   -710.0040904130  3.1427E-02  0.0000E+00  7.8450E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003407
time for cinew                         0.067673
time for eigenvalue solver             0.000000
time for vector access                 0.000004

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064
   ht  14     8.20627337    -2.53022710    -3.13157608    -1.63402854     0.00199043     0.18884505    -1.09554096     2.15387864
   ht  15     0.59205004    -1.67968770    -0.45691776     0.06667060    -0.25065161     0.19839805     0.00055937     0.08527799
   ht  16    -0.11027834     0.54752908    -0.38280370     0.00511263     0.10457420    -0.04042400     0.05759094    -0.00878583
   ht  17    -0.04132982    -0.19507700     0.34744958    -0.09214318    -0.05706148     0.03747659     0.00556795     0.01664780

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320
   ht  14     0.26859395    -0.00045955     0.00408052     0.22653162    -1.05674394    -0.45254959
   ht  15    -0.04743852    -0.00017635     0.00009233     0.00068118    -0.05324268    -0.04925493    -0.02561361
   ht  16     0.09584443    -0.00020956     0.00010042     0.00161277    -0.00238020     0.00472781     0.00478046    -0.00769364
   ht  17    -0.02481368     0.00020961    -0.00009665    -0.00075956     0.00898391     0.00291341    -0.00121381     0.00161489

                ht  17
   ht  17    -0.00338397

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950226      -6.716794E-04   6.649406E-02   9.427753E-03  -0.140305      -8.769163E-03   9.911788E-03  -6.728456E-02
 ref    2  -3.290813E-04   0.956720       1.938658E-03   0.111160       7.025317E-03   6.857708E-03  -5.350669E-02   7.202122E-03
 ref    3  -3.774691E-02   3.204725E-03  -0.932068      -6.373545E-04  -0.169899       9.743988E-02  -3.274502E-04  -6.452199E-02
 ref    4   8.102044E-04   0.103586       1.618352E-02  -0.932395      -8.427451E-02   2.280753E-03  -0.155766       8.276329E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -1.587702E-02   0.116826       2.678853E-02  -1.180706E-02   2.497245E-03  -9.279990E-03  -0.192375      -5.123565E-02
 ref    2  -5.600971E-02  -2.212297E-03  -0.105629      -4.738390E-02   0.102685      -5.068416E-02  -2.723826E-02   4.646754E-02
 ref    3   6.953638E-02  -0.103159       6.625549E-02  -8.116457E-02  -6.693188E-02  -9.950636E-02  -5.907929E-02  -6.957057E-02
 ref    4  -4.466610E-02   6.251064E-03   0.242464      -6.049922E-02   0.121353      -5.506179E-03   3.187686E-02   1.675111E-03

              v     17
 ref    1   2.898020E-02
 ref    2  -5.025212E-02
 ref    3   2.678758E-02
 ref    4   1.015621E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904355       0.926054       0.873438       0.881807       5.570261E-02   9.623658E-03   2.722440E-02   1.559193E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.021954E-02   2.433417E-02   7.505361E-02   1.263248E-02   2.975674E-02   1.258684E-02   4.225653E-02   9.627195E-03

              v     17
 ref    1   4.083734E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022594    -0.00067168     0.06649406     0.00942775    -0.14030491    -0.00876916     0.00991179    -0.06728456
 ref:   2    -0.00032908     0.95672027     0.00193866     0.11115981     0.00702532     0.00685771    -0.05350669     0.00720212
 ref:   3    -0.03774691     0.00320472    -0.93206832    -0.00063735    -0.16989878     0.09743988    -0.00032745    -0.06452199
 ref:   4     0.00081020     0.10358601     0.01618352    -0.93239543    -0.08427451     0.00228075    -0.15576613     0.08276329

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.01587702     0.11682610     0.02678853    -0.01180706     0.00249725    -0.00927999    -0.19237492    -0.05123565
 ref:   2    -0.05600971    -0.00221230    -0.10562869    -0.04738390     0.10268465    -0.05068416    -0.02723826     0.04646754
 ref:   3     0.06953638    -0.10315942     0.06625549    -0.08116457    -0.06693188    -0.09950636    -0.05907929    -0.06957057
 ref:   4    -0.04466610     0.00625106     0.24246396    -0.06049922     0.12135275    -0.00550618     0.03187686     0.00167511

                ci  17
 ref:   1     0.02898020
 ref:   2    -0.05025212
 ref:   3     0.02678758
 ref:   4     0.00101562

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -711.6262403901  1.0312E-09  0.0000E+00  3.9492E-04  1.0000E-03   
 mr-sdci # 25  2   -711.4954318206  1.2414E-08  0.0000E+00  6.6597E-04  1.0000E-03   
 mr-sdci # 25  3   -711.4773958628  1.2008E-05  3.9205E-06  2.1134E-03  1.0000E-03   
 mr-sdci # 25  4   -711.4560309412  8.1394E-03  0.0000E+00  7.1667E-02  1.0000E-03   
 mr-sdci # 25  5   -711.4172040373  6.5156E-04  0.0000E+00  1.1221E-01  1.0000E-04   
 mr-sdci # 25  6   -711.3099122978  1.2852E-03  0.0000E+00  1.5281E-01  1.0000E-04   
 mr-sdci # 25  7   -711.0436932251  2.2377E-02  0.0000E+00  4.1511E-01  1.0000E-04   
 mr-sdci # 25  8   -710.9894939567  3.1127E-02  0.0000E+00  3.5940E-01  1.0000E-04   
 mr-sdci # 25  9   -710.7902283705  1.2518E-02  0.0000E+00  5.5279E-01  1.0000E-04   
 mr-sdci # 25 10   -710.6958249450  4.3585E-03  0.0000E+00  4.8979E-01  1.0000E-04   
 mr-sdci # 25 11   -710.5990875767  3.0421E-02  0.0000E+00  6.0355E-01  1.0000E-04   
 mr-sdci # 25 12   -710.4002040434  5.7595E-03  0.0000E+00  6.5459E-01  1.0000E-04   
 mr-sdci # 25 13   -710.2552021099  5.7927E-02  0.0000E+00  7.5376E-01  1.0000E-04   
 mr-sdci # 25 14   -710.1260649365  3.4156E-02  0.0000E+00  8.2345E-01  1.0000E-04   
 mr-sdci # 25 15   -710.0879772868  5.6314E-02  0.0000E+00  6.5357E-01  1.0000E-04   
 mr-sdci # 25 16   -710.0240470796  1.9957E-02  0.0000E+00  7.8018E-01  1.0000E-04   
 mr-sdci # 25 17   -709.9659169625  1.4431E-02  0.0000E+00  6.9980E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.004234
time for cinew                         0.072903
time for eigenvalue solver             0.000259
time for vector access                 0.000000

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064
   ht  14     8.20627337    -2.53022710    -3.13157608    -1.63402854     0.00199043     0.18884505    -1.09554096     2.15387864
   ht  15     0.59205004    -1.67968770    -0.45691776     0.06667060    -0.25065161     0.19839805     0.00055937     0.08527799
   ht  16    -0.11027834     0.54752908    -0.38280370     0.00511263     0.10457420    -0.04042400     0.05759094    -0.00878583
   ht  17    -0.04132982    -0.19507700     0.34744958    -0.09214318    -0.05706148     0.03747659     0.00556795     0.01664780
   ht  18     0.09522065     0.16893406    -0.11606843     0.12463834     0.05513551     0.01933033    -0.00767760     0.03757263

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320
   ht  14     0.26859395    -0.00045955     0.00408052     0.22653162    -1.05674394    -0.45254959
   ht  15    -0.04743852    -0.00017635     0.00009233     0.00068118    -0.05324268    -0.04925493    -0.02561361
   ht  16     0.09584443    -0.00020956     0.00010042     0.00161277    -0.00238020     0.00472781     0.00478046    -0.00769364
   ht  17    -0.02481368     0.00020961    -0.00009665    -0.00075956     0.00898391     0.00291341    -0.00121381     0.00161489
   ht  18     0.00015093    -0.00010733     0.00004394     0.00412626    -0.01146431    -0.00282100     0.00112384    -0.00086527

                ht  17         ht  18
   ht  17    -0.00338397
   ht  18     0.00056194    -0.00146572

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950227      -6.703600E-04  -6.648993E-02   5.519354E-03   0.139424       9.838675E-03  -3.528006E-02   5.908565E-02
 ref    2  -3.324693E-04   0.956754      -2.769251E-03   0.112788      -2.827016E-03  -7.289508E-03   3.248594E-02   1.692674E-02
 ref    3  -3.774628E-02   3.182012E-03   0.932186       2.583855E-04   0.168173      -9.619822E-02  -3.182233E-02   6.192527E-02
 ref    4   8.279842E-04   0.103334      -9.424021E-03  -0.939960       4.645303E-02   6.636714E-04   0.108214      -1.310534E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -2.266333E-02   0.116811       2.874779E-02   3.675544E-03   1.553729E-02  -5.794469E-02  -4.494581E-02  -0.173574    
 ref    2  -3.319979E-02  -2.111251E-03  -0.133209       7.083644E-02  -1.453680E-02  -9.282402E-02   2.694765E-02  -1.624649E-02
 ref    3   5.909761E-02  -0.103218       7.656670E-02   3.337658E-02   9.004626E-02   1.983116E-03   8.545876E-02  -8.055980E-02
 ref    4  -1.536541E-02   6.275107E-03   0.156407       0.226570      -0.116321      -1.503548E-02   1.752863E-02   1.297301E-02

              v     17       v     18
 ref    1  -7.235591E-02   1.107279E-02
 ref    2   5.029569E-02   4.888821E-02
 ref    3  -8.373029E-02   7.314909E-03
 ref    4  -6.148929E-04   1.572058E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904356       0.926066       0.873487       0.896276       4.988715E-02   9.404475E-03   1.502285E-02   7.784118E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   5.344476E-03   2.434254E-02   4.889671E-02   5.747942E-02   2.209161E-02   1.220388E-02   1.035675E-02   3.704996E-02

              v     17       v     18
 ref    1   1.477617E-02   2.813309E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022671    -0.00067036    -0.06648993     0.00551935     0.13942416     0.00983868    -0.03528006     0.05908565
 ref:   2    -0.00033247     0.95675359    -0.00276925     0.11278847    -0.00282702    -0.00728951     0.03248594     0.01692674
 ref:   3    -0.03774628     0.00318201     0.93218564     0.00025839     0.16817305    -0.09619822    -0.03182233     0.06192527
 ref:   4     0.00082798     0.10333374    -0.00942402    -0.93995972     0.04645303     0.00066367     0.10821355    -0.01310534

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.02266333     0.11681060     0.02874779     0.00367554     0.01553729    -0.05794469    -0.04494581    -0.17357371
 ref:   2    -0.03319979    -0.00211125    -0.13320942     0.07083644    -0.01453680    -0.09282402     0.02694765    -0.01624649
 ref:   3     0.05909761    -0.10321816     0.07656670     0.03337658     0.09004626     0.00198312     0.08545876    -0.08055980
 ref:   4    -0.01536541     0.00627511     0.15640673     0.22657032    -0.11632093    -0.01503548     0.01752863     0.01297301

                ci  17         ci  18
 ref:   1    -0.07235591     0.01107279
 ref:   2     0.05029569     0.04888821
 ref:   3    -0.08373029     0.00731491
 ref:   4    -0.00061489     0.01572058

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -711.6262403922  2.0349E-09  0.0000E+00  3.9238E-04  1.0000E-03   
 mr-sdci # 26  2   -711.4954318442  2.3603E-08  0.0000E+00  6.4464E-04  1.0000E-03   
 mr-sdci # 26  3   -711.4774005824  4.7196E-06  1.5270E-06  1.3399E-03  1.0000E-03   
 mr-sdci # 26  4   -711.4598977418  3.8668E-03  0.0000E+00  4.8813E-02  1.0000E-03   
 mr-sdci # 26  5   -711.4179838671  7.7983E-04  0.0000E+00  1.0975E-01  1.0000E-04   
 mr-sdci # 26  6   -711.3100434041  1.3111E-04  0.0000E+00  1.5287E-01  1.0000E-04   
 mr-sdci # 26  7   -711.1355949447  9.1902E-02  0.0000E+00  3.5636E-01  1.0000E-04   
 mr-sdci # 26  8   -710.9996113201  1.0117E-02  0.0000E+00  3.2795E-01  1.0000E-04   
 mr-sdci # 26  9   -710.8008841633  1.0656E-02  0.0000E+00  5.4675E-01  1.0000E-04   
 mr-sdci # 26 10   -710.6958250060  6.1018E-08  0.0000E+00  4.8980E-01  1.0000E-04   
 mr-sdci # 26 11   -710.6261899548  2.7102E-02  0.0000E+00  5.5216E-01  1.0000E-04   
 mr-sdci # 26 12   -710.4255375876  2.5334E-02  0.0000E+00  6.4712E-01  1.0000E-04   
 mr-sdci # 26 13   -710.3795400544  1.2434E-01  0.0000E+00  6.7405E-01  1.0000E-04   
 mr-sdci # 26 14   -710.1766311712  5.0566E-02  0.0000E+00  8.0087E-01  1.0000E-04   
 mr-sdci # 26 15   -710.1205657568  3.2588E-02  0.0000E+00  8.1853E-01  1.0000E-04   
 mr-sdci # 26 16   -710.0657826306  4.1736E-02  0.0000E+00  6.7660E-01  1.0000E-04   
 mr-sdci # 26 17   -710.0221869151  5.6270E-02  0.0000E+00  7.5410E-01  1.0000E-04   
 mr-sdci # 26 18   -709.9338626313  2.8312E-02  0.0000E+00  7.1557E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.004517
time for cinew                         0.068569
time for eigenvalue solver             0.000286
time for vector access                 0.000000

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064
   ht  14     8.20627337    -2.53022710    -3.13157608    -1.63402854     0.00199043     0.18884505    -1.09554096     2.15387864
   ht  15     0.59205004    -1.67968770    -0.45691776     0.06667060    -0.25065161     0.19839805     0.00055937     0.08527799
   ht  16    -0.11027834     0.54752908    -0.38280370     0.00511263     0.10457420    -0.04042400     0.05759094    -0.00878583
   ht  17    -0.04132982    -0.19507700     0.34744958    -0.09214318    -0.05706148     0.03747659     0.00556795     0.01664780
   ht  18     0.09522065     0.16893406    -0.11606843     0.12463834     0.05513551     0.01933033    -0.00767760     0.03757263
   ht  19     0.04986768     0.09070082    -0.04134754     0.06809405    -0.00106176    -0.00030209     0.01242938    -0.03121417

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320
   ht  14     0.26859395    -0.00045955     0.00408052     0.22653162    -1.05674394    -0.45254959
   ht  15    -0.04743852    -0.00017635     0.00009233     0.00068118    -0.05324268    -0.04925493    -0.02561361
   ht  16     0.09584443    -0.00020956     0.00010042     0.00161277    -0.00238020     0.00472781     0.00478046    -0.00769364
   ht  17    -0.02481368     0.00020961    -0.00009665    -0.00075956     0.00898391     0.00291341    -0.00121381     0.00161489
   ht  18     0.00015093    -0.00010733     0.00004394     0.00412626    -0.01146431    -0.00282100     0.00112384    -0.00086527
   ht  19    -0.01376072    -0.00005610     0.00002399     0.00047514    -0.00469365    -0.00036497     0.00015636    -0.00018581

                ht  17         ht  18         ht  19
   ht  17    -0.00338397
   ht  18     0.00056194    -0.00146572
   ht  19     0.00035341    -0.00017044    -0.00051319

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950226      -6.720222E-04  -6.639795E-02   1.027970E-03   0.139629       1.085351E-02  -2.741156E-02   5.512725E-02
 ref    2   3.319684E-04   0.956756      -3.048553E-03   0.110393       2.256362E-03  -9.377269E-03   3.875764E-02   1.900450E-02
 ref    3   3.774457E-02   3.176876E-03   0.932348      -3.108819E-03   0.168665      -9.534369E-02  -2.368294E-02   5.724425E-02
 ref    4  -8.346719E-04   0.103299      -6.346930E-03  -0.943187       2.154613E-02   2.690092E-03   6.116116E-02  -3.890980E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.380448E-02   3.742017E-02   0.121057      -1.707359E-02   2.317876E-02   7.594923E-02  -3.122898E-03   0.102072    
 ref    2   3.193504E-02   7.898751E-02  -7.479572E-02  -7.147361E-03   4.611008E-02   0.149635       1.795346E-02   8.858615E-03
 ref    3  -5.702349E-02  -0.140751      -1.473293E-02   3.988783E-03   9.350273E-02   1.247221E-02   5.705235E-02  -4.443114E-02
 ref    4   1.446256E-02  -3.197703E-02   4.416171E-02   0.297707      -1.354269E-02   4.122515E-02   9.013037E-03  -1.655694E-02

              v     17       v     18       v     19
 ref    1   0.153361       2.178231E-02  -1.461540E-02
 ref    2  -4.377984E-02  -4.260299E-03  -4.406245E-02
 ref    3   0.120394      -5.141737E-03  -8.737147E-03
 ref    4  -6.991555E-03  -5.400289E-03  -1.566702E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904355       0.926063       0.873731       0.901799       4.841352E-02   9.303388E-03   6.555117E-03   6.692228E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   5.047344E-03   2.847280E-02   2.241657E-02   8.898801E-02   1.158956E-02   3.001384E-02   3.668285E-03   1.274549E-02

              v     17       v     18       v     19
 ref    1   3.997970E-02   5.482195E-04   2.476903E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18        ev   19

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18       v     19

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022585    -0.00067202    -0.06639795     0.00102797     0.13962922     0.01085351    -0.02741156     0.05512725
 ref:   2     0.00033197     0.95675591    -0.00304855     0.11039333     0.00225636    -0.00937727     0.03875764     0.01900450
 ref:   3     0.03774457     0.00317688     0.93234805    -0.00310882     0.16866498    -0.09534369    -0.02368294     0.05724425
 ref:   4    -0.00083467     0.10329858    -0.00634693    -0.94318703     0.02154613     0.00269009     0.06116116    -0.00389098

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.02380448     0.03742017     0.12105725    -0.01707359     0.02317876     0.07594923    -0.00312290     0.10207230
 ref:   2     0.03193504     0.07898751    -0.07479572    -0.00714736     0.04611008     0.14963451     0.01795346     0.00885861
 ref:   3    -0.05702349    -0.14075145    -0.01473293     0.00398878     0.09350273     0.01247221     0.05705235    -0.04443114
 ref:   4     0.01446256    -0.03197703     0.04416171     0.29770709    -0.01354269     0.04122515     0.00901304    -0.01655694

                ci  17         ci  18         ci  19
 ref:   1     0.15336084     0.02178231    -0.01461540
 ref:   2    -0.04377984    -0.00426030    -0.04406245
 ref:   3     0.12039351    -0.00514174    -0.00873715
 ref:   4    -0.00699156    -0.00540029    -0.01566702

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -711.6262403928  6.4176E-10  0.0000E+00  3.9108E-04  1.0000E-03   
 mr-sdci # 27  2   -711.4954318451  9.1057E-10  0.0000E+00  6.4399E-04  1.0000E-03   
 mr-sdci # 27  3   -711.4774022644  1.6821E-06  0.0000E+00  7.3694E-04  1.0000E-03   
 mr-sdci # 27  4   -711.4615591730  1.6614E-03  9.7493E-04  3.3353E-02  1.0000E-03   
 mr-sdci # 27  5   -711.4188647575  8.8089E-04  0.0000E+00  1.0875E-01  1.0000E-04   
 mr-sdci # 27  6   -711.3103307609  2.8736E-04  0.0000E+00  1.5234E-01  1.0000E-04   
 mr-sdci # 27  7   -711.2120288333  7.6434E-02  0.0000E+00  2.5027E-01  1.0000E-04   
 mr-sdci # 27  8   -711.0008369378  1.2256E-03  0.0000E+00  3.2783E-01  1.0000E-04   
 mr-sdci # 27  9   -710.8009486019  6.4439E-05  0.0000E+00  5.4696E-01  1.0000E-04   
 mr-sdci # 27 10   -710.7013644879  5.5395E-03  0.0000E+00  4.9104E-01  1.0000E-04   
 mr-sdci # 27 11   -710.6920118829  6.5822E-02  0.0000E+00  5.0295E-01  1.0000E-04   
 mr-sdci # 27 12   -710.4688390694  4.3301E-02  0.0000E+00  6.6022E-01  1.0000E-04   
 mr-sdci # 27 13   -710.3952186221  1.5679E-02  0.0000E+00  6.5362E-01  1.0000E-04   
 mr-sdci # 27 14   -710.2065433239  2.9912E-02  0.0000E+00  7.3827E-01  1.0000E-04   
 mr-sdci # 27 15   -710.1472430049  2.6677E-02  0.0000E+00  8.3301E-01  1.0000E-04   
 mr-sdci # 27 16   -710.1076487446  4.1866E-02  0.0000E+00  7.5466E-01  1.0000E-04   
 mr-sdci # 27 17   -710.0372433369  1.5056E-02  0.0000E+00  6.7316E-01  1.0000E-04   
 mr-sdci # 27 18   -709.9855991461  5.1737E-02  0.0000E+00  7.8212E-01  1.0000E-04   
 mr-sdci # 27 19   -709.9324976003  1.2437E-01  0.0000E+00  7.0052E-01  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000343
time for cinew                         0.155884
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955053
   ht   2     0.00000000  -242.74873363
   ht   3     0.00000000     0.00000000  -242.72997791
   ht   4    -0.00000000    -0.00000000     0.00000000  -242.66972561
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.62874335
   ht   6    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000  -242.47829037
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000  -242.18218128
   ht   8    -0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000     0.00000000    -0.00000000  -242.05409146
   ht   9     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    -0.01073638     0.02557491    -0.08115742     0.02033211     0.02445333     0.09049717     0.04555012     0.04713610
   ht  11    -0.07283572    -0.02443519     0.06677667     0.04159507     0.02230489     0.05418601     0.05311475    -0.00343694
   ht  12    -5.44823954    -1.07514576     0.64327882     1.27563282    -0.46184593    -0.16623339     0.80533026    -1.51410474
   ht  13    24.57839660     2.67708993    -4.43110284    -5.74092476     0.48533567     1.35300389    -2.99123251     6.04417064
   ht  14     8.20627337    -2.53022710    -3.13157608    -1.63402854     0.00199043     0.18884505    -1.09554096     2.15387864
   ht  15     0.59205004    -1.67968770    -0.45691776     0.06667060    -0.25065161     0.19839805     0.00055937     0.08527799
   ht  16    -0.11027834     0.54752908    -0.38280370     0.00511263     0.10457420    -0.04042400     0.05759094    -0.00878583
   ht  17    -0.04132982    -0.19507700     0.34744958    -0.09214318    -0.05706148     0.03747659     0.00556795     0.01664780
   ht  18     0.09522065     0.16893406    -0.11606843     0.12463834     0.05513551     0.01933033    -0.00767760     0.03757263
   ht  19     0.04986768     0.09070082    -0.04134754     0.06809405    -0.00106176    -0.00030209     0.01242938    -0.03121417
   ht  20    29.20003447     1.23087599    -5.88032594    -4.69918585     1.58688175     1.42336261    -3.70139206     6.65140817

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9  -241.86867387
   ht  10     0.03996115    -0.00167147
   ht  11     0.00974223     0.00011505    -0.00062590
   ht  12     0.05541188     0.00043085    -0.00274755    -0.24809067
   ht  13     0.24811972    -0.00155131     0.01132453     0.72814866    -3.31243320
   ht  14     0.26859395    -0.00045955     0.00408052     0.22653162    -1.05674394    -0.45254959
   ht  15    -0.04743852    -0.00017635     0.00009233     0.00068118    -0.05324268    -0.04925493    -0.02561361
   ht  16     0.09584443    -0.00020956     0.00010042     0.00161277    -0.00238020     0.00472781     0.00478046    -0.00769364
   ht  17    -0.02481368     0.00020961    -0.00009665    -0.00075956     0.00898391     0.00291341    -0.00121381     0.00161489
   ht  18     0.00015093    -0.00010733     0.00004394     0.00412626    -0.01146431    -0.00282100     0.00112384    -0.00086527
   ht  19    -0.01376072    -0.00005610     0.00002399     0.00047514    -0.00469365    -0.00036497     0.00015636    -0.00018581
   ht  20     0.18175857    -0.00141701     0.01354578     0.83803961    -3.72230623    -1.27182862    -0.07178706     0.00282483

                ht  17         ht  18         ht  19         ht  20
   ht  17    -0.00338397
   ht  18     0.00056194    -0.00146572
   ht  19     0.00035341    -0.00017044    -0.00051319
   ht  20     0.01188363    -0.01807105    -0.00581507    -4.59158600

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950226       6.706075E-04  -6.639964E-02   5.293311E-04   0.138647      -8.842928E-03   3.401742E-02   5.496731E-02
 ref    2   3.319981E-04  -0.956765      -3.167148E-03   0.109719       4.204993E-03   7.856575E-03  -3.634982E-02   1.351754E-02
 ref    3   3.774455E-02  -3.170974E-03   0.932372      -3.606308E-03   0.167901       9.649271E-02   1.885439E-02   6.074910E-02
 ref    4  -8.353326E-04  -0.103196      -5.157802E-03  -0.946323       1.062985E-02  -2.734993E-03  -5.098564E-02  -1.608543E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -9.143037E-03  -1.884281E-02  -0.120524      -4.558903E-02   2.647028E-02   1.205492E-02  -6.270392E-02   0.121152    
 ref    2   6.581502E-02  -6.093084E-02   1.878663E-02   4.174974E-02   2.191446E-02   0.158055      -4.810169E-02   2.557439E-02
 ref    3  -4.425058E-02   8.250076E-02   9.031073E-02  -5.891769E-02   0.105372      -2.924417E-02  -8.219741E-02   3.733045E-02
 ref    4  -6.942393E-02   1.155407E-02  -2.172255E-02   0.233598       6.878910E-02  -8.675705E-02  -6.333256E-02   8.826447E-02

              v     17       v     18       v     19       v     20
 ref    1  -9.929586E-02  -0.105550      -1.128983E-02  -3.609658E-02
 ref    2  -8.716549E-03   3.940820E-02   4.506172E-02  -1.308525E-02
 ref    3   4.592776E-02  -9.668793E-02  -2.099564E-03  -1.628650E-02
 ref    4   1.825779E-02   2.992709E-02   4.326769E-02   5.354279E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904355       0.926059       0.873763       0.907579       4.754442E-02   9.458247E-03   5.433518E-03   6.897170E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.119301E-02   1.100749E-02   2.350678E-02   6.186086E-02   1.701617E-02   3.350859E-02   1.701298E-02   2.451609E-02

              v     17       v     18       v     19       v     20
 ref    1   1.237835E-02   2.293806E-02   4.034520E-03   4.606267E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

               ev   17        ev   18        ev   19        ev   20

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

              v     17       v     18       v     19       v     20

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022592     0.00067061    -0.06639964     0.00052933     0.13864657    -0.00884293     0.03401742     0.05496731
 ref:   2     0.00033200    -0.95676509    -0.00316715     0.10971882     0.00420499     0.00785658    -0.03634982     0.01351754
 ref:   3     0.03774455    -0.00317097     0.93237210    -0.00360631     0.16790137     0.09649271     0.01885439     0.06074910
 ref:   4    -0.00083533    -0.10319636    -0.00515780    -0.94632312     0.01062985    -0.00273499    -0.05098564    -0.00160854

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00914304    -0.01884281    -0.12052364    -0.04558903     0.02647028     0.01205492    -0.06270392     0.12115223
 ref:   2     0.06581502    -0.06093084     0.01878663     0.04174974     0.02191446     0.15805463    -0.04810169     0.02557439
 ref:   3    -0.04425058     0.08250076     0.09031073    -0.05891769     0.10537224    -0.02924417    -0.08219741     0.03733045
 ref:   4    -0.06942393     0.01155407    -0.02172255     0.23359829     0.06878910    -0.08675705    -0.06333256     0.08826447

                ci  17         ci  18         ci  19         ci  20
 ref:   1    -0.09929586    -0.10555031    -0.01128983    -0.03609658
 ref:   2    -0.00871655     0.03940820     0.04506172    -0.01308525
 ref:   3     0.04592776    -0.09668793    -0.00209956    -0.01628650
 ref:   4     0.01825779     0.02992709     0.04326769     0.05354279

 trial vector basis is being transformed.  new dimension:   9

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -711.6262403928  7.2475E-12  0.0000E+00  3.9088E-04  1.0000E-03   
 mr-sdci # 28  2   -711.4954318562  1.1140E-08  0.0000E+00  6.3640E-04  1.0000E-03   
 mr-sdci # 28  3   -711.4774026149  3.5045E-07  0.0000E+00  4.7574E-04  1.0000E-03   
 mr-sdci # 28  4   -711.4625263203  9.6715E-04  2.5348E-04  1.6197E-02  1.0000E-03   
 mr-sdci # 28  5   -711.4192247558  3.6000E-04  0.0000E+00  1.0664E-01  1.0000E-04   
 mr-sdci # 28  6   -711.3106069488  2.7619E-04  0.0000E+00  1.5151E-01  1.0000E-04   
 mr-sdci # 28  7   -711.2482639446  3.6235E-02  0.0000E+00  1.9552E-01  1.0000E-04   
 mr-sdci # 28  8   -711.0028412074  2.0043E-03  0.0000E+00  3.2789E-01  1.0000E-04   
 mr-sdci # 28  9   -710.8527758845  5.1827E-02  0.0000E+00  4.4965E-01  1.0000E-04   
 mr-sdci # 28 10   -710.7963326521  9.4968E-02  0.0000E+00  5.4717E-01  1.0000E-04   
 mr-sdci # 28 11   -710.6949143483  2.9025E-03  0.0000E+00  4.9176E-01  1.0000E-04   
 mr-sdci # 28 12   -710.5135762232  4.4737E-02  0.0000E+00  6.2037E-01  1.0000E-04   
 mr-sdci # 28 13   -710.4031126765  7.8941E-03  0.0000E+00  6.6739E-01  1.0000E-04   
 mr-sdci # 28 14   -710.2604518784  5.3909E-02  0.0000E+00  6.6909E-01  1.0000E-04   
 mr-sdci # 28 15   -710.1514763917  4.2334E-03  0.0000E+00  8.0429E-01  1.0000E-04   
 mr-sdci # 28 16   -710.1336695722  2.6021E-02  0.0000E+00  7.2134E-01  1.0000E-04   
 mr-sdci # 28 17   -710.1076390335  7.0396E-02  0.0000E+00  7.5591E-01  1.0000E-04   
 mr-sdci # 28 18   -710.0215316550  3.5933E-02  0.0000E+00  7.1567E-01  1.0000E-04   
 mr-sdci # 28 19   -709.9406713536  8.1738E-03  0.0000E+00  7.6235E-01  1.0000E-04   
 mr-sdci # 28 20   -709.9039834887  1.9375E-01  0.0000E+00  7.2698E-01  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001865
time for cinew                         0.093327
time for eigenvalue solver             0.000324
time for vector access                 0.000000

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955054
   ht   2    -0.00000000  -242.74874201
   ht   3    -0.00000000     0.00000000  -242.73071277
   ht   4    -0.00000000     0.00000000     0.00000000  -242.71583647
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.67253491
   ht   6    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000  -242.56391710
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000  -242.50157410
   ht   8     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.25615136
   ht   9     0.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    24.87934782    -0.16885485     3.98503333    -0.40581901    -3.80449213    -0.95010985     1.91264760    -0.69522807

                ht   9         ht  10
   ht   9  -242.10608604
   ht  10    -3.70074706    -3.10270385

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950228      -6.785259E-04   6.638087E-02   8.933286E-04   0.140510      -9.914889E-03  -2.284754E-02  -6.170758E-02
 ref    2   3.323030E-04   0.956765       3.168183E-03  -0.109512       4.659771E-03   7.536655E-03   3.755381E-02  -1.313534E-02
 ref    3   3.774506E-02   3.168833E-03  -0.932377       3.994464E-03   0.168172       9.639406E-02  -1.635026E-02  -6.259732E-02
 ref    4  -8.347896E-04   0.103189       5.123015E-03   0.946420       9.540600E-03  -2.773681E-03   4.993132E-02   1.155205E-03

              v      9       v     10
 ref    1   4.302122E-02  -0.212843    
 ref    2  -6.013719E-02  -4.389759E-02
 ref    3   4.768273E-02  -2.432180E-02
 ref    4   6.490220E-02   2.415108E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904360       0.926057       0.873770       0.907720       4.813758E-02   9.454614E-03   4.692766E-03   7.900121E-03

              v      9       v     10
 ref    1   1.195324E-02   4.840385E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022841    -0.00067853     0.06638087     0.00089333     0.14051021    -0.00991489    -0.02284754    -0.06170758
 ref:   2     0.00033230     0.95676471     0.00316818    -0.10951175     0.00465977     0.00753666     0.03755381    -0.01313534
 ref:   3     0.03774506     0.00316883    -0.93237725     0.00399446     0.16817171     0.09639406    -0.01635026    -0.06259732
 ref:   4    -0.00083479     0.10318930     0.00512302     0.94642003     0.00954060    -0.00277368     0.04993132     0.00115520

                ci   9         ci  10
 ref:   1     0.04302122    -0.21284273
 ref:   2    -0.06013719    -0.04389759
 ref:   3     0.04768273    -0.02432180
 ref:   4     0.06490220     0.02415108

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -711.6262403930  1.6368E-10  0.0000E+00  3.9076E-04  1.0000E-03   
 mr-sdci # 29  2   -711.4954318576  1.3722E-09  0.0000E+00  6.3353E-04  1.0000E-03   
 mr-sdci # 29  3   -711.4774026221  7.2367E-09  0.0000E+00  4.6817E-04  1.0000E-03   
 mr-sdci # 29  4   -711.4625647289  3.8409E-05  3.4216E-03  1.4602E-02  1.0000E-03   
 mr-sdci # 29  5   -711.4193095512  8.4795E-05  0.0000E+00  1.0507E-01  1.0000E-04   
 mr-sdci # 29  6   -711.3106394935  3.2545E-05  0.0000E+00  1.5139E-01  1.0000E-04   
 mr-sdci # 29  7   -711.2507678236  2.5039E-03  0.0000E+00  1.8795E-01  1.0000E-04   
 mr-sdci # 29  8   -711.0034344184  5.9321E-04  0.0000E+00  3.2451E-01  1.0000E-04   
 mr-sdci # 29  9   -710.8685072859  1.5731E-02  0.0000E+00  4.4648E-01  1.0000E-04   
 mr-sdci # 29 10   -710.2626498582 -5.3368E-01  0.0000E+00  6.4084E-01  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001785
time for cinew                         0.061584
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955054
   ht   2    -0.00000000  -242.74874201
   ht   3    -0.00000000     0.00000000  -242.73071277
   ht   4    -0.00000000     0.00000000     0.00000000  -242.71583647
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.67253491
   ht   6    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000  -242.56391710
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000  -242.50157410
   ht   8     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.25615136
   ht   9     0.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    24.87934782    -0.16885485     3.98503333    -0.40581901    -3.80449213    -0.95010985     1.91264760    -0.69522807
   ht  11   385.82977780    -1.19421184    87.60758565    -0.61828947   -89.06874459   -13.66349712    39.58670091    -6.54291808

                ht   9         ht  10         ht  11
   ht   9  -242.10608604
   ht  10    -3.70074706    -3.10270385
   ht  11   -58.38282031   -48.31246913  -781.66371374

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950228      -6.777409E-04   6.638099E-02  -1.063000E-03   0.139588       8.680900E-03   2.787120E-02   6.052413E-02
 ref    2   3.312408E-04   0.956769       3.187623E-03   0.109774       4.822316E-03  -7.276305E-03  -3.230125E-02   2.596920E-03
 ref    3   3.774505E-02   3.166670E-03  -0.932384      -4.486018E-03   0.167642      -9.708420E-02   1.560873E-02   6.510622E-02
 ref    4  -8.312753E-04   0.103162       4.975724E-03  -0.947187       5.804618E-03   3.474373E-03  -4.893747E-02   4.574974E-03

              v      9       v     10       v     11
 ref    1  -1.376088E-02  -0.125606      -0.181591    
 ref    2   6.873571E-02  -1.932147E-02  -3.785424E-02
 ref    3  -2.870870E-02  -4.629869E-02  -1.030968E-02
 ref    4  -5.854689E-02  -4.903841E-03   2.474044E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904358       0.926060       0.873781       0.909235       4.764574E-02   9.565716E-03   4.458683E-03   7.929664E-03

              v      9       v     10       v     11
 ref    1   9.165887E-03   1.831782E-02   3.512655E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022752    -0.00067774     0.06638099    -0.00106300     0.13958830     0.00868090     0.02787120     0.06052413
 ref:   2     0.00033124     0.95676917     0.00318762     0.10977364     0.00482232    -0.00727630    -0.03230125     0.00259692
 ref:   3     0.03774505     0.00316667    -0.93238366    -0.00448602     0.16764217    -0.09708420     0.01560873     0.06510622
 ref:   4    -0.00083128     0.10316151     0.00497572    -0.94718741     0.00580462     0.00347437    -0.04893747     0.00457497

                ci   9         ci  10         ci  11
 ref:   1    -0.01376088    -0.12560607    -0.18159081
 ref:   2     0.06873571    -0.01932147    -0.03785424
 ref:   3    -0.02870870    -0.04629869    -0.01030968
 ref:   4    -0.05854689    -0.00490384     0.02474044

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -711.6262403939  9.5486E-10  0.0000E+00  3.8906E-04  1.0000E-03   
 mr-sdci # 30  2   -711.4954318622  4.5704E-09  0.0000E+00  6.2796E-04  1.0000E-03   
 mr-sdci # 30  3   -711.4774026523  3.0140E-08  0.0000E+00  4.4931E-04  1.0000E-03   
 mr-sdci # 30  4   -711.4627160925  1.5136E-04  7.5396E-05  8.8219E-03  1.0000E-03   
 mr-sdci # 30  5   -711.4196510374  3.4149E-04  0.0000E+00  1.0487E-01  1.0000E-04   
 mr-sdci # 30  6   -711.3107767973  1.3730E-04  0.0000E+00  1.5051E-01  1.0000E-04   
 mr-sdci # 30  7   -711.2615562952  1.0788E-02  0.0000E+00  1.8125E-01  1.0000E-04   
 mr-sdci # 30  8   -711.0071107638  3.6763E-03  0.0000E+00  3.1950E-01  1.0000E-04   
 mr-sdci # 30  9   -710.9215762795  5.3069E-02  0.0000E+00  4.1670E-01  1.0000E-04   
 mr-sdci # 30 10   -710.4144235220  1.5177E-01  0.0000E+00  7.4711E-01  1.0000E-04   
 mr-sdci # 30 11   -710.2415505945 -4.5336E-01  0.0000E+00  6.7954E-01  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002769
time for cinew                         0.061867
time for eigenvalue solver             0.000179
time for vector access                 0.000000

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955054
   ht   2    -0.00000000  -242.74874201
   ht   3    -0.00000000     0.00000000  -242.73071277
   ht   4    -0.00000000     0.00000000     0.00000000  -242.71583647
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.67253491
   ht   6    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000  -242.56391710
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000  -242.50157410
   ht   8     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.25615136
   ht   9     0.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    24.87934782    -0.16885485     3.98503333    -0.40581901    -3.80449213    -0.95010985     1.91264760    -0.69522807
   ht  11   385.82977780    -1.19421184    87.60758565    -0.61828947   -89.06874459   -13.66349712    39.58670091    -6.54291808
   ht  12     0.11439348    -0.20603961     1.06378394     0.35876010    -1.02113768    -0.03592811    -0.18965698     0.32721198

                ht   9         ht  10         ht  11         ht  12
   ht   9  -242.10608604
   ht  10    -3.70074706    -3.10270385
   ht  11   -58.38282031   -48.31246913  -781.66371374
   ht  12    -0.66958598    -0.05934795    -1.31699355    -0.03790702

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950227      -6.765473E-04   6.638646E-02   8.133694E-04   0.139131      -6.243462E-03  -3.325387E-02  -3.253469E-02
 ref    2   3.312670E-04   0.956770       3.196021E-03  -0.109686       5.071315E-03   6.004449E-03   2.947896E-02   3.822715E-02
 ref    3   3.774533E-02   3.165596E-03  -0.932389       4.710830E-03   0.167638       9.718471E-02  -6.319337E-03  -6.311236E-02
 ref    4  -8.307465E-04   0.103154       4.889623E-03   0.947174       4.431118E-03  -4.356984E-03   4.094558E-02  -1.114046E-02

              v      9       v     10       v     11       v     12
 ref    1  -4.599790E-02   8.105678E-02   0.191745      -8.770948E-02
 ref    2  -4.423269E-02  -3.106327E-02   5.191559E-02   6.051266E-03
 ref    3  -2.870463E-02   3.843006E-02   3.339636E-02   1.421707E-02
 ref    4   1.873428E-02   6.330507E-02  -3.740024E-02  -2.034004E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904357       0.926060       0.873790       0.909193       4.750550E-02   9.538885E-03   3.691304E-03   6.627101E-03

              v      9       v     10       v     11       v     12
 ref    1   5.247267E-03   1.301953E-02   4.197565E-02   8.345413E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022688    -0.00067655     0.06638646     0.00081337     0.13913128    -0.00624346    -0.03325387    -0.03253469
 ref:   2     0.00033127     0.95676980     0.00319602    -0.10968602     0.00507131     0.00600445     0.02947896     0.03822715
 ref:   3     0.03774533     0.00316560    -0.93238879     0.00471083     0.16763839     0.09718471    -0.00631934    -0.06311236
 ref:   4    -0.00083075     0.10315408     0.00488962     0.94717403     0.00443112    -0.00435698     0.04094558    -0.01114046

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.04599790     0.08105678     0.19174547    -0.08770948
 ref:   2    -0.04423269    -0.03106327     0.05191559     0.00605127
 ref:   3    -0.02870463     0.03843006     0.03339636     0.01421707
 ref:   4     0.01873428     0.06330507    -0.03740024    -0.02034004

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -711.6262403942  2.5994E-10  0.0000E+00  3.8735E-04  1.0000E-03   
 mr-sdci # 31  2   -711.4954318632  9.6594E-10  0.0000E+00  6.2749E-04  1.0000E-03   
 mr-sdci # 31  3   -711.4774026773  2.5032E-08  0.0000E+00  4.2696E-04  1.0000E-03   
 mr-sdci # 31  4   -711.4627879795  7.1887E-05  2.0018E-05  4.5648E-03  1.0000E-03   
 mr-sdci # 31  5   -711.4197091111  5.8074E-05  0.0000E+00  1.0474E-01  1.0000E-04   
 mr-sdci # 31  6   -711.3111072902  3.3049E-04  0.0000E+00  1.4915E-01  1.0000E-04   
 mr-sdci # 31  7   -711.2758233588  1.4267E-02  0.0000E+00  1.6684E-01  1.0000E-04   
 mr-sdci # 31  8   -711.0399127568  3.2802E-02  0.0000E+00  3.4151E-01  1.0000E-04   
 mr-sdci # 31  9   -710.9890376939  6.7461E-02  0.0000E+00  3.5723E-01  1.0000E-04   
 mr-sdci # 31 10   -710.6081624859  1.9374E-01  0.0000E+00  6.2794E-01  1.0000E-04   
 mr-sdci # 31 11   -710.2949018992  5.3351E-02  0.0000E+00  6.5054E-01  1.0000E-04   
 mr-sdci # 31 12   -710.1216586195 -3.9192E-01  0.0000E+00  7.9872E-01  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001652
time for cinew                         0.068443
time for eigenvalue solver             0.000000
time for vector access                 0.000004

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955054
   ht   2    -0.00000000  -242.74874201
   ht   3    -0.00000000     0.00000000  -242.73071277
   ht   4    -0.00000000     0.00000000     0.00000000  -242.71583647
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.67253491
   ht   6    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000  -242.56391710
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000  -242.50157410
   ht   8     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.25615136
   ht   9     0.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    24.87934782    -0.16885485     3.98503333    -0.40581901    -3.80449213    -0.95010985     1.91264760    -0.69522807
   ht  11   385.82977780    -1.19421184    87.60758565    -0.61828947   -89.06874459   -13.66349712    39.58670091    -6.54291808
   ht  12     0.11439348    -0.20603961     1.06378394     0.35876010    -1.02113768    -0.03592811    -0.18965698     0.32721198
   ht  13    -0.35565671    -0.13153743     0.41219507     0.31787786    -0.43627028    -0.02664245     0.04985990     0.10824019

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9  -242.10608604
   ht  10    -3.70074706    -3.10270385
   ht  11   -58.38282031   -48.31246913  -781.66371374
   ht  12    -0.66958598    -0.05934795    -1.31699355    -0.03790702
   ht  13    -0.01196676     0.02419376     0.16502181    -0.00508455    -0.00965042

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.950225      -6.734166E-04  -6.638051E-02   1.139108E-03   0.139363      -7.053499E-03  -3.113948E-02   1.804861E-02
 ref    2  -3.316816E-04   0.956769      -3.199730E-03  -0.109712       5.160135E-03   6.459145E-03   2.495796E-02  -4.539423E-02
 ref    3  -3.774493E-02   3.167925E-03   0.932393       4.964713E-03   0.167549       9.668934E-02  -1.086523E-02   2.959176E-02
 ref    4   8.333616E-04   0.103163      -4.860625E-03   0.947347       3.586509E-03  -3.253508E-03   4.065326E-02   4.820924E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1  -5.472573E-02   2.781190E-02   0.208784       9.407865E-02   2.398670E-02
 ref    2  -2.111348E-02  -4.574985E-02   4.677652E-02  -1.240302E-02  -2.139201E-02
 ref    3  -5.738936E-02   4.489798E-02   3.592730E-02  -1.381089E-02   3.710031E-03
 ref    4   8.335131E-03   4.804134E-02  -2.221664E-02   3.633084E-02   5.907071E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904353       0.926059       0.873798       0.909530       4.753398E-02   9.450887E-03   3.363308E-03   3.285302E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   6.803698E-03   7.190349E-03   4.756321E-02   1.051530E-02   4.536093E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95022507    -0.00067342    -0.06638051     0.00113911     0.13936250    -0.00705350    -0.03113948     0.01804861
 ref:   2    -0.00033168     0.95676865    -0.00319973    -0.10971171     0.00516013     0.00645915     0.02495796    -0.04539423
 ref:   3    -0.03774493     0.00316793     0.93239340     0.00496471     0.16754874     0.09668934    -0.01086523     0.02959176
 ref:   4     0.00083336     0.10316329    -0.00486062     0.94734741     0.00358651    -0.00325351     0.04065326     0.00482092

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.05472573     0.02781190     0.20878413     0.09407865     0.02398670
 ref:   2    -0.02111348    -0.04574985     0.04677652    -0.01240302    -0.02139201
 ref:   3    -0.05738936     0.04489798     0.03592730    -0.01381089     0.00371003
 ref:   4     0.00833513     0.04804134    -0.02221664     0.03633084     0.05907071

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -711.6262403965  2.2680E-09  0.0000E+00  3.8190E-04  1.0000E-03   
 mr-sdci # 32  2   -711.4954318665  3.3441E-09  0.0000E+00  6.2632E-04  1.0000E-03   
 mr-sdci # 32  3   -711.4774026860  8.6948E-09  0.0000E+00  4.1160E-04  1.0000E-03   
 mr-sdci # 32  4   -711.4628055407  1.7561E-05  4.5248E-06  2.2654E-03  1.0000E-03   
 mr-sdci # 32  5   -711.4200431509  3.3404E-04  0.0000E+00  1.0436E-01  1.0000E-04   
 mr-sdci # 32  6   -711.3111865273  7.9237E-05  0.0000E+00  1.4955E-01  1.0000E-04   
 mr-sdci # 32  7   -711.2854579556  9.6346E-03  0.0000E+00  1.5497E-01  1.0000E-04   
 mr-sdci # 32  8   -711.1060266333  6.6114E-02  0.0000E+00  3.0255E-01  1.0000E-04   
 mr-sdci # 32  9   -711.0024653299  1.3428E-02  0.0000E+00  3.2687E-01  1.0000E-04   
 mr-sdci # 32 10   -710.7932688334  1.8511E-01  0.0000E+00  4.9093E-01  1.0000E-04   
 mr-sdci # 32 11   -710.3030658751  8.1640E-03  0.0000E+00  6.4514E-01  1.0000E-04   
 mr-sdci # 32 12   -710.1247173322  3.0587E-03  0.0000E+00  7.9817E-01  1.0000E-04   
 mr-sdci # 32 13   -710.0703157502 -3.3280E-01  0.0000E+00  7.5962E-01  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003490
time for cinew                         0.068665
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955054
   ht   2    -0.00000000  -242.74874201
   ht   3    -0.00000000     0.00000000  -242.73071277
   ht   4    -0.00000000     0.00000000     0.00000000  -242.71583647
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.67253491
   ht   6    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000  -242.56391710
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000  -242.50157410
   ht   8     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.25615136
   ht   9     0.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    24.87934782    -0.16885485     3.98503333    -0.40581901    -3.80449213    -0.95010985     1.91264760    -0.69522807
   ht  11   385.82977780    -1.19421184    87.60758565    -0.61828947   -89.06874459   -13.66349712    39.58670091    -6.54291808
   ht  12     0.11439348    -0.20603961     1.06378394     0.35876010    -1.02113768    -0.03592811    -0.18965698     0.32721198
   ht  13    -0.35565671    -0.13153743     0.41219507     0.31787786    -0.43627028    -0.02664245     0.04985990     0.10824019
   ht  14     0.15074687     0.18106009    -0.13108225     0.07763397     0.08541771    -0.00656302     0.02665700    -0.02360925

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9  -242.10608604
   ht  10    -3.70074706    -3.10270385
   ht  11   -58.38282031   -48.31246913  -781.66371374
   ht  12    -0.66958598    -0.05934795    -1.31699355    -0.03790702
   ht  13    -0.01196676     0.02419376     0.16502181    -0.00508455    -0.00965042
   ht  14     0.00949302    -0.01021647    -0.16184204     0.00217159     0.00081088    -0.00182355

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950225      -6.734109E-04   6.638031E-02   1.268929E-03  -0.138730       8.364323E-03   3.283329E-02  -1.416929E-02
 ref    2   3.315783E-04   0.956769       3.199646E-03  -0.109638      -6.170603E-03  -7.359822E-03  -2.487954E-02   3.244362E-02
 ref    3   3.774472E-02   3.167943E-03  -0.932394       5.248441E-03  -0.168502      -9.638299E-02   1.217320E-02  -2.709027E-02
 ref    4  -8.335328E-04   0.103163       4.859795E-03   0.947436      -3.587979E-03   1.843180E-03  -4.097255E-02   1.469028E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   5.596571E-02  -2.023801E-02  -0.142851      -0.171639       5.403160E-02  -3.079670E-02
 ref    2   1.673905E-02   6.286129E-02  -3.795374E-02  -2.127776E-02  -1.887057E-02   1.835940E-02
 ref    3   5.904685E-02  -3.162614E-02  -5.854102E-02   1.066874E-02  -5.657842E-03  -8.322860E-04
 ref    4  -7.144642E-03  -3.734398E-02  -1.615909E-02   3.966042E-02   5.997880E-02  -4.503578E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904353       0.926059       0.873799       0.909684       4.768976E-02   9.417207E-03   3.523952E-03   1.989398E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   6.949933E-03   6.755905E-03   2.553495E-02   3.159961E-02   6.904980E-03   3.314418E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022506    -0.00067341     0.06638031     0.00126893    -0.13873013     0.00836432     0.03283329    -0.01416929
 ref:   2     0.00033158     0.95676866     0.00319965    -0.10963824    -0.00617060    -0.00735982    -0.02487954     0.03244362
 ref:   3     0.03774472     0.00316794    -0.93239392     0.00524844    -0.16850152    -0.09638299     0.01217320    -0.02709027
 ref:   4    -0.00083353     0.10316331     0.00485979     0.94743589    -0.00358798     0.00184318    -0.04097255     0.00146903

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.05596571    -0.02023801    -0.14285060    -0.17163942     0.05403160    -0.03079670
 ref:   2     0.01673905     0.06286129    -0.03795374    -0.02127776    -0.01887057     0.01835940
 ref:   3     0.05904685    -0.03162614    -0.05854102     0.01066874    -0.00565784    -0.00083229
 ref:   4    -0.00714464    -0.03734398    -0.01615909     0.03966042     0.05997880    -0.04503578

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -711.6262403965  1.5660E-11  0.0000E+00  3.8194E-04  1.0000E-03   
 mr-sdci # 33  2   -711.4954318665  2.8422E-14  0.0000E+00  6.2633E-04  1.0000E-03   
 mr-sdci # 33  3   -711.4774026860  2.1970E-11  0.0000E+00  4.1159E-04  1.0000E-03   
 mr-sdci # 33  4   -711.4628099241  4.3834E-06  1.3164E-06  1.1515E-03  1.0000E-03   
 mr-sdci # 33  5   -711.4206745948  6.3144E-04  0.0000E+00  1.0123E-01  1.0000E-04   
 mr-sdci # 33  6   -711.3112434442  5.6917E-05  0.0000E+00  1.4967E-01  1.0000E-04   
 mr-sdci # 33  7   -711.2944093979  8.9514E-03  0.0000E+00  1.4437E-01  1.0000E-04   
 mr-sdci # 33  8   -711.1538463267  4.7820E-02  0.0000E+00  2.7534E-01  1.0000E-04   
 mr-sdci # 33  9   -711.0030254961  5.6017E-04  0.0000E+00  3.2648E-01  1.0000E-04   
 mr-sdci # 33 10   -710.8766596391  8.3391E-02  0.0000E+00  4.2608E-01  1.0000E-04   
 mr-sdci # 33 11   -710.3924797174  8.9414E-02  0.0000E+00  6.9476E-01  1.0000E-04   
 mr-sdci # 33 12   -710.1959058303  7.1188E-02  0.0000E+00  6.7964E-01  1.0000E-04   
 mr-sdci # 33 13   -710.1129496306  4.2634E-02  0.0000E+00  7.8524E-01  1.0000E-04   
 mr-sdci # 33 14   -710.0663467580 -1.9411E-01  0.0000E+00  7.6520E-01  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002975
time for cinew                         0.068008
time for eigenvalue solver             0.000000
time for vector access                 0.000004

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      660550 2x:       39515 4x:        1785
All internal counts: zz :      413563 yy:     1246697 xx:           0 ww:           0
One-external counts: yz :      362335 yx:           0 yw:           0
Two-external counts: yy :       88770 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:      102888    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -242.87955054
   ht   2    -0.00000000  -242.74874201
   ht   3    -0.00000000     0.00000000  -242.73071277
   ht   4    -0.00000000     0.00000000     0.00000000  -242.71583647
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -242.67253491
   ht   6    -0.00000000     0.00000000     0.00000000     0.00000000     0.00000000  -242.56391710
   ht   7     0.00000000    -0.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000  -242.50157410
   ht   8     0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000  -242.25615136
   ht   9     0.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000
   ht  10    24.87934782    -0.16885485     3.98503333    -0.40581901    -3.80449213    -0.95010985     1.91264760    -0.69522807
   ht  11   385.82977780    -1.19421184    87.60758565    -0.61828947   -89.06874459   -13.66349712    39.58670091    -6.54291808
   ht  12     0.11439348    -0.20603961     1.06378394     0.35876010    -1.02113768    -0.03592811    -0.18965698     0.32721198
   ht  13    -0.35565671    -0.13153743     0.41219507     0.31787786    -0.43627028    -0.02664245     0.04985990     0.10824019
   ht  14     0.15074687     0.18106009    -0.13108225     0.07763397     0.08541771    -0.00656302     0.02665700    -0.02360925
   ht  15    -0.01121644    -0.16678826     0.07588570    -0.07865815    -0.01424096    -0.01526921     0.02163545     0.01764670

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9  -242.10608604
   ht  10    -3.70074706    -3.10270385
   ht  11   -58.38282031   -48.31246913  -781.66371374
   ht  12    -0.66958598    -0.05934795    -1.31699355    -0.03790702
   ht  13    -0.01196676     0.02419376     0.16502181    -0.00508455    -0.00965042
   ht  14     0.00949302    -0.01021647    -0.16184204     0.00217159     0.00081088    -0.00182355
   ht  15    -0.00979771    -0.00133992    -0.03252292    -0.00003539    -0.00063532     0.00022927    -0.00063671

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.950225      -6.710178E-04  -6.637925E-02  -1.328629E-03  -0.137932      -2.185329E-02   2.970921E-02   9.669572E-03
 ref    2   3.342065E-04   0.956761      -3.202279E-03   0.109722      -4.527614E-03   7.760350E-03  -1.050188E-02  -4.589165E-02
 ref    3   3.774447E-02   3.172808E-03   0.932396      -5.345404E-03  -0.168420       8.131572E-02   5.087189E-02   6.330517E-03
 ref    4  -8.314280E-04   0.103160      -4.859362E-03  -0.947366      -1.610472E-03   5.236294E-03  -2.921550E-02  -2.842204E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   5.681095E-02   7.279259E-03  -5.502952E-02  -0.171507      -0.146094      -1.440923E-02  -1.164517E-02
 ref    2   6.163685E-03   4.194297E-02   5.621310E-02  -7.712562E-02   2.572860E-02   4.767315E-03   2.737127E-02
 ref    3   7.132209E-02  -1.010463E-02  -3.766925E-02  -4.360266E-02   1.950672E-02   1.134751E-04   1.891255E-02
 ref    4   1.702810E-02  -4.980261E-02  -2.202732E-03  -1.772474E-03   2.731015E-03  -5.236587E-02   9.277839E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.904353       0.926043       0.873802       0.909572       4.741368E-02   7.177454E-03   4.434421E-03   3.047432E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   8.642272E-03   4.394604E-03   7.611986E-03   3.726729E-02   2.239343E-02   2.972551E-03   9.850310E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -711.6262403980  1.5382E-09  0.0000E+00  3.7862E-04  1.0000E-03   
 mr-sdci # 34  2   -711.4954318784  1.1947E-08  0.0000E+00  6.1881E-04  1.0000E-03   
 mr-sdci # 34  3   -711.4774026870  9.8404E-10  0.0000E+00  4.1394E-04  1.0000E-03   
 mr-sdci # 34  4   -711.4628112614  1.3373E-06  3.9633E-07  6.4015E-04  1.0000E-03   
 mr-sdci # 34  5   -711.4212189689  5.4437E-04  0.0000E+00  9.8502E-02  1.0000E-04   
 mr-sdci # 34  6   -711.3138764186  2.6330E-03  0.0000E+00  1.4792E-01  1.0000E-04   
 mr-sdci # 34  7   -711.3033619073  8.9525E-03  0.0000E+00  1.4713E-01  1.0000E-04   
 mr-sdci # 34  8   -711.1941332880  4.0287E-02  0.0000E+00  2.4361E-01  1.0000E-04   
 mr-sdci # 34  9   -711.0204481732  1.7423E-02  0.0000E+00  3.0794E-01  1.0000E-04   
 mr-sdci # 34 10   -710.9496465657  7.2987E-02  0.0000E+00  3.8541E-01  1.0000E-04   
 mr-sdci # 34 11   -710.5509878218  1.5851E-01  0.0000E+00  6.0388E-01  1.0000E-04   
 mr-sdci # 34 12   -710.3284919185  1.3259E-01  0.0000E+00  6.4336E-01  1.0000E-04   
 mr-sdci # 34 13   -710.1436250955  3.0675E-02  0.0000E+00  7.5035E-01  1.0000E-04   
 mr-sdci # 34 14   -710.1082628031  4.1916E-02  0.0000E+00  7.7934E-01  1.0000E-04   
 mr-sdci # 34 15   -709.9513291173 -2.0015E-01  0.0000E+00  7.2131E-01  1.0000E-04   
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.003872
time for cinew                         0.065571
time for eigenvalue solver             0.000233
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after 34 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -711.6262403980  1.5382E-09  0.0000E+00  3.7862E-04  1.0000E-03   
 mr-sdci # 34  2   -711.4954318784  1.1947E-08  0.0000E+00  6.1881E-04  1.0000E-03   
 mr-sdci # 34  3   -711.4774026870  9.8404E-10  0.0000E+00  4.1394E-04  1.0000E-03   
 mr-sdci # 34  4   -711.4628112614  1.3373E-06  3.9633E-07  6.4015E-04  1.0000E-03   
 mr-sdci # 34  5   -711.4212189689  5.4437E-04  0.0000E+00  9.8502E-02  1.0000E-04   
 mr-sdci # 34  6   -711.3138764186  2.6330E-03  0.0000E+00  1.4792E-01  1.0000E-04   
 mr-sdci # 34  7   -711.3033619073  8.9525E-03  0.0000E+00  1.4713E-01  1.0000E-04   
 mr-sdci # 34  8   -711.1941332880  4.0287E-02  0.0000E+00  2.4361E-01  1.0000E-04   
 mr-sdci # 34  9   -711.0204481732  1.7423E-02  0.0000E+00  3.0794E-01  1.0000E-04   
 mr-sdci # 34 10   -710.9496465657  7.2987E-02  0.0000E+00  3.8541E-01  1.0000E-04   
 mr-sdci # 34 11   -710.5509878218  1.5851E-01  0.0000E+00  6.0388E-01  1.0000E-04   
 mr-sdci # 34 12   -710.3284919185  1.3259E-01  0.0000E+00  6.4336E-01  1.0000E-04   
 mr-sdci # 34 13   -710.1436250955  3.0675E-02  0.0000E+00  7.5035E-01  1.0000E-04   
 mr-sdci # 34 14   -710.1082628031  4.1916E-02  0.0000E+00  7.7934E-01  1.0000E-04   
 mr-sdci # 34 15   -709.9513291173 -2.0015E-01  0.0000E+00  7.2131E-01  1.0000E-04   

####################CIUDGINFO####################

   ci vector at position   1 energy= -711.626240398026
   ci vector at position   2 energy= -711.495431878448
   ci vector at position   3 energy= -711.477402687001
   ci vector at position   4 energy= -711.462811261398

################END OF CIUDGINFO################

 
    4 of the  16 expansion vectors are transformed.
    4 of the  15 matrix-vector products are transformed.

    4 expansion eigenvectors written to unit nvfile (= 11)
    4 matrix-vector products written to unit nhvfil (= 10)


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -711.6262403980

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18   19   20   21   22   23

                                          orbital    13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29
                                               30   31   32   33   34   35

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.888194                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 z*  1  1       7  0.298063                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-    -      
 z*  1  1       8  0.139636                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-         - 
 z*  1  1      15 -0.030679                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-   +-      
 z*  1  1      16 -0.011193                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-   +     - 
 z*  1  1      17 -0.055386                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-        +- 
 z*  1  1      21 -0.025522                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-    -      
 z*  1  1      22  0.016262                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 z*  1  1      29 -0.019050                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-   +-      
 z*  1  1      30 -0.061736                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-   +     - 
 z*  1  1      31  0.058908                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-        +- 
 z*  1  1      41 -0.043598                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-   +-      
 z*  1  1      42  0.013294                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-   +     - 
 z*  1  1      43 -0.076328                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-        +- 
 z*  1  1      47  0.010909                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-   +-    - 
 z*  1  1      48 -0.031130                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-    -   +- 
 z   1  1      51  0.035693                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +-    -      
 z   1  1      52 -0.028418                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +-         - 
 z   1  1      81 -0.046463                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-   +-    -    - 
 z   1  1      85 -0.011185                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +     -   +-    -   +- 
 z   1  1      91  0.012764                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +-    -      
 z   1  1      92 -0.015579                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +-         - 
 z   1  1      99  0.013044                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-   +-      
 z   1  1     100 -0.012701                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-   +     - 
 z   1  1     101 -0.015340                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-        +- 
 z   1  1     105  0.015116                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-    -    - 
 z   1  1     112 -0.039335                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +-   +     - 
 z   1  1     121 -0.033082                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +    +-   +-    -    - 
 z   1  1     171  0.014439                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +- 
                                             +-   +-   +-   +-    -      
 y   1  1     772  0.015120              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     775 -0.030202              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     776  0.015946              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     777 -0.017469              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     778 -0.040093              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     779 -0.015723              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     780  0.022283             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     784 -0.016099             14( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     785 -0.033200             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     788 -0.039726             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     924 -0.039959              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-           
 y   1  1     939  0.038790             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-           
 y   1  1    1234  0.010256              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -    -      
 y   1  1    1247  0.010868             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -    -      
 y   1  1    1298  0.012545             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -         - 
 y   1  1    1398 -0.013894             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-    -      
 y   1  1    1449 -0.011125             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-         - 
 y   1  1    2820 -0.013370             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -    -      
 y   1  1    2828  0.010168             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -    -      
 y   1  1    2964  0.034227              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-    -      
 y   1  1    3576  0.011975              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-    -      
 y   1  1    4596 -0.018315              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                              -   +-   +-   +-           
 y   1  1    7299 -0.035916              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-    -      
 y   1  1    7350  0.067199              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-         - 
 y   1  1    7911  0.025178              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +-    -      
 y   1  1    7962 -0.065675              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +-         - 
 y   1  1    8370 -0.012256              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -    -   +-   +     - 
 y   1  1    8625 -0.027133              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +    +-    -    - 
 y   1  1   12177 -0.013603             34( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-    -      
 y   1  1   12807  0.025034              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +-         - 
 y   1  1   12825  0.011681             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +-         - 
 y   1  1   14287 -0.011486              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +-           
 y   1  1   14288  0.011502              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +-           
 y   1  1   14294  0.011985              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +-           
 y   1  1   15315  0.013953             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +-    -      
 y   1  1   15316  0.012154             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +-    -      
 y   1  1   15365 -0.012739              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +-         - 
 y   1  1   15367  0.013666             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +-         - 
 y   1  1   19131  0.010604              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23977  0.036524              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23978 -0.040855              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23979 -0.025908              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23980  0.016210              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23989 -0.017018             14( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23990  0.015057             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23992  0.034806             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23993 -0.010088             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   24283 -0.014873              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +-    -      
 y   1  1   24284  0.015580              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +-    -      
 y   1  1   24285  0.010147              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +-    -      
 y   1  1   24298 -0.013284             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +-    -      
 y   1  1   24334 -0.012511              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +-         - 
 y   1  1   24335  0.016845              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +-         - 
 y   1  1   24336  0.010864              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +-         - 
 y   1  1   24997 -0.016963              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   24998  0.018717              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   24999  0.013893              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   25012 -0.011001             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   25016 -0.012583             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   25048 -0.012699              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   25049  0.010144              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   25056  0.014321             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   25057  0.012713             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   25063 -0.010259             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   28826 -0.012578              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   29900  0.012984              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   29903  0.010285             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   29916  0.010343             25( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   33667  0.019182              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   33668 -0.015333              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   34688  0.011451              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   38518 -0.010506              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   39538 -0.013620              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   49228 -0.015450              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   49280 -0.013039              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   49296 -0.011035             25( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   53047  0.019494              2( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   57892  0.011629              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   57902  0.014741             12( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   58972  0.011549             11( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 y   1  1   67583 -0.014179              3( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   67585  0.011724              5( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   67592  0.016628             12( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   67593 -0.010183             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   67597  0.010023             17( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   78350 -0.011231              9( a  )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             112
     0.01> rq > 0.001           3564
    0.001> rq > 0.0001         14471
   0.0001> rq > 0.00001        23422
  0.00001> rq > 0.000001       28798
 0.000001> rq                  21435
           all                 91805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      203100 2x:           0 4x:           0
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:        4545    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.888193537474   -631.957382017114
     2     2      0.002417221952     -1.719981228005
     3     3      0.000313268638     -0.222995638315
     4     4     -0.006362356561      4.526040287668
     5     5     -0.005601380081      3.985478569665
     6     6     -0.003355436152      2.386940539426
     7     7      0.298063418301   -212.094518114534
     8     8      0.139636402037    -99.370879201180
     9     9     -0.000222512786      0.158380884255
    10    10      0.000264089466     -0.187862712828
    11    11     -0.000373296625      0.265594083232
    12    12     -0.000366435671      0.260794034671
    13    13      0.000232233111     -0.165289998646
    14    14      0.000887362990     -0.631140471137
    15    15     -0.030679385480     21.831708901370
    16    16     -0.011192501071      7.970623601350
    17    17     -0.055386067771     39.411013535693
    18    18     -0.000052833597      0.037589753965
    19    19     -0.000053959602      0.038387991558
    20    20      0.000319554420     -0.227284080118
    21    21     -0.025522389384     18.155130931710
    22    22      0.016261518001    -11.564973916955
    23    23      0.000088074717     -0.062696065439
    24    24     -0.000510888155      0.363512189859
    25    25      0.000596220595     -0.424184182298
    26    26      0.000241138176     -0.171611534577
    27    27     -0.000038745039      0.027507678412
    28    28     -0.000844007674      0.600297340119
    29    29     -0.019049886876     13.557442771116
    30    30     -0.061736111497     43.935001429110
    31    31      0.058908238176    -41.909243230825
    32    32      0.000050782551     -0.036145793394
    33    33      0.000098353781     -0.069998523637
    34    34     -0.000582136183      0.414170550097
    35    35      0.002060663569     -1.466291498433
    36    36     -0.000067325363      0.047907566771
    37    37      0.000114418195     -0.081419530022
    38    38     -0.009324758879      6.635514129254
    39    39      0.006181031079     -4.398539865182
    40    40      0.000036477421     -0.025958381812
    41    41     -0.043598148197     31.030143509683
    42    42      0.013294225608     -9.453387701032
    43    43     -0.076327560886     54.309086663110
    44    44      0.000033032501     -0.023497103010
    45    45     -0.000262709850      0.186958387656
    46    46      0.000851467532     -0.605730246807
    47    47      0.010909130099     -7.762337387556
    48    48     -0.031130034123     22.152350014916
    49    49      0.000043951107     -0.031283973361
    50    50      0.007832526958     -5.574396183494

 number of reference csfs (nref) is    50.  root number (iroot) is  1.
 c0**2 =   0.91919749  c**2 (all zwalks) =   0.92841175

 pople ci energy extrapolation is computed with 42 correlated electrons.

 eref      =   -711.519063125540   "relaxed" cnot**2         =   0.919197486139
 eci       =   -711.626240398026   deltae = eci - eref       =  -0.107177272486
 eci+dv1   =   -711.634900591072   dv1 = (1-cnot**2)*deltae  =  -0.008660193046
 eci+dv2   =   -711.635661869664   dv2 = dv1 / cnot**2       =  -0.009421471638
 eci+dv3   =   -711.636569888698   dv3 = dv1 / (2*cnot**2-1) =  -0.010329490672
 eci+pople =   -711.635984503066   ( 42e- scaled deltae )    =  -0.116921377526


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -711.4954318784

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18   19   20   21   22   23

                                          orbital    13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29
                                               30   31   32   33   34   35

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2 -0.935410                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +     -      
 z*  1  1       3 -0.127512                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +          - 
 z*  1  1       9  0.032316                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -   +-      
 z*  1  1      10 -0.018446                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -   +     - 
 z*  1  1      12  0.060919                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +     -    - 
 z*  1  1      19  0.032823                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +     -   +- 
 z*  1  1      23  0.029725                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +-      
 z*  1  1      24  0.120869                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +     - 
 z*  1  1      26 -0.042527                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 z*  1  1      33 -0.065292                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +     -   +- 
 z*  1  1      37  0.010080                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +     -    -   +- 
 z*  1  1      45  0.096230                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +     -   +- 
 z   1  1      53  0.029728                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -   +-      
 z   1  1      56  0.021342                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +     -    - 
 z   1  1      66 -0.011356                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +     -   +-    - 
 z   1  1      82  0.033479                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-    -   +-    - 
 z   1  1      94  0.025518                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -   +     - 
 z   1  1      95  0.037820                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -        +- 
 z   1  1     106 -0.010429                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +     -   +-    - 
 z   1  1     114  0.021925                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +    +-    - 
 z   1  1     122  0.024648                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +    +-    -   +-    - 
 z   1  1     131  0.010704                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +- 
                                             +-   +-   +-   +-    -      
 y   1  1     771  0.020489              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     786  0.011321             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     836 -0.013961             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-         -      
 y   1  1     839 -0.015152             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-         -      
 y   1  1    1281  0.019834              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -         - 
 y   1  1    2811 -0.042315              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -    -      
 y   1  1    2833 -0.010362             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -    -      
 y   1  1    2862 -0.045759              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -         - 
 y   1  1    2880 -0.010163             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -         - 
 y   1  1    3678 -0.010125              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-    -   +-      
 y   1  1    7146  0.100926              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -    -      
 y   1  1    7197 -0.040328              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -         - 
 y   1  1    7452 -0.021518              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -    -   +     - 
 y   1  1    7554 -0.047046              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +     -    - 
 y   1  1    8013 -0.011847              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-    -   +-      
 y   1  1    8064  0.040938              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-    -   +     - 
 y   1  1    8166  0.076258              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +     -    - 
 y   1  1    9084  0.011594              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +     -    -    -   +- 
 y   1  1    9237 -0.019065              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +         +-    -    -   +- 
 y   1  1   12009 -0.014147             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -    -      
 y   1  1   12014  0.013818             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -    -      
 y   1  1   12023  0.011525             33( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -    -      
 y   1  1   12024  0.015962             34( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -    -      
 y   1  1   12064 -0.011755             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -         - 
 y   1  1   12909 -0.012637              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-    -   +     - 
 y   1  1   13011 -0.020711              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +     -    - 
 y   1  1   13029 -0.011081             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +     -    - 
 y   1  1   15569  0.011887              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   15571 -0.012905             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   24029 -0.010583              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +     -      
 y   1  1   24539 -0.010517              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +     -    - 
 y   1  1   25260 -0.013201             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   25261 -0.011483             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   26542  0.010818             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   30104 -0.011895              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   30120 -0.010159             25( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   38620  0.010444              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +          - 
 y   1  1   39640  0.010097              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +-      
 y   1  1   48310  0.011702              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +          - 
 y   1  1   49330  0.011276              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +-      
 y   1  1   49484  0.012836              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   49500  0.010189             25( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   60442  0.010862              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   60451 -0.010497             11( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   70132  0.017082              2( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   70133 -0.021664              3( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   70134 -0.011281              4( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   70135  0.010342              5( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   74977  0.011730              2( a  )   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   74978 -0.014616              3( a  )   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   74979 -0.010461              4( a  )   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      
 y   1  1   78554  0.011287              9( a  )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   79823  0.010340              3( a  )   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -    -      

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              71
     0.01> rq > 0.001           3601
    0.001> rq > 0.0001         15854
   0.0001> rq > 0.00001        28149
  0.00001> rq > 0.000001       29223
 0.000001> rq                  14903
           all                 91805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      203100 2x:           0 4x:           0
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:        4545    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.002426026498     -1.725729256631
     2     2     -0.935409766985    665.455098302967
     3     3     -0.127511825069     90.718194020798
     4     4      0.000530581601     -0.377453622127
     5     5     -0.000927175728      0.659613008537
     6     6     -0.000036830360      0.026212157625
     7     7     -0.000761520921      0.541594152656
     8     8      0.003069535273     -2.183782104236
     9     9      0.032315906824    -22.996031390815
    10    10     -0.018445635411     13.116517693638
    11    11      0.009924357767     -7.059505470717
    12    12      0.060919331794    -43.347201641477
    13    13      0.000042862964     -0.030484324533
    14    14      0.000306479596     -0.218045350790
    15    15     -0.000186702897      0.132748061390
    16    16     -0.000531674323      0.378286998212
    17    17     -0.000585832582      0.416714003905
    18    18      0.002895459958     -2.059047327337
    19    19      0.032822873131    -23.348908751803
    20    20     -0.000008456540      0.006023584188
    21    21      0.000515714822     -0.366495501356
    22    22     -0.000056881367      0.040500249760
    23    23      0.029724512285    -21.150477869288
    24    24      0.120868753341    -85.996301900017
    25    25     -0.009555952568      6.790980145820
    26    26     -0.042527301334     30.253380403689
    27    27     -0.000077396552      0.055101320617
    28    28     -0.000452426568      0.321866948191
    29    29      0.000238379623     -0.169572519143
    30    30      0.000654961993     -0.465917755469
    31    31      0.000109589250     -0.077913102610
    32    32      0.009226943607     -6.564664454273
    33    33     -0.065291669254     46.451584105205
    34    34      0.000005554448     -0.003960123766
    35    35     -0.000591712868      0.420991653044
    36    36      0.005664167310     -4.029508711676
    37    37      0.010079889869     -7.173022585753
    38    38      0.000051155942     -0.036342616975
    39    39      0.000041193200     -0.029317672993
    40    40      0.001976289932     -1.406051588983
    41    41     -0.000134874551      0.095923637996
    42    42      0.000012922273     -0.009153601476
    43    43     -0.000047299322      0.033614400187
    44    44     -0.008079998366      5.744709312385
    45    45      0.096229787236    -68.463595225683
    46    46     -0.000057016577      0.040568955610
    47    47      0.000158413294     -0.112722831249
    48    48      0.000045659979     -0.032609637050
    49    49     -0.005533506991      3.937808500010
    50    50      0.000043859257     -0.031173565031

 number of reference csfs (nref) is    50.  root number (iroot) is  2.
 c0**2 =   0.92878431  c**2 (all zwalks) =   0.93578398

 pople ci energy extrapolation is computed with 42 correlated electrons.

 eref      =   -711.408345027412   "relaxed" cnot**2         =   0.928784310361
 eci       =   -711.495431878448   deltae = eci - eref       =  -0.087086851037
 eci+dv1   =   -711.501633828604   dv1 = (1-cnot**2)*deltae  =  -0.006201950155
 eci+dv2   =   -711.502109370829   dv2 = dv1 / cnot**2       =  -0.006677492380
 eci+dv3   =   -711.502663894645   dv3 = dv1 / (2*cnot**2-1) =  -0.007232016197
 eci+pople =   -711.502263364042   ( 42e- scaled deltae )    =  -0.093918336630


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -711.4774026870

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18   19   20   21   22   23

                                          orbital    13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29
                                               30   31   32   33   34   35

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.198586                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 z*  1  1       4  0.011708                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-        +-      
 z*  1  1       7 -0.794246                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-    -      
 z*  1  1       8  0.424948                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-         - 
 z*  1  1      15  0.054361                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-   +-      
 z*  1  1      16 -0.068930                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-   +     - 
 z*  1  1      17 -0.059196                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-        +- 
 z*  1  1      21  0.017413                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-    -      
 z*  1  1      22  0.050661                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-         - 
 z*  1  1      30  0.147216                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-   +     - 
 z*  1  1      35 -0.096344                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-    -    - 
 z*  1  1      39 -0.025478                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +         +-    -   +- 
 z*  1  1      41 -0.033848                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-   +-      
 z*  1  1      43  0.011935                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-        +- 
 z*  1  1      47  0.028664                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-   +-    - 
 z*  1  1      48  0.090590                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-    -   +- 
 z   1  1      52 -0.044569                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +-         - 
 z   1  1      59  0.050699                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-   +-      
 z   1  1      60  0.022390                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-   +     - 
 z   1  1      61 -0.033202                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-        +- 
 z   1  1      68 -0.011039                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-        +-   +-    - 
 z   1  1      69  0.017637                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-        +-    -   +- 
 z   1  1      72  0.022374                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-   +-   +     - 
 z   1  1      81 -0.020209                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-   +-    -    - 
 z   1  1      84  0.027421                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +     -   +-   +-    - 
 z   1  1      85 -0.023904                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +     -   +-    -   +- 
 z   1  1      91 -0.018899                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +-    -      
 z   1  1      92 -0.033331                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-   +-         - 
 z   1  1      99  0.017794                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-   +-      
 z   1  1     100 -0.012692                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-   +     - 
 z   1  1     101  0.028574                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-        +- 
 z   1  1     105 -0.017568                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +    +-    -    - 
 z   1  1     111  0.012788                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +-   +-      
 z   1  1     112 -0.022066                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +-   +     - 
 z   1  1     117  0.015596                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +    +-   +-    - 
 z   1  1     121 -0.016745                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +    +-   +-    -    - 
 z   1  1     124  0.015042                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +     -   +-   +-    - 
 z   1  1     171  0.019010                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +- 
                                             +-   +-   +-   +-    -      
 y   1  1     772  0.017663              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     775 -0.028154              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     776  0.012137              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     777 -0.011414              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     778 -0.024759              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     780  0.010666             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     785 -0.018767             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     788 -0.018627             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     939  0.020360             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-           
 y   1  1     946  0.011194             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-           
 y   1  1     961 -0.012031             38( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-           
 y   1  1    1434  0.025408              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-         - 
 y   1  1    1449 -0.011451             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +-         - 
 y   1  1    2825 -0.010707             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -    -      
 y   1  1    2979  0.011994             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-    -      
 y   1  1    3015 -0.056553              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +-         - 
 y   1  1    3576  0.015720              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +-    -      
 y   1  1    3984 -0.014533              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-         -   +-   +-      
 y   1  1    4086  0.010127              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-         -   +-        +- 
 y   1  1    4290 -0.016492              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +    +-    -    - 
 y   1  1    4596 -0.017930              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                              -   +-   +-   +-           
 y   1  1    7299  0.138998              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-    -      
 y   1  1    7368 -0.010020             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -   +-         - 
 y   1  1    7758 -0.021035              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-        +-    -    - 
 y   1  1    8370  0.037875              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -    -   +-   +     - 
 y   1  1    8421 -0.020830              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -    -   +-        +- 
 y   1  1    8625  0.066136              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +    +-    -    - 
 y   1  1    9339 -0.027219              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +          -   +-    -   +- 
 y   1  1   12177  0.010027             34( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-    -      
 y   1  1   12195  0.019877              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-         - 
 y   1  1   12217 -0.010274             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +-         - 
 y   1  1   13215 -0.015841              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -    -   +-   +     - 
 y   1  1   13470 -0.014645              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +    +-    -    - 
 y   1  1   13488 -0.012030             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +    +-    -    - 
 y   1  1   14294  0.014691              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +-           
 y   1  1   14295  0.010635             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +-           
 y   1  1   14653 -0.011254             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +    +-         - 
 y   1  1   15316  0.011808             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +-    -      
 y   1  1   16030 -0.018398             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +    +-    -    - 
 y   1  1   16041  0.010221             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +    +-    -    - 
 y   1  1   23977  0.022774              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23978 -0.021935              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23979 -0.013911              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   23992  0.019147             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   24692 -0.010920              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-        +-   +-      
 y   1  1   25711  0.012853              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +    +-    -    - 
 y   1  1   25712 -0.010848              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +    +-    -    - 
 y   1  1   25719 -0.014185             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +    +-    -    - 
 y   1  1   28826 -0.010248              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   30563 -0.011633              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +    +-    -    - 
 y   1  1   33667  0.012059              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   38875  0.010025              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-         - 
 y   1  1   39538 -0.010115              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   48565  0.010988              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-         - 
 y   1  1   49228 -0.010823              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +-    -      
 y   1  1   49943  0.010508              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +    +-    -    - 
 y   1  1   53047  0.014649              2( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-           
 y   1  1   70286 -0.014580              3( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +-    -      

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              91
     0.01> rq > 0.001           4487
    0.001> rq > 0.0001         17235
   0.0001> rq > 0.00001        25628
  0.00001> rq > 0.000001       28847
 0.000001> rq                  15512
           all                 91805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      203100 2x:           0 4x:           0
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:        4545    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.198585568592   -141.255294434896
     2     2      0.002627436004     -1.869197836865
     3     3      0.001754814057     -1.248390006793
     4     4      0.011707849344     -8.328317757871
     5     5      0.004905128768     -3.488861837938
     6     6     -0.005877807562      4.180691857355
     7     7     -0.794246411102    565.002906219077
     8     8      0.424947825197   -302.303359104658
     9     9      0.000349652915     -0.248698530930
    10    10     -0.000961596612      0.684121857183
    11    11     -0.000317856910      0.226080346381
    12    12     -0.000374899739      0.266799964056
    13    13     -0.002349763459      1.671309406030
    14    14     -0.003664127036      2.606057767116
    15    15      0.054361175634    -38.679149258255
    16    16     -0.068930054186     49.036913317534
    17    17     -0.059195642135     42.114455396748
    18    18     -0.000014908151      0.010586648007
    19    19     -0.000324637685      0.230917112776
    20    20     -0.000352888311      0.251091104629
    21    21      0.017412578462    -12.371028714721
    22    22      0.050660804690    -36.044318999850
    23    23      0.000066318020     -0.047178629149
    24    24     -0.000161316804      0.114769055849
    25    25     -0.000064339548      0.045789076228
    26    26      0.000031881596     -0.022663377900
    27    27     -0.000719488230      0.511717710206
    28    28      0.002333277559     -1.659659870670
    29    29      0.007238343480     -5.153750345578
    30    30      0.147215653654   -104.730371512678
    31    31     -0.008137431817      5.790121380239
    32    32     -0.000087788368      0.062482221750
    33    33      0.000565986958     -0.402670995305
    34    34      0.000945171509     -0.672461874836
    35    35     -0.096344402094     68.544074192290
    36    36     -0.000192555841      0.136989258112
    37    37     -0.000506706880      0.360490939368
    38    38     -0.001775935169      1.263277823370
    39    39     -0.025478468645     18.124084638703
    40    40     -0.000034216939      0.024333128774
    41    41     -0.033848234516     24.081966986537
    42    42     -0.002473231977      1.761191035831
    43    43      0.011935050778     -8.493839584693
    44    44      0.000147680118     -0.105059227581
    45    45     -0.000249427821      0.177448803351
    46    46     -0.000798307855      0.568000746053
    47    47      0.028664085546    -20.397895317009
    48    48      0.090590071845    -64.453882111362
    49    49     -0.000005125427      0.003671015334
    50    50     -0.003329391939      2.369443788059

 number of reference csfs (nref) is    50.  root number (iroot) is  3.
 c0**2 =   0.90721809  c**2 (all zwalks) =   0.92158049

 pople ci energy extrapolation is computed with 42 correlated electrons.

 eref      =   -711.375148724667   "relaxed" cnot**2         =   0.907218090346
 eci       =   -711.477402687001   deltae = eci - eref       =  -0.102253962334
 eci+dv1   =   -711.486890004896   dv1 = (1-cnot**2)*deltae  =  -0.009487317895
 eci+dv2   =   -711.487860280380   dv2 = dv1 / cnot**2       =  -0.010457593379
 eci+dv3   =   -711.489051626623   dv3 = dv1 / (2*cnot**2-1) =  -0.011648939622
 eci+pople =   -711.488371523792   ( 42e- scaled deltae )    =  -0.113222799125


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 4) =      -711.4628112614

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18   19   20   21   22   23

                                          orbital    13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29
                                               30   31   32   33   34   35

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.114669                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +     -      
 z*  1  1       3 -0.929474                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +          - 
 z*  1  1       9 -0.067534                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -   +-      
 z*  1  1      10  0.027799                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -   +     - 
 z*  1  1      11  0.096290                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -        +- 
 z*  1  1      12 -0.015215                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +     -    - 
 z*  1  1      18 -0.029723                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +    +-    - 
 z*  1  1      19 -0.014235                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +     -   +- 
 z*  1  1      23  0.110499                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +-      
 z*  1  1      24 -0.031493                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -   +     - 
 z*  1  1      25 -0.037204                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -        +- 
 z*  1  1      26  0.030681                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 z*  1  1      32  0.028049                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +     -   +    +-    - 
 z*  1  1      44 -0.064774                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +    +-    - 
 z*  1  1      45 -0.013389                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +     -   +- 
 z   1  1      53 -0.015442                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -   +-      
 z   1  1      54 -0.059482                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -   +     - 
 z   1  1      55  0.029055                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -        +- 
 z   1  1      56  0.035142                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-   +     -    - 
 z   1  1      67 -0.027089                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +     -    -   +- 
 z   1  1      83  0.047626                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +    +-    -    -   +- 
 z   1  1      93  0.017017                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -   +-      
 z   1  1      94  0.054892                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -   +     - 
 z   1  1      95 -0.013490                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -        +- 
 z   1  1     102  0.017606                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-    -   +    +-    - 
 z   1  1     115 -0.022910                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-   +     -   +- 
 z   1  1     123  0.012992                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +    +-    -    -   +- 
 z   1  1     153 -0.010185                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +- 
                                             +-    -   +-   +-        +- 
 z   1  1     332 -0.010042                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-         - 
 z   1  1     425 -0.010028                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +    +-    -    - 
 y   1  1     786  0.021554             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     793 -0.013703             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     794 -0.021486             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     797  0.018561             27( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     808  0.018722             38( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -           
 y   1  1     887 -0.014940             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-              - 
 y   1  1     890 -0.018031             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-              - 
 y   1  1     975 -0.010789              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +     -      
 y   1  1    1026  0.012831              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-    -   +          - 
 y   1  1    1230  0.038366              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -    -      
 y   1  1    1281 -0.018528              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +     -         - 
 y   1  1    1653 -0.014857             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-        +     -    - 
 y   1  1    2811 -0.071561              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -    -      
 y   1  1    2829 -0.011614             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -    -      
 y   1  1    2862  0.041463              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -         - 
 y   1  1    2884 -0.013064             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -         - 
 y   1  1    2885  0.011222             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -         - 
 y   1  1    3729 -0.017039              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-    -   +     - 
 y   1  1    3831  0.018761              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +     -    - 
 y   1  1    3849  0.011340             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-        +-   +     -    - 
 y   1  1    4698  0.019208              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                              -   +-   +-   +          - 
 y   1  1    4716  0.012104             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                              -   +-   +-   +          - 
 y   1  1    7146 -0.085423              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -    -      
 y   1  1    7197 -0.070097              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -         - 
 y   1  1    7215  0.013486             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-   +-    -         - 
 y   1  1    7401  0.010727              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -    -   +-      
 y   1  1    7503 -0.034660              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +    +-    -    -        +- 
 y   1  1    8013 -0.019799              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-    -   +-      
 y   1  1    8064 -0.017856              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-    -   +     - 
 y   1  1    8115  0.047042              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +     -   +-    -        +- 
 y   1  1    9186 -0.013756              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +         +-    -   +-    - 
 y   1  1    9237  0.016322              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +         +-    -    -   +- 
 y   1  1   11991 -0.011270              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -    -      
 y   1  1   12042 -0.034977              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-   +-   +-    -         - 
 y   1  1   12960 -0.016955              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +  
                                             +-    -   +-    -        +- 
 y   1  1   14389 -0.010356              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +          - 
 y   1  1   14398  0.013341             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +          - 
 y   1  1   14399 -0.010636             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +-   +-   +          - 
 y   1  1   15518  0.011036              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-    -        +- 
 y   1  1   15520 -0.016056             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-    -        +- 
 y   1  1   15570  0.011542             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   15571  0.015792             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   24080 -0.010503              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +-   +          - 
 y   1  1   24538 -0.013818              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +     -    - 
 y   1  1   24539  0.016722              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +     -    - 
 y   1  1   24540  0.010644              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +     -    - 
 y   1  1   24553 -0.015325             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +-   +    +     -    - 
 y   1  1   25209 -0.013897             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-    -        +- 
 y   1  1   25210 -0.010567             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-    -        +- 
 y   1  1   25252 -0.012944              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   25253  0.013024              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   25254  0.010211              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   30053 -0.012091              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +- 
                                             +-   +    +-    -        +- 
 y   1  1   38569  0.012123              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +     -      
 y   1  1   38620 -0.010381              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +          - 
 y   1  1   39793 -0.014368              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   48259  0.013134              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +     -      
 y   1  1   48261 -0.010091             10( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +     -      
 y   1  1   48310 -0.012775              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +          - 
 y   1  1   49433  0.012248              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -        +- 
 y   1  1   49483 -0.015342              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-   +     -    - 
 y   1  1   70183  0.018332              2( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -         - 
 y   1  1   70184 -0.018518              3( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -         - 
 y   1  1   70185 -0.010456              4( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -         - 
 y   1  1   75029 -0.014251              3( a  )   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-    -         - 
 y   1  1   78503  0.010275              9( a  )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +    +-    -        +- 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              93
     0.01> rq > 0.001           3667
    0.001> rq > 0.0001         16235
   0.0001> rq > 0.00001        28284
  0.00001> rq > 0.000001       27978
 0.000001> rq                  15545
           all                 91805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      203100 2x:           0 4x:           0
All internal counts: zz :      413563 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      244001    task #     2:      249944    task #     3:      837479    task #     4:       32008
task #     5:       45339    task #     6:        4545    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000919695180     -0.653959463621
     2     2      0.114669407709    -81.565886681743
     3     3     -0.929474314127    661.195912909807
     4     4     -0.001054591430      0.750191801351
     5     5      0.000218473685     -0.155583836705
     6     6      0.000411582378     -0.292698525504
     7     7     -0.002694338008      1.916567954273
     8     8     -0.002698476615      1.919495583617
     9     9     -0.067533832399     48.042977408365
    10    10      0.027799044941    -19.781049539433
    11    11      0.096290211341    -68.499370277820
    12    12     -0.015215214075     10.837109223093
    13    13     -0.000093730071      0.066679777380
    14    14      0.000112445651     -0.079985132973
    15    15     -0.000222219402      0.158025968124
    16    16     -0.000219961879      0.156419528877
    17    17     -0.000106811473      0.076030802459
    18    18     -0.029722640188     21.140019520450
    19    19     -0.014234832119     10.124533378788
    20    20      0.000047718560     -0.033958378488
    21    21      0.000117834183     -0.083797500609
    22    22     -0.000746156057      0.530982228986
    23    23      0.110498731656    -78.612317536368
    24    24     -0.031493354717     22.398825584423
    25    25     -0.037203601383     26.458944933496
    26    26      0.030681465608    -21.826513858711
    27    27      0.000272321456     -0.193757912981
    28    28     -0.000118833025      0.084534259541
    29    29      0.000189022759     -0.134534555958
    30    30      0.000606771252     -0.431737774591
    31    31      0.001044174846     -0.742817105247
    32    32      0.028048883913    -19.955741656058
    33    33      0.001007867935     -0.717673252428
    34    34     -0.000047901877      0.034077596521
    35    35      0.000280374397     -0.199458560959
    36    36      0.007194106629     -5.119915052543
    37    37      0.003486324915     -2.480307405776
    38    38      0.000001183914     -0.000878339923
    39    39     -0.000109727493      0.078044260420
    40    40      0.002024495167     -1.441137895355
    41    41      0.000262258593     -0.186549304486
    42    42      0.000061449677     -0.043659424247
    43    43     -0.000712699999      0.506967682786
    44    44     -0.064773738486     46.086765436075
    45    45     -0.013388879330      9.531514303991
    46    46      0.000040157136     -0.028565258816
    47    47     -0.000083251324      0.059262500306
    48    48      0.000091050634     -0.064817044873
    49    49     -0.003468642609      2.468448140470
    50    50      0.000026722760     -0.018988642799

 number of reference csfs (nref) is    50.  root number (iroot) is  4.
 c0**2 =   0.91378420  c**2 (all zwalks) =   0.92885101

 pople ci energy extrapolation is computed with 42 correlated electrons.

 eref      =   -711.366627540558   "relaxed" cnot**2         =   0.913784201826
 eci       =   -711.462811261398   deltae = eci - eref       =  -0.096183720840
 eci+dv1   =   -711.471103817661   dv1 = (1-cnot**2)*deltae  =  -0.008292556264
 eci+dv2   =   -711.471886222693   dv2 = dv1 / cnot**2       =  -0.009074961295
 eci+dv3   =   -711.472831649111   dv3 = dv1 / (2*cnot**2-1) =  -0.010020387713
 eci+pople =   -711.472256175493   ( 42e- scaled deltae )    =  -0.105628634935
maximum overlap with reference    1(overlap= 0.95022)
weight of reference states=  0.9044

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.95676)
weight of reference states=  0.9260

 information on vector: 2 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    3(overlap= 0.93240)
weight of reference states=  0.8738

 information on vector: 3 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    4(overlap= 0.94737)
weight of reference states=  0.9096

 information on vector: 4 from unit 11 written to unit 48 filename civout              
 passed aftci ... 
                       Size (real*8) of d2temp for two-external contributions    1054527
 
                       Size (real*8) of d2temp for all-internal contributions      44022
                       Size (real*8) of d2temp for one-external contributions     351900
                       Size (real*8) of d2temp for two-external contributions    1054527
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   52    70125
                1    2    1    1   52    70125
                1    3    1    1   52    70125
                1    4    1    1   52    70125
                1    5    1    1   52    70125
                1    6    1    1   52    70125
                1    7    1    1   52    70125
                1    8    1    1   52    70125
                1    9    1    1   52    70125
                1   10    1    1   52    70125
                1   11    1    1   52    70125
                1   12    1    1   52    70125
                1   13    1    1   52    70125
                1   14    1    1   52    70125
                1   15    1    1   52    70125
                1   16    1    1   52    70125
                1   17    1    1   52    70125
                1   18    1    1   52    70125
                1   19    1    1   52    70125
                1   20    1    1   52    70125
                1   21    1    1   52    70125
                1   22    1    1   52    70125
                1   23    1    1   52    70125
                       Size (real*8) of d2temp for three-external contributions    1612875
                       Size (real*8) of d2temp for four-external contributions     948753
 serial operation: forcing vdisk for temporary dd012 matrices
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart= 1468417
 files%d4ext =     unit=  24  vdsk=   1  filestart= 3088417
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                  6145     55297    411649         1         1
d2rec                     8        58       172        27        16
recsize                6144      6144      6144     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                4048417  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -468.746689847934     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.62624040  -242.87955055
 residuum:     0.00037862
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.12115223
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.02557439
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.03733045
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.08826447

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24
 ref:   1    -0.09929586    -0.10555031    -0.01128983    -0.03609658     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00871655     0.03940820     0.04506172    -0.01308525     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.04592776    -0.09668793    -0.00209956    -0.01628650     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   4     0.01825779     0.02992709     0.04326769     0.05354279     0.00000000     0.00000000     0.00000000     0.00000000

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.00000000
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.00000000
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.00000000
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y=   34801  D0X=       0  D0W=       0
  DDZI=   17300 DDYI=   39515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1785 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y= 1246697  D0X=       0  D0W=       0
  DDZI=  203100 DDYI=  457450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    42.000000
   42  correlated and    24  frozen core electrons
   7396 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000    
  a     6    0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000    
  a     7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000    
  a     8    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000    

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     9    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    10    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    11    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    12    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a    13    0.00000        0.00000        0.00000        0.00000        1.99919       7.300956E-05   4.424093E-04   1.077592E-04
  a    14    0.00000        0.00000        0.00000        0.00000       7.300956E-05    1.99941      -5.401735E-04   1.346367E-04
  a    15    0.00000        0.00000        0.00000        0.00000       4.424093E-04  -5.401735E-04    1.99888      -5.088120E-04
  a    16    0.00000        0.00000        0.00000        0.00000       1.077592E-04   1.346367E-04  -5.088120E-04    1.99830    
  a    17    0.00000        0.00000        0.00000        0.00000      -2.082282E-04  -2.534720E-04   1.215156E-04   9.120846E-04
  a    18    0.00000        0.00000        0.00000        0.00000      -2.546604E-04  -6.402814E-05   2.675355E-04  -2.500115E-04
  a    19    0.00000        0.00000        0.00000        0.00000       1.930421E-04   2.283961E-05  -3.749611E-04   1.107133E-04
  a    20    0.00000        0.00000        0.00000        0.00000       1.261109E-04  -1.831810E-04  -3.962270E-04  -3.839154E-05
  a    21    0.00000        0.00000        0.00000        0.00000       8.042573E-05  -1.185922E-04  -4.219615E-04  -1.861652E-04
  a    22    0.00000        0.00000        0.00000        0.00000       1.979712E-04   2.493752E-04  -2.289514E-05  -4.529174E-04
  a    23    0.00000        0.00000        0.00000        0.00000      -4.732157E-05   4.892477E-05  -9.080763E-05  -1.748736E-04
  a    24    0.00000        0.00000        0.00000        0.00000       2.141174E-04   1.497474E-04  -2.832701E-04  -6.932249E-04
  a    25    0.00000        0.00000        0.00000        0.00000      -9.103052E-05   1.323826E-04   1.362240E-04   2.360044E-04
  a    26    0.00000        0.00000        0.00000        0.00000       4.034789E-04   3.594551E-04  -1.758553E-04  -2.132955E-03
  a    27    0.00000        0.00000        0.00000        0.00000       8.377533E-05   2.038439E-06  -1.381527E-04  -2.910273E-04
  a    28    0.00000        0.00000        0.00000        0.00000      -4.322408E-04   2.646058E-04   8.106821E-04   8.064890E-04
  a    29    0.00000        0.00000        0.00000        0.00000      -4.203898E-06   2.493868E-07   2.257194E-06  -4.991684E-07
  a    30    0.00000        0.00000        0.00000        0.00000      -6.960143E-06   1.432129E-05  -3.045658E-06  -2.090466E-06
  a    31    0.00000        0.00000        0.00000        0.00000      -3.320142E-06   9.222687E-06   2.994819E-05   4.971198E-06
  a    32    0.00000        0.00000        0.00000        0.00000       2.656386E-05  -4.572867E-05   1.069532E-04  -5.500146E-05
  a    33    0.00000        0.00000        0.00000        0.00000      -8.754323E-05  -3.507179E-04  -4.887459E-04   3.344275E-04
  a    34    0.00000        0.00000        0.00000        0.00000       1.242075E-04  -2.853816E-04   4.815627E-04  -2.893931E-04
  a    35    0.00000        0.00000        0.00000        0.00000       1.856355E-05   5.242070E-05  -1.088439E-04   5.396446E-05
  a    36    0.00000        0.00000        0.00000        0.00000       3.748201E-05  -3.029171E-04   4.809783E-04  -2.464809E-04
  a    37    0.00000        0.00000        0.00000        0.00000       2.070082E-03   1.244758E-03   7.679873E-05  -6.295143E-03
  a    38    0.00000        0.00000        0.00000        0.00000      -1.568922E-03  -1.769249E-03   4.762577E-03   8.989783E-03
  a    39    0.00000        0.00000        0.00000        0.00000      -4.785115E-04   1.507460E-03  -7.540634E-04   7.605918E-03
  a    40    0.00000        0.00000        0.00000        0.00000       5.194174E-04   4.385788E-04  -2.089886E-03  -4.229712E-03
  a    41    0.00000        0.00000        0.00000        0.00000      -7.351551E-04   3.467648E-04  -9.356956E-04  -1.952291E-03
  a    42    0.00000        0.00000        0.00000        0.00000       2.817954E-04  -3.010596E-04   1.227829E-04   6.132317E-04
  a    43    0.00000        0.00000        0.00000        0.00000      -1.830296E-03   1.221879E-03  -1.005301E-03   2.017820E-03
  a    44    0.00000        0.00000        0.00000        0.00000      -2.745835E-03  -8.087810E-04   2.233133E-03   6.900954E-03
  a    45    0.00000        0.00000        0.00000        0.00000       9.353081E-04   3.385385E-03   1.558087E-03  -2.769933E-03
  a    46    0.00000        0.00000        0.00000        0.00000       4.394152E-04   1.553964E-03   7.141095E-04  -1.719373E-03
  a    47    0.00000        0.00000        0.00000        0.00000       1.742153E-03  -1.968327E-03  -2.326354E-03  -7.857478E-03
  a    48    0.00000        0.00000        0.00000        0.00000       1.878985E-04  -2.976386E-03   2.219838E-03   6.383766E-03
  a    49    0.00000        0.00000        0.00000        0.00000      -6.351568E-04   7.702302E-04   1.439343E-04  -4.448931E-03
  a    50    0.00000        0.00000        0.00000        0.00000       6.453546E-04   5.116834E-04   9.852774E-04  -1.164366E-03
  a    51    0.00000        0.00000        0.00000        0.00000      -3.516873E-05  -3.456872E-05  -6.138442E-05   5.501421E-05
  a    52    0.00000        0.00000        0.00000        0.00000      -1.698695E-04   2.562665E-04  -3.526889E-03  -7.201902E-03
  a    53    0.00000        0.00000        0.00000        0.00000      -3.315537E-04  -1.921392E-03  -1.880661E-03   3.774980E-03
  a    54    0.00000        0.00000        0.00000        0.00000      -4.857649E-05  -1.391564E-05  -4.129867E-05  -1.015625E-04
  a    55    0.00000        0.00000        0.00000        0.00000       6.227241E-04   1.282834E-03   1.477996E-03   1.426346E-03
  a    56    0.00000        0.00000        0.00000        0.00000      -2.008560E-03  -6.022878E-04   4.255477E-04  -3.973598E-03
  a    57    0.00000        0.00000        0.00000        0.00000       2.827236E-04   6.469896E-04   2.472026E-03  -2.741864E-03
  a    58    0.00000        0.00000        0.00000        0.00000      -1.749328E-05   1.273690E-05   8.396367E-06   8.532373E-05
  a    59    0.00000        0.00000        0.00000        0.00000      -1.184778E-04   1.171696E-04   9.563607E-05  -1.359537E-04
  a    60    0.00000        0.00000        0.00000        0.00000      -1.271195E-03   1.037136E-03   7.491480E-04  -1.679740E-03
  a    61    0.00000        0.00000        0.00000        0.00000      -5.240887E-04  -9.442152E-04  -1.561405E-03  -2.052192E-04
  a    62    0.00000        0.00000        0.00000        0.00000       1.345814E-04  -6.598075E-04  -2.584659E-04   2.761794E-04
  a    63    0.00000        0.00000        0.00000        0.00000      -9.016392E-04   2.796103E-03   2.337014E-04  -9.180292E-04
  a    64    0.00000        0.00000        0.00000        0.00000       1.901754E-04   2.628566E-04  -5.503944E-04   2.926457E-06
  a    65    0.00000        0.00000        0.00000        0.00000      -1.356759E-03   3.615748E-04   1.816313E-03   7.837982E-04
  a    66    0.00000        0.00000        0.00000        0.00000       1.284054E-04  -7.027070E-04  -2.194039E-03  -5.159930E-04
  a    67    0.00000        0.00000        0.00000        0.00000      -1.686236E-04  -7.294343E-04  -1.614325E-03  -1.235576E-03
  a    68    0.00000        0.00000        0.00000        0.00000      -6.573809E-05   7.886018E-04  -1.134479E-04  -4.249170E-05
  a    69    0.00000        0.00000        0.00000        0.00000       1.024966E-04  -6.890219E-04   2.175394E-04   8.991005E-05
  a    70    0.00000        0.00000        0.00000        0.00000       1.732027E-04  -6.308339E-04   6.182324E-04   7.944131E-05
  a    71    0.00000        0.00000        0.00000        0.00000      -8.208726E-04  -2.232331E-03   1.277196E-03   1.395049E-03
  a    72    0.00000        0.00000        0.00000        0.00000      -4.091465E-03  -4.469569E-04   1.310568E-03   3.305769E-03
  a    73    0.00000        0.00000        0.00000        0.00000      -2.151540E-04  -2.249250E-04   1.490182E-04   2.638260E-04
  a    74    0.00000        0.00000        0.00000        0.00000      -1.126250E-03   1.264468E-03  -9.643074E-04  -8.138354E-04
  a    75    0.00000        0.00000        0.00000        0.00000       8.624383E-05  -1.238854E-03  -1.175999E-03  -3.061238E-03
  a    76    0.00000        0.00000        0.00000        0.00000       6.906751E-04  -6.128470E-04   1.199858E-05   1.514227E-03
  a    77    0.00000        0.00000        0.00000        0.00000       2.688856E-03   3.496115E-04  -1.173351E-03  -2.271179E-03
  a    78    0.00000        0.00000        0.00000        0.00000      -5.613472E-04  -9.827722E-04  -1.124511E-03   2.294988E-04
  a    79    0.00000        0.00000        0.00000        0.00000       9.387661E-05   1.708552E-03   4.812581E-04  -2.409068E-04
  a    80    0.00000        0.00000        0.00000        0.00000       1.425378E-03  -1.088806E-05  -7.135794E-04  -8.168823E-04
  a    81    0.00000        0.00000        0.00000        0.00000       7.615881E-05  -6.141963E-04  -2.451258E-04  -4.861838E-04
  a    82    0.00000        0.00000        0.00000        0.00000      -1.893737E-04  -6.788233E-04  -9.630763E-04   1.219735E-03
  a    83    0.00000        0.00000        0.00000        0.00000      -1.404922E-04  -2.825147E-04   1.438293E-03   1.777814E-03
  a    84    0.00000        0.00000        0.00000        0.00000       6.578268E-05  -3.735887E-04   2.064226E-04  -8.655138E-05
  a    85    0.00000        0.00000        0.00000        0.00000       2.075340E-04  -1.741138E-04   3.604643E-04  -5.729065E-04
  a    86    0.00000        0.00000        0.00000        0.00000       2.956048E-04   1.795355E-04  -4.237178E-04  -3.210101E-04

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a    13  -2.082282E-04  -2.546604E-04   1.930421E-04   1.261109E-04   8.042573E-05   1.979712E-04  -4.732157E-05   2.141174E-04
  a    14  -2.534720E-04  -6.402814E-05   2.283961E-05  -1.831810E-04  -1.185922E-04   2.493752E-04   4.892477E-05   1.497474E-04
  a    15   1.215156E-04   2.675355E-04  -3.749611E-04  -3.962270E-04  -4.219615E-04  -2.289514E-05  -9.080763E-05  -2.832701E-04
  a    16   9.120846E-04  -2.500115E-04   1.107133E-04  -3.839154E-05  -1.861652E-04  -4.529174E-04  -1.748736E-04  -6.932249E-04
  a    17    1.99739      -4.366858E-04  -5.254018E-04  -1.472512E-04  -1.711606E-04   5.324773E-04   1.379282E-04   1.391016E-03
  a    18  -4.366858E-04    1.99825       2.635936E-05   6.670754E-05   7.621499E-04   2.610143E-04   3.469470E-04   4.317711E-04
  a    19  -5.254018E-04   2.635936E-05    1.99756      -4.537087E-04  -3.450207E-04  -5.195044E-04  -4.093285E-04  -1.926280E-04
  a    20  -1.472512E-04   6.670754E-05  -4.537087E-04    1.99798      -3.029996E-04  -1.980059E-04  -1.221602E-04  -9.549478E-04
  a    21  -1.711606E-04   7.621499E-04  -3.450207E-04  -3.029996E-04    1.99763      -2.414428E-05  -1.242998E-03  -3.684362E-04
  a    22   5.324773E-04   2.610143E-04  -5.195044E-04  -1.980059E-04  -2.414428E-05    1.99855       1.077355E-04  -6.714126E-04
  a    23   1.379282E-04   3.469470E-04  -4.093285E-04  -1.221602E-04  -1.242998E-03   1.077355E-04    1.99849      -3.427239E-04
  a    24   1.391016E-03   4.317711E-04  -1.926280E-04  -9.549478E-04  -3.684362E-04  -6.714126E-04  -3.427239E-04    1.99696    
  a    25   3.772396E-04   2.652505E-04  -6.443711E-05  -1.699814E-04   2.782889E-04   2.707260E-04  -4.763787E-04  -5.813087E-04
  a    26   3.055655E-03   5.813759E-05   2.204857E-03   1.620576E-04   5.352229E-04  -1.312789E-03  -1.292506E-04  -3.154065E-03
  a    27   4.010597E-04  -3.509364E-06   4.283944E-04   5.096024E-05   1.247423E-04  -9.296605E-05   6.970071E-05  -3.850884E-04
  a    28  -1.008806E-03  -4.660147E-04  -1.538477E-04   3.098633E-04   6.154193E-04   6.275075E-05   2.821102E-04   1.321264E-03
  a    29  -2.302082E-06  -2.550022E-07   5.932909E-06   7.235795E-06   3.270627E-06  -2.616249E-06  -3.713499E-06   2.841698E-06
  a    30   6.673842E-06   5.678994E-06   1.447859E-05  -9.984420E-07   3.130205E-05  -1.818913E-05  -8.416644E-07  -8.535454E-06
  a    31   3.442123E-05  -3.584335E-06  -8.860524E-06   1.161976E-05   1.732983E-06  -5.804409E-06  -3.577432E-05  -4.116123E-05
  a    32   3.915701E-05   8.218345E-05  -1.435437E-04  -3.532966E-05  -6.913428E-05   3.627931E-04   2.721831E-04   1.201053E-04
  a    33   1.412642E-04   1.238796E-03  -5.127854E-04   3.174644E-04  -1.222055E-03  -2.030666E-04  -6.101939E-04   2.287450E-04
  a    34   1.103528E-04   3.089944E-04  -5.736115E-04  -2.806390E-04  -2.176669E-04   1.640050E-03   1.165661E-03   4.781579E-04
  a    35   4.220914E-05   2.246792E-05  -3.711721E-05   1.143269E-04  -1.766019E-04  -1.839929E-04   5.497588E-05   1.541138E-06
  a    36   1.397716E-05   4.375657E-04  -4.443719E-04  -1.301964E-04  -8.345206E-05   1.348810E-03   9.555498E-04   4.185470E-04
  a    37   8.495995E-03   1.175602E-03   1.319927E-02  -2.073406E-02  -3.190715E-03  -3.260516E-03  -6.258184E-06  -2.036043E-02
  a    38  -1.533095E-02  -1.618829E-03  -1.090544E-02  -8.744652E-03  -3.097245E-03   4.651802E-03   4.162425E-03   1.468158E-02
  a    39  -8.008975E-03  -1.671884E-03  -2.129229E-03  -5.559182E-03  -3.135847E-03  -1.009939E-02   3.812901E-03   5.721728E-03
  a    40   1.238804E-02   7.213042E-03   1.522112E-03   5.203163E-03  -3.124537E-03  -1.026026E-02   5.019884E-04   8.357752E-03
  a    41  -1.324587E-03  -9.275322E-03  -2.914900E-03  -2.994253E-03  -3.173710E-03   4.578145E-04   1.409755E-03   5.385175E-03
  a    42   3.559681E-03   3.382855E-03   7.174856E-04   3.502389E-03  -1.052109E-02  -5.619042E-04   1.059283E-02  -5.232508E-03
  a    43  -2.426924E-03   1.014313E-02  -4.034209E-03  -2.686854E-03  -1.119185E-02   3.316409E-03  -1.226762E-02  -3.484495E-04
  a    44  -4.949802E-03   5.174689E-03  -4.491421E-03   3.532085E-04  -2.786645E-03  -2.208781E-03  -4.515688E-03   5.613120E-03
  a    45   3.371189E-03   2.623522E-03   2.076270E-03   3.072363E-03   7.970607E-03  -9.133739E-04   5.047109E-03   5.018637E-03
  a    46  -2.052625E-03  -1.536451E-03  -1.187005E-02  -9.161610E-03  -8.389055E-04  -5.911573E-03  -3.485848E-03  -4.592057E-03
  a    47   1.865049E-02   5.834599E-03   1.591493E-02   9.964543E-03  -4.064936E-03   1.372197E-03  -3.741800E-03   2.297420E-03
  a    48  -1.153700E-02  -2.258852E-03  -4.978721E-03  -2.936417E-03  -2.281879E-03  -9.206573E-03  -4.604300E-03  -4.703662E-03
  a    49   4.282020E-03  -1.039809E-03   3.835951E-03  -1.344330E-03  -2.930872E-03   3.166274E-03  -2.258030E-03   5.107162E-03
  a    50   4.003665E-03  -1.474913E-03  -3.174423E-04   2.357331E-03  -4.003319E-03  -2.582485E-03  -2.023733E-03  -7.667746E-03
  a    51  -2.015304E-04   8.280821E-05   8.228543E-05  -1.083910E-04   2.299304E-04   1.050359E-04   5.831698E-05   4.618121E-04
  a    52   1.134032E-02   3.825621E-03   6.354362E-03  -1.237883E-03   8.624789E-04  -2.171758E-03  -6.674851E-04  -9.929466E-03
  a    53  -5.558568E-03   4.468287E-03  -1.467123E-03  -1.046187E-03  -1.427710E-03   9.712380E-04  -1.694186E-03   4.821782E-03
  a    54   2.228261E-04  -2.985968E-04   1.219846E-04  -2.478796E-04   3.527461E-04   6.215351E-05   2.645258E-04  -3.198133E-04
  a    55  -1.918297E-03   3.075591E-03  -6.261480E-04   5.779920E-03  -6.524358E-03  -2.400277E-03  -5.266474E-03   4.426042E-03
  a    56   4.970707E-03   2.327830E-03   7.564139E-04  -3.047651E-03  -4.627540E-03  -7.473260E-03  -4.152006E-03  -4.587051E-03
  a    57   2.142543E-04  -1.083413E-03   2.945153E-03   2.349868E-03   3.314205E-03  -6.061216E-03   1.807537E-03   2.929472E-03
  a    58   1.937979E-04  -3.742494E-04  -1.775136E-06  -1.361928E-04  -3.388242E-05   1.375796E-04  -9.135507E-05  -4.208289E-04
  a    59   2.029587E-04  -3.020930E-04   7.490562E-05  -2.308814E-04   4.366021E-05   1.155202E-05  -3.091707E-04  -3.079835E-04
  a    60   9.115443E-04  -3.232199E-03   5.338005E-04  -4.758394E-03   4.838361E-03   1.882223E-03   1.009725E-03  -2.208412E-03
  a    61  -4.487311E-03   2.117566E-03   2.120645E-03  -1.864908E-03   3.415607E-03   1.327886E-03   2.602282E-03   5.564406E-03
  a    62  -8.098953E-04  -1.585755E-04   8.403505E-04  -1.411460E-04   8.190275E-04  -5.047887E-04   8.298121E-04   8.181356E-04
  a    63   1.609929E-03   1.434794E-03  -2.550518E-03   2.997957E-04  -4.961031E-03   2.713016E-03  -2.505670E-03  -7.875038E-04
  a    64   5.243017E-04   4.855445E-04  -8.749431E-04  -4.484198E-05   2.196646E-03   1.333852E-03   7.885264E-04  -6.180486E-05
  a    65  -2.313365E-03   1.659116E-03   1.053881E-03   3.418794E-04  -1.415049E-03  -1.329599E-03   1.191505E-03  -2.608795E-04
  a    66   2.848219E-03   1.870633E-03  -5.738404E-04   5.058562E-04   1.464550E-03  -2.951582E-03   2.904104E-03  -3.768521E-06
  a    67  -4.806070E-05  -1.410270E-04  -1.181297E-03   2.437802E-03   6.586990E-04   1.707432E-03  -1.115521E-03  -4.468340E-04
  a    68   3.674600E-04   1.049503E-04  -4.629991E-04  -1.032181E-03  -1.585063E-03   1.921598E-05   1.956364E-03   3.255950E-04
  a    69  -3.294777E-04  -3.959709E-05   4.292860E-04   6.744661E-04   1.114029E-03   1.081388E-04  -1.240187E-03  -1.450916E-04
  a    70  -6.848029E-05   1.019362E-04   2.290697E-04  -1.154088E-05   1.625465E-04   4.535792E-05  -2.113385E-04  -1.206904E-05
  a    71   1.698086E-04  -5.561455E-04  -1.065138E-04   2.515182E-03  -1.238518E-03  -9.636362E-04  -8.465063E-04   1.648719E-03
  a    72  -1.698088E-03  -2.153736E-04  -1.143810E-03   2.284415E-03   2.309365E-04   1.448998E-03   1.153025E-03   3.096227E-03
  a    73  -1.754580E-04  -1.468677E-04   7.466793E-05   2.901480E-04  -9.227761E-05   2.852880E-04   2.340739E-04   3.670105E-04
  a    74   5.685164E-04   4.732788E-04  -1.599585E-03   4.635383E-04   2.387280E-04  -1.276265E-03  -2.031313E-03   3.585944E-04
  a    75   1.280100E-03  -9.832603E-04  -2.545330E-03  -4.618678E-04  -8.895329E-04  -3.226383E-05  -8.830129E-04  -7.671839E-04
  a    76   6.125784E-05   2.673546E-04   1.273151E-03   3.561452E-04  -7.132537E-04  -2.050789E-04   5.679245E-04   1.938456E-04
  a    77   2.650106E-03  -3.466822E-04  -1.550853E-03  -1.026974E-03  -2.703238E-04  -3.456055E-03   5.925644E-04  -4.494466E-03
  a    78  -7.807000E-04  -3.380480E-03   1.075323E-03   6.465881E-04  -1.739241E-03   3.845048E-04   3.401840E-04   2.264439E-04
  a    79   4.290078E-04   1.663464E-03  -1.067335E-03  -8.044867E-04   2.926487E-04   5.181841E-04   1.272831E-03   4.212080E-04
  a    80   3.133245E-04  -1.004122E-03  -1.174251E-04  -1.346204E-03  -3.066854E-07   1.370927E-04  -5.138034E-04  -9.572263E-04
  a    81   7.927488E-04  -6.452390E-04  -4.803121E-04   6.509495E-05   1.928803E-04  -1.340417E-03   2.643780E-04  -1.205725E-03
  a    82   8.543419E-04   3.292049E-03  -8.326392E-04   2.561939E-04  -9.905010E-04  -1.047913E-03  -8.353832E-04  -6.626122E-04
  a    83   2.389319E-04  -1.074140E-03   1.956286E-03   1.023522E-03   6.249298E-04   1.174194E-04   7.369456E-04  -9.881345E-04
  a    84   5.216075E-05   6.553060E-04  -2.093545E-04  -5.231267E-05   2.170270E-04   1.861402E-05  -2.228747E-04  -1.156224E-04
  a    85  -1.855666E-04  -4.642476E-04   2.938031E-04  -8.198594E-04   8.503573E-05   4.032962E-04  -1.433572E-04  -2.795610E-04
  a    86  -1.552876E-05   1.473397E-04  -2.355913E-04  -6.148113E-04  -5.134451E-05  -1.326458E-04  -1.999929E-04  -2.475773E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a    13  -9.103052E-05   4.034789E-04   8.377533E-05  -4.322408E-04  -4.203898E-06  -6.960143E-06  -3.320142E-06   2.656386E-05
  a    14   1.323826E-04   3.594551E-04   2.038439E-06   2.646058E-04   2.493868E-07   1.432129E-05   9.222687E-06  -4.572867E-05
  a    15   1.362240E-04  -1.758553E-04  -1.381527E-04   8.106821E-04   2.257194E-06  -3.045658E-06   2.994819E-05   1.069532E-04
  a    16   2.360044E-04  -2.132955E-03  -2.910273E-04   8.064890E-04  -4.991684E-07  -2.090466E-06   4.971198E-06  -5.500146E-05
  a    17   3.772396E-04   3.055655E-03   4.010597E-04  -1.008806E-03  -2.302082E-06   6.673842E-06   3.442123E-05   3.915701E-05
  a    18   2.652505E-04   5.813759E-05  -3.509364E-06  -4.660147E-04  -2.550022E-07   5.678994E-06  -3.584335E-06   8.218345E-05
  a    19  -6.443711E-05   2.204857E-03   4.283944E-04  -1.538477E-04   5.932909E-06   1.447859E-05  -8.860524E-06  -1.435437E-04
  a    20  -1.699814E-04   1.620576E-04   5.096024E-05   3.098633E-04   7.235795E-06  -9.984420E-07   1.161976E-05  -3.532966E-05
  a    21   2.782889E-04   5.352229E-04   1.247423E-04   6.154193E-04   3.270627E-06   3.130205E-05   1.732983E-06  -6.913428E-05
  a    22   2.707260E-04  -1.312789E-03  -9.296605E-05   6.275075E-05  -2.616249E-06  -1.818913E-05  -5.804409E-06   3.627931E-04
  a    23  -4.763787E-04  -1.292506E-04   6.970071E-05   2.821102E-04  -3.713499E-06  -8.416644E-07  -3.577432E-05   2.721831E-04
  a    24  -5.813087E-04  -3.154065E-03  -3.850884E-04   1.321264E-03   2.841698E-06  -8.535454E-06  -4.116123E-05   1.201053E-04
  a    25    1.99795      -4.556752E-04   1.565921E-05  -4.394044E-04  -9.040079E-06  -5.967791E-06   2.180952E-05   1.367275E-04
  a    26  -4.556752E-04    1.98812      -1.594464E-03   2.299744E-03   1.001033E-05  -1.862209E-05   1.053285E-05  -4.598205E-04
  a    27   1.565921E-05  -1.594464E-03    1.99908       6.153600E-04  -1.375410E-04   1.982773E-04  -1.777454E-04   3.745261E-03
  a    28  -4.394044E-04   2.299744E-03   6.153600E-04    1.99561      -3.089598E-05   8.159852E-05   1.597992E-05   4.443194E-04
  a    29  -9.040079E-06   1.001033E-05  -1.375410E-04  -3.089598E-05    1.99347       2.245577E-03   1.869711E-03   5.195323E-03
  a    30  -5.967791E-06  -1.862209E-05   1.982773E-04   8.159852E-05   2.245577E-03    1.98183      -2.337958E-03  -8.301163E-03
  a    31   2.180952E-05   1.053285E-05  -1.777454E-04   1.597992E-05   1.869711E-03  -2.337958E-03    1.93734      -2.274482E-02
  a    32   1.367275E-04  -4.598205E-04   3.745261E-03   4.443194E-04   5.195323E-03  -8.301163E-03  -2.274482E-02    1.84595    
  a    33   1.973557E-06   5.328375E-04   7.916426E-05  -2.971658E-04  -5.656883E-05   1.248074E-04   2.876271E-04   9.711393E-04
  a    34   7.320446E-04  -2.234604E-03   1.703923E-02   2.058563E-03   1.733876E-02  -4.970385E-02  -4.241939E-02  -0.365489    
  a    35  -2.904394E-04   4.907457E-04  -2.924725E-03  -5.079827E-04  -1.795149E-02   3.477610E-02   1.105466E-02  -0.162195    
  a    36   3.635646E-04  -1.808053E-03   1.252407E-02   1.095517E-03  -1.408008E-03   2.613204E-02   6.071367E-04   5.618206E-02
  a    37  -1.349344E-03  -3.827926E-02  -5.214954E-03   1.162639E-02   2.533185E-05   2.877628E-04   3.491251E-05   1.864625E-04
  a    38   7.954514E-03   4.291567E-02   8.374603E-03  -1.216826E-02   2.270944E-04   1.562172E-03   6.018666E-04   4.163433E-05
  a    39  -4.582686E-04   2.715392E-02   1.505405E-03  -9.301694E-03  -1.715717E-04  -1.994268E-03  -3.922421E-04  -5.431234E-04
  a    40   4.003913E-03  -1.654888E-02  -1.847901E-03  -4.248208E-04   2.896840E-06   2.058315E-04  -3.175729E-05  -2.224275E-04
  a    41  -1.407143E-02  -3.771376E-04  -1.621507E-03  -5.409342E-04  -1.190610E-04  -4.233601E-04  -3.332878E-05  -7.914271E-04
  a    42  -1.952346E-03  -2.944058E-03  -1.800105E-03   2.149259E-03  -6.267648E-05  -2.265289E-05   1.838419E-04  -1.196620E-03
  a    43  -1.029870E-02   1.266797E-03  -6.522414E-04   3.647734E-03  -1.290447E-04  -5.604079E-04  -2.908652E-04  -8.630652E-04
  a    44  -3.522239E-03   7.012409E-03   2.530997E-03  -1.338559E-02   1.015191E-05  -1.780669E-04   5.601855E-06  -7.134042E-05
  a    45  -3.763288E-03  -1.029294E-02  -1.167983E-03  -2.449437E-03  -1.989057E-05   4.366676E-04  -4.652646E-06   6.907523E-05
  a    46  -4.502552E-03   7.700857E-03   4.953686E-03  -9.884427E-03   2.212278E-04   8.436155E-04   3.616232E-04   3.770720E-04
  a    47  -3.635331E-03  -8.945353E-03   3.745759E-04   8.838016E-03   1.600149E-04   9.408638E-04   3.199012E-04   6.719046E-04
  a    48  -7.686636E-03  -7.709125E-03   3.948023E-03  -3.859919E-03   2.785065E-04   1.563229E-03   5.847959E-04   7.130115E-04
  a    49  -2.927578E-03   1.806320E-02   3.855003E-03  -1.010429E-02   6.453259E-05   2.463738E-04   4.326198E-05   1.478120E-04
  a    50   1.409114E-03  -1.631872E-02  -2.890869E-03   4.759213E-03   8.141201E-05  -6.385122E-04   3.175514E-04  -2.769737E-03
  a    51  -1.295945E-04   1.062317E-03  -2.275280E-04  -3.691329E-04   1.894086E-03  -1.923120E-03   8.215071E-03  -4.404925E-02
  a    52  -1.605487E-03  -3.737655E-02  -6.430123E-03   1.062345E-02  -8.807650E-05  -2.106893E-04  -1.489343E-04  -2.521981E-04
  a    53  -1.291324E-04   1.145891E-02   2.433567E-03  -3.766458E-03   4.262708E-05   3.389510E-04   1.800785E-04  -4.059024E-04
  a    54   4.378908E-05  -3.486804E-04   1.560134E-03   4.103146E-04  -2.915982E-04   5.686495E-03   1.638996E-03  -3.940911E-03
  a    55   1.797787E-03   1.911930E-04   8.978684E-04  -3.037435E-03   1.251728E-05   2.451456E-04  -3.120796E-05  -1.476922E-04
  a    56  -3.004597E-03  -3.916160E-03   1.944232E-03  -3.485560E-03   1.203864E-04   1.413366E-04  -4.065781E-05   5.850128E-05
  a    57  -8.781037E-04  -3.876536E-03   4.636757E-04   2.664354E-03   8.704944E-05   3.001307E-04  -2.905046E-04  -1.102317E-04
  a    58  -8.151513E-05   6.787223E-05   1.295979E-03  -5.365587E-05   1.584719E-04   4.008307E-03  -1.144658E-02  -4.517481E-03
  a    59   1.486973E-05  -3.367081E-04   1.988146E-03   2.133655E-05  -7.207535E-04   5.028856E-03   3.931626E-03   3.987595E-03
  a    60  -3.050690E-03  -2.508680E-04  -1.505057E-04   4.956702E-04   8.289226E-05  -5.712569E-04   2.429588E-04  -1.635006E-05
  a    61   3.387407E-04  -1.273135E-03  -1.210282E-03   5.062165E-03  -1.398997E-04   4.693739E-04  -7.197995E-04  -7.191959E-04
  a    62  -7.321695E-04   5.456263E-04   5.161174E-04   1.299131E-03   1.266451E-03  -1.152828E-03   3.505709E-03   5.372295E-03
  a    63   4.342088E-03  -4.836898E-03  -2.505491E-04  -4.168973E-03   2.404125E-04  -6.814823E-05   4.270840E-04   8.854212E-04
  a    64   6.606448E-04  -4.928225E-04   2.067890E-03   1.116562E-03   4.728429E-04   1.228083E-03   3.280859E-04   1.481569E-03
  a    65   2.849137E-03   3.676630E-03   1.088480E-03  -3.618758E-04   7.856168E-05   2.427637E-04   4.762125E-05   1.889503E-04
  a    66   3.289309E-03  -2.605073E-03  -5.241522E-04  -2.887999E-03  -1.782272E-05  -8.842261E-05  -1.921421E-05   1.491026E-06
  a    67  -3.264084E-03  -3.969951E-03  -9.922050E-04   7.913358E-04   3.860778E-05   3.561123E-04  -1.146615E-04   2.568553E-04
  a    68  -1.271106E-03  -3.326495E-04  -2.758089E-04  -1.334467E-03   5.299107E-04   1.410893E-03  -3.014908E-03   3.046620E-03
  a    69   1.115680E-03   2.762515E-04   7.096762E-04   1.084886E-03   4.740957E-04   6.453526E-04  -3.703165E-03   2.755982E-03
  a    70   8.763080E-04   7.013413E-04  -1.799017E-03   2.563738E-04   2.542145E-04   2.103837E-03   1.329131E-05   1.192527E-03
  a    71  -3.923676E-04   2.863319E-03   9.181272E-04   2.127992E-03  -5.196609E-05  -2.195312E-04   6.961511E-05  -6.088952E-04
  a    72   2.117101E-03   7.001166E-03   1.463615E-04   1.599792E-03  -6.241322E-05  -9.267353E-05   1.351559E-05  -2.530577E-04
  a    73   1.169759E-04   7.153960E-05   2.557013E-03   6.114789E-04   1.150466E-03   2.099062E-03  -2.560651E-03   7.876010E-03
  a    74  -1.910851E-03   3.110357E-04  -1.695674E-04  -1.182985E-03   1.189515E-04   2.972573E-04  -1.213662E-04   5.336328E-04
  a    75  -2.768894E-04  -5.546476E-04   4.181452E-04  -2.170549E-03  -2.800670E-05  -9.558115E-05  -3.649957E-05  -9.508833E-05
  a    76  -6.618902E-04   1.034382E-03   7.248613E-04  -4.366869E-03  -3.296148E-05   5.266748E-05  -3.244059E-05  -1.060996E-04
  a    77   1.002113E-03  -5.396879E-03  -7.369776E-04   5.340085E-03   2.377807E-05   4.258436E-05  -5.559023E-05   4.258855E-05
  a    78   8.766960E-04  -1.613661E-04   2.293867E-04   1.036516E-04  -2.461135E-05   8.784328E-06   1.763239E-04  -2.716679E-04
  a    79  -1.582479E-03   2.115053E-03  -8.839093E-05   5.518111E-04  -1.206130E-04  -1.835801E-04   1.907860E-06  -2.785441E-04
  a    80   5.981622E-05   3.611186E-03   7.871180E-04  -1.355789E-03   8.225812E-05   8.203271E-05   1.616381E-05   1.753073E-04
  a    81   4.387444E-04  -3.101565E-03  -2.616518E-04   3.984308E-04   2.869072E-06   1.364871E-05   1.279488E-05   3.221672E-06
  a    82  -9.018332E-04  -1.489093E-03  -7.352895E-05  -3.070560E-04   1.607838E-05  -4.887657E-05  -6.371365E-06  -5.456749E-06
  a    83   2.211926E-04  -1.371853E-03   7.525320E-06   2.874572E-04   3.478964E-05   7.027424E-05   4.948920E-05   4.942175E-05
  a    84   1.619180E-05  -7.986235E-04  -4.077391E-04  -1.273817E-03  -2.275799E-05  -4.020493E-05  -5.975351E-05  -2.703263E-05
  a    85   4.350453E-06  -2.009075E-04  -2.713053E-04  -3.845146E-04  -4.111757E-06  -6.402401E-06  -3.509665E-05  -1.615732E-05
  a    86  -4.631079E-04   1.168375E-04   1.730615E-04  -7.184452E-04   2.826483E-05   3.157707E-06   3.271682E-05   4.991105E-05

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a    13  -8.754323E-05   1.242075E-04   1.856355E-05   3.748201E-05   2.070082E-03  -1.568922E-03  -4.785115E-04   5.194174E-04
  a    14  -3.507179E-04  -2.853816E-04   5.242070E-05  -3.029171E-04   1.244758E-03  -1.769249E-03   1.507460E-03   4.385788E-04
  a    15  -4.887459E-04   4.815627E-04  -1.088439E-04   4.809783E-04   7.679873E-05   4.762577E-03  -7.540634E-04  -2.089886E-03
  a    16   3.344275E-04  -2.893931E-04   5.396446E-05  -2.464809E-04  -6.295143E-03   8.989783E-03   7.605918E-03  -4.229712E-03
  a    17   1.412642E-04   1.103528E-04   4.220914E-05   1.397716E-05   8.495995E-03  -1.533095E-02  -8.008975E-03   1.238804E-02
  a    18   1.238796E-03   3.089944E-04   2.246792E-05   4.375657E-04   1.175602E-03  -1.618829E-03  -1.671884E-03   7.213042E-03
  a    19  -5.127854E-04  -5.736115E-04  -3.711721E-05  -4.443719E-04   1.319927E-02  -1.090544E-02  -2.129229E-03   1.522112E-03
  a    20   3.174644E-04  -2.806390E-04   1.143269E-04  -1.301964E-04  -2.073406E-02  -8.744652E-03  -5.559182E-03   5.203163E-03
  a    21  -1.222055E-03  -2.176669E-04  -1.766019E-04  -8.345206E-05  -3.190715E-03  -3.097245E-03  -3.135847E-03  -3.124537E-03
  a    22  -2.030666E-04   1.640050E-03  -1.839929E-04   1.348810E-03  -3.260516E-03   4.651802E-03  -1.009939E-02  -1.026026E-02
  a    23  -6.101939E-04   1.165661E-03   5.497588E-05   9.555498E-04  -6.258184E-06   4.162425E-03   3.812901E-03   5.019884E-04
  a    24   2.287450E-04   4.781579E-04   1.541138E-06   4.185470E-04  -2.036043E-02   1.468158E-02   5.721728E-03   8.357752E-03
  a    25   1.973557E-06   7.320446E-04  -2.904394E-04   3.635646E-04  -1.349344E-03   7.954514E-03  -4.582686E-04   4.003913E-03
  a    26   5.328375E-04  -2.234604E-03   4.907457E-04  -1.808053E-03  -3.827926E-02   4.291567E-02   2.715392E-02  -1.654888E-02
  a    27   7.916426E-05   1.703923E-02  -2.924725E-03   1.252407E-02  -5.214954E-03   8.374603E-03   1.505405E-03  -1.847901E-03
  a    28  -2.971658E-04   2.058563E-03  -5.079827E-04   1.095517E-03   1.162639E-02  -1.216826E-02  -9.301694E-03  -4.248208E-04
  a    29  -5.656883E-05   1.733876E-02  -1.795149E-02  -1.408008E-03   2.533185E-05   2.270944E-04  -1.715717E-04   2.896840E-06
  a    30   1.248074E-04  -4.970385E-02   3.477610E-02   2.613204E-02   2.877628E-04   1.562172E-03  -1.994268E-03   2.058315E-04
  a    31   2.876271E-04  -4.241939E-02   1.105466E-02   6.071367E-04   3.491251E-05   6.018666E-04  -3.922421E-04  -3.175729E-05
  a    32   9.711393E-04  -0.365489      -0.162195       5.618206E-02   1.864625E-04   4.163433E-05  -5.431234E-04  -2.224275E-04
  a    33    1.98923       2.960792E-03   5.822891E-04  -4.234369E-04   1.753324E-02  -2.362038E-03   2.089684E-04  -3.453854E-02
  a    34   2.960792E-03   0.131875       3.448328E-02  -1.352159E-02  -5.694219E-05  -1.528456E-04   3.333915E-04  -2.556364E-05
  a    35   5.822891E-04   3.448328E-02   8.937249E-02  -5.731317E-03   8.173069E-06  -4.485880E-05   3.090157E-05  -2.317605E-05
  a    36  -4.234369E-04  -1.352159E-02  -5.731317E-03   1.661205E-02   9.895145E-06   8.661491E-05  -8.015927E-05   1.770833E-05
  a    37   1.753324E-02  -5.694219E-05   8.173069E-06   9.895145E-06   4.863071E-03  -3.843774E-03  -2.028272E-03   5.357379E-04
  a    38  -2.362038E-03  -1.528456E-04  -4.485880E-05   8.661491E-05  -3.843774E-03   5.235084E-03   2.798341E-03  -1.764531E-03
  a    39   2.089684E-04   3.333915E-04   3.090157E-05  -8.015927E-05  -2.028272E-03   2.798341E-03   2.131942E-03  -8.783887E-04
  a    40  -3.453854E-02  -2.556364E-05  -2.317605E-05   1.770833E-05   5.357379E-04  -1.764531E-03  -8.783887E-04   3.062228E-03
  a    41   1.855572E-02   2.956830E-04  -5.897369E-05   1.031025E-05   4.596160E-04  -3.653227E-04  -2.297705E-04  -7.728163E-04
  a    42  -2.016386E-02   2.986441E-04  -1.557863E-04   4.725797E-05   2.466132E-04  -4.729669E-04  -2.531206E-04   8.609892E-04
  a    43  -4.604742E-02   2.173463E-04   1.012862E-05  -2.019286E-05  -6.021551E-04   4.283678E-04   1.994538E-05   1.562907E-03
  a    44  -1.842335E-02  -2.069898E-05   4.327112E-05  -4.267464E-06  -5.696124E-04   1.194251E-03   6.536586E-04  -1.617943E-04
  a    45   2.563374E-02   1.605820E-06  -5.730381E-05   2.121592E-05   6.547652E-04  -7.974730E-04   9.307929E-05  -2.498203E-04
  a    46  -5.086852E-03  -1.691959E-04  -5.744436E-06   2.124012E-05  -4.572662E-04   1.713719E-04   3.744091E-04   7.053602E-04
  a    47  -4.806257E-03  -2.847744E-04  -3.297294E-05   4.094684E-05   6.547279E-04  -1.297329E-03  -5.146068E-04   8.453968E-04
  a    48  -4.621441E-03  -3.376550E-04  -1.077295E-05   5.777354E-05   1.672456E-04  -7.923394E-06   1.846136E-04  -1.203666E-04
  a    49  -1.850607E-02  -1.123034E-04   4.083272E-05   1.173410E-05  -1.460857E-03   1.456944E-03   6.941142E-04   2.048818E-04
  a    50  -3.799164E-02   7.744489E-04   3.890786E-04  -1.302491E-04   6.175410E-04  -1.408918E-03  -9.065646E-04   1.873875E-03
  a    51   2.561898E-03   1.302981E-02   5.965443E-03  -1.917710E-03  -4.155540E-05   8.543845E-05   7.770162E-05  -1.123924E-04
  a    52  -6.797701E-03   5.410819E-05   2.208124E-06  -8.477709E-06   2.751948E-03  -3.165815E-03  -1.870773E-03   1.288754E-03
  a    53  -4.510963E-02  -4.115962E-06  -1.398310E-05   9.718267E-06  -1.609207E-03   1.208946E-03   5.712614E-04   8.995003E-04
  a    54   3.798726E-05  -4.049176E-04   1.852784E-03  -2.567196E-04   4.576240E-05  -1.035907E-05  -4.855987E-05  -1.426866E-05
  a    55   1.775836E-03  -2.508693E-05   1.297523E-04  -1.644909E-05  -3.450227E-04   1.957203E-04   2.273255E-04   2.717955E-04
  a    56   7.997873E-05  -2.330087E-05   2.302545E-05   4.437986E-06   7.543926E-04  -6.068489E-04  -3.787455E-04   1.365744E-04
  a    57  -1.251565E-03   3.332429E-05  -4.065388E-05   1.298161E-06   4.021097E-04  -2.247251E-04  -3.119017E-04   9.302036E-05
  a    58   3.726154E-05   1.564304E-03  -6.029824E-04  -4.718320E-04   8.161164E-06   1.874476E-06   2.633031E-05  -1.323923E-05
  a    59  -8.316823E-06  -7.735688E-04  -1.306056E-03   2.313289E-04   4.834568E-05   1.651424E-06  -8.741400E-06  -1.095924E-05
  a    60   5.734208E-04  -2.625986E-05   1.066913E-04   8.978513E-06   4.798403E-04   3.923516E-05   1.255111E-04  -2.829262E-04
  a    61   7.737124E-04   1.844089E-04  -1.119514E-04  -3.020501E-05  -1.294959E-04   8.998818E-05  -1.113764E-04  -1.355721E-04
  a    62  -4.678387E-07  -1.408468E-03   7.460147E-04   2.758575E-04  -7.943128E-05   2.939079E-05   3.240003E-05  -4.290131E-05
  a    63  -3.482468E-04  -2.711253E-04   1.315907E-04   4.225079E-05   3.773472E-04  -1.853060E-04  -1.948793E-04   8.566204E-05
  a    64  -8.531743E-05  -5.211540E-04   7.947102E-05   6.197743E-05  -3.552313E-05  -3.639134E-05  -5.239669E-05   7.557373E-05
  a    65   6.723951E-04  -7.026412E-05   1.117789E-05   1.036529E-05  -1.644171E-04   3.168150E-04   2.404944E-04  -9.227855E-05
  a    66  -2.388015E-03  -1.043729E-05  -1.790105E-08  -9.958505E-06   6.977135E-05  -1.405207E-04  -9.989002E-05   2.974606E-04
  a    67  -1.545590E-03  -1.348732E-04   2.898261E-05  -1.986398E-06   6.666758E-05  -3.124255E-04  -2.145713E-04  -2.298419E-05
  a    68  -4.347654E-05  -1.602356E-03   4.523477E-04  -6.489277E-05   6.220923E-05   1.679043E-05   3.707182E-05   1.466566E-05
  a    69   1.084817E-04  -1.578857E-03   4.536479E-04  -8.950026E-05  -4.371680E-05   4.287677E-06  -3.478128E-05  -2.381975E-05
  a    70   1.158972E-04  -5.059249E-04   1.299913E-04  -3.535615E-06  -4.916279E-06   4.399488E-05   1.189814E-05  -2.123416E-05
  a    71  -1.107384E-03   2.272884E-04  -4.047243E-05   1.954735E-05  -2.886980E-04   2.020433E-04   1.810539E-04   7.209683E-05
  a    72  -1.330880E-03   8.825353E-05  -1.085982E-05   1.590866E-07  -6.184622E-04   5.819761E-04   4.368458E-04  -9.282919E-06
  a    73  -9.521415E-05  -2.872489E-03   5.520934E-04  -1.034971E-04  -3.971467E-05   4.232377E-05   9.508495E-06  -1.453968E-05
  a    74  -2.769139E-04  -2.235878E-04   5.294510E-05  -2.617851E-06  -1.161885E-04  -6.485075E-05  -6.023300E-05   1.914983E-04
  a    75  -1.963414E-04   5.670026E-05   1.294886E-06   1.101784E-06   2.594042E-05  -1.881770E-04  -1.942755E-04   1.070692E-04
  a    76   5.178776E-05   4.076901E-05  -8.232397E-06  -2.997194E-06  -1.899004E-04   1.858570E-04   3.974090E-05  -9.281099E-05
  a    77  -3.413024E-05  -1.165381E-05   4.696936E-06   8.888253E-07   7.054716E-04  -7.537221E-04  -2.849539E-04   3.272964E-04
  a    78   1.201502E-04   9.973897E-05  -2.977417E-05   2.722023E-06  -2.384987E-05   4.590300E-05  -2.770011E-05  -1.401251E-04
  a    79   1.749203E-04   1.256937E-04  -2.254350E-05  -1.922188E-05  -6.345151E-05   1.061981E-04   1.204097E-04  -1.652449E-05
  a    80   3.920393E-04  -6.868942E-05   1.077178E-05   2.003264E-05  -7.117795E-05   1.814792E-04   1.330978E-04  -1.919085E-04
  a    81   1.209978E-04   4.874330E-06  -2.078198E-06   1.294215E-06   1.175908E-04  -2.545989E-04  -1.400512E-04   1.766483E-04
  a    82  -1.073189E-03   5.149127E-06  -7.407323E-06   3.073654E-06   7.341313E-05  -1.060328E-04  -4.348870E-06   8.513625E-05
  a    83   2.965799E-04  -2.202746E-05  -2.507323E-06   1.256256E-05   2.863364E-05  -2.046791E-05   1.556845E-05   1.457136E-05
  a    84  -4.526238E-04   4.333675E-06   5.170847E-06  -3.183653E-06  -8.637623E-06   1.612743E-05  -4.033243E-05   2.508222E-05
  a    85   4.096392E-04   4.101864E-06   4.659788E-06  -5.309283E-06   6.245302E-05   1.016472E-05   2.301930E-05  -4.922350E-05
  a    86  -3.029190E-05  -1.201926E-05  -5.996844E-06   5.177131E-06   2.496639E-05   5.302531E-05  -1.052994E-05  -1.578379E-05

               a    41        a    42        a    43        a    44        a    45        a    46        a    47        a    48
  a    13  -7.351551E-04   2.817954E-04  -1.830296E-03  -2.745835E-03   9.353081E-04   4.394152E-04   1.742153E-03   1.878985E-04
  a    14   3.467648E-04  -3.010596E-04   1.221879E-03  -8.087810E-04   3.385385E-03   1.553964E-03  -1.968327E-03  -2.976386E-03
  a    15  -9.356956E-04   1.227829E-04  -1.005301E-03   2.233133E-03   1.558087E-03   7.141095E-04  -2.326354E-03   2.219838E-03
  a    16  -1.952291E-03   6.132317E-04   2.017820E-03   6.900954E-03  -2.769933E-03  -1.719373E-03  -7.857478E-03   6.383766E-03
  a    17  -1.324587E-03   3.559681E-03  -2.426924E-03  -4.949802E-03   3.371189E-03  -2.052625E-03   1.865049E-02  -1.153700E-02
  a    18  -9.275322E-03   3.382855E-03   1.014313E-02   5.174689E-03   2.623522E-03  -1.536451E-03   5.834599E-03  -2.258852E-03
  a    19  -2.914900E-03   7.174856E-04  -4.034209E-03  -4.491421E-03   2.076270E-03  -1.187005E-02   1.591493E-02  -4.978721E-03
  a    20  -2.994253E-03   3.502389E-03  -2.686854E-03   3.532085E-04   3.072363E-03  -9.161610E-03   9.964543E-03  -2.936417E-03
  a    21  -3.173710E-03  -1.052109E-02  -1.119185E-02  -2.786645E-03   7.970607E-03  -8.389055E-04  -4.064936E-03  -2.281879E-03
  a    22   4.578145E-04  -5.619042E-04   3.316409E-03  -2.208781E-03  -9.133739E-04  -5.911573E-03   1.372197E-03  -9.206573E-03
  a    23   1.409755E-03   1.059283E-02  -1.226762E-02  -4.515688E-03   5.047109E-03  -3.485848E-03  -3.741800E-03  -4.604300E-03
  a    24   5.385175E-03  -5.232508E-03  -3.484495E-04   5.613120E-03   5.018637E-03  -4.592057E-03   2.297420E-03  -4.703662E-03
  a    25  -1.407143E-02  -1.952346E-03  -1.029870E-02  -3.522239E-03  -3.763288E-03  -4.502552E-03  -3.635331E-03  -7.686636E-03
  a    26  -3.771376E-04  -2.944058E-03   1.266797E-03   7.012409E-03  -1.029294E-02   7.700857E-03  -8.945353E-03  -7.709125E-03
  a    27  -1.621507E-03  -1.800105E-03  -6.522414E-04   2.530997E-03  -1.167983E-03   4.953686E-03   3.745759E-04   3.948023E-03
  a    28  -5.409342E-04   2.149259E-03   3.647734E-03  -1.338559E-02  -2.449437E-03  -9.884427E-03   8.838016E-03  -3.859919E-03
  a    29  -1.190610E-04  -6.267648E-05  -1.290447E-04   1.015191E-05  -1.989057E-05   2.212278E-04   1.600149E-04   2.785065E-04
  a    30  -4.233601E-04  -2.265289E-05  -5.604079E-04  -1.780669E-04   4.366676E-04   8.436155E-04   9.408638E-04   1.563229E-03
  a    31  -3.332878E-05   1.838419E-04  -2.908652E-04   5.601855E-06  -4.652646E-06   3.616232E-04   3.199012E-04   5.847959E-04
  a    32  -7.914271E-04  -1.196620E-03  -8.630652E-04  -7.134042E-05   6.907523E-05   3.770720E-04   6.719046E-04   7.130115E-04
  a    33   1.855572E-02  -2.016386E-02  -4.604742E-02  -1.842335E-02   2.563374E-02  -5.086852E-03  -4.806257E-03  -4.621441E-03
  a    34   2.956830E-04   2.986441E-04   2.173463E-04  -2.069898E-05   1.605820E-06  -1.691959E-04  -2.847744E-04  -3.376550E-04
  a    35  -5.897369E-05  -1.557863E-04   1.012862E-05   4.327112E-05  -5.730381E-05  -5.744436E-06  -3.297294E-05  -1.077295E-05
  a    36   1.031025E-05   4.725797E-05  -2.019286E-05  -4.267464E-06   2.121592E-05   2.124012E-05   4.094684E-05   5.777354E-05
  a    37   4.596160E-04   2.466132E-04  -6.021551E-04  -5.696124E-04   6.547652E-04  -4.572662E-04   6.547279E-04   1.672456E-04
  a    38  -3.653227E-04  -4.729669E-04   4.283678E-04   1.194251E-03  -7.974730E-04   1.713719E-04  -1.297329E-03  -7.923394E-06
  a    39  -2.297705E-04  -2.531206E-04   1.994538E-05   6.536586E-04   9.307929E-05   3.744091E-04  -5.146068E-04   1.846136E-04
  a    40  -7.728163E-04   8.609892E-04   1.562907E-03  -1.617943E-04  -2.498203E-04   7.053602E-04   8.453968E-04  -1.203666E-04
  a    41   1.384390E-03  -6.108297E-04  -1.015954E-03  -1.112791E-04   6.418307E-04   1.668261E-04  -1.732870E-04  -6.835473E-05
  a    42  -6.108297E-04   1.080907E-03   1.091013E-03   5.321274E-04  -8.965777E-04  -6.022449E-05   4.507660E-04   2.047632E-05
  a    43  -1.015954E-03   1.091013E-03   4.073696E-03   1.214372E-03  -1.884941E-03   2.723267E-04   3.976071E-04   5.107251E-04
  a    44  -1.112791E-04   5.321274E-04   1.214372E-03   3.298343E-03  -7.849068E-04  -6.904089E-04   7.189790E-05   4.256023E-04
  a    45   6.418307E-04  -8.965777E-04  -1.884941E-03  -7.849068E-04   2.938882E-03   3.148748E-04  -3.963407E-05  -1.748362E-04
  a    46   1.668261E-04  -6.022449E-05   2.723267E-04  -6.904089E-04   3.148748E-04   2.526769E-03  -4.302609E-04  -2.563271E-04
  a    47  -1.732870E-04   4.507660E-04   3.976071E-04   7.189790E-05  -3.963407E-05  -4.302609E-04   1.792993E-03  -2.360593E-04
  a    48  -6.835473E-05   2.047632E-05   5.107251E-04   4.256023E-04  -1.748362E-04  -2.563271E-04  -2.360593E-04   1.063739E-03
  a    49  -9.171930E-05   4.245881E-05   1.059782E-03   4.927337E-04  -5.066119E-04   3.819555E-04  -2.565634E-04  -2.144470E-04
  a    50  -7.085914E-04   1.125325E-03   1.721497E-03   2.342710E-04  -9.135983E-04   4.980456E-04   5.954597E-04   8.834953E-05
  a    51   8.011261E-05  -2.061843E-05  -8.212607E-05  -1.247584E-05   5.838965E-05  -4.477550E-05  -5.841863E-05  -3.031849E-05
  a    52   1.350882E-04   4.003957E-04   2.712875E-04  -2.616105E-04   5.532414E-07  -5.868583E-04   8.726706E-04   1.020049E-04
  a    53  -1.015400E-03   7.465703E-04   2.288300E-03   9.630135E-04  -1.886560E-03  -6.857769E-05   7.489050E-05   3.820456E-04
  a    54   5.531378E-06   3.837760E-06  -3.128126E-05   1.788465E-05  -3.208614E-05  -1.016440E-05   1.632761E-05   7.802377E-06
  a    55  -1.672057E-04  -1.080539E-04   3.230681E-04  -1.556583E-04   5.421934E-04   2.219005E-04  -1.349390E-05   1.644969E-04
  a    56   3.545845E-04   4.320502E-05  -1.051991E-04   5.224894E-04   9.692887E-05   7.796002E-05  -9.966550E-05  -1.872534E-04
  a    57  -4.327665E-05   1.573806E-04   4.609530E-04   1.798955E-04  -5.083786E-04  -6.766096E-04   8.132710E-05   3.182085E-05
  a    58   1.069389E-05   1.008672E-05  -1.508944E-06   5.181433E-05   9.462124E-07   1.188163E-05   2.067785E-05   1.790312E-06
  a    59   1.735681E-05   2.134198E-05   1.066211E-05   8.250812E-05  -6.919064E-06  -2.129282E-05   2.204038E-05   6.375693E-06
  a    60   1.218931E-04   1.270868E-04   4.428056E-05   9.973031E-04  -1.668054E-04  -3.485796E-04   2.892510E-04   2.936013E-05
  a    61  -1.377167E-04  -1.469766E-04  -1.927710E-04  -4.989521E-04  -6.042094E-05  -3.503552E-04  -2.640212E-04  -1.359653E-06
  a    62  -4.418596E-05  -4.608927E-05  -6.164729E-05  -1.116059E-04  -3.271164E-05  -1.979078E-05  -2.256872E-05   9.757924E-06
  a    63   1.142334E-04   3.575402E-05   2.888716E-04   2.969277E-04  -3.592042E-05  -3.566229E-04  -1.913764E-05   7.855670E-05
  a    64  -4.824750E-05   2.540914E-06  -7.698354E-05  -5.444664E-05   6.475462E-05   1.913031E-04   2.114871E-05  -9.382120E-05
  a    65  -1.046080E-04  -2.351249E-05  -1.336295E-05  -1.564730E-04   6.659532E-05  -7.509653E-05  -2.437313E-04   3.838832E-05
  a    66  -2.486567E-04   1.457016E-04  -5.697608E-05   5.527386E-05  -1.831615E-04  -1.028849E-04  -4.332092E-06  -7.634633E-05
  a    67   1.209949E-04   5.385348E-05   5.801664E-05   1.725350E-04  -1.432444E-04  -1.693523E-04   1.781269E-04   9.849305E-05
  a    68   1.618196E-05   2.750358E-05   2.356885E-05   7.963578E-05   3.743571E-05   1.238543E-05   8.678717E-06   1.619586E-05
  a    69  -3.183153E-05  -3.980672E-05  -2.620403E-05  -6.062485E-05  -3.317228E-05  -2.501523E-05  -3.964843E-06  -5.316860E-06
  a    70  -2.173114E-05  -1.260994E-05   3.896064E-06  -9.778135E-07  -1.041397E-05  -6.591888E-05   2.489307E-06   6.838350E-06
  a    71   2.284854E-05  -3.943819E-05   2.105997E-05   6.531759E-05   3.088239E-05  -4.759707E-05   7.842858E-05   5.391448E-05
  a    72   7.963162E-05  -1.070494E-04   2.760010E-05   2.025315E-04   1.193373E-04   1.550982E-04  -1.529386E-05  -5.999951E-05
  a    73  -1.527115E-05  -3.492526E-05  -1.237274E-05   1.015455E-05  -6.351275E-06  -9.811071E-06   2.014604E-05   2.617666E-05
  a    74   5.426695E-05  -1.137432E-05  -3.267662E-05  -1.575244E-04   1.043520E-04   4.201479E-04  -1.004013E-04  -1.099151E-04
  a    75   4.893781E-05  -3.794831E-06  -5.917059E-05  -2.204038E-04  -7.452917E-05   2.634053E-04  -9.619389E-05  -8.875198E-05
  a    76  -1.172384E-04   5.536086E-05  -5.099847E-05  -3.534729E-05  -1.903762E-04   3.955419E-05  -4.775940E-05  -2.841766E-05
  a    77   6.570058E-05   6.380909E-05  -6.865728E-05  -1.660826E-04   2.304193E-04   8.491130E-05   1.671967E-04   5.126746E-05
  a    78   4.838524E-05  -3.291242E-05   2.322173E-05  -3.471127E-05  -1.674561E-04  -2.593100E-04  -3.688546E-05   6.642947E-05
  a    79   2.015771E-05   3.480694E-05   5.285402E-05   1.594229E-04   1.206942E-04   1.161210E-04   2.123088E-05  -3.844897E-05
  a    80  -7.175477E-07  -2.999358E-05   2.511200E-05   1.544543E-04  -5.502983E-05  -9.821048E-05   1.029017E-05   5.452246E-05
  a    81   1.406221E-05   8.406315E-07  -5.359677E-05  -1.495001E-04   1.125526E-04   1.091085E-04  -8.477269E-06   3.036461E-05
  a    82  -7.879933E-05   6.867738E-05   4.923618E-05   1.094951E-04  -5.567527E-05  -4.056194E-05   1.213553E-04   5.384326E-05
  a    83  -1.653955E-05   1.517441E-05  -2.896683E-05   4.903198E-05   9.201179E-05   3.723132E-05   4.896998E-05  -1.924485E-05
  a    84  -2.940242E-05   3.434657E-05   7.653311E-05   9.684578E-05  -7.684868E-05  -1.784927E-05  -1.877247E-05   1.064133E-05
  a    85   2.087481E-05  -2.366527E-05  -2.635090E-05  -1.157161E-05   4.040789E-05   2.612720E-05   4.080091E-06   1.016879E-05
  a    86  -1.267087E-05   2.809207E-05   7.855569E-05   1.048973E-04  -6.655573E-05   3.956761E-05  -3.881099E-05  -9.496364E-06

               a    49        a    50        a    51        a    52        a    53        a    54        a    55        a    56
  a    13  -6.351568E-04   6.453546E-04  -3.516873E-05  -1.698695E-04  -3.315537E-04  -4.857649E-05   6.227241E-04  -2.008560E-03
  a    14   7.702302E-04   5.116834E-04  -3.456872E-05   2.562665E-04  -1.921392E-03  -1.391564E-05   1.282834E-03  -6.022878E-04
  a    15   1.439343E-04   9.852774E-04  -6.138442E-05  -3.526889E-03  -1.880661E-03  -4.129867E-05   1.477996E-03   4.255477E-04
  a    16  -4.448931E-03  -1.164366E-03   5.501421E-05  -7.201902E-03   3.774980E-03  -1.015625E-04   1.426346E-03  -3.973598E-03
  a    17   4.282020E-03   4.003665E-03  -2.015304E-04   1.134032E-02  -5.558568E-03   2.228261E-04  -1.918297E-03   4.970707E-03
  a    18  -1.039809E-03  -1.474913E-03   8.280821E-05   3.825621E-03   4.468287E-03  -2.985968E-04   3.075591E-03   2.327830E-03
  a    19   3.835951E-03  -3.174423E-04   8.228543E-05   6.354362E-03  -1.467123E-03   1.219846E-04  -6.261480E-04   7.564139E-04
  a    20  -1.344330E-03   2.357331E-03  -1.083910E-04  -1.237883E-03  -1.046187E-03  -2.478796E-04   5.779920E-03  -3.047651E-03
  a    21  -2.930872E-03  -4.003319E-03   2.299304E-04   8.624789E-04  -1.427710E-03   3.527461E-04  -6.524358E-03  -4.627540E-03
  a    22   3.166274E-03  -2.582485E-03   1.050359E-04  -2.171758E-03   9.712380E-04   6.215351E-05  -2.400277E-03  -7.473260E-03
  a    23  -2.258030E-03  -2.023733E-03   5.831698E-05  -6.674851E-04  -1.694186E-03   2.645258E-04  -5.266474E-03  -4.152006E-03
  a    24   5.107162E-03  -7.667746E-03   4.618121E-04  -9.929466E-03   4.821782E-03  -3.198133E-04   4.426042E-03  -4.587051E-03
  a    25  -2.927578E-03   1.409114E-03  -1.295945E-04  -1.605487E-03  -1.291324E-04   4.378908E-05   1.797787E-03  -3.004597E-03
  a    26   1.806320E-02  -1.631872E-02   1.062317E-03  -3.737655E-02   1.145891E-02  -3.486804E-04   1.911930E-04  -3.916160E-03
  a    27   3.855003E-03  -2.890869E-03  -2.275280E-04  -6.430123E-03   2.433567E-03   1.560134E-03   8.978684E-04   1.944232E-03
  a    28  -1.010429E-02   4.759213E-03  -3.691329E-04   1.062345E-02  -3.766458E-03   4.103146E-04  -3.037435E-03  -3.485560E-03
  a    29   6.453259E-05   8.141201E-05   1.894086E-03  -8.807650E-05   4.262708E-05  -2.915982E-04   1.251728E-05   1.203864E-04
  a    30   2.463738E-04  -6.385122E-04  -1.923120E-03  -2.106893E-04   3.389510E-04   5.686495E-03   2.451456E-04   1.413366E-04
  a    31   4.326198E-05   3.175514E-04   8.215071E-03  -1.489343E-04   1.800785E-04   1.638996E-03  -3.120796E-05  -4.065781E-05
  a    32   1.478120E-04  -2.769737E-03  -4.404925E-02  -2.521981E-04  -4.059024E-04  -3.940911E-03  -1.476922E-04   5.850128E-05
  a    33  -1.850607E-02  -3.799164E-02   2.561898E-03  -6.797701E-03  -4.510963E-02   3.798726E-05   1.775836E-03   7.997873E-05
  a    34  -1.123034E-04   7.744489E-04   1.302981E-02   5.410819E-05  -4.115962E-06  -4.049176E-04  -2.508693E-05  -2.330087E-05
  a    35   4.083272E-05   3.890786E-04   5.965443E-03   2.208124E-06  -1.398310E-05   1.852784E-03   1.297523E-04   2.302545E-05
  a    36   1.173410E-05  -1.302491E-04  -1.917710E-03  -8.477709E-06   9.718267E-06  -2.567196E-04  -1.644909E-05   4.437986E-06
  a    37  -1.460857E-03   6.175410E-04  -4.155540E-05   2.751948E-03  -1.609207E-03   4.576240E-05  -3.450227E-04   7.543926E-04
  a    38   1.456944E-03  -1.408918E-03   8.543845E-05  -3.165815E-03   1.208946E-03  -1.035907E-05   1.957203E-04  -6.068489E-04
  a    39   6.941142E-04  -9.065646E-04   7.770162E-05  -1.870773E-03   5.712614E-04  -4.855987E-05   2.273255E-04  -3.787455E-04
  a    40   2.048818E-04   1.873875E-03  -1.123924E-04   1.288754E-03   8.995003E-04  -1.426866E-05   2.717955E-04   1.365744E-04
  a    41  -9.171930E-05  -7.085914E-04   8.011261E-05   1.350882E-04  -1.015400E-03   5.531378E-06  -1.672057E-04   3.545845E-04
  a    42   4.245881E-05   1.125325E-03  -2.061843E-05   4.003957E-04   7.465703E-04   3.837760E-06  -1.080539E-04   4.320502E-05
  a    43   1.059782E-03   1.721497E-03  -8.212607E-05   2.712875E-04   2.288300E-03  -3.128126E-05   3.230681E-04  -1.051991E-04
  a    44   4.927337E-04   2.342710E-04  -1.247584E-05  -2.616105E-04   9.630135E-04   1.788465E-05  -1.556583E-04   5.224894E-04
  a    45  -5.066119E-04  -9.135983E-04   5.838965E-05   5.532414E-07  -1.886560E-03  -3.208614E-05   5.421934E-04   9.692887E-05
  a    46   3.819555E-04   4.980456E-04  -4.477550E-05  -5.868583E-04  -6.857769E-05  -1.016440E-05   2.219005E-04   7.796002E-05
  a    47  -2.565634E-04   5.954597E-04  -5.841863E-05   8.726706E-04   7.489050E-05   1.632761E-05  -1.349390E-05  -9.966550E-05
  a    48  -2.144470E-04   8.834953E-05  -3.031849E-05   1.020049E-04   3.820456E-04   7.802377E-06   1.644969E-04  -1.872534E-04
  a    49   1.588964E-03   1.083705E-05  -1.004290E-05  -6.912519E-04   8.842889E-04  -7.082086E-06   1.744642E-04   1.333787E-04
  a    50   1.083705E-05   2.315211E-03  -1.736156E-05   1.015121E-03   1.025111E-03   1.277684E-05  -3.268235E-06   1.147465E-04
  a    51  -1.004290E-05  -1.736156E-05   2.144414E-03  -5.518093E-05  -5.376587E-05   2.187411E-04   9.295579E-06  -9.803021E-06
  a    52  -6.912519E-04   1.015121E-03  -5.518093E-05   2.781468E-03  -3.131679E-04   2.887026E-05  -3.420437E-04   5.918209E-04
  a    53   8.842889E-04   1.025111E-03  -5.376587E-05  -3.131679E-04   2.844944E-03  -2.469469E-06  -1.736701E-04  -2.484999E-04
  a    54  -7.082086E-06   1.277684E-05   2.187411E-04   2.887026E-05  -2.469469E-06   5.971775E-04  -1.653939E-05   2.602271E-05
  a    55   1.744642E-04  -3.268235E-06   9.295579E-06  -3.420437E-04  -1.736701E-04  -1.653939E-05   7.940531E-04  -1.840620E-04
  a    56   1.333787E-04   1.147465E-04  -9.803021E-06   5.918209E-04  -2.484999E-04   2.602271E-05  -1.840620E-04   1.054286E-03
  a    57   9.669376E-05   5.589166E-05  -1.547409E-07   4.618844E-04   3.858195E-07   2.089836E-05  -1.687462E-04   7.974511E-05
  a    58  -8.215141E-06   1.486792E-05   1.718540E-04  -8.068833E-06  -7.583685E-06   4.604782E-05  -9.315333E-06   3.504580E-06
  a    59  -3.189080E-06  -7.684548E-06  -2.020672E-04   2.072811E-05  -1.675314E-05  -1.999334E-04  -2.703033E-05   2.812305E-05
  a    60  -1.534619E-04  -6.123004E-05   7.031898E-06   2.088761E-04  -1.005307E-04   3.370024E-05  -3.332774E-04   8.479943E-05
  a    61  -1.220942E-04  -1.969065E-04   4.351527E-05  -2.009037E-05   2.270028E-04   4.842300E-06   9.046301E-06  -1.256983E-04
  a    62  -3.565803E-05  -5.109179E-05  -2.470999E-04  -5.052399E-05   6.515145E-05  -7.184392E-05  -2.269642E-05  -6.161040E-05
  a    63   1.959015E-04   3.673292E-05  -4.186303E-05   3.456678E-04  -1.327229E-04  -3.540718E-07   1.124680E-04   2.908444E-04
  a    64  -8.047202E-05   7.053720E-05  -7.096174E-05  -4.044728E-05  -4.104307E-05   7.989785E-06  -2.749866E-05  -5.555753E-05
  a    65   2.861817E-04  -2.163711E-04   4.628393E-06  -1.244873E-04  -2.912909E-06  -4.858055E-06   1.201398E-04   4.958252E-05
  a    66  -4.791597E-05   8.094414E-05  -6.004288E-06   2.082987E-04   1.460912E-04   7.244046E-06  -1.203567E-04   9.993892E-05
  a    67  -8.559581E-05   8.150040E-05  -1.920917E-05   2.693617E-04   8.721274E-05   5.001659E-06  -7.511345E-05   1.821182E-05
  a    68   6.917736E-06  -1.677787E-05  -1.823351E-04   8.030497E-06  -6.659921E-06  -4.140003E-05  -2.922490E-06   2.302315E-05
  a    69  -3.222052E-06  -1.603923E-05  -1.805974E-04  -1.234450E-05   4.664539E-06  -8.000452E-05  -6.327707E-06  -2.399612E-05
  a    70   9.187099E-07  -4.361839E-05  -5.270301E-05  -5.192588E-07   2.196058E-05   4.058861E-05  -3.137646E-06  -1.889204E-05
  a    71   9.030060E-05  -6.654074E-05   3.169688E-05  -9.984907E-05   1.008865E-04  -1.790932E-05   4.943245E-05  -5.054034E-05
  a    72   2.638983E-04  -2.166281E-04   2.411889E-05  -2.860133E-04   1.045555E-04  -1.143797E-05   3.807719E-05  -9.051814E-05
  a    73   2.036147E-05  -4.143000E-05  -3.614369E-04  -2.071306E-05   1.084974E-05   1.816918E-04   1.418045E-05  -6.066871E-06
  a    74   8.241691E-05   1.596905E-04  -3.601709E-05  -1.029292E-04  -1.341775E-04   1.478819E-06   8.525088E-05   8.424913E-05
  a    75   2.141663E-05   9.224326E-05  -1.439236E-06   3.691858E-05  -4.254629E-05   1.218466E-06  -6.762634E-06   8.386144E-05
  a    76  -5.366129E-05   4.193137E-05   2.025163E-06  -2.207089E-04   9.581555E-05   3.610803E-06  -3.465587E-05  -7.195133E-05
  a    77  -2.161423E-04   1.785895E-04  -1.369143E-05   4.932377E-04  -1.969627E-04   2.666384E-06  -8.968538E-06   9.813137E-05
  a    78   5.562572E-05  -1.113178E-04   2.051404E-05   6.874795E-05   7.365722E-05  -2.631087E-06  -3.121114E-05  -1.044933E-05
  a    79   5.794631E-05  -3.645092E-06   1.382893E-05  -1.020896E-04  -5.240901E-05  -4.278012E-06   3.592852E-05   6.921693E-06
  a    80   5.492955E-05  -1.320801E-04   4.861578E-07  -1.082079E-04   8.282216E-05  -5.452720E-08  -1.609393E-05  -3.133140E-05
  a    81  -5.121037E-05   1.060355E-04  -6.540460E-06   1.188225E-04  -1.259370E-04  -3.037992E-06   5.008069E-05   1.946383E-06
  a    82  -1.247905E-04   3.990505E-05  -2.288388E-06   8.886351E-05   7.269793E-05  -1.948961E-07  -2.474193E-05  -4.381270E-05
  a    83  -3.679006E-05   4.478509E-05  -4.170336E-06   1.588720E-05  -5.962520E-05  -1.075563E-06   2.011874E-05  -1.726701E-06
  a    84   3.765420E-05   3.388040E-05  -1.689118E-06   2.219027E-05  -6.722334E-06   9.932749E-07  -9.514447E-07   1.503273E-05
  a    85  -1.750885E-06  -1.458710E-05   1.084613E-06   1.940367E-06  -7.738974E-06  -8.713604E-07   4.247279E-06   4.877230E-06
  a    86   5.792285E-05   2.770362E-05  -3.554714E-06  -1.490725E-05  -2.838321E-05   2.808949E-07   1.165935E-05   2.225217E-05

               a    57        a    58        a    59        a    60        a    61        a    62        a    63        a    64
  a    13   2.827236E-04  -1.749328E-05  -1.184778E-04  -1.271195E-03  -5.240887E-04   1.345814E-04  -9.016392E-04   1.901754E-04
  a    14   6.469896E-04   1.273690E-05   1.171696E-04   1.037136E-03  -9.442152E-04  -6.598075E-04   2.796103E-03   2.628566E-04
  a    15   2.472026E-03   8.396367E-06   9.563607E-05   7.491480E-04  -1.561405E-03  -2.584659E-04   2.337014E-04  -5.503944E-04
  a    16  -2.741864E-03   8.532373E-05  -1.359537E-04  -1.679740E-03  -2.052192E-04   2.761794E-04  -9.180292E-04   2.926457E-06
  a    17   2.142543E-04   1.937979E-04   2.029587E-04   9.115443E-04  -4.487311E-03  -8.098953E-04   1.609929E-03   5.243017E-04
  a    18  -1.083413E-03  -3.742494E-04  -3.020930E-04  -3.232199E-03   2.117566E-03  -1.585755E-04   1.434794E-03   4.855445E-04
  a    19   2.945153E-03  -1.775136E-06   7.490562E-05   5.338005E-04   2.120645E-03   8.403505E-04  -2.550518E-03  -8.749431E-04
  a    20   2.349868E-03  -1.361928E-04  -2.308814E-04  -4.758394E-03  -1.864908E-03  -1.411460E-04   2.997957E-04  -4.484198E-05
  a    21   3.314205E-03  -3.388242E-05   4.366021E-05   4.838361E-03   3.415607E-03   8.190275E-04  -4.961031E-03   2.196646E-03
  a    22  -6.061216E-03   1.375796E-04   1.155202E-05   1.882223E-03   1.327886E-03  -5.047887E-04   2.713016E-03   1.333852E-03
  a    23   1.807537E-03  -9.135507E-05  -3.091707E-04   1.009725E-03   2.602282E-03   8.298121E-04  -2.505670E-03   7.885264E-04
  a    24   2.929472E-03  -4.208289E-04  -3.079835E-04  -2.208412E-03   5.564406E-03   8.181356E-04  -7.875038E-04  -6.180486E-05
  a    25  -8.781037E-04  -8.151513E-05   1.486973E-05  -3.050690E-03   3.387407E-04  -7.321695E-04   4.342088E-03   6.606448E-04
  a    26  -3.876536E-03   6.787223E-05  -3.367081E-04  -2.508680E-04  -1.273135E-03   5.456263E-04  -4.836898E-03  -4.928225E-04
  a    27   4.636757E-04   1.295979E-03   1.988146E-03  -1.505057E-04  -1.210282E-03   5.161174E-04  -2.505491E-04   2.067890E-03
  a    28   2.664354E-03  -5.365587E-05   2.133655E-05   4.956702E-04   5.062165E-03   1.299131E-03  -4.168973E-03   1.116562E-03
  a    29   8.704944E-05   1.584719E-04  -7.207535E-04   8.289226E-05  -1.398997E-04   1.266451E-03   2.404125E-04   4.728429E-04
  a    30   3.001307E-04   4.008307E-03   5.028856E-03  -5.712569E-04   4.693739E-04  -1.152828E-03  -6.814823E-05   1.228083E-03
  a    31  -2.905046E-04  -1.144658E-02   3.931626E-03   2.429588E-04  -7.197995E-04   3.505709E-03   4.270840E-04   3.280859E-04
  a    32  -1.102317E-04  -4.517481E-03   3.987595E-03  -1.635006E-05  -7.191959E-04   5.372295E-03   8.854212E-04   1.481569E-03
  a    33  -1.251565E-03   3.726154E-05  -8.316823E-06   5.734208E-04   7.737124E-04  -4.678387E-07  -3.482468E-04  -8.531743E-05
  a    34   3.332429E-05   1.564304E-03  -7.735688E-04  -2.625986E-05   1.844089E-04  -1.408468E-03  -2.711253E-04  -5.211540E-04
  a    35  -4.065388E-05  -6.029824E-04  -1.306056E-03   1.066913E-04  -1.119514E-04   7.460147E-04   1.315907E-04   7.947102E-05
  a    36   1.298161E-06  -4.718320E-04   2.313289E-04   8.978513E-06  -3.020501E-05   2.758575E-04   4.225079E-05   6.197743E-05
  a    37   4.021097E-04   8.161164E-06   4.834568E-05   4.798403E-04  -1.294959E-04  -7.943128E-05   3.773472E-04  -3.552313E-05
  a    38  -2.247251E-04   1.874476E-06   1.651424E-06   3.923516E-05   8.998818E-05   2.939079E-05  -1.853060E-04  -3.639134E-05
  a    39  -3.119017E-04   2.633031E-05  -8.741400E-06   1.255111E-04  -1.113764E-04   3.240003E-05  -1.948793E-04  -5.239669E-05
  a    40   9.302036E-05  -1.323923E-05  -1.095924E-05  -2.829262E-04  -1.355721E-04  -4.290131E-05   8.566204E-05   7.557373E-05
  a    41  -4.327665E-05   1.069389E-05   1.735681E-05   1.218931E-04  -1.377167E-04  -4.418596E-05   1.142334E-04  -4.824750E-05
  a    42   1.573806E-04   1.008672E-05   2.134198E-05   1.270868E-04  -1.469766E-04  -4.608927E-05   3.575402E-05   2.540914E-06
  a    43   4.609530E-04  -1.508944E-06   1.066211E-05   4.428056E-05  -1.927710E-04  -6.164729E-05   2.888716E-04  -7.698354E-05
  a    44   1.798955E-04   5.181433E-05   8.250812E-05   9.973031E-04  -4.989521E-04  -1.116059E-04   2.969277E-04  -5.444664E-05
  a    45  -5.083786E-04   9.462124E-07  -6.919064E-06  -1.668054E-04  -6.042094E-05  -3.271164E-05  -3.592042E-05   6.475462E-05
  a    46  -6.766096E-04   1.188163E-05  -2.129282E-05  -3.485796E-04  -3.503552E-04  -1.979078E-05  -3.566229E-04   1.913031E-04
  a    47   8.132710E-05   2.067785E-05   2.204038E-05   2.892510E-04  -2.640212E-04  -2.256872E-05  -1.913764E-05   2.114871E-05
  a    48   3.182085E-05   1.790312E-06   6.375693E-06   2.936013E-05  -1.359653E-06   9.757924E-06   7.855670E-05  -9.382120E-05
  a    49   9.669376E-05  -8.215141E-06  -3.189080E-06  -1.534619E-04  -1.220942E-04  -3.565803E-05   1.959015E-04  -8.047202E-05
  a    50   5.589166E-05   1.486792E-05  -7.684548E-06  -6.123004E-05  -1.969065E-04  -5.109179E-05   3.673292E-05   7.053720E-05
  a    51  -1.547409E-07   1.718540E-04  -2.020672E-04   7.031898E-06   4.351527E-05  -2.470999E-04  -4.186303E-05  -7.096174E-05
  a    52   4.618844E-04  -8.068833E-06   2.072811E-05   2.088761E-04  -2.009037E-05  -5.052399E-05   3.456678E-04  -4.044728E-05
  a    53   3.858195E-07  -7.583685E-06  -1.675314E-05  -1.005307E-04   2.270028E-04   6.515145E-05  -1.327229E-04  -4.104307E-05
  a    54   2.089836E-05   4.604782E-05  -1.999334E-04   3.370024E-05   4.842300E-06  -7.184392E-05  -3.540718E-07   7.989785E-06
  a    55  -1.687462E-04  -9.315333E-06  -2.703033E-05  -3.332774E-04   9.046301E-06  -2.269642E-05   1.124680E-04  -2.749866E-05
  a    56   7.974511E-05   3.504580E-06   2.812305E-05   8.479943E-05  -1.256983E-04  -6.161040E-05   2.908444E-04  -5.555753E-05
  a    57   8.481036E-04   1.324303E-06   2.771297E-05   2.816565E-04  -3.195688E-05  -3.888422E-05   3.092885E-04  -1.066644E-04
  a    58   1.324303E-06   3.785793E-04  -6.935181E-05   2.891420E-05  -3.089141E-06  -1.348464E-04  -1.558649E-05  -7.255289E-06
  a    59   2.771297E-05  -6.935181E-05   3.401270E-04   4.705083E-05  -3.745594E-05   4.176326E-05   2.371913E-05   5.010834E-06
  a    60   2.816565E-04   2.891420E-05   4.705083E-05   9.243073E-04  -2.764625E-04  -4.988300E-05   9.477440E-05  -1.135077E-05
  a    61  -3.195688E-05  -3.089141E-06  -3.745594E-05  -2.764625E-04   5.347704E-04   6.504856E-05  -1.641162E-04   4.113404E-07
  a    62  -3.888422E-05  -1.348464E-04   4.176326E-05  -4.988300E-05   6.504856E-05   2.202313E-04  -5.760381E-05   3.597905E-05
  a    63   3.092885E-04  -1.558649E-05   2.371913E-05   9.477440E-05  -1.641162E-04  -5.760381E-05   5.197085E-04  -1.278109E-04
  a    64  -1.066644E-04  -7.255289E-06   5.010834E-06  -1.135077E-05   4.113404E-07   3.597905E-05  -1.278109E-04   7.740936E-05
  a    65   1.127027E-04  -1.133067E-05  -3.460386E-06  -1.393281E-04   1.423779E-05  -1.274830E-05   1.738420E-04  -8.197411E-05
  a    66   5.981203E-05  -4.439720E-06  -2.614912E-06   1.824387E-07   8.704960E-05   1.549703E-05  -3.257406E-05   1.996502E-05
  a    67   2.750624E-05   2.887921E-06   1.096508E-05   1.195368E-04  -1.764278E-05  -5.142924E-06   4.085024E-05  -1.334646E-05
  a    68   8.528730E-06   8.124930E-06   9.019170E-05   4.635023E-05  -4.029037E-05   1.345132E-05   2.970919E-05   6.813329E-06
  a    69   3.129775E-06   1.027002E-05   1.241318E-04  -4.734019E-05   3.219932E-05   3.017741E-05  -1.700690E-05   1.886303E-05
  a    70   1.945512E-05  -1.995029E-05  -2.435621E-05   5.349086E-06   1.799717E-05  -5.336675E-06   5.265354E-06  -4.470702E-06
  a    71  -3.869411E-05   1.827563E-06   8.000860E-06  -6.533772E-06  -2.715086E-05  -8.271199E-06   1.674422E-05  -2.596415E-05
  a    72  -6.158415E-05   8.678644E-06   6.020203E-06   9.432143E-05  -1.176515E-04  -1.747845E-05  -2.512737E-06  -6.258559E-06
  a    73   3.767391E-06   2.013542E-05  -1.147143E-04   9.351020E-06  -5.386902E-06   7.186484E-05   2.082186E-05   3.061605E-05
  a    74  -1.378527E-04  -4.998575E-06  -5.547085E-06  -1.448536E-04  -3.149657E-05  -1.189161E-06  -4.863416E-05   4.194594E-05
  a    75  -8.005318E-05  -3.062851E-06  -9.044561E-06  -1.065872E-04   7.339617E-05   1.286857E-05  -7.035090E-05   3.310707E-05
  a    76  -3.719787E-05  -2.113764E-07  -7.494670E-06  -7.005045E-05   5.437225E-05   1.994816E-05  -1.083742E-04   2.482434E-05
  a    77  -2.530996E-05   2.850172E-06   2.476093E-06   2.764903E-05  -6.662178E-05  -1.645611E-05   4.117764E-05   1.086639E-05
  a    78   1.249019E-04  -6.323648E-06   3.659697E-06   1.334733E-05   9.308259E-05   7.185587E-06   5.810752E-05  -4.662743E-05
  a    79  -5.550930E-05   8.401134E-06   3.145663E-06   5.057018E-05  -7.887456E-05  -1.578126E-05  -1.257362E-05   1.571631E-05
  a    80   8.984274E-06   1.814496E-06   5.757266E-06   9.635714E-05  -9.120259E-07   4.031597E-06  -2.236049E-06  -1.158693E-05
  a    81  -3.896519E-05  -1.862238E-06  -3.503390E-06  -6.997953E-05  -8.087903E-06  -2.908373E-06  -3.468326E-06   1.499165E-05
  a    82   1.193759E-05   3.687783E-06   2.885339E-06   7.588335E-05  -2.520769E-05   7.917875E-07  -2.231708E-05   7.383437E-06
  a    83  -2.941423E-05   8.102291E-07   2.332534E-06   3.015460E-05  -1.486843E-05  -1.017749E-07  -3.428029E-05   1.760174E-05
  a    84   4.412172E-05  -9.191187E-07   5.677112E-07   5.838722E-06  -1.677296E-05  -5.359690E-06   7.694570E-06   4.091015E-06
  a    85  -2.543419E-05   1.165214E-06   8.587311E-07   1.433256E-05  -1.286958E-06  -2.020775E-06   5.486053E-06  -4.685314E-06
  a    86   1.502003E-05   8.015231E-08   1.696514E-06  -2.474856E-07  -1.931678E-05  -2.166964E-06  -1.033641E-05   1.240151E-05

               a    65        a    66        a    67        a    68        a    69        a    70        a    71        a    72
  a    13  -1.356759E-03   1.284054E-04  -1.686236E-04  -6.573809E-05   1.024966E-04   1.732027E-04  -8.208726E-04  -4.091465E-03
  a    14   3.615748E-04  -7.027070E-04  -7.294343E-04   7.886018E-04  -6.890219E-04  -6.308339E-04  -2.232331E-03  -4.469569E-04
  a    15   1.816313E-03  -2.194039E-03  -1.614325E-03  -1.134479E-04   2.175394E-04   6.182324E-04   1.277196E-03   1.310568E-03
  a    16   7.837982E-04  -5.159930E-04  -1.235576E-03  -4.249170E-05   8.991005E-05   7.944131E-05   1.395049E-03   3.305769E-03
  a    17  -2.313365E-03   2.848219E-03  -4.806070E-05   3.674600E-04  -3.294777E-04  -6.848029E-05   1.698086E-04  -1.698088E-03
  a    18   1.659116E-03   1.870633E-03  -1.410270E-04   1.049503E-04  -3.959709E-05   1.019362E-04  -5.561455E-04  -2.153736E-04
  a    19   1.053881E-03  -5.738404E-04  -1.181297E-03  -4.629991E-04   4.292860E-04   2.290697E-04  -1.065138E-04  -1.143810E-03
  a    20   3.418794E-04   5.058562E-04   2.437802E-03  -1.032181E-03   6.744661E-04  -1.154088E-05   2.515182E-03   2.284415E-03
  a    21  -1.415049E-03   1.464550E-03   6.586990E-04  -1.585063E-03   1.114029E-03   1.625465E-04  -1.238518E-03   2.309365E-04
  a    22  -1.329599E-03  -2.951582E-03   1.707432E-03   1.921598E-05   1.081388E-04   4.535792E-05  -9.636362E-04   1.448998E-03
  a    23   1.191505E-03   2.904104E-03  -1.115521E-03   1.956364E-03  -1.240187E-03  -2.113385E-04  -8.465063E-04   1.153025E-03
  a    24  -2.608795E-04  -3.768521E-06  -4.468340E-04   3.255950E-04  -1.450916E-04  -1.206904E-05   1.648719E-03   3.096227E-03
  a    25   2.849137E-03   3.289309E-03  -3.264084E-03  -1.271106E-03   1.115680E-03   8.763080E-04  -3.923676E-04   2.117101E-03
  a    26   3.676630E-03  -2.605073E-03  -3.969951E-03  -3.326495E-04   2.762515E-04   7.013413E-04   2.863319E-03   7.001166E-03
  a    27   1.088480E-03  -5.241522E-04  -9.922050E-04  -2.758089E-04   7.096762E-04  -1.799017E-03   9.181272E-04   1.463615E-04
  a    28  -3.618758E-04  -2.887999E-03   7.913358E-04  -1.334467E-03   1.084886E-03   2.563738E-04   2.127992E-03   1.599792E-03
  a    29   7.856168E-05  -1.782272E-05   3.860778E-05   5.299107E-04   4.740957E-04   2.542145E-04  -5.196609E-05  -6.241322E-05
  a    30   2.427637E-04  -8.842261E-05   3.561123E-04   1.410893E-03   6.453526E-04   2.103837E-03  -2.195312E-04  -9.267353E-05
  a    31   4.762125E-05  -1.921421E-05  -1.146615E-04  -3.014908E-03  -3.703165E-03   1.329131E-05   6.961511E-05   1.351559E-05
  a    32   1.889503E-04   1.491026E-06   2.568553E-04   3.046620E-03   2.755982E-03   1.192527E-03  -6.088952E-04  -2.530577E-04
  a    33   6.723951E-04  -2.388015E-03  -1.545590E-03  -4.347654E-05   1.084817E-04   1.158972E-04  -1.107384E-03  -1.330880E-03
  a    34  -7.026412E-05  -1.043729E-05  -1.348732E-04  -1.602356E-03  -1.578857E-03  -5.059249E-04   2.272884E-04   8.825353E-05
  a    35   1.117789E-05  -1.790105E-08   2.898261E-05   4.523477E-04   4.536479E-04   1.299913E-04  -4.047243E-05  -1.085982E-05
  a    36   1.036529E-05  -9.958505E-06  -1.986398E-06  -6.489277E-05  -8.950026E-05  -3.535615E-06   1.954735E-05   1.590866E-07
  a    37  -1.644171E-04   6.977135E-05   6.666758E-05   6.220923E-05  -4.371680E-05  -4.916279E-06  -2.886980E-04  -6.184622E-04
  a    38   3.168150E-04  -1.405207E-04  -3.124255E-04   1.679043E-05   4.287677E-06   4.399488E-05   2.020433E-04   5.819761E-04
  a    39   2.404944E-04  -9.989002E-05  -2.145713E-04   3.707182E-05  -3.478128E-05   1.189814E-05   1.810539E-04   4.368458E-04
  a    40  -9.227855E-05   2.974606E-04  -2.298419E-05   1.466566E-05  -2.381975E-05  -2.123416E-05   7.209683E-05  -9.282919E-06
  a    41  -1.046080E-04  -2.486567E-04   1.209949E-04   1.618196E-05  -3.183153E-05  -2.173114E-05   2.284854E-05   7.963162E-05
  a    42  -2.351249E-05   1.457016E-04   5.385348E-05   2.750358E-05  -3.980672E-05  -1.260994E-05  -3.943819E-05  -1.070494E-04
  a    43  -1.336295E-05  -5.697608E-05   5.801664E-05   2.356885E-05  -2.620403E-05   3.896064E-06   2.105997E-05   2.760010E-05
  a    44  -1.564730E-04   5.527386E-05   1.725350E-04   7.963578E-05  -6.062485E-05  -9.778135E-07   6.531759E-05   2.025315E-04
  a    45   6.659532E-05  -1.831615E-04  -1.432444E-04   3.743571E-05  -3.317228E-05  -1.041397E-05   3.088239E-05   1.193373E-04
  a    46  -7.509653E-05  -1.028849E-04  -1.693523E-04   1.238543E-05  -2.501523E-05  -6.591888E-05  -4.759707E-05   1.550982E-04
  a    47  -2.437313E-04  -4.332092E-06   1.781269E-04   8.678717E-06  -3.964843E-06   2.489307E-06   7.842858E-05  -1.529386E-05
  a    48   3.838832E-05  -7.634633E-05   9.849305E-05   1.619586E-05  -5.316860E-06   6.838350E-06   5.391448E-05  -5.999951E-05
  a    49   2.861817E-04  -4.791597E-05  -8.559581E-05   6.917736E-06  -3.222052E-06   9.187099E-07   9.030060E-05   2.638983E-04
  a    50  -2.163711E-04   8.094414E-05   8.150040E-05  -1.677787E-05  -1.603923E-05  -4.361839E-05  -6.654074E-05  -2.166281E-04
  a    51   4.628393E-06  -6.004288E-06  -1.920917E-05  -1.823351E-04  -1.805974E-04  -5.270301E-05   3.169688E-05   2.411889E-05
  a    52  -1.244873E-04   2.082987E-04   2.693617E-04   8.030497E-06  -1.234450E-05  -5.192588E-07  -9.984907E-05  -2.860133E-04
  a    53  -2.912909E-06   1.460912E-04   8.721274E-05  -6.659921E-06   4.664539E-06   2.196058E-05   1.008865E-04   1.045555E-04
  a    54  -4.858055E-06   7.244046E-06   5.001659E-06  -4.140003E-05  -8.000452E-05   4.058861E-05  -1.790932E-05  -1.143797E-05
  a    55   1.201398E-04  -1.203567E-04  -7.511345E-05  -2.922490E-06  -6.327707E-06  -3.137646E-06   4.943245E-05   3.807719E-05
  a    56   4.958252E-05   9.993892E-05   1.821182E-05   2.302315E-05  -2.399612E-05  -1.889204E-05  -5.054034E-05  -9.051814E-05
  a    57   1.127027E-04   5.981203E-05   2.750624E-05   8.528730E-06   3.129775E-06   1.945512E-05  -3.869411E-05  -6.158415E-05
  a    58  -1.133067E-05  -4.439720E-06   2.887921E-06   8.124930E-06   1.027002E-05  -1.995029E-05   1.827563E-06   8.678644E-06
  a    59  -3.460386E-06  -2.614912E-06   1.096508E-05   9.019170E-05   1.241318E-04  -2.435621E-05   8.000860E-06   6.020203E-06
  a    60  -1.393281E-04   1.824387E-07   1.195368E-04   4.635023E-05  -4.734019E-05   5.349086E-06  -6.533772E-06   9.432143E-05
  a    61   1.423779E-05   8.704960E-05  -1.764278E-05  -4.029037E-05   3.219932E-05   1.799717E-05  -2.715086E-05  -1.176515E-04
  a    62  -1.274830E-05   1.549703E-05  -5.142924E-06   1.345132E-05   3.017741E-05  -5.336675E-06  -8.271199E-06  -1.747845E-05
  a    63   1.738420E-04  -3.257406E-05   4.085024E-05   2.970919E-05  -1.700690E-05   5.265354E-06   1.674422E-05  -2.512737E-06
  a    64  -8.197411E-05   1.996502E-05  -1.334646E-05   6.813329E-06   1.886303E-05  -4.470702E-06  -2.596415E-05  -6.258559E-06
  a    65   3.159725E-04  -1.552624E-05  -8.679097E-05   4.525917E-06   3.716865E-06   1.331841E-05   4.956636E-05   1.275879E-04
  a    66  -1.552624E-05   2.258129E-04  -1.468995E-06   5.547367E-07   2.178585E-06   2.982366E-06  -2.777884E-05  -8.395089E-05
  a    67  -8.679097E-05  -1.468995E-06   1.662993E-04   5.560992E-06   1.145605E-05  -1.212677E-06   1.081572E-05  -4.677483E-05
  a    68   4.525917E-06   5.547367E-07   5.560992E-06   2.331641E-04   2.524047E-04  -7.480429E-06  -7.234905E-06   3.331652E-06
  a    69   3.716865E-06   2.178585E-06   1.145605E-05   2.524047E-04   3.551839E-04  -2.166634E-05   2.513808E-06  -4.024592E-06
  a    70   1.331841E-05   2.982366E-06  -1.212677E-06  -7.480429E-06  -2.166634E-05   4.718376E-05   3.382106E-06   9.526817E-06
  a    71   4.956636E-05  -2.777884E-05   1.081572E-05  -7.234905E-06   2.513808E-06   3.382106E-06   1.109401E-04   1.244803E-04
  a    72   1.275879E-04  -8.395089E-05  -4.677483E-05   3.331652E-06  -4.024592E-06   9.526817E-06   1.244803E-04   4.045278E-04
  a    73   1.489563E-05  -6.195157E-06   7.508697E-06   5.717623E-05   3.259489E-05   5.092592E-05  -8.516217E-06   1.198128E-05
  a    74  -1.220011E-05  -1.416900E-05  -4.082185E-05   5.026611E-06   1.075012E-05  -1.805252E-05  -1.483292E-05  -3.542230E-06
  a    75  -5.089015E-05   5.386414E-05  -1.702357E-05  -1.213241E-05   1.872452E-06  -1.208006E-05  -6.151396E-05  -6.237961E-05
  a    76  -7.514150E-05   3.675138E-05  -1.538645E-05  -6.959926E-06   4.039506E-07  -7.889157E-06  -4.331031E-05  -1.131903E-04
  a    77  -2.710536E-05   1.330907E-05   2.419265E-05   7.883184E-06  -7.659682E-06  -7.950447E-07  -1.749087E-05  -4.794118E-05
  a    78   4.547351E-05   1.901180E-05   2.533898E-05  -1.087717E-05   3.647661E-06   1.064954E-05   6.950686E-06   4.407512E-06
  a    79  -1.247600E-05  -2.875523E-05  -7.163096E-06   3.868400E-06  -1.293474E-05  -5.248865E-06   6.204244E-06   4.026105E-05
  a    80  -2.653474E-05   6.754561E-06   2.535507E-05   6.758393E-06   2.392732E-06   8.963079E-06  -2.458838E-06  -1.760620E-05
  a    81  -1.537521E-05   8.464646E-06   7.466542E-06  -4.972467E-06   3.836894E-07  -8.207859E-06  -8.546988E-06  -3.264329E-05
  a    82  -5.014511E-05   3.564228E-05   3.079676E-05   7.560510E-06  -4.719172E-06   1.844158E-06  -5.364275E-06  -2.634468E-05
  a    83  -1.595318E-05  -1.494051E-05  -8.796606E-06  -2.037637E-06   1.678521E-06  -6.060630E-07   4.453043E-06   3.879911E-05
  a    84  -1.040863E-05   2.483966E-05  -1.315098E-06  -1.242328E-06   2.578247E-06   1.217982E-06  -2.163643E-05  -1.407454E-05
  a    85   2.443318E-06  -9.912857E-06   2.605122E-06   4.179137E-06  -2.510415E-06   3.159822E-07   3.354670E-06  -1.108323E-05
  a    86  -2.414557E-05   2.583718E-05  -1.186244E-05  -1.510769E-06   1.402243E-06  -1.913093E-06  -2.736595E-05  -3.997955E-05

               a    73        a    74        a    75        a    76        a    77        a    78        a    79        a    80
  a    13  -2.151540E-04  -1.126250E-03   8.624383E-05   6.906751E-04   2.688856E-03  -5.613472E-04   9.387661E-05   1.425378E-03
  a    14  -2.249250E-04   1.264468E-03  -1.238854E-03  -6.128470E-04   3.496115E-04  -9.827722E-04   1.708552E-03  -1.088806E-05
  a    15   1.490182E-04  -9.643074E-04  -1.175999E-03   1.199858E-05  -1.173351E-03  -1.124511E-03   4.812581E-04  -7.135794E-04
  a    16   2.638260E-04  -8.138354E-04  -3.061238E-03   1.514227E-03  -2.271179E-03   2.294988E-04  -2.409068E-04  -8.168823E-04
  a    17  -1.754580E-04   5.685164E-04   1.280100E-03   6.125784E-05   2.650106E-03  -7.807000E-04   4.290078E-04   3.133245E-04
  a    18  -1.468677E-04   4.732788E-04  -9.832603E-04   2.673546E-04  -3.466822E-04  -3.380480E-03   1.663464E-03  -1.004122E-03
  a    19   7.466793E-05  -1.599585E-03  -2.545330E-03   1.273151E-03  -1.550853E-03   1.075323E-03  -1.067335E-03  -1.174251E-04
  a    20   2.901480E-04   4.635383E-04  -4.618678E-04   3.561452E-04  -1.026974E-03   6.465881E-04  -8.044867E-04  -1.346204E-03
  a    21  -9.227761E-05   2.387280E-04  -8.895329E-04  -7.132537E-04  -2.703238E-04  -1.739241E-03   2.926487E-04  -3.066854E-07
  a    22   2.852880E-04  -1.276265E-03  -3.226383E-05  -2.050789E-04  -3.456055E-03   3.845048E-04   5.181841E-04   1.370927E-04
  a    23   2.340739E-04  -2.031313E-03  -8.830129E-04   5.679245E-04   5.925644E-04   3.401840E-04   1.272831E-03  -5.138034E-04
  a    24   3.670105E-04   3.585944E-04  -7.671839E-04   1.938456E-04  -4.494466E-03   2.264439E-04   4.212080E-04  -9.572263E-04
  a    25   1.169759E-04  -1.910851E-03  -2.768894E-04  -6.618902E-04   1.002113E-03   8.766960E-04  -1.582479E-03   5.981622E-05
  a    26   7.153960E-05   3.110357E-04  -5.546476E-04   1.034382E-03  -5.396879E-03  -1.613661E-04   2.115053E-03   3.611186E-03
  a    27   2.557013E-03  -1.695674E-04   4.181452E-04   7.248613E-04  -7.369776E-04   2.293867E-04  -8.839093E-05   7.871180E-04
  a    28   6.114789E-04  -1.182985E-03  -2.170549E-03  -4.366869E-03   5.340085E-03   1.036516E-04   5.518111E-04  -1.355789E-03
  a    29   1.150466E-03   1.189515E-04  -2.800670E-05  -3.296148E-05   2.377807E-05  -2.461135E-05  -1.206130E-04   8.225812E-05
  a    30   2.099062E-03   2.972573E-04  -9.558115E-05   5.266748E-05   4.258436E-05   8.784328E-06  -1.835801E-04   8.203271E-05
  a    31  -2.560651E-03  -1.213662E-04  -3.649957E-05  -3.244059E-05  -5.559023E-05   1.763239E-04   1.907860E-06   1.616381E-05
  a    32   7.876010E-03   5.336328E-04  -9.508833E-05  -1.060996E-04   4.258855E-05  -2.716679E-04  -2.785441E-04   1.753073E-04
  a    33  -9.521415E-05  -2.769139E-04  -1.963414E-04   5.178776E-05  -3.413024E-05   1.201502E-04   1.749203E-04   3.920393E-04
  a    34  -2.872489E-03  -2.235878E-04   5.670026E-05   4.076901E-05  -1.165381E-05   9.973897E-05   1.256937E-04  -6.868942E-05
  a    35   5.520934E-04   5.294510E-05   1.294886E-06  -8.232397E-06   4.696936E-06  -2.977417E-05  -2.254350E-05   1.077178E-05
  a    36  -1.034971E-04  -2.617851E-06   1.101784E-06  -2.997194E-06   8.888253E-07   2.722023E-06  -1.922188E-05   2.003264E-05
  a    37  -3.971467E-05  -1.161885E-04   2.594042E-05  -1.899004E-04   7.054716E-04  -2.384987E-05  -6.345151E-05  -7.117795E-05
  a    38   4.232377E-05  -6.485075E-05  -1.881770E-04   1.858570E-04  -7.537221E-04   4.590300E-05   1.061981E-04   1.814792E-04
  a    39   9.508495E-06  -6.023300E-05  -1.942755E-04   3.974090E-05  -2.849539E-04  -2.770011E-05   1.204097E-04   1.330978E-04
  a    40  -1.453968E-05   1.914983E-04   1.070692E-04  -9.281099E-05   3.272964E-04  -1.401251E-04  -1.652449E-05  -1.919085E-04
  a    41  -1.527115E-05   5.426695E-05   4.893781E-05  -1.172384E-04   6.570058E-05   4.838524E-05   2.015771E-05  -7.175477E-07
  a    42  -3.492526E-05  -1.137432E-05  -3.794831E-06   5.536086E-05   6.380909E-05  -3.291242E-05   3.480694E-05  -2.999358E-05
  a    43  -1.237274E-05  -3.267662E-05  -5.917059E-05  -5.099847E-05  -6.865728E-05   2.322173E-05   5.285402E-05   2.511200E-05
  a    44   1.015455E-05  -1.575244E-04  -2.204038E-04  -3.534729E-05  -1.660826E-04  -3.471127E-05   1.594229E-04   1.544543E-04
  a    45  -6.351275E-06   1.043520E-04  -7.452917E-05  -1.903762E-04   2.304193E-04  -1.674561E-04   1.206942E-04  -5.502983E-05
  a    46  -9.811071E-06   4.201479E-04   2.634053E-04   3.955419E-05   8.491130E-05  -2.593100E-04   1.161210E-04  -9.821048E-05
  a    47   2.014604E-05  -1.004013E-04  -9.619389E-05  -4.775940E-05   1.671967E-04  -3.688546E-05   2.123088E-05   1.029017E-05
  a    48   2.617666E-05  -1.099151E-04  -8.875198E-05  -2.841766E-05   5.126746E-05   6.642947E-05  -3.844897E-05   5.452246E-05
  a    49   2.036147E-05   8.241691E-05   2.141663E-05  -5.366129E-05  -2.161423E-04   5.562572E-05   5.794631E-05   5.492955E-05
  a    50  -4.143000E-05   1.596905E-04   9.224326E-05   4.193137E-05   1.785895E-04  -1.113178E-04  -3.645092E-06  -1.320801E-04
  a    51  -3.614369E-04  -3.601709E-05  -1.439236E-06   2.025163E-06  -1.369143E-05   2.051404E-05   1.382893E-05   4.861578E-07
  a    52  -2.071306E-05  -1.029292E-04   3.691858E-05  -2.207089E-04   4.932377E-04   6.874795E-05  -1.020896E-04  -1.082079E-04
  a    53   1.084974E-05  -1.341775E-04  -4.254629E-05   9.581555E-05  -1.969627E-04   7.365722E-05  -5.240901E-05   8.282216E-05
  a    54   1.816918E-04   1.478819E-06   1.218466E-06   3.610803E-06   2.666384E-06  -2.631087E-06  -4.278012E-06  -5.452720E-08
  a    55   1.418045E-05   8.525088E-05  -6.762634E-06  -3.465587E-05  -8.968538E-06  -3.121114E-05   3.592852E-05  -1.609393E-05
  a    56  -6.066871E-06   8.424913E-05   8.386144E-05  -7.195133E-05   9.813137E-05  -1.044933E-05   6.921693E-06  -3.133140E-05
  a    57   3.767391E-06  -1.378527E-04  -8.005318E-05  -3.719787E-05  -2.530996E-05   1.249019E-04  -5.550930E-05   8.984274E-06
  a    58   2.013542E-05  -4.998575E-06  -3.062851E-06  -2.113764E-07   2.850172E-06  -6.323648E-06   8.401134E-06   1.814496E-06
  a    59  -1.147143E-04  -5.547085E-06  -9.044561E-06  -7.494670E-06   2.476093E-06   3.659697E-06   3.145663E-06   5.757266E-06
  a    60   9.351020E-06  -1.448536E-04  -1.065872E-04  -7.005045E-05   2.764903E-05   1.334733E-05   5.057018E-05   9.635714E-05
  a    61  -5.386902E-06  -3.149657E-05   7.339617E-05   5.437225E-05  -6.662178E-05   9.308259E-05  -7.887456E-05  -9.120259E-07
  a    62   7.186484E-05  -1.189161E-06   1.286857E-05   1.994816E-05  -1.645611E-05   7.185587E-06  -1.578126E-05   4.031597E-06
  a    63   2.082186E-05  -4.863416E-05  -7.035090E-05  -1.083742E-04   4.117764E-05   5.810752E-05  -1.257362E-05  -2.236049E-06
  a    64   3.061605E-05   4.194594E-05   3.310707E-05   2.482434E-05   1.086639E-05  -4.662743E-05   1.571631E-05  -1.158693E-05
  a    65   1.489563E-05  -1.220011E-05  -5.089015E-05  -7.514150E-05  -2.710536E-05   4.547351E-05  -1.247600E-05  -2.653474E-05
  a    66  -6.195157E-06  -1.416900E-05   5.386414E-05   3.675138E-05   1.330907E-05   1.901180E-05  -2.875523E-05   6.754561E-06
  a    67   7.508697E-06  -4.082185E-05  -1.702357E-05  -1.538645E-05   2.419265E-05   2.533898E-05  -7.163096E-06   2.535507E-05
  a    68   5.717623E-05   5.026611E-06  -1.213241E-05  -6.959926E-06   7.883184E-06  -1.087717E-05   3.868400E-06   6.758393E-06
  a    69   3.259489E-05   1.075012E-05   1.872452E-06   4.039506E-07  -7.659682E-06   3.647661E-06  -1.293474E-05   2.392732E-06
  a    70   5.092592E-05  -1.805252E-05  -1.208006E-05  -7.889157E-06  -7.950447E-07   1.064954E-05  -5.248865E-06   8.963079E-06
  a    71  -8.516217E-06  -1.483292E-05  -6.151396E-05  -4.331031E-05  -1.749087E-05   6.950686E-06   6.204244E-06  -2.458838E-06
  a    72   1.198128E-05  -3.542230E-06  -6.237961E-05  -1.131903E-04  -4.794118E-05   4.407512E-06   4.026105E-05  -1.760620E-05
  a    73   3.059685E-04   8.483992E-06  -8.277755E-06  -7.552758E-06  -4.106395E-06  -3.947093E-06  -7.830528E-06   5.080014E-06
  a    74   8.483992E-06   1.597491E-04   9.103728E-05   2.405358E-05  -1.487809E-06  -6.502317E-05   1.104232E-05  -6.001996E-05
  a    75  -8.277755E-06   9.103728E-05   1.835213E-04   1.860543E-05   3.001905E-05   2.195454E-05  -3.064863E-05  -1.224682E-05
  a    76  -7.552758E-06   2.405358E-05   1.860543E-05   1.635228E-04  -1.212356E-04  -2.855247E-05  -1.915933E-05   2.175022E-06
  a    77  -4.106395E-06  -1.487809E-06   3.001905E-05  -1.212356E-04   2.915013E-04  -3.023213E-05   5.384872E-06  -2.269052E-05
  a    78  -3.947093E-06  -6.502317E-05   2.195454E-05  -2.855247E-05  -3.023213E-05   1.145846E-04  -4.656207E-05   2.993493E-05
  a    79  -7.830528E-06   1.104232E-05  -3.064863E-05  -1.915933E-05   5.384872E-06  -4.656207E-05   6.759213E-05   2.104432E-05
  a    80   5.080014E-06  -6.001996E-05  -1.224682E-05   2.175022E-06  -2.269052E-05   2.993493E-05   2.104432E-05   9.093294E-05
  a    81  -3.799968E-06   4.669892E-05   3.476269E-05  -1.557045E-05   6.367764E-05  -1.881495E-05  -3.476451E-06  -2.557387E-05
  a    82  -2.253302E-06  -3.159132E-05  -1.237618E-05   2.874973E-05   1.718106E-05  -1.125944E-05  -5.652229E-06   4.921858E-06
  a    83   1.921100E-06   8.569104E-06  -1.997749E-05  -2.283650E-06  -2.985940E-06  -1.740014E-05   1.720598E-05  -1.432287E-05
  a    84  -1.811499E-06   1.894127E-07   1.694079E-05   1.170159E-05  -1.473210E-05   8.912521E-06  -1.807585E-06  -2.281870E-06
  a    85  -5.926435E-07  -1.951284E-06  -8.828320E-06  -4.117513E-06  -4.037812E-06  -1.219744E-06   3.984962E-06   1.384549E-05
  a    86  -2.522321E-06   6.475327E-06   2.187246E-05   1.396752E-05  -1.841237E-05   5.529233E-06   1.582281E-05   1.726481E-05

               a    81        a    82        a    83        a    84        a    85        a    86
  a    13   7.615881E-05  -1.893737E-04  -1.404922E-04   6.578268E-05   2.075340E-04   2.956048E-04
  a    14  -6.141963E-04  -6.788233E-04  -2.825147E-04  -3.735887E-04  -1.741138E-04   1.795355E-04
  a    15  -2.451258E-04  -9.630763E-04   1.438293E-03   2.064226E-04   3.604643E-04  -4.237178E-04
  a    16  -4.861838E-04   1.219735E-03   1.777814E-03  -8.655138E-05  -5.729065E-04  -3.210101E-04
  a    17   7.927488E-04   8.543419E-04   2.389319E-04   5.216075E-05  -1.855666E-04  -1.552876E-05
  a    18  -6.452390E-04   3.292049E-03  -1.074140E-03   6.553060E-04  -4.642476E-04   1.473397E-04
  a    19  -4.803121E-04  -8.326392E-04   1.956286E-03  -2.093545E-04   2.938031E-04  -2.355913E-04
  a    20   6.509495E-05   2.561939E-04   1.023522E-03  -5.231267E-05  -8.198594E-04  -6.148113E-04
  a    21   1.928803E-04  -9.905010E-04   6.249298E-04   2.170270E-04   8.503573E-05  -5.134451E-05
  a    22  -1.340417E-03  -1.047913E-03   1.174194E-04   1.861402E-05   4.032962E-04  -1.326458E-04
  a    23   2.643780E-04  -8.353832E-04   7.369456E-04  -2.228747E-04  -1.433572E-04  -1.999929E-04
  a    24  -1.205725E-03  -6.626122E-04  -9.881345E-04  -1.156224E-04  -2.795610E-04  -2.475773E-04
  a    25   4.387444E-04  -9.018332E-04   2.211926E-04   1.619180E-05   4.350453E-06  -4.631079E-04
  a    26  -3.101565E-03  -1.489093E-03  -1.371853E-03  -7.986235E-04  -2.009075E-04   1.168375E-04
  a    27  -2.616518E-04  -7.352895E-05   7.525320E-06  -4.077391E-04  -2.713053E-04   1.730615E-04
  a    28   3.984308E-04  -3.070560E-04   2.874572E-04  -1.273817E-03  -3.845146E-04  -7.184452E-04
  a    29   2.869072E-06   1.607838E-05   3.478964E-05  -2.275799E-05  -4.111757E-06   2.826483E-05
  a    30   1.364871E-05  -4.887657E-05   7.027424E-05  -4.020493E-05  -6.402401E-06   3.157707E-06
  a    31   1.279488E-05  -6.371365E-06   4.948920E-05  -5.975351E-05  -3.509665E-05   3.271682E-05
  a    32   3.221672E-06  -5.456749E-06   4.942175E-05  -2.703263E-05  -1.615732E-05   4.991105E-05
  a    33   1.209978E-04  -1.073189E-03   2.965799E-04  -4.526238E-04   4.096392E-04  -3.029190E-05
  a    34   4.874330E-06   5.149127E-06  -2.202746E-05   4.333675E-06   4.101864E-06  -1.201926E-05
  a    35  -2.078198E-06  -7.407323E-06  -2.507323E-06   5.170847E-06   4.659788E-06  -5.996844E-06
  a    36   1.294215E-06   3.073654E-06   1.256256E-05  -3.183653E-06  -5.309283E-06   5.177131E-06
  a    37   1.175908E-04   7.341313E-05   2.863364E-05  -8.637623E-06   6.245302E-05   2.496639E-05
  a    38  -2.545989E-04  -1.060328E-04  -2.046791E-05   1.612743E-05   1.016472E-05   5.302531E-05
  a    39  -1.400512E-04  -4.348870E-06   1.556845E-05  -4.033243E-05   2.301930E-05  -1.052994E-05
  a    40   1.766483E-04   8.513625E-05   1.457136E-05   2.508222E-05  -4.922350E-05  -1.578379E-05
  a    41   1.406221E-05  -7.879933E-05  -1.653955E-05  -2.940242E-05   2.087481E-05  -1.267087E-05
  a    42   8.406315E-07   6.867738E-05   1.517441E-05   3.434657E-05  -2.366527E-05   2.809207E-05
  a    43  -5.359677E-05   4.923618E-05  -2.896683E-05   7.653311E-05  -2.635090E-05   7.855569E-05
  a    44  -1.495001E-04   1.094951E-04   4.903198E-05   9.684578E-05  -1.157161E-05   1.048973E-04
  a    45   1.125526E-04  -5.567527E-05   9.201179E-05  -7.684868E-05   4.040789E-05  -6.655573E-05
  a    46   1.091085E-04  -4.056194E-05   3.723132E-05  -1.784927E-05   2.612720E-05   3.956761E-05
  a    47  -8.477269E-06   1.213553E-04   4.896998E-05  -1.877247E-05   4.080091E-06  -3.881099E-05
  a    48   3.036461E-05   5.384326E-05  -1.924485E-05   1.064133E-05   1.016879E-05  -9.496364E-06
  a    49  -5.121037E-05  -1.247905E-04  -3.679006E-05   3.765420E-05  -1.750885E-06   5.792285E-05
  a    50   1.060355E-04   3.990505E-05   4.478509E-05   3.388040E-05  -1.458710E-05   2.770362E-05
  a    51  -6.540460E-06  -2.288388E-06  -4.170336E-06  -1.689118E-06   1.084613E-06  -3.554714E-06
  a    52   1.188225E-04   8.886351E-05   1.588720E-05   2.219027E-05   1.940367E-06  -1.490725E-05
  a    53  -1.259370E-04   7.269793E-05  -5.962520E-05  -6.722334E-06  -7.738974E-06  -2.838321E-05
  a    54  -3.037992E-06  -1.948961E-07  -1.075563E-06   9.932749E-07  -8.713604E-07   2.808949E-07
  a    55   5.008069E-05  -2.474193E-05   2.011874E-05  -9.514447E-07   4.247279E-06   1.165935E-05
  a    56   1.946383E-06  -4.381270E-05  -1.726701E-06   1.503273E-05   4.877230E-06   2.225217E-05
  a    57  -3.896519E-05   1.193759E-05  -2.941423E-05   4.412172E-05  -2.543419E-05   1.502003E-05
  a    58  -1.862238E-06   3.687783E-06   8.102291E-07  -9.191187E-07   1.165214E-06   8.015231E-08
  a    59  -3.503390E-06   2.885339E-06   2.332534E-06   5.677112E-07   8.587311E-07   1.696514E-06
  a    60  -6.997953E-05   7.588335E-05   3.015460E-05   5.838722E-06   1.433256E-05  -2.474856E-07
  a    61  -8.087903E-06  -2.520769E-05  -1.486843E-05  -1.677296E-05  -1.286958E-06  -1.931678E-05
  a    62  -2.908373E-06   7.917875E-07  -1.017749E-07  -5.359690E-06  -2.020775E-06  -2.166964E-06
  a    63  -3.468326E-06  -2.231708E-05  -3.428029E-05   7.694570E-06   5.486053E-06  -1.033641E-05
  a    64   1.499165E-05   7.383437E-06   1.760174E-05   4.091015E-06  -4.685314E-06   1.240151E-05
  a    65  -1.537521E-05  -5.014511E-05  -1.595318E-05  -1.040863E-05   2.443318E-06  -2.414557E-05
  a    66   8.464646E-06   3.564228E-05  -1.494051E-05   2.483966E-05  -9.912857E-06   2.583718E-05
  a    67   7.466542E-06   3.079676E-05  -8.796606E-06  -1.315098E-06   2.605122E-06  -1.186244E-05
  a    68  -4.972467E-06   7.560510E-06  -2.037637E-06  -1.242328E-06   4.179137E-06  -1.510769E-06
  a    69   3.836894E-07  -4.719172E-06   1.678521E-06   2.578247E-06  -2.510415E-06   1.402243E-06
  a    70  -8.207859E-06   1.844158E-06  -6.060630E-07   1.217982E-06   3.159822E-07  -1.913093E-06
  a    71  -8.546988E-06  -5.364275E-06   4.453043E-06  -2.163643E-05   3.354670E-06  -2.736595E-05
  a    72  -3.264329E-05  -2.634468E-05   3.879911E-05  -1.407454E-05  -1.108323E-05  -3.997955E-05
  a    73  -3.799968E-06  -2.253302E-06   1.921100E-06  -1.811499E-06  -5.926435E-07  -2.522321E-06
  a    74   4.669892E-05  -3.159132E-05   8.569104E-06   1.894127E-07  -1.951284E-06   6.475327E-06
  a    75   3.476269E-05  -1.237618E-05  -1.997749E-05   1.694079E-05  -8.828320E-06   2.187246E-05
  a    76  -1.557045E-05   2.874973E-05  -2.283650E-06   1.170159E-05  -4.117513E-06   1.396752E-05
  a    77   6.367764E-05   1.718106E-05  -2.985940E-06  -1.473210E-05  -4.037812E-06  -1.841237E-05
  a    78  -1.881495E-05  -1.125944E-05  -1.740014E-05   8.912521E-06  -1.219744E-06   5.529233E-06
  a    79  -3.476451E-06  -5.652229E-06   1.720598E-05  -1.807585E-06   3.984962E-06   1.582281E-05
  a    80  -2.557387E-05   4.921858E-06  -1.432287E-05  -2.281870E-06   1.384549E-05   1.726481E-05
  a    81   6.320033E-05  -7.872088E-06   5.040940E-06   1.706937E-06  -2.684886E-06  -2.772853E-06
  a    82  -7.872088E-06   8.755754E-05  -5.485700E-06   5.425264E-06  -8.041089E-06  -3.286610E-07
  a    83   5.040940E-06  -5.485700E-06   5.533829E-05  -3.339110E-06   7.527269E-06  -2.192662E-06
  a    84   1.706937E-06   5.425264E-06  -3.339110E-06   3.588545E-05  -1.789873E-05   2.811211E-05
  a    85  -2.684886E-06  -8.041089E-06   7.527269E-06  -1.789873E-05   2.828185E-05   4.652517E-07
  a    86  -2.772853E-06  -3.286610E-07  -2.192662E-06   2.811211E-05   4.652517E-07   5.368122E-05

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99997712     1.99986106     1.99974590     1.99966760
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.99954954     1.99934830     1.99933605     1.99890641     1.99877821     1.99851882     1.99830920     1.99780576
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     1.99758472     1.99684982     1.99495026     1.99418100     1.99346986     1.98908566     1.98400385     1.95335804
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     1.92354570     0.07418631     0.05448713     0.01440713     0.01023813     0.00647398     0.00489219     0.00297742
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00210001     0.00199563     0.00142609     0.00122811     0.00112219     0.00096962     0.00096216     0.00064739
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00061265     0.00056727     0.00040754     0.00039287     0.00036003     0.00033553     0.00028214     0.00025578
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62       MO    63       MO    64
  occ(*)=     0.00023906     0.00023809     0.00018344     0.00015582     0.00014990     0.00013271     0.00011491     0.00010782
              MO    65       MO    66       MO    67       MO    68       MO    69       MO    70       MO    71       MO    72
  occ(*)=     0.00008204     0.00007788     0.00006951     0.00004808     0.00003914     0.00003798     0.00003471     0.00002498
              MO    73       MO    74       MO    75       MO    76       MO    77       MO    78       MO    79       MO    80
  occ(*)=     0.00001975     0.00001595     0.00001525     0.00001264     0.00001053     0.00000790     0.00000705     0.00000473
              MO    81       MO    82       MO    83       MO    84       MO    85       MO    86
  occ(*)=     0.00000382     0.00000283     0.00000160     0.00000088     0.00000059     0.00000026


 total number of electrons =   66.0000000000

 item #                     2 suffix=:.drt1.state2:
 read_civout: repnuc=  -468.746689847934     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.62624040  -242.87955055
 residuum:     0.00037862
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -711.49543188  -242.74874203
 residuum:     0.00061881
 deltae:     0.00000001

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.12115223
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.02557439
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.03733045
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.08826447

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24
 ref:   1    -0.09929586    -0.10555031    -0.01128983    -0.03609658     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00871655     0.03940820     0.04506172    -0.01308525     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.04592776    -0.09668793    -0.00209956    -0.01628650     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   4     0.01825779     0.02992709     0.04326769     0.05354279     0.00000000     0.00000000     0.00000000     0.00000000

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.00000000
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.00000000
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.00000000
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y=   34801  D0X=       0  D0W=       0
  DDZI=   17300 DDYI=   39515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1785 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y= 1246697  D0X=       0  D0W=       0
  DDZI=  203100 DDYI=  457450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    42.000000
   42  correlated and    24  frozen core electrons
   7396 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000    
  a     6    0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000    
  a     7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000    
  a     8    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000    

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     9    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    10    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    11    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    12    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a    13    0.00000        0.00000        0.00000        0.00000        1.99891       2.119508E-04   7.364916E-04   2.950796E-04
  a    14    0.00000        0.00000        0.00000        0.00000       2.119508E-04    1.99931      -6.067517E-04   1.558732E-04
  a    15    0.00000        0.00000        0.00000        0.00000       7.364916E-04  -6.067517E-04    1.99856      -7.918358E-04
  a    16    0.00000        0.00000        0.00000        0.00000       2.950796E-04   1.558732E-04  -7.918358E-04    1.99803    
  a    17    0.00000        0.00000        0.00000        0.00000      -4.911233E-04  -2.162610E-04   2.767146E-04   8.652101E-04
  a    18    0.00000        0.00000        0.00000        0.00000      -2.985752E-04  -1.359912E-04   4.107611E-04  -1.368090E-04
  a    19    0.00000        0.00000        0.00000        0.00000       3.608321E-04   5.626245E-05  -6.660205E-04  -3.125840E-04
  a    20    0.00000        0.00000        0.00000        0.00000       1.339221E-04  -2.079106E-04  -4.856205E-04  -1.184555E-06
  a    21    0.00000        0.00000        0.00000        0.00000       2.438048E-04  -2.668080E-04  -6.117576E-04  -2.431011E-04
  a    22    0.00000        0.00000        0.00000        0.00000       3.269935E-04   1.754968E-04  -1.452533E-04  -4.428828E-04
  a    23    0.00000        0.00000        0.00000        0.00000       1.434341E-05  -3.100465E-05  -2.140047E-04  -1.703376E-04
  a    24    0.00000        0.00000        0.00000        0.00000       2.982542E-04   1.151663E-05  -3.055255E-04  -2.916013E-04
  a    25    0.00000        0.00000        0.00000        0.00000      -1.740076E-04   1.395065E-04   1.693188E-04   3.397448E-04
  a    26    0.00000        0.00000        0.00000        0.00000       1.181004E-04   2.547285E-04   2.140527E-04  -8.773328E-04
  a    27    0.00000        0.00000        0.00000        0.00000       2.017096E-05  -5.913756E-06  -1.592205E-05  -1.447642E-04
  a    28    0.00000        0.00000        0.00000        0.00000      -4.511219E-04   2.416595E-04   8.115176E-04   6.205678E-04
  a    29    0.00000        0.00000        0.00000        0.00000      -7.244556E-06   3.464890E-06   8.183226E-06   6.499825E-06
  a    30    0.00000        0.00000        0.00000        0.00000      -7.360497E-06  -1.421766E-05   2.651324E-05  -1.848435E-05
  a    31    0.00000        0.00000        0.00000        0.00000      -2.692824E-06   2.559465E-06   3.460191E-05   1.921301E-05
  a    32    0.00000        0.00000        0.00000        0.00000      -1.070249E-06  -2.134880E-06   2.407125E-05   3.961251E-06
  a    33    0.00000        0.00000        0.00000        0.00000       2.150861E-03  -1.728560E-03  -2.091189E-03   7.166706E-04
  a    34    0.00000        0.00000        0.00000        0.00000      -6.721793E-05   1.624446E-04  -3.421544E-04   1.005586E-04
  a    35    0.00000        0.00000        0.00000        0.00000       3.992754E-05  -1.952925E-06   4.432906E-05   4.711024E-05
  a    36    0.00000        0.00000        0.00000        0.00000      -2.718125E-05   7.315513E-05  -5.817368E-05  -3.613733E-05
  a    37    0.00000        0.00000        0.00000        0.00000      -1.947547E-03  -1.740110E-03  -4.886646E-04   3.978322E-03
  a    38    0.00000        0.00000        0.00000        0.00000       1.420484E-05  -7.352961E-04  -2.739731E-03  -2.383957E-03
  a    39    0.00000        0.00000        0.00000        0.00000       1.738162E-04  -1.069494E-03   4.164744E-04  -1.747975E-03
  a    40    0.00000        0.00000        0.00000        0.00000       1.844362E-03   2.207201E-03   3.338774E-03   1.512922E-03
  a    41    0.00000        0.00000        0.00000        0.00000      -1.572191E-03  -1.847532E-03  -8.294292E-04  -7.698112E-04
  a    42    0.00000        0.00000        0.00000        0.00000       1.135837E-03   7.804386E-04   5.129094E-04   5.491315E-05
  a    43    0.00000        0.00000        0.00000        0.00000       3.437938E-03   9.721728E-04   1.227565E-03   1.156757E-03
  a    44    0.00000        0.00000        0.00000        0.00000       1.694673E-03  -8.069529E-04  -1.336561E-03  -4.773680E-04
  a    45    0.00000        0.00000        0.00000        0.00000      -4.662472E-04   6.711720E-05   1.446548E-03   2.588689E-03
  a    46    0.00000        0.00000        0.00000        0.00000      -1.345327E-03  -1.027492E-03   5.188792E-04   4.036235E-04
  a    47    0.00000        0.00000        0.00000        0.00000       9.448230E-04   1.088288E-03   8.348140E-04   2.197495E-03
  a    48    0.00000        0.00000        0.00000        0.00000       9.490193E-04   1.266444E-03  -7.179945E-05  -8.831899E-04
  a    49    0.00000        0.00000        0.00000        0.00000      -4.153744E-04  -1.218908E-03  -6.007551E-04   8.899441E-04
  a    50    0.00000        0.00000        0.00000        0.00000       8.932484E-04   8.105542E-04   7.974064E-04   4.672275E-04
  a    51    0.00000        0.00000        0.00000        0.00000      -5.021876E-05  -5.060058E-05  -5.184829E-05  -1.963440E-05
  a    52    0.00000        0.00000        0.00000        0.00000      -4.892049E-04  -4.761144E-04   4.953817E-04   1.995956E-03
  a    53    0.00000        0.00000        0.00000        0.00000       4.490438E-04   6.691977E-04  -1.494073E-05  -1.458827E-03
  a    54    0.00000        0.00000        0.00000        0.00000      -7.836827E-05  -7.901384E-05  -8.115558E-05  -6.901174E-05
  a    55    0.00000        0.00000        0.00000        0.00000       1.670691E-03   1.242308E-03   1.428982E-03   1.459678E-03
  a    56    0.00000        0.00000        0.00000        0.00000       5.239950E-06  -9.061882E-04  -6.549748E-04   1.135869E-03
  a    57    0.00000        0.00000        0.00000        0.00000       9.013230E-04  -5.655768E-04  -1.644269E-03   6.909691E-04
  a    58    0.00000        0.00000        0.00000        0.00000      -2.301216E-05  -5.666790E-05  -3.098273E-05  -2.816162E-06
  a    59    0.00000        0.00000        0.00000        0.00000       6.113133E-05  -1.287045E-04  -7.304803E-05   7.146518E-05
  a    60    0.00000        0.00000        0.00000        0.00000       2.696761E-04  -1.607448E-03  -1.677102E-03   8.379654E-04
  a    61    0.00000        0.00000        0.00000        0.00000      -2.961157E-04   6.399477E-04   6.744941E-04  -6.836307E-04
  a    62    0.00000        0.00000        0.00000        0.00000      -2.107535E-04   2.384998E-04   1.191229E-04  -2.881612E-04
  a    63    0.00000        0.00000        0.00000        0.00000       1.030057E-03  -7.065552E-04   1.120106E-04   5.836407E-04
  a    64    0.00000        0.00000        0.00000        0.00000      -2.435488E-04  -2.157190E-06   6.178542E-05   5.602532E-05
  a    65    0.00000        0.00000        0.00000        0.00000       4.714901E-04  -6.106598E-05  -4.081874E-04  -3.808923E-04
  a    66    0.00000        0.00000        0.00000        0.00000      -1.222644E-04   2.099995E-04   4.606194E-04  -3.455194E-04
  a    67    0.00000        0.00000        0.00000        0.00000       1.431581E-04   3.340401E-04   5.133268E-04   7.620521E-05
  a    68    0.00000        0.00000        0.00000        0.00000       8.730660E-05  -3.761180E-04  -3.980126E-05   2.588482E-04
  a    69    0.00000        0.00000        0.00000        0.00000      -7.135871E-05   3.269754E-04  -3.541789E-05  -2.219856E-04
  a    70    0.00000        0.00000        0.00000        0.00000      -7.448185E-05   2.744545E-04  -1.956932E-04  -5.327860E-05
  a    71    0.00000        0.00000        0.00000        0.00000       1.433138E-04   9.695538E-04  -1.253772E-06  -5.279901E-04
  a    72    0.00000        0.00000        0.00000        0.00000       1.112251E-03  -3.913377E-04  -1.029266E-03  -1.767054E-03
  a    73    0.00000        0.00000        0.00000        0.00000       5.479652E-05   6.453853E-05  -5.363846E-05  -1.501705E-04
  a    74    0.00000        0.00000        0.00000        0.00000       4.188570E-05  -6.060609E-04   3.224141E-04  -2.107267E-05
  a    75    0.00000        0.00000        0.00000        0.00000      -8.967561E-04  -3.548710E-04  -4.359637E-04  -5.036814E-04
  a    76    0.00000        0.00000        0.00000        0.00000       2.212345E-05   6.627418E-04   4.023063E-04  -2.633307E-05
  a    77    0.00000        0.00000        0.00000        0.00000      -8.294875E-04   3.336911E-05   4.550129E-04   8.101852E-04
  a    78    0.00000        0.00000        0.00000        0.00000      -3.199066E-04   5.187883E-05  -4.119659E-04  -1.220455E-03
  a    79    0.00000        0.00000        0.00000        0.00000       2.834418E-04  -4.161878E-04  -1.819454E-04   7.960732E-04
  a    80    0.00000        0.00000        0.00000        0.00000      -4.829633E-04  -1.222843E-04  -1.010277E-06   4.035494E-04
  a    81    0.00000        0.00000        0.00000        0.00000       2.952219E-04   7.511893E-04   7.189706E-04  -5.080950E-05
  a    82    0.00000        0.00000        0.00000        0.00000       1.481849E-04   3.862775E-04   4.195440E-04  -1.252352E-04
  a    83    0.00000        0.00000        0.00000        0.00000      -1.138219E-04   2.020132E-04  -1.288367E-04   3.002476E-05
  a    84    0.00000        0.00000        0.00000        0.00000       1.027683E-03   1.336382E-04  -6.409802E-04   7.234858E-05
  a    85    0.00000        0.00000        0.00000        0.00000      -1.092739E-03  -6.241067E-04  -4.518338E-04   3.530967E-04
  a    86    0.00000        0.00000        0.00000        0.00000       1.250340E-04  -1.020355E-03  -1.120233E-03   2.852232E-04

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a    13  -4.911233E-04  -2.985752E-04   3.608321E-04   1.339221E-04   2.438048E-04   3.269935E-04   1.434341E-05   2.982542E-04
  a    14  -2.162610E-04  -1.359912E-04   5.626245E-05  -2.079106E-04  -2.668080E-04   1.754968E-04  -3.100465E-05   1.151663E-05
  a    15   2.767146E-04   4.107611E-04  -6.660205E-04  -4.856205E-04  -6.117576E-04  -1.452533E-04  -2.140047E-04  -3.055255E-04
  a    16   8.652101E-04  -1.368090E-04  -3.125840E-04  -1.184555E-06  -2.431011E-04  -4.428828E-04  -1.703376E-04  -2.916013E-04
  a    17    1.99757      -3.633265E-04  -1.146167E-04  -1.022735E-04  -1.459841E-04   4.383004E-04   6.697801E-05   9.442248E-04
  a    18  -3.633265E-04    1.99803       2.511019E-04   3.194302E-04   7.580738E-04   2.486721E-04   3.901381E-04   4.080604E-04
  a    19  -1.146167E-04   2.511019E-04    1.99755      -6.397745E-04  -4.175429E-04  -6.667075E-04  -4.861392E-04  -5.542951E-04
  a    20  -1.022735E-04   3.194302E-04  -6.397745E-04    1.99865      -3.644597E-04  -1.823731E-04  -2.085047E-04  -4.905835E-04
  a    21  -1.459841E-04   7.580738E-04  -4.175429E-04  -3.644597E-04    1.99756      -3.504893E-05  -1.424592E-03  -4.541057E-04
  a    22   4.383004E-04   2.486721E-04  -6.667075E-04  -1.823731E-04  -3.504893E-05    1.99887       1.242289E-04  -4.428002E-04
  a    23   6.697801E-05   3.901381E-04  -4.861392E-04  -2.085047E-04  -1.424592E-03   1.242289E-04    1.99856      -2.788366E-04
  a    24   9.442248E-04   4.080604E-04  -5.542951E-04  -4.905835E-04  -4.541057E-04  -4.428002E-04  -2.788366E-04    1.99833    
  a    25   1.977869E-04   4.447163E-04  -1.050250E-04  -1.316861E-04   3.003036E-04   3.508275E-04  -4.561596E-04  -3.040001E-04
  a    26   1.207091E-03  -3.559449E-04   1.439842E-03   6.293888E-04   6.651963E-04  -3.012563E-04   2.990794E-04  -3.966144E-04
  a    27   1.303318E-04  -3.656929E-05   2.859389E-04   1.194440E-04   2.001750E-04   5.702257E-05   1.465417E-04   2.590263E-05
  a    28  -6.690756E-04  -4.669685E-04   2.579532E-04   1.036716E-04   5.394431E-04  -5.814493E-05   1.561569E-04   5.475330E-04
  a    29  -6.669318E-06  -7.145661E-06   1.346279E-05   4.867970E-06   9.021686E-06  -2.792401E-06  -1.739951E-06   4.838414E-06
  a    30   1.433114E-06   1.159847E-05   8.302871E-06  -1.664152E-06   2.501475E-05   2.101000E-05   2.753123E-05   5.515382E-06
  a    31   2.931588E-05  -2.179918E-05   1.440349E-05   2.038602E-05   4.155209E-06   1.198045E-05   1.327180E-05  -2.753971E-05
  a    32   9.247411E-06  -4.098540E-06  -5.807196E-07   6.527500E-06   1.655860E-05   1.283632E-05   2.501315E-05  -4.656748E-06
  a    33  -1.226547E-03   3.607567E-03   3.691218E-04  -2.626583E-03  -6.414860E-03  -5.077772E-04  -6.167813E-03  -6.656565E-04
  a    34   3.091003E-05  -1.418794E-04   1.305849E-04  -5.212959E-05  -5.431419E-05  -1.014697E-04   1.060056E-05  -1.832130E-06
  a    35   2.726504E-06  -3.196495E-06   4.844499E-05   6.487912E-05   1.224760E-04   3.827125E-05  -2.907908E-05  -4.626526E-06
  a    36  -3.727354E-05   9.626015E-05  -1.681902E-05  -3.503328E-05  -2.639816E-05  -1.346292E-04  -1.157877E-04  -8.151583E-05
  a    37  -8.049184E-04   2.568751E-03  -5.320472E-03   8.373602E-03   1.418841E-03  -2.334922E-03   6.140259E-04   8.519397E-03
  a    38   5.398056E-03   8.380028E-04   3.135923E-03  -2.948801E-04   1.586463E-03  -3.223317E-03  -7.315951E-04  -5.074116E-03
  a    39   3.284477E-03   2.336958E-03   4.954669E-04   6.421326E-04   1.802868E-03   1.425766E-03   8.690737E-04  -2.373259E-03
  a    40  -4.667720E-03  -3.792188E-03   3.107927E-03  -5.268470E-03   7.409913E-04   4.173170E-03   4.136271E-05  -2.631166E-03
  a    41   8.284104E-04  -6.655957E-04  -1.225865E-03   5.727329E-04   3.422572E-03   8.114094E-04  -9.511954E-04  -9.844851E-04
  a    42  -2.292018E-03   8.043472E-04   5.378640E-04   3.858617E-05   3.212893E-03  -5.191244E-04  -2.942762E-03   1.730820E-03
  a    43  -6.154665E-04  -2.551968E-03   6.514715E-04   7.450242E-03  -2.332021E-04   8.342808E-04   1.280279E-03   2.949024E-03
  a    44   1.425168E-03  -2.789465E-04   8.363679E-04  -1.271753E-03  -8.903893E-04   1.277044E-03   1.254269E-03  -2.507162E-03
  a    45   9.783589E-04   1.353100E-03   8.089232E-04   3.570960E-03  -4.484449E-03  -7.183027E-04  -3.147607E-03  -1.372549E-03
  a    46   1.879218E-03  -4.346058E-03   2.230802E-03   3.250542E-03   7.140560E-03   1.005057E-03   6.091816E-03   1.097773E-03
  a    47  -4.985782E-03  -1.222977E-03  -5.697719E-03  -8.778052E-04   1.166743E-03  -1.272371E-03  -2.256679E-04   3.392522E-04
  a    48   2.222848E-03   2.244793E-03   2.816318E-03   2.527517E-03  -2.575904E-03   2.816071E-03  -1.758005E-03   2.047833E-04
  a    49  -1.146673E-03  -1.035756E-03   5.897747E-04  -4.449688E-04   5.696075E-04   1.155702E-03   1.275205E-03  -2.674166E-03
  a    50  -2.142940E-03   5.716547E-04   4.658623E-05   2.045449E-03   1.773192E-03   4.242484E-04   6.503104E-04   4.607182E-03
  a    51   1.190482E-04  -1.944634E-05  -1.323012E-05  -1.231267E-04  -1.307444E-04  -2.278126E-05  -5.170162E-05  -2.874053E-04
  a    52  -3.551990E-03  -2.347388E-03  -2.846489E-03  -2.305035E-03  -1.317163E-04   1.230272E-03   5.135269E-04   9.856911E-04
  a    53   1.795635E-03  -2.369660E-03  -8.701587E-04  -3.393026E-04   1.789720E-03  -9.735333E-04   1.633894E-03  -1.521320E-03
  a    54  -5.698554E-05   1.344049E-05  -1.542433E-04  -2.114480E-04   6.115395E-05  -6.427069E-06   1.039002E-04  -1.998441E-05
  a    55   2.502374E-04   8.851118E-04   2.148841E-03   3.481777E-03  -1.896343E-03   1.075305E-03  -1.384540E-03   1.859877E-03
  a    56  -2.411412E-03  -2.168227E-03   8.381719E-04  -9.808942E-04   2.523983E-03   1.907528E-03   3.184681E-03   1.479712E-03
  a    57  -6.084630E-04   1.325402E-03  -1.186015E-03  -2.549938E-04  -2.789816E-03   6.196608E-04  -7.193154E-04  -3.809006E-04
  a    58   1.829908E-05   2.731280E-05  -1.129671E-04  -4.352180E-05   1.076179E-04   1.115849E-04   1.943291E-05   6.898620E-06
  a    59  -5.508685E-05   2.756245E-05  -9.418181E-05  -3.120138E-05  -5.392178E-05   1.789580E-04   4.159035E-05   8.582867E-05
  a    60   7.663935E-04  -7.175037E-04  -1.943936E-03  -1.033604E-03  -7.700223E-04   9.610747E-04  -8.782211E-04  -1.305796E-03
  a    61   1.135658E-03  -3.602820E-04  -3.706739E-04  -5.768750E-04  -2.148613E-03  -8.395745E-04  -1.243590E-03  -1.309235E-03
  a    62   3.718623E-04  -3.972533E-06  -4.740325E-04  -3.046688E-04  -1.611256E-04   8.166974E-05  -2.079120E-04  -3.977449E-04
  a    63  -1.906639E-03   1.022601E-03   2.352147E-03   1.345727E-03  -5.964698E-04  -6.664538E-04   1.322706E-04   1.516871E-03
  a    64   3.440269E-04  -5.572500E-04  -1.425992E-04  -4.282846E-04   3.718696E-04  -3.359120E-04   1.878184E-04  -1.873755E-04
  a    65  -7.361002E-05   1.389275E-04   1.049304E-03  -3.101132E-05  -2.052079E-04  -9.383358E-05   6.985005E-04  -3.245397E-04
  a    66  -6.605190E-04  -5.753099E-04   7.792438E-04  -3.632677E-03  -5.250332E-05   4.370552E-04   5.642932E-04  -1.340944E-03
  a    67  -2.314385E-04  -7.367229E-04   3.370971E-04  -2.542992E-04  -1.261110E-03   1.285724E-03  -1.162555E-03  -1.100193E-03
  a    68   4.032536E-05  -6.488791E-05   6.697367E-05   2.910672E-04   5.348250E-04  -1.749012E-04  -5.829666E-04  -4.338017E-05
  a    69  -6.330443E-06   9.896999E-05  -1.412475E-04  -2.176622E-04  -4.171253E-04  -6.024597E-06   4.149892E-04   2.822489E-05
  a    70   5.063860E-05   1.589853E-04  -1.907143E-04  -1.235878E-04  -2.066988E-04  -2.464300E-04   1.241841E-04   3.928312E-05
  a    71  -1.365455E-04  -3.488406E-04   7.463293E-04  -1.635757E-03   1.940171E-04   1.205381E-03  -6.198020E-04  -7.224873E-04
  a    72   6.861878E-04  -2.089328E-03  -2.085471E-04  -2.338200E-03   2.169348E-03  -1.459619E-04   1.714549E-03  -1.674755E-03
  a    73   1.755998E-05  -1.491802E-05  -1.787503E-05  -1.800991E-04   1.120528E-05   2.025301E-05  -8.206170E-05  -1.152164E-04
  a    74  -4.700690E-04  -1.310185E-03   1.303733E-03  -2.497159E-04   1.505230E-03   1.147270E-03   1.063173E-03   2.614467E-04
  a    75  -4.099576E-04  -2.011348E-03  -4.286310E-04  -9.003958E-04   1.993519E-03  -3.130027E-04   9.296894E-04   2.157587E-04
  a    76   2.696212E-04   7.617415E-04  -3.886638E-04   1.100385E-04   1.844121E-04  -4.796874E-04  -3.860022E-04  -4.627333E-04
  a    77  -5.092101E-04   1.994154E-04  -7.190545E-05   7.652675E-04   8.592369E-04   1.896478E-04   6.605684E-04   6.395619E-04
  a    78  -9.075456E-06   1.177837E-04  -1.297938E-03  -7.876508E-04  -6.666055E-06   1.350773E-04  -6.631154E-04  -6.299525E-05
  a    79  -4.690503E-04   9.626470E-04   1.088155E-03   7.488600E-04   5.672686E-05   1.404085E-04  -1.996326E-04   1.463862E-04
  a    80  -1.003384E-04   1.258211E-03  -4.698750E-04   1.710874E-04  -1.189224E-03   2.775326E-05  -4.191412E-04   1.929014E-04
  a    81  -5.041076E-04  -3.628455E-04   1.456902E-03  -7.043857E-04  -3.757319E-05   8.560727E-04  -2.465820E-04   1.596970E-04
  a    82   3.120143E-04  -3.246290E-04  -3.798693E-04  -8.799714E-04   4.111338E-04  -2.207948E-04   3.565444E-04  -9.592848E-04
  a    83   4.130755E-04   1.034471E-03  -1.870235E-05   5.457335E-05   7.335647E-04  -1.586287E-04   4.328985E-04   1.982061E-04
  a    84  -6.636249E-04   2.633227E-04   2.043315E-04  -1.668810E-04  -3.735248E-04   2.345013E-04  -1.133862E-04   2.206355E-04
  a    85   7.335688E-04  -1.885600E-05  -5.394279E-05   2.142465E-04   2.753738E-04  -2.138741E-04   2.922872E-04  -1.947127E-04
  a    86  -1.565031E-04   7.089660E-04  -1.283428E-05  -3.615354E-04  -1.304764E-04   3.458891E-04   1.169327E-04   1.933733E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a    13  -1.740076E-04   1.181004E-04   2.017096E-05  -4.511219E-04  -7.244556E-06  -7.360497E-06  -2.692824E-06  -1.070249E-06
  a    14   1.395065E-04   2.547285E-04  -5.913756E-06   2.416595E-04   3.464890E-06  -1.421766E-05   2.559465E-06  -2.134880E-06
  a    15   1.693188E-04   2.140527E-04  -1.592205E-05   8.115176E-04   8.183226E-06   2.651324E-05   3.460191E-05   2.407125E-05
  a    16   3.397448E-04  -8.773328E-04  -1.447642E-04   6.205678E-04   6.499825E-06  -1.848435E-05   1.921301E-05   3.961251E-06
  a    17   1.977869E-04   1.207091E-03   1.303318E-04  -6.690756E-04  -6.669318E-06   1.433114E-06   2.931588E-05   9.247411E-06
  a    18   4.447163E-04  -3.559449E-04  -3.656929E-05  -4.669685E-04  -7.145661E-06   1.159847E-05  -2.179918E-05  -4.098540E-06
  a    19  -1.050250E-04   1.439842E-03   2.859389E-04   2.579532E-04   1.346279E-05   8.302871E-06   1.440349E-05  -5.807196E-07
  a    20  -1.316861E-04   6.293888E-04   1.194440E-04   1.036716E-04   4.867970E-06  -1.664152E-06   2.038602E-05   6.527500E-06
  a    21   3.003036E-04   6.651963E-04   2.001750E-04   5.394431E-04   9.021686E-06   2.501475E-05   4.155209E-06   1.655860E-05
  a    22   3.508275E-04  -3.012563E-04   5.702257E-05  -5.814493E-05  -2.792401E-06   2.101000E-05   1.198045E-05   1.283632E-05
  a    23  -4.561596E-04   2.990794E-04   1.465417E-04   1.561569E-04  -1.739951E-06   2.753123E-05   1.327180E-05   2.501315E-05
  a    24  -3.040001E-04  -3.966144E-04   2.590263E-05   5.475330E-04   4.838414E-06   5.515382E-06  -2.753971E-05  -4.656748E-06
  a    25    1.99825       4.261820E-05   8.625656E-05  -5.251716E-04  -1.321892E-05   7.728490E-06   1.821803E-05   5.649153E-06
  a    26   4.261820E-05    1.99576      -5.396261E-04   2.149658E-04   7.464760E-06  -2.256170E-05   1.433061E-06   4.667837E-06
  a    27   8.625656E-05  -5.396261E-04    1.99935       2.993295E-04  -1.117991E-04   7.097977E-05  -2.333027E-04  -1.032456E-04
  a    28  -5.251716E-04   2.149658E-04   2.993295E-04    1.99650      -1.833231E-05   1.906055E-05  -1.534050E-05  -8.175703E-07
  a    29  -1.321892E-05   7.464760E-06  -1.117991E-04  -1.833231E-05    1.99388       1.916300E-04  -2.673799E-03  -1.904622E-04
  a    30   7.728490E-06  -2.256170E-05   7.097977E-05   1.906055E-05   1.916300E-04    1.97329      -2.575034E-03  -1.709803E-03
  a    31   1.821803E-05   1.433061E-06  -2.333027E-04  -1.534050E-05  -2.673799E-03  -2.575034E-03    1.92655      -2.796581E-02
  a    32   5.649153E-06   4.667837E-06  -1.032456E-04  -8.175703E-07  -1.904622E-04  -1.709803E-03  -2.796581E-02    1.97717    
  a    33  -4.469278E-03   1.506106E-03   2.248585E-03  -1.018581E-02  -2.597825E-05  -1.393807E-05   7.593948E-04   5.519524E-04
  a    34  -3.690652E-05  -9.635245E-05   1.103391E-03   1.927427E-04  -1.244336E-02   2.339143E-02  -4.167214E-02   2.507249E-02
  a    35   1.375629E-04  -1.452482E-04   3.093753E-04  -5.162083E-05  -1.919630E-02   2.742579E-02  -3.329820E-02   6.138724E-02
  a    36  -6.786518E-05   3.146979E-04  -2.270690E-03  -3.514345E-04   1.090222E-02   4.526290E-03   1.117599E-02   1.381996E-02
  a    37   1.716364E-03   1.176560E-02   1.946453E-03  -5.873946E-03   5.683848E-05  -8.637724E-05   1.285475E-05   4.013557E-06
  a    38  -1.194397E-03  -1.319555E-02  -3.112658E-03   5.918956E-03   3.701731E-06  -4.483880E-04  -2.075747E-04   5.675042E-05
  a    39   3.684301E-04  -9.355558E-03  -5.418549E-04   3.308065E-03  -5.572059E-06   2.569600E-04   2.053014E-04  -7.406027E-06
  a    40  -1.621883E-04   5.434653E-03   3.162754E-04   1.179629E-03  -6.749661E-05  -4.188689E-06  -1.248024E-04  -7.420653E-05
  a    41   2.374758E-03   2.536441E-03   4.642915E-04  -6.381460E-04  -5.232831E-05  -2.530548E-06   2.326141E-05   5.902165E-05
  a    42   5.382053E-04   1.976555E-03  -2.926661E-04   7.185067E-04  -1.348068E-04  -2.218845E-04  -5.451247E-05   1.252394E-04
  a    43   5.326471E-03  -3.856982E-03  -5.849765E-04  -6.597325E-04  -8.245601E-05   1.506039E-04  -1.989531E-05  -6.978330E-05
  a    44  -6.885077E-05   1.067561E-03  -4.143360E-04   4.569947E-03   7.554194E-05  -1.173926E-04   2.027123E-05  -5.038283E-05
  a    45   2.338374E-03  -2.433856E-03  -2.349828E-04  -1.257478E-03  -2.631987E-05   1.102081E-04  -3.387520E-05   6.243323E-05
  a    46   1.144930E-03  -8.898473E-04  -1.575820E-03   2.291439E-03  -5.176157E-05  -2.080686E-04  -1.243974E-04  -2.306116E-05
  a    47  -1.714330E-03   3.378024E-03   3.029456E-04  -4.374109E-03   2.594731E-05  -1.691761E-04  -2.168293E-05   3.997969E-05
  a    48   3.778748E-04  -6.504958E-04  -1.499795E-03   3.197153E-03   5.159185E-05  -2.124927E-04  -1.492646E-04   3.285299E-05
  a    49   1.170508E-03  -4.323383E-03  -1.379509E-03   4.860461E-03   3.972287E-05  -5.109609E-05  -4.806699E-05  -4.793108E-05
  a    50  -1.531925E-03   2.668991E-03   4.029940E-04  -5.728406E-04  -1.181339E-04  -1.372898E-04  -3.944003E-04   2.445315E-04
  a    51   7.954269E-05  -1.621735E-04  -1.522938E-04   3.045301E-05  -2.008452E-03   8.545757E-04  -6.565299E-03   4.860090E-03
  a    52   1.139134E-03   1.222402E-02   2.050181E-03  -3.587373E-03  -1.409223E-05   1.784057E-04   2.130780E-05   1.219536E-05
  a    53   2.171417E-03  -1.377398E-03  -4.752018E-04   1.015623E-03  -5.946354E-05   1.161648E-04  -1.145188E-04   2.405186E-05
  a    54  -8.703179E-05   3.064080E-04  -1.418729E-04  -1.312760E-04   1.656331E-03   6.009016E-03  -2.745061E-03   3.976490E-04
  a    55   1.716280E-03  -4.939297E-03  -8.292058E-04   1.423554E-03   8.545580E-05   2.450525E-04  -1.822527E-04  -6.196397E-05
  a    56   3.132161E-03   2.470554E-03  -4.696812E-04   8.318982E-04   8.246060E-07  -2.610546E-04  -7.935417E-05  -4.745833E-05
  a    57   2.463562E-03   1.218423E-03  -9.499274E-05  -1.395793E-03  -5.009794E-05   1.335306E-07  -1.887105E-04  -3.792711E-05
  a    58  -1.555621E-04   9.137063E-05  -3.693077E-04   1.274711E-04  -1.176832E-03   1.468662E-03  -9.117582E-03  -1.985330E-03
  a    59  -4.192652E-05   1.071859E-04  -3.774550E-05   2.244572E-04  -1.735725E-03   6.515499E-03   5.187561E-03   6.464251E-04
  a    60  -5.740976E-04   2.485743E-03   2.940438E-04   1.818096E-03   1.653777E-04  -5.090708E-04   1.684532E-04   3.024915E-05
  a    61   4.964736E-04  -2.764264E-05   1.949541E-04  -2.751113E-03  -2.325068E-05   1.810284E-04  -8.064910E-04   6.076828E-05
  a    62   9.146560E-05  -2.490418E-06   4.500559E-04  -8.445937E-04  -2.927457E-04  -8.485291E-04   4.025491E-03  -1.104249E-03
  a    63  -9.337797E-05  -1.055406E-04  -3.934491E-04   4.050356E-03   3.891945E-05  -3.000090E-04   4.147763E-04  -1.589539E-04
  a    64  -2.483010E-04   3.030234E-04  -5.552309E-04  -1.063650E-03  -1.461211E-07  -2.774638E-05   1.544984E-04  -2.959721E-04
  a    65   9.729574E-04  -1.328484E-03  -5.005069E-04   9.203633E-04  -9.091339E-06   1.141951E-05   2.265207E-05  -2.976293E-05
  a    66   2.489270E-04   2.423490E-03   3.846856E-04   4.902176E-04   1.461983E-05   1.215662E-05  -1.065982E-05   2.362462E-06
  a    67  -7.814516E-04   1.114528E-03   2.777561E-04   4.769019E-04  -1.978111E-05   2.438752E-04  -1.004556E-04  -1.114267E-04
  a    68   8.133381E-04   7.518604E-05   1.728714E-04   6.030772E-04  -9.795630E-04   2.191705E-03  -1.922548E-03  -7.010940E-04
  a    69  -5.679025E-04  -1.886787E-05   4.908836E-05  -4.661796E-04  -1.583877E-03   2.300697E-03  -2.307465E-03  -3.976498E-04
  a    70  -7.790895E-05   6.499609E-05   8.775846E-04   1.445067E-04   4.585167E-04   6.931176E-04  -1.187824E-04  -8.439435E-04
  a    71  -9.149132E-04  -1.011162E-04  -3.205126E-04   7.383893E-04  -1.865835E-04  -8.606690E-05   3.143793E-05   1.606998E-04
  a    72   3.015090E-04  -1.954128E-04  -2.235188E-05   5.369932E-04  -9.205866E-05   2.602666E-05   4.471905E-05   7.168190E-05
  a    73  -8.573577E-05   4.577236E-05  -3.310738E-04  -2.313759E-05   2.791918E-03   1.110564E-03  -1.418599E-03  -2.403288E-03
  a    74  -6.828291E-04  -1.303898E-03  -7.389726E-05  -8.467402E-04   4.261444E-05   1.255061E-04  -9.061109E-05  -1.749720E-04
  a    75  -3.672776E-04   9.032963E-04   1.236246E-04  -7.967976E-04   2.077217E-05  -8.061046E-07   9.831739E-06  -1.534300E-07
  a    76  -3.970368E-04  -4.660898E-06   1.821712E-04  -1.232172E-03   3.777085E-05   3.228531E-05  -2.641668E-06  -1.811398E-05
  a    77   4.438155E-04   6.004853E-04  -1.531519E-05  -2.300603E-04   5.250367E-06   3.677899E-05  -4.613378E-05  -1.604962E-05
  a    78   3.007916E-04   8.891391E-04  -5.092800E-06   7.570299E-04  -8.037885E-05  -7.206549E-05   8.736009E-05   1.184827E-04
  a    79  -4.706254E-05  -3.470453E-04   1.784139E-05   7.225955E-04  -1.604860E-05   6.255420E-05   2.025844E-05   6.832138E-05
  a    80   1.067631E-04   1.016007E-03  -8.450904E-05   1.688618E-03   1.940914E-05  -4.437176E-05  -1.817311E-05  -4.340074E-05
  a    81  -4.262408E-04  -6.687554E-04  -1.501743E-04  -1.849838E-04  -5.829007E-06  -1.650640E-05   1.768242E-06   6.425033E-06
  a    82  -1.979932E-04   1.119240E-03   2.108751E-04   1.853520E-04   1.013895E-05   1.146955E-05   7.503332E-06  -5.518922E-07
  a    83  -5.118210E-04  -7.559238E-04  -2.803162E-04  -1.797953E-04  -1.710033E-05  -6.153549E-05   7.061839E-06   9.585090E-06
  a    84  -6.381766E-05   2.041649E-04   4.544920E-05   5.230127E-04   4.936499E-06   2.530515E-05  -3.715458E-05  -3.820347E-05
  a    85   3.186138E-04  -1.590817E-04   7.643329E-05   1.584595E-05   1.053288E-05   3.817882E-05  -2.235281E-05  -2.781054E-05
  a    86  -9.080112E-05   1.701711E-04  -1.377227E-04   2.518553E-04  -8.396548E-07  -3.520450E-05   3.812313E-06  -1.155313E-06

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a    13   2.150861E-03  -6.721793E-05   3.992754E-05  -2.718125E-05  -1.947547E-03   1.420484E-05   1.738162E-04   1.844362E-03
  a    14  -1.728560E-03   1.624446E-04  -1.952925E-06   7.315513E-05  -1.740110E-03  -7.352961E-04  -1.069494E-03   2.207201E-03
  a    15  -2.091189E-03  -3.421544E-04   4.432906E-05  -5.817368E-05  -4.886646E-04  -2.739731E-03   4.164744E-04   3.338774E-03
  a    16   7.166706E-04   1.005586E-04   4.711024E-05  -3.613733E-05   3.978322E-03  -2.383957E-03  -1.747975E-03   1.512922E-03
  a    17  -1.226547E-03   3.091003E-05   2.726504E-06  -3.727354E-05  -8.049184E-04   5.398056E-03   3.284477E-03  -4.667720E-03
  a    18   3.607567E-03  -1.418794E-04  -3.196495E-06   9.626015E-05   2.568751E-03   8.380028E-04   2.336958E-03  -3.792188E-03
  a    19   3.691218E-04   1.305849E-04   4.844499E-05  -1.681902E-05  -5.320472E-03   3.135923E-03   4.954669E-04   3.107927E-03
  a    20  -2.626583E-03  -5.212959E-05   6.487912E-05  -3.503328E-05   8.373602E-03  -2.948801E-04   6.421326E-04  -5.268470E-03
  a    21  -6.414860E-03  -5.431419E-05   1.224760E-04  -2.639816E-05   1.418841E-03   1.586463E-03   1.802868E-03   7.409913E-04
  a    22  -5.077772E-04  -1.014697E-04   3.827125E-05  -1.346292E-04  -2.334922E-03  -3.223317E-03   1.425766E-03   4.173170E-03
  a    23  -6.167813E-03   1.060056E-05  -2.907908E-05  -1.157877E-04   6.140259E-04  -7.315951E-04   8.690737E-04   4.136271E-05
  a    24  -6.656565E-04  -1.832130E-06  -4.626526E-06  -8.151583E-05   8.519397E-03  -5.074116E-03  -2.373259E-03  -2.631166E-03
  a    25  -4.469278E-03  -3.690652E-05   1.375629E-04  -6.786518E-05   1.716364E-03  -1.194397E-03   3.684301E-04  -1.621883E-04
  a    26   1.506106E-03  -9.635245E-05  -1.452482E-04   3.146979E-04   1.176560E-02  -1.319555E-02  -9.355558E-03   5.434653E-03
  a    27   2.248585E-03   1.103391E-03   3.093753E-04  -2.270690E-03   1.946453E-03  -3.112658E-03  -5.418549E-04   3.162754E-04
  a    28  -1.018581E-02   1.927427E-04  -5.162083E-05  -3.514345E-04  -5.873946E-03   5.918956E-03   3.308065E-03   1.179629E-03
  a    29  -2.597825E-05  -1.244336E-02  -1.919630E-02   1.090222E-02   5.683848E-05   3.701731E-06  -5.572059E-06  -6.749661E-05
  a    30  -1.393807E-05   2.339143E-02   2.742579E-02   4.526290E-03  -8.637724E-05  -4.483880E-04   2.569600E-04  -4.188689E-06
  a    31   7.593948E-04  -4.167214E-02  -3.329820E-02   1.117599E-02   1.285475E-05  -2.075747E-04   2.053014E-04  -1.248024E-04
  a    32   5.519524E-04   2.507249E-02   6.138724E-02   1.381996E-02   4.013557E-06   5.675042E-05  -7.406027E-06  -7.420653E-05
  a    33    1.00014      -3.740682E-03   7.849889E-04  -3.970155E-04  -5.169696E-03   2.169473E-04   5.107767E-04   7.634205E-03
  a    34  -3.740682E-03   0.982304       0.124042      -2.073511E-02   4.216006E-04   3.075557E-03  -2.847193E-03   5.537569E-04
  a    35   7.849889E-04   0.124042       0.112648      -9.028996E-03   4.726901E-05   2.300292E-04  -2.637376E-04  -1.207048E-05
  a    36  -3.970155E-04  -2.073511E-02  -9.028996E-03   2.986155E-02   2.361320E-05   9.875218E-05  -1.447663E-04   2.274613E-05
  a    37  -5.169696E-03   4.216006E-04   4.726901E-05   2.361320E-05   2.135869E-03  -1.828023E-03  -9.213867E-04   6.884416E-04
  a    38   2.169473E-04   3.075557E-03   2.300292E-04   9.875218E-05  -1.828023E-03   2.859506E-03   1.402714E-03  -1.136578E-03
  a    39   5.107767E-04  -2.847193E-03  -2.637376E-04  -1.447663E-04  -9.213867E-04   1.402714E-03   1.213744E-03  -4.844968E-04
  a    40   7.634205E-03   5.537569E-04  -1.207048E-05   2.274613E-05   6.884416E-04  -1.136578E-03  -4.844968E-04   1.481749E-03
  a    41  -2.863052E-03   2.256778E-04  -1.269021E-04   3.889273E-05   1.302654E-04  -3.625149E-04  -3.066466E-04  -2.138915E-04
  a    42   2.877071E-03   1.932704E-03  -1.240858E-04   9.773955E-05   2.952949E-04  -1.722694E-04  -8.665453E-05   1.827511E-04
  a    43   6.753612E-03  -3.869417E-04  -1.086733E-04   6.005686E-06  -4.907493E-05   3.647702E-04  -2.245219E-05   3.358807E-04
  a    44   1.340073E-03   1.042576E-04   2.073131E-05  -2.811952E-06  -1.281217E-04   9.765421E-04   4.865963E-04  -6.534414E-04
  a    45  -2.006465E-03   2.850867E-04  -3.529967E-05   4.430743E-05   8.569289E-05  -5.060220E-04   1.687626E-04   4.948722E-04
  a    46   3.514851E-03   5.946917E-04   7.792032E-05   3.441894E-06   4.709866E-06  -5.848798E-04  -1.201349E-04   8.920587E-04
  a    47   1.605997E-04   6.356710E-04   7.342255E-05   5.919338E-06   3.961325E-04  -4.933284E-04  -3.232918E-05   1.750557E-04
  a    48  -4.773919E-04   7.049666E-04   8.097729E-05   4.405648E-05  -1.378316E-04   2.530903E-04   2.606306E-04  -2.469246E-04
  a    49   6.572726E-03   1.085659E-04   5.414765E-05   1.968241E-05  -4.444055E-04   6.055979E-04   1.464861E-04  -9.287832E-05
  a    50   1.297852E-02  -6.068120E-04  -7.864438E-05   1.460948E-05   3.164065E-04  -6.309035E-04  -3.472118E-04   6.760208E-04
  a    51  -8.396567E-04  -1.030771E-02  -1.983303E-03   3.995826E-04  -2.392655E-05   1.336142E-06   5.836332E-05  -4.679298E-05
  a    52   2.329429E-03  -1.171630E-04  -3.726612E-05   1.279430E-06   9.391121E-04  -1.055382E-03  -6.603769E-04   3.748046E-04
  a    53   1.372692E-02   3.563044E-04  -5.475896E-05   1.483423E-05  -4.753313E-04   5.862118E-04   2.496788E-04  -2.347758E-04
  a    54  -5.909447E-06   1.353089E-03   7.810057E-05   1.091198E-03   2.044987E-05   2.203771E-05  -3.501605E-05  -1.579482E-05
  a    55  -1.317867E-03  -1.812477E-04   1.370898E-05   4.602671E-05  -8.491843E-05  -3.929595E-05   7.373731E-05   3.636617E-04
  a    56   2.872226E-04  -2.096205E-05   1.296145E-05  -3.165336E-06   2.419795E-04  -1.671282E-04  -1.929943E-04  -3.249455E-05
  a    57  -1.032546E-03  -6.307713E-06  -9.661614E-05   3.170786E-05   2.642997E-04   5.028651E-05  -1.892264E-04  -6.905455E-05
  a    58   8.301580E-06  -4.268241E-03  -3.654147E-03   7.530068E-04   1.778805E-06  -1.818662E-05   5.196921E-05  -7.824928E-06
  a    59  -8.968112E-05   2.043629E-03   4.234602E-04   3.693465E-04   1.528040E-05  -1.643193E-05   4.420611E-05  -1.434565E-05
  a    60  -7.964098E-04  -2.496957E-05   1.347413E-04  -6.934526E-05   2.311538E-04   2.362139E-04   2.176846E-04  -2.325570E-04
  a    61   8.090436E-05   2.889891E-04  -3.467098E-04   4.960516E-05  -1.304118E-04   7.266296E-05  -8.966158E-05  -8.691750E-05
  a    62   1.623270E-04  -3.402022E-03   2.022553E-03  -7.988557E-05  -5.594375E-05  -6.408130E-05   8.807321E-05  -3.583050E-05
  a    63  -5.626508E-04  -4.842837E-04   2.829995E-04  -1.406446E-06   1.748755E-04   5.907755E-06  -5.856240E-05  -3.016414E-05
  a    64   3.918783E-04  -1.770758E-04   2.819062E-04   6.310121E-05  -2.573515E-05  -4.792106E-05  -2.969190E-05   8.771653E-05
  a    65  -5.985545E-05   5.077610E-05   4.021087E-05   1.623271E-05  -7.847962E-06   1.285666E-04   9.317580E-05  -4.317920E-05
  a    66   6.786966E-04  -7.716085E-05  -8.034352E-06  -1.272494E-05   8.435563E-05  -5.335721E-05  -3.691279E-05   1.301536E-04
  a    67   4.652184E-04  -3.529664E-05   4.759992E-05   5.648985E-05  -3.004902E-05  -1.050680E-04  -8.976901E-05  -1.032502E-04
  a    68  -1.378385E-04  -1.387361E-03   7.045401E-04   4.520623E-04   2.899347E-05   3.269394E-05   1.706354E-05   4.893864E-06
  a    69   6.121254E-05  -1.748150E-03   6.941829E-04   4.193207E-04  -1.588000E-05  -1.152425E-05  -3.617004E-05  -1.028128E-05
  a    70  -1.115048E-04   4.603738E-04   2.268380E-04   2.689373E-04   1.883883E-05   4.948754E-05  -4.383732E-05  -1.003163E-05
  a    71   7.719252E-04   2.747600E-04  -7.564007E-05  -1.522206E-05  -3.619510E-05   2.966846E-05   6.815885E-05   3.765426E-05
  a    72   1.486001E-03   6.329118E-05  -3.394262E-05  -4.998825E-06  -1.521515E-04   2.054230E-04   2.059849E-04  -4.650153E-05
  a    73   7.919950E-05  -2.412894E-03   1.077042E-03   2.066610E-04  -6.339608E-06   5.807635E-06   7.160262E-06  -1.424434E-05
  a    74   6.225760E-04  -1.479577E-04   1.014688E-04   5.406151E-05  -9.173539E-05  -4.776632E-05  -4.433617E-05   1.493533E-04
  a    75   1.427509E-05   5.487510E-05  -7.811029E-06   2.264071E-07  -3.710172E-05  -7.877974E-05  -1.092081E-04   5.870823E-05
  a    76  -8.808212E-04   7.650678E-05  -2.133589E-05  -6.925285E-06  -7.728088E-05   6.990573E-05  -2.821146E-05  -4.440156E-05
  a    77   6.835780E-04  -1.412919E-05   5.138013E-06   2.185884E-06   1.909938E-04  -2.266580E-04  -1.361109E-05   1.680588E-04
  a    78  -4.994038E-04   8.911726E-05  -4.605290E-05  -2.294917E-05   1.940091E-06   4.054484E-05  -3.741818E-05  -1.109581E-04
  a    79   2.836641E-04   3.715539E-05  -6.565557E-05  -1.680437E-05   3.378985E-05  -4.278721E-05   2.830441E-05   3.321532E-05
  a    80  -6.015443E-04  -3.036565E-05   3.168957E-05   2.335549E-05   8.631804E-05  -5.312685E-05  -2.615374E-05  -6.357782E-05
  a    81   4.243026E-04   1.416110E-05  -4.021209E-07  -2.219190E-06  -3.994430E-05  -7.398405E-05  -3.520551E-05   9.888944E-05
  a    82  -8.818474E-04  -1.028676E-06  -6.452630E-06  -8.178193E-07   2.206090E-06   2.892391E-05   6.015739E-05  -1.041065E-05
  a    83   5.931369E-04   4.268190E-05   1.253807E-05   5.209608E-06  -3.947979E-05   5.325286E-05   6.132044E-05   1.971668E-05
  a    84  -4.651979E-04   6.206723E-05   4.682731E-06   1.874739E-05  -2.051488E-05   6.641898E-05  -5.368597E-06  -2.477580E-05
  a    85   1.522960E-04   4.823775E-05   4.836320E-06   9.622649E-06   4.343070E-05  -4.320335E-05   1.785129E-06   2.710645E-05
  a    86  -4.833053E-04  -1.886936E-05  -5.686963E-07  -2.407084E-06   4.106692E-05  -5.686416E-06  -2.938715E-05   4.330249E-05

               a    41        a    42        a    43        a    44        a    45        a    46        a    47        a    48
  a    13  -1.572191E-03   1.135837E-03   3.437938E-03   1.694673E-03  -4.662472E-04  -1.345327E-03   9.448230E-04   9.490193E-04
  a    14  -1.847532E-03   7.804386E-04   9.721728E-04  -8.069529E-04   6.711720E-05  -1.027492E-03   1.088288E-03   1.266444E-03
  a    15  -8.294292E-04   5.129094E-04   1.227565E-03  -1.336561E-03   1.446548E-03   5.188792E-04   8.348140E-04  -7.179945E-05
  a    16  -7.698112E-04   5.491315E-05   1.156757E-03  -4.773680E-04   2.588689E-03   4.036235E-04   2.197495E-03  -8.831899E-04
  a    17   8.284104E-04  -2.292018E-03  -6.154665E-04   1.425168E-03   9.783589E-04   1.879218E-03  -4.985782E-03   2.222848E-03
  a    18  -6.655957E-04   8.043472E-04  -2.551968E-03  -2.789465E-04   1.353100E-03  -4.346058E-03  -1.222977E-03   2.244793E-03
  a    19  -1.225865E-03   5.378640E-04   6.514715E-04   8.363679E-04   8.089232E-04   2.230802E-03  -5.697719E-03   2.816318E-03
  a    20   5.727329E-04   3.858617E-05   7.450242E-03  -1.271753E-03   3.570960E-03   3.250542E-03  -8.778052E-04   2.527517E-03
  a    21   3.422572E-03   3.212893E-03  -2.332021E-04  -8.903893E-04  -4.484449E-03   7.140560E-03   1.166743E-03  -2.575904E-03
  a    22   8.114094E-04  -5.191244E-04   8.342808E-04   1.277044E-03  -7.183027E-04   1.005057E-03  -1.272371E-03   2.816071E-03
  a    23  -9.511954E-04  -2.942762E-03   1.280279E-03   1.254269E-03  -3.147607E-03   6.091816E-03  -2.256679E-04  -1.758005E-03
  a    24  -9.844851E-04   1.730820E-03   2.949024E-03  -2.507162E-03  -1.372549E-03   1.097773E-03   3.392522E-04   2.047833E-04
  a    25   2.374758E-03   5.382053E-04   5.326471E-03  -6.885077E-05   2.338374E-03   1.144930E-03  -1.714330E-03   3.778748E-04
  a    26   2.536441E-03   1.976555E-03  -3.856982E-03   1.067561E-03  -2.433856E-03  -8.898473E-04   3.378024E-03  -6.504958E-04
  a    27   4.642915E-04  -2.926661E-04  -5.849765E-04  -4.143360E-04  -2.349828E-04  -1.575820E-03   3.029456E-04  -1.499795E-03
  a    28  -6.381460E-04   7.185067E-04  -6.597325E-04   4.569947E-03  -1.257478E-03   2.291439E-03  -4.374109E-03   3.197153E-03
  a    29  -5.232831E-05  -1.348068E-04  -8.245601E-05   7.554194E-05  -2.631987E-05  -5.176157E-05   2.594731E-05   5.159185E-05
  a    30  -2.530548E-06  -2.218845E-04   1.506039E-04  -1.173926E-04   1.102081E-04  -2.080686E-04  -1.691761E-04  -2.124927E-04
  a    31   2.326141E-05  -5.451247E-05  -1.989531E-05   2.027123E-05  -3.387520E-05  -1.243974E-04  -2.168293E-05  -1.492646E-04
  a    32   5.902165E-05   1.252394E-04  -6.978330E-05  -5.038283E-05   6.243323E-05  -2.306116E-05   3.997969E-05   3.285299E-05
  a    33  -2.863052E-03   2.877071E-03   6.753612E-03   1.340073E-03  -2.006465E-03   3.514851E-03   1.605997E-04  -4.773919E-04
  a    34   2.256778E-04   1.932704E-03  -3.869417E-04   1.042576E-04   2.850867E-04   5.946917E-04   6.356710E-04   7.049666E-04
  a    35  -1.269021E-04  -1.240858E-04  -1.086733E-04   2.073131E-05  -3.529967E-05   7.792032E-05   7.342255E-05   8.097729E-05
  a    36   3.889273E-05   9.773955E-05   6.005686E-06  -2.811952E-06   4.430743E-05   3.441894E-06   5.919338E-06   4.405648E-05
  a    37   1.302654E-04   2.952949E-04  -4.907493E-05  -1.281217E-04   8.569289E-05   4.709866E-06   3.961325E-04  -1.378316E-04
  a    38  -3.625149E-04  -1.722694E-04   3.647702E-04   9.765421E-04  -5.060220E-04  -5.848798E-04  -4.933284E-04   2.530903E-04
  a    39  -3.066466E-04  -8.665453E-05  -2.245219E-05   4.865963E-04   1.687626E-04  -1.201349E-04  -3.232918E-05   2.606306E-04
  a    40  -2.138915E-04   1.827511E-04   3.358807E-04  -6.534414E-04   4.948722E-04   8.920587E-04   1.750557E-04  -2.469246E-04
  a    41   8.567647E-04  -3.117135E-04  -3.670889E-04   5.292120E-07   1.637193E-04   1.925043E-04  -3.158185E-05  -8.714818E-05
  a    42  -3.117135E-04   5.388530E-04   2.737518E-04   3.267227E-04  -4.475338E-04  -8.664908E-05   2.488474E-04  -7.448922E-06
  a    43  -3.670889E-04   2.737518E-04   2.041865E-03   5.712386E-04  -6.387204E-04  -6.563049E-05  -2.643890E-05   4.196050E-05
  a    44   5.292120E-07   3.267227E-04   5.712386E-04   2.755703E-03  -4.902608E-04  -1.111740E-03   3.104536E-04   1.819476E-04
  a    45   1.637193E-04  -4.475338E-04  -6.387204E-04  -4.902608E-04   2.069731E-03   7.570608E-04  -8.014298E-05  -2.030647E-04
  a    46   1.925043E-04  -8.664908E-05  -6.563049E-05  -1.111740E-03   7.570608E-04   2.355239E-03  -2.603457E-04  -5.277076E-04
  a    47  -3.158185E-05   2.488474E-04  -2.643890E-05   3.104536E-04  -8.014298E-05  -2.603457E-04   9.696740E-04   1.839071E-04
  a    48  -8.714818E-05  -7.448922E-06   4.196050E-05   1.819476E-04  -2.030647E-04  -5.277076E-04   1.839071E-04   6.623509E-04
  a    49   1.477564E-04  -1.420570E-04   4.251108E-04   1.139140E-04  -1.350604E-04   8.509412E-05  -4.060625E-04  -8.821697E-05
  a    50  -1.922166E-04   3.891988E-04   2.926536E-04  -2.323697E-04  -1.324742E-04   5.918382E-04   2.540097E-04  -1.098710E-04
  a    51   1.423000E-05  -3.856125E-05  -1.051621E-05   1.485126E-05   5.091147E-06  -4.483018E-05  -2.446668E-05  -2.685828E-06
  a    52   2.124114E-04   1.660246E-05   1.046786E-04  -8.296222E-05  -1.165173E-05  -2.576223E-04   2.087170E-04  -7.996857E-05
  a    53  -2.978450E-04   8.209913E-05   4.373479E-04   2.701453E-04  -7.396106E-04  -5.257604E-04  -3.575811E-06   2.618772E-04
  a    54   6.342764E-06   1.186113E-05  -2.351522E-05   2.909497E-05  -4.034255E-05  -9.592810E-06   2.290494E-05   2.238632E-05
  a    55  -1.512231E-04  -6.137522E-05   3.176855E-04  -2.413966E-04   5.583324E-04   3.364012E-04  -6.177638E-05   3.325986E-05
  a    56   2.825508E-04  -5.493465E-05  -1.535830E-04   3.459586E-04   1.388601E-04   3.213859E-05  -2.469000E-04  -2.439060E-04
  a    57  -7.373444E-05   1.563867E-04   6.750560E-04   3.097793E-04  -7.147119E-04  -6.741244E-04   2.228317E-05   6.694560E-05
  a    58   8.522066E-06   9.600635E-06  -6.981042E-07   5.238653E-05  -3.938369E-06   3.940497E-06   3.029769E-05   6.653752E-06
  a    59   1.086798E-05   1.443487E-05   3.691185E-05   8.214880E-05  -2.073504E-05  -4.090232E-05   8.613980E-06  -1.850161E-05
  a    60  -3.115251E-05   2.065629E-04   2.996138E-04   1.129242E-03  -3.235097E-04  -4.498113E-04   3.668280E-04   2.536207E-05
  a    61  -1.121976E-04  -1.413756E-04  -1.269617E-04  -4.169234E-04  -1.659393E-06  -2.350761E-04  -2.629525E-04   1.100093E-05
  a    62  -2.670446E-05  -6.819166E-05  -5.140705E-05  -9.541480E-05  -1.932240E-05  -2.045348E-05  -3.445505E-05  -6.650006E-06
  a    63   9.684676E-05   1.593378E-05   2.475245E-04   2.099453E-04  -1.704689E-04  -3.992354E-04  -1.972808E-05   8.237401E-05
  a    64  -2.842314E-05   5.613671E-06  -2.739583E-05  -3.082007E-05   1.094203E-04   2.343943E-04  -7.490750E-06  -1.066567E-04
  a    65  -6.642029E-05  -1.809838E-05   8.580423E-06  -1.726358E-04   6.050667E-05  -1.352868E-04  -1.892682E-04   3.616907E-05
  a    66  -1.789155E-04   9.693868E-05  -2.763060E-05   3.402574E-05  -1.352191E-04  -8.294473E-05  -4.404525E-05  -4.337745E-05
  a    67   1.075240E-04   8.651086E-06  -7.094482E-06   1.397185E-04  -1.263122E-04  -1.291386E-04   1.134912E-04   4.954599E-05
  a    68  -1.414454E-05   1.468875E-05   3.399590E-05   7.249033E-05   2.035589E-05  -1.278609E-05   1.617038E-05   6.281931E-06
  a    69  -7.498816E-06  -2.175987E-05  -2.317401E-05  -5.173371E-05  -1.618130E-05  -8.697561E-06  -1.504661E-05   2.105368E-07
  a    70  -1.771515E-05   7.912983E-06   1.565521E-05   1.599205E-05  -6.799282E-06  -6.188080E-05  -1.217602E-06   1.215885E-05
  a    71   3.918074E-05  -4.133442E-05  -7.662993E-05   3.166318E-05   1.024109E-04  -7.898404E-05   6.097352E-05   5.781268E-05
  a    72   1.301722E-04  -9.406219E-05  -3.995637E-05   1.974340E-04   2.131082E-04   3.755901E-05   2.366510E-05  -1.321402E-05
  a    73  -2.910645E-06  -2.675976E-05  -1.289348E-05   1.224093E-05  -3.220873E-06  -2.052057E-05   1.218810E-05   1.928504E-05
  a    74   4.165109E-05  -2.815932E-05  -9.231518E-05  -2.187249E-04   1.650936E-04   4.044288E-04  -1.062694E-04  -1.099461E-04
  a    75   2.992071E-05  -1.563341E-05  -5.057137E-05  -2.106172E-04  -4.424865E-05   2.374114E-04  -1.054960E-04  -6.867134E-05
  a    76  -9.445411E-05   5.188839E-05  -2.464574E-05  -4.492621E-05  -1.837681E-04   2.967680E-05  -2.915479E-05  -1.578420E-05
  a    77   3.566704E-05  -1.114231E-05  -7.532982E-05  -1.245464E-04   2.004580E-04   1.272486E-04   8.857949E-05   1.121636E-05
  a    78   2.528031E-05  -1.207771E-05   6.757078E-05  -1.421479E-06  -1.885846E-04  -2.478607E-04  -2.942693E-05   6.526297E-05
  a    79   2.425308E-05   3.523435E-05  -3.739485E-06   1.215963E-04   1.099237E-04   9.628535E-05   5.205690E-05  -2.288104E-05
  a    80   9.105961E-06   2.285841E-05   3.000632E-05   1.392104E-04  -8.858823E-05  -1.369025E-04   7.058533E-05   5.587750E-05
  a    81   2.429072E-05  -3.310018E-05  -3.192895E-05  -1.690363E-04   1.178259E-04   1.750968E-04  -3.003362E-05  -1.806412E-05
  a    82  -7.832227E-05   4.980027E-05   3.288506E-05   1.150171E-04  -6.664017E-05  -5.217337E-05   8.955724E-05   3.309919E-05
  a    83  -1.455752E-05   2.001527E-06  -5.614022E-07   5.619785E-05   1.112515E-04   8.779670E-05   1.944247E-05  -3.471450E-05
  a    84  -1.329174E-05   3.040784E-05   5.345083E-05   9.825416E-05  -1.047209E-04  -4.578836E-05  -2.330681E-06   3.394695E-05
  a    85   1.609268E-07  -5.456716E-06  -3.178464E-05  -5.437791E-05   4.825380E-05   5.264488E-05   2.609704E-05  -2.503946E-06
  a    86  -1.872350E-05   3.797497E-05   6.868319E-05   6.101581E-05  -5.510387E-05   6.102066E-05  -9.470601E-06  -1.427400E-05

               a    49        a    50        a    51        a    52        a    53        a    54        a    55        a    56
  a    13  -4.153744E-04   8.932484E-04  -5.021876E-05  -4.892049E-04   4.490438E-04  -7.836827E-05   1.670691E-03   5.239950E-06
  a    14  -1.218908E-03   8.105542E-04  -5.060058E-05  -4.761144E-04   6.691977E-04  -7.901384E-05   1.242308E-03  -9.061882E-04
  a    15  -6.007551E-04   7.974064E-04  -5.184829E-05   4.953817E-04  -1.494073E-05  -8.115558E-05   1.428982E-03  -6.549748E-04
  a    16   8.899441E-04   4.672275E-04  -1.963440E-05   1.995956E-03  -1.458827E-03  -6.901174E-05   1.459678E-03   1.135869E-03
  a    17  -1.146673E-03  -2.142940E-03   1.190482E-04  -3.551990E-03   1.795635E-03  -5.698554E-05   2.502374E-04  -2.411412E-03
  a    18  -1.035756E-03   5.716547E-04  -1.944634E-05  -2.347388E-03  -2.369660E-03   1.344049E-05   8.851118E-04  -2.168227E-03
  a    19   5.897747E-04   4.658623E-05  -1.323012E-05  -2.846489E-03  -8.701587E-04  -1.542433E-04   2.148841E-03   8.381719E-04
  a    20  -4.449688E-04   2.045449E-03  -1.231267E-04  -2.305035E-03  -3.393026E-04  -2.114480E-04   3.481777E-03  -9.808942E-04
  a    21   5.696075E-04   1.773192E-03  -1.307444E-04  -1.317163E-04   1.789720E-03   6.115395E-05  -1.896343E-03   2.523983E-03
  a    22   1.155702E-03   4.242484E-04  -2.278126E-05   1.230272E-03  -9.735333E-04  -6.427069E-06   1.075305E-03   1.907528E-03
  a    23   1.275205E-03   6.503104E-04  -5.170162E-05   5.135269E-04   1.633894E-03   1.039002E-04  -1.384540E-03   3.184681E-03
  a    24  -2.674166E-03   4.607182E-03  -2.874053E-04   9.856911E-04  -1.521320E-03  -1.998441E-05   1.859877E-03   1.479712E-03
  a    25   1.170508E-03  -1.531925E-03   7.954269E-05   1.139134E-03   2.171417E-03  -8.703179E-05   1.716280E-03   3.132161E-03
  a    26  -4.323383E-03   2.668991E-03  -1.621735E-04   1.222402E-02  -1.377398E-03   3.064080E-04  -4.939297E-03   2.470554E-03
  a    27  -1.379509E-03   4.029940E-04  -1.522938E-04   2.050181E-03  -4.752018E-04  -1.418729E-04  -8.292058E-04  -4.696812E-04
  a    28   4.860461E-03  -5.728406E-04   3.045301E-05  -3.587373E-03   1.015623E-03  -1.312760E-04   1.423554E-03   8.318982E-04
  a    29   3.972287E-05  -1.181339E-04  -2.008452E-03  -1.409223E-05  -5.946354E-05   1.656331E-03   8.545580E-05   8.246060E-07
  a    30  -5.109609E-05  -1.372898E-04   8.545757E-04   1.784057E-04   1.161648E-04   6.009016E-03   2.450525E-04  -2.610546E-04
  a    31  -4.806699E-05  -3.944003E-04  -6.565299E-03   2.130780E-05  -1.145188E-04  -2.745061E-03  -1.822527E-04  -7.935417E-05
  a    32  -4.793108E-05   2.445315E-04   4.860090E-03   1.219536E-05   2.405186E-05   3.976490E-04  -6.196397E-05  -4.745833E-05
  a    33   6.572726E-03   1.297852E-02  -8.396567E-04   2.329429E-03   1.372692E-02  -5.909447E-06  -1.317867E-03   2.872226E-04
  a    34   1.085659E-04  -6.068120E-04  -1.030771E-02  -1.171630E-04   3.563044E-04   1.353089E-03  -1.812477E-04  -2.096205E-05
  a    35   5.414765E-05  -7.864438E-05  -1.983303E-03  -3.726612E-05  -5.475896E-05   7.810057E-05   1.370898E-05   1.296145E-05
  a    36   1.968241E-05   1.460948E-05   3.995826E-04   1.279430E-06   1.483423E-05   1.091198E-03   4.602671E-05  -3.165336E-06
  a    37  -4.444055E-04   3.164065E-04  -2.392655E-05   9.391121E-04  -4.753313E-04   2.044987E-05  -8.491843E-05   2.419795E-04
  a    38   6.055979E-04  -6.309035E-04   1.336142E-06  -1.055382E-03   5.862118E-04   2.203771E-05  -3.929595E-05  -1.671282E-04
  a    39   1.464861E-04  -3.472118E-04   5.836332E-05  -6.603769E-04   2.496788E-04  -3.501605E-05   7.373731E-05  -1.929943E-04
  a    40  -9.287832E-05   6.760208E-04  -4.679298E-05   3.748046E-04  -2.347758E-04  -1.579482E-05   3.636617E-04  -3.249455E-05
  a    41   1.477564E-04  -1.922166E-04   1.423000E-05   2.124114E-04  -2.978450E-04   6.342764E-06  -1.512231E-04   2.825508E-04
  a    42  -1.420570E-04   3.891988E-04  -3.856125E-05   1.660246E-05   8.209913E-05   1.186113E-05  -6.137522E-05  -5.493465E-05
  a    43   4.251108E-04   2.926536E-04  -1.051621E-05   1.046786E-04   4.373479E-04  -2.351522E-05   3.176855E-04  -1.535830E-04
  a    44   1.139140E-04  -2.323697E-04   1.485126E-05  -8.296222E-05   2.701453E-04   2.909497E-05  -2.413966E-04   3.459586E-04
  a    45  -1.350604E-04  -1.324742E-04   5.091147E-06  -1.165173E-05  -7.396106E-04  -4.034255E-05   5.583324E-04   1.388601E-04
  a    46   8.509412E-05   5.918382E-04  -4.483018E-05  -2.576223E-04  -5.257604E-04  -9.592810E-06   3.364012E-04   3.213859E-05
  a    47  -4.060625E-04   2.540097E-04  -2.446668E-05   2.087170E-04  -3.575811E-06   2.290494E-05  -6.177638E-05  -2.469000E-04
  a    48  -8.821697E-05  -1.098710E-04  -2.685828E-06  -7.996857E-05   2.618772E-04   2.238632E-05   3.325986E-05  -2.439060E-04
  a    49   8.424095E-04  -1.435677E-04   5.088615E-06  -6.591042E-05   1.253028E-04   6.548988E-06   1.059796E-04   1.957561E-04
  a    50  -1.435677E-04   9.715632E-04  -2.273961E-05   1.198132E-04   1.309365E-06  -7.404204E-06   1.107963E-04  -8.251047E-05
  a    51   5.088615E-06  -2.273961E-05   6.171874E-04  -5.315694E-06  -1.490577E-06   6.495781E-05  -4.284376E-07   4.476586E-06
  a    52  -6.591042E-05   1.198132E-04  -5.315694E-06   1.082344E-03  -1.204246E-04   1.284759E-05  -1.434112E-04   2.275350E-04
  a    53   1.253028E-04   1.309365E-06  -1.490577E-06  -1.204246E-04   1.009143E-03   1.623844E-05  -2.261749E-04  -1.985115E-04
  a    54   6.548988E-06  -7.404204E-06   6.495781E-05   1.284759E-05   1.623844E-05   9.778374E-04   7.984613E-06   1.722375E-05
  a    55   1.059796E-04   1.107963E-04  -4.284376E-07  -1.434112E-04  -2.261749E-04   7.984613E-06   6.740813E-04  -1.218621E-04
  a    56   1.957561E-04  -8.251047E-05   4.476586E-06   2.275350E-04  -1.985115E-04   1.722375E-05  -1.218621E-04   7.306040E-04
  a    57   1.927897E-04  -8.208594E-05   6.764092E-06   3.402121E-04   2.120203E-04   1.903489E-05  -1.956205E-04   2.795862E-05
  a    58  -1.371488E-05   1.473947E-05   1.827136E-04  -5.268289E-06  -9.937371E-07   2.641734E-04   3.444845E-06  -5.016377E-06
  a    59  -7.040378E-06  -2.839325E-06  -8.335451E-05   1.404433E-05  -6.682115E-06  -3.956934E-04  -3.050736E-05   6.369572E-06
  a    60  -1.357442E-04  -7.913719E-05   2.287540E-06   1.476201E-04   1.067438E-04   2.717019E-05  -2.569350E-04  -1.055710E-05
  a    61  -2.304829E-05  -1.644152E-04   1.879449E-05  -1.742016E-05   1.947636E-04   2.622075E-05   5.139491E-05  -2.652255E-05
  a    62  -3.582600E-05  -2.050095E-05  -3.596156E-05  -2.571607E-05   3.633003E-05  -1.978432E-04  -1.864228E-05  -3.923233E-05
  a    63   2.240406E-04  -6.126389E-05  -6.861026E-07   1.978466E-04  -4.334322E-05  -6.713317E-06   4.582894E-05   1.846833E-04
  a    64  -6.455989E-05   7.874883E-05  -1.357374E-05  -4.548255E-05  -5.587852E-05  -4.604622E-06   9.848245E-06  -3.325615E-05
  a    65   2.396177E-04  -1.436599E-04   6.644506E-06   4.363537E-05  -3.568159E-05  -3.615781E-06   8.633189E-05   8.037089E-05
  a    66  -4.524378E-05   1.640513E-05  -6.638904E-07   6.740538E-05   8.737512E-05   7.664963E-06  -8.608389E-05   5.976352E-05
  a    67  -4.741576E-05  -1.507067E-05  -2.883417E-07   1.048142E-04   7.380951E-05   2.631661E-06  -5.767526E-05  -9.678052E-06
  a    68  -3.062032E-06  -1.188444E-05  -3.542161E-05   4.137115E-06   6.058379E-06  -8.142834E-05  -6.096115E-06   4.708242E-06
  a    69   4.035662E-06   2.410125E-06  -4.734855E-05  -4.191974E-06  -4.275841E-06  -1.447561E-04  -9.660434E-06  -8.597117E-06
  a    70   4.948909E-06  -2.526000E-05   3.368079E-06   1.740312E-05   1.955808E-05   4.614704E-05  -6.522735E-06  -3.913547E-06
  a    71   6.970002E-06  -4.531555E-05   8.997400E-07   4.696121E-06  -1.249523E-05  -2.589241E-05   3.234449E-05  -1.984055E-05
  a    72   1.374779E-04  -1.371259E-04   8.247840E-06  -3.277059E-06  -7.162473E-05  -8.466024E-06   5.615423E-06  -1.137466E-06
  a    73   1.137064E-05  -1.096649E-05   1.984752E-05   1.032971E-06  -1.299135E-06   3.055485E-04   1.616361E-05   2.337089E-07
  a    74   5.556763E-05   1.346972E-04  -9.591018E-06  -1.149803E-04  -1.529631E-04   2.463162E-07   1.030084E-04   5.002851E-05
  a    75   3.433101E-05   6.956940E-05  -3.581300E-06  -3.748402E-05  -2.540987E-05   1.820104E-06   8.779028E-06   3.714184E-05
  a    76  -9.596091E-05   6.705692E-05  -3.512263E-06  -1.278571E-04   9.162125E-05   4.115033E-06  -4.185700E-05  -4.762949E-05
  a    77  -4.052302E-05   3.243482E-05  -1.463916E-06   1.259714E-04  -8.496500E-05  -1.898885E-06   3.141412E-05  -1.115254E-05
  a    78   6.469915E-05  -8.085364E-05   4.105963E-06   7.736278E-05   8.056522E-05  -5.434590E-06  -4.480871E-05   5.858795E-06
  a    79  -4.401769E-06   3.529223E-05   3.091555E-07  -1.710332E-05  -8.270740E-05  -1.130046E-06   2.265498E-05   1.539012E-05
  a    80  -3.851111E-05  -4.144061E-05   1.686953E-06   4.671132E-05   5.773231E-05   1.912513E-06  -5.072398E-05  -1.360155E-05
  a    81   2.043487E-05   5.755551E-05  -3.609333E-06  -8.888431E-06  -8.163223E-05  -5.444063E-06   6.461355E-05  -1.749418E-05
  a    82  -9.773829E-05  -5.049319E-06   4.819760E-07  -5.054646E-06   8.320237E-05   4.093732E-07  -3.378905E-05  -7.241303E-05
  a    83  -4.921724E-06   3.498116E-05  -3.175898E-06  -3.655313E-05  -6.276132E-05  -4.065735E-06   4.297116E-05   3.655515E-06
  a    84   4.215111E-05   8.954757E-06  -6.336841E-07  -1.007418E-05   9.733855E-06   2.224256E-06  -1.811137E-05   5.497944E-06
  a    85  -2.365301E-05   2.047516E-05  -1.270721E-06   1.065149E-05  -1.803174E-05  -1.344471E-07   1.268696E-05  -8.827203E-07
  a    86   3.247653E-05   5.100898E-05  -3.066869E-06   5.143600E-06  -3.353285E-05   3.260581E-07   1.242431E-05   1.499275E-05

               a    57        a    58        a    59        a    60        a    61        a    62        a    63        a    64
  a    13   9.013230E-04  -2.301216E-05   6.113133E-05   2.696761E-04  -2.961157E-04  -2.107535E-04   1.030057E-03  -2.435488E-04
  a    14  -5.655768E-04  -5.666790E-05  -1.287045E-04  -1.607448E-03   6.399477E-04   2.384998E-04  -7.065552E-04  -2.157190E-06
  a    15  -1.644269E-03  -3.098273E-05  -7.304803E-05  -1.677102E-03   6.744941E-04   1.191229E-04   1.120106E-04   6.178542E-05
  a    16   6.909691E-04  -2.816162E-06   7.146518E-05   8.379654E-04  -6.836307E-04  -2.881612E-04   5.836407E-04   5.602532E-05
  a    17  -6.084630E-04   1.829908E-05  -5.508685E-05   7.663935E-04   1.135658E-03   3.718623E-04  -1.906639E-03   3.440269E-04
  a    18   1.325402E-03   2.731280E-05   2.756245E-05  -7.175037E-04  -3.602820E-04  -3.972533E-06   1.022601E-03  -5.572500E-04
  a    19  -1.186015E-03  -1.129671E-04  -9.418181E-05  -1.943936E-03  -3.706739E-04  -4.740325E-04   2.352147E-03  -1.425992E-04
  a    20  -2.549938E-04  -4.352180E-05  -3.120138E-05  -1.033604E-03  -5.768750E-04  -3.046688E-04   1.345727E-03  -4.282846E-04
  a    21  -2.789816E-03   1.076179E-04  -5.392178E-05  -7.700223E-04  -2.148613E-03  -1.611256E-04  -5.964698E-04   3.718696E-04
  a    22   6.196608E-04   1.115849E-04   1.789580E-04   9.610747E-04  -8.395745E-04   8.166974E-05  -6.664538E-04  -3.359120E-04
  a    23  -7.193154E-04   1.943291E-05   4.159035E-05  -8.782211E-04  -1.243590E-03  -2.079120E-04   1.322706E-04   1.878184E-04
  a    24  -3.809006E-04   6.898620E-06   8.582867E-05  -1.305796E-03  -1.309235E-03  -3.977449E-04   1.516871E-03  -1.873755E-04
  a    25   2.463562E-03  -1.555621E-04  -4.192652E-05  -5.740976E-04   4.964736E-04   9.146560E-05  -9.337797E-05  -2.483010E-04
  a    26   1.218423E-03   9.137063E-05   1.071859E-04   2.485743E-03  -2.764264E-05  -2.490418E-06  -1.055406E-04   3.030234E-04
  a    27  -9.499274E-05  -3.693077E-04  -3.774550E-05   2.940438E-04   1.949541E-04   4.500559E-04  -3.934491E-04  -5.552309E-04
  a    28  -1.395793E-03   1.274711E-04   2.244572E-04   1.818096E-03  -2.751113E-03  -8.445937E-04   4.050356E-03  -1.063650E-03
  a    29  -5.009794E-05  -1.176832E-03  -1.735725E-03   1.653777E-04  -2.325068E-05  -2.927457E-04   3.891945E-05  -1.461211E-07
  a    30   1.335306E-07   1.468662E-03   6.515499E-03  -5.090708E-04   1.810284E-04  -8.485291E-04  -3.000090E-04  -2.774638E-05
  a    31  -1.887105E-04  -9.117582E-03   5.187561E-03   1.684532E-04  -8.064910E-04   4.025491E-03   4.147763E-04   1.544984E-04
  a    32  -3.792711E-05  -1.985330E-03   6.464251E-04   3.024915E-05   6.076828E-05  -1.104249E-03  -1.589539E-04  -2.959721E-04
  a    33  -1.032546E-03   8.301580E-06  -8.968112E-05  -7.964098E-04   8.090436E-05   1.623270E-04  -5.626508E-04   3.918783E-04
  a    34  -6.307713E-06  -4.268241E-03   2.043629E-03  -2.496957E-05   2.889891E-04  -3.402022E-03  -4.842837E-04  -1.770758E-04
  a    35  -9.661614E-05  -3.654147E-03   4.234602E-04   1.347413E-04  -3.467098E-04   2.022553E-03   2.829995E-04   2.819062E-04
  a    36   3.170786E-05   7.530068E-04   3.693465E-04  -6.934526E-05   4.960516E-05  -7.988557E-05  -1.406446E-06   6.310121E-05
  a    37   2.642997E-04   1.778805E-06   1.528040E-05   2.311538E-04  -1.304118E-04  -5.594375E-05   1.748755E-04  -2.573515E-05
  a    38   5.028651E-05  -1.818662E-05  -1.643193E-05   2.362139E-04   7.266296E-05  -6.408130E-05   5.907755E-06  -4.792106E-05
  a    39  -1.892264E-04   5.196921E-05   4.420611E-05   2.176846E-04  -8.966158E-05   8.807321E-05  -5.856240E-05  -2.969190E-05
  a    40  -6.905455E-05  -7.824928E-06  -1.434565E-05  -2.325570E-04  -8.691750E-05  -3.583050E-05  -3.016414E-05   8.771653E-05
  a    41  -7.373444E-05   8.522066E-06   1.086798E-05  -3.115251E-05  -1.121976E-04  -2.670446E-05   9.684676E-05  -2.842314E-05
  a    42   1.563867E-04   9.600635E-06   1.443487E-05   2.065629E-04  -1.413756E-04  -6.819166E-05   1.593378E-05   5.613671E-06
  a    43   6.750560E-04  -6.981042E-07   3.691185E-05   2.996138E-04  -1.269617E-04  -5.140705E-05   2.475245E-04  -2.739583E-05
  a    44   3.097793E-04   5.238653E-05   8.214880E-05   1.129242E-03  -4.169234E-04  -9.541480E-05   2.099453E-04  -3.082007E-05
  a    45  -7.147119E-04  -3.938369E-06  -2.073504E-05  -3.235097E-04  -1.659393E-06  -1.932240E-05  -1.704689E-04   1.094203E-04
  a    46  -6.741244E-04   3.940497E-06  -4.090232E-05  -4.498113E-04  -2.350761E-04  -2.045348E-05  -3.992354E-04   2.343943E-04
  a    47   2.228317E-05   3.029769E-05   8.613980E-06   3.668280E-04  -2.629525E-04  -3.445505E-05  -1.972808E-05  -7.490750E-06
  a    48   6.694560E-05   6.653752E-06  -1.850161E-05   2.536207E-05   1.100093E-05  -6.650006E-06   8.237401E-05  -1.066567E-04
  a    49   1.927897E-04  -1.371488E-05  -7.040378E-06  -1.357442E-04  -2.304829E-05  -3.582600E-05   2.240406E-04  -6.455989E-05
  a    50  -8.208594E-05   1.473947E-05  -2.839325E-06  -7.913719E-05  -1.644152E-04  -2.050095E-05  -6.126389E-05   7.874883E-05
  a    51   6.764092E-06   1.827136E-04  -8.335451E-05   2.287540E-06   1.879449E-05  -3.596156E-05  -6.861026E-07  -1.357374E-05
  a    52   3.402121E-04  -5.268289E-06   1.404433E-05   1.476201E-04  -1.742016E-05  -2.571607E-05   1.978466E-04  -4.548255E-05
  a    53   2.120203E-04  -9.937371E-07  -6.682115E-06   1.067438E-04   1.947636E-04   3.633003E-05  -4.334322E-05  -5.587852E-05
  a    54   1.903489E-05   2.641734E-04  -3.956934E-04   2.717019E-05   2.622075E-05  -1.978432E-04  -6.713317E-06  -4.604622E-06
  a    55  -1.956205E-04   3.444845E-06  -3.050736E-05  -2.569350E-04   5.139491E-05  -1.864228E-05   4.582894E-05   9.848245E-06
  a    56   2.795862E-05  -5.016377E-06   6.369572E-06  -1.055710E-05  -2.652255E-05  -3.923233E-05   1.846833E-04  -3.325615E-05
  a    57   7.972006E-04   4.696093E-06   2.467046E-05   3.124188E-04  -2.952634E-05  -4.045956E-05   3.039744E-04  -1.129565E-04
  a    58   4.696093E-06   6.236951E-04  -1.001182E-04   1.722142E-05   1.390951E-05  -1.642113E-04  -1.566657E-05  -6.528530E-06
  a    59   2.467046E-05  -1.001182E-04   6.530076E-04   3.097750E-05  -5.867131E-05   2.685696E-04   4.074566E-05   2.027965E-05
  a    60   3.124188E-04   1.722142E-05   3.097750E-05   9.336446E-04  -2.426930E-04  -4.951961E-05   5.468222E-05  -6.620174E-06
  a    61  -2.952634E-05   1.390951E-05  -5.867131E-05  -2.426930E-04   4.300730E-04   1.087516E-05  -9.628355E-05  -1.703271E-05
  a    62  -4.045956E-05  -1.642113E-04   2.685696E-04  -4.951961E-05   1.087516E-05   4.560367E-04   2.339234E-06   4.108655E-05
  a    63   3.039744E-04  -1.566657E-05   4.074566E-05   5.468222E-05  -9.628355E-05   2.339234E-06   4.119163E-04  -1.193820E-04
  a    64  -1.129565E-04  -6.528530E-06   2.027965E-05  -6.620174E-06  -1.703271E-05   4.108655E-05  -1.193820E-04   7.320664E-05
  a    65   1.201460E-04  -1.318807E-05  -2.437646E-06  -1.346625E-04   3.228346E-05  -1.269750E-05   1.759507E-04  -7.520519E-05
  a    66   5.897505E-05   3.588242E-07   1.341465E-06   2.601593E-05   7.167836E-05   1.618985E-05  -2.864953E-05   8.780688E-06
  a    67   3.512395E-05   3.357030E-06   1.541775E-05   1.190481E-04  -1.174739E-05  -5.757514E-06   2.077416E-05  -1.137452E-05
  a    68   1.925173E-05   4.589930E-05   1.790986E-04   4.179285E-05  -2.586103E-05   4.762066E-06   9.351317E-06   1.599354E-05
  a    69   2.123517E-06   5.840879E-05   2.402824E-04  -5.766846E-05   2.305154E-05   2.454978E-05  -5.359868E-06   1.841065E-05
  a    70   2.905514E-05  -4.441063E-05  -3.808544E-05   1.423897E-05   1.533344E-05  -5.371443E-05   4.424494E-06   1.212693E-06
  a    71  -3.700802E-05  -6.175119E-06   5.982031E-06   1.399878E-05  -1.999177E-05  -1.859661E-05   3.776555E-05  -2.331717E-05
  a    72  -1.675341E-05   9.050924E-06   1.439017E-05   1.357958E-04  -1.230217E-04  -2.171117E-05   3.511509E-05  -5.135827E-06
  a    73   5.278903E-06   7.981566E-05  -1.505320E-04   1.087121E-05  -3.980537E-06   5.032547E-05   2.331213E-05   1.925231E-05
  a    74  -1.815436E-04  -3.195747E-06  -2.428275E-06  -1.772499E-04  -4.047897E-06   5.027765E-06  -6.878976E-05   4.654007E-05
  a    75  -8.273688E-05  -4.474698E-06  -1.285988E-05  -1.123730E-04   7.857039E-05   1.423877E-05  -7.783913E-05   2.825991E-05
  a    76  -3.721497E-05  -3.331461E-06  -1.038174E-05  -6.155981E-05   6.132625E-05   1.505587E-05  -9.697122E-05   2.438240E-05
  a    77  -5.774896E-05   1.974428E-06  -3.380317E-06  -7.136181E-06  -5.178679E-05  -1.085071E-05   9.612996E-06   7.769922E-06
  a    78   1.432636E-04  -9.414787E-06   2.567853E-06   3.021913E-05   6.999595E-05  -1.113156E-06   8.396459E-05  -4.887353E-05
  a    79  -5.418208E-05   9.688636E-06   3.318633E-06   3.742302E-05  -7.990003E-05  -1.314550E-05  -1.251199E-05   1.816200E-05
  a    80   5.364407E-05   3.086852E-07   5.166608E-06   1.049230E-04  -4.321263E-06  -6.607875E-07   1.194581E-05  -9.700919E-06
  a    81  -7.384143E-05  -2.800155E-06  -6.676665E-06  -9.734479E-05  -4.229565E-06  -1.400654E-07  -2.494109E-05   1.754026E-05
  a    82   8.293360E-06   4.623585E-06   3.372477E-06   1.000352E-04  -1.495249E-05   3.516489E-06  -3.824367E-05   9.406304E-06
  a    83  -4.845589E-05  -2.746850E-07   7.429322E-07   2.939941E-05  -2.553836E-05  -6.347329E-06  -2.621672E-05   1.911681E-05
  a    84   4.537384E-05  -8.096626E-07   1.954284E-06   8.635341E-06  -1.846022E-05  -5.641648E-06   1.134803E-05  -3.029458E-06
  a    85  -2.436970E-05   6.218159E-07   7.671132E-07   7.648037E-07  -1.638659E-06  -1.874858E-06  -2.900973E-06   4.785027E-07
  a    86   1.202180E-05   9.788761E-07   1.012159E-06  -5.031384E-06  -2.168764E-05  -1.296498E-06  -1.086282E-05   1.266994E-05

               a    65        a    66        a    67        a    68        a    69        a    70        a    71        a    72
  a    13   4.714901E-04  -1.222644E-04   1.431581E-04   8.730660E-05  -7.135871E-05  -7.448185E-05   1.433138E-04   1.112251E-03
  a    14  -6.106598E-05   2.099995E-04   3.340401E-04  -3.761180E-04   3.269754E-04   2.744545E-04   9.695538E-04  -3.913377E-04
  a    15  -4.081874E-04   4.606194E-04   5.133268E-04  -3.980126E-05  -3.541789E-05  -1.956932E-04  -1.253772E-06  -1.029266E-03
  a    16  -3.808923E-04  -3.455194E-04   7.620521E-05   2.588482E-04  -2.219856E-04  -5.327860E-05  -5.279901E-04  -1.767054E-03
  a    17  -7.361002E-05  -6.605190E-04  -2.314385E-04   4.032536E-05  -6.330443E-06   5.063860E-05  -1.365455E-04   6.861878E-04
  a    18   1.389275E-04  -5.753099E-04  -7.367229E-04  -6.488791E-05   9.896999E-05   1.589853E-04  -3.488406E-04  -2.089328E-03
  a    19   1.049304E-03   7.792438E-04   3.370971E-04   6.697367E-05  -1.412475E-04  -1.907143E-04   7.463293E-04  -2.085471E-04
  a    20  -3.101132E-05  -3.632677E-03  -2.542992E-04   2.910672E-04  -2.176622E-04  -1.235878E-04  -1.635757E-03  -2.338200E-03
  a    21  -2.052079E-04  -5.250332E-05  -1.261110E-03   5.348250E-04  -4.171253E-04  -2.066988E-04   1.940171E-04   2.169348E-03
  a    22  -9.383358E-05   4.370552E-04   1.285724E-03  -1.749012E-04  -6.024597E-06  -2.464300E-04   1.205381E-03  -1.459619E-04
  a    23   6.985005E-04   5.642932E-04  -1.162555E-03  -5.829666E-04   4.149892E-04   1.241841E-04  -6.198020E-04   1.714549E-03
  a    24  -3.245397E-04  -1.340944E-03  -1.100193E-03  -4.338017E-05   2.822489E-05   3.928312E-05  -7.224873E-04  -1.674755E-03
  a    25   9.729574E-04   2.489270E-04  -7.814516E-04   8.133381E-04  -5.679025E-04  -7.790895E-05  -9.149132E-04   3.015090E-04
  a    26  -1.328484E-03   2.423490E-03   1.114528E-03   7.518604E-05  -1.886787E-05   6.499609E-05  -1.011162E-04  -1.954128E-04
  a    27  -5.005069E-04   3.846856E-04   2.777561E-04   1.728714E-04   4.908836E-05   8.775846E-04  -3.205126E-04  -2.235188E-05
  a    28   9.203633E-04   4.902176E-04   4.769019E-04   6.030772E-04  -4.661796E-04   1.445067E-04   7.383893E-04   5.369932E-04
  a    29  -9.091339E-06   1.461983E-05  -1.978111E-05  -9.795630E-04  -1.583877E-03   4.585167E-04  -1.865835E-04  -9.205866E-05
  a    30   1.141951E-05   1.215662E-05   2.438752E-04   2.191705E-03   2.300697E-03   6.931176E-04  -8.606690E-05   2.602666E-05
  a    31   2.265207E-05  -1.065982E-05  -1.004556E-04  -1.922548E-03  -2.307465E-03  -1.187824E-04   3.143793E-05   4.471905E-05
  a    32  -2.976293E-05   2.362462E-06  -1.114267E-04  -7.010940E-04  -3.976498E-04  -8.439435E-04   1.606998E-04   7.168190E-05
  a    33  -5.985545E-05   6.786966E-04   4.652184E-04  -1.378385E-04   6.121254E-05  -1.115048E-04   7.719252E-04   1.486001E-03
  a    34   5.077610E-05  -7.716085E-05  -3.529664E-05  -1.387361E-03  -1.748150E-03   4.603738E-04   2.747600E-04   6.329118E-05
  a    35   4.021087E-05  -8.034352E-06   4.759992E-05   7.045401E-04   6.941829E-04   2.268380E-04  -7.564007E-05  -3.394262E-05
  a    36   1.623271E-05  -1.272494E-05   5.648985E-05   4.520623E-04   4.193207E-04   2.689373E-04  -1.522206E-05  -4.998825E-06
  a    37  -7.847962E-06   8.435563E-05  -3.004902E-05   2.899347E-05  -1.588000E-05   1.883883E-05  -3.619510E-05  -1.521515E-04
  a    38   1.285666E-04  -5.335721E-05  -1.050680E-04   3.269394E-05  -1.152425E-05   4.948754E-05   2.966846E-05   2.054230E-04
  a    39   9.317580E-05  -3.691279E-05  -8.976901E-05   1.706354E-05  -3.617004E-05  -4.383732E-05   6.815885E-05   2.059849E-04
  a    40  -4.317920E-05   1.301536E-04  -1.032502E-04   4.893864E-06  -1.028128E-05  -1.003163E-05   3.765426E-05  -4.650153E-05
  a    41  -6.642029E-05  -1.789155E-04   1.075240E-04  -1.414454E-05  -7.498816E-06  -1.771515E-05   3.918074E-05   1.301722E-04
  a    42  -1.809838E-05   9.693868E-05   8.651086E-06   1.468875E-05  -2.175987E-05   7.912983E-06  -4.133442E-05  -9.406219E-05
  a    43   8.580423E-06  -2.763060E-05  -7.094482E-06   3.399590E-05  -2.317401E-05   1.565521E-05  -7.662993E-05  -3.995637E-05
  a    44  -1.726358E-04   3.402574E-05   1.397185E-04   7.249033E-05  -5.173371E-05   1.599205E-05   3.166318E-05   1.974340E-04
  a    45   6.050667E-05  -1.352191E-04  -1.263122E-04   2.035589E-05  -1.618130E-05  -6.799282E-06   1.024109E-04   2.131082E-04
  a    46  -1.352868E-04  -8.294473E-05  -1.291386E-04  -1.278609E-05  -8.697561E-06  -6.188080E-05  -7.898404E-05   3.755901E-05
  a    47  -1.892682E-04  -4.404525E-05   1.134912E-04   1.617038E-05  -1.504661E-05  -1.217602E-06   6.097352E-05   2.366510E-05
  a    48   3.616907E-05  -4.337745E-05   4.954599E-05   6.281931E-06   2.105368E-07   1.215885E-05   5.781268E-05  -1.321402E-05
  a    49   2.396177E-04  -4.524378E-05  -4.741576E-05  -3.062032E-06   4.035662E-06   4.948909E-06   6.970002E-06   1.374779E-04
  a    50  -1.436599E-04   1.640513E-05  -1.507067E-05  -1.188444E-05   2.410125E-06  -2.526000E-05  -4.531555E-05  -1.371259E-04
  a    51   6.644506E-06  -6.638904E-07  -2.883417E-07  -3.542161E-05  -4.734855E-05   3.368079E-06   8.997400E-07   8.247840E-06
  a    52   4.363537E-05   6.740538E-05   1.048142E-04   4.137115E-06  -4.191974E-06   1.740312E-05   4.696121E-06  -3.277059E-06
  a    53  -3.568159E-05   8.737512E-05   7.380951E-05   6.058379E-06  -4.275841E-06   1.955808E-05  -1.249523E-05  -7.162473E-05
  a    54  -3.615781E-06   7.664963E-06   2.631661E-06  -8.142834E-05  -1.447561E-04   4.614704E-05  -2.589241E-05  -8.466024E-06
  a    55   8.633189E-05  -8.608389E-05  -5.767526E-05  -6.096115E-06  -9.660434E-06  -6.522735E-06   3.234449E-05   5.615423E-06
  a    56   8.037089E-05   5.976352E-05  -9.678052E-06   4.708242E-06  -8.597117E-06  -3.913547E-06  -1.984055E-05  -1.137466E-06
  a    57   1.201460E-04   5.897505E-05   3.512395E-05   1.925173E-05   2.123517E-06   2.905514E-05  -3.700802E-05  -1.675341E-05
  a    58  -1.318807E-05   3.588242E-07   3.357030E-06   4.589930E-05   5.840879E-05  -4.441063E-05  -6.175119E-06   9.050924E-06
  a    59  -2.437646E-06   1.341465E-06   1.541775E-05   1.790986E-04   2.402824E-04  -3.808544E-05   5.982031E-06   1.439017E-05
  a    60  -1.346625E-04   2.601593E-05   1.190481E-04   4.179285E-05  -5.766846E-05   1.423897E-05   1.399878E-05   1.357958E-04
  a    61   3.228346E-05   7.167836E-05  -1.174739E-05  -2.586103E-05   2.305154E-05   1.533344E-05  -1.999177E-05  -1.230217E-04
  a    62  -1.269750E-05   1.618985E-05  -5.757514E-06   4.762066E-06   2.454978E-05  -5.371443E-05  -1.859661E-05  -2.171117E-05
  a    63   1.759507E-04  -2.864953E-05   2.077416E-05   9.351317E-06  -5.359868E-06   4.424494E-06   3.776555E-05   3.511509E-05
  a    64  -7.520519E-05   8.780688E-06  -1.137452E-05   1.599354E-05   1.841065E-05   1.212693E-06  -2.331717E-05  -5.135827E-06
  a    65   2.695247E-04  -4.691611E-06  -6.088471E-05   3.572009E-06   5.239128E-06   1.373394E-05   3.309263E-05   7.784762E-05
  a    66  -4.691611E-06   1.502516E-04  -1.275340E-05   7.285888E-07   1.122095E-06   9.481530E-07  -2.127062E-05  -7.168178E-05
  a    67  -6.088471E-05  -1.275340E-05   1.251554E-04   1.434688E-05   1.757135E-05   8.491863E-06   9.244909E-06  -2.168957E-05
  a    68   3.572009E-06   7.285888E-07   1.434688E-05   3.140061E-04   3.648804E-04   1.232811E-05  -6.892132E-07   8.618482E-06
  a    69   5.239128E-06   1.122095E-06   1.757135E-05   3.648804E-04   4.854459E-04  -2.218060E-05   1.656000E-06  -8.161888E-06
  a    70   1.373394E-05   9.481530E-07   8.491863E-06   1.232811E-05  -2.218060E-05   1.060741E-04   3.955719E-06   2.740700E-06
  a    71   3.309263E-05  -2.127062E-05   9.244909E-06  -6.892132E-07   1.656000E-06   3.955719E-06   7.353163E-05   6.796626E-05
  a    72   7.784762E-05  -7.168178E-05  -2.168957E-05   8.618482E-06  -8.161888E-06   2.740700E-06   6.796626E-05   2.728529E-04
  a    73   1.043022E-05  -3.780828E-06   8.934348E-06   3.582764E-05  -1.814119E-06   6.255817E-05  -1.369811E-05   4.551707E-06
  a    74  -7.275238E-06  -2.177635E-05  -4.747092E-05   7.194192E-06   1.840227E-05  -1.071223E-05  -1.241055E-05  -7.151021E-06
  a    75  -3.244284E-05   3.181446E-05  -2.768555E-05  -1.430181E-05   1.694110E-06  -9.302488E-06  -4.567494E-05  -4.363429E-05
  a    76  -8.157603E-05   3.237657E-05  -3.044289E-06  -9.504086E-06   4.900159E-07  -4.299338E-06  -3.958239E-05  -1.127808E-04
  a    77   5.015728E-06  -9.528390E-06  -3.222708E-06   6.169183E-06  -4.636259E-06   2.009716E-06   8.439064E-06   2.574116E-05
  a    78   4.870372E-05   2.111351E-05   2.181062E-05  -1.034537E-05   1.566438E-06   1.121805E-05   4.377615E-06  -3.015139E-06
  a    79  -2.227247E-05  -1.928738E-05  -2.288072E-06  -2.079262E-06  -1.012499E-05  -7.292085E-06   1.984068E-06   3.512524E-05
  a    80  -3.873163E-05   1.875265E-05   3.681266E-05   8.592756E-06   3.155545E-06   9.963165E-06  -6.264539E-06  -1.525805E-05
  a    81  -2.387500E-06  -1.946772E-05  -1.029914E-05  -6.512883E-06   1.950502E-06  -6.697665E-06  -4.532915E-06  -3.335096E-06
  a    82  -4.869406E-05   1.970980E-05   2.135000E-05   8.227092E-06  -5.751877E-06   2.481972E-06  -7.925611E-06  -2.350280E-05
  a    83  -1.462307E-05  -1.652383E-05  -1.274188E-05  -1.718750E-07   4.503855E-07   5.815765E-07   6.592760E-06   4.121559E-05
  a    84  -6.895226E-06   1.362260E-05  -2.540238E-06   9.704372E-07   3.686960E-06   3.492838E-06  -1.572653E-05  -9.453768E-06
  a    85   5.963572E-07   5.795756E-07   1.513499E-06   2.307673E-06  -4.107918E-07   1.177268E-06   5.753639E-06  -4.950276E-06
  a    86  -2.221277E-05   2.664247E-05  -1.060494E-05  -2.803653E-06   1.075011E-06  -3.305141E-06  -2.226409E-05  -2.991723E-05

               a    73        a    74        a    75        a    76        a    77        a    78        a    79        a    80
  a    13   5.479652E-05   4.188570E-05  -8.967561E-04   2.212345E-05  -8.294875E-04  -3.199066E-04   2.834418E-04  -4.829633E-04
  a    14   6.453853E-05  -6.060609E-04  -3.548710E-04   6.627418E-04   3.336911E-05   5.187883E-05  -4.161878E-04  -1.222843E-04
  a    15  -5.363846E-05   3.224141E-04  -4.359637E-04   4.023063E-04   4.550129E-04  -4.119659E-04  -1.819454E-04  -1.010277E-06
  a    16  -1.501705E-04  -2.107267E-05  -5.036814E-04  -2.633307E-05   8.101852E-04  -1.220455E-03   7.960732E-04   4.035494E-04
  a    17   1.755998E-05  -4.700690E-04  -4.099576E-04   2.696212E-04  -5.092101E-04  -9.075456E-06  -4.690503E-04  -1.003384E-04
  a    18  -1.491802E-05  -1.310185E-03  -2.011348E-03   7.617415E-04   1.994154E-04   1.177837E-04   9.626470E-04   1.258211E-03
  a    19  -1.787503E-05   1.303733E-03  -4.286310E-04  -3.886638E-04  -7.190545E-05  -1.297938E-03   1.088155E-03  -4.698750E-04
  a    20  -1.800991E-04  -2.497159E-04  -9.003958E-04   1.100385E-04   7.652675E-04  -7.876508E-04   7.488600E-04   1.710874E-04
  a    21   1.120528E-05   1.505230E-03   1.993519E-03   1.844121E-04   8.592369E-04  -6.666055E-06   5.672686E-05  -1.189224E-03
  a    22   2.025301E-05   1.147270E-03  -3.130027E-04  -4.796874E-04   1.896478E-04   1.350773E-04   1.404085E-04   2.775326E-05
  a    23  -8.206170E-05   1.063173E-03   9.296894E-04  -3.860022E-04   6.605684E-04  -6.631154E-04  -1.996326E-04  -4.191412E-04
  a    24  -1.152164E-04   2.614467E-04   2.157587E-04  -4.627333E-04   6.395619E-04  -6.299525E-05   1.463862E-04   1.929014E-04
  a    25  -8.573577E-05  -6.828291E-04  -3.672776E-04  -3.970368E-04   4.438155E-04   3.007916E-04  -4.706254E-05   1.067631E-04
  a    26   4.577236E-05  -1.303898E-03   9.032963E-04  -4.660898E-06   6.004853E-04   8.891391E-04  -3.470453E-04   1.016007E-03
  a    27  -3.310738E-04  -7.389726E-05   1.236246E-04   1.821712E-04  -1.531519E-05  -5.092800E-06   1.784139E-05  -8.450904E-05
  a    28  -2.313759E-05  -8.467402E-04  -7.967976E-04  -1.232172E-03  -2.300603E-04   7.570299E-04   7.225955E-04   1.688618E-03
  a    29   2.791918E-03   4.261444E-05   2.077217E-05   3.777085E-05   5.250367E-06  -8.037885E-05  -1.604860E-05   1.940914E-05
  a    30   1.110564E-03   1.255061E-04  -8.061046E-07   3.228531E-05   3.677899E-05  -7.206549E-05   6.255420E-05  -4.437176E-05
  a    31  -1.418599E-03  -9.061109E-05   9.831739E-06  -2.641668E-06  -4.613378E-05   8.736009E-05   2.025844E-05  -1.817311E-05
  a    32  -2.403288E-03  -1.749720E-04  -1.534300E-07  -1.811398E-05  -1.604962E-05   1.184827E-04   6.832138E-05  -4.340074E-05
  a    33   7.919950E-05   6.225760E-04   1.427509E-05  -8.808212E-04   6.835780E-04  -4.994038E-04   2.836641E-04  -6.015443E-04
  a    34  -2.412894E-03  -1.479577E-04   5.487510E-05   7.650678E-05  -1.412919E-05   8.911726E-05   3.715539E-05  -3.036565E-05
  a    35   1.077042E-03   1.014688E-04  -7.811029E-06  -2.133589E-05   5.138013E-06  -4.605290E-05  -6.565557E-05   3.168957E-05
  a    36   2.066610E-04   5.406151E-05   2.264071E-07  -6.925285E-06   2.185884E-06  -2.294917E-05  -1.680437E-05   2.335549E-05
  a    37  -6.339608E-06  -9.173539E-05  -3.710172E-05  -7.728088E-05   1.909938E-04   1.940091E-06   3.378985E-05   8.631804E-05
  a    38   5.807635E-06  -4.776632E-05  -7.877974E-05   6.990573E-05  -2.266580E-04   4.054484E-05  -4.278721E-05  -5.312685E-05
  a    39   7.160262E-06  -4.433617E-05  -1.092081E-04  -2.821146E-05  -1.361109E-05  -3.741818E-05   2.830441E-05  -2.615374E-05
  a    40  -1.424434E-05   1.493533E-04   5.870823E-05  -4.440156E-05   1.680588E-04  -1.109581E-04   3.321532E-05  -6.357782E-05
  a    41  -2.910645E-06   4.165109E-05   2.992071E-05  -9.445411E-05   3.566704E-05   2.528031E-05   2.425308E-05   9.105961E-06
  a    42  -2.675976E-05  -2.815932E-05  -1.563341E-05   5.188839E-05  -1.114231E-05  -1.207771E-05   3.523435E-05   2.285841E-05
  a    43  -1.289348E-05  -9.231518E-05  -5.057137E-05  -2.464574E-05  -7.532982E-05   6.757078E-05  -3.739485E-06   3.000632E-05
  a    44   1.224093E-05  -2.187249E-04  -2.106172E-04  -4.492621E-05  -1.245464E-04  -1.421479E-06   1.215963E-04   1.392104E-04
  a    45  -3.220873E-06   1.650936E-04  -4.424865E-05  -1.837681E-04   2.004580E-04  -1.885846E-04   1.099237E-04  -8.858823E-05
  a    46  -2.052057E-05   4.044288E-04   2.374114E-04   2.967680E-05   1.272486E-04  -2.478607E-04   9.628535E-05  -1.369025E-04
  a    47   1.218810E-05  -1.062694E-04  -1.054960E-04  -2.915479E-05   8.857949E-05  -2.942693E-05   5.205690E-05   7.058533E-05
  a    48   1.928504E-05  -1.099461E-04  -6.867134E-05  -1.578420E-05   1.121636E-05   6.526297E-05  -2.288104E-05   5.587750E-05
  a    49   1.137064E-05   5.556763E-05   3.433101E-05  -9.596091E-05  -4.052302E-05   6.469915E-05  -4.401769E-06  -3.851111E-05
  a    50  -1.096649E-05   1.346972E-04   6.956940E-05   6.705692E-05   3.243482E-05  -8.085364E-05   3.529223E-05  -4.144061E-05
  a    51   1.984752E-05  -9.591018E-06  -3.581300E-06  -3.512263E-06  -1.463916E-06   4.105963E-06   3.091555E-07   1.686953E-06
  a    52   1.032971E-06  -1.149803E-04  -3.748402E-05  -1.278571E-04   1.259714E-04   7.736278E-05  -1.710332E-05   4.671132E-05
  a    53  -1.299135E-06  -1.529631E-04  -2.540987E-05   9.162125E-05  -8.496500E-05   8.056522E-05  -8.270740E-05   5.773231E-05
  a    54   3.055485E-04   2.463162E-07   1.820104E-06   4.115033E-06  -1.898885E-06  -5.434590E-06  -1.130046E-06   1.912513E-06
  a    55   1.616361E-05   1.030084E-04   8.779028E-06  -4.185700E-05   3.141412E-05  -4.480871E-05   2.265498E-05  -5.072398E-05
  a    56   2.337089E-07   5.002851E-05   3.714184E-05  -4.762949E-05  -1.115254E-05   5.858795E-06   1.539012E-05  -1.360155E-05
  a    57   5.278903E-06  -1.815436E-04  -8.273688E-05  -3.721497E-05  -5.774896E-05   1.432636E-04  -5.418208E-05   5.364407E-05
  a    58   7.981566E-05  -3.195747E-06  -4.474698E-06  -3.331461E-06   1.974428E-06  -9.414787E-06   9.688636E-06   3.086852E-07
  a    59  -1.505320E-04  -2.428275E-06  -1.285988E-05  -1.038174E-05  -3.380317E-06   2.567853E-06   3.318633E-06   5.166608E-06
  a    60   1.087121E-05  -1.772499E-04  -1.123730E-04  -6.155981E-05  -7.136181E-06   3.021913E-05   3.742302E-05   1.049230E-04
  a    61  -3.980537E-06  -4.047897E-06   7.857039E-05   6.132625E-05  -5.178679E-05   6.999595E-05  -7.990003E-05  -4.321263E-06
  a    62   5.032547E-05   5.027765E-06   1.423877E-05   1.505587E-05  -1.085071E-05  -1.113156E-06  -1.314550E-05  -6.607875E-07
  a    63   2.331213E-05  -6.878976E-05  -7.783913E-05  -9.697122E-05   9.612996E-06   8.396459E-05  -1.251199E-05   1.194581E-05
  a    64   1.925231E-05   4.654007E-05   2.825991E-05   2.438240E-05   7.769922E-06  -4.887353E-05   1.816200E-05  -9.700919E-06
  a    65   1.043022E-05  -7.275238E-06  -3.244284E-05  -8.157603E-05   5.015728E-06   4.870372E-05  -2.227247E-05  -3.873163E-05
  a    66  -3.780828E-06  -2.177635E-05   3.181446E-05   3.237657E-05  -9.528390E-06   2.111351E-05  -1.928738E-05   1.875265E-05
  a    67   8.934348E-06  -4.747092E-05  -2.768555E-05  -3.044289E-06  -3.222708E-06   2.181062E-05  -2.288072E-06   3.681266E-05
  a    68   3.582764E-05   7.194192E-06  -1.430181E-05  -9.504086E-06   6.169183E-06  -1.034537E-05  -2.079262E-06   8.592756E-06
  a    69  -1.814119E-06   1.840227E-05   1.694110E-06   4.900159E-07  -4.636259E-06   1.566438E-06  -1.012499E-05   3.155545E-06
  a    70   6.255817E-05  -1.071223E-05  -9.302488E-06  -4.299338E-06   2.009716E-06   1.121805E-05  -7.292085E-06   9.963165E-06
  a    71  -1.369811E-05  -1.241055E-05  -4.567494E-05  -3.958239E-05   8.439064E-06   4.377615E-06   1.984068E-06  -6.264539E-06
  a    72   4.551707E-06  -7.151021E-06  -4.363429E-05  -1.127808E-04   2.574116E-05  -3.015139E-06   3.512524E-05  -1.525805E-05
  a    73   2.875792E-04   9.178273E-06  -5.028142E-06  -7.372321E-06   7.008156E-07  -5.261333E-06  -5.984970E-06   4.238877E-06
  a    74   9.178273E-06   1.465593E-04   8.073176E-05   2.079222E-05   1.866807E-06  -6.123124E-05   1.304550E-05  -5.686665E-05
  a    75  -5.028142E-06   8.073176E-05   1.411972E-04   2.353269E-05   1.703205E-06   1.577313E-05  -1.991695E-05  -8.752859E-06
  a    76  -7.372321E-06   2.079222E-05   2.353269E-05   1.352835E-04  -7.624398E-05  -2.954525E-05  -1.913031E-05   1.025163E-06
  a    77   7.008156E-07   1.866807E-06   1.703205E-06  -7.624398E-05   1.469415E-04  -2.345901E-05   1.739928E-05  -5.471748E-06
  a    78  -5.261333E-06  -6.123124E-05   1.577313E-05  -2.954525E-05  -2.345901E-05   1.009433E-04  -3.929997E-05   2.795513E-05
  a    79  -5.984970E-06   1.304550E-05  -1.991695E-05  -1.913031E-05   1.739928E-05  -3.929997E-05   5.305958E-05   8.728237E-06
  a    80   4.238877E-06  -5.686665E-05  -8.752859E-06   1.025163E-06  -5.471748E-06   2.795513E-05   8.728237E-06   6.313046E-05
  a    81  -2.630958E-06   4.807402E-05   2.658162E-05  -9.263402E-06   3.283747E-05  -2.086951E-05   5.049529E-06  -2.172692E-05
  a    82  -1.657191E-06  -3.614500E-05  -1.911937E-05   2.979496E-05   9.919634E-07  -9.622046E-06   3.207411E-07   1.905140E-05
  a    83   5.198887E-07   1.627061E-05  -9.962645E-06  -8.562839E-06   2.836929E-06  -1.955963E-05   2.059243E-05  -1.206741E-05
  a    84   7.442793E-07  -4.809731E-06   8.289290E-06   6.243301E-06  -1.247979E-05   1.040108E-05   4.508501E-06   6.221546E-06
  a    85   8.219374E-07   4.131164E-06  -5.538022E-07  -5.668726E-06   7.142530E-06   3.873271E-09  -1.727803E-06   1.800344E-06
  a    86  -2.420716E-06   7.639202E-06   2.020146E-05   6.834684E-06  -6.744774E-06   4.972900E-06   1.193671E-05   8.643600E-06

               a    81        a    82        a    83        a    84        a    85        a    86
  a    13   2.952219E-04   1.481849E-04  -1.138219E-04   1.027683E-03  -1.092739E-03   1.250340E-04
  a    14   7.511893E-04   3.862775E-04   2.020132E-04   1.336382E-04  -6.241067E-04  -1.020355E-03
  a    15   7.189706E-04   4.195440E-04  -1.288367E-04  -6.409802E-04  -4.518338E-04  -1.120233E-03
  a    16  -5.080950E-05  -1.252352E-04   3.002476E-05   7.234858E-05   3.530967E-04   2.852232E-04
  a    17  -5.041076E-04   3.120143E-04   4.130755E-04  -6.636249E-04   7.335688E-04  -1.565031E-04
  a    18  -3.628455E-04  -3.246290E-04   1.034471E-03   2.633227E-04  -1.885600E-05   7.089660E-04
  a    19   1.456902E-03  -3.798693E-04  -1.870235E-05   2.043315E-04  -5.394279E-05  -1.283428E-05
  a    20  -7.043857E-04  -8.799714E-04   5.457335E-05  -1.668810E-04   2.142465E-04  -3.615354E-04
  a    21  -3.757319E-05   4.111338E-04   7.335647E-04  -3.735248E-04   2.753738E-04  -1.304764E-04
  a    22   8.560727E-04  -2.207948E-04  -1.586287E-04   2.345013E-04  -2.138741E-04   3.458891E-04
  a    23  -2.465820E-04   3.565444E-04   4.328985E-04  -1.133862E-04   2.922872E-04   1.169327E-04
  a    24   1.596970E-04  -9.592848E-04   1.982061E-04   2.206355E-04  -1.947127E-04   1.933733E-04
  a    25  -4.262408E-04  -1.979932E-04  -5.118210E-04  -6.381766E-05   3.186138E-04  -9.080112E-05
  a    26  -6.687554E-04   1.119240E-03  -7.559238E-04   2.041649E-04  -1.590817E-04   1.701711E-04
  a    27  -1.501743E-04   2.108751E-04  -2.803162E-04   4.544920E-05   7.643329E-05  -1.377227E-04
  a    28  -1.849838E-04   1.853520E-04  -1.797953E-04   5.230127E-04   1.584595E-05   2.518553E-04
  a    29  -5.829007E-06   1.013895E-05  -1.710033E-05   4.936499E-06   1.053288E-05  -8.396548E-07
  a    30  -1.650640E-05   1.146955E-05  -6.153549E-05   2.530515E-05   3.817882E-05  -3.520450E-05
  a    31   1.768242E-06   7.503332E-06   7.061839E-06  -3.715458E-05  -2.235281E-05   3.812313E-06
  a    32   6.425033E-06  -5.518922E-07   9.585090E-06  -3.820347E-05  -2.781054E-05  -1.155313E-06
  a    33   4.243026E-04  -8.818474E-04   5.931369E-04  -4.651979E-04   1.522960E-04  -4.833053E-04
  a    34   1.416110E-05  -1.028676E-06   4.268190E-05   6.206723E-05   4.823775E-05  -1.886936E-05
  a    35  -4.021209E-07  -6.452630E-06   1.253807E-05   4.682731E-06   4.836320E-06  -5.686963E-07
  a    36  -2.219190E-06  -8.178193E-07   5.209608E-06   1.874739E-05   9.622649E-06  -2.407084E-06
  a    37  -3.994430E-05   2.206090E-06  -3.947979E-05  -2.051488E-05   4.343070E-05   4.106692E-05
  a    38  -7.398405E-05   2.892391E-05   5.325286E-05   6.641898E-05  -4.320335E-05  -5.686416E-06
  a    39  -3.520551E-05   6.015739E-05   6.132044E-05  -5.368597E-06   1.785129E-06  -2.938715E-05
  a    40   9.888944E-05  -1.041065E-05   1.971668E-05  -2.477580E-05   2.710645E-05   4.330249E-05
  a    41   2.429072E-05  -7.832227E-05  -1.455752E-05  -1.329174E-05   1.609268E-07  -1.872350E-05
  a    42  -3.310018E-05   4.980027E-05   2.001527E-06   3.040784E-05  -5.456716E-06   3.797497E-05
  a    43  -3.192895E-05   3.288506E-05  -5.614022E-07   5.345083E-05  -3.178464E-05   6.868319E-05
  a    44  -1.690363E-04   1.150171E-04   5.619785E-05   9.825416E-05  -5.437791E-05   6.101581E-05
  a    45   1.178259E-04  -6.664017E-05   1.112515E-04  -1.047209E-04   4.825380E-05  -5.510387E-05
  a    46   1.750968E-04  -5.217337E-05   8.779670E-05  -4.578836E-05   5.264488E-05   6.102066E-05
  a    47  -3.003362E-05   8.955724E-05   1.944247E-05  -2.330681E-06   2.609704E-05  -9.470601E-06
  a    48  -1.806412E-05   3.309919E-05  -3.471450E-05   3.394695E-05  -2.503946E-06  -1.427400E-05
  a    49   2.043487E-05  -9.773829E-05  -4.921724E-06   4.215111E-05  -2.365301E-05   3.247653E-05
  a    50   5.755551E-05  -5.049319E-06   3.498116E-05   8.954757E-06   2.047516E-05   5.100898E-05
  a    51  -3.609333E-06   4.819760E-07  -3.175898E-06  -6.336841E-07  -1.270721E-06  -3.066869E-06
  a    52  -8.888431E-06  -5.054646E-06  -3.655313E-05  -1.007418E-05   1.065149E-05   5.143600E-06
  a    53  -8.163223E-05   8.320237E-05  -6.276132E-05   9.733855E-06  -1.803174E-05  -3.353285E-05
  a    54  -5.444063E-06   4.093732E-07  -4.065735E-06   2.224256E-06  -1.344471E-07   3.260581E-07
  a    55   6.461355E-05  -3.378905E-05   4.297116E-05  -1.811137E-05   1.268696E-05   1.242431E-05
  a    56  -1.749418E-05  -7.241303E-05   3.655515E-06   5.497944E-06  -8.827203E-07   1.499275E-05
  a    57  -7.384143E-05   8.293360E-06  -4.845589E-05   4.537384E-05  -2.436970E-05   1.202180E-05
  a    58  -2.800155E-06   4.623585E-06  -2.746850E-07  -8.096626E-07   6.218159E-07   9.788761E-07
  a    59  -6.676665E-06   3.372477E-06   7.429322E-07   1.954284E-06   7.671132E-07   1.012159E-06
  a    60  -9.734479E-05   1.000352E-04   2.939941E-05   8.635341E-06   7.648037E-07  -5.031384E-06
  a    61  -4.229565E-06  -1.495249E-05  -2.553836E-05  -1.846022E-05  -1.638659E-06  -2.168764E-05
  a    62  -1.400654E-07   3.516489E-06  -6.347329E-06  -5.641648E-06  -1.874858E-06  -1.296498E-06
  a    63  -2.494109E-05  -3.824367E-05  -2.621672E-05   1.134803E-05  -2.900973E-06  -1.086282E-05
  a    64   1.754026E-05   9.406304E-06   1.911681E-05  -3.029458E-06   4.785027E-07   1.266994E-05
  a    65  -2.387500E-06  -4.869406E-05  -1.462307E-05  -6.895226E-06   5.963572E-07  -2.221277E-05
  a    66  -1.946772E-05   1.970980E-05  -1.652383E-05   1.362260E-05   5.795756E-07   2.664247E-05
  a    67  -1.029914E-05   2.135000E-05  -1.274188E-05  -2.540238E-06   1.513499E-06  -1.060494E-05
  a    68  -6.512883E-06   8.227092E-06  -1.718750E-07   9.704372E-07   2.307673E-06  -2.803653E-06
  a    69   1.950502E-06  -5.751877E-06   4.503855E-07   3.686960E-06  -4.107918E-07   1.075011E-06
  a    70  -6.697665E-06   2.481972E-06   5.815765E-07   3.492838E-06   1.177268E-06  -3.305141E-06
  a    71  -4.532915E-06  -7.925611E-06   6.592760E-06  -1.572653E-05   5.753639E-06  -2.226409E-05
  a    72  -3.335096E-06  -2.350280E-05   4.121559E-05  -9.453768E-06  -4.950276E-06  -2.991723E-05
  a    73  -2.630958E-06  -1.657191E-06   5.198887E-07   7.442793E-07   8.219374E-07  -2.420716E-06
  a    74   4.807402E-05  -3.614500E-05   1.627061E-05  -4.809731E-06   4.131164E-06   7.639202E-06
  a    75   2.658162E-05  -1.911937E-05  -9.962645E-06   8.289290E-06  -5.538022E-07   2.020146E-05
  a    76  -9.263402E-06   2.979496E-05  -8.562839E-06   6.243301E-06  -5.668726E-06   6.834684E-06
  a    77   3.283747E-05   9.919634E-07   2.836929E-06  -1.247979E-05   7.142530E-06  -6.744774E-06
  a    78  -2.086951E-05  -9.622046E-06  -1.955963E-05   1.040108E-05   3.873271E-09   4.972900E-06
  a    79   5.049529E-06   3.207411E-07   2.059243E-05   4.508501E-06  -1.727803E-06   1.193671E-05
  a    80  -2.172692E-05   1.905140E-05  -1.206741E-05   6.221546E-06   1.800344E-06   8.643600E-06
  a    81   4.728937E-05  -1.503361E-05   8.701203E-06  -4.682260E-06   3.731246E-06   1.252968E-06
  a    82  -1.503361E-05   6.195256E-05  -2.306224E-06  -1.708268E-06   2.915945E-07  -6.159963E-07
  a    83   8.701203E-06  -2.306224E-06   4.707776E-05  -4.444659E-06   6.154201E-06   8.000451E-07
  a    84  -4.682260E-06  -1.708268E-06  -4.444659E-06   3.289797E-05  -1.277925E-05   2.667755E-05
  a    85   3.731246E-06   2.915945E-07   6.154201E-06  -1.277925E-05   1.978436E-05   5.622958E-07
  a    86   1.252968E-06  -6.159963E-07   8.000451E-07   2.667755E-05   5.622958E-07   4.893165E-05

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99997923     1.99990815     1.99967691     1.99964244
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.99949539     1.99941701     1.99909454     1.99875912     1.99865307     1.99847639     1.99839210     1.99767955
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     1.99722428     1.99692542     1.99472931     1.99423300     1.99400677     1.99366482     1.97448094     1.91492402
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     1.00246312     0.99409900     0.09356727     0.02859389     0.00739775     0.00497236     0.00292478     0.00270404
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00215544     0.00167770     0.00157433     0.00106066     0.00084686     0.00076828     0.00075045     0.00051872
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00048847     0.00045650     0.00043994     0.00033221     0.00030857     0.00028596     0.00028046     0.00026072
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62       MO    63       MO    64
  occ(*)=     0.00022519     0.00019862     0.00018119     0.00017351     0.00013391     0.00012950     0.00012370     0.00008525
              MO    65       MO    66       MO    67       MO    68       MO    69       MO    70       MO    71       MO    72
  occ(*)=     0.00008271     0.00006976     0.00005425     0.00003870     0.00003663     0.00003036     0.00002584     0.00002123
              MO    73       MO    74       MO    75       MO    76       MO    77       MO    78       MO    79       MO    80
  occ(*)=     0.00002063     0.00001657     0.00001541     0.00001200     0.00001031     0.00000598     0.00000496     0.00000393
              MO    81       MO    82       MO    83       MO    84       MO    85       MO    86
  occ(*)=     0.00000313     0.00000256     0.00000197     0.00000116     0.00000082     0.00000028


 total number of electrons =   66.0000000000

 item #                     3 suffix=:.drt1.state3:
 read_civout: repnuc=  -468.746689847934     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.62624040  -242.87955055
 residuum:     0.00037862
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -711.49543188  -242.74874203
 residuum:     0.00061881
 deltae:     0.00000001
================================================================================
  Reading record                      3  of civout
 INFO:ref#  3vector#  3 method:  0 last record  0max overlap with ref# 93% root-following 0
 MR-CISD energy:  -711.47740269  -242.73071284
 residuum:     0.00041394
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.12115223
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.02557439
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.03733045
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.08826447

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24
 ref:   1    -0.09929586    -0.10555031    -0.01128983    -0.03609658     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00871655     0.03940820     0.04506172    -0.01308525     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.04592776    -0.09668793    -0.00209956    -0.01628650     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   4     0.01825779     0.02992709     0.04326769     0.05354279     0.00000000     0.00000000     0.00000000     0.00000000

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.00000000
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.00000000
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.00000000
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y=   34801  D0X=       0  D0W=       0
  DDZI=   17300 DDYI=   39515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1785 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y= 1246697  D0X=       0  D0W=       0
  DDZI=  203100 DDYI=  457450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    42.000000
   42  correlated and    24  frozen core electrons
   7396 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000    
  a     6    0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000    
  a     7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000    
  a     8    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000    

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     9    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    10    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    11    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    12    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a    13    0.00000        0.00000        0.00000        0.00000        1.99898       1.985813E-04   5.955394E-04   3.782361E-05
  a    14    0.00000        0.00000        0.00000        0.00000       1.985813E-04    1.99931      -5.974011E-04   1.076652E-04
  a    15    0.00000        0.00000        0.00000        0.00000       5.955394E-04  -5.974011E-04    1.99869      -5.956639E-04
  a    16    0.00000        0.00000        0.00000        0.00000       3.782361E-05   1.076652E-04  -5.956639E-04    1.99839    
  a    17    0.00000        0.00000        0.00000        0.00000      -2.220945E-04  -1.189201E-04   7.517451E-05   3.621425E-04
  a    18    0.00000        0.00000        0.00000        0.00000      -3.690514E-04  -2.154150E-04   3.694842E-04  -2.116231E-04
  a    19    0.00000        0.00000        0.00000        0.00000       3.521982E-04   1.589269E-05  -7.436620E-04  -2.792583E-04
  a    20    0.00000        0.00000        0.00000        0.00000       1.110648E-04  -1.771648E-04  -4.452064E-04   8.124194E-07
  a    21    0.00000        0.00000        0.00000        0.00000       2.968214E-04  -2.189185E-04  -6.596405E-04  -2.220401E-04
  a    22    0.00000        0.00000        0.00000        0.00000       3.085581E-04   1.710532E-04  -1.171115E-04  -2.679649E-04
  a    23    0.00000        0.00000        0.00000        0.00000       1.857911E-05  -2.507398E-05  -2.651421E-04  -1.874790E-04
  a    24    0.00000        0.00000        0.00000        0.00000       2.274874E-04  -6.752351E-05  -3.243604E-04  -1.594879E-04
  a    25    0.00000        0.00000        0.00000        0.00000      -1.944637E-04   1.405484E-04   1.406911E-04   2.238874E-04
  a    26    0.00000        0.00000        0.00000        0.00000       1.435934E-05   2.309672E-04   3.536786E-04  -7.074473E-04
  a    27    0.00000        0.00000        0.00000        0.00000       1.317052E-05  -2.940119E-06  -2.499465E-05  -1.088140E-04
  a    28    0.00000        0.00000        0.00000        0.00000      -4.643420E-04   3.397237E-04   8.872212E-04   3.851134E-04
  a    29    0.00000        0.00000        0.00000        0.00000      -1.044322E-05  -5.262644E-06   9.095472E-06  -3.358821E-06
  a    30    0.00000        0.00000        0.00000        0.00000       1.641098E-05  -1.674973E-05  -1.794610E-05   2.080717E-05
  a    31    0.00000        0.00000        0.00000        0.00000       9.114954E-06   1.977162E-05  -8.417543E-05   5.415611E-05
  a    32    0.00000        0.00000        0.00000        0.00000      -4.958179E-05   1.846728E-04  -5.746020E-04   3.107986E-04
  a    33    0.00000        0.00000        0.00000        0.00000      -2.194913E-05  -2.119788E-04  -4.004486E-04   1.911412E-04
  a    34    0.00000        0.00000        0.00000        0.00000      -1.431373E-04   7.528210E-05   6.615062E-05  -1.706515E-04
  a    35    0.00000        0.00000        0.00000        0.00000       1.176830E-04  -2.040084E-04   2.171887E-04  -8.899987E-05
  a    36    0.00000        0.00000        0.00000        0.00000      -5.994596E-05   4.547936E-05  -2.981725E-05  -1.197191E-05
  a    37    0.00000        0.00000        0.00000        0.00000       9.137193E-04   6.284637E-04  -9.138971E-04  -3.720683E-03
  a    38    0.00000        0.00000        0.00000        0.00000      -1.131940E-03   6.081877E-04   1.621020E-03   3.683357E-03
  a    39    0.00000        0.00000        0.00000        0.00000      -4.901911E-04  -2.585257E-04   2.834986E-04   6.462346E-04
  a    40    0.00000        0.00000        0.00000        0.00000       6.670376E-04  -1.552268E-03  -2.882778E-03  -1.964166E-03
  a    41    0.00000        0.00000        0.00000        0.00000       1.262276E-03   8.678703E-04   4.958362E-04   1.421720E-03
  a    42    0.00000        0.00000        0.00000        0.00000      -1.153015E-03   5.206869E-04  -1.025554E-03  -1.767701E-03
  a    43    0.00000        0.00000        0.00000        0.00000      -2.043869E-04  -2.151538E-03  -1.490873E-03   1.606299E-04
  a    44    0.00000        0.00000        0.00000        0.00000      -2.638852E-03   1.857785E-04   7.142403E-04  -2.596445E-04
  a    45    0.00000        0.00000        0.00000        0.00000       6.466039E-04  -2.110452E-03  -1.481627E-03  -7.739059E-05
  a    46    0.00000        0.00000        0.00000        0.00000       3.462653E-03  -3.136527E-04  -3.256486E-03  -1.453646E-03
  a    47    0.00000        0.00000        0.00000        0.00000      -1.137666E-03   5.610352E-04  -2.128454E-04  -1.897740E-03
  a    48    0.00000        0.00000        0.00000        0.00000      -1.281361E-03   7.880186E-05   2.792759E-04   7.531384E-04
  a    49    0.00000        0.00000        0.00000        0.00000       1.073045E-03  -1.828390E-04  -7.048955E-04   1.020284E-03
  a    50    0.00000        0.00000        0.00000        0.00000      -4.001689E-04   2.236694E-04  -1.716291E-03  -1.171674E-03
  a    51    0.00000        0.00000        0.00000        0.00000       1.749449E-05  -7.492155E-06   1.212435E-04   5.979503E-05
  a    52    0.00000        0.00000        0.00000        0.00000       7.329936E-04  -2.348155E-04   2.154331E-04  -1.161913E-03
  a    53    0.00000        0.00000        0.00000        0.00000      -3.842343E-04  -1.099983E-04   1.488619E-03   4.768559E-04
  a    54    0.00000        0.00000        0.00000        0.00000       7.222992E-06   9.389924E-05   1.012349E-04   1.292773E-06
  a    55    0.00000        0.00000        0.00000        0.00000      -4.952486E-04  -1.748961E-03  -1.857935E-03  -3.303853E-04
  a    56    0.00000        0.00000        0.00000        0.00000       4.143282E-05  -4.128182E-04  -7.518779E-04   9.277355E-04
  a    57    0.00000        0.00000        0.00000        0.00000       2.442664E-04   2.947094E-04   7.158031E-04   4.135294E-04
  a    58    0.00000        0.00000        0.00000        0.00000      -2.588273E-05   6.378393E-05   2.960236E-05  -1.248898E-04
  a    59    0.00000        0.00000        0.00000        0.00000      -4.978496E-05   3.306894E-05   5.490436E-05  -8.262768E-05
  a    60    0.00000        0.00000        0.00000        0.00000      -1.501803E-04   6.477895E-04   1.318615E-03  -1.145943E-03
  a    61    0.00000        0.00000        0.00000        0.00000       1.340504E-04  -6.125325E-04   6.366771E-04   1.483240E-03
  a    62    0.00000        0.00000        0.00000        0.00000       6.550250E-05  -7.997513E-06   1.177208E-04   1.740227E-04
  a    63    0.00000        0.00000        0.00000        0.00000      -5.708710E-04  -3.155798E-04  -1.478838E-05   1.434507E-04
  a    64    0.00000        0.00000        0.00000        0.00000       2.676668E-04  -1.037884E-05  -1.258513E-04  -2.718949E-04
  a    65    0.00000        0.00000        0.00000        0.00000       2.693806E-05  -1.672490E-04  -2.300707E-04   8.577753E-04
  a    66    0.00000        0.00000        0.00000        0.00000       8.184146E-07  -4.795658E-05   4.028109E-05   1.942288E-05
  a    67    0.00000        0.00000        0.00000        0.00000      -3.828281E-04  -2.306142E-04  -4.385299E-05  -7.355845E-05
  a    68    0.00000        0.00000        0.00000        0.00000       4.598115E-06   7.683310E-05   1.790867E-04  -1.880901E-04
  a    69    0.00000        0.00000        0.00000        0.00000      -3.356018E-05  -3.984411E-05  -1.238616E-04   1.677378E-04
  a    70    0.00000        0.00000        0.00000        0.00000      -4.645041E-05   6.762557E-05  -5.425052E-05   7.204883E-05
  a    71    0.00000        0.00000        0.00000        0.00000      -5.236655E-04  -1.487184E-04  -2.024730E-04   8.214757E-04
  a    72    0.00000        0.00000        0.00000        0.00000      -1.264732E-04  -1.994812E-04   3.088360E-04   1.148764E-03
  a    73    0.00000        0.00000        0.00000        0.00000      -5.923292E-05  -5.551286E-06   7.155532E-06   1.390523E-04
  a    74    0.00000        0.00000        0.00000        0.00000       4.221529E-04  -1.938867E-04  -1.669278E-04   2.318749E-04
  a    75    0.00000        0.00000        0.00000        0.00000       8.614192E-04   6.210274E-04  -1.169496E-04  -4.212841E-04
  a    76    0.00000        0.00000        0.00000        0.00000      -6.664715E-04   1.359563E-04   2.861481E-04  -3.097988E-04
  a    77    0.00000        0.00000        0.00000        0.00000       8.552844E-04   4.096151E-04  -7.408614E-04  -1.692637E-03
  a    78    0.00000        0.00000        0.00000        0.00000       1.339438E-04   2.213150E-04   3.171374E-04   3.410895E-04
  a    79    0.00000        0.00000        0.00000        0.00000       1.393881E-04  -6.110361E-04  -2.507844E-04  -3.652620E-04
  a    80    0.00000        0.00000        0.00000        0.00000      -6.305327E-05   3.272168E-04  -1.331269E-04  -9.041277E-04
  a    81    0.00000        0.00000        0.00000        0.00000       1.296347E-04  -2.297260E-04  -7.156364E-04  -6.125811E-04
  a    82    0.00000        0.00000        0.00000        0.00000      -6.396722E-05  -2.710130E-05  -1.300084E-04  -5.130957E-04
  a    83    0.00000        0.00000        0.00000        0.00000      -2.409212E-04  -1.435514E-04  -1.468770E-04  -5.386039E-04
  a    84    0.00000        0.00000        0.00000        0.00000      -2.508625E-04  -2.482210E-04   2.588444E-05   1.458992E-04
  a    85    0.00000        0.00000        0.00000        0.00000       5.086353E-04   3.232608E-04   1.010473E-04  -1.719318E-05
  a    86    0.00000        0.00000        0.00000        0.00000       4.761876E-04  -6.856068E-05  -1.360868E-04   7.625356E-05

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a    13  -2.220945E-04  -3.690514E-04   3.521982E-04   1.110648E-04   2.968214E-04   3.085581E-04   1.857911E-05   2.274874E-04
  a    14  -1.189201E-04  -2.154150E-04   1.589269E-05  -1.771648E-04  -2.189185E-04   1.710532E-04  -2.507398E-05  -6.752351E-05
  a    15   7.517451E-05   3.694842E-04  -7.436620E-04  -4.452064E-04  -6.596405E-04  -1.171115E-04  -2.651421E-04  -3.243604E-04
  a    16   3.621425E-04  -2.116231E-04  -2.792583E-04   8.124194E-07  -2.220401E-04  -2.679649E-04  -1.874790E-04  -1.594879E-04
  a    17    1.99798      -3.746261E-04  -3.085700E-04  -1.142295E-04  -1.709133E-04   2.653792E-04   6.047575E-06   6.957565E-04
  a    18  -3.746261E-04    1.99772       2.639317E-04   2.279830E-04   9.293904E-04   3.691460E-04   3.875050E-04   4.648145E-04
  a    19  -3.085700E-04   2.639317E-04    1.99734      -6.198767E-04  -5.194890E-04  -6.269007E-04  -5.050709E-04  -4.243963E-04
  a    20  -1.142295E-04   2.279830E-04  -6.198767E-04    1.99810      -3.105090E-04  -2.107184E-04  -1.659477E-04  -7.962992E-04
  a    21  -1.709133E-04   9.293904E-04  -5.194890E-04  -3.105090E-04    1.99744       1.957628E-05  -1.468885E-03  -4.998888E-04
  a    22   2.653792E-04   3.691460E-04  -6.269007E-04  -2.107184E-04   1.957628E-05    1.99872       2.813493E-04  -4.276486E-04
  a    23   6.047575E-06   3.875050E-04  -5.050709E-04  -1.659477E-04  -1.468885E-03   2.813493E-04    1.99840      -2.351034E-04
  a    24   6.957565E-04   4.648145E-04  -4.243963E-04  -7.962992E-04  -4.998888E-04  -4.276486E-04  -2.351034E-04    1.99791    
  a    25   1.693297E-04   3.098690E-04   1.060399E-05  -1.745377E-04   2.294418E-04   4.168596E-04  -5.270477E-04  -3.296386E-04
  a    26   1.114872E-03  -2.413672E-04   1.697379E-03   5.053483E-04   5.120422E-04  -4.271067E-04   4.497513E-05  -9.220009E-04
  a    27   1.281662E-04  -1.691467E-05   3.218116E-04   1.097792E-04   1.006818E-04  -3.757542E-05   2.697986E-05  -8.679839E-05
  a    28  -4.524062E-04  -7.647498E-04   4.506127E-04   2.292629E-04   1.111457E-03  -1.001946E-04   5.431403E-04   7.462635E-04
  a    29  -7.726254E-06   2.649844E-06   2.171080E-06  -5.444881E-06  -1.815284E-06   4.418760E-05   2.909340E-05   2.025030E-05
  a    30  -6.563136E-06  -4.497945E-05   4.855665E-05   2.404641E-05  -3.015649E-05  -4.028947E-05  -4.154757E-06  -1.434443E-05
  a    31  -1.957551E-05  -3.452851E-05   1.017276E-04   1.738833E-05   2.805923E-05  -1.842775E-04  -7.140957E-05  -3.922139E-05
  a    32  -3.907830E-05  -3.584999E-04   5.946972E-04   1.427158E-04  -1.285288E-04  -1.404988E-03  -7.173791E-04  -4.921973E-04
  a    33   2.476015E-04   1.160950E-03  -2.855361E-04   3.765187E-04  -1.073882E-03  -1.975377E-04  -5.898473E-04  -1.216427E-04
  a    34  -1.425395E-05   2.213036E-04  -2.905593E-04  -5.359922E-05  -2.989449E-05   9.022248E-04   6.473449E-04   4.597930E-04
  a    35   6.553016E-05   5.982238E-05  -2.553057E-04  -9.911695E-06  -8.887398E-05   6.693899E-04   2.039896E-04   5.293441E-05
  a    36  -6.966555E-05   2.528955E-05   3.022343E-05  -2.944016E-05  -1.172260E-04   4.756519E-04   2.577650E-04   7.451273E-05
  a    37   1.373205E-03  -1.899963E-03  -3.489338E-04  -5.891344E-03   1.040644E-03  -2.083379E-03  -8.078845E-04  -4.778491E-03
  a    38  -4.097657E-03  -8.910202E-04   2.101879E-03   1.084065E-03   8.360933E-04   4.295982E-03  -1.203242E-04   6.526590E-03
  a    39  -3.578037E-03   3.021349E-04  -1.699669E-04   9.618185E-04  -1.572583E-04   3.450812E-03  -9.730428E-04   2.111273E-03
  a    40   2.891378E-03  -2.293444E-04  -3.689526E-03   1.715033E-03   1.575423E-03  -2.958050E-03   5.272129E-04   2.532484E-04
  a    41   4.534996E-04   2.859475E-03   1.155421E-03  -1.918188E-03  -5.279003E-04   3.984253E-05  -1.541754E-03   4.166608E-04
  a    42   3.569932E-05   2.555302E-04  -1.132140E-04   4.770293E-04   2.850258E-03   4.306142E-04   7.886943E-04  -4.545827E-04
  a    43   2.655301E-05  -3.263048E-03   5.853477E-04  -1.361188E-03   3.144185E-03   2.524359E-04   4.382510E-03  -3.133165E-03
  a    44  -1.910295E-03   1.178428E-03   5.297101E-03   1.628962E-03  -6.078172E-04   2.363985E-03   3.505556E-04   1.139563E-03
  a    45  -1.458210E-03  -1.609852E-04  -1.431139E-04  -3.372342E-04  -5.375916E-03   7.407547E-04  -3.855311E-03  -3.603187E-03
  a    46  -3.293549E-03   2.088482E-03  -3.054991E-03   1.445390E-03  -1.588639E-03  -1.852553E-03   1.253522E-03  -5.226661E-04
  a    47   1.366623E-03   6.133402E-04   1.525021E-03  -1.384078E-03   5.966857E-04  -7.722771E-04   1.545537E-03  -1.091059E-03
  a    48   2.783075E-04  -9.971631E-05  -1.599769E-03  -1.558420E-03   1.091076E-03  -3.164800E-04   6.452290E-04  -5.868020E-04
  a    49  -7.027974E-04   5.271810E-04   6.639553E-04  -2.925419E-04   1.255702E-03   1.697879E-03   4.972229E-04   9.212865E-04
  a    50   8.407058E-05   1.953359E-03  -1.173903E-03  -2.606827E-03   1.575756E-03  -1.313038E-03   1.253463E-03  -2.813356E-04
  a    51  -1.100495E-05  -1.096838E-04   5.861111E-05   1.407097E-04  -5.707292E-05   1.379180E-04  -1.150099E-05   5.069183E-05
  a    52   1.731343E-03  -8.146194E-04   9.641966E-04  -9.162626E-05  -5.615081E-04  -1.485010E-03  -8.371499E-04  -2.836819E-03
  a    53   5.075883E-04  -1.387560E-03   8.425542E-05   1.130699E-03  -1.844960E-03   1.616117E-04  -1.275389E-03  -4.571419E-04
  a    54   5.713165E-05   4.550925E-05   8.069244E-05   5.067305E-05   1.252460E-04   1.139601E-04   1.974564E-05   1.223326E-04
  a    55  -8.614213E-04   1.358698E-04  -1.010127E-03  -1.806862E-03  -3.773522E-04  -4.539188E-04   1.103359E-03  -7.303731E-04
  a    56   8.871442E-04   2.032556E-03   2.371887E-03  -5.671203E-04  -2.588759E-03   2.822602E-03  -2.603490E-03  -1.016311E-03
  a    57   1.197258E-03  -3.682789E-03   5.303968E-04   1.941768E-03   3.729298E-03   8.197742E-04   2.606076E-03   7.690771E-05
  a    58  -1.040589E-04   1.653007E-04   3.347611E-05  -6.105484E-05   5.625348E-05  -5.187760E-05   6.753777E-05   1.454437E-04
  a    59  -3.956685E-05   1.497848E-04   1.495479E-04  -8.720320E-05   1.028446E-04   1.031396E-04   2.485654E-04   1.244021E-04
  a    60  -9.261138E-04  -9.372157E-04   2.564895E-03   1.435022E-03   3.834042E-04  -1.249112E-03   1.953928E-03  -7.809699E-05
  a    61   6.996952E-04  -1.294372E-03   9.045321E-04   9.524646E-04  -1.633478E-03   4.067416E-04  -1.503653E-04  -2.749167E-03
  a    62  -4.000643E-05  -2.043537E-04  -8.404148E-06   2.534179E-04  -4.082918E-04   7.598458E-05  -1.288030E-04  -3.246814E-04
  a    63   1.250103E-03   7.238604E-04   2.936834E-04  -1.113550E-03   2.304734E-03  -3.671107E-04  -7.885621E-05  -4.243347E-04
  a    64  -3.283366E-04  -3.549465E-04  -1.354749E-04   3.661987E-04  -7.296040E-04  -4.429236E-04  -4.031227E-05   5.353030E-04
  a    65  -1.413592E-04   1.878833E-04  -2.171851E-04   7.252326E-04   7.114234E-04   1.413429E-03   5.235306E-04  -9.689737E-04
  a    66   4.351030E-04   3.106646E-04  -9.315815E-04   1.931194E-03   2.171052E-04  -9.522412E-04  -4.476831E-04   5.603635E-04
  a    67   5.336502E-04   5.727454E-04   1.567666E-04   9.974869E-04  -7.460267E-04  -7.245787E-04   2.357023E-05  -1.854117E-03
  a    68   7.356741E-05  -3.111317E-04   9.413265E-05  -2.010529E-04   2.087841E-05  -9.140537E-05  -4.089515E-04   1.638845E-04
  a    69  -7.854726E-05   2.069438E-04   4.902348E-06   1.301505E-04   4.456655E-05   4.513705E-05   2.299942E-04  -1.312653E-04
  a    70  -9.658969E-05   6.887964E-05   2.851198E-05  -8.029318E-05   2.460328E-04   6.452103E-05  -2.891294E-05   1.587039E-04
  a    71   2.603224E-05   1.186318E-03  -7.513669E-04   9.248122E-05  -1.583969E-04  -2.086692E-05  -1.075142E-04   1.010524E-03
  a    72  -1.535930E-03   4.638046E-04   6.107238E-04   1.810353E-03  -6.480627E-04   8.468533E-04   2.344001E-05   2.187394E-03
  a    73  -6.328879E-05   7.817153E-05   3.570249E-05   7.233807E-05  -2.188228E-06   6.657025E-06  -1.697035E-05   7.565402E-05
  a    74   2.351193E-04   1.566600E-04  -3.163753E-04   5.298942E-04  -6.164290E-04  -1.410733E-04   2.749947E-04  -4.857515E-05
  a    75  -2.441307E-04   9.042777E-04  -3.339686E-04   7.291507E-04  -2.742494E-04  -7.464884E-04   1.991759E-05   1.212389E-04
  a    76  -3.735109E-04   2.712106E-04   8.764479E-05   1.007862E-04   2.308762E-04   2.000941E-04  -3.081865E-05  -2.504174E-04
  a    77   1.080750E-03   3.821227E-05  -1.135783E-03  -3.431827E-04  -2.669816E-04  -7.717208E-04  -3.491348E-05  -1.464002E-03
  a    78  -2.851561E-04   1.294238E-03   2.161397E-04   2.334616E-05   8.172853E-04  -1.401244E-04   1.649498E-04   2.279771E-04
  a    79  -6.040776E-04  -1.048453E-03   1.161504E-04   1.391329E-04  -2.715584E-04  -1.326683E-04   2.110600E-04  -1.885714E-04
  a    80  -7.869364E-04  -1.539111E-04  -7.802561E-04  -2.216835E-04  -1.486015E-04  -6.255070E-04   1.432447E-04  -3.281026E-06
  a    81   3.824858E-04   1.323776E-04  -7.957758E-04   2.847233E-04   5.472377E-04  -8.069076E-04   4.840002E-04   2.374164E-04
  a    82  -1.358968E-04   2.662078E-04  -2.984921E-05   4.291003E-04  -1.322540E-03  -4.092904E-04  -9.344653E-04   2.340681E-05
  a    83  -1.284882E-03   5.783642E-05   1.395376E-03   3.988293E-04   1.137686E-04   3.232014E-04   3.680681E-04   6.908208E-04
  a    84   1.803124E-04  -2.628383E-04   1.858123E-04   4.335628E-05   6.551154E-05  -2.839439E-05   2.280247E-04   3.768714E-05
  a    85  -2.967286E-04   7.931326E-05   5.594322E-05  -7.078701E-05  -1.699947E-04  -6.305919E-05  -7.450386E-05   1.130411E-04
  a    86  -2.497107E-04  -4.909578E-05  -1.998727E-04  -1.386986E-04  -2.826156E-04  -6.210510E-05  -1.502888E-04   1.359291E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a    13  -1.944637E-04   1.435934E-05   1.317052E-05  -4.643420E-04  -1.044322E-05   1.641098E-05   9.114954E-06  -4.958179E-05
  a    14   1.405484E-04   2.309672E-04  -2.940119E-06   3.397237E-04  -5.262644E-06  -1.674973E-05   1.977162E-05   1.846728E-04
  a    15   1.406911E-04   3.536786E-04  -2.499465E-05   8.872212E-04   9.095472E-06  -1.794610E-05  -8.417543E-05  -5.746020E-04
  a    16   2.238874E-04  -7.074473E-04  -1.088140E-04   3.851134E-04  -3.358821E-06   2.080717E-05   5.415611E-05   3.107986E-04
  a    17   1.693297E-04   1.114872E-03   1.281662E-04  -4.524062E-04  -7.726254E-06  -6.563136E-06  -1.957551E-05  -3.907830E-05
  a    18   3.098690E-04  -2.413672E-04  -1.691467E-05  -7.647498E-04   2.649844E-06  -4.497945E-05  -3.452851E-05  -3.584999E-04
  a    19   1.060399E-05   1.697379E-03   3.218116E-04   4.506127E-04   2.171080E-06   4.855665E-05   1.017276E-04   5.946972E-04
  a    20  -1.745377E-04   5.053483E-04   1.097792E-04   2.292629E-04  -5.444881E-06   2.404641E-05   1.738833E-05   1.427158E-04
  a    21   2.294418E-04   5.120422E-04   1.006818E-04   1.111457E-03  -1.815284E-06  -3.015649E-05   2.805923E-05  -1.285288E-04
  a    22   4.168596E-04  -4.271067E-04  -3.757542E-05  -1.001946E-04   4.418760E-05  -4.028947E-05  -1.842775E-04  -1.404988E-03
  a    23  -5.270477E-04   4.497513E-05   2.697986E-05   5.431403E-04   2.909340E-05  -4.154757E-06  -7.140957E-05  -7.173791E-04
  a    24  -3.296386E-04  -9.220009E-04  -8.679839E-05   7.462635E-04   2.025030E-05  -1.434443E-05  -3.922139E-05  -4.921973E-04
  a    25    1.99807      -2.208428E-04   1.093824E-05  -4.680599E-04   3.923454E-06   2.986913E-05  -1.533448E-04  -8.438062E-04
  a    26  -2.208428E-04    1.99393      -6.824053E-04   2.549479E-04  -4.590137E-05   4.219918E-05   1.955555E-04   1.856370E-03
  a    27   1.093824E-05  -6.824053E-04    1.99875       2.494932E-04   3.464511E-04  -6.642339E-04  -1.734311E-03  -1.628887E-02
  a    28  -4.680599E-04   2.549479E-04   2.494932E-04    1.99611       2.155224E-05  -1.239600E-04  -1.731668E-04  -1.596836E-03
  a    29   3.923454E-06  -4.590137E-05   3.464511E-04   2.155224E-05    1.99180       1.599263E-03   1.520027E-03  -3.838040E-03
  a    30   2.986913E-05   4.219918E-05  -6.642339E-04  -1.239600E-04   1.599263E-03    1.96185      -6.209072E-04   9.048514E-03
  a    31  -1.533448E-04   1.955555E-04  -1.734311E-03  -1.731668E-04   1.520027E-03  -6.209072E-04    1.90589      -1.321737E-02
  a    32  -8.438062E-04   1.856370E-03  -1.628887E-02  -1.596836E-03  -3.838040E-03   9.048514E-03  -1.321737E-02    1.04798    
  a    33  -1.443316E-05  -8.203804E-05  -4.646022E-05   3.644311E-04   1.129297E-04  -8.415237E-06   3.090722E-04  -1.391198E-03
  a    34   6.210081E-04  -1.495308E-03   1.214925E-02   1.204280E-03  -1.772280E-02   2.982688E-02   8.861834E-02   0.320719    
  a    35   5.056065E-04  -7.596965E-04   5.460409E-03   1.736624E-04   2.313437E-02   4.640569E-02   1.879480E-02  -0.157783    
  a    36   3.213956E-04  -3.807790E-04   7.614735E-04  -2.081298E-04   3.578885E-03   2.974839E-03   2.191014E-02  -7.967837E-03
  a    37  -2.642892E-03  -8.401685E-03  -6.239458E-04  -8.440551E-04  -2.802014E-05   1.148071E-04  -1.291298E-04   4.318937E-04
  a    38  -7.476543E-04   6.795048E-03   2.237017E-04   1.257811E-03   3.000939E-05   3.259978E-04  -4.417808E-04   4.349487E-04
  a    39   8.526084E-05   4.849530E-03   7.709916E-04   8.611363E-04   1.064677E-04  -3.148521E-04   5.066772E-04  -1.027057E-03
  a    40   1.944943E-03  -2.137657E-03   2.362243E-04   8.773747E-04   1.176162E-04   1.644447E-04   3.164407E-05  -1.039495E-04
  a    41   1.241744E-03  -9.168694E-04   1.215455E-03  -2.967563E-04   2.179518E-04   5.162073E-04   3.739725E-04  -7.468434E-04
  a    42   5.972930E-04  -2.553124E-03   8.950546E-04  -1.739617E-04   3.444356E-04   7.764973E-04   2.780191E-04  -9.043503E-04
  a    43   3.290741E-03  -1.311802E-03  -1.158170E-04  -1.700943E-03   1.271592E-04  -7.098195E-05   2.370191E-04  -4.365734E-04
  a    44  -1.295673E-03  -2.859398E-03  -4.617048E-04  -5.472004E-03  -5.936395E-05  -6.647683E-05  -1.035949E-04   1.910626E-04
  a    45  -9.618209E-04   2.900292E-03   1.659092E-03  -1.935329E-03   6.982911E-06   5.795456E-04   9.582504E-05  -5.147870E-05
  a    46   3.101101E-03   1.660393E-03  -3.861964E-04   3.820866E-03   2.897352E-06   5.611962E-05  -2.284808E-04   1.982900E-04
  a    47   1.023532E-03  -1.807067E-03  -7.033784E-04  -2.924601E-04  -9.448876E-06  -1.063655E-04  -2.793485E-04   1.409687E-04
  a    48  -1.251694E-04   1.594162E-03  -1.868142E-04   9.689964E-05  -3.726672E-05   8.997431E-05  -2.121061E-04   2.376025E-04
  a    49  -1.493940E-03   1.847800E-03   1.337298E-04  -1.292843E-03  -4.665038E-05   7.407550E-05  -1.817026E-05   9.309458E-05
  a    50  -1.034500E-04  -2.282066E-03  -6.146818E-04   1.042387E-03   9.420870E-05  -5.003360E-04   1.576030E-04   3.745277E-04
  a    51   3.270427E-05   3.535998E-05   8.482958E-04   3.958837E-05   2.821833E-03  -2.804883E-03   2.955656E-03   5.603253E-03
  a    52  -8.641764E-04  -5.033770E-03   1.308091E-04  -2.059208E-03   8.355895E-05   1.891354E-04   1.313789E-04   1.064870E-05
  a    53   9.572788E-04   1.545331E-03   3.752000E-04   1.074458E-03   1.455705E-04   1.919056E-04   1.080933E-04   4.900938E-05
  a    54  -1.252669E-04  -3.709491E-04   4.526285E-04   7.027631E-06   2.016648E-03   8.980187E-03  -2.400586E-03  -3.083188E-04
  a    55   1.669062E-03   2.830180E-03   2.751492E-04  -2.564749E-05   2.990204E-05   4.977905E-04  -1.584027E-04   9.013358E-05
  a    56  -2.496667E-03  -3.923176E-03  -8.498458E-04  -1.221656E-04  -7.793869E-05  -1.712596E-04  -1.556744E-04   1.231996E-04
  a    57   7.232835E-04  -4.026088E-03  -6.833026E-04  -2.800342E-03  -2.577033E-05  -1.038460E-04  -1.666420E-04  -5.989309E-05
  a    58   3.930726E-05  -7.121331E-05  -4.369665E-05  -5.524160E-05   1.697783E-03   4.177619E-04  -5.486401E-03  -1.732296E-03
  a    59   4.758591E-05  -4.855645E-04   1.204095E-03  -7.850673E-05   1.609605E-04   5.955551E-03   7.417398E-03   9.907733E-05
  a    60   2.756217E-03  -3.126252E-03  -4.538050E-04  -3.327294E-03  -1.396042E-04  -3.882993E-04  -2.687684E-04   1.843025E-04
  a    61   6.706669E-04   1.029505E-03   7.649172E-04  -1.929043E-04   3.722824E-04   4.938653E-05  -4.975816E-04  -4.466743E-04
  a    62   2.861979E-04   5.345237E-04  -9.009122E-04   8.312845E-06  -2.391815E-03   3.148114E-04   2.863965E-03   3.231408E-03
  a    63  -2.702593E-03  -1.354151E-03  -3.849025E-04  -2.304839E-04  -3.993519E-04  -1.430505E-04   1.757588E-04   3.894498E-04
  a    64   7.196016E-04  -1.456714E-04  -4.620814E-04  -1.237064E-04  -3.908252E-04   3.135798E-04   2.149747E-05   6.186231E-04
  a    65  -1.229212E-03   1.804278E-03   4.519443E-05   4.258900E-05  -4.578306E-05   8.695224E-05  -4.953235E-06   9.530418E-05
  a    66  -1.773409E-04  -1.698384E-03  -7.409910E-05  -4.237171E-04   5.504101E-06   1.824685E-05  -2.155797E-06  -4.147068E-05
  a    67   3.351742E-04  -1.276207E-03   8.259679E-05  -3.770323E-04  -6.522849E-05   2.783700E-04  -3.600178E-05   1.592401E-04
  a    68   4.510766E-04  -1.829056E-04  -1.385476E-04  -3.147234E-04  -1.655225E-03   1.745456E-03  -2.198908E-03   2.596970E-04
  a    69  -4.109378E-04   2.819527E-04  -6.051427E-04   2.472162E-04  -2.004107E-03   1.594727E-03  -2.799527E-03  -2.337639E-04
  a    70  -2.274730E-04   1.588235E-04   5.098367E-05   2.063161E-04  -2.468136E-05   1.230019E-03   2.663131E-04   1.506193E-03
  a    71  -6.960686E-05   1.182475E-03   1.216953E-04   4.774043E-04   6.755875E-05  -2.275120E-04   3.609142E-05  -8.556942E-05
  a    72   9.151549E-04   2.234640E-03   1.467979E-04   6.615276E-04   6.476399E-05  -3.127101E-05   4.974611E-05  -5.704675E-05
  a    73  -2.114688E-05   2.827037E-04  -8.943999E-04   1.303254E-05  -9.902573E-04   1.700316E-03  -1.658148E-03   1.011665E-03
  a    74   5.341329E-04   2.223903E-04   9.503717E-05  -5.511247E-04  -1.558686E-04   3.403878E-04  -7.832690E-05   1.670980E-04
  a    75   4.217747E-04  -1.609043E-04  -6.715348E-06   1.270442E-04   3.775994E-05  -6.766891E-05   1.408701E-05  -2.067675E-05
  a    76  -8.929559E-04   3.888624E-04   1.379279E-04  -1.166183E-03   4.359179E-05   1.260081E-05  -3.616206E-06  -1.391443E-05
  a    77   2.276496E-04  -1.348435E-03  -2.590849E-04   2.281029E-03   2.669331E-05   3.018770E-05  -7.949841E-06   9.784239E-06
  a    78  -1.015552E-03  -9.521633E-05  -9.044961E-05   3.905663E-04   6.463982E-05  -1.409161E-04   6.590199E-05  -7.893741E-05
  a    79   8.019236E-04   6.620424E-04   1.437479E-04  -3.846733E-05   1.102768E-04  -7.367666E-05   8.400835E-05  -9.417456E-05
  a    80   4.100269E-04   8.606455E-04   2.749808E-04  -2.097022E-04  -7.003430E-05   5.815160E-05  -3.132439E-05   4.741090E-05
  a    81   4.192531E-04  -7.559229E-04  -1.070524E-04  -4.006062E-04  -1.323025E-05  -6.221601E-06   7.085269E-06   8.173688E-06
  a    82   3.015078E-04  -4.091348E-04   1.167043E-04  -1.453871E-04  -3.015005E-06   5.320644E-06   1.219129E-05  -7.111061E-07
  a    83  -2.627150E-05  -9.358322E-04  -1.579959E-04  -3.822876E-04  -3.000401E-05  -4.781160E-05  -2.962723E-05   1.323180E-05
  a    84  -7.629556E-05  -5.886477E-05   6.066103E-05   5.888028E-05   1.926907E-05   5.745174E-05   2.765074E-06   4.277402E-05
  a    85  -1.833108E-04  -3.399476E-04  -8.709847E-06  -1.883181E-04  -1.643602E-05   3.958518E-05   3.464783E-06   5.318844E-05
  a    86   1.328398E-04   1.409618E-04   1.703464E-05   2.052780E-04  -1.282396E-05   1.262671E-05   1.576024E-05  -9.580626E-06

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a    13  -2.194913E-05  -1.431373E-04   1.176830E-04  -5.994596E-05   9.137193E-04  -1.131940E-03  -4.901911E-04   6.670376E-04
  a    14  -2.119788E-04   7.528210E-05  -2.040084E-04   4.547936E-05   6.284637E-04   6.081877E-04  -2.585257E-04  -1.552268E-03
  a    15  -4.004486E-04   6.615062E-05   2.171887E-04  -2.981725E-05  -9.138971E-04   1.621020E-03   2.834986E-04  -2.882778E-03
  a    16   1.911412E-04  -1.706515E-04  -8.899987E-05  -1.197191E-05  -3.720683E-03   3.683357E-03   6.462346E-04  -1.964166E-03
  a    17   2.476015E-04  -1.425395E-05   6.553016E-05  -6.966555E-05   1.373205E-03  -4.097657E-03  -3.578037E-03   2.891378E-03
  a    18   1.160950E-03   2.213036E-04   5.982238E-05   2.528955E-05  -1.899963E-03  -8.910202E-04   3.021349E-04  -2.293444E-04
  a    19  -2.855361E-04  -2.905593E-04  -2.553057E-04   3.022343E-05  -3.489338E-04   2.101879E-03  -1.699669E-04  -3.689526E-03
  a    20   3.765187E-04  -5.359922E-05  -9.911695E-06  -2.944016E-05  -5.891344E-03   1.084065E-03   9.618185E-04   1.715033E-03
  a    21  -1.073882E-03  -2.989449E-05  -8.887398E-05  -1.172260E-04   1.040644E-03   8.360933E-04  -1.572583E-04   1.575423E-03
  a    22  -1.975377E-04   9.022248E-04   6.693899E-04   4.756519E-04  -2.083379E-03   4.295982E-03   3.450812E-03  -2.958050E-03
  a    23  -5.898473E-04   6.473449E-04   2.039896E-04   2.577650E-04  -8.078845E-04  -1.203242E-04  -9.730428E-04   5.272129E-04
  a    24  -1.216427E-04   4.597930E-04   5.293441E-05   7.451273E-05  -4.778491E-03   6.526590E-03   2.111273E-03   2.532484E-04
  a    25  -1.443316E-05   6.210081E-04   5.056065E-04   3.213956E-04  -2.642892E-03  -7.476543E-04   8.526084E-05   1.944943E-03
  a    26  -8.203804E-05  -1.495308E-03  -7.596965E-04  -3.807790E-04  -8.401685E-03   6.795048E-03   4.849530E-03  -2.137657E-03
  a    27  -4.646022E-05   1.214925E-02   5.460409E-03   7.614735E-04  -6.239458E-04   2.237017E-04   7.709916E-04   2.362243E-04
  a    28   3.644311E-04   1.204280E-03   1.736624E-04  -2.081298E-04  -8.440551E-04   1.257811E-03   8.611363E-04   8.773747E-04
  a    29   1.129297E-04  -1.772280E-02   2.313437E-02   3.578885E-03  -2.802014E-05   3.000939E-05   1.064677E-04   1.176162E-04
  a    30  -8.415237E-06   2.982688E-02   4.640569E-02   2.974839E-03   1.148071E-04   3.259978E-04  -3.148521E-04   1.644447E-04
  a    31   3.090722E-04   8.861834E-02   1.879480E-02   2.191014E-02  -1.291298E-04  -4.417808E-04   5.066772E-04   3.164407E-05
  a    32  -1.391198E-03   0.320719      -0.157783      -7.967837E-03   4.318937E-04   4.349487E-04  -1.027057E-03  -1.039495E-04
  a    33    1.99308       2.688295E-05   5.195432E-04  -1.787677E-04   8.003978E-03  -3.371280E-05   9.010633E-05  -1.270467E-02
  a    34   2.688295E-05   0.758052      -0.340007       8.118572E-04   1.082708E-03   1.224906E-03  -2.620455E-03  -6.259253E-04
  a    35   5.195432E-04  -0.340007       0.297089       9.679767E-04  -5.310202E-04  -8.036183E-04   1.404483E-03   1.679166E-04
  a    36  -1.787677E-04   8.118572E-04   9.679767E-04   3.433152E-02  -1.490978E-05   6.379470E-05  -2.374587E-04   8.054681E-07
  a    37   8.003978E-03   1.082708E-03  -5.310202E-04  -1.490978E-05   3.085980E-03  -1.872114E-03  -9.409475E-04   7.086764E-05
  a    38  -3.371280E-05   1.224906E-03  -8.036183E-04   6.379470E-05  -1.872114E-03   3.113310E-03   1.553391E-03  -1.325400E-03
  a    39   9.010633E-05  -2.620455E-03   1.404483E-03  -2.374587E-04  -9.409475E-04   1.553391E-03   1.399568E-03  -5.827077E-04
  a    40  -1.270467E-02  -6.259253E-04   1.679166E-04   8.054681E-07   7.086764E-05  -1.325400E-03  -5.827077E-04   2.764349E-03
  a    41   6.712079E-03  -2.497824E-03   1.159631E-03  -1.661283E-04   1.682957E-04  -1.247673E-04  -2.077230E-04  -7.975403E-04
  a    42  -6.284501E-03  -3.325129E-03   1.333358E-03  -1.200998E-04   1.714459E-04  -3.403339E-04  -6.568141E-05   6.573388E-04
  a    43  -1.510013E-02  -1.656121E-03   7.688706E-04  -5.993269E-06  -2.594453E-04  -7.219594E-05  -2.891732E-04   1.288606E-03
  a    44  -2.559303E-03   6.407271E-04  -2.805770E-04  -2.590534E-06  -3.507938E-04   1.024138E-03   5.197764E-04  -5.453614E-04
  a    45   8.442457E-03  -4.361885E-04   2.509074E-04  -5.453815E-07   1.025583E-04  -4.234446E-04   3.102545E-04  -1.657836E-05
  a    46  -2.897322E-03   7.376088E-04  -3.890314E-04   5.624341E-05  -2.496587E-04  -3.848408E-04   6.524719E-05   8.676970E-04
  a    47  -3.252363E-03   9.989712E-04  -5.770147E-04   9.449820E-05   2.991885E-04  -7.085343E-04  -1.946636E-04   5.767628E-04
  a    48  -2.567116E-04   1.147719E-03  -6.087959E-04   1.297562E-04   1.218888E-04  -1.941658E-05   1.263263E-04  -1.246798E-04
  a    49  -1.792730E-03   4.030101E-04  -1.729589E-04   8.578461E-06  -7.693934E-04   6.744394E-04   2.071582E-04   1.243831E-04
  a    50  -6.400877E-03  -7.706375E-04   5.733209E-04   2.887043E-06   9.638904E-05  -8.015250E-04  -3.962927E-04   1.362434E-03
  a    51   3.864652E-04  -1.621582E-02   1.117736E-02   3.570130E-04  -3.517060E-05  -2.977850E-06   1.175271E-04  -7.053803E-05
  a    52  -1.913533E-03  -3.833373E-04   1.646982E-04  -1.995469E-05   1.078229E-03  -1.267588E-03  -8.279825E-04   6.094729E-04
  a    53  -8.535406E-03  -5.701650E-04   1.442767E-04   3.800395E-05  -8.575427E-04   4.477229E-04   1.304961E-04   5.681602E-04
  a    54   6.410634E-05  -1.342991E-03   7.437658E-04   9.668752E-04   2.267529E-05   2.316010E-05  -2.089729E-05  -2.833376E-05
  a    55  -3.217041E-04   1.784568E-04  -3.354849E-05   3.714347E-05  -1.504990E-04  -1.485053E-04   5.785794E-05   4.736876E-04
  a    56   7.978641E-05   4.391793E-04  -2.057819E-04  -1.181117E-05   1.123504E-04   5.275457E-05  -4.360459E-05  -1.524314E-04
  a    57   4.618626E-04  -2.103120E-04   3.934701E-05   1.339584E-05   3.403593E-04   9.215696E-05  -2.195556E-04  -1.455914E-04
  a    58   4.621729E-05  -8.216730E-03   2.346183E-03  -6.637854E-04  -1.079233E-05  -3.012117E-05   9.439635E-05  -4.258779E-06
  a    59   3.420726E-05  -5.704439E-03   3.627986E-03   1.020628E-03   1.104820E-05  -1.001915E-05   6.760093E-05  -1.808591E-05
  a    60   1.044581E-03   8.821221E-04  -4.062893E-04  -4.672618E-05   3.437811E-04   3.274562E-04   2.006655E-04  -3.951065E-04
  a    61   3.322340E-04  -1.357325E-03   3.871368E-04  -2.817682E-05  -8.170081E-05   4.876192E-05  -7.051041E-05  -8.926957E-05
  a    62  -7.728659E-05   9.157704E-03  -2.628230E-03   1.296034E-04  -3.199806E-05  -3.258658E-05   3.083793E-05  -2.286136E-05
  a    63   4.032162E-04   1.548704E-03  -5.139368E-04  -1.498858E-05   1.812035E-04   7.750657E-05  -6.675037E-05  -6.921939E-05
  a    64  -2.688356E-04   2.162145E-03  -7.721436E-04   8.680263E-05  -4.005554E-05  -4.460826E-05  -4.265803E-05   8.814240E-05
  a    65   1.273298E-04   2.778112E-04  -9.609214E-05   1.873330E-05   2.421818E-06   9.570583E-05   1.079625E-04  -2.237101E-06
  a    66  -1.106810E-03  -3.913795E-05   1.212803E-05  -9.469003E-06   1.365854E-05   4.595964E-05   3.118708E-05   1.656665E-04
  a    67  -3.975258E-04   4.355825E-04  -1.282969E-04   4.204620E-05  -9.693259E-05  -9.177340E-05  -1.148195E-04  -1.419098E-04
  a    68   5.493722E-05   5.905302E-03  -1.995912E-03  -3.325780E-05   5.174879E-05   3.858465E-05  -2.484250E-06   9.550451E-06
  a    69  -5.216112E-05   5.796351E-03  -2.057421E-03  -1.569871E-04  -1.624429E-05  -2.823375E-06  -5.777342E-05  -1.907684E-05
  a    70   4.745647E-05   2.472447E-03  -7.979548E-04   3.161574E-04   2.454554E-05   5.139094E-05  -5.427442E-05  -1.902517E-05
  a    71  -6.184882E-04  -6.940858E-04   1.910051E-04  -2.776515E-05  -1.205978E-04   5.643717E-05   8.122984E-05   1.035649E-04
  a    72  -1.315838E-03  -4.065467E-04   1.333964E-04  -1.281105E-05  -2.647843E-04   1.470041E-04   1.476609E-04   9.957788E-05
  a    73  -1.060926E-04   1.084758E-02  -3.230114E-03   3.067951E-04  -2.820020E-07   3.514647E-05  -4.791716E-05  -1.399106E-05
  a    74  -2.389951E-04   7.603266E-04  -2.322390E-04   8.419535E-06  -1.079728E-04  -8.787975E-05  -3.442601E-05   2.108984E-04
  a    75  -2.963691E-04  -3.817125E-05   2.832113E-05   6.861226E-06  -2.109852E-05  -1.125228E-04  -1.143632E-04   1.125594E-04
  a    76  -6.860419E-05  -1.077167E-04   4.209943E-05   6.356920E-06  -8.581634E-05   7.713312E-05  -6.218908E-06  -3.130379E-05
  a    77   1.304910E-04   6.693506E-05  -1.170811E-05   3.779582E-06   2.794812E-04  -3.241919E-04  -8.253649E-05   1.825907E-04
  a    78  -1.143410E-04  -4.616433E-04   1.336294E-04   5.910486E-06   1.217179E-05   5.997897E-05  -2.417276E-05  -1.246143E-04
  a    79   3.458615E-04  -4.392447E-04   1.656705E-04   1.602407E-05  -9.215900E-06   1.501242E-05   6.766229E-05  -4.196510E-05
  a    80   5.330155E-04   2.395205E-04  -7.763343E-05  -1.302667E-05   2.838944E-05   6.997703E-05   3.150594E-05  -1.820880E-04
  a    81   3.946864E-05   1.039278E-05  -3.975838E-06   1.151546E-06  -3.065318E-05  -1.272732E-04  -6.714316E-05   1.466604E-04
  a    82  -5.649301E-04  -3.435738E-05   1.432637E-05  -7.396635E-06   2.043078E-06  -2.363453E-05   1.997781E-05   7.027183E-05
  a    83   9.310631E-05   6.749566E-05  -4.278149E-05   1.506965E-06  -4.030675E-06   2.091189E-05   4.701033E-05  -1.847119E-05
  a    84  -3.585759E-04   7.498932E-05  -2.931545E-05   1.321669E-05  -2.917781E-05   3.530245E-05  -1.680957E-05   1.466780E-05
  a    85   4.822638E-04   7.559601E-05  -2.806476E-05   9.173082E-06   7.110234E-05   1.354478E-06   2.351542E-05  -5.047415E-05
  a    86   8.297994E-05   1.348327E-05  -4.205958E-06  -6.363180E-06   4.080140E-05   2.524271E-05  -3.183908E-06  -1.252303E-06

               a    41        a    42        a    43        a    44        a    45        a    46        a    47        a    48
  a    13   1.262276E-03  -1.153015E-03  -2.043869E-04  -2.638852E-03   6.466039E-04   3.462653E-03  -1.137666E-03  -1.281361E-03
  a    14   8.678703E-04   5.206869E-04  -2.151538E-03   1.857785E-04  -2.110452E-03  -3.136527E-04   5.610352E-04   7.880186E-05
  a    15   4.958362E-04  -1.025554E-03  -1.490873E-03   7.142403E-04  -1.481627E-03  -3.256486E-03  -2.128454E-04   2.792759E-04
  a    16   1.421720E-03  -1.767701E-03   1.606299E-04  -2.596445E-04  -7.739059E-05  -1.453646E-03  -1.897740E-03   7.531384E-04
  a    17   4.534996E-04   3.569932E-05   2.655301E-05  -1.910295E-03  -1.458210E-03  -3.293549E-03   1.366623E-03   2.783075E-04
  a    18   2.859475E-03   2.555302E-04  -3.263048E-03   1.178428E-03  -1.609852E-04   2.088482E-03   6.133402E-04  -9.971631E-05
  a    19   1.155421E-03  -1.132140E-04   5.853477E-04   5.297101E-03  -1.431139E-04  -3.054991E-03   1.525021E-03  -1.599769E-03
  a    20  -1.918188E-03   4.770293E-04  -1.361188E-03   1.628962E-03  -3.372342E-04   1.445390E-03  -1.384078E-03  -1.558420E-03
  a    21  -5.279003E-04   2.850258E-03   3.144185E-03  -6.078172E-04  -5.375916E-03  -1.588639E-03   5.966857E-04   1.091076E-03
  a    22   3.984253E-05   4.306142E-04   2.524359E-04   2.363985E-03   7.407547E-04  -1.852553E-03  -7.722771E-04  -3.164800E-04
  a    23  -1.541754E-03   7.886943E-04   4.382510E-03   3.505556E-04  -3.855311E-03   1.253522E-03   1.545537E-03   6.452290E-04
  a    24   4.166608E-04  -4.545827E-04  -3.133165E-03   1.139563E-03  -3.603187E-03  -5.226661E-04  -1.091059E-03  -5.868020E-04
  a    25   1.241744E-03   5.972930E-04   3.290741E-03  -1.295673E-03  -9.618209E-04   3.101101E-03   1.023532E-03  -1.251694E-04
  a    26  -9.168694E-04  -2.553124E-03  -1.311802E-03  -2.859398E-03   2.900292E-03   1.660393E-03  -1.807067E-03   1.594162E-03
  a    27   1.215455E-03   8.950546E-04  -1.158170E-04  -4.617048E-04   1.659092E-03  -3.861964E-04  -7.033784E-04  -1.868142E-04
  a    28  -2.967563E-04  -1.739617E-04  -1.700943E-03  -5.472004E-03  -1.935329E-03   3.820866E-03  -2.924601E-04   9.689964E-05
  a    29   2.179518E-04   3.444356E-04   1.271592E-04  -5.936395E-05   6.982911E-06   2.897352E-06  -9.448876E-06  -3.726672E-05
  a    30   5.162073E-04   7.764973E-04  -7.098195E-05  -6.647683E-05   5.795456E-04   5.611962E-05  -1.063655E-04   8.997431E-05
  a    31   3.739725E-04   2.780191E-04   2.370191E-04  -1.035949E-04   9.582504E-05  -2.284808E-04  -2.793485E-04  -2.121061E-04
  a    32  -7.468434E-04  -9.043503E-04  -4.365734E-04   1.910626E-04  -5.147870E-05   1.982900E-04   1.409687E-04   2.376025E-04
  a    33   6.712079E-03  -6.284501E-03  -1.510013E-02  -2.559303E-03   8.442457E-03  -2.897322E-03  -3.252363E-03  -2.567116E-04
  a    34  -2.497824E-03  -3.325129E-03  -1.656121E-03   6.407271E-04  -4.361885E-04   7.376088E-04   9.989712E-04   1.147719E-03
  a    35   1.159631E-03   1.333358E-03   7.688706E-04  -2.805770E-04   2.509074E-04  -3.890314E-04  -5.770147E-04  -6.087959E-04
  a    36  -1.661283E-04  -1.200998E-04  -5.993269E-06  -2.590534E-06  -5.453815E-07   5.624341E-05   9.449820E-05   1.297562E-04
  a    37   1.682957E-04   1.714459E-04  -2.594453E-04  -3.507938E-04   1.025583E-04  -2.496587E-04   2.991885E-04   1.218888E-04
  a    38  -1.247673E-04  -3.403339E-04  -7.219594E-05   1.024138E-03  -4.234446E-04  -3.848408E-04  -7.085343E-04  -1.941658E-05
  a    39  -2.077230E-04  -6.568141E-05  -2.891732E-04   5.197764E-04   3.102545E-04   6.524719E-05  -1.946636E-04   1.263263E-04
  a    40  -7.975403E-04   6.573388E-04   1.288606E-03  -5.453614E-04  -1.657836E-05   8.676970E-04   5.767628E-04  -1.246798E-04
  a    41   1.351253E-03  -6.164214E-04  -8.870349E-04  -1.180903E-04   2.555594E-04   1.800512E-04  -2.890951E-04  -9.084183E-05
  a    42  -6.164214E-04   9.204411E-04   8.946640E-04   4.818590E-04  -6.586041E-04  -1.520713E-04   4.543317E-04   2.193036E-05
  a    43  -8.870349E-04   8.946640E-04   3.462353E-03   6.821391E-04  -1.275590E-03   5.948585E-05   4.019059E-04   1.226331E-04
  a    44  -1.180903E-04   4.818590E-04   6.821391E-04   3.142349E-03  -5.173860E-04  -1.308573E-03   1.533577E-04   1.546713E-04
  a    45   2.555594E-04  -6.586041E-04  -1.275590E-03  -5.173860E-04   2.790825E-03   6.345246E-04  -2.667418E-04  -7.802360E-05
  a    46   1.800512E-04  -1.520713E-04   5.948585E-05  -1.308573E-03   6.345246E-04   2.833053E-03  -4.246928E-04  -4.714888E-04
  a    47  -2.890951E-04   4.543317E-04   4.019059E-04   1.533577E-04  -2.667418E-04  -4.246928E-04   1.363442E-03   1.343432E-04
  a    48  -9.084183E-05   2.193036E-05   1.226331E-04   1.546713E-04  -7.802360E-05  -4.714888E-04   1.343432E-04   8.358798E-04
  a    49   1.535466E-04  -9.891036E-05   4.742592E-04   1.535398E-04  -2.866713E-04   2.385937E-04  -3.462042E-04  -1.549107E-04
  a    50  -6.002309E-04   7.410772E-04   1.175911E-03  -9.811887E-05  -3.623308E-04   5.028412E-04   4.977450E-04   6.120087E-05
  a    51   1.108805E-04   4.116141E-05  -2.653606E-05  -1.044570E-05   3.118693E-05  -5.689977E-05  -6.134406E-05  -3.919499E-05
  a    52   5.207877E-05   1.422798E-04   3.449273E-04  -1.609824E-04  -2.566916E-04  -3.895702E-04   3.437126E-04   4.634324E-05
  a    53  -5.526612E-04   3.878815E-04   1.103699E-03   3.625350E-04  -1.049677E-03  -2.201154E-04   1.475046E-04   8.134597E-05
  a    54   1.966525E-05   1.326214E-05  -2.055185E-05   3.047233E-05  -5.041712E-05  -2.020977E-05   1.604670E-05   1.837090E-05
  a    55  -2.521875E-04  -4.090162E-05   3.545464E-04  -2.102064E-04   8.271968E-04   3.318084E-04  -8.777677E-06   8.260195E-05
  a    56   3.399372E-04  -9.766106E-05  -2.645990E-04   5.322179E-04   1.033329E-04   6.324227E-05  -3.236192E-04  -2.614297E-04
  a    57  -8.192555E-05   2.383944E-04   7.924917E-04   4.832997E-04  -9.925907E-04  -8.709474E-04   1.141329E-04   6.126840E-05
  a    58   4.443685E-05   6.262995E-05   2.838180E-05   4.158387E-05   1.624475E-07  -1.235337E-05   6.641862E-06  -1.626384E-05
  a    59   3.382001E-05   5.854040E-05   5.712614E-05   9.753090E-05  -2.422955E-05  -6.113964E-05  -3.339978E-07  -1.787771E-05
  a    60  -7.600372E-05   2.720869E-04   3.924556E-04   1.264673E-03  -4.869472E-04  -6.498348E-04   2.606178E-04  -1.171967E-05
  a    61  -6.683610E-05  -1.783193E-04  -2.733002E-04  -4.249898E-04   4.632061E-05  -2.247310E-04  -2.163527E-04   1.292715E-05
  a    62  -3.813282E-05  -8.039337E-05  -9.229212E-05  -1.111205E-04  -8.520435E-06   8.780466E-06  -7.192058E-06   1.588779E-05
  a    63   1.140068E-04   4.927728E-05   2.406992E-04   3.769550E-04  -2.308514E-04  -4.813730E-04  -8.087116E-06   9.385281E-05
  a    64  -5.077673E-05  -1.847492E-05  -2.351102E-05  -6.958541E-05   1.167653E-04   2.348641E-04  -9.047892E-06  -9.648764E-05
  a    65  -1.137705E-04   2.316753E-05  -4.719174E-05  -1.349543E-04   6.428997E-05  -6.056384E-05  -1.616910E-04   1.640767E-05
  a    66  -2.062771E-04   1.041904E-04  -6.459687E-05   9.818412E-05  -1.774759E-04  -8.232848E-05  -5.055234E-05  -5.232890E-05
  a    67   1.191361E-04  -1.162269E-06   2.381925E-05   1.855369E-04  -1.303570E-04  -1.612239E-04   7.819061E-05   4.973382E-05
  a    68  -3.973354E-05  -2.138669E-06   4.250722E-05   8.834726E-05   2.301587E-05   5.103130E-06   1.559920E-05   1.326181E-06
  a    69  -3.361024E-05  -6.101071E-05  -5.072245E-05  -5.454253E-05  -3.764098E-05  -9.931215E-06   9.925636E-06   1.996940E-05
  a    70  -4.255972E-05  -2.199233E-05  -3.306182E-06   1.831474E-05  -2.422306E-05  -6.790760E-05   1.145678E-05   1.752720E-05
  a    71   1.746503E-05  -3.048840E-05  -8.966776E-05   2.123711E-05   8.870480E-05  -8.443653E-05   5.734953E-05   5.619978E-05
  a    72   9.355889E-05  -5.682034E-05  -5.142942E-05   7.197760E-05   9.353583E-05   6.728721E-05   6.001264E-05  -4.743844E-05
  a    73  -4.945940E-05  -7.294522E-05  -3.963487E-05   1.962326E-05  -1.608073E-05  -1.016845E-05   3.461852E-05   4.024518E-05
  a    74   3.539306E-05  -4.407676E-05  -8.025157E-05  -2.593079E-04   1.850090E-04   4.697203E-04  -9.654095E-05  -9.180244E-05
  a    75   4.050164E-05  -1.867995E-05  -4.093033E-05  -2.576516E-04  -4.982743E-05   3.268323E-04  -9.845602E-05  -7.774109E-05
  a    76  -7.467985E-05   3.517786E-05   4.752443E-05  -3.009322E-05  -1.338203E-04   7.924983E-05  -4.040172E-05  -2.783051E-05
  a    77   2.541172E-05   9.192303E-06  -8.330955E-05  -1.807544E-04   1.864894E-04   1.294857E-04   8.865978E-05   1.387561E-05
  a    78   3.855259E-05   5.976649E-06   2.710832E-05   2.781664E-05  -2.287659E-04  -2.669581E-04  -1.370880E-05   6.166134E-05
  a    79   4.511847E-05   2.281036E-05  -5.685550E-05   1.065054E-04   1.136750E-04   5.151070E-05   3.481091E-05   1.884638E-05
  a    80   3.857273E-05  -1.744971E-05  -1.622374E-05   1.611223E-04  -5.293129E-05  -1.468575E-04  -2.188147E-06   6.101561E-05
  a    81   1.408880E-05  -2.597843E-05  -2.489795E-05  -1.614492E-04   1.140712E-04   1.401753E-04  -1.795255E-05   1.243904E-05
  a    82  -8.758051E-05   7.669494E-05   6.852148E-05   1.004800E-04  -1.028090E-04  -1.566865E-05   9.248894E-05   1.345433E-05
  a    83  -6.985809E-06   6.741556E-06  -4.330978E-05   4.659102E-05   7.447459E-05   6.093026E-06   5.346553E-05  -2.581268E-06
  a    84  -2.080471E-05   3.084434E-05   4.678845E-05   6.474313E-05  -1.096471E-04  -4.135250E-05   2.101924E-05   3.724951E-05
  a    85   2.048429E-05  -2.524006E-05  -6.709588E-05  -2.625935E-05   5.761945E-05   3.787205E-05  -8.467699E-06   7.852466E-06
  a    86  -5.016425E-06   2.086603E-05   2.417977E-05   6.630898E-05  -4.310405E-05   4.404287E-05  -3.114713E-05   6.408402E-06

               a    49        a    50        a    51        a    52        a    53        a    54        a    55        a    56
  a    13   1.073045E-03  -4.001689E-04   1.749449E-05   7.329936E-04  -3.842343E-04   7.222992E-06  -4.952486E-04   4.143282E-05
  a    14  -1.828390E-04   2.236694E-04  -7.492155E-06  -2.348155E-04  -1.099983E-04   9.389924E-05  -1.748961E-03  -4.128182E-04
  a    15  -7.048955E-04  -1.716291E-03   1.212435E-04   2.154331E-04   1.488619E-03   1.012349E-04  -1.857935E-03  -7.518779E-04
  a    16   1.020284E-03  -1.171674E-03   5.979503E-05  -1.161913E-03   4.768559E-04   1.292773E-06  -3.303853E-04   9.277355E-04
  a    17  -7.027974E-04   8.407058E-05  -1.100495E-05   1.731343E-03   5.075883E-04   5.713165E-05  -8.614213E-04   8.871442E-04
  a    18   5.271810E-04   1.953359E-03  -1.096838E-04  -8.146194E-04  -1.387560E-03   4.550925E-05   1.358698E-04   2.032556E-03
  a    19   6.639553E-04  -1.173903E-03   5.861111E-05   9.641966E-04   8.425542E-05   8.069244E-05  -1.010127E-03   2.371887E-03
  a    20  -2.925419E-04  -2.606827E-03   1.407097E-04  -9.162626E-05   1.130699E-03   5.067305E-05  -1.806862E-03  -5.671203E-04
  a    21   1.255702E-03   1.575756E-03  -5.707292E-05  -5.615081E-04  -1.844960E-03   1.252460E-04  -3.773522E-04  -2.588759E-03
  a    22   1.697879E-03  -1.313038E-03   1.379180E-04  -1.485010E-03   1.616117E-04   1.139601E-04  -4.539188E-04   2.822602E-03
  a    23   4.972229E-04   1.253463E-03  -1.150099E-05  -8.371499E-04  -1.275389E-03   1.974564E-05   1.103359E-03  -2.603490E-03
  a    24   9.212865E-04  -2.813356E-04   5.069183E-05  -2.836819E-03  -4.571419E-04   1.223326E-04  -7.303731E-04  -1.016311E-03
  a    25  -1.493940E-03  -1.034500E-04   3.270427E-05  -8.641764E-04   9.572788E-04  -1.252669E-04   1.669062E-03  -2.496667E-03
  a    26   1.847800E-03  -2.282066E-03   3.535998E-05  -5.033770E-03   1.545331E-03  -3.709491E-04   2.830180E-03  -3.923176E-03
  a    27   1.337298E-04  -6.146818E-04   8.482958E-04   1.308091E-04   3.752000E-04   4.526285E-04   2.751492E-04  -8.498458E-04
  a    28  -1.292843E-03   1.042387E-03   3.958837E-05  -2.059208E-03   1.074458E-03   7.027631E-06  -2.564749E-05  -1.221656E-04
  a    29  -4.665038E-05   9.420870E-05   2.821833E-03   8.355895E-05   1.455705E-04   2.016648E-03   2.990204E-05  -7.793869E-05
  a    30   7.407550E-05  -5.003360E-04  -2.804883E-03   1.891354E-04   1.919056E-04   8.980187E-03   4.977905E-04  -1.712596E-04
  a    31  -1.817026E-05   1.576030E-04   2.955656E-03   1.313789E-04   1.080933E-04  -2.400586E-03  -1.584027E-04  -1.556744E-04
  a    32   9.309458E-05   3.745277E-04   5.603253E-03   1.064870E-05   4.900938E-05  -3.083188E-04   9.013358E-05   1.231996E-04
  a    33  -1.792730E-03  -6.400877E-03   3.864652E-04  -1.913533E-03  -8.535406E-03   6.410634E-05  -3.217041E-04   7.978641E-05
  a    34   4.030101E-04  -7.706375E-04  -1.621582E-02  -3.833373E-04  -5.701650E-04  -1.342991E-03   1.784568E-04   4.391793E-04
  a    35  -1.729589E-04   5.733209E-04   1.117736E-02   1.646982E-04   1.442767E-04   7.437658E-04  -3.354849E-05  -2.057819E-04
  a    36   8.578461E-06   2.887043E-06   3.570130E-04  -1.995469E-05   3.800395E-05   9.668752E-04   3.714347E-05  -1.181117E-05
  a    37  -7.693934E-04   9.638904E-05  -3.517060E-05   1.078229E-03  -8.575427E-04   2.267529E-05  -1.504990E-04   1.123504E-04
  a    38   6.744394E-04  -8.015250E-04  -2.977850E-06  -1.267588E-03   4.477229E-04   2.316010E-05  -1.485053E-04   5.275457E-05
  a    39   2.071582E-04  -3.962927E-04   1.175271E-04  -8.279825E-04   1.304961E-04  -2.089729E-05   5.785794E-05  -4.360459E-05
  a    40   1.243831E-04   1.362434E-03  -7.053803E-05   6.094729E-04   5.681602E-04  -2.833376E-05   4.736876E-04  -1.524314E-04
  a    41   1.535466E-04  -6.002309E-04   1.108805E-04   5.207877E-05  -5.526612E-04   1.966525E-05  -2.521875E-04   3.399372E-04
  a    42  -9.891036E-05   7.410772E-04   4.116141E-05   1.422798E-04   3.878815E-04   1.326214E-05  -4.090162E-05  -9.766106E-05
  a    43   4.742592E-04   1.175911E-03  -2.653606E-05   3.449273E-04   1.103699E-03  -2.055185E-05   3.545464E-04  -2.645990E-04
  a    44   1.535398E-04  -9.811887E-05  -1.044570E-05  -1.609824E-04   3.625350E-04   3.047233E-05  -2.102064E-04   5.322179E-04
  a    45  -2.866713E-04  -3.623308E-04   3.118693E-05  -2.566916E-04  -1.049677E-03  -5.041712E-05   8.271968E-04   1.033329E-04
  a    46   2.385937E-04   5.028412E-04  -5.689977E-05  -3.895702E-04  -2.201154E-04  -2.020977E-05   3.318084E-04   6.324227E-05
  a    47  -3.462042E-04   4.977450E-04  -6.134406E-05   3.437126E-04   1.475046E-04   1.604670E-05  -8.777677E-06  -3.236192E-04
  a    48  -1.549107E-04   6.120087E-05  -3.919499E-05   4.634324E-05   8.134597E-05   1.837090E-05   8.260195E-05  -2.614297E-04
  a    49   1.105902E-03  -5.368723E-05  -1.155897E-05  -1.080649E-04   3.586307E-04   9.807932E-06  -1.514067E-05   2.089068E-04
  a    50  -5.368723E-05   1.524896E-03  -2.899519E-05   2.762098E-04   4.757497E-04  -1.870127E-05   2.069164E-04  -1.617463E-04
  a    51  -1.155897E-05  -2.899519E-05   1.143220E-03  -7.593823E-06  -1.685327E-05   5.679113E-05  -1.730683E-05  -5.181434E-06
  a    52  -1.080649E-04   2.762098E-04  -7.593823E-06   1.369888E-03   2.132885E-05   1.519882E-05  -1.940085E-04   1.029846E-04
  a    53   3.586307E-04   4.757497E-04  -1.685327E-05   2.132885E-05   1.424291E-03   1.344775E-05  -2.189789E-04  -1.382992E-04
  a    54   9.807932E-06  -1.870127E-05   5.679113E-05   1.519882E-05   1.344775E-05   9.416858E-04  -5.512020E-06   1.110756E-05
  a    55  -1.514067E-05   2.069164E-04  -1.730683E-05  -1.940085E-04  -2.189789E-04  -5.512020E-06   8.420749E-04  -1.246684E-04
  a    56   2.089068E-04  -1.617463E-04  -5.181434E-06   1.029846E-04  -1.382992E-04   1.110756E-05  -1.246684E-04   8.385013E-04
  a    57   1.454518E-04  -3.912994E-05   8.220853E-06   3.920515E-04   2.356498E-04   2.628519E-05  -3.219784E-04   5.831124E-07
  a    58  -1.837385E-05   1.549352E-05   2.759686E-04  -1.033521E-06   2.997938E-06   3.999110E-05  -9.141319E-06  -6.634100E-06
  a    59  -7.993798E-06   6.841868E-06   1.733152E-04   1.311818E-05   4.082558E-07  -1.012419E-04  -2.282685E-05   1.048823E-05
  a    60  -1.186512E-04  -1.163972E-04  -2.134034E-05   1.443546E-04   6.517849E-05   2.714262E-05  -3.394415E-04   5.164913E-05
  a    61  -8.002351E-05  -2.057824E-04   4.907438E-05  -3.002385E-06   1.616376E-04   1.129030E-05   2.098784E-05  -5.694418E-05
  a    62  -2.958701E-05  -3.687944E-05  -2.285818E-04  -1.840752E-05   3.552938E-05  -9.255569E-05  -1.565958E-05  -4.649669E-05
  a    63   2.113249E-04  -9.043633E-06  -3.857349E-05   1.598255E-04  -8.136716E-05   3.377994E-06  -2.250745E-06   2.313735E-04
  a    64  -6.430624E-05   5.610246E-05  -6.550303E-05  -4.155913E-05  -2.461555E-05  -1.361231E-07   3.087893E-05  -4.302896E-05
  a    65   2.172588E-04  -1.035035E-04  -2.561374E-06   4.961293E-05  -4.323354E-05  -1.999688E-06   6.304003E-05   5.378635E-05
  a    66  -2.228917E-05  -1.935549E-05   2.199043E-06   7.062192E-05   1.096635E-04   7.304726E-06  -1.010921E-04   5.926599E-05
  a    67  -1.937856E-05  -4.685343E-05  -1.021269E-05   1.172314E-04   6.721127E-05   8.493694E-06  -7.742832E-05   2.426217E-05
  a    68   1.304368E-06  -8.631075E-06  -1.791931E-04  -4.088896E-07  -5.071055E-06  -4.120842E-05   1.271805E-05   1.949217E-05
  a    69   1.369445E-06  -5.893107E-06  -1.827000E-04  -6.178191E-06  -5.139392E-07  -9.181012E-05  -1.714042E-05  -1.387452E-05
  a    70  -2.654889E-06  -2.595688E-05  -7.306736E-05   1.207524E-05   1.521888E-05   4.436549E-05  -1.185667E-05  -7.245884E-06
  a    71   2.973613E-05  -2.784684E-05   1.966232E-05   9.233372E-06  -5.026240E-06  -2.285360E-05   2.586424E-05  -2.145016E-05
  a    72   1.467724E-04  -6.606402E-05   1.534705E-05   4.543337E-05   1.234096E-05  -6.518927E-06  -1.863556E-05  -3.272390E-05
  a    73   1.774633E-05  -2.157649E-05  -2.874019E-04  -2.969188E-06  -3.163570E-06   2.381731E-04   1.102969E-05   3.194541E-06
  a    74   8.101329E-05   1.288382E-04  -3.072910E-05  -1.046677E-04  -1.275537E-04  -1.476312E-06   1.220483E-04   5.191171E-05
  a    75   4.609094E-05   6.789859E-05  -2.045997E-06  -3.830335E-05  -8.081155E-06  -1.830381E-06   1.696641E-05   2.653665E-05
  a    76  -5.584415E-05   4.853270E-05  -4.253004E-07  -1.157012E-04   8.760233E-05   1.972621E-06  -1.968225E-05  -3.225793E-05
  a    77  -7.470458E-05   5.939160E-05  -5.570437E-06   1.343024E-04  -9.732921E-05  -3.091152E-06   4.457107E-05  -1.048346E-05
  a    78   4.220511E-05  -7.225778E-05   1.653699E-05   7.151750E-05   7.461204E-05  -3.132733E-07  -8.647388E-05  -9.069592E-06
  a    79   9.450909E-06   3.958666E-06   1.459926E-05  -5.921664E-05  -8.093202E-05   3.542724E-07   1.547713E-05   1.793415E-05
  a    80  -4.382614E-06  -9.054132E-05  -1.741968E-06  -3.075961E-05   1.198906E-05   2.160946E-06  -4.379224E-05  -4.296255E-06
  a    81   1.982991E-05   5.307695E-05  -3.868846E-06   3.346730E-05  -7.947112E-05  -6.062690E-06   7.024660E-05  -3.325048E-05
  a    82  -6.292413E-05   4.248779E-05  -1.453463E-06   4.079612E-05   1.008207E-04   1.592814E-06  -4.753927E-05  -5.190269E-05
  a    83  -3.283554E-05   1.803234E-05  -3.777119E-06  -3.478729E-06  -4.433750E-05  -1.699673E-06   8.165474E-06   3.552773E-06
  a    84   3.765669E-05   2.944343E-05  -3.374953E-06   2.895266E-06   2.749600E-05   2.335782E-06  -2.396845E-05  -1.776372E-07
  a    85  -1.984450E-05  -2.151328E-05  -4.182424E-07  -6.728574E-06  -4.207632E-05   9.658966E-07   4.703911E-06   3.477429E-06
  a    86   3.307452E-05   2.615194E-05  -1.825819E-06  -2.042062E-05  -2.747161E-05   4.233627E-07   3.323481E-06   1.115126E-05

               a    57        a    58        a    59        a    60        a    61        a    62        a    63        a    64
  a    13   2.442664E-04  -2.588273E-05  -4.978496E-05  -1.501803E-04   1.340504E-04   6.550250E-05  -5.708710E-04   2.676668E-04
  a    14   2.947094E-04   6.378393E-05   3.306894E-05   6.477895E-04  -6.125325E-04  -7.997513E-06  -3.155798E-04  -1.037884E-05
  a    15   7.158031E-04   2.960236E-05   5.490436E-05   1.318615E-03   6.366771E-04   1.177208E-04  -1.478838E-05  -1.258513E-04
  a    16   4.135294E-04  -1.248898E-04  -8.262768E-05  -1.145943E-03   1.483240E-03   1.740227E-04   1.434507E-04  -2.718949E-04
  a    17   1.197258E-03  -1.040589E-04  -3.956685E-05  -9.261138E-04   6.996952E-04  -4.000643E-05   1.250103E-03  -3.283366E-04
  a    18  -3.682789E-03   1.653007E-04   1.497848E-04  -9.372157E-04  -1.294372E-03  -2.043537E-04   7.238604E-04  -3.549465E-04
  a    19   5.303968E-04   3.347611E-05   1.495479E-04   2.564895E-03   9.045321E-04  -8.404148E-06   2.936834E-04  -1.354749E-04
  a    20   1.941768E-03  -6.105484E-05  -8.720320E-05   1.435022E-03   9.524646E-04   2.534179E-04  -1.113550E-03   3.661987E-04
  a    21   3.729298E-03   5.625348E-05   1.028446E-04   3.834042E-04  -1.633478E-03  -4.082918E-04   2.304734E-03  -7.296040E-04
  a    22   8.197742E-04  -5.187760E-05   1.031396E-04  -1.249112E-03   4.067416E-04   7.598458E-05  -3.671107E-04  -4.429236E-04
  a    23   2.606076E-03   6.753777E-05   2.485654E-04   1.953928E-03  -1.503653E-04  -1.288030E-04  -7.885621E-05  -4.031227E-05
  a    24   7.690771E-05   1.454437E-04   1.244021E-04  -7.809699E-05  -2.749167E-03  -3.246814E-04  -4.243347E-04   5.353030E-04
  a    25   7.232835E-04   3.930726E-05   4.758591E-05   2.756217E-03   6.706669E-04   2.861979E-04  -2.702593E-03   7.196016E-04
  a    26  -4.026088E-03  -7.121331E-05  -4.855645E-04  -3.126252E-03   1.029505E-03   5.345237E-04  -1.354151E-03  -1.456714E-04
  a    27  -6.833026E-04  -4.369665E-05   1.204095E-03  -4.538050E-04   7.649172E-04  -9.009122E-04  -3.849025E-04  -4.620814E-04
  a    28  -2.800342E-03  -5.524160E-05  -7.850673E-05  -3.327294E-03  -1.929043E-04   8.312845E-06  -2.304839E-04  -1.237064E-04
  a    29  -2.577033E-05   1.697783E-03   1.609605E-04  -1.396042E-04   3.722824E-04  -2.391815E-03  -3.993519E-04  -3.908252E-04
  a    30  -1.038460E-04   4.177619E-04   5.955551E-03  -3.882993E-04   4.938653E-05   3.148114E-04  -1.430505E-04   3.135798E-04
  a    31  -1.666420E-04  -5.486401E-03   7.417398E-03  -2.687684E-04  -4.975816E-04   2.863965E-03   1.757588E-04   2.149747E-05
  a    32  -5.989309E-05  -1.732296E-03   9.907733E-05   1.843025E-04  -4.466743E-04   3.231408E-03   3.894498E-04   6.186231E-04
  a    33   4.618626E-04   4.621729E-05   3.420726E-05   1.044581E-03   3.322340E-04  -7.728659E-05   4.032162E-04  -2.688356E-04
  a    34  -2.103120E-04  -8.216730E-03  -5.704439E-03   8.821221E-04  -1.357325E-03   9.157704E-03   1.548704E-03   2.162145E-03
  a    35   3.934701E-05   2.346183E-03   3.627986E-03  -4.062893E-04   3.871368E-04  -2.628230E-03  -5.139368E-04  -7.721436E-04
  a    36   1.339584E-05  -6.637854E-04   1.020628E-03  -4.672618E-05  -2.817682E-05   1.296034E-04  -1.498858E-05   8.680263E-05
  a    37   3.403593E-04  -1.079233E-05   1.104820E-05   3.437811E-04  -8.170081E-05  -3.199806E-05   1.812035E-04  -4.005554E-05
  a    38   9.215696E-05  -3.012117E-05  -1.001915E-05   3.274562E-04   4.876192E-05  -3.258658E-05   7.750657E-05  -4.460826E-05
  a    39  -2.195556E-04   9.439635E-05   6.760093E-05   2.006655E-04  -7.051041E-05   3.083793E-05  -6.675037E-05  -4.265803E-05
  a    40  -1.455914E-04  -4.258779E-06  -1.808591E-05  -3.951065E-04  -8.926957E-05  -2.286136E-05  -6.921939E-05   8.814240E-05
  a    41  -8.192555E-05   4.443685E-05   3.382001E-05  -7.600372E-05  -6.683610E-05  -3.813282E-05   1.140068E-04  -5.077673E-05
  a    42   2.383944E-04   6.262995E-05   5.854040E-05   2.720869E-04  -1.783193E-04  -8.039337E-05   4.927728E-05  -1.847492E-05
  a    43   7.924917E-04   2.838180E-05   5.712614E-05   3.924556E-04  -2.733002E-04  -9.229212E-05   2.406992E-04  -2.351102E-05
  a    44   4.832997E-04   4.158387E-05   9.753090E-05   1.264673E-03  -4.249898E-04  -1.111205E-04   3.769550E-04  -6.958541E-05
  a    45  -9.925907E-04   1.624475E-07  -2.422955E-05  -4.869472E-04   4.632061E-05  -8.520435E-06  -2.308514E-04   1.167653E-04
  a    46  -8.709474E-04  -1.235337E-05  -6.113964E-05  -6.498348E-04  -2.247310E-04   8.780466E-06  -4.813730E-04   2.348641E-04
  a    47   1.141329E-04   6.641862E-06  -3.339978E-07   2.606178E-04  -2.163527E-04  -7.192058E-06  -8.087116E-06  -9.047892E-06
  a    48   6.126840E-05  -1.626384E-05  -1.787771E-05  -1.171967E-05   1.292715E-05   1.588779E-05   9.385281E-05  -9.648764E-05
  a    49   1.454518E-04  -1.837385E-05  -7.993798E-06  -1.186512E-04  -8.002351E-05  -2.958701E-05   2.113249E-04  -6.430624E-05
  a    50  -3.912994E-05   1.549352E-05   6.841868E-06  -1.163972E-04  -2.057824E-04  -3.687944E-05  -9.043633E-06   5.610246E-05
  a    51   8.220853E-06   2.759686E-04   1.733152E-04  -2.134034E-05   4.907438E-05  -2.285818E-04  -3.857349E-05  -6.550303E-05
  a    52   3.920515E-04  -1.033521E-06   1.311818E-05   1.443546E-04  -3.002385E-06  -1.840752E-05   1.598255E-04  -4.155913E-05
  a    53   2.356498E-04   2.997938E-06   4.082558E-07   6.517849E-05   1.616376E-04   3.552938E-05  -8.136716E-05  -2.461555E-05
  a    54   2.628519E-05   3.999110E-05  -1.012419E-04   2.714262E-05   1.129030E-05  -9.255569E-05   3.377994E-06  -1.361231E-07
  a    55  -3.219784E-04  -9.141319E-06  -2.282685E-05  -3.394415E-04   2.098784E-05  -1.565958E-05  -2.250745E-06   3.087893E-05
  a    56   5.831124E-07  -6.634100E-06   1.048823E-05   5.164913E-05  -5.694418E-05  -4.649669E-05   2.313735E-04  -4.302896E-05
  a    57   1.002337E-03   1.501826E-05   4.059964E-05   5.159778E-04  -7.674554E-05  -4.885792E-05   3.427919E-04  -1.237069E-04
  a    58   1.501826E-05   6.711761E-04   3.515123E-05   1.151412E-05   1.672725E-05  -1.313630E-04  -1.728627E-05  -2.549488E-05
  a    59   4.059964E-05   3.515123E-05   5.201878E-04   4.229682E-05  -3.275668E-05   1.009101E-04   2.757432E-05  -6.271985E-06
  a    60   5.159778E-04   1.151412E-05   4.229682E-05   1.091386E-03  -2.398945E-04  -5.343785E-05   1.304151E-04  -2.819959E-05
  a    61  -7.674554E-05   1.672725E-05  -3.275668E-05  -2.398945E-04   4.289969E-04   2.812517E-05  -1.389039E-04  -8.591476E-06
  a    62  -4.885792E-05  -1.313630E-04   1.009101E-04  -5.343785E-05   2.812517E-05   4.033673E-04  -1.606309E-05   5.791032E-05
  a    63   3.427919E-04  -1.728627E-05   2.757432E-05   1.304151E-04  -1.389039E-04  -1.606309E-05   4.731575E-04  -1.199898E-04
  a    64  -1.237069E-04  -2.549488E-05  -6.271985E-06  -2.819959E-05  -8.591476E-06   5.791032E-05  -1.199898E-04   7.615728E-05
  a    65   7.609871E-05  -1.246571E-05  -4.355822E-06  -8.814513E-05   4.527430E-06  -9.734719E-06   1.320936E-04  -5.786177E-05
  a    66   5.179148E-05   2.562844E-06   4.992588E-06   6.249307E-05   6.879672E-05   1.488170E-05  -2.928437E-05   7.900413E-06
  a    67   6.052860E-05  -6.135320E-06   7.625511E-06   1.217134E-04  -4.516819E-06   1.909290E-06   2.851846E-05  -1.095639E-05
  a    68   1.858594E-05  -9.447620E-05   3.196497E-05   6.696082E-05  -4.736777E-05   9.126930E-05   2.864100E-05   3.076675E-05
  a    69  -2.938122E-06  -9.363692E-05   5.366851E-05  -4.083008E-05   1.155575E-05   1.106203E-04   7.359328E-06   2.954962E-05
  a    70   2.980163E-05  -7.692401E-05  -4.983979E-05   2.292774E-05   7.671422E-06  -1.598236E-05   9.756301E-06   9.454665E-06
  a    71  -5.601119E-05   7.134266E-06   2.969503E-06  -3.997343E-06  -9.082124E-06  -2.190465E-05   2.325775E-05  -2.172958E-05
  a    72  -2.081761E-05   1.366032E-05   7.978866E-06   6.532865E-05  -9.397528E-05  -1.837290E-05   6.810158E-06  -2.013276E-06
  a    73   2.049344E-06  -1.623866E-04  -1.245016E-04   2.168210E-05  -2.868421E-05   2.088806E-04   4.270077E-05   5.353145E-05
  a    74  -2.207636E-04  -1.699152E-05  -1.443738E-05  -2.106869E-04  -2.007584E-05   1.267546E-05  -6.255495E-05   4.437614E-05
  a    75  -1.149560E-04  -5.495914E-06  -1.246787E-05  -1.379407E-04   5.984823E-05   1.286230E-05  -8.404446E-05   3.233491E-05
  a    76  -1.132915E-05  -2.849964E-06  -6.544243E-06  -4.581934E-05   4.089101E-05   9.820514E-06  -7.594925E-05   1.991590E-05
  a    77  -9.089243E-05  -2.573773E-06  -6.930037E-06  -6.526947E-05  -1.953973E-05  -1.745339E-06  -1.354498E-05   1.638522E-05
  a    78   1.527783E-04   3.128278E-06   7.212002E-06   6.654301E-05   6.115281E-05  -5.998844E-06   6.743749E-05  -4.505928E-05
  a    79  -5.696784E-05   1.375772E-05   7.085729E-06   3.757192E-06  -6.243690E-05  -1.565318E-05   1.009889E-05   2.458956E-06
  a    80   5.220497E-05  -3.574837E-06   2.614603E-06   9.653851E-05  -2.494897E-06   7.087213E-07   2.516523E-05  -1.237459E-05
  a    81  -7.345008E-05  -3.128277E-06  -7.848252E-06  -1.015992E-04  -8.096523E-06  -1.440178E-07  -2.261398E-05   1.738651E-05
  a    82   3.432963E-05   3.720578E-06   2.838805E-06   8.378821E-05  -2.055419E-05   1.917100E-06  -2.961066E-05   8.924531E-06
  a    83  -2.851364E-05  -5.536038E-07  -6.167404E-07   2.242489E-05  -1.492523E-05  -2.311906E-06  -1.246160E-05   7.080087E-06
  a    84   4.600738E-05  -3.024885E-06  -2.040915E-07  -6.264604E-06  -1.042962E-05  -1.259598E-06   1.227316E-05  -4.077397E-06
  a    85  -2.755458E-05  -3.256123E-07   5.319663E-07   1.606084E-05  -2.726754E-06  -4.893137E-07  -3.973714E-07  -1.784530E-06
  a    86   2.753407E-06   9.821115E-07   1.334535E-06  -1.645345E-06  -1.828288E-05  -5.997709E-07  -8.131786E-06   7.648095E-06

               a    65        a    66        a    67        a    68        a    69        a    70        a    71        a    72
  a    13   2.693806E-05   8.184146E-07  -3.828281E-04   4.598115E-06  -3.356018E-05  -4.645041E-05  -5.236655E-04  -1.264732E-04
  a    14  -1.672490E-04  -4.795658E-05  -2.306142E-04   7.683310E-05  -3.984411E-05   6.762557E-05  -1.487184E-04  -1.994812E-04
  a    15  -2.300707E-04   4.028109E-05  -4.385299E-05   1.790867E-04  -1.238616E-04  -5.425052E-05  -2.024730E-04   3.088360E-04
  a    16   8.577753E-04   1.942288E-05  -7.355845E-05  -1.880901E-04   1.677378E-04   7.204883E-05   8.214757E-04   1.148764E-03
  a    17  -1.413592E-04   4.351030E-04   5.336502E-04   7.356741E-05  -7.854726E-05  -9.658969E-05   2.603224E-05  -1.535930E-03
  a    18   1.878833E-04   3.106646E-04   5.727454E-04  -3.111317E-04   2.069438E-04   6.887964E-05   1.186318E-03   4.638046E-04
  a    19  -2.171851E-04  -9.315815E-04   1.567666E-04   9.413265E-05   4.902348E-06   2.851198E-05  -7.513669E-04   6.107238E-04
  a    20   7.252326E-04   1.931194E-03   9.974869E-04  -2.010529E-04   1.301505E-04  -8.029318E-05   9.248122E-05   1.810353E-03
  a    21   7.114234E-04   2.171052E-04  -7.460267E-04   2.087841E-05   4.456655E-05   2.460328E-04  -1.583969E-04  -6.480627E-04
  a    22   1.413429E-03  -9.522412E-04  -7.245787E-04  -9.140537E-05   4.513705E-05   6.452103E-05  -2.086692E-05   8.468533E-04
  a    23   5.235306E-04  -4.476831E-04   2.357023E-05  -4.089515E-04   2.299942E-04  -2.891294E-05  -1.075142E-04   2.344001E-05
  a    24  -9.689737E-04   5.603635E-04  -1.854117E-03   1.638845E-04  -1.312653E-04   1.587039E-04   1.010524E-03   2.187394E-03
  a    25  -1.229212E-03  -1.773409E-04   3.351742E-04   4.510766E-04  -4.109378E-04  -2.274730E-04  -6.960686E-05   9.151549E-04
  a    26   1.804278E-03  -1.698384E-03  -1.276207E-03  -1.829056E-04   2.819527E-04   1.588235E-04   1.182475E-03   2.234640E-03
  a    27   4.519443E-05  -7.409910E-05   8.259679E-05  -1.385476E-04  -6.051427E-04   5.098367E-05   1.216953E-04   1.467979E-04
  a    28   4.258900E-05  -4.237171E-04  -3.770323E-04  -3.147234E-04   2.472162E-04   2.063161E-04   4.774043E-04   6.615276E-04
  a    29  -4.578306E-05   5.504101E-06  -6.522849E-05  -1.655225E-03  -2.004107E-03  -2.468136E-05   6.755875E-05   6.476399E-05
  a    30   8.695224E-05   1.824685E-05   2.783700E-04   1.745456E-03   1.594727E-03   1.230019E-03  -2.275120E-04  -3.127101E-05
  a    31  -4.953235E-06  -2.155797E-06  -3.600178E-05  -2.198908E-03  -2.799527E-03   2.663131E-04   3.609142E-05   4.974611E-05
  a    32   9.530418E-05  -4.147068E-05   1.592401E-04   2.596970E-04  -2.337639E-04   1.506193E-03  -8.556942E-05  -5.704675E-05
  a    33   1.273298E-04  -1.106810E-03  -3.975258E-04   5.493722E-05  -5.216112E-05   4.745647E-05  -6.184882E-04  -1.315838E-03
  a    34   2.778112E-04  -3.913795E-05   4.355825E-04   5.905302E-03   5.796351E-03   2.472447E-03  -6.940858E-04  -4.065467E-04
  a    35  -9.609214E-05   1.212803E-05  -1.282969E-04  -1.995912E-03  -2.057421E-03  -7.979548E-04   1.910051E-04   1.333964E-04
  a    36   1.873330E-05  -9.469003E-06   4.204620E-05  -3.325780E-05  -1.569871E-04   3.161574E-04  -2.776515E-05  -1.281105E-05
  a    37   2.421818E-06   1.365854E-05  -9.693259E-05   5.174879E-05  -1.624429E-05   2.454554E-05  -1.205978E-04  -2.647843E-04
  a    38   9.570583E-05   4.595964E-05  -9.177340E-05   3.858465E-05  -2.823375E-06   5.139094E-05   5.643717E-05   1.470041E-04
  a    39   1.079625E-04   3.118708E-05  -1.148195E-04  -2.484250E-06  -5.777342E-05  -5.427442E-05   8.122984E-05   1.476609E-04
  a    40  -2.237101E-06   1.656665E-04  -1.419098E-04   9.550451E-06  -1.907684E-05  -1.902517E-05   1.035649E-04   9.957788E-05
  a    41  -1.137705E-04  -2.062771E-04   1.191361E-04  -3.973354E-05  -3.361024E-05  -4.255972E-05   1.746503E-05   9.355889E-05
  a    42   2.316753E-05   1.041904E-04  -1.162269E-06  -2.138669E-06  -6.101071E-05  -2.199233E-05  -3.048840E-05  -5.682034E-05
  a    43  -4.719174E-05  -6.459687E-05   2.381925E-05   4.250722E-05  -5.072245E-05  -3.306182E-06  -8.966776E-05  -5.142942E-05
  a    44  -1.349543E-04   9.818412E-05   1.855369E-04   8.834726E-05  -5.454253E-05   1.831474E-05   2.123711E-05   7.197760E-05
  a    45   6.428997E-05  -1.774759E-04  -1.303570E-04   2.301587E-05  -3.764098E-05  -2.422306E-05   8.870480E-05   9.353583E-05
  a    46  -6.056384E-05  -8.232848E-05  -1.612239E-04   5.103130E-06  -9.931215E-06  -6.790760E-05  -8.443653E-05   6.728721E-05
  a    47  -1.616910E-04  -5.055234E-05   7.819061E-05   1.559920E-05   9.925636E-06   1.145678E-05   5.734953E-05   6.001264E-05
  a    48   1.640767E-05  -5.232890E-05   4.973382E-05   1.326181E-06   1.996940E-05   1.752720E-05   5.619978E-05  -4.743844E-05
  a    49   2.172588E-04  -2.228917E-05  -1.937856E-05   1.304368E-06   1.369445E-06  -2.654889E-06   2.973613E-05   1.467724E-04
  a    50  -1.035035E-04  -1.935549E-05  -4.685343E-05  -8.631075E-06  -5.893107E-06  -2.595688E-05  -2.784684E-05  -6.606402E-05
  a    51  -2.561374E-06   2.199043E-06  -1.021269E-05  -1.791931E-04  -1.827000E-04  -7.306736E-05   1.966232E-05   1.534705E-05
  a    52   4.961293E-05   7.062192E-05   1.172314E-04  -4.088896E-07  -6.178191E-06   1.207524E-05   9.233372E-06   4.543337E-05
  a    53  -4.323354E-05   1.096635E-04   6.721127E-05  -5.071055E-06  -5.139392E-07   1.521888E-05  -5.026240E-06   1.234096E-05
  a    54  -1.999688E-06   7.304726E-06   8.493694E-06  -4.120842E-05  -9.181012E-05   4.436549E-05  -2.285360E-05  -6.518927E-06
  a    55   6.304003E-05  -1.010921E-04  -7.742832E-05   1.271805E-05  -1.714042E-05  -1.185667E-05   2.586424E-05  -1.863556E-05
  a    56   5.378635E-05   5.926599E-05   2.426217E-05   1.949217E-05  -1.387452E-05  -7.245884E-06  -2.145016E-05  -3.272390E-05
  a    57   7.609871E-05   5.179148E-05   6.052860E-05   1.858594E-05  -2.938122E-06   2.980163E-05  -5.601119E-05  -2.081761E-05
  a    58  -1.246571E-05   2.562844E-06  -6.135320E-06  -9.447620E-05  -9.363692E-05  -7.692401E-05   7.134266E-06   1.366032E-05
  a    59  -4.355822E-06   4.992588E-06   7.625511E-06   3.196497E-05   5.366851E-05  -4.983979E-05   2.969503E-06   7.978866E-06
  a    60  -8.814513E-05   6.249307E-05   1.217134E-04   6.696082E-05  -4.083008E-05   2.292774E-05  -3.997343E-06   6.532865E-05
  a    61   4.527430E-06   6.879672E-05  -4.516819E-06  -4.736777E-05   1.155575E-05   7.671422E-06  -9.082124E-06  -9.397528E-05
  a    62  -9.734719E-06   1.488170E-05   1.909290E-06   9.126930E-05   1.106203E-04  -1.598236E-05  -2.190465E-05  -1.837290E-05
  a    63   1.320936E-04  -2.928437E-05   2.851846E-05   2.864100E-05   7.359328E-06   9.756301E-06   2.325775E-05   6.810158E-06
  a    64  -5.786177E-05   7.900413E-06  -1.095639E-05   3.076675E-05   2.954962E-05   9.454665E-06  -2.172958E-05  -2.013276E-06
  a    65   2.524333E-04   7.464607E-06  -5.777953E-05   6.270490E-06   4.152748E-06   1.210662E-05   3.980938E-05   8.440190E-05
  a    66   7.464607E-06   1.718738E-04  -2.870091E-06   1.883059E-06  -9.345554E-07   4.435679E-07  -5.014157E-06  -5.527511E-05
  a    67  -5.777953E-05  -2.870091E-06   1.432624E-04   9.061113E-06   1.526294E-05   9.126549E-06   2.205075E-06  -3.141147E-05
  a    68   6.270490E-06   1.883059E-06   9.061113E-06   2.398826E-04   2.456665E-04   4.015375E-05  -1.064309E-05  -1.890366E-06
  a    69   4.152748E-06  -9.345554E-07   1.526294E-05   2.456665E-04   3.214374E-04   1.397590E-05  -4.492330E-06  -6.752577E-06
  a    70   1.210662E-05   4.435679E-07   9.126549E-06   4.015375E-05   1.397590E-05   1.161075E-04   1.047177E-06   1.398736E-06
  a    71   3.980938E-05  -5.014157E-06   2.205075E-06  -1.064309E-05  -4.492330E-06   1.047177E-06   9.026105E-05   7.842831E-05
  a    72   8.440190E-05  -5.527511E-05  -3.141147E-05  -1.890366E-06  -6.752577E-06   1.398736E-06   7.842831E-05   2.994292E-04
  a    73   1.515999E-05  -3.946248E-06   1.726657E-05   1.462485E-04   1.191378E-04   1.005265E-04  -1.796629E-05   1.465995E-06
  a    74   8.403370E-06  -1.899944E-05  -4.856111E-05   8.728428E-06   1.599700E-05  -8.897294E-06  -5.972709E-07   7.575796E-06
  a    75  -2.701655E-05   3.002788E-05  -3.183646E-05  -1.110232E-05   3.112606E-06  -9.829247E-06  -4.339680E-05  -2.562716E-05
  a    76  -6.401800E-05   2.215287E-05   7.634126E-07  -5.328522E-06  -2.600603E-07  -3.712687E-06  -3.769375E-05  -9.300754E-05
  a    77  -1.025315E-05  -4.394293E-06  -1.024341E-05   1.363124E-06  -5.863170E-08   3.220867E-07  -5.681909E-06  -6.171730E-06
  a    78   3.147922E-05   2.613239E-05   2.265882E-05  -1.243116E-05  -6.908927E-07   8.679254E-06   5.240418E-06   3.954720E-06
  a    79  -1.192229E-05  -3.130195E-05  -4.360714E-06  -6.373399E-06  -1.133275E-05  -9.627529E-06   1.122710E-06   2.314126E-05
  a    80  -3.196680E-05   1.942273E-06   3.028678E-05   7.588762E-06   4.039439E-06   9.131063E-06  -1.524864E-05  -2.834509E-05
  a    81  -2.261272E-06  -4.226024E-06  -6.677799E-07  -5.828069E-06   9.803775E-07  -6.675500E-06   3.788254E-06   6.892299E-07
  a    82  -3.566383E-05   3.630252E-05   1.992765E-05   5.882467E-06  -3.811554E-06   1.789560E-06  -7.080018E-06  -1.399880E-05
  a    83  -7.370304E-06  -1.536113E-05  -7.678877E-06  -1.077112E-06   2.351144E-06   2.035104E-06   9.823878E-06   3.882510E-05
  a    84  -1.109032E-05   1.509039E-05  -2.058624E-06  -2.017180E-06   5.854183E-06   3.871662E-06  -1.526958E-05  -3.404838E-06
  a    85   4.934802E-06  -2.306900E-06   1.464445E-06   3.937031E-06  -2.362730E-06   1.242983E-06   6.258194E-06  -1.413130E-05
  a    86  -1.728089E-05   2.260103E-05  -1.156862E-05  -1.436551E-06  -7.180411E-07  -4.167776E-06  -1.863850E-05  -3.169354E-05

               a    73        a    74        a    75        a    76        a    77        a    78        a    79        a    80
  a    13  -5.923292E-05   4.221529E-04   8.614192E-04  -6.664715E-04   8.552844E-04   1.339438E-04   1.393881E-04  -6.305327E-05
  a    14  -5.551286E-06  -1.938867E-04   6.210274E-04   1.359563E-04   4.096151E-04   2.213150E-04  -6.110361E-04   3.272168E-04
  a    15   7.155532E-06  -1.669278E-04  -1.169496E-04   2.861481E-04  -7.408614E-04   3.171374E-04  -2.507844E-04  -1.331269E-04
  a    16   1.390523E-04   2.318749E-04  -4.212841E-04  -3.097988E-04  -1.692637E-03   3.410895E-04  -3.652620E-04  -9.041277E-04
  a    17  -6.328879E-05   2.351193E-04  -2.441307E-04  -3.735109E-04   1.080750E-03  -2.851561E-04  -6.040776E-04  -7.869364E-04
  a    18   7.817153E-05   1.566600E-04   9.042777E-04   2.712106E-04   3.821227E-05   1.294238E-03  -1.048453E-03  -1.539111E-04
  a    19   3.570249E-05  -3.163753E-04  -3.339686E-04   8.764479E-05  -1.135783E-03   2.161397E-04   1.161504E-04  -7.802561E-04
  a    20   7.233807E-05   5.298942E-04   7.291507E-04   1.007862E-04  -3.431827E-04   2.334616E-05   1.391329E-04  -2.216835E-04
  a    21  -2.188228E-06  -6.164290E-04  -2.742494E-04   2.308762E-04  -2.669816E-04   8.172853E-04  -2.715584E-04  -1.486015E-04
  a    22   6.657025E-06  -1.410733E-04  -7.464884E-04   2.000941E-04  -7.717208E-04  -1.401244E-04  -1.326683E-04  -6.255070E-04
  a    23  -1.697035E-05   2.749947E-04   1.991759E-05  -3.081865E-05  -3.491348E-05   1.649498E-04   2.110600E-04   1.432447E-04
  a    24   7.565402E-05  -4.857515E-05   1.212389E-04  -2.504174E-04  -1.464002E-03   2.279771E-04  -1.885714E-04  -3.281026E-06
  a    25  -2.114688E-05   5.341329E-04   4.217747E-04  -8.929559E-04   2.276496E-04  -1.015552E-03   8.019236E-04   4.100269E-04
  a    26   2.827037E-04   2.223903E-04  -1.609043E-04   3.888624E-04  -1.348435E-03  -9.521633E-05   6.620424E-04   8.606455E-04
  a    27  -8.943999E-04   9.503717E-05  -6.715348E-06   1.379279E-04  -2.590849E-04  -9.044961E-05   1.437479E-04   2.749808E-04
  a    28   1.303254E-05  -5.511247E-04   1.270442E-04  -1.166183E-03   2.281029E-03   3.905663E-04  -3.846733E-05  -2.097022E-04
  a    29  -9.902573E-04  -1.558686E-04   3.775994E-05   4.359179E-05   2.669331E-05   6.463982E-05   1.102768E-04  -7.003430E-05
  a    30   1.700316E-03   3.403878E-04  -6.766891E-05   1.260081E-05   3.018770E-05  -1.409161E-04  -7.367666E-05   5.815160E-05
  a    31  -1.658148E-03  -7.832690E-05   1.408701E-05  -3.616206E-06  -7.949841E-06   6.590199E-05   8.400835E-05  -3.132439E-05
  a    32   1.011665E-03   1.670980E-04  -2.067675E-05  -1.391443E-05   9.784239E-06  -7.893741E-05  -9.417456E-05   4.741090E-05
  a    33  -1.060926E-04  -2.389951E-04  -2.963691E-04  -6.860419E-05   1.304910E-04  -1.143410E-04   3.458615E-04   5.330155E-04
  a    34   1.084758E-02   7.603266E-04  -3.817125E-05  -1.077167E-04   6.693506E-05  -4.616433E-04  -4.392447E-04   2.395205E-04
  a    35  -3.230114E-03  -2.322390E-04   2.832113E-05   4.209943E-05  -1.170811E-05   1.336294E-04   1.656705E-04  -7.763343E-05
  a    36   3.067951E-04   8.419535E-06   6.861226E-06   6.356920E-06   3.779582E-06   5.910486E-06   1.602407E-05  -1.302667E-05
  a    37  -2.820020E-07  -1.079728E-04  -2.109852E-05  -8.581634E-05   2.794812E-04   1.217179E-05  -9.215900E-06   2.838944E-05
  a    38   3.514647E-05  -8.787975E-05  -1.125228E-04   7.713312E-05  -3.241919E-04   5.997897E-05   1.501242E-05   6.997703E-05
  a    39  -4.791716E-05  -3.442601E-05  -1.143632E-04  -6.218908E-06  -8.253649E-05  -2.417276E-05   6.766229E-05   3.150594E-05
  a    40  -1.399106E-05   2.108984E-04   1.125594E-04  -3.130379E-05   1.825907E-04  -1.246143E-04  -4.196510E-05  -1.820880E-04
  a    41  -4.945940E-05   3.539306E-05   4.050164E-05  -7.467985E-05   2.541172E-05   3.855259E-05   4.511847E-05   3.857273E-05
  a    42  -7.294522E-05  -4.407676E-05  -1.867995E-05   3.517786E-05   9.192303E-06   5.976649E-06   2.281036E-05  -1.744971E-05
  a    43  -3.963487E-05  -8.025157E-05  -4.093033E-05   4.752443E-05  -8.330955E-05   2.710832E-05  -5.685550E-05  -1.622374E-05
  a    44   1.962326E-05  -2.593079E-04  -2.576516E-04  -3.009322E-05  -1.807544E-04   2.781664E-05   1.065054E-04   1.611223E-04
  a    45  -1.608073E-05   1.850090E-04  -4.982743E-05  -1.338203E-04   1.864894E-04  -2.287659E-04   1.136750E-04  -5.293129E-05
  a    46  -1.016845E-05   4.697203E-04   3.268323E-04   7.924983E-05   1.294857E-04  -2.669581E-04   5.151070E-05  -1.468575E-04
  a    47   3.461852E-05  -9.654095E-05  -9.845602E-05  -4.040172E-05   8.865978E-05  -1.370880E-05   3.481091E-05  -2.188147E-06
  a    48   4.024518E-05  -9.180244E-05  -7.774109E-05  -2.783051E-05   1.387561E-05   6.166134E-05   1.884638E-05   6.101561E-05
  a    49   1.774633E-05   8.101329E-05   4.609094E-05  -5.584415E-05  -7.470458E-05   4.220511E-05   9.450909E-06  -4.382614E-06
  a    50  -2.157649E-05   1.288382E-04   6.789859E-05   4.853270E-05   5.939160E-05  -7.225778E-05   3.958666E-06  -9.054132E-05
  a    51  -2.874019E-04  -3.072910E-05  -2.045997E-06  -4.253004E-07  -5.570437E-06   1.653699E-05   1.459926E-05  -1.741968E-06
  a    52  -2.969188E-06  -1.046677E-04  -3.830335E-05  -1.157012E-04   1.343024E-04   7.151750E-05  -5.921664E-05  -3.075961E-05
  a    53  -3.163570E-06  -1.275537E-04  -8.081155E-06   8.760233E-05  -9.732921E-05   7.461204E-05  -8.093202E-05   1.198906E-05
  a    54   2.381731E-04  -1.476312E-06  -1.830381E-06   1.972621E-06  -3.091152E-06  -3.132733E-07   3.542724E-07   2.160946E-06
  a    55   1.102969E-05   1.220483E-04   1.696641E-05  -1.968225E-05   4.457107E-05  -8.647388E-05   1.547713E-05  -4.379224E-05
  a    56   3.194541E-06   5.191171E-05   2.653665E-05  -3.225793E-05  -1.048346E-05  -9.069592E-06   1.793415E-05  -4.296255E-06
  a    57   2.049344E-06  -2.207636E-04  -1.149560E-04  -1.132915E-05  -9.089243E-05   1.527783E-04  -5.696784E-05   5.220497E-05
  a    58  -1.623866E-04  -1.699152E-05  -5.495914E-06  -2.849964E-06  -2.573773E-06   3.128278E-06   1.375772E-05  -3.574837E-06
  a    59  -1.245016E-04  -1.443738E-05  -1.246787E-05  -6.544243E-06  -6.930037E-06   7.212002E-06   7.085729E-06   2.614603E-06
  a    60   2.168210E-05  -2.106869E-04  -1.379407E-04  -4.581934E-05  -6.526947E-05   6.654301E-05   3.757192E-06   9.653851E-05
  a    61  -2.868421E-05  -2.007584E-05   5.984823E-05   4.089101E-05  -1.953973E-05   6.115281E-05  -6.243690E-05  -2.494897E-06
  a    62   2.088806E-04   1.267546E-05   1.286230E-05   9.820514E-06  -1.745339E-06  -5.998844E-06  -1.565318E-05   7.087213E-07
  a    63   4.270077E-05  -6.255495E-05  -8.404446E-05  -7.594925E-05  -1.354498E-05   6.743749E-05   1.009889E-05   2.516523E-05
  a    64   5.353145E-05   4.437614E-05   3.233491E-05   1.991590E-05   1.638522E-05  -4.505928E-05   2.458956E-06  -1.237459E-05
  a    65   1.515999E-05   8.403370E-06  -2.701655E-05  -6.401800E-05  -1.025315E-05   3.147922E-05  -1.192229E-05  -3.196680E-05
  a    66  -3.946248E-06  -1.899944E-05   3.002788E-05   2.215287E-05  -4.394293E-06   2.613239E-05  -3.130195E-05   1.942273E-06
  a    67   1.726657E-05  -4.856111E-05  -3.183646E-05   7.634126E-07  -1.024341E-05   2.265882E-05  -4.360714E-06   3.028678E-05
  a    68   1.462485E-04   8.728428E-06  -1.110232E-05  -5.328522E-06   1.363124E-06  -1.243116E-05  -6.373399E-06   7.588762E-06
  a    69   1.191378E-04   1.599700E-05   3.112606E-06  -2.600603E-07  -5.863170E-08  -6.908927E-07  -1.133275E-05   4.039439E-06
  a    70   1.005265E-04  -8.897294E-06  -9.829247E-06  -3.712687E-06   3.220867E-07   8.679254E-06  -9.627529E-06   9.131063E-06
  a    71  -1.796629E-05  -5.972709E-07  -4.339680E-05  -3.769375E-05  -5.681909E-06   5.240418E-06   1.122710E-06  -1.524864E-05
  a    72   1.465995E-06   7.575796E-06  -2.562716E-05  -9.300754E-05  -6.171730E-06   3.954720E-06   2.314126E-05  -2.834509E-05
  a    73   3.975441E-04   1.963252E-05  -6.361675E-06  -8.015727E-06  -6.077583E-07  -9.885204E-06  -1.200930E-05   6.611563E-06
  a    74   1.963252E-05   1.574432E-04   8.847564E-05   1.106548E-05   2.010640E-05  -6.471624E-05   1.529135E-05  -5.092305E-05
  a    75  -6.361675E-06   8.847564E-05   1.566473E-04   1.195409E-05   3.113412E-05   1.162303E-05  -2.338544E-05  -1.553596E-05
  a    76  -8.015727E-06   1.106548E-05   1.195409E-05   1.205979E-04  -7.412437E-05  -2.285560E-05  -1.647345E-05   4.198391E-06
  a    77  -6.077583E-07   2.010640E-05   3.113412E-05  -7.412437E-05   1.794026E-04  -2.780571E-05   1.474918E-05  -1.699609E-05
  a    78  -9.885204E-06  -6.471624E-05   1.162303E-05  -2.285560E-05  -2.780571E-05   9.960343E-05  -3.793053E-05   2.377458E-05
  a    79  -1.200930E-05   1.529135E-05  -2.338544E-05  -1.647345E-05   1.474918E-05  -3.793053E-05   6.496800E-05   1.864202E-05
  a    80   6.611563E-06  -5.092305E-05  -1.553596E-05   4.198391E-06  -1.699609E-05   2.377458E-05   1.864202E-05   6.918562E-05
  a    81  -1.891706E-06   4.693800E-05   2.924930E-05  -1.495821E-05   4.325386E-05  -2.133838E-05   4.795727E-06  -1.226073E-05
  a    82  -1.800084E-06  -2.577218E-05  -6.361283E-06   2.882592E-05  -2.731482E-06  -2.849357E-06  -8.661148E-06   2.046508E-06
  a    83   3.124891E-06   1.796162E-06  -1.816766E-05  -9.831824E-06   1.101391E-06  -9.330299E-06   1.717791E-05  -5.018248E-06
  a    84   2.304782E-06  -3.983065E-06   1.130950E-05   5.431715E-06  -7.278658E-06   1.059603E-05   7.782656E-06   4.297726E-06
  a    85   2.111977E-06   3.156034E-06  -5.971390E-06  -3.791694E-06  -1.860551E-06  -9.385031E-07   1.964242E-07   6.859706E-06
  a    86  -2.839290E-06   9.915924E-06   1.961094E-05   7.267815E-06  -5.666721E-06   2.504035E-06   1.159206E-05   1.120532E-05

               a    81        a    82        a    83        a    84        a    85        a    86
  a    13   1.296347E-04  -6.396722E-05  -2.409212E-04  -2.508625E-04   5.086353E-04   4.761876E-04
  a    14  -2.297260E-04  -2.710130E-05  -1.435514E-04  -2.482210E-04   3.232608E-04  -6.856068E-05
  a    15  -7.156364E-04  -1.300084E-04  -1.468770E-04   2.588444E-05   1.010473E-04  -1.360868E-04
  a    16  -6.125811E-04  -5.130957E-04  -5.386039E-04   1.458992E-04  -1.719318E-05   7.625356E-05
  a    17   3.824858E-04  -1.358968E-04  -1.284882E-03   1.803124E-04  -2.967286E-04  -2.497107E-04
  a    18   1.323776E-04   2.662078E-04   5.783642E-05  -2.628383E-04   7.931326E-05  -4.909578E-05
  a    19  -7.957758E-04  -2.984921E-05   1.395376E-03   1.858123E-04   5.594322E-05  -1.998727E-04
  a    20   2.847233E-04   4.291003E-04   3.988293E-04   4.335628E-05  -7.078701E-05  -1.386986E-04
  a    21   5.472377E-04  -1.322540E-03   1.137686E-04   6.551154E-05  -1.699947E-04  -2.826156E-04
  a    22  -8.069076E-04  -4.092904E-04   3.232014E-04  -2.839439E-05  -6.305919E-05  -6.210510E-05
  a    23   4.840002E-04  -9.344653E-04   3.680681E-04   2.280247E-04  -7.450386E-05  -1.502888E-04
  a    24   2.374164E-04   2.340681E-05   6.908208E-04   3.768714E-05   1.130411E-04   1.359291E-04
  a    25   4.192531E-04   3.015078E-04  -2.627150E-05  -7.629556E-05  -1.833108E-04   1.328398E-04
  a    26  -7.559229E-04  -4.091348E-04  -9.358322E-04  -5.886477E-05  -3.399476E-04   1.409618E-04
  a    27  -1.070524E-04   1.167043E-04  -1.579959E-04   6.066103E-05  -8.709847E-06   1.703464E-05
  a    28  -4.006062E-04  -1.453871E-04  -3.822876E-04   5.888028E-05  -1.883181E-04   2.052780E-04
  a    29  -1.323025E-05  -3.015005E-06  -3.000401E-05   1.926907E-05  -1.643602E-05  -1.282396E-05
  a    30  -6.221601E-06   5.320644E-06  -4.781160E-05   5.745174E-05   3.958518E-05   1.262671E-05
  a    31   7.085269E-06   1.219129E-05  -2.962723E-05   2.765074E-06   3.464783E-06   1.576024E-05
  a    32   8.173688E-06  -7.111061E-07   1.323180E-05   4.277402E-05   5.318844E-05  -9.580626E-06
  a    33   3.946864E-05  -5.649301E-04   9.310631E-05  -3.585759E-04   4.822638E-04   8.297994E-05
  a    34   1.039278E-05  -3.435738E-05   6.749566E-05   7.498932E-05   7.559601E-05   1.348327E-05
  a    35  -3.975838E-06   1.432637E-05  -4.278149E-05  -2.931545E-05  -2.806476E-05  -4.205958E-06
  a    36   1.151546E-06  -7.396635E-06   1.506965E-06   1.321669E-05   9.173082E-06  -6.363180E-06
  a    37  -3.065318E-05   2.043078E-06  -4.030675E-06  -2.917781E-05   7.110234E-05   4.080140E-05
  a    38  -1.272732E-04  -2.363453E-05   2.091189E-05   3.530245E-05   1.354478E-06   2.524271E-05
  a    39  -6.714316E-05   1.997781E-05   4.701033E-05  -1.680957E-05   2.351542E-05  -3.183908E-06
  a    40   1.466604E-04   7.027183E-05  -1.847119E-05   1.466780E-05  -5.047415E-05  -1.252303E-06
  a    41   1.408880E-05  -8.758051E-05  -6.985809E-06  -2.080471E-05   2.048429E-05  -5.016425E-06
  a    42  -2.597843E-05   7.669494E-05   6.741556E-06   3.084434E-05  -2.524006E-05   2.086603E-05
  a    43  -2.489795E-05   6.852148E-05  -4.330978E-05   4.678845E-05  -6.709588E-05   2.417977E-05
  a    44  -1.614492E-04   1.004800E-04   4.659102E-05   6.474313E-05  -2.625935E-05   6.630898E-05
  a    45   1.140712E-04  -1.028090E-04   7.447459E-05  -1.096471E-04   5.761945E-05  -4.310405E-05
  a    46   1.401753E-04  -1.566865E-05   6.093026E-06  -4.135250E-05   3.787205E-05   4.404287E-05
  a    47  -1.795255E-05   9.248894E-05   5.346553E-05   2.101924E-05  -8.467699E-06  -3.114713E-05
  a    48   1.243904E-05   1.345433E-05  -2.581268E-06   3.724951E-05   7.852466E-06   6.408402E-06
  a    49   1.982991E-05  -6.292413E-05  -3.283554E-05   3.765669E-05  -1.984450E-05   3.307452E-05
  a    50   5.307695E-05   4.248779E-05   1.803234E-05   2.944343E-05  -2.151328E-05   2.615194E-05
  a    51  -3.868846E-06  -1.453463E-06  -3.777119E-06  -3.374953E-06  -4.182424E-07  -1.825819E-06
  a    52   3.346730E-05   4.079612E-05  -3.478729E-06   2.895266E-06  -6.728574E-06  -2.042062E-05
  a    53  -7.947112E-05   1.008207E-04  -4.433750E-05   2.749600E-05  -4.207632E-05  -2.747161E-05
  a    54  -6.062690E-06   1.592814E-06  -1.699673E-06   2.335782E-06   9.658966E-07   4.233627E-07
  a    55   7.024660E-05  -4.753927E-05   8.165474E-06  -2.396845E-05   4.703911E-06   3.323481E-06
  a    56  -3.325048E-05  -5.190269E-05   3.552773E-06  -1.776372E-07   3.477429E-06   1.115126E-05
  a    57  -7.345008E-05   3.432963E-05  -2.851364E-05   4.600738E-05  -2.755458E-05   2.753407E-06
  a    58  -3.128277E-06   3.720578E-06  -5.536038E-07  -3.024885E-06  -3.256123E-07   9.821115E-07
  a    59  -7.848252E-06   2.838805E-06  -6.167404E-07  -2.040915E-07   5.319663E-07   1.334535E-06
  a    60  -1.015992E-04   8.378821E-05   2.242489E-05  -6.264604E-06   1.606084E-05  -1.645345E-06
  a    61  -8.096523E-06  -2.055419E-05  -1.492523E-05  -1.042962E-05  -2.726754E-06  -1.828288E-05
  a    62  -1.440178E-07   1.917100E-06  -2.311906E-06  -1.259598E-06  -4.893137E-07  -5.997709E-07
  a    63  -2.261398E-05  -2.961066E-05  -1.246160E-05   1.227316E-05  -3.973714E-07  -8.131786E-06
  a    64   1.738651E-05   8.924531E-06   7.080087E-06  -4.077397E-06  -1.784530E-06   7.648095E-06
  a    65  -2.261272E-06  -3.566383E-05  -7.370304E-06  -1.109032E-05   4.934802E-06  -1.728089E-05
  a    66  -4.226024E-06   3.630252E-05  -1.536113E-05   1.509039E-05  -2.306900E-06   2.260103E-05
  a    67  -6.677799E-07   1.992765E-05  -7.678877E-06  -2.058624E-06   1.464445E-06  -1.156862E-05
  a    68  -5.828069E-06   5.882467E-06  -1.077112E-06  -2.017180E-06   3.937031E-06  -1.436551E-06
  a    69   9.803775E-07  -3.811554E-06   2.351144E-06   5.854183E-06  -2.362730E-06  -7.180411E-07
  a    70  -6.675500E-06   1.789560E-06   2.035104E-06   3.871662E-06   1.242983E-06  -4.167776E-06
  a    71   3.788254E-06  -7.080018E-06   9.823878E-06  -1.526958E-05   6.258194E-06  -1.863850E-05
  a    72   6.892299E-07  -1.399880E-05   3.882510E-05  -3.404838E-06  -1.413130E-05  -3.169354E-05
  a    73  -1.891706E-06  -1.800084E-06   3.124891E-06   2.304782E-06   2.111977E-06  -2.839290E-06
  a    74   4.693800E-05  -2.577218E-05   1.796162E-06  -3.983065E-06   3.156034E-06   9.915924E-06
  a    75   2.924930E-05  -6.361283E-06  -1.816766E-05   1.130950E-05  -5.971390E-06   1.961094E-05
  a    76  -1.495821E-05   2.882592E-05  -9.831824E-06   5.431715E-06  -3.791694E-06   7.267815E-06
  a    77   4.325386E-05  -2.731482E-06   1.101391E-06  -7.278658E-06  -1.860551E-06  -5.666721E-06
  a    78  -2.133838E-05  -2.849357E-06  -9.330299E-06   1.059603E-05  -9.385031E-07   2.504035E-06
  a    79   4.795727E-06  -8.661148E-06   1.717791E-05   7.782656E-06   1.964242E-07   1.159206E-05
  a    80  -1.226073E-05   2.046508E-06  -5.018248E-06   4.297726E-06   6.859706E-06   1.120532E-05
  a    81   6.246218E-05  -1.081409E-05   1.073920E-06  -3.526338E-06   1.674044E-07  -1.411369E-06
  a    82  -1.081409E-05   7.022007E-05  -4.597672E-06   4.796390E-06  -4.998949E-06   1.646273E-06
  a    83   1.073920E-06  -4.597672E-06   4.181733E-05  -4.095979E-06   7.165579E-06  -4.694109E-06
  a    84  -3.526338E-06   4.796390E-06  -4.095979E-06   3.886862E-05  -1.965737E-05   2.321161E-05
  a    85   1.674044E-07  -4.998949E-06   7.165579E-06  -1.965737E-05   3.013470E-05   2.660843E-06
  a    86  -1.411369E-06   1.646273E-06  -4.694109E-06   2.321161E-05   2.660843E-06   4.438056E-05

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99995832     1.99981688     1.99959789     1.99955216
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.99928525     1.99923878     1.99890356     1.99865266     1.99851937     1.99819625     1.99809734     1.99761644
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     1.99721911     1.99687239     1.99446622     1.99285103     1.99264462     1.99172364     1.96367906     1.91318141
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     1.35181408     0.62940254     0.11312599     0.03407422     0.00804607     0.00754908     0.00565765     0.00301497
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00240455     0.00184399     0.00172382     0.00130312     0.00109339     0.00103744     0.00087908     0.00079764
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00069525     0.00054626     0.00052975     0.00049810     0.00044030     0.00039553     0.00038361     0.00035347
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62       MO    63       MO    64
  occ(*)=     0.00030551     0.00026014     0.00022295     0.00020598     0.00018400     0.00016639     0.00015913     0.00014926
              MO    65       MO    66       MO    67       MO    68       MO    69       MO    70       MO    71       MO    72
  occ(*)=     0.00012439     0.00008697     0.00007461     0.00006774     0.00005590     0.00005148     0.00003920     0.00003073
              MO    73       MO    74       MO    75       MO    76       MO    77       MO    78       MO    79       MO    80
  occ(*)=     0.00002186     0.00002005     0.00001959     0.00001708     0.00001377     0.00001151     0.00000842     0.00000647
              MO    81       MO    82       MO    83       MO    84       MO    85       MO    86
  occ(*)=     0.00000490     0.00000327     0.00000257     0.00000236     0.00000105     0.00000043


 total number of electrons =   66.0000000000

 item #                     4 suffix=:.drt1.state4:
 read_civout: repnuc=  -468.746689847934     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.62624040  -242.87955055
 residuum:     0.00037862
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -711.49543188  -242.74874203
 residuum:     0.00061881
 deltae:     0.00000001
================================================================================
  Reading record                      3  of civout
 INFO:ref#  3vector#  3 method:  0 last record  0max overlap with ref# 93% root-following 0
 MR-CISD energy:  -711.47740269  -242.73071284
 residuum:     0.00041394
 deltae:     0.00000000
================================================================================
  Reading record                      4  of civout
 INFO:ref#  4vector#  4 method:  0 last record  1max overlap with ref# 95% root-following 0
 MR-CISD energy:  -711.46281126  -242.71612141
 residuum:     0.00064015
 deltae:     0.00000134
 apxde:     0.00000040

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.12115223
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.02557439
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.03733045
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.08826447

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24
 ref:   1    -0.09929586    -0.10555031    -0.01128983    -0.03609658     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00871655     0.03940820     0.04506172    -0.01308525     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.04592776    -0.09668793    -0.00209956    -0.01628650     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   4     0.01825779     0.02992709     0.04326769     0.05354279     0.00000000     0.00000000     0.00000000     0.00000000

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95022495    -0.00067102    -0.06637925    -0.00132863    -0.13793204    -0.02185329     0.02970921     0.00966957
 ref:   2     0.00033421     0.95676063    -0.00320228     0.10972166    -0.00452761     0.00776035    -0.01050188    -0.04589165
 ref:   3     0.03774447     0.00317281     0.93239583    -0.00534540    -0.16842013     0.08131572     0.05087189     0.00633052
 ref:   4    -0.00083143     0.10316029    -0.00485936    -0.94736625    -0.00161047     0.00523629    -0.02921550    -0.02842204

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05681095     0.00727926    -0.05502952    -0.17150684    -0.14609415    -0.01440923    -0.01164517     0.00000000
 ref:   2     0.00616368     0.04194297     0.05621310    -0.07712562     0.02572860     0.00476731     0.02737127     0.00000000
 ref:   3     0.07132209    -0.01010463    -0.03766925    -0.04360266     0.01950672     0.00011348     0.01891255     0.00000000
 ref:   4     0.01702810    -0.04980261    -0.00220273    -0.00177247     0.00273102    -0.05236587     0.09277839     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    4
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y=   34801  D0X=       0  D0W=       0
  DDZI=   17300 DDYI=   39515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1785 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    4
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y= 1246697  D0X=       0  D0W=       0
  DDZI=  203100 DDYI=  457450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    42.000000
   42  correlated and    24  frozen core electrons
   7396 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000    
  a     6    0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000    
  a     7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000    
  a     8    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000    

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     9    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    10    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    11    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a    12    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a    13    0.00000        0.00000        0.00000        0.00000        1.99904      -1.014881E-07   4.869404E-04   1.502802E-04
  a    14    0.00000        0.00000        0.00000        0.00000      -1.014881E-07    1.99941      -4.999710E-04   1.661900E-04
  a    15    0.00000        0.00000        0.00000        0.00000       4.869404E-04  -4.999710E-04    1.99887      -5.454542E-04
  a    16    0.00000        0.00000        0.00000        0.00000       1.502802E-04   1.661900E-04  -5.454542E-04    1.99826    
  a    17    0.00000        0.00000        0.00000        0.00000      -2.480825E-04  -2.752357E-04   4.854323E-05   6.669145E-04
  a    18    0.00000        0.00000        0.00000        0.00000      -4.098232E-04  -1.468978E-04   2.513586E-04  -4.157595E-04
  a    19    0.00000        0.00000        0.00000        0.00000       4.502319E-04  -3.277137E-05  -7.280685E-04  -2.851978E-04
  a    20    0.00000        0.00000        0.00000        0.00000      -1.675654E-05  -3.346176E-04  -5.205938E-04   6.937116E-05
  a    21    0.00000        0.00000        0.00000        0.00000       2.186526E-04  -9.874937E-05  -4.812533E-04  -1.005193E-04
  a    22    0.00000        0.00000        0.00000        0.00000       3.291467E-04   2.644340E-04  -9.310736E-05  -3.814650E-04
  a    23    0.00000        0.00000        0.00000        0.00000       1.209651E-05  -1.118366E-06  -2.117320E-04  -8.963290E-05
  a    24    0.00000        0.00000        0.00000        0.00000       1.136414E-04   1.664854E-05  -2.863847E-04  -2.595634E-04
  a    25    0.00000        0.00000        0.00000        0.00000      -2.138190E-04   5.887812E-06   8.933581E-05   3.118911E-04
  a    26    0.00000        0.00000        0.00000        0.00000       5.612387E-05   3.353801E-04   4.046267E-04  -7.002304E-04
  a    27    0.00000        0.00000        0.00000        0.00000       2.308230E-05   7.442198E-06   2.653492E-06  -9.017602E-05
  a    28    0.00000        0.00000        0.00000        0.00000      -4.069728E-04   1.301150E-04   6.728831E-04   5.412384E-04
  a    29    0.00000        0.00000        0.00000        0.00000      -5.415869E-06  -7.493611E-06   1.431021E-05  -5.491142E-06
  a    30    0.00000        0.00000        0.00000        0.00000       4.244202E-06   7.617581E-06  -1.225377E-05   9.874986E-06
  a    31    0.00000        0.00000        0.00000        0.00000       2.015791E-06   1.416529E-05   4.587157E-06   2.184178E-05
  a    32    0.00000        0.00000        0.00000        0.00000       1.012463E-05   4.028079E-06  -3.220864E-07   6.784026E-06
  a    33    0.00000        0.00000        0.00000        0.00000       9.126617E-05   5.139232E-04  -1.947011E-04   3.370440E-04
  a    34    0.00000        0.00000        0.00000        0.00000       2.844100E-05  -1.680994E-04   4.460345E-04  -2.907771E-04
  a    35    0.00000        0.00000        0.00000        0.00000      -3.714839E-05  -6.650124E-05   2.004471E-04  -4.377948E-05
  a    36    0.00000        0.00000        0.00000        0.00000       3.768300E-05  -2.218376E-04   2.751261E-04  -9.980123E-05
  a    37    0.00000        0.00000        0.00000        0.00000       9.580276E-04   9.733194E-04   4.461457E-05  -2.578552E-03
  a    38    0.00000        0.00000        0.00000        0.00000      -6.329177E-04   2.143075E-03   3.833006E-03  -2.694692E-03
  a    39    0.00000        0.00000        0.00000        0.00000      -3.972053E-04   1.254376E-03  -2.056515E-03  -3.787643E-04
  a    40    0.00000        0.00000        0.00000        0.00000      -3.176260E-03  -1.635248E-03  -3.206925E-03  -1.660908E-03
  a    41    0.00000        0.00000        0.00000        0.00000       2.447980E-03   2.710720E-03   6.646461E-04   1.895098E-03
  a    42    0.00000        0.00000        0.00000        0.00000      -2.135829E-03  -1.662289E-04   1.666421E-03   7.059731E-04
  a    43    0.00000        0.00000        0.00000        0.00000      -7.720548E-03   2.212534E-03   3.033670E-03  -6.475252E-03
  a    44    0.00000        0.00000        0.00000        0.00000      -3.739535E-03   4.753721E-03   4.706622E-03  -3.866298E-03
  a    45    0.00000        0.00000        0.00000        0.00000       8.735051E-04  -2.204445E-03  -6.595468E-03  -3.629849E-03
  a    46    0.00000        0.00000        0.00000        0.00000      -3.698438E-04   3.536899E-03   6.255090E-04  -1.497504E-03
  a    47    0.00000        0.00000        0.00000        0.00000      -5.139630E-04  -1.979654E-03   4.908883E-05   3.750090E-03
  a    48    0.00000        0.00000        0.00000        0.00000      -2.480768E-04  -2.489849E-03  -1.328660E-03  -1.700843E-03
  a    49    0.00000        0.00000        0.00000        0.00000      -1.720179E-03   3.250640E-03   3.012328E-03  -6.801686E-04
  a    50    0.00000        0.00000        0.00000        0.00000      -1.703365E-03  -1.811658E-05   6.567868E-04   1.086270E-03
  a    51    0.00000        0.00000        0.00000        0.00000       9.885173E-05  -9.775151E-06  -5.499113E-05  -5.338782E-05
  a    52    0.00000        0.00000        0.00000        0.00000       6.540097E-04   9.493599E-04   4.716780E-04   1.994361E-04
  a    53    0.00000        0.00000        0.00000        0.00000       4.930525E-04  -4.102174E-04   1.006748E-03  -1.022447E-03
  a    54    0.00000        0.00000        0.00000        0.00000       1.037537E-04   8.658041E-05   2.088369E-04   2.503148E-04
  a    55    0.00000        0.00000        0.00000        0.00000      -3.079821E-03  -1.516644E-03  -2.277508E-03  -4.245059E-03
  a    56    0.00000        0.00000        0.00000        0.00000      -1.038966E-03   3.029416E-03   4.115301E-03  -3.965674E-04
  a    57    0.00000        0.00000        0.00000        0.00000      -2.834986E-03  -1.902748E-04   2.761796E-03  -5.931071E-04
  a    58    0.00000        0.00000        0.00000        0.00000       3.149943E-05   7.349148E-05   1.114246E-04   8.182280E-05
  a    59    0.00000        0.00000        0.00000        0.00000      -1.763601E-04   1.785096E-04   2.675430E-04   4.840632E-05
  a    60    0.00000        0.00000        0.00000        0.00000      -9.050369E-04   1.861450E-03   2.687488E-03   4.218247E-04
  a    61    0.00000        0.00000        0.00000        0.00000       1.658677E-03  -1.236428E-03  -2.000748E-03   1.219923E-03
  a    62    0.00000        0.00000        0.00000        0.00000       6.134909E-04  -4.518525E-04  -3.429379E-04   2.586535E-04
  a    63    0.00000        0.00000        0.00000        0.00000      -2.104978E-03   8.435406E-04   1.225902E-04   8.273399E-04
  a    64    0.00000        0.00000        0.00000        0.00000       1.879190E-04   3.250824E-04   1.054995E-04  -5.356955E-04
  a    65    0.00000        0.00000        0.00000        0.00000      -5.358806E-05   2.935185E-05  -2.907516E-04  -1.144999E-03
  a    66    0.00000        0.00000        0.00000        0.00000      -3.251767E-05   5.681963E-04   6.267115E-04   1.034845E-03
  a    67    0.00000        0.00000        0.00000        0.00000      -2.513264E-04  -3.463555E-04  -3.277293E-04   7.229565E-04
  a    68    0.00000        0.00000        0.00000        0.00000      -3.503789E-04   5.276812E-04  -1.271291E-05  -5.408544E-04
  a    69    0.00000        0.00000        0.00000        0.00000       3.035011E-04  -4.965441E-04   7.237146E-05   4.446421E-04
  a    70    0.00000        0.00000        0.00000        0.00000       1.991525E-04  -5.323885E-04   1.739006E-04   1.055156E-04
  a    71    0.00000        0.00000        0.00000        0.00000       1.169742E-03  -1.443534E-03   2.189429E-06  -4.447589E-04
  a    72    0.00000        0.00000        0.00000        0.00000       1.441647E-03   1.461439E-03   2.243420E-04   8.755070E-05
  a    73    0.00000        0.00000        0.00000        0.00000       1.309953E-04  -1.113943E-04   2.405392E-05   7.871175E-05
  a    74    0.00000        0.00000        0.00000        0.00000       4.293260E-04   1.890477E-03   1.348212E-04  -1.332846E-04
  a    75    0.00000        0.00000        0.00000        0.00000       1.806693E-03   1.477927E-03   1.863353E-03   1.891796E-03
  a    76    0.00000        0.00000        0.00000        0.00000      -2.504709E-04  -9.739589E-04   3.718494E-04   3.114628E-04
  a    77    0.00000        0.00000        0.00000        0.00000       9.876117E-04  -1.101996E-03  -1.121963E-03  -4.579304E-04
  a    78    0.00000        0.00000        0.00000        0.00000       1.047210E-03  -4.517450E-04   1.152819E-03   2.275732E-03
  a    79    0.00000        0.00000        0.00000        0.00000      -9.408669E-04   2.726436E-04   3.685862E-04  -1.281345E-04
  a    80    0.00000        0.00000        0.00000        0.00000      -1.364262E-05  -8.020910E-04   2.485106E-04   1.491437E-03
  a    81    0.00000        0.00000        0.00000        0.00000       4.036542E-05  -7.179731E-04  -8.215417E-04  -2.644592E-04
  a    82    0.00000        0.00000        0.00000        0.00000       8.420407E-04   3.305842E-04   6.768189E-05  -2.234324E-04
  a    83    0.00000        0.00000        0.00000        0.00000       5.521752E-04  -7.604943E-04   3.816917E-04   6.677929E-04
  a    84    0.00000        0.00000        0.00000        0.00000      -1.633408E-03  -2.517396E-04   1.101377E-03   3.195599E-05
  a    85    0.00000        0.00000        0.00000        0.00000       1.541086E-03   7.918547E-04   9.012011E-04  -2.029321E-04
  a    86    0.00000        0.00000        0.00000        0.00000      -5.783986E-04   1.905485E-03   1.953851E-03  -3.951567E-04

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a    13  -2.480825E-04  -4.098232E-04   4.502319E-04  -1.675654E-05   2.186526E-04   3.291467E-04   1.209651E-05   1.136414E-04
  a    14  -2.752357E-04  -1.468978E-04  -3.277137E-05  -3.346176E-04  -9.874937E-05   2.644340E-04  -1.118366E-06   1.664854E-05
  a    15   4.854323E-05   2.513586E-04  -7.280685E-04  -5.205938E-04  -4.812533E-04  -9.310736E-05  -2.117320E-04  -2.863847E-04
  a    16   6.669145E-04  -4.157595E-04  -2.851978E-04   6.937116E-05  -1.005193E-04  -3.814650E-04  -8.963290E-05  -2.595634E-04
  a    17    1.99774      -2.398206E-04  -2.516212E-04  -3.092893E-05  -1.962514E-04   2.887244E-04   1.058050E-05   8.143879E-04
  a    18  -2.398206E-04    1.99773       6.787849E-05   1.574557E-04   1.145787E-03   3.586106E-04   5.562179E-04   4.766528E-04
  a    19  -2.516212E-04   6.787849E-05    1.99721      -7.861742E-04  -5.304341E-04  -8.539723E-04  -5.338500E-04  -6.607793E-04
  a    20  -3.092893E-05   1.574557E-04  -7.861742E-04    1.99806      -7.546241E-05  -3.417704E-04  -5.854658E-05  -7.146267E-04
  a    21  -1.962514E-04   1.145787E-03  -5.304341E-04  -7.546241E-05    1.99694       1.171159E-04  -2.016241E-03  -4.703580E-04
  a    22   2.887244E-04   3.586106E-04  -8.539723E-04  -3.417704E-04   1.171159E-04    1.99863       3.207129E-04  -4.814951E-04
  a    23   1.058050E-05   5.562179E-04  -5.338500E-04  -5.854658E-05  -2.016241E-03   3.207129E-04    1.99787      -2.046950E-04
  a    24   8.143879E-04   4.766528E-04  -6.607793E-04  -7.146267E-04  -4.703580E-04  -4.814951E-04  -2.046950E-04    1.99792    
  a    25   8.722557E-05   4.435173E-04   5.858923E-05  -2.089658E-04   4.296506E-05   4.516478E-04  -7.073690E-04  -3.193693E-04
  a    26   1.089540E-03  -3.022937E-04   1.575204E-03   7.179918E-04   5.217719E-04  -2.981923E-04   6.857581E-05  -5.443781E-04
  a    27   1.345797E-04  -6.776704E-05   3.547490E-04   1.609887E-04   2.083799E-04   3.929081E-05   1.560254E-04   2.649083E-05
  a    28  -6.971747E-04  -6.928440E-04   3.538785E-04  -7.170512E-05   7.659829E-04  -5.914684E-05   3.319400E-04   5.501545E-04
  a    29  -5.536548E-06  -3.793598E-07   6.171040E-06   1.368356E-06   1.430395E-06   1.704046E-05   2.226208E-06   1.333880E-06
  a    30   4.246397E-07  -3.847431E-05   4.141435E-05   2.399595E-05  -2.155896E-06  -3.985755E-05   5.278435E-06   2.540126E-06
  a    31   1.976826E-05  -3.178703E-06   2.865898E-05  -1.447555E-07   6.418489E-05  -8.017018E-05  -4.153483E-05  -2.681376E-05
  a    32   6.121358E-06   2.057349E-06   3.376530E-05   1.138839E-05   5.723030E-05  -4.031658E-05  -1.370894E-05  -1.807799E-06
  a    33  -1.130398E-03   2.829744E-03  -3.838264E-03  -4.392412E-04  -8.184492E-03  -4.943942E-04  -9.453386E-03  -1.672083E-03
  a    34   8.241827E-05   1.246187E-04  -5.515755E-04  -3.279492E-05  -5.234284E-04   1.073748E-03   3.225968E-04  -4.824617E-05
  a    35   1.196690E-04   3.680518E-06  -1.282827E-04  -6.733662E-05  -1.116893E-04   4.908638E-04   3.174037E-04   1.137001E-04
  a    36   7.910726E-05  -1.979900E-05  -2.501055E-04  -5.141004E-05  -2.639161E-04   7.880976E-04   2.836317E-04  -7.923350E-05
  a    37  -4.031410E-03  -4.006768E-03  -3.788460E-03   6.262775E-03  -1.004723E-03   1.524609E-03  -8.204914E-04   4.560878E-03
  a    38   2.703011E-03   1.062832E-04   3.591776E-03   1.015187E-02  -2.589089E-03   6.567537E-03   1.275589E-03  -5.428564E-03
  a    39   1.198584E-03  -4.553374E-03   3.812811E-03   2.813148E-03  -1.775111E-03   1.934602E-03  -7.851715E-03   1.680284E-03
  a    40  -3.246684E-03  -6.605209E-04  -6.802149E-03   1.231225E-03   1.494770E-03   1.193202E-03   3.737857E-03  -7.245989E-03
  a    41  -2.152916E-04   7.725301E-03   2.902892E-03   2.262132E-03  -9.551942E-03  -3.064338E-03   1.725809E-03  -3.388028E-03
  a    42   3.929564E-03  -8.578698E-03  -3.634249E-03  -3.873083E-03  -8.733909E-04   2.407418E-03   1.949280E-03   2.211680E-03
  a    43   1.175486E-03  -3.410090E-03   3.077555E-03  -6.222129E-03   1.754538E-02  -8.038107E-03   1.400712E-02   8.690911E-04
  a    44   3.263196E-03  -4.057608E-03   3.962355E-03  -7.525346E-04   5.684863E-03   1.654516E-03  -2.024439E-03  -4.721510E-04
  a    45  -4.791104E-03   7.502942E-03  -4.873100E-03  -1.176640E-02  -2.094715E-03   5.184406E-04  -4.456681E-03   7.119281E-04
  a    46   1.098268E-03   6.260801E-03  -2.242313E-03   1.782693E-03  -9.503729E-03  -5.923683E-05  -7.056408E-03   6.281889E-03
  a    47  -5.030475E-03  -6.601114E-03  -2.698448E-03  -8.140890E-03   3.370356E-03   2.636117E-04   3.242026E-03  -4.100618E-03
  a    48   4.553207E-03  -3.688936E-03   4.021449E-03  -1.127833E-03   3.788134E-03   6.270246E-04   6.143483E-03   3.260848E-03
  a    49  -1.087076E-03   1.674763E-03  -4.522754E-03   4.529083E-04  -2.472868E-03  -6.914629E-03  -2.317556E-03   2.512145E-03
  a    50   3.461613E-03  -5.476211E-03  -2.572777E-03  -2.904116E-03  -2.101451E-03   1.952016E-03   1.292510E-03  -7.440729E-03
  a    51  -1.986320E-04   3.138510E-04   1.309542E-04   1.492069E-04   9.039329E-05  -1.425059E-04  -9.180526E-05   4.073346E-04
  a    52  -5.934094E-03   1.723731E-03  -1.148750E-03   6.853701E-04   3.371400E-03  -1.183875E-03   4.631164E-04   8.953323E-03
  a    53  -1.037687E-03  -1.738273E-03   6.253594E-03   2.260503E-03   8.166364E-03   1.839404E-03   5.787274E-03   3.044139E-03
  a    54  -6.344236E-06  -3.955311E-05   1.994518E-04   4.883034E-04  -3.178533E-04   1.269910E-04  -2.119468E-04   9.523216E-05
  a    55   1.277836E-03  -5.696944E-04  -3.645783E-03  -8.653575E-03   5.305922E-03  -2.005659E-03   3.474815E-03  -4.703853E-03
  a    56   9.184758E-04   2.041019E-03  -1.645432E-03   1.008472E-03  -3.938920E-03  -1.552610E-03  -5.271756E-03  -8.564355E-04
  a    57  -6.507482E-04  -1.395405E-03   3.321582E-03  -5.651167E-04   4.866725E-03  -1.475150E-03   3.400255E-04   3.219066E-03
  a    58  -9.308004E-06   6.659818E-05   1.200774E-04   2.033450E-04  -2.093224E-04  -2.159351E-04  -2.801760E-05  -1.334896E-05
  a    59   4.965315E-05   1.963513E-04   1.515870E-04   1.271065E-04   1.095190E-04  -1.541834E-04   8.090409E-07   7.348873E-05
  a    60  -1.853677E-03   4.924827E-03   4.030526E-03   3.134400E-03   2.941383E-03  -3.145660E-03   8.112414E-04   6.272355E-03
  a    61  -6.123949E-04   1.290644E-03   1.684338E-03   8.021995E-04   4.913398E-03   1.790576E-03   1.455263E-03   3.657858E-03
  a    62  -4.915589E-04   1.034020E-04   8.283327E-04   5.732246E-04   7.882109E-04  -3.107859E-04   3.162157E-04   9.538167E-04
  a    63   3.559028E-03  -3.583713E-03  -2.374589E-03  -2.542177E-03  -1.549180E-03   2.694159E-03  -1.397455E-03  -3.821895E-03
  a    64  -8.930677E-04   1.654867E-03  -4.890573E-04   6.306735E-04  -4.342261E-04   8.612157E-04   6.355286E-04  -1.280078E-04
  a    65   8.611905E-04  -4.352416E-04  -2.403676E-03  -1.906415E-03  -7.325906E-04  -1.110040E-03  -3.432361E-03   3.199392E-03
  a    66  -1.018979E-03  -6.887571E-04   7.657776E-04   3.832168E-03   1.010637E-04   1.982113E-03  -6.880103E-04   1.612816E-03
  a    67   2.384895E-05   1.690832E-03   1.946809E-03  -1.081182E-03   2.911858E-03  -8.818123E-04   1.821269E-03   3.272440E-03
  a    68  -3.879076E-04   5.419628E-04  -3.192385E-04   2.959268E-05  -4.169182E-04   2.567635E-04   1.274264E-03   2.383458E-04
  a    69   2.757262E-04  -6.297829E-04   3.633913E-04   5.020078E-05   3.588499E-04  -2.815297E-05  -8.823746E-04  -1.937125E-04
  a    70  -8.605466E-05  -5.813126E-04   4.587496E-04   2.636527E-04   1.815079E-04   3.298633E-04  -9.475281E-05  -1.650337E-04
  a    71  -3.397205E-04   4.634670E-04  -3.846374E-04   1.003330E-03  -1.227638E-03  -1.960106E-03   4.689229E-04  -3.227870E-04
  a    72   8.804840E-05   2.621991E-03  -8.048301E-04  -4.265799E-04  -3.259123E-03  -4.379296E-04  -3.494231E-03   5.857074E-04
  a    73   4.949574E-05  -3.358534E-05   9.049048E-05   6.828756E-05  -9.484578E-05  -8.970137E-05  -3.824303E-05   2.246108E-05
  a    74   1.141888E-03   1.923000E-03  -3.640220E-03  -7.584892E-04  -3.004047E-03  -2.056344E-03  -2.325852E-03  -1.073332E-03
  a    75   7.030269E-04   1.473599E-03  -7.619954E-04   5.233431E-04  -3.041029E-03   1.456481E-04  -1.107724E-03  -8.252576E-04
  a    76  -2.270956E-04  -2.696571E-03   6.546734E-04   6.902389E-04   2.016627E-04   1.034747E-03   1.833003E-03   1.116760E-04
  a    77   2.322719E-04   2.594578E-04  -3.481438E-04  -1.818161E-03  -2.146576E-03  -5.117800E-04  -1.627817E-03  -1.219241E-03
  a    78   4.590775E-04  -1.293569E-03   2.831145E-03   1.587869E-03  -4.062285E-04   3.207534E-04   1.439396E-03   2.025962E-04
  a    79   1.766678E-03  -8.848905E-04  -2.827893E-03  -1.403898E-03  -7.436510E-04  -3.189912E-04  -5.995662E-04   4.455029E-04
  a    80   5.829691E-04  -1.800990E-03   3.084488E-04   1.106793E-03   1.336145E-03  -8.909226E-05   5.663557E-04   4.548498E-04
  a    81   7.919988E-04   2.742908E-04  -2.510318E-03   1.841694E-03   1.272466E-04  -1.928786E-03   6.942045E-04   2.159753E-04
  a    82  -6.146707E-04  -1.070483E-03   7.859257E-04   1.047054E-03   7.394599E-04   7.161055E-04  -1.034893E-04   1.546852E-03
  a    83   6.521596E-04  -2.441686E-03   6.685106E-04   9.923382E-05  -1.481465E-03   5.712976E-04  -9.355781E-04  -7.276082E-04
  a    84   1.358288E-03  -6.185350E-04  -5.699128E-04  -3.837646E-05  -8.586809E-05   1.510379E-04   1.399878E-04  -9.418785E-04
  a    85  -1.040255E-03   4.961871E-05   5.041321E-04   7.919394E-04  -1.665021E-04   4.776774E-04   2.377288E-05   1.112374E-03
  a    86   5.569617E-04  -6.684108E-04  -4.677044E-04   1.135429E-03  -4.103704E-04  -6.081354E-04  -3.671671E-04  -4.189854E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a    13  -2.138190E-04   5.612387E-05   2.308230E-05  -4.069728E-04  -5.415869E-06   4.244202E-06   2.015791E-06   1.012463E-05
  a    14   5.887812E-06   3.353801E-04   7.442198E-06   1.301150E-04  -7.493611E-06   7.617581E-06   1.416529E-05   4.028079E-06
  a    15   8.933581E-05   4.046267E-04   2.653492E-06   6.728831E-04   1.431021E-05  -1.225377E-05   4.587157E-06  -3.220864E-07
  a    16   3.118911E-04  -7.002304E-04  -9.017602E-05   5.412384E-04  -5.491142E-06   9.874986E-06   2.184178E-05   6.784026E-06
  a    17   8.722557E-05   1.089540E-03   1.345797E-04  -6.971747E-04  -5.536548E-06   4.246397E-07   1.976826E-05   6.121358E-06
  a    18   4.435173E-04  -3.022937E-04  -6.776704E-05  -6.928440E-04  -3.793598E-07  -3.847431E-05  -3.178703E-06   2.057349E-06
  a    19   5.858923E-05   1.575204E-03   3.547490E-04   3.538785E-04   6.171040E-06   4.141435E-05   2.865898E-05   3.376530E-05
  a    20  -2.089658E-04   7.179918E-04   1.609887E-04  -7.170512E-05   1.368356E-06   2.399595E-05  -1.447555E-07   1.138839E-05
  a    21   4.296506E-05   5.217719E-04   2.083799E-04   7.659829E-04   1.430395E-06  -2.155896E-06   6.418489E-05   5.723030E-05
  a    22   4.516478E-04  -2.981923E-04   3.929081E-05  -5.914684E-05   1.704046E-05  -3.985755E-05  -8.017018E-05  -4.031658E-05
  a    23  -7.073690E-04   6.857581E-05   1.560254E-04   3.319400E-04   2.226208E-06   5.278435E-06  -4.153483E-05  -1.370894E-05
  a    24  -3.193693E-04  -5.443781E-04   2.649083E-05   5.501545E-04   1.333880E-06   2.540126E-06  -2.681376E-05  -1.807799E-06
  a    25    1.99799      -1.348353E-04   7.444323E-05  -5.532790E-04   9.165449E-06   2.765182E-05  -1.098317E-04  -4.722772E-05
  a    26  -1.348353E-04    1.99484      -6.615864E-04   1.259628E-04   9.418157E-06   3.088279E-05   1.117268E-04   4.323529E-05
  a    27   7.444323E-05  -6.615864E-04    1.99948       3.690269E-04  -2.091631E-04  -4.586270E-04  -7.832395E-04  -4.148160E-04
  a    28  -5.532790E-04   1.259628E-04   3.690269E-04    1.99545      -3.856273E-05  -4.141418E-05  -5.230282E-05  -2.662764E-05
  a    29   9.165449E-06   9.418157E-06  -2.091631E-04  -3.856273E-05    1.99230      -1.864889E-04  -2.083317E-03  -2.120636E-03
  a    30   2.765182E-05   3.088279E-05  -4.586270E-04  -4.141418E-05  -1.864889E-04    1.97198       3.118001E-03   1.168554E-04
  a    31  -1.098317E-04   1.117268E-04  -7.832395E-04  -5.230282E-05  -2.083317E-03   3.118001E-03    1.94061      -2.790077E-02
  a    32  -4.722772E-05   4.323529E-05  -4.148160E-04  -2.662764E-05  -2.120636E-03   1.168554E-04  -2.790077E-02    1.96847    
  a    33  -4.994424E-03   9.858245E-04   2.646344E-03  -7.120289E-03  -1.632542E-04   3.834317E-04  -7.263964E-04   2.535088E-03
  a    34   1.239305E-03  -8.528401E-04   5.462580E-03   4.664315E-04  -2.403519E-02  -8.186391E-02   6.260637E-02   3.957521E-02
  a    35   1.480190E-04  -6.389811E-04   5.191436E-03   5.857311E-04   1.617050E-02   3.518579E-02   2.176533E-02   8.962713E-02
  a    36   6.004100E-04  -3.629899E-04   3.591863E-04  -6.051442E-04  -5.584187E-03   2.495965E-02   2.100103E-02   2.290180E-02
  a    37   1.104503E-03   6.571518E-03   2.433774E-03  -1.310047E-02   2.790904E-05   3.235216E-04   1.855676E-05  -3.239881E-05
  a    38  -3.313199E-03  -1.384752E-02  -4.406333E-04   2.128731E-03   2.688174E-04   1.345731E-03  -7.994572E-04  -5.896104E-04
  a    39  -2.671169E-03  -8.163994E-03  -2.843290E-03  -4.010200E-03  -3.334063E-04  -1.704116E-03   4.242600E-04   5.143290E-04
  a    40  -2.738722E-03   8.271494E-03   1.604992E-03   1.422273E-03   3.932931E-05   3.651640E-04  -3.000593E-04  -1.850928E-04
  a    41   4.474385E-03  -8.881165E-04   1.694519E-03  -7.066088E-03  -6.959553E-06   8.418000E-04  -6.384987E-04  -2.038737E-04
  a    42   2.492882E-03  -6.741526E-04   2.492422E-03  -4.092236E-03   1.677110E-04   2.045476E-03  -1.365323E-03  -6.605178E-04
  a    43   1.219262E-03   2.097387E-03  -5.331927E-04   9.487695E-05  -6.168621E-05  -3.644720E-04  -2.928037E-04  -9.931303E-05
  a    44  -2.615802E-03  -6.188345E-03  -1.532400E-04  -6.104235E-03   4.775259E-06  -2.127288E-04   1.608641E-04   8.173891E-05
  a    45  -1.263396E-02   9.596814E-03   2.604545E-03   3.392591E-03   7.454521E-05   9.925214E-04  -3.251742E-04  -1.477994E-04
  a    46   7.030551E-03   1.810958E-03  -7.858518E-05   1.576406E-02   1.325920E-04   2.927679E-04  -2.064806E-04  -1.536489E-04
  a    47   4.126124E-03  -3.233348E-04   1.899324E-03  -1.358641E-02   1.304679E-04   3.427121E-04   1.593668E-04  -7.286679E-05
  a    48   6.418010E-03   1.146073E-03   1.200537E-03  -5.625981E-03   1.351932E-04   6.117657E-04   3.649035E-05  -1.048284E-04
  a    49   3.532597E-03  -5.056956E-03   8.388143E-05  -3.128123E-04   2.710035E-06   1.123263E-04   7.141740E-05  -2.139507E-05
  a    50   8.035374E-03   1.065441E-02   1.209427E-03  -1.550408E-03  -2.959067E-04  -3.854042E-04  -3.258893E-04   7.367408E-04
  a    51  -4.480697E-04  -5.889816E-04  -5.291260E-04  -5.810635E-06  -3.526688E-03   1.319024E-03  -9.198514E-03   1.061850E-02
  a    52  -3.946607E-03   7.383391E-03   1.362817E-03  -6.458190E-03  -1.939177E-06   1.916044E-04  -9.318017E-05  -7.268003E-06
  a    53  -4.407313E-03  -8.309904E-03  -2.491322E-03   7.701490E-03   3.981168E-05   1.740664E-04  -3.793892E-04  -1.401306E-04
  a    54   3.086998E-04  -5.264531E-04   1.636801E-03  -1.298186E-04  -1.010228E-03   1.585740E-02  -5.835321E-03  -3.958526E-03
  a    55  -6.098872E-03   8.046197E-03   7.006340E-04   3.529057E-03  -8.863102E-05   6.605275E-04  -2.936652E-04  -2.649103E-04
  a    56  -6.393633E-03   9.109975E-04   1.116978E-03  -5.072617E-04  -4.743396E-05  -2.838020E-04   4.005575E-05  -4.457311E-05
  a    57  -5.778951E-03  -2.610355E-03   8.512937E-04  -5.713923E-03   4.107220E-05   4.431637E-05   3.498483E-05   1.013175E-04
  a    58   2.178539E-04  -2.087741E-04   9.370190E-04  -4.371650E-04  -9.621087E-04   3.438063E-03  -1.527231E-03   2.868547E-03
  a    59  -1.347384E-04  -4.307935E-04   1.689465E-03  -7.952690E-04   2.403826E-03   9.395719E-03   4.545840E-04   1.394684E-03
  a    60  -3.368710E-03  -6.353179E-03  -4.297746E-04  -9.234157E-03  -1.438752E-04  -8.962903E-04   7.738282E-05  -2.705966E-04
  a    61  -1.096377E-03  -2.307351E-03  -8.385000E-04   8.817678E-03  -2.432718E-05   2.966526E-04  -4.665246E-04   1.926894E-04
  a    62  -1.432369E-04  -3.178374E-04  -6.626663E-04   2.441958E-03   3.183395E-04  -4.627341E-04   3.656206E-03  -5.762777E-04
  a    63   2.021743E-03  -2.951400E-04   1.145107E-03  -1.185413E-02  -1.928183E-05  -1.404331E-04   6.187769E-04  -8.117457E-05
  a    64  -4.315096E-04   4.700870E-04   7.126819E-04   3.711198E-03   1.324665E-04   6.276837E-04   6.001388E-04  -2.027354E-05
  a    65  -1.996527E-03   1.411466E-05   9.317782E-04  -3.405512E-03   3.769169E-05   2.123301E-04   5.289852E-05  -1.159460E-05
  a    66  -5.201258E-04  -1.644151E-03  -9.692731E-04   3.191333E-03  -1.372657E-05  -4.938823E-05   1.958345E-06   2.334052E-05
  a    67   7.843973E-04  -2.263239E-03  -6.734083E-04   9.137804E-04  -1.589557E-05   4.040931E-04   4.834086E-05   5.660296E-05
  a    68  -1.561562E-03  -2.586536E-04  -2.386925E-05  -9.668027E-04  -5.770335E-04   2.120153E-03   8.795867E-04   2.328806E-03
  a    69   1.172134E-03   9.182107E-05  -3.456154E-04   5.445593E-04  -7.594552E-04   1.207629E-03   5.053597E-04   3.147198E-03
  a    70   1.207446E-04  -2.416022E-04  -8.334052E-04  -8.879327E-04   7.603359E-05   2.459829E-03   3.377175E-04  -7.024332E-04
  a    71  -3.474938E-04   8.125419E-04   1.072488E-03  -3.355000E-03   8.759894E-05  -4.063330E-04  -2.839470E-04   3.944310E-05
  a    72  -2.295475E-03  -3.198829E-04   6.617118E-04  -6.045661E-03   4.659969E-05  -7.549212E-05  -1.121245E-04   2.601482E-06
  a    73   3.028922E-05  -7.541015E-05   4.247678E-06  -5.211460E-04  -1.388857E-03   4.357710E-03   4.338706E-03   2.147437E-04
  a    74   9.419552E-04   3.044775E-03   2.583969E-04   2.537323E-03  -4.272406E-05   4.589162E-04   9.745376E-05  -1.632972E-05
  a    75   1.817924E-03   2.822205E-04   5.612999E-05   2.943258E-03   1.266068E-05   1.567453E-05   3.153592E-05   4.280549E-06
  a    76   1.688212E-03   4.847658E-04  -6.200122E-04   4.625029E-03   7.998795E-06  -4.664925E-05  -2.331109E-05  -4.317037E-05
  a    77  -1.272500E-03  -5.341270E-04   2.893262E-04  -1.491271E-03   6.670588E-06   6.789579E-06   3.119306E-05   1.188654E-05
  a    78   9.095073E-04  -2.007915E-03  -4.310045E-04  -1.171880E-03   5.546506E-05  -2.828035E-04  -1.098444E-04  -3.429767E-05
  a    79  -4.695929E-04   1.299154E-03   4.457088E-05  -1.802538E-03   1.842658E-05  -1.568889E-04   8.236257E-06   5.785910E-05
  a    80   1.248094E-03   6.818183E-05   4.104062E-04  -1.424791E-03  -1.658321E-05   1.279397E-04   6.848672E-06  -1.633538E-05
  a    81   1.368975E-03   4.936118E-04   1.772491E-04  -3.274384E-04   8.342203E-06  -2.176057E-06  -1.453575E-06   2.031110E-06
  a    82   1.573673E-04  -1.852520E-03  -4.790993E-04   8.616770E-04  -2.058732E-05  -1.565050E-05  -1.977280E-05  -1.828315E-05
  a    83   7.919160E-04  -3.214664E-04   4.414295E-04  -1.083251E-03   2.545523E-05   4.268683E-05  -7.830748E-06  -2.590066E-06
  a    84   5.470632E-04   7.818921E-05  -1.812554E-04   9.429364E-04  -2.593231E-05   7.408719E-05  -3.037657E-05  -8.575380E-06
  a    85   2.500027E-04  -3.554380E-04  -3.497621E-04  -5.671466E-05  -1.448026E-05   1.841517E-05  -2.317284E-05   2.429979E-07
  a    86   5.151302E-04   4.498302E-04   1.953131E-04   1.506588E-03   2.686132E-06   8.204374E-06  -2.638381E-06   8.469385E-06

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a    13   9.126617E-05   2.844100E-05  -3.714839E-05   3.768300E-05   9.580276E-04  -6.329177E-04  -3.972053E-04  -3.176260E-03
  a    14   5.139232E-04  -1.680994E-04  -6.650124E-05  -2.218376E-04   9.733194E-04   2.143075E-03   1.254376E-03  -1.635248E-03
  a    15  -1.947011E-04   4.460345E-04   2.004471E-04   2.751261E-04   4.461457E-05   3.833006E-03  -2.056515E-03  -3.206925E-03
  a    16   3.370440E-04  -2.907771E-04  -4.377948E-05  -9.980123E-05  -2.578552E-03  -2.694692E-03  -3.787643E-04  -1.660908E-03
  a    17  -1.130398E-03   8.241827E-05   1.196690E-04   7.910726E-05  -4.031410E-03   2.703011E-03   1.198584E-03  -3.246684E-03
  a    18   2.829744E-03   1.246187E-04   3.680518E-06  -1.979900E-05  -4.006768E-03   1.062832E-04  -4.553374E-03  -6.605209E-04
  a    19  -3.838264E-03  -5.515755E-04  -1.282827E-04  -2.501055E-04  -3.788460E-03   3.591776E-03   3.812811E-03  -6.802149E-03
  a    20  -4.392412E-04  -3.279492E-05  -6.733662E-05  -5.141004E-05   6.262775E-03   1.015187E-02   2.813148E-03   1.231225E-03
  a    21  -8.184492E-03  -5.234284E-04  -1.116893E-04  -2.639161E-04  -1.004723E-03  -2.589089E-03  -1.775111E-03   1.494770E-03
  a    22  -4.943942E-04   1.073748E-03   4.908638E-04   7.880976E-04   1.524609E-03   6.567537E-03   1.934602E-03   1.193202E-03
  a    23  -9.453386E-03   3.225968E-04   3.174037E-04   2.836317E-04  -8.204914E-04   1.275589E-03  -7.851715E-03   3.737857E-03
  a    24  -1.672083E-03  -4.824617E-05   1.137001E-04  -7.923350E-05   4.560878E-03  -5.428564E-03   1.680284E-03  -7.245989E-03
  a    25  -4.994424E-03   1.239305E-03   1.480190E-04   6.004100E-04   1.104503E-03  -3.313199E-03  -2.671169E-03  -2.738722E-03
  a    26   9.858245E-04  -8.528401E-04  -6.389811E-04  -3.629899E-04   6.571518E-03  -1.384752E-02  -8.163994E-03   8.271494E-03
  a    27   2.646344E-03   5.462580E-03   5.191436E-03   3.591863E-04   2.433774E-03  -4.406333E-04  -2.843290E-03   1.604992E-03
  a    28  -7.120289E-03   4.664315E-04   5.857311E-04  -6.051442E-04  -1.310047E-02   2.128731E-03  -4.010200E-03   1.422273E-03
  a    29  -1.632542E-04  -2.403519E-02   1.617050E-02  -5.584187E-03   2.790904E-05   2.688174E-04  -3.334063E-04   3.932931E-05
  a    30   3.834317E-04  -8.186391E-02   3.518579E-02   2.495965E-02   3.235216E-04   1.345731E-03  -1.704116E-03   3.651640E-04
  a    31  -7.263964E-04   6.260637E-02   2.176533E-02   2.100103E-02   1.855676E-05  -7.994572E-04   4.242600E-04  -3.000593E-04
  a    32   2.535088E-03   3.957521E-02   8.962713E-02   2.290180E-02  -3.239881E-05  -5.896104E-04   5.143290E-04  -1.850928E-04
  a    33    1.00025       2.233282E-05  -2.209814E-03   2.561460E-04  -2.882774E-03   2.520343E-03   6.111985E-04   4.086042E-03
  a    34   2.233282E-05   0.115337      -0.108977      -1.393704E-02   7.037772E-05  -7.670579E-04   4.140118E-04  -2.767756E-04
  a    35  -2.209814E-03  -0.108977       0.974462       6.450749E-03  -1.098479E-03   2.739397E-03  -6.747348E-05   1.699393E-03
  a    36   2.561460E-04  -1.393704E-02   6.450749E-03   3.044912E-02   1.100705E-05   1.129521E-04  -2.063735E-04  -3.109436E-06
  a    37  -2.882774E-03   7.037772E-05  -1.098479E-03   1.100705E-05   2.368127E-03  -1.973227E-03  -9.499716E-04   7.897342E-04
  a    38   2.520343E-03  -7.670579E-04   2.739397E-03   1.129521E-04  -1.973227E-03   3.245019E-03   1.584980E-03  -1.135940E-03
  a    39   6.111985E-04   4.140118E-04  -6.747348E-05  -2.063735E-04  -9.499716E-04   1.584980E-03   1.385706E-03  -5.103688E-04
  a    40   4.086042E-03  -2.767756E-04   1.699393E-03  -3.109436E-06   7.897342E-04  -1.135940E-03  -5.103688E-04   1.576325E-03
  a    41  -4.071539E-03  -6.994549E-04   4.774890E-03  -6.442831E-05   1.501060E-04  -3.573179E-04  -3.896943E-04  -2.535718E-04
  a    42   3.656503E-03  -1.413434E-03   8.405869E-03   8.841584E-06   3.562816E-04  -1.992331E-04  -3.744250E-05   2.133081E-04
  a    43   8.511138E-03  -8.054645E-05   1.462242E-03  -6.236929E-05  -7.669173E-05   2.951398E-04  -1.065685E-04   4.286841E-04
  a    44   7.490691E-03   1.253385E-04  -1.199223E-03   6.470175E-06  -1.148367E-04   9.573792E-04   4.646520E-04  -6.529686E-04
  a    45  -3.508096E-03  -2.727570E-04   1.296175E-03   4.584503E-05   4.078283E-05  -6.594476E-04   2.192329E-04   4.000966E-04
  a    46  -2.561591E-03  -7.465615E-05  -2.999029E-04   4.337610E-05  -9.094461E-05  -4.724296E-04   2.367891E-05   6.769285E-04
  a    47  -5.510271E-04  -1.268913E-04  -5.617070E-04   1.049991E-04   4.762076E-04  -6.146548E-04  -1.020177E-04   2.645100E-04
  a    48   3.236875E-03  -2.237194E-04  -5.362239E-04   1.343577E-04  -1.153653E-04   1.396376E-04   2.290878E-04  -9.986329E-05
  a    49   7.704881E-03   1.756228E-05  -6.378642E-04   8.359038E-06  -4.627860E-04   6.095871E-04   1.769877E-04  -1.296975E-04
  a    50   1.476694E-02   4.172838E-04  -1.995955E-03  -5.820399E-05   3.742215E-04  -6.209734E-04  -3.555929E-04   6.904528E-04
  a    51  -9.895450E-04   3.201350E-03  -1.953667E-02  -3.412321E-05   4.593623E-06  -2.759749E-05   2.201417E-05  -8.219459E-05
  a    52   2.190471E-03  -4.522701E-05   2.798342E-04   2.532594E-06   1.007801E-03  -1.151270E-03  -7.230436E-04   3.441906E-04
  a    53   1.731106E-02  -2.335686E-04   1.065704E-03   2.662245E-05  -5.556478E-04   6.369185E-04   2.338288E-04  -1.147242E-04
  a    54   9.163965E-08  -5.665651E-03   7.345428E-04   1.018004E-03   2.584454E-05   4.406740E-05  -5.634530E-05  -1.731918E-05
  a    55  -1.637778E-03  -1.148631E-04  -7.867974E-04   1.376278E-05  -1.406966E-04  -1.586464E-04   7.397178E-05   4.182269E-04
  a    56  -3.885587E-04   1.173100E-04  -8.039650E-04  -2.188589E-05   2.160670E-04  -1.158487E-04  -1.622851E-04  -1.585951E-04
  a    57  -1.190235E-04  -4.912568E-05   4.960959E-04  -7.474819E-06   2.756693E-04   1.239090E-04  -2.181611E-04  -8.700520E-05
  a    58  -7.101637E-05  -1.359867E-03   1.337575E-02  -7.029073E-04  -9.339566E-06   5.383591E-05   2.743862E-05   2.258129E-05
  a    59  -2.680604E-05  -1.221314E-03   1.979878E-02   2.191236E-04  -1.273668E-06   8.436903E-05   1.239165E-05   3.021592E-05
  a    60   3.584425E-04   2.432680E-04  -2.226975E-03  -1.351791E-06   2.858570E-04   2.882881E-04   1.983723E-04  -2.683882E-04
  a    61   9.467157E-04  -6.077827E-04   2.626803E-03   9.388334E-05  -1.843625E-04   8.531408E-05  -6.528286E-05  -8.127181E-05
  a    62   2.016418E-04   4.393515E-03  -1.703125E-02  -9.561899E-04  -3.523107E-05  -7.366820E-05   2.761497E-05  -6.377234E-05
  a    63  -2.422884E-04   5.860044E-04  -2.825705E-03  -1.290097E-04   2.345634E-04   8.338785E-06  -6.256640E-05  -2.764747E-05
  a    64  -9.459927E-05   4.597393E-04  -2.679588E-03  -6.447740E-05  -3.329514E-05  -4.507489E-05  -3.574050E-05   6.229478E-05
  a    65  -6.824796E-04   2.678611E-05  -2.544118E-04  -3.426704E-06   1.442581E-05   8.963109E-05   1.402251E-04  -6.103786E-05
  a    66   6.743004E-04  -4.250579E-06  -1.930786E-05   2.668801E-06   6.361329E-05   2.444820E-05   9.089335E-06   9.940733E-05
  a    67   1.098063E-03   4.012107E-05  -3.322034E-04  -2.072767E-05  -7.515467E-05  -1.100090E-04  -1.000907E-04  -1.405171E-04
  a    68  -9.407258E-05   9.301162E-04  -7.398532E-03  -4.256799E-04   4.483556E-05  -8.902606E-06   3.166590E-05  -1.736982E-06
  a    69   3.213485E-05   1.066591E-03  -7.804772E-03  -5.285030E-04  -8.493736E-06  -4.641825E-05  -2.359691E-05  -3.974315E-05
  a    70   3.699740E-05   2.467728E-04  -1.826385E-03  -1.176762E-05   2.264597E-05   1.056680E-05  -3.776219E-06  -1.702261E-05
  a    71   3.375453E-04  -8.632864E-05   1.103731E-03  -1.325140E-05  -6.992247E-06   3.572275E-05   7.549193E-05   4.536091E-05
  a    72  -8.459763E-04  -4.172920E-05   5.475725E-04  -1.001694E-05  -1.117696E-04   1.842975E-04   1.763656E-04  -8.013646E-05
  a    73  -9.308164E-06   9.755722E-04  -1.585344E-02   3.590232E-04   2.066568E-05  -4.828193E-05  -6.119972E-06  -5.533269E-05
  a    74  -6.135993E-04   1.237540E-04  -9.799190E-04  -4.398887E-05  -6.463725E-05  -6.857066E-05  -3.760796E-05   1.388678E-04
  a    75  -9.123821E-04  -1.192920E-05   8.726231E-05   1.260408E-05  -2.655667E-05  -9.219083E-05  -9.892438E-05   7.225891E-05
  a    76  -3.421892E-05  -4.025544E-05   2.132633E-04   1.207660E-05  -1.001353E-04   8.971701E-05  -2.290405E-05  -2.675749E-05
  a    77  -4.560197E-05   7.581910E-06  -1.014152E-04   5.867857E-06   2.036163E-04  -2.744717E-04  -3.493191E-05   1.778757E-04
  a    78  -1.646458E-04  -6.840153E-05   6.261135E-04   1.739261E-05  -1.449023E-05   7.029828E-05  -6.729983E-06  -1.152849E-04
  a    79   4.187712E-04  -5.947229E-05   5.874559E-04   2.560917E-05   4.816908E-05  -6.429292E-05   9.950410E-06   3.735289E-05
  a    80   6.479883E-04   2.748346E-05  -2.656946E-04  -1.497843E-05   6.984349E-05  -6.879273E-05  -2.786404E-05  -3.278634E-05
  a    81   7.704958E-05   7.241005E-06  -1.288346E-05   2.575257E-06  -3.894940E-05  -7.592150E-05  -3.952329E-05   9.781218E-05
  a    82  -7.894647E-04  -3.713921E-06   6.318633E-05  -2.723426E-06  -3.355118E-06   2.353673E-05   6.526857E-05   1.670610E-05
  a    83   2.486215E-04  -1.183231E-06  -5.113530E-05   6.107937E-06  -3.362896E-05   8.566108E-05   7.063229E-05  -5.588049E-05
  a    84  -2.811597E-04   6.316998E-06   8.591309E-06  -6.616227E-06  -3.713716E-05   4.769912E-05  -1.777660E-05   4.193348E-06
  a    85   5.108350E-04   1.074041E-05  -3.178658E-05  -6.426606E-06   3.682311E-05  -3.374920E-07   2.796641E-05  -1.363418E-05
  a    86   1.437827E-04   1.174878E-06  -1.198025E-05   5.649497E-07   2.299265E-05   8.378804E-06  -1.351044E-05   3.761770E-05

               a    41        a    42        a    43        a    44        a    45        a    46        a    47        a    48
  a    13   2.447980E-03  -2.135829E-03  -7.720548E-03  -3.739535E-03   8.735051E-04  -3.698438E-04  -5.139630E-04  -2.480768E-04
  a    14   2.710720E-03  -1.662289E-04   2.212534E-03   4.753721E-03  -2.204445E-03   3.536899E-03  -1.979654E-03  -2.489849E-03
  a    15   6.646461E-04   1.666421E-03   3.033670E-03   4.706622E-03  -6.595468E-03   6.255090E-04   4.908883E-05  -1.328660E-03
  a    16   1.895098E-03   7.059731E-04  -6.475252E-03  -3.866298E-03  -3.629849E-03  -1.497504E-03   3.750090E-03  -1.700843E-03
  a    17  -2.152916E-04   3.929564E-03   1.175486E-03   3.263196E-03  -4.791104E-03   1.098268E-03  -5.030475E-03   4.553207E-03
  a    18   7.725301E-03  -8.578698E-03  -3.410090E-03  -4.057608E-03   7.502942E-03   6.260801E-03  -6.601114E-03  -3.688936E-03
  a    19   2.902892E-03  -3.634249E-03   3.077555E-03   3.962355E-03  -4.873100E-03  -2.242313E-03  -2.698448E-03   4.021449E-03
  a    20   2.262132E-03  -3.873083E-03  -6.222129E-03  -7.525346E-04  -1.176640E-02   1.782693E-03  -8.140890E-03  -1.127833E-03
  a    21  -9.551942E-03  -8.733909E-04   1.754538E-02   5.684863E-03  -2.094715E-03  -9.503729E-03   3.370356E-03   3.788134E-03
  a    22  -3.064338E-03   2.407418E-03  -8.038107E-03   1.654516E-03   5.184406E-04  -5.923683E-05   2.636117E-04   6.270246E-04
  a    23   1.725809E-03   1.949280E-03   1.400712E-02  -2.024439E-03  -4.456681E-03  -7.056408E-03   3.242026E-03   6.143483E-03
  a    24  -3.388028E-03   2.211680E-03   8.690911E-04  -4.721510E-04   7.119281E-04   6.281889E-03  -4.100618E-03   3.260848E-03
  a    25   4.474385E-03   2.492882E-03   1.219262E-03  -2.615802E-03  -1.263396E-02   7.030551E-03   4.126124E-03   6.418010E-03
  a    26  -8.881165E-04  -6.741526E-04   2.097387E-03  -6.188345E-03   9.596814E-03   1.810958E-03  -3.233348E-04   1.146073E-03
  a    27   1.694519E-03   2.492422E-03  -5.331927E-04  -1.532400E-04   2.604545E-03  -7.858518E-05   1.899324E-03   1.200537E-03
  a    28  -7.066088E-03  -4.092236E-03   9.487695E-05  -6.104235E-03   3.392591E-03   1.576406E-02  -1.358641E-02  -5.625981E-03
  a    29  -6.959553E-06   1.677110E-04  -6.168621E-05   4.775259E-06   7.454521E-05   1.325920E-04   1.304679E-04   1.351932E-04
  a    30   8.418000E-04   2.045476E-03  -3.644720E-04  -2.127288E-04   9.925214E-04   2.927679E-04   3.427121E-04   6.117657E-04
  a    31  -6.384987E-04  -1.365323E-03  -2.928037E-04   1.608641E-04  -3.251742E-04  -2.064806E-04   1.593668E-04   3.649035E-05
  a    32  -2.038737E-04  -6.605178E-04  -9.931303E-05   8.173891E-05  -1.477994E-04  -1.536489E-04  -7.286679E-05  -1.048284E-04
  a    33  -4.071539E-03   3.656503E-03   8.511138E-03   7.490691E-03  -3.508096E-03  -2.561591E-03  -5.510271E-04   3.236875E-03
  a    34  -6.994549E-04  -1.413434E-03  -8.054645E-05   1.253385E-04  -2.727570E-04  -7.465615E-05  -1.268913E-04  -2.237194E-04
  a    35   4.774890E-03   8.405869E-03   1.462242E-03  -1.199223E-03   1.296175E-03  -2.999029E-04  -5.617070E-04  -5.362239E-04
  a    36  -6.442831E-05   8.841584E-06  -6.236929E-05   6.470175E-06   4.584503E-05   4.337610E-05   1.049991E-04   1.343577E-04
  a    37   1.501060E-04   3.562816E-04  -7.669173E-05  -1.148367E-04   4.078283E-05  -9.094461E-05   4.762076E-04  -1.153653E-04
  a    38  -3.573179E-04  -1.992331E-04   2.951398E-04   9.573792E-04  -6.594476E-04  -4.724296E-04  -6.146548E-04   1.396376E-04
  a    39  -3.896943E-04  -3.744250E-05  -1.065685E-04   4.646520E-04   2.192329E-04   2.367891E-05  -1.020177E-04   2.290878E-04
  a    40  -2.535718E-04   2.133081E-04   4.286841E-04  -6.529686E-04   4.000966E-04   6.769285E-04   2.645100E-04  -9.986329E-05
  a    41   1.255824E-03  -4.152312E-04  -6.339605E-04  -1.629627E-04   7.659312E-05   2.689611E-04  -1.392551E-04  -1.754923E-04
  a    42  -4.152312E-04   8.978774E-04   5.469117E-04   4.945675E-04  -4.887894E-04  -2.378617E-04   3.791250E-04   2.463377E-05
  a    43  -6.339605E-04   5.469117E-04   2.723043E-03   5.753380E-04  -9.752684E-04  -2.983207E-04   2.924579E-04   3.966364E-04
  a    44  -1.629627E-04   4.945675E-04   5.753380E-04   2.771027E-03  -5.255826E-04  -1.208288E-03   2.572848E-04   5.244905E-05
  a    45   7.659312E-05  -4.887894E-04  -9.752684E-04  -5.255826E-04   2.449129E-03   5.514650E-04  -2.680851E-06  -5.180190E-06
  a    46   2.689611E-04  -2.378617E-04  -2.983207E-04  -1.208288E-03   5.514650E-04   2.545152E-03  -4.527940E-04  -3.400431E-04
  a    47  -1.392551E-04   3.791250E-04   2.924579E-04   2.572848E-04  -2.680851E-06  -4.527940E-04   1.090049E-03   1.231307E-04
  a    48  -1.754923E-04   2.463377E-05   3.966364E-04   5.244905E-05  -5.180190E-06  -3.400431E-04   1.231307E-04   6.935484E-04
  a    49   2.045357E-04  -1.710340E-04   3.406069E-04   5.948669E-05  -2.961528E-04   2.218153E-04  -3.709388E-04  -4.500683E-05
  a    50  -3.310057E-04   5.379422E-04   6.180577E-04   2.673302E-05  -2.392434E-04   2.537849E-04   3.769642E-04   6.590885E-05
  a    51  -9.266711E-05  -2.292630E-04  -7.173145E-05   2.862475E-05  -1.452848E-05  -8.158059E-06  -8.668913E-06   1.049660E-05
  a    52   2.662450E-04  -2.071902E-05  -4.055452E-05  -1.801752E-04  -1.611078E-05  -2.697842E-04   2.229309E-04  -6.146193E-05
  a    53  -3.415822E-04   5.666578E-05   6.220710E-04   2.639373E-04  -6.179011E-04  -2.549442E-04  -1.390985E-04   1.544147E-04
  a    54   2.838239E-05   3.713498E-05  -3.188522E-05   3.305856E-05  -4.409883E-05  -7.706821E-06   1.944810E-05   1.870466E-05
  a    55  -3.529909E-04  -1.690808E-05   4.514125E-04  -1.927844E-04   7.920983E-04   1.570504E-04   9.977413E-05   2.134359E-04
  a    56   4.296720E-04  -1.174700E-04  -4.822330E-04   5.551920E-04   1.638210E-04  -7.533387E-06  -2.775743E-04  -3.747175E-04
  a    57  -8.762774E-05   2.553230E-04   8.360337E-04   3.903194E-04  -9.585641E-04  -7.401677E-04   9.603079E-05  -3.567066E-05
  a    58   8.941703E-05   1.735728E-04   2.735421E-05   3.007434E-05   1.386451E-05  -9.143286E-06   1.421451E-05  -7.777351E-06
  a    59   1.294362E-04   2.516469E-04   7.302581E-05   5.914533E-05   7.818322E-07  -5.741085E-05   5.531078E-06  -2.627878E-05
  a    60  -9.997508E-05   2.869924E-04   3.757067E-04   1.079686E-03  -5.477521E-04  -5.394965E-04   3.096034E-04  -4.863809E-05
  a    61  -8.601273E-05  -1.960318E-04  -2.157279E-04  -3.759746E-04   1.007919E-04  -1.693306E-04  -2.694110E-04   1.958058E-05
  a    62  -1.397291E-04  -2.564463E-04  -9.339302E-05  -7.743512E-05  -1.778121E-05   2.313265E-05  -3.692735E-05   1.216335E-05
  a    63   1.248335E-04   7.200215E-05   2.378718E-04   3.219931E-04  -2.306842E-04  -4.466062E-04   1.025468E-04   5.793287E-05
  a    64  -5.487128E-05  -5.098822E-05  -6.733496E-05  -5.461606E-05   8.353313E-05   1.986138E-04  -2.107520E-05  -6.744779E-05
  a    65  -1.057297E-04   3.187453E-05  -3.901666E-05  -9.994233E-05   7.665939E-05  -7.624270E-05  -1.093537E-04   3.934900E-05
  a    66  -1.582358E-04   7.108513E-05  -1.349809E-04   6.668809E-05  -1.298050E-04  -1.808448E-05  -8.935702E-05  -8.801529E-05
  a    67   1.129405E-04  -2.236640E-05   1.208825E-05   1.128557E-04  -1.000090E-04  -9.349531E-05   5.118795E-05   3.487370E-05
  a    68  -7.150325E-05  -6.757024E-05   2.658823E-05   8.301173E-05   9.741855E-06  -8.456068E-06   2.539809E-05   2.836359E-06
  a    69  -5.924986E-05  -1.313566E-04  -4.838637E-05  -3.149218E-05  -4.646558E-05  -5.886187E-06  -4.131882E-06   1.408797E-05
  a    70  -3.394332E-05  -3.748751E-05  -2.086779E-06   1.157269E-05  -2.671000E-05  -5.989230E-05   1.480316E-05   1.661494E-05
  a    71   5.545759E-05  -2.907736E-05  -9.520826E-05   8.651652E-06   1.039739E-04  -8.531973E-05   4.995366E-05   5.782031E-05
  a    72   1.663084E-04  -9.739363E-05  -1.463976E-04   7.193868E-05   1.198595E-04   2.736028E-05   2.529199E-05  -3.124466E-05
  a    73  -1.180275E-04  -2.177202E-04  -5.385781E-05   3.926045E-05  -3.472471E-05  -1.192042E-05   3.128367E-05   3.691827E-05
  a    74   3.740798E-05  -5.432191E-05  -1.028093E-04  -1.716558E-04   1.576816E-04   3.701130E-04  -1.103508E-04  -8.252485E-05
  a    75   5.538492E-05  -2.233921E-05  -1.037723E-04  -1.944207E-04  -2.249325E-05   2.898846E-04  -1.101132E-04  -7.730607E-05
  a    76  -7.633185E-05   4.017186E-05   5.205480E-05  -1.482855E-05  -1.579607E-04   6.397466E-05  -5.182897E-05  -2.221947E-05
  a    77   4.284922E-05  -2.074285E-05  -1.032504E-04  -1.595767E-04   2.281441E-04   1.345209E-04   7.410765E-05   1.550432E-05
  a    78   3.862501E-05   1.122350E-05   1.702682E-05   4.590737E-07  -2.016747E-04  -2.001475E-04  -7.885314E-06   4.795838E-05
  a    79   2.990357E-05   3.600371E-05   6.549609E-06   7.158044E-05   1.051768E-04   5.916086E-05   4.527098E-05   2.652204E-06
  a    80   1.518641E-05   1.728671E-05   4.396162E-05   7.705802E-05  -5.520103E-05  -7.491952E-05   5.017084E-05   5.287526E-05
  a    81   1.193062E-05  -2.186873E-05  -3.372945E-06  -1.448059E-04   6.300380E-05   1.104990E-04  -1.639484E-05   4.066721E-05
  a    82  -5.917119E-05   2.893044E-05   8.823794E-06   4.006575E-05  -4.703242E-05   5.049185E-05   4.978234E-05   4.251731E-06
  a    83  -2.168440E-07   8.598653E-06  -6.068565E-05   7.506691E-05   1.211572E-05  -2.149160E-05   4.298469E-05  -1.211605E-06
  a    84  -1.466920E-05   1.466951E-05   6.085420E-05   3.554437E-05  -7.548246E-05  -1.473970E-06  -1.785284E-05   4.699306E-06
  a    85  -5.236208E-08  -8.168109E-06  -4.111029E-05  -1.510164E-05   2.950758E-05   4.050957E-05   8.037768E-06   5.014946E-06
  a    86  -1.818072E-05   2.172640E-05   3.943930E-05   2.295396E-05  -3.953322E-05   8.287544E-05  -2.887809E-05  -1.498855E-05

               a    49        a    50        a    51        a    52        a    53        a    54        a    55        a    56
  a    13  -1.720179E-03  -1.703365E-03   9.885173E-05   6.540097E-04   4.930525E-04   1.037537E-04  -3.079821E-03  -1.038966E-03
  a    14   3.250640E-03  -1.811658E-05  -9.775151E-06   9.493599E-04  -4.102174E-04   8.658041E-05  -1.516644E-03   3.029416E-03
  a    15   3.012328E-03   6.567868E-04  -5.499113E-05   4.716780E-04   1.006748E-03   2.088369E-04  -2.277508E-03   4.115301E-03
  a    16  -6.801686E-04   1.086270E-03  -5.338782E-05   1.994361E-04  -1.022447E-03   2.503148E-04  -4.245059E-03  -3.965674E-04
  a    17  -1.087076E-03   3.461613E-03  -1.986320E-04  -5.934094E-03  -1.037687E-03  -6.344236E-06   1.277836E-03   9.184758E-04
  a    18   1.674763E-03  -5.476211E-03   3.138510E-04   1.723731E-03  -1.738273E-03  -3.955311E-05  -5.696944E-04   2.041019E-03
  a    19  -4.522754E-03  -2.572777E-03   1.309542E-04  -1.148750E-03   6.253594E-03   1.994518E-04  -3.645783E-03  -1.645432E-03
  a    20   4.529083E-04  -2.904116E-03   1.492069E-04   6.853701E-04   2.260503E-03   4.883034E-04  -8.653575E-03   1.008472E-03
  a    21  -2.472868E-03  -2.101451E-03   9.039329E-05   3.371400E-03   8.166364E-03  -3.178533E-04   5.305922E-03  -3.938920E-03
  a    22  -6.914629E-03   1.952016E-03  -1.425059E-04  -1.183875E-03   1.839404E-03   1.269910E-04  -2.005659E-03  -1.552610E-03
  a    23  -2.317556E-03   1.292510E-03  -9.180526E-05   4.631164E-04   5.787274E-03  -2.119468E-04   3.474815E-03  -5.271756E-03
  a    24   2.512145E-03  -7.440729E-03   4.073346E-04   8.953323E-03   3.044139E-03   9.523216E-05  -4.703853E-03  -8.564355E-04
  a    25   3.532597E-03   8.035374E-03  -4.480697E-04  -3.946607E-03  -4.407313E-03   3.086998E-04  -6.098872E-03  -6.393633E-03
  a    26  -5.056956E-03   1.065441E-02  -5.889816E-04   7.383391E-03  -8.309904E-03  -5.264531E-04   8.046197E-03   9.109975E-04
  a    27   8.388143E-05   1.209427E-03  -5.291260E-04   1.362817E-03  -2.491322E-03   1.636801E-03   7.006340E-04   1.116978E-03
  a    28  -3.128123E-04  -1.550408E-03  -5.810635E-06  -6.458190E-03   7.701490E-03  -1.298186E-04   3.529057E-03  -5.072617E-04
  a    29   2.710035E-06  -2.959067E-04  -3.526688E-03  -1.939177E-06   3.981168E-05  -1.010228E-03  -8.863102E-05  -4.743396E-05
  a    30   1.123263E-04  -3.854042E-04   1.319024E-03   1.916044E-04   1.740664E-04   1.585740E-02   6.605275E-04  -2.838020E-04
  a    31   7.141740E-05  -3.258893E-04  -9.198514E-03  -9.318017E-05  -3.793892E-04  -5.835321E-03  -2.936652E-04   4.005575E-05
  a    32  -2.139507E-05   7.367408E-04   1.061850E-02  -7.268003E-06  -1.401306E-04  -3.958526E-03  -2.649103E-04  -4.457311E-05
  a    33   7.704881E-03   1.476694E-02  -9.895450E-04   2.190471E-03   1.731106E-02   9.163965E-08  -1.637778E-03  -3.885587E-04
  a    34   1.756228E-05   4.172838E-04   3.201350E-03  -4.522701E-05  -2.335686E-04  -5.665651E-03  -1.148631E-04   1.173100E-04
  a    35  -6.378642E-04  -1.995955E-03  -1.953667E-02   2.798342E-04   1.065704E-03   7.345428E-04  -7.867974E-04  -8.039650E-04
  a    36   8.359038E-06  -5.820399E-05  -3.412321E-05   2.532594E-06   2.662245E-05   1.018004E-03   1.376278E-05  -2.188589E-05
  a    37  -4.627860E-04   3.742215E-04   4.593623E-06   1.007801E-03  -5.556478E-04   2.584454E-05  -1.406966E-04   2.160670E-04
  a    38   6.095871E-04  -6.209734E-04  -2.759749E-05  -1.151270E-03   6.369185E-04   4.406740E-05  -1.586464E-04  -1.158487E-04
  a    39   1.769877E-04  -3.555929E-04   2.201417E-05  -7.230436E-04   2.338288E-04  -5.634530E-05   7.397178E-05  -1.622851E-04
  a    40  -1.296975E-04   6.904528E-04  -8.219459E-05   3.441906E-04  -1.147242E-04  -1.731918E-05   4.182269E-04  -1.585951E-04
  a    41   2.045357E-04  -3.310057E-04  -9.266711E-05   2.662450E-04  -3.415822E-04   2.838239E-05  -3.529909E-04   4.296720E-04
  a    42  -1.710340E-04   5.379422E-04  -2.292630E-04  -2.071902E-05   5.666578E-05   3.713498E-05  -1.690808E-05  -1.174700E-04
  a    43   3.406069E-04   6.180577E-04  -7.173145E-05  -4.055452E-05   6.220710E-04  -3.188522E-05   4.514125E-04  -4.822330E-04
  a    44   5.948669E-05   2.673302E-05   2.862475E-05  -1.801752E-04   2.639373E-04   3.305856E-05  -1.927844E-04   5.551920E-04
  a    45  -2.961528E-04  -2.392434E-04  -1.452848E-05  -1.611078E-05  -6.179011E-04  -4.409883E-05   7.920983E-04   1.638210E-04
  a    46   2.218153E-04   2.537849E-04  -8.158059E-06  -2.697842E-04  -2.549442E-04  -7.706821E-06   1.570504E-04  -7.533387E-06
  a    47  -3.709388E-04   3.769642E-04  -8.668913E-06   2.229309E-04  -1.390985E-04   1.944810E-05   9.977413E-05  -2.775743E-04
  a    48  -4.500683E-05   6.590885E-05   1.049660E-05  -6.146193E-05   1.544147E-04   1.870466E-05   2.134359E-04  -3.747175E-04
  a    49   9.330205E-04  -1.291217E-04   2.181571E-05  -1.025363E-04   2.312377E-04   1.420775E-05  -6.004634E-05   1.276784E-04
  a    50  -1.291217E-04   1.231971E-03   1.466165E-05   9.141141E-05   1.041778E-04  -1.120703E-05   1.747964E-04  -1.349211E-04
  a    51   2.181571E-05   1.466165E-05   1.159312E-03  -1.289890E-05  -3.155161E-05   9.902957E-05   1.540991E-05   2.713465E-05
  a    52  -1.025363E-04   9.141141E-05  -1.289890E-05   1.196016E-03  -1.091700E-04   1.728096E-05  -2.188645E-04   2.229231E-04
  a    53   2.312377E-04   1.041778E-04  -3.155161E-05  -1.091700E-04   1.128352E-03   1.138594E-05  -1.453552E-04  -1.794071E-04
  a    54   1.420775E-05  -1.120703E-05   9.902957E-05   1.728096E-05   1.138594E-05   1.189152E-03  -9.438601E-07   1.637724E-05
  a    55  -6.004634E-05   1.747964E-04   1.540991E-05  -2.188645E-04  -1.453552E-04  -9.438601E-07   8.878390E-04  -2.023453E-04
  a    56   1.276784E-04  -1.349211E-04   2.713465E-05   2.229231E-04  -1.794071E-04   1.637724E-05  -2.023453E-04   1.000276E-03
  a    57   1.579892E-04  -1.583211E-05  -1.022294E-05   3.268316E-04   1.864915E-04   2.193648E-05  -2.940290E-04  -1.014830E-05
  a    58  -1.932960E-05  -1.653862E-05  -2.214590E-04  -3.114745E-06   8.350810E-06  -5.829544E-05  -2.427373E-05  -7.621382E-06
  a    59  -2.278125E-05  -4.912744E-05  -5.572065E-04   1.614110E-05   1.813227E-05  -1.133747E-04  -4.834615E-05  -6.103672E-06
  a    60  -8.213864E-05  -4.116252E-05   5.589315E-05   1.223582E-04   6.481552E-05   2.795832E-05  -3.555553E-04   4.476675E-05
  a    61  -5.473659E-05  -2.416074E-04  -3.883717E-05   1.710079E-05   2.478493E-04   2.049721E-05   2.617993E-05  -4.377390E-05
  a    62  -9.715796E-06   1.918736E-07   3.398081E-04  -1.935145E-05   2.919089E-05  -2.223246E-04   3.232040E-06  -2.564572E-05
  a    63   1.674620E-04   6.972872E-05   5.694273E-05   1.302528E-04  -1.371751E-04  -1.336071E-05  -4.869326E-06   2.311089E-04
  a    64  -5.700738E-05   4.275495E-05   5.533388E-05  -3.764700E-05  -1.398054E-05  -5.352855E-06   1.567227E-05  -3.909512E-05
  a    65   1.771748E-04  -1.137636E-04   1.253300E-05   3.315822E-05  -5.875142E-05  -7.071962E-07   6.241052E-05   4.547527E-05
  a    66  -2.680353E-05  -2.517809E-05   1.478947E-06   4.909249E-05   8.492681E-05   7.731280E-06  -1.202191E-04   6.682452E-05
  a    67  -7.743953E-06  -5.319793E-05   1.086512E-05   9.646363E-05   9.072949E-05   1.304390E-05  -7.948006E-05   2.659837E-05
  a    68  -2.082561E-06   2.216090E-05   1.794781E-04  -3.486389E-06  -1.237529E-05   5.051104E-05   2.607128E-05   1.924224E-05
  a    69   1.362064E-05   2.052549E-05   1.897585E-04  -5.060837E-06  -9.409936E-06   1.556700E-05  -4.027984E-06  -2.905313E-06
  a    70  -2.388474E-06  -1.096129E-05   4.356034E-05   1.008057E-05   1.073287E-05   5.790504E-05  -7.059484E-06  -9.341447E-06
  a    71   1.862839E-05  -4.137942E-05  -2.387660E-05   8.908143E-06  -3.535181E-05  -2.303252E-05   1.531288E-05  -1.192659E-05
  a    72   1.115931E-04  -1.545031E-04  -3.877617E-06   3.425793E-05  -7.586141E-05  -7.024941E-06  -4.849447E-05  -2.821597E-07
  a    73   2.541708E-05   3.231118E-05   3.933232E-04  -6.332222E-06  -2.557084E-05   3.196594E-04   3.020099E-05   1.852507E-05
  a    74   7.970621E-05   9.830460E-05   1.749403E-05  -7.844646E-05  -1.440031E-04   6.784181E-06   9.255646E-05   6.322050E-05
  a    75   4.791155E-05   4.689929E-05  -4.407982E-06  -3.006474E-05  -4.861044E-05   1.178635E-06   2.513686E-06   3.506338E-05
  a    76  -4.941087E-05   6.124618E-05  -8.693690E-06  -1.103309E-04   8.651362E-05   3.876980E-06  -2.627740E-05  -3.643083E-05
  a    77  -4.027510E-05   2.329241E-05   1.335475E-06   1.149486E-04  -9.410269E-05  -2.664238E-06   5.979752E-05  -9.677035E-06
  a    78   3.546129E-05  -6.388552E-05  -1.209215E-05   3.645329E-05   6.442596E-05  -5.238511E-06  -8.224601E-05  -1.037238E-05
  a    79   1.328304E-05   2.937189E-05  -1.550319E-05   2.042583E-06  -6.906472E-05  -4.415573E-06   3.781906E-05   1.628704E-05
  a    80  -9.446264E-06  -5.219958E-06   6.830056E-06   1.689541E-05   2.386801E-05   2.389777E-06  -1.145667E-05  -2.523391E-05
  a    81   3.191776E-05   3.869058E-05  -1.892347E-06  -1.113055E-06  -6.026797E-05  -5.657139E-06   5.821750E-05  -6.237989E-05
  a    82  -5.767313E-05  -2.212226E-05  -2.617973E-07   6.053212E-06   6.501383E-05   4.111310E-07  -4.092689E-05  -5.308514E-05
  a    83  -1.871283E-05   1.292148E-05   4.187437E-07  -2.230384E-05  -1.943681E-05   8.273203E-07  -2.406858E-05   7.824563E-06
  a    84   3.536511E-05   1.753915E-05  -1.157389E-06  -8.948076E-06   8.242945E-07   2.603807E-06  -4.979275E-07   2.066506E-07
  a    85  -7.392201E-06  -1.688403E-06   8.359220E-07   2.506595E-06   4.894416E-06   1.103480E-06  -1.088815E-05  -1.701001E-06
  a    86   3.510876E-05   4.132943E-05  -2.508386E-06  -1.056759E-05  -1.604628E-05  -3.670178E-07   7.809235E-06   4.768925E-06

               a    57        a    58        a    59        a    60        a    61        a    62        a    63        a    64
  a    13  -2.834986E-03   3.149943E-05  -1.763601E-04  -9.050369E-04   1.658677E-03   6.134909E-04  -2.104978E-03   1.879190E-04
  a    14  -1.902748E-04   7.349148E-05   1.785096E-04   1.861450E-03  -1.236428E-03  -4.518525E-04   8.435406E-04   3.250824E-04
  a    15   2.761796E-03   1.114246E-04   2.675430E-04   2.687488E-03  -2.000748E-03  -3.429379E-04   1.225902E-04   1.054995E-04
  a    16  -5.931071E-04   8.182280E-05   4.840632E-05   4.218247E-04   1.219923E-03   2.586535E-04   8.273399E-04  -5.356955E-04
  a    17  -6.507482E-04  -9.308004E-06   4.965315E-05  -1.853677E-03  -6.123949E-04  -4.915589E-04   3.559028E-03  -8.930677E-04
  a    18  -1.395405E-03   6.659818E-05   1.963513E-04   4.924827E-03   1.290644E-03   1.034020E-04  -3.583713E-03   1.654867E-03
  a    19   3.321582E-03   1.200774E-04   1.515870E-04   4.030526E-03   1.684338E-03   8.283327E-04  -2.374589E-03  -4.890573E-04
  a    20  -5.651167E-04   2.033450E-04   1.271065E-04   3.134400E-03   8.021995E-04   5.732246E-04  -2.542177E-03   6.306735E-04
  a    21   4.866725E-03  -2.093224E-04   1.095190E-04   2.941383E-03   4.913398E-03   7.882109E-04  -1.549180E-03  -4.342261E-04
  a    22  -1.475150E-03  -2.159351E-04  -1.541834E-04  -3.145660E-03   1.790576E-03  -3.107859E-04   2.694159E-03   8.612157E-04
  a    23   3.400255E-04  -2.801760E-05   8.090409E-07   8.112414E-04   1.455263E-03   3.162157E-04  -1.397455E-03   6.355286E-04
  a    24   3.219066E-03  -1.334896E-05   7.348873E-05   6.272355E-03   3.657858E-03   9.538167E-04  -3.821895E-03  -1.280078E-04
  a    25  -5.778951E-03   2.178539E-04  -1.347384E-04  -3.368710E-03  -1.096377E-03  -1.432369E-04   2.021743E-03  -4.315096E-04
  a    26  -2.610355E-03  -2.087741E-04  -4.307935E-04  -6.353179E-03  -2.307351E-03  -3.178374E-04  -2.951400E-04   4.700870E-04
  a    27   8.512937E-04   9.370190E-04   1.689465E-03  -4.297746E-04  -8.385000E-04  -6.626663E-04   1.145107E-03   7.126819E-04
  a    28  -5.713923E-03  -4.371650E-04  -7.952690E-04  -9.234157E-03   8.817678E-03   2.441958E-03  -1.185413E-02   3.711198E-03
  a    29   4.107220E-05  -9.621087E-04   2.403826E-03  -1.438752E-04  -2.432718E-05   3.183395E-04  -1.928183E-05   1.324665E-04
  a    30   4.431637E-05   3.438063E-03   9.395719E-03  -8.962903E-04   2.966526E-04  -4.627341E-04  -1.404331E-04   6.276837E-04
  a    31   3.498483E-05  -1.527231E-03   4.545840E-04   7.738282E-05  -4.665246E-04   3.656206E-03   6.187769E-04   6.001388E-04
  a    32   1.013175E-04   2.868547E-03   1.394684E-03  -2.705966E-04   1.926894E-04  -5.762777E-04  -8.117457E-05  -2.027354E-05
  a    33  -1.190235E-04  -7.101637E-05  -2.680604E-05   3.584425E-04   9.467157E-04   2.016418E-04  -2.422884E-04  -9.459927E-05
  a    34  -4.912568E-05  -1.359867E-03  -1.221314E-03   2.432680E-04  -6.077827E-04   4.393515E-03   5.860044E-04   4.597393E-04
  a    35   4.960959E-04   1.337575E-02   1.979878E-02  -2.226975E-03   2.626803E-03  -1.703125E-02  -2.825705E-03  -2.679588E-03
  a    36  -7.474819E-06  -7.029073E-04   2.191236E-04  -1.351791E-06   9.388334E-05  -9.561899E-04  -1.290097E-04  -6.447740E-05
  a    37   2.756693E-04  -9.339566E-06  -1.273668E-06   2.858570E-04  -1.843625E-04  -3.523107E-05   2.345634E-04  -3.329514E-05
  a    38   1.239090E-04   5.383591E-05   8.436903E-05   2.882881E-04   8.531408E-05  -7.366820E-05   8.338785E-06  -4.507489E-05
  a    39  -2.181611E-04   2.743862E-05   1.239165E-05   1.983723E-04  -6.528286E-05   2.761497E-05  -6.256640E-05  -3.574050E-05
  a    40  -8.700520E-05   2.258129E-05   3.021592E-05  -2.683882E-04  -8.127181E-05  -6.377234E-05  -2.764747E-05   6.229478E-05
  a    41  -8.762774E-05   8.941703E-05   1.294362E-04  -9.997508E-05  -8.601273E-05  -1.397291E-04   1.248335E-04  -5.487128E-05
  a    42   2.553230E-04   1.735728E-04   2.516469E-04   2.869924E-04  -1.960318E-04  -2.564463E-04   7.200215E-05  -5.098822E-05
  a    43   8.360337E-04   2.735421E-05   7.302581E-05   3.757067E-04  -2.157279E-04  -9.339302E-05   2.378718E-04  -6.733496E-05
  a    44   3.903194E-04   3.007434E-05   5.914533E-05   1.079686E-03  -3.759746E-04  -7.743512E-05   3.219931E-04  -5.461606E-05
  a    45  -9.585641E-04   1.386451E-05   7.818322E-07  -5.477521E-04   1.007919E-04  -1.778121E-05  -2.306842E-04   8.353313E-05
  a    46  -7.401677E-04  -9.143286E-06  -5.741085E-05  -5.394965E-04  -1.693306E-04   2.313265E-05  -4.466062E-04   1.986138E-04
  a    47   9.603079E-05   1.421451E-05   5.531078E-06   3.096034E-04  -2.694110E-04  -3.692735E-05   1.025468E-04  -2.107520E-05
  a    48  -3.567066E-05  -7.777351E-06  -2.627878E-05  -4.863809E-05   1.958058E-05   1.216335E-05   5.793287E-05  -6.744779E-05
  a    49   1.579892E-04  -1.932960E-05  -2.278125E-05  -8.213864E-05  -5.473659E-05  -9.715796E-06   1.674620E-04  -5.700738E-05
  a    50  -1.583211E-05  -1.653862E-05  -4.912744E-05  -4.116252E-05  -2.416074E-04   1.918736E-07   6.972872E-05   4.275495E-05
  a    51  -1.022294E-05  -2.214590E-04  -5.572065E-04   5.589315E-05  -3.883717E-05   3.398081E-04   5.694273E-05   5.533388E-05
  a    52   3.268316E-04  -3.114745E-06   1.614110E-05   1.223582E-04   1.710079E-05  -1.935145E-05   1.302528E-04  -3.764700E-05
  a    53   1.864915E-04   8.350810E-06   1.813227E-05   6.481552E-05   2.478493E-04   2.919089E-05  -1.371751E-04  -1.398054E-05
  a    54   2.193648E-05  -5.829544E-05  -1.133747E-04   2.795832E-05   2.049721E-05  -2.223246E-04  -1.336071E-05  -5.352855E-06
  a    55  -2.940290E-04  -2.427373E-05  -4.834615E-05  -3.555553E-04   2.617993E-05   3.232040E-06  -4.869326E-06   1.567227E-05
  a    56  -1.014830E-05  -7.621382E-06  -6.103672E-06   4.476675E-05  -4.377390E-05  -2.564572E-05   2.311089E-04  -3.909512E-05
  a    57   1.012481E-03   2.276902E-05   4.589721E-05   4.981063E-04  -8.313264E-05  -5.585804E-05   3.150943E-04  -1.065764E-04
  a    58   2.276902E-05   8.434157E-04   6.320157E-05  -5.681752E-06   5.259433E-05  -3.977432E-04  -4.923265E-05  -4.449668E-05
  a    59   4.589721E-05   6.320157E-05   9.693576E-04  -2.578303E-06   1.992654E-05  -3.206396E-04  -4.489617E-05  -7.306547E-05
  a    60   4.981063E-04  -5.681752E-06  -2.578303E-06   1.033532E-03  -2.437178E-04  -2.803085E-06   1.224343E-04  -2.078771E-05
  a    61  -8.313264E-05   5.259433E-05   1.992654E-05  -2.437178E-04   4.888124E-04  -9.400174E-06  -1.817637E-04  -3.472591E-06
  a    62  -5.585804E-05  -3.977432E-04  -3.206396E-04  -2.803085E-06  -9.400174E-06   6.822554E-04   2.020242E-05   1.047696E-04
  a    63   3.150943E-04  -4.923265E-05  -4.489617E-05   1.224343E-04  -1.817637E-04   2.020242E-05   4.891629E-04  -1.123004E-04
  a    64  -1.065764E-04  -4.449668E-05  -7.306547E-05  -2.078771E-05  -3.472591E-06   1.047696E-04  -1.123004E-04   7.414839E-05
  a    65   8.123719E-05  -1.040569E-05  -6.617413E-06  -5.711431E-05  -1.651923E-05  -6.581107E-06   1.272605E-04  -5.840009E-05
  a    66   1.672824E-05   7.054886E-07   2.725359E-07   5.534031E-05   7.793601E-05   1.618464E-05  -5.811440E-05   1.910711E-05
  a    67   3.310574E-05  -6.394875E-06  -2.910467E-06   9.928578E-05   2.583343E-05   1.661561E-05   3.238985E-06  -6.472931E-06
  a    68   1.237428E-05  -8.582332E-05  -1.858819E-04   7.818707E-05  -6.499254E-05   1.934279E-04   4.773236E-05   4.227415E-05
  a    69  -7.029148E-06  -8.293244E-05  -1.882033E-04  -2.119958E-05   4.737923E-07   2.089375E-04   2.742066E-05   4.239951E-05
  a    70   2.126115E-05  -6.477640E-05  -6.034989E-05   1.663838E-05  -3.064708E-06   6.253687E-05   2.298280E-05   7.841215E-06
  a    71  -5.534411E-05   2.000687E-05   3.975430E-05   1.093858E-05  -1.861355E-05  -3.676133E-05   2.309139E-05  -2.700864E-05
  a    72   1.276256E-05   1.474535E-05   2.491959E-05   9.513001E-05  -1.009482E-04  -3.154084E-05   3.141177E-05  -1.436564E-05
  a    73  -5.239312E-06  -2.334293E-04  -5.687465E-04   6.096868E-05  -6.082254E-05   4.134708E-04   8.290142E-05   8.859157E-05
  a    74  -1.701730E-04  -1.826256E-05  -3.734827E-05  -1.431629E-04  -2.393107E-05   2.801189E-05  -4.449201E-05   3.851083E-05
  a    75  -1.168868E-04  -1.358115E-06  -6.619926E-06  -1.075525E-04   4.204147E-05   7.852582E-06  -7.537756E-05   3.250653E-05
  a    76  -1.058414E-05  -5.325198E-07   4.610929E-07  -5.864313E-05   4.315658E-05   7.726360E-06  -6.636692E-05   1.779001E-05
  a    77  -9.282456E-05  -2.711049E-06  -8.314481E-06  -5.936338E-05  -3.291247E-05  -3.416992E-06   4.786271E-07   1.232246E-05
  a    78   1.072101E-04   5.561994E-06   2.480263E-05   4.360996E-05   5.635786E-05  -1.242571E-05   4.740452E-05  -3.766599E-05
  a    79  -2.768026E-05   9.730134E-06   2.085415E-05  -6.583696E-07  -4.644398E-05  -2.648024E-05   1.344122E-05   6.549369E-07
  a    80   2.551727E-05  -1.693753E-06  -7.102556E-06   4.889369E-05  -8.518770E-06   7.045555E-06   1.645840E-05  -6.280033E-06
  a    81  -6.114164E-05  -1.363865E-06  -5.805087E-06  -7.545257E-05  -1.394411E-05  -9.423668E-08  -1.666178E-05   1.158145E-05
  a    82   6.657180E-06   3.672949E-06   1.771185E-06   6.012218E-05  -4.209285E-06   4.553421E-06  -4.279029E-05   1.110663E-05
  a    83  -1.164029E-05   1.370216E-06   6.253381E-07   2.432684E-05  -8.922597E-06   9.103435E-08   1.442727E-06   1.947547E-06
  a    84   3.490545E-05  -2.087126E-06  -1.369109E-07  -1.708630E-05  -1.942702E-06  -9.273271E-07   6.620789E-06   2.263568E-06
  a    85  -2.882075E-05  -4.743001E-07   5.298431E-07   2.010774E-05  -5.468354E-07   1.992704E-06  -9.273972E-06   3.796922E-07
  a    86  -1.088126E-05   2.916610E-07  -1.104661E-06  -1.893542E-05  -1.105964E-05   9.009925E-07  -2.213804E-05   1.345513E-05

               a    65        a    66        a    67        a    68        a    69        a    70        a    71        a    72
  a    13  -5.358806E-05  -3.251767E-05  -2.513264E-04  -3.503789E-04   3.035011E-04   1.991525E-04   1.169742E-03   1.441647E-03
  a    14   2.935185E-05   5.681963E-04  -3.463555E-04   5.276812E-04  -4.965441E-04  -5.323885E-04  -1.443534E-03   1.461439E-03
  a    15  -2.907516E-04   6.267115E-04  -3.277293E-04  -1.271291E-05   7.237146E-05   1.739006E-04   2.189429E-06   2.243420E-04
  a    16  -1.144999E-03   1.034845E-03   7.229565E-04  -5.408544E-04   4.446421E-04   1.055156E-04  -4.447589E-04   8.755070E-05
  a    17   8.611905E-04  -1.018979E-03   2.384895E-05  -3.879076E-04   2.757262E-04  -8.605466E-05  -3.397205E-04   8.804840E-05
  a    18  -4.352416E-04  -6.887571E-04   1.690832E-03   5.419628E-04  -6.297829E-04  -5.813126E-04   4.634670E-04   2.621991E-03
  a    19  -2.403676E-03   7.657776E-04   1.946809E-03  -3.192385E-04   3.633913E-04   4.587496E-04  -3.846374E-04  -8.048301E-04
  a    20  -1.906415E-03   3.832168E-03  -1.081182E-03   2.959268E-05   5.020078E-05   2.636527E-04   1.003330E-03  -4.265799E-04
  a    21  -7.325906E-04   1.010637E-04   2.911858E-03  -4.169182E-04   3.588499E-04   1.815079E-04  -1.227638E-03  -3.259123E-03
  a    22  -1.110040E-03   1.982113E-03  -8.818123E-04   2.567635E-04  -2.815297E-05   3.298633E-04  -1.960106E-03  -4.379296E-04
  a    23  -3.432361E-03  -6.880103E-04   1.821269E-03   1.274264E-03  -8.823746E-04  -9.475281E-05   4.689229E-04  -3.494231E-03
  a    24   3.199392E-03   1.612816E-03   3.272440E-03   2.383458E-04  -1.937125E-04  -1.650337E-04  -3.227870E-04   5.857074E-04
  a    25  -1.996527E-03  -5.201258E-04   7.843973E-04  -1.561562E-03   1.172134E-03   1.207446E-04  -3.474938E-04  -2.295475E-03
  a    26   1.411466E-05  -1.644151E-03  -2.263239E-03  -2.586536E-04   9.182107E-05  -2.416022E-04   8.125419E-04  -3.198829E-04
  a    27   9.317782E-04  -9.692731E-04  -6.734083E-04  -2.386925E-05  -3.456154E-04  -8.334052E-04   1.072488E-03   6.617118E-04
  a    28  -3.405512E-03   3.191333E-03   9.137804E-04  -9.668027E-04   5.445593E-04  -8.879327E-04  -3.355000E-03  -6.045661E-03
  a    29   3.769169E-05  -1.372657E-05  -1.589557E-05  -5.770335E-04  -7.594552E-04   7.603359E-05   8.759894E-05   4.659969E-05
  a    30   2.123301E-04  -4.938823E-05   4.040931E-04   2.120153E-03   1.207629E-03   2.459829E-03  -4.063330E-04  -7.549212E-05
  a    31   5.289852E-05   1.958345E-06   4.834086E-05   8.795867E-04   5.053597E-04   3.377175E-04  -2.839470E-04  -1.121245E-04
  a    32  -1.159460E-05   2.334052E-05   5.660296E-05   2.328806E-03   3.147198E-03  -7.024332E-04   3.944310E-05   2.601482E-06
  a    33  -6.824796E-04   6.743004E-04   1.098063E-03  -9.407258E-05   3.213485E-05   3.699740E-05   3.375453E-04  -8.459763E-04
  a    34   2.678611E-05  -4.250579E-06   4.012107E-05   9.301162E-04   1.066591E-03   2.467728E-04  -8.632864E-05  -4.172920E-05
  a    35  -2.544118E-04  -1.930786E-05  -3.322034E-04  -7.398532E-03  -7.804772E-03  -1.826385E-03   1.103731E-03   5.475725E-04
  a    36  -3.426704E-06   2.668801E-06  -2.072767E-05  -4.256799E-04  -5.285030E-04  -1.176762E-05  -1.325140E-05  -1.001694E-05
  a    37   1.442581E-05   6.361329E-05  -7.515467E-05   4.483556E-05  -8.493736E-06   2.264597E-05  -6.992247E-06  -1.117696E-04
  a    38   8.963109E-05   2.444820E-05  -1.100090E-04  -8.902606E-06  -4.641825E-05   1.056680E-05   3.572275E-05   1.842975E-04
  a    39   1.402251E-04   9.089335E-06  -1.000907E-04   3.166590E-05  -2.359691E-05  -3.776219E-06   7.549193E-05   1.763656E-04
  a    40  -6.103786E-05   9.940733E-05  -1.405171E-04  -1.736982E-06  -3.974315E-05  -1.702261E-05   4.536091E-05  -8.013646E-05
  a    41  -1.057297E-04  -1.582358E-04   1.129405E-04  -7.150325E-05  -5.924986E-05  -3.394332E-05   5.545759E-05   1.663084E-04
  a    42   3.187453E-05   7.108513E-05  -2.236640E-05  -6.757024E-05  -1.313566E-04  -3.748751E-05  -2.907736E-05  -9.739363E-05
  a    43  -3.901666E-05  -1.349809E-04   1.208825E-05   2.658823E-05  -4.838637E-05  -2.086779E-06  -9.520826E-05  -1.463976E-04
  a    44  -9.994233E-05   6.668809E-05   1.128557E-04   8.301173E-05  -3.149218E-05   1.157269E-05   8.651652E-06   7.193868E-05
  a    45   7.665939E-05  -1.298050E-04  -1.000090E-04   9.741855E-06  -4.646558E-05  -2.671000E-05   1.039739E-04   1.198595E-04
  a    46  -7.624270E-05  -1.808448E-05  -9.349531E-05  -8.456068E-06  -5.886187E-06  -5.989230E-05  -8.531973E-05   2.736028E-05
  a    47  -1.093537E-04  -8.935702E-05   5.118795E-05   2.539809E-05  -4.131882E-06   1.480316E-05   4.995366E-05   2.529199E-05
  a    48   3.934900E-05  -8.801529E-05   3.487370E-05   2.836359E-06   1.408797E-05   1.661494E-05   5.782031E-05  -3.124466E-05
  a    49   1.771748E-04  -2.680353E-05  -7.743953E-06  -2.082561E-06   1.362064E-05  -2.388474E-06   1.862839E-05   1.115931E-04
  a    50  -1.137636E-04  -2.517809E-05  -5.319793E-05   2.216090E-05   2.052549E-05  -1.096129E-05  -4.137942E-05  -1.545031E-04
  a    51   1.253300E-05   1.478947E-06   1.086512E-05   1.794781E-04   1.897585E-04   4.356034E-05  -2.387660E-05  -3.877617E-06
  a    52   3.315822E-05   4.909249E-05   9.646363E-05  -3.486389E-06  -5.060837E-06   1.008057E-05   8.908143E-06   3.425793E-05
  a    53  -5.875142E-05   8.492681E-05   9.072949E-05  -1.237529E-05  -9.409936E-06   1.073287E-05  -3.535181E-05  -7.586141E-05
  a    54  -7.071962E-07   7.731280E-06   1.304390E-05   5.051104E-05   1.556700E-05   5.790504E-05  -2.303252E-05  -7.024941E-06
  a    55   6.241052E-05  -1.202191E-04  -7.948006E-05   2.607128E-05  -4.027984E-06  -7.059484E-06   1.531288E-05  -4.849447E-05
  a    56   4.547527E-05   6.682452E-05   2.659837E-05   1.924224E-05  -2.905313E-06  -9.341447E-06  -1.192659E-05  -2.821597E-07
  a    57   8.123719E-05   1.672824E-05   3.310574E-05   1.237428E-05  -7.029148E-06   2.126115E-05  -5.534411E-05   1.276256E-05
  a    58  -1.040569E-05   7.054886E-07  -6.394875E-06  -8.582332E-05  -8.293244E-05  -6.477640E-05   2.000687E-05   1.474535E-05
  a    59  -6.617413E-06   2.725359E-07  -2.910467E-06  -1.858819E-04  -1.882033E-04  -6.034989E-05   3.975430E-05   2.491959E-05
  a    60  -5.711431E-05   5.534031E-05   9.928578E-05   7.818707E-05  -2.119958E-05   1.663838E-05   1.093858E-05   9.513001E-05
  a    61  -1.651923E-05   7.793601E-05   2.583343E-05  -6.499254E-05   4.737923E-07  -3.064708E-06  -1.861355E-05  -1.009482E-04
  a    62  -6.581107E-06   1.618464E-05   1.661561E-05   1.934279E-04   2.089375E-04   6.253687E-05  -3.676133E-05  -3.154084E-05
  a    63   1.272605E-04  -5.811440E-05   3.238985E-06   4.773236E-05   2.742066E-05   2.298280E-05   2.309139E-05   3.141177E-05
  a    64  -5.840009E-05   1.910711E-05  -6.472931E-06   4.227415E-05   4.239951E-05   7.841215E-06  -2.700864E-05  -1.436564E-05
  a    65   2.310838E-04  -5.369109E-06  -5.034891E-05   8.910028E-06   2.603017E-06   8.448881E-06   3.829081E-05   8.046645E-05
  a    66  -5.369109E-06   1.546033E-04  -8.276524E-06   1.885053E-06   1.511447E-06   2.057258E-06  -1.362997E-05  -5.887801E-05
  a    67  -5.034891E-05  -8.276524E-06   1.301748E-04   8.249363E-06   1.723990E-05   2.619756E-06  -6.680636E-06  -3.401347E-05
  a    68   8.910028E-06   1.885053E-06   8.249363E-06   2.788832E-04   2.893697E-04   3.300520E-05  -1.847952E-05  -2.350203E-06
  a    69   2.603017E-06   1.511447E-06   1.723990E-05   2.893697E-04   3.649702E-04   2.135256E-05  -1.806101E-05  -1.421304E-05
  a    70   8.448881E-06   2.057258E-06   2.619756E-06   3.300520E-05   2.135256E-05   6.121882E-05  -3.286616E-06   2.579799E-06
  a    71   3.829081E-05  -1.362997E-05  -6.680636E-06  -1.847952E-05  -1.806101E-05  -3.286616E-06   8.573242E-05   6.448433E-05
  a    72   8.046645E-05  -5.887801E-05  -3.401347E-05  -2.350203E-06  -1.421304E-05   2.579799E-06   6.448433E-05   2.645431E-04
  a    73   1.657861E-05  -2.444080E-06   2.063595E-05   2.892146E-04   2.743542E-04   1.145260E-04  -4.384001E-05  -1.185125E-05
  a    74   7.352904E-06  -9.940759E-06  -4.110560E-05   1.729512E-05   2.235664E-05  -8.379254E-06  -2.477806E-06   9.598211E-08
  a    75  -2.769249E-05   3.939995E-05  -2.931389E-05  -9.879675E-06   1.522370E-06  -9.613581E-06  -3.840631E-05  -2.053880E-05
  a    76  -6.039387E-05   1.963784E-05  -2.315880E-08  -1.088307E-05   9.133213E-07  -4.725496E-06  -3.062318E-05  -8.050993E-05
  a    77  -7.738520E-06  -3.547433E-06  -7.598390E-06   5.303777E-06  -2.114498E-06  -1.213082E-07  -5.217374E-06  -1.115765E-05
  a    78   2.369834E-05   2.317289E-05   1.555465E-05  -1.908139E-05  -6.534770E-06   6.618082E-06   3.748259E-06   6.795612E-06
  a    79  -7.450812E-06  -2.719700E-05  -4.109253E-06  -1.058931E-05  -1.349280E-05  -7.063263E-06   3.814353E-06   2.643755E-05
  a    80  -2.867338E-05   2.889316E-06   2.243259E-05   5.586914E-06   8.860798E-06   7.974688E-06  -6.756490E-06  -1.223184E-05
  a    81  -5.645420E-06  -8.702702E-06   3.536951E-06  -3.946291E-06   9.097252E-08  -6.057469E-06   3.404855E-07  -1.274296E-05
  a    82  -2.779748E-05   2.499263E-05   1.208750E-05   4.258453E-06  -3.164200E-06   1.233499E-06  -9.206384E-06  -6.682152E-06
  a    83  -1.232880E-05  -1.029263E-05  -8.100358E-06  -3.872988E-06   3.809125E-06   2.701302E-06   4.002464E-06   3.441109E-05
  a    84  -9.560616E-06   6.936080E-06  -5.155724E-06  -3.727000E-06   5.244347E-06   1.742319E-06  -1.727965E-05  -5.623180E-06
  a    85   3.225409E-06   5.862593E-06   3.825123E-06   4.103168E-06  -1.479186E-06   4.776918E-07   6.145270E-06  -5.408836E-06
  a    86  -1.638034E-05   2.204809E-05  -1.416491E-05  -2.657214E-06   1.310892E-06  -2.598151E-06  -1.611884E-05  -2.352674E-05

               a    73        a    74        a    75        a    76        a    77        a    78        a    79        a    80
  a    13   1.309953E-04   4.293260E-04   1.806693E-03  -2.504709E-04   9.876117E-04   1.047210E-03  -9.408669E-04  -1.364262E-05
  a    14  -1.113943E-04   1.890477E-03   1.477927E-03  -9.739589E-04  -1.101996E-03  -4.517450E-04   2.726436E-04  -8.020910E-04
  a    15   2.405392E-05   1.348212E-04   1.863353E-03   3.718494E-04  -1.121963E-03   1.152819E-03   3.685862E-04   2.485106E-04
  a    16   7.871175E-05  -1.332846E-04   1.891796E-03   3.114628E-04  -4.579304E-04   2.275732E-03  -1.281345E-04   1.491437E-03
  a    17   4.949574E-05   1.141888E-03   7.030269E-04  -2.270956E-04   2.322719E-04   4.590775E-04   1.766678E-03   5.829691E-04
  a    18  -3.358534E-05   1.923000E-03   1.473599E-03  -2.696571E-03   2.594578E-04  -1.293569E-03  -8.848905E-04  -1.800990E-03
  a    19   9.049048E-05  -3.640220E-03  -7.619954E-04   6.546734E-04  -3.481438E-04   2.831145E-03  -2.827893E-03   3.084488E-04
  a    20   6.828756E-05  -7.584892E-04   5.233431E-04   6.902389E-04  -1.818161E-03   1.587869E-03  -1.403898E-03   1.106793E-03
  a    21  -9.484578E-05  -3.004047E-03  -3.041029E-03   2.016627E-04  -2.146576E-03  -4.062285E-04  -7.436510E-04   1.336145E-03
  a    22  -8.970137E-05  -2.056344E-03   1.456481E-04   1.034747E-03  -5.117800E-04   3.207534E-04  -3.189912E-04  -8.909226E-05
  a    23  -3.824303E-05  -2.325852E-03  -1.107724E-03   1.833003E-03  -1.627817E-03   1.439396E-03  -5.995662E-04   5.663557E-04
  a    24   2.246108E-05  -1.073332E-03  -8.252576E-04   1.116760E-04  -1.219241E-03   2.025962E-04   4.455029E-04   4.548498E-04
  a    25   3.028922E-05   9.419552E-04   1.817924E-03   1.688212E-03  -1.272500E-03   9.095073E-04  -4.695929E-04   1.248094E-03
  a    26  -7.541015E-05   3.044775E-03   2.822205E-04   4.847658E-04  -5.341270E-04  -2.007915E-03   1.299154E-03   6.818183E-05
  a    27   4.247678E-06   2.583969E-04   5.612999E-05  -6.200122E-04   2.893262E-04  -4.310045E-04   4.457088E-05   4.104062E-04
  a    28  -5.211460E-04   2.537323E-03   2.943258E-03   4.625029E-03  -1.491271E-03  -1.171880E-03  -1.802538E-03  -1.424791E-03
  a    29  -1.388857E-03  -4.272406E-05   1.266068E-05   7.998795E-06   6.670588E-06   5.546506E-05   1.842658E-05  -1.658321E-05
  a    30   4.357710E-03   4.589162E-04   1.567453E-05  -4.664925E-05   6.789579E-06  -2.828035E-04  -1.568889E-04   1.279397E-04
  a    31   4.338706E-03   9.745376E-05   3.153592E-05  -2.331109E-05   3.119306E-05  -1.098444E-04   8.236257E-06   6.848672E-06
  a    32   2.147437E-04  -1.632972E-05   4.280549E-06  -4.317037E-05   1.188654E-05  -3.429767E-05   5.785910E-05  -1.633538E-05
  a    33  -9.308164E-06  -6.135993E-04  -9.123821E-04  -3.421892E-05  -4.560197E-05  -1.646458E-04   4.187712E-04   6.479883E-04
  a    34   9.755722E-04   1.237540E-04  -1.192920E-05  -4.025544E-05   7.581910E-06  -6.840153E-05  -5.947229E-05   2.748346E-05
  a    35  -1.585344E-02  -9.799190E-04   8.726231E-05   2.132633E-04  -1.014152E-04   6.261135E-04   5.874559E-04  -2.656946E-04
  a    36   3.590232E-04  -4.398887E-05   1.260408E-05   1.207660E-05   5.867857E-06   1.739261E-05   2.560917E-05  -1.497843E-05
  a    37   2.066568E-05  -6.463725E-05  -2.655667E-05  -1.001353E-04   2.036163E-04  -1.449023E-05   4.816908E-05   6.984349E-05
  a    38  -4.828193E-05  -6.857066E-05  -9.219083E-05   8.971701E-05  -2.744717E-04   7.029828E-05  -6.429292E-05  -6.879273E-05
  a    39  -6.119972E-06  -3.760796E-05  -9.892438E-05  -2.290405E-05  -3.493191E-05  -6.729983E-06   9.950410E-06  -2.786404E-05
  a    40  -5.533269E-05   1.388678E-04   7.225891E-05  -2.675749E-05   1.778757E-04  -1.152849E-04   3.735289E-05  -3.278634E-05
  a    41  -1.180275E-04   3.740798E-05   5.538492E-05  -7.633185E-05   4.284922E-05   3.862501E-05   2.990357E-05   1.518641E-05
  a    42  -2.177202E-04  -5.432191E-05  -2.233921E-05   4.017186E-05  -2.074285E-05   1.122350E-05   3.600371E-05   1.728671E-05
  a    43  -5.385781E-05  -1.028093E-04  -1.037723E-04   5.205480E-05  -1.032504E-04   1.702682E-05   6.549609E-06   4.396162E-05
  a    44   3.926045E-05  -1.716558E-04  -1.944207E-04  -1.482855E-05  -1.595767E-04   4.590737E-07   7.158044E-05   7.705802E-05
  a    45  -3.472471E-05   1.576816E-04  -2.249325E-05  -1.579607E-04   2.281441E-04  -2.016747E-04   1.051768E-04  -5.520103E-05
  a    46  -1.192042E-05   3.701130E-04   2.898846E-04   6.397466E-05   1.345209E-04  -2.001475E-04   5.916086E-05  -7.491952E-05
  a    47   3.128367E-05  -1.103508E-04  -1.101132E-04  -5.182897E-05   7.410765E-05  -7.885314E-06   4.527098E-05   5.017084E-05
  a    48   3.691827E-05  -8.252485E-05  -7.730607E-05  -2.221947E-05   1.550432E-05   4.795838E-05   2.652204E-06   5.287526E-05
  a    49   2.541708E-05   7.970621E-05   4.791155E-05  -4.941087E-05  -4.027510E-05   3.546129E-05   1.328304E-05  -9.446264E-06
  a    50   3.231118E-05   9.830460E-05   4.689929E-05   6.124618E-05   2.329241E-05  -6.388552E-05   2.937189E-05  -5.219958E-06
  a    51   3.933232E-04   1.749403E-05  -4.407982E-06  -8.693690E-06   1.335475E-06  -1.209215E-05  -1.550319E-05   6.830056E-06
  a    52  -6.332222E-06  -7.844646E-05  -3.006474E-05  -1.103309E-04   1.149486E-04   3.645329E-05   2.042583E-06   1.689541E-05
  a    53  -2.557084E-05  -1.440031E-04  -4.861044E-05   8.651362E-05  -9.410269E-05   6.442596E-05  -6.906472E-05   2.386801E-05
  a    54   3.196594E-04   6.784181E-06   1.178635E-06   3.876980E-06  -2.664238E-06  -5.238511E-06  -4.415573E-06   2.389777E-06
  a    55   3.020099E-05   9.255646E-05   2.513686E-06  -2.627740E-05   5.979752E-05  -8.224601E-05   3.781906E-05  -1.145667E-05
  a    56   1.852507E-05   6.322050E-05   3.506338E-05  -3.643083E-05  -9.677035E-06  -1.037238E-05   1.628704E-05  -2.523391E-05
  a    57  -5.239312E-06  -1.701730E-04  -1.168868E-04  -1.058414E-05  -9.282456E-05   1.072101E-04  -2.768026E-05   2.551727E-05
  a    58  -2.334293E-04  -1.826256E-05  -1.358115E-06  -5.325198E-07  -2.711049E-06   5.561994E-06   9.730134E-06  -1.693753E-06
  a    59  -5.687465E-04  -3.734827E-05  -6.619926E-06   4.610929E-07  -8.314481E-06   2.480263E-05   2.085415E-05  -7.102556E-06
  a    60   6.096868E-05  -1.431629E-04  -1.075525E-04  -5.864313E-05  -5.936338E-05   4.360996E-05  -6.583696E-07   4.889369E-05
  a    61  -6.082254E-05  -2.393107E-05   4.204147E-05   4.315658E-05  -3.291247E-05   5.635786E-05  -4.644398E-05  -8.518770E-06
  a    62   4.134708E-04   2.801189E-05   7.852582E-06   7.726360E-06  -3.416992E-06  -1.242571E-05  -2.648024E-05   7.045555E-06
  a    63   8.290142E-05  -4.449201E-05  -7.537756E-05  -6.636692E-05   4.786271E-07   4.740452E-05   1.344122E-05   1.645840E-05
  a    64   8.859157E-05   3.851083E-05   3.250653E-05   1.779001E-05   1.232246E-05  -3.766599E-05   6.549369E-07  -6.280033E-06
  a    65   1.657861E-05   7.352904E-06  -2.769249E-05  -6.039387E-05  -7.738520E-06   2.369834E-05  -7.450812E-06  -2.867338E-05
  a    66  -2.444080E-06  -9.940759E-06   3.939995E-05   1.963784E-05  -3.547433E-06   2.317289E-05  -2.719700E-05   2.889316E-06
  a    67   2.063595E-05  -4.110560E-05  -2.931389E-05  -2.315880E-08  -7.598390E-06   1.555465E-05  -4.109253E-06   2.243259E-05
  a    68   2.892146E-04   1.729512E-05  -9.879675E-06  -1.088307E-05   5.303777E-06  -1.908139E-05  -1.058931E-05   5.586914E-06
  a    69   2.743542E-04   2.235664E-05   1.522370E-06   9.133213E-07  -2.114498E-06  -6.534770E-06  -1.349280E-05   8.860798E-06
  a    70   1.145260E-04  -8.379254E-06  -9.613581E-06  -4.725496E-06  -1.213082E-07   6.618082E-06  -7.063263E-06   7.974688E-06
  a    71  -4.384001E-05  -2.477806E-06  -3.840631E-05  -3.062318E-05  -5.217374E-06   3.748259E-06   3.814353E-06  -6.756490E-06
  a    72  -1.185125E-05   9.598211E-08  -2.053880E-05  -8.050993E-05  -1.115765E-05   6.795612E-06   2.643755E-05  -1.223184E-05
  a    73   7.470968E-04   3.811391E-05  -6.651245E-06  -1.008244E-05   8.963350E-07  -2.399496E-05  -2.227622E-05   1.270689E-05
  a    74   3.811391E-05   1.372981E-04   8.007697E-05   6.800405E-06   1.810176E-05  -5.515153E-05   1.435593E-05  -3.781069E-05
  a    75  -6.651245E-06   8.007697E-05   1.527252E-04   5.923527E-07   3.744444E-05   1.815065E-05  -1.884288E-05  -3.765718E-06
  a    76  -1.008244E-05   6.800405E-06   5.923527E-07   1.130236E-04  -7.143065E-05  -1.819665E-05  -1.556625E-05   8.626112E-07
  a    77   8.963350E-07   1.810176E-05   3.744444E-05  -7.143065E-05   1.800781E-04  -2.524566E-05   1.240040E-05  -7.256394E-06
  a    78  -2.399496E-05  -5.515153E-05   1.815065E-05  -1.819665E-05  -2.524566E-05   9.063396E-05  -3.437432E-05   1.980036E-05
  a    79  -2.227622E-05   1.435593E-05  -1.884288E-05  -1.556625E-05   1.240040E-05  -3.437432E-05   6.243311E-05   1.668662E-05
  a    80   1.270689E-05  -3.781069E-05  -3.765718E-06   8.626112E-07  -7.256394E-06   1.980036E-05   1.668662E-05   6.051739E-05
  a    81  -2.470343E-06   3.492201E-05   2.511382E-05  -1.228512E-05   4.169085E-05  -1.450196E-05   4.525102E-06   8.942028E-07
  a    82  -2.117858E-06  -2.027499E-05  -7.011446E-06   1.880799E-05   4.401735E-06  -5.355101E-06  -3.953684E-06  -8.680343E-07
  a    83   2.839579E-06  -5.059482E-06  -1.666554E-05   6.246476E-07  -6.379884E-06  -4.960669E-07   1.504289E-05  -1.734074E-06
  a    84   6.227275E-07   5.554176E-07   9.307107E-06   6.226090E-06  -7.256587E-06   6.351342E-06   7.783129E-06   9.568996E-07
  a    85   1.439484E-06   3.091481E-07  -1.967514E-06  -2.521140E-06  -2.609434E-06   2.327949E-06  -4.881030E-06   3.119178E-06
  a    86  -2.105816E-06   1.393189E-05   2.201065E-05   5.409629E-06  -2.332330E-06   8.971699E-07   1.058993E-05   5.948384E-06

               a    81        a    82        a    83        a    84        a    85        a    86
  a    13   4.036542E-05   8.420407E-04   5.521752E-04  -1.633408E-03   1.541086E-03  -5.783986E-04
  a    14  -7.179731E-04   3.305842E-04  -7.604943E-04  -2.517396E-04   7.918547E-04   1.905485E-03
  a    15  -8.215417E-04   6.768189E-05   3.816917E-04   1.101377E-03   9.012011E-04   1.953851E-03
  a    16  -2.644592E-04  -2.234324E-04   6.677929E-04   3.195599E-05  -2.029321E-04  -3.951567E-04
  a    17   7.919988E-04  -6.146707E-04   6.521596E-04   1.358288E-03  -1.040255E-03   5.569617E-04
  a    18   2.742908E-04  -1.070483E-03  -2.441686E-03  -6.185350E-04   4.961871E-05  -6.684108E-04
  a    19  -2.510318E-03   7.859257E-04   6.685106E-04  -5.699128E-04   5.041321E-04  -4.677044E-04
  a    20   1.841694E-03   1.047054E-03   9.923382E-05  -3.837646E-05   7.919394E-04   1.135429E-03
  a    21   1.272466E-04   7.394599E-04  -1.481465E-03  -8.586809E-05  -1.665021E-04  -4.103704E-04
  a    22  -1.928786E-03   7.161055E-04   5.712976E-04   1.510379E-04   4.776774E-04  -6.081354E-04
  a    23   6.942045E-04  -1.034893E-04  -9.355781E-04   1.399878E-04   2.377288E-05  -3.671671E-04
  a    24   2.159753E-04   1.546852E-03  -7.276082E-04  -9.418785E-04   1.112374E-03  -4.189854E-04
  a    25   1.368975E-03   1.573673E-04   7.919160E-04   5.470632E-04   2.500027E-04   5.151302E-04
  a    26   4.936118E-04  -1.852520E-03  -3.214664E-04   7.818921E-05  -3.554380E-04   4.498302E-04
  a    27   1.772491E-04  -4.790993E-04   4.414295E-04  -1.812554E-04  -3.497621E-04   1.953131E-04
  a    28  -3.274384E-04   8.616770E-04  -1.083251E-03   9.429364E-04  -5.671466E-05   1.506588E-03
  a    29   8.342203E-06  -2.058732E-05   2.545523E-05  -2.593231E-05  -1.448026E-05   2.686132E-06
  a    30  -2.176057E-06  -1.565050E-05   4.268683E-05   7.408719E-05   1.841517E-05   8.204374E-06
  a    31  -1.453575E-06  -1.977280E-05  -7.830748E-06  -3.037657E-05  -2.317284E-05  -2.638381E-06
  a    32   2.031110E-06  -1.828315E-05  -2.590066E-06  -8.575380E-06   2.429979E-07   8.469385E-06
  a    33   7.704958E-05  -7.894647E-04   2.486215E-04  -2.811597E-04   5.108350E-04   1.437827E-04
  a    34   7.241005E-06  -3.713921E-06  -1.183231E-06   6.316998E-06   1.074041E-05   1.174878E-06
  a    35  -1.288346E-05   6.318633E-05  -5.113530E-05   8.591309E-06  -3.178658E-05  -1.198025E-05
  a    36   2.575257E-06  -2.723426E-06   6.107937E-06  -6.616227E-06  -6.426606E-06   5.649497E-07
  a    37  -3.894940E-05  -3.355118E-06  -3.362896E-05  -3.713716E-05   3.682311E-05   2.299265E-05
  a    38  -7.592150E-05   2.353673E-05   8.566108E-05   4.769912E-05  -3.374920E-07   8.378804E-06
  a    39  -3.952329E-05   6.526857E-05   7.063229E-05  -1.777660E-05   2.796641E-05  -1.351044E-05
  a    40   9.781218E-05   1.670610E-05  -5.588049E-05   4.193348E-06  -1.363418E-05   3.761770E-05
  a    41   1.193062E-05  -5.917119E-05  -2.168440E-07  -1.466920E-05  -5.236208E-08  -1.818072E-05
  a    42  -2.186873E-05   2.893044E-05   8.598653E-06   1.466951E-05  -8.168109E-06   2.172640E-05
  a    43  -3.372945E-06   8.823794E-06  -6.068565E-05   6.085420E-05  -4.111029E-05   3.943930E-05
  a    44  -1.448059E-04   4.006575E-05   7.506691E-05   3.554437E-05  -1.510164E-05   2.295396E-05
  a    45   6.300380E-05  -4.703242E-05   1.211572E-05  -7.548246E-05   2.950758E-05  -3.953322E-05
  a    46   1.104990E-04   5.049185E-05  -2.149160E-05  -1.473970E-06   4.050957E-05   8.287544E-05
  a    47  -1.639484E-05   4.978234E-05   4.298469E-05  -1.785284E-05   8.037768E-06  -2.887809E-05
  a    48   4.066721E-05   4.251731E-06  -1.211605E-06   4.699306E-06   5.014946E-06  -1.498855E-05
  a    49   3.191776E-05  -5.767313E-05  -1.871283E-05   3.536511E-05  -7.392201E-06   3.510876E-05
  a    50   3.869058E-05  -2.212226E-05   1.292148E-05   1.753915E-05  -1.688403E-06   4.132943E-05
  a    51  -1.892347E-06  -2.617973E-07   4.187437E-07  -1.157389E-06   8.359220E-07  -2.508386E-06
  a    52  -1.113055E-06   6.053212E-06  -2.230384E-05  -8.948076E-06   2.506595E-06  -1.056759E-05
  a    53  -6.026797E-05   6.501383E-05  -1.943681E-05   8.242945E-07   4.894416E-06  -1.604628E-05
  a    54  -5.657139E-06   4.111310E-07   8.273203E-07   2.603807E-06   1.103480E-06  -3.670178E-07
  a    55   5.821750E-05  -4.092689E-05  -2.406858E-05  -4.979275E-07  -1.088815E-05   7.809235E-06
  a    56  -6.237989E-05  -5.308514E-05   7.824563E-06   2.066506E-07  -1.701001E-06   4.768925E-06
  a    57  -6.114164E-05   6.657180E-06  -1.164029E-05   3.490545E-05  -2.882075E-05  -1.088126E-05
  a    58  -1.363865E-06   3.672949E-06   1.370216E-06  -2.087126E-06  -4.743001E-07   2.916610E-07
  a    59  -5.805087E-06   1.771185E-06   6.253381E-07  -1.369109E-07   5.298431E-07  -1.104661E-06
  a    60  -7.545257E-05   6.012218E-05   2.432684E-05  -1.708630E-05   2.010774E-05  -1.893542E-05
  a    61  -1.394411E-05  -4.209285E-06  -8.922597E-06  -1.942702E-06  -5.468354E-07  -1.105964E-05
  a    62  -9.423668E-08   4.553421E-06   9.103435E-08  -9.273271E-07   1.992704E-06   9.009925E-07
  a    63  -1.666178E-05  -4.279029E-05   1.442727E-06   6.620789E-06  -9.273972E-06  -2.213804E-05
  a    64   1.158145E-05   1.110663E-05   1.947547E-06   2.263568E-06   3.796922E-07   1.345513E-05
  a    65  -5.645420E-06  -2.779748E-05  -1.232880E-05  -9.560616E-06   3.225409E-06  -1.638034E-05
  a    66  -8.702702E-06   2.499263E-05  -1.029263E-05   6.936080E-06   5.862593E-06   2.204809E-05
  a    67   3.536951E-06   1.208750E-05  -8.100358E-06  -5.155724E-06   3.825123E-06  -1.416491E-05
  a    68  -3.946291E-06   4.258453E-06  -3.872988E-06  -3.727000E-06   4.103168E-06  -2.657214E-06
  a    69   9.097252E-08  -3.164200E-06   3.809125E-06   5.244347E-06  -1.479186E-06   1.310892E-06
  a    70  -6.057469E-06   1.233499E-06   2.701302E-06   1.742319E-06   4.776918E-07  -2.598151E-06
  a    71   3.404855E-07  -9.206384E-06   4.002464E-06  -1.727965E-05   6.145270E-06  -1.611884E-05
  a    72  -1.274296E-05  -6.682152E-06   3.441109E-05  -5.623180E-06  -5.408836E-06  -2.352674E-05
  a    73  -2.470343E-06  -2.117858E-06   2.839579E-06   6.227275E-07   1.439484E-06  -2.105816E-06
  a    74   3.492201E-05  -2.027499E-05  -5.059482E-06   5.554176E-07   3.091481E-07   1.393189E-05
  a    75   2.511382E-05  -7.011446E-06  -1.666554E-05   9.307107E-06  -1.967514E-06   2.201065E-05
  a    76  -1.228512E-05   1.880799E-05   6.246476E-07   6.226090E-06  -2.521140E-06   5.409629E-06
  a    77   4.169085E-05   4.401735E-06  -6.379884E-06  -7.256587E-06  -2.609434E-06  -2.332330E-06
  a    78  -1.450196E-05  -5.355101E-06  -4.960669E-07   6.351342E-06   2.327949E-06   8.971699E-07
  a    79   4.525102E-06  -3.953684E-06   1.504289E-05   7.783129E-06  -4.881030E-06   1.058993E-05
  a    80   8.942028E-07  -8.680343E-07  -1.734074E-06   9.568996E-07   3.119178E-06   5.948384E-06
  a    81   6.368248E-05  -8.657502E-06  -4.847207E-06   6.876047E-08  -9.396902E-08   1.854836E-06
  a    82  -8.657502E-06   5.943955E-05   5.392455E-06  -1.448392E-07   4.192003E-06   3.140678E-06
  a    83  -4.847207E-06   5.392455E-06   4.664977E-05   2.379259E-06   5.059463E-06  -1.462510E-06
  a    84   6.876047E-08  -1.448392E-07   2.379259E-06   2.915555E-05  -1.608593E-05   1.897791E-05
  a    85  -9.396902E-08   4.192003E-06   5.059463E-06  -1.608593E-05   2.588481E-05   2.653814E-06
  a    86   1.854836E-06   3.140678E-06  -1.462510E-06   1.897791E-05   2.653814E-06   4.011238E-05

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99997834     1.99988622     1.99976761     1.99972086
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.99961019     1.99935655     1.99926135     1.99882684     1.99862291     1.99829203     1.99809935     1.99759554
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     1.99667403     1.99622570     1.99401783     1.99347777     1.99321095     1.99039109     1.97757190     1.93028375
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     1.00107553     0.97983295     0.09760498     0.02696263     0.00736061     0.00545647     0.00361730     0.00272877
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00206873     0.00154410     0.00122936     0.00104232     0.00097396     0.00084240     0.00068932     0.00066964
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00063880     0.00054640     0.00049676     0.00043261     0.00039434     0.00037700     0.00035085     0.00028809
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62       MO    63       MO    64
  occ(*)=     0.00025140     0.00021805     0.00019191     0.00015924     0.00014758     0.00014091     0.00013243     0.00011671
              MO    65       MO    66       MO    67       MO    68       MO    69       MO    70       MO    71       MO    72
  occ(*)=     0.00008273     0.00007999     0.00006833     0.00005251     0.00004407     0.00004157     0.00003064     0.00002928
              MO    73       MO    74       MO    75       MO    76       MO    77       MO    78       MO    79       MO    80
  occ(*)=     0.00002684     0.00002140     0.00001671     0.00001323     0.00001069     0.00000709     0.00000623     0.00000404
              MO    81       MO    82       MO    83       MO    84       MO    85       MO    86
  occ(*)=     0.00000388     0.00000294     0.00000188     0.00000169     0.00000087     0.00000044


 total number of electrons =   66.0000000000

 accstate=                     5
 accpdens=                     5
logrecs(*)=   1   2logvrecs(*)=   1   2
 item #                     5 suffix=:.trd1to2:
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   2)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y=   34801  D0X=       0  D0W=       0
  DDZI=   17300 DDYI=   39515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1785 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   2)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y= 1246697  D0X=       0  D0W=       0
  DDZI=  203100 DDYI=  457450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  2.601020996419109E-003
 accstate=                     5
 accpdens=                     5
logrecs(*)=   2   3logvrecs(*)=   2   3
 item #                     6 suffix=:.trd2to3:
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
1e-transition density (   2,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y=   34801  D0X=       0  D0W=       0
  DDZI=   17300 DDYI=   39515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1785 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   2,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y= 1246697  D0X=       0  D0W=       0
  DDZI=  203100 DDYI=  457450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  2.973323435037353E-003
 accstate=                     5
 accpdens=                     5
logrecs(*)=   2   4logvrecs(*)=   2   4
 item #                     7 suffix=:.trd2to4:
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
1e-transition density (   2,   4)
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   4)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   4)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y=   34801  D0X=       0  D0W=       0
  DDZI=   17300 DDYI=   39515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1785 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   2,   4)
--------------------------------------------------------------------------------
  2e-transition density (root #    2 -> root #   4)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7929  DYX=       0  DYW=       0
   D0Z=   13778  D0Y= 1246697  D0X=       0  D0W=       0
  DDZI=  203100 DDYI=  457450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  0.113388436818984     
