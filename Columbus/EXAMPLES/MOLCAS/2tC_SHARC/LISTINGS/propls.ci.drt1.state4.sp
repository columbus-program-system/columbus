

     ******************************************
     **    PROGRAM:              EXPTVL      **
     **    PROGRAM VERSION:      5.5.2b      **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

        Calculation of expectation values of one-electron properties.

 This Version of Program EXPTVL   is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

 workspace allocation information: lcore=  52428800 mem1=          0 ifirst=          1

echo of the input file:
 ------------------------------------------------------------------------
  &input
   quadrup=1
   lvlprt=1,
   moment=1
   mofilen='mocoef'
   nofilen='mocoef_prop'
   seward=1
  &end
 ------------------------------------------------------------------------
Unique Atoms:   13
atom #  1 xyz=    1.090922    0.033809    3.058704
atom #  2 xyz=    2.912001    0.497299    1.229819
atom #  3 xyz=    2.110226    0.458287   -1.259693
atom #  4 xyz=   -0.264736   -0.031221   -1.778675
atom #  5 xyz=   -2.124327   -0.512129    0.101887
atom #  6 xyz=   -1.349643   -0.454574    2.515924
atom #  7 xyz=    5.874427    1.079735    2.095953
atom #  8 xyz=   -0.917004   -0.149862   -4.283784
atom #  9 xyz=    1.693287    0.081374    4.849045
atom # 10 xyz=   -2.592101   -0.789217    4.104251
atom # 11 xyz=   -4.063241   -0.922145   -0.382223
atom # 12 xyz=   -2.701375    0.290293   -4.717355
atom # 13 xyz=    0.369902    0.605417   -5.446181

  Charge of molecule-66.00
 symmetry  =   1
 slabel(*) =a   
 nbpsy(*)  =  86

################################################################################

          O U T P U T:

           Dipole moments:

                     X               Y               Z   
   orign x       0.00000000      0.00000000      0.00000000
   orign y       0.00000000      0.00000000      0.00000000
   orign z       0.00000000      0.00000000      0.00000000
   nuclear      97.72608200     15.93337000     26.96310400
   electronic  -98.09404127    -15.54760819    -26.08133062
   total        -0.36795927      0.38576181      0.88177338
   total Dipole moment =   2.61902598    Debye
