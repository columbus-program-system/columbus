License is going to expire in 128 days on Thursday April 30th, 2009
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.3 patchlevel 337          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                              ^                
                              ^^^^^^^       ^^^^ ^^^                             ^o^               
                              ^^^ ^^^       ^^^^ ^^^                            ^^^o^              
                              ^^^ ^^^^      ^^^  ^^^                           ^^^^^o^             
                              ^^^  ^^ ^^^^^  ^^  ^^^^                            ^^o               
                             ^^^^      ^^^^   ^   ^^^                           ^^o^^              
                             ^   ^^^   ^^^^   ^^^^  ^                          ^^o^^^^             
                            ^   ^^^^    ^^    ^^^^   ^                        ^^o^^^^^^            
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                   ^^^o^^^^^^^           
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                   ^o^^^              
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^              ^^^o^^^             
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^           ^^^^^o^^^            
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^          ^^^^^^^o^^^           
               ^^^^^^^^^^^^^                          ^^^      ^^^^         ^^^^^^^^^o^^^          
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^        ^^^^^^^^^^^o^^^         
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^               ^                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^               ^                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module CASPT2 with 250 MB of memory                                  
                                              at 14:03:24 Tue Dec 23 2008                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()



 ****************** WARNING ********************
  Default frozen orbitals as max of non valence 
  orbitals and orbitals frozen in the CASSCF is 
  overwritten by user input.                    
Default values:   1   0   0   0
 ***********************************************


      Wave function specifications:
      -----------------------------

      Number of closed shell electrons           0
      Number of electrons in active shells       8
      Max number of holes in RAS1 space          0
      Max number of electrons in RAS3 space      0
      Number of inactive orbitals                0
      Number of active orbitals                  5
      Number of secondary orbitals              38
      Spin quantum number                      0.0
      State symmetry                             1
      Number of configuration state fnc.         8
      Number of root(s) available                1
      Root passed to geometry opt.               1
      A file JOBMIX will be created.
      This is a CASSCF reference function


      Orbital specifications:
      -----------------------

      Symmetry species                           1   2   3   4
                                                a1  b1  a2  b2
      Frozen orbitals                            1   0   0   0
      Inactive orbitals                          0   0   0   0
      Active orbitals                            3   1   0   1
      Secondary orbitals                        16   9   4   9
      Deleted orbitals                           0   0   0   0
      Number of basis functions                 20  10   4  10


      Type of  Fock operator to use: STANDARD
      Type of HZERO operator to use: STANDARD IPEA           
      Extra imaginary denominator shift SHIFTI=      0.05000000
      The CANONICAL keyword was used in the RASSCF program.
      Therefore, input orbitals should not need to be transformed.
      The input orbitals and the CI vector will be used as they are.

--------------------------------------------------------------------------------
 Estimated memory requirements:
  POLY3 :                   100680
  MKRHS :                   104534
  SIGMA :                   121860
  DIADNS:                     4096
  PRPCTL:                   128036
 Available workspace:     32766001

********************************************************************************
 Single-state initialization phase begins for state  1
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  Single-state initialization phase finished.

********************************************************************************
  CASPT2 EQUATION SOLUTION
--------------------------------------------------------------------------------
  Total nr of CASPT2 parameters:
   Before reduction:        6282
   After  reduction:        5360

 The contributions to the second order correlation energy in atomic units.
-----------------------------------------------------------------------------------------------------------------------------
  IT.      VJTU        VJTI        ATVX        AIVX        VJAI        BVAT        BJAT        BJAI        TOTAL       RNORM  
-----------------------------------------------------------------------------------------------------------------------------
   1     0.000000    0.000000   -0.045922    0.000000    0.000000   -0.195592    0.000000    0.000000   -0.241514    0.012656
   2     0.000000    0.000000   -0.047153    0.000000    0.000000   -0.195572    0.000000    0.000000   -0.242725    0.001479
   3     0.000000    0.000000   -0.047176    0.000000    0.000000   -0.195578    0.000000    0.000000   -0.242755    0.000170
   4     0.000000    0.000000   -0.047171    0.000000    0.000000   -0.195578    0.000000    0.000000   -0.242749    0.000021
   5     0.000000    0.000000   -0.047171    0.000000    0.000000   -0.195577    0.000000    0.000000   -0.242749    0.000004
-----------------------------------------------------------------------------------------------------------------------------

  FINAL CASPT2 RESULT:

      Reference energy:        -100.0823305449
      E2 (Non-variational):      -0.2427486210
      Shift correction:          -0.0000219470
      E2 (Variational):          -0.2427705680
      Total energy:            -100.3251011129
      Residual norm:              0.0000006583
      Reference weight:           0.96109

      Contributions to the CASPT2 correlation energy
      Active & Virtual Only:         -0.2427486210
      One Inactive Excited:           0.0000000000
      Two Inactive Excited:           0.0000000000


----------------------------------------------------------------------------------------------------
 Report on small energy denominators, large coefficients, and large energy contributions.
CASE  SYMM ACTIVE-MIX  NON-ACTIVE INDICES          DENOMINATOR     RHS VALUE       COEFFICIENT     CONTRIBUTION
ATVX     2  Mu2.0002  Se2.002                       3.14557377     -0.13005883      0.04167858     -0.00542067
ATVX     4  Mu4.0002  Se4.002                       3.14557341     -0.13005826      0.04167841     -0.00542062
BVATM    3  Mu3.0001  Se3.001 Se1.009               5.74603316     -0.17923408      0.03119030     -0.00559037
BVATM    3  Mu3.0001  Se4.002 Se2.002               3.00577097     -0.08106376      0.02696190     -0.00218563
BVATM    3  Mu3.0001  Se4.003 Se2.003               3.26754711     -0.09076475      0.02777115     -0.00252064

********************************************************************************
  CASPT2 PROPERTY SECTION
--------------------------------------------------------------------------------


      Mulliken population Analysis:
      -----------------------------

      Mulliken charges per center and basis function type
      ---------------------------------------------------

               H       F   
      1s     1.0897  1.9987
      2s     0.0000  1.9012
      2px    0.0000  1.9050
      2pz    0.0000  1.5475
      2py    0.0000  1.9050
      *s    -0.5176  0.0016
      *px    0.0352  0.0540
      *pz    0.0243 -0.0782
      *py    0.0352  0.0540
      *d2+   0.0001  0.0044
      *d1+   0.0043  0.0052
      *d0    0.0019  0.0118
      *d1-   0.0043  0.0052
      *d2-   0.0001  0.0044
      *f3+   0.0000  0.0003
      *f2+   0.0000  0.0003
      *f1+   0.0000  0.0003
      *f0    0.0000  0.0007
      *f1-   0.0000  0.0003
      *f2-   0.0000  0.0003
      *f3-   0.0000  0.0003
      Total  0.6777  9.3223

      N-E    0.3223 -0.3223

      Total electronic charge=   10.000000

      Total            charge=    0.000000

      Expectation values of various properties:
      -----------------------------------------
Dipole Moment (Debye):                                                          
Origin of the operator (Ang)=    0.0000    0.0000    0.0000
               X=    0.0000               Y=    0.0000               Z=   -1.8184           Total=    1.8184
Quadrupole Moment (Debye*Ang):                                                  
Origin of the operator (Ang)=    0.0000    0.0000    0.8663
              XX=   -5.5717              XY=    0.0000              XZ=    0.0000              YY=   -5.5717
              YZ=    0.0000              ZZ=   -3.4053
In traceless form (Debye*Ang)
              XX=   -1.0832              XY=    0.0000              XZ=    0.0000              YY=   -1.0832
              YZ=    0.0000              ZZ=    2.1664

********************************************************************************
 CASPT2 TIMING INFORMATION: CPU(s), I/O(s).
  Inizialization             0.03    0.02
  CASPT2 equations           0.05    0.07
  Properties                 0.08    0.17
  Gradient/MS coupling       0.00    0.00
 Total time                  0.16    0.26

  A NEW JOBIPH FILE NAMED 'JOBMIX' IS PREPARED.
********************************************************************************

  The CI coefficients for the MIXED state nr.   1
--------------------------------------------------------------------------------
 CI COEFFICIENTS LARGER THAN 0.50D-01
  Occupation of active orbitals, and spin coupling
  of open shells. (u,d: Spin up or down).
   ConfOccupation       Coef       Weight                                        
      3   220 2 2          0.994925         0.989875
      6   202 2 2         -0.088953         0.007913

--- Stop Module: caspt2 at Tue Dec 23 14:03:24 2008 /rc=0 ---

     Happy landing!

