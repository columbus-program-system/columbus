License is going to expire in 128 days on Thursday April 30th, 2009
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.3 patchlevel 337          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                              ^                
                              ^^^^^^^       ^^^^ ^^^                             ^o^               
                              ^^^ ^^^       ^^^^ ^^^                            ^^^o^              
                              ^^^ ^^^^      ^^^  ^^^                           ^^^^^o^             
                              ^^^  ^^ ^^^^^  ^^  ^^^^                            ^^o               
                             ^^^^      ^^^^   ^   ^^^                           ^^o^^              
                             ^   ^^^   ^^^^   ^^^^  ^                          ^^o^^^^             
                            ^   ^^^^    ^^    ^^^^   ^                        ^^o^^^^^^            
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                   ^^^o^^^^^^^           
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                   ^o^^^              
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^              ^^^o^^^             
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^           ^^^^^o^^^            
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^          ^^^^^^^o^^^           
               ^^^^^^^^^^^^^                          ^^^      ^^^^         ^^^^^^^^^o^^^          
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^        ^^^^^^^^^^^o^^^         
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^               ^                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^               ^                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module RASSCF with 250 MB of memory                                  
                                              at 14:03:21 Tue Dec 23 2008                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()



      **************************************************************************************************************************
      *                                                                                                                        *
      *                                                        Project:                                                        *
      *                                                     HF valence CAS                                                     *
      *                                                                                                                        *
      **************************************************************************************************************************


      Header of the ONEINT file:
      --------------------------
      HF Monomer C2v                                                                                                          
      Integrals generated by seward 4.2.0  , Tue Dec 23 14:03:20 2008                                                         


      OrdInt status: non-squared


      Cartesian coordinates in Angstrom:
      -----------------------------------------
      No.  Label     X         Y         Z     
      -----------------------------------------
       1   H        0.00000   0.00000   0.00000
       2   F        0.00000   0.00000   0.91225
      -----------------------------------------
      Nuclear repulsion energy =    5.220720



      Wave function specifications:
      -----------------------------

      Number of closed shell electrons           2
      Number of electrons in active shells       8
      Max number of holes in RAS1 space          0
      Max nr of electrons in RAS3 space          0
      Number of inactive orbitals                1
      Number of active orbitals                  5
      Number of secondary orbitals              38
      Spin quantum number                      0.0
      State symmetry                             1


      Orbital specifications:
      -----------------------

      Symmetry species                           1   2   3   4
                                                a1  b1  a2  b2
      Frozen orbitals                            0   0   0   0
      Inactive orbitals                          1   0   0   0
      Active orbitals                            3   1   0   1
      RAS1 orbitals                              0   0   0   0
      RAS2 orbitals                              3   1   0   1
      RAS3 orbitals                              0   0   0   0
      Secondary orbitals                        16   9   4   9
      Deleted orbitals                           0   0   0   0
      Number of basis functions                 20  10   4  10


      CI expansion specifications:
      ----------------------------

      Number of configuration state fnc.         8
      Number of determinants                     8
      Number of root(s) required                 1
      Root chosen for geometry opt.              1
      CI root used                               1
      highest root included in the CI            1
      max. size of the explicit Hamiltonian      8


      Optimization specifications:
      ----------------------------

      RASSCF algorithm: Conventional
      Maximum number of macro iterations        50
      Maximum number of SX iterations           25
      Threshold for RASSCF energy            0.100E-08
      Threshold for max MO rotation          0.100E-05
      Threshold for max BLB element          0.100E-05
      Level shift parameter                  0.100E+01
      Make Quasi-Newton update
      The MO-coefficients are taken from the file INPORB
      Title:ocoef_scfpq                                                                    

      Total molecular charge    0.00

      ************************************************************************************************************************
      *                                                                                                                      *
      *                                            Wave function  control section                                            *
      *                                                                                                                      *
      ************************************************************************************************************************


                          RASSCF iterations: Energy and convergence statistics
                          ----------------------------------------------------

      Iter CI   SX   CI       RASSCF       Energy    max BLB   max BLB  max ROT   Level Ln srch  Step   QN    Time(min)
          iter iter root      energy       change    element    value    param    shift minimum  type update     CPU
      WARNING: large rotation in molecular orbital   4 of symmetry 1 MO space 2  weight is    0.116920
      WARNING: large rotation in molecular orbital   5 of symmetry 1 MO space 3  weight is    0.480146
        1   1    9    1  -100.05916945    0.00E+00    4   5 1  0.25E-02* -0.72E+00*  0.00   0.00     SX     NO      0.00
        2   1   13    1  -100.07851337   -0.19E-01*   3  14 1 -0.19E-01*  0.11E+00*  0.00   0.00     SX     NO      0.00
        3   1   11    1  -100.08172841   -0.32E-02*   3  14 1 -0.59E-02*  0.30E-01*  0.00   0.00     SX     NO      0.00
        4   1   12    1  -100.08213715   -0.41E-03*   1   4 1 -0.28E-02*  0.15E-01*  0.00   0.00     SX     NO      0.00
        5   1   11    1  -100.08225568   -0.12E-03*   1   4 1 -0.21E-02*  0.19E-01*  0.00   2.40     LS    YES      0.00
        6   1   11    1  -100.08231771   -0.62E-04*   1   4 1 -0.14E-02* -0.79E-02*  0.00   1.08     QN    YES      0.00
        7   1   10    1  -100.08232794   -0.10E-04*   1   4 1 -0.50E-03* -0.49E-02*  0.00   1.63     LS    YES      0.00
        8   1   10    1  -100.08232974   -0.18E-05*   1  20 1  0.55E-03*  0.18E-02*  0.00   1.01     QN    YES      0.00
        9   1    9    1  -100.08233045   -0.71E-06*   1   4 1  0.13E-03* -0.76E-03*  0.00   1.13     QN    YES      0.00
       10   1    9    1  -100.08233053   -0.78E-07*   3   7 1 -0.38E-04*  0.26E-03*  0.00   1.32     QN    YES      0.00
       11   1    8    1  -100.08233054   -0.11E-07*   1   4 1 -0.19E-04*  0.97E-04*  0.00   1.15     QN    YES      0.00
       12   1    4    1  -100.08233054   -0.68E-09    1   4 1 -0.92E-05*  0.11E-04*  0.00   1.26     QN    YES      0.00
       13   1    3    1  -100.08233054   -0.73E-10    1   4 1 -0.16E-05*  0.46E-05*  0.00   1.22     QN    YES      0.00
       14   1    2    1  -100.08233054   -0.62E-11    1   8 1  0.55E-06  -0.79E-06   0.00   1.16     QN    YES      0.00
      Convergence after 14 iterations
       15   1    2    1  -100.08233054   -0.18E-12    3  11 1 -0.13E-06  -0.79E-06   0.00   1.16     QN    YES      0.00

      ************************************************************************************************************************
                                                      Wave function printout:
                       occupation of active orbitals, and spin coupling of open shells (u,d: Spin up or down)
      ************************************************************************************************************************

      Note: transformation to natural orbitals
      has been made, which may change the order of the CSFs.

      printout of CI-coefficients larger than  0.05 for root  1
      energy=    -100.082331
      conf/sym  111 2 4     Coeff  Weight
             3  220 2 2   0.99492 0.98987
             6  202 2 2  -0.08895 0.00791

      Natural orbitals and occupation numbers for root  1
      sym 1:   1.999800   1.980490   0.020154
      sym 2:   1.999778
      sym 4:   1.999778

      ************************************************************************************************************************
      *                                                                                                                      *
      *                                                    Final results                                                    *
      *                                                                                                                      *
      ************************************************************************************************************************


      Wave function specifications:
      -----------------------------

      Number of closed shell electrons           2
      Number of electrons in active shells       8
      Max number of holes in RAS1 space          0
      Max nr of electrons in RAS3 space          0
      Number of inactive orbitals                1
      Number of active orbitals                  5
      Number of secondary orbitals              38
      Spin quantum number                      0.0
      State symmetry                             1


      Orbital specifications:
      -----------------------

      Symmetry species                           1   2   3   4
                                                a1  b1  a2  b2
      Frozen orbitals                            0   0   0   0
      Inactive orbitals                          1   0   0   0
      Active orbitals                            3   1   0   1
      RAS1 orbitals                              0   0   0   0
      RAS2 orbitals                              3   1   0   1
      RAS3 orbitals                              0   0   0   0
      Secondary orbitals                        16   9   4   9
      Deleted orbitals                           0   0   0   0
      Number of basis functions                 20  10   4  10


      CI expansion specifications:
      ----------------------------

      Number of configuration state fnc.         8
      Number of determinants                     8
      Number of root(s) required                 1
      CI root used                               1
      highest root included in the CI            1
      Root passed to geometry opt.               1


      Final optimization conditions:
      ------------------------------

      Average CI energy                            -100.08233054
      RASSCF energy for state  1                   -100.08233054
      Super-CI energy                                 0.00000000
      RASSCF energy change                            0.00000000
      Max change in MO coefficients              -0.441E-05
      Max non-diagonal density matrix element    -0.788E-06
      Maximum BLB matrix element                 -0.129E-06
      (orbital pair   3,  11 in symmetry   1)
      Norm of electronic gradient            0.396E-06


      Final state energy(ies):
      ------------------------

      root number  1 E =       -100.08233054 a.u.


      Molecular orbitals:
      -------------------

      All orbitals are eigenfunctions of the PT2 Fock matrix




      Molecular orbitals for symmetry species 1: a1 


      Orbital          1         2         3         4
      Energy    -26.1112   -1.7526   -0.7791    0.8199

    1 H   1s     -0.0042    0.0630   -0.8009    1.8475
    2 H   *s      0.0054   -0.0691    0.1629   -0.1954
    3 H   *s      0.0013   -0.0276    0.2843   -0.6966
    4 H   *pz     0.0000    0.0078   -0.0195    0.0404
    5 H   *pz     0.0000    0.0033   -0.0209    0.1126
    6 H   *d0     0.0000    0.0018   -0.0033    0.0047
    7 H   *d2+    0.0000    0.0000    0.0000    0.0000
    8 F   1s      0.9962    0.0804   -0.0011   -0.0285
    9 F   2s     -0.0817    0.9469   -0.1576   -0.4736
   10 F   *s      0.0008   -0.0008    0.0055   -0.0469
   11 F   *s     -0.0023    0.0357    0.1300   -0.1177
   12 F   2pz    -0.0219    0.2383    0.7820    1.1802
   13 F   *pz     0.0033   -0.0060   -0.0027   -0.1372
   14 F   *pz     0.0007    0.0009   -0.0523   -0.3904
   15 F   *d0     0.0000   -0.0005   -0.0087   -0.0230
   16 F   *d0    -0.0004   -0.0022   -0.0222    0.0021
   17 F   *d2+    0.0000    0.0000    0.0000    0.0000
   18 F   *d2+    0.0000    0.0000    0.0000    0.0000
   19 F   *f0     0.0001    0.0001    0.0046    0.0196
   20 F   *f2+    0.0000    0.0000    0.0000    0.0000



      Molecular orbitals for symmetry species 2: b1 


      Orbital          1
      Energy     -0.6473

    1 H   *px     0.0112
    2 H   *px     0.0310
    3 H   *d1+    0.0064
    4 F   2px     0.9616
    5 F   *px    -0.0014
    6 F   *px     0.0293
    7 F   *d1+   -0.0036
    8 F   *d1+   -0.0124
    9 F   *f1+    0.0028
   10 F   *f3+    0.0000



      Molecular orbitals for symmetry species 4: b2 


      Orbital          1
      Energy     -0.6473

    1 H   *py     0.0112
    2 H   *py     0.0310
    3 H   *d1-    0.0064
    4 F   2py     0.9616
    5 F   *py    -0.0014
    6 F   *py     0.0293
    7 F   *d1-   -0.0036
    8 F   *d1-   -0.0124
    9 F   *f3-    0.0000
   10 F   *f1-    0.0028

      Mulliken population Analysis for root number: 1
      -----------------------------------------------


      Mulliken charges per center and basis function type
      ---------------------------------------------------

               H       F   
      1s     1.1051  1.9988
      2s     0.0000  1.9174
      2px    0.0000  1.9200
      2pz    0.0000  1.5611
      2py    0.0000  1.9200
      *s    -0.5263 -0.0047
      *px    0.0334  0.0412
      *pz    0.0238 -0.0848
      *py    0.0334  0.0412
      *d2+   0.0000  0.0000
      *d1+   0.0042  0.0009
      *d0    0.0019  0.0078
      *d1-   0.0042  0.0009
      *d2-   0.0000  0.0000
      *f3+   0.0000  0.0000
      *f2+   0.0000  0.0000
      *f1+   0.0000  0.0001
      *f0    0.0000  0.0004
      *f1-   0.0000  0.0001
      *f2-   0.0000  0.0000
      *f3-   0.0000  0.0000
      Total  0.6796  9.3204

      N-E    0.3204 -0.3204

      Total electronic charge=   10.000000

      Total            charge=    0.000000


      Expectation values of various properties for root number: 1
      -----------------------------------------------------------

Dipole Moment (Debye):                                                          
Origin of the operator (Ang)=    0.0000    0.0000    0.0000
               X=    0.0000               Y=    0.0000               Z=   -1.8050           Total=    1.8050
Quadrupole Moment (Debye*Ang):                                                  
Origin of the operator (Ang)=    0.0000    0.0000    0.8663
              XX=   -5.5314              XY=    0.0000              XZ=    0.0000              YY=   -5.5314
              YZ=    0.0000              ZZ=   -3.3869
In traceless form (Debye*Ang)
              XX=   -1.0722              XY=    0.0000              XZ=    0.0000              YY=   -1.0722
              YZ=    0.0000              ZZ=    2.1445
      Canonical orbitals are written to the RASORB           file
      Natural orbitals for root   1 are written to the RASORB.1         file
      Spin density orbitals for root   1 are written to the SPDORB.1         file
--- Stop Module: rasscf at Tue Dec 23 14:03:22 2008 /rc=0 ---

     Happy landing!

