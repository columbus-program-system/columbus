1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1   -100.0823305449 -2.4869E-14  2.8280E-01  1.4291E+00  1.0000E-04
 mraqcc  #  2  1   -100.3156776260  2.3335E-01  1.2672E-02  2.7847E-01  1.0000E-04
 mraqcc  #  3  1   -100.3301859549  1.4508E-02  7.4372E-04  6.6877E-02  1.0000E-04
 mraqcc  #  4  1   -100.3312096801  1.0237E-03  6.0370E-05  1.8320E-02  1.0000E-04
 mraqcc  #  5  1   -100.3312916491  8.1969E-05  5.1787E-06  5.3826E-03  1.0000E-04
 mraqcc  #  6  1   -100.3312982553  6.6062E-06  5.1972E-07  1.6746E-03  1.0000E-04
 mraqcc  #  7  1   -100.3312988560  6.0068E-07  1.0337E-07  6.3083E-04  1.0000E-04
 mraqcc  #  8  1   -100.3312989804  1.2447E-07  1.5379E-08  2.9173E-04  1.0000E-04
 mraqcc  #  9  1   -100.3312989987  1.8258E-08  2.0975E-09  1.0963E-04  1.0000E-04
 mraqcc  # 10  1   -100.3312990012  2.5149E-09  2.4922E-10  3.7319E-05  1.0000E-04

 mraqcc   convergence criteria satisfied after 10 iterations.

 final mraqcc   convergence information:
 mraqcc  # 10  1   -100.3312990012  2.5149E-09  2.4922E-10  3.7319E-05  1.0000E-04

 number of reference csfs (nref) is     8.  root number (iroot) is  1.
 c0**2 =   0.95821810  c**2 (all zwalks) =   0.95821810
