

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    SEWARD INTEGRALS                                                                
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  

 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -227.5825319506    nuclear repulsion=   100.5855518353
 demc=             0.0000000000    wnorm=                 0.0000000086
 knorm=            0.0000000071    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1 -0.9375071491  0.8789196546  333300
     72 -0.2099888966  0.0440953367  123312
     51 -0.1198654561  0.0143677276  133320
     22  0.1174857062  0.0138028912  313302
     41  0.1147029530  0.0131567674  303330
     43  0.1033779499  0.0106870005  303303
     91  0.1020747113  0.0104192467  033330
     93  0.1004471303  0.0100896260  033303
    105 -0.0469600111  0.0022052426  003333
    101  0.0274326643  0.0007525511  013332
     88 -0.0240592512  0.0005788476  103323
     10  0.0202492015  0.0004100302  331212
     15  0.0107115879  0.0001147381  330330
      4  0.0106130888  0.0001126377  333030
      6  0.0097199004  0.0000944765  333003
     17  0.0094638436  0.0000895643  330303
     81 -0.0072770452  0.0000529554  113322
     79 -0.0044826764  0.0000200944  121233
     46 -0.0023538543  0.0000055406  303033
     50 -0.0023263542  0.0000054119  300333
     96 -0.0021555047  0.0000046462  033033
    100 -0.0021502827  0.0000046237  030333
     66 -0.0016423985  0.0000026975  131232
     27 -0.0012436740  0.0000015467  313032
     38 -0.0011895225  0.0000014150  310332
     37 -0.0010685090  0.0000011417  311223
     69 -0.0010450319  0.0000010921  130323

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: