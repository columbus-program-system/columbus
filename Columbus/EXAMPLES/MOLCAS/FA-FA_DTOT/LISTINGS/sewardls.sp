   This run of MOLCAS is using the pymolcas driver

                             OPE
                             OPE          NMOL  CASOP  ENMOLC A   SO
                 OPE        NMOLC        AS  OP EN  MO LC     AS  OP
                OPENM       OL CA        SO  PE NM  OL CA     SOP EN
                OP EN      MO   LC       AS  OP ENMOL  CASO   PENMOL
                OP  EN     MO   LC       AS  OP EN     MO     LC ASO
               OP   E  NMOL  C  AS       OP  EN MO     LC     AS  OP
               OP  E   NMO   LC AS        OPEN  MO     LCASOP EN   M
               O  PEN   MO  LCA  SO
            OPE   NMO   L   CAS    OP
        OPENMOL  CASOP     ENMOL   CASOPE
     OPENMOLCA   SOPENMOLCASOPEN   MOLCASOPE
    OPENMOLCAS   OP           EN   MOL    CAS
    OPENMOLCAS       OP  ENM        O     LCA
    OPENMOLCAS    OPEN  MOLCASO     P  E  NMO
    OPENMOLCAS     OP               E  N  MOL
    OPENMOLCA   SO           PENM   O  L  CAS    OPEN    MO    LCAS
    OPENMOLCA   SOP           ENM   O  L  CAS   OP  EN  MOLC  AS   O
    OPENMOLCA   SOPE           NM      O  LCA   S      OP  EN MO
    OPENMOLC                AS         O  PEN   M      OL  CA  SOPE
    OPENMO        LCASOPE  NMOL        C  ASO   P      ENMOLC     AS
    OPE     NMO      LCA  SO     P     E   NM   OL  CA SO  PE N   MO
          OPENMOLCA            SOPE   NMO        LCAS  O    P  ENMO
     OPENMOLCASOPENMOLCASOPENMOLCASOPENMOLCA
        OPENMOLCASOPENMOLCASOPENMOLCASOPE
            OPENMOLCASOPENMOLCASOPENM
               OPENMOLCASOPENMOLCA        version v18.09-1-g9345699-dirty
                   OPENMOLCASO
                       OPE                tag

 OpenMolcas is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License version 2.1.
 OpenMolcas is distributed in the hope that it will be useful, but it
 is provided "as is" and without any express or implied warranties.
 For more details see the full text of the license in the file
 LICENSE or in <http://www.gnu.org/licenses/>.

                 Copyright (C) The OpenMolcas Authors
           For the author list and the recommended citation,
                   consult the file CONTRIBUTORS.md

           *************************************************
           * pymolcas version py2.03                       *
           *   build cc5afe975c0d8f3d2e37f599b10b09cf      *
           *   (after the EMIL interpreter by V. Veryazov) *
           *************************************************

configuration info
------------------
C Compiler ID: GNU
C flags: -std=gnu99
Fortran Compiler ID: Intel
Fortran flags: -fpp -i8 -r8 -heap-arrays
Definitions: _MOLCAS_;_I8_;_LINUX_;_MKL_
Parallel: OFF (GA=OFF)


   --------------------------------------------------------------------------------------------------------------------------
  |
  |           Project: molcas
  |    Submitted from: /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/NAC_OpenMolcas/EXAMPLES_Yafu/FA-FA_DTOT/RUN6.min/WORK
  |      Scratch area: /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/NAC_OpenMolcas/EXAMPLES_Yafu/FA-FA_DTOT/RUN6.min/WORK
  |   Save outputs to: WORKDIR
  |            Molcas: /home/cm/cmfp2/programs/Molcas/OpenMolcas/build-col
  |
  | Scratch area is NOT empty
  |
  |        MOLCASMEM = 2000
  |    MOLCAS_DRIVER = /mnt/gpfs01/home/cm/cmfp2/bin/pymolcas
  |    MOLCAS_NPROCS = 1
  |    MOLCAS_SOURCE = /home/cm/cmfp2/programs/Molcas/OpenMolcas
  | MOLCAS_STRUCTURE = 0
  |
   --------------------------------------------------------------------------------------------------------------------------

++ ---------   Input file   ---------

&SEWARD
ANGMOM
 0 0 0
AMFI
Basis set
C.6-31G
C1        3.30702000     0.00000000     1.11918800
C2       -3.30702000     0.00000000     1.11918800
End of Basis
Basis set
O.6-31G
O3        3.30702000     0.00000000    -1.12509400
O4       -3.30702000     0.00000000    -1.12509400
End of Basis
Basis set
H.6-31G
H5        3.30702000     1.74780900     2.21235400
H6       -3.30702000     1.74780900     2.21235400
H7        3.30702000    -1.74780900     2.21235400
H8       -3.30702000    -1.74780900     2.21235400
End of Basis
End of input

-- ----------------------------------

--- Start Module: seward at Thu Apr  8 14:29:11 2021 ---
 
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()

                                              &SEWARD

                                   only a single process is used
                       available to each process: 2.0 GB of memory, 1 thread?
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
 
               SEWARD will generate:
                  Multipole Moment integrals up to order  2
                  Kinetic Energy integrals
                  Nuclear Attraction integrals (point charge)
                  One-Electron Hamiltonian integrals
                  Velocity integrals
                  Orbital angular momentum around ( 0.0000  0.0000  0.0000 )
                  Atomic mean-field integrals
                  Two-Electron Repulsion integrals
 
                   Integrals are discarded if absolute value <: 0.10E-13
                   Integral cutoff threshold is set to       <: 0.10E-15
 
++    Symmetry information:
      ---------------------
 
                    Character Table for C1 
 
                             E  
                    a        1  x, y, xy, Rz, z, xz, Ry, yz, Rx, I
--
 
 
++    Basis set information:
      ----------------------
 
 
      Basis set label: C.6-31G.........
 
      Electronic valence basis set:
      ------------------
      Associated Effective Charge   6.000000 au
      Associated Actual Charge      6.000000 au
      Nuclear Model: Point charge
 
      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s      10       3        X                  
         p       4       2        X                  
 
 
      Basis set label: O.6-31G.........
 
      Electronic valence basis set:
      ------------------
      Associated Effective Charge   8.000000 au
      Associated Actual Charge      8.000000 au
      Nuclear Model: Point charge
 
      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s      10       3        X                  
         p       4       2        X                  
 
 
      Basis set label: H.6-31G.........
 
      Electronic valence basis set:
      ------------------
      Associated Effective Charge   1.000000 au
      Associated Actual Charge      1.000000 au
      Nuclear Model: Point charge
 
      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       4       2        X                  
--
 
 
++    Molecular structure info:
      -------------------------
 
                    ************************************************ 
                    **** Cartesian Coordinates / Bohr, Angstrom **** 
                    ************************************************ 
 
     Center  Label                x              y              z                     x              y              z
        1      C1               3.307020       0.000000       1.119188              1.750000       0.000000       0.592249
        2      C2              -3.307020       0.000000       1.119188             -1.750000       0.000000       0.592249
        3      O3               3.307020       0.000000      -1.125094              1.750000       0.000000      -0.595374
        4      O4              -3.307020       0.000000      -1.125094             -1.750000       0.000000      -0.595374
        5      H5               3.307020       1.747809       2.212354              1.750000       0.924901       1.170727
        6      H6              -3.307020       1.747809       2.212354             -1.750000       0.924901       1.170727
        7      H7               3.307020      -1.747809       2.212354              1.750000      -0.924901       1.170727
        8      H8              -3.307020      -1.747809       2.212354             -1.750000      -0.924901       1.170727
 
                    *************************************** 
                    *    InterNuclear Distances / Bohr    * 
                    *************************************** 
 
               1 C1            2 C2            3 O3            4 O4            5 H5            6 H6    
    1 C1       0.000000
    2 C2       6.614040        0.000000
    3 O3       2.244282        6.984435        0.000000
    4 O4       6.984435        2.244282        6.614040        0.000000
    5 H5       2.061516        6.927869        3.767412        7.611762        0.000000
    6 H6       6.927869        2.061516        7.611762        3.767412        6.614040        0.000000
    7 H7       2.061516        6.927869        3.767412        7.611762        3.495618        7.480967
    8 H8       6.927869        2.061516        7.611762        3.767412        7.480967        3.495618
 
               7 H7            8 H8    
    7 H7       0.000000
    8 H8       6.614040        0.000000
 
                    ******************************************* 
                    *    InterNuclear Distances / Angstrom    * 
                    ******************************************* 
 
               1 C1            2 C2            3 O3            4 O4            5 H5            6 H6    
    1 C1       0.000000
    2 C2       3.499999        0.000000
    3 O3       1.187623        3.696004        0.000000
    4 O4       3.696004        1.187623        3.499999        0.000000
    5 H5       1.090907        3.666071        1.993629        4.027971        0.000000
    6 H6       3.666071        1.090907        4.027971        1.993629        3.499999        0.000000
    7 H7       1.090907        3.666071        1.993629        4.027971        1.849801        3.958757
    8 H8       3.666071        1.090907        4.027971        1.993629        3.958757        1.849801
 
               7 H7            8 H8    
    7 H7       0.000000
    8 H8       3.499999        0.000000
 
                    ************************************** 
                    *    Valence Bond Angles / Degree    * 
                    ************************************** 
                          Atom centers                 Phi
                      3 O3       1 C1       5 H5       122.02
                      3 O3       1 C1       7 H7       122.02
                      5 H5       1 C1       7 H7       115.95
                      4 O4       2 C2       6 H6       122.02
                      4 O4       2 C2       8 H8       122.02
                      6 H6       2 C2       8 H8       115.95
--
 
 
            Nuclear Potential Energy            100.58555184 au
 
 
      Basis set specifications :
      Symmetry species         a  
      Basis functions           44
 
 
  Input file to MOLDEN was generated!
 
--- Stop Module: seward at Thu Apr  8 14:29:16 2021 /rc=_RC_ALL_IS_WELL_ ---
--- Module seward spent 5 seconds ---

    Timing: Wall=5.01 User=0.64 System=0.53
