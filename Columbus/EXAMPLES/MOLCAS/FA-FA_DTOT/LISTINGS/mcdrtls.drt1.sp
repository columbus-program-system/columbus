
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/NAC_OpenMolcas/EXAMPLES_Yafu/FA
 
 input the spin multiplicity [  0]: spin multiplicity:    1    singlet 
 input the total number of electrons [  0]: nelt:     32
 input the number of irreps (1-8) [  0]: nsym:      1
 enter symmetry labels:(y,[n]) enter 1 labels (a4):
 enter symmetry label, default=   1
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:     12
 number of inactive electrons:     24
 number of active electrons:      8
 level(*)        1   2   3   4   5   6   7   8   9  10  11  12
 symd(*)         1   1   1   1   1   1   1   1   1   1   1   1
 slabel(*)     a   a   a   a   a   a   a   a   a   a   a   a  
 doub(*)         1   2   3   4   5   6   7   8   9  10  11  12
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      6
 level(*)        1   2   3   4   5   6
 syml(*)         1   1   1   1   1   1
 slabel(*)     a   a   a   a   a   a  
 modrt(*)       13  14  15  16  17  18
 input the minimum cumulative occupation for each active level:
  a   a   a   a   a   a  
   13  14  15  16  17  18
 input the maximum cumulative occupation for each active level:
  a   a   a   a   a   a  
   13  14  15  16  17  18
 slabel(*)     a   a   a   a   a   a  
 modrt(*)       13  14  15  16  17  18
 occmin(*)       0   0   0   0   0   8
 occmax(*)       8   8   8   8   8   8
 input the minimum b value for each active level:
  a   a   a   a   a   a  
   13  14  15  16  17  18
 input the maximum b value for each active level:
  a   a   a   a   a   a  
   13  14  15  16  17  18
 slabel(*)     a   a   a   a   a   a  
 modrt(*)       13  14  15  16  17  18
 bmin(*)         0   0   0   0   0   0
 bmax(*)         8   8   8   8   8   8
 input the step masks for each active level:
 modrt:smask=
  13:1111  14:1111  15:1111  16:1111  17:1111  18:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     26
 are any arcs to be manually removed?(y,[n])
 nwalk=     105
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   6

 level  0 through level  6 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  26   6 4 0   1 a    18    0   0   0   0   1     1     0     0     0   105     0
 ........................................

  23   5 4 0   1 a    17   26   0   0   0   1     1     0     0     0    15     0

  24   5 3 1   1 a    17    0   0  26   0   1     1     1     1     0    40    15

  25   5 3 0   1 a    17    0   0   0  26   1     1     1     1     1    50    55
 ........................................

  17   4 4 0   1 a    16   23   0   0   0   1     1     0     0     0     1     0

  18   4 3 1   1 a    16   24   0  23   0   1     2     1     1     0     4     1

  19   4 3 0   1 a    16   25  24   0  23   1     3     2     1     1    10     5

  20   4 2 2   1 a    16    0   0  24   0   1     1     1     1     0     6    29

  21   4 2 1   1 a    16    0   0  25  24   1     2     2     2     1    20    35

  22   4 2 0   1 a    16    0   0   0  25   1     1     1     1     1    20    85
 ........................................

  11   3 3 0   1 a    15   19  18   0  17   1     6     3     1     1     1     0

  12   3 2 1   1 a    15   21  20  19  18   1     8     6     5     2     3     2

  13   3 2 0   1 a    15   22  21   0  19   1     6     5     3     3     6     9

  14   3 1 2   1 a    15    0   0  21  20   1     3     3     3     1     3    32

  15   3 1 1   1 a    15    0   0  22  21   1     3     3     3     2     8    47

  16   3 1 0   1 a    15    0   0   0  22   1     1     1     1     1     6    99
 ........................................

   5   2 2 0   1 a    14   13  12   0  11   1    20    14     6     6     1     0

   6   2 1 1   1 a    14   15  14  13  12   1    20    17    14     8     2     3

   7   2 1 0   1 a    14   16  15   0  13   1    10     9     6     6     3    12

   8   2 0 2   1 a    14    0   0  15  14   1     6     6     6     3     1    34

   9   2 0 1   1 a    14    0   0  16  15   1     4     4     4     3     2    53

  10   2 0 0   1 a    14    0   0   0  16   1     1     1     1     1     1   104
 ........................................

   2   1 1 0   1 a    13    7   6   0   5   1    50    40    20    20     1     0

   3   1 0 1   1 a    13    9   8   7   6   1    40    36    30    20     1     4

   4   1 0 0   1 a    13   10   9   0   7   1    15    14    10    10     1    14
 ........................................

   1   0 0 0   0       0    4   3   0   2   1   105    90    50    50     1     0
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=     105
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
      105 csfs selected from     105 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
      105 csfs selected from     105 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
      105 csfs selected from     105 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/NAC_OpenMolcas/EXAMPLES_Yafu/FA
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 3 3 3 0 0
   CSF#     2    3 3 3 1 2 0
   CSF#     3    3 3 3 1 0 2
   CSF#     4    3 3 3 0 3 0
   CSF#     5    3 3 3 0 1 2
   CSF#     6    3 3 3 0 0 3
   CSF#     7    3 3 1 3 2 0
   CSF#     8    3 3 1 3 0 2
   CSF#     9    3 3 1 2 3 0
   CSF#    10    3 3 1 2 1 2
   CSF#    11    3 3 1 2 0 3
   CSF#    12    3 3 1 1 2 2
   CSF#    13    3 3 1 0 3 2
   CSF#    14    3 3 1 0 2 3
   CSF#    15    3 3 0 3 3 0
   CSF#    16    3 3 0 3 1 2
   CSF#    17    3 3 0 3 0 3
   CSF#    18    3 3 0 1 3 2
   CSF#    19    3 3 0 1 2 3
   CSF#    20    3 3 0 0 3 3
   CSF#    21    3 1 3 3 2 0
   CSF#    22    3 1 3 3 0 2
   CSF#    23    3 1 3 2 3 0
   CSF#    24    3 1 3 2 1 2
   CSF#    25    3 1 3 2 0 3
   CSF#    26    3 1 3 1 2 2
   CSF#    27    3 1 3 0 3 2
   CSF#    28    3 1 3 0 2 3
   CSF#    29    3 1 2 3 3 0
   CSF#    30    3 1 2 3 1 2
   CSF#    31    3 1 2 3 0 3
   CSF#    32    3 1 2 1 3 2
   CSF#    33    3 1 2 1 2 3
   CSF#    34    3 1 2 0 3 3
   CSF#    35    3 1 1 3 2 2
   CSF#    36    3 1 1 2 3 2
   CSF#    37    3 1 1 2 2 3
   CSF#    38    3 1 0 3 3 2
   CSF#    39    3 1 0 3 2 3
   CSF#    40    3 1 0 2 3 3
   CSF#    41    3 0 3 3 3 0
   CSF#    42    3 0 3 3 1 2
   CSF#    43    3 0 3 3 0 3
   CSF#    44    3 0 3 1 3 2
   CSF#    45    3 0 3 1 2 3
   CSF#    46    3 0 3 0 3 3
   CSF#    47    3 0 1 3 3 2
   CSF#    48    3 0 1 3 2 3
   CSF#    49    3 0 1 2 3 3
   CSF#    50    3 0 0 3 3 3
   CSF#    51    1 3 3 3 2 0
   CSF#    52    1 3 3 3 0 2
   CSF#    53    1 3 3 2 3 0
   CSF#    54    1 3 3 2 1 2
   CSF#    55    1 3 3 2 0 3
   CSF#    56    1 3 3 1 2 2
   CSF#    57    1 3 3 0 3 2
   CSF#    58    1 3 3 0 2 3
   CSF#    59    1 3 2 3 3 0
   CSF#    60    1 3 2 3 1 2
   CSF#    61    1 3 2 3 0 3
   CSF#    62    1 3 2 1 3 2
   CSF#    63    1 3 2 1 2 3
   CSF#    64    1 3 2 0 3 3
   CSF#    65    1 3 1 3 2 2
   CSF#    66    1 3 1 2 3 2
   CSF#    67    1 3 1 2 2 3
   CSF#    68    1 3 0 3 3 2
   CSF#    69    1 3 0 3 2 3
   CSF#    70    1 3 0 2 3 3
   CSF#    71    1 2 3 3 3 0
   CSF#    72    1 2 3 3 1 2
   CSF#    73    1 2 3 3 0 3
   CSF#    74    1 2 3 1 3 2
   CSF#    75    1 2 3 1 2 3
   CSF#    76    1 2 3 0 3 3
   CSF#    77    1 2 1 3 3 2
   CSF#    78    1 2 1 3 2 3
   CSF#    79    1 2 1 2 3 3
   CSF#    80    1 2 0 3 3 3
   CSF#    81    1 1 3 3 2 2
   CSF#    82    1 1 3 2 3 2
   CSF#    83    1 1 3 2 2 3
   CSF#    84    1 1 2 3 3 2
   CSF#    85    1 1 2 3 2 3
   CSF#    86    1 1 2 2 3 3
   CSF#    87    1 0 3 3 3 2
   CSF#    88    1 0 3 3 2 3
   CSF#    89    1 0 3 2 3 3
   CSF#    90    1 0 2 3 3 3
   CSF#    91    0 3 3 3 3 0
   CSF#    92    0 3 3 3 1 2
   CSF#    93    0 3 3 3 0 3
   CSF#    94    0 3 3 1 3 2
   CSF#    95    0 3 3 1 2 3
   CSF#    96    0 3 3 0 3 3
   CSF#    97    0 3 1 3 3 2
   CSF#    98    0 3 1 3 2 3
   CSF#    99    0 3 1 2 3 3
   CSF#   100    0 3 0 3 3 3
   CSF#   101    0 1 3 3 3 2
   CSF#   102    0 1 3 3 2 3
   CSF#   103    0 1 3 2 3 3
   CSF#   104    0 1 2 3 3 3
   CSF#   105    0 0 3 3 3 3
