1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       665       49140           0           0       49805
      internal walks       665        1890           0           0        2555
valid internal walks       665        1890           0           0        2555
  MR-CIS calculation - skip 3- and 4-external 2e integrals
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1981 ci%nnlev=                   990  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             262105537             262083011
 lencor,maxblo             262144000                 60000
========================================
 current settings:
 minbl3           0
 minbl4           0
 locmaxbl3    18204
 locmaxbuf     9102
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================
 Orig.  diagonal integrals:  1electron:        44
                             0ext.    :       342
                             2ext.    :       936
                             4ext.    :       702


 Orig. off-diag. integrals:  4ext.    :         0
                             3ext.    :         0
                             2ext.    :    180063
                             1ext.    :     88920
                             0ext.    :     17442
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       351


 Sorted integrals            3ext.  w :         0 x :         0
                             4ext.  w :         0 x :         0


Cycle #  1 sortfile size=   1572864(       3 records of   524288) #buckets=   2
distributed memory consumption per node=         0 available core 262105537
Cycle #  2 sortfile size=   1572864(       3 records of   524288) #buckets=   2
distributed memory consumption per node=         0 available core 262105537
 minimum size of srtscr:    524288 WP (     1 records)
 maximum size of srtscr:   1572864 WP (     3 records)
diagi   file:      4 records  of   6144 WP each=>      24576 WP total
ofdgi   file:     49 records  of   6144 WP each=>     301056 WP total
fil3w   file:      0 records  of  30006 WP each=>          0 WP total
fil3x   file:      0 records  of  30006 WP each=>          0 WP total
fil4w   file:      0 records  of  30006 WP each=>          0 WP total
fil4x   file:      0 records  of  30006 WP each=>          0 WP total
 compressed index vector length=                     3
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 32
  NROOT = 3
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 3
  RTOLBK = 1e-4,1e-4,1e-4,
  NITER = 90
  NVCIMN = 5
  RTOLCI = 1e-4,1e-4,1e-4,
  NVCIMX = 8
  NVRFMX = 8
  NVBKMX = 8
   iden=2
  CSFPRN = 10,
    MOLCAS=1
 /&end
 transition
   1  1  1  2
   1  1  1  3
   1  2  1  3
 ------------------------------------------------------------------------
transition densities: resetting nroot to    3
lodens (list->root)=  1  2  3
invlodens (root->list)=  1  2  3
 bummer (warning):resetting fileloc for seriel operation0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    3      noldv  =   0      noldhv =   0
 nunitv =    5      nbkitr =    1      niter  =  90      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    8      ibktv  =  -1      ibkthv =  -1
 nvcimx =    8      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    3      nvcimn =    5      maxseg = 300      nrfitr =  30
 ncorel =   32      nvrfmx =    8      nvrfmn =   5      iden   =   2
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 Computing density:                    .drt1.state2
 Computing density:                    .drt1.state3
 Computing transition density:          drt1.state1-> drt1.state2 (.trd1to2)
 Computing transition density:          drt1.state1-> drt1.state3 (.trd1to3)
 Computing transition density:          drt1.state2-> drt1.state3 (.trd2to3)
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          262143999 DP per process
 gapointer%node_offset(*)=                     0                     0
 gapointer%node_width(*)=                     0                     0

********** Integral sort section *************


 workspace allocation information: lencor= 262143999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =  100.585551835                                          
 MCSCF energy =    -227.582531951                                                
 SIFS file created by program tran.      login002.cm.clust 14:29:27.252 08-Apr-21

 input energy(*) values:
 energy( 1)=  1.005855518353E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   1.005855518353E+02

 nsym = 1 nmot=  44

 symmetry  =    1
 slabel(*) = a   
 nmpsy(*)  =   44

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044

 input parameters:
 prnopt=  0
 ldamin=   32767 ldamax=  524288 ldainc=    4096
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    6144
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  44 nfctd =   0 nfvtc =   0 nmot  =  44
 nlevel =  44 niot  =  18 lowinl=  27
 orbital-to-level map(*)
   27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44   1   2
    3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22
   23  24  25  26
 compressed map(*)
   27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44   1   2
    3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22
   23  24  25  26
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1
 repartitioning mu(*)=
   2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  0.  0.  0.  0.  0.  0.
  ... transition density matrix calculation requested 
  ... setting rmu(*) to zero 

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1980
 number with all external indices:       702
 number with half external - half internal indices:       936
 number with all internal indices:       342

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      70200 strt=          1
    3-external integrals: num=     175500 strt=      70201
    2-external integrals: num=     180063 strt=     245701
    1-external integrals: num=      88920 strt=     425764
    0-external integrals: num=      17442 strt=     514684

 total number of off-diagonal integrals:      532125


 indxof(2nd)  ittp=   3 numx(ittp)=      180063
 indxof(2nd)  ittp=   4 numx(ittp)=       88920
 indxof(2nd)  ittp=   5 numx(ittp)=       17442

 intermediate da file sorting parameters:
 nbuk=   2 lendar=  524288 nipbk=  349524 nipsg= 261149184
 pro2e        1     991    1981    2971    3961    4132    4303    5293  704341 1403389
  1927677 1935869 1941329 1963168

 pro2e:    415111 integrals read in    77 records.

 pro2e:    196926 integrals 34-ext integrals skipped.
 pro1e        1     991    1981    2971    3961    4132    4303    5293  704341 1403389
  1927677 1935869 1941329 1963168
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     3  records of                 524288 
 WP =              12582912 Bytes
 putdg        1     991    1981    2971    9115  533403  882928    5293  704341 1403389
  1927677 1935869 1941329 1963168

 putf:       4 buffers of length    6144 written to file 12
 diagonal integral file completed.
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd    180063         3    245701    245701    180063    532125
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     88920         4    425764    425764     88920    532125
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     17442         5    514684    514684     17442    532125

 putf:      49 buffers of length    6144 written to file 13
 off-diagonal files sort completed.
 !timer: cisrt complete                  cpu_time=     0.079 walltime=     0.969
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  6144
 diagfile 4ext:     702 2ext:     936 0ext:     342
 fil4w,fil4x  :   70200 fil3w,fil3x :  175500
 ofdgint  2ext:  180063 1ext:   88920 0ext:   17442so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1050 minbl3    1050 maxbl2    1053nbas:  26   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 262143999

 core energy values from the integral file:
 energy( 1)=  1.005855518353E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  1.005855518353E+02
 nmot  =    44 niot  =    18 nfct  =     0 nfvt  =     0
 nrow  =   135 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       2555      665     1890        0        0
 nvalwt,nvalw:     2555      665     1890        0        0
 ncsft:           49805
 total number of valid internal walks:    2555
 nvalz,nvaly,nvalx,nvalw =      665    1890       0       0

 cisrt info file parameters:
 file number  12 blocksize   6144
 mxbld   6144
 nd4ext,nd2ext,nd0ext   702   936   342
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    70200   175500   180063    88920    17442        0        0        0
 minbl4,minbl3,maxbl2  1050  1050  1053
 maxbuf 30006
 number of external orbitals per symmetry block:  26
 nmsym   1 number of internal orbitals  18
 bummer (warning):transition densities: resetting ref occupation number to 00
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    18                     1
 block size     0
 pthz,pthy,pthx,pthw:   665  1890     0     0 total internal walks:    2555
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
setref: retained number of references =   105
 setref: total/valid number of walks=                   665
                   665
 nmb.of records onel     1
 nmb.of records 2-ext    30
 nmb.of records 1-ext    15
 nmb.of records 0-ext     3
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            67183
    threx             60019
    twoex             13385
    onex               7601
    allin              6144
    diagon             7237
               =======
   maximum            67183
 
  __ static summary __ 
   reflst               665
   hrfspc               665
               -------
   static->             665
 
  __ core required  __ 
   totstc               665
   max n-ex           67183
               -------
   totnec->           67848
 
  __ core available __ 
   totspc         262143999
   totnec -           67848
               -------
   totvec->       262076151

 number of external paths / symmetry
 vertex x     325
 vertex w     351
segment: free space=   262076151
 reducing frespc by                 12565 to              262063586 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         665|       665|         0|       665|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1890|     49140|       665|      1890|       665|         2|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=        7560DP  drtbuffer=        5001 DP

dimension of the ci-matrix ->>>     49805

 executing brd_struct for civct
 gentasklist: ntask=                     6
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1    1890     665      49140        665    1890     665
     2  1   1     1      allint zz    OX  1 1     665     665        665        665     665     665
     3  2   2     5      0ex2ex yy    OX  2 2    1890    1890      49140      49140    1890    1890
     4  2   2    42      four-ext y   4X  2 2    1890    1890      49140      49140    1890    1890
     5  1   1    75      dg-024ext z  OX  1 1     665     665        665        665     665     665
     6  2   2    76      dg-024ext y  OX  2 2    1890    1890      49140      49140    1890    1890
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=   5.000 N=  1 (task/type/sgbra)=(   1/11/0) (
REDTASK #   2 TIME=   4.000 N=  1 (task/type/sgbra)=(   2/ 1/0) (
REDTASK #   3 TIME=   3.000 N=  1 (task/type/sgbra)=(   3/ 5/0) (
REDTASK #   4 TIME=   2.000 N=  1 (task/type/sgbra)=(   4/42/1) (
REDTASK #   5 TIME=   1.000 N=  1 (task/type/sgbra)=(   5/75/1) (
REDTASK #   6 TIME=   0.000 N=  1 (task/type/sgbra)=(   6/76/1) (
 initializing v-file: 1:                 49805

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      5 vectors will be written to unit 11 beginning with logical record   1

            5 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:           0    task #
 reference space has dimension     105
 dsyevx: computed roots 1 to   10(converged:  10)

    root           eigenvalues
    ----           ------------
       1        -227.6831283034
       2        -227.5324466902
       3        -227.5320208582
       4        -227.4089741304
       5        -227.3797382805
       6        -227.3397144514
       7        -227.3395707897
       8        -227.2685413742
       9        -227.2170750943
      10        -227.2169656747

 strefv generated    5 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   665

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   665

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   665

         vector  3 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   665)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             49805
 number of initial trial vectors:                         5
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1  -328.26868014
   ht   2     0.00000000  -328.11799853
   ht   3     0.00000000    -0.00000000  -328.11757269
   ht   4    -0.00000000     0.00000000    -0.00000000  -327.99452597
   ht   5     0.00000000    -0.00000000    -0.00000000     0.00000000  -327.96529012

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   -1.00000      -3.587369E-14  -2.155410E-13  -4.227082E-14  -1.356312E-19
 ref    2  -3.588346E-14    1.00000      -9.841715E-11   4.655634E-13   4.872321E-26
 ref    3  -2.154058E-13   9.841713E-11    1.00000      -1.330645E-13   2.716561E-26

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1    1.00000        1.00000        1.00000       2.362422E-25   1.839581E-38

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -1.00000000    -0.00000000    -0.00000000    -0.00000000    -0.00000000
 ref:   2    -0.00000000     1.00000000    -0.00000000     0.00000000     0.00000000
 ref:   3    -0.00000000     0.00000000     1.00000000    -0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -227.6831283034 -2.2737E-13  9.0965E-02  4.3527E-01  1.0000E-04   
 mr-sdci #  1  2   -227.5324466902 -3.9790E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  1  3   -227.5320208582 -1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  1  4   -227.4089741304 -1.1369E-13  0.0000E+00  3.9442E-01  1.0000E-04   
 mr-sdci #  1  5   -227.3797382805 -2.8422E-13  0.0000E+00  3.7422E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000379
time for cinew                         0.028975
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -227.6831283034 -2.2737E-13  9.0965E-02  4.3527E-01  1.0000E-04   
 mr-sdci #  1  2   -227.5324466902 -3.9790E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  1  3   -227.5320208582 -1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  1  4   -227.4089741304 -1.1369E-13  0.0000E+00  3.9442E-01  1.0000E-04   
 mr-sdci #  1  5   -227.3797382805 -2.8422E-13  0.0000E+00  3.7422E-01  1.0000E-04   
 
    3 of the   6 expansion vectors are transformed.
    3 of the   5 matrix-vector products are transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             49805
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                3
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                   90
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting ci iteration   1

 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1  -328.26868014
   ht   2    -0.00000000  -328.11799853
   ht   3    -0.00000000     0.00000000  -328.11757269

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3
 ref    1    1.00000       8.214131E-14  -1.306329E-14
 ref    2  -8.221834E-14    1.00000       1.786614E-11
 ref    3   1.306329E-14  -1.786614E-11    1.00000    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3
 ref    1    1.00000        1.00000        1.00000    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000    -0.00000000
 ref:   2    -0.00000000     1.00000000     0.00000000
 ref:   3     0.00000000    -0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -227.6831283034  0.0000E+00  9.0965E-02  4.3527E-01  1.0000E-04   
 mr-sdci #  1  2   -227.5324466902  0.0000E+00  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  1  3   -227.5320208582 -5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000605
time for cinew                         0.037604
time for eigenvalue solver             0.000123
time for vector access                 0.000000

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1  -328.26868014
   ht   2    -0.00000000  -328.11799853
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.09096479    -0.00000000     0.00000000   -15.43720274

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1   0.981473       5.815121E-10   2.041714E-10  -0.191599    
 ref    2   5.305895E-10   -1.00000       1.075734E-10  -3.170824E-10
 ref    3  -1.862590E-10   1.075736E-10    1.00000       1.114995E-10

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1   0.963290        1.00000        1.00000       3.671029E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.98147323     0.00000000     0.00000000    -0.19159930
 ref:   2     0.00000000    -1.00000000     0.00000000    -0.00000000
 ref:   3    -0.00000000     0.00000000     1.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -227.7647587104  8.1630E-02  1.1282E-02  1.0886E-01  1.0000E-04   
 mr-sdci #  2  2   -227.5324466902  1.7053E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  2  3   -227.5320208582  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  2  4   -225.5411205107 -1.8679E+00  0.0000E+00  8.0773E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000367
time for cinew                         0.028083
time for eigenvalue solver             0.000000
time for vector access                 0.000001

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1  -328.26868014
   ht   2    -0.00000000  -328.11799853
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.09096479    -0.00000000     0.00000000   -15.43720274
   ht   5    39.58245556     0.00000053    -0.00000004     0.00860981    -9.36615933

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.972992       1.207516E-09   2.807570E-10  -0.160103      -0.166295    
 ref    2  -9.524943E-10   -1.00000       6.260076E-11  -1.634391E-09  -1.147142E-10
 ref    3   2.358406E-10   6.260082E-11    1.00000       2.366197E-10   8.059639E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.946713        1.00000        1.00000       2.563295E-02   2.765392E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.97299184     0.00000000     0.00000000    -0.16010294    -0.16629469
 ref:   2    -0.00000000    -1.00000000     0.00000000    -0.00000000    -0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -227.7738006623  9.0420E-03  1.1676E-03  4.7599E-02  1.0000E-04   
 mr-sdci #  3  2   -227.5324466902  5.6843E-14  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  3  3   -227.5320208582  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  3  4   -226.8356796661  1.2946E+00  0.0000E+00  7.1323E-01  1.0000E-04   
 mr-sdci #  3  5   -225.3645385937 -2.0152E+00  0.0000E+00  7.1636E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000511
time for cinew                         0.029347
time for eigenvalue solver             0.000058
time for vector access                 0.000001

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.26868014
   ht   2    -0.00000000  -328.11799853
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.09096479    -0.00000000     0.00000000   -15.43720274
   ht   5    39.58245556     0.00000053    -0.00000004     0.00860981    -9.36615933
   ht   6    -0.57840949     0.00000009    -0.00000000     0.13998877     0.01126609    -0.23293292

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.971125       1.427368E-09   3.266041E-10   0.170026       4.982005E-02  -0.159765    
 ref    2   1.060403E-09   -1.00000       1.137875E-11   2.259363E-09   2.224299E-10  -1.471668E-11
 ref    3  -2.590621E-10   1.137872E-11    1.00000      -3.737998E-10  -1.003995E-10   4.046406E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.943084        1.00000        1.00000       2.890899E-02   2.482038E-03   2.552479E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97112521     0.00000000     0.00000000     0.17002645     0.04982005    -0.15976479
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000    -0.00000000
 ref:   3    -0.00000000     0.00000000     1.00000000    -0.00000000    -0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -227.7747538686  9.5321E-04  1.4190E-04  1.6429E-02  1.0000E-04   
 mr-sdci #  4  2   -227.5324466902 -4.5475E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  4  3   -227.5320208582 -1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  4  4   -226.9557239718  1.2004E-01  0.0000E+00  4.9837E-01  1.0000E-04   
 mr-sdci #  4  5   -225.4738892446  1.0935E-01  0.0000E+00  1.1664E+00  1.0000E-04   
 mr-sdci #  4  6   -225.3364416855  3.2592E+02  0.0000E+00  6.9643E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000684
time for cinew                         0.030325
time for eigenvalue solver             0.000077
time for vector access                 0.000000

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.26868014
   ht   2    -0.00000000  -328.11799853
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.09096479    -0.00000000     0.00000000   -15.43720274
   ht   5    39.58245556     0.00000053    -0.00000004     0.00860981    -9.36615933
   ht   6    -0.57840949     0.00000009    -0.00000000     0.13998877     0.01126609    -0.23293292
   ht   7    -0.12912840    -0.00000002    -0.00000000    -0.03849524    -0.04589027    -0.00612016    -0.02964332

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970609       1.861883E-09   3.626646E-10  -0.168061      -3.971390E-02   0.164266      -3.336948E-02
 ref    2   1.205691E-09   -1.00000       7.668425E-12  -4.488024E-09   1.734691E-09  -1.399612E-10  -8.764736E-10
 ref    3  -2.708781E-10   7.668379E-12    1.00000       5.540626E-10  -1.423178E-10  -7.031035E-11   2.199565E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.942081        1.00000        1.00000       2.824466E-02   1.577193E-03   2.698324E-02   1.113522E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97060877     0.00000000     0.00000000    -0.16806148    -0.03971390     0.16426577    -0.03336948
 ref:   2     0.00000000    -1.00000000     0.00000000    -0.00000000     0.00000000    -0.00000000    -0.00000000
 ref:   3    -0.00000000     0.00000000     1.00000000     0.00000000    -0.00000000    -0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -227.7748712249  1.1736E-04  2.8707E-05  7.3773E-03  1.0000E-04   
 mr-sdci #  5  2   -227.5324466902  5.6843E-14  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  5  3   -227.5320208582  3.4106E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  5  4   -227.0347596501  7.9036E-02  0.0000E+00  4.5926E-01  1.0000E-04   
 mr-sdci #  5  5   -225.8868454490  4.1296E-01  0.0000E+00  1.1755E+00  1.0000E-04   
 mr-sdci #  5  6   -225.3706605685  3.4219E-02  0.0000E+00  7.7056E-01  1.0000E-04   
 mr-sdci #  5  7   -225.0919041067  3.2568E+02  0.0000E+00  1.1976E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000828
time for cinew                         0.028086
time for eigenvalue solver             0.000085
time for vector access                 0.000000

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.26868014
   ht   2    -0.00000000  -328.11799853
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.09096479    -0.00000000     0.00000000   -15.43720274
   ht   5    39.58245556     0.00000053    -0.00000004     0.00860981    -9.36615933
   ht   6    -0.57840949     0.00000009    -0.00000000     0.13998877     0.01126609    -0.23293292
   ht   7    -0.12912840    -0.00000002    -0.00000000    -0.03849524    -0.04589027    -0.00612016    -0.02964332
   ht   8     0.04133363    -0.00000001    -0.00000000    -0.00757871    -0.01536419     0.00442281    -0.00148897    -0.00612256

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970404       2.627125E-09   4.088666E-10  -0.148422      -9.166838E-02   0.117729       0.118391      -2.802875E-03
 ref    2   1.352072E-09   -1.00000       2.469745E-12  -1.189443E-08   5.274047E-09  -5.210038E-10   7.867098E-10  -4.757166E-10
 ref    3  -2.790641E-10   2.469721E-12    1.00000       9.851699E-10  -2.298772E-10  -6.955439E-11  -3.948055E-11   1.794740E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941684        1.00000        1.00000       2.202916E-02   8.403092E-03   1.386008E-02   1.401631E-02   7.856111E-06

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97040378     0.00000000     0.00000000    -0.14842222    -0.09166838     0.11772886     0.11839050    -0.00280288
 ref:   2     0.00000000    -1.00000000     0.00000000    -0.00000001     0.00000001    -0.00000000     0.00000000    -0.00000000
 ref:   3    -0.00000000     0.00000000     1.00000000     0.00000000    -0.00000000    -0.00000000    -0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -227.7748988334  2.7609E-05  3.7403E-06  2.6836E-03  1.0000E-04   
 mr-sdci #  6  2   -227.5324466902 -5.6843E-14  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  6  3   -227.5320208582 -1.7053E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  6  4   -227.1360430562  1.0128E-01  0.0000E+00  4.1224E-01  1.0000E-04   
 mr-sdci #  6  5   -226.5735064665  6.8666E-01  0.0000E+00  6.8406E-01  1.0000E-04   
 mr-sdci #  6  6   -225.4060285204  3.5368E-02  0.0000E+00  1.0138E+00  1.0000E-04   
 mr-sdci #  6  7   -225.2961339252  2.0423E-01  0.0000E+00  8.3801E-01  1.0000E-04   
 mr-sdci #  6  8   -224.4924188904  3.2508E+02  0.0000E+00  1.0884E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000078
time for cinew                         0.028926
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045067
   ht   2     0.00000000  -328.11799853
   ht   3    -0.00000000    -0.00000000  -328.11757269
   ht   4    -0.00000000     0.00000000     0.00000000  -327.72159489
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.15905830
   ht   6     0.08676500     0.00000001     0.00000000    -0.02282826     0.01706276    -0.00080509

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970372      -3.046357E-09  -4.291738E-10   0.135018       0.107777      -2.618101E-02
 ref    2   1.411796E-09    1.00000      -8.755956E-12   1.816926E-08  -7.969961E-09  -1.850314E-09
 ref    3  -2.825486E-10  -8.755935E-12   -1.00000      -1.264231E-09   2.762998E-10   4.543512E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941622        1.00000        1.00000       1.822973E-02   1.161580E-02   6.854453E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037218    -0.00000000    -0.00000000     0.13501751     0.10777660    -0.02618101
 ref:   2     0.00000000     1.00000000    -0.00000000     0.00000002    -0.00000001    -0.00000000
 ref:   3    -0.00000000    -0.00000000    -1.00000000    -0.00000000     0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -227.7749023404  3.5069E-06  1.3115E-06  1.5212E-03  1.0000E-04   
 mr-sdci #  7  2   -227.5324466902  5.6843E-14  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  7  3   -227.5320208582  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  7  4   -227.1737221111  3.7679E-02  0.0000E+00  3.9664E-01  1.0000E-04   
 mr-sdci #  7  5   -226.7430257264  1.6952E-01  0.0000E+00  6.4348E-01  1.0000E-04   
 mr-sdci #  7  6   -225.7080759055  3.0205E-01  0.0000E+00  1.0974E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000312
time for cinew                         0.028942
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045067
   ht   2     0.00000000  -328.11799853
   ht   3    -0.00000000    -0.00000000  -328.11757269
   ht   4    -0.00000000     0.00000000     0.00000000  -327.72159489
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.15905830
   ht   6     0.08676500     0.00000001     0.00000000    -0.02282826     0.01706276    -0.00080509
   ht   7    -0.05658645     0.00000000    -0.00000000    -0.05300455     0.03768714     0.00008014    -0.00031407

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970372      -3.307518E-09  -4.379508E-10   0.109609       0.132923       2.726665E-02   2.156550E-02
 ref    2   1.441672E-09    1.00000       7.885757E-12   2.719339E-08  -8.710230E-09   1.556787E-09   1.071684E-10
 ref    3  -2.825043E-10   7.885824E-12   -1.00000      -1.568267E-09   1.596336E-10   3.170307E-11  -5.939031E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941621        1.00000        1.00000       1.201409E-02   1.766855E-02   7.434700E-04   4.650709E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97037170    -0.00000000    -0.00000000     0.10960882     0.13292311     0.02726665     0.02156550
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000003    -0.00000001     0.00000000     0.00000000
 ref:   3    -0.00000000     0.00000000    -1.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -227.7749034153  1.0750E-06  3.4062E-07  7.5618E-04  1.0000E-04   
 mr-sdci #  8  2   -227.5324466902 -2.8422E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  8  3   -227.5320208582 -1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  8  4   -227.2264271863  5.2705E-02  0.0000E+00  3.7725E-01  1.0000E-04   
 mr-sdci #  8  5   -226.8686008242  1.2558E-01  0.0000E+00  5.5094E-01  1.0000E-04   
 mr-sdci #  8  6   -226.0021894179  2.9411E-01  0.0000E+00  8.3032E-01  1.0000E-04   
 mr-sdci #  8  7   -225.1099885283 -1.8615E-01  0.0000E+00  1.5752E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000698
time for cinew                         0.026930
time for eigenvalue solver             0.000068
time for vector access                 0.000001

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045067
   ht   2     0.00000000  -328.11799853
   ht   3    -0.00000000    -0.00000000  -328.11757269
   ht   4    -0.00000000     0.00000000     0.00000000  -327.72159489
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.15905830
   ht   6     0.08676500     0.00000001     0.00000000    -0.02282826     0.01706276    -0.00080509
   ht   7    -0.05658645     0.00000000    -0.00000000    -0.05300455     0.03768714     0.00008014    -0.00031407
   ht   8     0.03126239     0.00000000     0.00000000    -0.00616017     0.01811584    -0.00001503    -0.00001315    -0.00008384

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970365      -3.504079E-09  -4.390970E-10   9.455078E-02   0.139303       4.396719E-02  -1.067304E-02  -3.350167E-02
 ref    2   1.447922E-09    1.00000      -9.017371E-12   3.238214E-08  -7.045589E-09  -4.385883E-10  -1.612289E-09   5.593335E-10
 ref    3  -2.827217E-10  -9.017309E-12   -1.00000      -1.569095E-09  -9.618132E-11   2.127460E-10   1.314546E-10  -4.771899E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941609        1.00000        1.00000       8.939851E-03   1.940535E-02   1.933114E-03   1.139137E-04   1.122362E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036549    -0.00000000    -0.00000000     0.09455078     0.13930308     0.04396719    -0.01067304    -0.03350167
 ref:   2     0.00000000     1.00000000    -0.00000000     0.00000003    -0.00000001    -0.00000000    -0.00000000     0.00000000
 ref:   3    -0.00000000    -0.00000000    -1.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000    -0.00000000

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -227.7749036200  2.0467E-07  7.9495E-08  3.7473E-04  1.0000E-04   
 mr-sdci #  9  2   -227.5324466902  1.1369E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci #  9  3   -227.5320208582 -5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci #  9  4   -227.2602724140  3.3845E-02  0.0000E+00  3.0749E-01  1.0000E-04   
 mr-sdci #  9  5   -226.9398563335  7.1256E-02  0.0000E+00  4.9832E-01  1.0000E-04   
 mr-sdci #  9  6   -226.2270535447  2.2486E-01  0.0000E+00  8.2935E-01  1.0000E-04   
 mr-sdci #  9  7   -225.6267139128  5.1673E-01  0.0000E+00  1.0343E+00  1.0000E-04   
 mr-sdci #  9  8   -223.9292625474 -5.6316E-01  0.0000E+00  1.7390E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.029911
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045546
   ht   2     0.00000000  -328.11799853
   ht   3     0.00000000    -0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000     0.00000000  -327.84582425
   ht   5     0.00000000    -0.00000000     0.00000000     0.00000000  -327.52540817
   ht   6     0.01537441    -0.00000000     0.00000000    -0.00834557    -0.00840875    -0.00001882

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970366       3.492392E-09   4.394883E-10  -8.406808E-02  -0.139189      -5.019319E-02
 ref    2   1.448887E-09   -1.00000       2.601748E-11  -3.407359E-08   5.000940E-09   1.974679E-09
 ref    3  -2.824231E-10   2.601744E-11    1.00000       1.491709E-09   3.252677E-10  -1.542326E-10

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941611        1.00000        1.00000       7.067442E-03   1.937345E-02   2.519356E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036631     0.00000000     0.00000000    -0.08406808    -0.13918855    -0.05019319
 ref:   2     0.00000000    -1.00000000     0.00000000    -0.00000003     0.00000001     0.00000000
 ref:   3    -0.00000000     0.00000000     1.00000000     0.00000000     0.00000000    -0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -227.7749036824  6.2354E-08  4.2438E-08  2.5972E-04  1.0000E-04   
 mr-sdci # 10  2   -227.5324466902 -2.2737E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci # 10  3   -227.5320208582 -1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 10  4   -227.2714003604  1.1128E-02  0.0000E+00  2.7104E-01  1.0000E-04   
 mr-sdci # 10  5   -227.0113521444  7.1496E-02  0.0000E+00  4.8267E-01  1.0000E-04   
 mr-sdci # 10  6   -225.6825508268 -5.4450E-01  0.0000E+00  1.5930E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000719
time for cinew                         0.029545
time for eigenvalue solver             0.000071
time for vector access                 0.000001

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045546
   ht   2     0.00000000  -328.11799853
   ht   3     0.00000000    -0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000     0.00000000  -327.84582425
   ht   5     0.00000000    -0.00000000     0.00000000     0.00000000  -327.52540817
   ht   6     0.01537441    -0.00000000     0.00000000    -0.00834557    -0.00840875    -0.00001882
   ht   7    -0.01247877    -0.00000000     0.00000000     0.00286980    -0.00600952     0.00000075    -0.00001102

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970369       3.470648E-09   4.439273E-10  -6.023234E-02  -0.131496       8.555759E-02   2.778612E-02
 ref    2   1.448390E-09   -1.00000      -3.832809E-11  -3.397208E-08  -4.259887E-09  -6.655865E-09   6.870963E-10
 ref    3  -2.819942E-10  -3.832802E-11    1.00000       1.199626E-09   9.212652E-10   3.129435E-10  -8.747038E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941617        1.00000        1.00000       3.627935E-03   1.729132E-02   7.320102E-03   7.720682E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036932     0.00000000     0.00000000    -0.06023234    -0.13149647     0.08555759     0.02778612
 ref:   2     0.00000000    -1.00000000    -0.00000000    -0.00000003    -0.00000000    -0.00000001     0.00000000
 ref:   3    -0.00000000    -0.00000000     1.00000000     0.00000000     0.00000000     0.00000000    -0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -227.7749037179  3.5575E-08  1.0395E-08  1.4339E-04  1.0000E-04   
 mr-sdci # 11  2   -227.5324466902  2.8422E-13  0.0000E+00  4.0708E-01  1.0000E-04   
 mr-sdci # 11  3   -227.5320208582  5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 11  4   -227.2889228552  1.7522E-02  0.0000E+00  2.5832E-01  1.0000E-04   
 mr-sdci # 11  5   -227.1319230425  1.2057E-01  0.0000E+00  5.0309E-01  1.0000E-04   
 mr-sdci # 11  6   -226.6090757155  9.2652E-01  0.0000E+00  7.8536E-01  1.0000E-04   
 mr-sdci # 11  7   -224.2731536222 -1.3536E+00  0.0000E+00  1.5431E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.027869
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045546
   ht   2     0.00000000  -328.11799853
   ht   3     0.00000000    -0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000     0.00000000  -327.84582425
   ht   5     0.00000000    -0.00000000     0.00000000     0.00000000  -327.52540817
   ht   6     0.01537441    -0.00000000     0.00000000    -0.00834557    -0.00840875    -0.00001882
   ht   7    -0.01247877    -0.00000000     0.00000000     0.00286980    -0.00600952     0.00000075    -0.00001102
   ht   8    -0.00520278     0.00000000     0.00000000    -0.00018272     0.00017884    -0.00000024    -0.00000028    -0.00000205

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   6.001256E-09   3.190445E-08   5.422490E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -1.688540E-05  -3.627636E-05   4.781749E-05   4.832292E-09  -3.489554E-08   0.316761       0.271253       0.908892    
   x:   2   6.798680E-13  -5.483336E-13  -1.489397E-12  -2.975506E-06   -1.00000      -9.348777E-08   3.759401E-08  -1.703144E-08
   x:   3   7.602816E-14   1.476665E-13   9.849117E-14    1.00000      -2.975506E-06  -9.946706E-09  -5.972441E-09  -6.780430E-11
   x:   4   1.481095E-07   7.723626E-06  -2.587710E-05   1.020940E-08  -6.980972E-08   0.901147       0.212939      -0.377612    
   x:   5   1.530698E-06  -1.932698E-05  -2.466594E-05   2.650375E-09   6.597158E-08  -0.295967       0.938658      -0.176988    
   x:   6  -3.086024E-02   4.011677E-02   0.998718      -2.173872E-20  -8.765535E-19   6.482892E-07   1.663291E-05  -5.670540E-05
   x:   7  -1.041639E-02   0.999127      -4.045506E-02  -1.695230E-20  -1.095296E-18  -1.282622E-06   2.564658E-05   3.460571E-05
   x:   8   0.999469       1.165149E-02   3.041543E-02    0.00000        0.00000       5.677861E-06   3.894378E-06   1.429195E-05
 bummer (warning):overlap matrix: # small eigenvalues=1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970371       3.541823E-09   4.617247E-10  -3.710273E-02  -0.126731      -0.102638      -2.848944E-02   1.631495E-02
 ref    2   1.447534E-09   -1.00000       5.903397E-11  -3.030607E-08  -1.439566E-08   8.215646E-09  -7.437671E-10   2.039853E-10
 ref    3  -2.820724E-10   5.903380E-11    1.00000       6.287509E-10   1.586921E-09  -4.118139E-10   2.009894E-10   3.127560E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941620        1.00000        1.00000       1.376612E-03   1.606081E-02   1.053463E-02   8.116480E-04   2.661775E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037089     0.00000000     0.00000000    -0.03710273    -0.12673125    -0.10263835    -0.02848944     0.01631495
 ref:   2     0.00000000    -1.00000000     0.00000000    -0.00000003    -0.00000001     0.00000001    -0.00000000     0.00000000
 ref:   3    -0.00000000     0.00000000     1.00000000     0.00000000     0.00000000    -0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -227.7749037254  7.5059E-09  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 12  2   -227.5324466902 -5.6843E-14  8.0719E-02  4.0708E-01  1.0000E-04   
 mr-sdci # 12  3   -227.5320208582 -5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 12  4   -227.3015125440  1.2590E-02  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 12  5   -227.1954620518  6.3539E-02  0.0000E+00  3.8065E-01  1.0000E-04   
 mr-sdci # 12  6   -226.7416320258  1.3256E-01  0.0000E+00  5.4295E-01  1.0000E-04   
 mr-sdci # 12  7   -224.9964053502  7.2325E-01  0.0000E+00  1.1322E+00  1.0000E-04   
 mr-sdci # 12  8   -223.8927699616 -3.6493E-02  0.0000E+00  1.6014E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000772
time for cinew                         0.059143
time for eigenvalue solver             0.000207
time for vector access                 0.000001

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2     0.00000000  -328.11799853
   ht   3     0.00000000     0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000    -0.00000000  -327.88706438
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -327.78101389
   ht   6    -0.00000003    -0.08071939    -0.00000000    -0.00000050    -0.00000060   -14.30575717

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970371      -1.983311E-09   4.626062E-10   3.710273E-02  -0.126731       1.109580E-09
 ref    2   1.446407E-09   0.979946       2.307066E-09   9.462924E-09  -6.150221E-09   0.199264    
 ref    3  -2.829165E-10  -2.260274E-09    1.00000      -6.280899E-10   1.587607E-09  -4.623028E-10

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941620       0.960294        1.00000       1.376612E-03   1.606081E-02   3.970625E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037089    -0.00000000     0.00000000     0.03710273    -0.12673125     0.00000000
 ref:   2     0.00000000     0.97994579     0.00000000     0.00000001    -0.00000001     0.19926427
 ref:   3    -0.00000000    -0.00000000     1.00000000    -0.00000000     0.00000000    -0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -227.7749037254  5.6843E-14  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 13  2   -227.6108363412  7.8390E-02  1.0322E-02  1.1183E-01  1.0000E-04   
 mr-sdci # 13  3   -227.5320208582  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 13  4   -227.3015125440 -1.1369E-13  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 13  5   -227.1954620518  2.8422E-13  0.0000E+00  3.8065E-01  1.0000E-04   
 mr-sdci # 13  6   -225.6365967193 -1.1050E+00  0.0000E+00  8.5895E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000115
time for cinew                         0.037702
time for eigenvalue solver             0.000188
time for vector access                 0.000000

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2     0.00000000  -328.11799853
   ht   3     0.00000000     0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000    -0.00000000  -327.88706438
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -327.78101389
   ht   6    -0.00000003    -0.08071939    -0.00000000    -0.00000050    -0.00000060   -14.30575717
   ht   7     0.00000307    17.42776112     0.00000009     0.00000038     0.00000069     0.10777416    -4.59204131

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970371      -2.093908E-09  -4.621735E-10   3.710273E-02   0.126731      -6.433386E-09   7.427122E-10
 ref    2  -1.445939E-09   0.968341      -4.629087E-09   5.344366E-09   1.262922E-09  -0.186669      -0.165742    
 ref    3   2.825461E-10  -4.277496E-09   -1.00000      -6.292401E-10  -1.586692E-09   2.548250E-09   6.837372E-11

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941620       0.937684        1.00000       1.376612E-03   1.606081E-02   3.484540E-02   2.747046E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037089    -0.00000000    -0.00000000     0.03710273     0.12673125    -0.00000001     0.00000000
 ref:   2    -0.00000000     0.96834092    -0.00000000     0.00000001     0.00000000    -0.18666923    -0.16574215
 ref:   3     0.00000000    -0.00000000    -1.00000000    -0.00000000    -0.00000000     0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -227.7749037254  1.7053E-13  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 14  2   -227.6214480839  1.0612E-02  2.2111E-03  5.9317E-02  1.0000E-04   
 mr-sdci # 14  3   -227.5320208582 -1.7053E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 14  4   -227.3015125440 -5.6843E-14  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 14  5   -227.1954620518 -1.7053E-13  0.0000E+00  3.8065E-01  1.0000E-04   
 mr-sdci # 14  6   -226.8035816198  1.1670E+00  0.0000E+00  7.1866E-01  1.0000E-04   
 mr-sdci # 14  7   -225.4189911119  4.2259E-01  0.0000E+00  7.8099E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000840
time for cinew                         0.029511
time for eigenvalue solver             0.000090
time for vector access                 0.000000

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2     0.00000000  -328.11799853
   ht   3     0.00000000     0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000    -0.00000000  -327.88706438
   ht   5    -0.00000000    -0.00000000    -0.00000000     0.00000000  -327.78101389
   ht   6    -0.00000003    -0.08071939    -0.00000000    -0.00000050    -0.00000060   -14.30575717
   ht   7     0.00000307    17.42776112     0.00000009     0.00000038     0.00000069     0.10777416    -4.59204131
   ht   8    -0.00001793     0.66259606     0.00000004     0.00000004    -0.00000154     0.30405093     0.03223234    -0.68980431

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970371       7.946982E-10  -4.621750E-10   3.710273E-02  -0.126731       1.854074E-08   4.888093E-08  -6.123835E-09
 ref    2  -1.444953E-09  -0.964321      -7.265847E-09   1.221164E-09  -1.285653E-08  -0.202594       2.506563E-02  -0.168559    
 ref    3   2.825467E-10   6.302310E-09   -1.00000      -6.287650E-10   1.586839E-09   6.286222E-09   2.116544E-09  -1.904587E-10

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941620       0.929915        1.00000       1.376612E-03   1.606081E-02   4.104445E-02   6.282856E-04   2.841202E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037089     0.00000000    -0.00000000     0.03710273    -0.12673125     0.00000002     0.00000005    -0.00000001
 ref:   2    -0.00000000    -0.96432113    -0.00000001     0.00000000    -0.00000001    -0.20259430     0.02506563    -0.16855865
 ref:   3     0.00000000     0.00000001    -1.00000000    -0.00000000     0.00000000     0.00000001     0.00000000    -0.00000000

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -227.7749037254 -2.2737E-13  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 15  2   -227.6231003773  1.6523E-03  7.0992E-04  3.1578E-02  1.0000E-04   
 mr-sdci # 15  3   -227.5320208582  2.2737E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 15  4   -227.3015125440 -1.1369E-13  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 15  5   -227.1954620518  0.0000E+00  0.0000E+00  3.8065E-01  1.0000E-04   
 mr-sdci # 15  6   -226.9732708605  1.6969E-01  0.0000E+00  5.3921E-01  1.0000E-04   
 mr-sdci # 15  7   -225.8369369978  4.1795E-01  0.0000E+00  1.2710E+00  1.0000E-04   
 mr-sdci # 15  8   -225.4106714273  1.5179E+00  0.0000E+00  7.5054E-01  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000041
time for cinew                         0.029677
time for eigenvalue solver             0.000092
time for vector access                 0.000000

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20865221
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4    -0.00000000     0.00000000    -0.00000000  -327.88706438
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.78101389
   ht   6     0.00007790    -0.36479345     0.00000009    -0.00000006    -0.00000710    -0.27778938

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970371      -5.423865E-09  -4.621662E-10   3.710273E-02   0.126731       3.582707E-07
 ref    2   1.447386E-09   0.963606      -9.427137E-09   4.106319E-09  -5.437146E-09   5.061934E-02
 ref    3  -2.825438E-10  -8.154161E-09   -1.00000      -6.287106E-10  -1.586787E-09  -7.540269E-09

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941620       0.928536        1.00000       1.376612E-03   1.606081E-02   2.562318E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037089    -0.00000001    -0.00000000     0.03710273     0.12673125     0.00000036
 ref:   2     0.00000000     0.96360582    -0.00000001     0.00000000    -0.00000001     0.05061934
 ref:   3    -0.00000000    -0.00000001    -1.00000000    -0.00000000    -0.00000000    -0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -227.7749037254  5.6843E-14  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 16  2   -227.6235104197  4.1004E-04  2.3109E-04  1.7944E-02  1.0000E-04   
 mr-sdci # 16  3   -227.5320208582  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 16  4   -227.3015125440  2.2737E-13  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 16  5   -227.1954620518 -1.7053E-13  0.0000E+00  3.8065E-01  1.0000E-04   
 mr-sdci # 16  6   -226.1751938394 -7.9808E-01  0.0000E+00  1.3853E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.026986
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20865221
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4    -0.00000000     0.00000000    -0.00000000  -327.88706438
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.78101389
   ht   6     0.00007790    -0.36479345     0.00000009    -0.00000006    -0.00000710    -0.27778938
   ht   7     0.00024971     0.10580072    -0.00000001    -0.00000010    -0.00002265    -0.03738222    -0.07829998

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970371      -2.092968E-08  -4.620728E-10  -3.710273E-02   0.126731       1.868921E-06  -1.526487E-06
 ref    2   1.444231E-09  -0.962014      -1.588935E-08   5.162952E-08   4.077692E-07  -9.863311E-02   1.083868E-03
 ref    3  -2.825329E-10   1.193691E-08   -1.00000       6.277253E-10  -1.586936E-09   3.530604E-08  -3.325339E-09

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941620       0.925470        1.00000       1.376612E-03   1.606081E-02   9.728490E-03   1.174772E-06

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97037089    -0.00000002    -0.00000000    -0.03710273     0.12673125     0.00000187    -0.00000153
 ref:   2     0.00000000    -0.96201371    -0.00000002     0.00000005     0.00000041    -0.09863311     0.00108387
 ref:   3    -0.00000000     0.00000001    -1.00000000     0.00000000    -0.00000000     0.00000004    -0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -227.7749037254 -1.1369E-13  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 17  2   -227.6237773383  2.6692E-04  6.7357E-05  1.0104E-02  1.0000E-04   
 mr-sdci # 17  3   -227.5320208582 -1.7053E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 17  4   -227.3015125440  5.6843E-14  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 17  5   -227.1954620518  3.9790E-12  0.0000E+00  3.8065E-01  1.0000E-04   
 mr-sdci # 17  6   -226.9169733004  7.4178E-01  0.0000E+00  7.6843E-01  1.0000E-04   
 mr-sdci # 17  7   -225.7906440828 -4.6293E-02  0.0000E+00  1.4604E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000189
time for cinew                         0.037541
time for eigenvalue solver             0.000000
time for vector access                 0.000001

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20865221
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4    -0.00000000     0.00000000    -0.00000000  -327.88706438
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.78101389
   ht   6     0.00007790    -0.36479345     0.00000009    -0.00000006    -0.00000710    -0.27778938
   ht   7     0.00024971     0.10580072    -0.00000001    -0.00000010    -0.00002265    -0.03738222    -0.07829998
   ht   8     0.00161866    -0.61124817     0.00000006    -0.00000032    -0.00014679    -0.02385942     0.00158306    -0.02111098

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970371       1.342285E-07  -4.632867E-10   3.710273E-02  -9.655600E-05   0.126731       5.421268E-06   2.656464E-05
 ref    2   1.285287E-09  -0.960797      -2.703079E-08   1.615177E-06   0.128337       1.070010E-04   1.708133E-03   2.947211E-02
 ref    3  -2.828850E-10   1.521100E-08   -1.00000      -6.303783E-10  -8.921796E-08  -1.659382E-09   2.186703E-09  -8.552847E-09

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941620       0.923130        1.00000       1.376612E-03   1.647040E-02   1.606081E-02   2.917747E-06   8.686058E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037089     0.00000013    -0.00000000     0.03710273    -0.00009656     0.12673122     0.00000542     0.00002656
 ref:   2     0.00000000    -0.96079664    -0.00000003     0.00000162     0.12833701     0.00010700     0.00170813     0.02947211
 ref:   3    -0.00000000     0.00000002    -1.00000000    -0.00000000    -0.00000009    -0.00000000     0.00000000    -0.00000001

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -227.7749037254 -1.7053E-13  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 18  2   -227.6238436561  6.6318E-05  1.8350E-05  5.7113E-03  1.0000E-04   
 mr-sdci # 18  3   -227.5320208582 -2.2737E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 18  4   -227.3015125441  1.8247E-11  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 18  5   -227.2037786820  8.3166E-03  0.0000E+00  4.4731E-01  1.0000E-04   
 mr-sdci # 18  6   -227.1954620462  2.7849E-01  0.0000E+00  3.8065E-01  1.0000E-04   
 mr-sdci # 18  7   -225.7969050798  6.2610E-03  0.0000E+00  1.4670E+00  1.0000E-04   
 mr-sdci # 18  8   -225.5003764928  8.9705E-02  0.0000E+00  1.0896E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000002
time for cinew                         0.033696
time for eigenvalue solver             0.000077
time for vector access                 0.000000

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20939549
   ht   3     0.00000000    -0.00000000  -328.11757269
   ht   4     0.00000000     0.00000000    -0.00000000  -327.88706438
   ht   5    -0.00000000     0.00000000     0.00000000    -0.00000000  -327.78933052
   ht   6     0.00938901     0.53987748     0.00000009    -0.00000085    -0.21852967    -0.00499995

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970371      -7.442854E-07   4.443922E-10  -3.710277E-02   1.285429E-04  -2.012392E-04
 ref    2  -4.525349E-09  -0.960039       4.655085E-08   4.408638E-05  -0.159077       0.129663    
 ref    3   2.826184E-10   1.782798E-08    1.00000       5.787907E-10   1.591989E-07  -2.482186E-08

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941620       0.921674        1.00000       1.376617E-03   2.530548E-02   1.681263E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037089    -0.00000074     0.00000000    -0.03710277     0.00012854    -0.00020124
 ref:   2    -0.00000000    -0.96003854     0.00000005     0.00004409    -0.15907691     0.12966338
 ref:   3     0.00000000     0.00000002     1.00000000     0.00000000     0.00000016    -0.00000002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -227.7749037254  2.8422E-13  0.0000E+00  5.9036E-05  1.0000E-04   
 mr-sdci # 19  2   -227.6238583605  1.4704E-05  5.4115E-06  3.0236E-03  1.0000E-04   
 mr-sdci # 19  3   -227.5320208582  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 19  4   -227.3015125464  2.3135E-09  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 19  5   -227.2775993930  7.3821E-02  0.0000E+00  2.7352E-01  1.0000E-04   
 mr-sdci # 19  6   -225.3401254491 -1.8553E+00  0.0000E+00  1.2201E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000095
time for cinew                         0.029656
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20939549
   ht   3     0.00000000    -0.00000000  -328.11757269
   ht   4     0.00000000     0.00000000    -0.00000000  -327.88706438
   ht   5    -0.00000000     0.00000000     0.00000000    -0.00000000  -327.78933052
   ht   6     0.00938901     0.53987748     0.00000009    -0.00000085    -0.21852967    -0.00499995
   ht   7    -0.04353007    -0.02062217     0.00000005    -0.00001317    -0.06948149     0.00035035    -0.00134242

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970371      -2.066105E-06  -6.149598E-10  -3.710280E-02   1.659239E-04   1.369872E-03   1.444321E-03
 ref    2  -1.597077E-08   0.959743      -6.997178E-08  -1.146100E-03  -0.172583       0.131519      -5.096118E-02
 ref    3  -2.822611E-10  -2.019257E-08   -1.00000       2.275533E-09   2.331728E-07  -8.153610E-08  -1.493340E-08

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941620       0.921106        1.00000       1.377931E-03   2.978497E-02   1.729905E-02   2.599128E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97037089    -0.00000207    -0.00000000    -0.03710280     0.00016592     0.00136987     0.00144432
 ref:   2    -0.00000002     0.95974293    -0.00000007    -0.00114610    -0.17258314     0.13151872    -0.05096118
 ref:   3    -0.00000000    -0.00000002    -1.00000000     0.00000000     0.00000023    -0.00000008    -0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -227.7749037254  1.1369E-13  0.0000E+00  5.9035E-05  1.0000E-04   
 mr-sdci # 20  2   -227.6238623960  4.0354E-06  1.0662E-06  1.1784E-03  1.0000E-04   
 mr-sdci # 20  3   -227.5320208582  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 20  4   -227.3015127784  2.3207E-07  0.0000E+00  2.2692E-01  1.0000E-04   
 mr-sdci # 20  5   -227.2973493805  1.9750E-02  0.0000E+00  1.9921E-01  1.0000E-04   
 mr-sdci # 20  6   -226.0308458392  6.9072E-01  0.0000E+00  8.2310E-01  1.0000E-04   
 mr-sdci # 20  7   -224.8296747953 -9.6723E-01  0.0000E+00  1.5201E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000074
time for cinew                         0.027961
time for eigenvalue solver             0.000000
time for vector access                 0.000001

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20939549
   ht   3     0.00000000    -0.00000000  -328.11757269
   ht   4     0.00000000     0.00000000    -0.00000000  -327.88706438
   ht   5    -0.00000000     0.00000000     0.00000000    -0.00000000  -327.78933052
   ht   6     0.00938901     0.53987748     0.00000009    -0.00000085    -0.21852967    -0.00499995
   ht   7    -0.04353007    -0.02062217     0.00000005    -0.00001317    -0.06948149     0.00035035    -0.00134242
   ht   8    -0.17548182     0.15194555    -0.00000006    -0.00000950     0.01979708    -0.00019438    -0.00003176    -0.00050853

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970371       7.652577E-06  -2.109081E-09   2.888258E-03  -3.703042E-02  -8.450639E-03   1.068598E-02   6.906678E-03
 ref    2  -1.071476E-08   0.959711       8.467503E-08  -0.172642      -8.812488E-03   5.678257E-02   0.133407      -1.429072E-02
 ref    3   2.813067E-10  -2.204732E-08    1.00000       3.230964E-07   1.498186E-08  -2.100612E-07   3.708252E-08   7.230795E-09

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941620       0.921046        1.00000       2.981365E-02   1.448912E-03   3.295673E-03   1.791172E-02   2.519269E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037087     0.00000765    -0.00000000     0.00288826    -0.03703042    -0.00845064     0.01068598     0.00690668
 ref:   2    -0.00000001     0.95971131     0.00000008    -0.17264213    -0.00881249     0.05678257     0.13340739    -0.01429072
 ref:   3     0.00000000    -0.00000002     1.00000000     0.00000032     0.00000001    -0.00000021     0.00000004     0.00000001

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -227.7749037254  2.5011E-12  0.0000E+00  5.8978E-05  1.0000E-04   
 mr-sdci # 21  2   -227.6238630756  6.7967E-07  2.6219E-07  5.3019E-04  1.0000E-04   
 mr-sdci # 21  3   -227.5320208582  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 21  4   -227.3043467717  2.8340E-03  0.0000E+00  1.7279E-01  1.0000E-04   
 mr-sdci # 21  5   -227.3015094528  4.1601E-03  0.0000E+00  2.2687E-01  1.0000E-04   
 mr-sdci # 21  6   -226.4604736502  4.2963E-01  0.0000E+00  8.1873E-01  1.0000E-04   
 mr-sdci # 21  7   -225.4954489356  6.6577E-01  0.0000E+00  9.9092E-01  1.0000E-04   
 mr-sdci # 21  8   -224.6969657573 -8.0341E-01  0.0000E+00  1.5520E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000835
time for cinew                         0.029318
time for eigenvalue solver             0.000081
time for vector access                 0.000000

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20941491
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000    -0.00000000  -327.88989861
   ht   5     0.00000000     0.00000000     0.00000000    -0.00000000  -327.88706129
   ht   6    -0.59316790    -0.01913271     0.00000006     0.00054958    -0.00008717    -0.00128916

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970371       6.280323E-06   1.426878E-08   2.733640E-03   3.755454E-02  -6.518205E-02
 ref    2   1.114903E-08  -0.959709       8.988720E-08   0.172219      -7.257423E-03   1.852654E-02
 ref    3  -2.893041E-10   2.284677E-08    1.00000      -3.749695E-07   5.247749E-09   2.233384E-07

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921042        1.00000       2.966694E-02   1.463014E-03   4.591932E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037053     0.00000628     0.00000001     0.00273364     0.03755454    -0.06518205
 ref:   2     0.00000001    -0.95970940     0.00000009     0.17221924    -0.00725742     0.01852654
 ref:   3    -0.00000000     0.00000002     1.00000000    -0.00000037     0.00000001     0.00000022

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -227.7749037255  4.4849E-11  0.0000E+00  5.7854E-05  1.0000E-04   
 mr-sdci # 22  2   -227.6238631431  6.7492E-08  1.7209E-07  4.1307E-04  1.0000E-04   
 mr-sdci # 22  3   -227.5320208582  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 22  4   -227.3056998777  1.3531E-03  0.0000E+00  1.6696E-01  1.0000E-04   
 mr-sdci # 22  5   -227.3015830207  7.3568E-05  0.0000E+00  2.2581E-01  1.0000E-04   
 mr-sdci # 22  6   -226.0801454266 -3.8033E-01  0.0000E+00  1.0805E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000297
time for cinew                         0.027951
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20941491
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000    -0.00000000  -327.88989861
   ht   5     0.00000000     0.00000000     0.00000000    -0.00000000  -327.88706129
   ht   6    -0.59316790    -0.01913271     0.00000006     0.00054958    -0.00008717    -0.00128916
   ht   7    -0.50447588     0.01673761    -0.00000005     0.00268476     0.00000902    -0.00104107    -0.00092880

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -7.658072E-07   3.453552E-09   6.585631E-04   3.772619E-02  -6.807393E-03   7.034084E-02
 ref    2  -4.391064E-08  -0.959712      -9.544516E-08  -0.169310       2.098517E-03  -5.319101E-02  -4.205713E-03
 ref    3  -2.812674E-10   2.570995E-08   -1.00000       6.369855E-07  -2.090392E-10  -8.879668E-07  -3.021875E-08

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941619       0.921048        1.00000       2.866646E-02   1.427669E-03   2.875625E-03   4.965522E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97037047    -0.00000077     0.00000000     0.00065856     0.03772619    -0.00680739     0.07034084
 ref:   2    -0.00000004    -0.95971228    -0.00000010    -0.16931043     0.00209852    -0.05319101    -0.00420571
 ref:   3    -0.00000000     0.00000003    -1.00000000     0.00000064    -0.00000000    -0.00000089    -0.00000003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -227.7749037255  8.8676E-12  0.0000E+00  5.7567E-05  1.0000E-04   
 mr-sdci # 23  2   -227.6238632580  1.1483E-07  6.2482E-08  3.0169E-04  1.0000E-04   
 mr-sdci # 23  3   -227.5320208582  6.8212E-13  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 23  4   -227.3095283453  3.8285E-03  0.0000E+00  1.6117E-01  1.0000E-04   
 mr-sdci # 23  5   -227.3016071736  2.4153E-05  0.0000E+00  2.2550E-01  1.0000E-04   
 mr-sdci # 23  6   -226.4983188297  4.1817E-01  0.0000E+00  1.1210E+00  1.0000E-04   
 mr-sdci # 23  7   -226.0402310359  5.4478E-01  0.0000E+00  1.0469E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000090
time for cinew                         0.026920
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20941491
   ht   3    -0.00000000     0.00000000  -328.11757269
   ht   4     0.00000000    -0.00000000    -0.00000000  -327.88989861
   ht   5     0.00000000     0.00000000     0.00000000    -0.00000000  -327.88706129
   ht   6    -0.59316790    -0.01913271     0.00000006     0.00054958    -0.00008717    -0.00128916
   ht   7    -0.50447588     0.01673761    -0.00000005     0.00268476     0.00000902    -0.00104107    -0.00092880
   ht   8     0.05239713     0.00694320    -0.00000003    -0.00248060    -0.00011619     0.00010996     0.00009013    -0.00002643

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370      -4.243356E-09  -1.358825E-09   1.434842E-04  -3.773586E-02  -3.858600E-04  -7.077654E-02   3.208966E-05
 ref    2  -1.050633E-08  -0.959717      -7.666894E-08  -0.165621      -4.693794E-04   6.451632E-02  -1.690624E-04   9.877092E-03
 ref    3  -2.858618E-10   2.845619E-08   -1.00000       1.025294E-06   5.765689E-09   2.079461E-06   5.206592E-10  -1.949249E-07

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941619       0.921057        1.00000       2.743036E-02   1.424216E-03   4.162505E-03   5.009347E-03   9.755797E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047    -0.00000000    -0.00000000     0.00014348    -0.03773586    -0.00038586    -0.07077654     0.00003209
 ref:   2    -0.00000001    -0.95971692    -0.00000008    -0.16562107    -0.00046938     0.06451632    -0.00016906     0.00987709
 ref:   3    -0.00000000     0.00000003    -1.00000000     0.00000103     0.00000001     0.00000208     0.00000000    -0.00000019

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -227.7749037255  1.8758E-12  0.0000E+00  5.7530E-05  1.0000E-04   
 mr-sdci # 24  2   -227.6238633037  4.5788E-08  1.0039E-08  1.3303E-04  1.0000E-04   
 mr-sdci # 24  3   -227.5320208582  1.6485E-12  0.0000E+00  4.0682E-01  1.0000E-04   
 mr-sdci # 24  4   -227.3119068366  2.3785E-03  0.0000E+00  1.5255E-01  1.0000E-04   
 mr-sdci # 24  5   -227.3016099240  2.7504E-06  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 24  6   -226.9579946354  4.5968E-01  0.0000E+00  5.8775E-01  1.0000E-04   
 mr-sdci # 24  7   -226.0470808164  6.8498E-03  0.0000E+00  1.0399E+00  1.0000E-04   
 mr-sdci # 24  8   -224.8061318005  1.0917E-01  0.0000E+00  1.1457E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000788
time for cinew                         0.030370
time for eigenvalue solver             0.000077
time for vector access                 0.000000

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20941514
   ht   3    -0.00000000    -0.00000000  -328.11757269
   ht   4    -0.00000000    -0.00000000    -0.00000000  -327.89745867
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6     0.00207926     0.00101352     0.00000007     0.00139737    -0.00000281    -0.00000246

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   7.457074E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   6.332256E-06  -0.322624       0.409550      -2.201911E-02   0.368218       0.769489    
   x:   2   3.088068E-06   0.630964       0.324355      -4.513407E-02  -0.594829       0.375259    
   x:   3   1.995095E-10  -0.704914       7.845476E-02  -4.734420E-06  -0.704941       2.424421E-05
   x:   4   4.252708E-06   2.228851E-02  -0.845185       6.757920E-02  -0.116333       0.516784    
   x:   5  -8.558416E-09   1.993531E-02   8.106246E-02   0.996449      -1.091962E-02  -1.047136E-03
   x:   6    1.00000        0.00000        0.00000      -5.885434E-11    0.00000      -8.229169E-06
 bummer (warning):overlap matrix: # small eigenvalues=1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370       1.283042E-07  -2.292614E-09   1.878904E-04   3.773588E-02  -1.998278E-03
 ref    2   9.237080E-09   0.959715       1.450741E-07  -0.165833       4.771651E-04   7.893389E-03
 ref    3   2.854500E-10  -3.058824E-08    1.00000       1.349034E-06  -5.602365E-09  -2.212443E-06

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921053        1.00000       2.750065E-02   1.424225E-03   6.629871E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037047     0.00000013    -0.00000000     0.00018789     0.03773588    -0.00199828
 ref:   2     0.00000001     0.95971512     0.00000015    -0.16583309     0.00047717     0.00789339
 ref:   3     0.00000000    -0.00000003     1.00000000     0.00000135    -0.00000001    -0.00000221

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -227.7749037255  2.2737E-13  0.0000E+00  5.7529E-05  1.0000E-04   
 mr-sdci # 25  2   -227.6238633109  7.1346E-09  0.0000E+00  7.6220E-05  1.0000E-04   
 mr-sdci # 25  3   -227.5320208582  6.1391E-12  8.0626E-02  4.0682E-01  1.0000E-04   
 mr-sdci # 25  4   -227.3126265042  7.1967E-04  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 25  5   -227.3016099244  3.6141E-10  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 25  6   -225.7251343483 -1.2329E+00  0.0000E+00  1.3572E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000515
time for cinew                         0.056826
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20941514
   ht   3    -0.00000000    -0.00000000  -328.11757269
   ht   4    -0.00000000    -0.00000000    -0.00000000  -327.89745867
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6     0.00207926     0.00101352     0.00000007     0.00139737    -0.00000281    -0.00000246
   ht   7     0.00011645     0.00014939     0.08062621    -0.00007869    -0.00000117     0.00000003   -14.29418070

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   7.457074E-09   4.380696E-02    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -6.332256E-06   3.708730E-07  -0.127841       0.590499       4.571183E-02  -0.199852       0.770025    
   x:   2  -3.088068E-06   4.760188E-07   0.143615      -0.667021      -5.804193E-02  -0.623715       0.376920    
   x:   3  -1.995095E-10  -5.415166E-12   0.968555       0.218831      -0.118382       2.419250E-05   2.417986E-05
   x:   4  -4.252708E-06  -2.500153E-07   8.575818E-02  -0.394806      -2.790462E-02   0.755647       0.514774    
   x:   5   8.558416E-09  -3.736575E-09  -0.132582       5.134168E-02  -0.989822       6.038586E-03  -1.056111E-03
   x:   6   -1.00000      -1.904050E-09  -1.852695E-12   8.678124E-12   8.840633E-13  -2.190693E-08  -8.229140E-06
   x:   7  -1.901295E-09    1.00000        0.00000        0.00000        0.00000       5.599655E-07  -3.363047E-07
 bummer (warning):overlap matrix: # small eigenvalues=1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370       1.283042E-07   7.490668E-09  -1.878904E-04   3.773588E-02   1.998278E-03   8.168254E-08
 ref    2   9.237082E-09   0.959715      -5.828935E-08   0.165833       4.771651E-04  -7.893389E-03  -1.438696E-07
 ref    3   2.857261E-10  -5.417957E-08  -0.979989      -3.438933E-07  -4.385979E-09   1.294695E-05  -0.199053    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941619       0.921053       0.960378       2.750065E-02   1.424225E-03   6.629887E-05   3.962209E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037047     0.00000013     0.00000001    -0.00018789     0.03773588     0.00199828     0.00000008
 ref:   2     0.00000001     0.95971512    -0.00000006     0.16583309     0.00047717    -0.00789339    -0.00000014
 ref:   3     0.00000000    -0.00000005    -0.97998873    -0.00000034    -0.00000000     0.00001295    -0.19905299

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -227.7749037255 -1.1369E-13  0.0000E+00  5.7529E-05  1.0000E-04   
 mr-sdci # 26  2   -227.6238633109  5.6843E-14  0.0000E+00  7.6220E-05  1.0000E-04   
 mr-sdci # 26  3   -227.6102650840  7.8244E-02  1.0237E-02  1.1156E-01  1.0000E-04   
 mr-sdci # 26  4   -227.3126265042  7.9581E-13  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 26  5   -227.3016099244  3.4106E-13  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 26  6   -225.7251343485  2.3704E-10  0.0000E+00  1.3572E+00  1.0000E-04   
 mr-sdci # 26  7   -225.6355024809 -4.1158E-01  0.0000E+00  8.5742E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000532
time for cinew                         0.029311
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20941514
   ht   3    -0.00000000    -0.00000000  -328.11757269
   ht   4    -0.00000000    -0.00000000    -0.00000000  -327.89745867
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6     0.00207926     0.00101352     0.00000007     0.00139737    -0.00000281    -0.00000246
   ht   7     0.00011645     0.00014939     0.08062621    -0.00007869    -0.00000117     0.00000003   -14.29418070
   ht   8     0.00166005     0.00006308   -17.31011326    -0.00001101    -0.00000660     0.00000000     0.09538857    -4.51053362

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   7.457074E-09   1.096197E-02   4.380878E-02    1.00000        1.00000        1.00000        1.00000        1.00281    
 
   x:   1   6.332256E-06   5.106889E-06  -3.315689E-07  -0.503043       0.337419      -0.200410      -0.770021       9.569202E-05
   x:   2   3.088068E-06   1.974554E-07  -4.745128E-07   0.565327      -0.387424      -0.623090      -0.376918       3.635467E-06
   x:   3   4.770271E-11  -5.326340E-02  -4.100447E-04  -4.614788E-05   3.138117E-05  -2.195538E-05  -7.483565E-05  -0.998580    
   x:   4   4.252708E-06  -3.565103E-08   2.497482E-07   0.337387      -0.222750       0.756011      -0.514781      -6.334980E-07
   x:   5  -8.558416E-09  -2.035245E-08   3.580004E-09  -0.559931      -0.828513       6.489170E-03   1.056063E-03  -3.810456E-07
   x:   6    1.00000      -2.917895E-09   1.898460E-09   2.831782E-11  -3.084687E-11  -2.184805E-08   8.229140E-06  -4.136015E-10
   x:   7   1.917377E-09   7.443280E-03  -0.999972      -2.813312E-10   4.946580E-10   5.599676E-07   3.363007E-07   1.359819E-05
   x:   8   2.877532E-09   0.998553       7.431990E-03   6.106806E-20  -1.852889E-19  -2.626392E-16  -6.512696E-14  -5.326498E-02
 bummer (warning):overlap matrix: # small eigenvalues=1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370      -1.283042E-07  -1.284163E-07   1.878904E-04  -3.773588E-02  -1.154692E-06  -1.998278E-03   4.678126E-07
 ref    2   9.237029E-09  -0.959715      -5.641991E-08  -0.165833      -4.771651E-04   1.912245E-07   7.893389E-03  -1.879864E-07
 ref    3   2.844293E-10   1.168372E-07  -0.968497       7.871037E-08  -3.408463E-09   0.186039      -7.522750E-06   0.165538    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941619       0.921053       0.937987       2.750065E-02   1.424225E-03   3.461053E-02   6.629876E-05   2.740271E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037047    -0.00000013    -0.00000013     0.00018789    -0.03773588    -0.00000115    -0.00199828     0.00000047
 ref:   2     0.00000001    -0.95971512    -0.00000006    -0.16583309    -0.00047717     0.00000019     0.00789339    -0.00000019
 ref:   3     0.00000000     0.00000012    -0.96849716     0.00000008    -0.00000000     0.18603907    -0.00000752     0.16553764

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -227.7749037255  5.6843E-14  0.0000E+00  5.7529E-05  1.0000E-04   
 mr-sdci # 27  2   -227.6238633109 -2.2737E-13  0.0000E+00  7.6220E-05  1.0000E-04   
 mr-sdci # 27  3   -227.6208398592  1.0575E-02  2.0938E-03  5.8110E-02  1.0000E-04   
 mr-sdci # 27  4   -227.3126265042  3.9790E-13  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 27  5   -227.3016099244 -3.4106E-13  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 27  6   -226.7979956175  1.0729E+00  0.0000E+00  7.1305E-01  1.0000E-04   
 mr-sdci # 27  7   -225.7251343484  8.9632E-02  0.0000E+00  1.3572E+00  1.0000E-04   
 mr-sdci # 27  8   -225.4188705925  6.1274E-01  0.0000E+00  7.7891E-01  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000745
time for cinew                         0.036642
time for eigenvalue solver             0.000204
time for vector access                 0.000000

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000     0.00000000  -328.20639169
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817834
   ht   5    -0.00000000    -0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6     0.01167760    -0.00001051    -0.90632425     0.00001886     0.00003856    -0.64404644

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370       1.283045E-07   5.525167E-07  -1.878904E-04  -3.773588E-02   2.300324E-05
 ref    2  -9.237213E-09   0.959715      -1.496131E-07   0.165833      -4.771651E-04  -1.017815E-06
 ref    3  -2.795570E-10  -1.930493E-07  -0.967629       2.180943E-09  -7.979840E-09   4.367021E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921053       0.936305       2.750065E-02   1.424225E-03   1.907088E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037047     0.00000013     0.00000055    -0.00018789    -0.03773588     0.00002300
 ref:   2    -0.00000001     0.95971512    -0.00000015     0.16583309    -0.00047717    -0.00000102
 ref:   3    -0.00000000    -0.00000019    -0.96762875     0.00000000    -0.00000001     0.04367021

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -227.7749037255  0.0000E+00  0.0000E+00  5.7529E-05  1.0000E-04   
 mr-sdci # 28  2   -227.6238633109 -2.8422E-13  0.0000E+00  7.6220E-05  1.0000E-04   
 mr-sdci # 28  3   -227.6222381631  1.3983E-03  6.6939E-04  2.7889E-02  1.0000E-04   
 mr-sdci # 28  4   -227.3126265042  1.7053E-13  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 28  5   -227.3016099244  5.1159E-13  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 28  6   -226.0246183455 -7.7338E-01  0.0000E+00  1.2971E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000729
time for cinew                         0.027905
time for eigenvalue solver             0.000000
time for vector access                 0.000002

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000     0.00000000  -328.20639169
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817834
   ht   5    -0.00000000    -0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6     0.01167760    -0.00001051    -0.90632425     0.00001886     0.00003856    -0.64404644
   ht   7    -0.04747058    -0.00002741    -1.47714770    -0.00001043    -0.00015006     0.10104368    -0.30106650

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370       1.283026E-07   2.949440E-06  -1.878903E-04  -3.773588E-02  -1.155817E-04  -7.891388E-05
 ref    2   9.237121E-09   0.959715       3.954331E-07   0.165833      -4.771651E-04  -1.345814E-06   4.343533E-07
 ref    3   2.866852E-10  -3.907122E-07   0.964916      -1.965654E-07   5.630261E-08   0.115728      -1.130330E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941619       0.921053       0.931064       2.750065E-02   1.424225E-03   1.339292E-02   1.283874E-06

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037047     0.00000013     0.00000295    -0.00018789    -0.03773588    -0.00011558    -0.00007891
 ref:   2     0.00000001     0.95971512     0.00000040     0.16583309    -0.00047717    -0.00000135     0.00000043
 ref:   3     0.00000000    -0.00000039     0.96491642    -0.00000020     0.00000006     0.11572773    -0.00113033

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -227.7749037255  0.0000E+00  0.0000E+00  5.7529E-05  1.0000E-04   
 mr-sdci # 29  2   -227.6238633109  1.7053E-13  0.0000E+00  7.6220E-05  1.0000E-04   
 mr-sdci # 29  3   -227.6228453179  6.0715E-04  2.6910E-04  2.0642E-02  1.0000E-04   
 mr-sdci # 29  4   -227.3126265042  1.0232E-12  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 29  5   -227.3016099244  4.5475E-13  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 29  6   -226.8366497426  8.1203E-01  0.0000E+00  9.7776E-01  1.0000E-04   
 mr-sdci # 29  7   -225.8503197157  1.2519E-01  0.0000E+00  1.3589E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000687
time for cinew                         0.027641
time for eigenvalue solver             0.000070
time for vector access                 0.000000

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000     0.00000000  -328.20639169
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817834
   ht   5    -0.00000000    -0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6     0.01167760    -0.00001051    -0.90632425     0.00001886     0.00003856    -0.64404644
   ht   7    -0.04747058    -0.00002741    -1.47714770    -0.00001043    -0.00015006     0.10104368    -0.30106650
   ht   8    -0.24752764     0.00001561    -0.36953959     0.00002095    -0.00077187     0.05799247    -0.00234128    -0.07213731

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370       1.282438E-07   1.844209E-05  -1.878511E-04   3.773589E-02   5.640554E-04  -2.641282E-04  -1.422301E-03
 ref    2   9.237968E-09   0.959715       4.744369E-07   0.165833       4.771641E-04  -9.862081E-06   3.306604E-07  -8.484495E-07
 ref    3   3.475109E-10   3.748043E-07  -0.961369       1.007731E-05   1.807791E-06   0.169728      -3.759440E-03  -5.341188E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941619       0.921053       0.924230       2.750065E-02   1.424225E-03   2.880787E-02   1.420316E-05   2.854852E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037047     0.00000013     0.00001844    -0.00018785     0.03773589     0.00056406    -0.00026413    -0.00142230
 ref:   2     0.00000001     0.95971512     0.00000047     0.16583309     0.00047716    -0.00000986     0.00000033    -0.00000085
 ref:   3     0.00000000     0.00000037    -0.96136896     0.00001008     0.00000181     0.16972789    -0.00375944    -0.05341188

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -227.7749037255 -2.8422E-13  0.0000E+00  5.7529E-05  1.0000E-04   
 mr-sdci # 30  2   -227.6238633109  0.0000E+00  0.0000E+00  7.6220E-05  1.0000E-04   
 mr-sdci # 30  3   -227.6231276709  2.8235E-04  5.7353E-05  1.0176E-02  1.0000E-04   
 mr-sdci # 30  4   -227.3126265046  3.6124E-10  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 30  5   -227.3016099244  1.0118E-11  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 30  6   -227.2146225189  3.7797E-01  0.0000E+00  4.0114E-01  1.0000E-04   
 mr-sdci # 30  7   -225.8629127216  1.2593E-02  0.0000E+00  1.3948E+00  1.0000E-04   
 mr-sdci # 30  8   -225.1577224232 -2.6115E-01  0.0000E+00  1.1428E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000829
time for cinew                         0.031591
time for eigenvalue solver             0.000102
time for vector access                 0.000000

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3    -0.00000000    -0.00000000  -328.20867951
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817834
   ht   5    -0.00000000    -0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6    -1.52996950    -0.00003324     0.76112648    -0.00019543     0.00474206    -0.02223094

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370       1.219221E-07  -6.206433E-05   1.860670E-04   3.773619E-02  -2.123698E-02
 ref    2   9.238801E-09   0.959715      -3.459960E-06  -0.165833       4.771275E-04  -4.154307E-06
 ref    3   3.962675E-11  -3.115773E-06  -0.960939       1.586749E-06  -1.681723E-07   0.115393    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921053       0.923403       2.750065E-02   1.424248E-03   1.376667E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037047     0.00000012    -0.00006206     0.00018607     0.03773619    -0.02123698
 ref:   2     0.00000001     0.95971512    -0.00000346    -0.16583309     0.00047713    -0.00000415
 ref:   3     0.00000000    -0.00000312    -0.96093850     0.00000159    -0.00000017     0.11539350

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -227.7749037255 -4.5475E-13  0.0000E+00  5.7529E-05  1.0000E-04   
 mr-sdci # 31  2   -227.6238633109 -1.7053E-13  0.0000E+00  7.6218E-05  1.0000E-04   
 mr-sdci # 31  3   -227.6231616481  3.3977E-05  1.8931E-05  4.3926E-03  1.0000E-04   
 mr-sdci # 31  4   -227.3126265190  1.4365E-08  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 31  5   -227.3016099248  4.1581E-10  0.0000E+00  2.2548E-01  1.0000E-04   
 mr-sdci # 31  6   -225.2579126540 -1.9567E+00  0.0000E+00  1.4351E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000080
time for cinew                         0.028179
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3    -0.00000000    -0.00000000  -328.20867951
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817834
   ht   5    -0.00000000    -0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6    -1.52996950    -0.00003324     0.76112648    -0.00019543     0.00474206    -0.02223094
   ht   7     5.14063993     0.00001479     0.00802087     0.00062573    -0.01592908     0.02757625    -0.09651875

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370       3.843669E-08  -5.373494E-05   1.571426E-04  -3.774111E-02   5.835381E-02   2.949424E-02
 ref    2   9.243205E-09   0.959715      -2.471509E-06  -0.165833      -4.764640E-04  -2.399372E-05   5.693634E-06
 ref    3   1.530497E-09   2.049717E-06   0.960786      -4.048564E-05  -6.993462E-06   8.973307E-02  -0.104199    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941619       0.921053       0.923110       2.750064E-02   1.424618E-03   1.145719E-02   1.172732E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037047     0.00000004    -0.00005373     0.00015714    -0.03774111     0.05835381     0.02949424
 ref:   2     0.00000001     0.95971512    -0.00000247    -0.16583310    -0.00047646    -0.00002399     0.00000569
 ref:   3     0.00000000     0.00000205     0.96078617    -0.00004049    -0.00000699     0.08973307    -0.10419890

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -227.7749037255  3.4106E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 32  2   -227.6238633109  2.8422E-12  0.0000E+00  7.6176E-05  1.0000E-04   
 mr-sdci # 32  3   -227.6231669686  5.3205E-06  1.3345E-05  3.4943E-03  1.0000E-04   
 mr-sdci # 32  4   -227.3126267791  2.6009E-07  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 32  5   -227.3016099323  7.5079E-09  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 32  6   -226.1709417261  9.1303E-01  0.0000E+00  1.0487E+00  1.0000E-04   
 mr-sdci # 32  7   -225.2405126435 -6.2240E-01  0.0000E+00  1.4270E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000280
time for cinew                         0.028284
time for eigenvalue solver             0.000000
time for vector access                 0.000002

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3    -0.00000000    -0.00000000  -328.20867951
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817834
   ht   5    -0.00000000    -0.00000000     0.00000000     0.00000000  -327.88716176
   ht   6    -1.52996950    -0.00003324     0.76112648    -0.00019543     0.00474206    -0.02223094
   ht   7     5.14063993     0.00001479     0.00802087     0.00062573    -0.01592908     0.02757625    -0.09651875
   ht   8     4.45033080     0.00001530     0.11862078     0.00055063    -0.01378634     0.02345711    -0.07955913    -0.07232502

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370       2.302385E-08  -4.030539E-06   1.522815E-04   3.774190E-02   4.403431E-03  -7.041552E-02   1.255683E-03
 ref    2   9.244989E-09   0.959715      -5.916224E-07  -0.165833       4.763589E-04  -1.279353E-06   1.788826E-05  -2.052943E-06
 ref    3   3.298403E-10  -5.917930E-07  -0.960468      -7.636645E-06   1.597967E-06  -0.152117      -1.103095E-02  -4.786186E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941619       0.921053       0.922498       2.750064E-02   1.424678E-03   2.315894E-02   5.080027E-03   2.292334E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037047     0.00000002    -0.00000403     0.00015228     0.03774190     0.00440343    -0.07041552     0.00125568
 ref:   2     0.00000001     0.95971512    -0.00000059    -0.16583311     0.00047636    -0.00000128     0.00001789    -0.00000205
 ref:   3     0.00000000    -0.00000059    -0.96046762    -0.00000764     0.00000160    -0.15211689    -0.01103095    -0.04786186

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -227.7749037255 -3.4106E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 33  2   -227.6238633109  6.8212E-13  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 33  3   -227.6231743026  7.3340E-06  3.0889E-06  2.0767E-03  1.0000E-04   
 mr-sdci # 33  4   -227.3126268222  4.3120E-08  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 33  5   -227.3016099335  1.1446E-09  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 33  6   -226.6387477540  4.6781E-01  0.0000E+00  9.5222E-01  1.0000E-04   
 mr-sdci # 33  7   -226.0463240954  8.0581E-01  0.0000E+00  1.0410E+00  1.0000E-04   
 mr-sdci # 33  8   -224.5919900192 -5.6573E-01  0.0000E+00  1.3642E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000792
time for cinew                         0.031380
time for eigenvalue solver             0.000074
time for vector access                 0.000001

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3    -0.00000000     0.00000000  -328.20872614
   ht   4    -0.00000000    -0.00000000     0.00000000  -327.89817866
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.88716177
   ht   6     0.33371753     0.00000586    -0.20018081     0.00001306    -0.00104493    -0.00145287

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370       2.204227E-08  -1.408488E-05   1.519705E-04   3.774194E-02   1.701839E-02
 ref    2   9.244742E-09   0.959715       4.412074E-07  -0.165833       4.763579E-04  -8.765845E-06
 ref    3   3.339306E-10  -4.501391E-07   0.960450      -8.024864E-06   1.652646E-06   1.679750E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921053       0.922465       2.750064E-02   1.424681E-03   5.717818E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037047     0.00000002    -0.00001408     0.00015197     0.03774194     0.01701839
 ref:   2     0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636    -0.00000877
 ref:   3     0.00000000    -0.00000045     0.96045028    -0.00000802     0.00000165     0.01679750

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -227.7749037255  5.1159E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 34  2   -227.6238633109 -2.8422E-13  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 34  3   -227.6231761893  1.8868E-06  1.5880E-06  1.3133E-03  1.0000E-04   
 mr-sdci # 34  4   -227.3126268226  4.5191E-10  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 34  5   -227.3016099335  8.9244E-12  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 34  6   -225.9579031827 -6.8084E-01  0.0000E+00  1.4891E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000723
time for cinew                         0.029627
time for eigenvalue solver             0.000071
time for vector access                 0.000000

          starting ci iteration  35

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3    -0.00000000     0.00000000  -328.20872614
   ht   4    -0.00000000    -0.00000000     0.00000000  -327.89817866
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.88716177
   ht   6     0.33371753     0.00000586    -0.20018081     0.00001306    -0.00104493    -0.00145287
   ht   7     1.16634534     0.00000088     0.02209152     0.00003809    -0.00364679    -0.00133623    -0.00516504

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370      -2.244362E-08  -2.465706E-05   1.520916E-04   3.774200E-02   3.384018E-02  -4.845601E-02
 ref    2   9.245241E-09  -0.959715      -4.238562E-07  -0.165833       4.763580E-04  -3.146128E-06   1.388045E-05
 ref    3   3.298889E-10   4.404933E-07  -0.960409      -8.166137E-06   1.582408E-06  -4.846690E-02   1.658709E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941619       0.921053       0.922386       2.750064E-02   1.424686E-03   3.494199E-03   2.623117E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037047    -0.00000002    -0.00002466     0.00015209     0.03774200     0.03384018    -0.04845601
 ref:   2     0.00000001    -0.95971512    -0.00000042    -0.16583311     0.00047636    -0.00000315     0.00001388
 ref:   3     0.00000000     0.00000044    -0.96040917    -0.00000817     0.00000158    -0.04846690     0.01658709

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -227.7749037255 -1.7053E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 35  2   -227.6238633109  2.2737E-13  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 35  3   -227.6231768853  6.9594E-07  2.0946E-06  1.1784E-03  1.0000E-04   
 mr-sdci # 35  4   -227.3126268226  5.4001E-12  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 35  5   -227.3016099335  1.2506E-12  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 35  6   -226.6874747518  7.2957E-01  0.0000E+00  9.9919E-01  1.0000E-04   
 mr-sdci # 35  7   -225.5016950908 -5.4463E-01  0.0000E+00  1.3889E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000685
time for cinew                         0.028992
time for eigenvalue solver             0.000074
time for vector access                 0.000002

          starting ci iteration  36

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3    -0.00000000     0.00000000  -328.20872614
   ht   4    -0.00000000    -0.00000000     0.00000000  -327.89817866
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.88716177
   ht   6     0.33371753     0.00000586    -0.20018081     0.00001306    -0.00104493    -0.00145287
   ht   7     1.16634534     0.00000088     0.02209152     0.00003809    -0.00364679    -0.00133623    -0.00516504
   ht   8     2.04156642    -0.00000120     0.04393779     0.00006588    -0.00638083    -0.00235369    -0.00825178    -0.01504791

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370       2.270917E-08   2.158647E-06  -1.522024E-04   3.774199E-02   2.449275E-03  -7.046188E-02   1.174705E-03
 ref    2   9.245785E-09   0.959715      -4.496510E-07   0.165833       4.763579E-04  -5.708309E-06   1.730565E-05  -1.073869E-06
 ref    3   3.072968E-10  -4.515841E-07  -0.960318       7.572931E-06   1.508463E-06   7.938378E-02  -8.471717E-03  -5.576017E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941619       0.921053       0.922210       2.750064E-02   1.424685E-03   6.307784E-03   5.036647E-03   3.110576E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037047     0.00000002     0.00000216    -0.00015220     0.03774199     0.00244928    -0.07046188     0.00117470
 ref:   2     0.00000001     0.95971512    -0.00000045     0.16583311     0.00047636    -0.00000571     0.00001731    -0.00000107
 ref:   3     0.00000000    -0.00000045    -0.96031766     0.00000757     0.00000151     0.07938378    -0.00847172    -0.05576017

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -227.7749037255 -5.6843E-14  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 36  2   -227.6238633109 -3.9790E-13  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 36  3   -227.6231777518  8.6650E-07  4.3737E-07  8.4846E-04  1.0000E-04   
 mr-sdci # 36  4   -227.3126268227  1.4722E-11  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 36  5   -227.3016099335  3.9790E-13  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 36  6   -227.0651221195  3.7765E-01  0.0000E+00  6.7242E-01  1.0000E-04   
 mr-sdci # 36  7   -226.0466445498  5.4495E-01  0.0000E+00  1.0399E+00  1.0000E-04   
 mr-sdci # 36  8   -224.8668231647  2.7483E-01  0.0000E+00  1.4084E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030416
time for eigenvalue solver             0.000059
time for vector access                 0.000002

          starting ci iteration  37

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000     0.00000000  -328.20872959
   ht   4    -0.00000000     0.00000000    -0.00000000  -327.89817866
   ht   5     0.00000000    -0.00000000     0.00000000    -0.00000000  -327.88716177
   ht   6    -0.17876043    -0.00000245     0.08826411     0.00000516     0.00055753    -0.00024348

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370      -2.286650E-08   7.976406E-06   1.522226E-04  -3.774195E-02  -2.593694E-02
 ref    2   9.245885E-09  -0.959715       4.407683E-07  -0.165833      -4.763579E-04   6.458306E-06
 ref    3   4.088360E-10   4.464839E-07   0.960255      -7.698255E-06  -1.731123E-06   0.159550    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921053       0.922090       2.750064E-02   1.424682E-03   2.612887E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037047    -0.00000002     0.00000798     0.00015222    -0.03774195    -0.02593694
 ref:   2     0.00000001    -0.95971512     0.00000044    -0.16583311    -0.00047636     0.00000646
 ref:   3     0.00000000     0.00000045     0.96025539    -0.00000770    -0.00000173     0.15954982

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -227.7749037255  2.8422E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 37  2   -227.6238633109 -5.6843E-14  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 37  3   -227.6231780308  2.7906E-07  2.4477E-07  4.2503E-04  1.0000E-04   
 mr-sdci # 37  4   -227.3126268227  7.9581E-13  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 37  5   -227.3016099335  2.9559E-12  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 37  6   -225.7956211478 -1.2695E+00  0.0000E+00  1.2731E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000280
time for cinew                         0.028944
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  38

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000     0.00000000  -328.20872959
   ht   4    -0.00000000     0.00000000    -0.00000000  -327.89817866
   ht   5     0.00000000    -0.00000000     0.00000000    -0.00000000  -327.88716177
   ht   6    -0.17876043    -0.00000245     0.08826411     0.00000516     0.00055753    -0.00024348
   ht   7    -0.66041168     0.00000006    -0.00236172     0.00002174     0.00206354    -0.00041121    -0.00158107

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370      -2.258660E-08   4.153933E-06  -1.521531E-04   3.774191E-02  -4.827712E-02   4.647497E-02
 ref    2   9.245124E-09  -0.959715      -4.418744E-07   0.165833       4.763579E-04   1.289657E-05  -1.188705E-05
 ref    3   3.888259E-10   4.450804E-07  -0.960233       7.833339E-06   1.640576E-06  -0.146313      -0.114509    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941619       0.921053       0.922047       2.750064E-02   1.424678E-03   2.373804E-02   1.527215E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037047    -0.00000002     0.00000415    -0.00015215     0.03774191    -0.04827712     0.04647497
 ref:   2     0.00000001    -0.95971512    -0.00000044     0.16583311     0.00047636     0.00001290    -0.00001189
 ref:   3     0.00000000     0.00000045    -0.96023281     0.00000783     0.00000164    -0.14631255    -0.11450861

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -227.7749037255  1.1369E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 38  2   -227.6238633109  5.6843E-14  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 38  3   -227.6231780905  5.9624E-08  1.1127E-07  3.2130E-04  1.0000E-04   
 mr-sdci # 38  4   -227.3126268227  1.4779E-12  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 38  5   -227.3016099335  7.9581E-13  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 38  6   -226.2493198007  4.5370E-01  0.0000E+00  1.0894E+00  1.0000E-04   
 mr-sdci # 38  7   -225.7277914419 -3.1885E-01  0.0000E+00  1.2352E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000002
time for cinew                         0.027174
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  39

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000     0.00000000  -328.20872959
   ht   4    -0.00000000     0.00000000    -0.00000000  -327.89817866
   ht   5     0.00000000    -0.00000000     0.00000000    -0.00000000  -327.88716177
   ht   6    -0.17876043    -0.00000245     0.08826411     0.00000516     0.00055753    -0.00024348
   ht   7    -0.66041168     0.00000006    -0.00236172     0.00002174     0.00206354    -0.00041121    -0.00158107
   ht   8    -0.34395186     0.00000009    -0.01692074     0.00001122     0.00107585    -0.00021076    -0.00078649    -0.00044352

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370      -2.276339E-08  -4.898591E-07   1.522191E-04   3.774197E-02  -3.995277E-03   7.043737E-02  -1.461372E-03
 ref    2   9.244349E-09  -0.959715      -4.375023E-07  -0.165833       4.763581E-04  -1.856929E-06  -1.745114E-05   2.396580E-06
 ref    3   2.409256E-10   4.430259E-07  -0.960195      -8.506845E-06   9.321499E-07   0.189768       1.306036E-02   6.409783E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941619       0.921053       0.921975       2.750064E-02   1.424684E-03   3.602773E-02   5.131997E-03   4.110667E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037047    -0.00000002    -0.00000049     0.00015222     0.03774197    -0.00399528     0.07043737    -0.00146137
 ref:   2     0.00000001    -0.95971512    -0.00000044    -0.16583311     0.00047636    -0.00000186    -0.00001745     0.00000240
 ref:   3     0.00000000     0.00000044    -0.96019516    -0.00000851     0.00000093     0.18976766     0.01306036     0.06409783

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -227.7749037255  1.1369E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 39  2   -227.6238633109 -1.7053E-13  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 39  3   -227.6231781556  6.5181E-08  4.3833E-08  2.4723E-04  1.0000E-04   
 mr-sdci # 39  4   -227.3126268227  1.1312E-11  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 39  5   -227.3016099335  1.1653E-11  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 39  6   -226.7783683696  5.2905E-01  0.0000E+00  1.0017E+00  1.0000E-04   
 mr-sdci # 39  7   -226.0466877365  3.1890E-01  0.0000E+00  1.0406E+00  1.0000E-04   
 mr-sdci # 39  8   -225.0056331055  1.3881E-01  0.0000E+00  1.2514E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000618
time for cinew                         0.029661
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  40

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000    -0.00000000  -328.20872999
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817866
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.88716177
   ht   6     0.04052487    -0.00000059    -0.02307334     0.00000163    -0.00012752    -0.00002094

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370       2.266067E-08  -1.746174E-06  -1.521629E-04   3.774199E-02   1.720148E-02
 ref    2   9.244700E-09   0.959715       4.365463E-07   0.165833       4.763580E-04  -8.981882E-06
 ref    3   2.607133E-10  -4.414552E-07   0.960191       8.611453E-06   9.591780E-07   3.146293E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921053       0.921967       2.750064E-02   1.424685E-03   1.285807E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037047     0.00000002    -0.00000175    -0.00015216     0.03774199     0.01720148
 ref:   2     0.00000001     0.95971512     0.00000044     0.16583311     0.00047636    -0.00000898
 ref:   3     0.00000000    -0.00000044     0.96019108     0.00000861     0.00000096     0.03146293

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -227.7749037255 -3.9790E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 40  2   -227.6238633109  1.1369E-13  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 40  3   -227.6231781829  2.7216E-08  2.3295E-08  1.5580E-04  1.0000E-04   
 mr-sdci # 40  4   -227.3126268227  1.4154E-11  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 40  5   -227.3016099335  9.6634E-13  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 40  6   -226.0125341440 -7.6583E-01  0.0000E+00  1.4738E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000002
time for cinew                         0.029787
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  41

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000    -0.00000000  -328.20872999
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817866
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.88716177
   ht   6     0.04052487    -0.00000059    -0.02307334     0.00000163    -0.00012752    -0.00002094
   ht   7     0.14461174    -0.00000011     0.00201166     0.00000478    -0.00045283    -0.00002014    -0.00007920

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370       2.273497E-08  -2.861913E-06  -1.522461E-04  -3.774202E-02   3.290473E-02  -4.953481E-02
 ref    2   9.245823E-09   0.959715      -4.355303E-07   0.165833      -4.763581E-04  -2.617502E-06   1.397649E-05
 ref    3   2.643282E-10  -4.413369E-07  -0.960185       8.733566E-06  -9.066911E-07  -6.530345E-02   1.313907E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.941619       0.921053       0.921955       2.750064E-02   1.424687E-03   5.347262E-03   2.626332E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037047     0.00000002    -0.00000286    -0.00015225    -0.03774202     0.03290473    -0.04953481
 ref:   2     0.00000001     0.95971512    -0.00000044     0.16583311    -0.00047636    -0.00000262     0.00001398
 ref:   3     0.00000000    -0.00000044    -0.96018477     0.00000873    -0.00000091    -0.06530345     0.01313907

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -227.7749037255 -2.2737E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 41  2   -227.6238633109 -3.4106E-13  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 41  3   -227.6231781927  9.8144E-09  2.8871E-08  1.3905E-04  1.0000E-04   
 mr-sdci # 41  4   -227.3126268227  2.3306E-12  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 41  5   -227.3016099335  2.2737E-13  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 41  6   -226.6944929584  6.8196E-01  0.0000E+00  1.0154E+00  1.0000E-04   
 mr-sdci # 41  7   -225.5461006587 -5.0059E-01  0.0000E+00  1.3737E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000725
time for cinew                         0.028360
time for eigenvalue solver             0.000000
time for vector access                 0.000002

          starting ci iteration  42

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045556
   ht   2     0.00000000  -328.20941515
   ht   3     0.00000000    -0.00000000  -328.20872999
   ht   4     0.00000000     0.00000000     0.00000000  -327.89817866
   ht   5    -0.00000000     0.00000000     0.00000000     0.00000000  -327.88716177
   ht   6     0.04052487    -0.00000059    -0.02307334     0.00000163    -0.00012752    -0.00002094
   ht   7     0.14461174    -0.00000011     0.00201166     0.00000478    -0.00045283    -0.00002014    -0.00007920
   ht   8     0.23692640     0.00000013     0.00537152     0.00000769    -0.00074030    -0.00003326    -0.00011862    -0.00020293

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370       2.275335E-08   2.711060E-07  -1.522031E-04   3.774199E-02   2.641231E-03  -7.045310E-02   1.157833E-03
 ref    2  -9.244785E-09   0.959715      -4.370170E-07   0.165833       4.763581E-04  -7.370576E-06   1.728410E-05  -9.782475E-07
 ref    3  -2.644541E-10  -4.413606E-07  -0.960172       9.015908E-06   6.865421E-07   9.879452E-02  -7.235704E-03  -5.650070E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.941619       0.921053       0.921930       2.750064E-02   1.424685E-03   9.767334E-03   5.015996E-03   3.193670E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047     0.00000002     0.00000027    -0.00015220     0.03774199     0.00264123    -0.07045310     0.00115783
 ref:   2    -0.00000001     0.95971512    -0.00000044     0.16583311     0.00047636    -0.00000737     0.00001728    -0.00000098
 ref:   3    -0.00000000    -0.00000044    -0.96017169     0.00000902     0.00000069     0.09879452    -0.00723570    -0.05650070

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -227.7749037255  1.7053E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 42  2   -227.6238633109  0.0000E+00  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 42  3   -227.6231782047  1.2061E-08  6.6365E-09  1.0543E-04  1.0000E-04   
 mr-sdci # 42  4   -227.3126268227  2.2737E-12  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 42  5   -227.3016099335  1.8190E-12  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 42  6   -227.0710109678  3.7652E-01  0.0000E+00  7.0203E-01  1.0000E-04   
 mr-sdci # 42  7   -226.0463856506  5.0028E-01  0.0000E+00  1.0399E+00  1.0000E-04   
 mr-sdci # 42  8   -224.9189375908 -8.6696E-02  0.0000E+00  1.4144E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000322
time for cinew                         0.030291
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  43

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045556
   ht   2    -0.00000000  -328.20941515
   ht   3     0.00000000     0.00000000  -328.20873004
   ht   4    -0.00000000    -0.00000000     0.00000000  -327.89817866
   ht   5    -0.00000000    -0.00000000     0.00000000    -0.00000000  -327.88716177
   ht   6     0.02247320    -0.00000026     0.00994873     0.00000068     0.00006957    -0.00000371

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   5.717728E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   6.844063E-05  -0.125033       0.384534      -1.694629E-05  -2.230358E-02  -0.914332    
   x:   2  -7.926415E-10  -1.266639E-04   2.803010E-05    1.00000      -5.697935E-07   1.058928E-05
   x:   3   3.031223E-05   0.282238      -0.867793       6.439457E-05   5.734452E-02  -0.404956    
   x:   4   2.064530E-09  -0.950972      -0.309278      -1.117844E-04   2.387691E-06  -2.758106E-05
   x:   5   2.121660E-07   1.900724E-02  -5.845113E-02   3.507223E-06  -0.998105      -2.834502E-03
   x:   6    1.00000        0.00000        0.00000        0.00000      -5.478317E-12   7.485317E-05
 bummer (warning):overlap matrix: # small eigenvalues=1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370       2.269012E-08   1.024661E-06   1.522189E-04   3.774196E-02  -2.641190E-02
 ref    2  -9.243862E-09   0.959715       4.370213E-07  -0.165833       4.763581E-04   5.246726E-06
 ref    3  -3.794863E-10  -4.412877E-07   0.960164      -9.108379E-06   8.919488E-07   0.153945    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.941619       0.921053       0.921915       2.750064E-02   1.424682E-03   2.439679E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037047     0.00000002     0.00000102     0.00015222     0.03774196    -0.02641190
 ref:   2    -0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636     0.00000525
 ref:   3    -0.00000000    -0.00000044     0.96016413    -0.00000911     0.00000089     0.15394543

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -227.7749037255  2.8422E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 43  2   -227.6238633109  5.6843E-14  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 43  3   -227.6231782090  4.3059E-09  3.9279E-09  5.3027E-05  1.0000E-04   
 mr-sdci # 43  4   -227.3126268227  2.8422E-13  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 43  5   -227.3016099335  2.7285E-12  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 43  6   -225.8342430872 -1.2368E+00  0.0000E+00  1.2933E+00  1.0000E-04   
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000603
time for cinew                         0.027330
time for eigenvalue solver             0.000187
time for vector access                 0.000002

 mr-sdci  convergence criteria satisfied after 43 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -227.7749037255  2.8422E-13  0.0000E+00  5.7527E-05  1.0000E-04   
 mr-sdci # 43  2   -227.6238633109  5.6843E-14  0.0000E+00  7.6167E-05  1.0000E-04   
 mr-sdci # 43  3   -227.6231782090  4.3059E-09  3.9279E-09  5.3027E-05  1.0000E-04   
 mr-sdci # 43  4   -227.3126268227  2.8422E-13  0.0000E+00  1.5027E-01  1.0000E-04   
 mr-sdci # 43  5   -227.3016099335  2.7285E-12  0.0000E+00  2.2547E-01  1.0000E-04   
 mr-sdci # 43  6   -225.8342430872 -1.2368E+00  0.0000E+00  1.2933E+00  1.0000E-04   

####################CIUDGINFO####################

   ci vector at position   1 energy= -227.774903725502
   ci vector at position   2 energy= -227.623863310887
   ci vector at position   3 energy= -227.623178209046

################END OF CIUDGINFO################

 
    3 of the   7 expansion vectors are transformed.
    3 of the   6 matrix-vector products are transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -227.7749037255

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                          orbital     1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.943740                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-      
                                                
 z*  1  1       4 -0.014212                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +- 
                                                
 z*  1  1       6 -0.012657                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
                                             +- 
 z*  1  1      10 -0.026725                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +  
                                              - 
 z*  1  1      15 -0.014278                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +- 
                                                
 z*  1  1      17 -0.012353                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
                                             +- 
 z*  1  1      22 -0.082042                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 z*  1  1      41 -0.079248                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +- 
                                                
 z*  1  1      43 -0.074826                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
                                             +- 
 z*  1  1      51  0.086807                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 z*  1  1      72  0.148901                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-   +  
                                              - 
 z*  1  1      88  0.011576                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-   +-    - 
                                             +- 
 z*  1  1      91 -0.073795                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-   +- 
                                                
 z*  1  1      93 -0.070791                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-      
                                             +- 
 z*  1  1     101 -0.012930                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-   +-   +- 
                                              - 
 z*  1  1     105  0.022523                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +-   +- 
                                             +- 
 y   1  1     670 -0.033290              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     676  0.011517             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     682 -0.012698             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     746 -0.034329              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     752  0.011914              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     759 -0.012498             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1    1214  0.010945              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     - 
                                                
 y   1  1    1346  0.010914              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    - 
                                                
 y   1  1    1708 -0.022633              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1714  0.018419              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1721 -0.014031             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1728 -0.017048             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1736 -0.019896              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1742  0.017081             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1748 -0.013693             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1755 -0.012967             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1788 -0.022407              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1794  0.018293             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1800 -0.014986             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1807 -0.015089             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1812 -0.020281              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    1818  0.016891              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    1825 -0.012825             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    1832 -0.014726             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    2114  0.016032             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-    - 
                                                
 y   1  1    2143  0.013161             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
                                              - 
 y   1  1    3157 -0.010213             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    - 
                                                
 y   1  1    3920  0.020248              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3926 -0.016718             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3932  0.013798             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3939  0.012870             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3944  0.019969              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3950 -0.017278              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3957  0.013014             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3964  0.015291             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3996  0.020603              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4002 -0.016508              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4009  0.012924             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4016  0.014619             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4024  0.019934              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4030 -0.017119             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4036  0.013875             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4043  0.013487             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4327 -0.015885             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-    - 
                                                
 y   1  1    4338 -0.012396              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-      
                                              - 
 y   1  1    4350 -0.017959             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-      
                                              - 
 y   1  1    5104  0.012804             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-    - 
                                                
 y   1  1    5133  0.012538             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-      
                                              - 
 y   1  1    6131  0.011623              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-      
                                                
 y   1  1    6137 -0.013894             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-      
                                                
 y   1  1    6649 -0.014016              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6658  0.022584             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6660  0.011001             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6665 -0.025558             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6677  0.016317              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    6683 -0.020604             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    6689 -0.018043             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    6692  0.019247             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    7431 -0.016456              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7437  0.019513             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7443  0.017927             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7446 -0.018642             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7455  0.012937              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1    7464 -0.023146             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1    7466 -0.010042             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1    7471  0.024159             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1   11589 -0.010055              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-      
                                                
 y   1  1   11598  0.016593             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-      
                                                
 y   1  1   11605 -0.011496             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-      
                                                
 y   1  1   12111  0.015764              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12117 -0.020044             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12123 -0.019135             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12126  0.020667             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12135 -0.014727              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12144  0.023328             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12146  0.010043             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12151 -0.023980             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12889  0.014341              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12898 -0.022454             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12900 -0.010315             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12905  0.023307             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12917 -0.015169              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   12923  0.020498             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   12929  0.017807             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   12932 -0.019667             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   17568 -0.012769              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   17596 -0.012681              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   18350  0.011056              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   18374  0.014963              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   23030  0.012011              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   23054  0.012974              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   23808 -0.011329              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   23836 -0.014136              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   28486  0.012107              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   28513 -0.011617              2( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   28529  0.010164             18( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   29267  0.010899              2( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   29292 -0.013097              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   31086  0.014680              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   31113 -0.015644              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   31867  0.014607              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   31892 -0.015030              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +-      
                                              - 
 y   1  1   33947  0.011155              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   33963 -0.010626             18( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   33972 -0.012193              1( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   34726  0.010863              1( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   34753 -0.012802              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   34769  0.010714             18( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   36547  0.015511              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   36572 -0.014896              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   37326  0.014889              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   37353 -0.014691              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-   +-      
                                              - 
 y   1  1   39409  0.015005              4( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   39418 -0.014684             13( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   39430 -0.010088             25( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   39437 -0.014104              6( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   39443  0.014131             12( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   40191  0.013980              6( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   40197 -0.013658             12( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   40215 -0.014088              4( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   40224  0.014589             13( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   42018 -0.010562             13( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   42043  0.010491             12( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   42797 -0.010312             12( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   42824  0.010126             13( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-   +-      
                                              - 
 y   1  1   44871 -0.015382              6( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   44877  0.014533             12( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   44895  0.013544              4( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   44904 -0.014500             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   45649 -0.013700              4( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   45658  0.013685             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   45677  0.014316              6( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   45683 -0.014258             12( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   47477  0.010726             12( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   47504 -0.010338             13( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   48258  0.010301             13( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   48283 -0.010116             12( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-      
                                              - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             151
     0.01> rq > 0.001            867
    0.001> rq > 0.0001          2619
   0.0001> rq > 0.00001         4572
  0.00001> rq > 0.000001        2962
 0.000001> rq                  38632
           all                 49805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:        4961    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.943740312673   -214.850208683674
     2     2     -0.000000215466      0.000048897634
     3     3      0.000000001061     -0.000000241448
     4     4     -0.014211792204      3.234066133832
     5     5      0.000000000175     -0.000000039820
     6     6     -0.012656899748      2.880230534607
     7     7      0.000000000863     -0.000000196438
     8     8     -0.000000020290      0.000004581940
     9     9      0.000000000261     -0.000000059381
    10    10     -0.026725336391      6.081686598588
    11    11     -0.000000000292      0.000000066318
    12    12      0.000359856392     -0.081908291958
    13    13     -0.000000039627      0.000008987669
    14    14     -0.000000000052      0.000000011795
    15    15     -0.014277822994      3.249097000377
    16    16     -0.000000000239      0.000000054445
    17    17     -0.012353499799      2.811177392554
    18    18     -0.000000000056      0.000000012584
    19    19     -0.000000039969      0.000009061916
    20    20      0.000720031384     -0.163777578397
    21    21      0.000000001686     -0.000000385537
    22    22     -0.082042312796     18.693766372179
    23    23      0.000000000561     -0.000000127148
    24    24      0.000000207125     -0.000046949486
    25    25      0.000000000393     -0.000000088851
    26    26      0.000000006622     -0.000001453592
    27    27      0.001069114418     -0.243456543464
    28    28     -0.000000000001      0.000000000114
    29    29      0.000000135764     -0.000030862825
    30    30     -0.000000000336      0.000000075623
    31    31      0.000000050070     -0.000011416830
    32    32      0.000000000008     -0.000000001741
    33    33      0.000564004176     -0.128464936867
    34    34     -0.000000001187      0.000000276828
    35    35     -0.000000000037      0.000000008434
    36    36      0.000000000021     -0.000000004794
    37    37      0.000888043560     -0.202234539444
    38    38      0.001059201312     -0.241171356063
    39    39      0.000000000020     -0.000000004625
    40    40     -0.000000000001      0.000000000251
    41    41     -0.079247891203     18.066323636553
    42    42      0.000000015373     -0.000003504574
    43    43     -0.074825753187     17.057539001305
    44    44     -0.000000000084      0.000000018709
    45    45     -0.000000204534      0.000046260194
    46    46      0.002235362163     -0.509303742857
    47    47      0.000000156726     -0.000035467383
    48    48      0.000000000062     -0.000000013764
    49    49      0.000000000441     -0.000000100454
    50    50      0.002216172091     -0.504930673139
    51    51      0.086806930391    -19.776896815251
    52    52      0.000000011207     -0.000002552164
    53    53     -0.000000059386      0.000013510924
    54    54      0.000000000263     -0.000000059016
    55    55     -0.000000087822      0.000020013426
    56    56     -0.000000000261      0.000000059168
    57    57     -0.000000000165      0.000000037582
    58    58      0.000956985042     -0.217910865181
    59    59     -0.000000000411      0.000000092914
    60    60     -0.000000223886      0.000050740329
    61    61     -0.000000000447      0.000000101251
    62    62      0.000869498106     -0.197963507023
    63    63     -0.000000000080      0.000000018133
    64    64      0.000000000006     -0.000000001272
    65    65     -0.000000066095      0.000014980603
    66    66      0.001570732840     -0.357678676627
    67    67     -0.000000000133      0.000000030378
    68    68     -0.000000000137      0.000000031253
    69    69      0.001000039381     -0.227727087706
    70    70      0.000000002934     -0.000000655251
    71    71     -0.000000001199      0.000000273354
    72    72      0.148900518474    -33.944954406918
    73    73      0.000000001774     -0.000000404266
    74    74      0.000000110706     -0.000025188822
    75    75     -0.000000000234      0.000000052999
    76    76     -0.000000000043      0.000000009830
    77    77      0.000000000272     -0.000000061397
    78    78     -0.000000068793      0.000015741267
    79    79      0.004250926883     -0.968576385149
    80    80      0.000000000023     -0.000000005240
    81    81      0.000455627623     -0.105484253412
    82    82     -0.000000003991      0.000000927010
    83    83     -0.000000000009      0.000000002004
    84    84      0.000000000003     -0.000000000739
    85    85     -0.000000025137      0.000005697840
    86    86      0.000005099175     -0.001165756283
    87    87     -0.000000001820      0.000000415050
    88    88      0.011575534707     -2.641613851134
    89    89     -0.000000027844      0.000006362444
    90    90      0.000000000094     -0.000000021133
    91    91     -0.073795364167     16.822515777398
    92    92     -0.000000015058      0.000003432698
    93    93     -0.070791244920     16.138630902450
    94    94     -0.000000000065      0.000000014480
    95    95     -0.000000201229      0.000045495745
    96    96      0.002056830083     -0.468641582181
    97    97      0.000000171933     -0.000038910130
    98    98      0.000000000062     -0.000000013698
    99    99     -0.000000000425      0.000000096946
   100   100      0.002044906993     -0.465927129214
   101   101     -0.012929869922      2.950979587678
   102   102     -0.000000000157      0.000000036108
   103   103      0.000000000102     -0.000000022943
   104   104     -0.000000013703      0.000003183180
   105   105      0.022523190040     -5.143691648481

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.95170691  c**2 (all zwalks) =   0.95176052

 pople ci energy extrapolation is computed with 32 correlated electrons.

 eref      =   -227.675887510661   "relaxed" cnot**2         =   0.951706910883
 eci       =   -227.774903725502   deltae = eci - eref       =  -0.099016214842
 eci+dv1   =   -227.779685524390   dv1 = (1-cnot**2)*deltae  =  -0.004781798887
 eci+dv2   =   -227.779928170353   dv2 = dv1 / cnot**2       =  -0.005024444850
 eci+dv3   =   -227.780196758188   dv3 = dv1 / (2*cnot**2-1) =  -0.005293032686
 eci+pople =   -227.779832191908   ( 32e- scaled deltae )    =  -0.103944681248


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -227.6238633109

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                          orbital     1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.715647                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     - 
                                                
 z*  1  1       8  0.604847                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-      
                                              - 
 z*  1  1      13 -0.018815                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +- 
                                              - 
 z*  1  1      19 -0.017246                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
                                             +- 
 z*  1  1      24  0.080632                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +  
                                              - 
 z*  1  1      26 -0.044123                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     - 
                                              - 
 z*  1  1      29  0.089667                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +- 
                                                
 z*  1  1      31  0.037881                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                             +- 
 z*  1  1      45 -0.060091                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +     - 
                                             +- 
 z*  1  1      47  0.046401                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-   +- 
                                              - 
 z*  1  1      53 -0.038226                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +- 
                                                
 z*  1  1      55 -0.069620                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                             +- 
 z*  1  1      60 -0.084168                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-   +  
                                              - 
 z*  1  1      65 -0.047821                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +    +-    - 
                                              - 
 z*  1  1      74  0.079203                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +    +- 
                                              - 
 z*  1  1      78 -0.067593                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +    +-    - 
                                             +- 
 z*  1  1      82 -0.011507                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +    +-    -   +- 
                                              - 
 z*  1  1      89 -0.017733                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    -   +- 
                                             +- 
 z*  1  1      95 -0.048844                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +     - 
                                             +- 
 z*  1  1      97  0.052636                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +    +-   +- 
                                              - 
 z*  1  1     104 -0.018107                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     -   +-   +- 
                                             +- 
 z   1  1     246 -0.073541                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    - 
                                                
 z   1  1     267  0.031093                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +  
                                              - 
 z   1  1     276 -0.010230                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +    +-   +-    - 
                                              - 
 z   1  1     286 -0.026727                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +-   +-   +-   +- 
                                                
 z   1  1     288 -0.015979                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +-   +-   +-      
                                             +- 
 z   1  1     307  0.010436                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +     -   +-   +-   +- 
                                              - 
 z   1  1     317  0.059848                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-      
                                              - 
 z   1  1     336 -0.019941                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +- 
                                                
 z   1  1     338 -0.021633                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-      
                                             +- 
 z   1  1     357  0.028801                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +-   +  
                                              - 
 y   1  1     672  0.021645              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     684  0.020911             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     751  0.012400              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     765  0.017539             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     772 -0.020810              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     800  0.012971              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +       
                                              - 
 y   1  1     902  0.014849              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1     908 -0.011985              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1     922  0.014662             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1     982  0.011616              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-    - 
                                                
 y   1  1     988 -0.010376             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-    - 
                                                
 y   1  1    1001  0.011109             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-    - 
                                                
 y   1  1    1474  0.012738              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +     - 
                                              - 
 y   1  1    1727  0.011403             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1762 -0.011083              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-         - 
                                              - 
 y   1  1    1892  0.011126              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
                                             +- 
 y   1  1    1916 -0.017960              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1922  0.015006              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1929 -0.011168             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1936 -0.013457             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1994  0.011759              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
                                              - 
 y   1  1    2022  0.010122              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
                                              - 
 y   1  1    2906 -0.011815              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +     - 
                                              - 
 y   1  1    3963 -0.010995             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    4048  0.014984              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4054 -0.010316              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4128  0.016064              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4134 -0.013707             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4140  0.011335             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4147  0.010674             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4206 -0.011655              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     -    - 
                                              - 
 y   1  1    4230 -0.010262              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-        +-    - 
                                              - 
 y   1  1    4454 -0.011546             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +     - 
                                              - 
 y   1  1    6190 -0.011881             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +       
                                              - 
 y   1  1    6294 -0.011047             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    - 
                                                
 y   1  1    6301  0.013240             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    - 
                                                
 y   1  1    6717 -0.012074             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-    -   +- 
                                                
 y   1  1    6781  0.011460              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6787 -0.014439             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6793 -0.012369             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6796  0.013385             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    7022 -0.011593             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7029  0.013311             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7489  0.010671             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -   +- 
                                                
 y   1  1    7495  0.010177             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -   +- 
                                                
 y   1  1    7498 -0.010572             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -   +- 
                                                
 y   1  1    7568 -0.014146             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7575  0.014925             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7801 -0.011813             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1    7807 -0.010522             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1    7810  0.011078             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1   11649  0.010875             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +       
                                              - 
 y   1  1   11762 -0.011025             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-    - 
                                                
 y   1  1   12239 -0.010313              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12248  0.016326             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12255 -0.016600             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12378  0.010367             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -   +-      
                                             +- 
 y   1  1   12385 -0.011238             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -   +-      
                                             +- 
 y   1  1   12481  0.010245             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12487  0.010103             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12490 -0.010729             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12950 -0.012224             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    -   +- 
                                                
 y   1  1   12957  0.013261             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    -   +- 
                                                
 y   1  1   13027  0.012536             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13033  0.011053             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13036 -0.012100             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13262  0.013568             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   13269 -0.013837             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   17064 -0.011711             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   18478  0.010262              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   19780  0.024919              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19792  0.016129             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19804  0.019836              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19817  0.013623             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19856  0.024829              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19869  0.016111             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19884  0.019878              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   19896  0.013291             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   22527  0.010720             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   25238 -0.020537              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25251 -0.015023             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25266 -0.022256              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25278 -0.014153             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25318 -0.020846              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25330 -0.014702             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25342 -0.022054              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   25355 -0.014101             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   28018 -0.010984              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +       
                                              - 
 y   1  1   30697  0.018635              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   30722 -0.017236              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   30736  0.010335             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   30774 -0.017608              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   30788  0.010986             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   30801  0.016994              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   31217 -0.010482              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +     - 
                                              - 
 y   1  1   33479 -0.010447              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +       
                                              - 
 y   1  1   36156  0.017818              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   36170 -0.010945             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   36183 -0.017793              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   36235 -0.017732              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   36260  0.016812              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   36274 -0.010193             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   36676 -0.010045              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +     - 
                                              - 
 y   1  1   45008 -0.010059             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     - 
                                              - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             133
     0.01> rq > 0.001           2170
    0.001> rq > 0.0001          4284
   0.0001> rq > 0.00001         3610
  0.00001> rq > 0.000001        1891
 0.000001> rq                  37715
           all                 49805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:        4961    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.000000038093      0.000008645914
     2     2      0.715647422739   -162.810081741469
     3     3      0.000000023975     -0.000005457856
     4     4      0.000000018563     -0.000004223471
     5     5      0.000000000060     -0.000000013514
     6     6      0.000000004245     -0.000000967979
     7     7      0.000000037619     -0.000008556037
     8     8      0.604846701727   -137.613672156559
     9     9     -0.000000000063      0.000000014350
    10    10      0.000000032470     -0.000007362894
    11    11      0.000000000036     -0.000000008290
    12    12      0.000000014892     -0.000003388245
    13    13     -0.018815232601      4.278823652348
    14    14     -0.000000003242      0.000000735194
    15    15      0.000000007706     -0.000001753406
    16    16     -0.000000000057      0.000000012864
    17    17      0.000000011274     -0.000002567374
    18    18     -0.000000003536      0.000000802182
    19    19     -0.017246286677      3.921972928933
    20    20     -0.000000001371      0.000000311082
    21    21      0.000000001935     -0.000000440023
    22    22     -0.000000107450      0.000024405961
    23    23     -0.000000003574      0.000000810972
    24    24      0.080631592828    -18.365026797128
    25    25     -0.000000005832      0.000001325828
    26    26     -0.044123073440     10.045462925946
    27    27      0.000000008265     -0.000001866295
    28    28      0.000000000092     -0.000000021038
    29    29      0.089667101719    -20.420511944762
    30    30     -0.000000002735      0.000000624142
    31    31      0.037881133657     -8.629565107685
    32    32      0.000000000131     -0.000000029871
    33    33     -0.000000000511      0.000000107415
    34    34     -0.003681517903      0.838052318758
    35    35     -0.000000010133      0.000002301158
    36    36     -0.000000000035      0.000000007918
    37    37     -0.000000001407      0.000000309466
    38    38      0.000000006271     -0.000001412071
    39    39     -0.000000000016      0.000000003609
    40    40     -0.000000001430      0.000000324631
    41    41     -0.000000021150      0.000004812019
    42    42      0.000000000274     -0.000000062461
    43    43     -0.000000004426      0.000001011630
    44    44      0.000000008769     -0.000002004334
    45    45     -0.060090979271     13.688955139602
    46    46     -0.000000002866      0.000000644793
    47    47      0.046401229799    -10.571750832991
    48    48     -0.000000005443      0.000001252015
    49    49      0.000000000007     -0.000000001515
    50    50     -0.000000002819      0.000000634233
    51    51      0.000000113455     -0.000025770612
    52    52     -0.000000001463      0.000000332724
    53    53     -0.038226311233      8.708023808417
    54    54     -0.000000008571      0.000001950554
    55    55     -0.069620311676     15.856350867696
    56    56     -0.000000000054      0.000000012035
    57    57     -0.000000000042      0.000000009557
    58    58      0.000000004869     -0.000001093994
    59    59     -0.000000002426      0.000000557697
    60    60     -0.084168317264     19.171368619733
    61    61     -0.000000005789      0.000001318219
    62    62     -0.000000005280      0.000001193498
    63    63     -0.000000000097      0.000000022135
    64    64     -0.000000000693      0.000000156976
    65    65     -0.047821011819     10.890210503541
    66    66     -0.000000000698      0.000000147405
    67    67      0.000000000104     -0.000000023669
    68    68      0.000000000076     -0.000000017358
    69    69      0.000000006540     -0.000001474695
    70    70     -0.003281116889      0.746958196550
    71    71     -0.000000000276      0.000000062919
    72    72      0.000000043601     -0.000009886056
    73    73     -0.000000000261      0.000000059371
    74    74      0.079202874018    -18.042602516833
    75    75     -0.000000010310      0.000002328932
    76    76     -0.000000000000      0.000000000063
    77    77      0.000000013075     -0.000002963639
    78    78     -0.067592852240     15.400442689855
    79    79      0.000000001986     -0.000000435317
    80    80      0.000000000017     -0.000000003826
    81    81      0.000000020293     -0.000004611319
    82    82     -0.011506846175      2.623416793025
    83    83     -0.000000000548      0.000000124317
    84    84     -0.000000000267      0.000000060854
    85    85     -0.009881386663      2.251926889667
    86    86      0.000000000271     -0.000000061815
    87    87      0.000000000232     -0.000000052927
    88    88      0.000000027852     -0.000006327579
    89    89     -0.017733099421      4.046103148305
    90    90      0.000000000124     -0.000000024100
    91    91     -0.000000001020      0.000000238100
    92    92      0.000000000259     -0.000000058967
    93    93     -0.000000014504      0.000003302613
    94    94     -0.000000007615      0.000001727588
    95    95     -0.048844076838     11.125971400882
    96    96     -0.000000003032      0.000000682973
    97    97      0.052636134230    -11.993100201632
    98    98      0.000000009311     -0.000002108844
    99    99      0.000000000011     -0.000000002621
   100   100     -0.000000002615      0.000000587795
   101   101     -0.000000029008      0.000006591003
   102   102     -0.000000000226      0.000000051527
   103   103      0.000000003288     -0.000000745499
   104   104     -0.018107251194      4.131144171487
   105   105      0.000000016580     -0.000003764546

 number of reference csfs (nref) is   105.  root number (iroot) is  2.
 c0**2 =   0.93490398  c**2 (all zwalks) =   0.94816043

 pople ci energy extrapolation is computed with 32 correlated electrons.

 eref      =   -227.524406595161   "relaxed" cnot**2         =   0.934903975192
 eci       =   -227.623863310887   deltae = eci - eref       =  -0.099456715726
 eci+dv1   =   -227.630337547721   dv1 = (1-cnot**2)*deltae  =  -0.006474236834
 eci+dv2   =   -227.630788339559   dv2 = dv1 / cnot**2       =  -0.006925028673
 eci+dv3   =   -227.631306605503   dv3 = dv1 / (2*cnot**2-1) =  -0.007443294616
 eci+pople =   -227.630774499444   ( 32e- scaled deltae )    =  -0.106367904283


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -227.6231782090

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                          orbital     1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       3 -0.613936                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +       
                                              - 
 z*  1  1       7 -0.708045                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    - 
                                                
 z*  1  1      14 -0.017681                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          - 
                                             +- 
 z*  1  1      18 -0.019188                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +- 
                                              - 
 z*  1  1      23 -0.089337                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +- 
                                                
 z*  1  1      25 -0.038793                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                             +- 
 z*  1  1      30 -0.080897                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +  
                                              - 
 z*  1  1      35  0.044703                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +    +-    - 
                                              - 
 z*  1  1      44 -0.047063                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +    +- 
                                              - 
 z*  1  1      48  0.059557                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-    - 
                                             +- 
 z*  1  1      54  0.084653                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +  
                                              - 
 z*  1  1      56  0.047622                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +     - 
                                              - 
 z*  1  1      59  0.037912                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-   +- 
                                                
 z*  1  1      61  0.070162                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                             +- 
 z*  1  1      75  0.068482                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +     - 
                                             +- 
 z*  1  1      77 -0.078471                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +    +-   +- 
                                              - 
 z*  1  1      84  0.011584                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -   +-   +- 
                                              - 
 z*  1  1      90  0.017775                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -   +-   +- 
                                             +- 
 z*  1  1      94 -0.053255                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +    +- 
                                              - 
 z*  1  1      98  0.048329                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +    +-    - 
                                             +- 
 z*  1  1     103  0.018193                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-    -   +- 
                                             +- 
 z   1  1     247  0.059939                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-      
                                              - 
 z   1  1     266 -0.019735                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +- 
                                                
 z   1  1     268 -0.022030                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-      
                                             +- 
 z   1  1     287  0.028889                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +-   +-   +-   +  
                                              - 
 z   1  1     316 -0.071648                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    - 
                                                
 z   1  1     337  0.030587                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +  
                                              - 
 z   1  1     356 -0.026002                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +-   +- 
                                                
 z   1  1     358 -0.016080                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +-      
                                             +- 
 z   1  1     377  0.010132                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +     -   +-   +-   +- 
                                              - 
 y   1  1     673 -0.012584              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     687 -0.017842             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     694 -0.011908              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-         - 
                                                
 y   1  1     700  0.010071              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-         - 
                                                
 y   1  1     714 -0.011759             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-         - 
                                                
 y   1  1     750 -0.020828              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     762 -0.020057             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     774 -0.021400              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     780  0.012093             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     793 -0.010900             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     798  0.014868              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +       
                                              - 
 y   1  1     904  0.010409              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1    1476  0.012678              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +     - 
                                              - 
 y   1  1    1760 -0.010292              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-         - 
                                              - 
 y   1  1    1805 -0.011325             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1890  0.011500              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
                                             +- 
 y   1  1    1918 -0.016876              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1924  0.013578             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1930 -0.011252             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1937 -0.010772             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1996  0.012653              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
                                              - 
 y   1  1    2002 -0.011214             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
                                              - 
 y   1  1    2020  0.011279              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
                                              - 
 y   1  1    2904 -0.011823              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +     - 
                                              - 
 y   1  1    3972  0.010151              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-         - 
                                              - 
 y   1  1    4041  0.010834             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4050  0.014432              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4056 -0.010239             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4126  0.016650              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4132 -0.013815              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4139  0.010425             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4146  0.012241             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4204 -0.011169              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     -    - 
                                              - 
 y   1  1    4688  0.011370             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +    +-    - 
                                              - 
 y   1  1    6164  0.011137             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +     - 
                                                
 y   1  1    6171 -0.012895             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +     - 
                                                
 y   1  1    6320  0.011829             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-      
                                              - 
 y   1  1    6788  0.011774             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6795 -0.013506             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6873  0.011945             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-   +- 
                                                
 y   1  1    7015 -0.011357              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7021  0.014299             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7027  0.012251             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7030 -0.013247             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7549 -0.010106             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -      
                                             +- 
 y   1  1    7561 -0.010081              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7567  0.011979             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7573  0.010667             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7576 -0.011240             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7645 -0.010569             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    -   +-   +- 
                                                
 y   1  1    7651 -0.010087             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    -   +-   +- 
                                                
 y   1  1    7654  0.010475             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    -   +-   +- 
                                                
 y   1  1    7802  0.013994             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1    7809 -0.014769             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1   11632  0.010782             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +     - 
                                                
 y   1  1   11779 -0.010790             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-      
                                              - 
 y   1  1   12222 -0.010521             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    -      
                                             +- 
 y   1  1   12229  0.011393             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    -      
                                             +- 
 y   1  1   12247 -0.010411             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12253 -0.010239             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12256  0.010884             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12473  0.010227              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12482 -0.016172             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12489  0.016438             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   13028 -0.013752             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13035  0.014029             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13106  0.012093             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-    -   +-   +- 
                                                
 y   1  1   13113 -0.013131             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-    -   +-   +- 
                                                
 y   1  1   13261 -0.012398             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   13267 -0.010949             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   13270  0.011978             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   17067  0.010793             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   19778 -0.020768              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19791 -0.014910             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19806 -0.022435              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19818 -0.014305             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19858 -0.020999              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19870 -0.014669             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19882 -0.022283              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   19895 -0.014445             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   22524 -0.011369             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   25240  0.024598              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25252  0.015865             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25264  0.019755              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25277  0.013546             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25316  0.024457              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25322 -0.010007              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25329  0.015920             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25344  0.019866              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   25356  0.013402             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   28148  0.010364              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-      
                                              - 
 y   1  1   30696  0.016988              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   30710 -0.010977             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   30723 -0.017717              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   30775 -0.017881              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   30800  0.017739              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   30814 -0.010361             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   31451  0.010401              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +    +-    - 
                                              - 
 y   1  1   33609  0.010449              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-      
                                              - 
 y   1  1   36157  0.018425              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   36182 -0.016461              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   36196  0.010338             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   36234 -0.018382              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   36248  0.010811             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   36261  0.017237              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-      
                                              - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             133
     0.01> rq > 0.001           2182
    0.001> rq > 0.0001          4272
   0.0001> rq > 0.00001         3606
  0.00001> rq > 0.000001        1902
 0.000001> rq                  37708
           all                 49805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:        4961    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000004784735     -0.001086651268
     2     2      0.000000037706     -0.000008577954
     3     3     -0.613935886023    139.680119616936
     4     4      0.000000513106     -0.000116456748
     5     5     -0.000000004371      0.000000994128
     6     6      0.000000458976     -0.000104146291
     7     7     -0.708044530629    161.081062771916
     8     8      0.000000016154     -0.000003675310
     9     9     -0.000000005091      0.000001157779
    10    10      0.000000917885     -0.000208365502
    11    11     -0.000000003233      0.000000735151
    12    12     -0.000000012553      0.000002853125
    13    13     -0.000000000546      0.000000124045
    14    14     -0.017681073858      4.020811384402
    15    15      0.000000515054     -0.000116898357
    16    16     -0.000000003795      0.000000863007
    17    17      0.000000449450     -0.000101980824
    18    18     -0.019188175635      4.363575976310
    19    19     -0.000000000966      0.000000219677
    20    20     -0.000000021236      0.000004834777
    21    21     -0.000000006854      0.000001558762
    22    22      0.000003362586     -0.000763932851
    23    23     -0.089337483586     20.345301635191
    24    24      0.000000009335     -0.000002126912
    25    25     -0.038792633443      8.836918813495
    26    26     -0.000000002954      0.000000671179
    27    27     -0.000000033373      0.000007604820
    28    28     -0.000000000198      0.000000045110
    29    29      0.000000006206     -0.000001414109
    30    30     -0.080897261424     18.425445903910
    31    31      0.000000006428     -0.000001463830
    32    32     -0.000000000489      0.000000111317
    33    33     -0.000000018614      0.000004243093
    34    34     -0.000000000363      0.000000082721
    35    35      0.044702971995    -10.177734771766
    36    36      0.000000000481     -0.000000109427
    37    37     -0.000000030654      0.000006985219
    38    38     -0.000000033255      0.000007576984
    39    39      0.000000000189     -0.000000043073
    40    40     -0.003714979527      0.845687384149
    41    41      0.000003298234     -0.000748284329
    42    42     -0.000000000712      0.000000162054
    43    43      0.000003140304     -0.000711986496
    44    44     -0.047063266359     10.722320486029
    45    45      0.000000003531     -0.000000804697
    46    46     -0.000000068726      0.000015681813
    47    47     -0.000000007172      0.000001633893
    48    48      0.059557489527    -13.567585343039
    49    49      0.000000000672     -0.000000153146
    50    50     -0.000000068244      0.000015571706
    51    51     -0.000003544340      0.000805211753
    52    52      0.000000005289     -0.000001202825
    53    53      0.000000002890     -0.000000658290
    54    54      0.084653029552    -19.281457519462
    55    55      0.000000000919     -0.000000209019
    56    56      0.047622383586    -10.844665336302
    57    57      0.000000000555     -0.000000126443
    58    58     -0.000000029796      0.000006790041
    59    59      0.037911779831     -8.636451299897
    60    60      0.000000002401     -0.000000546371
    61    61      0.070162314588    -15.979700862569
    62    62     -0.000000028470      0.000006486507
    63    63      0.000000000588     -0.000000133747
    64    64     -0.003339133523      0.760181721194
    65    65     -0.000000009271      0.000002109606
    66    66     -0.000000050250      0.000011451478
    67    67     -0.000000000080      0.000000018120
    68    68     -0.000000000008      0.000000001652
    69    69     -0.000000030918      0.000007046158
    70    70      0.000000000113     -0.000000025783
    71    71      0.000000000875     -0.000000199073
    72    72     -0.000005854326      0.001328046095
    73    73      0.000000000587     -0.000000133678
    74    74      0.000000002454     -0.000000558858
    75    75      0.068482228713    -15.602755398449
    76    76      0.000000000675     -0.000000153675
    77    77     -0.078471453816     17.876210222522
    78    78     -0.000000004022      0.000000916422
    79    79     -0.000000129919      0.000029638574
    80    80      0.000000000580     -0.000000132137
    81    81      0.000000062862     -0.000013772147
    82    82     -0.000000001020      0.000000232436
    83    83      0.009871138521     -2.249702629537
    84    84      0.011584182542     -2.640867540745
    85    85     -0.000000000657      0.000000149595
    86    86     -0.000000000009      0.000000002287
    87    87     -0.000000000875      0.000000199261
    88    88     -0.000000389842      0.000089098393
    89    89      0.000000000599     -0.000000136350
    90    90      0.017775466352     -4.055747934548
    91    91      0.000003107338     -0.000704561000
    92    92     -0.000000000722      0.000000164258
    93    93      0.000002934822     -0.000665558566
    94    94     -0.053255310408     12.133996762904
    95    95     -0.000000009274      0.000002112629
    96    96     -0.000000063179      0.000014417697
    97    97      0.000000009782     -0.000002228390
    98    98      0.048328921272    -11.008808537105
    99    99      0.000000000599     -0.000000136404
   100   100     -0.000000062850      0.000014342629
   101   101      0.000000434977     -0.000099418879
   102   102      0.000000000805     -0.000000183293
   103   103      0.018193204659     -4.150703209154
   104   104     -0.000000001823      0.000000416127
   105   105     -0.000000748564      0.000171306952

 number of reference csfs (nref) is   105.  root number (iroot) is  3.
 c0**2 =   0.93543298  c**2 (all zwalks) =   0.94835105

 pople ci energy extrapolation is computed with 32 correlated electrons.

 eref      =   -227.524097911652   "relaxed" cnot**2         =   0.935432979821
 eci       =   -227.623178209046   deltae = eci - eref       =  -0.099080297393
 eci+dv1   =   -227.629575528607   dv1 = (1-cnot**2)*deltae  =  -0.006397319561
 eci+dv2   =   -227.630017095101   dv2 = dv1 / cnot**2       =  -0.006838886055
 eci+dv3   =   -227.630524138107   dv3 = dv1 / (2*cnot**2-1) =  -0.007345929061
 eci+pople =   -227.629999622880   ( 32e- scaled deltae )    =  -0.105901711228
maximum overlap with reference    1(overlap= 0.97037)
weight of reference states=  0.9416

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.95972)
weight of reference states=  0.9211

 information on vector: 2 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    3(overlap= 0.96016)
weight of reference states=  0.9219

 information on vector: 3 from unit 11 written to unit 48 filename civout              
 passed aftci ... 
                       Size (real*8) of d2temp for two-external contributions     169767
 
                       Size (real*8) of d2temp for all-internal contributions      17442
                       Size (real*8) of d2temp for one-external contributions      88920
                       Size (real*8) of d2temp for two-external contributions     169767
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   27     9750
                1    2    1    1   27     9750
                1    3    1    1   27     9750
                1    4    1    1   27     9750
                1    5    1    1   27     9750
                1    6    1    1   27     9750
                1    7    1    1   27     9750
                1    8    1    1   27     9750
                1    9    1    1   27     9750
                1   10    1    1   27     9750
                1   11    1    1   27     9750
                1   12    1    1   27     9750
                1   13    1    1   27     9750
                1   14    1    1   27     9750
                1   15    1    1   27     9750
                1   16    1    1   27     9750
                1   17    1    1   27     9750
                1   18    1    1   27     9750
                       Size (real*8) of d2temp for three-external contributions     175500
                       Size (real*8) of d2temp for four-external contributions      71253
 serial operation: forcing vdisk for temporary dd012 matrices
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=  288769
 files%d4ext =     unit=  24  vdsk=   1  filestart=  468769
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                  6145     24577    116737         1         1
d2rec                     3        15        28         3         2
recsize                6144      6144      6144     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                 588769  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=   100.585551835256     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -227.77490373  -328.36045556
 residuum:     0.00005753
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047     0.00000002     0.00000102     0.00015222     0.03774196    -0.02641190    -0.07045310     0.00115783
 ref:   2    -0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636     0.00000525     0.00001728    -0.00000098
 ref:   3    -0.00000000    -0.00000044     0.96016413    -0.00000911     0.00000089     0.15394543    -0.00723570    -0.05650070

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047     0.00000002     0.00000102     0.00015222     0.03774196    -0.02641190     0.00000000     0.00000000
 ref:   2    -0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636     0.00000525     0.00000000     0.00000000
 ref:   3    -0.00000000    -0.00000044     0.96016413    -0.00000911     0.00000089     0.15394543     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    32.000000
   32  correlated and     0  frozen core electrons
   1936 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    2.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        2.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        2.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        2.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        1.99639      -1.603803E-12   5.523851E-04   7.029420E-10
  a     6    0.00000        0.00000        0.00000        0.00000      -1.603803E-12    1.99639       6.405029E-10  -5.873311E-04
  a     7    0.00000        0.00000        0.00000        0.00000       5.523851E-04   6.405029E-10    1.99669       3.348903E-12
  a     8    0.00000        0.00000        0.00000        0.00000       7.029420E-10  -5.873311E-04   3.348903E-12    1.99665    
  a     9    0.00000        0.00000        0.00000        0.00000       1.339456E-13   2.132737E-11  -6.105713E-13   2.043514E-10
  a    10    0.00000        0.00000        0.00000        0.00000      -3.447277E-11  -6.759577E-12   1.882879E-10  -2.074840E-12
  a    11    0.00000        0.00000        0.00000        0.00000       3.261799E-03   2.498346E-09  -4.212492E-04  -1.995512E-10
  a    12    0.00000        0.00000        0.00000        0.00000      -2.452797E-09   3.252557E-03  -1.619675E-10   4.432006E-04
  a    13    0.00000        0.00000        0.00000        0.00000      -1.328869E-06  -5.127210E-13   4.709505E-05   1.474654E-11
  a    14    0.00000        0.00000        0.00000        0.00000       2.643611E-12  -3.780525E-06  -3.178990E-12   7.531888E-05
  a    15    0.00000        0.00000        0.00000        0.00000       4.543809E-13  -2.247860E-10   1.151214E-13  -1.019254E-10
  a    16    0.00000        0.00000        0.00000        0.00000       5.743198E-11  -5.996342E-13   3.970738E-10  -5.342870E-13
  a    17    0.00000        0.00000        0.00000        0.00000      -3.273129E-04  -3.529984E-10   1.352093E-03   9.695490E-11
  a    18    0.00000        0.00000        0.00000        0.00000       1.000376E-10  -9.441588E-05  -6.917264E-11   2.077572E-03
  a    19    0.00000        0.00000        0.00000        0.00000       1.428113E-03   1.725212E-09  -9.534817E-03   1.028166E-10
  a    20    0.00000        0.00000        0.00000        0.00000      -1.802885E-09   1.636989E-03  -5.753948E-10   9.471892E-03
  a    21    0.00000        0.00000        0.00000        0.00000       1.561727E-13   1.623014E-10  -3.110108E-11   3.689015E-10
  a    22    0.00000        0.00000        0.00000        0.00000       9.360878E-03   1.105688E-08   2.750295E-03  -1.593913E-10
  a    23    0.00000        0.00000        0.00000        0.00000       4.715761E-10   8.430463E-12   2.790339E-10  -1.846018E-11
  a    24    0.00000        0.00000        0.00000        0.00000      -1.027503E-08   9.311582E-03   2.672863E-10  -1.606577E-03
  a    25    0.00000        0.00000        0.00000        0.00000       9.672807E-05   8.336046E-11   1.249626E-03  -3.511065E-11
  a    26    0.00000        0.00000        0.00000        0.00000      -8.389996E-10   7.489465E-04   2.201908E-10  -2.561797E-03
  a    27    0.00000        0.00000        0.00000        0.00000      -2.313465E-11  -1.406685E-10   9.373371E-13   4.887319E-10
  a    28    0.00000        0.00000        0.00000        0.00000       3.218827E-03   3.760827E-09   3.797288E-03  -3.411540E-11
  a    29    0.00000        0.00000        0.00000        0.00000      -1.379034E-10  -1.186884E-11  -2.977746E-10   2.868607E-11
  a    30    0.00000        0.00000        0.00000        0.00000       8.945850E-09  -8.111340E-03  -1.203806E-10  -6.508342E-05
  a    31    0.00000        0.00000        0.00000        0.00000      -7.853194E-03  -9.366269E-09   2.444999E-03   9.542493E-11
  a    32    0.00000        0.00000        0.00000        0.00000      -1.742105E-09   1.607871E-03  -1.043493E-10   1.579706E-03
  a    33    0.00000        0.00000        0.00000        0.00000      -2.436742E-03  -2.897063E-09   5.933334E-03   1.027116E-11
  a    34    0.00000        0.00000        0.00000        0.00000       5.710096E-11  -8.310562E-11  -1.308370E-10  -1.937753E-10
  a    35    0.00000        0.00000        0.00000        0.00000       3.313846E-11  -1.197497E-11  -1.200258E-10  -2.277988E-11
  a    36    0.00000        0.00000        0.00000        0.00000       3.082001E-09  -2.740838E-03   6.117077E-10  -6.528352E-03
  a    37    0.00000        0.00000        0.00000        0.00000       1.648193E-04   7.349720E-10   9.080919E-04   1.319376E-09
  a    38    0.00000        0.00000        0.00000        0.00000       1.954921E-03   2.343749E-09  -4.256269E-04   3.626522E-10
  a    39    0.00000        0.00000        0.00000        0.00000      -9.942825E-10   1.035430E-03  -2.296641E-11  -1.894309E-03
  a    40    0.00000        0.00000        0.00000        0.00000       8.701399E-10  -7.973693E-04   4.838331E-11  -4.799716E-04
  a    41    0.00000        0.00000        0.00000        0.00000      -2.634726E-11  -2.948176E-11   1.361946E-11   1.637697E-10
  a    42    0.00000        0.00000        0.00000        0.00000      -1.140022E-10   1.072299E-11  -2.799662E-10   5.127053E-11
  a    43    0.00000        0.00000        0.00000        0.00000      -3.256166E-03  -3.932065E-09  -1.297121E-03   6.129939E-11
  a    44    0.00000        0.00000        0.00000        0.00000       3.298236E-09  -3.150680E-03  -1.486724E-10   8.031006E-04

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     5   1.339456E-13  -3.447277E-11   3.261799E-03  -2.452797E-09  -1.328869E-06   2.643611E-12   4.543809E-13   5.743198E-11
  a     6   2.132737E-11  -6.759577E-12   2.498346E-09   3.252557E-03  -5.127210E-13  -3.780525E-06  -2.247860E-10  -5.996342E-13
  a     7  -6.105713E-13   1.882879E-10  -4.212492E-04  -1.619675E-10   4.709505E-05  -3.178990E-12   1.151214E-13   3.970738E-10
  a     8   2.043514E-10  -2.074840E-12  -1.995512E-10   4.432006E-04   1.474654E-11   7.531888E-05  -1.019254E-10  -5.342870E-13
  a     9    1.99843       1.479568E-11   3.599207E-13   1.302936E-10   9.439499E-12   2.098870E-08   1.402763E-03   3.290102E-10
  a    10   1.479568E-11    1.99838       5.982805E-11   1.126817E-11   1.568228E-08   9.668798E-12   3.103237E-10  -1.419830E-03
  a    11   3.599207E-13   5.982805E-11    1.99286      -1.404091E-11   1.890338E-05  -3.581273E-12  -6.765821E-14  -3.606058E-10
  a    12   1.302936E-10   1.126817E-11  -1.404091E-11    1.99283       1.345748E-11  -3.627921E-06  -2.217429E-10  -1.869453E-12
  a    13   9.439499E-12   1.568228E-08   1.890338E-05   1.345748E-11    1.92414      -8.793708E-10  -1.444399E-10   4.310745E-08
  a    14   2.098870E-08   9.668798E-12  -3.581273E-12  -3.627921E-06  -8.793708E-10    1.92080      -7.008273E-08   1.876501E-10
  a    15   1.402763E-03   3.103237E-10  -6.765821E-14  -2.217429E-10  -1.444399E-10  -7.008273E-08    1.99113      -5.154451E-11
  a    16   3.290102E-10  -1.419830E-03  -3.606058E-10  -1.869453E-12   4.310745E-08   1.876501E-10  -5.154451E-11    1.99118    
  a    17   4.965180E-10  -2.016838E-07  -4.368697E-04  -8.234838E-11  -9.436519E-02   1.056301E-09  -1.090539E-09  -2.031182E-07
  a    18  -1.325210E-07   5.055639E-10  -1.503856E-10   3.182489E-04  -1.272499E-08  -8.737704E-02  -3.885544E-08   1.372022E-09
  a    19  -4.178232E-11  -1.777451E-09   1.555332E-04  -1.027490E-11   9.705746E-04  -1.096711E-10  -2.759804E-11   7.340448E-09
  a    20  -1.085254E-09   2.896813E-11  -2.648377E-10   4.642411E-04  -1.623239E-11   3.176784E-04  -4.989273E-09  -2.528447E-12
  a    21   3.035425E-03   3.015385E-10   1.889922E-11   7.719035E-10  -6.061504E-11  -5.515343E-09  -4.050700E-02  -1.385967E-09
  a    22   3.093279E-11  -5.646539E-09  -1.005672E-02  -4.304779E-09  -3.647749E-04   1.528551E-11  -1.110360E-10  -2.215200E-08
  a    23  -9.854878E-10   3.148537E-03  -1.932436E-10  -1.683286E-12  -6.303824E-09  -3.183779E-11   2.265622E-10   3.916818E-02
  a    24  -6.296368E-09   1.875675E-11   3.967467E-09  -1.167286E-02  -1.523980E-10  -2.067252E-03   1.706607E-08   6.398197E-11
  a    25   3.983816E-13   3.374530E-09   1.532508E-03   5.831500E-10  -1.238976E-02   1.320518E-09  -2.264667E-10  -6.408759E-08
  a    26   8.606211E-09   3.529668E-11  -1.851306E-10   4.862581E-04  -4.931251E-10  -1.022247E-02   7.120447E-09   1.175576E-10
  a    27   9.695636E-03   1.934259E-09  -2.974160E-11   3.018969E-10   5.610170E-11   8.680381E-09   1.204719E-02   7.160769E-10
  a    28  -1.172267E-11   1.096329E-09  -2.760126E-03  -1.200309E-09   4.586051E-03  -2.860681E-10   8.599882E-11   5.742090E-09
  a    29  -2.333649E-09   9.943761E-03   4.469660E-10   9.660593E-12   7.098425E-09   7.862442E-11  -5.074600E-10  -1.156492E-02
  a    30  -3.056908E-09  -7.807207E-12  -4.754601E-09   1.368924E-02  -1.973150E-10  -2.997296E-03   7.907975E-09   1.444921E-11
  a    31   2.387285E-11  -2.085553E-09   1.653356E-02   6.966003E-09   8.675904E-04  -1.026377E-10   2.948241E-11   7.831065E-10
  a    32  -1.836514E-09   3.783439E-12   3.037478E-09  -8.492778E-03  -2.006827E-10  -3.317853E-03   6.481949E-09   6.793426E-11
  a    33   7.514660E-11  -1.385455E-08   2.035097E-03   1.091844E-09  -2.917715E-04  -1.051686E-10  -2.931951E-10  -6.557293E-09
  a    34   2.560830E-03   4.394887E-10  -3.574486E-11   8.023544E-11  -6.260130E-11  -4.051174E-09  -1.327220E-02  -7.687375E-10
  a    35  -5.183019E-10   2.147125E-03   6.276306E-11   3.356576E-11  -4.763046E-09  -5.178864E-11   1.262114E-10   1.341962E-02
  a    36  -8.909417E-09  -1.696851E-11  -2.941068E-09   6.470934E-03   5.423519E-10   6.903815E-04   8.160719E-09  -7.678811E-11
  a    37  -5.899191E-11   5.928991E-08  -3.708527E-03  -2.937371E-09   3.068159E-03   2.388215E-10   1.994594E-11   3.084158E-08
  a    38   2.381904E-11  -2.421275E-08  -1.032562E-02  -4.258876E-09  -1.072346E-03  -2.443954E-10   1.472145E-11  -1.280307E-08
  a    39   1.679213E-08  -7.210947E-13   2.204541E-09  -8.732536E-03   1.484899E-10   1.447814E-03  -1.692021E-08  -2.444109E-11
  a    40   6.035046E-08  -7.301041E-11  -8.574266E-10   2.852618E-03   7.215882E-10   3.903781E-03  -5.964581E-08  -9.901891E-11
  a    41  -2.800356E-03  -7.976090E-10  -8.243481E-12   2.224962E-10  -3.792932E-11  -1.313490E-08  -6.817623E-03  -4.332132E-10
  a    42   4.626082E-10  -2.331431E-03  -7.902238E-11   3.691204E-12  -1.099153E-08  -3.762892E-11   6.056035E-10   5.631201E-03
  a    43   3.201309E-11   4.306678E-10  -1.115237E-03  -4.160055E-10   2.017789E-04  -8.874576E-12  -2.868021E-11  -5.610265E-10
  a    44  -9.648228E-11  -1.055658E-11   3.641517E-10  -9.843146E-04   2.572429E-11   4.208255E-04  -9.586152E-12  -1.570547E-11

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a     5  -3.273129E-04   1.000376E-10   1.428113E-03  -1.802885E-09   1.561727E-13   9.360878E-03   4.715761E-10  -1.027503E-08
  a     6  -3.529984E-10  -9.441588E-05   1.725212E-09   1.636989E-03   1.623014E-10   1.105688E-08   8.430463E-12   9.311582E-03
  a     7   1.352093E-03  -6.917264E-11  -9.534817E-03  -5.753948E-10  -3.110108E-11   2.750295E-03   2.790339E-10   2.672863E-10
  a     8   9.695490E-11   2.077572E-03   1.028166E-10   9.471892E-03   3.689015E-10  -1.593913E-10  -1.846018E-11  -1.606577E-03
  a     9   4.965180E-10  -1.325210E-07  -4.178232E-11  -1.085254E-09   3.035425E-03   3.093279E-11  -9.854878E-10  -6.296368E-09
  a    10  -2.016838E-07   5.055639E-10  -1.777451E-09   2.896813E-11   3.015385E-10  -5.646539E-09   3.148537E-03   1.875675E-11
  a    11  -4.368697E-04  -1.503856E-10   1.555332E-04  -2.648377E-10   1.889922E-11  -1.005672E-02  -1.932436E-10   3.967467E-09
  a    12  -8.234838E-11   3.182489E-04  -1.027490E-11   4.642411E-04   7.719035E-10  -4.304779E-09  -1.683286E-12  -1.167286E-02
  a    13  -9.436519E-02  -1.272499E-08   9.705746E-04  -1.623239E-11  -6.061504E-11  -3.647749E-04  -6.303824E-09  -1.523980E-10
  a    14   1.056301E-09  -8.737704E-02  -1.096711E-10   3.176784E-04  -5.515343E-09   1.528551E-11  -3.183779E-11  -2.067252E-03
  a    15  -1.090539E-09  -3.885544E-08  -2.759804E-11  -4.989273E-09  -4.050700E-02  -1.110360E-10   2.265622E-10   1.706607E-08
  a    16  -2.031182E-07   1.372022E-09   7.340448E-09  -2.528447E-12  -1.385967E-09  -2.215200E-08   3.916818E-02   6.398197E-11
  a    17   7.971615E-02   6.666788E-10  -3.599050E-04   7.049852E-12   5.885499E-11  -1.438098E-04   7.077955E-09   6.502175E-12
  a    18   6.666788E-10   7.617232E-02  -5.647034E-12  -3.145824E-04   1.045813E-08  -2.040722E-13   5.475736E-11  -4.777927E-05
  a    19  -3.599050E-04  -5.647034E-12   1.795455E-03   4.092338E-12   9.692703E-13   5.424958E-04   6.387466E-13   5.423757E-12
  a    20   7.049852E-12  -3.145824E-04   4.092338E-12   1.795469E-03  -1.022749E-10  -5.769851E-12   7.180113E-13   6.288265E-04
  a    21   5.885499E-11   1.045813E-08   9.692703E-13  -1.022749E-10   4.506327E-03   2.845154E-12  -3.699147E-11  -2.431420E-10
  a    22  -1.438098E-04  -2.040722E-13   5.424958E-04  -5.769851E-12   2.845154E-12   2.737589E-03  -3.172078E-10   6.771349E-12
  a    23   7.077955E-09   5.475736E-11   6.387466E-13   7.180113E-13  -3.699147E-11  -3.172078E-10   4.334408E-03  -1.043270E-13
  a    24   6.502175E-12  -4.777927E-05   5.423757E-12   6.288265E-04  -2.431420E-10   6.771349E-12  -1.043270E-13   2.887147E-03
  a    25   4.488448E-03   3.850097E-11  -2.101467E-05   1.763088E-13   6.679107E-12   1.042937E-04  -7.920773E-11   6.330965E-13
  a    26   5.708728E-12   3.205538E-03   9.149295E-13   4.132848E-05   7.445647E-10   9.135572E-12  -9.632232E-13   2.865734E-04
  a    27  -2.387777E-11  -5.919647E-09  -1.100345E-12   1.053654E-10  -1.853350E-03  -3.088599E-12   3.096701E-12  -2.184278E-10
  a    28  -8.097189E-04  -4.213223E-11   1.721179E-04  -2.774863E-12  -5.858206E-12   1.197241E-03  -9.584807E-11   1.932957E-12
  a    29  -6.553185E-09  -2.255325E-11   7.453472E-11  -8.756433E-13   2.483458E-11  -6.793503E-11  -1.827423E-03  -1.756345E-12
  a    30   7.314015E-12   4.129974E-04  -4.373468E-12  -5.514661E-04  -4.779412E-11  -3.232345E-12   2.231452E-12  -2.933792E-03
  a    31  -4.167634E-04  -4.163861E-12  -5.078123E-04   1.473099E-12   2.657133E-12  -2.616260E-03  -8.801743E-11  -1.887220E-11
  a    32  -1.091254E-12   5.660144E-04  -2.740046E-12  -1.315484E-04   3.571620E-11   2.970887E-12   7.863380E-13   6.809052E-04
  a    33  -1.639570E-03  -1.721253E-11  -1.095811E-03   7.161685E-12   5.035362E-11  -1.122581E-03  -4.229073E-10   4.009204E-12
  a    34   2.254585E-11   5.358110E-09   2.516162E-11  -8.776131E-11   2.354387E-03   2.660942E-11  -4.121617E-11   2.042261E-11
  a    35   5.398020E-09   2.781475E-12  -3.380349E-11  -2.814365E-12   9.641848E-12  -4.986866E-11   2.395562E-03  -6.287430E-12
  a    36   1.229086E-09  -1.173138E-03  -1.879581E-11  -1.130606E-03  -4.860245E-10   1.379550E-10  -9.087062E-12  -1.774600E-03
  a    37   6.432414E-03   3.218506E-10  -9.871803E-05   2.237465E-10   1.272007E-11   4.738260E-04   1.417601E-09   3.515751E-10
  a    38  -2.716540E-03  -8.199232E-11   4.754049E-04   2.108458E-11  -6.221469E-12   1.825957E-03  -5.045128E-10  -4.326341E-11
  a    39  -2.033656E-10   1.423006E-03   3.658172E-11   7.160035E-05   7.147332E-10   1.175732E-10   1.499647E-12   1.572868E-03
  a    40  -1.747053E-10   5.753355E-03  -2.639276E-12  -2.236992E-04   2.372191E-09  -5.113224E-12   4.051198E-12  -7.050867E-04
  a    41   3.347931E-11   6.005375E-09   9.224073E-13  -1.089416E-10   1.977571E-03   2.105799E-12   3.988706E-11   1.990694E-10
  a    42   6.592840E-09   3.069408E-11  -8.468130E-11   9.622791E-13  -7.933867E-11   1.077156E-10   1.707137E-03  -3.366380E-13
  a    43   1.986445E-05   1.541976E-13   2.188806E-04  -4.447424E-12   1.646530E-12  -8.085612E-04  -6.989463E-11   9.694147E-12
  a    44  -2.408560E-13  -9.402511E-05   5.097280E-12   1.709941E-04   3.237862E-12  -1.482970E-11   2.198575E-14  -7.650193E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a     5   9.672807E-05  -8.389996E-10  -2.313465E-11   3.218827E-03  -1.379034E-10   8.945850E-09  -7.853194E-03  -1.742105E-09
  a     6   8.336046E-11   7.489465E-04  -1.406685E-10   3.760827E-09  -1.186884E-11  -8.111340E-03  -9.366269E-09   1.607871E-03
  a     7   1.249626E-03   2.201908E-10   9.373371E-13   3.797288E-03  -2.977746E-10  -1.203806E-10   2.444999E-03  -1.043493E-10
  a     8  -3.511065E-11  -2.561797E-03   4.887319E-10  -3.411540E-11   2.868607E-11  -6.508342E-05   9.542493E-11   1.579706E-03
  a     9   3.983816E-13   8.606211E-09   9.695636E-03  -1.172267E-11  -2.333649E-09  -3.056908E-09   2.387285E-11  -1.836514E-09
  a    10   3.374530E-09   3.529668E-11   1.934259E-09   1.096329E-09   9.943761E-03  -7.807207E-12  -2.085553E-09   3.783439E-12
  a    11   1.532508E-03  -1.851306E-10  -2.974160E-11  -2.760126E-03   4.469660E-10  -4.754601E-09   1.653356E-02   3.037478E-09
  a    12   5.831500E-10   4.862581E-04   3.018969E-10  -1.200309E-09   9.660593E-12   1.368924E-02   6.966003E-09  -8.492778E-03
  a    13  -1.238976E-02  -4.931251E-10   5.610170E-11   4.586051E-03   7.098425E-09  -1.973150E-10   8.675904E-04  -2.006827E-10
  a    14   1.320518E-09  -1.022247E-02   8.680381E-09  -2.860681E-10   7.862442E-11  -2.997296E-03  -1.026377E-10  -3.317853E-03
  a    15  -2.264667E-10   7.120447E-09   1.204719E-02   8.599882E-11  -5.074600E-10   7.907975E-09   2.948241E-11   6.481949E-09
  a    16  -6.408759E-08   1.175576E-10   7.160769E-10   5.742090E-09  -1.156492E-02   1.444921E-11   7.831065E-10   6.793426E-11
  a    17   4.488448E-03   5.708728E-12  -2.387777E-11  -8.097189E-04  -6.553185E-09   7.314015E-12  -4.167634E-04  -1.091254E-12
  a    18   3.850097E-11   3.205538E-03  -5.919647E-09  -4.213223E-11  -2.255325E-11   4.129974E-04  -4.163861E-12   5.660144E-04
  a    19  -2.101467E-05   9.149295E-13  -1.100345E-12   1.721179E-04   7.453472E-11  -4.373468E-12  -5.078123E-04  -2.740046E-12
  a    20   1.763088E-13   4.132848E-05   1.053654E-10  -2.774863E-12  -8.756433E-13  -5.514661E-04   1.473099E-12  -1.315484E-04
  a    21   6.679107E-12   7.445647E-10  -1.853350E-03  -5.858206E-12   2.483458E-11  -4.779412E-11   2.657133E-12   3.571620E-11
  a    22   1.042937E-04   9.135572E-12  -3.088599E-12   1.197241E-03  -6.793503E-11  -3.232345E-12  -2.616260E-03   2.970887E-12
  a    23  -7.920773E-11  -9.632232E-13   3.096701E-12  -9.584807E-11  -1.827423E-03   2.231452E-12  -8.801743E-11   7.863380E-13
  a    24   6.330965E-13   2.865734E-04  -2.184278E-10   1.932957E-12  -1.756345E-12  -2.933792E-03  -1.887220E-11   6.809052E-04
  a    25   5.093722E-04  -2.625883E-12  -8.699628E-13   5.070671E-05  -9.125706E-10  -2.905341E-12   1.694743E-04  -6.163087E-13
  a    26  -2.625883E-12   3.782104E-04  -8.597146E-10   3.374458E-12   3.287914E-12  -7.819001E-05  -4.450092E-12  -1.413462E-04
  a    27  -8.699628E-13  -8.597146E-10   1.867399E-03   1.937222E-12  -6.353694E-12  -1.893112E-10  -2.084338E-13  -2.591208E-10
  a    28   5.070671E-05   3.374458E-12   1.937222E-12   6.522867E-04   3.221591E-10  -2.530069E-12  -8.214187E-04   2.660056E-12
  a    29  -9.125706E-10   3.287914E-12  -6.353694E-12   3.221591E-10   1.822150E-03   4.915906E-13   1.748983E-11   2.735146E-13
  a    30  -2.905341E-12  -7.819001E-05  -1.893112E-10  -2.530069E-12   4.915906E-13   3.289306E-03   1.463588E-11  -1.083470E-03
  a    31   1.694743E-04  -4.450092E-12  -2.084338E-13  -8.214187E-04   1.748983E-11   1.463588E-11   3.951583E-03  -8.444112E-12
  a    32  -6.163087E-13  -1.413462E-04  -2.591208E-10   2.660056E-12   2.735146E-13  -1.083470E-03  -8.444112E-12   7.928944E-04
  a    33  -7.055638E-05  -1.370063E-12  -2.313181E-11  -3.440262E-04  -3.116818E-11  -5.900374E-12   1.572720E-03   1.532732E-12
  a    34   6.793529E-13   6.274490E-10  -1.142264E-03   5.478608E-12   2.544163E-11   8.083645E-11  -3.220167E-11   1.201652E-10
  a    35   5.374877E-10  -2.730843E-12  -1.435877E-11  -1.811007E-10  -1.207184E-03   8.661715E-12  -2.436585E-11  -3.434656E-12
  a    36   7.887186E-11   9.462783E-06   1.513657E-10   3.234505E-11   5.220447E-12   2.166550E-03  -2.409789E-10  -8.886931E-04
  a    37   4.373891E-04   1.387046E-12  -8.323131E-12   9.769892E-05   4.456209E-10  -4.296734E-10  -9.096010E-04   1.765480E-10
  a    38  -3.491928E-04  -1.155472E-13   2.881094E-12   5.877641E-04  -2.208360E-10   5.938975E-11  -2.905020E-03  -2.602547E-11
  a    39  -2.204878E-11   3.941484E-05  -1.943154E-11   3.938441E-11  -1.776163E-12  -1.936032E-03  -1.914285E-10   8.787007E-04
  a    40  -1.104853E-11   3.218787E-04   8.741324E-11  -2.629757E-12  -3.968103E-12   8.453440E-04   1.214283E-11  -2.952490E-04
  a    41   3.415236E-12   8.661622E-10  -1.199649E-03  -2.320158E-12  -2.195320E-11   2.649192E-10   3.606669E-13   2.888406E-10
  a    42   9.173917E-10  -3.118940E-13   3.736698E-11  -2.833442E-10  -1.034494E-03   1.145451E-12  -1.312757E-11   3.011524E-14
  a    43  -2.631335E-05  -1.629595E-12   3.294085E-13  -3.436596E-04   3.729083E-11  -1.014456E-11   6.200817E-04   1.737210E-12
  a    44  -7.634377E-13  -9.486179E-05   2.649359E-11  -6.031738E-12   6.182297E-13   7.031912E-04   1.356854E-11  -1.321032E-04

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a     5  -2.436742E-03   5.710096E-11   3.313846E-11   3.082001E-09   1.648193E-04   1.954921E-03  -9.942825E-10   8.701399E-10
  a     6  -2.897063E-09  -8.310562E-11  -1.197497E-11  -2.740838E-03   7.349720E-10   2.343749E-09   1.035430E-03  -7.973693E-04
  a     7   5.933334E-03  -1.308370E-10  -1.200258E-10   6.117077E-10   9.080919E-04  -4.256269E-04  -2.296641E-11   4.838331E-11
  a     8   1.027116E-11  -1.937753E-10  -2.277988E-11  -6.528352E-03   1.319376E-09   3.626522E-10  -1.894309E-03  -4.799716E-04
  a     9   7.514660E-11   2.560830E-03  -5.183019E-10  -8.909417E-09  -5.899191E-11   2.381904E-11   1.679213E-08   6.035046E-08
  a    10  -1.385455E-08   4.394887E-10   2.147125E-03  -1.696851E-11   5.928991E-08  -2.421275E-08  -7.210947E-13  -7.301041E-11
  a    11   2.035097E-03  -3.574486E-11   6.276306E-11  -2.941068E-09  -3.708527E-03  -1.032562E-02   2.204541E-09  -8.574266E-10
  a    12   1.091844E-09   8.023544E-11   3.356576E-11   6.470934E-03  -2.937371E-09  -4.258876E-09  -8.732536E-03   2.852618E-03
  a    13  -2.917715E-04  -6.260130E-11  -4.763046E-09   5.423519E-10   3.068159E-03  -1.072346E-03   1.484899E-10   7.215882E-10
  a    14  -1.051686E-10  -4.051174E-09  -5.178864E-11   6.903815E-04   2.388215E-10  -2.443954E-10   1.447814E-03   3.903781E-03
  a    15  -2.931951E-10  -1.327220E-02   1.262114E-10   8.160719E-09   1.994594E-11   1.472145E-11  -1.692021E-08  -5.964581E-08
  a    16  -6.557293E-09  -7.687375E-10   1.341962E-02  -7.678811E-11   3.084158E-08  -1.280307E-08  -2.444109E-11  -9.901891E-11
  a    17  -1.639570E-03   2.254585E-11   5.398020E-09   1.229086E-09   6.432414E-03  -2.716540E-03  -2.033656E-10  -1.747053E-10
  a    18  -1.721253E-11   5.358110E-09   2.781475E-12  -1.173138E-03   3.218506E-10  -8.199232E-11   1.423006E-03   5.753355E-03
  a    19  -1.095811E-03   2.516162E-11  -3.380349E-11  -1.879581E-11  -9.871803E-05   4.754049E-04   3.658172E-11  -2.639276E-12
  a    20   7.161685E-12  -8.776131E-11  -2.814365E-12  -1.130606E-03   2.237465E-10   2.108458E-11   7.160035E-05  -2.236992E-04
  a    21   5.035362E-11   2.354387E-03   9.641848E-12  -4.860245E-10   1.272007E-11  -6.221469E-12   7.147332E-10   2.372191E-09
  a    22  -1.122581E-03   2.660942E-11  -4.986866E-11   1.379550E-10   4.738260E-04   1.825957E-03   1.175732E-10  -5.113224E-12
  a    23  -4.229073E-10  -4.121617E-11   2.395562E-03  -9.087062E-12   1.417601E-09  -5.045128E-10   1.499647E-12   4.051198E-12
  a    24   4.009204E-12   2.042261E-11  -6.287430E-12  -1.774600E-03   3.515751E-10  -4.326341E-11   1.572868E-03  -7.050867E-04
  a    25  -7.055638E-05   6.793529E-13   5.374877E-10   7.887186E-11   4.373891E-04  -3.491928E-04  -2.204878E-11  -1.104853E-11
  a    26  -1.370063E-12   6.274490E-10  -2.730843E-12   9.462783E-06   1.387046E-12  -1.155472E-13   3.941484E-05   3.218787E-04
  a    27  -2.313181E-11  -1.142264E-03  -1.435877E-11   1.513657E-10  -8.323131E-12   2.881094E-12  -1.943154E-11   8.741324E-11
  a    28  -3.440262E-04   5.478608E-12  -1.811007E-10   3.234505E-11   9.769892E-05   5.877641E-04   3.938441E-11  -2.629757E-12
  a    29  -3.116818E-11   2.544163E-11  -1.207184E-03   5.220447E-12   4.456209E-10  -2.208360E-10  -1.776163E-12  -3.968103E-12
  a    30  -5.900374E-12   8.083645E-11   8.661715E-12   2.166550E-03  -4.296734E-10   5.938975E-11  -1.936032E-03   8.453440E-04
  a    31   1.572720E-03  -3.220167E-11  -2.436585E-11  -2.409789E-10  -9.096010E-04  -2.905020E-03  -1.914285E-10   1.214283E-11
  a    32   1.532732E-12   1.201652E-10  -3.434656E-12  -8.886931E-04   1.765480E-10  -2.602547E-11   8.787007E-04  -2.952490E-04
  a    33   1.231435E-03   2.421092E-12  -1.734325E-10  -1.509624E-10  -5.658605E-04  -1.349566E-03  -7.995440E-11   2.581985E-12
  a    34   2.421092E-12   1.305187E-03  -7.042242E-12  -2.307715E-10   1.354335E-11   2.752782E-11   2.713813E-10   8.954691E-10
  a    35  -1.734325E-10  -7.042242E-12   1.407367E-03   3.522558E-12   4.647087E-10  -1.847990E-10  -6.116137E-12   2.740945E-12
  a    36  -1.509624E-10  -2.307715E-10   3.522558E-12   2.356468E-03  -2.130351E-10   2.315573E-10  -1.771631E-03   6.234853E-04
  a    37  -5.658605E-04   1.354335E-11   4.647087E-10  -2.130351E-10   1.214471E-03   5.510425E-04   3.865826E-10  -1.272757E-10
  a    38  -1.349566E-03   2.752782E-11  -1.847990E-10   2.315573E-10   5.510425E-04   3.110994E-03   1.239372E-10   1.013156E-11
  a    39  -7.995440E-11   2.713813E-10  -6.116137E-12  -1.771631E-03   3.865826E-10   1.239372E-10   1.926895E-03  -5.231157E-04
  a    40   2.581985E-12   8.954691E-10   2.740945E-12   6.234853E-04  -1.272757E-10   1.013156E-11  -5.231157E-04   1.060431E-03
  a    41   2.278664E-11   1.129270E-03   4.034619E-11  -9.927567E-11   9.389770E-12  -3.395741E-12  -5.901825E-11  -2.050964E-10
  a    42   7.438164E-11  -5.402101E-11   1.045192E-03  -4.576887E-12  -4.517853E-10   2.122309E-10   1.188459E-12   5.035633E-12
  a    43  -8.689041E-07   2.305690E-13  -2.911086E-11  -6.604404E-12  -2.022588E-05  -8.914261E-05  -3.191853E-12  -5.565641E-13
  a    44   1.989443E-14  -7.120277E-12   1.235906E-13   4.053028E-05  -7.929636E-12   4.655438E-12  -1.229731E-04   4.443435E-05

               a    41        a    42        a    43        a    44
  a     5  -2.634726E-11  -1.140022E-10  -3.256166E-03   3.298236E-09
  a     6  -2.948176E-11   1.072299E-11  -3.932065E-09  -3.150680E-03
  a     7   1.361946E-11  -2.799662E-10  -1.297121E-03  -1.486724E-10
  a     8   1.637697E-10   5.127053E-11   6.129939E-11   8.031006E-04
  a     9  -2.800356E-03   4.626082E-10   3.201309E-11  -9.648228E-11
  a    10  -7.976090E-10  -2.331431E-03   4.306678E-10  -1.055658E-11
  a    11  -8.243481E-12  -7.902238E-11  -1.115237E-03   3.641517E-10
  a    12   2.224962E-10   3.691204E-12  -4.160055E-10  -9.843146E-04
  a    13  -3.792932E-11  -1.099153E-08   2.017789E-04   2.572429E-11
  a    14  -1.313490E-08  -3.762892E-11  -8.874576E-12   4.208255E-04
  a    15  -6.817623E-03   6.056035E-10  -2.868021E-11  -9.586152E-12
  a    16  -4.332132E-10   5.631201E-03  -5.610265E-10  -1.570547E-11
  a    17   3.347931E-11   6.592840E-09   1.986445E-05  -2.408560E-13
  a    18   6.005375E-09   3.069408E-11   1.541976E-13  -9.402511E-05
  a    19   9.224073E-13  -8.468130E-11   2.188806E-04   5.097280E-12
  a    20  -1.089416E-10   9.622791E-13  -4.447424E-12   1.709941E-04
  a    21   1.977571E-03  -7.933867E-11   1.646530E-12   3.237862E-12
  a    22   2.105799E-12   1.077156E-10  -8.085612E-04  -1.482970E-11
  a    23   3.988706E-11   1.707137E-03  -6.989463E-11   2.198575E-14
  a    24   1.990694E-10  -3.366380E-13   9.694147E-12  -7.650193E-04
  a    25   3.415236E-12   9.173917E-10  -2.631335E-05  -7.634377E-13
  a    26   8.661622E-10  -3.118940E-13  -1.629595E-12  -9.486179E-05
  a    27  -1.199649E-03   3.736698E-11   3.294085E-13   2.649359E-11
  a    28  -2.320158E-12  -2.833442E-10  -3.436596E-04  -6.031738E-12
  a    29  -2.195320E-11  -1.034494E-03   3.729083E-11   6.182297E-13
  a    30   2.649192E-10   1.145451E-12  -1.014456E-11   7.031912E-04
  a    31   3.606669E-13  -1.312757E-11   6.200817E-04   1.356854E-11
  a    32   2.888406E-10   3.011524E-14   1.737210E-12  -1.321032E-04
  a    33   2.278664E-11   7.438164E-11  -8.689041E-07   1.989443E-14
  a    34   1.129270E-03  -5.402101E-11   2.305690E-13  -7.120277E-12
  a    35   4.034619E-11   1.045192E-03  -2.911086E-11   1.235906E-13
  a    36  -9.927567E-11  -4.576887E-12  -6.604404E-12   4.053028E-05
  a    37   9.389770E-12  -4.517853E-10  -2.022588E-05  -7.929636E-12
  a    38  -3.395741E-12   2.122309E-10  -8.914261E-05   4.655438E-12
  a    39  -5.901825E-11   1.188459E-12  -3.191853E-12  -1.229731E-04
  a    40  -2.050964E-10   5.035633E-12  -5.565641E-13   4.443435E-05
  a    41   1.216697E-03  -1.325199E-11   1.979890E-13  -2.270144E-11
  a    42  -1.325199E-11   9.587100E-04  -2.384392E-11   3.075653E-14
  a    43   1.979890E-13  -2.384392E-11   5.904443E-04   2.162877E-12
  a    44  -2.270144E-11   3.075653E-14   2.162877E-12   5.407870E-04

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99877616     1.99873601     1.99840321     1.99838427
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99679986     1.99676030     1.99185235     1.99182992     1.99117479     1.99115668     1.92904997     1.92500599
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.07582848     0.07268851     0.00941325     0.00939655     0.00683411     0.00659697     0.00211533     0.00205720
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00165341     0.00149665     0.00084590     0.00078259     0.00055747     0.00044113     0.00043130     0.00032940
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00013511     0.00012846     0.00010990     0.00010713     0.00005391     0.00003290     0.00000976     0.00000832
              MO    41       MO    42       MO    43       MO    44
  occ(*)=     0.00000718     0.00000496     0.00000265     0.00000199


 total number of electrons =   32.0000000000

 item #                     2 suffix=:.drt1.state2:
 read_civout: repnuc=   100.585551835256     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -227.77490373  -328.36045556
 residuum:     0.00005753
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -227.62386331  -328.20941515
 residuum:     0.00007617
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047     0.00000002     0.00000102     0.00015222     0.03774196    -0.02641190    -0.07045310     0.00115783
 ref:   2    -0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636     0.00000525     0.00001728    -0.00000098
 ref:   3    -0.00000000    -0.00000044     0.96016413    -0.00000911     0.00000089     0.15394543    -0.00723570    -0.05650070

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047     0.00000002     0.00000102     0.00015222     0.03774196    -0.02641190     0.00000000     0.00000000
 ref:   2    -0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636     0.00000525     0.00000000     0.00000000
 ref:   3    -0.00000000    -0.00000044     0.96016413    -0.00000911     0.00000089     0.15394543     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    32.000000
   32  correlated and     0  frozen core electrons
   1936 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    2.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        2.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        2.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        2.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        1.99679      -2.303827E-10   1.068473E-03   1.311315E-09
  a     6    0.00000        0.00000        0.00000        0.00000      -2.303827E-10    1.99678       1.220022E-09  -1.081288E-03
  a     7    0.00000        0.00000        0.00000        0.00000       1.068473E-03   1.220022E-09    1.99518       2.380616E-10
  a     8    0.00000        0.00000        0.00000        0.00000       1.311315E-09  -1.081288E-03   2.380616E-10    1.99509    
  a     9    0.00000        0.00000        0.00000        0.00000      -7.780260E-13  -8.525031E-11  -2.573138E-12  -1.376580E-10
  a    10    0.00000        0.00000        0.00000        0.00000      -3.075072E-11  -4.736971E-12  -1.608150E-10   1.119469E-12
  a    11    0.00000        0.00000        0.00000        0.00000       2.302155E-03   1.818865E-09   5.686377E-04  -2.590559E-11
  a    12    0.00000        0.00000        0.00000        0.00000      -1.670720E-09   2.289810E-03   5.118886E-10  -5.877689E-04
  a    13    0.00000        0.00000        0.00000        0.00000       2.527993E-05   2.746173E-11  -5.660926E-05   2.195265E-12
  a    14    0.00000        0.00000        0.00000        0.00000      -3.949426E-11   3.422334E-05   7.976580E-12  -1.098651E-04
  a    15    0.00000        0.00000        0.00000        0.00000      -3.506249E-12  -1.386701E-10  -1.815277E-11  -9.370522E-10
  a    16    0.00000        0.00000        0.00000        0.00000      -1.209291E-10  -4.299174E-13   9.894352E-10  -1.721607E-12
  a    17    0.00000        0.00000        0.00000        0.00000      -2.976649E-05   1.168783E-11  -3.793491E-04   4.779910E-11
  a    18    0.00000        0.00000        0.00000        0.00000       2.162636E-11   1.066548E-05  -5.333556E-11  -2.435913E-03
  a    19    0.00000        0.00000        0.00000        0.00000      -2.447500E-03  -1.725861E-09   5.385999E-03   2.038636E-09
  a    20    0.00000        0.00000        0.00000        0.00000       3.930322E-09  -2.424087E-03  -1.643979E-09  -5.079119E-03
  a    21    0.00000        0.00000        0.00000        0.00000       1.350766E-11  -2.286017E-11  -2.835178E-11  -4.134703E-11
  a    22    0.00000        0.00000        0.00000        0.00000      -4.003499E-04   1.591184E-09  -3.119831E-03  -1.220776E-09
  a    23    0.00000        0.00000        0.00000        0.00000      -3.323535E-11   1.864062E-11   5.279831E-11  -1.000586E-11
  a    24    0.00000        0.00000        0.00000        0.00000       2.697301E-09  -5.602944E-04   9.777077E-10   2.959486E-03
  a    25    0.00000        0.00000        0.00000        0.00000      -5.893759E-05   2.281102E-11  -4.639251E-04  -3.595266E-10
  a    26    0.00000        0.00000        0.00000        0.00000       3.468314E-10  -1.091727E-04   4.474985E-10   7.537171E-04
  a    27    0.00000        0.00000        0.00000        0.00000      -1.588214E-11   5.671576E-11   1.053618E-11  -7.624639E-11
  a    28    0.00000        0.00000        0.00000        0.00000      -3.706693E-04   4.589547E-10  -1.600430E-03  -1.164650E-09
  a    29    0.00000        0.00000        0.00000        0.00000      -3.053948E-11  -6.197600E-12   2.334501E-11   1.785682E-11
  a    30    0.00000        0.00000        0.00000        0.00000      -3.162466E-09   1.035339E-03  -1.098191E-09  -1.244394E-03
  a    31    0.00000        0.00000        0.00000        0.00000       8.670992E-04  -8.139229E-10   7.612921E-04   7.048146E-10
  a    32    0.00000        0.00000        0.00000        0.00000      -1.694348E-10   3.662728E-04  -1.865950E-11   9.240040E-05
  a    33    0.00000        0.00000        0.00000        0.00000       1.637096E-03   1.034894E-09  -1.268277E-03  -6.352709E-10
  a    34    0.00000        0.00000        0.00000        0.00000      -3.118309E-11   8.526605E-12   2.751698E-11   5.768068E-11
  a    35    0.00000        0.00000        0.00000        0.00000      -8.020755E-12   5.862320E-12   5.258670E-11   1.534511E-12
  a    36    0.00000        0.00000        0.00000        0.00000      -2.965581E-09   1.764234E-03   4.040012E-10   1.021644E-03
  a    37    0.00000        0.00000        0.00000        0.00000      -2.764337E-04  -6.518193E-10  -7.432435E-04  -2.828424E-10
  a    38    0.00000        0.00000        0.00000        0.00000      -1.497218E-03  -1.202511E-09  -1.088573E-03  -1.755530E-10
  a    39    0.00000        0.00000        0.00000        0.00000       1.240340E-09  -9.690921E-04   1.763441E-10   1.465621E-03
  a    40    0.00000        0.00000        0.00000        0.00000      -7.880646E-10   4.816929E-04  -1.287646E-11  -4.347298E-05
  a    41    0.00000        0.00000        0.00000        0.00000      -2.642617E-11  -2.775456E-11   9.678677E-12   4.092255E-11
  a    42    0.00000        0.00000        0.00000        0.00000       2.823236E-11   1.437451E-11   7.207761E-11   5.032589E-11
  a    43    0.00000        0.00000        0.00000        0.00000       1.751885E-03   1.270464E-09   8.259061E-04   2.197895E-10
  a    44    0.00000        0.00000        0.00000        0.00000      -2.701085E-09   1.802719E-03  -1.546554E-11  -7.805138E-04

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     5  -7.780260E-13  -3.075072E-11   2.302155E-03  -1.670720E-09   2.527993E-05  -3.949426E-11  -3.506249E-12  -1.209291E-10
  a     6  -8.525031E-11  -4.736971E-12   1.818865E-09   2.289810E-03   2.746173E-11   3.422334E-05  -1.386701E-10  -4.299174E-13
  a     7  -2.573138E-12  -1.608150E-10   5.686377E-04   5.118886E-10  -5.660926E-05   7.976580E-12  -1.815277E-11   9.894352E-10
  a     8  -1.376580E-10   1.119469E-12  -2.590559E-11  -5.877689E-04   2.195265E-12  -1.098651E-04  -9.370522E-10  -1.721607E-12
  a     9    1.98958       1.774600E-09   5.260410E-12  -8.925791E-11   5.894327E-11  -4.207309E-09  -3.153603E-02  -1.667836E-08
  a    10   1.774600E-09    1.98678      -1.498740E-10  -1.939659E-11  -6.683703E-09   1.236158E-10  -4.794064E-09   5.045094E-02
  a    11   5.260410E-12  -1.498740E-10    1.99429       8.984694E-11   1.149307E-05  -3.964335E-11   5.464337E-11   1.441227E-09
  a    12  -8.925791E-11  -1.939659E-11   8.984694E-11    1.99426      -2.407310E-11   6.223313E-05   1.605241E-10   1.449807E-10
  a    13   5.894327E-11  -6.683703E-09   1.149307E-05  -2.407310E-11    1.94211       2.607293E-09   9.649156E-10   9.317940E-08
  a    14  -4.207309E-09   1.236158E-10  -3.964335E-11   6.223313E-05   2.607293E-09    1.93764      -7.693619E-08  -1.487221E-09
  a    15  -3.153603E-02  -4.794064E-09   5.464337E-11   1.605241E-10   9.649156E-10  -7.693619E-08    1.57679       4.407432E-08
  a    16  -1.667836E-08   5.045094E-02   1.441227E-09   1.449807E-10   9.317940E-08  -1.487221E-09   4.407432E-08    1.42728    
  a    17   3.049053E-10  -1.145028E-08  -6.827418E-05   2.268293E-10   3.931342E-02   6.896450E-09  -2.555327E-10   1.714534E-09
  a    18  -9.307268E-09   3.039722E-10   3.321161E-10  -6.013515E-04   1.317360E-08   3.650210E-02  -3.090538E-09   3.992863E-10
  a    19  -4.306576E-11   8.965261E-11  -3.387563E-03  -6.559493E-10   1.884058E-04   9.467378E-11  -1.418589E-11  -8.185983E-10
  a    20  -3.059991E-10   3.846504E-11   1.844675E-09  -3.180161E-03  -4.639205E-11   3.349521E-04  -1.402799E-09  -7.064792E-12
  a    21  -3.796604E-04  -4.499915E-09   8.925989E-12  -2.006661E-10  -1.360186E-11   9.132066E-09  -2.753136E-02   2.827595E-09
  a    22   1.350013E-11   1.039300E-10   2.576749E-03   4.027576E-10  -2.150303E-03   5.932188E-11  -8.691825E-11  -1.460416E-09
  a    23  -4.061391E-09   9.446384E-04   9.175368E-11  -1.554140E-11   8.005918E-09  -2.543717E-12  -2.178610E-09   1.995784E-02
  a    24  -6.784219E-11  -2.456307E-11  -1.564352E-09   2.204927E-03  -2.883569E-10  -1.508853E-03   8.697864E-10   4.163802E-11
  a    25  -2.787316E-11   1.672196E-09   3.071104E-04  -3.452200E-11  -8.722493E-03   1.041152E-10  -1.864221E-10  -5.540650E-09
  a    26   5.348600E-10  -1.312479E-11  -4.776947E-10   7.760923E-04  -1.778062E-09  -6.005771E-03   4.158166E-09   1.092300E-10
  a    27  -5.081156E-03  -3.199777E-09  -2.177891E-11  -6.280116E-11   3.618695E-11  -2.798156E-10   3.212391E-03  -7.205021E-10
  a    28  -2.255461E-11  -2.775193E-10   1.406684E-03  -6.029419E-11   3.145585E-03   2.298703E-10   6.570305E-11   2.057511E-09
  a    29  -9.224290E-10  -5.317682E-03  -1.390322E-10  -2.575144E-11  -5.602188E-10   5.568248E-11   5.238411E-11   2.325662E-04
  a    30   7.996737E-11   1.933412E-11   1.735514E-09  -1.567070E-03  -4.916103E-10  -2.922746E-03   1.351893E-09   1.401076E-11
  a    31   1.444939E-11  -7.327018E-11  -9.203811E-04   7.497525E-10  -9.738573E-05   7.997425E-11   2.011196E-11   2.167590E-10
  a    32   1.318276E-10  -1.346817E-11  -8.517959E-11  -4.895046E-04  -5.925149E-10  -3.470768E-03   1.519976E-09   5.458048E-11
  a    33  -2.488202E-12  -4.215792E-10  -9.069728E-04   2.604760E-10  -2.576102E-03   1.612271E-10  -2.317694E-10  -1.676773E-11
  a    34  -1.415260E-04  -9.476760E-10   1.888205E-11  -3.737067E-11  -3.313840E-11   2.997060E-09  -9.627082E-03  -1.164594E-09
  a    35  -9.548080E-10   3.430861E-04   1.163698E-11  -2.589442E-12   2.697374E-09  -5.335909E-11   4.898279E-10   6.639913E-03
  a    36  -2.676467E-10  -4.745831E-12   1.946737E-09  -1.604309E-03   2.236714E-09  -6.969935E-04   1.935983E-10  -3.730068E-11
  a    37   5.254699E-12   1.746429E-09   1.149885E-03   8.202366E-11   1.065320E-02  -3.944974E-10   7.208009E-11   2.291901E-09
  a    38  -5.577531E-12  -8.289600E-10   3.450782E-03  -5.277229E-10  -4.626515E-03   2.472221E-10  -1.149695E-11  -9.968410E-10
  a    39   3.629950E-10  -5.142919E-12  -2.494466E-09   3.235233E-03  -2.625175E-10   3.467428E-03  -8.618930E-10  -3.431473E-11
  a    40   1.495673E-09   7.767870E-12   9.839140E-10  -1.126587E-03  -2.683531E-10   1.161101E-02  -2.519486E-09  -1.072704E-10
  a    41   8.244867E-04  -3.940912E-10  -1.661812E-11  -6.648689E-11  -6.030360E-12   3.508513E-09   2.980353E-03   2.147654E-09
  a    42  -9.075902E-10   9.320467E-04   1.939033E-11   7.833691E-12   2.994826E-09  -1.019321E-11  -1.815623E-09  -6.486234E-03
  a    43   3.221113E-11   5.907990E-11  -1.876973E-03  -8.523027E-10   1.104109E-04   6.509240E-11  -2.431135E-11   2.765176E-11
  a    44  -8.607952E-13  -7.508451E-12   4.805664E-10  -1.780140E-03   3.020442E-11   2.280514E-04  -1.687787E-10  -8.501667E-12

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a     5  -2.976649E-05   2.162636E-11  -2.447500E-03   3.930322E-09   1.350766E-11  -4.003499E-04  -3.323535E-11   2.697301E-09
  a     6   1.168783E-11   1.066548E-05  -1.725861E-09  -2.424087E-03  -2.286017E-11   1.591184E-09   1.864062E-11  -5.602944E-04
  a     7  -3.793491E-04  -5.333556E-11   5.385999E-03  -1.643979E-09  -2.835178E-11  -3.119831E-03   5.279831E-11   9.777077E-10
  a     8   4.779910E-11  -2.435913E-03   2.038636E-09  -5.079119E-03  -4.134703E-11  -1.220776E-09  -1.000586E-11   2.959486E-03
  a     9   3.049053E-10  -9.307268E-09  -4.306576E-11  -3.059991E-10  -3.796604E-04   1.350013E-11  -4.061391E-09  -6.784219E-11
  a    10  -1.145028E-08   3.039722E-10   8.965261E-11   3.846504E-11  -4.499915E-09   1.039300E-10   9.446384E-04  -2.456307E-11
  a    11  -6.827418E-05   3.321161E-10  -3.387563E-03   1.844675E-09   8.925989E-12   2.576749E-03   9.175368E-11  -1.564352E-09
  a    12   2.268293E-10  -6.013515E-04  -6.559493E-10  -3.180161E-03  -2.006661E-10   4.027576E-10  -1.554140E-11   2.204927E-03
  a    13   3.931342E-02   1.317360E-08   1.884058E-04  -4.639205E-11  -1.360186E-11  -2.150303E-03   8.005918E-09  -2.883569E-10
  a    14   6.896450E-09   3.650210E-02   9.467378E-11   3.349521E-04   9.132066E-09   5.932188E-11  -2.543717E-12  -1.508853E-03
  a    15  -2.555327E-10  -3.090538E-09  -1.418589E-11  -1.402799E-09  -2.753136E-02  -8.691825E-11  -2.178610E-09   8.697864E-10
  a    16   1.714534E-09   3.992863E-10  -8.185983E-10  -7.064792E-12   2.827595E-09  -1.460416E-09   1.995784E-02   4.163802E-11
  a    17   0.638538       3.803931E-08   2.387821E-03   2.586179E-10   4.337779E-10   1.134803E-04  -3.177960E-08  -3.359193E-10
  a    18   3.803931E-08   0.477034       2.064193E-10   2.763225E-04  -2.890213E-08  -3.595065E-10   3.428236E-10   3.884149E-04
  a    19   2.387821E-03   2.064193E-10   2.939056E-03   2.705652E-10   1.037977E-12   9.643853E-04  -1.732203E-10  -5.243388E-11
  a    20   2.586179E-10   2.763225E-04   2.705652E-10   2.993912E-03   3.024331E-11  -5.490783E-11   5.888116E-13   9.647977E-04
  a    21   4.337779E-10  -2.890213E-08   1.037977E-12   3.024331E-11   5.034508E-03   3.901894E-12   2.908724E-10  -8.841224E-11
  a    22   1.134803E-04  -3.595065E-10   9.643853E-04  -5.490783E-11   3.901894E-12   2.417401E-03  -1.460742E-10   8.975365E-11
  a    23  -3.177960E-08   3.428236E-10  -1.732203E-10   5.888116E-13   2.908724E-10  -1.460742E-10   4.583242E-03   7.554633E-13
  a    24  -3.359193E-10   3.884149E-04  -5.243388E-11   9.647977E-04  -8.841224E-11   8.975365E-11   7.554633E-13   2.451611E-03
  a    25  -1.174144E-02  -1.972064E-09   1.998997E-04   4.873323E-12  -6.156296E-12   3.277795E-04   5.228956E-10   1.913800E-11
  a    26  -2.351564E-09  -4.978900E-03   3.148558E-11   4.510378E-04   1.114965E-10   3.485733E-11  -6.727888E-12   4.131497E-04
  a    27  -2.424720E-10   2.395763E-09  -2.628304E-12  -7.011245E-11  -1.883874E-03  -4.127243E-12  -2.198804E-10  -3.112794E-11
  a    28   7.204406E-04   3.950971E-10   8.523152E-04   8.030436E-12  -4.873841E-12   1.192164E-03   7.448024E-11   3.630884E-12
  a    29   2.215517E-09  -1.992646E-10   3.292844E-11  -2.920934E-12  -1.665065E-10   4.746228E-11  -1.674672E-03  -2.704417E-12
  a    30  -5.363648E-10  -2.330775E-04   6.951101E-11  -8.358039E-04  -6.036958E-11  -1.559813E-11   1.836593E-12  -2.403141E-03
  a    31   9.861085E-04   1.295237E-10  -3.125325E-04   1.274789E-10   2.895752E-12  -2.041372E-03  -1.069038E-10  -1.476434E-11
  a    32  -6.677494E-10  -3.836784E-04  -1.567776E-10  -8.217611E-04  -5.723506E-11  -6.179654E-12  -6.182243E-14   3.249572E-04
  a    33   3.424583E-03   2.044741E-10  -1.683146E-03  -1.323101E-10   6.542197E-11  -1.191921E-03  -2.935253E-10   1.644886E-11
  a    34   1.979148E-10  -7.297546E-09   3.965817E-11   3.437976E-11   2.897281E-03   3.003632E-11  -9.314975E-11  -2.912341E-11
  a    35  -7.746520E-09   1.966334E-10  -2.202941E-11  -3.437097E-12   1.143610E-11  -8.979723E-11   2.782137E-03  -4.154851E-12
  a    36  -2.189421E-09   1.970603E-03  -1.614242E-10  -1.439178E-03  -2.209651E-10   8.452812E-11  -8.562353E-12  -1.556877E-03
  a    37  -1.458338E-02  -1.444493E-09  -4.478046E-04   2.039001E-10   3.892045E-12   1.970502E-04   1.452168E-09   2.924330E-10
  a    38   5.706464E-03   5.287273E-10   2.577534E-04  -4.472319E-11  -1.276385E-12   1.424553E-03  -5.144723E-10  -2.250867E-11
  a    39   1.608862E-11  -2.527191E-03  -1.179824E-10  -3.280854E-04   3.321575E-10   9.691798E-11   6.128423E-13   1.081226E-03
  a    40  -1.704912E-09  -9.875083E-03   4.982666E-12  -1.895099E-04   1.240460E-09  -1.292670E-11  -1.465373E-12  -6.773850E-04
  a    41   1.644888E-10  -2.695992E-09   1.327589E-12   5.814625E-12   1.615053E-03   1.654725E-12   2.548144E-10  -3.811154E-12
  a    42  -2.309371E-09   1.168492E-10  -1.842334E-11   6.248766E-13   1.080764E-10  -2.978751E-11   1.198976E-03  -2.931814E-13
  a    43  -2.183611E-04   5.621918E-11   3.491485E-04  -4.081288E-11   7.746967E-13  -5.797627E-04   3.384013E-12  -6.653632E-11
  a    44   1.280153E-10   1.621106E-04  -2.254497E-11   3.413830E-04  -6.682711E-12  -8.162671E-11   1.552274E-13  -5.577172E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a     5  -5.893759E-05   3.468314E-10  -1.588214E-11  -3.706693E-04  -3.053948E-11  -3.162466E-09   8.670992E-04  -1.694348E-10
  a     6   2.281102E-11  -1.091727E-04   5.671576E-11   4.589547E-10  -6.197600E-12   1.035339E-03  -8.139229E-10   3.662728E-04
  a     7  -4.639251E-04   4.474985E-10   1.053618E-11  -1.600430E-03   2.334501E-11  -1.098191E-09   7.612921E-04  -1.865950E-11
  a     8  -3.595266E-10   7.537171E-04  -7.624639E-11  -1.164650E-09   1.785682E-11  -1.244394E-03   7.048146E-10   9.240040E-05
  a     9  -2.787316E-11   5.348600E-10  -5.081156E-03  -2.255461E-11  -9.224290E-10   7.996737E-11   1.444939E-11   1.318276E-10
  a    10   1.672196E-09  -1.312479E-11  -3.199777E-09  -2.775193E-10  -5.317682E-03   1.933412E-11  -7.327018E-11  -1.346817E-11
  a    11   3.071104E-04  -4.776947E-10  -2.177891E-11   1.406684E-03  -1.390322E-10   1.735514E-09  -9.203811E-04  -8.517959E-11
  a    12  -3.452200E-11   7.760923E-04  -6.280116E-11  -6.029419E-11  -2.575144E-11  -1.567070E-03   7.497525E-10  -4.895046E-04
  a    13  -8.722493E-03  -1.778062E-09   3.618695E-11   3.145585E-03  -5.602188E-10  -4.916103E-10  -9.738573E-05  -5.925149E-10
  a    14   1.041152E-10  -6.005771E-03  -2.798156E-10   2.298703E-10   5.568248E-11  -2.922746E-03   7.997425E-11  -3.470768E-03
  a    15  -1.864221E-10   4.158166E-09   3.212391E-03   6.570305E-11   5.238411E-11   1.351893E-09   2.011196E-11   1.519976E-09
  a    16  -5.540650E-09   1.092300E-10  -7.205021E-10   2.057511E-09   2.325662E-04   1.401076E-11   2.167590E-10   5.458048E-11
  a    17  -1.174144E-02  -2.351564E-09  -2.424720E-10   7.204406E-04   2.215517E-09  -5.363648E-10   9.861085E-04  -6.677494E-10
  a    18  -1.972064E-09  -4.978900E-03   2.395763E-09   3.950971E-10  -1.992646E-10  -2.330775E-04   1.295237E-10  -3.836784E-04
  a    19   1.998997E-04   3.148558E-11  -2.628304E-12   8.523152E-04   3.292844E-11   6.951101E-11  -3.125325E-04  -1.567776E-10
  a    20   4.873323E-12   4.510378E-04  -7.011245E-11   8.030436E-12  -2.920934E-12  -8.358039E-04   1.274789E-10  -8.217611E-04
  a    21  -6.156296E-12   1.114965E-10  -1.883874E-03  -4.873841E-12  -1.665065E-10  -6.036958E-11   2.895752E-12  -5.723506E-11
  a    22   3.277795E-04   3.485733E-11  -4.127243E-12   1.192164E-03   4.746228E-11  -1.559813E-11  -2.041372E-03  -6.179654E-12
  a    23   5.228956E-10  -6.727888E-12  -2.198804E-10   7.448024E-11  -1.674672E-03   1.836593E-12  -1.069038E-10  -6.182243E-14
  a    24   1.913800E-11   4.131497E-04  -3.112794E-11   3.630884E-12  -2.704417E-12  -2.403141E-03  -1.476434E-11   3.249572E-04
  a    25   1.344296E-03   1.572322E-10   4.217372E-12   6.021111E-05  -2.765779E-11   4.472696E-11   9.303150E-05   4.425756E-11
  a    26   1.572322E-10   7.291697E-04   7.094626E-11  -3.039207E-11   4.505620E-12  -1.046369E-04  -8.718350E-12  -2.097558E-04
  a    27   4.217372E-12   7.094626E-11   1.644060E-03   9.524665E-13   3.610224E-11   1.307160E-11  -6.384472E-13   3.063517E-11
  a    28   6.021111E-05  -3.039207E-11   9.524665E-13   9.305944E-04  -1.288103E-11   3.499624E-12  -6.216160E-04  -2.852078E-11
  a    29  -2.765779E-11   4.505620E-12   3.610224E-11  -1.288103E-11   1.523794E-03   4.861280E-13  -1.829406E-12   1.099302E-12
  a    30   4.472696E-11  -1.046369E-04   1.307160E-11   3.499624E-12   4.861280E-13   2.723151E-03  -6.172351E-11  -6.162213E-04
  a    31   9.303150E-05  -8.718350E-12  -6.384472E-13  -6.216160E-04  -1.829406E-12  -6.172351E-11   3.081524E-03   3.002258E-11
  a    32   4.425756E-11  -2.097558E-04   3.063517E-11  -2.852078E-11   1.099302E-12  -6.162213E-04   3.002258E-11   9.731671E-04
  a    33  -3.006014E-04  -4.390773E-11  -2.577208E-11  -7.085940E-04  -1.227183E-11  -3.947310E-11   1.179274E-03   8.681836E-11
  a    34   7.222585E-13  -9.857759E-11  -1.206987E-03   1.318004E-11  -2.373632E-11  -2.707229E-11  -2.356334E-11  -4.546918E-11
  a    35   2.627352E-11  -5.257478E-12  -7.045408E-11   4.543515E-11  -1.167469E-03   7.444191E-12  -4.324219E-12  -1.805910E-12
  a    36   6.344708E-11  -2.044430E-04   3.022441E-11   2.321419E-11   4.612059E-12   1.817187E-03  -2.825409E-10  -3.950698E-04
  a    37   5.559636E-04   1.051052E-10  -3.565054E-13   5.913452E-05  -7.832313E-11  -3.528607E-10  -7.474594E-04   9.651512E-11
  a    38  -3.770506E-04  -2.766581E-11  -6.538054E-13   3.612040E-04   1.588453E-11   1.006454E-10  -2.192554E-03  -5.041512E-11
  a    39  -2.331484E-11  -6.848957E-05   1.981225E-12   3.848137E-12   1.909126E-13  -1.413607E-03  -1.214228E-10   7.749546E-04
  a    40   6.091342E-11   2.829525E-04  -4.550432E-11   9.476949E-12   2.116316E-12   5.625214E-04  -2.558634E-11  -2.998490E-04
  a    41  -2.506398E-12  -4.320242E-11  -1.015044E-03  -9.868114E-13  -4.775629E-11   5.143139E-12   5.726766E-13  -2.978210E-12
  a    42   6.330125E-11  -2.840450E-12   6.091695E-13   1.484170E-12  -8.137187E-04   4.306964E-13  -4.450533E-12  -8.806542E-13
  a    43   3.645378E-05  -1.300771E-11   3.800210E-14  -1.733071E-04  -4.235344E-12   6.694827E-11   5.414657E-04  -1.246441E-11
  a    44  -1.334136E-11  -1.801161E-05   4.687962E-13  -3.971129E-11   2.287456E-13   5.292314E-04   7.324682E-11  -2.300663E-04

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a     5   1.637096E-03  -3.118309E-11  -8.020755E-12  -2.965581E-09  -2.764337E-04  -1.497218E-03   1.240340E-09  -7.880646E-10
  a     6   1.034894E-09   8.526605E-12   5.862320E-12   1.764234E-03  -6.518193E-10  -1.202511E-09  -9.690921E-04   4.816929E-04
  a     7  -1.268277E-03   2.751698E-11   5.258670E-11   4.040012E-10  -7.432435E-04  -1.088573E-03   1.763441E-10  -1.287646E-11
  a     8  -6.352709E-10   5.768068E-11   1.534511E-12   1.021644E-03  -2.828424E-10  -1.755530E-10   1.465621E-03  -4.347298E-05
  a     9  -2.488202E-12  -1.415260E-04  -9.548080E-10  -2.676467E-10   5.254699E-12  -5.577531E-12   3.629950E-10   1.495673E-09
  a    10  -4.215792E-10  -9.476760E-10   3.430861E-04  -4.745831E-12   1.746429E-09  -8.289600E-10  -5.142919E-12   7.767870E-12
  a    11  -9.069728E-04   1.888205E-11   1.163698E-11   1.946737E-09   1.149885E-03   3.450782E-03  -2.494466E-09   9.839140E-10
  a    12   2.604760E-10  -3.737067E-11  -2.589442E-12  -1.604309E-03   8.202366E-11  -5.277229E-10   3.235233E-03  -1.126587E-03
  a    13  -2.576102E-03  -3.313840E-11   2.697374E-09   2.236714E-09   1.065320E-02  -4.626515E-03  -2.625175E-10  -2.683531E-10
  a    14   1.612271E-10   2.997060E-09  -5.335909E-11  -6.969935E-04  -3.944974E-10   2.472221E-10   3.467428E-03   1.161101E-02
  a    15  -2.317694E-10  -9.627082E-03   4.898279E-10   1.935983E-10   7.208009E-11  -1.149695E-11  -8.618930E-10  -2.519486E-09
  a    16  -1.676773E-11  -1.164594E-09   6.639913E-03  -3.730068E-11   2.291901E-09  -9.968410E-10  -3.431473E-11  -1.072704E-10
  a    17   3.424583E-03   1.979148E-10  -7.746520E-09  -2.189421E-09  -1.458338E-02   5.706464E-03   1.608862E-11  -1.704912E-09
  a    18   2.044741E-10  -7.297546E-09   1.966334E-10   1.970603E-03  -1.444493E-09   5.287273E-10  -2.527191E-03  -9.875083E-03
  a    19  -1.683146E-03   3.965817E-11  -2.202941E-11  -1.614242E-10  -4.478046E-04   2.577534E-04  -1.179824E-10   4.982666E-12
  a    20  -1.323101E-10   3.437976E-11  -3.437097E-12  -1.439178E-03   2.039001E-10  -4.472319E-11  -3.280854E-04  -1.895099E-04
  a    21   6.542197E-11   2.897281E-03   1.143610E-11  -2.209651E-10   3.892045E-12  -1.276385E-12   3.321575E-10   1.240460E-09
  a    22  -1.191921E-03   3.003632E-11  -8.979723E-11   8.452812E-11   1.970502E-04   1.424553E-03   9.691798E-11  -1.292670E-11
  a    23  -2.935253E-10  -9.314975E-11   2.782137E-03  -8.562353E-12   1.452168E-09  -5.144723E-10   6.128423E-13  -1.465373E-12
  a    24   1.644886E-11  -2.912341E-11  -4.154851E-12  -1.556877E-03   2.924330E-10  -2.250867E-11   1.081226E-03  -6.773850E-04
  a    25  -3.006014E-04   7.222585E-13   2.627352E-11   6.344708E-11   5.559636E-04  -3.770506E-04  -2.331484E-11   6.091342E-11
  a    26  -4.390773E-11  -9.857759E-11  -5.257478E-12  -2.044430E-04   1.051052E-10  -2.766581E-11  -6.848957E-05   2.829525E-04
  a    27  -2.577208E-11  -1.206987E-03  -7.045408E-11   3.022441E-11  -3.565054E-13  -6.538054E-13   1.981225E-12  -4.550432E-11
  a    28  -7.085940E-04   1.318004E-11   4.543515E-11   2.321419E-11   5.913452E-05   3.612040E-04   3.848137E-12   9.476949E-12
  a    29  -1.227183E-11  -2.373632E-11  -1.167469E-03   4.612059E-12  -7.832313E-11   1.588453E-11   1.909126E-13   2.116316E-12
  a    30  -3.947310E-11  -2.707229E-11   7.444191E-12   1.817187E-03  -3.528607E-10   1.006454E-10  -1.413607E-03   5.625214E-04
  a    31   1.179274E-03  -2.356334E-11  -4.324219E-12  -2.825409E-10  -7.474594E-04  -2.192554E-03  -1.214228E-10  -2.558634E-11
  a    32   8.681836E-11  -4.546918E-11  -1.805910E-12  -3.950698E-04   9.651512E-11  -5.041512E-11   7.749546E-04  -2.998490E-04
  a    33   1.535646E-03   7.617733E-12  -3.843245E-11  -8.210299E-11  -6.024185E-04  -8.935494E-04  -6.441126E-12  -7.464680E-11
  a    34   7.617733E-12   1.759836E-03  -1.533152E-10  -4.119754E-11   5.365780E-12   2.200947E-11   6.508736E-11   2.635262E-10
  a    35  -3.843245E-11  -1.533152E-10   1.756027E-03   2.148184E-12   2.886277E-10  -1.004642E-10  -5.461833E-12  -2.746773E-12
  a    36  -8.210299E-11  -4.119754E-11   2.148184E-12   2.088951E-03   7.599313E-12   1.472271E-10  -1.319831E-03   3.455646E-04
  a    37  -6.024185E-04   5.365780E-12   2.886277E-10   7.599313E-12   2.276169E-03  -4.257018E-05   3.698190E-10   2.332909E-10
  a    38  -8.935494E-04   2.200947E-11  -1.004642E-10   1.472271E-10  -4.257018E-05   2.699944E-03   9.879037E-11  -1.298896E-10
  a    39  -6.441126E-12   6.508736E-11  -5.461833E-12  -1.319831E-03   3.698190E-10   9.879037E-11   1.705607E-03  -1.323871E-04
  a    40  -7.464680E-11   2.635262E-10  -2.746773E-12   3.455646E-04   2.332909E-10  -1.298896E-10  -1.323871E-04   1.901681E-03
  a    41   1.957071E-11   9.443600E-04   1.303872E-10  -1.566172E-11   5.407871E-12  -1.705457E-12   1.886572E-11   8.306013E-11
  a    42  -4.943962E-12   4.901575E-11   7.862926E-04  -2.927317E-12   9.474946E-11  -2.792451E-11   1.146690E-12   4.193827E-12
  a    43  -9.487232E-05   2.035855E-12   8.247587E-12   1.472177E-11  -4.056668E-05  -1.259701E-04  -1.472583E-11   2.516950E-12
  a    44   2.003466E-11   1.930349E-12   1.490466E-14  -1.296295E-05  -1.849445E-12  -4.840518E-12  -1.708762E-04   4.237823E-05

               a    41        a    42        a    43        a    44
  a     5  -2.642617E-11   2.823236E-11   1.751885E-03  -2.701085E-09
  a     6  -2.775456E-11   1.437451E-11   1.270464E-09   1.802719E-03
  a     7   9.678677E-12   7.207761E-11   8.259061E-04  -1.546554E-11
  a     8   4.092255E-11   5.032589E-11   2.197895E-10  -7.805138E-04
  a     9   8.244867E-04  -9.075902E-10   3.221113E-11  -8.607952E-13
  a    10  -3.940912E-10   9.320467E-04   5.907990E-11  -7.508451E-12
  a    11  -1.661812E-11   1.939033E-11  -1.876973E-03   4.805664E-10
  a    12  -6.648689E-11   7.833691E-12  -8.523027E-10  -1.780140E-03
  a    13  -6.030360E-12   2.994826E-09   1.104109E-04   3.020442E-11
  a    14   3.508513E-09  -1.019321E-11   6.509240E-11   2.280514E-04
  a    15   2.980353E-03  -1.815623E-09  -2.431135E-11  -1.687787E-10
  a    16   2.147654E-09  -6.486234E-03   2.765176E-11  -8.501667E-12
  a    17   1.644888E-10  -2.309371E-09  -2.183611E-04   1.280153E-10
  a    18  -2.695992E-09   1.168492E-10   5.621918E-11   1.621106E-04
  a    19   1.327589E-12  -1.842334E-11   3.491485E-04  -2.254497E-11
  a    20   5.814625E-12   6.248766E-13  -4.081288E-11   3.413830E-04
  a    21   1.615053E-03   1.080764E-10   7.746967E-13  -6.682711E-12
  a    22   1.654725E-12  -2.978751E-11  -5.797627E-04  -8.162671E-11
  a    23   2.548144E-10   1.198976E-03   3.384013E-12   1.552274E-13
  a    24  -3.811154E-12  -2.931814E-13  -6.653632E-11  -5.577172E-04
  a    25  -2.506398E-12   6.330125E-11   3.645378E-05  -1.334136E-11
  a    26  -4.320242E-11  -2.840450E-12  -1.300771E-11  -1.801161E-05
  a    27  -1.015044E-03   6.091695E-13   3.800210E-14   4.687962E-13
  a    28  -9.868114E-13   1.484170E-12  -1.733071E-04  -3.971129E-11
  a    29  -4.775629E-11  -8.137187E-04  -4.235344E-12   2.287456E-13
  a    30   5.143139E-12   4.306964E-13   6.694827E-11   5.292314E-04
  a    31   5.726766E-13  -4.450533E-12   5.414657E-04   7.324682E-11
  a    32  -2.978210E-12  -8.806542E-13  -1.246441E-11  -2.300663E-04
  a    33   1.957071E-11  -4.943962E-12  -9.487232E-05   2.003466E-11
  a    34   9.443600E-04   4.901575E-11   2.035855E-12   1.930349E-12
  a    35   1.303872E-10   7.862926E-04   8.247587E-12   1.490466E-14
  a    36  -1.566172E-11  -2.927317E-12   1.472177E-11  -1.296295E-05
  a    37   5.407871E-12   9.474946E-11  -4.056668E-05  -1.849445E-12
  a    38  -1.705457E-12  -2.792451E-11  -1.259701E-04  -4.840518E-12
  a    39   1.886572E-11   1.146690E-12  -1.472583E-11  -1.708762E-04
  a    40   8.306013E-11   4.193827E-12   2.516950E-12   4.237823E-05
  a    41   1.025747E-03  -3.109229E-11  -2.822885E-15  -1.372064E-12
  a    42  -3.109229E-11   7.650838E-04   4.908712E-13   1.161085E-13
  a    43  -2.822885E-15   4.908712E-13   4.941850E-04   1.046724E-12
  a    44  -1.372064E-12   1.161085E-13   1.046724E-12   4.612044E-04

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99859306     1.99857132     1.99478785     1.99468981
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99293910     1.99291943     1.99199208     1.99131506     1.94341369     1.93865826     1.57494167     1.42310722
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.63799450     0.47640824     0.00807003     0.00802628     0.00777181     0.00723910     0.00373243     0.00366991
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00213976     0.00157493     0.00147879     0.00130059     0.00084719     0.00084147     0.00080361     0.00052576
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00047135     0.00033812     0.00025365     0.00021326     0.00013985     0.00012329     0.00003165     0.00002511
              MO    41       MO    42       MO    43       MO    44
  occ(*)=     0.00002421     0.00001665     0.00000553     0.00000437


 total number of electrons =   32.0000000000

 item #                     3 suffix=:.drt1.state3:
 read_civout: repnuc=   100.585551835256     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -227.77490373  -328.36045556
 residuum:     0.00005753
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -227.62386331  -328.20941515
 residuum:     0.00007617
 deltae:     0.00000000
================================================================================
  Reading record                      3  of civout
 INFO:ref#  3vector#  3 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:  -227.62317821  -328.20873004
 residuum:     0.00005303
 deltae:     0.00000000
 apxde:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047     0.00000002     0.00000102     0.00015222     0.03774196    -0.02641190    -0.07045310     0.00115783
 ref:   2    -0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636     0.00000525     0.00001728    -0.00000098
 ref:   3    -0.00000000    -0.00000044     0.96016413    -0.00000911     0.00000089     0.15394543    -0.00723570    -0.05650070

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037047     0.00000002     0.00000102     0.00015222     0.03774196    -0.02641190     0.00000000     0.00000000
 ref:   2    -0.00000001     0.95971512     0.00000044    -0.16583311     0.00047636     0.00000525     0.00000000     0.00000000
 ref:   3    -0.00000000    -0.00000044     0.96016413    -0.00000911     0.00000089     0.15394543     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    32.000000
   32  correlated and     0  frozen core electrons
   1936 mo coefficients read in LUMORB format.
  ... reading mocoef_lumorb (MOLCAS format)

          modens reordered block   1

               a     1        a     2        a     3        a     4        a     5        a     6        a     7        a     8
  a     1    2.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     2    0.00000        2.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     3    0.00000        0.00000        2.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a     4    0.00000        0.00000        0.00000        2.00000        0.00000        0.00000        0.00000        0.00000    
  a     5    0.00000        0.00000        0.00000        0.00000        1.99679       3.483843E-11   1.069218E-03   1.248969E-09
  a     6    0.00000        0.00000        0.00000        0.00000       3.483843E-11    1.99678       1.292789E-09  -1.087497E-03
  a     7    0.00000        0.00000        0.00000        0.00000       1.069218E-03   1.292789E-09    1.99516       1.453437E-10
  a     8    0.00000        0.00000        0.00000        0.00000       1.248969E-09  -1.087497E-03   1.453437E-10    1.99512    
  a     9    0.00000        0.00000        0.00000        0.00000      -3.459812E-10  -3.309542E-11   3.317147E-09   9.170662E-12
  a    10    0.00000        0.00000        0.00000        0.00000      -5.938791E-11   2.228630E-10  -1.720558E-11   5.802728E-09
  a    11    0.00000        0.00000        0.00000        0.00000       2.305149E-03   1.695238E-09   5.739188E-04   1.484060E-10
  a    12    0.00000        0.00000        0.00000        0.00000      -1.798534E-09   2.291105E-03   3.348347E-10  -5.790006E-04
  a    13    0.00000        0.00000        0.00000        0.00000       5.133986E-05   5.786831E-11  -2.046801E-05  -1.351283E-11
  a    14    0.00000        0.00000        0.00000        0.00000      -2.919045E-13  -4.337374E-06   7.973321E-12  -5.534116E-05
  a    15    0.00000        0.00000        0.00000        0.00000      -8.115616E-09  -2.814335E-12   2.488822E-08  -2.037484E-11
  a    16    0.00000        0.00000        0.00000        0.00000       6.989345E-13   7.138052E-10   1.391963E-11  -4.292069E-08
  a    17    0.00000        0.00000        0.00000        0.00000       2.239032E-04   2.468512E-10   1.660454E-05  -1.665544E-10
  a    18    0.00000        0.00000        0.00000        0.00000       3.218002E-10  -2.591426E-04   4.851585E-11  -1.807772E-03
  a    19    0.00000        0.00000        0.00000        0.00000      -2.498313E-03  -3.129957E-09   5.342876E-03  -7.399961E-10
  a    20    0.00000        0.00000        0.00000        0.00000       2.516620E-09  -2.411909E-03   9.847148E-10  -5.263741E-03
  a    21    0.00000        0.00000        0.00000        0.00000      -5.437564E-10   1.102369E-11  -1.606314E-09   2.072947E-11
  a    22    0.00000        0.00000        0.00000        0.00000      -4.255963E-04  -1.036231E-09  -3.162289E-03   2.446424E-10
  a    23    0.00000        0.00000        0.00000        0.00000      -7.366899E-12  -1.229421E-09  -7.767817E-11  -1.683715E-09
  a    24    0.00000        0.00000        0.00000        0.00000       5.968416E-11  -5.367011E-04  -3.875576E-10   2.915845E-03
  a    25    0.00000        0.00000        0.00000        0.00000      -6.532453E-05  -8.069922E-11  -4.663012E-04   1.899444E-10
  a    26    0.00000        0.00000        0.00000        0.00000       4.465702E-11  -1.001741E-04  -2.280378E-10   7.371696E-04
  a    27    0.00000        0.00000        0.00000        0.00000       1.185062E-09   8.652550E-12  -7.270269E-09  -4.249994E-11
  a    28    0.00000        0.00000        0.00000        0.00000      -3.769656E-04  -6.547622E-10  -1.634049E-03   3.065396E-10
  a    29    0.00000        0.00000        0.00000        0.00000      -1.136828E-11  -1.784833E-09   4.046531E-11  -2.642069E-09
  a    30    0.00000        0.00000        0.00000        0.00000      -6.736907E-10   1.045644E-03   1.957034E-10  -1.288355E-03
  a    31    0.00000        0.00000        0.00000        0.00000       8.617309E-04   1.483318E-09   7.255677E-04   9.195200E-11
  a    32    0.00000        0.00000        0.00000        0.00000      -4.276080E-10   3.245958E-04   8.197502E-11   1.276582E-04
  a    33    0.00000        0.00000        0.00000        0.00000       1.647726E-03   2.140087E-09  -1.290056E-03   4.473789E-10
  a    34    0.00000        0.00000        0.00000        0.00000      -4.093354E-10   8.541904E-12  -1.808501E-09  -5.659624E-12
  a    35    0.00000        0.00000        0.00000        0.00000      -1.571583E-11   1.914614E-11   3.467912E-11   4.358896E-10
  a    36    0.00000        0.00000        0.00000        0.00000      -1.861960E-09   1.789615E-03  -7.026577E-10   1.047050E-03
  a    37    0.00000        0.00000        0.00000        0.00000      -2.617366E-04  -6.790801E-10  -7.454089E-04  -5.421900E-11
  a    38    0.00000        0.00000        0.00000        0.00000      -1.490579E-03  -1.885579E-09  -1.093588E-03  -8.922277E-11
  a    39    0.00000        0.00000        0.00000        0.00000       9.148618E-10  -9.775955E-04  -3.774931E-10   1.521684E-03
  a    40    0.00000        0.00000        0.00000        0.00000      -4.977189E-10   5.130159E-04   2.414191E-11  -1.188784E-04
  a    41    0.00000        0.00000        0.00000        0.00000      -2.747643E-10   1.379445E-11  -5.572852E-09  -1.462691E-12
  a    42    0.00000        0.00000        0.00000        0.00000      -5.449495E-12   5.965034E-10   3.900773E-12   1.976949E-09
  a    43    0.00000        0.00000        0.00000        0.00000       1.765410E-03   2.275104E-09   8.626999E-04  -9.114996E-11
  a    44    0.00000        0.00000        0.00000        0.00000      -1.720578E-09   1.772586E-03   1.240939E-10  -7.292652E-04

               a     9        a    10        a    11        a    12        a    13        a    14        a    15        a    16
  a     5  -3.459812E-10  -5.938791E-11   2.305149E-03  -1.798534E-09   5.133986E-05  -2.919045E-13  -8.115616E-09   6.989345E-13
  a     6  -3.309542E-11   2.228630E-10   1.695238E-09   2.291105E-03   5.786831E-11  -4.337374E-06  -2.814335E-12   7.138052E-10
  a     7   3.317147E-09  -1.720558E-11   5.739188E-04   3.348347E-10  -2.046801E-05   7.973321E-12   2.488822E-08   1.391963E-11
  a     8   9.170662E-12   5.802728E-09   1.484060E-10  -5.790006E-04  -1.351283E-11  -5.534116E-05  -2.037484E-11  -4.292069E-08
  a     9    1.98731       7.367621E-10  -1.136363E-09  -1.001134E-10   2.810670E-07   7.454417E-10  -4.838755E-02  -1.184229E-08
  a    10   7.367621E-10    1.98944      -1.236018E-10   2.539758E-09   5.375989E-10   3.332202E-07  -6.073364E-09   3.224739E-02
  a    11  -1.136363E-09  -1.236018E-10    1.99430       1.045002E-10   3.776015E-05  -1.862787E-11  -9.291322E-09   8.195430E-10
  a    12  -1.001134E-10   2.539758E-09   1.045002E-10    1.99426       1.751887E-11   2.602440E-05  -7.010532E-10  -6.007479E-09
  a    13   2.810670E-07   5.375989E-10   3.776015E-05   1.751887E-11    1.94199      -1.815240E-09  -4.025786E-06  -4.670120E-09
  a    14   7.454417E-10   3.332202E-07  -1.862787E-11   2.602440E-05  -1.815240E-09    1.93762       6.333671E-09   3.798915E-06
  a    15  -4.838755E-02  -6.073364E-09  -9.291322E-09  -7.010532E-10  -4.025786E-06   6.333671E-09    1.43868      -4.113087E-08
  a    16  -1.184229E-08   3.224739E-02   8.195430E-10  -6.007479E-09  -4.670120E-09   3.798915E-06  -4.113087E-08    1.56503    
  a    17  -1.156728E-08  -5.769831E-10   2.971323E-05  -4.258802E-11   3.913136E-02  -1.611611E-08   3.661474E-06   8.434173E-09
  a    18  -3.268319E-10   4.205999E-09   2.943389E-10  -7.016646E-04  -1.012420E-08   3.728980E-02  -6.728582E-09  -3.216940E-06
  a    19   1.780647E-08   5.429305E-11  -3.399196E-03  -1.572549E-09   1.507902E-04   5.799283E-12   1.618916E-08   9.124226E-11
  a    20   8.320706E-11  -2.041581E-08   1.060315E-09  -3.267591E-03   1.075256E-10   3.609200E-04  -7.060319E-11   2.273219E-09
  a    21   7.197556E-04   8.624785E-11   9.232267E-09   2.242824E-11  -8.354681E-07  -2.853610E-10  -2.019813E-02   4.123782E-10
  a    22  -2.928896E-09  -5.678175E-11   2.579543E-03   1.583060E-09  -2.168425E-03   5.377270E-11  -6.854019E-09  -6.345580E-10
  a    23   2.930367E-11  -2.653081E-04   5.032534E-11  -1.129038E-08  -2.106977E-10  -8.765684E-07  -1.029365E-09   2.635896E-02
  a    24  -5.848590E-11   1.683767E-08  -2.078560E-10   2.221957E-03  -2.155103E-11  -1.529251E-03  -5.172670E-11   6.913279E-10
  a    25   1.284388E-08   3.885497E-11   2.865318E-04   1.215591E-10  -8.704583E-03   1.905814E-10  -2.030773E-08  -7.953226E-11
  a    26  -4.050614E-10  -6.393655E-09  -3.034630E-10   7.890370E-04  -6.240539E-10  -5.961750E-03   1.169678E-10  -1.144350E-09
  a    27  -5.497456E-03  -1.412278E-10   1.751701E-09  -1.362125E-10   6.561479E-07   6.479898E-10   1.120985E-03  -3.901016E-10
  a    28  -2.350279E-09  -5.488047E-12   1.440986E-03   7.777272E-10   3.086969E-03  -1.896131E-10  -1.637535E-08   2.429855E-11
  a    29   2.118150E-09  -4.954609E-03  -1.220202E-10  -1.032728E-10   2.115009E-10   6.503688E-07   4.668019E-10  -2.310489E-03
  a    30   1.895529E-11  -3.383648E-09  -2.093089E-10  -1.614600E-03  -2.594248E-10  -2.914665E-03  -2.957237E-11   8.713293E-09
  a    31   4.248008E-10   1.787024E-11  -9.581066E-04  -1.304860E-09  -1.049905E-04   1.809966E-11   3.137321E-09   4.516727E-12
  a    32   9.310913E-12  -5.609173E-09   6.125856E-10  -4.735260E-04  -3.501128E-10  -3.451497E-03  -1.145165E-11   3.081421E-09
  a    33   2.293501E-09  -1.248340E-11  -8.824751E-04  -3.993172E-10  -2.587599E-03   3.105543E-10  -1.932103E-08   1.689146E-10
  a    34   1.158706E-04   1.936302E-10   2.020050E-09  -7.973182E-13  -5.378108E-07  -3.163255E-10  -7.507981E-03  -1.884570E-10
  a    35   1.469824E-10  -5.642121E-05  -2.012538E-11  -4.119616E-09  -6.442737E-10  -5.697459E-07  -2.960872E-10   8.856673E-03
  a    36  -1.463600E-11   1.109325E-08   5.301985E-10  -1.562281E-03   2.076205E-09  -7.065992E-04  -9.095708E-12   8.017881E-09
  a    37  -1.864561E-08   1.239573E-10   1.159347E-03   1.070826E-09   1.068952E-02  -1.060923E-09   6.271143E-08  -2.394668E-11
  a    38   5.953619E-09  -1.495447E-10   3.443061E-03   1.954953E-09  -4.637194E-03   3.431977E-10  -3.228079E-08   8.950486E-11
  a    39  -1.549002E-11  -2.389532E-10  -3.273828E-10   3.228139E-03  -1.639414E-10   3.461445E-03   6.752683E-11  -1.513078E-08
  a    40   1.377471E-10  -2.301075E-08   1.797128E-10  -1.115029E-03   2.228073E-10   1.156611E-02   2.408977E-10  -4.723347E-08
  a    41   1.241687E-03  -9.492601E-11  -1.107876E-09   1.533120E-11  -3.687632E-07  -9.600442E-11   6.313409E-03   7.026169E-10
  a    42  -4.756323E-10   5.764923E-04  -4.726750E-12   3.904289E-09  -1.458300E-10  -3.314831E-07  -8.440074E-10  -3.794512E-03
  a    43   1.507323E-09   4.633833E-11  -1.875629E-03  -7.447778E-10   1.375615E-04  -1.053275E-11  -7.098327E-09  -4.163395E-12
  a    44   1.388026E-11   4.345049E-09   6.030514E-10  -1.787993E-03   3.039584E-11   2.117322E-04   3.206474E-12  -6.625900E-09

               a    17        a    18        a    19        a    20        a    21        a    22        a    23        a    24
  a     5   2.239032E-04   3.218002E-10  -2.498313E-03   2.516620E-09  -5.437564E-10  -4.255963E-04  -7.366899E-12   5.968416E-11
  a     6   2.468512E-10  -2.591426E-04  -3.129957E-09  -2.411909E-03   1.102369E-11  -1.036231E-09  -1.229421E-09  -5.367011E-04
  a     7   1.660454E-05   4.851585E-11   5.342876E-03   9.847148E-10  -1.606314E-09  -3.162289E-03  -7.767817E-11  -3.875576E-10
  a     8  -1.665544E-10  -1.807772E-03  -7.399961E-10  -5.263741E-03   2.072947E-11   2.446424E-10  -1.683715E-09   2.915845E-03
  a     9  -1.156728E-08  -3.268319E-10   1.780647E-08   8.320706E-11   7.197556E-04  -2.928896E-09   2.930367E-11  -5.848590E-11
  a    10  -5.769831E-10   4.205999E-09   5.429305E-11  -2.041581E-08   8.624785E-11  -5.678175E-11  -2.653081E-04   1.683767E-08
  a    11   2.971323E-05   2.943389E-10  -3.399196E-03   1.060315E-09   9.232267E-09   2.579543E-03   5.032534E-11  -2.078560E-10
  a    12  -4.258802E-11  -7.016646E-04  -1.572549E-09  -3.267591E-03   2.242824E-11   1.583060E-09  -1.129038E-08   2.221957E-03
  a    13   3.913136E-02  -1.012420E-08   1.507902E-04   1.075256E-10  -8.354681E-07  -2.168425E-03  -2.106977E-10  -2.155103E-11
  a    14  -1.611611E-08   3.728980E-02   5.799283E-12   3.609200E-04  -2.853610E-10   5.377270E-11  -8.765684E-07  -1.529251E-03
  a    15   3.661474E-06  -6.728582E-09   1.618916E-08  -7.060319E-11  -2.019813E-02  -6.854019E-09  -1.029365E-09  -5.172670E-11
  a    16   8.434173E-09  -3.216940E-06   9.124226E-11   2.273219E-09   4.123782E-10  -6.345580E-10   2.635896E-02   6.913279E-10
  a    17   0.626895      -3.841427E-08   9.128997E-04  -1.335184E-10   1.274666E-06  -1.074413E-04   1.451720E-09  -2.075390E-11
  a    18  -3.841427E-08   0.488982      -1.649327E-10   1.386409E-03   1.245575E-09   5.265026E-11   1.094303E-06   6.435571E-04
  a    19   9.128997E-04  -1.649327E-10   2.926442E-03   1.067125E-10   1.097716E-09   9.640545E-04   2.766924E-11   3.742314E-11
  a    20  -1.335184E-10   1.386409E-03   1.067125E-10   3.005765E-03   6.021770E-12   2.893207E-11   2.955587E-09   9.712949E-04
  a    21   1.274666E-06   1.245575E-09   1.097716E-09   6.021770E-12   4.633967E-03   3.563052E-09  -6.275871E-11   2.094213E-12
  a    22  -1.074413E-04   5.265026E-11   9.640545E-04   2.893207E-11   3.563052E-09   2.418879E-03  -6.287507E-11  -2.733783E-11
  a    23   1.451720E-09   1.094303E-06   2.766924E-11   2.955587E-09  -6.275871E-11  -6.287507E-11   4.885680E-03   7.990224E-09
  a    24  -2.075390E-11   6.435571E-04   3.742314E-11   9.712949E-04   2.094213E-12  -2.733783E-11   7.990224E-09   2.455080E-03
  a    25  -1.087892E-02   1.252759E-09   2.512936E-04   3.077169E-11  -2.276774E-09   3.285603E-04  -1.355250E-11   1.596834E-12
  a    26   9.209471E-10  -5.204171E-03   5.205489E-11   4.331958E-04  -1.463240E-10   1.303803E-11   3.835231E-09   4.123742E-04
  a    27  -6.324144E-07  -6.327767E-11   1.835572E-10  -3.999671E-11  -1.764404E-03  -5.359963E-09  -1.007535E-11  -3.571923E-11
  a    28   6.337763E-04  -1.179848E-10   8.555930E-04   4.800908E-11  -8.402397E-09   1.194051E-03   3.770983E-11   3.749204E-12
  a    29  -4.671316E-10  -5.516957E-07   1.453459E-12  -1.333317E-09   3.348910E-12   4.934671E-11  -1.805628E-03  -7.385379E-09
  a    30   1.116716E-10  -1.912603E-04  -2.996547E-11  -8.390001E-04   5.164044E-12   2.901616E-11   6.605371E-09  -2.406383E-03
  a    31   7.328262E-04  -8.528259E-11  -3.181456E-04   1.621926E-11   1.402180E-09  -2.043507E-03  -4.795900E-11   3.851507E-11
  a    32   1.808915E-10  -6.571175E-04  -7.009158E-11  -8.251683E-04  -5.343430E-12  -3.338569E-11   6.528627E-09   3.228662E-04
  a    33   3.398120E-03  -2.773136E-10  -1.699890E-03  -4.481385E-11   1.358519E-08  -1.193110E-03   4.489050E-12  -1.564736E-12
  a    34   5.546661E-07   6.110931E-10   5.511319E-10   1.656857E-12   2.764653E-03   2.782628E-09  -9.109982E-11  -2.090737E-12
  a    35   1.165476E-09   5.118458E-07   3.400288E-11   1.066540E-09  -2.202759E-11  -5.895493E-11   2.895832E-03   5.261062E-09
  a    36  -2.901187E-09   2.026666E-03  -1.085714E-10  -1.428996E-03  -2.917234E-12   8.205468E-11   6.641064E-09  -1.557648E-03
  a    37  -1.371983E-02   7.131116E-10  -3.743829E-04   2.624077E-10  -5.753993E-08   2.005846E-04   8.657666E-11   2.979990E-10
  a    38   5.578894E-03  -3.447611E-10   2.287221E-04   3.310451E-11   2.332179E-08   1.426346E-03   2.965946E-11  -6.258463E-11
  a    39   7.283120E-10  -2.793106E-03  -1.084822E-11  -3.443971E-04  -2.454366E-12   6.148731E-11  -1.476607E-08   1.080046E-03
  a    40   1.611252E-09  -1.030359E-02   5.287531E-12  -2.468461E-04  -2.343739E-11   1.187335E-11  -5.281272E-08  -6.824506E-04
  a    41   4.828026E-07   1.923122E-10   2.757459E-11   2.271809E-12   1.459106E-03   4.500507E-09   7.889413E-11  -2.679585E-12
  a    42   2.537204E-10   3.714927E-07   9.135732E-13   7.421157E-10  -9.005168E-12  -3.254583E-11   1.307137E-03   5.164796E-09
  a    43  -1.734467E-05   7.071665E-12   3.531806E-04   3.198308E-12  -5.363822E-10  -5.784462E-04  -1.470692E-11   2.309268E-11
  a    44  -2.425616E-11   5.066400E-05   2.188232E-11   3.428266E-04  -5.024697E-13   7.060544E-12  -7.649580E-10  -5.571365E-04

               a    25        a    26        a    27        a    28        a    29        a    30        a    31        a    32
  a     5  -6.532453E-05   4.465702E-11   1.185062E-09  -3.769656E-04  -1.136828E-11  -6.736907E-10   8.617309E-04  -4.276080E-10
  a     6  -8.069922E-11  -1.001741E-04   8.652550E-12  -6.547622E-10  -1.784833E-09   1.045644E-03   1.483318E-09   3.245958E-04
  a     7  -4.663012E-04  -2.280378E-10  -7.270269E-09  -1.634049E-03   4.046531E-11   1.957034E-10   7.255677E-04   8.197502E-11
  a     8   1.899444E-10   7.371696E-04  -4.249994E-11   3.065396E-10  -2.642069E-09  -1.288355E-03   9.195200E-11   1.276582E-04
  a     9   1.284388E-08  -4.050614E-10  -5.497456E-03  -2.350279E-09   2.118150E-09   1.895529E-11   4.248008E-10   9.310913E-12
  a    10   3.885497E-11  -6.393655E-09  -1.412278E-10  -5.488047E-12  -4.954609E-03  -3.383648E-09   1.787024E-11  -5.609173E-09
  a    11   2.865318E-04  -3.034630E-10   1.751701E-09   1.440986E-03  -1.220202E-10  -2.093089E-10  -9.581066E-04   6.125856E-10
  a    12   1.215591E-10   7.890370E-04  -1.362125E-10   7.777272E-10  -1.032728E-10  -1.614600E-03  -1.304860E-09  -4.735260E-04
  a    13  -8.704583E-03  -6.240539E-10   6.561479E-07   3.086969E-03   2.115009E-10  -2.594248E-10  -1.049905E-04  -3.501128E-10
  a    14   1.905814E-10  -5.961750E-03   6.479898E-10  -1.896131E-10   6.503688E-07  -2.914665E-03   1.809966E-11  -3.451497E-03
  a    15  -2.030773E-08   1.169678E-10   1.120985E-03  -1.637535E-08   4.668019E-10  -2.957237E-11   3.137321E-09  -1.145165E-11
  a    16  -7.953226E-11  -1.144350E-09  -3.901016E-10   2.429855E-11  -2.310489E-03   8.713293E-09   4.516727E-12   3.081421E-09
  a    17  -1.087892E-02   9.209471E-10  -6.324144E-07   6.337763E-04  -4.671316E-10   1.116716E-10   7.328262E-04   1.808915E-10
  a    18   1.252759E-09  -5.204171E-03  -6.327767E-11  -1.179848E-10  -5.516957E-07  -1.912603E-04  -8.528259E-11  -6.571175E-04
  a    19   2.512936E-04   5.205489E-11   1.835572E-10   8.555930E-04   1.453459E-12  -2.996547E-11  -3.181456E-04  -7.009158E-11
  a    20   3.077169E-11   4.331958E-04  -3.999671E-11   4.800908E-11  -1.333317E-09  -8.390001E-04   1.621926E-11  -8.251683E-04
  a    21  -2.276774E-09  -1.463240E-10  -1.764404E-03  -8.402397E-09   3.348910E-12   5.164044E-12   1.402180E-09  -5.343430E-12
  a    22   3.285603E-04   1.303803E-11  -5.359963E-09   1.194051E-03   4.934671E-11   2.901616E-11  -2.043507E-03  -3.338569E-11
  a    23  -1.355250E-11   3.835231E-09  -1.007535E-11   3.770983E-11  -1.805628E-03   6.605371E-09  -4.795900E-11   6.528627E-09
  a    24   1.596834E-12   4.123742E-04  -3.571923E-11   3.749204E-12  -7.385379E-09  -2.406383E-03   3.851507E-11   3.228662E-04
  a    25   1.294427E-03  -3.808484E-11  -1.345226E-08   6.221522E-05   5.217646E-12  -2.289255E-11   1.015755E-04  -2.490533E-11
  a    26  -3.808484E-11   7.330042E-04   7.141255E-11   2.828082E-11  -1.101414E-08  -1.055733E-04  -1.688304E-12  -2.067183E-04
  a    27  -1.345226E-08   7.141255E-11   1.615974E-03   9.434285E-09   2.300566E-11   8.983303E-12  -3.996978E-10   1.927703E-11
  a    28   6.221522E-05   2.828082E-11   9.434285E-09   9.283095E-04  -3.349776E-12   2.537404E-12  -6.220325E-04  -2.438383E-11
  a    29   5.217646E-12  -1.101414E-08   2.300566E-11  -3.349776E-12   1.576234E-03  -7.585309E-09  -4.655239E-12  -7.503506E-09
  a    30  -2.289255E-11  -1.055733E-04   8.983303E-12   2.537404E-12  -7.585309E-09   2.724997E-03  -5.029324E-11  -6.161326E-04
  a    31   1.015755E-04  -1.688304E-12  -3.996978E-10  -6.220325E-04  -4.655239E-12  -5.029324E-11   3.082565E-03   1.738263E-11
  a    32  -2.490533E-11  -2.067183E-04   1.927703E-11  -2.438383E-11  -7.503506E-09  -6.161326E-04   1.738263E-11   9.738981E-04
  a    33  -2.925914E-04  -2.806367E-11  -1.034734E-08  -7.113202E-04  -1.559212E-11  -1.136028E-11   1.178979E-03   4.544945E-11
  a    34   6.386495E-09  -9.495280E-11  -1.172749E-03  -5.916269E-09   2.187085E-11   9.140116E-12   3.733266E-10  -4.957296E-12
  a    35  -2.415464E-11   7.251679E-09  -1.666649E-11   1.179184E-11  -1.215467E-03   5.160476E-09   4.924282E-12   5.235462E-09
  a    36   8.449519E-11  -2.066468E-04   2.014924E-11   5.891285E-12  -4.776043E-09   1.820064E-03  -2.408993E-10  -3.982406E-04
  a    37   5.066808E-04   3.883336E-11   4.403228E-08   6.265180E-05  -3.771419E-11  -3.345352E-10  -7.374666E-04   8.773219E-11
  a    38  -3.636573E-04   8.334592E-12  -1.830424E-08   3.604677E-04   6.487861E-12   8.411061E-11  -2.198112E-03  -5.444785E-11
  a    39  -2.541867E-11  -6.429458E-05   5.522324E-12   3.707341E-12   1.251748E-08  -1.415887E-03  -1.028813E-10   7.788806E-04
  a    40  -1.609240E-11   2.897253E-04  -9.976497E-12  -9.464136E-12   4.308048E-08   5.616760E-04  -5.908129E-12  -2.896365E-04
  a    41   9.703899E-09  -6.987138E-11  -9.642904E-04  -7.021194E-09  -4.294085E-11   3.400438E-12   4.512237E-10  -4.047657E-12
  a    42   1.667838E-12   6.966788E-09   7.585200E-12   3.605125E-12  -8.561829E-04   5.210563E-09   5.428223E-13   5.057990E-09
  a    43   2.794459E-05   5.553095E-12   2.346498E-10  -1.732367E-04  -7.905340E-13  -2.006680E-11   5.424295E-04  -4.229340E-12
  a    44   5.284429E-12  -1.516577E-05   1.525212E-12   9.099332E-12   5.227840E-10   5.292734E-04   5.567779E-12  -2.295658E-04

               a    33        a    34        a    35        a    36        a    37        a    38        a    39        a    40
  a     5   1.647726E-03  -4.093354E-10  -1.571583E-11  -1.861960E-09  -2.617366E-04  -1.490579E-03   9.148618E-10  -4.977189E-10
  a     6   2.140087E-09   8.541904E-12   1.914614E-11   1.789615E-03  -6.790801E-10  -1.885579E-09  -9.775955E-04   5.130159E-04
  a     7  -1.290056E-03  -1.808501E-09   3.467912E-11  -7.026577E-10  -7.454089E-04  -1.093588E-03  -3.774931E-10   2.414191E-11
  a     8   4.473789E-10  -5.659624E-12   4.358896E-10   1.047050E-03  -5.421900E-11  -8.922277E-11   1.521684E-03  -1.188784E-04
  a     9   2.293501E-09   1.158706E-04   1.469824E-10  -1.463600E-11  -1.864561E-08   5.953619E-09  -1.549002E-11   1.377471E-10
  a    10  -1.248340E-11   1.936302E-10  -5.642121E-05   1.109325E-08   1.239573E-10  -1.495447E-10  -2.389532E-10  -2.301075E-08
  a    11  -8.824751E-04   2.020050E-09  -2.012538E-11   5.301985E-10   1.159347E-03   3.443061E-03  -3.273828E-10   1.797128E-10
  a    12  -3.993172E-10  -7.973182E-13  -4.119616E-09  -1.562281E-03   1.070826E-09   1.954953E-09   3.228139E-03  -1.115029E-03
  a    13  -2.587599E-03  -5.378108E-07  -6.442737E-10   2.076205E-09   1.068952E-02  -4.637194E-03  -1.639414E-10   2.228073E-10
  a    14   3.105543E-10  -3.163255E-10  -5.697459E-07  -7.065992E-04  -1.060923E-09   3.431977E-10   3.461445E-03   1.156611E-02
  a    15  -1.932103E-08  -7.507981E-03  -2.960872E-10  -9.095708E-12   6.271143E-08  -3.228079E-08   6.752683E-11   2.408977E-10
  a    16   1.689146E-10  -1.884570E-10   8.856673E-03   8.017881E-09  -2.394668E-11   8.950486E-11  -1.513078E-08  -4.723347E-08
  a    17   3.398120E-03   5.546661E-07   1.165476E-09  -2.901187E-09  -1.371983E-02   5.578894E-03   7.283120E-10   1.611252E-09
  a    18  -2.773136E-10   6.110931E-10   5.118458E-07   2.026666E-03   7.131116E-10  -3.447611E-10  -2.793106E-03  -1.030359E-02
  a    19  -1.699890E-03   5.511319E-10   3.400288E-11  -1.085714E-10  -3.743829E-04   2.287221E-04  -1.084822E-11   5.287531E-12
  a    20  -4.481385E-11   1.656857E-12   1.066540E-09  -1.428996E-03   2.624077E-10   3.310451E-11  -3.443971E-04  -2.468461E-04
  a    21   1.358519E-08   2.764653E-03  -2.202759E-11  -2.917234E-12  -5.753993E-08   2.332179E-08  -2.454366E-12  -2.343739E-11
  a    22  -1.193110E-03   2.782628E-09  -5.895493E-11   8.205468E-11   2.005846E-04   1.426346E-03   6.148731E-11   1.187335E-11
  a    23   4.489050E-12  -9.109982E-11   2.895832E-03   6.641064E-09   8.657666E-11   2.965946E-11  -1.476607E-08  -5.281272E-08
  a    24  -1.564736E-12  -2.090737E-12   5.261062E-09  -1.557648E-03   2.979990E-10  -6.258463E-11   1.080046E-03  -6.824506E-04
  a    25  -2.925914E-04   6.386495E-09  -2.415464E-11   8.449519E-11   5.066808E-04  -3.636573E-04  -2.541867E-11  -1.609240E-11
  a    26  -2.806367E-11  -9.495280E-11   7.251679E-09  -2.066468E-04   3.883336E-11   8.334592E-12  -6.429458E-05   2.897253E-04
  a    27  -1.034734E-08  -1.172749E-03  -1.666649E-11   2.014924E-11   4.403228E-08  -1.830424E-08   5.522324E-12  -9.976497E-12
  a    28  -7.113202E-04  -5.916269E-09   1.179184E-11   5.891285E-12   6.265180E-05   3.604677E-04   3.707341E-12  -9.464136E-12
  a    29  -1.559212E-11   2.187085E-11  -1.215467E-03  -4.776043E-09  -3.771419E-11   6.487861E-12   1.251748E-08   4.308048E-08
  a    30  -1.136028E-11   9.140116E-12   5.160476E-09   1.820064E-03  -3.345352E-10   8.411061E-11  -1.415887E-03   5.616760E-04
  a    31   1.178979E-03   3.733266E-10   4.924282E-12  -2.408993E-10  -7.374666E-04  -2.198112E-03  -1.028813E-10  -5.908129E-12
  a    32   4.544945E-11  -4.957296E-12   5.235462E-09  -3.982406E-04   8.773219E-11  -5.444785E-11   7.788806E-04  -2.896365E-04
  a    33   1.536912E-03   6.450230E-09   2.185395E-11  -1.457940E-10  -5.981637E-04  -8.914795E-04  -1.872223E-11   1.468311E-11
  a    34   6.450230E-09   1.718064E-03  -4.295099E-11   4.893676E-12  -2.885872E-08   1.173744E-08  -8.495911E-12  -1.755869E-11
  a    35   2.185395E-11  -4.295099E-11   1.796295E-03   3.194609E-09  -2.202031E-11   1.955366E-11  -8.565294E-09  -2.981508E-08
  a    36  -1.457940E-10   4.893676E-12   3.194609E-09   2.088221E-03   5.257319E-11   1.243823E-10  -1.320425E-03   3.471142E-04
  a    37  -5.981637E-04  -2.885872E-08  -2.202031E-11   5.257319E-11   2.217301E-03  -3.153746E-05   2.351655E-10  -1.405718E-10
  a    38  -8.914795E-04   1.173744E-08   1.955366E-11   1.243823E-10  -3.153746E-05   2.696082E-03   7.359618E-11   4.092804E-11
  a    39  -1.872223E-11  -8.495911E-12  -8.565294E-09  -1.320425E-03   2.351655E-10   7.359618E-11   1.709736E-03  -1.197757E-04
  a    40   1.468311E-11  -1.755869E-11  -2.981508E-08   3.471142E-04  -1.405718E-10   4.092804E-11  -1.197757E-04   1.914458E-03
  a    41   8.815601E-09   8.920572E-04   5.463842E-11  -3.442843E-12  -3.700210E-08   1.547720E-08   2.619902E-12   1.577216E-11
  a    42   6.163006E-12  -1.978386E-11   8.270297E-04   3.759833E-09   3.578821E-11  -7.183305E-12  -9.221393E-09  -3.197757E-08
  a    43  -9.302079E-05  -2.618811E-10   1.659006E-12  -1.447488E-11  -4.927670E-05  -1.251054E-04  -8.830661E-12  -1.089588E-12
  a    44  -9.992824E-12  -3.834933E-13  -5.346028E-10  -1.422904E-05  -1.794867E-12   6.003221E-12  -1.704287E-04   4.623787E-05

               a    41        a    42        a    43        a    44
  a     5  -2.747643E-10  -5.449495E-12   1.765410E-03  -1.720578E-09
  a     6   1.379445E-11   5.965034E-10   2.275104E-09   1.772586E-03
  a     7  -5.572852E-09   3.900773E-12   8.626999E-04   1.240939E-10
  a     8  -1.462691E-12   1.976949E-09  -9.114996E-11  -7.292652E-04
  a     9   1.241687E-03  -4.756323E-10   1.507323E-09   1.388026E-11
  a    10  -9.492601E-11   5.764923E-04   4.633833E-11   4.345049E-09
  a    11  -1.107876E-09  -4.726750E-12  -1.875629E-03   6.030514E-10
  a    12   1.533120E-11   3.904289E-09  -7.447778E-10  -1.787993E-03
  a    13  -3.687632E-07  -1.458300E-10   1.375615E-04   3.039584E-11
  a    14  -9.600442E-11  -3.314831E-07  -1.053275E-11   2.117322E-04
  a    15   6.313409E-03  -8.440074E-10  -7.098327E-09   3.206474E-12
  a    16   7.026169E-10  -3.794512E-03  -4.163395E-12  -6.625900E-09
  a    17   4.828026E-07   2.537204E-10  -1.734467E-05  -2.425616E-11
  a    18   1.923122E-10   3.714927E-07   7.071665E-12   5.066400E-05
  a    19   2.757459E-11   9.135732E-13   3.531806E-04   2.188232E-11
  a    20   2.271809E-12   7.421157E-10   3.198308E-12   3.428266E-04
  a    21   1.459106E-03  -9.005168E-12  -5.363822E-10  -5.024697E-13
  a    22   4.500507E-09  -3.254583E-11  -5.784462E-04   7.060544E-12
  a    23   7.889413E-11   1.307137E-03  -1.470692E-11  -7.649580E-10
  a    24  -2.679585E-12   5.164796E-09   2.309268E-11  -5.571365E-04
  a    25   9.703899E-09   1.667838E-12   2.794459E-05   5.284429E-12
  a    26  -6.987138E-11   6.966788E-09   5.553095E-12  -1.516577E-05
  a    27  -9.642904E-04   7.585200E-12   2.346498E-10   1.525212E-12
  a    28  -7.021194E-09   3.605125E-12  -1.732367E-04   9.099332E-12
  a    29  -4.294085E-11  -8.561829E-04  -7.905340E-13   5.227840E-10
  a    30   3.400438E-12   5.210563E-09  -2.006680E-11   5.292734E-04
  a    31   4.512237E-10   5.428223E-13   5.424295E-04   5.567779E-12
  a    32  -4.047657E-12   5.057990E-09  -4.229340E-12  -2.295658E-04
  a    33   8.815601E-09   6.163006E-12  -9.302079E-05  -9.992824E-12
  a    34   8.920572E-04  -1.978386E-11  -2.618811E-10  -3.834933E-13
  a    35   5.463842E-11   8.270297E-04   1.659006E-12  -5.346028E-10
  a    36  -3.442843E-12   3.759833E-09  -1.447488E-11  -1.422904E-05
  a    37  -3.700210E-08   3.578821E-11  -4.927670E-05  -1.794867E-12
  a    38   1.547720E-08  -7.183305E-12  -1.251054E-04   6.003221E-12
  a    39   2.619902E-12  -9.221393E-09  -8.830661E-12  -1.704287E-04
  a    40   1.577216E-11  -3.197757E-08  -1.089588E-12   4.623787E-05
  a    41   9.662460E-04   1.238394E-11  -3.128628E-10   1.928596E-13
  a    42   1.238394E-11   7.979394E-04  -3.058102E-14  -4.575784E-10
  a    43  -3.128628E-10  -3.058102E-14   4.916029E-04  -5.314276E-12
  a    44   1.928596E-13  -4.575784E-10  -5.314276E-12   4.608671E-04

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99859386     1.99857549     1.99476635     1.99470921
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99293707     1.99292282     1.99189538     1.99156198     1.94326989     1.93868474     1.56309754     1.43479166
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.62630275     0.48833489     0.00807588     0.00803478     0.00748489     0.00746641     0.00373452     0.00367426
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00209595     0.00157861     0.00147372     0.00129994     0.00084476     0.00083241     0.00080705     0.00052656
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00046938     0.00033688     0.00023492     0.00022369     0.00013577     0.00012183     0.00003068     0.00002421
              MO    41       MO    42       MO    43       MO    44
  occ(*)=     0.00001961     0.00001934     0.00000588     0.00000445


 total number of electrons =   32.0000000000

 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   2logvrecs(*)=   1   2
 item #                     4 suffix=:.trd1to2:
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   2)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   2)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  1.820321516125822E-007
 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   3logvrecs(*)=   1   3
 item #                     5 suffix=:.trd1to3:
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  3.237056082988632E-006
 accstate=                     4
 accpdens=                     4
logrecs(*)=   2   3logvrecs(*)=   2   3
 item #                     6 suffix=:.trd2to3:
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 =========== Executing IN-CORE method ==========
1e-transition density (   2,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   2,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  1.508603333921776E-008
