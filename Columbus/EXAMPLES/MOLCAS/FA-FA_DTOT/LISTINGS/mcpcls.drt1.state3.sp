

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    SEWARD INTEGRALS                                                                
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  

 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -227.5825319506    nuclear repulsion=   100.5855518353
 demc=             0.0000000000    wnorm=                 0.0000000086
 knorm=            0.0000000071    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  3 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      7 -0.6900049025  0.4761067654  331320
      3 -0.6298722782  0.3967390869  333102
     23 -0.1330418162  0.0177001249  313230
     54  0.1296566807  0.0168108549  133212
     30 -0.1259028353  0.0158515239  312312
     77 -0.1087563016  0.0118279331  121332
     61  0.1078084278  0.0116226571  132303
     75  0.0995881174  0.0099177931  123123
     48  0.0836753167  0.0070015586  301323
     94 -0.0807326316  0.0065177578  033132
     56  0.0686950362  0.0047190080  133122
     35  0.0673720802  0.0045389972  311322
     44 -0.0669267028  0.0044791835  303132
     98  0.0634539930  0.0040264092  031323
     25 -0.0617931682  0.0038183956  313203
     59  0.0601587062  0.0036190699  132330
     90  0.0409149767  0.0016740353  102333
    103  0.0406924908  0.0016558788  013233
     84  0.0236712588  0.0005603285  112332
     83  0.0195829323  0.0003834912  113223
     18 -0.0146530851  0.0002147129  330132
     14 -0.0135035703  0.0001823464  331023
     40 -0.0041576932  0.0000172864  310233
     64 -0.0038472155  0.0000148011  132033

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: