 total ao core energy =  100.585551835
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          3             0.333 0.333 0.333

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105

 Number of active-double rotations:        72
 Number of active-active rotations:         0
 Number of double-virtual rotations:      312
 Number of active-virtual rotations:      156
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -227.5825319506  2.276E+02  7.938E-07  7.548E-07  1.242E-13  F   *not conv.*     
    2   -227.5825319506 -1.137E-13  4.787E-08  7.103E-08  7.061E-16  F   *not conv.*     

 final mcscf convergence values:
    3   -227.5825319506  5.684E-14  8.564E-09  7.119E-09  2.171E-16  F   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=     -227.683128303, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.333 total energy=     -227.532446690, rel. (eV)=   4.100257
   DRT #1 state # 3 wt 0.333 total energy=     -227.532020858, rel. (eV)=   4.111845
   ------------------------------------------------------------


