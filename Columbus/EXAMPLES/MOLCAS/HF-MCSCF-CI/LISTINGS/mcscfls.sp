

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        32768000 of real*8 words (  250.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,23, 11,-12, 2,26
   ncoupl=5,
   FCIORB=  1,2,20,1,3,20,1,4,20,2,1,20,4,1,20
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***



 Integral file header information:
 SEWARD INTEGRALS                                                                
 total ao core energy =    5.220720459


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  44

 irrep no.              1    2    3    4
 irrep label           a1   b1   a2   b2 
 no. of bas.fcions.    20   10    4   10
 map-vector 1 , imtype=                        3
  1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2   2   2   2   2
  1   1   1   2   2   2   2   2   2   2   1   2   2   2   1   1   1   2   2   2
  2   2   2   2
 map-vector 2 , imtype=                        4
  1   1   1   2   2   4   4   1   1   1   1   2   2   2   4   4   4   4   6   6
  2   2   4   2   2   2   4   4   6   6   4   4   4   6   2   2   4   2   2   2
  4   4   6   6
 using sifcfg values:                     4096                     3272 
                     4096                     2730


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

AO integrals are read from Seward integral files 
 ... RUNFILE, ONEINT , ORDINT 

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  a1         2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       2(  2)    20
       1       3(  3)    20
       1       4(  4)    20
       2       1( 21)    20
       4       1( 35)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 11:  construct the hmc(*) matrix explicitly.
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.
 26:  Read AO integrals in MOLCAS native format 


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   10
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   8
 Total number of CSFs:         8

 !timer: initialization                  user+sys=     0.000 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   3 possible.


 Number of active-double rotations:         3
 Number of active-active rotations:         0
 Number of double-virtual rotations:       16
 Number of active-virtual rotations:       66

 Size of orbital-Hessian matrix B:                     4207
 Size of the orbital-state Hessian matrix C:            680
 Total size of the state Hessian matrix M:               36
 Size of HESSIAN-matrix for quadratic conv.:           4923



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is  19
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  5488

 twoao_o processed      96284 two electron ao integrals.

      96284 ao integrals were written into   90 records

 srtinc_o read in     128745 integrals and wrote out     128745 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.030 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     10991
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.030 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4207
 Total size of the state Hessian matrix M:               36
 Total size of HESSIAN-matrix for linear con           4243

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0591694495     -105.2798899089
    2       -99.4555981276     -104.6763185871
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   1.2655610389711915E-003
 Total number of micro iterations:   10

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.38516477 pnorm= 0.0000E+00 rznorm= 7.4354E-06 rpnorm= 0.0000E+00 noldr= 10 nnewr= 10 nolds=  0 nnews=  0
 *** warning *** small z0.

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.578559

 qvv(*) eigenvalues. symmetry block  1
     1.130088    1.629868    2.893549    3.103157    4.455541    4.554642    6.430038    7.090598    8.481496   10.287444
    12.491014   15.203930   17.012707   17.813917   19.380297   25.049068

 qvv(*) eigenvalues. symmetry block  2
     1.706913    1.975764    5.072456    7.706500    8.609159   10.709335   14.816852   16.910576   18.662611

 qvv(*) eigenvalues. symmetry block  3
     4.455541    7.090598   15.203930   17.813917

 qvv(*) eigenvalues. symmetry block  4
     1.706913    1.975764    5.072456    7.706500    8.609159   10.709335   14.816852   16.910576   18.662611
 *** warning *** small active-orbital occupation. i=  1 nocc= 9.7136E-04
 *** warning *** large active-orbital occupation. i=  2 nocc= 1.9991E+00
 *** warning *** large active-orbital occupation. i=  3 nocc= 2.0000E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 2.0000E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 2.0000E+00
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.040 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=   -100.0591694495 demc= 1.0006E+02 wnorm= 1.0124E-02 knorm= 9.2285E-01 apxde= 8.8234E-03    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     0.070 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     10991
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.030 walltime=     1.000

 Size of orbital-Hessian matrix B:                     4207
 Total size of the state Hessian matrix M:               36
 Total size of HESSIAN-matrix for linear con           4243

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0781938849     -105.2989143443
    2       -98.5622229213     -103.7829433807
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   7.9571196071055166E-003
 Total number of micro iterations:    8

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.97328319 pnorm= 0.0000E+00 rznorm= 8.9368E-06 rpnorm= 0.0000E+00 noldr=  8 nnewr=  8 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.563728

 qvv(*) eigenvalues. symmetry block  1
     0.741152    1.589943    2.367133    3.065470    4.455704    4.492938    6.293220    7.091145    8.168876   10.006436
    12.450817   15.204746   16.908995   17.816580   19.296434   25.043549

 qvv(*) eigenvalues. symmetry block  2
     1.707545    1.976132    5.073109    7.707668    8.610362   10.712054   14.817626   16.912165   18.664446

 qvv(*) eigenvalues. symmetry block  3
     4.455704    7.091145   15.204746   17.816580

 qvv(*) eigenvalues. symmetry block  4
     1.707545    1.976132    5.073109    7.707668    8.610362   10.712054   14.817626   16.912165   18.664446
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.9999E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9999E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9999E+00
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.030 walltime=     1.000

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc=   -100.0781938849 demc= 1.9024E-02 wnorm= 6.3657E-02 knorm= 2.2961E-01 apxde= 2.9085E-03    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 user+sys=     0.100 walltime=     1.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     10991
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.030 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4207
 Total size of the state Hessian matrix M:               36
 Total size of HESSIAN-matrix for linear con           4243

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0819671304     -105.3026875898
    2       -98.7627452529     -103.9834657124
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   3.0235551886140097E-003
 Total number of micro iterations:    8

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99774710 pnorm= 0.0000E+00 rznorm= 2.0743E-06 rpnorm= 0.0000E+00 noldr=  8 nnewr=  8 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.517195

 qvv(*) eigenvalues. symmetry block  1
     0.553392    1.585315    2.419287    3.067509    4.452980    4.519412    6.346717    7.097248    8.355498   10.186947
    12.474170   15.200353   16.968385   17.810447   19.351679   25.049889

 qvv(*) eigenvalues. symmetry block  2
     1.710400    1.974539    5.071979    7.717288    8.613040   10.708471   14.812794   16.908977   18.658363

 qvv(*) eigenvalues. symmetry block  3
     4.452980    7.097248   15.200353   17.810447

 qvv(*) eigenvalues. symmetry block  4
     1.710400    1.974539    5.071979    7.717288    8.613040   10.708471   14.812794   16.908977   18.658363
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.030 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc=   -100.0819671304 demc= 3.7732E-03 wnorm= 2.4188E-02 knorm= 6.7087E-02 apxde= 2.2298E-04    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 user+sys=     0.130 walltime=     1.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     10991
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.020 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4207
 Total size of the state Hessian matrix M:               36
 Total size of HESSIAN-matrix for linear con           4243

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0822718836     -105.3029923430
    2       -98.8022190688     -104.0229395282
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   1.9041317085934102E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99977096 pnorm= 0.0000E+00 rznorm= 3.7132E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.283040

 qvv(*) eigenvalues. symmetry block  1
     0.541896    1.584482    2.479650    3.062433    4.452132    4.522333    6.368305    7.098885    8.389926   10.214050
    12.475449   15.199102   16.983441   17.809378   19.362209   25.050028

 qvv(*) eigenvalues. symmetry block  2
     1.711144    1.973837    5.071489    7.719827    8.613754   10.707810   14.811736   16.907892   18.657036

 qvv(*) eigenvalues. symmetry block  3
     4.452132    7.098885   15.199102   17.809378

 qvv(*) eigenvalues. symmetry block  4
     1.711144    1.973837    5.071489    7.719827    8.613754   10.707810   14.811736   16.907892   18.657036
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.040 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc=   -100.0822718836 demc= 3.0475E-04 wnorm= 1.5233E-02 knorm= 2.1402E-02 apxde= 3.2693E-05    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 user+sys=     0.170 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     11074
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.030 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4207
 Size of the orbital-state Hessian matrix C:            680
 Total size of the state Hessian matrix M:               36
 Size of HESSIAN-matrix for quadratic conv.:           4923

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.010 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0823188013     -105.3030392607
    2       -98.8153236637     -104.0360441231
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cadu                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   9.8063762480724286E-004
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    9

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99987349 pnorm= 4.5782E-03 rznorm= 1.9365E-06 rpnorm= 6.8403E-06 noldr=  9 nnewr=  9 nolds=  5 nnews=  5

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.204832

 qvv(*) eigenvalues. symmetry block  1
     0.542018    1.583580    2.501535    3.059034    4.452050    4.521920    6.374833    7.099562    8.398385   10.219527
    12.475206   15.199029   16.988063   17.809726   19.365607   25.050619

 qvv(*) eigenvalues. symmetry block  2
     1.711507    1.973669    5.071519    7.720823    8.614239   10.708151   14.811805   16.907899   18.657164

 qvv(*) eigenvalues. symmetry block  3
     4.452050    7.099562   15.199029   17.809726

 qvv(*) eigenvalues. symmetry block  4
     1.711507    1.973669    5.071519    7.720823    8.614239   10.708151   14.811805   16.907899   18.657164
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.040 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    5 emc=   -100.0823188013 demc= 4.6918E-05 wnorm= 7.8451E-03 knorm= 1.5906E-02 apxde= 1.1728E-05    *not converged* 

               starting mcscf iteration...   6
 !timer:                                 user+sys=     0.210 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     11095
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.030 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4207
 Size of the orbital-state Hessian matrix C:            680
 Total size of the state Hessian matrix M:               36
 Size of HESSIAN-matrix for quadratic conv.:           4923

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0823305297     -105.3030509891
    2       -98.8307019010     -104.0514223604
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cadu                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   5.3774526745750872E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999889 pnorm= 4.5169E-05 rznorm= 6.3668E-07 rpnorm= 2.1291E-07 noldr=  7 nnewr=  7 nolds=  5 nnews=  5

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.234739

 qvv(*) eigenvalues. symmetry block  1
     0.541071    1.582588    2.517194    3.055716    4.452048    4.521052    6.379583    7.100081    8.403850   10.222487
    12.474779   15.199067   16.991355   17.810159   19.368093   25.051176

 qvv(*) eigenvalues. symmetry block  2
     1.711796    1.973565    5.071587    7.721572    8.614647   10.708542   14.811974   16.907999   18.657387

 qvv(*) eigenvalues. symmetry block  3
     4.452048    7.100081   15.199067   17.810159

 qvv(*) eigenvalues. symmetry block  4
     1.711796    1.973565    5.071587    7.721572    8.614647   10.708542   14.811974   16.907999   18.657387
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.040 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    6 emc=   -100.0823305297 demc= 1.1728E-05 wnorm= 4.3020E-04 knorm= 1.4915E-03 apxde= 1.5092E-08    *not converged* 

               starting mcscf iteration...   7
 !timer:                                 user+sys=     0.250 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     10995
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.030 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4207
 Size of the orbital-state Hessian matrix C:            680
 Total size of the state Hessian matrix M:               36
 Size of HESSIAN-matrix for quadratic conv.:           4923

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0823305449     -105.3030510043
    2       -98.8293541407     -104.0500746001
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cadu                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   2.3662544772569660E-007
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 6.7438E-07 rpnorm= 1.1076E-07 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.222526

 qvv(*) eigenvalues. symmetry block  1
     0.540984    1.582562    2.517183    3.055673    4.452056    4.521029    6.379586    7.100074    8.403765   10.222386
    12.474763   15.199080   16.991357   17.810175   19.368110   25.051182

 qvv(*) eigenvalues. symmetry block  2
     1.711794    1.973570    5.071595    7.721561    8.614647   10.708553   14.811986   16.908009   18.657410

 qvv(*) eigenvalues. symmetry block  3
     4.452056    7.100074   15.199080   17.810175

 qvv(*) eigenvalues. symmetry block  4
     1.711794    1.973570    5.071595    7.721561    8.614647   10.708553   14.811986   16.908009   18.657410
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.030 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    7 emc=   -100.0823305449 demc= 1.5179E-08 wnorm= 1.8930E-06 knorm= 2.0796E-08 apxde= 2.4180E-14    *not converged* 

               starting mcscf iteration...   8
 !timer:                                 user+sys=     0.280 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     10994
 number of records written:     5
 !timer: 2-e transformation              user+sys=     0.030 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4207
 Size of the orbital-state Hessian matrix C:            680
 Total size of the state Hessian matrix M:               36
 Size of HESSIAN-matrix for quadratic conv.:           4923

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*     -100.0823305449     -105.3030510043
    2       -98.8293541448     -104.0500746042
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cadu                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   8.7356660667336286E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 9.9589E-07 rpnorm= 5.2478E-08 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -52.222526

 qvv(*) eigenvalues. symmetry block  1
     0.540984    1.582562    2.517183    3.055673    4.452056    4.521029    6.379586    7.100074    8.403765   10.222386
    12.474763   15.199080   16.991357   17.810175   19.368110   25.051182

 qvv(*) eigenvalues. symmetry block  2
     1.711794    1.973570    5.071595    7.721561    8.614647   10.708553   14.811986   16.908009   18.657410

 qvv(*) eigenvalues. symmetry block  3
     4.452056    7.100074   15.199080   17.810175

 qvv(*) eigenvalues. symmetry block  4
     1.711794    1.973570    5.071595    7.721561    8.614647   10.708553   14.811986   16.908009   18.657410
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 *** warning *** large active-orbital occupation. i=  1 nocc= 1.9998E+00
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.040 walltime=     0.000

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    8 emc=   -100.0823305449 demc= 2.8422E-14 wnorm= 6.9885E-07 knorm= 3.1293E-08 apxde= 1.0610E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=     -100.082330545
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  a1  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
 H   1s      -0.00414973     0.06299806    -0.80089929     1.84747525    -0.21487281     0.19149728     0.46905284     0.33113621
 H   *s       0.00541285    -0.06910274     0.16292001    -0.19539624    -0.49468782     3.69166366     0.69628671     3.35386708
 H   *s       0.00131458    -0.02756168     0.28434899    -0.69662247     1.82154333    -0.10705062     0.14297467     0.71966701
 H   *pz     -0.00002935     0.00776515    -0.01947555     0.04038618    -0.02056692     0.07418445    -0.18854304     0.12424269
 H   *pz     -0.00004680     0.00330777    -0.02090146     0.11258712     0.26928614     1.49703227     1.97413552     2.34829158
 H   *d0     -0.00000675     0.00179750    -0.00329056     0.00473680     0.01508720     0.14361728     0.01856679     0.27067246
 H   *d2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 F   1s       0.99618420     0.08035396    -0.00106268    -0.02849780     0.04757136    -0.02625226    -0.26500559     0.42907861
 F   2s      -0.08169733     0.94686244    -0.15762850    -0.47364197     0.04864327    -0.29600644    -1.20619610     1.95855854
 F   *s       0.00081359    -0.00083001     0.00547846    -0.04686087    -0.02326380     0.04254448     0.06969184    -0.08520270
 F   *s      -0.00231878     0.03574754     0.13002192    -0.11775051    -0.77346565    -2.88261254    -0.67537692    -6.22963222
 F   2pz     -0.02192866     0.23829166     0.78204455     1.18021792    -0.10820840    -0.33630918     0.20121671     0.11523337
 F   *pz      0.00334422    -0.00595819    -0.00272041    -0.13722361    -0.01394273     0.10512349     0.26747276     0.70422009
 F   *pz      0.00073057     0.00087107    -0.05228996    -0.39040301     0.26510901     2.99489852     0.31609241     1.81933525
 F   *d0      0.00002149    -0.00050689    -0.00871569    -0.02304541    -0.00341625     0.03466425    -0.00075125     0.01664582
 F   *d0     -0.00039500    -0.00218872    -0.02223047     0.00206762    -0.03029396    -0.24780047     0.02582005    -0.14810126
 F   *d2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 F   *d2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 F   *f0      0.00014677     0.00014742     0.00460155     0.01961666     0.00074896     0.02283250    -0.02644484     0.03728908
 F   *f2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
 H   1s       0.00000000    -4.56079081    -4.01085416     0.00000000     1.67955934    -0.16528058    -2.34547335     0.00000000
 H   *s       0.00000000     8.88725128     0.46859160     0.00000000    -2.70981552     3.29537817    -2.86600839     0.00000000
 H   *s       0.00000000     1.27388837     1.11579072     0.00000000    -0.80740833    -0.01058510     0.87333569     0.00000000
 H   *pz      0.00000000     0.31119470     0.16259251     0.00000000     0.95226082     0.89788186    -1.15494825     0.00000000
 H   *pz      0.00000000     2.70018759    -1.26668194     0.00000000    -1.93864188     1.67412197    -2.05983353     0.00000000
 H   *d0      0.00000000     0.39144159    -0.32382425     0.00000000    -0.85959929     1.46360922    -0.79838832     0.00000000
 H   *d2+     0.17138401     0.00000000     0.00000000    -1.00532009     0.00000000     0.00000000     0.00000000     0.24660452
 F   1s       0.00000000    -0.11546446    -0.08973892     0.00000000     0.29066845    -0.32249960    -0.10775629     0.00000000
 F   2s       0.00000000    -0.34130741    -0.42897769     0.00000000     0.90231269    -1.50015149     0.27738627     0.00000000
 F   *s       0.00000000     0.14348595    -0.02187132     0.00000000    -0.08818649    -0.04930949     0.33028038     0.00000000
 F   *s       0.00000000    -4.80003185     2.73857085     0.00000000     1.29960508    -1.86880261     3.84621840     0.00000000
 F   2pz      0.00000000    -0.11989473    -0.42168152     0.00000000     1.06803698     1.24311518     2.43725272     0.00000000
 F   *pz      0.00000000     1.11670047     0.06763571     0.00000000    -1.49092261     0.31589197    -3.87505795     0.00000000
 F   *pz      0.00000000     2.51615464    -1.24971403     0.00000000    -0.62876226     0.73415315    -2.09240422     0.00000000
 F   *d0      0.00000000     0.00871312     0.01831187     0.00000000     0.06247317     0.09775372    -0.07457672     0.00000000
 F   *d0      0.00000000    -0.98021410     1.15724801     0.00000000    -0.39920265    -0.60482108     1.14142891     0.00000000
 F   *d2+    -0.00980538     0.00000000     0.00000000     0.00189971     0.00000000     0.00000000     0.00000000    -0.08675263
 F   *d2+     0.94942339     0.00000000     0.00000000     0.40919724     0.00000000     0.00000000     0.00000000    -0.01811962
 F   *f0      0.00000000     0.10727378     0.02550305     0.00000000    -0.05093909     0.19862863     0.21529814     0.00000000
 F   *f2+    -0.00570723     0.00000000     0.00000000     0.04482843     0.00000000     0.00000000     0.00000000     1.01522554

               MO   17        MO   18        MO   19        MO   20
 H   1s      -3.03043540     0.00000000     2.95160173     3.09032499
 H   *s       0.98541726     0.00000000    -0.36833826     0.90672966
 H   *s       1.00080684     0.00000000    -1.09598938    -1.59114536
 H   *pz     -0.77282717     0.00000000     0.38741643     1.40873142
 H   *pz     -0.12136712     0.00000000     0.34421183     1.18622358
 H   *d0     -0.17156069     0.00000000    -0.15253639     0.95507752
 H   *d2+     0.00000000     0.10208953     0.00000000     0.00000000
 F   1s      -0.88684335     0.00000000     0.04569588    -2.87391450
 F   2s      -1.73931642     0.00000000     0.23364813    -6.78966409
 F   *s       1.23204144     0.00000000     0.12102248     3.70020978
 F   *s       1.99941147     0.00000000    -1.49601912     1.94799837
 F   2pz      0.83806621     0.00000000    -0.66250955    -1.02457200
 F   *pz     -1.30191918     0.00000000     0.88493714     2.57070806
 F   *pz     -0.58774199     0.00000000     1.00295927     0.85604238
 F   *d0      0.40686209     0.00000000     1.13312084    -0.23745261
 F   *d0      0.41537263     0.00000000    -1.18195320    -0.85904294
 F   *d2+     0.00000000     1.15881708     0.00000000     0.00000000
 F   *d2+     0.00000000    -0.60210523     0.00000000     0.00000000
 F   *f0     -0.92425194     0.00000000     0.50860191     0.55268178
 F   *f2+     0.00000000     0.09136286     0.00000000     0.00000000

          mcscf orbitals of the final iteration,  b1  block   2

               MO   21        MO   22        MO   23        MO   24        MO   25        MO   26        MO   27        MO   28
 H   *px      0.01116998     0.08600841     0.03719484     0.16472825     1.18449282     0.30327151     0.30103668     0.00000000
 H   *px      0.03098356    -1.03070543    -0.84432569    -0.70529610    -0.88004546     0.53515083     0.12922679     0.00000000
 H   *d1+     0.00640463    -0.00707985    -0.06039766     0.08957086    -0.42962446     1.19603940     0.60637638     0.00000000
 F   2px      0.96157316     0.43950411    -0.47728126    -0.10918186    -0.31458454    -1.22565754     2.79195656     0.00000000
 F   *px     -0.00141488     0.22679405    -0.34606384    -0.00241319     0.36283730     0.70726404    -2.88899132     0.00000000
 F   *px      0.02928627    -0.25985790     1.60183199     0.44786293     0.34623053    -0.15458276    -0.53192032     0.00000000
 F   *d1+    -0.00361824     0.01353071     0.01917841    -0.00047442    -0.00452279    -0.04004426    -0.02454879     0.00000000
 F   *d1+    -0.01238848    -0.00929212    -0.02828927    -1.03819859    -0.00984109     0.94134423     0.45422898     0.00000000
 F   *f1+     0.00281233     0.00200102     0.00975388    -0.00502409     0.10083322    -0.00493147     0.12126262     0.00000000
 F   *f3+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000

               MO   29        MO   30
 H   *px     -0.44098011    -0.33363554
 H   *px     -0.07178406    -0.37325914
 H   *d1+    -0.57598991    -0.50369071
 F   2px     -0.44510954    -0.29267293
 F   *px      0.64619693     0.46514532
 F   *px      0.18420189     0.32652777
 F   *d1+    -0.55660076     1.05338577
 F   *d1+    -0.17140777    -1.02770625
 F   *f1+     0.96154684     0.63195128
 F   *f3+     0.00000000     0.00000000

          mcscf orbitals of the final iteration,  a2  block   3

               MO   31        MO   32        MO   33        MO   34
 H   *d2-     0.17138401    -1.00532009     0.24660452     0.10208953
 F   *d2-    -0.00980538     0.00189971    -0.08675263     1.15881708
 F   *d2-     0.94942339     0.40919724    -0.01811962    -0.60210523
 F   *f2-    -0.00570723     0.04482843     1.01522554     0.09136286

          mcscf orbitals of the final iteration,  b2  block   4

               MO   35        MO   36        MO   37        MO   38        MO   39        MO   40        MO   41        MO   42
 H   *py      0.01116998     0.08600841     0.03719484     0.16472825     1.18449282     0.30327151     0.30103668     0.00000000
 H   *py      0.03098356    -1.03070543    -0.84432569    -0.70529610    -0.88004546     0.53515083     0.12922679     0.00000000
 H   *d1-     0.00640463    -0.00707985    -0.06039766     0.08957086    -0.42962446     1.19603940     0.60637638     0.00000000
 F   2py      0.96157316     0.43950411    -0.47728126    -0.10918186    -0.31458454    -1.22565754     2.79195656     0.00000000
 F   *py     -0.00141488     0.22679405    -0.34606384    -0.00241319     0.36283730     0.70726404    -2.88899132     0.00000000
 F   *py      0.02928627    -0.25985790     1.60183199     0.44786293     0.34623053    -0.15458276    -0.53192032     0.00000000
 F   *d1-    -0.00361824     0.01353071     0.01917841    -0.00047442    -0.00452279    -0.04004426    -0.02454879     0.00000000
 F   *d1-    -0.01238848    -0.00929212    -0.02828927    -1.03819859    -0.00984109     0.94134423     0.45422898     0.00000000
 F   *f3-     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 F   *f1-     0.00281233     0.00200102     0.00975388    -0.00502409     0.10083322    -0.00493147     0.12126262     0.00000000

               MO   43        MO   44
 H   *py     -0.44098011    -0.33363554
 H   *py     -0.07178406    -0.37325914
 H   *d1-    -0.57598991    -0.50369071
 F   2py     -0.44510954    -0.29267293
 F   *py      0.64619693     0.46514532
 F   *py      0.18420189     0.32652777
 F   *d1-    -0.55660076     1.05338577
 F   *d1-    -0.17140777    -1.02770625
 F   *f3-     0.00000000     0.00000000
 F   *f1-     0.96154684     0.63195128

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     1.99980040     1.98048984     0.02015440     0.00000000     0.00000000     0.00000000     0.00000000

 H   1s      -0.00414973     0.06299808    -0.80089929     1.84747526    -0.21487282     0.19149728     0.46905284     0.33113621
 H   *s       0.00541285    -0.06910274     0.16292001    -0.19539624    -0.49468783     3.69166366     0.69628671     3.35386707
 H   *s       0.00131458    -0.02756169     0.28434899    -0.69662247     1.82154333    -0.10705061     0.14297467     0.71966700
 H   *pz     -0.00002935     0.00776515    -0.01947555     0.04038618    -0.02056692     0.07418445    -0.18854304     0.12424269
 H   *pz     -0.00004680     0.00330777    -0.02090146     0.11258712     0.26928614     1.49703227     1.97413553     2.34829158
 H   *d0     -0.00000675     0.00179750    -0.00329056     0.00473680     0.01508720     0.14361729     0.01856679     0.27067246
 H   *d2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 F   1s       0.99618420     0.08035396    -0.00106268    -0.02849780     0.04757136    -0.02625226    -0.26500559     0.42907861
 F   2s      -0.08169733     0.94686242    -0.15762852    -0.47364199     0.04864327    -0.29600644    -1.20619610     1.95855854
 F   *s       0.00081359    -0.00083001     0.00547846    -0.04686087    -0.02326380     0.04254448     0.06969184    -0.08520270
 F   *s      -0.00231878     0.03574754     0.13002192    -0.11775051    -0.77346565    -2.88261254    -0.67537692    -6.22963221
 F   2pz     -0.02192866     0.23829170     0.78204455     1.18021791    -0.10820840    -0.33630918     0.20121671     0.11523337
 F   *pz      0.00334422    -0.00595819    -0.00272041    -0.13722361    -0.01394273     0.10512349     0.26747276     0.70422009
 F   *pz      0.00073057     0.00087106    -0.05228996    -0.39040301     0.26510901     2.99489852     0.31609241     1.81933524
 F   *d0      0.00002149    -0.00050689    -0.00871569    -0.02304541    -0.00341625     0.03466425    -0.00075125     0.01664582
 F   *d0     -0.00039500    -0.00218872    -0.02223046     0.00206762    -0.03029396    -0.24780047     0.02582004    -0.14810126
 F   *d2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 F   *d2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 F   *f0      0.00014677     0.00014742     0.00460155     0.01961666     0.00074896     0.02283250    -0.02644484     0.03728908
 F   *f2+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 H   1s       0.00000000    -4.56079082    -4.01085415     0.00000000     1.67955934    -0.16528058    -2.34547335     0.00000000
 H   *s       0.00000000     8.88725129     0.46859158     0.00000000    -2.70981552     3.29537818    -2.86600839     0.00000000
 H   *s       0.00000000     1.27388837     1.11579071     0.00000000    -0.80740833    -0.01058510     0.87333569     0.00000000
 H   *pz      0.00000000     0.31119470     0.16259251     0.00000000     0.95226082     0.89788186    -1.15494825     0.00000000
 H   *pz      0.00000000     2.70018759    -1.26668195     0.00000000    -1.93864188     1.67412197    -2.05983353     0.00000000
 H   *d0      0.00000000     0.39144159    -0.32382425     0.00000000    -0.85959929     1.46360922    -0.79838832     0.00000000
 H   *d2+     0.17138401     0.00000000     0.00000000    -1.00532009     0.00000000     0.00000000     0.00000000     0.24660452
 F   1s       0.00000000    -0.11546446    -0.08973892     0.00000000     0.29066845    -0.32249960    -0.10775629     0.00000000
 F   2s       0.00000000    -0.34130741    -0.42897769     0.00000000     0.90231269    -1.50015149     0.27738628     0.00000000
 F   *s       0.00000000     0.14348595    -0.02187132     0.00000000    -0.08818649    -0.04930949     0.33028038     0.00000000
 F   *s       0.00000000    -4.80003185     2.73857086     0.00000000     1.29960507    -1.86880261     3.84621840     0.00000000
 F   2pz      0.00000000    -0.11989473    -0.42168152     0.00000000     1.06803698     1.24311518     2.43725272     0.00000000
 F   *pz      0.00000000     1.11670047     0.06763570     0.00000000    -1.49092261     0.31589197    -3.87505795     0.00000000
 F   *pz      0.00000000     2.51615464    -1.24971404     0.00000000    -0.62876225     0.73415315    -2.09240422     0.00000000
 F   *d0      0.00000000     0.00871312     0.01831187     0.00000000     0.06247317     0.09775372    -0.07457672     0.00000000
 F   *d0      0.00000000    -0.98021409     1.15724801     0.00000000    -0.39920266    -0.60482108     1.14142891     0.00000000
 F   *d2+    -0.00980538     0.00000000     0.00000000     0.00189971     0.00000000     0.00000000     0.00000000    -0.08675263
 F   *d2+     0.94942339     0.00000000     0.00000000     0.40919724     0.00000000     0.00000000     0.00000000    -0.01811962
 F   *f0      0.00000000     0.10727378     0.02550305     0.00000000    -0.05093909     0.19862863     0.21529814     0.00000000
 F   *f2+    -0.00570723     0.00000000     0.00000000     0.04482843     0.00000000     0.00000000     0.00000000     1.01522554

               MO   17        MO   18        MO   19        MO   20

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000

 H   1s      -3.03043540     0.00000000     2.95160173     3.09032499
 H   *s       0.98541726     0.00000000    -0.36833826     0.90672966
 H   *s       1.00080684     0.00000000    -1.09598938    -1.59114536
 H   *pz     -0.77282717     0.00000000     0.38741643     1.40873142
 H   *pz     -0.12136712     0.00000000     0.34421183     1.18622358
 H   *d0     -0.17156069     0.00000000    -0.15253639     0.95507752
 H   *d2+     0.00000000     0.10208953     0.00000000     0.00000000
 F   1s      -0.88684335     0.00000000     0.04569588    -2.87391450
 F   2s      -1.73931642     0.00000000     0.23364813    -6.78966409
 F   *s       1.23204144     0.00000000     0.12102248     3.70020978
 F   *s       1.99941147     0.00000000    -1.49601913     1.94799837
 F   2pz      0.83806621     0.00000000    -0.66250955    -1.02457200
 F   *pz     -1.30191918     0.00000000     0.88493714     2.57070806
 F   *pz     -0.58774199     0.00000000     1.00295927     0.85604238
 F   *d0      0.40686209     0.00000000     1.13312084    -0.23745261
 F   *d0      0.41537263     0.00000000    -1.18195320    -0.85904294
 F   *d2+     0.00000000     1.15881708     0.00000000     0.00000000
 F   *d2+     0.00000000    -0.60210523     0.00000000     0.00000000
 F   *f0     -0.92425194     0.00000000     0.50860191     0.55268178
 F   *f2+     0.00000000     0.09136286     0.00000000     0.00000000

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     1.99977768     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 H   *px      0.01116998     0.08600841     0.03719484     0.16472825     1.18449282     0.30327151     0.30103668     0.00000000
 H   *px      0.03098356    -1.03070543    -0.84432568    -0.70529610    -0.88004546     0.53515083     0.12922679     0.00000000
 H   *d1+     0.00640463    -0.00707985    -0.06039766     0.08957086    -0.42962446     1.19603940     0.60637638     0.00000000
 F   2px      0.96157316     0.43950411    -0.47728126    -0.10918187    -0.31458454    -1.22565754     2.79195656     0.00000000
 F   *px     -0.00141488     0.22679405    -0.34606384    -0.00241319     0.36283730     0.70726404    -2.88899132     0.00000000
 F   *px      0.02928627    -0.25985789     1.60183199     0.44786293     0.34623053    -0.15458276    -0.53192032     0.00000000
 F   *d1+    -0.00361824     0.01353071     0.01917841    -0.00047442    -0.00452279    -0.04004426    -0.02454879     0.00000000
 F   *d1+    -0.01238848    -0.00929212    -0.02828927    -1.03819859    -0.00984109     0.94134423     0.45422898     0.00000000
 F   *f1+     0.00281233     0.00200102     0.00975388    -0.00502409     0.10083322    -0.00493147     0.12126262     0.00000000
 F   *f3+     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000

               MO    9        MO   10

  occ(*)=     0.00000000     0.00000000

 H   *px     -0.44098011    -0.33363554
 H   *px     -0.07178406    -0.37325914
 H   *d1+    -0.57598991    -0.50369071
 F   2px     -0.44510954    -0.29267293
 F   *px      0.64619693     0.46514532
 F   *px      0.18420189     0.32652777
 F   *d1+    -0.55660076     1.05338577
 F   *d1+    -0.17140777    -1.02770625
 F   *f1+     0.96154683     0.63195128
 F   *f3+     0.00000000     0.00000000

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000

 H   *d2-     0.17138401    -1.00532009     0.24660452     0.10208953
 F   *d2-    -0.00980538     0.00189971    -0.08675263     1.15881708
 F   *d2-     0.94942339     0.40919724    -0.01811962    -0.60210523
 F   *f2-    -0.00570723     0.04482843     1.01522554     0.09136286

          natural orbitals of the final iteration, block  4

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     1.99977768     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 H   *py      0.01116998     0.08600841     0.03719484     0.16472825     1.18449282     0.30327151     0.30103668     0.00000000
 H   *py      0.03098356    -1.03070543    -0.84432568    -0.70529610    -0.88004546     0.53515083     0.12922679     0.00000000
 H   *d1-     0.00640463    -0.00707985    -0.06039766     0.08957086    -0.42962446     1.19603940     0.60637638     0.00000000
 F   2py      0.96157316     0.43950411    -0.47728126    -0.10918187    -0.31458454    -1.22565754     2.79195656     0.00000000
 F   *py     -0.00141488     0.22679405    -0.34606384    -0.00241319     0.36283730     0.70726404    -2.88899132     0.00000000
 F   *py      0.02928627    -0.25985789     1.60183199     0.44786293     0.34623053    -0.15458276    -0.53192032     0.00000000
 F   *d1-    -0.00361824     0.01353071     0.01917841    -0.00047442    -0.00452279    -0.04004426    -0.02454879     0.00000000
 F   *d1-    -0.01238848    -0.00929212    -0.02828927    -1.03819859    -0.00984109     0.94134423     0.45422898     0.00000000
 F   *f3-     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 F   *f1-     0.00281233     0.00200102     0.00975388    -0.00502409     0.10083322    -0.00493147     0.12126262     0.00000000

               MO    9        MO   10

  occ(*)=     0.00000000     0.00000000

 H   *py     -0.44098011    -0.33363554
 H   *py     -0.07178406    -0.37325914
 H   *d1-    -0.57598991    -0.50369071
 F   2py     -0.44510954    -0.29267293
 F   *py      0.64619693     0.46514532
 F   *py      0.18420189     0.32652777
 F   *d1-    -0.55660076     1.05338577
 F   *d1-    -0.17140777    -1.02770625
 F   *f3-     0.00000000     0.00000000
 F   *f1-     0.96154683     0.63195128
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
         65 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
    H _ s       0.000218  -0.025801   0.591173   0.013155   0.000000   0.000000
    H _ p      -0.000009   0.007121   0.017777  -0.001137   0.000000   0.000000
    H _ d      -0.000001   0.000872   0.001075  -0.000036   0.000000   0.000000
    F _ s       1.999088   1.905193   0.006896   0.000359   0.000000   0.000000
    F _ p       0.000704   0.112411   1.355319   0.007856   0.000000   0.000000
    F _ d       0.000000   0.000006   0.007839  -0.000012   0.000000   0.000000
    F _ f       0.000000  -0.000001   0.000412  -0.000030   0.000000   0.000000

   ao class       7a1        8a1        9a1       10a1       11a1       12a1 

   ao class      13a1       14a1       15a1       16a1       17a1       18a1 

   ao class      19a1       20a1 

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
    H _ p       0.033356   0.000000   0.000000   0.000000   0.000000   0.000000
    H _ d       0.004239   0.000000   0.000000   0.000000   0.000000   0.000000
    F _ p       1.961213   0.000000   0.000000   0.000000   0.000000   0.000000
    F _ d       0.000917   0.000000   0.000000   0.000000   0.000000   0.000000
    F _ f       0.000052   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7b1        8b1        9b1       10b1 

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
    H _ p       0.033356   0.000000   0.000000   0.000000   0.000000   0.000000
    H _ d       0.004239   0.000000   0.000000   0.000000   0.000000   0.000000
    F _ p       1.961213   0.000000   0.000000   0.000000   0.000000   0.000000
    F _ d       0.000917   0.000000   0.000000   0.000000   0.000000   0.000000
    F _ f       0.000052   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7b2        8b2        9b2       10b2 


                        gross atomic populations
     ao           H _        F _
      s         0.578744   3.911535
      p         0.090464   5.398716
      d         0.010388   0.009667
      f         0.000000   0.000486
    total       0.679595   9.320405


 Total number of electrons:   10.00000000

 !timer: mcscf                           user+sys=     0.320 walltime=     1.000
