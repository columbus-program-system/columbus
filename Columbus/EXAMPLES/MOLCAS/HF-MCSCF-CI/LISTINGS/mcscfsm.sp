 total ao core energy =    5.220720459
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   8
 Total number of CSFs:         8

 Number of active-double rotations:         3
 Number of active-active rotations:         0
 Number of double-virtual rotations:       16
 Number of active-virtual rotations:       66

 iter=    1 emc=   -100.0591694495 demc= 1.0006E+02 wnorm= 1.0124E-02 knorm= 9.2285E-01 apxde= 8.8234E-03    *not converged* 
 iter=    2 emc=   -100.0781938849 demc= 1.9024E-02 wnorm= 6.3657E-02 knorm= 2.2961E-01 apxde= 2.9085E-03    *not converged* 
 iter=    3 emc=   -100.0819671304 demc= 3.7732E-03 wnorm= 2.4188E-02 knorm= 6.7087E-02 apxde= 2.2298E-04    *not converged* 
 iter=    4 emc=   -100.0822718836 demc= 3.0475E-04 wnorm= 1.5233E-02 knorm= 2.1402E-02 apxde= 3.2693E-05    *not converged* 
 iter=    5 emc=   -100.0823188013 demc= 4.6918E-05 wnorm= 7.8451E-03 knorm= 1.5906E-02 apxde= 1.1728E-05    *not converged* 
 iter=    6 emc=   -100.0823305297 demc= 1.1728E-05 wnorm= 4.3020E-04 knorm= 1.4915E-03 apxde= 1.5092E-08    *not converged* 
 iter=    7 emc=   -100.0823305449 demc= 1.5179E-08 wnorm= 1.8930E-06 knorm= 2.0796E-08 apxde= 2.4180E-14    *not converged* 

 final mcscf convergence values:
 iter=    8 emc=   -100.0823305449 demc= 2.8422E-14 wnorm= 6.9885E-07 knorm= 3.1293E-08 apxde= 1.0610E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=     -100.082330545
   ------------------------------------------------------------


