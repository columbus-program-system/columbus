
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 mcdrtky                                                                         

 input the spin multiplicity [  0]: spin multiplicity:    1    singlet 
 input the total number of electrons [  0]: nelt:     10
 input the number of irreps (1-8) [  0]: nsym:      4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1

 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:      1
 number of inactive electrons:      2
 number of active electrons:      8
 level(*)        1
 symd(*)         1
 slabel(*)     a1 
 doub(*)         1

 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      5
 level(*)        1   2   3   4   5
 syml(*)         1   1   1   2   4
 slabel(*)     a1  a1  a1  b1  b2 
 modrt(*)        2   3   4   1   1
 input the minimum cumulative occupation for each active level:
  a1  a1  a1  b1  b2 
    2   3   4   1   1
 input the maximum cumulative occupation for each active level:
  a1  a1  a1  b1  b2 
    2   3   4   1   1
 slabel(*)     a1  a1  a1  b1  b2 
 modrt(*)        2   3   4   1   1
 occmin(*)       0   0   0   0   8
 occmax(*)       8   8   8   8   8
 input the minimum b value for each active level:
  a1  a1  a1  b1  b2 
    2   3   4   1   1
 input the maximum b value for each active level:
  a1  a1  a1  b1  b2 
    2   3   4   1   1
 slabel(*)     a1  a1  a1  b1  b2 
 modrt(*)        2   3   4   1   1
 bmin(*)         0   0   0   0   0
 bmax(*)         8   8   8   8   8
 input the step masks for each active level:
 modrt:smask=
   2:1111   3:1111   4:1111   1:1111   1:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     14
 are any arcs to be manually removed?(y,[n])
 nwalk=       8
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   5

 level  0 through level  5 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  14   5 4 0   4 b2    1    0   0   0   0   1     1     0     0     0     8     0
                                            2     0     0     0     0     3     0
                                            3     0     0     0     0     1     0
                                            4     0     0     0     0     3     0
 ........................................

  11   4 4 0   2 b1    1   14   0   0   0   1     1     0     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  12   4 3 1   2 b1    1    0   0  14   0   1     0     0     0     0     3     0
                                            2     0     0     0     0     1     0
                                            3     0     0     0     0     0     0
                                            4     1     1     1     0     0     1

  13   4 3 0   2 b1    1    0   0   0  14   1     1     1     1     1     7     1
                                            2     0     0     0     0     3     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

   8   3 3 0   1 a1    4   13  12   0  11   1     2     1     1     1     1     0
                                            2     0     0     0     0     0     0
                                            3     1     1     0     0     0     1
                                            4     0     0     0     0     0     0

   9   3 2 1   1 a1    4    0   0  13  12   1     0     0     0     0     3     0
                                            2     1     1     1     0     0     2
                                            3     0     0     0     0     0     0
                                            4     1     1     1     1     0     1

  10   3 2 0   1 a1    4    0   0   0  13   1     1     1     1     1     6     2
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

   5   2 2 0   1 a1    3   10   9   0   8   1     3     2     2     2     1     0
                                            2     1     1     0     0     0     2
                                            3     1     1     1     1     0     1
                                            4     1     1     0     0     0     1

   6   2 1 1   1 a1    3    0   0  10   9   1     1     1     1     0     2     3
                                            2     1     1     1     1     0     2
                                            3     0     0     0     0     0     0
                                            4     1     1     1     1     0     1

   7   2 1 0   1 a1    3    0   0   0  10   1     1     1     1     1     3     5
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

   2   1 1 0   1 a1    2    7   6   0   5   1     5     4     3     3     1     0
                                            2     2     2     1     1     0     2
                                            3     1     1     1     1     0     1
                                            4     2     2     1     1     0     1

   3   1 0 1   1 a1    2    0   0   7   6   1     2     2     2     1     1     4
                                            2     1     1     1     1     0     2
                                            3     0     0     0     0     0     0
                                            4     1     1     1     1     0     1

   4   1 0 0   1 a1    2    0   0   0   7   1     1     1     1     1     1     7
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

   1   0 0 0   0       0    4   3   0   2   1     8     7     5     5     1     0
                                            2     3     3     2     2     0     2
                                            3     1     1     1     1     0     1
                                            4     3     3     2     2     0     1
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=       8
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
        8 csfs selected from       8 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
        8 csfs selected from       8 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
        8 csfs selected from       8 total walks.
  drt construction and csf selection complete.

 input a title card, default=mdrt2_title
  title                                                                          
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 mcdrtfl                                                                         

 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 3 3 3 0
   CSF#     2    3 3 3 0 3
   CSF#     3    3 3 0 3 3
   CSF#     4    3 1 2 3 3
   CSF#     5    3 0 3 3 3
   CSF#     6    1 3 2 3 3
   CSF#     7    1 2 3 3 3
   CSF#     8    0 3 3 3 3
