
 Work memory size (LMWORK) :   484966400 =    0.00 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 2 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    9



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 1



                      SYMGRP:Point group information
                      ------------------------------

Point group: Cs 

   * The point group was generated by:

      Reflection in the xy-plane

   * Group multiplication table

        |  E   Oxy
   -----+----------
     E  |  E 
    Oxy | Oxy   E 

   * Character table

        |  E   Oxy
   -----+----------
    A'  |   1    1
    A"  |   1   -1

   * Direct product table

        | A'   A" 
   -----+----------
    A'  | A' 
    A"  | A"   A' 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  B  1        1       5      26      14      [9s4p1d|3s2p1d]                        
  H  1        1       1       7       5      [4s1p|2s1p]                            
  H  2        1       1       7       5      [4s1p|2s1p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3       7      40      24

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   B  1     x     -0.3363944600
   2            y      0.0000000000
   3            z      0.0000000000

   4   H  1     x      1.8373582000
   5            y      1.4265897800
   6            z      0.0000000000

   7   H  2     x      1.8373582000
   8            y     -1.4265897800
   9            z      0.0000000000



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   6  3


  Symmetry 1

   1   B  1  x    1
   2   B  1  y    2
   3   H  1  x    4
   4   H  1  y    5
   5   H  2  x    7
   6   H  2  y    8


  Symmetry 2

   7   B  1  z    3
   8   H  1  z    6
   9   H  2  z    9


   Interatomic separations (in Angstroms):
   ---------------------------------------

            B  1        H  1        H  2

   B  1    0.000000
   H  1    1.375897    0.000000
   H  2    1.375897    1.509837    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       B  1                           1.375897
  bond distance:    H  2       B  1                           1.375897


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       B  1       H  1                  66.552


  Nuclear repulsion energy :    4.196537892870


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  B  1   1s    1     4570.000000    0.0007 -0.0001  0.0000
   gen. cont.  2      685.900000    0.0054 -0.0011  0.0000
               3      156.500000    0.0271 -0.0054  0.0000
               4       44.470000    0.1014 -0.0219  0.0000
               5       14.480000    0.2721 -0.0598  0.0000
               6        5.131000    0.4484 -0.1387  0.0000
               7        1.898000    0.2901 -0.1315  0.0000
               8        0.332900    0.0143  0.5395  0.0000
               9        0.104300   -0.0035  0.5808  1.0000

  B  1   2px  10        6.001000    0.0355  0.0000
   gen. cont. 11        1.241000    0.1981  0.0000
              12        0.336400    0.5052  0.0000
              13        0.095380    0.4795  1.0000

  B  1   2py  14        6.001000    0.0355  0.0000
   gen. cont. 15        1.241000    0.1981  0.0000
              16        0.336400    0.5052  0.0000
              17        0.095380    0.4795  1.0000

  B  1   2pz  18        6.001000    0.0355  0.0000
   gen. cont. 19        1.241000    0.1981  0.0000
              20        0.336400    0.5052  0.0000
              21        0.095380    0.4795  1.0000

  B  1   3d2- 22        0.343000    1.0000

  B  1   3d1- 23        0.343000    1.0000

  B  1   3d0  24        0.343000    1.0000

  B  1   3d1+ 25        0.343000    1.0000

  B  1   3d2+ 26        0.343000    1.0000

  H  1   1s   27       13.010000    0.0197  0.0000
   gen. cont. 28        1.962000    0.1380  0.0000
              29        0.444600    0.4781  0.0000
              30        0.122000    0.5012  1.0000

  H  1   2px  31        0.727000    1.0000

  H  1   2py  32        0.727000    1.0000

  H  1   2pz  33        0.727000    1.0000

  H  2   1s   34       13.010000    0.0197  0.0000
   gen. cont. 35        1.962000    0.1380  0.0000
              36        0.444600    0.4781  0.0000
              37        0.122000    0.5012  1.0000

  H  2   2px  38        0.727000    1.0000

  H  2   2py  39        0.727000    1.0000

  H  2   2pz  40        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  B  1    1s       1     2     3     4     5     6     7     8     9
   2  B  1    1s       1     2     3     4     5     6     7     8     9
   3  B  1    1s       9
   4  B  1    2px     10    11    12    13
   5  B  1    2py     14    15    16    17
   6  B  1    2pz     18    19    20    21
   7  B  1    2px     13
   8  B  1    2py     17
   9  B  1    2pz     21
  10  B  1    3d2-    22
  11  B  1    3d1-    23
  12  B  1    3d0     24
  13  B  1    3d1+    25
  14  B  1    3d2+    26
  15  H  1    1s      27    28    29    30
  16  H  1    1s      30
  17  H  1    2px     31
  18  H  1    2py     32
  19  H  1    2pz     33
  20  H  2    1s      34    35    36    37
  21  H  2    1s      37
  22  H  2    2px     38
  23  H  2    2py     39
  24  H  2    2pz     40




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        18  6


  Symmetry  A' ( 1)

    1     B  1     1s         1
    2     B  1     1s         2
    3     B  1     1s         3
    4     B  1     2px        4
    5     B  1     2py        5
    6     B  1     2px        7
    7     B  1     2py        8
    8     B  1     3d2-      10
    9     B  1     3d0       12
   10     B  1     3d2+      14
   11     H  1     1s        15
   12     H  1     1s        16
   13     H  1     2px       17
   14     H  1     2py       18
   15     H  2     1s        20
   16     H  2     1s        21
   17     H  2     2px       22
   18     H  2     2py       23


  Symmetry  A" ( 2)

   19     B  1     2pz        6
   20     B  1     2pz        9
   21     B  1     3d1-      11
   22     B  1     3d1+      13
   23     H  1     2pz       19
   24     H  2     2pz       24

  Symmetries of electric field:  A' (1)  A' (1)  A" (2)

  Symmetries of magnetic field:  A" (2)  A" (2)  A' (1)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    1 Z         0.10D-14                                                   
       5.0    1    3    1    1    1                                             
B  1  -0.336394460000000   0.000000000000000   0.000000000000000       *        
H   9   3                                                                       
       4570.00000000         0.00069600        -0.00013900         0.00000000   
        685.90000000         0.00535300        -0.00109700         0.00000000   
        156.50000000         0.02713400        -0.00544400         0.00000000   
         44.47000000         0.10138000        -0.02191600         0.00000000   
         14.48000000         0.27205500        -0.05975100         0.00000000   
          5.13100000         0.44840300        -0.13873200         0.00000000   
          1.89800000         0.29012300        -0.13148200         0.00000000   
          0.33290000         0.01432200         0.53952600         0.00000000   
          0.10430000        -0.00348600         0.58077400         1.00000000   
H   4   2                                                                       
          6.00100000         0.03548100         0.00000000                      
          1.24100000         0.19807200         0.00000000                      
          0.33640000         0.50523000         0.00000000                      
          0.09538000         0.47949900         1.00000000                      
H   1   1                                                                       
          0.34300000         1.00000000                                         
       1.0    2    2    1    1                                                  
H  1   1.837358200000000   1.426589780000000   0.000000000000000       *        
H  2   1.837358200000000  -1.426589780000000   0.000000000000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

found     131 non-vanashing overlap integrals
found     168 non-vanashing nuclear attraction integrals
found     131 non-vanashing kinetic energy integrals






 found     155 non-vanashing integrals ( typea=  1 typeb=  0)
 found     124 non-vanashing integrals ( typea=  1 typeb=  1)
 found      74 non-vanashing integrals ( typea=  1 typeb=  2)


 found     162 non-vanashing integrals ( typea=  1 typeb=  3)
 found     145 non-vanashing integrals ( typea=  1 typeb=  4)
 found      88 non-vanashing integrals ( typea=  1 typeb=  5)
 found     142 non-vanashing integrals ( typea=  1 typeb=  6)
 found      74 non-vanashing integrals ( typea=  1 typeb=  7)
 found     134 non-vanashing integrals ( typea=  1 typeb=  8)


 found      61 non-vanashing integrals ( typea=  2 typeb=  6)
 found      85 non-vanashing integrals ( typea=  2 typeb=  7)
 found     135 non-vanashing integrals ( typea=  2 typeb=  8)




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:     21669 (48.0%)
 Kilobytes written:                              350




 >>>> Total CPU  time used in HERMIT:   0.03 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
