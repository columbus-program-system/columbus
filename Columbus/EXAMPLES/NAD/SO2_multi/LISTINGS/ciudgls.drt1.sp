1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       290       18060           0           0       18350
      internal walks       290         645           0           0         935
valid internal walks       290         645           0           0         935
  MR-CIS calculation - skip 3- and 4-external 2e integrals
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1561 ci%nnlev=                   780  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             301427767             301404821
 lencor,maxblo             301465600                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================
 Orig.  diagonal integrals:  1electron:        39
                             0ext.    :       132
                             2ext.    :       616
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :    133056
                             2ext.    :     80388
                             1ext.    :     24024
                             0ext.    :      2805
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :         0 x :         0
                             4ext.  w :         0 x :         0


Cycle #  1 sortfile size=    589806(      18 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 301427767
Cycle #  2 sortfile size=    589806(      18 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 301427767
 minimum size of srtscr:    524272 WP (    16 records)
 maximum size of srtscr:    589806 WP (    18 records)
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 18
  NROOT = 3
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 3
  RTOLBK = 1e-4,1e-4,1e-4,
  NITER = 90
  NVCIMN = 5
  RTOLCI = 1e-4,1e-4,1e-4,
  NVCIMX = 8
  NVRFMX = 8
  NVBKMX = 8
   iden=2
  CSFPRN = 10,
 /&end
 transition
   1  1  1  2
   1  1  1  3
   1  2  1  3
 ------------------------------------------------------------------------
transition densities: resetting nroot to    3
lodens (list->root)=  1  2  3
invlodens (root->list)=  1  2  3
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    3      noldv  =   0      noldhv =   0
 nunitv =    3      nbkitr =    1      niter  =  90      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    8      ibktv  =  -1      ibkthv =  -1
 nvcimx =    8      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    3      nvcimn =    5      maxseg =   4      nrfitr =  30
 ncorel =   18      nvrfmx =    8      nvrfmn =   5      iden   =   2
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 Computing density:                    .drt1.state2
 Computing density:                    .drt1.state3
 Computing transition density:          drt1.state1-> drt1.state2 (.trd1to2)
 Computing transition density:          drt1.state1-> drt1.state3 (.trd1to3)
 Computing transition density:          drt1.state2-> drt1.state3 (.trd2to3)
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  3830                108772
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          301465599 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 301465599

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =   97.813233246                                          
 MCSCF energy =    -547.111981947                                                
 SIFS file created by program tran.      r24n12            15:26:09.272 10-Sep-12

 input energy(*) values:
 energy( 1)=  9.781323324553E+01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   9.781323324553E+01

 nsym = 1 nmot=  39

 symmetry  =    1
 slabel(*) =  A  
 nmpsy(*)  =   39

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=     766
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  46 nfctd =   7 nfvtc =   0 nmot  =  39
 nlevel =  39 niot  =  11 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  -1  -1  -1  29  30  31  32  33  34  35  36  37  38  39   1   2
    3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22
   23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34  35  36  37  38  39   1   2   3   4   5   6   7   8   9
   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  2.  2.  2.  2.  2.  0.  0.  0.  0.  0.
  ... transition density matrix calculation requested 
  ... setting rmu(*) to zero 

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -5.722942465718E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1560
 number with all external indices:       812
 number with half external - half internal indices:       616
 number with all internal indices:       132

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=     133056 strt=      93178
    2-external integrals: num=      80388 strt=     226234
    1-external integrals: num=      24024 strt=     306622
    0-external integrals: num=       2805 strt=     330646

 total number of off-diagonal integrals:      333450


 indxof(2nd)  ittp=   3 numx(ittp)=       80388
 indxof(2nd)  ittp=   4 numx(ittp)=       24024
 indxof(2nd)  ittp=   5 numx(ittp)=        2805

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 301290195
 pro2e        1     781    1561    2341    3121    3187    3253    4033   47721   91409
   124176  132368  137828  159667

 pro2e:    302346 integrals read in    56 records.
 pro1e        1     781    1561    2341    3121    3187    3253    4033   47721   91409
   124176  132368  137828  159667
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                    18  records of                  32767 
 WP =               4718448 Bytes
 putdg        1     781    1561    2341    3107   35874   57719    4033   47721   91409
   124176  132368  137828  159667

 putf:       5 buffers of length     766 written to file 12
 diagonal integral file completed.
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     80388         3    226234    226234     80388    333450
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     24024         4    306622    306622     24024    333450
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      2805         5    330646    330646      2805    333450

 putf:     142 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:     812 2ext:     616 0ext:     132
 fil4w,fil4x  :   93177 fil3w,fil3x :  133056
 ofdgint  2ext:   80388 1ext:   24024 0ext:    2805so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 301465599

 core energy values from the integral file:
 energy( 1)=  9.781323324553E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -5.722942465718E+02, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.744810133263E+02
 nmot  =    46 niot  =    11 nfct  =     7 nfvt  =     0
 nrow  =    88 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        935      290      645        0        0
 nvalwt,nvalw:      935      290      645        0        0
 ncsft:           18350
 total number of valid internal walks:     935
 nvalz,nvaly,nvalx,nvalw =      290     645       0       0

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   1532
 nd4ext,nd2ext,nd0ext   812   616   132
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177   133056    80388    24024     2805        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals  11
 bummer (warning):transition densities: resetting ref occupation number to 0                      0
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    11                     1
 block size     0
 pthz,pthy,pthx,pthw:   290   645     0     0 total internal walks:     935
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1 1

 setref:       50 references kept,
                0 references were marked as invalid, out of
               50 total.
 nmb.of records onel     1
 nmb.of records 2-ext   105
 nmb.of records 1-ext    32
 nmb.of records 0-ext     4
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61968
    threx             60019
    twoex              9152
    onex               2447
    allin               766
    diagon             2110
               =======
   maximum            61968
 
  __ static summary __ 
   reflst               290
   hrfspc               290
               -------
   static->             290
 
  __ core required  __ 
   totstc               290
   max n-ex           61968
               -------
   totnec->           62258
 
  __ core available __ 
   totspc         301465599
   totnec -           62258
               -------
   totvec->       301403341

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=   301403341
 reducing frespc by                  4672 to              301398669 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         290|       290|         0|       290|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         645|     18060|       290|       645|       290|         2|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=        2580DP  drtbuffer=        2088 DP

dimension of the ci-matrix ->>>     18350

 executing brd_struct for civct
 gentasklist: ntask=                     6
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1     645     290      18060        290     645     290
     2  1   1     1      allint zz    OX  1 1     290     290        290        290     290     290
     3  2   2     5      0ex2ex yy    OX  2 2     645     645      18060      18060     645     645
     4  2   2    42      four-ext y   4X  2 2     645     645      18060      18060     645     645
     5  1   1    75      dg-024ext z  OX  1 1     290     290        290        290     290     290
     6  2   2    76      dg-024ext y  OX  2 2     645     645      18060      18060     645     645
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=   5.000 N=  1 (task/type/sgbra)=(   1/11/0) (
REDTASK #   2 TIME=   4.000 N=  1 (task/type/sgbra)=(   2/ 1/0) (
REDTASK #   3 TIME=   3.000 N=  1 (task/type/sgbra)=(   3/ 5/0) (
REDTASK #   4 TIME=   2.000 N=  1 (task/type/sgbra)=(   4/42/1) (
REDTASK #   5 TIME=   1.000 N=  1 (task/type/sgbra)=(   5/75/1) (
REDTASK #   6 TIME=   0.000 N=  1 (task/type/sgbra)=(   6/76/1) (
 initializing v-file: 1:                 18350

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      3 vectors will be written to unit 11 beginning with logical record   1

            3 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       17280 2x:           0 4x:           0
All internal counts: zz :       50395 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      50
 dsyevx: computed roots 1 to    6(converged:   6)

    root           eigenvalues
    ----           ------------
       1        -547.1904446665
       2        -547.1403465662
       3        -547.0135383959
       4        -546.9776881837
       5        -546.8659346196
       6        -546.8653625187

 strefv generated    3 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   290

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   290

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   290

         vector  3 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   290)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             18350
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:           0 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:           0 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:           0 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1   -72.70943134
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000        0.00000       3.070354E-18
 refs   2    0.00000        1.00000       7.740863E-17
 refs   3    0.00000        0.00000        1.00000    

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1    1.00000      -2.949845E-13  -3.070398E-18
 civs   2  -2.948752E-13   -1.00000      -7.806256E-17
 civs   3    0.00000        0.00000        1.00000    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1    1.00000      -2.949845E-13  -4.480185E-23
 ref    2  -2.948752E-13   -1.00000      -6.539237E-19
 ref    3    0.00000        0.00000        1.00000    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000
 ref:   2     0.00000000    -1.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -547.1904446665  1.2790E-13  1.1413E-01  5.1225E-01  1.0000E-04
 mr-sdci #  1  2   -547.1403465662  1.4211E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  1  3   -547.0135383959  1.9895E-13  0.0000E+00  5.0976E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -547.1904446665  1.2790E-13  1.1413E-01  5.1225E-01  1.0000E-04
 mr-sdci #  1  2   -547.1403465662  1.4211E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  1  3   -547.0135383959  1.9895E-13  0.0000E+00  5.0976E-01  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    4 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             18350
 number of initial trial vectors:                         4
 number of initial matrix-vector products:                3
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                   90
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -72.70943134
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4    -0.11412556     0.00000000     0.00000000    -4.48221708

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000        0.00000       3.070354E-18  -2.542385E-13
 refs   2    0.00000        1.00000       7.740863E-17   1.214194E-13
 refs   3    0.00000        0.00000        1.00000      -2.695110E-15

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.977180       7.301681E-09   1.076893E-08  -0.212413    
 civs   2  -6.963760E-09    1.00000       9.434248E-14   2.339000E-09
 civs   3  -9.630528E-09  -9.433031E-14    1.00000       6.394012E-09
 civs   4   0.843748      -3.205239E-09  -1.669300E-08    3.88157    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.977180       7.301681E-09   1.076893E-08  -0.212413    
 ref    2  -6.963658E-09    1.00000       9.441989E-14   2.339471E-09
 ref    3  -9.630530E-09  -9.433031E-14    1.00000       6.394002E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97718006     0.00000001     0.00000001    -0.21241265
 ref:   2    -0.00000001     1.00000000     0.00000000     0.00000000
 ref:   3    -0.00000001     0.00000000     1.00000000     0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -547.2889865758  9.8542E-02  1.1268E-02  1.3767E-01  1.0000E-04
 mr-sdci #  1  2   -547.1403465662 -2.8422E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  1  3   -547.0135383959 -4.2633E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  1  4   -545.1049483418  7.0624E+01  0.0000E+00  1.0883E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -72.70943134
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4    -0.11412556     0.00000000     0.00000000    -4.48221708
   ht   5    -1.44581602     0.00000049    -0.00000013     0.10797542    -0.71006146

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000        0.00000       3.070354E-18  -2.542385E-13   1.988255E-02
 refs   2    0.00000        1.00000       7.740863E-17   1.214194E-13  -6.790117E-09
 refs   3    0.00000        0.00000        1.00000      -2.695110E-15   1.734851E-09

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.960942       1.096685E-08  -6.354478E-09  -0.342755       1.942152E-02
 civs   2   5.359103E-09    1.00000       1.281868E-14   6.046357E-08   4.190604E-08
 civs   3   8.214954E-09   1.323974E-14   -1.00000      -1.605166E-08  -1.860794E-08
 civs   4  -0.898586      -4.432172E-09   1.207907E-08    2.51382       -2.95404    
 civs   5  -0.614987      -3.748057E-08  -6.904057E-08    8.12867        6.15931    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.973169       1.022164E-08  -7.727181E-09  -0.181136       0.141884    
 ref    2   9.534831E-09    1.00000       1.321006E-14   5.269220E-09   8.322259E-11
 ref    3   7.148045E-09   1.317471E-14   -1.00000      -1.949623E-09  -7.922437E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.97316931     0.00000001    -0.00000001    -0.18113619     0.14188437
 ref:   2     0.00000001     1.00000000     0.00000000     0.00000001     0.00000000
 ref:   3     0.00000001     0.00000000    -1.00000000     0.00000000    -0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -547.2959292589  6.9427E-03  1.9350E-03  5.7084E-02  1.0000E-04
 mr-sdci #  2  2   -547.1403465662  2.8422E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  2  3   -547.0135383959  4.2633E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  2  4   -545.5991059473  4.9416E-01  0.0000E+00  1.3774E+00  1.0000E-04
 mr-sdci #  2  5   -544.8215937451  7.0341E+01  0.0000E+00  1.1394E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.70943134
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4    -0.11412556     0.00000000     0.00000000    -4.48221708
   ht   5    -1.44581602     0.00000049    -0.00000013     0.10797542    -0.71006146
   ht   6    -0.19252297    -0.00000009     0.00000007     0.07219570    -0.01223403    -0.10458478

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000        0.00000       3.070354E-18  -2.542385E-13   1.988255E-02   2.646398E-03
 refs   2    0.00000        1.00000       7.740863E-17   1.214194E-13  -6.790117E-09   1.209307E-09
 refs   3    0.00000        0.00000        1.00000      -2.695110E-15   1.734851E-09  -9.694556E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.957703       1.767084E-08   1.272835E-08   0.243717       0.199460       0.169611    
 civs   2  -7.985838E-09    1.00000      -5.131313E-14  -8.922616E-08   2.162129E-08  -2.684110E-08
 civs   3  -1.244993E-08   5.037872E-14    1.00000       7.277397E-09  -1.159161E-08   1.008256E-08
 civs   4   0.910454      -7.488489E-09  -2.119233E-08   -1.25697       -3.65596       0.489759    
 civs   5   0.756858      -1.134786E-07  -4.572411E-08   -6.99245       0.881800       -7.36728    
 civs   6  -0.834192       4.268108E-07   5.851701E-07    17.1463       -11.1882       -16.4097    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970544       1.654411E-08   1.336783E-08   0.150065       0.187384      -2.029561E-02
 ref    2  -1.413368E-08    1.00000      -5.021760E-14  -2.101159E-08   2.103344E-09   3.339278E-09
 ref    3  -1.032818E-08   4.976807E-14    1.00000      -2.147607E-08   7.846617E-10   1.320991E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97054402     0.00000002     0.00000001     0.15006521     0.18738418    -0.02029561
 ref:   2    -0.00000001     1.00000000     0.00000000    -0.00000002     0.00000000     0.00000000
 ref:   3    -0.00000001     0.00000000     1.00000000    -0.00000002     0.00000000     0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -547.2975443821  1.6151E-03  4.1392E-04  2.7523E-02  1.0000E-04
 mr-sdci #  3  2   -547.1403465662  0.0000E+00  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  3  3   -547.0135383959 -2.8422E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  3  4   -546.2387124108  6.3961E-01  0.0000E+00  1.0293E+00  1.0000E-04
 mr-sdci #  3  5   -544.9583239223  1.3673E-01  0.0000E+00  9.8243E-01  1.0000E-04
 mr-sdci #  3  6   -544.5818593906  7.0101E+01  0.0000E+00  1.4625E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.70943134
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4    -0.11412556     0.00000000     0.00000000    -4.48221708
   ht   5    -1.44581602     0.00000049    -0.00000013     0.10797542    -0.71006146
   ht   6    -0.19252297    -0.00000009     0.00000007     0.07219570    -0.01223403    -0.10458478
   ht   7    -0.05113623     0.00000001    -0.00000004    -0.05027243    -0.03851395     0.00346263    -0.02484418

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000        0.00000       3.070354E-18  -2.542385E-13   1.988255E-02   2.646398E-03   7.043095E-04
 refs   2    0.00000        1.00000       7.740863E-17   1.214194E-13  -6.790117E-09   1.209307E-09  -1.533712E-10
 refs   3    0.00000        0.00000        1.00000      -2.695110E-15   1.734851E-09  -9.694556E-10   6.031493E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.954627       2.982024E-08   2.760841E-08   0.212251       0.251297       6.483835E-03   0.161124    
 civs   2  -1.139055E-08    1.00000       1.541992E-13  -1.163574E-07  -1.027762E-08  -4.995202E-08  -2.228257E-08
 civs   3  -1.552964E-08  -1.617271E-13    1.00000      -4.911187E-08  -1.044064E-08   4.230081E-09   1.048106E-08
 civs   4   0.912829      -1.354527E-08  -4.093316E-08  -0.615887       -3.68858        1.17410       0.527683    
 civs   5   0.793967      -2.187741E-07  -2.397345E-07   -4.28172       -3.35213       -6.28600       -6.70498    
 civs   6   -1.05391       9.526332E-07   1.419241E-06    14.2992       -3.60007        12.9619       -17.4629    
 civs   7    1.03076      -2.086710E-06  -2.801645E-06   -29.3408        27.5350        39.8037       -5.21319    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.968350       2.652181E-08   2.462453E-08   0.144296       0.194514      -5.616145E-02  -2.207360E-02
 ref    2  -1.821415E-08    1.00000       1.580504E-13  -6.549198E-08   3.906643E-09   2.300977E-09   2.926699E-09
 ref    3  -1.250881E-08  -1.642888E-13    1.00000      -8.809930E-08   3.841769E-09   4.766368E-09   1.263407E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96834995     0.00000003     0.00000002     0.14429555     0.19451433    -0.05616145    -0.02207360
 ref:   2    -0.00000002     1.00000000     0.00000000    -0.00000007     0.00000000     0.00000000     0.00000000
 ref:   3    -0.00000001     0.00000000     1.00000000    -0.00000009     0.00000000     0.00000000     0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -547.2979711503  4.2677E-04  1.2639E-04  1.4843E-02  1.0000E-04
 mr-sdci #  4  2   -547.1403465662 -5.6843E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  4  3   -547.0135383959  2.8422E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  4  4   -546.7237535920  4.8504E-01  0.0000E+00  6.9055E-01  1.0000E-04
 mr-sdci #  4  5   -545.0110264255  5.2703E-02  0.0000E+00  1.1496E+00  1.0000E-04
 mr-sdci #  4  6   -544.8512928741  2.6943E-01  0.0000E+00  1.0884E+00  1.0000E-04
 mr-sdci #  4  7   -544.5784829728  7.0097E+01  0.0000E+00  1.4745E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.010000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.70943134
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4    -0.11412556     0.00000000     0.00000000    -4.48221708
   ht   5    -1.44581602     0.00000049    -0.00000013     0.10797542    -0.71006146
   ht   6    -0.19252297    -0.00000009     0.00000007     0.07219570    -0.01223403    -0.10458478
   ht   7    -0.05113623     0.00000001    -0.00000004    -0.05027243    -0.03851395     0.00346263    -0.02484418
   ht   8     0.09107691     0.00000008    -0.00000003    -0.01321996    -0.00858289     0.00686630    -0.00272605    -0.00755629

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000        0.00000       3.070354E-18  -2.542385E-13   1.988255E-02   2.646398E-03   7.043095E-04  -1.252344E-03
 refs   2    0.00000        1.00000       7.740863E-17   1.214194E-13  -6.790117E-09   1.209307E-09  -1.533712E-10  -1.145725E-09
 refs   3    0.00000        0.00000        1.00000      -2.695110E-15   1.734851E-09  -9.694556E-10   6.031493E-10   3.534104E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.954441       3.827732E-08   4.580514E-08   0.148453       0.300987       0.139949       0.149687       1.549826E-02
 civs   2  -1.289362E-08    1.00000      -7.150506E-14  -2.181256E-07   2.599047E-08   1.459258E-08   1.618493E-09  -1.015982E-07
 civs   3  -1.688679E-08   2.655404E-14    1.00000      -1.935168E-07  -1.022164E-08  -8.849192E-09   8.680471E-09   1.091942E-08
 civs   4   0.913119      -2.168858E-08  -7.869910E-08  -0.374263       -1.81798       -3.39867       0.665910      -0.472617    
 civs   5   0.803096      -3.442313E-07  -5.919010E-07   -2.99376       -7.03702        2.52001       -5.14023       -4.92703    
 civs   6   -1.10901       1.545425E-06   2.861764E-06    11.2377        10.3922       -12.3679       -17.7105       -5.24384    
 civs   7    1.29022      -4.246695E-06  -7.163605E-06   -29.3641        19.4850       -5.10914       -18.0602        41.2237    
 civs   8   0.850839      -5.189487E-06  -7.394038E-06   -31.9701        65.6181       -14.6276        13.1655       -69.7637    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.967316       3.903097E-08   4.582449E-08   0.138025       0.120122       0.172043      -2.859059E-02   2.006133E-02
 ref    2  -2.086047E-08    1.00000      -5.437761E-14  -1.430754E-07   8.171012E-09   6.726991E-11   2.789732E-09  -8.770868E-10
 ref    3  -1.333951E-08   2.006322E-14    1.00000      -2.386144E-07   2.437938E-09  -7.383678E-10   1.069230E-08   7.664229E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96731640     0.00000004     0.00000005     0.13802535     0.12012222     0.17204288    -0.02859059     0.02006133
 ref:   2    -0.00000002     1.00000000     0.00000000    -0.00000014     0.00000001     0.00000000     0.00000000     0.00000000
 ref:   3    -0.00000001     0.00000000     1.00000000    -0.00000024     0.00000000     0.00000000     0.00000001     0.00000001

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -547.2980786941  1.0754E-04  4.1406E-05  8.1112E-03  1.0000E-04
 mr-sdci #  5  2   -547.1403465662  4.2633E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  5  3   -547.0135383959  8.5265E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  5  4   -546.9061480325  1.8239E-01  0.0000E+00  4.8186E-01  1.0000E-04
 mr-sdci #  5  5   -545.4397908365  4.2876E-01  0.0000E+00  1.1820E+00  1.0000E-04
 mr-sdci #  5  6   -544.9346894101  8.3397E-02  0.0000E+00  9.1335E-01  1.0000E-04
 mr-sdci #  5  7   -544.5898109285  1.1328E-02  0.0000E+00  1.4733E+00  1.0000E-04
 mr-sdci #  5  8   -544.3504183350  6.9869E+01  0.0000E+00  1.3726E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81706537
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4     0.00000000     0.00000000     0.00000000   -72.42513471
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -70.95877751
   ht   6    -0.09974094    -0.00000001     0.00000001     0.11740882    -0.02280480    -0.00314889

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.967316       3.903097E-08   4.582449E-08   0.138025       0.120122       1.249585E-03
 refs   2  -2.086047E-08    1.00000      -5.437761E-14  -1.430754E-07   8.171012E-09   3.721923E-10
 refs   3  -1.333951E-08   2.006322E-14    1.00000      -2.386144E-07   2.437938E-09   1.725218E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.998946       2.085033E-08   1.041244E-07  -5.976540E-02   0.142770      -0.159384    
 civs   2  -2.536502E-09    1.00000      -3.297766E-13   1.338394E-07  -2.414322E-08   5.283406E-09
 civs   3  -1.369339E-09   1.242503E-13    1.00000       1.517366E-06  -7.064877E-08   5.043457E-08
 civs   4  -3.130866E-03   1.024707E-07   1.331249E-06  -0.901358      -0.394457       0.309308    
 civs   5   1.180259E-03  -2.217720E-08  -1.304316E-07   7.604532E-02  -0.702173      -0.709260    
 civs   6   0.754165      -1.277839E-05  -6.874286E-05    39.6462       -102.040        114.982    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.966949       5.471173E-08   2.287240E-07  -0.123546      -0.128195      -5.300035E-02
 ref    2  -2.263669E-08    1.00000      -6.034466E-13   2.794257E-07  -1.440017E-08   1.353951E-09
 ref    3  -1.381473E-08   1.173258E-13    1.00000       1.740265E-06   2.253998E-09  -3.136957E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96694868     0.00000005     0.00000023    -0.12354625    -0.12819495    -0.05300035
 ref:   2    -0.00000002     1.00000000     0.00000000     0.00000028    -0.00000001     0.00000000
 ref:   3    -0.00000001     0.00000000     1.00000000     0.00000174     0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -547.2981099217  3.1228E-05  1.9131E-05  5.6876E-03  1.0000E-04
 mr-sdci #  6  2   -547.1403465662 -5.6843E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  6  3   -547.0135383959 -1.2790E-13  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  6  4   -546.9986631738  9.2515E-02  0.0000E+00  3.7641E-01  1.0000E-04
 mr-sdci #  6  5   -545.9172465740  4.7746E-01  0.0000E+00  1.0488E+00  1.0000E-04
 mr-sdci #  6  6   -544.8603329319 -7.4356E-02  0.0000E+00  1.4147E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81706537
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4     0.00000000     0.00000000     0.00000000   -72.42513471
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -70.95877751
   ht   6    -0.09974094    -0.00000001     0.00000001     0.11740882    -0.02280480    -0.00314889
   ht   7     0.08712642     0.00000001     0.00000001     0.03635132    -0.04578106     0.00042488    -0.00122023

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.967316       3.903097E-08   4.582449E-08   0.138025       0.120122       1.249585E-03  -1.292816E-03
 refs   2  -2.086047E-08    1.00000      -5.437761E-14  -1.430754E-07   8.171012E-09   3.721923E-10  -7.572314E-11
 refs   3  -1.333951E-08   2.006322E-14    1.00000      -2.386144E-07   2.437938E-09   1.725218E-10  -4.869494E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.999378      -1.689995E-08   3.216746E-02   3.002720E-08  -8.407233E-05  -5.120182E-02   0.339323    
 civs   2  -4.095838E-09   -1.00000      -3.210483E-07  -4.724172E-14   7.380476E-08  -3.297688E-08   3.785876E-08
 civs   3  -2.077033E-09  -3.058999E-13   1.129368E-06   -1.00000       1.192745E-07  -5.574685E-08  -4.833920E-09
 civs   4  -4.377148E-03  -2.144426E-07   0.814733       9.909656E-07   0.587042      -0.278271      -0.119020    
 civs   5   1.710388E-03   4.750553E-08  -0.103320      -9.854596E-08   0.491795       0.882961       2.816555E-02
 civs   6    1.13198       2.768797E-05   -55.2848      -5.139484E-05    103.774       -76.7644       -82.6668    
 civs   7   0.817784       2.363385E-05   -43.8702      -3.996654E-05    122.088       -131.756        187.884    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.966674      -7.522635E-08   0.118792       9.560926E-08   0.111858       9.253872E-02  -3.101071E-02
 ref    2  -2.394371E-08   -1.00000      -4.563864E-07  -1.521809E-13   2.321279E-08  -3.474484E-09   3.044173E-09
 ref    3  -1.416832E-08  -2.697911E-13   9.249554E-07   -1.00000      -2.293164E-09   8.863555E-10   3.931445E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96667360    -0.00000008     0.11879200     0.00000010     0.11185754     0.09253872    -0.03101071
 ref:   2    -0.00000002    -1.00000000    -0.00000046     0.00000000     0.00000002     0.00000000     0.00000000
 ref:   3    -0.00000001     0.00000000     0.00000092    -1.00000000     0.00000000     0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -547.2981255673  1.5646E-05  5.6544E-06  3.0666E-03  1.0000E-04
 mr-sdci #  7  2   -547.1403465662 -5.6843E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  7  3   -547.0427069232  2.9169E-02  0.0000E+00  2.8595E-01  1.0000E-04
 mr-sdci #  7  4   -547.0135383959  1.4875E-02  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  7  5   -546.2446448253  3.2740E-01  0.0000E+00  8.2363E-01  1.0000E-04
 mr-sdci #  7  6   -544.9283856550  6.8053E-02  0.0000E+00  1.2152E+00  1.0000E-04
 mr-sdci #  7  7   -544.7279929253  1.3818E-01  0.0000E+00  1.5369E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81706537
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.53252507
   ht   4     0.00000000     0.00000000     0.00000000   -72.42513471
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -70.95877751
   ht   6    -0.09974094    -0.00000001     0.00000001     0.11740882    -0.02280480    -0.00314889
   ht   7     0.08712642     0.00000001     0.00000001     0.03635132    -0.04578106     0.00042488    -0.00122023
   ht   8    -0.01197986     0.00000002     0.00000000     0.02795156    -0.02200319    -0.00045004     0.00006361    -0.00041003

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.967316       3.903097E-08   4.582449E-08   0.138025       0.120122       1.249585E-03  -1.292816E-03   1.369309E-04
 refs   2  -2.086047E-08    1.00000      -5.437761E-14  -1.430754E-07   8.171012E-09   3.721923E-10  -7.572314E-11  -2.420575E-10
 refs   3  -1.333951E-08   2.006322E-14    1.00000      -2.386144E-07   2.437938E-09   1.725218E-10  -4.869494E-12   2.866510E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.999360      -2.480349E-08   3.074460E-02   1.511897E-08   1.060782E-02  -1.474941E-02  -0.333692       8.160847E-02
 civs   2  -4.581133E-09   -1.00000      -6.173678E-07  -5.995927E-14   1.535170E-07  -9.447341E-08  -2.766251E-08   8.144543E-08
 civs   3  -2.465631E-09  -3.309060E-13   6.671078E-07   -1.00000       1.613204E-07  -2.478947E-08  -1.170892E-08  -3.857286E-08
 civs   4  -4.705754E-03  -3.546654E-07   0.713422       5.880861E-07   0.726233      -9.695249E-02   2.575984E-02  -0.247907    
 civs   5   1.848463E-03   7.719836E-08  -0.109834      -5.717070E-08   0.272509       0.887429       0.177966       0.380155    
 civs   6    1.23855       4.474971E-05   -59.5163      -2.918729E-05    68.9959        16.2147        43.5061       -139.809    
 civs   7    1.04848       4.757041E-05   -61.1490      -2.822589E-05    116.729       -31.1915       -220.478       -52.1661    
 civs   8   0.780611       5.290868E-05   -69.1289      -2.845772E-05    187.867       -274.165        58.4451        300.729    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.966569      -1.010397E-07   0.110234       3.922578E-08   0.104266       0.101996       4.955273E-02   2.430631E-02
 ref    2  -2.454724E-08   -1.00000      -7.217677E-07  -9.234236E-14   2.298218E-08   1.717607E-09  -4.192095E-09  -2.560414E-09
 ref    3  -1.443829E-08  -2.568166E-13   4.842455E-07   -1.00000       5.273615E-09  -4.204654E-09  -2.715729E-09   5.173984E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96656893    -0.00000010     0.11023441     0.00000004     0.10426589     0.10199557     0.04955273     0.02430631
 ref:   2    -0.00000002    -1.00000000    -0.00000072     0.00000000     0.00000002     0.00000000     0.00000000     0.00000000
 ref:   3    -0.00000001     0.00000000     0.00000048    -1.00000000     0.00000001     0.00000000     0.00000000     0.00000001

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -547.2981299812  4.4139E-06  1.6387E-06  1.7328E-03  1.0000E-04
 mr-sdci #  8  2   -547.1403465662  8.5265E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  8  3   -547.0750333962  3.2326E-02  0.0000E+00  2.3320E-01  1.0000E-04
 mr-sdci #  8  4   -547.0135383959  7.1054E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  8  5   -546.5226734067  2.7803E-01  0.0000E+00  6.4807E-01  1.0000E-04
 mr-sdci #  8  6   -545.2833171072  3.5493E-01  0.0000E+00  1.1777E+00  1.0000E-04
 mr-sdci #  8  7   -544.7409146266  1.2922E-02  0.0000E+00  1.4869E+00  1.0000E-04
 mr-sdci #  8  8   -544.5133834488  1.6297E-01  0.0000E+00  1.2737E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711665
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.59402007
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.04166008
   ht   6     0.00024474     0.00000003    -0.00944231     0.00000000    -0.00268342    -0.00008743

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.966569      -1.010397E-07   0.110234       3.922578E-08   0.104266       1.409249E-05
 refs   2  -2.454724E-08   -1.00000      -7.217677E-07  -9.234236E-14   2.298218E-08   3.181731E-10
 refs   3  -1.443829E-08  -2.568166E-13   4.842455E-07   -1.00000       5.273615E-09   1.843964E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       5.978558E-10  -3.147697E-04  -5.448975E-11  -2.331730E-04   2.245564E-03
 civs   2  -1.027470E-09   -1.00000      -1.524163E-07   1.384017E-13  -3.501897E-08   2.939008E-07
 civs   3   5.775656E-04   1.841231E-07   -1.00503       4.630112E-08  -3.985390E-02  -5.143462E-02
 civs   4   1.648539E-10  -1.191625E-13  -4.969783E-08   -1.00000      -1.623170E-08  -2.074746E-08
 civs   5  -3.861573E-04  -5.262180E-08   4.984389E-02   1.797807E-08  -0.952071      -0.303155    
 civs   6  -0.797067      -8.537962E-05    73.2562       2.293899E-05   -245.891        870.351    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.966559       1.152244E-07  -0.104864      -3.197670E-08  -0.107352      -2.284266E-02
 ref    2   2.489541E-08    1.00000       9.022769E-07  -7.176492E-14  -3.632666E-08   1.312303E-08
 ref    3   1.453642E-08   4.632793E-13  -4.353659E-07    1.00000      -1.261900E-08   1.025829E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96655880     0.00000012    -0.10486391    -0.00000003    -0.10735242    -0.02284266
 ref:   2     0.00000002     1.00000000     0.00000090     0.00000000    -0.00000004     0.00000001
 ref:   3     0.00000001     0.00000000    -0.00000044     1.00000000    -0.00000001     0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -547.2981312873  1.3062E-06  6.6121E-07  1.0491E-03  1.0000E-04
 mr-sdci #  9  2   -547.1403465662 -9.9476E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci #  9  3   -547.0847654955  9.7321E-03  0.0000E+00  1.9174E-01  1.0000E-04
 mr-sdci #  9  4   -547.0135383959  1.4211E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci #  9  5   -546.6252102412  1.0254E-01  0.0000E+00  5.0711E-01  1.0000E-04
 mr-sdci #  9  6   -545.2584992212 -2.4818E-02  0.0000E+00  1.4458E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  10

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711665
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.59402007
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.04166008
   ht   6     0.00024474     0.00000003    -0.00944231     0.00000000    -0.00268342    -0.00008743
   ht   7     0.00454075     0.00000002    -0.01292121    -0.00000001     0.01270039    -0.00001121    -0.00004272

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.966569      -1.010397E-07   0.110234       3.922578E-08   0.104266       1.409249E-05  -5.999605E-05
 refs   2  -2.454724E-08   -1.00000      -7.217677E-07  -9.234236E-14   2.298218E-08   3.181731E-10   2.144772E-10
 refs   3  -1.443829E-08  -2.568166E-13   4.842455E-07   -1.00000       5.273615E-09   1.843964E-11  -2.969089E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.999944       1.069116E-08  -8.534827E-03  -1.229531E-09   2.670956E-02  -6.068581E-02  -5.547368E-02
 civs   2  -1.239644E-09   -1.00000      -3.983468E-07  -7.666065E-14   1.291175E-07  -1.746290E-07  -5.101779E-07
 civs   3   7.526609E-04   3.653474E-07  -0.976735       6.913562E-08  -0.193124       0.179243       0.203685    
 civs   4   1.156794E-10   8.969233E-14  -5.261027E-08   -1.00000      -8.046832E-08   1.014589E-07   1.126029E-07
 civs   5  -4.871216E-04  -9.138724E-08   7.839106E-02   2.298476E-08  -0.776590      -0.673075      -2.651420E-02
 civs   6   -1.17454      -1.644439E-04    127.987       3.019565E-05   -378.790        577.057       -599.305    
 civs   7   0.935522       1.529817E-04   -127.307      -1.856580E-05    431.292       -993.635       -863.415    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966555       1.306230E-07  -9.830423E-02  -2.885714E-08  -0.107658      -4.133063E-02   9.425154E-03
 ref    2   2.505801E-08    1.00000       1.118751E-06   1.252871E-13  -3.624897E-08   1.769700E-09  -1.194830E-08
 ref    3   1.463428E-08   3.358314E-13  -4.136927E-07    1.00000      -3.732223E-08   2.280822E-08   1.276138E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96655549     0.00000013    -0.09830423    -0.00000003    -0.10765796    -0.04133063     0.00942515
 ref:   2     0.00000003     1.00000000     0.00000112     0.00000000    -0.00000004     0.00000000    -0.00000001
 ref:   3     0.00000001     0.00000000    -0.00000041     1.00000000    -0.00000004     0.00000002     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -547.2981319059  6.1858E-07  1.4845E-07  5.2365E-04  1.0000E-04
 mr-sdci # 10  2   -547.1403465662 -1.4211E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci # 10  3   -547.0943570332  9.5915E-03  0.0000E+00  1.7638E-01  1.0000E-04
 mr-sdci # 10  4   -547.0135383959 -1.4211E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci # 10  5   -546.7320843668  1.0687E-01  0.0000E+00  4.6736E-01  1.0000E-04
 mr-sdci # 10  6   -545.9012698491  6.4277E-01  0.0000E+00  9.9822E-01  1.0000E-04
 mr-sdci # 10  7   -544.8123901738  7.1476E-02  0.0000E+00  1.4775E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  11

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711665
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.59402007
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.04166008
   ht   6     0.00024474     0.00000003    -0.00944231     0.00000000    -0.00268342    -0.00008743
   ht   7     0.00454075     0.00000002    -0.01292121    -0.00000001     0.01270039    -0.00001121    -0.00004272
   ht   8    -0.00596920    -0.00000001     0.00314673     0.00000000    -0.00221930     0.00000742     0.00000234    -0.00000826

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.966569      -1.010397E-07   0.110234       3.922578E-08   0.104266       1.409249E-05  -5.999605E-05   9.302882E-05
 refs   2  -2.454724E-08   -1.00000      -7.217677E-07  -9.234236E-14   2.298218E-08   3.181731E-10   2.144772E-10  -9.212340E-11
 refs   3  -1.443829E-08  -2.568166E-13   4.842455E-07   -1.00000       5.273615E-09   1.843964E-11  -2.969089E-11   8.218226E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.999996       1.439261E-10  -1.426331E-03  -2.839651E-10  -1.585453E-02   5.675690E-02   0.132410      -0.229898    
 civs   2  -1.384572E-09   -1.00000      -4.648110E-07   2.790060E-14   1.258287E-07  -2.668766E-08   5.834171E-07  -1.346282E-07
 civs   3   8.010063E-04   4.243640E-07  -0.970667       7.328324E-08  -0.226942       9.532278E-02  -0.251219       9.458944E-02
 civs   4   1.184699E-10  -1.266887E-14  -5.350245E-08   -1.00000      -8.855311E-08   5.109235E-08  -1.408987E-07   5.785247E-08
 civs   5  -5.332930E-04  -1.117610E-07   8.989734E-02   2.499182E-08  -0.671199      -0.727102       0.134694      -0.260392    
 civs   6   -1.24368      -1.926147E-04    142.048       3.221074E-05   -406.678        351.930        387.418        674.712    
 civs   7    1.10687       2.056815E-04   -157.657      -2.289633E-05    552.951       -765.441        998.806       -147.005    
 civs   8   0.763187       1.711585E-04   -110.740      -1.488609E-05    613.217       -1266.53       -842.635        2715.77    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.966545       1.371737E-07  -9.784771E-02  -2.837333E-08  -9.218369E-02  -7.738576E-02  -1.851922E-02   3.203796E-02
 ref    2   2.511270E-08    1.00000       1.189092E-06   1.883883E-14  -4.435581E-08   4.265610E-09   1.286374E-08  -1.024935E-09
 ref    3   1.464966E-08   4.648724E-13  -4.088348E-07    1.00000      -4.806608E-08   1.858858E-08  -5.158728E-09   8.936203E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96654528     0.00000014    -0.09784771    -0.00000003    -0.09218369    -0.07738576    -0.01851922     0.03203796
 ref:   2     0.00000003     1.00000000     0.00000119     0.00000000    -0.00000004     0.00000000     0.00000001     0.00000000
 ref:   3     0.00000001     0.00000000    -0.00000041     1.00000000    -0.00000005     0.00000002    -0.00000001     0.00000001

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -547.2981320192  1.1330E-07  5.4547E-08  2.9828E-04  1.0000E-04
 mr-sdci # 11  2   -547.1403465662  8.5265E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci # 11  3   -547.0963564205  1.9994E-03  0.0000E+00  1.6219E-01  1.0000E-04
 mr-sdci # 11  4   -547.0135383959 -1.4211E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci # 11  5   -546.7890249248  5.6941E-02  0.0000E+00  3.9399E-01  1.0000E-04
 mr-sdci # 11  6   -546.2032874650  3.0202E-01  0.0000E+00  7.7786E-01  1.0000E-04
 mr-sdci # 11  7   -544.8501462486  3.7756E-02  0.0000E+00  1.4689E+00  1.0000E-04
 mr-sdci # 11  8   -544.4464492644 -6.6934E-02  0.0000E+00  1.3166E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  12

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711869
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.61534309
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.30801160
   ht   6    -0.00123019     0.00000000     0.00196205     0.00000000    -0.00151517    -0.00000345

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.966545       1.371737E-07  -9.784771E-02  -2.837333E-08  -9.218369E-02  -9.433827E-06
 refs   2   2.511270E-08    1.00000       1.189092E-06   1.883883E-14  -4.435581E-08   1.714731E-11
 refs   3   1.464966E-08   4.648724E-13  -4.088348E-07    1.00000      -4.806608E-08  -2.655641E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.999991       2.408414E-09  -1.889146E-03  -6.293580E-10   1.048612E-02  -7.703399E-02
 civs   2  -1.573358E-11   -1.00000      -3.655291E-08   4.104846E-14   5.352020E-08  -2.486526E-07
 civs   3   1.113243E-05   2.635165E-08  -0.996589       5.204187E-09  -3.778079E-02   0.143726    
 civs   4  -1.110749E-12  -4.483620E-14  -2.447385E-09   -1.00000      -1.739071E-08   6.572377E-08
 civs   5  -4.411538E-05  -1.739265E-08   1.592002E-02   7.593865E-09  -0.978249      -0.226800    
 civs   6  -0.553974      -1.397057E-04    110.057       3.681866E-05   -616.550        4553.39    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.966544      -1.391587E-07   9.683409E-02   2.742505E-08   8.955653E-02   3.834502E-02
 ref    2  -2.512250E-08   -1.00000      -1.220456E-06   2.867658E-14   4.167756E-08   8.454089E-09
 ref    3  -1.465160E-08  -5.192398E-13   4.039078E-07   -1.00000       4.686696E-08   4.644438E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96654443    -0.00000014     0.09683409     0.00000003     0.08955653     0.03834502
 ref:   2    -0.00000003    -1.00000000    -0.00000122     0.00000000     0.00000004     0.00000001
 ref:   3    -0.00000001     0.00000000     0.00000040    -1.00000000     0.00000005     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -547.2981320494  3.0218E-08  1.6614E-08  1.6352E-04  1.0000E-04
 mr-sdci # 12  2   -547.1403465662 -5.6843E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci # 12  3   -547.0974002710  1.0439E-03  0.0000E+00  1.6180E-01  1.0000E-04
 mr-sdci # 12  4   -547.0135383959  8.5265E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci # 12  5   -546.8206711683  3.1646E-02  0.0000E+00  3.3907E-01  1.0000E-04
 mr-sdci # 12  6   -545.0685969931 -1.1347E+00  0.0000E+00  1.5972E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  13

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711869
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.61534309
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.30801160
   ht   6    -0.00123019     0.00000000     0.00196205     0.00000000    -0.00151517    -0.00000345
   ht   7    -0.00233539     0.00000000     0.00137816     0.00000000     0.00017769    -0.00000040    -0.00000117

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.966545       1.371737E-07  -9.784771E-02  -2.837333E-08  -9.218369E-02  -9.433827E-06  -3.583712E-05
 refs   2   2.511270E-08    1.00000       1.189092E-06   1.883883E-14  -4.435581E-08   1.714731E-11   1.820477E-11
 refs   3   1.464966E-08   4.648724E-13  -4.088348E-07    1.00000      -4.806608E-08  -2.655641E-12   5.799127E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00002      -4.867186E-09   4.769475E-03   7.682850E-10   3.493381E-02  -0.171470      -0.195913    
 civs   2  -6.007974E-11   -1.00000      -5.537244E-08  -2.438196E-14  -3.766440E-08  -8.809963E-08  -3.523478E-07
 civs   3   3.632825E-05   5.088273E-08  -0.997827       7.096855E-09   4.511550E-02   1.915026E-02   0.187573    
 civs   4   4.261027E-12   2.177734E-14  -5.102106E-09   -1.00000       1.743807E-08   1.205394E-08   8.519808E-08
 civs   5  -6.919694E-05  -3.013549E-08   2.943665E-02   1.201162E-08   0.927524       0.360437      -0.137804    
 civs   6  -0.844556      -2.330756E-04    195.519       5.622850E-05    1143.38       -2906.33        3474.87    
 civs   7   0.954020       2.770393E-04   -253.362      -5.390782E-05   -1695.87        6881.82        4275.97    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.966538      -1.423996E-07   9.754685E-02   2.723052E-08  -7.369363E-02  -8.857385E-02  -2.311049E-03
 ref    2  -2.512404E-08   -1.00000      -1.244326E-06  -3.531268E-14  -3.554865E-08  -1.017564E-08   9.313603E-09
 ref    3  -1.465437E-08  -4.617409E-13   4.008330E-07   -1.00000      -4.909727E-08  -3.903062E-09   5.517194E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96653764    -0.00000014     0.09754685     0.00000003    -0.07369363    -0.08857385    -0.00231105
 ref:   2    -0.00000003    -1.00000000    -0.00000124     0.00000000    -0.00000004    -0.00000001     0.00000001
 ref:   3    -0.00000001     0.00000000     0.00000040    -1.00000000    -0.00000005     0.00000000     0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -547.2981320653  1.5850E-08  6.1497E-09  1.0126E-04  1.0000E-04
 mr-sdci # 13  2   -547.1403465662 -1.4211E-14  0.0000E+00  5.0715E-01  1.0000E-04
 mr-sdci # 13  3   -547.0982801401  8.7987E-04  0.0000E+00  1.5019E-01  1.0000E-04
 mr-sdci # 13  4   -547.0135383959 -1.1369E-13  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci # 13  5   -546.8539553707  3.3284E-02  0.0000E+00  3.2452E-01  1.0000E-04
 mr-sdci # 13  6   -546.2153784019  1.1468E+00  0.0000E+00  1.0961E+00  1.0000E-04
 mr-sdci # 13  7   -544.6427973489 -2.0735E-01  0.0000E+00  1.5617E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  14

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711869
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.61534309
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.30801160
   ht   6    -0.00123019     0.00000000     0.00196205     0.00000000    -0.00151517    -0.00000345
   ht   7    -0.00233539     0.00000000     0.00137816     0.00000000     0.00017769    -0.00000040    -0.00000117
   ht   8     0.00155409     0.00000000    -0.00077759     0.00000000     0.00013174     0.00000039     0.00000014    -0.00000041

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   4.699066E-09   1.437225E-08   4.860037E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   1.771873E-05   2.990312E-05   2.369295E-05  -4.578979E-06  -6.590477E-06  -0.415565       0.514568      -0.750017    
   x:   2   1.078230E-11   3.153200E-11   5.561514E-11  -0.850641       0.525747       2.504726E-06  -5.222731E-07  -1.172614E-06
   x:   3  -6.856084E-06  -1.504417E-05  -3.041499E-05  -9.712807E-06  -1.063669E-05  -0.702411       0.342322       0.624046    
   x:   4  -6.631341E-12  -6.483538E-12  -1.484539E-11   0.525747       0.850641      -1.988602E-05  -2.860696E-08   3.142556E-07
   x:   5  -1.282746E-07  -5.421393E-06   1.974725E-05  -6.680483E-06  -9.325597E-06  -0.577862      -0.786152      -0.219182    
   x:   6   0.102866       0.152865      -0.982879       2.817332E-13   4.560246E-13   1.646488E-07  -1.643237E-05  -3.388922E-05
   x:   7   5.428591E-02  -0.987511      -0.147904       1.640610E-11   2.654577E-11   1.407471E-06   1.191232E-05  -3.536887E-05
   x:   8   0.993213       3.814216E-02   0.109880       2.559737E-11   4.141791E-11   2.396289E-06  -5.867515E-06   2.310264E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.966545       1.371737E-07  -9.784771E-02  -2.837333E-08  -9.218369E-02  -9.433827E-06  -3.583712E-05   2.272864E-05
 refs   2   2.511270E-08    1.00000       1.189092E-06   1.883883E-14  -4.435581E-08   1.714731E-11   1.820477E-11  -6.414149E-12
 refs   3   1.464966E-08   4.648724E-13  -4.088348E-07    1.00000      -4.806608E-08  -2.655641E-12   5.799127E-13   4.012557E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00001       4.735915E-09   4.066559E-03  -2.885445E-10  -1.756893E-02   2.372659E-02   0.356083      -0.113489    
 civs   2  -5.154542E-11    1.00000      -5.653734E-08  -1.043981E-13   5.770054E-08  -1.109092E-08   3.911535E-07   8.096155E-08
 civs   3   3.128038E-05  -5.091384E-08  -0.996962      -8.310145E-09  -6.722160E-02   3.655588E-02  -0.208636      -3.831162E-02
 civs   4  -1.065803E-13   1.078996E-13  -4.831358E-09    1.00000      -3.861573E-08   3.662988E-08  -1.251892E-07   2.108812E-08
 civs   5  -7.345536E-05   3.036489E-08   3.115546E-02  -1.492754E-08  -0.838138      -0.534409       3.272452E-02   0.141290    
 civs   6  -0.925289       2.352302E-04    208.938      -7.100207E-05   -1494.91        2095.56       -869.448       -4040.97    
 civs   7    1.21908      -2.832052E-04   -290.684       9.165328E-05    2784.86       -5622.88       -5280.53       -1260.10    
 civs   8   0.716098      -1.367610E-05   -78.2275       6.736539E-05    2169.29       -5672.16        8059.87       -10413.1    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.966538       1.423981E-07   9.741611E-02  -2.698888E-08   6.442761E-02   7.557246E-02   5.385887E-02  -5.297806E-02
 ref    2  -2.512226E-08    1.00000      -1.244504E-06  -9.476700E-14   2.565280E-08   2.662937E-08  -1.217977E-08   8.478788E-10
 ref    3  -1.465311E-08   5.913535E-13   4.002861E-07    1.00000       4.318474E-08   1.613339E-08  -4.660657E-09  -3.485059E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96653819     0.00000014     0.09741611    -0.00000003     0.06442761     0.07557246     0.05385887    -0.05297806
 ref:   2    -0.00000003     1.00000000    -0.00000124     0.00000000     0.00000003     0.00000003    -0.00000001     0.00000000
 ref:   3    -0.00000001     0.00000000     0.00000040     1.00000000     0.00000004     0.00000002     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -547.2981320697  4.4038E-09  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 14  2   -547.1403465662  2.8422E-14  1.0960E-01  5.0715E-01  1.0000E-04
 mr-sdci # 14  3   -547.0983217248  4.1585E-05  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 14  4   -547.0135383959  7.1054E-14  0.0000E+00  5.0976E-01  1.0000E-04
 mr-sdci # 14  5   -546.8802404896  2.6285E-02  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 14  6   -546.5460201916  3.3064E-01  0.0000E+00  6.7296E-01  1.0000E-04
 mr-sdci # 14  7   -544.9444941451  3.0170E-01  0.0000E+00  1.5243E+00  1.0000E-04
 mr-sdci # 14  8   -544.1800656003 -2.6638E-01  0.0000E+00  1.3421E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.010000
time for vector access                 0.000000

          starting ci iteration  15

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6     0.00000476    -0.10959983    -0.00000501    -0.00252323     0.00000020    -4.16536715

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.966538       1.423981E-07   9.741611E-02  -2.698888E-08   6.442761E-02  -6.196829E-08
 refs   2  -2.512226E-08    1.00000      -1.244504E-06  -9.476700E-14   2.565280E-08   1.034205E-12
 refs   3  -1.465311E-08   5.913535E-13   4.002861E-07    1.00000       4.318474E-08  -2.420886E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -5.583019E-08   1.879462E-14  -7.962022E-10  -6.159697E-15   2.634157E-07
 civs   2  -8.214563E-12  -0.978215      -1.045992E-06   1.052627E-02   2.754958E-08  -0.207329    
 civs   3   7.442170E-15   1.063716E-06   -1.00000      -2.236649E-08  -2.692464E-15   2.515265E-08
 civs   4  -1.123704E-13  -9.684337E-03   1.193611E-08  -0.999940       1.237628E-09  -5.075421E-03
 civs   5   1.884031E-15  -2.608938E-08  -2.176023E-14  -9.351250E-10   -1.00000      -9.831761E-09
 civs   6  -1.182609E-11  -0.853888       4.010742E-07  -1.217606E-02  -6.538148E-08    4.02817    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.966538      -3.814046E-08  -9.741611E-02   2.623206E-08  -6.442761E-02  -2.258706E-08
 ref    2   2.511405E-08  -0.978215       1.985116E-07   1.052627E-02   1.896774E-09  -0.207329    
 ref    3   1.465300E-08  -9.684337E-03  -3.883500E-07  -0.999940      -4.194711E-08  -5.075421E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96653819    -0.00000004    -0.09741611     0.00000003    -0.06442761    -0.00000002
 ref:   2     0.00000003    -0.97821461     0.00000020     0.01052627     0.00000000    -0.20732913
 ref:   3     0.00000001    -0.00968434    -0.00000039    -0.99994023    -0.00000004    -0.00507542

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -547.2981320697  9.9476E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 15  2   -547.2360167324  9.5670E-02  8.9671E-03  1.1820E-01  1.0000E-04
 mr-sdci # 15  3   -547.0983217248  1.5632E-13  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 15  4   -547.0135691207  3.0725E-05  0.0000E+00  5.0971E-01  1.0000E-04
 mr-sdci # 15  5   -546.8802404896  1.4211E-14  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 15  6   -545.0109451592 -1.5351E+00  0.0000E+00  1.0271E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  16

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6     0.00000476    -0.10959983    -0.00000501    -0.00252323     0.00000020    -4.16536715
   ht   7     0.00000447     2.69326689    -0.00001195     0.10226261     0.00000034    -0.19595928    -0.71926641

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.966538       1.423981E-07   9.741611E-02  -2.698888E-08   6.442761E-02  -6.196829E-08  -6.043525E-08
 refs   2  -2.512226E-08    1.00000      -1.244504E-06  -9.476700E-14   2.565280E-08   1.034205E-12  -3.706324E-02
 refs   3  -1.465311E-08   5.913535E-13   4.002861E-07    1.00000       4.318474E-08  -2.420886E-13  -1.418825E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -2.061811E-08  -8.521361E-14  -3.243785E-09  -7.709704E-15   5.365382E-07  -4.238269E-07
 civs   2  -8.016786E-12  -0.952669      -1.223194E-06   7.260463E-03   2.584299E-08   0.496003       8.049504E-02
 civs   3   7.457431E-15   1.065267E-06   -1.00000      -2.077979E-09  -9.392663E-15  -1.899725E-06   4.885370E-07
 civs   4  -1.160914E-13  -7.305177E-03  -2.624456E-09   -1.00001       1.299723E-09   1.273757E-02   1.405882E-03
 civs   5   1.886768E-15  -2.263545E-08  -2.064592E-14  -1.269961E-09   -1.00000       4.893448E-08  -3.629190E-09
 civs   6  -1.144356E-11  -0.899881       4.400065E-07  -1.029563E-02  -6.376562E-08   -1.54706       -3.74592    
 civs   7  -1.923825E-12   0.622002      -1.733855E-06  -4.183403E-02  -2.690193E-08    10.3782       -2.91095    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966538      -3.490021E-08  -9.741611E-02   2.776975E-08  -6.442761E-02  -1.243795E-07   5.718936E-08
 ref    2   2.511432E-08  -0.975722       8.557234E-08   8.810968E-03   1.187263E-09   0.111353       0.188384    
 ref    3   1.465300E-08  -8.187688E-03  -4.004505E-07  -0.999949      -4.184684E-08  -1.987256E-03   5.536010E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96653819    -0.00000003    -0.09741611     0.00000003    -0.06442761    -0.00000012     0.00000006
 ref:   2     0.00000003    -0.97572237     0.00000009     0.00881097     0.00000000     0.11135347     0.18838427
 ref:   3     0.00000001    -0.00818769    -0.00000040    -0.99994918    -0.00000004    -0.00198726     0.00553601

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -547.2981320697  1.4211E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 16  2   -547.2416035906  5.5869E-03  1.6336E-03  5.0876E-02  1.0000E-04
 mr-sdci # 16  3   -547.0983217248  2.8422E-14  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 16  4   -547.0135915003  2.2380E-05  0.0000E+00  5.0959E-01  1.0000E-04
 mr-sdci # 16  5   -546.8802404896 -2.8422E-14  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 16  6   -545.5918851572  5.8094E-01  0.0000E+00  1.2547E+00  1.0000E-04
 mr-sdci # 16  7   -544.9652836799  2.0790E-02  0.0000E+00  9.9681E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  17

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.65933324
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.53252507
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6     0.00000476    -0.10959983    -0.00000501    -0.00252323     0.00000020    -4.16536715
   ht   7     0.00000447     2.69326689    -0.00001195     0.10226261     0.00000034    -0.19595928    -0.71926641
   ht   8    -0.00000072    -0.59570215     0.00000108    -0.00785900    -0.00000042     0.05106907     0.07499023    -0.10504236

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.966538       1.423981E-07   9.741611E-02  -2.698888E-08   6.442761E-02  -6.196829E-08  -6.043525E-08   1.135186E-08
 refs   2  -2.512226E-08    1.00000      -1.244504E-06  -9.476700E-14   2.565280E-08   1.034205E-12  -3.706324E-02   8.197569E-03
 refs   3  -1.465311E-08   5.913535E-13   4.002861E-07    1.00000       4.318474E-08  -2.420886E-13  -1.418825E-03   1.240825E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       2.032847E-08  -8.858434E-14  -2.541168E-09  -6.820452E-15  -3.061752E-07  -5.894776E-07   1.973423E-07
 civs   2  -1.566283E-11   0.952345      -1.259912E-06   1.132411E-02   2.853480E-08  -0.313697      -0.338686      -0.242738    
 civs   3   7.463068E-15  -1.085719E-06   -1.00000      -5.958660E-08  -5.462367E-14   1.528039E-06   1.298164E-06   1.025052E-08
 civs   4  -1.150159E-12   1.051416E-02   3.651161E-08  -0.999681       5.187513E-09  -2.743022E-02   2.830037E-03   8.041175E-04
 civs   5   1.892126E-15   2.789350E-08   2.367422E-14  -7.189337E-09   -1.00000       4.153709E-08  -1.186111E-07  -6.124921E-08
 civs   6  -2.212167E-11   0.909267       4.564970E-07  -1.568896E-02  -7.086978E-08    1.26331      -0.626791        3.80345    
 civs   7   5.197650E-11  -0.753071      -2.326066E-06   6.180237E-02   7.317415E-08   -9.16191       -6.05692       0.855570    
 civs   8   2.754807E-10  -0.726239      -3.240602E-06   0.539101       4.857826E-07   -17.6716        17.8872        10.5460    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.966538       3.192929E-08  -9.741611E-02   2.322578E-08  -6.442761E-02   8.648373E-08   1.087091E-07  -1.447917E-08
 ref    2   2.510693E-08   0.974302       4.423902E-08   1.345283E-02   4.152159E-09  -0.118991       3.243492E-02  -0.187997    
 ref    3   1.465192E-08   1.149252E-02  -3.608763E-07  -0.999702      -3.804077E-08  -1.662380E-02   1.364325E-02   8.987857E-04

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653819     0.00000003    -0.09741611     0.00000002    -0.06442761     0.00000009     0.00000011    -0.00000001
 ref:   2     0.00000003     0.97430243     0.00000004     0.01345283     0.00000000    -0.11899130     0.03243492    -0.18799691
 ref:   3     0.00000001     0.01149252    -0.00000036    -0.99970227    -0.00000004    -0.01662380     0.01364325     0.00089879

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -547.2981320697  0.0000E+00  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 17  2   -547.2427903977  1.1868E-03  3.1014E-04  2.3325E-02  1.0000E-04
 mr-sdci # 17  3   -547.0983217248  2.8422E-14  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 17  4   -547.0141532208  5.6172E-04  0.0000E+00  5.0927E-01  1.0000E-04
 mr-sdci # 17  5   -546.8802404896  0.0000E+00  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 17  6   -545.9661811881  3.7430E-01  0.0000E+00  1.0575E+00  1.0000E-04
 mr-sdci # 17  7   -545.1138250106  1.4854E-01  0.0000E+00  1.3766E+00  1.0000E-04
 mr-sdci # 17  8   -544.9201894697  7.4012E-01  0.0000E+00  9.6899E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  18

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76177707
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.53313989
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6     0.00000108    -0.07254634    -0.00000091    -0.00259992     0.00000025    -0.01960532

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.966538       3.192929E-08  -9.741611E-02   2.322578E-08  -6.442761E-02   1.402497E-08
 refs   2   2.510693E-08   0.974302       4.423902E-08   1.345283E-02   4.152159E-09   2.507287E-04
 refs   3   1.465192E-08   1.149252E-02  -3.608763E-07  -0.999702      -3.804077E-08  -7.377309E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -1.213983E-08  -3.683187E-14  -3.832278E-08  -4.739845E-14   8.965196E-07
 civs   2  -3.090711E-12  -0.999092       7.798414E-09   6.097317E-03   5.906090E-09  -7.334118E-02
 civs   3  -3.770599E-17   5.551712E-09   -1.00000       5.855462E-08   1.411814E-13  -7.971895E-07
 civs   4   1.986112E-12   2.997785E-03   2.455548E-08   0.999203      -1.960998E-08   3.989531E-02
 civs   5  -4.746786E-17  -4.914334E-09  -1.079521E-13  -2.643166E-08   -1.00000       1.558142E-07
 civs   6  -6.702362E-10  -0.817924      -2.483918E-06   -2.58134      -3.195534E-06    60.3845    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.966538      -3.179263E-08   9.741611E-02   2.023798E-08   6.442761E-02   4.657551E-08
 ref    2  -2.511009E-08  -0.973583      -3.693346E-08   1.873553E-02   5.371371E-10  -5.577966E-02
 ref    3  -1.465389E-08  -1.441864E-02   3.366010E-07  -0.998645       5.794852E-08  -4.518106E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96653819    -0.00000003     0.09741611     0.00000002     0.06442761     0.00000005
 ref:   2    -0.00000003    -0.97358256    -0.00000004     0.01873553     0.00000000    -0.05577966
 ref:   3    -0.00000001    -0.01441864     0.00000034    -0.99864545     0.00000006    -0.04518106

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -547.2981320697 -4.2633E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 18  2   -547.2430440955  2.5370E-04  1.3122E-04  1.3135E-02  1.0000E-04
 mr-sdci # 18  3   -547.0983217248  1.1369E-13  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 18  4   -547.0162927755  2.1396E-03  0.0000E+00  5.0775E-01  1.0000E-04
 mr-sdci # 18  5   -546.8802404896  0.0000E+00  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 18  6   -545.8444454130 -1.2174E-01  0.0000E+00  1.4807E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  19

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76177707
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.53313989
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6     0.00000108    -0.07254634    -0.00000091    -0.00259992     0.00000025    -0.01960532
   ht   7     0.00000187    -0.34356392    -0.00000139     0.02476421     0.00000041     0.00151241    -0.01211735

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.966538       3.192929E-08  -9.741611E-02   2.322578E-08  -6.442761E-02   1.402497E-08   2.603430E-08
 refs   2   2.510693E-08   0.974302       4.423902E-08   1.345283E-02   4.152159E-09   2.507287E-04   5.239125E-03
 refs   3   1.465192E-08   1.149252E-02  -3.608763E-07  -0.999702      -3.804077E-08  -7.377309E-05   6.051991E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       6.359591E-09   2.242908E-14  -1.261657E-07  -3.153260E-13   8.033584E-07   2.267251E-06
 civs   2   1.042061E-11   -1.00298      -2.513457E-09   2.553364E-02   6.313951E-08  -0.194890      -0.344010    
 civs   3   3.169669E-16  -9.408542E-09   -1.00000       1.457025E-08   4.783051E-13  -4.715356E-07  -1.776487E-06
 civs   4  -4.127127E-12   6.506709E-03   5.746888E-08  -0.988846       7.732564E-08  -0.149473       2.487293E-02
 civs   5  -7.893800E-17   1.858449E-09  -2.584967E-13  -7.206322E-08   -1.00000      -5.621549E-08   4.439472E-07
 civs   6   1.589773E-10   -1.22180      -4.102354E-06    7.12991       5.739442E-06   -39.8559        45.4178    
 civs   7  -2.051709E-09   0.955213       3.259999E-06   -9.04609      -1.561854E-05    54.3910        62.1037    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.966538      -2.949072E-08   9.741611E-02  -3.249582E-08   6.442761E-02   1.204410E-07   1.964729E-07
 ref    2  -2.510755E-08  -0.972420      -2.986381E-08  -3.403121E-02  -2.198338E-08   8.307611E-02   1.920854E-03
 ref    3  -1.464893E-08  -1.736331E-02   3.056712E-07   0.982844      -4.841196E-08   0.183046       5.415407E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96653819    -0.00000003     0.09741611    -0.00000003     0.06442761     0.00000012     0.00000020
 ref:   2    -0.00000003    -0.97242021    -0.00000003    -0.03403121    -0.00000002     0.08307611     0.00192085
 ref:   3    -0.00000001    -0.01736331     0.00000031     0.98284444    -0.00000005     0.18304644     0.00541541

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -547.2981320697  2.8422E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 19  2   -547.2431694471  1.2535E-04  6.7708E-05  9.4450E-03  1.0000E-04
 mr-sdci # 19  3   -547.0983217248 -2.8422E-14  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 19  4   -547.0238859743  7.5932E-03  0.0000E+00  4.9719E-01  1.0000E-04
 mr-sdci # 19  5   -546.8802404896  0.0000E+00  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 19  6   -546.6900793908  8.4563E-01  0.0000E+00  7.6959E-01  1.0000E-04
 mr-sdci # 19  7   -544.7636238985 -3.5020E-01  0.0000E+00  1.4834E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  20

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76177707
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.53313989
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6     0.00000108    -0.07254634    -0.00000091    -0.00259992     0.00000025    -0.01960532
   ht   7     0.00000187    -0.34356392    -0.00000139     0.02476421     0.00000041     0.00151241    -0.01211735
   ht   8    -0.00000272     0.32741052     0.00000230    -0.01058416    -0.00000049     0.00328743     0.00210422    -0.00692365

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.966538       3.192929E-08  -9.741611E-02   2.322578E-08  -6.442761E-02   1.402497E-08   2.603430E-08  -3.760672E-08
 refs   2   2.510693E-08   0.974302       4.423902E-08   1.345283E-02   4.152159E-09   2.507287E-04   5.239125E-03  -4.544129E-03
 refs   3   1.465192E-08   1.149252E-02  -3.608763E-07  -0.999702      -3.804077E-08  -7.377309E-05   6.051991E-04  -2.098679E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -1.575142E-08  -2.430751E-13  -3.138243E-07   3.154314E-12   6.208477E-07  -4.451837E-06   1.009657E-06
 civs   2   1.697771E-11   -1.00090       2.201545E-08   1.302940E-02  -7.993445E-08   3.142196E-03   0.594065      -0.181330    
 civs   3   9.189593E-17   7.976730E-09   -1.00000       5.905314E-07  -4.000068E-12  -8.001138E-07   3.723774E-06  -7.146699E-07
 civs   4   7.155639E-12   9.147618E-03   1.874507E-07   0.917781       1.689544E-06   0.396636      -3.215499E-02   1.629236E-02
 civs   5  -1.176921E-16  -4.863431E-09  -3.007752E-13  -1.366992E-07    1.00000      -3.949572E-06  -6.785581E-07   2.467066E-07
 civs   6  -6.852043E-10   -1.39838      -8.472923E-06   -13.3942       1.179666E-04    29.7256        17.6391        51.7397    
 civs   7   1.080849E-10    1.37309       1.152644E-05    21.4584      -1.890185E-04   -48.7375       -28.0130        58.0954    
 civs   8   4.588327E-09   0.810318       1.107637E-05    17.8452      -1.676570E-04   -38.3421        107.150        33.4882    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.966538      -3.132310E-08   9.741611E-02  -2.396263E-08  -6.442761E-02   3.316216E-07  -5.094118E-07   5.118293E-08
 ref    2  -2.511075E-08  -0.971893      -1.233597E-08   5.301532E-02  -2.498576E-07  -6.525983E-02  -5.087708E-02  -1.128394E-02
 ref    3  -1.465973E-08  -1.988364E-02   1.790107E-07  -0.907129      -1.815912E-06  -0.420124      -1.769323E-03   5.942747E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96653819    -0.00000003     0.09741611    -0.00000002    -0.06442761     0.00000033    -0.00000051     0.00000005
 ref:   2    -0.00000003    -0.97189345    -0.00000001     0.05301532    -0.00000025    -0.06525983    -0.05087708    -0.01128394
 ref:   3    -0.00000001    -0.01988364     0.00000018    -0.90712891    -0.00000182    -0.42012363    -0.00176932     0.00594275

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -547.2981320697 -2.8422E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 20  2   -547.2432243149  5.4868E-05  1.5524E-05  5.3046E-03  1.0000E-04
 mr-sdci # 20  3   -547.0983217248 -7.1054E-14  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 20  4   -547.0426537392  1.8768E-02  0.0000E+00  4.4063E-01  1.0000E-04
 mr-sdci # 20  5   -546.8802404896  1.2790E-13  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 20  6   -546.8732212070  1.8314E-01  0.0000E+00  5.1919E-01  1.0000E-04
 mr-sdci # 20  7   -545.3175286465  5.5390E-01  0.0000E+00  1.2314E+00  1.0000E-04
 mr-sdci # 20  8   -544.7116188505 -2.0857E-01  0.0000E+00  1.4002E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  21

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76221099
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.56164041
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000187     0.18154906     0.00000134    -0.04317308     0.00000065    -0.00144756

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.966538      -3.132310E-08   9.741611E-02  -2.396263E-08  -6.442761E-02   2.589464E-08
 refs   2  -2.511075E-08  -0.971893      -1.233597E-08   5.301532E-02  -2.498576E-07   2.839961E-03
 refs   3  -1.465973E-08  -1.988364E-02   1.790107E-07  -0.907129      -1.815912E-06  -4.097087E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -2.387752E-08   7.071132E-13  -9.901738E-07   3.665497E-12  -6.892493E-06
 civs   2   6.293283E-11    1.00231      -6.572736E-08   9.290987E-02  -3.504449E-07   0.667452    
 civs   3  -6.735667E-16   1.884699E-08   -1.00000       3.864421E-07  -2.487895E-12   4.900419E-06
 civs   4   4.310090E-11   2.405487E-03  -2.982224E-07   0.967203       6.431747E-07  -0.299720    
 civs   5   7.301106E-18   2.366745E-09   2.463065E-14  -1.337625E-07    1.00000       3.002547E-06
 civs   6   2.265364E-08   0.931020      -2.752195E-05    38.5999      -1.428902E-04    268.674    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.966538      -2.873976E-08  -9.741611E-02   6.266717E-08  -6.442761E-02   5.655796E-07
 ref    2   2.511621E-08  -0.971370      -1.775566E-08   7.060035E-02  -2.809670E-07   9.844314E-02
 ref    3   1.461845E-08  -2.214986E-02   9.394993E-08  -0.880807      -2.386532E-06   0.247606    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96653819    -0.00000003    -0.09741611     0.00000006    -0.06442761     0.00000057
 ref:   2     0.00000003    -0.97136980    -0.00000002     0.07060035    -0.00000028     0.09844314
 ref:   3     0.00000001    -0.02214986     0.00000009    -0.88080660    -0.00000239     0.24760564

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -547.2981320697  0.0000E+00  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 21  2   -547.2432387683  1.4453E-05  8.2148E-06  3.1372E-03  1.0000E-04
 mr-sdci # 21  3   -547.0983217248  9.9476E-14  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 21  4   -547.0674492965  2.4796E-02  0.0000E+00  3.9590E-01  1.0000E-04
 mr-sdci # 21  5   -546.8802404896  3.2685E-13  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 21  6   -545.8414338276 -1.0318E+00  0.0000E+00  1.3510E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  22

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76221099
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.56164041
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000187     0.18154906     0.00000134    -0.04317308     0.00000065    -0.00144756
   ht   7     0.00000346    -0.17866574    -0.00000249     0.03056640    -0.00000049     0.00039878    -0.00117676

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.966538      -3.132310E-08   9.741611E-02  -2.396263E-08  -6.442761E-02   2.589464E-08  -4.789604E-08
 refs   2  -2.511075E-08  -0.971893      -1.233597E-08   5.301532E-02  -2.498576E-07   2.839961E-03  -2.627887E-03
 refs   3  -1.465973E-08  -1.988364E-02   1.790107E-07  -0.907129      -1.815912E-06  -4.097087E-05   7.142920E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       1.096460E-08   1.359837E-06   8.256628E-12  -1.500909E-11  -5.951794E-06   1.575788E-05
 civs   2   1.266221E-10    1.00112       1.819545E-03   1.215746E-08   3.309015E-07   0.120005       -1.05418    
 civs   3   1.415938E-15  -4.849307E-09   5.431481E-06   -1.00000       1.151229E-11   4.453502E-06  -1.136364E-05
 civs   4  -7.689460E-11   5.221696E-03   0.929123       6.015851E-06   2.179229E-06   0.339929       0.259263    
 civs   5   5.838847E-16  -8.660821E-09  -1.113865E-06  -6.977373E-12    1.00000      -2.903449E-06  -3.381687E-06
 civs   6  -5.184603E-09    1.47538        68.2471       4.143230E-04  -3.962969E-04   -164.436       -205.687    
 civs   7  -5.743301E-08    1.02880        65.5586       3.980168E-04  -5.305850E-04   -214.339        220.892    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966538      -3.187111E-08   5.201251E-07  -9.741611E-02  -6.442761E-02   8.643562E-07  -1.537788E-06
 ref    2   2.511982E-08  -0.971217       6.902766E-02   4.501702E-07  -1.870760E-07  -2.343416E-03  -0.126328    
 ref    3   1.468615E-08  -2.396820E-02  -0.798838      -5.369079E-06  -4.162089E-06  -0.457109      -4.801527E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96653819    -0.00000003     0.00000052    -0.09741611    -0.06442761     0.00000086    -0.00000154
 ref:   2     0.00000003    -0.97121679     0.06902766     0.00000045    -0.00000019    -0.00234342    -0.12632845
 ref:   3     0.00000001    -0.02396820    -0.79883831    -0.00000537    -0.00000416    -0.45710900    -0.04801527

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -547.2981320697  1.4211E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 22  2   -547.2432472198  8.4515E-06  4.1192E-06  2.4938E-03  1.0000E-04
 mr-sdci # 22  3   -547.1019761302  3.6544E-03  0.0000E+00  3.1723E-01  1.0000E-04
 mr-sdci # 22  4   -547.0983217248  3.0872E-02  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 22  5   -546.8802404896  1.6342E-12  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 22  6   -546.6082581311  7.6682E-01  0.0000E+00  8.6344E-01  1.0000E-04
 mr-sdci # 22  7   -545.0708700905 -2.4666E-01  0.0000E+00  1.2473E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  23

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76221099
   ht   3     0.00000000     0.00000000   -72.61730840
   ht   4     0.00000000     0.00000000     0.00000000   -72.56164041
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000187     0.18154906     0.00000134    -0.04317308     0.00000065    -0.00144756
   ht   7     0.00000346    -0.17866574    -0.00000249     0.03056640    -0.00000049     0.00039878    -0.00117676
   ht   8    -0.00000459     0.02111762     0.00000338     0.01652635     0.00000087    -0.00023449     0.00006445    -0.00032423

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.966538      -3.132310E-08   9.741611E-02  -2.396263E-08  -6.442761E-02   2.589464E-08  -4.789604E-08   6.324486E-08
 refs   2  -2.511075E-08  -0.971893      -1.233597E-08   5.301532E-02  -2.498576E-07   2.839961E-03  -2.627887E-03   2.344922E-04
 refs   3  -1.465973E-08  -1.988364E-02   1.790107E-07  -0.907129      -1.815912E-06  -4.097087E-05   7.142920E-04   4.082879E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -2.379204E-08   1.517713E-06  -3.608699E-12  -3.655957E-11  -3.618368E-06   3.075838E-05  -1.119247E-05
 civs   2   9.648074E-11    1.00089       2.089809E-02  -2.582251E-08   3.523848E-08  -0.123583      -0.716880      -0.788889    
 civs   3   8.157364E-15   1.938800E-08  -1.263206E-06   -1.00000       2.831283E-11   2.784019E-06  -2.261137E-05   8.548756E-06
 civs   4   1.128944E-10   6.685285E-03  -0.893801       1.145527E-07   8.868979E-07  -0.415007       5.213366E-02   0.326012    
 civs   5   2.929768E-15   4.463984E-09  -3.251164E-07   1.859376E-12    1.00000       2.147867E-06  -4.713416E-06   5.360596E-07
 civs   6   2.218873E-08    1.67539       -77.5526      -4.068835E-06  -1.963369E-04    122.377       -49.8985       -245.969    
 civs   7   2.082001E-09    1.40683       -90.4220       1.482960E-05  -1.454644E-04    190.918        204.346        114.969    
 civs   8   1.376571E-07   0.753938       -60.5200       6.997356E-05   5.498433E-04    151.156       -313.504        363.829    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.966538      -2.922122E-08  -1.193430E-07  -9.741611E-02  -6.442761E-02   2.338980E-07  -3.055680E-06   1.131682E-06
 ref    2   2.511279E-08  -0.971162      -6.451505E-02   9.388149E-09  -2.834777E-07  -2.061238E-02  -5.272875E-02  -0.131353    
 ref    3   1.461218E-08  -2.472160E-02   0.724258      -2.430826E-07  -2.492509E-06   0.571994      -1.303030E-02  -3.930369E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653819    -0.00000003    -0.00000012    -0.09741611    -0.06442761     0.00000023    -0.00000306     0.00000113
 ref:   2     0.00000003    -0.97116231    -0.06451505     0.00000001    -0.00000028    -0.02061238    -0.05272875    -0.13135263
 ref:   3     0.00000001    -0.02472160     0.72425756    -0.00000024    -0.00000249     0.57199406    -0.01303030    -0.03930369

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -547.2981320697 -2.8422E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 23  2   -547.2432503254  3.1056E-06  1.0448E-06  1.3832E-03  1.0000E-04
 mr-sdci # 23  3   -547.1222507044  2.0275E-02  0.0000E+00  2.3086E-01  1.0000E-04
 mr-sdci # 23  4   -547.0983217248  2.1316E-13  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 23  5   -546.8802404896  9.3792E-13  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 23  6   -546.7715352794  1.6328E-01  0.0000E+00  6.7859E-01  1.0000E-04
 mr-sdci # 23  7   -545.2448794864  1.7401E-01  0.0000E+00  1.3682E+00  1.0000E-04
 mr-sdci # 23  8   -544.8426611474  1.3104E-01  0.0000E+00  1.2435E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  24

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223700
   ht   3     0.00000000     0.00000000   -72.64123738
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000330    -0.00730940     0.00701208     0.00000241    -0.00000056    -0.00006307

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.966538      -2.922122E-08  -1.193430E-07  -9.741611E-02  -6.442761E-02  -4.542749E-08
 refs   2   2.511279E-08  -0.971162      -6.451505E-02   9.388149E-09  -2.834777E-07  -8.900688E-05
 refs   3   1.461218E-08  -2.472160E-02   0.724258      -2.430826E-07  -2.492509E-06   7.514124E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       3.668273E-08  -3.559588E-06   8.480570E-12   8.511422E-11  -4.849043E-05
 civs   2  -3.506960E-11  -0.999918      -8.628271E-03   2.018101E-08   1.943368E-07  -0.108332    
 civs   3  -2.051820E-10   5.963061E-04  -0.989831      -8.052008E-07  -9.600729E-07   0.175172    
 civs   4   1.135511E-14  -2.519055E-08   1.841351E-06    1.00000      -6.515656E-11   3.583599E-05
 civs   5  -5.511226E-15   1.091255E-08  -1.261483E-06   3.194481E-12    1.00000      -6.514857E-06
 civs   6   4.308089E-07  -0.810811        78.6663      -1.874673E-04  -1.880924E-03    1071.56    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.966538       3.227642E-08  -1.128571E-07  -9.741611E-02  -6.442761E-02  -4.899271E-06
 ref    2  -2.510384E-08   0.971116       6.523660E-02   5.842256E-08  -2.428559E-07  -1.468966E-03
 ref    3  -1.472755E-08   2.509053E-02  -0.710768      -8.408408E-07  -3.333988E-06   0.210066    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96653819     0.00000003    -0.00000011    -0.09741611    -0.06442761    -0.00000490
 ref:   2    -0.00000003     0.97111642     0.06523660     0.00000006    -0.00000024    -0.00146897
 ref:   3    -0.00000001     0.02509053    -0.71076819    -0.00000084    -0.00000333     0.21006570

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -547.2981320697  0.0000E+00  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 24  2   -547.2432511725  8.4715E-07  4.5913E-07  7.7359E-04  1.0000E-04
 mr-sdci # 24  3   -547.1301780927  7.9274E-03  0.0000E+00  1.9647E-01  1.0000E-04
 mr-sdci # 24  4   -547.0983217248  7.1054E-14  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 24  5   -546.8802404896  3.8227E-12  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 24  6   -545.6513630902 -1.1202E+00  0.0000E+00  1.5606E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  25

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223700
   ht   3     0.00000000     0.00000000   -72.64123738
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000330    -0.00730940     0.00701208     0.00000241    -0.00000056    -0.00006307
   ht   7    -0.00000490    -0.01409314     0.00545792     0.00000360    -0.00000091     0.00000405    -0.00004227

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.966538      -2.922122E-08  -1.193430E-07  -9.741611E-02  -6.442761E-02  -4.542749E-08  -6.764473E-08
 refs   2   2.511279E-08  -0.971162      -6.451505E-02   9.388149E-09  -2.834777E-07  -8.900688E-05  -1.860904E-04
 refs   3   1.461218E-08  -2.472160E-02   0.724258      -2.430826E-07  -2.492509E-06   7.514124E-05  -2.588164E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       7.316733E-09   1.975311E-06   4.513909E-11  -4.755209E-10  -3.461887E-05   1.026403E-04
 civs   2  -2.303702E-10    1.00006       8.260928E-03   1.307507E-07  -1.446194E-06  -0.118049       0.270432    
 civs   3   1.681545E-10  -1.012900E-03  -0.984298       3.027202E-07  -1.045829E-06  -0.142795      -0.184574    
 civs   4   5.380331E-14  -5.267460E-09  -1.171638E-06    1.00000       3.641928E-10   2.599962E-05  -7.574648E-05
 civs   5  -2.377941E-14   3.476711E-09   8.722815E-07   1.831626E-11   -1.00000       3.631792E-07   1.641901E-05
 civs   6  -7.091474E-08    1.21828        129.311       4.090594E-05  -2.929602E-03   -661.318       -845.037    
 civs   7   1.219195E-06  -0.927272       -116.220      -6.977948E-04   9.029388E-03    958.400       -956.241    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.966538      -2.850329E-08   2.533578E-07  -9.741611E-02   6.442761E-02  -3.863888E-06   1.020199E-05
 ref    2  -2.512048E-08  -0.971087       6.559723E-02  -1.090999E-08   3.359104E-07   4.370213E-03   2.435738E-03
 ref    3  -1.480557E-08  -2.512504E-02  -0.673293       1.566069E-07  -7.862765E-07  -0.398244       4.362935E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96653819    -0.00000003     0.00000025    -0.09741611     0.06442761    -0.00000386     0.00001020
 ref:   2    -0.00000003    -0.97108694     0.06559723    -0.00000001     0.00000034     0.00437021     0.00243574
 ref:   3    -0.00000001    -0.02512504    -0.67329311     0.00000016    -0.00000079    -0.39824413     0.04362935

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -547.2981320697  0.0000E+00  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 25  2   -547.2432515982  4.2574E-07  1.9060E-07  4.9758E-04  1.0000E-04
 mr-sdci # 25  3   -547.1361306828  5.9526E-03  0.0000E+00  1.7410E-01  1.0000E-04
 mr-sdci # 25  4   -547.0983217248  2.5580E-13  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 25  5   -546.8802404896  1.9099E-11  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 25  6   -546.6460620382  9.9470E-01  0.0000E+00  8.0759E-01  1.0000E-04
 mr-sdci # 25  7   -544.6708006599 -5.7408E-01  0.0000E+00  1.4924E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  26

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223700
   ht   3     0.00000000     0.00000000   -72.64123738
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000330    -0.00730940     0.00701208     0.00000241    -0.00000056    -0.00006307
   ht   7    -0.00000490    -0.01409314     0.00545792     0.00000360    -0.00000091     0.00000405    -0.00004227
   ht   8    -0.00000526    -0.02286428    -0.00197441     0.00000387    -0.00000093    -0.00001210    -0.00000508    -0.00002396

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.966538      -2.922122E-08  -1.193430E-07  -9.741611E-02  -6.442761E-02  -4.542749E-08  -6.764473E-08  -7.248160E-08
 refs   2   2.511279E-08  -0.971162      -6.451505E-02   9.388149E-09  -2.834777E-07  -8.900688E-05  -1.860904E-04  -3.381673E-04
 refs   3   1.461218E-08  -2.472160E-02   0.724258      -2.430826E-07  -2.492509E-06   7.514124E-05  -2.588164E-04   6.135312E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -3.364663E-08  -2.688346E-06  -1.419186E-10   6.941209E-09   2.546882E-05  -1.637945E-04   4.731015E-05
 civs   2  -1.071807E-09   0.999858      -1.470390E-02  -6.090714E-07   3.866767E-05   0.148847      -0.677272       2.864687E-02
 civs   3  -1.524901E-10  -1.130268E-03  -0.978856       7.444732E-07  -3.797764E-05  -0.161904      -5.574003E-02  -0.214196    
 civs   4   1.798244E-13   2.291501E-08   1.371508E-06   -1.00000      -5.407159E-09  -1.984671E-05   1.216467E-04  -3.469257E-05
 civs   5  -7.970571E-14  -1.024674E-08  -1.003610E-06  -6.129264E-11    1.00000      -2.277897E-04  -2.358756E-05   7.848653E-06
 civs   6   4.282109E-07    1.36065        149.608       3.223280E-04  -0.117015       -509.283       -263.390       -970.627    
 civs   7  -2.654666E-08   -1.25125       -160.528      -2.033187E-04   0.195328        862.363        611.547       -844.762    
 civs   8   3.316302E-06   0.780406        93.1971       1.953179E-03  -0.204976       -837.868        1863.04        740.986    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.966538      -3.186911E-08  -4.582609E-08   9.741611E-02  -6.442760E-02   1.753887E-05  -1.642971E-05   4.700570E-06
 ref    2  -2.521669E-08  -0.971104       6.247119E-02  -1.172658E-07   7.996855E-06   3.408216E-02  -5.904335E-02  -2.098463E-02
 ref    3  -1.445361E-08  -2.506274E-02  -0.650073       9.940062E-07  -1.028768E-04  -0.433807      -8.739330E-02   3.532430E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96653819    -0.00000003    -0.00000005     0.09741611    -0.06442760     0.00001754    -0.00001643     0.00000470
 ref:   2    -0.00000003    -0.97110388     0.06247119    -0.00000012     0.00000800     0.03408216    -0.05904335    -0.02098463
 ref:   3    -0.00000001    -0.02506274    -0.65007319     0.00000099    -0.00010288    -0.43380744    -0.08739330     0.03532430

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -547.2981320697  5.6843E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 26  2   -547.2432517470  1.4875E-07  4.5810E-08  3.0889E-04  1.0000E-04
 mr-sdci # 26  3   -547.1378987815  1.7681E-03  0.0000E+00  1.6419E-01  1.0000E-04
 mr-sdci # 26  4   -547.0983217248  6.8212E-13  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 26  5   -546.8802404900  4.1830E-10  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 26  6   -546.8730651187  2.2700E-01  0.0000E+00  5.1507E-01  1.0000E-04
 mr-sdci # 26  7   -545.4269712859  7.5617E-01  0.0000E+00  1.2172E+00  1.0000E-04
 mr-sdci # 26  8   -544.5590312167 -2.8363E-01  0.0000E+00  1.3843E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  27

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223842
   ht   3     0.00000000     0.00000000   -72.65688546
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000406     0.00950224    -0.00205339     0.00000300     0.00000127    -0.00000349

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.966538      -3.186911E-08  -4.582609E-08   9.741611E-02  -6.442760E-02   5.592313E-08
 refs   2  -2.521669E-08  -0.971104       6.247119E-02  -1.172658E-07   7.996855E-06   1.425974E-04
 refs   3  -1.445361E-08  -2.506274E-02  -0.650073       9.940062E-07  -1.028768E-04  -1.475767E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       4.278042E-08  -3.116794E-06  -3.575823E-10  -1.399631E-08  -3.168455E-04
 civs   2   1.672411E-09   -1.00010       7.282142E-03   8.362121E-07   3.277722E-05   0.742567    
 civs   3  -1.080799E-10  -1.368105E-06   0.998373      -6.937018E-07  -1.017084E-05  -0.170317    
 civs   4   5.035262E-13  -2.979545E-08   1.806487E-06   -1.00000       1.077398E-08   2.357340E-04
 civs   5  -1.496722E-13   1.242600E-08  -1.672062E-06  -2.472032E-10   -1.00000       1.432987E-04
 civs   6   1.272489E-05  -0.768065        55.9485       6.418697E-03   0.251229        5687.12    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.966538       2.656554E-08   3.540366E-07  -9.741611E-02   6.442760E-02   2.551444E-05
 ref    2   2.540039E-08   0.971092       6.327593E-02   1.771703E-07  -4.637692E-06   7.921942E-02
 ref    3   1.429417E-08   2.507748E-02  -0.650024      -6.587320E-07   1.049595E-04   8.178791E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96653819     0.00000003     0.00000035    -0.09741611     0.06442760     0.00002551
 ref:   2     0.00000003     0.97109167     0.06327593     0.00000018    -0.00000464     0.07921942
 ref:   3     0.00000001     0.02507748    -0.65002361    -0.00000066     0.00010496     0.00817879

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -547.2981320697 -2.8422E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 27  2   -547.2432517822  3.5185E-08  1.4344E-08  1.2995E-04  1.0000E-04
 mr-sdci # 27  3   -547.1380756158  1.7683E-04  0.0000E+00  1.6322E-01  1.0000E-04
 mr-sdci # 27  4   -547.0983217248  2.2453E-12  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 27  5   -546.8802404931  3.0647E-09  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 27  6   -545.3107452689 -1.5623E+00  0.0000E+00  1.3900E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  28

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223842
   ht   3     0.00000000     0.00000000   -72.65688546
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000406     0.00950224    -0.00205339     0.00000300     0.00000127    -0.00000349
   ht   7    -0.00000583     0.00710113    -0.00073047     0.00000418     0.00000038    -0.00000102    -0.00000204

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.966538      -3.186911E-08  -4.582609E-08   9.741611E-02  -6.442760E-02   5.592313E-08   8.099208E-08
 refs   2  -2.521669E-08  -0.971104       6.247119E-02  -1.172658E-07   7.996855E-06   1.425974E-04   1.075819E-04
 refs   3  -1.445361E-08  -2.506274E-02  -0.650073       9.940062E-07  -1.028768E-04  -1.475767E-05  -2.963842E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       1.039925E-08   2.438975E-06   2.103236E-09  -4.993865E-08  -3.353433E-04   5.600362E-04
 civs   2   4.836482E-09    1.00005       2.342164E-03  -2.376549E-06   1.981002E-05   0.215554      -0.982489    
 civs   3  -6.165012E-10   1.268092E-05   0.998314      -2.454205E-07   1.692600E-05   4.128274E-02   0.178166    
 civs   4   2.869385E-12  -5.783674E-09  -1.259009E-06    1.00000       3.659270E-08   2.409686E-04  -4.100226E-04
 civs   5   1.033857E-12  -3.357292E-08  -4.867308E-06  -9.531923E-10    1.00000      -2.023659E-04  -1.226251E-04
 civs   6  -4.085741E-06    1.03426        85.4785       3.030927E-03  -0.657455       -3091.41       -4781.44    
 civs   7   5.505956E-05  -0.850149       -90.0009      -2.839745E-02    1.08190        6343.49       -3669.88    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966538      -3.123654E-08  -6.649717E-09   9.741611E-02  -6.442760E-02   5.327240E-05  -5.534747E-05
 ref    2   2.582225E-08  -0.971098       6.259792E-02  -4.475706E-07   1.245801E-05   3.487133E-02  -0.111405    
 ref    3   1.316159E-08  -2.506235E-02  -0.647630       2.010036E-06  -1.367397E-04  -0.174628       8.813512E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96653819    -0.00000003    -0.00000001     0.09741611    -0.06442760     0.00005327    -0.00005535
 ref:   2     0.00000003    -0.97109764     0.06259792    -0.00000045     0.00001246     0.03487133    -0.11140523
 ref:   3     0.00000001    -0.02506235    -0.64762971     0.00000201    -0.00013674    -0.17462817     0.08813512

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -547.2981320697 -1.4211E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 28  2   -547.2432517944  1.2195E-08  0.0000E+00  8.7488E-05  1.0000E-04
 mr-sdci # 28  3   -547.1381945112  1.1890E-04  1.4262E-02  1.6225E-01  1.0000E-04
 mr-sdci # 28  4   -547.0983217248  1.1127E-11  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 28  5   -546.8802405037  1.0653E-08  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 28  6   -546.4897448400  1.1790E+00  0.0000E+00  9.5881E-01  1.0000E-04
 mr-sdci # 28  7   -544.9161924730 -5.1078E-01  0.0000E+00  1.2664E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  29

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223842
   ht   3     0.00000000     0.00000000   -72.65688546
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922716
   ht   6    -0.00000406     0.00950224    -0.00205339     0.00000300     0.00000127    -0.00000349
   ht   7    -0.00000583     0.00710113    -0.00073047     0.00000418     0.00000038    -0.00000102    -0.00000204
   ht   8    -0.00167929     2.43452194     0.50814572     0.00104959     0.00082288    -0.00049059    -0.00013316    -1.03220405

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.966538      -3.186911E-08  -4.582609E-08   9.741611E-02  -6.442760E-02   5.592313E-08   8.099208E-08   2.284225E-05
 refs   2  -2.521669E-08  -0.971104       6.247119E-02  -1.172658E-07   7.996855E-06   1.425974E-04   1.075819E-04   3.508385E-02
 refs   3  -1.445361E-08  -2.506274E-02  -0.650073       9.940062E-07  -1.028768E-04  -1.475767E-05  -2.963842E-05   4.046001E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -1.008623E-08  -1.019712E-05  -2.465100E-09   4.958032E-08  -3.077266E-04  -3.040432E-04   5.337472E-04
 civs   2   5.070480E-09   -1.00005       2.077410E-02   2.874320E-06  -1.920185E-05   0.182201       0.377695      -0.950492    
 civs   3   1.671745E-10  -1.496064E-05    1.00037      -6.225224E-06  -1.808809E-05   5.464920E-02  -9.209300E-03   0.179721    
 civs   4   2.946871E-12   5.645792E-09  -6.883476E-08   -1.00000      -3.633559E-08   2.224653E-04   2.024476E-04  -3.928287E-04
 civs   5   1.134366E-12   3.337764E-08   3.708908E-06   1.225369E-09   -1.00000      -2.126433E-04   8.170572E-05  -1.144123E-04
 civs   6  -4.216905E-06   -1.03413        77.7632      -3.707656E-03   0.658142       -3004.60       -626.776       -4855.93    
 civs   7   5.485573E-05   0.850734       -116.325       2.805716E-02   -1.08483        6213.98        1743.28       -3538.05    
 civs   8   8.100308E-09  -1.592101E-05   0.657932       1.850747E-05   2.406260E-05  -0.957873        8.64957       0.862467    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.966538       3.122937E-08  -1.921038E-07  -9.741611E-02   6.442760E-02   5.131016E-05   2.429293E-05  -5.339920E-05
 ref    2   2.588753E-08   0.971097       6.397760E-02   7.615929E-08  -1.249487E-05   3.293541E-02   3.427237E-02  -0.108560    
 ref    3   1.298199E-08   2.506318E-02  -0.621914       2.952765E-06   1.385303E-04  -0.218679       0.304064       0.118410    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653819     0.00000003    -0.00000019    -0.09741611     0.06442760     0.00005131     0.00002429    -0.00005340
 ref:   2     0.00000003     0.97109747     0.06397760     0.00000008    -0.00001249     0.03293541     0.03427237    -0.10856011
 ref:   3     0.00000001     0.02506318    -0.62191435     0.00000295     0.00013853    -0.21867946     0.30406390     0.11841002

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -547.2981320697 -4.2633E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 29  2   -547.2432517944  5.2864E-12  0.0000E+00  8.7846E-05  1.0000E-04
 mr-sdci # 29  3   -547.1476052804  9.4108E-03  4.6443E-03  6.2757E-02  1.0000E-04
 mr-sdci # 29  4   -547.0983217248  8.9528E-12  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 29  5   -546.8802405037  1.0800E-11  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 29  6   -546.5025746479  1.2830E-02  0.0000E+00  9.6284E-01  1.0000E-04
 mr-sdci # 29  7   -545.4463220518  5.3013E-01  0.0000E+00  1.1516E+00  1.0000E-04
 mr-sdci # 29  8   -544.9109503410  3.5192E-01  0.0000E+00  1.2616E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  30

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.66659195
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922718
   ht   6    -0.00153479     1.44421437    -3.92890140     0.00092546     0.00112109    -1.23478883

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.966538       3.122937E-08  -1.921038E-07  -9.741611E-02   6.442760E-02  -2.120572E-05
 refs   2   2.588753E-08   0.971097       6.397760E-02   7.615929E-08  -1.249487E-05  -2.004467E-02
 refs   3   1.298199E-08   2.506318E-02  -0.621914       2.952765E-06   1.385303E-04  -0.115305    

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       1.333353E-09  -1.441587E-05  -1.433465E-09  -2.124921E-08  -1.790552E-04
 civs   2   1.484383E-10   -1.00000       1.357195E-02   1.349673E-06   2.000901E-05   0.168609    
 civs   3  -1.730511E-10   3.445690E-07   0.959838      -1.008192E-05  -7.195672E-05  -0.539023    
 civs   4   7.918415E-14  -6.135143E-10   2.964489E-06   -1.00000       1.488048E-08   1.166822E-04
 civs   5   5.904650E-14  -4.278219E-10   2.598541E-06   6.807799E-11   -1.00000       2.504923E-04
 civs   6   7.475247E-09  -6.326344E-05   0.683965       6.801072E-05   1.008160E-03    8.49518    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.966538      -3.114447E-08  -8.758056E-07   9.741611E-02  -6.442760E-02  -2.202034E-06
 ref    2  -2.590429E-08  -0.971097       6.087798E-02  -7.737646E-07   7.113705E-06  -4.103301E-02
 ref    3  -1.373258E-08  -2.505613E-02  -0.675461      -4.490816E-06  -2.095237E-04  -0.640084    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96653819    -0.00000003    -0.00000088     0.09741611    -0.06442760    -0.00000220
 ref:   2    -0.00000003    -0.97109740     0.06087798    -0.00000077     0.00000711    -0.04103301
 ref:   3    -0.00000001    -0.02505613    -0.67546132    -0.00000449    -0.00020952    -0.64008377

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -547.2981320697 -1.4211E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 30  2   -547.2432517944  3.1278E-11  0.0000E+00  8.8466E-05  1.0000E-04
 mr-sdci # 30  3   -547.1507921022  3.1868E-03  3.2440E-03  6.7855E-02  1.0000E-04
 mr-sdci # 30  4   -547.0983217248  2.9942E-11  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 30  5   -546.8802405069  3.1754E-09  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 30  6   -546.6559779909  1.5340E-01  0.0000E+00  9.4469E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  31

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.66659195
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922718
   ht   6    -0.00153479     1.44421437    -3.92890140     0.00092546     0.00112109    -1.23478883
   ht   7     0.00230206    -1.39420983     0.37646694    -0.00135317    -0.00025323     0.01444263    -0.29414834

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.966538       3.122937E-08  -1.921038E-07  -9.741611E-02   6.442760E-02  -2.120572E-05   3.185085E-05
 refs   2   2.588753E-08   0.971097       6.397760E-02   7.615929E-08  -1.249487E-05  -2.004467E-02   1.861443E-02
 refs   3   1.298199E-08   2.506318E-02  -0.621914       2.952765E-06   1.385303E-04  -0.115305      -7.902596E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       2.420608E-10   1.538559E-05  -4.384341E-09  -8.608157E-05   1.920358E-08   5.516764E-04
 civs   2   4.589710E-10    1.00000       3.729088E-03   2.188165E-06   2.063793E-03   1.659821E-06  -0.365391    
 civs   3   9.786353E-11  -1.855005E-07   0.878462       8.891452E-06   0.589725      -1.245780E-04   0.323411    
 civs   4   4.500172E-13  -1.202814E-10  -5.447017E-06   -1.00000       6.224320E-05  -1.183007E-08  -3.323177E-04
 civs   5   1.678420E-13   1.186052E-10  -8.223338E-06   1.877043E-09   1.706570E-04    1.00000      -1.131843E-04
 civs   6  -1.249376E-08   1.133795E-04    1.84683      -6.635195E-05   -7.08604       1.880428E-03   -4.38601    
 civs   7  -3.689808E-08   8.325031E-05    1.71799      -1.829232E-04   -7.44724       1.861136E-03    14.5261    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.966538       3.126213E-08   5.174054E-07   9.741611E-02   1.083046E-06   6.442760E-02   4.746857E-05
 ref    2  -2.587196E-08   0.971097       5.478344E-02   5.426067E-07   4.314452E-02  -2.190179E-05   2.417237E-02
 ref    3  -1.129917E-08   2.504958E-02  -0.772759       6.686257E-07   0.509201      -1.548159E-05   0.180642    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96653819     0.00000003     0.00000052     0.09741611     0.00000108     0.06442760     0.00004747
 ref:   2    -0.00000003     0.97109737     0.05478344     0.00000054     0.04314452    -0.00002190     0.02417237
 ref:   3    -0.00000001     0.02504958    -0.77275913     0.00000067     0.50920097    -0.00001548     0.18064201

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -547.2981320697  4.2633E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 31  2   -547.2432517944  1.7963E-11  0.0000E+00  8.8822E-05  1.0000E-04
 mr-sdci # 31  3   -547.1564557006  5.6636E-03  1.3829E-03  4.3147E-02  1.0000E-04
 mr-sdci # 31  4   -547.0983217249  4.6242E-11  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 31  5   -547.0136017832  1.3336E-01  0.0000E+00  2.7934E-01  1.0000E-04
 mr-sdci # 31  6   -546.8802404949  2.2426E-01  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 31  7   -545.3452105697 -1.0111E-01  0.0000E+00  1.2862E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  32

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.66659195
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.39922718
   ht   6    -0.00153479     1.44421437    -3.92890140     0.00092546     0.00112109    -1.23478883
   ht   7     0.00230206    -1.39420983     0.37646694    -0.00135317    -0.00025323     0.01444263    -0.29414834
   ht   8    -0.00557910     2.74528251     0.21160206     0.00344644     0.00072793    -0.10176787     0.06272909    -0.22425902

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.966538       3.122937E-08  -1.921038E-07  -9.741611E-02   6.442760E-02  -2.120572E-05   3.185085E-05  -7.668460E-05
 refs   2   2.588753E-08   0.971097       6.397760E-02   7.615929E-08  -1.249487E-05  -2.004467E-02   1.861443E-02  -3.974287E-02
 refs   3   1.298199E-08   2.506318E-02  -0.621914       2.952765E-06   1.385303E-04  -0.115305      -7.902596E-03  -5.144866E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -3.064493E-08  -2.065621E-05  -4.785983E-08   9.010411E-05  -2.472822E-08   2.787625E-04  -1.883984E-03
 civs   2   6.765959E-09    1.00002       2.202992E-02   2.496668E-05  -8.231466E-02   2.167698E-05  -4.282027E-02   0.947283    
 civs   3   3.236249E-10   1.292759E-06   0.867199      -3.048679E-05   0.594013      -1.067249E-04  -0.377687       4.172508E-02
 civs   4   6.917798E-12   1.429475E-08   3.067780E-06   -1.00000      -1.006248E-04   1.965150E-08  -1.913020E-04   1.183988E-03
 civs   5   1.713996E-12   3.725663E-09  -4.691346E-06   3.864421E-09   1.225625E-04    1.00000       1.233720E-05   2.393909E-04
 civs   6   9.328662E-09   1.891721E-04    1.98458       2.266744E-04   -6.85540       1.640389E-03    4.91134      -0.115966    
 civs   7   6.998848E-09   2.137886E-04    1.92530       2.066483E-04   -7.49913       1.678100E-03   -13.3428       -5.74658    
 civs   8   1.778695E-07   4.361984E-04   0.518082       6.475890E-04   -2.38446       5.639080E-04   -10.4950        22.2502    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.966538       2.904474E-08  -1.292911E-06   9.741611E-02   1.986438E-05   6.442759E-02   2.574424E-05  -1.657712E-04
 ref    2  -2.642217E-08   0.971096       5.234254E-02  -4.215570E-06   5.065531E-02  -2.232789E-05   4.540335E-03  -6.635642E-02
 ref    3  -1.505974E-08   2.503704E-02  -0.785483      -1.446833E-05   0.490502       1.399839E-07  -0.173047      -5.789723E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96653819     0.00000003    -0.00000129     0.09741611     0.00001986     0.06442759     0.00002574    -0.00016577
 ref:   2    -0.00000003     0.97109605     0.05234254    -0.00000422     0.05065531    -0.00002233     0.00454034    -0.06635642
 ref:   3    -0.00000002     0.02503704    -0.78548316    -0.00001447     0.49050234     0.00000014    -0.17304677    -0.05789723

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -547.2981320697  0.0000E+00  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 32  2   -547.2432517950  5.5233E-10  0.0000E+00  8.1038E-05  1.0000E-04
 mr-sdci # 32  3   -547.1571725213  7.1682E-04  4.5661E-04  2.2551E-02  1.0000E-04
 mr-sdci # 32  4   -547.0983217259  1.0019E-09  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 32  5   -547.0295920005  1.5990E-02  0.0000E+00  2.1074E-01  1.0000E-04
 mr-sdci # 32  6   -546.8802404958  9.1133E-10  0.0000E+00  3.2298E-01  1.0000E-04
 mr-sdci # 32  7   -545.3523492061  7.1386E-03  0.0000E+00  1.2673E+00  1.0000E-04
 mr-sdci # 32  8   -545.3131319233  4.0218E-01  0.0000E+00  1.4207E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  33

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67615920
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.54857867
   ht   6    -0.00416819    -2.11117246    -0.28950110     0.00248691     0.50605388    -0.11653584

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.966538       2.904474E-08  -1.292911E-06   9.741611E-02   1.986438E-05   5.770407E-05
 refs   2  -2.642217E-08   0.971096       5.234254E-02  -4.215570E-06   5.065531E-02   2.992708E-02
 refs   3  -1.505974E-08   2.503704E-02  -0.785483      -1.446833E-05   0.490502      -8.829388E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -8.559263E-10  -3.002048E-05  -5.536322E-08  -2.028737E-04  -2.139654E-03
 civs   2  -7.437888E-09    1.00000      -1.521729E-02  -2.806311E-05  -0.102834       -1.08455    
 civs   3  -1.891491E-10   1.984975E-08   0.997737      -1.135102E-05  -2.797074E-02  -0.161376    
 civs   4   7.822838E-12   4.352777E-10   1.135815E-05   -1.00000       1.669333E-04   1.301380E-03
 civs   5  -1.156857E-09  -1.114481E-07  -8.975550E-03  -3.654844E-05  -0.970867       0.353713    
 civs   6   2.563515E-07   1.495401E-05   0.524466       9.672009E-04    3.54421        37.3792    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.966538       2.912051E-08   8.856137E-07  -9.741610E-02   5.439410E-06   2.228557E-04
 ref    2   2.680262E-08   0.971096       5.268772E-02   3.463578E-06  -4.443780E-02   7.492310E-02
 ref    3   1.219123E-08   2.503682E-02  -0.793120      -3.785142E-06  -0.488110      -5.693465E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96653819     0.00000003     0.00000089    -0.09741610     0.00000544     0.00022286
 ref:   2     0.00000003     0.97109607     0.05268772     0.00000346    -0.04443780     0.07492310
 ref:   3     0.00000001     0.02503682    -0.79311988    -0.00000379    -0.48810985    -0.05693465

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -547.2981320697  0.0000E+00  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 33  2   -547.2432517950  2.1316E-13  0.0000E+00  8.1161E-05  1.0000E-04
 mr-sdci # 33  3   -547.1574120406  2.3952E-04  2.5823E-04  1.6908E-02  1.0000E-04
 mr-sdci # 33  4   -547.0983217266  7.1884E-10  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 33  5   -547.0405355678  1.0944E-02  0.0000E+00  1.8654E-01  1.0000E-04
 mr-sdci # 33  6   -545.8146129902 -1.0656E+00  0.0000E+00  1.2436E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  34

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67615920
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.54857867
   ht   6    -0.00416819    -2.11117246    -0.28950110     0.00248691     0.50605388    -0.11653584
   ht   7     0.00357786     1.66620425     0.15342516    -0.00221216     0.31485725     0.05944392    -0.06706530

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.966538       2.904474E-08  -1.292911E-06   9.741611E-02   1.986438E-05   5.770407E-05  -4.929930E-05
 refs   2  -2.642217E-08   0.971096       5.234254E-02  -4.215570E-06   5.065531E-02   2.992708E-02  -2.411429E-02
 refs   3  -1.505974E-08   2.503704E-02  -0.785483      -1.446833E-05   0.490502      -8.829388E-03  -1.747672E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       1.575070E-08   1.263863E-05  -6.499147E-08   4.018061E-05   2.660205E-04   2.845635E-03
 civs   2  -1.022160E-08    1.00001       8.482218E-03  -3.140652E-05   3.693642E-02   4.801300E-02    1.37775    
 civs   3  -7.813426E-10   2.464084E-06  -0.996885      -7.226764E-06   4.941857E-02  -5.932586E-02   0.161671    
 civs   4   1.186115E-11  -9.044921E-09  -3.381995E-06   -1.00000      -3.181351E-05  -2.095198E-04  -1.758588E-03
 civs   5  -1.266936E-09  -1.623095E-06   1.814616E-02  -1.625031E-05   0.886452       0.624897      -4.701171E-02
 civs   6   8.328330E-08   4.714521E-04   -1.10766       4.782923E-04   -7.78695        32.4994       -22.0546    
 civs   7  -3.407801E-07   8.698338E-04   -1.03319      -7.655389E-04   -8.25401        43.2759        32.2214    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966538       2.767453E-08   5.550110E-07  -9.741610E-02   1.086126E-05  -8.922116E-06  -2.831363E-04
 ref    2   2.710104E-08   0.971095      -5.125756E-02   5.289708E-06   4.935802E-02   4.219830E-03  -9.301739E-02
 ref    3   1.465635E-08   2.502878E-02   0.803735       8.502568E-06   0.480093      -8.267391E-03   2.286203E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96653819     0.00000003     0.00000056    -0.09741610     0.00001086    -0.00000892    -0.00028314
 ref:   2     0.00000003     0.97109529    -0.05125756     0.00000529     0.04935802     0.00421983    -0.09301739
 ref:   3     0.00000001     0.02502878     0.80373491     0.00000850     0.48009345    -0.00826739     0.02286203

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -547.2981320697 -2.8422E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 34  2   -547.2432517952  2.2476E-10  0.0000E+00  8.3244E-05  1.0000E-04
 mr-sdci # 34  3   -547.1576789454  2.6690E-04  6.6027E-05  1.0232E-02  1.0000E-04
 mr-sdci # 34  4   -547.0983217267  1.1154E-10  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 34  5   -547.0580121118  1.7477E-02  0.0000E+00  1.3533E-01  1.0000E-04
 mr-sdci # 34  6   -546.4642882184  6.4968E-01  0.0000E+00  7.1900E-01  1.0000E-04
 mr-sdci # 34  7   -545.4612693447  1.0892E-01  0.0000E+00  1.3339E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  35

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67615920
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.54857867
   ht   6    -0.00416819    -2.11117246    -0.28950110     0.00248691     0.50605388    -0.11653584
   ht   7     0.00357786     1.66620425     0.15342516    -0.00221216     0.31485725     0.05944392    -0.06706530
   ht   8     0.00139804     0.30555747    -0.25661354    -0.00086367     0.13141125     0.01092358    -0.00716355    -0.00746042

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.966538       2.904474E-08  -1.292911E-06   9.741611E-02   1.986438E-05   5.770407E-05  -4.929930E-05  -1.931633E-05
 refs   2  -2.642217E-08   0.971096       5.234254E-02  -4.215570E-06   5.065531E-02   2.992708E-02  -2.411429E-02  -4.356948E-03
 refs   3  -1.505974E-08   2.503704E-02  -0.785483      -1.446833E-05   0.490502      -8.829388E-03  -1.747672E-03  -4.451584E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -2.114911E-08  -2.625129E-05  -1.420657E-07   1.452449E-04  -6.273840E-04  -3.364299E-03   5.353465E-04
 civs   2  -1.488491E-08    1.00000      -1.080093E-02  -4.557754E-05   5.370769E-02  -9.771763E-02   -1.37485      -0.303169    
 civs   3   6.516038E-09   1.260229E-05   0.999757       1.568925E-06   2.971700E-02   0.142366       3.611648E-02  -0.472755    
 civs   4   2.620048E-11   8.040280E-09   6.737470E-06   -1.00000      -1.589503E-04   3.966069E-04   2.102178E-03  -3.802012E-04
 civs   5  -3.596968E-09  -5.409395E-06  -2.092499E-02  -6.146404E-05   0.871262       0.490019      -0.178341       0.496234    
 civs   6   2.549468E-07   7.350459E-04    1.23923       1.579405E-03   -8.76414        24.3397        9.96634        30.7707    
 civs   7   1.895560E-08   1.388043E-03    1.26619       9.508947E-04   -10.0921        36.2905       -39.0548        7.18148    
 civs   8  -1.887647E-06  -2.462626E-03  -0.913088      -5.124420E-03    7.26318       -52.9849       -45.5678        101.247    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.966538       3.081733E-08   2.988127E-07  -9.741610E-02  -6.319750E-06   8.067046E-05   3.301065E-04  -4.331156E-05
 ref    2   2.752345E-08   0.971096       5.131270E-02   3.587712E-06   4.727859E-02   2.152958E-02   9.632073E-02   1.255957E-02
 ref    3   1.392338E-08   2.502653E-02  -0.804916      -1.084867E-05   0.468046       8.362106E-02   3.283891E-02  -0.127793    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653819     0.00000003     0.00000030    -0.09741610    -0.00000632     0.00008067     0.00033011    -0.00004331
 ref:   2     0.00000003     0.97109580     0.05131270     0.00000359     0.04727859     0.02152958     0.09632073     0.01255957
 ref:   3     0.00000001     0.02502653    -0.80491622    -0.00001085     0.46804555     0.08362106     0.03283891    -0.12779274

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -547.2981320697  1.4211E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 35  2   -547.2432517957  4.9445E-10  0.0000E+00  8.3126E-05  1.0000E-04
 mr-sdci # 35  3   -547.1577392368  6.0291E-05  3.2316E-05  6.5274E-03  1.0000E-04
 mr-sdci # 35  4   -547.0983217284  1.6538E-09  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 35  5   -547.0614276490  3.4155E-03  0.0000E+00  1.1751E-01  1.0000E-04
 mr-sdci # 35  6   -546.7039509683  2.3966E-01  0.0000E+00  5.0800E-01  1.0000E-04
 mr-sdci # 35  7   -545.4720505480  1.0781E-02  0.0000E+00  1.3464E+00  1.0000E-04
 mr-sdci # 35  8   -545.4086703916  9.5538E-02  0.0000E+00  1.1944E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  36

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67672591
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58041432
   ht   6    -0.00213482     0.06700647    -0.24693004     0.00131397     0.09781547    -0.00373611

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.966538       3.081733E-08   2.988127E-07  -9.741610E-02  -6.319750E-06  -2.941266E-05
 refs   2   2.752345E-08   0.971096       5.131270E-02   3.587712E-06   4.727859E-02  -7.255474E-04
 refs   3   1.392338E-08   2.502653E-02  -0.804916      -1.084867E-05   0.468046      -4.390351E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -1.068225E-07  -1.703391E-05  -2.883817E-07  -1.558539E-04  -4.754842E-03
 civs   2   4.489139E-09    1.00000       5.336271E-04   9.044225E-06   4.889405E-03   0.149334    
 civs   3  -1.537271E-08  -1.100324E-05   0.998018      -3.876848E-05  -1.987249E-02  -0.554500    
 civs   4   7.535658E-11   5.304011E-08   5.503768E-06   -1.00000       1.730612E-04   2.991657E-03
 civs   5   7.585375E-10  -7.367586E-07  -9.147170E-04  -6.172900E-05  -0.992317       0.250714    
 civs   6   4.853998E-06   3.644314E-03   0.581056       9.836991E-03    5.31629        162.185    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.966538       2.171091E-08  -8.586295E-07   9.741607E-02  -1.632113E-05  -4.677439E-04
 ref    2  -2.743882E-08   0.971096       5.126438E-02  -6.849881E-06  -4.704422E-02   1.074507E-02
 ref    3  -2.239302E-08   2.501913E-02  -0.806287      -2.979944E-05  -0.471672      -0.144642    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96653819     0.00000002    -0.00000086     0.09741607    -0.00001632    -0.00046774
 ref:   2    -0.00000003     0.97109582     0.05126438    -0.00000685    -0.04704422     0.01074507
 ref:   3    -0.00000002     0.02501913    -0.80628684    -0.00002980    -0.47167212    -0.14464151

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -547.2981320697 -1.4211E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 36  2   -547.2432517965  7.8671E-10  0.0000E+00  7.6578E-05  1.0000E-04
 mr-sdci # 36  3   -547.1577580145  1.8778E-05  1.0737E-05  3.6633E-03  1.0000E-04
 mr-sdci # 36  4   -547.0983217334  5.0373E-09  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 36  5   -547.0629228431  1.4952E-03  0.0000E+00  1.2007E-01  1.0000E-04
 mr-sdci # 36  6   -545.6701243020 -1.0338E+00  0.0000E+00  1.2781E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  37

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67672591
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58041432
   ht   6    -0.00213482     0.06700647    -0.24693004     0.00131397     0.09781547    -0.00373611
   ht   7     0.00241062    -0.13946425     0.08400561    -0.00146276    -0.04910384     0.00066569    -0.00146401

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.966538       3.081733E-08   2.988127E-07  -9.741610E-02  -6.319750E-06  -2.941266E-05   3.331833E-05
 refs   2   2.752345E-08   0.971096       5.131270E-02   3.587712E-06   4.727859E-02  -7.255474E-04   2.046034E-03
 refs   3   1.392338E-08   2.502653E-02  -0.804916      -1.084867E-05   0.468046      -4.390351E-03   1.680970E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000       1.048161E-07  -1.280819E-05   1.772748E-06   2.874156E-04   4.518836E-03  -8.210021E-03
 civs   2  -3.844434E-08   0.999989       1.525163E-03  -1.130810E-04  -2.585082E-02  -0.338272       0.374577    
 civs   3   1.924639E-08  -9.189364E-06  -0.998027       2.279192E-05  -2.221250E-02  -8.303807E-02  -0.596903    
 civs   4  -3.281193E-10  -4.543573E-08   2.351583E-06   0.999999      -3.633492E-04  -2.948260E-03   5.111928E-03
 civs   5  -1.723100E-08  -9.189379E-06   2.958887E-03  -1.968606E-04  -0.992292       7.687667E-02   0.282409    
 civs   6   1.510849E-06   6.438815E-03   -1.00540       1.340454E-02    11.8386        98.6341        129.750    
 civs   7   2.079043E-05   8.868801E-03   -1.27729       6.542253E-02    19.1664        223.851       -133.095    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966538       4.010067E-08  -1.152001E-06  -9.741591E-02   5.424860E-05   4.763098E-04  -8.154190E-04
 ref    2   3.180496E-08   0.971098      -5.147441E-02   9.768559E-06  -4.253198E-02   5.732238E-02  -1.998194E-02
 ref    3   1.771956E-08   2.501599E-02   0.807018      -7.304131E-05  -0.466963       3.760330E-02  -0.171364    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96653819     0.00000004    -0.00000115    -0.09741591     0.00005425     0.00047631    -0.00081542
 ref:   2     0.00000003     0.97109762    -0.05147441     0.00000977    -0.04253198     0.05732238    -0.01998194
 ref:   3     0.00000002     0.02501599     0.80701831    -0.00007304    -0.46696317     0.03760330    -0.17136437

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -547.2981320697  2.8422E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 37  2   -547.2432517973  7.8201E-10  0.0000E+00  7.3365E-05  1.0000E-04
 mr-sdci # 37  3   -547.1577717290  1.3714E-05  5.6538E-06  2.7131E-03  1.0000E-04
 mr-sdci # 37  4   -547.0983217636  3.0244E-08  0.0000E+00  1.5172E-01  1.0000E-04
 mr-sdci # 37  5   -547.0655147883  2.5919E-03  0.0000E+00  1.1811E-01  1.0000E-04
 mr-sdci # 37  6   -546.6821964437  1.0121E+00  0.0000E+00  6.8568E-01  1.0000E-04
 mr-sdci # 37  7   -545.3142463687 -1.5780E-01  0.0000E+00  1.3009E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  38

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67672591
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730840
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58041432
   ht   6    -0.00213482     0.06700647    -0.24693004     0.00131397     0.09781547    -0.00373611
   ht   7     0.00241062    -0.13946425     0.08400561    -0.00146276    -0.04910384     0.00066569    -0.00146401
   ht   8     0.00555916    -0.20015808    -0.01589742    -0.00338716    -0.03318138     0.00041035    -0.00039526    -0.00109569

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.966538       3.081733E-08   2.988127E-07  -9.741610E-02  -6.319750E-06  -2.941266E-05   3.331833E-05   7.670044E-05
 refs   2   2.752345E-08   0.971096       5.131270E-02   3.587712E-06   4.727859E-02  -7.255474E-04   2.046034E-03   2.859894E-03
 refs   3   1.392338E-08   2.502653E-02  -0.804916      -1.084867E-05   0.468046      -4.390351E-03   1.680970E-03   2.320124E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -1.264414E-07   2.387323E-05  -1.146759E-05  -4.239676E-04   3.641142E-03  -2.284178E-02   1.792336E-02
 civs   2   1.779888E-07   0.999996       4.355668E-04   3.851201E-04  -5.781839E-03   3.343420E-02   0.921559      -0.643710    
 civs   3   1.825202E-09  -8.053076E-06  -0.998147      -1.596558E-05  -2.075672E-02   3.641622E-02  -0.366670      -0.512629    
 civs   4   2.541645E-09   6.042794E-08  -6.306488E-06  -0.999993       5.686650E-04  -2.552239E-03   1.422780E-02  -1.114280E-02
 civs   5   2.317680E-08  -9.287785E-06   3.254795E-03  -1.848821E-04  -0.984315      -0.155132       0.299958       6.439019E-02
 civs   6   6.017494E-06   6.905714E-03   -1.10560       2.284935E-02    14.3296       -87.0020        70.1473        122.622    
 civs   7   5.482189E-06   1.040588E-02   -1.57890       3.449655E-02    26.3826       -211.086       -149.233       -25.8852    
 civs   8  -6.649877E-05  -3.517009E-03   0.572822      -0.156402       -11.4911        105.818       -207.547        293.085    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.966538       2.103234E-08   1.069267E-06   9.741496E-02  -6.322642E-05   3.725314E-04  -2.264886E-03   1.771993E-03
 ref    2  -3.681853E-08   0.971097      -5.143084E-02  -3.244910E-05  -4.249744E-02  -3.913461E-02  -5.950398E-02   4.789903E-02
 ref    3  -3.272235E-08   2.501493E-02   0.807292      -1.318116E-04  -0.465371      -4.939226E-02  -0.148386      -8.721411E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96653818     0.00000002     0.00000107     0.09741496    -0.00006323     0.00037253    -0.00226489     0.00177199
 ref:   2    -0.00000004     0.97109738    -0.05143084    -0.00003245    -0.04249744    -0.03913461    -0.05950398     0.04789903
 ref:   3    -0.00000003     0.02501493     0.80729153    -0.00013181    -0.46537137    -0.04939226    -0.14838560    -0.08721411

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -547.2981320697  5.6843E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 38  2   -547.2432517974  1.3516E-10  0.0000E+00  7.2729E-05  1.0000E-04
 mr-sdci # 38  3   -547.1577749676  3.2386E-06  2.5740E-06  1.7960E-03  1.0000E-04
 mr-sdci # 38  4   -547.0983219807  2.1704E-07  0.0000E+00  1.5173E-01  1.0000E-04
 mr-sdci # 38  5   -547.0666664859  1.1517E-03  0.0000E+00  1.1379E-01  1.0000E-04
 mr-sdci # 38  6   -546.8058168804  1.2362E-01  0.0000E+00  4.8951E-01  1.0000E-04
 mr-sdci # 38  7   -545.3703915970  5.6145E-02  0.0000E+00  1.3240E+00  1.0000E-04
 mr-sdci # 38  8   -545.2033803606 -2.0529E-01  0.0000E+00  1.3012E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  39

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676164
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730865
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58565316
   ht   6     0.00422688     0.15786048    -0.00874052    -0.00256683     0.02970688    -0.00064005

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.966538       2.103234E-08   1.069267E-06   9.741496E-02  -6.322642E-05  -5.836875E-05
 refs   2  -3.681853E-08   0.971097      -5.143084E-02  -3.244910E-05  -4.249744E-02  -2.263018E-03
 refs   3  -3.272235E-08   2.501493E-02   0.807292      -1.318116E-04  -0.465371       3.169220E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -2.807339E-07  -3.023945E-05   7.543580E-06   8.673373E-04   2.909555E-02
 civs   2  -1.381863E-07   0.999990      -1.130405E-03   2.819729E-04   3.241978E-02    1.08746    
 civs   3   6.491248E-09   4.358750E-07  -0.999937      -2.125151E-05  -2.223505E-03  -6.129039E-02
 civs   4   2.060530E-09   1.509526E-07   1.316575E-05   0.999995      -8.201232E-04  -1.796380E-02
 civs   5  -6.940161E-09  -7.688715E-08   1.835101E-04  -2.314188E-04  -0.993455       0.234459    
 civs   6  -6.370762E-05  -4.836786E-03  -0.520963       0.129958        14.9422        501.236    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.966538       4.671959E-08   1.382074E-06   9.741423E-02  -5.092203E-05  -2.899379E-03
 ref    2   4.675869E-08   0.971098       5.150100E-02  -4.179654E-05   4.000204E-02  -8.508759E-02
 ref    3   1.754509E-08   2.501352E-02  -0.807519       6.968747E-06   0.466077       2.746837E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96653818     0.00000005     0.00000138     0.09741423    -0.00005092    -0.00289938
 ref:   2     0.00000005     0.97109811     0.05150100    -0.00004180     0.04000204    -0.08508759
 ref:   3     0.00000002     0.02501352    -0.80751925     0.00000697     0.46607697     0.02746837

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -547.2981320697  7.1054E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 39  2   -547.2432517975  1.2419E-10  0.0000E+00  7.2813E-05  1.0000E-04
 mr-sdci # 39  3   -547.1577763085  1.3410E-06  1.2883E-06  1.2881E-03  1.0000E-04
 mr-sdci # 39  4   -547.0983220585  7.7785E-08  0.0000E+00  1.5174E-01  1.0000E-04
 mr-sdci # 39  5   -547.0677023606  1.0359E-03  0.0000E+00  1.0738E-01  1.0000E-04
 mr-sdci # 39  6   -545.9010437555 -9.0477E-01  0.0000E+00  1.2324E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  40

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676164
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730865
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58565316
   ht   6     0.00422688     0.15786048    -0.00874052    -0.00256683     0.02970688    -0.00064005
   ht   7     0.00383782     0.11246255    -0.01770708    -0.00234377    -0.00700985    -0.00031356    -0.00029832

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.966538       2.103234E-08   1.069267E-06   9.741496E-02  -6.322642E-05  -5.836875E-05  -5.299690E-05
 refs   2  -3.681853E-08   0.971097      -5.143084E-02  -3.244910E-05  -4.249744E-02  -2.263018E-03  -1.604645E-03
 refs   3  -3.272235E-08   2.501493E-02   0.807292      -1.318116E-04  -0.465371       3.169220E-04   1.153250E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       5.382314E-09   1.050653E-05  -1.144141E-05   3.271306E-04   1.121301E-03   4.318092E-02
 civs   2  -1.615324E-07   0.999995       7.469984E-04  -3.535758E-04   2.223717E-02  -0.203713        1.35785    
 civs   3   1.525721E-08  -1.882094E-06    1.00009       4.884502E-05   1.714179E-03  -7.847271E-02  -0.170881    
 civs   4   2.696093E-09  -3.568528E-09  -4.672414E-06  -0.999993      -2.938958E-04  -8.186730E-04  -2.666128E-02
 civs   5   1.774231E-10  -1.445728E-06  -2.274444E-04   9.919406E-05  -0.985390      -0.312594       3.054791E-02
 civs   6  -4.251524E-05  -1.042106E-02   0.938513      -3.861088E-02    27.0492       -506.328        195.906    
 civs   7  -4.484161E-05   1.157933E-02  -0.834291      -0.174565       -23.5842        578.931        603.532    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966538       2.057229E-08   2.185414E-07  -9.741384E-02   2.091797E-05  -1.040985E-04  -4.283399E-03
 ref    2   4.733015E-08   0.971098      -5.148546E-02   4.985583E-05   4.001421E-02   3.634676E-02  -8.569239E-02
 ref    3   2.227035E-08   2.501200E-02   0.807689       8.386782E-05   0.466365      -1.667497E-02   1.349239E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96653818     0.00000002     0.00000022    -0.09741384     0.00002092    -0.00010410    -0.00428340
 ref:   2     0.00000005     0.97109796    -0.05148546     0.00004986     0.04001421     0.03634676    -0.08569239
 ref:   3     0.00000002     0.02501200     0.80768884     0.00008387     0.46636529    -0.01667497     0.01349239

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -547.2981320697 -2.8422E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 40  2   -547.2432517978  2.2675E-10  0.0000E+00  7.1881E-05  1.0000E-04
 mr-sdci # 40  3   -547.1577773833  1.0748E-06  3.2341E-07  6.5118E-04  1.0000E-04
 mr-sdci # 40  4   -547.0983221018  4.3293E-08  0.0000E+00  1.5174E-01  1.0000E-04
 mr-sdci # 40  5   -547.0684851391  7.8278E-04  0.0000E+00  1.0177E-01  1.0000E-04
 mr-sdci # 40  6   -546.3924483960  4.9140E-01  0.0000E+00  8.7971E-01  1.0000E-04
 mr-sdci # 40  7   -545.3673607238 -3.0309E-03  0.0000E+00  1.3251E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  41

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676164
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730865
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58565316
   ht   6     0.00422688     0.15786048    -0.00874052    -0.00256683     0.02970688    -0.00064005
   ht   7     0.00383782     0.11246255    -0.01770708    -0.00234377    -0.00700985    -0.00031356    -0.00029832
   ht   8     0.00140109     0.02877538     0.01504535    -0.00086654    -0.01449575    -0.00008855    -0.00004601    -0.00005088

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.966538       2.103234E-08   1.069267E-06   9.741496E-02  -6.322642E-05  -5.836875E-05  -5.299690E-05  -1.934358E-05
 refs   2  -3.681853E-08   0.971097      -5.143084E-02  -3.244910E-05  -4.249744E-02  -2.263018E-03  -1.604645E-03  -4.262853E-04
 refs   3  -3.272235E-08   2.501493E-02   0.807292      -1.318116E-04  -0.465371       3.169220E-04   1.153250E-04  -3.711856E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -1.191810E-07  -2.335356E-05  -2.667880E-05   3.860775E-04  -1.088683E-02   1.454122E-02  -4.493744E-02
 civs   2  -2.564631E-07   0.999993      -1.017511E-03  -6.858028E-04   2.354375E-02  -0.321070      -8.698221E-03   -1.36567    
 civs   3  -7.388573E-08  -4.230865E-06   -1.00033      -2.386865E-04   2.846509E-03  -0.247446       0.249835       0.145081    
 civs   4   5.075439E-09   5.200385E-08   7.146226E-06  -0.999983      -3.810132E-04   7.626279E-03  -9.448293E-03   2.779251E-02
 civs   5   4.638117E-08  -3.101411E-07   3.362791E-04   2.443545E-04  -0.985734      -4.591276E-02  -0.505024       2.034390E-02
 civs   6  -7.345453E-05  -1.135347E-02   -1.05986      -0.208373        27.7571       -313.266       -461.676       -151.708    
 civs   7   2.899396E-05   1.364502E-02    1.07674       0.142896       -24.8741        451.198        309.817       -635.702    
 civs   8  -3.589360E-04  -9.320510E-03  -0.965776       -1.14940        4.46152       -856.656        1299.91       -136.510    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.966538       3.075618E-08   5.133100E-07  -9.741229E-02   1.017140E-05   1.166656E-03  -1.450291E-03   4.458327E-03
 ref    2   6.230971E-08   0.971098       5.152790E-02   1.005831E-04   3.980544E-02   5.298138E-02  -6.329173E-03   8.705692E-02
 ref    3   5.837073E-08   2.501291E-02  -0.807595       1.853304E-04   0.465891       8.430463E-02  -0.156596       2.768238E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653818     0.00000003     0.00000051    -0.09741229     0.00001017     0.00116666    -0.00145029     0.00445833
 ref:   2     0.00000006     0.97109836     0.05152790     0.00010058     0.03980544     0.05298138    -0.00632917     0.08705692
 ref:   3     0.00000006     0.02501291    -0.80759466     0.00018533     0.46589146     0.08430463    -0.15659586     0.00276824

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -547.2981320697  8.5265E-14  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 41  2   -547.2432517978  3.3850E-11  0.0000E+00  7.1855E-05  1.0000E-04
 mr-sdci # 41  3   -547.1577776957  3.1234E-07  2.2768E-07  5.6939E-04  1.0000E-04
 mr-sdci # 41  4   -547.0983224906  3.8885E-07  0.0000E+00  1.5175E-01  1.0000E-04
 mr-sdci # 41  5   -547.0684905731  5.4339E-06  0.0000E+00  1.0164E-01  1.0000E-04
 mr-sdci # 41  6   -546.7849955657  3.9255E-01  0.0000E+00  5.9856E-01  1.0000E-04
 mr-sdci # 41  7   -545.4797333113  1.1237E-01  0.0000E+00  1.1736E+00  1.0000E-04
 mr-sdci # 41  8   -545.3661627047  1.6278E-01  0.0000E+00  1.3259E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  42

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676437
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730916
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58747725
   ht   6    -0.00199253     0.00508641    -0.01826459     0.00121143     0.00577794    -0.00002310

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.966538       3.075618E-08   5.133100E-07  -9.741229E-02   1.017140E-05  -2.751026E-05
 refs   2   6.230971E-08   0.971098       5.152790E-02   1.005831E-04   3.980544E-02  -5.246033E-05
 refs   3   5.837073E-08   2.501291E-02  -0.807595       1.853304E-04   0.465891      -3.059425E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -3.286002E-07   1.599918E-05  -2.647152E-05  -3.741742E-04  -5.497340E-02
 civs   2   3.906683E-08    1.00000      -4.084139E-05   6.759597E-05   9.555412E-04   0.140434    
 civs   3  -1.393609E-07  -2.986387E-06  -0.999853      -2.468331E-04  -3.471490E-03  -0.505176    
 civs   4   8.297404E-09   1.703132E-07  -6.192397E-06  -0.999984       3.943425E-04   3.398565E-02
 civs   5   3.217223E-08   6.097540E-07  -1.358547E-05  -8.606767E-05  -0.998889       0.166585    
 civs   6   5.581416E-04   1.201061E-02  -0.584727       0.967445        13.6747        2009.02    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.966538       1.359927E-09   7.120192E-07   9.740968E-02  -6.311756E-05  -5.443862E-03
 ref    2  -5.955178E-08   0.971098      -5.152985E-02  -1.018363E-04  -3.972952E-02   1.158487E-02
 ref    3  -1.006154E-07   2.501195E-02   0.807647      -3.203763E-04  -0.466730      -0.125537    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96653818     0.00000000     0.00000071     0.09740968    -0.00006312    -0.00544386
 ref:   2    -0.00000006     0.97109841    -0.05152985    -0.00010184    -0.03972952     0.01158487
 ref:   3    -0.00000010     0.02501195     0.80764749    -0.00032038    -0.46673013    -0.12553679

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -547.2981320697  1.4211E-13  0.0000E+00  5.4675E-05  1.0000E-04
 mr-sdci # 42  2   -547.2432517979  5.9174E-11  0.0000E+00  7.1242E-05  1.0000E-04
 mr-sdci # 42  3   -547.1577778288  1.3313E-07  6.9474E-08  2.8790E-04  1.0000E-04
 mr-sdci # 42  4   -547.0983228407  3.5013E-07  0.0000E+00  1.5178E-01  1.0000E-04
 mr-sdci # 42  5   -547.0685593117  6.8739E-05  0.0000E+00  1.0333E-01  1.0000E-04
 mr-sdci # 42  6   -545.5848661952 -1.2001E+00  0.0000E+00  1.3190E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  43

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676437
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730916
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58747725
   ht   6    -0.00199253     0.00508641    -0.01826459     0.00121143     0.00577794    -0.00002310
   ht   7    -0.00228383     0.01219417    -0.00658068     0.00140057     0.00490478    -0.00000413    -0.00001090

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.966538       3.075618E-08   5.133100E-07  -9.741229E-02   1.017140E-05  -2.751026E-05  -3.153760E-05
 refs   2   6.230971E-08   0.971098       5.152790E-02   1.005831E-04   3.980544E-02  -5.246033E-05  -1.798462E-04
 refs   3   5.837073E-08   2.501291E-02  -0.807595       1.853304E-04   0.465891      -3.059425E-04  -1.531988E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       2.987749E-07  -1.197724E-05  -2.368893E-04   1.616611E-03  -5.570735E-02   8.919544E-02
 civs   2   4.601599E-07   0.999997       1.369898E-04   1.415080E-03  -1.181962E-02   0.380605      -0.346666    
 civs   3  -1.942719E-07  -2.509322E-06  -0.999870      -3.396653E-04  -2.619563E-03   2.696527E-02   0.551125    
 civs   4   4.301130E-08  -1.299688E-07   1.605166E-06  -0.999849      -2.199049E-03   3.737013E-02  -5.536305E-02
 civs   5   2.089782E-07  -1.189635E-06   9.383275E-05   1.571231E-03   -1.00205       6.403223E-02  -0.230192    
 civs   6  -2.565703E-04   1.987665E-02  -0.955670       -1.95447        41.6694       -1086.68       -1702.78    
 civs   7   2.853034E-03  -2.686912E-02    1.21568        9.25845       -87.9003        2724.26       -1358.31    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.966538       5.520163E-08  -1.141139E-06   9.738831E-02   2.673323E-04  -5.818292E-03   8.861910E-03
 ref    2  -1.167843E-07   0.971099      -5.155292E-02  -2.439096E-04  -3.787791E-02  -5.939383E-02   1.619825E-02
 ref    3  -1.511842E-07   2.501234E-02   0.807643       3.599937E-05  -0.464312      -6.731083E-02   0.168032    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96653818     0.00000006    -0.00000114     0.09738831     0.00026733    -0.00581829     0.00886191
 ref:   2    -0.00000012     0.97109895    -0.05155292    -0.00024391    -0.03787791    -0.05939383     0.01619825
 ref:   3    -0.00000015     0.02501234     0.80764273     0.00003600    -0.46431158    -0.06731083     0.16803192

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -547.2981320697  5.8265E-13  0.0000E+00  5.4667E-05  1.0000E-04
 mr-sdci # 43  2   -547.2432517979  4.8757E-11  0.0000E+00  7.0498E-05  1.0000E-04
 mr-sdci # 43  3   -547.1577779133  8.4458E-08  4.4120E-08  2.4208E-04  1.0000E-04
 mr-sdci # 43  4   -547.0983270862  4.2454E-06  0.0000E+00  1.5197E-01  1.0000E-04
 mr-sdci # 43  5   -547.0689188108  3.5950E-04  0.0000E+00  1.0111E-01  1.0000E-04
 mr-sdci # 43  6   -546.7056111147  1.1207E+00  0.0000E+00  7.4598E-01  1.0000E-04
 mr-sdci # 43  7   -545.3064701752 -1.7326E-01  0.0000E+00  1.3188E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  44

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676437
   ht   4     0.00000000     0.00000000     0.00000000   -72.61730916
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58747725
   ht   6    -0.00199253     0.00508641    -0.01826459     0.00121143     0.00577794    -0.00002310
   ht   7    -0.00228383     0.01219417    -0.00658068     0.00140057     0.00490478    -0.00000413    -0.00001090
   ht   8     0.00515161    -0.01654045    -0.00199513    -0.00313926    -0.00033284     0.00000230     0.00000323    -0.00000845

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.966538       3.075618E-08   5.133100E-07  -9.741229E-02   1.017140E-05  -2.751026E-05  -3.153760E-05   7.110491E-05
 refs   2   6.230971E-08   0.971098       5.152790E-02   1.005831E-04   3.980544E-02  -5.246033E-05  -1.798462E-04   2.354265E-04
 refs   3   5.837073E-08   2.501291E-02  -0.807595       1.853304E-04   0.465891      -3.059425E-04  -1.531988E-04  -1.434954E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       4.827494E-08   1.918939E-05  -1.115403E-03  -1.867552E-04   2.682557E-02  -0.235102      -0.187908    
 civs   2   1.545734E-06   0.999998       6.234224E-05   3.377113E-03  -7.950389E-03   0.107805       0.836633       0.609137    
 civs   3   7.624532E-08  -2.345381E-06  -0.999890       2.126646E-04  -1.471872E-03  -2.115318E-02  -0.337464       0.484725    
 civs   4   2.509344E-07  -1.469880E-08  -6.019035E-06  -0.999310       2.310582E-04  -1.827876E-02   0.146441       0.117013    
 civs   5   5.214328E-08  -1.304230E-06   1.169416E-04  -1.323151E-06   -1.00261       5.034362E-02   0.194784      -0.125814    
 civs   6   6.825974E-04   2.053848E-02   -1.06327        1.83489        50.2667       -990.274        1048.59       -1449.69    
 civs   7  -5.937114E-04  -2.918915E-02    1.56832       -2.60392       -114.399        2636.44        1498.32       -310.874    
 civs   8  -7.027591E-03  -4.314038E-03   0.555286       -16.2116       -33.9138        1164.97       -2253.33       -3354.59    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.966538       3.430147E-08   8.002696E-07   9.730212E-02  -3.861495E-05   2.783522E-03  -2.335159E-02  -1.862114E-02
 ref    2  -1.387336E-07   0.971099      -5.155257E-02  -2.546808E-04  -3.775270E-02  -4.233902E-02  -5.213800E-02  -4.628641E-02
 ref    3  -7.398014E-08   2.501239E-02   0.807639      -2.029197E-04  -0.463484      -7.441880E-02  -0.133778       0.104466    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96653814     0.00000003     0.00000080     0.09730212    -0.00003861     0.00278352    -0.02335159    -0.01862114
 ref:   2    -0.00000014     0.97109894    -0.05155257    -0.00025468    -0.03775270    -0.04233902    -0.05213800    -0.04628641
 ref:   3    -0.00000007     0.02501239     0.80763875    -0.00020292    -0.46348350    -0.07441880    -0.13377775     0.10446559

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -547.2981320697  4.6896E-12  0.0000E+00  5.4647E-05  1.0000E-04
 mr-sdci # 44  2   -547.2432517979  1.6769E-12  0.0000E+00  7.0494E-05  1.0000E-04
 mr-sdci # 44  3   -547.1577779378  2.4499E-08  2.2360E-08  1.7566E-04  1.0000E-04
 mr-sdci # 44  4   -547.0983461512  1.9065E-05  0.0000E+00  1.5282E-01  1.0000E-04
 mr-sdci # 44  5   -547.0689981741  7.9363E-05  0.0000E+00  1.0034E-01  1.0000E-04
 mr-sdci # 44  6   -546.8285961095  1.2298E-01  0.0000E+00  5.4141E-01  1.0000E-04
 mr-sdci # 44  7   -545.3806960982  7.4226E-02  0.0000E+00  1.3457E+00  1.0000E-04
 mr-sdci # 44  8   -545.1440812739 -2.2208E-01  0.0000E+00  1.3199E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  45

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676461
   ht   4     0.00000000     0.00000000     0.00000000   -72.61733282
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58798485
   ht   6     0.00316173     0.01124225    -0.00093933    -0.00193144     0.00211337    -0.00000443

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.966538       3.430147E-08   8.002696E-07   9.730212E-02  -3.861495E-05  -4.365067E-05
 refs   2  -1.387336E-07   0.971099      -5.155257E-02  -2.546808E-04  -3.775270E-02  -1.613426E-04
 refs   3  -7.398014E-08   2.501239E-02   0.807639      -2.029197E-04  -0.463484       2.423923E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -2.204505E-07  -2.417257E-05  -3.424702E-04   1.373108E-03   0.235681    
 civs   2  -6.577521E-07   0.999999      -8.602017E-05  -1.218690E-03   4.886221E-03   0.838656    
 civs   3   5.434422E-08   6.429649E-08  -0.999993       1.049099E-04  -4.166967E-04  -7.025590E-02
 civs   4   1.068075E-07   1.244950E-07   1.198907E-05  -0.999789      -1.165374E-03  -0.145799    
 civs   5  -1.020374E-07  -1.134952E-07  -8.828261E-06   8.639804E-05  -0.999063       0.163772    
 civs   6  -4.257222E-03  -5.077571E-03  -0.556731       -7.88754        31.6244        5427.95    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.966538       5.498504E-08   1.304627E-06  -9.726832E-02  -1.280799E-04  -2.333160E-02
 ref    2   1.878861E-07   0.971099       5.155882E-02   3.350850E-04   3.738174E-02  -6.386591E-02
 ref    3   4.549785E-08   2.501235E-02  -0.807644       2.589184E-05   0.463602       1.992897E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96653812     0.00000005     0.00000130    -0.09726832    -0.00012808    -0.02333160
 ref:   2     0.00000019     0.97109900     0.05155882     0.00033508     0.03738174    -0.06386591
 ref:   3     0.00000005     0.02501235    -0.80764449     0.00002589     0.46360157     0.01992897

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -547.2981320697  7.1054E-13  0.0000E+00  5.4668E-05  1.0000E-04
 mr-sdci # 45  2   -547.2432517979  1.1369E-12  0.0000E+00  7.0559E-05  1.0000E-04
 mr-sdci # 45  3   -547.1577779502  1.2449E-08  1.2639E-08  1.2697E-04  1.0000E-04
 mr-sdci # 45  4   -547.0983485225  2.3713E-06  0.0000E+00  1.5319E-01  1.0000E-04
 mr-sdci # 45  5   -547.0690353507  3.7177E-05  0.0000E+00  1.0044E-01  1.0000E-04
 mr-sdci # 45  6   -545.9738764494 -8.5472E-01  0.0000E+00  1.2482E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009999
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  46

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       53560 2x:        6515 4x:         645
All internal counts: zz :       50395 yy:      153125 xx:           0 ww:           0
One-external counts: yz :       64999 yx:           0 yw:           0
Two-external counts: yy :       20004 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.81711874
   ht   2     0.00000000   -72.76223847
   ht   3     0.00000000     0.00000000   -72.67676461
   ht   4     0.00000000     0.00000000     0.00000000   -72.61733282
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.58798485
   ht   6     0.00316173     0.01124225    -0.00093933    -0.00193144     0.00211337    -0.00000443
   ht   7     0.00330489     0.00828351    -0.00167097    -0.00201717    -0.00006180    -0.00000213    -0.00000244

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.966538       3.430147E-08   8.002696E-07   9.730212E-02  -3.861495E-05  -4.365067E-05  -4.565481E-05
 refs   2  -1.387336E-07   0.971099      -5.155257E-02  -2.546808E-04  -3.775270E-02  -1.613426E-04  -1.184764E-04
 refs   3  -7.398014E-08   2.501239E-02   0.807639      -2.029197E-04  -0.463484       2.423923E-05   1.791473E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       3.548702E-07  -7.951149E-06  -5.064795E-04  -8.105313E-05   7.159958E-03   0.361697    
 civs   2  -6.792978E-07    1.00000      -6.539495E-05  -1.413759E-03   3.211702E-03  -0.231685       0.999303    
 civs   3   6.542700E-08  -3.618050E-07   -1.00000       2.292418E-04   7.015316E-04  -5.312134E-02  -0.164941    
 civs   4   1.156433E-07  -2.026940E-07   3.751680E-06  -0.999689       2.669241E-05  -4.200192E-03  -0.222786    
 civs   5  -9.384229E-08  -4.006400E-07  -1.313130E-05  -6.422919E-05  -0.997665      -0.177545       5.368139E-02
 civs   6  -3.892093E-03  -1.950527E-02  -0.996676       -3.14372        74.8923       -5475.16        2018.81    
 civs   7  -6.847537E-04   2.647892E-02   0.778296       -8.15196       -73.4333        5395.72        6037.97    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.966538       1.177519E-10  -1.471747E-07  -9.725199E-02   4.626445E-05  -8.277783E-04  -3.587067E-02
 ref    2   1.882963E-07   0.971099       5.155842E-02   3.453394E-04   3.736405E-02   2.856471E-02  -6.412178E-02
 ref    3   4.669303E-08   2.501228E-02  -0.807649       1.601673E-04   0.463548      -2.458739E-03   2.404987E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96653812     0.00000000    -0.00000015    -0.09725199     0.00004626    -0.00082778    -0.03587067
 ref:   2     0.00000019     0.97109898     0.05155842     0.00034534     0.03736405     0.02856471    -0.06412178
 ref:   3     0.00000005     0.02501228    -0.80764855     0.00016017     0.46354812    -0.00245874     0.02404987

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -547.2981320697  1.4211E-14  0.0000E+00  5.4672E-05  1.0000E-04
 mr-sdci # 46  2   -547.2432517979  1.2506E-11  0.0000E+00  7.0669E-05  1.0000E-04
 mr-sdci # 46  3   -547.1577779601  9.8371E-09  3.6019E-09  6.8022E-05  1.0000E-04
 mr-sdci # 46  4   -547.0983495271  1.0045E-06  0.0000E+00  1.5337E-01  1.0000E-04
 mr-sdci # 46  5   -547.0691141338  7.8783E-05  0.0000E+00  9.9854E-02  1.0000E-04
 mr-sdci # 46  6   -546.4477011151  4.7382E-01  0.0000E+00  9.2520E-01  1.0000E-04
 mr-sdci # 46  7   -545.3805894978 -1.0660E-04  0.0000E+00  1.3685E+00  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009999
time for eigenvalue solver             0.010000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after 46 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -547.2981320697  1.4211E-14  0.0000E+00  5.4672E-05  1.0000E-04
 mr-sdci # 46  2   -547.2432517979  1.2506E-11  0.0000E+00  7.0669E-05  1.0000E-04
 mr-sdci # 46  3   -547.1577779601  9.8371E-09  3.6019E-09  6.8022E-05  1.0000E-04
 mr-sdci # 46  4   -547.0983495271  1.0045E-06  0.0000E+00  1.5337E-01  1.0000E-04
 mr-sdci # 46  5   -547.0691141338  7.8783E-05  0.0000E+00  9.9854E-02  1.0000E-04
 mr-sdci # 46  6   -546.4477011151  4.7382E-01  0.0000E+00  9.2520E-01  1.0000E-04
 mr-sdci # 46  7   -545.3805894978 -1.0660E-04  0.0000E+00  1.3685E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -547.298132069679
   ci vector at position   2 energy= -547.243251797916
   ci vector at position   3 energy= -547.157777960051

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    3 of the   8 expansion vectors are transformed.
    3 of the   7 matrix-vector products are transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96654)

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.97110)

 information on vector: 2 from unit 11 written to unit 48 filename civout              
 bummer (warning):Biggest overlap is smaller than 0.85                      0
   Overlap of CI vector #                     3  with reference vector #
                     1 -1.471747125821334E-007
   Overlap of CI vector #                     3  with reference vector #
                     2  5.155841895144185E-002
   Overlap of CI vector #                     3  with reference vector #
                     3 -0.807648550630926     

 information on vector: 3 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -547.2981320697

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11

                                          orbital     8    9   10   11   12   13   14   15   16   17   18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.816586                        +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       3 -0.110981                        +-   +-   +-   +-   +-   +-   +-   +-   +          - 
 z*  1  1       4 -0.038943                        +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       6 -0.088803                        +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       8  0.016107                        +-   +-   +-   +-   +-   +-   +-   +    +-         - 
 z*  1  1      15 -0.024638                        +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      17 -0.027960                        +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      21  0.419329                        +-   +-   +-   +-   +-   +-   +    +-   +-    -      
 z*  1  1      24  0.098310                        +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
 z*  1  1      26 -0.049747                        +-   +-   +-   +-   +-   +-   +    +-   +     -    - 
 z*  1  1      28  0.058887                        +-   +-   +-   +-   +-   +-   +    +-         -   +- 
 z*  1  1      41 -0.232417                        +-   +-   +-   +-   +-   +-        +-   +-   +-      
 z*  1  1      43 -0.021018                        +-   +-   +-   +-   +-   +-        +-   +-        +- 
 z*  1  1      44 -0.028014                        +-   +-   +-   +-   +-   +-        +-   +    +-    - 
 z*  1  1      46  0.043554                        +-   +-   +-   +-   +-   +-        +-        +-   +- 
 z   1  1      51  0.026485                        +-   +-   +-   +-   +-   +    +-   +-   +-    -      
 z   1  1      56 -0.010710                        +-   +-   +-   +-   +-   +    +-   +-   +     -    - 
 z   1  1      65  0.011048                        +-   +-   +-   +-   +-   +    +-   +    +-    -    - 
 z   1  1      71  0.045357                        +-   +-   +-   +-   +-   +     -   +-   +-   +-      
 z   1  1      73 -0.010229                        +-   +-   +-   +-   +-   +     -   +-   +-        +- 
 z   1  1      74  0.013243                        +-   +-   +-   +-   +-   +     -   +-   +    +-    - 
 z   1  1      95  0.018170                        +-   +-   +-   +-   +    +-   +-   +-    -        +- 
 z   1  1     127  0.010834                        +-   +-   +-   +-   +    +-        +-   +-   +-    - 
 z   1  1     133 -0.010947                        +-   +-   +-   +    +-   +-   +-   +-    -   +-      
 z   1  1     152 -0.016221                        +-   +-   +-   +    +-   +-    -   +-   +-   +     - 
 z   1  1     172 -0.013390                        +-   +-   +    +-   +-   +-   +-   +-   +-         - 
 z   1  1     173 -0.012445                        +-   +-   +    +-   +-   +-   +-   +-    -   +-      
 z   1  1     212  0.010570                        +-   +    +-   +-   +-   +-   +-   +-   +-         - 
 z   1  1     215  0.015785                        +-   +    +-   +-   +-   +-   +-   +-    -        +- 
 z   1  1     232 -0.021919                        +-   +    +-   +-   +-   +-    -   +-   +-   +     - 
 z   1  1     243  0.010384                        +-   +    +-   +-   +-   +-   +    +-    -    -   +- 
 y   1  1     291  0.013074              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     292  0.011874              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     299  0.013028              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     301  0.015602             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     302 -0.019024             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     306 -0.010408             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     375 -0.082758              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     379  0.014780              5( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     381  0.024381              7( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     385 -0.011261             11( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     431 -0.042598              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     437  0.026902              7( a  )   +-   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     441 -0.016350             11( a  )   +-   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     442 -0.024168             12( a  )   +-   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     445 -0.019237             15( a  )   +-   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     856 -0.020859              6( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-           
 y   1  1     912 -0.010472              6( a  )   +-   +-   +-   +-   +-   +-    -   +-   +          - 
 y   1  1     919 -0.017468             13( a  )   +-   +-   +-   +-   +-   +-    -   +-   +          - 
 y   1  1     920  0.017260             14( a  )   +-   +-   +-   +-   +-   +-    -   +-   +          - 
 y   1  1    1159 -0.015504              1( a  )   +-   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1495  0.036369              1( a  )   +-   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1501 -0.019591              7( a  )   +-   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1505  0.011056             11( a  )   +-   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1506  0.018048             12( a  )   +-   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1509  0.012985             15( a  )   +-   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1579 -0.010670              1( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +     - 
 y   1  1    1635  0.018324              1( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -    - 
 y   1  1    1641 -0.014569              7( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -    - 
 y   1  1    1646  0.014911             12( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -    - 
 y   1  1    1649  0.011099             15( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -    - 
 y   1  1    1691  0.013961              1( a  )   +-   +-   +-   +-   +-   +-   +     -         -   +- 
 y   1  1    1836  0.024555              6( a  )   +-   +-   +-   +-   +-   +-        +-   +-    -      
 y   1  1    1838 -0.010395              8( a  )   +-   +-   +-   +-   +-   +-        +-   +-    -      
 y   1  1    1843  0.017397             13( a  )   +-   +-   +-   +-   +-   +-        +-   +-    -      
 y   1  1    1844 -0.020562             14( a  )   +-   +-   +-   +-   +-   +-        +-   +-    -      
 y   1  1    1976  0.019573              6( a  )   +-   +-   +-   +-   +-   +-        +-   +     -    - 
 y   1  1    1983  0.022730             13( a  )   +-   +-   +-   +-   +-   +-        +-   +     -    - 
 y   1  1    2055 -0.011532              1( a  )   +-   +-   +-   +-   +-   +-         -   +-   +-      
 y   1  1    2139  0.022758              1( a  )   +-   +-   +-   +-   +-   +-         -   +    +-    - 
 y   1  1    2145 -0.014708              7( a  )   +-   +-   +-   +-   +-   +-         -   +    +-    - 
 y   1  1    2150  0.013048             12( a  )   +-   +-   +-   +-   +-   +-         -   +    +-    - 
 y   1  1    2425 -0.015989              7( a  )   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    3792  0.012107              2( a  )   +-   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3797  0.029201              7( a  )   +-   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3805  0.013315             15( a  )   +-   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3890 -0.012187             16( a  )   +-   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    4214  0.010019              4( a  )   +-   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    4216  0.014334              6( a  )   +-   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    4218  0.060602              8( a  )   +-   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    4223  0.021794             13( a  )   +-   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    4224  0.028946             14( a  )   +-   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    4273 -0.017167              7( a  )   +-   +-   +-   +-   +-   +     -   +-    -   +-      
 y   1  1    5115 -0.011109              9( a  )   +-   +-   +-   +-    -   +-   +-   +-   +          - 
 y   1  1    6480  0.014734              2( a  )   +-   +-   +-   +-   +    +-   +-   +-    -         - 
 y   1  1    6485  0.017853              7( a  )   +-   +-   +-   +-   +    +-   +-   +-    -         - 
 y   1  1    6493  0.010296             15( a  )   +-   +-   +-   +-   +    +-   +-   +-    -         - 
 y   1  1    6904  0.012071              6( a  )   +-   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1    6906  0.020431              8( a  )   +-   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1    6911  0.010399             13( a  )   +-   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1    6912  0.010137             14( a  )   +-   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1    8271 -0.012489              1( a  )   +-   +-   +-    -   +-   +-   +    +-   +-    -      
 y   1  1    9116 -0.015884              6( a  )   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1    9539  0.013572              9( a  )   +-   +-   +-   +    +-   +-    -   +-   +-    -      
 y   1  1    9564  0.010044              6( a  )   +-   +-   +-   +    +-   +-    -   +-   +-         - 
 y   1  1   11776 -0.028181              6( a  )   +-   +-   +    +-   +-   +-   +-   +-    -    -      
 y   1  1   11799  0.011107              1( a  )   +-   +-   +    +-   +-   +-   +-   +-    -         - 
 y   1  1   11800  0.016111              2( a  )   +-   +-   +    +-   +-   +-   +-   +-    -         - 
 y   1  1   11807  0.013155              9( a  )   +-   +-   +    +-   +-   +-   +-   +-    -         - 
 y   1  1   12191 -0.015169              1( a  )   +-   +-   +    +-   +-   +-    -   +-   +-    -      
 y   1  1   12192  0.013987              2( a  )   +-   +-   +    +-   +-   +-    -   +-   +-    -      
 y   1  1   12199  0.019757              9( a  )   +-   +-   +    +-   +-   +-    -   +-   +-    -      
 y   1  1   12212 -0.010270             22( a  )   +-   +-   +    +-   +-   +-    -   +-   +-    -      
 y   1  1   12224  0.022353              6( a  )   +-   +-   +    +-   +-   +-    -   +-   +-         - 
 y   1  1   13095  0.013821              9( a  )   +-    -   +-   +-   +-   +-   +-   +-   +          - 
 y   1  1   13103 -0.012126             17( a  )   +-    -   +-   +-   +-   +-   +-   +-   +          - 
 y   1  1   14454 -0.019845             24( a  )   +-   +    +-   +-   +-   +-   +-   +-    -    -      
 y   1  1   14475 -0.012829             17( a  )   +-   +    +-   +-   +-   +-   +-   +-    -         - 
 y   1  1   14485  0.016114             27( a  )   +-   +    +-   +-   +-   +-   +-   +-    -         - 
 y   1  1   14534  0.023385             20( a  )   +-   +    +-   +-   +-   +-   +-    -   +-    -      
 y   1  1   14535 -0.012260             21( a  )   +-   +    +-   +-   +-   +-   +-    -   +-    -      
 y   1  1   14554  0.010660             12( a  )   +-   +    +-   +-   +-   +-   +-    -   +-         - 
 y   1  1   14561  0.017985             19( a  )   +-   +    +-   +-   +-   +-   +-    -   +-         - 
 y   1  1   14568  0.010057             26( a  )   +-   +    +-   +-   +-   +-   +-    -   +-         - 
 y   1  1   14859 -0.010644              9( a  )   +-   +    +-   +-   +-   +-    -   +-   +-    -      
 y   1  1   14872  0.024408             22( a  )   +-   +    +-   +-   +-   +-    -   +-   +-    -      
 y   1  1   14877  0.012553             27( a  )   +-   +    +-   +-   +-   +-    -   +-   +-    -      
 y   1  1   14902  0.016197             24( a  )   +-   +    +-   +-   +-   +-    -   +-   +-         - 
 y   1  1   17520  0.010276             10( a  )   +    +-   +-   +-   +-   +-    -   +-   +-    -      

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             115
     0.01> rq > 0.001           1543
    0.001> rq > 0.0001          3831
   0.0001> rq > 0.00001         2991
  0.00001> rq > 0.000001         684
 0.000001> rq                   9182
           all                 18350
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       17280 2x:           0 4x:           0
All internal counts: zz :       50395 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.816586248566   -446.817680834421
     2     2      0.000000143782     -0.000078687326
     3     3     -0.110980910121     60.726488025373
     4     4     -0.038943222574     21.304270689017
     5     5     -0.000000044180      0.000024159427
     6     6     -0.088802858698     48.594540481065
     7     7      0.000000695092     -0.000379933813
     8     8      0.016106930835     -8.809024873493
     9     9      0.004636314546     -2.535938719915
    10    10     -0.000000543506      0.000296895409
    11    11      0.003266113350     -1.785838531804
    12    12      0.000000634355     -0.000346358056
    13    13     -0.001219099813      0.667259553767
    14    14     -0.000000555977      0.000303315084
    15    15     -0.024638366289     13.477087903840
    16    16     -0.000000007359      0.000004028640
    17    17     -0.027960021874     15.293532030301
    18    18      0.006367015563     -3.485632642180
    19    19     -0.000000013587      0.000007427268
    20    20      0.002727716475     -1.490899788161
    21    21      0.419329361350   -229.463604164624
    22    22      0.000000168484     -0.000092144888
    23    23     -0.000000161230      0.000088161276
    24    24      0.098309866250    -53.799631837033
    25    25      0.000000027273     -0.000014905034
    26    26     -0.049747157535     27.235965388937
    27    27     -0.000000015333      0.000008359140
    28    28      0.058887072626    -32.240285852951
    29    29      0.000000936931     -0.000511908828
    30    30      0.002865913215     -1.567204711657
    31    31      0.000000024729     -0.000013520880
    32    32      0.000000623676     -0.000340013980
    33    33     -0.000355501254      0.195153377981
    34    34     -0.000000018814      0.000010856384
    35    35     -0.000456529056      0.249928940577
    36    36     -0.000000039412      0.000021546031
    37    37     -0.001295051481      0.709008054246
    38    38     -0.000000011651      0.000006356925
    39    39      0.006644392554     -3.632739131702
    40    40     -0.000000006114      0.000003333081
    41    41     -0.232416725012    127.176132043884
    42    42      0.000000031939     -0.000017462486
    43    43     -0.021018108438     11.496666549172
    44    44     -0.028013713131     15.322349939026
    45    45     -0.000000023946      0.000013114679
    46    46      0.043553684757    -23.849002089911
    47    47     -0.001124847331      0.614287779455
    48    48      0.000000081053     -0.000044239005
    49    49     -0.000751306411      0.410592146251
    50    50      0.007164293321     -3.917956227887

 number of reference csfs (nref) is    50.  root number (iroot) is  1.
 c0**2 =   0.93895519  c**2 (all zwalks) =   0.94546914

 pople ci energy extrapolation is computed with 18 correlated electrons.

 eref      =   -547.188426531612   "relaxed" cnot**2         =   0.938955186470
 eci       =   -547.298132069679   deltae = eci - eref       =  -0.109705538067
 eci+dv1   =   -547.304829023794   dv1 = (1-cnot**2)*deltae  =  -0.006696954115
 eci+dv2   =   -547.305264416580   dv2 = dv1 / cnot**2       =  -0.007132346901
 eci+dv3   =   -547.305760358763   dv3 = dv1 / (2*cnot**2-1) =  -0.007628289084
 eci+pople =   -547.304806500161   ( 18e- scaled deltae )    =  -0.116379968549


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -547.2432517979

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11

                                          orbital     8    9   10   11   12   13   14   15   16   17   18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.013209                        +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 z*  1  1       7  0.937495                        +-   +-   +-   +-   +-   +-   +-   +    +-    -      
 z*  1  1      10  0.068912                        +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1      12  0.017706                        +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1      14  0.118145                        +-   +-   +-   +-   +-   +-   +-   +          -   +- 
 z*  1  1      23  0.023225                        +-   +-   +-   +-   +-   +-   +    +-    -   +-      
 z*  1  1      29 -0.200853                        +-   +-   +-   +-   +-   +-   +     -   +-   +-      
 z*  1  1      32 -0.059325                        +-   +-   +-   +-   +-   +-   +     -   +    +-    - 
 z*  1  1      34  0.039266                        +-   +-   +-   +-   +-   +-   +     -        +-   +- 
 z*  1  1      36  0.024707                        +-   +-   +-   +-   +-   +-   +    +     -   +-    - 
 z*  1  1      48 -0.022715                        +-   +-   +-   +-   +-   +-        +    +-    -   +- 
 z   1  1      53 -0.029248                        +-   +-   +-   +-   +-   +    +-   +-    -   +-      
 z   1  1      59  0.032040                        +-   +-   +-   +-   +-   +    +-    -   +-   +-      
 z   1  1      61 -0.010574                        +-   +-   +-   +-   +-   +    +-    -   +-        +- 
 z   1  1      91  0.039299                        +-   +-   +-   +-   +    +-   +-   +-   +-    -      
 z   1  1      94 -0.019161                        +-   +-   +-   +-   +    +-   +-   +-    -   +     - 
 z   1  1      96  0.010529                        +-   +-   +-   +-   +    +-   +-   +-   +     -    - 
 z   1  1     107  0.012786                        +-   +-   +-   +-   +    +-   +-   +     -    -   +- 
 z   1  1     111  0.028070                        +-   +-   +-   +-   +    +-    -   +-   +-   +-      
 z   1  1     124  0.010911                        +-   +-   +-   +-   +    +-   +     -   +-   +-    - 
 z   1  1     131 -0.038116                        +-   +-   +-   +    +-   +-   +-   +-   +-    -      
 z   1  1     134  0.017962                        +-   +-   +-   +    +-   +-   +-   +-    -   +     - 
 z   1  1     136 -0.022129                        +-   +-   +-   +    +-   +-   +-   +-   +     -    - 
 z   1  1     140 -0.011391                        +-   +-   +-   +    +-   +-   +-    -   +-   +     - 
 z   1  1     151 -0.024070                        +-   +-   +-   +    +-   +-    -   +-   +-   +-      
 z   1  1     220 -0.020026                        +-   +    +-   +-   +-   +-   +-    -   +-   +     - 
 z   1  1     223 -0.011849                        +-   +    +-   +-   +-   +-   +-    -   +     -   +- 
 z   1  1     227  0.019023                        +-   +    +-   +-   +-   +-   +-   +     -    -   +- 
 z   1  1     237 -0.010125                        +-   +    +-   +-   +-   +-    -   +    +-   +-    - 
 y   1  1     380  0.013372              6( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     387  0.012870             13( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     403 -0.014013              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     436  0.030185              6( a  )   +-   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     443  0.018520             13( a  )   +-   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     627  0.018309              1( a  )   +-   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     633 -0.013672              7( a  )   +-   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     638  0.017098             12( a  )   +-   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     641  0.012107             15( a  )   +-   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     711 -0.010092              1( a  )   +-   +-   +-   +-   +-   +-   +-         -   +     - 
 y   1  1     767  0.027688              1( a  )   +-   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     773 -0.024693              7( a  )   +-   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     777  0.016089             11( a  )   +-   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     778  0.024794             12( a  )   +-   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     781  0.018248             15( a  )   +-   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1    1031  0.010062             13( a  )   +-   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1    1108 -0.018598              6( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +     - 
 y   1  1    1115 -0.020000             13( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +     - 
 y   1  1    1116  0.012785             14( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +     - 
 y   1  1    1164  0.033882              6( a  )   +-   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1171  0.036542             13( a  )   +-   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1172 -0.021692             14( a  )   +-   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1327  0.011984              1( a  )   +-   +-   +-   +-   +-   +-    -        +    +-    - 
 y   1  1    1640  0.010188              6( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -    - 
 y   1  1    2060  0.012582              6( a  )   +-   +-   +-   +-   +-   +-         -   +-   +-      
 y   1  1    2067  0.010296             13( a  )   +-   +-   +-   +-   +-   +-         -   +-   +-      
 y   1  1    2068 -0.012046             14( a  )   +-   +-   +-   +-   +-   +-         -   +-   +-      
 y   1  1    2144 -0.010607              6( a  )   +-   +-   +-   +-   +-   +-         -   +    +-    - 
 y   1  1    2621 -0.014490              7( a  )   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3878  0.014531              4( a  )   +-   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3882  0.062027              8( a  )   +-   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3887  0.017379             13( a  )   +-   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3888  0.029990             14( a  )   +-   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3932 -0.013163              2( a  )   +-   +-   +-   +-   +-   +    +-    -    -   +-      
 y   1  1    3937 -0.031080              7( a  )   +-   +-   +-   +-   +-   +    +-    -    -   +-      
 y   1  1    3945 -0.012484             15( a  )   +-   +-   +-   +-   +-   +    +-    -    -   +-      
 y   1  1    4442  0.026652              8( a  )   +-   +-   +-   +-   +-   +     -    -   +-   +-      
 y   1  1    4448  0.015735             14( a  )   +-   +-   +-   +-   +-   +     -    -   +-   +-      
 y   1  1    5367  0.012606              9( a  )   +-   +-   +-   +-    -   +-   +-   +    +     -    - 
 y   1  1    6535 -0.014373              1( a  )   +-   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    6570  0.019111              8( a  )   +-   +-   +-   +-   +    +-   +-    -   +-         - 
 y   1  1    6576  0.011582             14( a  )   +-   +-   +-   +-   +    +-   +-    -   +-         - 
 y   1  1    6625 -0.011961              7( a  )   +-   +-   +-   +-   +    +-   +-    -    -   +     - 
 y   1  1    6760  0.014087              2( a  )   +-   +-   +-   +-   +    +-   +-   +     -    -    - 
 y   1  1    6765  0.016592              7( a  )   +-   +-   +-   +-   +    +-   +-   +     -    -    - 
 y   1  1    7268  0.011459              6( a  )   +-   +-   +-   +-   +    +-    -   +    +-    -    - 
 y   1  1    7739  0.011109              1( a  )   +-   +-   +-    -   +-   +-   +-   +-   +     -      
 y   1  1    8495 -0.011299              1( a  )   +-   +-   +-    -   +-   +-   +     -   +-   +-      
 y   1  1    9196  0.010509              2( a  )   +-   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1    9203  0.018798              9( a  )   +-   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1    9228  0.010463              6( a  )   +-   +-   +-   +    +-   +-   +-    -   +-         - 
 y   1  1    9256  0.012298              6( a  )   +-   +-   +-   +    +-   +-   +-    -    -   +-      
 y   1  1   10681 -0.011817              3( a  )   +-   +-    -   +-   +-   +-   +-   +    +     -    - 
 y   1  1   11855 -0.023967              1( a  )   +-   +-   +    +-   +-   +-   +-    -   +-    -      
 y   1  1   11856  0.014962              2( a  )   +-   +-   +    +-   +-   +-   +-    -   +-    -      
 y   1  1   11863  0.027173              9( a  )   +-   +-   +    +-   +-   +-   +-    -   +-    -      
 y   1  1   11877  0.010972             23( a  )   +-   +-   +    +-   +-   +-   +-    -   +-    -      
 y   1  1   11888  0.020920              6( a  )   +-   +-   +    +-   +-   +-   +-    -   +-         - 
 y   1  1   11916  0.020357              6( a  )   +-   +-   +    +-   +-   +-   +-    -    -   +-      
 y   1  1   11940 -0.011021              2( a  )   +-   +-   +    +-   +-   +-   +-    -    -   +     - 
 y   1  1   12079  0.011490              1( a  )   +-   +-   +    +-   +-   +-   +-   +     -    -    - 
 y   1  1   12080  0.013096              2( a  )   +-   +-   +    +-   +-   +-   +-   +     -    -    - 
 y   1  1   13347 -0.015351              9( a  )   +-    -   +-   +-   +-   +-   +-   +    +     -    - 
 y   1  1   13349  0.010816             11( a  )   +-    -   +-   +-   +-   +-   +-   +    +     -    - 
 y   1  1   13355  0.014176             17( a  )   +-    -   +-   +-   +-   +-   +-   +    +     -    - 
 y   1  1   13356  0.010975             18( a  )   +-    -   +-   +-   +-   +-   +-   +    +     -    - 
 y   1  1   14449 -0.020926             19( a  )   +-   +    +-   +-   +-   +-   +-   +-    -    -      
 y   1  1   14525  0.010187             11( a  )   +-   +    +-   +-   +-   +-   +-    -   +-    -      
 y   1  1   14536 -0.031599             22( a  )   +-   +    +-   +-   +-   +-   +-    -   +-    -      
 y   1  1   14541  0.012603             27( a  )   +-   +    +-   +-   +-   +-   +-    -   +-    -      
 y   1  1   14755 -0.013448             17( a  )   +-   +    +-   +-   +-   +-   +-   +     -    -    - 
 y   1  1   14765  0.015846             27( a  )   +-   +    +-   +-   +-   +-   +-   +     -    -    - 
 y   1  1   14785  0.013456             19( a  )   +-   +    +-   +-   +-   +-   +-        +-    -    - 
 y   1  1   14870  0.026502             20( a  )   +-   +    +-   +-   +-   +-    -   +-   +-    -      
 y   1  1   14871 -0.012497             21( a  )   +-   +    +-   +-   +-   +-    -   +-   +-    -      
 y   1  1   15255  0.013212             13( a  )   +-   +    +-   +-   +-   +-    -   +    +-    -    - 
 y   1  1   15266  0.016866             24( a  )   +-   +    +-   +-   +-   +-    -   +    +-    -    - 
 y   1  1   17184  0.010135             10( a  )   +    +-   +-   +-   +-   +-   +-    -   +-    -      
 y   1  1   17197  0.011022             23( a  )   +    +-   +-   +-   +-   +-   +-    -   +-    -      

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             105
     0.01> rq > 0.001           1476
    0.001> rq > 0.0001          3610
   0.0001> rq > 0.00001         3068
  0.00001> rq > 0.000001         732
 0.000001> rq                   9356
           all                 18350
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       17280 2x:           0 4x:           0
All internal counts: zz :       50395 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000000022921     -0.000012372991
     2     2      0.013208911986     -7.223349890449
     3     3      0.000000237934     -0.000129874798
     4     4      0.000000025602     -0.000013999853
     5     5     -0.005955491310      3.256072991734
     6     6      0.000000107902     -0.000058845607
     7     7      0.937494702067   -512.935748570141
     8     8     -0.000000025187      0.000013781811
     9     9      0.000000045970     -0.000025146615
    10    10      0.068911588600    -37.710662985974
    11    11     -0.000000014369      0.000007866914
    12    12      0.017706212935     -9.670491016509
    13    13     -0.000000002370      0.000001300510
    14    14      0.118144809341    -64.660888887140
    15    15      0.000000236912     -0.000129618863
    16    16      0.002972564780     -1.625455045284
    17    17      0.000000023081     -0.000012608996
    18    18      0.000000011620     -0.000006348231
    19    19     -0.002067920887      1.130948495919
    20    20     -0.000000033503      0.000018338862
    21    21     -0.000000085497      0.000046660889
    22    22      0.003011579503     -1.647286529675
    23    23      0.023224694533    -12.701944572389
    24    24     -0.000000105519      0.000057607582
    25    25     -0.000922128519      0.504395347085
    26    26      0.000000106277     -0.000057955266
    27    27     -0.001219654750      0.672071154703
    28    28      0.000000033040     -0.000017978494
    29    29     -0.200853083508    109.900475092827
    30    30      0.000000024291     -0.000013290341
    31    31      0.004710399274     -2.576830942671
    32    32     -0.059324611245     32.459082891973
    33    33     -0.000000010803      0.000005922096
    34    34      0.039265603746    -21.499027003714
    35    35      0.000000054926     -0.000030039884
    36    36      0.024707168652    -13.516873570962
    37    37      0.000000024225     -0.000013260633
    38    38     -0.000962616616      0.526911491125
    39    39      0.000000006939     -0.000003790284
    40    40     -0.000151508995      0.082611759436
    41    41      0.000000098922     -0.000054042479
    42    42     -0.001913664444      1.046479105751
    43    43      0.000000020075     -0.000010968241
    44    44     -0.000000023001      0.000012548697
    45    45     -0.000302028408      0.165226882143
    46    46      0.000000004378     -0.000002347258
    47    47     -0.000000026467      0.000014476235
    48    48     -0.022714960163     12.421946032500
    49    49      0.000000007040     -0.000003854540
    50    50     -0.000000007450      0.000004074754

 number of reference csfs (nref) is    50.  root number (iroot) is  2.
 c0**2 =   0.94524714  c**2 (all zwalks) =   0.95545316

 pople ci energy extrapolation is computed with 18 correlated electrons.

 eref      =   -547.138946890251   "relaxed" cnot**2         =   0.945247141664
 eci       =   -547.243251797916   deltae = eci - eref       =  -0.104304907665
 eci+dv1   =   -547.248962789749   dv1 = (1-cnot**2)*deltae  =  -0.005710991833
 eci+dv2   =   -547.249293595433   dv2 = dv1 / cnot**2       =  -0.006041797517
 eci+dv3   =   -547.249665080898   dv3 = dv1 / (2*cnot**2-1) =  -0.006413282982
 eci+pople =   -547.248873610010   ( 18e- scaled deltae )    =  -0.109926719759


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -547.1577779601

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11

                                          orbital     8    9   10   11   12   13   14   15   16   17   18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2 -0.815162                        +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 z*  1  1       5  0.034009                        +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       7  0.026287                        +-   +-   +-   +-   +-   +-   +-   +    +-    -      
 z*  1  1      16 -0.033438                        +-   +-   +-   +-   +-   +-   +-        +-   +     - 
 z*  1  1      19  0.030790                        +-   +-   +-   +-   +-   +-   +-        +     -   +- 
 z*  1  1      22 -0.022968                        +-   +-   +-   +-   +-   +-   +    +-   +-         - 
 z*  1  1      23 -0.168664                        +-   +-   +-   +-   +-   +-   +    +-    -   +-      
 z*  1  1      27  0.047919                        +-   +-   +-   +-   +-   +-   +    +-        +-    - 
 z*  1  1      29 -0.078125                        +-   +-   +-   +-   +-   +-   +     -   +-   +-      
 z*  1  1      42 -0.020728                        +-   +-   +-   +-   +-   +-        +-   +-   +     - 
 z*  1  1      45  0.026875                        +-   +-   +-   +-   +-   +-        +-   +     -   +- 
 z   1  1      53  0.040283                        +-   +-   +-   +-   +-   +    +-   +-    -   +-      
 z   1  1      59  0.045955                        +-   +-   +-   +-   +-   +    +-    -   +-   +-      
 z   1  1      81  0.010340                        +-   +-   +-   +-   +-   +    +    +-   +-    -    - 
 z   1  1      91 -0.330790                        +-   +-   +-   +-   +    +-   +-   +-   +-    -      
 z   1  1      94  0.086189                        +-   +-   +-   +-   +    +-   +-   +-    -   +     - 
 z   1  1      96 -0.069859                        +-   +-   +-   +-   +    +-   +-   +-   +     -    - 
 z   1  1      98 -0.041827                        +-   +-   +-   +-   +    +-   +-   +-         -   +- 
 z   1  1     100  0.012286                        +-   +-   +-   +-   +    +-   +-    -   +-   +     - 
 z   1  1     109 -0.011562                        +-   +-   +-   +-   +    +-   +-        +-    -   +- 
 z   1  1     111 -0.307871                        +-   +-   +-   +-   +    +-    -   +-   +-   +-      
 z   1  1     114  0.012401                        +-   +-   +-   +-   +    +-    -   +-   +    +-    - 
 z   1  1     116  0.047682                        +-   +-   +-   +-   +    +-    -   +-        +-   +- 
 z   1  1     122  0.034769                        +-   +-   +-   +-   +    +-   +    +-    -   +-    - 
 z   1  1     134  0.017547                        +-   +-   +-   +    +-   +-   +-   +-    -   +     - 
 z   1  1     171 -0.010324                        +-   +-   +    +-   +-   +-   +-   +-   +-    -      
 z   1  1     174  0.037838                        +-   +-   +    +-   +-   +-   +-   +-    -   +     - 
 z   1  1     176 -0.018529                        +-   +-   +    +-   +-   +-   +-   +-   +     -    - 
 z   1  1     191 -0.043876                        +-   +-   +    +-   +-   +-    -   +-   +-   +-      
 z   1  1     211 -0.051755                        +-   +    +-   +-   +-   +-   +-   +-   +-    -      
 z   1  1     214 -0.010534                        +-   +    +-   +-   +-   +-   +-   +-    -   +     - 
 z   1  1     218 -0.014393                        +-   +    +-   +-   +-   +-   +-   +-         -   +- 
 z   1  1     242  0.010995                        +-   +    +-   +-   +-   +-   +    +-    -   +-    - 
 z   1  1     251  0.014658                        +    +-   +-   +-   +-   +-   +-   +-   +-    -      
 y   1  1     296 -0.013025              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     303 -0.012295             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     320 -0.014015              2( a  )   +-   +-   +-   +-   +-   +-   +-   +-         -      
 y   1  1     327 -0.013374              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-         -      
 y   1  1     330  0.012474             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-         -      
 y   1  1     350  0.011257              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     352 -0.015276              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     359 -0.013940             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     403  0.065560              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     407 -0.013600              5( a  )   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     409 -0.018013              7( a  )   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     487  0.040063              1( a  )   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     493 -0.024659              7( a  )   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     497  0.014736             11( a  )   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     498  0.022322             12( a  )   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     501  0.016797             15( a  )   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     543 -0.013382              1( a  )   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     884  0.012430              6( a  )   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     892 -0.011147             14( a  )   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     968  0.020362              6( a  )   +-   +-   +-   +-   +-   +-    -   +-        +     - 
 y   1  1     975  0.023270             13( a  )   +-   +-   +-   +-   +-   +-    -   +-        +     - 
 y   1  1     976 -0.015704             14( a  )   +-   +-   +-   +-   +-   +-    -   +-        +     - 
 y   1  1    1075  0.010923              1( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +-      
 y   1  1    1414 -0.011059              4( a  )   +-   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1416  0.019469              6( a  )   +-   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1423  0.015852             13( a  )   +-   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1551 -0.034957              1( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1557  0.022767              7( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1561 -0.011643             11( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1562 -0.018595             12( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1565 -0.012429             15( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1892 -0.020337              6( a  )   +-   +-   +-   +-   +-   +-        +-    -   +-      
 y   1  1    1899 -0.020385             13( a  )   +-   +-   +-   +-   +-   +-        +-    -   +-      
 y   1  1    1900  0.014271             14( a  )   +-   +-   +-   +-   +-   +-        +-    -   +-      
 y   1  1    2481  0.015201              7( a  )   +-   +-   +-   +-   +-    -   +-   +-        +-      
 y   1  1    3012  0.010327              6( a  )   +-   +-   +-   +-   +-    -   +    +-    -   +-      
 y   1  1    3794 -0.012667              4( a  )   +-   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3798 -0.032530              8( a  )   +-   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3804 -0.018159             14( a  )   +-   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    4274 -0.017213              8( a  )   +-   +-   +-   +-   +-   +     -   +-    -   +-      
 y   1  1    5056 -0.022168              6( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-           
 y   1  1    5058 -0.037579              8( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-           
 y   1  1    5063 -0.012621             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-           
 y   1  1    5064 -0.013649             14( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-           
 y   1  1    5114  0.013717              8( a  )   +-   +-   +-   +-    -   +-   +-   +-   +          - 
 y   1  1    5219 -0.032574              1( a  )   +-   +-   +-   +-    -   +-   +-   +    +-    -      
 y   1  1    5756  0.015056              6( a  )   +-   +-   +-   +-    -   +-   +    +-   +     -    - 
 y   1  1    5763  0.015302             13( a  )   +-   +-   +-   +-    -   +-   +    +-   +     -    - 
 y   1  1    5919  0.012306              1( a  )   +-   +-   +-   +-    -   +-   +     -   +    +-    - 
 y   1  1    6176  0.012784              6( a  )   +-   +-   +-   +-    -   +-        +-   +-   +-      
 y   1  1    6183  0.010722             13( a  )   +-   +-   +-   +-    -   +-        +-   +-   +-      
 y   1  1    6267  0.012574             13( a  )   +-   +-   +-   +-    -   +-        +-   +    +-    - 
 y   1  1    6451 -0.016676              1( a  )   +-   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    6452 -0.022063              2( a  )   +-   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    6457 -0.037414              7( a  )   +-   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    6459 -0.019742              9( a  )   +-   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    6462  0.015614             12( a  )   +-   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    6465 -0.018529             15( a  )   +-   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    6466  0.010119             16( a  )   +-   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    6486 -0.012382              8( a  )   +-   +-   +-   +-   +    +-   +-   +-    -         - 
 y   1  1    6508 -0.011112              2( a  )   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1    6513 -0.011523              7( a  )   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1    6535  0.065119              1( a  )   +-   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    6539 -0.013098              5( a  )   +-   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    6541 -0.024571              7( a  )   +-   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    6545  0.012182             11( a  )   +-   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    6546  0.016206             12( a  )   +-   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    6550  0.010227             16( a  )   +-   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    6675 -0.020213              1( a  )   +-   +-   +-   +-   +    +-   +-    -   +     -    - 
 y   1  1    6681  0.013012              7( a  )   +-   +-   +-   +-   +    +-   +-    -   +     -    - 
 y   1  1    6731  0.010190              1( a  )   +-   +-   +-   +-   +    +-   +-    -         -   +- 
 y   1  1    6876  0.010250              6( a  )   +-   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    6878 -0.046133              8( a  )   +-   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    6884 -0.025645             14( a  )   +-   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    6933  0.016606              7( a  )   +-   +-   +-   +-   +    +-    -   +-    -   +-      
 y   1  1    6960 -0.010717              6( a  )   +-   +-   +-   +-   +    +-    -   +-    -   +     - 
 y   1  1    7179  0.024635              1( a  )   +-   +-   +-   +-   +    +-    -    -   +    +-    - 
 y   1  1    7185 -0.015999              7( a  )   +-   +-   +-   +-   +    +-    -    -   +    +-    - 
 y   1  1    7190  0.014959             12( a  )   +-   +-   +-   +-   +    +-    -    -   +    +-    - 
 y   1  1    7193  0.010683             15( a  )   +-   +-   +-   +-   +    +-    -    -   +    +-    - 
 y   1  1    7739 -0.016054              1( a  )   +-   +-   +-    -   +-   +-   +-   +-   +     -      
 y   1  1    9117  0.019052              7( a  )   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1    9119 -0.013385              9( a  )   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1    9538  0.016307              8( a  )   +-   +-   +-   +    +-   +-    -   +-   +-    -      
 y   1  1   11771  0.014812              1( a  )   +-   +-   +    +-   +-   +-   +-   +-    -    -      
 y   1  1   11772 -0.013792              2( a  )   +-   +-   +    +-   +-   +-   +-   +-    -    -      
 y   1  1   11777  0.017988              7( a  )   +-   +-   +    +-   +-   +-   +-   +-    -    -      
 y   1  1   11779 -0.022717              9( a  )   +-   +-   +    +-   +-   +-   +-   +-    -    -      
 y   1  1   11793 -0.011379             23( a  )   +-   +-   +    +-   +-   +-   +-   +-    -    -      
 y   1  1   11804 -0.014566              6( a  )   +-   +-   +    +-   +-   +-   +-   +-    -         - 
 y   1  1   11828 -0.010590              2( a  )   +-   +-   +    +-   +-   +-   +-   +-         -    - 
 y   1  1   11855  0.011296              1( a  )   +-   +-   +    +-   +-   +-   +-    -   +-    -      
 y   1  1   12198  0.010569              8( a  )   +-   +-   +    +-   +-   +-    -   +-   +-    -      
 y   1  1   13151 -0.011352              9( a  )   +-    -   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   13159  0.010638             17( a  )   +-    -   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   14432 -0.010790              2( a  )   +-   +    +-   +-   +-   +-   +-   +-    -    -      
 y   1  1   14457  0.015780             27( a  )   +-   +    +-   +-   +-   +-   +-   +-    -    -      
 y   1  1   14533  0.017235             19( a  )   +-   +    +-   +-   +-   +-   +-    -   +-    -      
 y   1  1   14590 -0.012009             20( a  )   +-   +    +-   +-   +-   +-   +-    -    -   +-      
 y   1  1   14666 -0.010143             12( a  )   +-   +    +-   +-   +-   +-   +-    -   +     -    - 
 y   1  1   14673 -0.013485             19( a  )   +-   +    +-   +-   +-   +-   +-    -   +     -    - 
 y   1  1   14871 -0.013318             21( a  )   +-   +    +-   +-   +-   +-    -   +-   +-    -      
 y   1  1   14874  0.010883             24( a  )   +-   +    +-   +-   +-   +-    -   +-   +-    -      
 y   1  1   14928 -0.012891             22( a  )   +-   +    +-   +-   +-   +-    -   +-    -   +-      
 y   1  1   15003 -0.010658             13( a  )   +-   +    +-   +-   +-   +-    -   +-   +     -    - 
 y   1  1   15014 -0.012074             24( a  )   +-   +    +-   +-   +-   +-    -   +-   +     -    - 
 y   1  1   17113 -0.012706             23( a  )   +    +-   +-   +-   +-   +-   +-   +-    -    -      
 y   1  1   17534 -0.010537             24( a  )   +    +-   +-   +-   +-   +-    -   +-   +-    -      

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             138
     0.01> rq > 0.001           1415
    0.001> rq > 0.0001          3817
   0.0001> rq > 0.00001         3009
  0.00001> rq > 0.000001         631
 0.000001> rq                   9336
           all                 18350
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       17280 2x:           0 4x:           0
All internal counts: zz :       50395 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000004072732     -0.002226023343
     2     2     -0.815162140987    445.895396078531
     3     3      0.000001964654     -0.001071278599
     4     4     -0.000000018389      0.000010095188
     5     5      0.034009458321    -18.586528167658
     6     6      0.000001139520     -0.000620908528
     7     7      0.026287036709    -14.389218843237
     8     8     -0.000000047735      0.000026159527
     9     9     -0.000000129555      0.000070868669
    10    10      0.009576645975     -5.233784787770
    11    11     -0.000000002581      0.000001445770
    12    12     -0.001067405138      0.578233796494
    13    13      0.000000006479     -0.000003531136
    14    14      0.001764728752     -0.968819154620
    15    15      0.000000225279     -0.000123177516
    16    16     -0.033437642343     18.293875656619
    17    17      0.000000144056     -0.000078622431
    18    18     -0.000000037653      0.000020659791
    19    19      0.030789787767    -16.835681684327
    20    20      0.000000002860     -0.000001533956
    21    21     -0.000004446074      0.002430807345
    22    22     -0.022968351383     12.565223745167
    23    23     -0.168663637550     92.207802840497
    24    24     -0.000001438065      0.000785158494
    25    25      0.006691833423     -3.655494499546
    26    26      0.000001288873     -0.000702624415
    27    27      0.047918547310    -26.276050393963
    28    28      0.000000129792     -0.000069709677
    29    29     -0.078124841589     42.722207520200
    30    30     -0.000000042831      0.000023390494
    31    31      0.003860891617     -2.110782022165
    32    32      0.008102642429     -4.442288992666
    33    33      0.000000013605     -0.000007418947
    34    34      0.008120198832     -4.441245166805
    35    35      0.000000103507     -0.000056562345
    36    36     -0.003173946824      1.731533882647
    37    37      0.000000061825     -0.000033764815
    38    38     -0.001007044382      0.550427024077
    39    39     -0.000000015749      0.000008655820
    40    40     -0.002154684630      1.175758689951
    41    41      0.000001545296     -0.000844543594
    42    42     -0.020727645448     11.338763600402
    43    43      0.000000114998     -0.000062747870
    44    44     -0.000000099534      0.000053841376
    45    45      0.026874530018    -14.694791154002
    46    46     -0.000000010123      0.000006215353
    47    47     -0.000000070082      0.000038286422
    48    48     -0.000268677779      0.147074390740
    49    49      0.000000034585     -0.000018893703
    50    50     -0.000000028269      0.000015477952

 number of reference csfs (nref) is    50.  root number (iroot) is  3.
 c0**2 =   0.70723262  c**2 (all zwalks) =   0.94213270

 pople ci energy extrapolation is computed with 18 correlated electrons.

 eref      =   -546.992088755419   "relaxed" cnot**2         =   0.707232622415
 eci       =   -547.157777960051   deltae = eci - eref       =  -0.165689204632
 eci+dv1   =   -547.206286353985   dv1 = (1-cnot**2)*deltae  =  -0.048508393934
 eci+dv2   =   -547.226366982102   dv2 = dv1 / cnot**2       =  -0.068589022051
 eci+dv3   =   -547.274816468199   dv3 = dv1 / (2*cnot**2-1) =  -0.117038508149
 eci+pople =   -547.244610351673   ( 18e- scaled deltae )    =  -0.252521596254
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
                       Size (real*8) of d2temp for two-external contributions      74382
 bummer (warning):inf%bufszi too small ... resetting to                    1190
 
                       Size (real*8) of d2temp for all-internal contributions       2805
                       Size (real*8) of d2temp for one-external contributions      24024
                       Size (real*8) of d2temp for two-external contributions      74382
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   29    12096
                1    2    1    1   29    12096
                1    3    1    1   29    12096
                1    4    1    1   29    12096
                1    5    1    1   29    12096
                1    6    1    1   29    12096
                1    7    1    1   29    12096
                1    8    1    1   29    12096
                1    9    1    1   29    12096
                1   10    1    1   29    12096
                1   11    1    1   29    12096
                       Size (real*8) of d2temp for three-external contributions     133056
                       Size (real*8) of d2temp for four-external contributions      94395
 serial operation: forcing vdisk for temporary dd012 matrices
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=  104721
 files%d4ext =     unit=  24  vdsk=   1  filestart=  284721
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                  1191      4761     29751         1         1
d2rec                     3        21        63         3         2
recsize                1190      1190      1190     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                 404721  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -474.481013326253     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.29813207   -72.81711874
 residuum:     0.00005467
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653812     0.00000000    -0.00000015    -0.09725199     0.00004626    -0.00082778    -0.03587067    -0.01862114
 ref:   2     0.00000019     0.97109898     0.05155842     0.00034534     0.03736405     0.02856471    -0.06412178    -0.04628641
 ref:   3     0.00000005     0.02501228    -0.80764855     0.00016017     0.46354812    -0.00245874     0.02404987     0.10446559

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653812     0.00000000    -0.00000015    -0.09725199     0.00004626     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000019     0.97109898     0.05155842     0.00034534     0.03736405     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000005     0.02501228    -0.80764855     0.00016017     0.46354812     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=    8527  D0X=       0  D0W=       0
  DDZI=    3020 DDYI=    6515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     645 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=  153125  D0X=       0  D0W=       0
  DDZI=   17280 DDYI=   36280 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    18.000000
   18  correlated and    14  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.99848744
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00169075
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00008045
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00039909
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00038260
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00067925
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00204792
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000004
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00038996
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00215797
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00256703
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00083886
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00009698
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00200251
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00054558
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003410
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00089773
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00075687
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00007554
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00007068
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00015807
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00028125
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00015643
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00021642
   MO  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00033910
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  44     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00016346
   MO  45     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00011914
   MO  46     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00012303

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   8    -0.00169075     0.00008045    -0.00039909     0.00038260     0.00000000     0.00000000    -0.00067925    -0.00204792
   MO   9     1.99247470    -0.00179354    -0.00008298    -0.00074270     0.00000000    -0.00000003     0.00067828    -0.00771200
   MO  10    -0.00179354     1.99388700     0.00256566    -0.00217360     0.00000000     0.00000007    -0.00195822    -0.00146849
   MO  11    -0.00008298     0.00256566     1.99685041     0.00024746    -0.00000001    -0.00000002     0.00302411    -0.00075920
   MO  12    -0.00074270    -0.00217360     0.00024746     1.99528841    -0.00000001    -0.00000024     0.00112300     0.00406805
   MO  13     0.00000000     0.00000000    -0.00000001    -0.00000001     1.98708544    -0.00434926    -0.00000004    -0.00000001
   MO  14    -0.00000003     0.00000007    -0.00000002    -0.00000024    -0.00434926     1.65579557    -0.00000026    -0.00000010
   MO  15     0.00067828    -0.00195822     0.00302411     0.00112300    -0.00000004    -0.00000026     1.97319487    -0.00452642
   MO  16    -0.00771200    -0.00146849    -0.00075920     0.00406805    -0.00000001    -0.00000010    -0.00452642     1.91757066
   MO  17     0.00000007    -0.00000009    -0.00000004     0.00000019    -0.04651187     0.34230071    -0.00000109     0.00000010
   MO  18    -0.00694319    -0.01808115     0.01828560    -0.00068972     0.00000004     0.00000022    -0.01790874    -0.07074390
   MO  19     0.00008854     0.00386484    -0.00523980    -0.00592190    -0.00000002    -0.00000009     0.07754278     0.01470511
   MO  20     0.00293166    -0.00023759    -0.00322223    -0.00448187    -0.00000001    -0.00000006     0.00722173     0.01473593
   MO  21     0.00290968     0.00165246     0.00086144     0.00026214    -0.00000001     0.00000000    -0.00227425     0.00397968
   MO  22     0.00000001    -0.00000001     0.00000001     0.00000000    -0.00568374    -0.00102860     0.00000005     0.00000000
   MO  23     0.00066971    -0.00229181     0.00386931     0.00215515     0.00000000     0.00000001    -0.01482686    -0.00129715
   MO  24     0.00000000     0.00000002    -0.00000002    -0.00000003     0.00033930    -0.01484059    -0.00000006    -0.00000003
   MO  25     0.00071126    -0.00088960     0.00711315     0.00599371    -0.00000004     0.00000002    -0.01777400     0.00554408
   MO  26    -0.00000002     0.00000000    -0.00000004    -0.00000002    -0.01344980    -0.00353283    -0.00000004    -0.00000005
   MO  27    -0.00101285     0.00079352    -0.00132495    -0.00302443    -0.00000001    -0.00000003     0.00052283     0.01327316
   MO  28    -0.00041800     0.00209968    -0.00137015    -0.00158472     0.00000000    -0.00000002     0.00425875     0.00710407
   MO  29     0.00205533     0.00042999    -0.00189746    -0.00748263    -0.00000001    -0.00000007     0.00727187     0.01693780
   MO  30    -0.00185114    -0.00164086     0.00211383     0.00273757     0.00000001     0.00000001     0.00112514    -0.02079524
   MO  31     0.00000000     0.00000002    -0.00000001    -0.00000003    -0.00054720     0.01065486    -0.00000003     0.00000001
   MO  32     0.00000000    -0.00000001    -0.00000001     0.00000000    -0.00367852     0.00177873    -0.00000006    -0.00000002
   MO  33     0.00086500    -0.00035366     0.00075139     0.00160316    -0.00000001    -0.00000002     0.00003557     0.00902425
   MO  34    -0.00068991    -0.00168318     0.00051075    -0.00082522     0.00000000     0.00000002     0.00213881    -0.01153346
   MO  35    -0.00190259    -0.00208752     0.00041336    -0.00122567     0.00000000     0.00000000     0.00104382    -0.00691250
   MO  36    -0.00147528    -0.00124814     0.00055167     0.00045568     0.00000000     0.00000000    -0.00133181    -0.00201381
   MO  37    -0.00009321     0.00002818    -0.00090208    -0.00072137     0.00000000     0.00000000     0.00140745    -0.00087158
   MO  38    -0.00000001     0.00000000     0.00000000     0.00000000     0.00012880     0.00004706     0.00000000     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00141465    -0.00067135    -0.00000001     0.00000000
   MO  40     0.00034988     0.00035558    -0.00034285    -0.00036437     0.00000000     0.00000000     0.00063139    -0.00060711
   MO  41     0.00039310     0.00062066     0.00016927     0.00042072     0.00000000     0.00000000    -0.00028235     0.00133967
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000    -0.00048570     0.00023972     0.00000001     0.00000001
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00074099    -0.00136159     0.00000000     0.00000000
   MO  44    -0.00012680     0.00020285     0.00011783     0.00078016     0.00000000     0.00000000     0.00048010     0.00223260
   MO  45    -0.00093930    -0.00039169     0.00025091     0.00100045     0.00000000     0.00000000    -0.00043298    -0.00495346
   MO  46     0.00016259     0.00007749     0.00026046    -0.00029849     0.00000000     0.00000000    -0.00038950     0.00039557

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   8     0.00000004    -0.00038996    -0.00215797     0.00256703     0.00083886     0.00000000     0.00009698     0.00000001
   MO   9     0.00000007    -0.00694319     0.00008854     0.00293166     0.00290968     0.00000001     0.00066971     0.00000000
   MO  10    -0.00000009    -0.01808115     0.00386484    -0.00023759     0.00165246    -0.00000001    -0.00229181     0.00000002
   MO  11    -0.00000004     0.01828560    -0.00523980    -0.00322223     0.00086144     0.00000001     0.00386931    -0.00000002
   MO  12     0.00000019    -0.00068972    -0.00592190    -0.00448187     0.00026214     0.00000000     0.00215515    -0.00000003
   MO  13    -0.04651187     0.00000004    -0.00000002    -0.00000001    -0.00000001    -0.00568374     0.00000000     0.00033930
   MO  14     0.34230071     0.00000022    -0.00000009    -0.00000006     0.00000000    -0.00102860     0.00000001    -0.01484059
   MO  15    -0.00000109    -0.01790874     0.07754278     0.00722173    -0.00227425     0.00000005    -0.01482686    -0.00000006
   MO  16     0.00000010    -0.07074390     0.01470511     0.01473593     0.00397968     0.00000000    -0.00129715    -0.00000003
   MO  17     0.35136959     0.00000004     0.00000002     0.00000000     0.00000000    -0.00012240     0.00000001    -0.01893592
   MO  18     0.00000004     0.08346507    -0.00320255    -0.00192372    -0.00123911     0.00000000     0.00044057     0.00000000
   MO  19     0.00000002    -0.00320255     0.01377492     0.00162661    -0.00016074     0.00000000    -0.00221938     0.00000002
   MO  20     0.00000000    -0.00192372     0.00162661     0.00193287     0.00066164     0.00000000     0.00005972     0.00000000
   MO  21     0.00000000    -0.00123911    -0.00016074     0.00066164     0.00081417     0.00000000     0.00003672     0.00000000
   MO  22    -0.00012240     0.00000000     0.00000000     0.00000000     0.00000000     0.00059666     0.00000000     0.00054873
   MO  23     0.00000001     0.00044057    -0.00221938     0.00005972     0.00003672     0.00000000     0.00072850     0.00000000
   MO  24    -0.01893592     0.00000000     0.00000002     0.00000000     0.00000000     0.00054873     0.00000000     0.00464458
   MO  25    -0.00000002     0.00035377    -0.00486499     0.00026300     0.00006319     0.00000000     0.00131441    -0.00000001
   MO  26     0.00674106     0.00000000     0.00000000     0.00000000     0.00000000     0.00084000     0.00000000     0.00071861
   MO  27    -0.00000002    -0.00140849     0.00015113     0.00081784     0.00006947     0.00000000     0.00009834     0.00000000
   MO  28     0.00000001    -0.00083616     0.00145076     0.00044551     0.00006148     0.00000000    -0.00021885     0.00000000
   MO  29     0.00000001    -0.00259730     0.00298553     0.00045382     0.00016309     0.00000000    -0.00059165     0.00000001
   MO  30     0.00000004     0.00281461     0.00331479    -0.00030881    -0.00049145     0.00000000    -0.00050869     0.00000001
   MO  31    -0.00780634     0.00000000     0.00000001     0.00000000     0.00000000     0.00030098     0.00000000     0.00156458
   MO  32     0.01587514     0.00000000     0.00000000     0.00000000     0.00000000     0.00045707     0.00000000    -0.00073625
   MO  33     0.00000005    -0.00126389     0.00305728     0.00103523     0.00004745     0.00000000    -0.00009514     0.00000001
   MO  34    -0.00000002     0.00178397    -0.00089493    -0.00071574    -0.00009969     0.00000000    -0.00015407     0.00000000
   MO  35     0.00000000    -0.00001809     0.00001740     0.00032344     0.00030941     0.00000000     0.00002948     0.00000000
   MO  36     0.00000000    -0.00039487    -0.00036323     0.00031310     0.00023365     0.00000000     0.00003963     0.00000000
   MO  37    -0.00000001    -0.00003433    -0.00041147    -0.00009035     0.00002787     0.00000000    -0.00000137     0.00000000
   MO  38    -0.00006014     0.00000000     0.00000000     0.00000000     0.00000000     0.00004348     0.00000000     0.00017491
   MO  39    -0.00021217     0.00000000     0.00000000     0.00000000     0.00000000     0.00004027     0.00000000     0.00042679
   MO  40     0.00000000     0.00005520    -0.00011126    -0.00016433    -0.00004243     0.00000000    -0.00001018     0.00000000
   MO  41     0.00000000    -0.00027796    -0.00020935     0.00017344     0.00009937     0.00000000     0.00005204     0.00000000
   MO  42     0.00048740     0.00000000     0.00000000     0.00000000     0.00000000     0.00002072     0.00000000    -0.00040535
   MO  43    -0.00134487     0.00000000     0.00000000     0.00000000     0.00000000    -0.00007129     0.00000000    -0.00004199
   MO  44     0.00000000    -0.00031770    -0.00005971     0.00017354     0.00003687     0.00000000     0.00006896     0.00000000
   MO  45     0.00000000     0.00090206    -0.00020200    -0.00021822    -0.00003089     0.00000000     0.00005954     0.00000000
   MO  46     0.00000000    -0.00016021    -0.00016292     0.00006463     0.00003683     0.00000000     0.00004146     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   8    -0.00200251     0.00000001     0.00054558    -0.00003410     0.00089773    -0.00075687     0.00000001     0.00000000
   MO   9     0.00071126    -0.00000002    -0.00101285    -0.00041800     0.00205533    -0.00185114     0.00000000     0.00000000
   MO  10    -0.00088960     0.00000000     0.00079352     0.00209968     0.00042999    -0.00164086     0.00000002    -0.00000001
   MO  11     0.00711315    -0.00000004    -0.00132495    -0.00137015    -0.00189746     0.00211383    -0.00000001    -0.00000001
   MO  12     0.00599371    -0.00000002    -0.00302443    -0.00158472    -0.00748263     0.00273757    -0.00000003     0.00000000
   MO  13    -0.00000004    -0.01344980    -0.00000001     0.00000000    -0.00000001     0.00000001    -0.00054720    -0.00367852
   MO  14     0.00000002    -0.00353283    -0.00000003    -0.00000002    -0.00000007     0.00000001     0.01065486     0.00177873
   MO  15    -0.01777400    -0.00000004     0.00052283     0.00425875     0.00727187     0.00112514    -0.00000003    -0.00000006
   MO  16     0.00554408    -0.00000005     0.01327316     0.00710407     0.01693780    -0.02079524     0.00000001    -0.00000002
   MO  17    -0.00000002     0.00674106    -0.00000002     0.00000001     0.00000001     0.00000004    -0.00780634     0.01587514
   MO  18     0.00035377     0.00000000    -0.00140849    -0.00083616    -0.00259730     0.00281461     0.00000000     0.00000000
   MO  19    -0.00486499     0.00000000     0.00015113     0.00145076     0.00298553     0.00331479     0.00000001     0.00000000
   MO  20     0.00026300     0.00000000     0.00081784     0.00044551     0.00045382    -0.00030881     0.00000000     0.00000000
   MO  21     0.00006319     0.00000000     0.00006947     0.00006148     0.00016309    -0.00049145     0.00000000     0.00000000
   MO  22     0.00000000     0.00084000     0.00000000     0.00000000     0.00000000     0.00000000     0.00030098     0.00045707
   MO  23     0.00131441     0.00000000     0.00009834    -0.00021885    -0.00059165    -0.00050869     0.00000000     0.00000000
   MO  24    -0.00000001     0.00071861     0.00000000     0.00000000     0.00000001     0.00000001     0.00156458    -0.00073625
   MO  25     0.00532310     0.00000000     0.00033435    -0.00060392    -0.00213712    -0.00247681     0.00000000     0.00000001
   MO  26     0.00000000     0.00479507     0.00000000     0.00000000     0.00000000     0.00000000     0.00145940     0.00238403
   MO  27     0.00033435     0.00000000     0.00244723     0.00055526    -0.00017915    -0.00025169     0.00000000     0.00000000
   MO  28    -0.00060392     0.00000000     0.00055526     0.00086714     0.00024923     0.00032931     0.00000000     0.00000000
   MO  29    -0.00213712     0.00000000    -0.00017915     0.00024923     0.00184633     0.00083338     0.00000000     0.00000000
   MO  30    -0.00247681     0.00000000    -0.00025169     0.00032931     0.00083338     0.00263447     0.00000001     0.00000000
   MO  31     0.00000000     0.00145940     0.00000000     0.00000000     0.00000000     0.00000001     0.00223769    -0.00022772
   MO  32     0.00000001     0.00238403     0.00000000     0.00000000     0.00000000     0.00000000    -0.00022772     0.00214973
   MO  33    -0.00047549     0.00000000     0.00010275     0.00036517     0.00076160     0.00101012     0.00000000     0.00000000
   MO  34    -0.00005793     0.00000000    -0.00021313    -0.00036592    -0.00032235    -0.00011241     0.00000000     0.00000000
   MO  35     0.00005980     0.00000000    -0.00064828    -0.00035935     0.00023984    -0.00005423     0.00000000     0.00000000
   MO  36     0.00017404     0.00000000    -0.00044124    -0.00006252     0.00007731    -0.00021263     0.00000000     0.00000000
   MO  37     0.00018719     0.00000000     0.00009631    -0.00007454     0.00000191     0.00001719     0.00000000     0.00000000
   MO  38     0.00000000    -0.00002102     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000183     0.00002885
   MO  39     0.00000000     0.00004063     0.00000000     0.00000000     0.00000000     0.00000000    -0.00004947     0.00004842
   MO  40     0.00010166     0.00000000    -0.00042331    -0.00019508     0.00018715    -0.00018637     0.00000000     0.00000000
   MO  41     0.00002304     0.00000000     0.00006475     0.00016941     0.00002450    -0.00008301     0.00000000     0.00000000
   MO  42     0.00000000    -0.00004052     0.00000000     0.00000000     0.00000000     0.00000000     0.00005149    -0.00006204
   MO  43     0.00000000    -0.00042495     0.00000000     0.00000000     0.00000000     0.00000000    -0.00012723    -0.00023018
   MO  44     0.00035121     0.00000000     0.00017026     0.00002281    -0.00004032    -0.00002611     0.00000000     0.00000000
   MO  45     0.00003652     0.00000000    -0.00040527    -0.00018383     0.00005425    -0.00001985     0.00000000     0.00000000
   MO  46    -0.00000546     0.00000000     0.00002698     0.00012267    -0.00002486    -0.00000203     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38         MO  39         MO  40
   MO   8    -0.00007554     0.00007068    -0.00015807    -0.00028125     0.00015643     0.00000000     0.00000000     0.00021642
   MO   9     0.00086500    -0.00068991    -0.00190259    -0.00147528    -0.00009321    -0.00000001     0.00000000     0.00034988
   MO  10    -0.00035366    -0.00168318    -0.00208752    -0.00124814     0.00002818     0.00000000     0.00000000     0.00035558
   MO  11     0.00075139     0.00051075     0.00041336     0.00055167    -0.00090208     0.00000000     0.00000000    -0.00034285
   MO  12     0.00160316    -0.00082522    -0.00122567     0.00045568    -0.00072137     0.00000000     0.00000000    -0.00036437
   MO  13    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00012880     0.00141465     0.00000000
   MO  14    -0.00000002     0.00000002     0.00000000     0.00000000     0.00000000     0.00004706    -0.00067135     0.00000000
   MO  15     0.00003557     0.00213881     0.00104382    -0.00133181     0.00140745     0.00000000    -0.00000001     0.00063139
   MO  16     0.00902425    -0.01153346    -0.00691250    -0.00201381    -0.00087158     0.00000000     0.00000000    -0.00060711
   MO  17     0.00000005    -0.00000002     0.00000000     0.00000000    -0.00000001    -0.00006014    -0.00021217     0.00000000
   MO  18    -0.00126389     0.00178397    -0.00001809    -0.00039487    -0.00003433     0.00000000     0.00000000     0.00005520
   MO  19     0.00305728    -0.00089493     0.00001740    -0.00036323    -0.00041147     0.00000000     0.00000000    -0.00011126
   MO  20     0.00103523    -0.00071574     0.00032344     0.00031310    -0.00009035     0.00000000     0.00000000    -0.00016433
   MO  21     0.00004745    -0.00009969     0.00030941     0.00023365     0.00002787     0.00000000     0.00000000    -0.00004243
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00004348     0.00004027     0.00000000
   MO  23    -0.00009514    -0.00015407     0.00002948     0.00003963    -0.00000137     0.00000000     0.00000000    -0.00001018
   MO  24     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00017491     0.00042679     0.00000000
   MO  25    -0.00047549    -0.00005793     0.00005980     0.00017404     0.00018719     0.00000000     0.00000000     0.00010166
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00002102     0.00004063     0.00000000
   MO  27     0.00010275    -0.00021313    -0.00064828    -0.00044124     0.00009631     0.00000000     0.00000000    -0.00042331
   MO  28     0.00036517    -0.00036592    -0.00035935    -0.00006252    -0.00007454     0.00000000     0.00000000    -0.00019508
   MO  29     0.00076160    -0.00032235     0.00023984     0.00007731     0.00000191     0.00000000     0.00000000     0.00018715
   MO  30     0.00101012    -0.00011241    -0.00005423    -0.00021263     0.00001719     0.00000000     0.00000000    -0.00018637
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000183    -0.00004947     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002885     0.00004842     0.00000000
   MO  33     0.00183898    -0.00061113     0.00001537    -0.00000996    -0.00012079     0.00000000     0.00000000     0.00001905
   MO  34    -0.00061113     0.00093973     0.00006765    -0.00008851     0.00009935     0.00000000     0.00000000    -0.00001163
   MO  35     0.00001537     0.00006765     0.00091025     0.00048927     0.00003484     0.00000000     0.00000000    -0.00001167
   MO  36    -0.00000996    -0.00008851     0.00048927     0.00045470    -0.00004789     0.00000000     0.00000000     0.00001472
   MO  37    -0.00012079     0.00009935     0.00003484    -0.00004789     0.00074528     0.00000000     0.00000000    -0.00008024
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00091558    -0.00022943     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00022943     0.00064500     0.00000000
   MO  40     0.00001905    -0.00001163    -0.00001167     0.00001472    -0.00008024     0.00000000     0.00000000     0.00092898
   MO  41     0.00000979    -0.00007031     0.00000346     0.00004894     0.00001066     0.00000000     0.00000000     0.00003027
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00019447    -0.00042454     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00007890    -0.00004073     0.00000000
   MO  44     0.00011135    -0.00010818     0.00006570     0.00002551     0.00033258     0.00000000     0.00000000    -0.00004257
   MO  45     0.00007818     0.00010114    -0.00019852    -0.00016754    -0.00001066     0.00000000     0.00000000     0.00037715
   MO  46    -0.00001759    -0.00003186     0.00003411     0.00006148     0.00001060     0.00000000     0.00000000    -0.00008302

                MO  41         MO  42         MO  43         MO  44         MO  45         MO  46
   MO   8    -0.00033910     0.00000000     0.00000000    -0.00016346    -0.00011914    -0.00012303
   MO   9     0.00039310     0.00000000     0.00000000    -0.00012680    -0.00093930     0.00016259
   MO  10     0.00062066     0.00000000     0.00000000     0.00020285    -0.00039169     0.00007749
   MO  11     0.00016927     0.00000000     0.00000000     0.00011783     0.00025091     0.00026046
   MO  12     0.00042072     0.00000000     0.00000000     0.00078016     0.00100045    -0.00029849
   MO  13     0.00000000    -0.00048570     0.00074099     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00023972    -0.00136159     0.00000000     0.00000000     0.00000000
   MO  15    -0.00028235     0.00000001     0.00000000     0.00048010    -0.00043298    -0.00038950
   MO  16     0.00133967     0.00000001     0.00000000     0.00223260    -0.00495346     0.00039557
   MO  17     0.00000000     0.00048740    -0.00134487     0.00000000     0.00000000     0.00000000
   MO  18    -0.00027796     0.00000000     0.00000000    -0.00031770     0.00090206    -0.00016021
   MO  19    -0.00020935     0.00000000     0.00000000    -0.00005971    -0.00020200    -0.00016292
   MO  20     0.00017344     0.00000000     0.00000000     0.00017354    -0.00021822     0.00006463
   MO  21     0.00009937     0.00000000     0.00000000     0.00003687    -0.00003089     0.00003683
   MO  22     0.00000000     0.00002072    -0.00007129     0.00000000     0.00000000     0.00000000
   MO  23     0.00005204     0.00000000     0.00000000     0.00006896     0.00005954     0.00004146
   MO  24     0.00000000    -0.00040535    -0.00004199     0.00000000     0.00000000     0.00000000
   MO  25     0.00002304     0.00000000     0.00000000     0.00035121     0.00003652    -0.00000546
   MO  26     0.00000000    -0.00004052    -0.00042495     0.00000000     0.00000000     0.00000000
   MO  27     0.00006475     0.00000000     0.00000000     0.00017026    -0.00040527     0.00002698
   MO  28     0.00016941     0.00000000     0.00000000     0.00002281    -0.00018383     0.00012267
   MO  29     0.00002450     0.00000000     0.00000000    -0.00004032     0.00005425    -0.00002486
   MO  30    -0.00008301     0.00000000     0.00000000    -0.00002611    -0.00001985    -0.00000203
   MO  31     0.00000000     0.00005149    -0.00012723     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000    -0.00006204    -0.00023018     0.00000000     0.00000000     0.00000000
   MO  33     0.00000979     0.00000000     0.00000000     0.00011135     0.00007818    -0.00001759
   MO  34    -0.00007031     0.00000000     0.00000000    -0.00010818     0.00010114    -0.00003186
   MO  35     0.00000346     0.00000000     0.00000000     0.00006570    -0.00019852     0.00003411
   MO  36     0.00004894     0.00000000     0.00000000     0.00002551    -0.00016754     0.00006148
   MO  37     0.00001066     0.00000000     0.00000000     0.00033258    -0.00001066     0.00001060
   MO  38     0.00000000    -0.00019447     0.00007890     0.00000000     0.00000000     0.00000000
   MO  39     0.00000000    -0.00042454    -0.00004073     0.00000000     0.00000000     0.00000000
   MO  40     0.00003027     0.00000000     0.00000000    -0.00004257     0.00037715    -0.00008302
   MO  41     0.00030222     0.00000000     0.00000000     0.00002960     0.00001146     0.00010435
   MO  42     0.00000000     0.00090656    -0.00004786     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000    -0.00004786     0.00007466     0.00000000     0.00000000     0.00000000
   MO  44     0.00002960     0.00000000     0.00000000     0.00024281    -0.00012319     0.00001866
   MO  45     0.00001146     0.00000000     0.00000000    -0.00012319     0.00073544    -0.00009980
   MO  46     0.00010435     0.00000000     0.00000000     0.00001866    -0.00009980     0.00009821

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99919106
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99874006     1.99667115     1.99417391     1.99042244     1.98928362     1.97636168     1.91999078     1.73948630
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.26813103     0.08043358     0.01636175     0.00699210     0.00454792     0.00327264     0.00297746     0.00224790
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00140249     0.00133437     0.00131365     0.00105225     0.00100758     0.00087608     0.00072765     0.00062483
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00043881     0.00036448     0.00034731     0.00024922     0.00021648     0.00019027     0.00016652     0.00014332
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO
  occ(*)=     0.00011847     0.00004562     0.00003022     0.00002646     0.00002154     0.00001701


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     1_ s       2.000501   0.000166   0.000087   2.000004   0.000000   0.000131
     1_ p       0.000134   0.000236   0.000149   0.000000   2.000010   1.999807
     1_ d       0.000001   0.000015   0.000016   0.000000   0.000000   0.000001
     3_ s       0.000033   0.000000   1.999749   0.000000   0.000000   0.000002
     3_ p      -0.000093  -0.000001   0.000001   0.000000   0.000002   0.000005
     3_ d      -0.000003   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s      -0.000055   1.999583   0.000000   0.000000   0.000000   0.000019
     4_ p      -0.000500   0.000003  -0.000001  -0.000003  -0.000013   0.000020
     4_ d      -0.000016   0.000000   0.000000   0.000000   0.000001   0.000015
 
   ao class       7a         8a         9a        10a        11a        12a  
     1_ s       0.000003   0.257647   0.020288   0.133713   0.922153   0.408834
     1_ p       2.000012   0.464544   0.034254   0.500909   0.233449   0.069791
     1_ d       0.000003   0.052958  -0.004260   0.047063   0.012130   0.015375
     3_ s      -0.000008   0.005799   0.000829   0.002286   0.660277   1.280421
     3_ p      -0.000018   0.022025   0.003702   0.078389   0.044550   0.073257
     3_ d       0.000000   0.000243   0.000088   0.000515   0.001311   0.000312
     4_ s      -0.000006   0.566506   1.271181   0.008090   0.009604   0.002336
     4_ p      -0.000005   0.622739   0.670768   1.221277   0.110507   0.139740
     4_ d       0.000019   0.006730   0.001889   0.004429   0.000192   0.000355
 
   ao class      13a        14a        15a        16a        17a        18a  
     1_ s       0.000000   0.054425  -0.000366   0.000000   0.000000   0.002704
     1_ p       0.969785   0.025623   0.502628   0.229126   0.098973   0.039023
     1_ d       0.032168   0.037017   0.061075   0.048738   0.002206   0.000200
     3_ s       0.000000   0.000603  -0.004875   0.000000   0.000000   0.000238
     3_ p       0.037898   1.764103   1.136801   1.026451   0.105006   0.032628
     3_ d       0.000682   0.000623   0.002964   0.001362  -0.000021   0.000109
     4_ s       0.000000   0.009696   0.000064   0.000000   0.000000   0.000022
     4_ p       0.942898   0.084141   0.221928   0.433962   0.061996   0.005479
     4_ d       0.005852   0.000131  -0.000228  -0.000153  -0.000028   0.000030
 
   ao class      19a        20a        21a        22a        23a        24a  
     1_ s       0.000602   0.000000   0.000010   0.000000   0.000146   0.000220
     1_ p       0.005262   0.000352   0.000941   0.000152   0.000093   0.000152
     1_ d       0.002854   0.003442   0.001932   0.002037   0.000768   0.000967
     3_ s       0.000008   0.000000  -0.000006   0.000000   0.001529   0.000418
     3_ p       0.006199   0.000508   0.000095   0.000569   0.000097   0.000101
     3_ d       0.000083   0.000056   0.000165   0.000220   0.000192   0.000260
     4_ s      -0.000154   0.000000  -0.000003   0.000000   0.000001  -0.000003
     4_ p       0.001493   0.002530   0.001350   0.000292   0.000151   0.000108
     4_ d       0.000015   0.000104   0.000063   0.000003   0.000001   0.000026
 
   ao class      25a        26a        27a        28a        29a        30a  
     1_ s       0.000000   0.000010   0.000043   0.000000   0.000007   0.000000
     1_ p      -0.000004   0.000083   0.000103   0.000002   0.000061   0.000017
     1_ d       0.000034   0.000070   0.000118   0.000001   0.000175   0.000263
     3_ s       0.000000   0.000026   0.000004   0.000000   0.000013   0.000000
     3_ p       0.000607   0.000273   0.000566   0.000004   0.000015   0.000266
     3_ d       0.000761   0.000754   0.000380   0.001044   0.000516   0.000328
     4_ s       0.000000   0.000021   0.000007   0.000000   0.000086   0.000000
     4_ p       0.000003   0.000082   0.000086   0.000002   0.000096   0.000001
     4_ d       0.000001   0.000016   0.000008   0.000000   0.000038   0.000002
 
   ao class      31a        32a        33a        34a        35a        36a  
     1_ s       0.000034   0.000124   0.000000   0.000000   0.000080   0.000013
     1_ p       0.000014   0.000018   0.000009   0.000250   0.000017   0.000037
     1_ d       0.000276   0.000044   0.000036   0.000081   0.000018   0.000045
     3_ s       0.000008   0.000007  -0.000001   0.000000   0.000010   0.000001
     3_ p       0.000025   0.000006   0.000048   0.000005   0.000073   0.000015
     3_ d       0.000234   0.000287   0.000142   0.000005   0.000044   0.000003
     4_ s       0.000068   0.000009   0.000022   0.000000   0.000006   0.000024
     4_ p       0.000030   0.000110   0.000145   0.000011   0.000053   0.000037
     4_ d       0.000038   0.000021   0.000037   0.000014   0.000046   0.000074
 
   ao class      37a        38a        39a        40a        41a        42a  
     1_ s       0.000024   0.000000  -0.000002   0.000000   0.000018   0.000003
     1_ p       0.000070   0.000001   0.000020   0.000029   0.000048   0.000019
     1_ d       0.000021   0.000002   0.000029   0.000032   0.000001   0.000005
     3_ s       0.000004   0.000000   0.000012   0.000000   0.000002   0.000006
     3_ p       0.000031   0.000000   0.000032   0.000000   0.000004   0.000003
     3_ d       0.000011   0.000000   0.000014   0.000000   0.000002   0.000001
     4_ s      -0.000001   0.000000   0.000013   0.000000   0.000000   0.000000
     4_ p       0.000018   0.000001   0.000024   0.000080   0.000018   0.000010
     4_ d       0.000039   0.000186   0.000023   0.000003   0.000026   0.000000
 
   ao class      43a        44a        45a        46a  
     1_ s       0.000000  -0.000001   0.000000   0.000000
     1_ p       0.000001   0.000005   0.000000   0.000000
     1_ d       0.000000   0.000002   0.000001   0.000001
     3_ s       0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000
     4_ s       0.000001   0.000014   0.000000   0.000001
     4_ p       0.000003   0.000003   0.000000   0.000000
     4_ d       0.000024   0.000003   0.000020   0.000015


                        gross atomic populations
     ao            1_         3_         4_
      s         5.801622   3.947396   3.867149
      p         9.176205   4.334248   4.521669
      d         0.317960   0.013686   0.020064
    total      15.295787   8.295330   8.408883
 

 Total number of electrons:   32.00000000

 item #                     2 suffix=:.drt1.state2:
 read_civout: repnuc=  -474.481013326253     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.29813207   -72.81711874
 residuum:     0.00005467
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.24325180   -72.76223847
 residuum:     0.00007067
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653812     0.00000000    -0.00000015    -0.09725199     0.00004626    -0.00082778    -0.03587067    -0.01862114
 ref:   2     0.00000019     0.97109898     0.05155842     0.00034534     0.03736405     0.02856471    -0.06412178    -0.04628641
 ref:   3     0.00000005     0.02501228    -0.80764855     0.00016017     0.46354812    -0.00245874     0.02404987     0.10446559

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653812     0.00000000    -0.00000015    -0.09725199     0.00004626     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000019     0.97109898     0.05155842     0.00034534     0.03736405     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000005     0.02501228    -0.80764855     0.00016017     0.46354812     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=    8527  D0X=       0  D0W=       0
  DDZI=    3020 DDYI=    6515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     645 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=  153125  D0X=       0  D0W=       0
  DDZI=   17280 DDYI=   36280 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    18.000000
   18  correlated and    14  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.99839093
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00196255
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00022670
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00024798
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00051200
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00183364
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00208168
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00076355
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00051653
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00013599
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00266513
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00174219
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00260421
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003358
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00050220
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00002494
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00100699
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00054626
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000166
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00159944
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00019193
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00008533
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00031575
   MO  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00029392
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  44     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00019980
   MO  45     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00020786
   MO  46     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00007689

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   8    -0.00196255     0.00022670    -0.00024798     0.00051200     0.00000000     0.00000000    -0.00183364    -0.00208168
   MO   9     1.99187676    -0.00157805    -0.00007897    -0.00063947     0.00000000     0.00000000    -0.00082731    -0.00730672
   MO  10    -0.00157805     1.99372980     0.00173407    -0.00255894     0.00000000     0.00000000     0.00417910    -0.00097412
   MO  11    -0.00007897     0.00173407     1.99380673    -0.00306110     0.00000000     0.00000000     0.03155808    -0.00265145
   MO  12    -0.00063947    -0.00255894    -0.00306110     1.99229793    -0.00000001     0.00000000     0.03067026     0.00189099
   MO  13     0.00000000     0.00000000     0.00000000    -0.00000001     1.98838859    -0.00944822    -0.00000004     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000    -0.00944822     1.93653647    -0.00000018     0.00000001
   MO  15    -0.00082731     0.00417910     0.03155808     0.03067026    -0.00000004    -0.00000018     1.00510149     0.00530525
   MO  16    -0.00730672    -0.00097412    -0.00265145     0.00189099     0.00000000     0.00000001     0.00530525     1.93189442
   MO  17     0.00000000     0.00000001     0.00000000     0.00000001    -0.03000516    -0.19932514    -0.00000033     0.00000003
   MO  18     0.00098992    -0.00177438     0.00869751    -0.01168013     0.00000002     0.00000008     0.00026050     0.00619646
   MO  19     0.00137858    -0.00417417     0.00912106     0.01086960     0.00000000     0.00000003    -0.01617425     0.00845682
   MO  20    -0.00063578    -0.00066681     0.00016248    -0.00547659    -0.00000001    -0.00000001    -0.00162944     0.00335978
   MO  21    -0.00013191    -0.00770264     0.00397678    -0.00123219     0.00000000     0.00000000     0.00124593    -0.00197149
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000    -0.00830397    -0.00299107     0.00000000     0.00000000
   MO  23     0.00078773    -0.00081788     0.00388912     0.00011527     0.00000000     0.00000000    -0.00006241     0.00154313
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00303396    -0.00077330    -0.00000001     0.00000000
   MO  25    -0.00096166     0.00100328    -0.01000539     0.00067524    -0.00000002    -0.00000001     0.01164484     0.00943338
   MO  26     0.00000000     0.00000000     0.00000002     0.00000000    -0.00665375     0.00595740    -0.00000002    -0.00000001
   MO  27     0.00132017     0.00358120    -0.00296599     0.00718450    -0.00000001     0.00000000     0.00371203     0.00686214
   MO  28     0.00091993     0.00129577     0.00431196     0.00168049     0.00000000     0.00000000    -0.00276901     0.00271875
   MO  29     0.00079343    -0.00232134     0.00189279     0.00072513     0.00000000     0.00000000    -0.00758121     0.00580144
   MO  30    -0.00143115     0.00055366     0.00176371     0.00369494     0.00000001     0.00000003    -0.01580609    -0.01044689
   MO  31     0.00000000     0.00000000     0.00000000    -0.00000001    -0.00347031     0.01112391     0.00000001     0.00000002
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000    -0.00208388     0.00476543    -0.00000001    -0.00000001
   MO  33     0.00032371     0.00034780     0.00190858     0.00143083     0.00000000     0.00000001    -0.01176389     0.00811845
   MO  34    -0.00062971     0.00165899     0.00006226     0.00098135     0.00000001     0.00000000     0.00638445    -0.00566492
   MO  35    -0.00451886    -0.00182158     0.00015310    -0.00003100     0.00000000     0.00000000     0.00064766    -0.00535222
   MO  36    -0.00318966    -0.00039585    -0.00045551    -0.00225598     0.00000000     0.00000000     0.00025282    -0.00312973
   MO  37     0.00002205     0.00029409    -0.00026907    -0.00065383     0.00000000     0.00000000    -0.00112885    -0.00109875
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000    -0.00018206     0.00022032     0.00000000     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000    -0.00079351    -0.00029578     0.00000000     0.00000000
   MO  40    -0.00107094    -0.00011430    -0.00021519     0.00008270     0.00000000     0.00000000    -0.00027898    -0.00005893
   MO  41    -0.00011766     0.00034512    -0.00046634    -0.00117480     0.00000000     0.00000000     0.00001171    -0.00015234
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000    -0.00071963    -0.00000820     0.00000000     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00021453    -0.00120702     0.00000000     0.00000000
   MO  44    -0.00025286     0.00006180    -0.00085130     0.00022502     0.00000000     0.00000000    -0.00123793     0.00087676
   MO  45    -0.00040879    -0.00103667     0.00014736    -0.00065524     0.00000000     0.00000000    -0.00025310    -0.00047612
   MO  46    -0.00004493    -0.00004035    -0.00047265     0.00030059     0.00000000     0.00000000    -0.00000693    -0.00013671

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   8     0.00000000    -0.00076355    -0.00051653     0.00013599     0.00266513     0.00000000    -0.00174219     0.00000000
   MO   9     0.00000000     0.00098992     0.00137858    -0.00063578    -0.00013191     0.00000000     0.00078773     0.00000000
   MO  10     0.00000001    -0.00177438    -0.00417417    -0.00066681    -0.00770264     0.00000000    -0.00081788     0.00000000
   MO  11     0.00000000     0.00869751     0.00912106     0.00016248     0.00397678     0.00000000     0.00388912     0.00000000
   MO  12     0.00000001    -0.01168013     0.01086960    -0.00547659    -0.00123219     0.00000000     0.00011527     0.00000000
   MO  13    -0.03000516     0.00000002     0.00000000    -0.00000001     0.00000000    -0.00830397     0.00000000     0.00303396
   MO  14    -0.19932514     0.00000008     0.00000003    -0.00000001     0.00000000    -0.00299107     0.00000000    -0.00077330
   MO  15    -0.00000033     0.00026050    -0.01617425    -0.00162944     0.00124593     0.00000000    -0.00006241    -0.00000001
   MO  16     0.00000003     0.00619646     0.00845682     0.00335978    -0.00197149     0.00000000     0.00154313     0.00000000
   MO  17     1.05389522    -0.00000003     0.00000011     0.00000001     0.00000000    -0.00277933    -0.00000001     0.01085518
   MO  18    -0.00000003     0.06953482    -0.00106819    -0.00039014    -0.00061497     0.00000000     0.00004457     0.00000000
   MO  19     0.00000011    -0.00106819     0.00395972     0.00028690     0.00001432     0.00000000    -0.00051575     0.00000000
   MO  20     0.00000001    -0.00039014     0.00028690     0.00168938     0.00062896     0.00000000     0.00022033     0.00000000
   MO  21     0.00000000    -0.00061497     0.00001432     0.00062896     0.00071885     0.00000000     0.00003552     0.00000000
   MO  22    -0.00277933     0.00000000     0.00000000     0.00000000     0.00000000     0.00090615     0.00000000    -0.00015000
   MO  23    -0.00000001     0.00004457    -0.00051575     0.00022033     0.00003552     0.00000000     0.00038185     0.00000000
   MO  24     0.01085518     0.00000000     0.00000000     0.00000000     0.00000000    -0.00015000     0.00000000     0.00522899
   MO  25    -0.00000004    -0.00048708    -0.00085258     0.00048467    -0.00006672     0.00000000     0.00044536     0.00000000
   MO  26     0.00225197     0.00000000     0.00000000     0.00000000     0.00000000     0.00124976     0.00000000    -0.00002411
   MO  27     0.00000000     0.00018219    -0.00036522     0.00108849     0.00020580     0.00000000     0.00022899     0.00000000
   MO  28     0.00000001     0.00015178     0.00047932     0.00026111     0.00007996     0.00000000    -0.00003574     0.00000000
   MO  29     0.00000003    -0.00031145     0.00083672     0.00015566     0.00015820     0.00000000    -0.00014303     0.00000000
   MO  30     0.00000004     0.00061955     0.00128718    -0.00040493    -0.00031438     0.00000000    -0.00014915     0.00000000
   MO  31     0.01248697     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003573     0.00000000     0.00283489
   MO  32     0.00509585     0.00000000     0.00000000     0.00000000     0.00000000     0.00074295     0.00000000    -0.00122990
   MO  33     0.00000002    -0.00057995     0.00129380     0.00078900     0.00009714     0.00000000     0.00018372     0.00000000
   MO  34    -0.00000001     0.00029523    -0.00062350    -0.00044855    -0.00006454     0.00000000    -0.00008748     0.00000000
   MO  35     0.00000000    -0.00084998     0.00000806     0.00029415     0.00027142     0.00000000     0.00001481     0.00000000
   MO  36     0.00000000    -0.00054050    -0.00010523     0.00030134     0.00020035     0.00000000    -0.00002592     0.00000000
   MO  37     0.00000000    -0.00006806    -0.00035875    -0.00010864     0.00003294     0.00000000     0.00002100     0.00000000
   MO  38    -0.00002801     0.00000000     0.00000000     0.00000000     0.00000000     0.00003002     0.00000000     0.00019745
   MO  39    -0.00027844     0.00000000     0.00000000     0.00000000     0.00000000    -0.00004095     0.00000000     0.00036552
   MO  40     0.00000000     0.00013610    -0.00007627     0.00004154     0.00000496     0.00000000     0.00001834     0.00000000
   MO  41     0.00000000    -0.00006427    -0.00028769     0.00021569     0.00010217     0.00000000     0.00007140     0.00000000
   MO  42    -0.00020325     0.00000000     0.00000000     0.00000000     0.00000000     0.00012769     0.00000000    -0.00033594
   MO  43    -0.00185072     0.00000000     0.00000000     0.00000000     0.00000000    -0.00013038     0.00000000    -0.00000280
   MO  44     0.00000000    -0.00016953    -0.00007953     0.00015124     0.00004197     0.00000000     0.00006694     0.00000000
   MO  45     0.00000000     0.00032634    -0.00018073    -0.00017975     0.00000217     0.00000000     0.00004013     0.00000000
   MO  46     0.00000000    -0.00009760    -0.00019187     0.00005819     0.00003564     0.00000000     0.00004446     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   8     0.00260421     0.00000000    -0.00003358    -0.00050220    -0.00002494    -0.00100699     0.00000000     0.00000000
   MO   9    -0.00096166     0.00000000     0.00132017     0.00091993     0.00079343    -0.00143115     0.00000000     0.00000000
   MO  10     0.00100328     0.00000000     0.00358120     0.00129577    -0.00232134     0.00055366     0.00000000     0.00000000
   MO  11    -0.01000539     0.00000002    -0.00296599     0.00431196     0.00189279     0.00176371     0.00000000     0.00000000
   MO  12     0.00067524     0.00000000     0.00718450     0.00168049     0.00072513     0.00369494    -0.00000001     0.00000000
   MO  13    -0.00000002    -0.00665375    -0.00000001     0.00000000     0.00000000     0.00000001    -0.00347031    -0.00208388
   MO  14    -0.00000001     0.00595740     0.00000000     0.00000000     0.00000000     0.00000003     0.01112391     0.00476543
   MO  15     0.01164484    -0.00000002     0.00371203    -0.00276901    -0.00758121    -0.01580609     0.00000001    -0.00000001
   MO  16     0.00943338    -0.00000001     0.00686214     0.00271875     0.00580144    -0.01044689     0.00000002    -0.00000001
   MO  17    -0.00000004     0.00225197     0.00000000     0.00000001     0.00000003     0.00000004     0.01248697     0.00509585
   MO  18    -0.00048708     0.00000000     0.00018219     0.00015178    -0.00031145     0.00061955     0.00000000     0.00000000
   MO  19    -0.00085258     0.00000000    -0.00036522     0.00047932     0.00083672     0.00128718     0.00000000     0.00000000
   MO  20     0.00048467     0.00000000     0.00108849     0.00026111     0.00015566    -0.00040493     0.00000000     0.00000000
   MO  21    -0.00006672     0.00000000     0.00020580     0.00007996     0.00015820    -0.00031438     0.00000000     0.00000000
   MO  22     0.00000000     0.00124976     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003573     0.00074295
   MO  23     0.00044536     0.00000000     0.00022899    -0.00003574    -0.00014303    -0.00014915     0.00000000     0.00000000
   MO  24     0.00000000    -0.00002411     0.00000000     0.00000000     0.00000000     0.00000000     0.00283489    -0.00122990
   MO  25     0.00361316     0.00000000     0.00015524    -0.00016166    -0.00118605    -0.00145646     0.00000000     0.00000000
   MO  26     0.00000000     0.00586783     0.00000000     0.00000000     0.00000000     0.00000000     0.00159496     0.00305004
   MO  27     0.00015524     0.00000000     0.00283896     0.00044930    -0.00032381    -0.00018430     0.00000000     0.00000000
   MO  28    -0.00016166     0.00000000     0.00044930     0.00070808    -0.00006253     0.00022473     0.00000000     0.00000000
   MO  29    -0.00118605     0.00000000    -0.00032381    -0.00006253     0.00115523     0.00049969     0.00000000     0.00000000
   MO  30    -0.00145646     0.00000000    -0.00018430     0.00022473     0.00049969     0.00168357     0.00000000     0.00000000
   MO  31     0.00000000     0.00159496     0.00000000     0.00000000     0.00000000     0.00000000     0.00379794    -0.00051893
   MO  32     0.00000000     0.00305004     0.00000000     0.00000000     0.00000000     0.00000000    -0.00051893     0.00256020
   MO  33     0.00008751     0.00000000     0.00022494     0.00016773     0.00039743     0.00056904     0.00000000     0.00000000
   MO  34    -0.00026096     0.00000000    -0.00012146    -0.00025475    -0.00008321    -0.00020168     0.00000000     0.00000000
   MO  35     0.00007339     0.00000000    -0.00061620    -0.00030560     0.00029158    -0.00013618     0.00000000     0.00000000
   MO  36     0.00006427     0.00000000    -0.00036894    -0.00005060     0.00012007    -0.00014592     0.00000000     0.00000000
   MO  37     0.00031820     0.00000000    -0.00004586    -0.00012399     0.00011696     0.00001142     0.00000000     0.00000000
   MO  38     0.00000000    -0.00001636     0.00000000     0.00000000     0.00000000     0.00000000     0.00005410     0.00002345
   MO  39     0.00000000    -0.00000119     0.00000000     0.00000000     0.00000000     0.00000000     0.00002149    -0.00000691
   MO  40     0.00005551     0.00000000     0.00044475     0.00015388    -0.00031896     0.00018819     0.00000000     0.00000000
   MO  41     0.00000814     0.00000000     0.00023495     0.00023587    -0.00006911    -0.00002084     0.00000000     0.00000000
   MO  42     0.00000000     0.00003731     0.00000000     0.00000000     0.00000000     0.00000000    -0.00008026     0.00001993
   MO  43     0.00000000    -0.00060011     0.00000000     0.00000000     0.00000000     0.00000000    -0.00019066    -0.00032653
   MO  44     0.00039814     0.00000000     0.00018305     0.00002236    -0.00004936     0.00001507     0.00000000     0.00000000
   MO  45    -0.00003349     0.00000000    -0.00034137    -0.00017605     0.00020435    -0.00012002     0.00000000     0.00000000
   MO  46    -0.00000927     0.00000000     0.00004442     0.00012930    -0.00003517     0.00000976     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38         MO  39         MO  40
   MO   8     0.00054626    -0.00000166    -0.00159944    -0.00019193    -0.00008533     0.00000000     0.00000000    -0.00031575
   MO   9     0.00032371    -0.00062971    -0.00451886    -0.00318966     0.00002205     0.00000000     0.00000000    -0.00107094
   MO  10     0.00034780     0.00165899    -0.00182158    -0.00039585     0.00029409     0.00000000     0.00000000    -0.00011430
   MO  11     0.00190858     0.00006226     0.00015310    -0.00045551    -0.00026907     0.00000000     0.00000000    -0.00021519
   MO  12     0.00143083     0.00098135    -0.00003100    -0.00225598    -0.00065383     0.00000000     0.00000000     0.00008270
   MO  13     0.00000000     0.00000001     0.00000000     0.00000000     0.00000000    -0.00018206    -0.00079351     0.00000000
   MO  14     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00022032    -0.00029578     0.00000000
   MO  15    -0.01176389     0.00638445     0.00064766     0.00025282    -0.00112885     0.00000000     0.00000000    -0.00027898
   MO  16     0.00811845    -0.00566492    -0.00535222    -0.00312973    -0.00109875     0.00000000     0.00000000    -0.00005893
   MO  17     0.00000002    -0.00000001     0.00000000     0.00000000     0.00000000    -0.00002801    -0.00027844     0.00000000
   MO  18    -0.00057995     0.00029523    -0.00084998    -0.00054050    -0.00006806     0.00000000     0.00000000     0.00013610
   MO  19     0.00129380    -0.00062350     0.00000806    -0.00010523    -0.00035875     0.00000000     0.00000000    -0.00007627
   MO  20     0.00078900    -0.00044855     0.00029415     0.00030134    -0.00010864     0.00000000     0.00000000     0.00004154
   MO  21     0.00009714    -0.00006454     0.00027142     0.00020035     0.00003294     0.00000000     0.00000000     0.00000496
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00003002    -0.00004095     0.00000000
   MO  23     0.00018372    -0.00008748     0.00001481    -0.00002592     0.00002100     0.00000000     0.00000000     0.00001834
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00019745     0.00036552     0.00000000
   MO  25     0.00008751    -0.00026096     0.00007339     0.00006427     0.00031820     0.00000000     0.00000000     0.00005551
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00001636    -0.00000119     0.00000000
   MO  27     0.00022494    -0.00012146    -0.00061620    -0.00036894    -0.00004586     0.00000000     0.00000000     0.00044475
   MO  28     0.00016773    -0.00025475    -0.00030560    -0.00005060    -0.00012399     0.00000000     0.00000000     0.00015388
   MO  29     0.00039743    -0.00008321     0.00029158     0.00012007     0.00011696     0.00000000     0.00000000    -0.00031896
   MO  30     0.00056904    -0.00020168    -0.00013618    -0.00014592     0.00001142     0.00000000     0.00000000     0.00018819
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00005410     0.00002149     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002345    -0.00000691     0.00000000
   MO  33     0.00135767    -0.00043398     0.00002094     0.00000507    -0.00003516     0.00000000     0.00000000    -0.00006172
   MO  34    -0.00043398     0.00058846    -0.00000130    -0.00005483     0.00005073     0.00000000     0.00000000    -0.00010181
   MO  35     0.00002094    -0.00000130     0.00085778     0.00050596     0.00003464     0.00000000     0.00000000    -0.00003640
   MO  36     0.00000507    -0.00005483     0.00050596     0.00043472    -0.00008417     0.00000000     0.00000000     0.00003805
   MO  37    -0.00003516     0.00005073     0.00003464    -0.00008417     0.00097206     0.00000000     0.00000000    -0.00004094
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00099220    -0.00031860     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00031860     0.00056508     0.00000000
   MO  40    -0.00006172    -0.00010181    -0.00003640     0.00003805    -0.00004094     0.00000000     0.00000000     0.00135420
   MO  41    -0.00001231    -0.00006619    -0.00000371     0.00005400    -0.00001471     0.00000000     0.00000000     0.00019896
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00014160    -0.00029145     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00008884    -0.00004077     0.00000000
   MO  44     0.00010847    -0.00010104     0.00007263     0.00001681     0.00042461     0.00000000     0.00000000     0.00017170
   MO  45     0.00007240     0.00013228    -0.00022271    -0.00021996     0.00016035     0.00000000     0.00000000    -0.00041194
   MO  46    -0.00001991    -0.00001994     0.00003514     0.00006344    -0.00001434     0.00000000     0.00000000     0.00005909

                MO  41         MO  42         MO  43         MO  44         MO  45         MO  46
   MO   8     0.00029392     0.00000000     0.00000000     0.00019980     0.00020786     0.00007689
   MO   9    -0.00011766     0.00000000     0.00000000    -0.00025286    -0.00040879    -0.00004493
   MO  10     0.00034512     0.00000000     0.00000000     0.00006180    -0.00103667    -0.00004035
   MO  11    -0.00046634     0.00000000     0.00000000    -0.00085130     0.00014736    -0.00047265
   MO  12    -0.00117480     0.00000000     0.00000000     0.00022502    -0.00065524     0.00030059
   MO  13     0.00000000    -0.00071963     0.00021453     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000    -0.00000820    -0.00120702     0.00000000     0.00000000     0.00000000
   MO  15     0.00001171     0.00000000     0.00000000    -0.00123793    -0.00025310    -0.00000693
   MO  16    -0.00015234     0.00000000     0.00000000     0.00087676    -0.00047612    -0.00013671
   MO  17     0.00000000    -0.00020325    -0.00185072     0.00000000     0.00000000     0.00000000
   MO  18    -0.00006427     0.00000000     0.00000000    -0.00016953     0.00032634    -0.00009760
   MO  19    -0.00028769     0.00000000     0.00000000    -0.00007953    -0.00018073    -0.00019187
   MO  20     0.00021569     0.00000000     0.00000000     0.00015124    -0.00017975     0.00005819
   MO  21     0.00010217     0.00000000     0.00000000     0.00004197     0.00000217     0.00003564
   MO  22     0.00000000     0.00012769    -0.00013038     0.00000000     0.00000000     0.00000000
   MO  23     0.00007140     0.00000000     0.00000000     0.00006694     0.00004013     0.00004446
   MO  24     0.00000000    -0.00033594    -0.00000280     0.00000000     0.00000000     0.00000000
   MO  25     0.00000814     0.00000000     0.00000000     0.00039814    -0.00003349    -0.00000927
   MO  26     0.00000000     0.00003731    -0.00060011     0.00000000     0.00000000     0.00000000
   MO  27     0.00023495     0.00000000     0.00000000     0.00018305    -0.00034137     0.00004442
   MO  28     0.00023587     0.00000000     0.00000000     0.00002236    -0.00017605     0.00012930
   MO  29    -0.00006911     0.00000000     0.00000000    -0.00004936     0.00020435    -0.00003517
   MO  30    -0.00002084     0.00000000     0.00000000     0.00001507    -0.00012002     0.00000976
   MO  31     0.00000000    -0.00008026    -0.00019066     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00001993    -0.00032653     0.00000000     0.00000000     0.00000000
   MO  33    -0.00001231     0.00000000     0.00000000     0.00010847     0.00007240    -0.00001991
   MO  34    -0.00006619     0.00000000     0.00000000    -0.00010104     0.00013228    -0.00001994
   MO  35    -0.00000371     0.00000000     0.00000000     0.00007263    -0.00022271     0.00003514
   MO  36     0.00005400     0.00000000     0.00000000     0.00001681    -0.00021996     0.00006344
   MO  37    -0.00001471     0.00000000     0.00000000     0.00042461     0.00016035    -0.00001434
   MO  38     0.00000000    -0.00014160     0.00008884     0.00000000     0.00000000     0.00000000
   MO  39     0.00000000    -0.00029145    -0.00004077     0.00000000     0.00000000     0.00000000
   MO  40     0.00019896     0.00000000     0.00000000     0.00017170    -0.00041194     0.00005909
   MO  41     0.00038010     0.00000000     0.00000000     0.00007010    -0.00008573     0.00014213
   MO  42     0.00000000     0.00067353    -0.00004540     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000    -0.00004540     0.00009165     0.00000000     0.00000000     0.00000000
   MO  44     0.00007010     0.00000000     0.00000000     0.00029897    -0.00011356     0.00002310
   MO  45    -0.00008573     0.00000000     0.00000000    -0.00011356     0.00075812    -0.00009736
   MO  46     0.00014213     0.00000000     0.00000000     0.00002310    -0.00009736     0.00011239

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99910886
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99844390     1.99351427     1.99245184     1.99027320     1.99011244     1.97871487     1.93109087     1.01034766
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.00403516     0.06946483     0.00818300     0.00757383     0.00567361     0.00429000     0.00333453     0.00226034
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00170434     0.00154703     0.00134343     0.00117580     0.00092201     0.00084827     0.00076195     0.00067133
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00046836     0.00034152     0.00028155     0.00020941     0.00019193     0.00016838     0.00016600     0.00013163
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO
  occ(*)=     0.00008568     0.00003506     0.00003157     0.00001497     0.00001390     0.00001271


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     1_ s       2.000501   0.000166   0.000087   2.000004   0.000000   0.000131
     1_ p       0.000134   0.000236   0.000149   0.000000   2.000010   1.999807
     1_ d       0.000001   0.000015   0.000016   0.000000   0.000000   0.000001
     3_ s       0.000033   0.000000   1.999749   0.000000   0.000000   0.000002
     3_ p      -0.000093  -0.000001   0.000001   0.000000   0.000002   0.000005
     3_ d      -0.000003   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s      -0.000055   1.999583   0.000000   0.000000   0.000000   0.000019
     4_ p      -0.000500   0.000003  -0.000001  -0.000003  -0.000013   0.000020
     4_ d      -0.000016   0.000000   0.000000   0.000000   0.000001   0.000015
 
   ao class       7a         8a         9a        10a        11a        12a  
     1_ s       0.000003   0.228796   0.050504   1.149781   0.091921   0.000000
     1_ p       2.000012   0.331978   0.120679   0.257705   0.580596   0.913301
     1_ d       0.000003   0.053898   0.002890   0.014007   0.030413   0.025227
     3_ s      -0.000008   0.003824  -0.000787   0.508972   0.256089   0.000000
     3_ p      -0.000018   0.001686   0.002253   0.010773   0.031422   0.256144
     3_ d       0.000000   0.000096   0.000038   0.001377   0.000570   0.001008
     4_ s      -0.000006   1.025636   0.802159   0.022863   0.008240   0.000000
     4_ p      -0.000005   0.348413   1.017102   0.028023   0.989251   0.789660
     4_ d       0.000019   0.004781   0.003605   0.000013   0.003949   0.004934
 
   ao class      13a        14a        15a        16a        17a        18a  
     1_ s       0.275007   0.000000  -0.003231   0.000000   0.002775   0.002023
     1_ p       0.011082   0.021451   0.421148   0.538868   0.006943   0.035256
     1_ d       0.025145   0.037062   0.066881   0.023726   0.008367  -0.000125
     3_ s       1.184965   0.000000  -0.002868   0.000000   0.000023   0.000282
     3_ p       0.094983   1.616305   1.205217   0.039562   0.963865   0.026797
     3_ d       0.000481   0.000403   0.003107   0.000216   0.000364   0.000130
     4_ s       0.011150   0.000000   0.000366   0.000000  -0.000485   0.000015
     4_ p       0.385888   0.302578   0.240725   0.408306   0.022144   0.005056
     4_ d       0.001411   0.000915  -0.000253  -0.000330   0.000038   0.000030
 
   ao class      19a        20a        21a        22a        23a        24a  
     1_ s       0.000000   0.000000   0.000137   0.000014   0.000075   0.000130
     1_ p       0.000885   0.000057   0.001134   0.000730   0.000362   0.000099
     1_ d       0.003367   0.002468   0.001257   0.002196   0.001621   0.000578
     3_ s       0.000000   0.000000   0.000004   0.000129   0.000551   0.001040
     3_ p       0.000068   0.004925   0.002264   0.000288   0.000083   0.000217
     3_ d       0.000005   0.000072   0.000174   0.000333   0.000098   0.000177
     4_ s       0.000000   0.000000  -0.000013   0.000001   0.000003  -0.000003
     4_ p       0.003711   0.000045   0.000687   0.000570   0.000518   0.000009
     4_ d       0.000146   0.000006   0.000030   0.000030   0.000023   0.000014
 
   ao class      25a        26a        27a        28a        29a        30a  
     1_ s       0.000014   0.000000   0.000006   0.000000   0.000034   0.000000
     1_ p      -0.000004  -0.000009   0.000008   0.000006   0.000142   0.000110
     1_ d       0.000071   0.000779   0.000061   0.000002   0.000082   0.000143
     3_ s       0.000178   0.000000   0.000010   0.000000   0.000000   0.000000
     3_ p       0.000056   0.000496   0.000227   0.000000   0.000271   0.000012
     3_ d       0.001369   0.000277   0.001021   0.001167   0.000130   0.000574
     4_ s       0.000005   0.000000   0.000002   0.000000   0.000035   0.000000
     4_ p       0.000013   0.000000   0.000009   0.000001   0.000159   0.000008
     4_ d       0.000002   0.000004   0.000001   0.000000   0.000068   0.000001
 
   ao class      31a        32a        33a        34a        35a        36a  
     1_ s       0.000084   0.000065   0.000000   0.000067   0.000058   0.000010
     1_ p       0.000031   0.000010   0.000357   0.000018   0.000027   0.000055
     1_ d       0.000240   0.000009   0.000025   0.000060   0.000017   0.000051
     3_ s       0.000015  -0.000002   0.000000   0.000006   0.000003   0.000000
     3_ p       0.000000   0.000034   0.000010   0.000070   0.000007   0.000003
     3_ d       0.000044   0.000538   0.000065   0.000017   0.000003   0.000002
     4_ s       0.000098   0.000000   0.000000   0.000004   0.000004   0.000024
     4_ p       0.000077   0.000016   0.000010   0.000098   0.000108   0.000023
     4_ d       0.000173   0.000001   0.000001   0.000000   0.000055   0.000040
 
   ao class      37a        38a        39a        40a        41a        42a  
     1_ s       0.000000   0.000001   0.000000  -0.000002   0.000013   0.000002
     1_ p      -0.000003   0.000039   0.000000   0.000021   0.000023   0.000017
     1_ d       0.000079   0.000008   0.000005   0.000023   0.000005   0.000003
     3_ s       0.000000   0.000001   0.000000   0.000010   0.000004   0.000002
     3_ p       0.000000   0.000022   0.000000   0.000033   0.000016   0.000000
     3_ d       0.000000   0.000004   0.000000   0.000008   0.000004   0.000000
     4_ s       0.000000   0.000008   0.000000   0.000015   0.000005  -0.000001
     4_ p       0.000100   0.000028   0.000007   0.000014   0.000010   0.000009
     4_ d       0.000016   0.000057   0.000154   0.000009   0.000006   0.000002
 
   ao class      43a        44a        45a        46a  
     1_ s       0.000001   0.000000   0.000000   0.000000
     1_ p       0.000004   0.000001   0.000000   0.000001
     1_ d       0.000000   0.000001   0.000001   0.000000
     3_ s       0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000
     4_ s       0.000000   0.000002   0.000000   0.000005
     4_ p       0.000001   0.000001   0.000000   0.000002
     4_ d       0.000026   0.000010   0.000013   0.000003


                        gross atomic populations
     ao            1_         3_         4_
      s         5.799178   3.952228   3.869678
      p         9.243476   4.258004   4.542880
      d         0.300680   0.013871   0.020005
    total      15.343334   8.224103   8.432563
 

 Total number of electrons:   32.00000000

 item #                     3 suffix=:.drt1.state3:
 read_civout: repnuc=  -474.481013326253     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.29813207   -72.81711874
 residuum:     0.00005467
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.24325180   -72.76223847
 residuum:     0.00007067
 deltae:     0.00000000
================================================================================
  Reading record                      3  of civout
 INFO:ref#  3vector#  3 method:  0 last record  1max overlap with ref# 81% root-following 0
 MR-CISD energy:  -547.15777796   -72.67676463
 residuum:     0.00006802
 deltae:     0.00000001
 apxde:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653812     0.00000000    -0.00000015    -0.09725199     0.00004626    -0.00082778    -0.03587067    -0.01862114
 ref:   2     0.00000019     0.97109898     0.05155842     0.00034534     0.03736405     0.02856471    -0.06412178    -0.04628641
 ref:   3     0.00000005     0.02501228    -0.80764855     0.00016017     0.46354812    -0.00245874     0.02404987     0.10446559

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96653812     0.00000000    -0.00000015    -0.09725199     0.00004626     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000019     0.97109898     0.05155842     0.00034534     0.03736405     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000005     0.02501228    -0.80764855     0.00016017     0.46354812     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=    8527  D0X=       0  D0W=       0
  DDZI=    3020 DDYI=    6515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     645 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=  153125  D0X=       0  D0W=       0
  DDZI=   17280 DDYI=   36280 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    18.000000
   18  correlated and    14  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.99846325
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00183372
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00055454
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00033085
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00685738
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000005
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00023759
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01262182
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000006
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00086895
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00414306
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00091160
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00087106
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00129704
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00026490
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00074540
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00226386
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00007750
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00042353
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00027905
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00205819
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00137230
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00219956
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00036553
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00028668
   MO  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00054814
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  44     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00009615
   MO  45     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00025403
   MO  46     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002617

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   8    -0.00183372     0.00055454    -0.00033085     0.00685738     0.00000000     0.00000005    -0.00023759     0.01262182
   MO   9     1.99207525    -0.00034125     0.00029171     0.02032007     0.00000000     0.00000022     0.00122799     0.04146846
   MO  10    -0.00034125     1.99166302     0.00323186    -0.02408933     0.00000005     0.00000015     0.00292637    -0.01962297
   MO  11     0.00029171     0.00323186     1.99678869     0.00227124     0.00000000    -0.00000008     0.00073140    -0.00068822
   MO  12     0.02032007    -0.02408933     0.00227124     1.75401932     0.00000029    -0.00000051     0.01855285    -0.33463892
   MO  13     0.00000000     0.00000005     0.00000000     0.00000029     1.99186205    -0.00898140    -0.00000009    -0.00000001
   MO  14     0.00000022     0.00000015    -0.00000008    -0.00000051    -0.00898140     1.83660942    -0.00000035    -0.00000312
   MO  15     0.00122799     0.00292637     0.00073140     0.01855285    -0.00000009    -0.00000035     1.95968777    -0.00510000
   MO  16     0.04146846    -0.01962297    -0.00068822    -0.33463892    -0.00000001    -0.00000312    -0.00510000     1.21962374
   MO  17     0.00000026     0.00000020    -0.00000005     0.00000013     0.03249754     0.23855033    -0.00000064    -0.00000343
   MO  18    -0.00554059    -0.00233271     0.00297109     0.01387273    -0.00000008    -0.00000042     0.01383363    -0.05102362
   MO  19    -0.00071163     0.00171568    -0.01701741    -0.00938413     0.00000007     0.00000001     0.09766410     0.00700996
   MO  20     0.00339569     0.00137711    -0.00173762    -0.00632965     0.00000001    -0.00000011    -0.00059445     0.02721489
   MO  21     0.00175145     0.00033498    -0.00135346     0.00085818    -0.00000002     0.00000000    -0.00596862     0.01101842
   MO  22     0.00000002     0.00000003     0.00000000     0.00000002    -0.00149155     0.00588989    -0.00000001     0.00000010
   MO  23     0.00052486     0.00274274     0.00798540    -0.00248276     0.00000003    -0.00000001    -0.02121131     0.00772476
   MO  24     0.00000003     0.00000002     0.00000000    -0.00000002     0.00106989    -0.00994664     0.00000000    -0.00000013
   MO  25     0.00148580     0.00216455    -0.00264794    -0.00976989     0.00000018     0.00000010    -0.02418095     0.03256645
   MO  26     0.00000001     0.00000002    -0.00000006    -0.00000010     0.01002801     0.03307990     0.00000004    -0.00000013
   MO  27     0.00108681     0.00248787    -0.00030074    -0.00591057     0.00000000    -0.00000005     0.00433147     0.01942981
   MO  28     0.00195111    -0.00017465    -0.00107027    -0.00310091     0.00000001    -0.00000001     0.00214191     0.00264809
   MO  29     0.00017114    -0.00156406    -0.00007040    -0.00529921    -0.00000008    -0.00000017     0.00973340     0.00338984
   MO  30    -0.00035793     0.00002959    -0.00051836     0.00328825     0.00000002     0.00000008     0.00846536    -0.02125250
   MO  31    -0.00000001     0.00000000    -0.00000001    -0.00000004     0.00499377     0.00979534    -0.00000001    -0.00000008
   MO  32     0.00000001     0.00000000    -0.00000001    -0.00000010     0.00656353     0.02079576    -0.00000001    -0.00000015
   MO  33    -0.00016665    -0.00017066    -0.00218120    -0.00179158     0.00000006     0.00000004     0.00288938     0.01806095
   MO  34    -0.00167861    -0.00236426    -0.00520308     0.00454949    -0.00000002     0.00000001     0.01080312    -0.00981371
   MO  35    -0.00224726    -0.00020565     0.00012659    -0.00019754     0.00000001     0.00000001     0.00114985    -0.00002248
   MO  36    -0.00019399    -0.00153830     0.00004724     0.00002793     0.00000000     0.00000000    -0.00492108     0.00234107
   MO  37    -0.00004652    -0.00010151    -0.00058850    -0.00029325     0.00000000    -0.00000001     0.00380615     0.00030731
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00045199    -0.00025231    -0.00000003     0.00000000
   MO  39     0.00000000     0.00000001     0.00000000    -0.00000001     0.00057269     0.00217207     0.00000001     0.00000000
   MO  40     0.00004919    -0.00005575    -0.00023720    -0.00168446     0.00000000     0.00000003    -0.00122498    -0.00032492
   MO  41     0.00045845    -0.00051922     0.00018753    -0.00030170     0.00000000     0.00000001    -0.00048313     0.00165493
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003162    -0.00229661     0.00000000    -0.00000001
   MO  43     0.00000000     0.00000000     0.00000000     0.00000001    -0.00084552    -0.00268136     0.00000000     0.00000002
   MO  44    -0.00005633     0.00003523     0.00014011    -0.00080029     0.00000001     0.00000000     0.00065244     0.00307783
   MO  45     0.00004192     0.00078357    -0.00029720     0.00118513     0.00000000     0.00000003     0.00074254    -0.00371366
   MO  46    -0.00005149    -0.00061435    -0.00020782     0.00004434     0.00000000    -0.00000001    -0.00067185     0.00048669

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   8     0.00000006     0.00086895     0.00414306    -0.00091160    -0.00087106     0.00000000    -0.00129704     0.00000001
   MO   9     0.00000026    -0.00554059    -0.00071163     0.00339569     0.00175145     0.00000002     0.00052486     0.00000003
   MO  10     0.00000020    -0.00233271     0.00171568     0.00137711     0.00033498     0.00000003     0.00274274     0.00000002
   MO  11    -0.00000005     0.00297109    -0.01701741    -0.00173762    -0.00135346     0.00000000     0.00798540     0.00000000
   MO  12     0.00000013     0.01387273    -0.00938413    -0.00632965     0.00085818     0.00000002    -0.00248276    -0.00000002
   MO  13     0.03249754    -0.00000008     0.00000007     0.00000001    -0.00000002    -0.00149155     0.00000003     0.00106989
   MO  14     0.23855033    -0.00000042     0.00000001    -0.00000011     0.00000000     0.00588989    -0.00000001    -0.00994664
   MO  15    -0.00000064     0.01383363     0.09766410    -0.00059445    -0.00596862    -0.00000001    -0.02121131     0.00000000
   MO  16    -0.00000343    -0.05102362     0.00700996     0.02721489     0.01101842     0.00000010     0.00772476    -0.00000013
   MO  17     1.14705161    -0.00000106    -0.00000011    -0.00000018     0.00000000     0.00149517    -0.00000004     0.00713068
   MO  18    -0.00000106     0.05428859    -0.00059234    -0.00361087    -0.00168662     0.00000005    -0.00115706    -0.00000007
   MO  19    -0.00000011    -0.00059234     0.01617978     0.00092013    -0.00070810     0.00000000    -0.00263080     0.00000001
   MO  20    -0.00000018    -0.00361087     0.00092013     0.00201515     0.00079895     0.00000001     0.00020911    -0.00000001
   MO  21     0.00000000    -0.00168662    -0.00070810     0.00079895     0.00073702     0.00000000     0.00020801     0.00000000
   MO  22     0.00149517     0.00000005     0.00000000     0.00000001     0.00000000     0.00108615     0.00000000    -0.00043098
   MO  23    -0.00000004    -0.00115706    -0.00263080     0.00020911     0.00020801     0.00000000     0.00076383     0.00000000
   MO  24     0.00713068    -0.00000007     0.00000001    -0.00000001     0.00000000    -0.00043098     0.00000000     0.00449349
   MO  25    -0.00000004    -0.00287668    -0.00473025     0.00046782     0.00025918     0.00000001     0.00142354     0.00000001
   MO  26     0.03507752    -0.00000001     0.00000000     0.00000000     0.00000000     0.00129383     0.00000000     0.00057740
   MO  27     0.00000002    -0.00415697     0.00019948     0.00131701     0.00045412     0.00000000     0.00012667    -0.00000001
   MO  28    -0.00000001    -0.00057466     0.00113400     0.00034266     0.00001846     0.00000000    -0.00007940     0.00000000
   MO  29    -0.00000017    -0.00190212     0.00296372     0.00049085     0.00008126     0.00000000    -0.00046528    -0.00000001
   MO  30     0.00000016     0.00292690     0.00408167    -0.00061948    -0.00061316     0.00000000    -0.00080406     0.00000001
   MO  31     0.00977474    -0.00000008     0.00000000    -0.00000001     0.00000000    -0.00028041     0.00000000     0.00261423
   MO  32     0.01557973    -0.00000001     0.00000000     0.00000000     0.00000000     0.00063537     0.00000000    -0.00104331
   MO  33    -0.00000001    -0.00201986     0.00325238     0.00102899     0.00012615     0.00000000    -0.00016965     0.00000000
   MO  34     0.00000005     0.00147684     0.00008899    -0.00062486    -0.00013844     0.00000000    -0.00038258     0.00000000
   MO  35     0.00000000     0.00047462     0.00023205     0.00017608     0.00014958     0.00000000    -0.00006134     0.00000000
   MO  36     0.00000000    -0.00001138    -0.00056164     0.00025018     0.00016449     0.00000000     0.00009941     0.00000000
   MO  37     0.00000000     0.00022160    -0.00048406    -0.00007735     0.00002862     0.00000000     0.00004001     0.00000000
   MO  38     0.00099490     0.00000000     0.00000000     0.00000000     0.00000000     0.00001035     0.00000000     0.00012605
   MO  39    -0.00020374     0.00000000     0.00000000     0.00000000     0.00000000    -0.00004832     0.00000000     0.00031534
   MO  40     0.00000000    -0.00012010    -0.00026384    -0.00008314    -0.00001308     0.00000000     0.00003586     0.00000000
   MO  41     0.00000000    -0.00030121    -0.00008675     0.00018219     0.00009272     0.00000000     0.00000533     0.00000000
   MO  42    -0.00032046     0.00000000     0.00000000     0.00000000     0.00000000     0.00008877     0.00000000    -0.00033558
   MO  43    -0.00241416     0.00000000     0.00000000     0.00000000     0.00000000    -0.00012372     0.00000000    -0.00001199
   MO  44     0.00000000    -0.00006056    -0.00024147     0.00012955     0.00005439     0.00000000     0.00007021     0.00000000
   MO  45     0.00000000    -0.00003439    -0.00018590    -0.00025903    -0.00003104     0.00000000     0.00000531     0.00000000
   MO  46     0.00000000    -0.00000045    -0.00007366     0.00005769     0.00001935     0.00000000     0.00002201     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   8     0.00026490     0.00000001    -0.00074540    -0.00226386     0.00007750     0.00042353     0.00000000     0.00000000
   MO   9     0.00148580     0.00000001     0.00108681     0.00195111     0.00017114    -0.00035793    -0.00000001     0.00000001
   MO  10     0.00216455     0.00000002     0.00248787    -0.00017465    -0.00156406     0.00002959     0.00000000     0.00000000
   MO  11    -0.00264794    -0.00000006    -0.00030074    -0.00107027    -0.00007040    -0.00051836    -0.00000001    -0.00000001
   MO  12    -0.00976989    -0.00000010    -0.00591057    -0.00310091    -0.00529921     0.00328825    -0.00000004    -0.00000010
   MO  13     0.00000018     0.01002801     0.00000000     0.00000001    -0.00000008     0.00000002     0.00499377     0.00656353
   MO  14     0.00000010     0.03307990    -0.00000005    -0.00000001    -0.00000017     0.00000008     0.00979534     0.02079576
   MO  15    -0.02418095     0.00000004     0.00433147     0.00214191     0.00973340     0.00846536    -0.00000001    -0.00000001
   MO  16     0.03256645    -0.00000013     0.01942981     0.00264809     0.00338984    -0.02125250    -0.00000008    -0.00000015
   MO  17    -0.00000004     0.03507752     0.00000002    -0.00000001    -0.00000017     0.00000016     0.00977474     0.01557973
   MO  18    -0.00287668    -0.00000001    -0.00415697    -0.00057466    -0.00190212     0.00292690    -0.00000008    -0.00000001
   MO  19    -0.00473025     0.00000000     0.00019948     0.00113400     0.00296372     0.00408167     0.00000000     0.00000000
   MO  20     0.00046782     0.00000000     0.00131701     0.00034266     0.00049085    -0.00061948    -0.00000001     0.00000000
   MO  21     0.00025918     0.00000000     0.00045412     0.00001846     0.00008126    -0.00061316     0.00000000     0.00000000
   MO  22     0.00000001     0.00129383     0.00000000     0.00000000     0.00000000     0.00000000    -0.00028041     0.00063537
   MO  23     0.00142354     0.00000000     0.00012667    -0.00007940    -0.00046528    -0.00080406     0.00000000     0.00000000
   MO  24     0.00000001     0.00057740    -0.00000001     0.00000000    -0.00000001     0.00000001     0.00261423    -0.00104331
   MO  25     0.00622647     0.00000001     0.00012474    -0.00027741    -0.00183131    -0.00275470     0.00000001     0.00000001
   MO  26     0.00000001     0.00654360     0.00000000     0.00000000    -0.00000001     0.00000001     0.00138861     0.00305768
   MO  27     0.00012474     0.00000000     0.00249047     0.00034862     0.00031121    -0.00073161     0.00000000     0.00000000
   MO  28    -0.00027741     0.00000000     0.00034862     0.00047435     0.00017241     0.00029869     0.00000000     0.00000000
   MO  29    -0.00183131    -0.00000001     0.00031121     0.00017241     0.00133465     0.00096937    -0.00000001    -0.00000001
   MO  30    -0.00275470     0.00000001    -0.00073161     0.00029869     0.00096937     0.00256802     0.00000001     0.00000000
   MO  31     0.00000001     0.00138861     0.00000000     0.00000000    -0.00000001     0.00000001     0.00300887    -0.00042209
   MO  32     0.00000001     0.00305768     0.00000000     0.00000000    -0.00000001     0.00000000    -0.00042209     0.00239413
   MO  33    -0.00043338     0.00000001     0.00045423     0.00037395     0.00088502     0.00083279     0.00000000     0.00000000
   MO  34    -0.00061246     0.00000000    -0.00017148    -0.00024990    -0.00005591     0.00003186     0.00000000     0.00000000
   MO  35    -0.00008579     0.00000000    -0.00041292    -0.00012772     0.00013387     0.00007688     0.00000000     0.00000000
   MO  36     0.00023702     0.00000000    -0.00021996    -0.00000317    -0.00005996    -0.00019396     0.00000000     0.00000000
   MO  37     0.00038748     0.00000000     0.00003876    -0.00009821    -0.00000407    -0.00001271     0.00000000     0.00000000
   MO  38     0.00000000     0.00002723     0.00000000     0.00000000     0.00000000     0.00000000     0.00004178     0.00000883
   MO  39     0.00000000    -0.00007881     0.00000000     0.00000000     0.00000000     0.00000000     0.00001773    -0.00001952
   MO  40     0.00006216     0.00000000    -0.00010941    -0.00008723     0.00003822    -0.00010719     0.00000000     0.00000000
   MO  41    -0.00002192     0.00000000     0.00023061    -0.00000683     0.00003090    -0.00008449     0.00000000     0.00000000
   MO  42     0.00000000     0.00006020     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003912     0.00004009
   MO  43     0.00000000    -0.00051646     0.00000000     0.00000000     0.00000000     0.00000000    -0.00014380    -0.00027434
   MO  44     0.00033193     0.00000000     0.00015033     0.00000468    -0.00001717    -0.00004665     0.00000000     0.00000000
   MO  45    -0.00011725     0.00000000    -0.00035872    -0.00019657     0.00011700    -0.00003145     0.00000000     0.00000000
   MO  46     0.00002444     0.00000000     0.00003714     0.00006939    -0.00003003    -0.00000391     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38         MO  39         MO  40
   MO   8     0.00027905     0.00205819     0.00137230    -0.00219956     0.00036553     0.00000000     0.00000000     0.00028668
   MO   9    -0.00016665    -0.00167861    -0.00224726    -0.00019399    -0.00004652     0.00000000     0.00000000     0.00004919
   MO  10    -0.00017066    -0.00236426    -0.00020565    -0.00153830    -0.00010151     0.00000000     0.00000001    -0.00005575
   MO  11    -0.00218120    -0.00520308     0.00012659     0.00004724    -0.00058850     0.00000000     0.00000000    -0.00023720
   MO  12    -0.00179158     0.00454949    -0.00019754     0.00002793    -0.00029325     0.00000000    -0.00000001    -0.00168446
   MO  13     0.00000006    -0.00000002     0.00000001     0.00000000     0.00000000     0.00045199     0.00057269     0.00000000
   MO  14     0.00000004     0.00000001     0.00000001     0.00000000    -0.00000001    -0.00025231     0.00217207     0.00000003
   MO  15     0.00288938     0.01080312     0.00114985    -0.00492108     0.00380615    -0.00000003     0.00000001    -0.00122498
   MO  16     0.01806095    -0.00981371    -0.00002248     0.00234107     0.00030731     0.00000000     0.00000000    -0.00032492
   MO  17    -0.00000001     0.00000005     0.00000000     0.00000000     0.00000000     0.00099490    -0.00020374     0.00000000
   MO  18    -0.00201986     0.00147684     0.00047462    -0.00001138     0.00022160     0.00000000     0.00000000    -0.00012010
   MO  19     0.00325238     0.00008899     0.00023205    -0.00056164    -0.00048406     0.00000000     0.00000000    -0.00026384
   MO  20     0.00102899    -0.00062486     0.00017608     0.00025018    -0.00007735     0.00000000     0.00000000    -0.00008314
   MO  21     0.00012615    -0.00013844     0.00014958     0.00016449     0.00002862     0.00000000     0.00000000    -0.00001308
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00001035    -0.00004832     0.00000000
   MO  23    -0.00016965    -0.00038258    -0.00006134     0.00009941     0.00004001     0.00000000     0.00000000     0.00003586
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00012605     0.00031534     0.00000000
   MO  25    -0.00043338    -0.00061246    -0.00008579     0.00023702     0.00038748     0.00000000     0.00000000     0.00006216
   MO  26     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00002723    -0.00007881     0.00000000
   MO  27     0.00045423    -0.00017148    -0.00041292    -0.00021996     0.00003876     0.00000000     0.00000000    -0.00010941
   MO  28     0.00037395    -0.00024990    -0.00012772    -0.00000317    -0.00009821     0.00000000     0.00000000    -0.00008723
   MO  29     0.00088502    -0.00005591     0.00013387    -0.00005996    -0.00000407     0.00000000     0.00000000     0.00003822
   MO  30     0.00083279     0.00003186     0.00007688    -0.00019396    -0.00001271     0.00000000     0.00000000    -0.00010719
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00004178     0.00001773     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000883    -0.00001952     0.00000000
   MO  33     0.00163323    -0.00048616    -0.00002009    -0.00006119    -0.00006204     0.00000000     0.00000000     0.00000381
   MO  34    -0.00048616     0.00076242     0.00001489    -0.00013024     0.00006997     0.00000000     0.00000000     0.00001819
   MO  35    -0.00002009     0.00001489     0.00051782     0.00028140     0.00003772     0.00000000     0.00000000    -0.00010348
   MO  36    -0.00006119    -0.00013024     0.00028140     0.00029486    -0.00005547     0.00000000     0.00000000    -0.00006906
   MO  37    -0.00006204     0.00006997     0.00003772    -0.00005547     0.00087296     0.00000000     0.00000000     0.00005392
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00037593     0.00000657     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000657     0.00051348     0.00000000
   MO  40     0.00000381     0.00001819    -0.00010348    -0.00006906     0.00005392     0.00000000     0.00000000     0.00042720
   MO  41     0.00003428    -0.00001897    -0.00000411     0.00001480     0.00001956     0.00000000     0.00000000    -0.00002416
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003833    -0.00026419     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00003119     0.00001151     0.00000000
   MO  44     0.00004883    -0.00007633     0.00005011     0.00001639     0.00040854     0.00000000     0.00000000    -0.00000109
   MO  45     0.00002610     0.00012842    -0.00018577    -0.00017412     0.00005756     0.00000000     0.00000000     0.00029487
   MO  46    -0.00000663    -0.00004236     0.00003330     0.00004427     0.00000931     0.00000000     0.00000000    -0.00004537

                MO  41         MO  42         MO  43         MO  44         MO  45         MO  46
   MO   8    -0.00054814     0.00000000     0.00000000     0.00009615    -0.00025403     0.00002617
   MO   9     0.00045845     0.00000000     0.00000000    -0.00005633     0.00004192    -0.00005149
   MO  10    -0.00051922     0.00000000     0.00000000     0.00003523     0.00078357    -0.00061435
   MO  11     0.00018753     0.00000000     0.00000000     0.00014011    -0.00029720    -0.00020782
   MO  12    -0.00030170     0.00000000     0.00000001    -0.00080029     0.00118513     0.00004434
   MO  13     0.00000000    -0.00003162    -0.00084552     0.00000001     0.00000000     0.00000000
   MO  14     0.00000001    -0.00229661    -0.00268136     0.00000000     0.00000003    -0.00000001
   MO  15    -0.00048313     0.00000000     0.00000000     0.00065244     0.00074254    -0.00067185
   MO  16     0.00165493    -0.00000001     0.00000002     0.00307783    -0.00371366     0.00048669
   MO  17     0.00000000    -0.00032046    -0.00241416     0.00000000     0.00000000     0.00000000
   MO  18    -0.00030121     0.00000000     0.00000000    -0.00006056    -0.00003439    -0.00000045
   MO  19    -0.00008675     0.00000000     0.00000000    -0.00024147    -0.00018590    -0.00007366
   MO  20     0.00018219     0.00000000     0.00000000     0.00012955    -0.00025903     0.00005769
   MO  21     0.00009272     0.00000000     0.00000000     0.00005439    -0.00003104     0.00001935
   MO  22     0.00000000     0.00008877    -0.00012372     0.00000000     0.00000000     0.00000000
   MO  23     0.00000533     0.00000000     0.00000000     0.00007021     0.00000531     0.00002201
   MO  24     0.00000000    -0.00033558    -0.00001199     0.00000000     0.00000000     0.00000000
   MO  25    -0.00002192     0.00000000     0.00000000     0.00033193    -0.00011725     0.00002444
   MO  26     0.00000000     0.00006020    -0.00051646     0.00000000     0.00000000     0.00000000
   MO  27     0.00023061     0.00000000     0.00000000     0.00015033    -0.00035872     0.00003714
   MO  28    -0.00000683     0.00000000     0.00000000     0.00000468    -0.00019657     0.00006939
   MO  29     0.00003090     0.00000000     0.00000000    -0.00001717     0.00011700    -0.00003003
   MO  30    -0.00008449     0.00000000     0.00000000    -0.00004665    -0.00003145    -0.00000391
   MO  31     0.00000000    -0.00003912    -0.00014380     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00004009    -0.00027434     0.00000000     0.00000000     0.00000000
   MO  33     0.00003428     0.00000000     0.00000000     0.00004883     0.00002610    -0.00000663
   MO  34    -0.00001897     0.00000000     0.00000000    -0.00007633     0.00012842    -0.00004236
   MO  35    -0.00000411     0.00000000     0.00000000     0.00005011    -0.00018577     0.00003330
   MO  36     0.00001480     0.00000000     0.00000000     0.00001639    -0.00017412     0.00004427
   MO  37     0.00001956     0.00000000     0.00000000     0.00040854     0.00005756     0.00000931
   MO  38     0.00000000    -0.00003833     0.00003119     0.00000000     0.00000000     0.00000000
   MO  39     0.00000000    -0.00026419     0.00001151     0.00000000     0.00000000     0.00000000
   MO  40    -0.00002416     0.00000000     0.00000000    -0.00000109     0.00029487    -0.00004537
   MO  41     0.00041845     0.00000000     0.00000000     0.00002948    -0.00000446    -0.00001739
   MO  42     0.00000000     0.00069686    -0.00001992     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000    -0.00001992     0.00008193     0.00000000     0.00000000     0.00000000
   MO  44     0.00002948     0.00000000     0.00000000     0.00026473    -0.00008875     0.00001932
   MO  45    -0.00000446     0.00000000     0.00000000    -0.00008875     0.00064170    -0.00010135
   MO  46    -0.00001739     0.00000000     0.00000000     0.00001932    -0.00010135     0.00004973

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99913167
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99896487     1.99508821     1.99322180     1.99188078     1.97172405     1.91244427     1.90779596     1.07203954
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.05880986     0.05257539     0.01630273     0.00691217     0.00655317     0.00369090     0.00272078     0.00151016
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00126508     0.00115725     0.00105341     0.00088173     0.00083354     0.00059068     0.00040382     0.00037961
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00036361     0.00032213     0.00028592     0.00023476     0.00023312     0.00017969     0.00016271     0.00008978
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO
  occ(*)=     0.00006876     0.00003453     0.00003028     0.00002860     0.00002266     0.00001203


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     1_ s       2.000501   0.000166   0.000087   2.000004   0.000000   0.000131
     1_ p       0.000134   0.000236   0.000149   0.000000   2.000010   1.999807
     1_ d       0.000001   0.000015   0.000016   0.000000   0.000000   0.000001
     3_ s       0.000033   0.000000   1.999749   0.000000   0.000000   0.000002
     3_ p      -0.000093  -0.000001   0.000001   0.000000   0.000002   0.000005
     3_ d      -0.000003   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s      -0.000055   1.999583   0.000000   0.000000   0.000000   0.000019
     4_ p      -0.000500   0.000003  -0.000001  -0.000003  -0.000013   0.000020
     4_ d      -0.000016   0.000000   0.000000   0.000000   0.000001   0.000015
 
   ao class       7a         8a         9a        10a        11a        12a  
     1_ s       0.000003   0.326038   0.007554   0.770618   0.000000   0.587590
     1_ p       2.000012   0.468359  -0.025189   0.224952   0.778329   0.368918
     1_ d       0.000003   0.047440   0.002910   0.025111   0.034526   0.006172
     3_ s      -0.000008   0.009331   0.000315   0.873070   0.000000   0.976151
     3_ p      -0.000018   0.012006   0.000727   0.045541   0.027707  -0.003901
     3_ d       0.000000   0.000453   0.000030   0.001148   0.000491   0.000066
     4_ s      -0.000006   0.137896   1.710139   0.012991   0.000000   0.010124
     4_ p      -0.000005   0.989050   0.302150   0.041534   1.146750   0.046157
     4_ d       0.000019   0.008557   0.000328   0.000124   0.005418   0.000604
 
   ao class      13a        14a        15a        16a        17a        18a  
     1_ s       0.041875   0.000000   0.027503   0.000000   0.020948   0.001687
     1_ p       0.056940   0.398152   0.564846   0.432505   0.076969   0.025692
     1_ d       0.047537   0.036741   0.009313   0.010397   0.037936   0.000676
     3_ s      -0.000588   0.000000   0.082030   0.000000   0.005121  -0.000150
     3_ p       1.761770   1.046303   0.637001   0.454488   0.352778   0.020668
     3_ d       0.001112   0.001353   0.001994  -0.000136   0.000409   0.000167
     4_ s       0.000005   0.000000   0.000554   0.000000  -0.000082   0.000007
     4_ p       0.062656   0.430214   0.581277   0.174781   0.564609   0.003794
     4_ d       0.000417  -0.000318   0.003278   0.000005   0.000122   0.000033
 
   ao class      19a        20a        21a        22a        23a        24a  
     1_ s       0.000716   0.000000   0.000000   0.000018   0.000001   0.000243
     1_ p       0.005892   0.000029   0.000978   0.000392   0.000280   0.000184
     1_ d       0.002222   0.003331   0.001928   0.002628   0.001721   0.000040
     3_ s       0.000005   0.000000   0.000000   0.000009   0.000095   0.000878
     3_ p       0.005933   0.002188   0.002266   0.000049   0.000154   0.000067
     3_ d       0.000135   0.000036   0.000032   0.000022   0.000257   0.000063
     4_ s      -0.000176   0.000000   0.000000  -0.000012   0.000001   0.000001
     4_ p       0.001572   0.001253   0.001306   0.000571   0.000203   0.000031
     4_ d       0.000004   0.000074   0.000043   0.000014   0.000010   0.000001
 
   ao class      25a        26a        27a        28a        29a        30a  
     1_ s       0.000004   0.000000   0.000021   0.000031   0.000000   0.000000
     1_ p       0.000015   0.000001   0.000022   0.000137   0.000537   0.000105
     1_ d       0.000030   0.000292   0.000034   0.000049   0.000107   0.000161
     3_ s       0.000002   0.000000   0.000001   0.000060   0.000000   0.000000
     3_ p       0.000236   0.000233   0.000127   0.000227  -0.000003   0.000089
     3_ d       0.000971   0.000616   0.000804   0.000114   0.000054   0.000233
     4_ s       0.000000   0.000000   0.000001   0.000050   0.000000   0.000000
     4_ p       0.000007   0.000014   0.000042   0.000199   0.000138  -0.000002
     4_ d       0.000001   0.000002   0.000001   0.000015   0.000001   0.000004
 
   ao class      31a        32a        33a        34a        35a        36a  
     1_ s       0.000010   0.000000   0.000078   0.000000   0.000056   0.000000
     1_ p       0.000006   0.000000   0.000008   0.000002   0.000007   0.000002
     1_ d       0.000023   0.000001   0.000103   0.000019   0.000040   0.000107
     3_ s       0.000002   0.000000   0.000028   0.000000   0.000010   0.000000
     3_ p       0.000013   0.000000   0.000033   0.000001   0.000018   0.000001
     3_ d       0.000006   0.000373   0.000028   0.000005   0.000116   0.000000
     4_ s       0.000005   0.000000   0.000021   0.000000  -0.000001   0.000000
     4_ p       0.000009   0.000002   0.000009   0.000023   0.000038   0.000096
     4_ d       0.000331   0.000004   0.000056   0.000273   0.000001   0.000029
 
   ao class      37a        38a        39a        40a        41a        42a  
     1_ s       0.000000   0.000035   0.000001   0.000009   0.000004   0.000001
     1_ p       0.000033   0.000030   0.000023   0.000037   0.000018   0.000012
     1_ d       0.000029   0.000014   0.000026   0.000011   0.000002   0.000004
     3_ s       0.000000   0.000001   0.000001   0.000000   0.000001   0.000006
     3_ p       0.000052   0.000008   0.000009   0.000006   0.000016   0.000004
     3_ d       0.000075   0.000022   0.000016   0.000005   0.000004   0.000001
     4_ s       0.000001   0.000028   0.000011   0.000002   0.000002   0.000000
     4_ p       0.000037   0.000038   0.000071   0.000016   0.000012   0.000006
     4_ d       0.000006   0.000004   0.000004   0.000004   0.000009   0.000001
 
   ao class      43a        44a        45a        46a  
     1_ s       0.000000   0.000000   0.000001  -0.000001
     1_ p       0.000000   0.000000   0.000003   0.000000
     1_ d       0.000001   0.000000   0.000000   0.000001
     3_ s       0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000001   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000
     4_ s       0.000000   0.000000   0.000002   0.000007
     4_ p       0.000002   0.000000   0.000000   0.000002
     4_ d       0.000027   0.000028   0.000016   0.000002


                        gross atomic populations
     ao            1_         3_         4_
      s         5.785933   3.946156   3.871118
      p         9.379579   4.366710   4.348166
      d         0.271718   0.011071   0.019549
    total      15.437230   8.323937   8.238833
 

 Total number of electrons:   32.00000000

 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   2logvrecs(*)=   1   2
 item #                     4 suffix=:.trd1to2:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   2)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=    8527  D0X=       0  D0W=       0
  DDZI=    3020 DDYI=    6515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     645 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   2)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=  153125  D0X=       0  D0W=       0
  DDZI=   17280 DDYI=   36280 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  4.139973353023407E-007
 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   3logvrecs(*)=   1   3
 item #                     5 suffix=:.trd1to3:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=    8527  D0X=       0  D0W=       0
  DDZI=    3020 DDYI=    6515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     645 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=  153125  D0X=       0  D0W=       0
  DDZI=   17280 DDYI=   36280 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  2.946375598500417E-006
 accstate=                     4
 accpdens=                     4
logrecs(*)=   2   3logvrecs(*)=   2   3
 item #                     6 suffix=:.trd2to3:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   2,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=    8527  D0X=       0  D0W=       0
  DDZI=    3020 DDYI=    6515 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     645 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   2,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    2829  DYX=       0  DYW=       0
   D0Z=    3338  D0Y=  153125  D0X=       0  D0W=       0
  DDZI=   17280 DDYI=   36280 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  2.736056866812347E-002
 DA ...
