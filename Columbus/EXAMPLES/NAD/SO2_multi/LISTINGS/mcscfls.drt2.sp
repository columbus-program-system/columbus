

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       301465600 of real*8 words ( 2300.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   NITER=1,NMITER=0
   NMITER=0
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,30,13,18
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,14,20,1,15,20,1,16,20,1,17,20,1,18,20
  
  
  
  
  
  
  
  
  
 NAVST(1) = 3
 WAVST(1,1)=1
 WAVST(1,2)=1
 WAVST(1,3)=1
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /fhgfs/global/lv70151/plasser1/1684021/WORK/WORK.2/aoints
    

 Integral file header information:
 Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12

 Core type energy values:
 energy( 1)=  9.781323324553E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   97.813233246


   ******  Basis set information:  ******

 Number of irreps:                  1
 Total number of basis functions:  46

 irrep no.              1
 irrep label           A  
 no. of bas.fcions.    46


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=     1

 maximum number of psci micro-iterations:   nmiter=    0
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          3             0.333 0.333 0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        4

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1      14( 14)    20
       1      15( 15)    20
       1      16( 16)    20
       1      17( 17)    20
       1      18( 18)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 18:  skip the 2-e integral transformation on the first iteration.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        45
 

 faar:   0 active-active rotations allowed out of:  10 possible.


 Number of active-double rotations:        65
 Number of active-active rotations:         0
 Number of double-virtual rotations:      364
 Number of active-virtual rotations:      140
 lenbfsdef=                131071  lenbfs=                   784
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         120
 class  2 (pq|ri):         #         975
 class  3 (pq|ia):         #        5460
 class  4 (pi|qa):         #        9100
 class  5 (pq|ra):         #        2100
 class  6 (pq|ij)/(pi|qj): #        3900
 class  7 (pq|ab):         #        6090
 class  8 (pa|qb):         #       11760
 class  9 p(bp,ai)         #       50960
 class 10p(ai,jp):        #       23660
 class 11p(ai,bj):        #       71344

 Size of orbital-Hessian matrix B:                   169359
 Size of the orbital-state Hessian matrix C:          76815
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         246174


 Source of the initial MO coeficients:

 Input MO coefficient file: /fhgfs/global/lv70151/plasser1/1684021/WORK/WORK.2/mocoef   
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 mosort: allocated sort2 space, avc2is=   301318097 available sort2 space, avcisx=   301318349

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    16
 ciiter=  27 noldhv=  7 noldv=  7

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -547.1456443344     -644.9588775799        0.0000004547        0.0000010000
    2      -547.1387850442     -644.9520182898        0.0000002827        0.0000010000
    3      -547.0431326739     -644.8563659195        0.0000007391        0.0000010000
    4      -546.9307074281     -644.7439406736        0.0027640970        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.717657815282311E-003
 *** psci convergence not reached ***
 Total number of micro iterations:    0

 ***  micro: final psci convergence values:  ***
    imxov=  0 z0= 0.00000000 pnorm= 0.0000E+00 rznorm= 0.0000E+00 rpnorm= 0.0000E+00 noldr=  0 nnewr=  1 nolds=  0 nnews=  0
 *** warning *** small z0.
 

 fdd(*) eigenvalues. symmetry block  1
  -184.243452  -41.326404  -41.234140  -18.231000  -13.592826  -13.588482  -13.584808   -2.859222   -2.622655   -1.768002
    -1.240753   -1.210549   -1.194732

 qvv(*) eigenvalues. symmetry block  1
     0.414511    1.098913    1.215334    1.236942    1.365527    1.487561    1.629077    1.632807    1.827248    2.027573
     2.297493    2.423713    2.463814    2.545796    2.665581    2.895691    3.701469    4.072596    5.780692    5.780996
     5.789382    5.802231    5.844260    5.916680    6.162657    6.289488    6.473473    7.227649

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -547.1091873508 demc= 5.4711E+02 wnorm= 3.7741E-02 knorm= 0.0000E+00 apxde=-2.3588E-03    *not conv.*     

 final mcscf convergence values:
 iter=    1 emc=   -547.1091873508 demc= 5.4711E+02 wnorm= 3.7741E-02 knorm= 0.0000E+00 apxde=-2.3588E-03    *not conv.*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=     -547.145644334, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.333 total energy=     -547.138785044, rel. (eV)=   0.186651
   DRT #1 state # 3 wt 0.333 total energy=     -547.043132674, rel. (eV)=   2.789485
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A  
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.66426243     1.64500730     1.62528144
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     1.01661823     0.04883061     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        679 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     3
 Number of unique bra states (ndbra):                     3
 qind: F
 (Transition) density matrices:
 d1(*) written to the 1-particle density matrix file.
        679 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           
 d1(*) written to the 1-particle density matrix file.
        679 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st02                                           
 d1(*) written to the 1-particle density matrix file.
        679 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st03                                           

          state spec. NOs: DRT 1, State  1

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99007490     1.94694632     1.00939049
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     1.00003703     0.05355125     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          state spec. NOs: DRT 1, State  2
 *** warning *** large active-orbital occupation. i=  5 nocc= 1.9994E+00

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99937919     1.94879929     1.00166867
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.99833146     0.05182139     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          state spec. NOs: DRT 1, State  3

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99340688     1.96403985     1.02137121
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.98234113     0.03884093     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000486   0.000001   0.000147
     1_ p       0.000000   0.000149   0.000236   0.000147   2.000016   1.999785
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000032  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000017   0.000006
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000056  -0.000007   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000499  -0.000005   0.000023
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000018   0.000017
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.406571   0.017581   1.177390   0.178039   0.004421
     1_ p       2.000010   0.102885   0.104506   0.094870   0.684364   0.365146
     1_ d       0.000000   0.028171   0.029303   0.001579   0.021923   0.052488
     3_ s       0.000000   0.183335   1.601917   0.116930   0.004452   0.037073
     3_ p       0.000002   0.009476   0.005717   0.038120   0.015270   0.080817
     3_ d       0.000000   0.000376   0.000396   0.000785   0.000438   0.000745
     4_ s       0.000000   1.179263   0.232771   0.337576   0.114995   0.007898
     4_ p      -0.000013   0.087132   0.007626   0.230845   0.976247   1.446545
     4_ d       0.000001   0.002790   0.000182   0.001905   0.004272   0.004868
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.004910   0.000000   0.012365   0.000000   0.001478
     1_ p       0.863332   0.013705   0.063695   0.306651   0.541476   0.024674
     1_ d       0.034428   0.018061   0.028339   0.054929   0.019945  -0.000081
     3_ s       0.000000   0.000017   0.000000   0.013973   0.000000   0.000032
     3_ p       0.021729   1.606374   1.402549   1.004863   0.126131   0.018957
     3_ d       0.000509   0.000575   0.000922   0.002194   0.000091   0.000153
     4_ s       0.000000  -0.000808   0.000000   0.000002   0.000000   0.000013
     4_ p       1.074297   0.021405   0.149495   0.230545   0.329311   0.003585
     4_ d       0.005705   0.000024   0.000007  -0.000240  -0.000336   0.000018
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.803646   3.957505   3.871248
      p         9.165645   4.329900   4.556539
      d         0.289120   0.007182   0.019214
    total      15.258412   8.294587   8.447001
 

 Total number of electrons:   32.00000000

 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000501   0.000003   0.000131
     1_ p       0.000000   0.000149   0.000236   0.000134   2.000012   1.999807
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000033  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000018   0.000005
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000055  -0.000006   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000500  -0.000005   0.000020
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000019   0.000015
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.404274   0.025390   1.170965   0.176470   0.006902
     1_ p       2.000010   0.106793   0.101726   0.095539   0.685057   0.362652
     1_ d       0.000000   0.029401   0.028118   0.001527   0.022187   0.052231
     3_ s       0.000000   0.151955   1.625195   0.122899   0.004016   0.039643
     3_ p       0.000002   0.009074   0.006456   0.037251   0.014609   0.082011
     3_ d       0.000000   0.000355   0.000428   0.000768   0.000436   0.000752
     4_ s       0.000000   1.206689   0.205503   0.337320   0.113662   0.009328
     4_ p      -0.000013   0.088624   0.007038   0.231820   0.979264   1.441654
     4_ d       0.000001   0.002834   0.000146   0.001912   0.004298   0.004826
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.000000   0.015324   0.000000   0.003191   0.001594
     1_ p       0.863332   0.026796   0.342329   0.563119   0.008385   0.027739
     1_ d       0.034428   0.030654   0.065006   0.021644   0.010909  -0.000070
     3_ s       0.000000   0.000000   0.015622   0.000000   0.000021   0.000065
     3_ p       0.021729   1.815207   1.236469   0.065155   0.965555   0.019874
     3_ d       0.000509   0.000933   0.002463   0.000183   0.000345   0.000173
     4_ s       0.000000   0.000000   0.000014   0.000000  -0.000492   0.000014
     4_ p       1.074297   0.116401   0.269996   0.359662   0.012108   0.004142
     4_ d       0.005705   0.000083  -0.000277  -0.000372   0.000015   0.000020
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.805001   3.959192   3.871577
      p         9.183814   4.273285   4.584506
      d         0.296072   0.007343   0.019210
    total      15.284887   8.239820   8.475293
 

 Total number of electrons:   32.00000000

 Mulliken population for:
DRT 1, state 02


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000501   0.000003   0.000131
     1_ p       0.000000   0.000149   0.000236   0.000134   2.000012   1.999807
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000033  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000018   0.000005
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000055  -0.000006   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000500  -0.000005   0.000020
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000019   0.000015
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.404274   0.025390   1.170965   0.176470   0.006902
     1_ p       2.000010   0.106793   0.101726   0.095539   0.685057   0.362652
     1_ d       0.000000   0.029401   0.028118   0.001527   0.022187   0.052231
     3_ s       0.000000   0.151955   1.625195   0.122899   0.004016   0.039643
     3_ p       0.000002   0.009074   0.006456   0.037251   0.014609   0.082011
     3_ d       0.000000   0.000355   0.000428   0.000768   0.000436   0.000752
     4_ s       0.000000   1.206689   0.205503   0.337320   0.113662   0.009328
     4_ p      -0.000013   0.088624   0.007038   0.231820   0.979264   1.441654
     4_ d       0.000001   0.002834   0.000146   0.001912   0.004298   0.004826
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.005689   0.015042   0.000000   0.000000   0.001569
     1_ p       0.863332   0.015986   0.368472   0.203882   0.367188   0.026177
     1_ d       0.034428   0.021579   0.065973   0.023727   0.013137  -0.000085
     3_ s       0.000000   0.000007   0.016785   0.000000   0.000000   0.000033
     3_ p       0.021729   1.929255   1.204894   0.536278   0.440558   0.020133
     3_ d       0.000509   0.000694   0.002631   0.000790  -0.000138   0.000163
     4_ s       0.000000  -0.000961  -0.000007   0.000000   0.000000   0.000013
     4_ p       1.074297   0.027104   0.275295   0.237167   0.177737   0.003799
     4_ d       0.005705   0.000026  -0.000286  -0.000175  -0.000151   0.000019
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.807193   3.960308   3.871087
      p         9.197151   4.302144   4.543299
      d         0.292258   0.007386   0.019174
    total      15.296602   8.269838   8.433561
 

 Total number of electrons:   32.00000000

 Mulliken population for:
DRT 1, state 03


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000501   0.000003   0.000131
     1_ p       0.000000   0.000149   0.000236   0.000134   2.000012   1.999807
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000033  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000018   0.000005
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000055  -0.000006   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000500  -0.000005   0.000020
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000019   0.000015
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.404274   0.025390   1.170965   0.176470   0.006902
     1_ p       2.000010   0.106793   0.101726   0.095539   0.685057   0.362652
     1_ d       0.000000   0.029401   0.028118   0.001527   0.022187   0.052231
     3_ s       0.000000   0.151955   1.625195   0.122899   0.004016   0.039643
     3_ p       0.000002   0.009074   0.006456   0.037251   0.014609   0.082011
     3_ d       0.000000   0.000355   0.000428   0.000768   0.000436   0.000752
     4_ s       0.000000   1.206689   0.205503   0.337320   0.113662   0.009328
     4_ p      -0.000013   0.088624   0.007038   0.231820   0.979264   1.441654
     4_ d       0.000001   0.002834   0.000146   0.001912   0.004298   0.004826
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.006150   0.000000   0.000000   0.006491   0.001209
     1_ p       0.863332   0.017523   0.147869   0.506658   0.209840   0.018636
     1_ d       0.034428   0.021855   0.037620   0.018070   0.033644  -0.000086
     3_ s       0.000000   0.000053   0.000000   0.000000   0.009496  -0.000017
     3_ p       0.021729   1.924504   1.523636   0.205206   0.573490   0.016410
     3_ d       0.000509   0.000686   0.001267   0.000005   0.001499   0.000115
     4_ s       0.000000  -0.000983   0.000000   0.000000   0.000011   0.000010
     4_ p       1.074297   0.023586   0.253729   0.291723   0.148028   0.002548
     4_ d       0.005705   0.000032  -0.000081  -0.000291  -0.000158   0.000015
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.798744   3.953015   3.871080
      p         9.115972   4.414272   4.541812
      d         0.279031   0.006818   0.019258
    total      15.193746   8.374104   8.432150
 

 Total number of electrons:   32.00000000

 !timer: mcscf                           cpu_time=     0.074 walltime=     0.301
 *** cpu_time / walltime =      0.246
 bummer (warning):timer: cpu_time << walltime.  If possible, increase core memory. event =                      1
