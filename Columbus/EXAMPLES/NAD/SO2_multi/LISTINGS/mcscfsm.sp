 total ao core energy =   97.813233246
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          3             0.167 0.167 0.167
  2   ground state          3             0.167 0.167 0.167

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    32
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        50

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    32
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        45

 Number of active-double rotations:        65
 Number of active-active rotations:         0
 Number of double-virtual rotations:      364
 Number of active-virtual rotations:      140
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -547.1119819469  5.471E+02  4.826E-07  1.000E-06  8.213E-14  F   *not conv.*     

 final mcscf convergence values:
    2   -547.1119819469 -6.821E-13  3.850E-07  1.440E-06  1.347E-13  F   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.167 total energy=     -547.190444667, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.167 total energy=     -547.140346566, rel. (eV)=   1.363239
   DRT #1 state # 3 wt 0.167 total energy=     -547.013538396, rel. (eV)=   4.813867
   DRT #2 state # 1 wt 0.167 total energy=     -547.145644334, rel. (eV)=   1.219080
   DRT #2 state # 2 wt 0.167 total energy=     -547.138785044, rel. (eV)=   1.405730
   DRT #2 state # 3 wt 0.167 total energy=     -547.043132674, rel. (eV)=   4.008565
   ------------------------------------------------------------


