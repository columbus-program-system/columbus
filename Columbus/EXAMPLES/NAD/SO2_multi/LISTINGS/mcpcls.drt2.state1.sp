

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        50

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        45

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -547.1119819469    nuclear repulsion=    97.8132332455
 demc=             0.0000000000    wnorm=                 0.0000003850
 knorm=            0.0000014402    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.1667 0.1667 0.1667
  2    a      3   0.1667 0.1667 0.1667

 Input the DRT No of interest: [  1]:
In the DRT No.: 2 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry sym  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      4  0.9705566004  0.9419801146  31310
     28  0.1526154072  0.0232914625  11330
     12  0.1343167696  0.0180409946  31013
      6 -0.0682073341  0.0046522404  31211
     34  0.0668284175  0.0044660374  11132
     36 -0.0545379418  0.0029743871  11033
     32  0.0476926561  0.0022745894  11231
      9  0.0342019474  0.0011697732  31112
      1 -0.0220242715  0.0004850685  33110
     44 -0.0161117116  0.0002595873  01313
     26 -0.0148298414  0.0002199242  12131
     19 -0.0099095615  0.0000981994  13130
      8  0.0080217655  0.0000643487  31121
     37  0.0027369191  0.0000074907  10331
     31 -0.0024729257  0.0000061154  11303
      3  0.0023121030  0.0000053458  33011
     13  0.0016076064  0.0000025844  30311
     40  0.0010397613  0.0000010811  03311

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: