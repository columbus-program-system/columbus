1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       345       25452           0           0       25797
      internal walks       345         963           0           0        1308
valid internal walks       345         909           0           0        1254
  MR-CIS calculation - skip 3- and 4-external 2e integrals
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1561 ci%nnlev=                   780  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             301427767             301404821
 lencor,maxblo             301465600                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================
 Orig.  diagonal integrals:  1electron:        39
                             0ext.    :       132
                             2ext.    :       616
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :    133056
                             2ext.    :     80388
                             1ext.    :     24024
                             0ext.    :      2805
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :         0 x :         0
                             4ext.  w :         0 x :         0


Cycle #  1 sortfile size=    589806(      18 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 301427767
Cycle #  2 sortfile size=    589806(      18 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 301427767
 minimum size of srtscr:    524272 WP (    16 records)
 maximum size of srtscr:    589806 WP (    18 records)
 compressed index vector length=                   113
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 18
  NROOT = 3
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 3
  RTOLBK = 1e-4,1e-4,1e-4,
  NITER = 90
  NVCIMN = 5
  RTOLCI = 1e-4,1e-4,1e-4,
  NVCIMX = 8
  NVRFMX = 8
  NVBKMX = 8
   iden=2
  CSFPRN = 10,
 /&end
 transition
   1  1  1  2
   1  1  1  3
   1  2  1  3
 ------------------------------------------------------------------------
transition densities: resetting nroot to    3
lodens (list->root)=  1  2  3
invlodens (root->list)=  1  2  3
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    3      noldv  =   0      noldhv =   0
 nunitv =    3      nbkitr =    1      niter  =  90      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    8      ibktv  =  -1      ibkthv =  -1
 nvcimx =    8      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    3      nvcimn =    5      maxseg =   4      nrfitr =  30
 ncorel =   18      nvrfmx =    8      nvrfmn =   5      iden   =   2
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 Computing density:                    .drt1.state2
 Computing density:                    .drt1.state3
 Computing transition density:          drt1.state1-> drt1.state2 (.trd1to2)
 Computing transition density:          drt1.state1-> drt1.state3 (.trd1to3)
 Computing transition density:          drt1.state2-> drt1.state3 (.trd2to3)
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  3830                108772
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          301465599 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 301465599

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =   97.813233246                                          
 MCSCF energy =    -547.111981947                                                
 SIFS file created by program tran.      r24n12            15:26:09.272 10-Sep-12

 input energy(*) values:
 energy( 1)=  9.781323324553E+01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   9.781323324553E+01

 nsym = 1 nmot=  39

 symmetry  =    1
 slabel(*) =  A  
 nmpsy(*)  =   39

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=     766
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  46 nfctd =   7 nfvtc =   0 nmot  =  39
 nlevel =  39 niot  =  11 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  -1  -1  -1  29  30  31  32  33  34  35  36  37  38  39   1   2
    3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22
   23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34  35  36  37  38  39   1   2   3   4   5   6   7   8   9
   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  2.  2.  2.  2.  2.  0.  0.  0.  0.  0.
  ... transition density matrix calculation requested 
  ... setting rmu(*) to zero 

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -5.722942465718E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1560
 number with all external indices:       812
 number with half external - half internal indices:       616
 number with all internal indices:       132

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=     133056 strt=      93178
    2-external integrals: num=      80388 strt=     226234
    1-external integrals: num=      24024 strt=     306622
    0-external integrals: num=       2805 strt=     330646

 total number of off-diagonal integrals:      333450


 indxof(2nd)  ittp=   3 numx(ittp)=       80388
 indxof(2nd)  ittp=   4 numx(ittp)=       24024
 indxof(2nd)  ittp=   5 numx(ittp)=        2805

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 301290195
 pro2e        1     781    1561    2341    3121    3187    3253    4033   47721   91409
   124176  132368  137828  159667

 pro2e:    302346 integrals read in    56 records.
 pro1e        1     781    1561    2341    3121    3187    3253    4033   47721   91409
   124176  132368  137828  159667
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                    18  records of                  32767 
 WP =               4718448 Bytes
 putdg        1     781    1561    2341    3107   35874   57719    4033   47721   91409
   124176  132368  137828  159667

 putf:       5 buffers of length     766 written to file 12
 diagonal integral file completed.
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     80388         3    226234    226234     80388    333450
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     24024         4    306622    306622     24024    333450
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      2805         5    330646    330646      2805    333450

 putf:     142 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:     812 2ext:     616 0ext:     132
 fil4w,fil4x  :   93177 fil3w,fil3x :  133056
 ofdgint  2ext:   80388 1ext:   24024 0ext:    2805so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 301465599

 core energy values from the integral file:
 energy( 1)=  9.781323324553E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -5.722942465718E+02, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.744810133263E+02
 nmot  =    46 niot  =    11 nfct  =     7 nfvt  =     0
 nrow  =   104 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       1308      345      963        0        0
 nvalwt,nvalw:     1254      345      909        0        0
 ncsft:           25797
 total number of valid internal walks:    1254
 nvalz,nvaly,nvalx,nvalw =      345     909       0       0

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   1532
 nd4ext,nd2ext,nd0ext   812   616   132
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177   133056    80388    24024     2805        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals  11
 bummer (warning):transition densities: resetting ref occupation number to 0                      0
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    11                     1
 block size     0
 pthz,pthy,pthx,pthw:   345   963     0     0 total internal walks:    1308
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1 1

 setref:       45 references kept,
                0 references were marked as invalid, out of
               45 total.
 nmb.of records onel     1
 nmb.of records 2-ext   105
 nmb.of records 1-ext    32
 nmb.of records 0-ext     4
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61968
    threx             60019
    twoex              9152
    onex               2447
    allin               766
    diagon             2110
               =======
   maximum            61968
 
  __ static summary __ 
   reflst               345
   hrfspc               345
               -------
   static->             345
 
  __ core required  __ 
   totstc               345
   max n-ex           61968
               -------
   totnec->           62313
 
  __ core available __ 
   totspc         301465599
   totnec -           62313
               -------
   totvec->       301403286

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=   301403286
 reducing frespc by                  6724 to              301396562 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         345|       345|         0|       345|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         963|     25452|       345|       909|       345|         2|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=         220DP  conft+indsym=        3636DP  drtbuffer=        2868 DP

dimension of the ci-matrix ->>>     25797

 executing brd_struct for civct
 gentasklist: ntask=                     6
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1     963     345      25452        345     909     345
     2  1   1     1      allint zz    OX  1 1     345     345        345        345     345     345
     3  2   2     5      0ex2ex yy    OX  2 2     963     963      25452      25452     909     909
     4  2   2    42      four-ext y   4X  2 2     963     963      25452      25452     909     909
     5  1   1    75      dg-024ext z  OX  1 1     345     345        345        345     345     345
     6  2   2    76      dg-024ext y  OX  2 2     963     963      25452      25452     909     909
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=   5.000 N=  1 (task/type/sgbra)=(   1/11/0) (
REDTASK #   2 TIME=   4.000 N=  1 (task/type/sgbra)=(   2/ 1/0) (
REDTASK #   3 TIME=   3.000 N=  1 (task/type/sgbra)=(   3/ 5/0) (
REDTASK #   4 TIME=   2.000 N=  1 (task/type/sgbra)=(   4/42/1) (
REDTASK #   5 TIME=   1.000 N=  1 (task/type/sgbra)=(   5/75/1) (
REDTASK #   6 TIME=   0.000 N=  1 (task/type/sgbra)=(   6/76/1) (
 initializing v-file: 1:                 25797

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      3 vectors will be written to unit 11 beginning with logical record   1

            3 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       21120 2x:           0 4x:           0
All internal counts: zz :       66424 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      45
 dsyevx: computed roots 1 to    6(converged:   6)

    root           eigenvalues
    ----           ------------
       1        -547.1456443344
       2        -547.1387850442
       3        -547.0431326739
       4        -546.9366892078
       5        -546.9307008274
       6        -546.9097542934

 strefv generated    3 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   345

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   345

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   345

         vector  3 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   345)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             25797
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:           0 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:           0 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:           0 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1   -72.66463101
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.56211935

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000        0.00000       1.309780E-16
 refs   2    0.00000        1.00000       3.127004E-18
 refs   3    0.00000        0.00000        1.00000    

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   -1.00000       1.334934E-12   4.222699E-13
 civs   2   1.335043E-12    1.00000       3.599896E-15
 civs   3   4.223983E-13  -3.603023E-15    1.00000    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   -1.00000       1.334934E-12   4.224009E-13
 ref    2   1.335043E-12    1.00000       3.603023E-15
 ref    3   4.223983E-13  -3.603023E-15    1.00000    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -1.00000000     0.00000000     0.00000000
 ref:   2     0.00000000     1.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -547.1456443344 -3.9790E-13  1.1205E-01  5.0478E-01  1.0000E-04
 mr-sdci #  1  2   -547.1387850442  2.8422E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  1  3   -547.0431326739 -3.2685E-13  0.0000E+00  4.9657E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -547.1456443344 -3.9790E-13  1.1205E-01  5.0478E-01  1.0000E-04
 mr-sdci #  1  2   -547.1387850442  2.8422E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  1  3   -547.0431326739 -3.2685E-13  0.0000E+00  4.9657E-01  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    4 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             25797
 number of initial trial vectors:                         4
 number of initial matrix-vector products:                3
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                   90
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -72.66463101
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.56211935
   ht   4     0.11204673     0.00000000    -0.00198627    -4.45069431

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000        0.00000       1.309780E-16   4.883662E-13
 refs   2    0.00000        1.00000       3.127004E-18  -1.872074E-13
 refs   3    0.00000        0.00000        1.00000      -6.047761E-14

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.977699       1.300120E-09  -9.008734E-03   0.209820    
 civs   2   1.266761E-09   -1.00000      -1.348844E-11   2.930519E-10
 civs   3  -8.374238E-03   1.733098E-12  -0.999957      -3.912256E-03
 civs   4  -0.836248       7.963363E-11  -8.240783E-03    3.89631    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.977699       1.300120E-09  -9.008734E-03   0.209820    
 ref    2   1.266918E-09   -1.00000      -1.348690E-11   2.923225E-10
 ref    3  -8.374238E-03   1.733098E-12  -0.999957      -3.912256E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97769855     0.00000000    -0.00900873     0.20981991
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000
 ref:   3    -0.00837424     0.00000000    -0.99995728    -0.00391226

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -547.2414804264  9.5836E-02  9.6415E-03  1.2059E-01  1.0000E-04
 mr-sdci #  1  2   -547.1387850442  1.4211E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  1  3   -547.0431490430  1.6369E-05  0.0000E+00  4.9654E-01  1.0000E-04
 mr-sdci #  1  4   -545.0649600138  7.0584E+01  0.0000E+00  1.0359E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -72.66463101
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.56211935
   ht   4     0.11204673     0.00000000    -0.00198627    -4.45069431
   ht   5    -3.22147225     0.00000088     0.12570670    -0.18317759    -0.85435351

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000        0.00000       1.309780E-16   4.883662E-13   4.432981E-02
 refs   2    0.00000        1.00000       3.127004E-18  -1.872074E-13  -1.204471E-08
 refs   3    0.00000        0.00000        1.00000      -6.047761E-14  -1.738517E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.947493      -3.328625E-11  -6.625657E-03  -0.546403      -4.965231E-02
 civs   2  -7.843299E-09    1.00000      -3.550356E-10   1.158298E-07  -3.769241E-08
 civs   3   6.195643E-03  -5.978753E-11   -1.00001       1.576278E-02  -1.220389E-03
 civs   4   0.883730      -4.844896E-11  -7.147317E-03   -1.56383       -3.58408    
 civs   5  -0.613027      -9.169661E-09  -2.534333E-02    9.55198       -3.09056    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.974669      -4.397756E-10  -7.749122E-03  -0.122966      -0.186656    
 ref    2  -4.597311E-10    1.00000      -4.978127E-11   7.793025E-10  -4.668047E-10
 ref    3   7.261401E-03  -4.384592E-11  -0.999965      -8.435002E-04   4.152608E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.97466852     0.00000000    -0.00774912    -0.12296593    -0.18665641
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00726140     0.00000000    -0.99996466    -0.00084350     0.00415261

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -547.2474020470  5.9216E-03  1.9135E-03  5.4546E-02  1.0000E-04
 mr-sdci #  2  2   -547.1387850442  1.4211E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  2  3   -547.0431581179  9.0749E-06  0.0000E+00  4.9649E-01  1.0000E-04
 mr-sdci #  2  4   -545.7002851966  6.3533E-01  0.0000E+00  1.2673E+00  1.0000E-04
 mr-sdci #  2  5   -544.9985301738  7.0518E+01  0.0000E+00  9.9552E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.66463101
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.56211935
   ht   4     0.11204673     0.00000000    -0.00198627    -4.45069431
   ht   5    -3.22147225     0.00000088     0.12570670    -0.18317759    -0.85435351
   ht   6    -0.63947877     0.00000029     0.02068157    -0.05339816    -0.08618337    -0.12181217

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000        0.00000       1.309780E-16   4.883662E-13   4.432981E-02   8.799368E-03
 refs   2    0.00000        1.00000       3.127004E-18  -1.872074E-13  -1.204471E-08  -4.038081E-09
 refs   3    0.00000        0.00000        1.00000      -6.047761E-14  -1.738517E-03  -2.995868E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.945879       3.870863E-09  -1.144077E-02   0.392323       0.287629      -0.290740    
 civs   2  -3.301584E-09    1.00000       1.302678E-09  -5.500512E-08  -1.210612E-07   5.062655E-08
 civs   3   9.547862E-03   2.028630E-09  -0.999797      -2.509094E-02  -2.789769E-03   2.797897E-04
 civs   4   0.894092       2.382172E-10  -1.140777E-02    1.25620       -1.44830       -3.41190    
 civs   5  -0.751663      -4.928796E-08   6.134819E-02   -8.84025       -5.16389      -9.611945E-02
 civs   6   0.705823       2.059733E-07  -0.414793        14.6402       -15.3721        13.6550    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.972989       3.498373E-09  -1.237113E-02   0.129261      -7.654963E-02  -0.174845    
 ref    2   2.901641E-09    1.00000       2.238726E-09  -7.645415E-09   3.210402E-09  -3.354959E-09
 ref    3   1.064319E-02   2.052612E-09  -0.999779      -1.410802E-02   1.079302E-02  -3.643955E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97298907     0.00000000    -0.01237113     0.12926121    -0.07654963    -0.17484540
 ref:   2     0.00000000     1.00000000     0.00000000    -0.00000001     0.00000000     0.00000000
 ref:   3     0.01064319     0.00000000    -0.99977893    -0.01410802     0.01079302    -0.00364395

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -547.2487532496  1.3512E-03  4.0996E-04  2.5838E-02  1.0000E-04
 mr-sdci #  3  2   -547.1387850442 -4.2633E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  3  3   -547.0435666768  4.0856E-04  0.0000E+00  4.9624E-01  1.0000E-04
 mr-sdci #  3  4   -546.0473810050  3.4710E-01  0.0000E+00  1.0761E+00  1.0000E-04
 mr-sdci #  3  5   -545.0587076010  6.0177E-02  0.0000E+00  1.3103E+00  1.0000E-04
 mr-sdci #  3  6   -544.9533981679  7.0472E+01  0.0000E+00  1.0135E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.66463101
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.56211935
   ht   4     0.11204673     0.00000000    -0.00198627    -4.45069431
   ht   5    -3.22147225     0.00000088     0.12570670    -0.18317759    -0.85435351
   ht   6    -0.63947877     0.00000029     0.02068157    -0.05339816    -0.08618337    -0.12181217
   ht   7     0.11927933    -0.00000035     0.00541545    -0.05333140     0.04581372     0.00316309    -0.02991767

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000        0.00000       1.309780E-16   4.883662E-13   4.432981E-02   8.799368E-03  -1.642540E-03
 refs   2    0.00000        1.00000       3.127004E-18  -1.872074E-13  -1.204471E-08  -4.038081E-09   4.882365E-09
 refs   3    0.00000        0.00000        1.00000      -6.047761E-14  -1.738517E-03  -2.995868E-04  -8.286809E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.942265      -1.184690E-08   2.872802E-02  -0.271410      -0.437556      -0.233529       0.101412    
 civs   2   4.553175E-09   -1.00000      -2.599020E-08   1.886772E-07   2.355533E-09  -1.873891E-08  -2.029345E-07
 civs   3  -1.353774E-02  -1.139385E-08   0.996821       8.084230E-02   6.465549E-03   4.156677E-04   1.693125E-03
 civs   4  -0.896535      -1.374028E-09   2.303980E-02  -0.574092      -0.244131       -3.88049      -0.635071    
 civs   5   0.793553       1.034441E-07  -0.235199        4.68357        9.37684      -0.201416        2.09106    
 civs   6  -0.919798      -4.576287E-07    1.12060       -12.7897        5.53403        6.39023       -20.0158    
 civs   7   -1.00171      -1.078985E-06    2.81653       -31.4985        26.3776        8.07753        30.3976    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970995      -9.515810E-09   2.353605E-02  -0.124593      -1.651310E-02  -0.199496      -3.194705E-02
 ref    2  -6.181267E-09   -1.00000      -1.393106E-08   3.012378E-08  -4.147375E-09  -2.679013E-09   1.116961E-09
 ref    3  -1.455877E-02  -1.134717E-08   0.996660       7.914170E-02  -1.368003E-02  -1.817964E-03   1.535247E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97099479    -0.00000001     0.02353605    -0.12459272    -0.01651310    -0.19949567    -0.03194705
 ref:   2    -0.00000001    -1.00000000    -0.00000001     0.00000003     0.00000000     0.00000000     0.00000000
 ref:   3    -0.01455877    -0.00000001     0.99666034     0.07914170    -0.01368003    -0.00181796     0.00153525

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -547.2491640184  4.1077E-04  1.3345E-04  1.4774E-02  1.0000E-04
 mr-sdci #  4  2   -547.1387850442  5.6843E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  4  3   -547.0459512969  2.3846E-03  0.0000E+00  4.9307E-01  1.0000E-04
 mr-sdci #  4  4   -546.6462976816  5.9892E-01  0.0000E+00  7.4915E-01  1.0000E-04
 mr-sdci #  4  5   -545.1944346703  1.3573E-01  0.0000E+00  1.2010E+00  1.0000E-04
 mr-sdci #  4  6   -544.9609540319  7.5559E-03  0.0000E+00  9.8120E-01  1.0000E-04
 mr-sdci #  4  7   -544.8771096967  7.0396E+01  0.0000E+00  1.3659E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.66463101
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.56211935
   ht   4     0.11204673     0.00000000    -0.00198627    -4.45069431
   ht   5    -3.22147225     0.00000088     0.12570670    -0.18317759    -0.85435351
   ht   6    -0.63947877     0.00000029     0.02068157    -0.05339816    -0.08618337    -0.12181217
   ht   7     0.11927933    -0.00000035     0.00541545    -0.05333140     0.04581372     0.00316309    -0.02991767
   ht   8     0.17271431    -0.00000005     0.01659584     0.00981951    -0.00346695     0.00580041     0.00197795    -0.00904979

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000        0.00000       1.309780E-16   4.883662E-13   4.432981E-02   8.799368E-03  -1.642540E-03  -2.376672E-03
 refs   2    0.00000        1.00000       3.127004E-18  -1.872074E-13  -1.204471E-08  -4.038081E-09   4.882365E-09   6.355020E-10
 refs   3    0.00000        0.00000        1.00000      -6.047761E-14  -1.738517E-03  -2.995868E-04  -8.286809E-05  -2.234338E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.942856      -1.575982E-08   3.514038E-02  -0.145276      -0.531635       0.141068      -0.262275       6.914539E-02
 civs   2   1.830665E-09   -1.00000      -7.942025E-08   2.024437E-07   4.245097E-08  -4.935631E-08   5.995945E-08  -2.171351E-07
 civs   3  -1.606783E-02  -3.926238E-08   0.979118       0.202551      -1.596053E-02  -1.470458E-02   4.484869E-03   1.890998E-02
 civs   4  -0.896905      -3.557611E-09   3.822037E-02  -0.343459       -1.19642       -1.75525       -3.06865       -1.35336    
 civs   5   0.805066       1.886364E-07  -0.462084        3.21537        8.70874       -4.83734      -0.266604        2.62702    
 civs   6  -0.979737      -8.354684E-07    1.99023       -9.97303       -7.28648       -12.9792        16.4990       -7.55304    
 civs   7   -1.28312      -2.619187E-06    6.14093       -30.1164        12.6017       -8.49309       -1.32834        39.0870    
 civs   8   0.865730       3.789471E-06   -7.22554        31.8332       -62.3930       -33.7150        12.5269        49.7886    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.969973      -1.945340E-08   3.925502E-02  -0.116685      -8.210621E-02  -9.349939E-02  -0.156503      -6.339461E-02
 ref    2  -9.624201E-09   -1.00000      -5.650087E-08   7.717801E-08  -1.114414E-08  -1.572762E-09  -1.977846E-09   4.201130E-09
 ref    3  -1.726104E-02  -3.996968E-08   0.980431       0.195331      -1.602146E-02   5.830508E-03  -2.683399E-03   2.242131E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96997324    -0.00000002     0.03925502    -0.11668489    -0.08210621    -0.09349939    -0.15650286    -0.06339461
 ref:   2    -0.00000001    -1.00000000    -0.00000006     0.00000008    -0.00000001     0.00000000     0.00000000     0.00000000
 ref:   3    -0.01726104    -0.00000004     0.98043091     0.19533146    -0.01602146     0.00583051    -0.00268340     0.00224213

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -547.2492795589  1.1554E-04  5.2983E-05  8.7590E-03  1.0000E-04
 mr-sdci #  5  2   -547.1387850442  0.0000E+00  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  5  3   -547.0517126759  5.7614E-03  0.0000E+00  4.8106E-01  1.0000E-04
 mr-sdci #  5  4   -546.8384122616  1.9211E-01  0.0000E+00  5.2967E-01  1.0000E-04
 mr-sdci #  5  5   -545.4693925150  2.7496E-01  0.0000E+00  1.1501E+00  1.0000E-04
 mr-sdci #  5  6   -545.0679968437  1.0704E-01  0.0000E+00  1.2398E+00  1.0000E-04
 mr-sdci #  5  7   -544.9428996998  6.5790E-02  0.0000E+00  1.0252E+00  1.0000E-04
 mr-sdci #  5  8   -544.7167015158  7.0236E+01  0.0000E+00  1.3841E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76826623
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.57069935
   ht   4     0.00000000     0.00000000     0.00000000   -72.35739894
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -70.98837919
   ht   6    -0.17872729    -0.00000011     0.01964102    -0.09362443    -0.01713277    -0.00443963

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.969973      -1.945340E-08   3.925502E-02  -0.116685      -8.210621E-02   2.335554E-03
 refs   2  -9.624201E-09   -1.00000      -5.650087E-08   7.717801E-08  -1.114414E-08  -1.438944E-09
 refs   3  -1.726104E-02  -3.996968E-08   0.980431       0.195331      -1.602146E-02  -5.703729E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.998060      -3.076894E-08  -4.795911E-02   8.131676E-02  -0.229062      -0.230722    
 civs   2  -2.605500E-09    1.00000      -1.593834E-07   1.229654E-07  -1.777561E-07  -1.655932E-07
 civs   3   3.376602E-03  -1.069569E-07  -0.969420      -0.219526       9.338268E-02   6.727210E-02
 civs   4  -3.594059E-03   7.128274E-08   0.162020      -0.908837      -0.340981      -0.246306    
 civs   5   1.478969E-03  -2.110675E-08  -3.647707E-02   7.081155E-02  -0.722265       0.688075    
 civs   6  -0.776055       1.048385E-05    17.4542       -31.0935        91.8722        93.0346    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.969474      -3.559607E-08  -5.971881E-02   9.787047E-02   9.514370E-02  -3.162116E-02
 ref    2   1.284309E-08   -1.00000       2.024133E-07  -1.375341E-07   2.421832E-08   3.463739E-09
 ref    3   1.985662E-02  -1.306385E-07  -0.918385      -0.393519       3.523632E-02   5.496377E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96947396    -0.00000004    -0.05971881     0.09787047     0.09514370    -0.03162116
 ref:   2     0.00000001    -1.00000000     0.00000020    -0.00000014     0.00000002     0.00000000
 ref:   3     0.01985662    -0.00000013    -0.91838481    -0.39351902     0.03523632     0.00549638

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -547.2493206783  4.1119E-05  2.1205E-05  5.9880E-03  1.0000E-04
 mr-sdci #  6  2   -547.1387850442 -8.5265E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  6  3   -547.0680353833  1.6323E-02  0.0000E+00  4.3137E-01  1.0000E-04
 mr-sdci #  6  4   -546.9178140270  7.9402E-02  0.0000E+00  4.4897E-01  1.0000E-04
 mr-sdci #  6  5   -545.8506885080  3.8130E-01  0.0000E+00  1.0815E+00  1.0000E-04
 mr-sdci #  6  6   -545.0929828279  2.4986E-02  0.0000E+00  1.2910E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76826623
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.57069935
   ht   4     0.00000000     0.00000000     0.00000000   -72.35739894
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -70.98837919
   ht   6    -0.17872729    -0.00000011     0.01964102    -0.09362443    -0.01713277    -0.00443963
   ht   7    -0.10735521     0.00000001    -0.02114615     0.06630529    -0.06881804    -0.00042262    -0.00159554

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.969973      -1.945340E-08   3.925502E-02  -0.116685      -8.210621E-02   2.335554E-03   1.609347E-03
 refs   2  -9.624201E-09   -1.00000      -5.650087E-08   7.717801E-08  -1.114414E-08  -1.438944E-09   4.728155E-11
 refs   3  -1.726104E-02  -3.996968E-08   0.980431       0.195331      -1.602146E-02  -5.703729E-05   6.801389E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.998503      -3.729284E-08  -3.691677E-02   4.459269E-02   7.094085E-03  -8.661461E-02  -0.445320    
 civs   2  -5.856937E-09    1.00000      -6.334137E-07   2.111617E-07  -2.259909E-07   9.400268E-08  -1.289693E-07
 civs   3   6.041504E-03  -4.254153E-07  -0.880143      -0.434077       0.194494      -7.294054E-02   9.680070E-03
 civs   4  -4.900665E-03   2.041323E-07   0.254471      -0.772770      -0.606370       0.208876      -0.103116    
 civs   5   2.072395E-03  -6.051011E-08  -6.329916E-02   8.214192E-02  -0.383825      -0.944048       5.928345E-03
 civs   6   -1.18614       3.267487E-05    32.6933       -37.2302        77.2912       -34.3065        94.8308    
 civs   7    1.02485      -3.972365E-05   -36.9581        36.2043       -136.378        116.398        142.509    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.969004      -7.879226E-08  -7.797539E-02   8.095253E-02   7.782312E-02   7.346188E-02   3.080564E-02
 ref    2   1.647931E-08   -1.00000       6.550516E-07  -1.923375E-07   5.474663E-08  -7.538087E-09  -5.034215E-09
 ref    3   2.230538E-02  -4.201388E-07  -0.815940      -0.574029       6.458788E-02  -4.219534E-03   1.224239E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96900355    -0.00000008    -0.07797539     0.08095253     0.07782312     0.07346188     0.03080564
 ref:   2     0.00000002    -1.00000000     0.00000066    -0.00000019     0.00000005    -0.00000001    -0.00000001
 ref:   3     0.02230538    -0.00000042    -0.81593997    -0.57402861     0.06458788    -0.00421953     0.00122424

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -547.2493424111  2.1733E-05  5.3070E-06  3.0290E-03  1.0000E-04
 mr-sdci #  7  2   -547.1387850442  5.6843E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  7  3   -547.0941516552  2.6116E-02  0.0000E+00  3.4314E-01  1.0000E-04
 mr-sdci #  7  4   -546.9499725316  3.2159E-02  0.0000E+00  4.5859E-01  1.0000E-04
 mr-sdci #  7  5   -546.3023393097  4.5165E-01  0.0000E+00  8.7301E-01  1.0000E-04
 mr-sdci #  7  6   -545.1633975762  7.0415E-02  0.0000E+00  1.1743E+00  1.0000E-04
 mr-sdci #  7  7   -544.9958935966  5.2994E-02  0.0000E+00  1.3618E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76826623
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.57069935
   ht   4     0.00000000     0.00000000     0.00000000   -72.35739894
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -70.98837919
   ht   6    -0.17872729    -0.00000011     0.01964102    -0.09362443    -0.01713277    -0.00443963
   ht   7    -0.10735521     0.00000001    -0.02114615     0.06630529    -0.06881804    -0.00042262    -0.00159554
   ht   8     0.02002036     0.00000003    -0.00042764     0.00489952    -0.01019971     0.00047164    -0.00005104    -0.00036627

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.969973      -1.945340E-08   3.925502E-02  -0.116685      -8.210621E-02   2.335554E-03   1.609347E-03  -2.935019E-04
 refs   2  -9.624201E-09   -1.00000      -5.650087E-08   7.717801E-08  -1.114414E-08  -1.438944E-09   4.728155E-11   4.642559E-10
 refs   3  -1.726104E-02  -3.996968E-08   0.980431       0.195331      -1.602146E-02  -5.703729E-05   6.801389E-05  -4.836555E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.998288      -1.388532E-07  -4.774798E-02   4.436986E-02  -4.696815E-03  -3.930123E-03   0.455836      -6.190010E-03
 civs   2  -8.285021E-09    1.00000      -2.524320E-06   2.673298E-07  -3.539304E-07   9.311309E-08   1.161667E-07   3.276178E-08
 civs   3   7.724220E-03  -1.831563E-06  -0.814816      -0.512544       0.269653       3.187470E-03   8.431792E-03  -8.559078E-02
 civs   4  -5.085382E-03   5.822749E-07   0.230413      -0.709913      -0.634373      -0.107206       3.845976E-02   0.339782    
 civs   5   2.362690E-03  -2.150207E-07  -7.725473E-02   8.486312E-02  -0.180447      -0.849648       0.143419      -0.513483    
 civs   6   -1.29798       1.018934E-04    36.2899       -36.2758        37.1827        36.9603       -78.9838       -98.9523    
 civs   7    1.30439      -1.509383E-04   -51.7405        40.7226       -102.697       -7.56182       -168.809        112.870    
 civs   8    1.11710      -2.271223E-04   -73.1190        32.1887       -224.229        279.000        39.9674       -308.269    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968871      -2.145973E-07  -7.589332E-02   7.015119E-02   8.224556E-02   7.084994E-02  -4.165648E-02   3.416417E-02
 ref    2   1.948553E-08   -1.00000       2.500850E-06  -2.254649E-07   1.293320E-07  -1.607474E-08   4.566659E-09   8.687681E-09
 ref    3   2.393072E-02  -1.731091E-06  -0.757037      -0.638625       0.135414      -8.107039E-03  -1.556422E-03   5.599520E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96887064    -0.00000021    -0.07589332     0.07015119     0.08224556     0.07084994    -0.04165648     0.03416417
 ref:   2     0.00000002    -1.00000000     0.00000250    -0.00000023     0.00000013    -0.00000002     0.00000000     0.00000001
 ref:   3     0.02393072    -0.00000173    -0.75703710    -0.63862479     0.13541442    -0.00810704    -0.00155642     0.00559952

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -547.2493483396  5.9286E-06  2.2176E-06  1.9002E-03  1.0000E-04
 mr-sdci #  8  2   -547.1387850442  8.5265E-14  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  8  3   -547.1198736110  2.5722E-02  0.0000E+00  2.7232E-01  1.0000E-04
 mr-sdci #  8  4   -546.9545337901  4.5613E-03  0.0000E+00  4.8279E-01  1.0000E-04
 mr-sdci #  8  5   -546.6369545333  3.3462E-01  0.0000E+00  6.4951E-01  1.0000E-04
 mr-sdci #  8  6   -545.4592565482  2.9586E-01  0.0000E+00  1.1966E+00  1.0000E-04
 mr-sdci #  8  7   -544.9999399811  4.0464E-03  0.0000E+00  1.3609E+00  1.0000E-04
 mr-sdci #  8  8   -544.8365669088  1.1987E-01  0.0000E+00  1.1798E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833501
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.63886028
   ht   4     0.00000000     0.00000000     0.00000000   -72.47352046
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.15594121
   ht   6     0.00202470     0.00000000     0.00063490     0.00319131     0.02848750    -0.00014028

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968871      -2.145973E-07  -7.589332E-02   7.015119E-02   8.224556E-02  -9.377593E-06
 refs   2   1.948553E-08   -1.00000       2.500850E-06  -2.254649E-07   1.293320E-07  -9.781263E-11
 refs   3   2.393072E-02  -1.731091E-06  -0.757037      -0.638625       0.135414      -2.542555E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00002      -5.858665E-09  -5.815360E-04   9.848382E-07   4.350678E-03   1.917167E-02
 civs   2  -4.942406E-09    1.00000      -7.767697E-06   1.893474E-10   3.380885E-07   2.195600E-07
 civs   3  -1.108309E-03   7.662727E-06   0.992741       5.682513E-05   9.741511E-02   7.066670E-02
 civs   4   3.318147E-05  -3.357250E-08  -2.936087E-03    1.00000       8.069589E-03   3.139915E-02
 civs   5  -4.122572E-04   5.703133E-07   5.078576E-02  -6.896221E-05  -0.891453       0.533697    
 civs   6   0.746702      -7.514262E-04   -65.6835       4.843723E-02    185.665        714.041    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.968844      -7.388736E-07  -7.019213E-02   7.013995E-02  -8.610142E-02   1.546291E-02
 ref    2   2.152276E-08   -1.00000       1.026404E-05  -2.255263E-07  -2.296563E-07  -5.035719E-08
 ref    3   2.467421E-02  -7.414426E-06  -0.741134      -0.638680      -0.204232      -1.897542E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96884393    -0.00000074    -0.07019213     0.07013995    -0.08610142     0.01546291
 ref:   2     0.00000002    -1.00000000     0.00001026    -0.00000023    -0.00000023    -0.00000005
 ref:   3     0.02467421    -0.00000741    -0.74113380    -0.63867971    -0.20423236    -0.01897542

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -547.2493499955  1.6559E-06  9.4477E-07  1.1720E-03  1.0000E-04
 mr-sdci #  9  2   -547.1387850442  4.8317E-13  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci #  9  3   -547.1326358203  1.2762E-02  0.0000E+00  2.2746E-01  1.0000E-04
 mr-sdci #  9  4   -546.9545337959  5.7344E-09  0.0000E+00  4.8281E-01  1.0000E-04
 mr-sdci #  9  5   -546.7477825405  1.1083E-01  0.0000E+00  5.6079E-01  1.0000E-04
 mr-sdci #  9  6   -545.0423661187 -4.1689E-01  0.0000E+00  1.3759E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  10

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833501
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.63886028
   ht   4     0.00000000     0.00000000     0.00000000   -72.47352046
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.15594121
   ht   6     0.00202470     0.00000000     0.00063490     0.00319131     0.02848750    -0.00014028
   ht   7    -0.02685203     0.00000003     0.00646166    -0.00720023     0.01550069     0.00000316    -0.00008185

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968871      -2.145973E-07  -7.589332E-02   7.015119E-02   8.224556E-02  -9.377593E-06  -3.736600E-04
 refs   2   1.948553E-08   -1.00000       2.500850E-06  -2.254649E-07   1.293320E-07  -9.781263E-11   1.638744E-10
 refs   3   2.393072E-02  -1.731091E-06  -0.757037      -0.638625       0.135414      -2.542555E-05  -1.851454E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.999688       3.835268E-02   6.054076E-07   1.339082E-02  -0.107702      -0.293088      -0.213378    
 civs   2   8.592754E-09   2.021123E-05   -1.00000      -1.279541E-07   9.556024E-07   6.875899E-07   1.224132E-07
 civs   3   1.757353E-03   0.964198       1.977847E-05  -3.085271E-02   0.235706       0.149300       2.062322E-03
 civs   4   1.132026E-04   1.863676E-02   2.978943E-07    1.00041       4.683785E-02  -4.700037E-02  -7.370079E-02
 civs   5   6.059083E-04   8.253513E-02   1.373204E-06   3.300087E-02  -0.707392       0.755840      -0.253457    
 civs   6   -1.13919       -108.303      -1.772906E-03   -19.3434        258.473        362.928       -585.973    
 civs   7  -0.921287       -105.956      -1.678100E-03   -37.3534        307.512        819.980        535.389    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.968848      -6.163239E-02  -1.095517E-06   7.640056E-02  -8.576206E-02   2.170384E-02  -1.399549E-02
 ref    2  -2.366403E-08  -1.779946E-05    1.00000      -1.744615E-07  -4.451736E-07  -1.126970E-07   7.475619E-09
 ref    3  -2.519785E-02  -0.725026      -1.315560E-05  -0.609557      -0.318983      -1.208167E-02   1.106413E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96884754    -0.06163239    -0.00000110     0.07640056    -0.08576206     0.02170384    -0.01399549
 ref:   2    -0.00000002    -0.00001780     1.00000000    -0.00000017    -0.00000045    -0.00000011     0.00000001
 ref:   3    -0.02519785    -0.72502588    -0.00001316    -0.60955740    -0.31898347    -0.01208167     0.01106413

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -547.2493508659  8.7041E-07  3.7370E-07  7.2553E-04  1.0000E-04
 mr-sdci # 10  2   -547.1435895948  4.8046E-03  0.0000E+00  1.8942E-01  1.0000E-04
 mr-sdci # 10  3   -547.1387850442  6.1492E-03  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci # 10  4   -546.9553942892  8.6049E-04  0.0000E+00  4.6402E-01  1.0000E-04
 mr-sdci # 10  5   -546.8478111586  1.0003E-01  0.0000E+00  4.8691E-01  1.0000E-04
 mr-sdci # 10  6   -545.9488217560  9.0646E-01  0.0000E+00  1.0152E+00  1.0000E-04
 mr-sdci # 10  7   -544.6841553167 -3.1578E-01  0.0000E+00  1.4008E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.010000
time for vector access                 0.000000

          starting ci iteration  11

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833501
   ht   2     0.00000000   -72.65777172
   ht   3     0.00000000     0.00000000   -72.63886028
   ht   4     0.00000000     0.00000000     0.00000000   -72.47352046
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.15594121
   ht   6     0.00202470     0.00000000     0.00063490     0.00319131     0.02848750    -0.00014028
   ht   7    -0.02685203     0.00000003     0.00646166    -0.00720023     0.01550069     0.00000316    -0.00008185
   ht   8    -0.04162627     0.00000000     0.00050158     0.00062990    -0.00153962     0.00001525    -0.00002054    -0.00005069

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968871      -2.145973E-07  -7.589332E-02   7.015119E-02   8.224556E-02  -9.377593E-06  -3.736600E-04  -6.161036E-04
 refs   2   1.948553E-08   -1.00000       2.500850E-06  -2.254649E-07   1.293320E-07  -9.781263E-11   1.638744E-10   2.329739E-11
 refs   3   2.393072E-02  -1.731091E-06  -0.757037      -0.638625       0.135414      -2.542555E-05  -1.851454E-05   3.113252E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.999992       3.568339E-03   5.751677E-08   2.540704E-02   2.107584E-02  -0.224320      -0.912024      -0.302172    
 civs   2  -1.057738E-08  -1.333305E-05   -1.00000       4.533426E-07   1.293931E-06  -7.054978E-07   2.676205E-07  -4.430878E-08
 civs   3  -1.988487E-03  -0.946885       1.310309E-05   9.327914E-02   0.286512      -0.126876       6.694184E-02   2.290534E-02
 civs   4  -2.187407E-04  -3.855816E-02   3.148146E-07  -0.986199       0.176631       1.410708E-02  -5.507616E-02   6.070529E-02
 civs   5  -6.454496E-04  -8.996838E-02   8.690499E-07  -8.926139E-02  -0.601275      -0.794995       0.167841       0.325665    
 civs   6    1.25857        123.404      -1.143913E-03    55.6619        263.879       -233.866        24.0600        661.207    
 civs   7    1.20149        144.790      -1.229451E-03    105.546        374.447       -652.695        598.060       -353.783    
 civs   8  -0.708400       -98.3664       6.769483E-04   -110.535       -268.331        802.707        1209.68        787.708    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968804       6.364518E-02  -6.889527E-07  -8.007951E-02  -5.629628E-02  -8.590049E-02  -8.048910E-02  -3.724575E-02
 ref    2   2.511296E-08   1.097153E-05    1.00000       5.152906E-10  -6.652844E-07   2.124460E-07   3.998030E-08   1.983686E-08
 ref    3   2.541186E-02   0.720473      -8.197504E-06   0.540905      -0.432614       1.703963E-02   1.137393E-02  -4.977656E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880434     0.06364518    -0.00000069    -0.08007951    -0.05629628    -0.08590049    -0.08048910    -0.03724575
 ref:   2     0.00000003     0.00001097     1.00000000     0.00000000    -0.00000067     0.00000021     0.00000004     0.00000002
 ref:   3     0.02541186     0.72047272    -0.00000820     0.54090549    -0.43261389     0.01703963     0.01137393    -0.00497766

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -547.2493511307  2.6473E-07  5.2434E-08  3.0601E-04  1.0000E-04
 mr-sdci # 11  2   -547.1483650078  4.7754E-03  0.0000E+00  1.6702E-01  1.0000E-04
 mr-sdci # 11  3   -547.1387850442  4.8317E-13  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci # 11  4   -546.9595880989  4.1938E-03  0.0000E+00  4.1576E-01  1.0000E-04
 mr-sdci # 11  5   -546.8827478146  3.4937E-02  0.0000E+00  4.8225E-01  1.0000E-04
 mr-sdci # 11  6   -546.2597376956  3.1092E-01  0.0000E+00  6.8559E-01  1.0000E-04
 mr-sdci # 11  7   -545.1046584447  4.2050E-01  0.0000E+00  1.3353E+00  1.0000E-04
 mr-sdci # 11  8   -544.5263082562 -3.1026E-01  0.0000E+00  1.3288E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  12

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833780
   ht   2     0.00000000   -72.66735168
   ht   3     0.00000000     0.00000000   -72.65777172
   ht   4     0.00000000     0.00000000     0.00000000   -72.47857477
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.40173449
   ht   6    -0.00607100    -0.00107956     0.00000001    -0.00070565    -0.00268987    -0.00000372

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968804       6.364518E-02  -6.889527E-07  -8.007951E-02  -5.629628E-02  -8.849584E-05
 refs   2   2.511296E-08   1.097153E-05    1.00000       5.152906E-10  -6.652844E-07   5.839229E-11
 refs   3   2.541186E-02   0.720473      -8.197504E-06   0.540905      -0.432614       2.121884E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.999930       1.570777E-02  -2.903033E-08   2.312155E-02   4.412685E-02  -0.397368    
 civs   2   7.845541E-05  -0.996206      -4.022942E-07   2.047714E-02   3.099317E-02  -0.105736    
 civs   3   2.478408E-10  -4.072237E-07    1.00000       1.726435E-08   1.321494E-08   2.850302E-07
 civs   4   2.771104E-05  -1.037918E-02   2.065682E-08  -0.990450       0.112157      -9.182032E-02
 civs   5   3.594009E-05  -1.359776E-02   2.686881E-08  -9.071503E-02  -0.968474      -0.291170    
 civs   6   0.832877       -187.090       3.460015E-04   -276.535       -527.973        4761.07    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.968810      -6.046812E-02  -7.202186E-07   8.779691E-02   5.148576E-02  -1.934786E-02
 ref    2   2.624457E-08  -1.133861E-05    1.00000       2.862040E-07   9.679048E-07  -4.133641E-07
 ref    3   2.546783E-02  -0.717468      -8.487800E-06  -0.481741       0.501972       1.226184E-04

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96880995    -0.06046812    -0.00000072     0.08779691     0.05148576    -0.01934786
 ref:   2     0.00000003    -0.00001134     1.00000000     0.00000029     0.00000097    -0.00000041
 ref:   3     0.02546783    -0.71746843    -0.00000849    -0.48174137     0.50197242     0.00012262

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -547.2493511743  4.3671E-08  2.1652E-08  1.7055E-04  1.0000E-04
 mr-sdci # 12  2   -547.1504247107  2.0597E-03  0.0000E+00  1.5547E-01  1.0000E-04
 mr-sdci # 12  3   -547.1387850442 -1.1369E-13  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci # 12  4   -546.9630444111  3.4563E-03  0.0000E+00  3.7806E-01  1.0000E-04
 mr-sdci # 12  5   -546.8984026321  1.5655E-02  0.0000E+00  4.9665E-01  1.0000E-04
 mr-sdci # 12  6   -545.6827277811 -5.7701E-01  0.0000E+00  1.2690E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  13

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833780
   ht   2     0.00000000   -72.66735168
   ht   3     0.00000000     0.00000000   -72.65777172
   ht   4     0.00000000     0.00000000     0.00000000   -72.47857477
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.40173449
   ht   6    -0.00607100    -0.00107956     0.00000001    -0.00070565    -0.00268987    -0.00000372
   ht   7     0.00702118     0.00034129     0.00000000    -0.00037123    -0.00026084     0.00000075    -0.00000242

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968804       6.364518E-02  -6.889527E-07  -8.007951E-02  -5.629628E-02  -8.849584E-05   1.006250E-04
 refs   2   2.511296E-08   1.097153E-05    1.00000       5.152906E-10  -6.652844E-07   5.839229E-11  -9.292766E-11
 refs   3   2.541186E-02   0.720473      -8.197504E-06   0.540905      -0.432614       2.121884E-06  -5.966922E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.999979      -2.254034E-03   2.231895E-07   6.009207E-03   1.231198E-03   0.159773      -0.692570    
 civs   2  -1.183071E-04   0.993869      -2.220589E-06  -4.377352E-02  -4.745630E-02  -8.131621E-02  -8.382354E-02
 civs   3  -1.017385E-09   2.184226E-06    1.00000      -3.370426E-07  -2.959543E-07  -3.166860E-07   3.223983E-07
 civs   4  -4.275379E-05   1.811966E-02   1.452347E-07   0.957156      -0.251467      -0.152321      -2.293695E-02
 civs   5  -5.222276E-05   2.176698E-02   1.520545E-07   0.190004       0.918717      -0.359790      -0.149941    
 civs   6   -1.15667        285.055       1.678229E-03    581.034        801.252        3418.00        3236.14    
 civs   7  -0.784101        224.702       3.772513E-03    565.804        706.853        4613.44       -4378.90    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.968806       6.014663E-02  -8.356071E-07  -9.043787E-02  -3.557655E-02   3.423718E-02  -5.110410E-02
 ref    2  -2.738778E-08   1.306972E-05    1.00000      -9.617189E-07  -1.446829E-06  -1.194684E-06   8.096130E-08
 ref    3  -2.549488E-02   0.715646      -9.797878E-06   0.402004      -0.570147      -1.542584E-03   7.462887E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96880639     0.06014663    -0.00000084    -0.09043787    -0.03557655     0.03423718    -0.05110410
 ref:   2    -0.00000003     0.00001307     1.00000000    -0.00000096    -0.00000145    -0.00000119     0.00000008
 ref:   3    -0.02549488     0.71564631    -0.00000980     0.40200405    -0.57014700    -0.00154258     0.00746289

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -547.2493511913  1.6978E-08  0.0000E+00  9.8684E-05  1.0000E-04
 mr-sdci # 13  2   -547.1516736852  1.2490E-03  1.1970E-02  1.5114E-01  1.0000E-04
 mr-sdci # 13  3   -547.1387850442  4.1211E-13  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci # 13  4   -546.9688441974  5.7998E-03  0.0000E+00  3.3486E-01  1.0000E-04
 mr-sdci # 13  5   -546.9092111891  1.0809E-02  0.0000E+00  5.2047E-01  1.0000E-04
 mr-sdci # 13  6   -546.3722791131  6.8955E-01  0.0000E+00  9.9398E-01  1.0000E-04
 mr-sdci # 13  7   -545.0751018701 -2.9557E-02  0.0000E+00  1.3184E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  14

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833780
   ht   2     0.00000000   -72.66735168
   ht   3     0.00000000     0.00000000   -72.65777172
   ht   4     0.00000000     0.00000000     0.00000000   -72.47857477
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.40173449
   ht   6    -0.00607100    -0.00107956     0.00000001    -0.00070565    -0.00268987    -0.00000372
   ht   7     0.00702118     0.00034129     0.00000000    -0.00037123    -0.00026084     0.00000075    -0.00000242
   ht   8    -2.79195295    -0.23102001     0.00000049     0.25367662    -0.60857867    -0.00039664     0.00026145    -0.77898177

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968804       6.364518E-02  -6.889527E-07  -8.007951E-02  -5.629628E-02  -8.849584E-05   1.006250E-04  -3.974662E-02
 refs   2   2.511296E-08   1.097153E-05    1.00000       5.152906E-10  -6.652844E-07   5.839229E-11  -9.292766E-11   6.389513E-09
 refs   3   2.541186E-02   0.720473      -8.197504E-06   0.540905      -0.432614       2.121884E-06  -5.966922E-06  -2.529088E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.999973      -3.519146E-02  -1.168607E-06   5.119346E-02  -2.844511E-02   9.857320E-02  -0.241022      -0.753191    
 civs   2   1.428719E-04   0.970496      -5.111738E-05   0.195765       9.510916E-03  -0.113724      -6.110120E-02  -9.618600E-02
 civs   3   3.101372E-09   4.827537E-05    1.00000       1.785214E-05  -4.430017E-06  -3.035999E-06  -6.147218E-06  -6.844925E-07
 civs   4   6.088783E-05   0.154193       6.451062E-06  -0.917864      -0.284260      -0.223903      -8.354759E-02  -3.559812E-02
 civs   5   3.834082E-05  -7.878094E-02  -5.936409E-06   0.319002      -0.895881      -0.291295       0.159748      -0.130049    
 civs   6    1.16337        281.937      -1.205231E-02   -137.764       -945.234        3257.22       -1690.68        2996.44    
 civs   7   0.808842        337.561      -1.610182E-03   -381.872       -758.775        4563.17       -91.7386       -4434.93    
 civs   8   2.073341E-04    1.14826       5.275824E-05   -1.99413       0.892662        1.81828        9.72869        1.96045    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968806       5.132585E-02  -4.184984E-06   7.143225E-02   7.317852E-02   3.024066E-02  -1.898394E-02  -5.561427E-02
 ref    2   2.974979E-08   5.896724E-05    1.00000       1.980327E-05  -3.709493E-06  -4.309802E-06  -6.958001E-06  -1.072590E-06
 ref    3   2.552758E-02   0.811487      -3.914769E-05  -0.485109       0.240206      -9.943744E-02  -0.192092      -2.357071E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880590     0.05132585    -0.00000418     0.07143225     0.07317852     0.03024066    -0.01898394    -0.05561427
 ref:   2     0.00000003     0.00005897     1.00000000     0.00001980    -0.00000371    -0.00000431    -0.00000696    -0.00000107
 ref:   3     0.02552758     0.81148726    -0.00003915    -0.48510878     0.24020627    -0.09943744    -0.19209188    -0.02357071

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -547.2493511918  4.6198E-10  0.0000E+00  9.4488E-05  1.0000E-04
 mr-sdci # 14  2   -547.1656893053  1.4016E-02  7.7932E-03  1.0242E-01  1.0000E-04
 mr-sdci # 14  3   -547.1387850443  5.6929E-11  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci # 14  4   -547.0145478165  4.5704E-02  0.0000E+00  3.2743E-01  1.0000E-04
 mr-sdci # 14  5   -546.9358315960  2.6620E-02  0.0000E+00  3.1952E-01  1.0000E-04
 mr-sdci # 14  6   -546.4072502046  3.4971E-02  0.0000E+00  9.5460E-01  1.0000E-04
 mr-sdci # 14  7   -545.4340269136  3.5893E-01  0.0000E+00  1.2145E+00  1.0000E-04
 mr-sdci # 14  8   -545.0608296123  5.3452E-01  0.0000E+00  1.3020E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  15

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.68467598
   ht   3     0.00000000     0.00000000   -72.65777172
   ht   4     0.00000000     0.00000000     0.00000000   -72.53353449
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.45481827
   ht   6     4.97090074     0.29720845    -0.00016768     2.06360816    -1.16268086    -1.13411552

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968806       5.132585E-02  -4.184984E-06   7.143225E-02   7.317852E-02   7.000164E-02
 refs   2   2.974979E-08   5.896724E-05    1.00000       1.980327E-05  -3.709493E-06   1.467867E-06
 refs   3   2.552758E-02   0.811487      -3.914769E-05  -0.485109       0.240206       1.457927E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       4.745984E-02  -2.728816E-05   0.107777      -4.445774E-02   0.669799    
 civs   2  -2.193670E-06   0.995878       1.140399E-04  -9.722842E-02   2.012337E-02  -1.098942E-02
 civs   3  -9.513991E-10   1.039800E-04   -1.00000      -8.828699E-05   1.785989E-05  -5.545933E-05
 civs   4   1.291383E-06  -6.947905E-02   5.328371E-05  -0.922814      -0.202877       0.425953    
 civs   5  -5.008253E-07   2.424163E-02  -1.717181E-05   0.144068      -0.970055      -0.250514    
 civs   6  -2.253434E-05   0.694794      -3.994822E-04    1.57777      -0.650819        9.80513    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968806       5.058255E-02   1.106027E-05  -5.433518E-02  -8.693377E-02   4.899984E-02
 ref    2  -3.083624E-08   1.622596E-04   -1.00000      -1.105103E-04   1.767066E-05  -3.233025E-05
 ref    3  -2.553047E-02   0.859012       9.519572E-05   0.429125      -0.128890      -0.115677    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96880576     0.05058255     0.00001106    -0.05433518    -0.08693377     0.04899984
 ref:   2    -0.00000003     0.00016226    -0.99999998    -0.00011051     0.00001767    -0.00003233
 ref:   3    -0.02553047     0.85901156     0.00009520     0.42912549    -0.12888954    -0.11567652

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -547.2493511918  6.0965E-12  0.0000E+00  9.4848E-05  1.0000E-04
 mr-sdci # 15  2   -547.1711423755  5.4531E-03  4.6982E-03  7.5636E-02  1.0000E-04
 mr-sdci # 15  3   -547.1387850462  1.9633E-09  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci # 15  4   -547.0472873143  3.2739E-02  0.0000E+00  2.5419E-01  1.0000E-04
 mr-sdci # 15  5   -546.9437626552  7.9311E-03  0.0000E+00  2.5858E-01  1.0000E-04
 mr-sdci # 15  6   -545.6999720894 -7.0728E-01  0.0000E+00  1.2944E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  16

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.68467598
   ht   3     0.00000000     0.00000000   -72.65777172
   ht   4     0.00000000     0.00000000     0.00000000   -72.53353449
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.45481827
   ht   6     4.97090074     0.29720845    -0.00016768     2.06360816    -1.16268086    -1.13411552
   ht   7    -5.27255472     2.91253368    -0.00001209    -1.09594943     0.67265778     0.50529718    -0.93018046

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968806       5.132585E-02  -4.184984E-06   7.143225E-02   7.317852E-02   7.000164E-02  -7.722223E-02
 refs   2   2.974979E-08   5.896724E-05    1.00000       1.980327E-05  -3.709493E-06   1.467867E-06  -2.381506E-06
 refs   3   2.552758E-02   0.811487      -3.914769E-05  -0.485109       0.240206       1.457927E-02  -4.954678E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.999996      -2.157636E-02   5.703539E-05  -2.224539E-02   1.256672E-02  -8.064929E-02    1.06691    
 civs   2  -1.641266E-05   -1.01407       4.011425E-04   0.110031      -2.043378E-03   0.332473      -0.392364    
 civs   3  -2.036228E-08  -4.038207E-04   -1.00000       5.073981E-04  -5.498122E-05  -1.649075E-04   3.371446E-05
 civs   4   1.074829E-05   0.140767       3.403780E-04   0.886635       0.271030       0.330564       0.332256    
 civs   5  -4.405658E-06  -5.059015E-02  -1.130484E-04  -0.201984       0.955424      -0.190547      -0.201776    
 civs   6  -1.054123E-04   -1.14224      -1.732910E-03   -2.32222       0.774002        8.38531        5.19155    
 civs   7  -1.517673E-04  -0.778928      -2.420611E-03   -1.88217       0.556251        9.01852       -9.83022    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.968806      -4.460014E-02   5.117744E-05   5.853834E-02   8.822370E-02  -4.577364E-03   7.772909E-02
 ref    2  -5.064387E-08  -4.604649E-04   -1.00000       5.332669E-04  -5.346678E-05  -1.472209E-04   4.896902E-05
 ref    3  -2.554109E-02  -0.881959       2.685193E-04  -0.330512       8.040636E-02  -0.262978       6.193359E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96880617    -0.04460014     0.00005118     0.05853834     0.08822370    -0.00457736     0.07772909
 ref:   2    -0.00000005    -0.00046046    -0.99999974     0.00053327    -0.00005347    -0.00014722     0.00004897
 ref:   3    -0.02554109    -0.88195871     0.00026852    -0.33051225     0.08040636    -0.26297805     0.06193359

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -547.2493511919  1.5417E-10  0.0000E+00  9.2109E-05  1.0000E-04
 mr-sdci # 16  2   -547.1748211905  3.6788E-03  1.9462E-03  4.7618E-02  1.0000E-04
 mr-sdci # 16  3   -547.1387850809  3.4617E-08  0.0000E+00  4.9443E-01  1.0000E-04
 mr-sdci # 16  4   -547.0726051065  2.5318E-02  0.0000E+00  1.5653E-01  1.0000E-04
 mr-sdci # 16  5   -546.9462159656  2.4533E-03  0.0000E+00  2.4894E-01  1.0000E-04
 mr-sdci # 16  6   -546.1407855465  4.4081E-01  0.0000E+00  1.1138E+00  1.0000E-04
 mr-sdci # 16  7   -545.1852776519 -2.4875E-01  0.0000E+00  1.2607E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  17

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.68467598
   ht   3     0.00000000     0.00000000   -72.65777172
   ht   4     0.00000000     0.00000000     0.00000000   -72.53353449
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.45481827
   ht   6     4.97090074     0.29720845    -0.00016768     2.06360816    -1.16268086    -1.13411552
   ht   7    -5.27255472     2.91253368    -0.00001209    -1.09594943     0.67265778     0.50529718    -0.93018046
   ht   8    -3.67129014    -0.10344629     0.00066682     0.12722008    -0.06574385     0.35183138    -0.28481931    -0.39732957

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968806       5.132585E-02  -4.184984E-06   7.143225E-02   7.317852E-02   7.000164E-02  -7.722223E-02  -5.204705E-02
 refs   2   2.974979E-08   5.896724E-05    1.00000       1.980327E-05  -3.709493E-06   1.467867E-06  -2.381506E-06  -8.570056E-06
 refs   3   2.552758E-02   0.811487      -3.914769E-05  -0.485109       0.240206       1.457927E-02  -4.954678E-02   7.326556E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.999989      -4.608548E-02  -4.333281E-04   8.286060E-02  -4.908880E-02   0.329077      -0.359266       -1.20401    
 civs   2  -9.386633E-06   -1.01740       1.076382E-03  -0.122571      -9.511057E-03   0.275895       6.074979E-02   0.419784    
 civs   3   4.379147E-08  -1.037397E-03  -0.999998      -1.649220E-03   2.635727E-04  -8.394016E-04   3.321406E-04   5.518356E-05
 civs   4   6.219036E-06   0.163573       1.009602E-03  -0.857813      -0.316147       0.277551       0.325780      -0.258410    
 civs   5  -1.699262E-06  -6.271569E-02  -4.229803E-04   0.226673      -0.938892      -0.208466      -0.174654       0.163030    
 civs   6  -6.569848E-05   -1.29437      -5.011375E-03    2.53138      -0.861208        5.10331        8.14052       -3.24218    
 civs   7  -7.822868E-05   -1.04412      -8.100667E-03    2.43589      -0.880958        7.56420        2.04030        10.6535    
 civs   8  -2.006251E-04   0.660570       1.343647E-02   -1.71334        1.07210       -10.4761        15.2128        4.17460    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.968807      -4.483540E-02   9.582557E-05  -5.298409E-02  -9.227671E-02   1.828409E-02  -1.782104E-02  -8.544427E-02
 ref    2   1.542720E-08  -1.098994E-03  -0.999998      -1.661675E-03   2.518783E-04  -7.375961E-04   2.195272E-04   8.272162E-06
 ref    3  -2.553688E-02  -0.883496       7.369293E-04   0.276894      -4.218551E-02  -0.329564      -3.081513E-02  -7.009650E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880667    -0.04483540     0.00009583    -0.05298409    -0.09227671     0.01828409    -0.01782104    -0.08544427
 ref:   2     0.00000002    -0.00109899    -0.99999769    -0.00166168     0.00025188    -0.00073760     0.00021953     0.00000827
 ref:   3    -0.02553688    -0.88349631     0.00073693     0.27689406    -0.04218551    -0.32956403    -0.03081513    -0.07009650

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -547.2493511921  1.3127E-10  0.0000E+00  8.7242E-05  1.0000E-04
 mr-sdci # 17  2   -547.1761084993  1.2873E-03  9.4662E-04  3.6683E-02  1.0000E-04
 mr-sdci # 17  3   -547.1387855814  5.0051E-07  0.0000E+00  4.9442E-01  1.0000E-04
 mr-sdci # 17  4   -547.0810325052  8.4274E-03  0.0000E+00  1.1173E-01  1.0000E-04
 mr-sdci # 17  5   -546.9491296635  2.9137E-03  0.0000E+00  2.2515E-01  1.0000E-04
 mr-sdci # 17  6   -546.5423885206  4.0160E-01  0.0000E+00  8.8291E-01  1.0000E-04
 mr-sdci # 17  7   -545.2509920535  6.5714E-02  0.0000E+00  1.2599E+00  1.0000E-04
 mr-sdci # 17  8   -545.1804434958  1.1961E-01  0.0000E+00  1.2581E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  18

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.69509517
   ht   3     0.00000000     0.00000000   -72.65777226
   ht   4     0.00000000     0.00000000     0.00000000   -72.60001918
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46811634
   ht   6    -1.88154791    -1.35117853    -0.00001471     0.12478266     0.10505135    -0.14726975

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.968807      -4.483540E-02   9.582557E-05  -5.298409E-02  -9.227671E-02   2.607844E-02
 refs   2   1.542720E-08  -1.098994E-03  -0.999998      -1.661675E-03   2.518783E-04  -1.935162E-05
 refs   3  -2.553688E-02  -0.883496       7.369293E-04   0.276894      -4.218551E-02  -1.907077E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.999993       1.421554E-02   1.404696E-03  -2.526564E-02   2.333917E-04  -0.805088    
 civs   2   1.530011E-06  -0.989591       2.386701E-03  -2.804038E-02   2.052865E-04  -0.595658    
 civs   3  -2.250098E-07  -1.333094E-03  -0.999996      -1.594957E-03   4.356606E-06  -1.684918E-03
 civs   4   2.155746E-06   8.412687E-03   1.436372E-03  -0.997801      -1.265300E-04   8.434876E-02
 civs   5  -3.800045E-07  -7.630955E-04  -7.474940E-05   1.314444E-03   0.999987       4.542347E-02
 civs   6  -2.708226E-04  -0.549640      -5.431698E-02   0.977031      -9.025793E-03    31.1362    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.968807       4.343161E-02  -3.276648E-04   5.500525E-02  -9.228727E-02   5.005548E-02
 ref    2   2.094637E-07   2.417111E-03   0.999990       3.265213E-03   2.476778E-04   1.608270E-03
 ref    3   2.554113E-02   0.886780      -1.444697E-03  -0.269556      -4.223519E-02  -2.553278E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96880710     0.04343161    -0.00032766     0.05500525    -0.09228727     0.05005548
 ref:   2     0.00000021     0.00241711     0.99999013     0.00326521     0.00024768     0.00160827
 ref:   3     0.02554113     0.88677954    -0.00144470    -0.26955563    -0.04223519    -0.02553278

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -547.2493511922  1.3182E-10  0.0000E+00  8.4107E-05  1.0000E-04
 mr-sdci # 18  2   -547.1766289032  5.2040E-04  4.6436E-04  2.1228E-02  1.0000E-04
 mr-sdci # 18  3   -547.1387905647  4.9833E-06  0.0000E+00  4.9441E-01  1.0000E-04
 mr-sdci # 18  4   -547.0826213827  1.5889E-03  0.0000E+00  1.0068E-01  1.0000E-04
 mr-sdci # 18  5   -546.9491297889  1.2541E-07  0.0000E+00  2.2512E-01  1.0000E-04
 mr-sdci # 18  6   -545.4759737783 -1.0664E+00  0.0000E+00  1.3076E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  19

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.69509517
   ht   3     0.00000000     0.00000000   -72.65777226
   ht   4     0.00000000     0.00000000     0.00000000   -72.60001918
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46811634
   ht   6    -1.88154791    -1.35117853    -0.00001471     0.12478266     0.10505135    -0.14726975
   ht   7    -1.74328569    -0.54818413    -0.00621532     0.01621590     0.04533662    -0.06752279    -0.10804880

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.968807      -4.483540E-02   9.582557E-05  -5.298409E-02  -9.227671E-02   2.607844E-02   2.460163E-02
 refs   2   1.542720E-08  -1.098994E-03  -0.999998      -1.661675E-03   2.518783E-04  -1.935162E-05  -1.153223E-04
 refs   3  -2.553688E-02  -0.883496       7.369293E-04   0.276894      -4.218551E-02  -1.907077E-02  -1.401550E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.999999       8.108631E-04   4.933641E-03  -7.916020E-03  -6.622102E-03  -0.283893        1.01111    
 civs   2  -9.448798E-07  -0.986683      -1.054858E-02   4.832514E-02   2.376799E-03   0.140324       0.601263    
 civs   3  -1.196137E-06  -7.663333E-03   0.999790       1.264163E-02   5.970093E-04   1.446951E-02   1.293433E-03
 civs   4   4.633328E-06   1.920778E-02  -1.042530E-02   0.990562      -4.800568E-03  -0.127943      -6.867175E-02
 civs   5  -6.145251E-07  -1.478285E-03   4.832846E-04  -4.143300E-03  -0.999880       1.235498E-02  -4.768017E-02
 civs   6  -5.134992E-04   -1.12991       0.337082       -2.43370      -0.305344       -18.0984       -25.9212    
 civs   7   5.095547E-04    1.18600      -0.569819        2.95745       0.606003        31.3848       -14.2285    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.968807       4.385304E-02   6.283683E-04  -5.264516E-02   9.294368E-02   2.445147E-02  -6.537875E-02
 ref    2   1.125066E-06   8.600478E-03  -0.999699      -1.463571E-02  -9.074679E-04  -1.767712E-02   2.903861E-04
 ref    3   2.554165E-02   0.882011       8.581214E-03   0.236934       3.625057E-02  -0.247387       0.119721    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96880664     0.04385304     0.00062837    -0.05264516     0.09294368     0.02445147    -0.06537875
 ref:   2     0.00000113     0.00860048    -0.99969910    -0.01463571    -0.00090747    -0.01767712     0.00029039
 ref:   3     0.02554165     0.88201116     0.00858121     0.23693428     0.03625057    -0.24738743     0.11972068

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -547.2493511923  1.1944E-10  0.0000E+00  8.6686E-05  1.0000E-04
 mr-sdci # 19  2   -547.1771800418  5.5114E-04  4.1610E-04  2.1758E-02  1.0000E-04
 mr-sdci # 19  3   -547.1389061555  1.1559E-04  0.0000E+00  4.9392E-01  1.0000E-04
 mr-sdci # 19  4   -547.0854355439  2.8142E-03  0.0000E+00  1.0380E-01  1.0000E-04
 mr-sdci # 19  5   -546.9492010969  7.1308E-05  0.0000E+00  2.2295E-01  1.0000E-04
 mr-sdci # 19  6   -546.7561932409  1.2802E+00  0.0000E+00  7.8054E-01  1.0000E-04
 mr-sdci # 19  7   -545.2151201956 -3.5872E-02  0.0000E+00  1.2617E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  20

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.69509517
   ht   3     0.00000000     0.00000000   -72.65777226
   ht   4     0.00000000     0.00000000     0.00000000   -72.60001918
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46811634
   ht   6    -1.88154791    -1.35117853    -0.00001471     0.12478266     0.10505135    -0.14726975
   ht   7    -1.74328569    -0.54818413    -0.00621532     0.01621590     0.04533662    -0.06752279    -0.10804880
   ht   8     1.55322062    -1.26166065     0.01294672     0.41355157    -0.13963797     0.02732110     0.02155335    -0.09773369

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.968807      -4.483540E-02   9.582557E-05  -5.298409E-02  -9.227671E-02   2.607844E-02   2.460163E-02  -2.278173E-02
 refs   2   1.542720E-08  -1.098994E-03  -0.999998      -1.661675E-03   2.518783E-04  -1.935162E-05  -1.153223E-04   1.542386E-04
 refs   3  -2.553688E-02  -0.883496       7.369293E-04   0.276894      -4.218551E-02  -1.907077E-02  -1.401550E-02  -2.184517E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.999993      -8.780104E-03  -9.847469E-03   1.270767E-02  -7.299911E-03   8.687486E-03    1.12757       0.767174    
 civs   2  -1.038322E-05   0.996264      -6.852170E-03   2.691122E-02   7.659384E-03  -0.111427       0.479697      -0.920434    
 civs   3  -4.045211E-06   2.116133E-02   0.996140       4.812067E-02  -2.612445E-03   6.847345E-02   3.819413E-03   1.742966E-02
 civs   4   8.756332E-06  -2.884632E-02  -3.973386E-02   0.986058       1.852089E-03  -0.143233      -3.157813E-02   0.271766    
 civs   5  -1.260007E-06   2.455813E-03   2.380114E-03  -6.747098E-03  -0.998288      -5.725831E-02  -5.772578E-02  -6.930570E-02
 civs   6  -6.683439E-04    1.43843        1.03339       -3.64498       0.351892       -14.5763       -23.8340        16.2780    
 civs   7   8.404325E-04   -1.81747       -1.96757        5.41135      -0.633865        27.4363       -16.3294       -13.1442    
 civs   8   4.504792E-04  -0.708194       -1.41755        2.25280      -0.627112        13.5417        5.62650        40.9084    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968807      -4.293700E-02   3.586100E-03  -5.376343E-02   9.247417E-02   1.263818E-02  -7.357658E-02  -5.431923E-02
 ref    2   4.011812E-06  -2.213509E-02  -0.996075      -4.999639E-02   2.319061E-03  -6.892054E-02  -1.096449E-03  -9.376627E-03
 ref    3   2.553949E-02  -0.874535       3.477256E-02   0.193710       5.191603E-02  -0.341342       0.101574      -0.148068    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880702    -0.04293700     0.00358610    -0.05376343     0.09247417     0.01263818    -0.07357658    -0.05431923
 ref:   2     0.00000401    -0.02213509    -0.99607518    -0.04999639     0.00231906    -0.06892054    -0.00109645    -0.00937663
 ref:   3     0.02553949    -0.87453535     0.03477256     0.19371036     0.05191603    -0.34134193     0.10157384    -0.14806849

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -547.2493511925  1.4154E-10  0.0000E+00  8.5607E-05  1.0000E-04
 mr-sdci # 20  2   -547.1774748682  2.9483E-04  1.5166E-04  1.5173E-02  1.0000E-04
 mr-sdci # 20  3   -547.1399937821  1.0876E-03  0.0000E+00  4.8807E-01  1.0000E-04
 mr-sdci # 20  4   -547.0878508413  2.4153E-03  0.0000E+00  9.8961E-02  1.0000E-04
 mr-sdci # 20  5   -546.9492098524  8.7555E-06  0.0000E+00  2.2510E-01  1.0000E-04
 mr-sdci # 20  6   -546.9450402739  1.8885E-01  0.0000E+00  3.8239E-01  1.0000E-04
 mr-sdci # 20  7   -545.2185027014  3.3825E-03  0.0000E+00  1.2709E+00  1.0000E-04
 mr-sdci # 20  8   -545.0383931992 -1.4205E-01  0.0000E+00  1.2115E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  21

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.69646154
   ht   3     0.00000000     0.00000000   -72.65898046
   ht   4     0.00000000     0.00000000     0.00000000   -72.60683752
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46819653
   ht   6    -0.56307636    -0.01546589    -0.04824666     0.15805141    -0.01958780    -0.01676760

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968807      -4.293700E-02   3.586100E-03  -5.376343E-02   9.247417E-02  -8.000511E-03
 refs   2   4.011812E-06  -2.213509E-02  -0.996075      -4.999639E-02   2.319061E-03  -3.868144E-04
 refs   3   2.553949E-02  -0.874535       3.477256E-02   0.193710       5.191603E-02   7.386752E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00001       6.224783E-03   5.010981E-02   7.438430E-03  -1.359220E-02  -0.592697    
 civs   2   1.610211E-06  -0.999111       3.853460E-02   1.834664E-03  -1.541119E-03  -2.344407E-02
 civs   3   1.261882E-05  -3.582008E-02  -0.991050       3.209296E-02  -1.682094E-02  -0.132860    
 civs   4   2.960111E-07   8.923460E-04   1.611429E-02   0.997332       7.569105E-03   0.181467    
 civs   5   9.878501E-07  -1.299196E-03  -1.205853E-02  -2.715406E-03   0.999144      -4.439368E-02
 civs   6   8.477658E-04  -0.804759       -6.47768      -0.961458        1.75673        76.5976    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968807       4.301012E-02  -3.912036E-03  -5.334900E-02   9.110739E-02  -5.194200E-02
 ref    2  -1.695721E-05   5.805859E-02   0.987979      -8.150494E-02   1.804810E-02   9.405085E-02
 ref    3  -2.553989E-02   0.872182      -6.917085E-02   0.192044       5.505115E-02   9.017368E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96880664     0.04301012    -0.00391204    -0.05334900     0.09110739    -0.05194200
 ref:   2    -0.00001696     0.05805859     0.98797918    -0.08150494     0.01804810     0.09405085
 ref:   3    -0.02553989     0.87218199    -0.06917085     0.19204372     0.05505115     0.09017368

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -547.2493511926  1.8045E-10  0.0000E+00  8.6142E-05  1.0000E-04
 mr-sdci # 21  2   -547.1775970061  1.2214E-04  2.4617E-04  2.1098E-02  1.0000E-04
 mr-sdci # 21  3   -547.1510447787  1.1051E-02  0.0000E+00  4.5702E-01  1.0000E-04
 mr-sdci # 21  4   -547.0881341647  2.8332E-04  0.0000E+00  1.0344E-01  1.0000E-04
 mr-sdci # 21  5   -546.9499639355  7.5408E-04  0.0000E+00  2.2739E-01  1.0000E-04
 mr-sdci # 21  6   -545.5878790847 -1.3572E+00  0.0000E+00  1.2919E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  22

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.69646154
   ht   3     0.00000000     0.00000000   -72.65898046
   ht   4     0.00000000     0.00000000     0.00000000   -72.60683752
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46819653
   ht   6    -0.56307636    -0.01546589    -0.04824666     0.15805141    -0.01958780    -0.01676760
   ht   7    -0.76860032    -0.27895435    -0.12753743     0.18228334     0.05407566    -0.01019606    -0.02589080

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968807      -4.293700E-02   3.586100E-03  -5.376343E-02   9.247417E-02  -8.000511E-03  -1.094431E-02
 refs   2   4.011812E-06  -2.213509E-02  -0.996075      -4.999639E-02   2.319061E-03  -3.868144E-04  -2.084719E-03
 refs   3   2.553949E-02  -0.874535       3.477256E-02   0.193710       5.191603E-02   7.386752E-04  -5.101008E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -3.463546E-02  -5.068451E-03   1.448110E-02   1.136270E-02  -0.134513       0.806726    
 civs   2   7.556141E-06  -0.106417       0.995996       4.439095E-03   4.930532E-04   0.132790       0.217634    
 civs   3   6.466513E-05  -0.988564      -6.121131E-02  -1.721980E-02   2.321768E-02  -0.176574       4.150732E-02
 civs   4   1.092725E-06  -1.456834E-02  -1.453380E-03   0.995908      -6.307946E-03   5.824397E-02  -0.219796    
 civs   5   9.957244E-07  -5.577689E-03   3.716023E-04  -3.387784E-03  -0.998739      -7.503195E-02  -2.367159E-02
 civs   6   1.742109E-03   -12.9174      -0.118427      -0.916699       -1.87997        72.9543       -29.8968    
 civs   7  -1.106779E-03    12.7361       0.566595      -0.699563       0.301364       -40.7101       -54.4758    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.968807      -1.195010E-03  -4.321525E-02  -5.314817E-02  -9.122208E-02  -2.421505E-02   5.426096E-02
 ref    2  -6.700928E-05   0.966200       3.786273E-02  -3.093267E-02  -2.503924E-02   0.226505       8.990690E-02
 ref    3  -2.553669E-02  -1.981423E-02  -0.876531       0.191522      -5.533209E-02   0.143235       4.370946E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96880687    -0.00119501    -0.04321525    -0.05314817    -0.09122208    -0.02421505     0.05426096
 ref:   2    -0.00006701     0.96620029     0.03786273    -0.03093267    -0.02503924     0.22650465     0.08990690
 ref:   3    -0.02553669    -0.01981423    -0.87653149     0.19152212    -0.05533209     0.14323459     0.04370946

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -547.2493511928  1.8211E-10  0.0000E+00  8.4562E-05  1.0000E-04
 mr-sdci # 22  2   -547.2099850418  3.2388E-02  3.5385E-02  2.4677E-01  1.0000E-04
 mr-sdci # 22  3   -547.1774568643  2.6412E-02  0.0000E+00  1.8692E-02  1.0000E-04
 mr-sdci # 22  4   -547.0884533821  3.1922E-04  0.0000E+00  8.9675E-02  1.0000E-04
 mr-sdci # 22  5   -546.9499999899  3.6054E-05  0.0000E+00  2.2756E-01  1.0000E-04
 mr-sdci # 22  6   -545.8040598000  2.1618E-01  0.0000E+00  1.2582E+00  1.0000E-04
 mr-sdci # 22  7   -545.2057956046 -1.2707E-02  0.0000E+00  1.2598E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  23

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.69646154
   ht   3     0.00000000     0.00000000   -72.65898046
   ht   4     0.00000000     0.00000000     0.00000000   -72.60683752
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46819653
   ht   6    -0.56307636    -0.01546589    -0.04824666     0.15805141    -0.01958780    -0.01676760
   ht   7    -0.76860032    -0.27895435    -0.12753743     0.18228334     0.05407566    -0.01019606    -0.02589080
   ht   8    10.43533140     4.86935186     2.93863687    -0.89992603    -0.88807289     0.10738899     0.19180018    -4.32935050

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968807      -4.293700E-02   3.586100E-03  -5.376343E-02   9.247417E-02  -8.000511E-03  -1.094431E-02   0.152201    
 refs   2   4.011812E-06  -2.213509E-02  -0.996075      -4.999639E-02   2.319061E-03  -3.868144E-04  -2.084719E-03   4.487708E-02
 refs   3   2.553949E-02  -0.874535       3.477256E-02   0.193710       5.191603E-02   7.386752E-04  -5.101008E-03   8.227861E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000       2.357462E-02  -1.502145E-03   6.062160E-03   3.543175E-03   9.777514E-03   0.122910      -0.999091    
 civs   2  -7.770671E-06  -4.183695E-02    1.00025      -2.750704E-04  -4.243854E-03  -0.202389       0.115394      -0.329075    
 civs   3  -9.385893E-05  -0.963761      -3.034609E-02  -4.393070E-02   9.697661E-03   7.826102E-02   0.276867      -0.192905    
 civs   4  -2.583807E-06  -3.646507E-02  -2.572372E-03   0.995032      -9.616994E-03  -7.082988E-02   9.692513E-02   0.195089    
 civs   5  -1.051716E-06  -2.855702E-03   7.591378E-04  -5.148659E-03  -0.997700       9.223981E-02  -2.780076E-02   4.767585E-02
 civs   6  -2.056745E-03   -11.3185       0.341434       -1.44171       -2.23839       -73.4188       0.374506        29.7503    
 civs   7   1.605410E-03    14.7194       0.269863      -0.651508       0.188616        31.5097        51.8773        35.0955    
 civs   8   3.394469E-05   0.638484       2.781476E-02  -8.348839E-02  -8.216775E-02   -1.57251        4.69822       -2.77674    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968807       3.835072E-03  -4.284437E-02  -5.403355E-02  -9.162210E-02   1.503362E-02   1.349876E-02  -6.945326E-02
 ref    2   9.677321E-05   0.965067       8.770346E-03  -7.826387E-03  -1.461338E-02  -0.177578      -0.180697      -1.949928E-02
 ref    3   2.553564E-02  -3.444536E-02  -0.875141       0.186738      -5.889663E-02  -0.173312       5.139594E-02  -8.968370E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880663     0.00383507    -0.04284437    -0.05403355    -0.09162210     0.01503362     0.01349876    -0.06945326
 ref:   2     0.00009677     0.96506662     0.00877035    -0.00782639    -0.01461338    -0.17757797    -0.18069683    -0.01949928
 ref:   3     0.02553564    -0.03444536    -0.87514087     0.18673760    -0.05889663    -0.17331231     0.05139594    -0.08968370

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -547.2493511929  2.7399E-11  0.0000E+00  8.4659E-05  1.0000E-04
 mr-sdci # 23  2   -547.2327385166  2.2753E-02  7.5412E-03  9.1960E-02  1.0000E-04
 mr-sdci # 23  3   -547.1775280192  7.1155E-05  0.0000E+00  6.8889E-03  1.0000E-04
 mr-sdci # 23  4   -547.0888804201  4.2704E-04  0.0000E+00  8.2730E-02  1.0000E-04
 mr-sdci # 23  5   -546.9503489397  3.4895E-04  0.0000E+00  2.2525E-01  1.0000E-04
 mr-sdci # 23  6   -545.8405668076  3.6507E-02  0.0000E+00  1.2799E+00  1.0000E-04
 mr-sdci # 23  7   -545.4133181816  2.0752E-01  0.0000E+00  1.2407E+00  1.0000E-04
 mr-sdci # 23  8   -545.1360006501  9.7607E-02  0.0000E+00  1.3018E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  24

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75172519
   ht   3     0.00000000     0.00000000   -72.69651469
   ht   4     0.00000000     0.00000000     0.00000000   -72.60786709
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46933561
   ht   6    -3.68229755    -0.61890602     2.03944314     0.07532548     0.16398540    -1.04086492

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968807       3.835072E-03  -4.284437E-02  -5.403355E-02  -9.162210E-02  -5.242577E-02
 refs   2   9.677321E-05   0.965067       8.770346E-03  -7.826387E-03  -1.461338E-02   1.561031E-02
 refs   3   2.553564E-02  -3.444536E-02  -0.875141       0.186738      -5.889663E-02   4.970479E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -2.618862E-02  -1.581457E-03  -4.378339E-03   8.917115E-03  -0.479903    
 civs   2   2.531637E-07   0.994143      -4.532473E-03  -5.265858E-03   6.192694E-03  -0.134041    
 civs   3   1.963332E-08   1.860395E-02    1.00086       1.972314E-03  -4.582532E-03   0.262611    
 civs   4  -3.496996E-09  -3.504380E-03  -3.739711E-04  -0.999859      -1.648626E-03   1.900529E-02
 civs   5  -2.622381E-09  -2.545834E-03  -2.118963E-04  -1.084928E-03   0.999422       3.998418E-02
 civs   6   5.682225E-07   0.517536       3.125227E-02   8.652335E-02  -0.176217        9.48368    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968807       1.677630E-03  -4.296538E-02   5.372634E-02  -9.066061E-02  -4.871208E-02
 ref    2  -9.651978E-05   0.967718       4.897498E-03   4.126764E-03  -1.140580E-02   2.020799E-02
 ref    3  -2.553564E-02  -2.597386E-02  -0.874284      -0.184003      -6.390453E-02   0.235120    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96880663     0.00167763    -0.04296538     0.05372634    -0.09066061    -0.04871208
 ref:   2    -0.00009652     0.96771792     0.00489750     0.00412676    -0.01140580     0.02020799
 ref:   3    -0.02553564    -0.02597386    -0.87428374    -0.18400321    -0.06390453     0.23511952

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -547.2493511929  9.9476E-14  0.0000E+00  8.4674E-05  1.0000E-04
 mr-sdci # 24  2   -547.2366472300  3.9087E-03  2.3343E-03  5.3703E-02  1.0000E-04
 mr-sdci # 24  3   -547.1775425907  1.4571E-05  0.0000E+00  5.5419E-03  1.0000E-04
 mr-sdci # 24  4   -547.0889800591  9.9639E-05  0.0000E+00  8.2257E-02  1.0000E-04
 mr-sdci # 24  5   -546.9507099155  3.6098E-04  0.0000E+00  2.2705E-01  1.0000E-04
 mr-sdci # 24  6   -545.9167611600  7.6194E-02  0.0000E+00  1.3714E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  25

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75172519
   ht   3     0.00000000     0.00000000   -72.69651469
   ht   4     0.00000000     0.00000000     0.00000000   -72.60786709
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46933561
   ht   6    -3.68229755    -0.61890602     2.03944314     0.07532548     0.16398540    -1.04086492
   ht   7     1.59260767     0.76411844    -0.04871747    -0.44178323     0.20963407     0.07126927    -0.24513240

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968807       3.835072E-03  -4.284437E-02  -5.403355E-02  -9.162210E-02  -5.242577E-02   2.189833E-02
 refs   2   9.677321E-05   0.965067       8.770346E-03  -7.826387E-03  -1.461338E-02   1.561031E-02  -1.044607E-02
 refs   3   2.553564E-02  -3.444536E-02  -0.875141       0.186738      -5.889663E-02   4.970479E-02   6.830451E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.999999       1.767294E-02  -9.124668E-04  -3.129604E-03   2.877463E-03  -7.905657E-03   0.645071    
 civs   2   6.553103E-05  -0.998667      -7.931806E-03  -8.022263E-03   1.512608E-02  -2.123277E-02   0.221352    
 civs   3   4.671647E-06  -3.227496E-02    1.00160       3.805636E-03  -2.946969E-02   0.151471      -0.208421    
 civs   4  -2.797829E-06   1.342429E-02  -1.274390E-03   -1.00111       6.072146E-03  -5.362406E-02  -9.044406E-02
 civs   5  -5.902674E-07   4.379941E-03  -4.933256E-04  -3.038672E-03   0.970105       0.248763       1.883517E-02
 civs   6   1.387402E-04  -0.918941       6.197860E-02   0.177899       -1.21461        6.04402       -7.17296    
 civs   7   3.458584E-04   -1.31421       0.101579       0.268291       -2.67664        13.6126        12.8893    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.968807      -1.298357E-03  -4.297005E-02   5.375894E-02  -8.561554E-02  -3.757679E-02   4.629246E-02
 ref    2  -3.490693E-05  -0.964847       1.053135E-03   1.450328E-04   9.115450E-03  -7.022773E-02  -3.432733E-02
 ref    3  -2.553320E-02   1.069227E-02  -0.872724      -0.179226      -0.109314       0.236703      -9.524359E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96880665    -0.00129836    -0.04297005     0.05375894    -0.08561554    -0.03757679     0.04629246
 ref:   2    -0.00003491    -0.96484678     0.00105313     0.00014503     0.00911545    -0.07022773    -0.03432733
 ref:   3    -0.02553320     0.01069227    -0.87272370    -0.17922568    -0.10931381     0.23670275    -0.09524359

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -547.2493511930  1.6497E-10  0.0000E+00  8.3000E-05  1.0000E-04
 mr-sdci # 25  2   -547.2397255387  3.0783E-03  1.0921E-03  4.0563E-02  1.0000E-04
 mr-sdci # 25  3   -547.1775592293  1.6639E-05  0.0000E+00  5.5980E-03  1.0000E-04
 mr-sdci # 25  4   -547.0890654141  8.5355E-05  0.0000E+00  8.2699E-02  1.0000E-04
 mr-sdci # 25  5   -546.9551197114  4.4098E-03  0.0000E+00  2.3815E-01  1.0000E-04
 mr-sdci # 25  6   -546.8303808905  9.1362E-01  0.0000E+00  6.1775E-01  1.0000E-04
 mr-sdci # 25  7   -545.1297601237 -2.8356E-01  0.0000E+00  1.3554E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  26

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75172519
   ht   3     0.00000000     0.00000000   -72.69651469
   ht   4     0.00000000     0.00000000     0.00000000   -72.60786709
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.46933561
   ht   6    -3.68229755    -0.61890602     2.03944314     0.07532548     0.16398540    -1.04086492
   ht   7     1.59260767     0.76411844    -0.04871747    -0.44178323     0.20963407     0.07126927    -0.24513240
   ht   8     1.10187626     1.12880624     0.61791159    -0.38417773    -0.03901691     0.10399857    -0.02507223    -0.12428153

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968807       3.835072E-03  -4.284437E-02  -5.403355E-02  -9.162210E-02  -5.242577E-02   2.189833E-02   1.626068E-02
 refs   2   9.677321E-05   0.965067       8.770346E-03  -7.826387E-03  -1.461338E-02   1.561031E-02  -1.044607E-02  -1.769793E-02
 refs   3   2.553564E-02  -3.444536E-02  -0.875141       0.186738      -5.889663E-02   4.970479E-02   6.830451E-03   7.271948E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -2.584604E-02  -1.677631E-03  -6.226397E-03   6.716762E-02  -7.001793E-02   8.342371E-02   0.741652    
 civs   2   7.931606E-05   0.987298      -1.010721E-02  -1.410335E-02   0.123648      -0.113292      -0.238912       0.452669    
 civs   3   4.623964E-06   2.976786E-02    1.00128       2.992544E-03  -4.426060E-02   4.408996E-02  -0.400358       3.478833E-02
 civs   4  -2.770923E-06  -1.236285E-02  -1.269891E-03   -1.00034      -8.436248E-03   2.887574E-04   9.147564E-02  -0.179293    
 civs   5  -6.886902E-07  -4.573943E-03  -5.732772E-04  -4.449223E-03   0.679285       0.735850       9.438804E-05   2.175607E-02
 civs   6   1.727582E-04    1.01316       7.261491E-02   0.246046       -3.71747        3.78357       -7.17146       -3.72278    
 civs   7   4.307856E-04    1.62425       0.135775       0.460935       -9.13552        9.43196        3.38720        13.2794    
 civs   8  -1.417205E-04  -0.674095      -6.442261E-02  -0.255247        5.21763       -5.61331       -23.3523        17.3443    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.968806       1.291456E-04  -4.307295E-02   5.335358E-02  -4.480274E-02  -8.501431E-02   8.834805E-04   5.741912E-02
 ref    2  -1.945081E-05   0.964009      -9.898746E-05  -2.147712E-03   5.414441E-02  -5.983023E-02   3.116897E-02  -6.547171E-02
 ref    3  -2.553244E-02  -6.206670E-03  -0.872095      -0.175309      -0.214626       0.131911      -0.125330      -3.006924E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880643     0.00012915    -0.04307295     0.05335358    -0.04480274    -0.08501431     0.00088348     0.05741912
 ref:   2    -0.00001945     0.96400902    -0.00009899    -0.00214771     0.05414441    -0.05983023     0.03116897    -0.06547171
 ref:   3    -0.02553244    -0.00620667    -0.87209513    -0.17530950    -0.21462612     0.13191087    -0.12533047    -0.03006924

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -547.2493511930  3.0496E-11  0.0000E+00  8.3438E-05  1.0000E-04
 mr-sdci # 26  2   -547.2404621439  7.3661E-04  2.8622E-04  2.1718E-02  1.0000E-04
 mr-sdci # 26  3   -547.1775653440  6.1147E-06  0.0000E+00  5.3282E-03  1.0000E-04
 mr-sdci # 26  4   -547.0891401949  7.4781E-05  0.0000E+00  8.3689E-02  1.0000E-04
 mr-sdci # 26  5   -546.9718611914  1.6741E-02  0.0000E+00  2.9426E-01  1.0000E-04
 mr-sdci # 26  6   -546.9276564963  9.7276E-02  0.0000E+00  3.1157E-01  1.0000E-04
 mr-sdci # 26  7   -545.2639826606  1.3422E-01  0.0000E+00  1.2839E+00  1.0000E-04
 mr-sdci # 26  8   -545.0563061642 -7.9694E-02  0.0000E+00  1.3741E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  27

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75944882
   ht   3     0.00000000     0.00000000   -72.69655202
   ht   4     0.00000000     0.00000000     0.00000000   -72.60812687
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.49084787
   ht   6    -0.40660437    -0.34874376     0.30488179     0.15092943     0.16073529    -0.02458720

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.968806       1.291456E-04  -4.307295E-02   5.335358E-02  -4.480274E-02   5.834163E-03
 refs   2  -1.945081E-05   0.964009      -9.898746E-05  -2.147712E-03   5.414441E-02   5.271362E-03
 refs   3  -2.553244E-02  -6.206670E-03  -0.872095      -0.175309      -0.214626       6.738989E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.999998      -3.915007E-03  -7.289101E-04   4.444847E-03  -4.298951E-02  -0.341799    
 civs   2  -8.213057E-06   0.996559      -1.218076E-03   5.315655E-03  -4.579975E-02  -0.303229    
 civs   3  -1.465919E-06   3.486474E-03    1.00054      -2.884636E-03   3.016268E-02   0.254713    
 civs   4  -1.114197E-06   2.664490E-03   6.581328E-04   -1.00137      -5.316682E-03   0.117043    
 civs   5   2.516532E-06  -6.108841E-03  -1.576508E-03   1.813524E-02  -0.975168       0.258254    
 civs   6  -2.996355E-04   0.698912       0.130411      -0.795369        7.69301        61.1696    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.968807       6.790655E-04  -4.293608E-02  -5.444860E-02   4.533516E-02   9.400199E-03
 ref    2   1.009262E-05   0.964039      -6.725930E-04   4.064446E-03  -5.638937E-02   4.384444E-02
 ref    3   2.553136E-02  -3.571918E-03  -0.871441       0.168667       0.237149       0.124749    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96880667     0.00067907    -0.04293608    -0.05444860     0.04533516     0.00940020
 ref:   2     0.00001009     0.96403890    -0.00067259     0.00406445    -0.05638937     0.04384444
 ref:   3     0.02553136    -0.00357192    -0.87144052     0.16866736     0.23714884     0.12474888

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -547.2493511931  3.6181E-11  0.0000E+00  8.2855E-05  1.0000E-04
 mr-sdci # 27  2   -547.2406622064  2.0006E-04  1.0829E-04  1.2404E-02  1.0000E-04
 mr-sdci # 27  3   -547.1775718762  6.5322E-06  0.0000E+00  4.4274E-03  1.0000E-04
 mr-sdci # 27  4   -547.0893487514  2.0856E-04  0.0000E+00  8.2003E-02  1.0000E-04
 mr-sdci # 27  5   -546.9946210436  2.2760E-02  0.0000E+00  2.3185E-01  1.0000E-04
 mr-sdci # 27  6   -545.5369244345 -1.3907E+00  0.0000E+00  1.2639E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  28

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75944882
   ht   3     0.00000000     0.00000000   -72.69655202
   ht   4     0.00000000     0.00000000     0.00000000   -72.60812687
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.49084787
   ht   6    -0.40660437    -0.34874376     0.30488179     0.15092943     0.16073529    -0.02458720
   ht   7     0.38268660    -0.11643144    -0.11943075    -0.08439023     0.09803476     0.00398125    -0.01086083

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.968806       1.291456E-04  -4.307295E-02   5.335358E-02  -4.480274E-02   5.834163E-03  -5.699463E-03
 refs   2  -1.945081E-05   0.964009      -9.898746E-05  -2.147712E-03   5.414441E-02   5.271362E-03   1.740537E-03
 refs   3  -2.553244E-02  -6.206670E-03  -0.872095      -0.175309      -0.214626       6.738989E-03  -7.616357E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -1.394450E-03  -1.759234E-04   1.575308E-03  -4.690939E-03   0.121255       0.543726    
 civs   2  -1.333147E-05   0.993696      -2.196591E-03   1.041857E-02  -9.136712E-02  -0.346873       0.105055    
 civs   3  -1.623378E-06   3.715317E-03    1.00054      -2.902513E-03   2.704546E-02   7.146998E-02  -0.269042    
 civs   4  -1.230672E-06   2.834075E-03   7.945236E-04   -1.00060      -2.740656E-02  -1.405557E-03  -0.149259    
 civs   5   4.079650E-06  -8.381633E-03  -2.426391E-03   3.030975E-02  -0.933771       0.404333      -6.298399E-02
 civs   6  -5.197894E-04    1.00272       0.203785       -1.29964        11.9953        45.7383       -40.8620    
 civs   7  -5.729022E-04   0.803456       0.183150       -1.08154        11.8544        71.6552        59.9741    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.968806       4.148244E-04  -4.297086E-02  -5.450896E-02   3.707044E-02  -4.539204E-02  -4.698990E-02
 ref    2   3.085692E-06   0.964156      -9.566477E-04   5.100636E-03  -5.471636E-02   5.331886E-02  -1.281110E-02
 ref    3   2.553021E-02  -1.924613E-03  -0.870934       0.163401       0.254124       0.103849      -6.126721E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96880648     0.00041482    -0.04297086    -0.05450896     0.03707044    -0.04539204    -0.04698990
 ref:   2     0.00000309     0.96415585    -0.00095665     0.00510064    -0.05471636     0.05331886    -0.01281110
 ref:   3     0.02553021    -0.00192461    -0.87093450     0.16340086     0.25412434     0.10384950    -0.06126721

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -547.2493511931  4.4210E-11  0.0000E+00  8.3063E-05  1.0000E-04
 mr-sdci # 28  2   -547.2407492142  8.7008E-05  2.0985E-05  5.6899E-03  1.0000E-04
 mr-sdci # 28  3   -547.1775760466  4.1704E-06  0.0000E+00  3.8441E-03  1.0000E-04
 mr-sdci # 28  4   -547.0894702063  1.2145E-04  0.0000E+00  8.1481E-02  1.0000E-04
 mr-sdci # 28  5   -547.0107982197  1.6177E-02  0.0000E+00  2.2003E-01  1.0000E-04
 mr-sdci # 28  6   -546.2456259936  7.0870E-01  0.0000E+00  9.8805E-01  1.0000E-04
 mr-sdci # 28  7   -545.0469773480 -2.1701E-01  0.0000E+00  1.3012E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  29

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75944882
   ht   3     0.00000000     0.00000000   -72.69655202
   ht   4     0.00000000     0.00000000     0.00000000   -72.60812687
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.49084787
   ht   6    -0.40660437    -0.34874376     0.30488179     0.15092943     0.16073529    -0.02458720
   ht   7     0.38268660    -0.11643144    -0.11943075    -0.08439023     0.09803476     0.00398125    -0.01086083
   ht   8    -0.13685017     0.08869303     0.01003781     0.03887015     0.01756776    -0.00180220     0.00065387    -0.00187368

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.968806       1.291456E-04  -4.307295E-02   5.335358E-02  -4.480274E-02   5.834163E-03  -5.699463E-03   1.814051E-03
 refs   2  -1.945081E-05   0.964009      -9.898746E-05  -2.147712E-03   5.414441E-02   5.271362E-03   1.740537E-03  -1.370453E-03
 refs   3  -2.553244E-02  -6.206670E-03  -0.872095      -0.175309      -0.214626       6.738989E-03  -7.616357E-04   6.643598E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.999999      -2.527378E-03  -3.706292E-04   1.870091E-03  -3.214017E-02   7.647750E-02  -0.111233      -0.664603    
 civs   2  -1.495689E-05   0.994123      -2.191608E-03   1.045439E-02  -8.357649E-02   9.690661E-02   0.495813       0.124864    
 civs   3  -1.714771E-06   3.862646E-03    1.00056      -2.951827E-03   2.899863E-02  -3.825830E-02  -0.170854       0.217137    
 civs   4  -1.446107E-06   3.173467E-03   8.538405E-04   -1.00066      -1.910900E-02  -4.694726E-02   1.045525E-02   0.173224    
 civs   5   4.378962E-06  -8.891067E-03  -2.606079E-03   3.120766E-02  -0.889267      -0.466323      -0.176191      -6.882005E-03
 civs   6  -5.606975E-04    1.06792       0.217840       -1.33327        13.9556       -27.9470       -52.7879        20.4355    
 civs   7  -6.799496E-04   0.975928       0.216870       -1.14775        17.2449       -56.2177       -15.3097       -72.6644    
 civs   8  -5.543320E-04   0.890258       0.156048      -0.241957        23.8440       -114.833        173.178        89.4787    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968806       3.642350E-04  -4.297632E-02  -5.452239E-02   3.281177E-02   4.318996E-02   1.549495E-03   5.203343E-02
 ref    2   1.893163E-06   0.963963      -1.042776E-03   5.222991E-03  -5.777537E-02  -1.952079E-02  -7.381833E-02  -2.176134E-02
 ref    3   2.552970E-02  -1.077515E-03  -0.870748       0.162917       0.267013      -8.268166E-02  -4.427781E-02   5.044348E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880645     0.00036424    -0.04297632    -0.05452239     0.03281177     0.04318996     0.00154950     0.05203343
 ref:   2     0.00000189     0.96396306    -0.00104278     0.00522299    -0.05777537    -0.01952079    -0.07381833    -0.02176134
 ref:   3     0.02552970    -0.00107751    -0.87074774     0.16291681     0.26701314    -0.08268166    -0.04427781     0.05044348

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -547.2493511931  7.3612E-12  0.0000E+00  8.2569E-05  1.0000E-04
 mr-sdci # 29  2   -547.2407678967  1.8682E-05  1.1063E-05  3.7522E-03  1.0000E-04
 mr-sdci # 29  3   -547.1775765701  5.2355E-07  0.0000E+00  3.7683E-03  1.0000E-04
 mr-sdci # 29  4   -547.0894712394  1.0331E-06  0.0000E+00  8.1432E-02  1.0000E-04
 mr-sdci # 29  5   -547.0211179549  1.0320E-02  0.0000E+00  2.0916E-01  1.0000E-04
 mr-sdci # 29  6   -546.6290889203  3.8346E-01  0.0000E+00  7.2218E-01  1.0000E-04
 mr-sdci # 29  7   -545.1989339698  1.5196E-01  0.0000E+00  1.1834E+00  1.0000E-04
 mr-sdci # 29  8   -545.0081085798 -4.8198E-02  0.0000E+00  1.3381E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  30

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75975457
   ht   3     0.00000000     0.00000000   -72.69656324
   ht   4     0.00000000     0.00000000     0.00000000   -72.60845791
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.54010463
   ht   6    -0.13107337     0.03373661     0.08722461     0.01160097    -0.04622715    -0.00138093

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968806       3.642350E-04  -4.297632E-02  -5.452239E-02   3.281177E-02  -1.933810E-03
 refs   2   1.893163E-06   0.963963      -1.042776E-03   5.222991E-03  -5.777537E-02  -5.541211E-04
 refs   3   2.552970E-02  -1.077515E-03  -0.870748       0.162917       0.267013       1.869011E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -9.584041E-04  -2.178067E-04  -7.146130E-04   3.544110E-02  -0.481048    
 civs   2  -6.515802E-08    1.00024       3.491578E-05   1.550038E-04  -8.101670E-03   0.121954    
 civs   3  -4.583801E-08   6.584229E-04    1.00014       4.653198E-04  -2.329517E-02   0.320015    
 civs   4  -4.335510E-09   6.075077E-05   9.860624E-06  -0.999933      -5.355818E-03   4.396721E-02
 civs   5  -3.573430E-08   5.445330E-04   2.046237E-04   1.859966E-03  -0.984854      -0.242332    
 civs   6  -3.717551E-05   0.532053       0.120920       0.396732       -19.6759        267.065    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968806       2.502013E-04  -4.299919E-02   5.448496E-02  -2.731074E-02  -7.446689E-02
 ref    2  -1.933283E-06   0.963872      -1.088045E-03  -5.401010E-03   5.998980E-02  -1.653106E-02
 ref    3  -2.552973E-02  -5.258586E-04  -0.870597      -0.162091      -0.279418       0.150539    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96880646     0.00025020    -0.04299919     0.05448496    -0.02731074    -0.07446689
 ref:   2    -0.00000193     0.96387202    -0.00108804    -0.00540101     0.05998980    -0.01653106
 ref:   3    -0.02552973    -0.00052586    -0.87059731    -0.16209141    -0.27941819     0.15053894

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -547.2493511931 -5.6843E-14  0.0000E+00  8.2550E-05  1.0000E-04
 mr-sdci # 30  2   -547.2407737828  5.8861E-06  3.0244E-06  1.9694E-03  1.0000E-04
 mr-sdci # 30  3   -547.1775768578  2.8761E-07  0.0000E+00  3.7521E-03  1.0000E-04
 mr-sdci # 30  4   -547.0894739705  2.7311E-06  0.0000E+00  8.1492E-02  1.0000E-04
 mr-sdci # 30  5   -547.0282932069  7.1753E-03  0.0000E+00  1.8968E-01  1.0000E-04
 mr-sdci # 30  6   -545.6992997697 -9.2979E-01  0.0000E+00  1.3864E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  31

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75975457
   ht   3     0.00000000     0.00000000   -72.69656324
   ht   4     0.00000000     0.00000000     0.00000000   -72.60845791
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.54010463
   ht   6    -0.13107337     0.03373661     0.08722461     0.01160097    -0.04622715    -0.00138093
   ht   7     0.05061595     0.01447687    -0.00656514    -0.01363149    -0.00378701     0.00010738    -0.00028445

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968806       3.642350E-04  -4.297632E-02  -5.452239E-02   3.281177E-02  -1.933810E-03   6.249456E-04
 refs   2   1.893163E-06   0.963963      -1.042776E-03   5.222991E-03  -5.777537E-02  -5.541211E-04  -2.506360E-04
 refs   3   2.552970E-02  -1.077515E-03  -0.870748       0.162917       0.267013       1.869011E-03  -4.515148E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000       7.169103E-04  -1.452109E-04  -5.187448E-04  -2.525062E-02  -7.578815E-03   0.599844    
 civs   2   4.753571E-07   -1.00063       1.508194E-04   7.340984E-04   2.856822E-02   0.159393      -2.795093E-02
 civs   3   1.965753E-07  -9.579978E-04    1.00025       9.772358E-04   3.989407E-02   0.162657      -0.275608    
 civs   4  -9.375066E-08   1.323528E-04  -6.168403E-05   -1.00015       1.384149E-04  -4.921366E-02  -9.557883E-02
 civs   5   1.611915E-07  -8.121832E-04   3.819090E-04   4.458731E-03   0.955394      -0.306943       0.151358    
 civs   6   2.022675E-04  -0.857322       0.239248       0.974103        38.6846        168.402       -204.890    
 civs   7   6.073308E-04   -1.18980       0.410801        1.77676        63.8755        425.193        331.795    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.968807      -1.373696E-04  -4.303633E-02   5.436453E-02   1.920949E-02  -6.691212E-02   4.445016E-02
 ref    2   2.077082E-06  -0.963745      -1.155573E-03  -5.759863E-03  -6.514595E-02  -2.892802E-02  -5.525214E-03
 ref    3   2.552993E-02   1.383934E-04  -0.870431      -0.160804       0.291726       8.085045E-02  -0.104267    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96880652    -0.00013737    -0.04303633     0.05436453     0.01920949    -0.06691212     0.04445016
 ref:   2     0.00000208    -0.96374475    -0.00115557    -0.00575986    -0.06514595    -0.02892802    -0.00552521
 ref:   3     0.02552993     0.00013839    -0.87043134    -0.16080361     0.29172590     0.08085045    -0.10426693

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -547.2493511931  9.5213E-13  0.0000E+00  8.2734E-05  1.0000E-04
 mr-sdci # 31  2   -547.2407773813  3.5985E-06  1.4878E-06  1.4719E-03  1.0000E-04
 mr-sdci # 31  3   -547.1775772373  3.7959E-07  0.0000E+00  3.7434E-03  1.0000E-04
 mr-sdci # 31  4   -547.0894794945  5.5240E-06  0.0000E+00  8.1179E-02  1.0000E-04
 mr-sdci # 31  5   -547.0353903727  7.0972E-03  0.0000E+00  1.8557E-01  1.0000E-04
 mr-sdci # 31  6   -546.6789315209  9.7963E-01  0.0000E+00  8.1341E-01  1.0000E-04
 mr-sdci # 31  7   -545.1124939769 -8.6440E-02  0.0000E+00  1.3527E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  32

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75975457
   ht   3     0.00000000     0.00000000   -72.69656324
   ht   4     0.00000000     0.00000000     0.00000000   -72.60845791
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.54010463
   ht   6    -0.13107337     0.03373661     0.08722461     0.01160097    -0.04622715    -0.00138093
   ht   7     0.05061595     0.01447687    -0.00656514    -0.01363149    -0.00378701     0.00010738    -0.00028445
   ht   8     0.04640351     0.00729703     0.02973309    -0.00866715    -0.00248950     0.00011705    -0.00001235    -0.00015813

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968806       3.642350E-04  -4.297632E-02  -5.452239E-02   3.281177E-02  -1.933810E-03   6.249456E-04   7.199913E-04
 refs   2   1.893163E-06   0.963963      -1.042776E-03   5.222991E-03  -5.777537E-02  -5.541211E-04  -2.506360E-04  -1.074577E-04
 refs   3   2.552970E-02  -1.077515E-03  -0.870748       0.162917       0.267013       1.869011E-03  -4.515148E-06   4.333055E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.999999      -1.096406E-03   3.699398E-04   1.331792E-03   5.603664E-02  -0.137334      -0.204723      -0.711552    
 civs   2  -9.461629E-07    1.00066      -1.763002E-04   2.461171E-04  -3.686939E-02   0.104287       0.129771      -0.113568    
 civs   3   4.010005E-07   7.597602E-04   -1.00014       1.586564E-03  -2.748361E-02   1.153791E-02   0.484112      -0.164352    
 civs   4  -8.715837E-08  -7.073807E-05   1.923106E-05   -1.00022       4.384864E-03  -2.189162E-02   6.582550E-03   0.144685    
 civs   5  -3.329729E-07   8.766494E-04  -4.690337E-04   1.184783E-03  -0.917850      -0.386317      -0.207132       9.315848E-03
 civs   6  -4.758431E-04   0.952136      -0.308535       0.147836       -52.0372        128.594        233.289        41.4892    
 civs   7  -1.616609E-03    1.53663      -0.647313      -0.702860       -108.004        376.045       -123.393       -362.358    
 civs   8   2.089010E-03  -0.705026       0.414641        3.27259        58.6940       -262.306        472.513       -603.384    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.968807       3.809749E-05   4.309810E-02   5.484628E-02  -8.083947E-03  -8.141627E-02  -1.762218E-02  -5.232296E-02
 ref    2  -2.342488E-06   0.963715       1.188825E-03  -5.314442E-03   6.713762E-02  -1.459863E-02  -1.252818E-02   2.358037E-02
 ref    3  -2.553010E-02  -7.810487E-05   0.870363      -0.162287      -0.290300       4.603094E-03   0.160179      -3.114478E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880681     0.00003810     0.04309810     0.05484628    -0.00808395    -0.08141627    -0.01762218    -0.05232296
 ref:   2    -0.00000234     0.96371455     0.00118882    -0.00531444     0.06713762    -0.01459863    -0.01252818     0.02358037
 ref:   3    -0.02553010    -0.00007810     0.87036335    -0.16228732    -0.29029993     0.00460309     0.16017913    -0.03114478

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -547.2493511931  9.3650E-12  0.0000E+00  8.2432E-05  1.0000E-04
 mr-sdci # 32  2   -547.2407784303  1.0490E-06  4.1715E-07  8.6604E-04  1.0000E-04
 mr-sdci # 32  3   -547.1775775625  3.2514E-07  0.0000E+00  3.7514E-03  1.0000E-04
 mr-sdci # 32  4   -547.0894952964  1.5802E-05  0.0000E+00  8.0926E-02  1.0000E-04
 mr-sdci # 32  5   -547.0401618286  4.7715E-03  0.0000E+00  1.8745E-01  1.0000E-04
 mr-sdci # 32  6   -546.8734073670  1.9448E-01  0.0000E+00  4.4898E-01  1.0000E-04
 mr-sdci # 32  7   -545.2097804912  9.7287E-02  0.0000E+00  1.3011E+00  1.0000E-04
 mr-sdci # 32  8   -544.9566293673 -5.1479E-02  0.0000E+00  1.3618E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  33

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976510
   ht   3     0.00000000     0.00000000   -72.69656424
   ht   4     0.00000000     0.00000000     0.00000000   -72.60848197
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.55914850
   ht   6    -0.01479445    -0.01068168    -0.00644915     0.00722276     0.00175436    -0.00003106

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.968807       3.809749E-05   4.309810E-02   5.484628E-02  -8.083947E-03   1.962574E-04
 refs   2  -2.342488E-06   0.963715       1.188825E-03  -5.314442E-03   6.713762E-02   1.638533E-04
 refs   3  -2.553010E-02  -7.810487E-05   0.870363      -0.162287      -0.290300       1.299677E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -1.392880E-04  -1.287935E-04  -1.327474E-03  -1.581943E-02  -0.341272    
 civs   2  -5.295802E-07   0.999899      -9.720020E-05  -9.766256E-04  -1.158805E-02  -0.246824    
 civs   3  -5.064148E-07  -6.489576E-05   -1.00006      -5.520263E-04  -6.690019E-03  -0.148556    
 civs   4   4.204672E-07   5.264868E-05   3.826870E-05  -0.999327       1.355148E-02   0.170521    
 civs   5  -9.724111E-07  -1.296723E-04  -1.816314E-04  -5.486403E-03  -0.997075       8.592871E-02
 civs   6   5.396076E-03   0.686404       0.633630        6.53001        77.8159        1678.61    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.968807       3.900130E-05  -4.309738E-02  -5.479336E-02   8.460761E-03   1.057713E-03
 ref    2   2.648169E-06   0.963721      -1.191140E-03   5.070645E-03  -6.543832E-02   4.186471E-02
 ref    3   2.553060E-02  -1.271391E-05  -0.870280       0.164173       0.291947       4.498058E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96880681     0.00003900    -0.04309738    -0.05479336     0.00846076     0.00105771
 ref:   2     0.00000265     0.96372076    -0.00119114     0.00507065    -0.06543832     0.04186471
 ref:   3     0.02553060    -0.00001271    -0.87028003     0.16417304     0.29194702     0.04498058

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -547.2493511932  1.7735E-11  0.0000E+00  8.2592E-05  1.0000E-04
 mr-sdci # 33  2   -547.2407787166  2.8633E-07  1.3077E-07  4.2892E-04  1.0000E-04
 mr-sdci # 33  3   -547.1775777958  2.3332E-07  0.0000E+00  3.7118E-03  1.0000E-04
 mr-sdci # 33  4   -547.0895177473  2.2451E-05  0.0000E+00  8.0697E-02  1.0000E-04
 mr-sdci # 33  5   -547.0434829236  3.3211E-03  0.0000E+00  1.7016E-01  1.0000E-04
 mr-sdci # 33  6   -545.4954947204 -1.3779E+00  0.0000E+00  1.2504E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  34

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976510
   ht   3     0.00000000     0.00000000   -72.69656424
   ht   4     0.00000000     0.00000000     0.00000000   -72.60848197
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.55914850
   ht   6    -0.01479445    -0.01068168    -0.00644915     0.00722276     0.00175436    -0.00003106
   ht   7     0.01381502    -0.00428840     0.00745564    -0.00324014     0.00013428     0.00000624    -0.00001393

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.968807       3.809749E-05   4.309810E-02   5.484628E-02  -8.083947E-03   1.962574E-04  -2.128707E-04
 refs   2  -2.342488E-06   0.963715       1.188825E-03  -5.314442E-03   6.713762E-02   1.638533E-04   6.704270E-05
 refs   3  -2.553010E-02  -7.810487E-05   0.870363      -0.162287      -0.290300       1.299677E-04  -1.180817E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -4.737871E-05  -4.883453E-05  -9.921266E-04  -3.486918E-03   0.145512       0.540345    
 civs   2  -1.061814E-06   0.999823      -1.670684E-04  -1.291329E-03  -2.243519E-02  -0.315925       6.915723E-02
 civs   3  -7.049958E-08  -1.169438E-05   -1.00001      -3.461460E-04   7.574631E-04   0.106229       0.267889    
 civs   4   3.286970E-07   4.138655E-05   2.774733E-05  -0.999347       1.337871E-02   3.528244E-02  -0.187812    
 civs   5  -1.326860E-06  -1.754087E-04  -2.451993E-04  -6.375494E-03  -0.994776       9.930630E-02  -4.754702E-02
 civs   6   7.251609E-03   0.914626       0.841684        7.52024        110.909        1287.42       -1127.52    
 civs   7   5.940497E-03   0.728037       0.643922        2.82671        100.396        2145.13        1638.73    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.968807       1.989821E-05  -4.311416E-02  -5.486091E-02   5.824528E-03  -5.729872E-02  -4.500017E-02
 ref    2   2.814757E-06   0.963730      -1.185367E-03   5.059783E-03  -6.357459E-02   5.690787E-02  -1.011222E-02
 ref    3   2.553062E-02  -9.951161E-06  -0.870269       0.164400       0.289922      -3.176487E-02  -7.640225E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96880696     0.00001990    -0.04311416    -0.05486091     0.00582453    -0.05729872    -0.04500017
 ref:   2     0.00000281     0.96373030    -0.00118537     0.00505978    -0.06357459     0.05690787    -0.01011222
 ref:   3     0.02553062    -0.00000995    -0.87026942     0.16439995     0.28992180    -0.03176487    -0.07640225

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -547.2493511932  6.3949E-12  0.0000E+00  8.2292E-05  1.0000E-04
 mr-sdci # 34  2   -547.2407788118  9.5203E-08  2.4956E-08  1.8877E-04  1.0000E-04
 mr-sdci # 34  3   -547.1775778661  7.0340E-08  0.0000E+00  3.7383E-03  1.0000E-04
 mr-sdci # 34  4   -547.0895189708  1.2234E-06  0.0000E+00  8.0642E-02  1.0000E-04
 mr-sdci # 34  5   -547.0450113904  1.5285E-03  0.0000E+00  1.6945E-01  1.0000E-04
 mr-sdci # 34  6   -546.1597377334  6.6424E-01  0.0000E+00  1.1470E+00  1.0000E-04
 mr-sdci # 34  7   -545.1082153638 -1.0157E-01  0.0000E+00  1.2748E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  35

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976510
   ht   3     0.00000000     0.00000000   -72.69656424
   ht   4     0.00000000     0.00000000     0.00000000   -72.60848197
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.55914850
   ht   6    -0.01479445    -0.01068168    -0.00644915     0.00722276     0.00175436    -0.00003106
   ht   7     0.01381502    -0.00428840     0.00745564    -0.00324014     0.00013428     0.00000624    -0.00001393
   ht   8    -0.00498040     0.00176573     0.00017864     0.00088888    -0.00065763    -0.00000267     0.00000061    -0.00000234

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.968807       3.809749E-05   4.309810E-02   5.484628E-02  -8.083947E-03   1.962574E-04  -2.128707E-04   6.377068E-05
 refs   2  -2.342488E-06   0.963715       1.188825E-03  -5.314442E-03   6.713762E-02   1.638533E-04   6.704270E-05  -2.538427E-05
 refs   3  -2.553010E-02  -7.810487E-05   0.870363      -0.162287      -0.290300       1.299677E-04  -1.180817E-04  -7.224379E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -8.680668E-05  -1.012396E-04  -1.635656E-03  -1.061901E-02   9.177858E-02   0.280329       0.629237    
 civs   2  -9.085146E-07   0.999826      -1.654103E-04  -1.340667E-03  -2.302120E-02   0.115466       0.247661      -0.308138    
 civs   3   2.665583E-07   2.799637E-06  -0.999987      -4.772344E-05   4.275566E-03  -0.107107       0.211993       0.164231    
 civs   4   4.941267E-07   4.840673E-05   3.576092E-05  -0.999164       1.661349E-02  -5.093626E-02  -0.158292      -0.106014    
 civs   5  -1.693491E-06  -1.917332E-04  -2.771178E-04  -7.588206E-03  -0.993665      -7.682206E-02  -9.251981E-02   6.952716E-02
 civs   6   8.468136E-03   0.967866       0.926081        8.91139        126.635       -734.441       -1558.69        469.157    
 civs   7   9.858561E-03   0.897875       0.895020        6.50383        143.549       -1588.61        664.441        2119.48    
 civs   8   2.075243E-02   0.889935        1.21165        15.4710        177.205       -3566.00        2377.33       -4708.23    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968807       1.388900E-05  -4.312294E-02  -5.497476E-02   4.435532E-03   4.875451E-02  -2.294447E-02  -4.904657E-02
 ref    2   2.872627E-06   0.963730      -1.186023E-03   5.011945E-03  -6.310612E-02  -3.006368E-02  -3.764226E-02   4.695127E-02
 ref    3   2.553056E-02  -1.229452E-05  -0.870269       0.164633       0.287987       5.288831E-02  -6.833077E-02  -3.136094E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880710     0.00001389    -0.04312294    -0.05497476     0.00443553     0.04875451    -0.02294447    -0.04904657
 ref:   2     0.00000287     0.96373027    -0.00118602     0.00501195    -0.06310612    -0.03006368    -0.03764226     0.04695127
 ref:   3     0.02553056    -0.00001229    -0.87026897     0.16463319     0.28798658     0.05288831    -0.06833077    -0.03136094

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -547.2493511932  1.2250E-11  0.0000E+00  8.2668E-05  1.0000E-04
 mr-sdci # 35  2   -547.2407788340  2.2209E-08  1.5995E-08  1.4922E-04  1.0000E-04
 mr-sdci # 35  3   -547.1775779037  3.7554E-08  0.0000E+00  3.7274E-03  1.0000E-04
 mr-sdci # 35  4   -547.0895241903  5.2195E-06  0.0000E+00  8.0819E-02  1.0000E-04
 mr-sdci # 35  5   -547.0456468109  6.3542E-04  0.0000E+00  1.6727E-01  1.0000E-04
 mr-sdci # 35  6   -546.6901017136  5.3036E-01  0.0000E+00  7.6370E-01  1.0000E-04
 mr-sdci # 35  7   -545.1492211640  4.1006E-02  0.0000E+00  1.1852E+00  1.0000E-04
 mr-sdci # 35  8   -544.9562612241 -3.6814E-04  0.0000E+00  1.3156E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  36

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976551
   ht   3     0.00000000     0.00000000   -72.69656458
   ht   4     0.00000000     0.00000000     0.00000000   -72.60851086
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.56463348
   ht   6    -0.00463030     0.00109855     0.00294825     0.00034843    -0.00139547    -0.00000175

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968807       1.388900E-05  -4.312294E-02  -5.497476E-02   4.435532E-03  -6.882578E-05
 refs   2   2.872627E-06   0.963730      -1.186023E-03   5.011945E-03  -6.310612E-02  -1.598618E-05
 refs   3   2.553056E-02  -1.229452E-05  -0.870269       0.164633       0.287987       5.718232E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.999999      -3.452883E-05   6.437274E-05   1.063983E-03   9.762860E-03  -0.472222    
 civs   2  -3.347718E-07    1.00001      -1.501663E-05  -2.506826E-04  -2.303844E-03   0.111977    
 civs   3  -8.082003E-07   2.222953E-05   -1.00004      -6.726651E-04  -6.188744E-03   0.300840    
 civs   4  -1.494849E-07   4.191257E-06  -9.946512E-06   -1.00008       8.329809E-04   3.343696E-02
 civs   5  -9.065053E-09   7.165792E-07  -1.126967E-05  -1.203289E-03  -0.996843      -0.162979    
 civs   6  -1.973383E-02   0.542062       -1.01153       -16.7202       -153.423        7421.25    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968807       8.847214E-06   4.313246E-02   5.512263E-02  -3.099350E-03  -6.881360E-02
 ref    2  -2.879003E-06   0.963729       1.188432E-03  -4.909891E-03   6.315081E-02  -6.273113E-04
 ref    3  -2.553098E-02  -6.290801E-07   0.870244      -0.165336      -0.290078       0.109064    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96880729     0.00000885     0.04313246     0.05512263    -0.00309935    -0.06881360
 ref:   2    -0.00000288     0.96372944     0.00118843    -0.00490989     0.06315081    -0.00062731
 ref:   3    -0.02553098    -0.00000063     0.87024359    -0.16533587    -0.29007825     0.10906383

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -547.2493511932  1.1525E-11  0.0000E+00  8.2172E-05  1.0000E-04
 mr-sdci # 36  2   -547.2407788427  8.6701E-09  0.0000E+00  7.5389E-05  1.0000E-04
 mr-sdci # 36  3   -547.1775779327  2.8977E-08  1.0425E-05  3.7255E-03  1.0000E-04
 mr-sdci # 36  4   -547.0895315934  7.4031E-06  0.0000E+00  8.0694E-02  1.0000E-04
 mr-sdci # 36  5   -547.0462605931  6.1378E-04  0.0000E+00  1.6837E-01  1.0000E-04
 mr-sdci # 36  6   -545.6097770230 -1.0803E+00  0.0000E+00  1.3829E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  37

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976551
   ht   3     0.00000000     0.00000000   -72.69656458
   ht   4     0.00000000     0.00000000     0.00000000   -72.60851086
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.56463348
   ht   6    -0.00463030     0.00109855     0.00294825     0.00034843    -0.00139547    -0.00000175
   ht   7     0.03754921    -0.16183011     0.00910900     0.00600805     0.04312443     0.00000714    -0.00123935

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968807       1.388900E-05  -4.312294E-02  -5.497476E-02   4.435532E-03  -6.882578E-05   5.760816E-04
 refs   2   2.872627E-06   0.963730      -1.186023E-03   5.011945E-03  -6.310612E-02  -1.598618E-05   2.106928E-03
 refs   3   2.553056E-02  -1.229452E-05  -0.870269       0.164633       0.287987       5.718232E-05  -2.215322E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000       3.498150E-05   4.989032E-04   6.912067E-05   1.475273E-02   0.231331       0.430125    
 civs   2  -7.915988E-06   -1.00001      -1.796293E-03   3.669722E-03  -2.265743E-02  -0.654661       4.177061E-02
 civs   3   6.836946E-07  -2.227534E-05  -0.999952      -1.042035E-03  -4.499471E-03  -1.517818E-02  -0.305516    
 civs   4   9.368041E-07  -4.003938E-06   2.446056E-04   -1.00020      -2.910239E-03   1.204327E-02  -3.771178E-02
 civs   5   3.099343E-07  -6.667568E-07  -1.723270E-04   2.018932E-03  -0.990812       0.230111       0.114843    
 civs   6   1.849181E-02  -0.542358       -1.37416       -15.2927       -157.913       -1321.44       -7314.33    
 civs   7   3.698915E-03   8.506535E-04   0.798487       -1.75305        9.12094        285.378       -68.4011    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.968807      -8.783475E-06   4.317785E-02   5.501514E-02  -2.210758E-03   3.223942E-02   6.305935E-02
 ref    2   2.725814E-06  -0.963729       1.171249E-03  -5.051546E-03   6.242306E-02  -2.296392E-02   5.994755E-03
 ref    3   2.553046E-02   5.203421E-07   0.869975      -0.163662      -0.292578      -5.140873E-02  -9.937062E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96880703    -0.00000878     0.04317785     0.05501514    -0.00221076     0.03223942     0.06305935
 ref:   2     0.00000273    -0.96372947     0.00117125    -0.00505155     0.06242306    -0.02296392     0.00599476
 ref:   3     0.02553046     0.00000052     0.86997496    -0.16366156    -0.29257754    -0.05140873    -0.09937062

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -547.2493511934  1.9052E-10  0.0000E+00  8.1147E-05  1.0000E-04
 mr-sdci # 37  2   -547.2407788427  9.9902E-12  0.0000E+00  7.5296E-05  1.0000E-04
 mr-sdci # 37  3   -547.1775862569  8.3242E-06  8.4154E-06  2.4843E-03  1.0000E-04
 mr-sdci # 37  4   -547.0895680469  3.6453E-05  0.0000E+00  8.0403E-02  1.0000E-04
 mr-sdci # 37  5   -547.0472297041  9.6911E-04  0.0000E+00  1.6571E-01  1.0000E-04
 mr-sdci # 37  6   -546.0617246436  4.5195E-01  0.0000E+00  1.1733E+00  1.0000E-04
 mr-sdci # 37  7   -545.5838215726  4.3460E-01  0.0000E+00  1.3741E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  38

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976551
   ht   3     0.00000000     0.00000000   -72.69656458
   ht   4     0.00000000     0.00000000     0.00000000   -72.60851086
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.56463348
   ht   6    -0.00463030     0.00109855     0.00294825     0.00034843    -0.00139547    -0.00000175
   ht   7     0.03754921    -0.16183011     0.00910900     0.00600805     0.04312443     0.00000714    -0.00123935
   ht   8    -0.01586916     0.46763390    -0.00454068     0.00380437    -0.02142699    -0.00000701     0.00096050    -0.00424313

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968807       1.388900E-05  -4.312294E-02  -5.497476E-02   4.435532E-03  -6.882578E-05   5.760816E-04  -2.227851E-04
 refs   2   2.872627E-06   0.963730      -1.186023E-03   5.011945E-03  -6.310612E-02  -1.598618E-05   2.106928E-03  -7.054585E-03
 refs   3   2.553056E-02  -1.229452E-05  -0.870269       0.164633       0.287987       5.718232E-05  -2.215322E-04  -2.203825E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000       3.524571E-05   7.314695E-04  -8.018913E-04   2.249609E-02   8.279868E-02   0.471925      -0.121983    
 civs   2   1.151172E-05   -1.00000       2.196944E-03  -7.000157E-03   8.156036E-02   0.629066      -0.252746        1.60796    
 civs   3   2.162832E-07  -2.242787E-05  -0.999939      -1.257747E-03  -2.658597E-03   6.969197E-03  -0.300255      -5.924710E-02
 civs   4   1.745360E-06  -3.765038E-06   4.913898E-04   -1.00029      -1.082146E-02   7.756877E-03  -3.548576E-02  -8.903769E-03
 civs   5  -1.706293E-06  -1.277801E-06  -9.199414E-04   9.845848E-03  -0.973389       0.222753       0.162392      -0.180325    
 civs   6   1.478422E-02  -0.543445       -2.34021       -11.1136       -186.570       -314.939       -7412.64       -479.582    
 civs   7   6.766520E-03   1.736934E-03    1.49692       -3.95509        30.6339        191.339       -10.2846       -221.769    
 civs   8   4.094040E-03   1.172748E-03   0.865246       -2.43193        23.7271        164.830       -25.4817        174.565    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968807      -8.724486E-06   4.321124E-02   5.489377E-02  -1.987914E-04   1.523473E-02   6.834415E-02  -1.319501E-02
 ref    2  -7.785241E-07  -0.963730       4.510854E-04  -3.378666E-03   4.011910E-02  -0.162412       2.294616E-02  -0.130024    
 ref    3   2.552865E-02   6.147291E-09   0.869394      -0.159995      -0.301902      -3.525428E-02  -0.101699      -2.173599E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880685    -0.00000872     0.04321124     0.05489377    -0.00019879     0.01523473     0.06834415    -0.01319501
 ref:   2    -0.00000078    -0.96373047     0.00045109    -0.00337867     0.04011910    -0.16241151     0.02294616    -0.13002396
 ref:   3     0.02552865     0.00000001     0.86939442    -0.15999505    -0.30190156    -0.03525428    -0.10169874    -0.02173599

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -547.2493511936  1.9533E-10  0.0000E+00  7.9940E-05  1.0000E-04
 mr-sdci # 38  2   -547.2407788427  1.5675E-11  0.0000E+00  7.5122E-05  1.0000E-04
 mr-sdci # 38  3   -547.1775935383  7.2815E-06  2.6250E-06  2.0442E-03  1.0000E-04
 mr-sdci # 38  4   -547.0896091387  4.1092E-05  0.0000E+00  8.0606E-02  1.0000E-04
 mr-sdci # 38  5   -547.0508891689  3.6595E-03  0.0000E+00  1.5283E-01  1.0000E-04
 mr-sdci # 38  6   -546.8495239954  7.8780E-01  0.0000E+00  5.1172E-01  1.0000E-04
 mr-sdci # 38  7   -545.5969475700  1.3126E-02  0.0000E+00  1.3812E+00  1.0000E-04
 mr-sdci # 38  8   -545.1565628658  2.0030E-01  0.0000E+00  1.0692E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  39

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976552
   ht   3     0.00000000     0.00000000   -72.69658021
   ht   4     0.00000000     0.00000000     0.00000000   -72.60859581
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.56987584
   ht   6    -0.01869068     0.13847774     0.00173352     0.00524357     0.00775928    -0.00044637

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968807      -8.724486E-06   4.321124E-02   5.489377E-02  -1.987914E-04  -2.805816E-04
 refs   2  -7.785241E-07  -0.963730       4.510854E-04  -3.378666E-03   4.011910E-02   1.870888E-03
 refs   3   2.552865E-02   6.147291E-09   0.869394      -0.159995      -0.301902       2.863489E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.999999      -6.208330E-07   1.896963E-04  -7.300257E-04   1.604199E-03  -0.163076    
 civs   2  -4.341617E-06    1.00000      -1.404857E-03   5.408049E-03  -1.188450E-02    1.20833    
 civs   3  -1.378539E-07   1.575323E-07   -1.00002      -1.694203E-05  -1.963073E-05   1.400139E-02
 civs   4  -2.923549E-08   2.278285E-08   2.621254E-05  -0.999784      -1.981941E-03   5.022782E-02
 civs   5  -4.774733E-07   5.153530E-07  -1.973311E-04   1.792444E-03   -1.00062       5.821896E-02
 civs   6  -2.279947E-03   2.407767E-03  -0.738203        2.84162       -6.24458        634.890    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968807      -8.790679E-06  -4.318713E-02  -5.497310E-02   2.873284E-04  -1.680901E-02
 ref    2   6.780285E-07  -0.963730      -4.862897E-04   3.554284E-03  -4.036673E-02   2.547532E-02
 ref    3  -2.552862E-02  -2.508045E-08  -0.869351       0.159394       0.302412      -1.578498E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96880692    -0.00000879    -0.04318713    -0.05497310     0.00028733    -0.01680901
 ref:   2     0.00000068    -0.96373036    -0.00048629     0.00355428    -0.04036673     0.02547532
 ref:   3    -0.02552862    -0.00000003    -0.86935104     0.15939413     0.30241181    -0.01578498

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -547.2493511936  1.9540E-11  0.0000E+00  7.9999E-05  1.0000E-04
 mr-sdci # 39  2   -547.2407788427  2.1529E-11  0.0000E+00  7.5136E-05  1.0000E-04
 mr-sdci # 39  3   -547.1775954761  1.9378E-06  1.0760E-06  9.2523E-04  1.0000E-04
 mr-sdci # 39  4   -547.0896360403  2.6902E-05  0.0000E+00  8.0547E-02  1.0000E-04
 mr-sdci # 39  5   -547.0510158600  1.2669E-04  0.0000E+00  1.5301E-01  1.0000E-04
 mr-sdci # 39  6   -545.7421973909 -1.1073E+00  0.0000E+00  1.2300E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  40

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976552
   ht   3     0.00000000     0.00000000   -72.69658021
   ht   4     0.00000000     0.00000000     0.00000000   -72.60859581
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.56987584
   ht   6    -0.01869068     0.13847774     0.00173352     0.00524357     0.00775928    -0.00044637
   ht   7    -0.01152802     0.15528545    -0.00805928     0.00410858     0.00115162    -0.00028848    -0.00049589

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.968807      -8.724486E-06   4.321124E-02   5.489377E-02  -1.987914E-04  -2.805816E-04  -1.654389E-04
 refs   2  -7.785241E-07  -0.963730       4.510854E-04  -3.378666E-03   4.011910E-02   1.870888E-03   2.354457E-03
 refs   3   2.552865E-02   6.147291E-09   0.869394      -0.159995      -0.301902       2.863489E-06   2.303368E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -5.433976E-07   1.481434E-04   5.573350E-04  -5.135497E-04   9.104363E-04  -0.199060    
 civs   2   2.055646E-06    1.00000      -3.439405E-04  -4.174897E-04  -2.095122E-02   0.516353        1.84514    
 civs   3  -7.531456E-07   3.416194E-07   -1.00011      -4.010611E-04   2.910901E-03  -6.654418E-02  -3.063323E-02
 civs   4   2.106288E-07  -4.966411E-08   8.299600E-05   0.999837       4.378161E-03  -2.470045E-03   6.328682E-02
 civs   5  -1.176554E-06   7.297546E-07  -3.777401E-04  -4.530686E-03    1.00030       2.108392E-02   6.125944E-02
 civs   6  -4.055729E-03   2.934339E-03   -1.05933       -4.55096        17.8875       -339.416        537.262    
 civs   7   4.581565E-03  -1.349743E-03   0.783451        3.86265       -25.7679        544.616        385.442    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.968807      -8.786194E-06  -4.318726E-02   5.496636E-02  -9.091829E-05   1.231326E-03  -1.953989E-02
 ref    2   1.948463E-06  -0.963731      -2.723848E-04  -2.577613E-03   3.310527E-02   0.150465       0.136673    
 ref    3  -2.552793E-02  -2.255841E-07  -0.869208      -0.158059      -0.306060       6.067317E-02   2.998601E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.96880691    -0.00000879    -0.04318726     0.05496636    -0.00009092     0.00123133    -0.01953989
 ref:   2     0.00000195    -0.96373073    -0.00027238    -0.00257761     0.03310527     0.15046492     0.13667325
 ref:   3    -0.02552793    -0.00000023    -0.86920802    -0.15805901    -0.30606042     0.06067317     0.02998601

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -547.2493511936  3.2998E-11  0.0000E+00  7.9729E-05  1.0000E-04
 mr-sdci # 40  2   -547.2407788427  2.8422E-12  0.0000E+00  7.5221E-05  1.0000E-04
 mr-sdci # 40  3   -547.1775963191  8.4297E-07  6.8208E-07  9.1333E-04  1.0000E-04
 mr-sdci # 40  4   -547.0896526795  1.6639E-05  0.0000E+00  8.0681E-02  1.0000E-04
 mr-sdci # 40  5   -547.0516981830  6.8232E-04  0.0000E+00  1.5586E-01  1.0000E-04
 mr-sdci # 40  6   -546.7180617031  9.7586E-01  0.0000E+00  8.1439E-01  1.0000E-04
 mr-sdci # 40  7   -545.2542336784 -3.4271E-01  0.0000E+00  1.1372E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  41

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976552
   ht   3     0.00000000     0.00000000   -72.69658021
   ht   4     0.00000000     0.00000000     0.00000000   -72.60859581
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.56987584
   ht   6    -0.01869068     0.13847774     0.00173352     0.00524357     0.00775928    -0.00044637
   ht   7    -0.01152802     0.15528545    -0.00805928     0.00410858     0.00115162    -0.00028848    -0.00049589
   ht   8    -0.00002172    -0.06879627    -0.00220392     0.00378946     0.01164071     0.00015525     0.00014115    -0.00012209

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.968807      -8.724486E-06   4.321124E-02   5.489377E-02  -1.987914E-04  -2.805816E-04  -1.654389E-04  -3.719420E-06
 refs   2  -7.785241E-07  -0.963730       4.510854E-04  -3.378666E-03   4.011910E-02   1.870888E-03   2.354457E-03  -9.514616E-04
 refs   3   2.552865E-02   6.147291E-09   0.869394      -0.159995      -0.301902       2.863489E-06   2.303368E-04   1.276133E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -3.868830E-07   1.111164E-04  -3.012398E-04  -2.962923E-03   1.456209E-02  -0.202054      -1.353354E-02
 civs   2   1.307521E-06    1.00000      -4.389533E-04  -1.194215E-04   3.386995E-02  -0.163594        1.71600       -1.18536    
 civs   3  -1.200831E-06   8.310025E-07   -1.00022       1.295870E-03  -1.582711E-02   6.952831E-02  -3.039911E-02   1.907565E-03
 civs   4   5.463808E-07  -4.128353E-07   1.831300E-04   -1.00015      -7.238288E-03  -1.399239E-02   7.063581E-02   5.838122E-02
 civs   5  -1.038364E-06   6.153105E-07  -4.421891E-04   9.529484E-03  -0.961245      -0.280800       8.463486E-02   0.193330    
 civs   6  -4.942683E-03   3.908542E-03   -1.34290        7.34271       -58.5438        242.866        567.874        217.488    
 civs   7   6.947818E-03  -3.920136E-03    1.47528       -9.99285        113.457       -484.955        354.464       -269.243    
 civs   8   4.345115E-03  -4.640535E-03    1.09134       -7.64993        102.435       -432.765        128.276        1083.69    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.968807      -8.767439E-06  -4.318970E-02  -5.493458E-02  -7.448832E-04   1.882182E-03  -2.017086E-02  -4.140032E-03
 ref    2   2.451301E-06  -0.963731      -1.238164E-04   1.365432E-03  -1.104970E-02  -0.129198       0.124329      -0.108193    
 ref    3  -2.552733E-02  -8.849251E-07  -0.869006       0.155004       0.316562      -1.840234E-02   3.120208E-02   1.050565E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880689    -0.00000877    -0.04318970    -0.05493458    -0.00074488     0.00188218    -0.02017086    -0.00414003
 ref:   2     0.00000245    -0.96373128    -0.00012382     0.00136543    -0.01104970    -0.12919842     0.12432877    -0.10819337
 ref:   3    -0.02552733    -0.00000088    -0.86900566     0.15500395     0.31656155    -0.01840234     0.03120208     0.01050565

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -547.2493511936  1.4509E-11  0.0000E+00  7.9398E-05  1.0000E-04
 mr-sdci # 41  2   -547.2407788427  1.6186E-11  0.0000E+00  7.5087E-05  1.0000E-04
 mr-sdci # 41  3   -547.1775970635  7.4439E-07  2.5039E-07  6.3734E-04  1.0000E-04
 mr-sdci # 41  4   -547.0896755566  2.2877E-05  0.0000E+00  8.0581E-02  1.0000E-04
 mr-sdci # 41  5   -547.0551020779  3.4039E-03  0.0000E+00  1.5929E-01  1.0000E-04
 mr-sdci # 41  6   -546.9755171638  2.5746E-01  0.0000E+00  3.5593E-01  1.0000E-04
 mr-sdci # 41  7   -545.2557528208  1.5191E-03  0.0000E+00  1.1335E+00  1.0000E-04
 mr-sdci # 41  8   -545.1469458052 -9.6171E-03  0.0000E+00  9.6467E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  42

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976552
   ht   3     0.00000000     0.00000000   -72.69658374
   ht   4     0.00000000     0.00000000     0.00000000   -72.60866223
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.57408875
   ht   6    -0.00138648     0.02525289    -0.00445453     0.00348829     0.00005540    -0.00002743

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.968807      -8.767439E-06  -4.318970E-02  -5.493458E-02  -7.448832E-04   2.287173E-05
 refs   2   2.451301E-06  -0.963731      -1.238164E-04   1.365432E-03  -1.104970E-02   3.960872E-04
 refs   3  -2.552733E-02  -8.849251E-07  -0.869006       0.155004       0.316562      -7.167611E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       1.364085E-08   1.283726E-05  -9.181337E-05   1.139653E-03  -3.767249E-02
 civs   2   1.286344E-06    1.00000      -2.340919E-04   1.673259E-03  -2.076795E-02   0.686268    
 civs   3  -2.141781E-07   4.139115E-08  -0.999959      -3.091269E-04   3.789916E-03  -0.121491    
 civs   4   1.399383E-07  -2.684668E-08  -1.981275E-05  -0.999761      -5.815296E-03   9.729882E-02
 civs   5   3.966417E-07  -8.081988E-08  -1.140379E-04   2.874561E-03  -0.999601      -2.818487E-02
 civs   6   3.706619E-03  -7.222437E-04  -0.674468        4.82104       -59.8373        1977.31    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.968807      -8.770993E-06   4.318610E-02   5.495399E-02   6.360641E-04   8.644148E-03
 ref    2  -2.227013E-06  -0.963731       8.349810E-05  -1.099849E-03   7.350822E-03   0.122267    
 ref    3   2.552740E-02  -8.992204E-07   0.868979      -0.154132      -0.316370      -2.902859E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96880687    -0.00000877     0.04318610     0.05495399     0.00063606     0.00864415
 ref:   2    -0.00000223    -0.96373132     0.00008350    -0.00109985     0.00735082     0.12226673
 ref:   3     0.02552740    -0.00000090     0.86897853    -0.15413157    -0.31637018    -0.02902859

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -547.2493511937  5.2580E-12  0.0000E+00  7.9297E-05  1.0000E-04
 mr-sdci # 42  2   -547.2407788427  1.1369E-13  0.0000E+00  7.5092E-05  1.0000E-04
 mr-sdci # 42  3   -547.1775972324  1.6888E-07  6.2827E-08  2.9519E-04  1.0000E-04
 mr-sdci # 42  4   -547.0896834595  7.9029E-06  0.0000E+00  8.0541E-02  1.0000E-04
 mr-sdci # 42  5   -547.0563342803  1.2322E-03  0.0000E+00  1.5043E-01  1.0000E-04
 mr-sdci # 42  6   -545.7099066321 -1.2656E+00  0.0000E+00  1.1993E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  43

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976552
   ht   3     0.00000000     0.00000000   -72.69658374
   ht   4     0.00000000     0.00000000     0.00000000   -72.60866223
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.57408875
   ht   6    -0.00138648     0.02525289    -0.00445453     0.00348829     0.00005540    -0.00002743
   ht   7     0.00237288     0.01306534    -0.00213248     0.00156594     0.00209934    -0.00000617    -0.00000765

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.968807      -8.767439E-06  -4.318970E-02  -5.493458E-02  -7.448832E-04   2.287173E-05  -3.154079E-05
 refs   2   2.451301E-06  -0.963731      -1.238164E-04   1.365432E-03  -1.104970E-02   3.960872E-04   1.733018E-04
 refs   3  -2.552733E-02  -8.849251E-07  -0.869006       0.155004       0.316562      -7.167611E-05  -4.358405E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       9.339148E-07   4.041354E-05  -2.214437E-04   4.326430E-03   0.121466      -5.919990E-02
 civs   2   1.033690E-06    1.00000      -1.659025E-04   1.378111E-03  -1.326051E-02  -3.754297E-02  -0.891666    
 civs   3  -1.737108E-07  -3.395452E-07  -0.999969      -2.677800E-04   2.711623E-03   1.369028E-02   0.151553    
 civs   4   1.100781E-07   2.542131E-07  -1.081911E-05  -0.999788      -5.759458E-03  -1.678795E-02  -0.116406    
 civs   5   4.030607E-07  -1.791852E-07  -1.303656E-04   3.432383E-03  -0.996662       0.121635      -6.184670E-02
 civs   6   4.345141E-03  -6.733683E-03  -0.859547        5.74733       -82.0522       -1562.92       -1251.51    
 civs   7  -2.637314E-03   2.473064E-02   0.737609       -3.43428        84.7512        2811.80       -2546.69    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.968807      -8.795876E-06   4.318528E-02   5.495715E-02   5.834729E-04  -6.515087E-03  -5.749816E-03
 ref    2  -2.187780E-06  -0.963732       7.249748E-05  -1.049887E-03   5.971933E-03  -9.695021E-02  -7.722083E-02
 ref    3   2.552743E-02  -1.226237E-06   0.868964      -0.153908      -0.316677       1.037977E-02   3.288789E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.96880687    -0.00000880     0.04318528     0.05495715     0.00058347    -0.00651509    -0.00574982
 ref:   2    -0.00000219    -0.96373169     0.00007250    -0.00104989     0.00597193    -0.09695021    -0.07722083
 ref:   3     0.02552743    -0.00000123     0.86896408    -0.15390839    -0.31667712     0.01037977     0.03288789

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -547.2493511937  6.5370E-13  0.0000E+00  7.9393E-05  1.0000E-04
 mr-sdci # 43  2   -547.2407788428  5.5167E-11  0.0000E+00  7.3869E-05  1.0000E-04
 mr-sdci # 43  3   -547.1775972787  4.6342E-08  2.7991E-08  1.5837E-04  1.0000E-04
 mr-sdci # 43  4   -547.0896843739  9.1436E-07  0.0000E+00  8.0459E-02  1.0000E-04
 mr-sdci # 43  5   -547.0568822208  5.4794E-04  0.0000E+00  1.4505E-01  1.0000E-04
 mr-sdci # 43  6   -546.2179582487  5.0805E-01  0.0000E+00  1.1369E+00  1.0000E-04
 mr-sdci # 43  7   -545.2932841251  3.7531E-02  0.0000E+00  1.2357E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  44

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976552
   ht   3     0.00000000     0.00000000   -72.69658374
   ht   4     0.00000000     0.00000000     0.00000000   -72.60866223
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.57408875
   ht   6    -0.00138648     0.02525289    -0.00445453     0.00348829     0.00005540    -0.00002743
   ht   7     0.00237288     0.01306534    -0.00213248     0.00156594     0.00209934    -0.00000617    -0.00000765
   ht   8    -0.00886307    -0.01948362    -0.00043073    -0.00034488     0.00102995     0.00000902     0.00000341    -0.00000983

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.968807      -8.767439E-06  -4.318970E-02  -5.493458E-02  -7.448832E-04   2.287173E-05  -3.154079E-05   1.273744E-04
 refs   2   2.451301E-06  -0.963731      -1.238164E-04   1.365432E-03  -1.104970E-02   3.960872E-04   1.733018E-04  -2.971067E-04
 refs   3  -2.552733E-02  -8.849251E-07  -0.869006       0.155004       0.316562      -7.167611E-05  -4.358405E-05  -1.685121E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       2.806313E-06  -3.961228E-05  -3.353097E-04  -7.372142E-03  -0.251119       0.192799      -0.537732    
 civs   2   1.576207E-06    1.00001      -3.383471E-04   1.125941E-03  -3.913561E-02  -0.625407      -0.417712       -1.22891    
 civs   3  -1.436919E-07  -1.182907E-07  -0.999978      -2.805046E-04   1.362124E-03  -3.116936E-02   0.157199       2.221723E-02
 civs   4   1.125335E-07   2.727607E-07  -1.186225E-05  -0.999789      -5.767440E-03  -5.673488E-03  -0.108124      -4.589833E-02
 civs   5   3.640825E-07  -4.552053E-07  -1.241268E-04   3.367182E-03  -0.991854       0.167191      -3.757258E-02  -4.238167E-02
 civs   6   4.567267E-03  -5.065501E-03  -0.944632        5.58765       -98.8789       -791.567       -1831.30        685.843    
 civs   7  -3.614889E-03   1.747859E-02    1.07671       -2.87554        143.979        2174.49       -1198.47       -2905.63    
 civs   8  -2.392852E-03  -1.757371E-02   0.761120        1.10943        114.537        2767.73       -1617.33        3529.73    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.968807      -8.963839E-06   4.319250E-02   5.496756E-02   1.641305E-03   2.410232E-02  -2.412399E-02   3.757525E-02
 ref    2  -2.080691E-06  -0.963731       3.754941E-05  -1.102181E-03   4.252841E-04  -0.158127      -4.972163E-02  -9.586500E-02
 ref    3   2.552745E-02  -1.058169E-06   0.868954      -0.153947      -0.316990       8.679896E-04   4.056691E-02  -8.109686E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96880689    -0.00000896     0.04319250     0.05496756     0.00164130     0.02410232    -0.02412399     0.03757525
 ref:   2    -0.00000208    -0.96373090     0.00003755    -0.00110218     0.00042528    -0.15812692    -0.04972163    -0.09586500
 ref:   3     0.02552745    -0.00000106     0.86895440    -0.15394693    -0.31699024     0.00086799     0.04056691    -0.00810969

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -547.2493511937  1.9895E-13  0.0000E+00  7.9418E-05  1.0000E-04
 mr-sdci # 44  2   -547.2407788428  1.2790E-11  0.0000E+00  7.3422E-05  1.0000E-04
 mr-sdci # 44  3   -547.1775973000  2.1305E-08  1.3956E-08  1.4224E-04  1.0000E-04
 mr-sdci # 44  4   -547.0896844104  3.6531E-08  0.0000E+00  8.0463E-02  1.0000E-04
 mr-sdci # 44  5   -547.0572404949  3.5827E-04  0.0000E+00  1.4522E-01  1.0000E-04
 mr-sdci # 44  6   -546.7926871071  5.7473E-01  0.0000E+00  6.5447E-01  1.0000E-04
 mr-sdci # 44  7   -545.3613270168  6.8043E-02  0.0000E+00  1.1495E+00  1.0000E-04
 mr-sdci # 44  8   -545.0072589548 -1.3969E-01  0.0000E+00  1.1230E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009999
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  45

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       73884 2x:        9339 4x:         909
All internal counts: zz :       66424 yy:      268506 xx:           0 ww:           0
One-external counts: yz :       94914 yx:           0 yw:           0
Two-external counts: yy :       32712 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -72.76833787
   ht   2     0.00000000   -72.75976552
   ht   3     0.00000000     0.00000000   -72.69658397
   ht   4     0.00000000     0.00000000     0.00000000   -72.60867108
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -72.57622717
   ht   6    -0.00814320     0.00817733    -0.00152044    -0.00057059     0.00132282    -0.00000302

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.968807      -8.963839E-06   4.319250E-02   5.496756E-02   1.641305E-03  -1.161404E-04
 refs   2  -2.080691E-06  -0.963731       3.754941E-05  -1.102181E-03   4.252841E-04   1.089065E-04
 refs   3   2.552745E-02  -1.058169E-06   0.868954      -0.153947      -0.316990       3.291130E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00001      -7.287741E-06   7.212500E-05   2.360827E-03   6.014941E-03  -0.891100    
 civs   2   5.833834E-06    1.00001      -7.243140E-05  -2.370951E-03  -6.040762E-03   0.894938    
 civs   3  -1.073893E-06  -1.349318E-06  -0.999987       4.446053E-04   1.130460E-03  -0.166624    
 civs   4  -2.683628E-07  -3.277263E-07   1.930546E-06  -0.999831       1.130872E-03  -6.517422E-02
 civs   5   1.232130E-06   1.566960E-06  -1.745041E-05  -1.075758E-03   -1.00096       0.138503    
 civs   6   5.182846E-02   6.520411E-02  -0.644610       -21.0979       -53.7530        7962.99    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968806      -9.550043E-06  -4.318685E-02  -5.477766E-02  -1.116263E-03  -7.208183E-02
 ref    2   2.103696E-06  -0.963731      -3.795642E-05   1.088805E-03  -4.592876E-04   4.869213E-03
 ref    3  -2.552718E-02  -7.170159E-07  -0.868957       0.154014       0.316486       6.066463E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96880643    -0.00000955    -0.04318685    -0.05477766    -0.00111626    -0.07208183
 ref:   2     0.00000210    -0.96373086    -0.00003796     0.00108880    -0.00045929     0.00486921
 ref:   3    -0.02552718    -0.00000072    -0.86895682     0.15401410     0.31648645     0.06066463

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -547.2493511937  6.1235E-11  0.0000E+00  7.8905E-05  1.0000E-04
 mr-sdci # 45  2   -547.2407788429  9.6321E-11  0.0000E+00  7.1895E-05  1.0000E-04
 mr-sdci # 45  3   -547.1775973090  8.9964E-09  7.5153E-09  8.1351E-05  1.0000E-04
 mr-sdci # 45  4   -547.0896934202  9.0098E-06  0.0000E+00  8.0320E-02  1.0000E-04
 mr-sdci # 45  5   -547.0572976224  5.7127E-05  0.0000E+00  1.4448E-01  1.0000E-04
 mr-sdci # 45  6   -545.8038813788 -9.8881E-01  0.0000E+00  1.2586E+00  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after 45 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -547.2493511937  6.1235E-11  0.0000E+00  7.8905E-05  1.0000E-04
 mr-sdci # 45  2   -547.2407788429  9.6321E-11  0.0000E+00  7.1895E-05  1.0000E-04
 mr-sdci # 45  3   -547.1775973090  8.9964E-09  7.5153E-09  8.1351E-05  1.0000E-04
 mr-sdci # 45  4   -547.0896934202  9.0098E-06  0.0000E+00  8.0320E-02  1.0000E-04
 mr-sdci # 45  5   -547.0572976224  5.7127E-05  0.0000E+00  1.4448E-01  1.0000E-04
 mr-sdci # 45  6   -545.8038813788 -9.8881E-01  0.0000E+00  1.2586E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -547.249351193714
   ci vector at position   2 energy= -547.240778842914
   ci vector at position   3 energy= -547.177597309011

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    3 of the   7 expansion vectors are transformed.
    3 of the   6 matrix-vector products are transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96881)

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.96373)

 information on vector: 2 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    3(overlap= 0.86896)

 information on vector: 3 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -547.2493511937

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11

                                          orbital     8    9   10   11   12   13   14   15   16   17   18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       4  0.950939                        +-   +-   +-   +-   +-   +-   +-   +    +-   +       
 z*  3  1       6 -0.064017                        +-   +-   +-   +-   +-   +-   +-   +     -   +    +  
 z*  3  1       8  0.014772                        +-   +-   +-   +-   +-   +-   +-   +    +     -   +  
 z*  3  1       9  0.012300                        +-   +-   +-   +-   +-   +-   +-   +    +    +     - 
 z*  3  1      12  0.114572                        +-   +-   +-   +-   +-   +-   +-   +         +    +- 
 z*  3  1      19 -0.016305                        +-   +-   +-   +-   +-   +-   +    +-   +    +-      
 z*  3  1      26 -0.016588                        +-   +-   +-   +-   +-   +-   +     -   +    +-   +  
 z*  3  1      28  0.103951                        +-   +-   +-   +-   +-   +-   +    +    +-   +-      
 z*  3  1      32  0.055328                        +-   +-   +-   +-   +-   +-   +    +     -   +-   +  
 z*  3  1      34  0.067421                        +-   +-   +-   +-   +-   +-   +    +    +    +-    - 
 z*  3  1      36 -0.033444                        +-   +-   +-   +-   +-   +-   +    +         +-   +- 
 z*  3  1      44 -0.023562                        +-   +-   +-   +-   +-   +-        +    +-   +    +- 
 z   3  1      49  0.019349                        +-   +-   +-   +-   +-   +    +-   +-   +    +-      
 z   3  1      58 -0.026007                        +-   +-   +-   +-   +-   +    +-   +    +-   +-      
 z   3  1      61  0.010358                        +-   +-   +-   +-   +-   +    +-   +    +-        +- 
 z   3  1      96  0.039941                        +-   +-   +-   +-   +    +-   +-   +-   +-   +       
 z   3  1      98  0.019651                        +-   +-   +-   +-   +    +-   +-   +-    -   +    +  
 z   3  1     113  0.011298                        +-   +-   +-   +-   +    +-   +-   +     -   +    +- 
 z   3  1     123  0.011870                        +-   +-   +-   +-   +    +-    -   +    +-   +-   +  
 z   3  1     126  0.024926                        +-   +-   +-   +-   +    +-   +    +-   +-   +-      
 z   3  1     138  0.011258                        +-   +-   +-   +-   +    +-   +    +    +-   +-    - 
 z   3  1     146 -0.056767                        +-   +-   +-   +    +-   +-   +-   +-   +-   +       
 z   3  1     148 -0.021445                        +-   +-   +-   +    +-   +-   +-   +-    -   +    +  
 z   3  1     151  0.023910                        +-   +-   +-   +    +-   +-   +-   +-   +    +     - 
 z   3  1     160  0.014477                        +-   +-   +-   +    +-   +-   +-   +    +-   +     - 
 z   3  1     176 -0.032777                        +-   +-   +-   +    +-   +-   +    +-   +-   +-      
 z   3  1     209  0.010447                        +-   +-   +    +-   +-   +-   +-   +    +-    -   +  
 z   3  1     255  0.018310                        +-   +    +-   +-   +-   +-   +-    -   +-   +    +  
 z   3  1     257 -0.010862                        +-   +    +-   +-   +-   +-   +-    -   +    +    +- 
 z   3  1     263  0.019521                        +-   +    +-   +-   +-   +-   +-   +     -   +    +- 
 y   3  1     575 -0.016765              6( a  )   +-   +-   +-   +-   +-   +-   +-   +     -        +  
 y   3  1     631 -0.022411              6( a  )   +-   +-   +-   +-   +-   +-   +-   +    +          - 
 y   3  1     638 -0.016761             13( a  )   +-   +-   +-   +-   +-   +-   +-   +    +          - 
 y   3  1     766  0.014796              1( a  )   +-   +-   +-   +-   +-   +-   +-        +-   +       
 y   3  1     772 -0.016274              7( a  )   +-   +-   +-   +-   +-   +-   +-        +-   +       
 y   3  1     776  0.010747             11( a  )   +-   +-   +-   +-   +-   +-   +-        +-   +       
 y   3  1     777  0.018989             12( a  )   +-   +-   +-   +-   +-   +-   +-        +-   +       
 y   3  1     780  0.015223             15( a  )   +-   +-   +-   +-   +-   +-   +-        +-   +       
 y   3  1     822  0.010268              1( a  )   +-   +-   +-   +-   +-   +-   +-         -   +    +  
 y   3  1     906 -0.025843              1( a  )   +-   +-   +-   +-   +-   +-   +-        +    +     - 
 y   3  1     912  0.021873              7( a  )   +-   +-   +-   +-   +-   +-   +-        +    +     - 
 y   3  1     916 -0.014611             11( a  )   +-   +-   +-   +-   +-   +-   +-        +    +     - 
 y   3  1     917 -0.022784             12( a  )   +-   +-   +-   +-   +-   +-   +-        +    +     - 
 y   3  1     920 -0.016572             15( a  )   +-   +-   +-   +-   +-   +-   +-        +    +     - 
 y   3  1    1163  0.017881              6( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +    +  
 y   3  1    1170  0.019846             13( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +    +  
 y   3  1    1171 -0.012485             14( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +    +  
 y   3  1    1219  0.010928              6( a  )   +-   +-   +-   +-   +-   +-    -   +    +     -   +  
 y   3  1    1226  0.012016             13( a  )   +-   +-   +-   +-   +-   +-    -   +    +     -   +  
 y   3  1    1247 -0.029997              6( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +     - 
 y   3  1    1254 -0.032139             13( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +     - 
 y   3  1    1255  0.019935             14( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +     - 
 y   3  1    1438  0.010006              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-           
 y   3  1    1695  0.015049              6( a  )   +-   +-   +-   +-   +-   +-   +     -   +-   +       
 y   3  1    1702  0.016197             13( a  )   +-   +-   +-   +-   +-   +-   +     -   +-   +       
 y   3  1    1947 -0.015265              6( a  )   +-   +-   +-   +-   +-   +-   +    +    +-    -      
 y   3  1    1954 -0.019182             13( a  )   +-   +-   +-   +-   +-   +-   +    +    +-    -      
 y   3  1    2194 -0.013946              1( a  )   +-   +-   +-   +-   +-   +-   +         +-   +-      
 y   3  1    2200  0.010514              7( a  )   +-   +-   +-   +-   +-   +-   +         +-   +-      
 y   3  1    2205 -0.010246             12( a  )   +-   +-   +-   +-   +-   +-   +         +-   +-      
 y   3  1    2362  0.010739              1( a  )   +-   +-   +-   +-   +-   +-   +         +    +-    - 
 y   3  1    2787 -0.012448              6( a  )   +-   +-   +-   +-   +-   +-        +    +-   +-      
 y   3  1    2794 -0.011809             13( a  )   +-   +-   +-   +-   +-   +-        +    +-   +-      
 y   3  1    2795  0.013043             14( a  )   +-   +-   +-   +-   +-   +-        +    +-   +-      
 y   3  1    3292 -0.016104              7( a  )   +-   +-   +-   +-   +-    -   +-   +    +    +-      
 y   3  1    4553 -0.020113              8( a  )   +-   +-   +-   +-   +-   +    +-    -   +-   +       
 y   3  1    4559 -0.010288             14( a  )   +-   +-   +-   +-   +-   +    +-    -   +-   +       
 y   3  1    4636  0.011449              7( a  )   +-   +-   +-   +-   +-   +    +-    -   +    +-      
 y   3  1    4801  0.013113              4( a  )   +-   +-   +-   +-   +-   +    +-   +    +-    -      
 y   3  1    4805  0.056112              8( a  )   +-   +-   +-   +-   +-   +    +-   +    +-    -      
 y   3  1    4810  0.014798             13( a  )   +-   +-   +-   +-   +-   +    +-   +    +-    -      
 y   3  1    4811  0.027745             14( a  )   +-   +-   +-   +-   +-   +    +-   +    +-    -      
 y   3  1    4855 -0.011437              2( a  )   +-   +-   +-   +-   +-   +    +-   +     -   +-      
 y   3  1    4860 -0.025858              7( a  )   +-   +-   +-   +-   +-   +    +-   +     -   +-      
 y   3  1    4868 -0.010863             15( a  )   +-   +-   +-   +-   +-   +    +-   +     -   +-      
 y   3  1    5561 -0.025498              8( a  )   +-   +-   +-   +-   +-   +     -   +    +-   +-      
 y   3  1    5567 -0.014841             14( a  )   +-   +-   +-   +-   +-   +     -   +    +-   +-      
 y   3  1    7130 -0.011752              9( a  )   +-   +-   +-   +-    -   +-   +-   +    +    +     - 
 y   3  1    8326 -0.010453              1( a  )   +-   +-   +-   +-   +    +-   +-    -   +-   +       
 y   3  1    8388  0.012325              7( a  )   +-   +-   +-   +-   +    +-   +-    -    -   +    +  
 y   3  1    8613  0.017339              8( a  )   +-   +-   +-   +-   +    +-   +-   +    +-         - 
 y   3  1    8619  0.010491             14( a  )   +-   +-   +-   +-   +    +-   +-   +    +-         - 
 y   3  1    8691 -0.013750              2( a  )   +-   +-   +-   +-   +    +-   +-   +     -   +     - 
 y   3  1    8696 -0.015927              7( a  )   +-   +-   +-   +-   +    +-   +-   +     -   +     - 
 y   3  1    9395 -0.010260              6( a  )   +-   +-   +-   +-   +    +-    -   +    +-   +     - 
 y   3  1    9397 -0.010264              8( a  )   +-   +-   +-   +-   +    +-    -   +    +-   +     - 
 y   3  1   10678  0.012389              1( a  )   +-   +-   +-    -   +-   +-   +-   +-   +    +       
 y   3  1   10762 -0.013149              1( a  )   +-   +-   +-    -   +-   +-   +-   +    +-   +       
 y   3  1   11434  0.015780              1( a  )   +-   +-   +-    -   +-   +-   +    +    +-   +-      
 y   3  1   12106  0.018653              1( a  )   +-   +-   +-   +    +-   +-   +-    -   +-   +       
 y   3  1   12366  0.015778              9( a  )   +-   +-   +-   +    +-   +-   +-   +    +-    -      
 y   3  1   12419  0.010128              6( a  )   +-   +-   +-   +    +-   +-   +-   +     -   +-      
 y   3  1   14684  0.010412              3( a  )   +-   +-    -   +-   +-   +-   +-   +    +    +     - 
 y   3  1   16138 -0.021700              1( a  )   +-   +-   +    +-   +-   +-   +-   +    +-    -      
 y   3  1   16139  0.015120              2( a  )   +-   +-   +    +-   +-   +-   +-   +    +-    -      
 y   3  1   16146  0.021752              9( a  )   +-   +-   +    +-   +-   +-   +-   +    +-    -      
 y   3  1   16171  0.018867              6( a  )   +-   +-   +    +-   +-   +-   +-   +    +-         - 
 y   3  1   16199  0.017493              6( a  )   +-   +-   +    +-   +-   +-   +-   +     -   +-      
 y   3  1   16251 -0.014243              2( a  )   +-   +-   +    +-   +-   +-   +-   +     -   +     - 
 y   3  1   16258 -0.011535              9( a  )   +-   +-   +    +-   +-   +-   +-   +     -   +     - 
 y   3  1   18470  0.014013              9( a  )   +-    -   +-   +-   +-   +-   +-   +    +    +     - 
 y   3  1   18472 -0.010279             11( a  )   +-    -   +-   +-   +-   +-   +-   +    +    +     - 
 y   3  1   18478 -0.013232             17( a  )   +-    -   +-   +-   +-   +-   +-   +    +    +     - 
 y   3  1   18479 -0.010266             18( a  )   +-    -   +-   +-   +-   +-   +-   +    +    +     - 
 y   3  1   19516 -0.021480             19( a  )   +-   +    +-   +-   +-   +-   +-   +-    -   +       
 y   3  1   19674 -0.012040              9( a  )   +-   +    +-   +-   +-   +-   +-    -   +-   +       
 y   3  1   19676  0.010625             11( a  )   +-   +    +-   +-   +-   +-   +-    -   +-   +       
 y   3  1   19687 -0.027435             22( a  )   +-   +    +-   +-   +-   +-   +-    -   +-   +       
 y   3  1   19692  0.014290             27( a  )   +-   +    +-   +-   +-   +-   +-    -   +-   +       
 y   3  1   20046  0.012448             17( a  )   +-   +    +-   +-   +-   +-   +-   +     -   +     - 
 y   3  1   20056 -0.014620             27( a  )   +-   +    +-   +-   +-   +-   +-   +     -   +     - 
 y   3  1   20216 -0.012232             19( a  )   +-   +    +-   +-   +-   +-   +-        +-   +     - 
 y   3  1   20357  0.024898             20( a  )   +-   +    +-   +-   +-   +-    -   +-   +-   +       
 y   3  1   20358 -0.011684             21( a  )   +-   +    +-   +-   +-   +-    -   +-   +-   +       
 y   3  1   20742 -0.011901             13( a  )   +-   +    +-   +-   +-   +-    -   +    +-   +     - 
 y   3  1   20753 -0.014669             24( a  )   +-   +    +-   +-   +-   +-    -   +    +-   +     - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             113
     0.01> rq > 0.001           1774
    0.001> rq > 0.0001          5065
   0.0001> rq > 0.00001         4663
  0.00001> rq > 0.000001        1279
 0.000001> rq                  12900
           all                 25797
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       21120 2x:           0 4x:           0
All internal counts: zz :       66424 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.003225742272     -1.762147917318
     2     2     -0.000003552961      0.001942806039
     3     3      0.004648723006     -2.541813322957
     4     4      0.950938754881   -520.294988767239
     5     5      0.000000039407     -0.000021541509
     6     6     -0.064017176824     35.032861595690
     7     7      0.000000318879     -0.000174393313
     8     8      0.014771721190     -8.076276985773
     9     9      0.012300150086     -6.747495155361
    10    10      0.000000019771     -0.000010791118
    11    11     -0.000000004684      0.000002624895
    12    12      0.114571660195    -62.704885159518
    13    13      0.000233575482     -0.128030325388
    14    14     -0.000000094525      0.000051652161
    15    15     -0.001349311974      0.737842760271
    16    16     -0.000002383852      0.001304063983
    17    17     -0.002082555811      1.139220021394
    18    18      0.000000614428     -0.000335405163
    19    19     -0.016305049291      8.918796899850
    20    20      0.000002484077     -0.001358156860
    21    21     -0.000003527146      0.001927689055
    22    22      0.000919618488     -0.502995094572
    23    23      0.000160184827     -0.091364216315
    24    24      0.000001064008     -0.000580979154
    25    25      0.000000399066     -0.000218153854
    26    26     -0.016588447622      9.075009136517
    27    27     -0.000000074397      0.000040592643
    28    28      0.103950882617    -56.890214422025
    29    29      0.000000344165     -0.000188192020
    30    30      0.000000240892     -0.000131753301
    31    31      0.000374743228     -0.204000245280
    32    32      0.055327599338    -30.272232404461
    33    33     -0.000000287952      0.000157300381
    34    34      0.067421017012    -36.895461676942
    35    35     -0.000000053627      0.000029299437
    36    36     -0.033443570758     18.317240566729
    37    37      0.003691540600     -2.018771891566
    38    38      0.000000803825     -0.000439012022
    39    39     -0.000016981660      0.009793940064
    40    40      0.001750735843     -0.957333184383
    41    41     -0.000001164628      0.000636918446
    42    42      0.000044988227     -0.024540133448
    43    43     -0.000000108258      0.000059138950
    44    44     -0.023561878957     12.885218943678
    45    45     -0.000000063784      0.000034793173

 number of reference csfs (nref) is    45.  root number (iroot) is  1.
 c0**2 =   0.94256200  c**2 (all zwalks) =   0.95463707

 pople ci energy extrapolation is computed with 18 correlated electrons.

 eref      =   -547.143653850554   "relaxed" cnot**2         =   0.942562000763
 eci       =   -547.249351193714   deltae = eci - eref       =  -0.105697343160
 eci+dv1   =   -547.255422237630   dv1 = (1-cnot**2)*deltae  =  -0.006071043916
 eci+dv2   =   -547.255792195909   dv2 = dv1 / cnot**2       =  -0.006441002195
 eci+dv3   =   -547.256210169299   dv3 = dv1 / (2*cnot**2-1) =  -0.006858975585
 eci+pople =   -547.255358963237   ( 18e- scaled deltae )    =  -0.111705112683


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -547.2407788429

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11

                                          orbital     8    9   10   11   12   13   14   15   16   17   18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       2 -0.053942                        +-   +-   +-   +-   +-   +-   +-   +-   +         +  
 z*  3  1      16  0.953340                        +-   +-   +-   +-   +-   +-   +    +-   +-   +       
 z*  3  1      18 -0.034666                        +-   +-   +-   +-   +-   +-   +    +-    -   +    +  
 z*  3  1      20  0.023386                        +-   +-   +-   +-   +-   +-   +    +-   +     -   +  
 z*  3  1      21 -0.023251                        +-   +-   +-   +-   +-   +-   +    +-   +    +     - 
 z*  3  1      24  0.122379                        +-   +-   +-   +-   +-   +-   +    +-        +    +- 
 z*  3  1      38  0.025716                        +-   +-   +-   +-   +-   +-   +         +-   +    +- 
 z*  3  1      41 -0.025820                        +-   +-   +-   +-   +-   +-        +-   +    +-   +  
 z   3  1      46 -0.102301                        +-   +-   +-   +-   +-   +    +-   +-   +-   +       
 z   3  1      48 -0.026636                        +-   +-   +-   +-   +-   +    +-   +-    -   +    +  
 z   3  1      50 -0.014900                        +-   +-   +-   +-   +-   +    +-   +-   +     -   +  
 z   3  1      51  0.045485                        +-   +-   +-   +-   +-   +    +-   +-   +    +     - 
 z   3  1      76 -0.074049                        +-   +-   +-   +-   +-   +    +    +-   +-   +-      
 z   3  1      84  0.011893                        +-   +-   +-   +-   +-   +    +    +-        +-   +- 
 z   3  1     128  0.013555                        +-   +-   +-   +-   +    +-   +    +-   +-   +     - 
 z   3  1     131  0.015045                        +-   +-   +-   +-   +    +-   +    +-    -   +    +- 
 z   3  1     178  0.010745                        +-   +-   +-   +    +-   +-   +    +-   +-   +     - 
 z   3  1     220 -0.010898                        +-   +-   +    +-   +-   +-    -   +-   +-   +    +  
 z   3  1     228  0.014185                        +-   +-   +    +-   +-   +-   +    +-   +-   +     - 
 z   3  1     247  0.011249                        +-   +    +-   +-   +-   +-   +-   +-   +-        +  
 z   3  1     270  0.013523                        +-   +    +-   +-   +-   +-    -   +-   +-   +    +  
 z   3  1     272 -0.011857                        +-   +    +-   +-   +-   +-    -   +-   +    +    +- 
 z   3  1     281  0.019867                        +-   +    +-   +-   +-   +-   +    +-    -   +    +- 
 y   3  1     514  0.016206              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-           
 y   3  1    1102  0.025022              1( a  )   +-   +-   +-   +-   +-   +-    -   +    +-   +       
 y   3  1    1108 -0.010246              7( a  )   +-   +-   +-   +-   +-   +-    -   +    +-   +       
 y   3  1    1158 -0.019277              1( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +    +  
 y   3  1    1164  0.014510              7( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +    +  
 y   3  1    1169 -0.012878             12( a  )   +-   +-   +-   +-   +-   +-    -   +     -   +    +  
 y   3  1    1242  0.024811              1( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +     - 
 y   3  1    1248 -0.014596              7( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +     - 
 y   3  1    1253  0.012982             12( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +     - 
 y   3  1    1256  0.010463             15( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +     - 
 y   3  1    1499 -0.017648              6( a  )   +-   +-   +-   +-   +-   +-   +    +-    -        +  
 y   3  1    1506 -0.010431             13( a  )   +-   +-   +-   +-   +-   +-   +    +-    -        +  
 y   3  1    1553  0.010796              4( a  )   +-   +-   +-   +-   +-   +-   +    +-   +          - 
 y   3  1    1555 -0.022526              6( a  )   +-   +-   +-   +-   +-   +-   +    +-   +          - 
 y   3  1    1562 -0.017816             13( a  )   +-   +-   +-   +-   +-   +-   +    +-   +          - 
 y   3  1    1690 -0.023083              1( a  )   +-   +-   +-   +-   +-   +-   +     -   +-   +       
 y   3  1    1746  0.022425              1( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +    +  
 y   3  1    1752 -0.014265              7( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +    +  
 y   3  1    1757  0.013973             12( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +    +  
 y   3  1    1760  0.011131             15( a  )   +-   +-   +-   +-   +-   +-   +     -    -   +    +  
 y   3  1    1802  0.016419              1( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -   +  
 y   3  1    1808 -0.011556              7( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -   +  
 y   3  1    1813  0.010696             12( a  )   +-   +-   +-   +-   +-   +-   +     -   +     -   +  
 y   3  1    1830 -0.040523              1( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +     - 
 y   3  1    1836  0.028628              7( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +     - 
 y   3  1    1840 -0.017440             11( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +     - 
 y   3  1    1841 -0.027170             12( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +     - 
 y   3  1    1844 -0.020212             15( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +     - 
 y   3  1    2451  0.017641              6( a  )   +-   +-   +-   +-   +-   +-        +-   +-   +       
 y   3  1    2458  0.025716             13( a  )   +-   +-   +-   +-   +-   +-        +-   +-   +       
 y   3  1    2570  0.010030             13( a  )   +-   +-   +-   +-   +-   +-        +-   +     -   +  
 y   3  1    2591 -0.020595              6( a  )   +-   +-   +-   +-   +-   +-        +-   +    +     - 
 y   3  1    2598 -0.025938             13( a  )   +-   +-   +-   +-   +-   +-        +-   +    +     - 
 y   3  1    2599  0.016636             14( a  )   +-   +-   +-   +-   +-   +-        +-   +    +     - 
 y   3  1    3124  0.010455              7( a  )   +-   +-   +-   +-   +-    -   +-   +-   +    +       
 y   3  1    3202 -0.017698              1( a  )   +-   +-   +-   +-   +-    -   +-   +    +-   +       
 y   3  1    3628 -0.014696              7( a  )   +-   +-   +-   +-   +-    -   +    +-   +    +-      
 y   3  1    3681 -0.010150              4( a  )   +-   +-   +-   +-   +-    -   +    +-   +    +     - 
 y   3  1    3683  0.010136              6( a  )   +-   +-   +-   +-   +-    -   +    +-   +    +     - 
 y   3  1    4379 -0.010657              2( a  )   +-   +-   +-   +-   +-   +    +-   +-    -   +       
 y   3  1    4384 -0.021713              7( a  )   +-   +-   +-   +-   +-   +    +-   +-    -   +       
 y   3  1    4546  0.032647              1( a  )   +-   +-   +-   +-   +-   +    +-    -   +-   +       
 y   3  1    5225 -0.042194              8( a  )   +-   +-   +-   +-   +-   +     -   +-   +-   +       
 y   3  1    5231 -0.025363             14( a  )   +-   +-   +-   +-   +-   +     -   +-   +-   +       
 y   3  1    5308  0.010949              7( a  )   +-   +-   +-   +-   +-   +     -   +-   +    +-      
 y   3  1    5893  0.013257              4( a  )   +-   +-   +-   +-   +-   +    +    +-   +-    -      
 y   3  1    5897  0.052138              8( a  )   +-   +-   +-   +-   +-   +    +    +-   +-    -      
 y   3  1    5902  0.011211             13( a  )   +-   +-   +-   +-   +-   +    +    +-   +-    -      
 y   3  1    5903  0.026127             14( a  )   +-   +-   +-   +-   +-   +    +    +-   +-    -      
 y   3  1    5947 -0.010065              2( a  )   +-   +-   +-   +-   +-   +    +    +-    -   +-      
 y   3  1    5952 -0.023795              7( a  )   +-   +-   +-   +-   +-   +    +    +-    -   +-      
 y   3  1    7466 -0.011709              9( a  )   +-   +-   +-   +-    -   +-   +    +-   +    +     - 
 y   3  1    9060  0.011519              7( a  )   +-   +-   +-   +-   +    +-    -   +-    -   +    +  
 y   3  1    9705  0.016059              8( a  )   +-   +-   +-   +-   +    +-   +    +-   +-         - 
 y   3  1    9711  0.010293             14( a  )   +-   +-   +-   +-   +    +-   +    +-   +-         - 
 y   3  1    9783 -0.013238              2( a  )   +-   +-   +-   +-   +    +-   +    +-    -   +     - 
 y   3  1    9788 -0.015382              7( a  )   +-   +-   +-   +-   +    +-   +    +-    -   +     - 
 y   3  1   11098  0.013547              1( a  )   +-   +-   +-    -   +-   +-   +    +-   +-   +       
 y   3  1   13458  0.014344              9( a  )   +-   +-   +-   +    +-   +-   +    +-   +-    -      
 y   3  1   13511  0.010216              6( a  )   +-   +-   +-   +    +-   +-   +    +-    -   +-      
 y   3  1   15020  0.011403              3( a  )   +-   +-    -   +-   +-   +-   +    +-   +    +     - 
 y   3  1   17230 -0.018698              1( a  )   +-   +-   +    +-   +-   +-   +    +-   +-    -      
 y   3  1   17231  0.013694              2( a  )   +-   +-   +    +-   +-   +-   +    +-   +-    -      
 y   3  1   17238  0.018841              9( a  )   +-   +-   +    +-   +-   +-   +    +-   +-    -      
 y   3  1   17263  0.018220              6( a  )   +-   +-   +    +-   +-   +-   +    +-   +-         - 
 y   3  1   17291  0.017513              6( a  )   +-   +-   +    +-   +-   +-   +    +-    -   +-      
 y   3  1   17343 -0.015167              2( a  )   +-   +-   +    +-   +-   +-   +    +-    -   +     - 
 y   3  1   17350 -0.012553              9( a  )   +-   +-   +    +-   +-   +-   +    +-    -   +     - 
 y   3  1   18806  0.015102              9( a  )   +-    -   +-   +-   +-   +-   +    +-   +    +     - 
 y   3  1   18808 -0.010669             11( a  )   +-    -   +-   +-   +-   +-   +    +-   +    +     - 
 y   3  1   18814 -0.013799             17( a  )   +-    -   +-   +-   +-   +-   +    +-   +    +     - 
 y   3  1   18815 -0.010805             18( a  )   +-    -   +-   +-   +-   +-   +    +-   +    +     - 
 y   3  1   19503  0.012067              6( a  )   +-   +    +-   +-   +-   +-   +-   +-    -   +       
 y   3  1   19518  0.012761             21( a  )   +-   +    +-   +-   +-   +-   +-   +-    -   +       
 y   3  1   19521 -0.016770             24( a  )   +-   +    +-   +-   +-   +-   +-   +-    -   +       
 y   3  1   19685  0.024575             20( a  )   +-   +    +-   +-   +-   +-   +-    -   +-   +       
 y   3  1   19686 -0.011851             21( a  )   +-   +    +-   +-   +-   +-   +-    -   +-   +       
 y   3  1   20346 -0.012232              9( a  )   +-   +    +-   +-   +-   +-    -   +-   +-   +       
 y   3  1   20359  0.026738             22( a  )   +-   +    +-   +-   +-   +-    -   +-   +-   +       
 y   3  1   20364  0.011346             27( a  )   +-   +    +-   +-   +-   +-    -   +-   +-   +       
 y   3  1   21138  0.012490             17( a  )   +-   +    +-   +-   +-   +-   +    +-    -   +     - 
 y   3  1   21148 -0.014963             27( a  )   +-   +    +-   +-   +-   +-   +    +-    -   +     - 
 y   3  1   21329 -0.010165             12( a  )   +-   +    +-   +-   +-   +-   +     -   +-   +     - 
 y   3  1   21336 -0.014724             19( a  )   +-   +    +-   +-   +-   +-   +     -   +-   +     - 
 y   3  1   21733 -0.012183             24( a  )   +-   +    +-   +-   +-   +-        +-   +-   +     - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             105
     0.01> rq > 0.001           1591
    0.001> rq > 0.0001          4186
   0.0001> rq > 0.00001         4724
  0.00001> rq > 0.000001        2329
 0.000001> rq                  12859
           all                 25797
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       21120 2x:           0 4x:           0
All internal counts: zz :       66424 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000003814774     -0.002085401460
     2     2     -0.053942013477     29.509551849688
     3     3     -0.000030409560      0.016623316448
     4     4     -0.000006125101      0.003355808489
     5     5     -0.000328896065      0.180195136556
     6     6      0.000025259930     -0.013804341207
     7     7      0.001951599035     -1.067278662325
     8     8      0.000011797510     -0.006446894690
     9     9     -0.000038343524      0.020953839294
    10    10      0.000514346742     -0.281139976789
    11    11     -0.000030121283      0.016823303345
    12    12      0.000002170259     -0.001190265104
    13    13      0.000004306145     -0.002353908147
    14    14     -0.000763056686      0.417083076394
    15    15     -0.000000578674      0.000315329474
    16    16      0.953340007758   -521.605042284501
    17    17      0.000019284847     -0.010545703007
    18    18     -0.034665678458     18.973907713662
    19    19     -0.000020727196      0.011337110394
    20    20      0.023385686531    -12.789844766293
    21    21     -0.023250504414     12.705252158229
    22    22      0.000002222925     -0.001214201644
    23    23     -0.000003317586      0.001810082990
    24    24      0.122378554791    -66.986177478518
    25    25      0.003714263988     -2.031492024126
    26    26     -0.000001882440      0.001028172251
    27    27     -0.001011525467      0.552594350785
    28    28     -0.000000592438      0.000316337194
    29    29      0.002765010205     -1.512437069439
    30    30      0.002826583305     -1.545263524330
    31    31     -0.000004009510      0.002191739437
    32    32     -0.000003959349      0.002161109229
    33    33     -0.002966786668      1.621828951022
    34    34     -0.000007331594      0.004003585330
    35    35     -0.000139346067      0.076470133425
    36    36      0.000007944317     -0.004345073404
    37    37     -0.000000846467      0.000461728820
    38    38      0.025715523235    -14.062587463891
    39    39      0.000000638502     -0.000348815104
    40    40      0.000011083525     -0.006060479597
    41    41     -0.025819798351     14.125436682189
    42    42      0.000005639667     -0.003083228912
    43    43     -0.000941513623      0.514733382907
    44    44     -0.000001681348      0.000918491658
    45    45     -0.000017561655      0.009542039083

 number of reference csfs (nref) is    45.  root number (iroot) is  2.
 c0**2 =   0.93040549  c**2 (all zwalks) =   0.95246732

 pople ci energy extrapolation is computed with 18 correlated electrons.

 eref      =   -547.137345782411   "relaxed" cnot**2         =   0.930405488353
 eci       =   -547.240778842914   deltae = eci - eref       =  -0.103433060504
 eci+dv1   =   -547.247977216248   dv1 = (1-cnot**2)*deltae  =  -0.007198373334
 eci+dv2   =   -547.248515655975   dv2 = dv1 / cnot**2       =  -0.007736813061
 eci+dv3   =   -547.249141158833   dv3 = dv1 / (2*cnot**2-1) =  -0.008362315919
 eci+pople =   -547.248076280917   ( 18e- scaled deltae )    =  -0.110730498506


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -547.1775973090

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11

                                          orbital     8    9   10   11   12   13   14   15   16   17   18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.851102                        +-   +-   +-   +-   +-   +-   +-   +-   +    +       
 z*  3  1       3  0.027227                        +-   +-   +-   +-   +-   +-   +-   +-        +    +  
 z*  3  1       4 -0.018280                        +-   +-   +-   +-   +-   +-   +-   +    +-   +       
 z*  3  1       9 -0.010140                        +-   +-   +-   +-   +-   +-   +-   +    +    +     - 
 z*  3  1      13 -0.034588                        +-   +-   +-   +-   +-   +-   +-        +-   +    +  
 z*  3  1      15 -0.032089                        +-   +-   +-   +-   +-   +-   +-        +    +    +- 
 z*  3  1      17 -0.063747                        +-   +-   +-   +-   +-   +-   +    +-   +-        +  
 z*  3  1      19 -0.192619                        +-   +-   +-   +-   +-   +-   +    +-   +    +-      
 z*  3  1      22  0.011457                        +-   +-   +-   +-   +-   +-   +    +-   +         +- 
 z*  3  1      23  0.095037                        +-   +-   +-   +-   +-   +-   +    +-        +-   +  
 z*  3  1      28 -0.053641                        +-   +-   +-   +-   +-   +-   +    +    +-   +-      
 z*  3  1      40 -0.022912                        +-   +-   +-   +-   +-   +-        +-   +-   +    +  
 z*  3  1      42 -0.032641                        +-   +-   +-   +-   +-   +-        +-   +    +    +- 
 z   3  1      49  0.027858                        +-   +-   +-   +-   +-   +    +-   +-   +    +-      
 z   3  1      53  0.010866                        +-   +-   +-   +-   +-   +    +-   +-        +-   +  
 z   3  1      58  0.023283                        +-   +-   +-   +-   +-   +    +-   +    +-   +-      
 z   3  1      96  0.278709                        +-   +-   +-   +-   +    +-   +-   +-   +-   +       
 z   3  1      98  0.071532                        +-   +-   +-   +-   +    +-   +-   +-    -   +    +  
 z   3  1     100  0.022031                        +-   +-   +-   +-   +    +-   +-   +-   +     -   +  
 z   3  1     101 -0.074584                        +-   +-   +-   +-   +    +-   +-   +-   +    +     - 
 z   3  1     104  0.034710                        +-   +-   +-   +-   +    +-   +-   +-        +    +- 
 z   3  1     105  0.012488                        +-   +-   +-   +-   +    +-   +-    -   +-   +    +  
 z   3  1     118  0.010453                        +-   +-   +-   +-   +    +-   +-        +-   +    +- 
 z   3  1     121 -0.013879                        +-   +-   +-   +-   +    +-    -   +-   +    +-   +  
 z   3  1     126  0.230588                        +-   +-   +-   +-   +    +-   +    +-   +-   +-      
 z   3  1     134 -0.037736                        +-   +-   +-   +-   +    +-   +    +-        +-   +- 
 z   3  1     146  0.038157                        +-   +-   +-   +    +-   +-   +-   +-   +-   +       
 z   3  1     148  0.019235                        +-   +-   +-   +    +-   +-   +-   +-    -   +    +  
 z   3  1     150  0.011536                        +-   +-   +-   +    +-   +-   +-   +-   +     -   +  
 z   3  1     155 -0.010416                        +-   +-   +-   +    +-   +-   +-    -   +-   +    +  
 z   3  1     176  0.016570                        +-   +-   +-   +    +-   +-   +    +-   +-   +-      
 z   3  1     196  0.040059                        +-   +-   +    +-   +-   +-   +-   +-   +-   +       
 z   3  1     198  0.031960                        +-   +-   +    +-   +-   +-   +-   +-    -   +    +  
 z   3  1     200  0.018165                        +-   +-   +    +-   +-   +-   +-   +-   +     -   +  
 z   3  1     201 -0.017346                        +-   +-   +    +-   +-   +-   +-   +-   +    +     - 
 z   3  1     226  0.040171                        +-   +-   +    +-   +-   +-   +    +-   +-   +-      
 z   3  1     246  0.051573                        +-   +    +-   +-   +-   +-   +-   +-   +-   +       
 z   3  1     254  0.014948                        +-   +    +-   +-   +-   +-   +-   +-        +    +- 
 z   3  1     271 -0.016329                        +-   +    +-   +-   +-   +-    -   +-   +    +-   +  
 z   3  1     296 -0.018851                        +    +-   +-   +-   +-   +-   +-   +-   +-   +       
 y   3  1     351 -0.023806              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +            
 y   3  1     374  0.012900              1( a  )   +-   +-   +-   +-   +-   +-   +-   +-        +       
 y   3  1     385 -0.014103             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-        +       
 y   3  1     407 -0.019057              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-             +  
 y   3  1     430 -0.066830              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +    +       
 y   3  1     434  0.013378              5( a  )   +-   +-   +-   +-   +-   +-   +-    -   +    +       
 y   3  1     436  0.016752              7( a  )   +-   +-   +-   +-   +-   +-   +-    -   +    +       
 y   3  1     486  0.039515              1( a  )   +-   +-   +-   +-   +-   +-   +-    -        +    +  
 y   3  1     492 -0.024717              7( a  )   +-   +-   +-   +-   +-   +-   +-    -        +    +  
 y   3  1     496  0.015140             11( a  )   +-   +-   +-   +-   +-   +-   +-    -        +    +  
 y   3  1     497  0.022492             12( a  )   +-   +-   +-   +-   +-   +-   +-    -        +    +  
 y   3  1     500  0.017273             15( a  )   +-   +-   +-   +-   +-   +-   +-    -        +    +  
 y   3  1    1023 -0.015422              6( a  )   +-   +-   +-   +-   +-   +-    -   +-   +    +       
 y   3  1    1079  0.022184              6( a  )   +-   +-   +-   +-   +-   +-    -   +-        +    +  
 y   3  1    1086  0.022434             13( a  )   +-   +-   +-   +-   +-   +-    -   +-        +    +  
 y   3  1    1087 -0.015318             14( a  )   +-   +-   +-   +-   +-   +-    -   +-        +    +  
 y   3  1    1186  0.013215              1( a  )   +-   +-   +-   +-   +-   +-    -   +    +    +-      
 y   3  1    1471  0.017443              6( a  )   +-   +-   +-   +-   +-   +-   +    +-    -   +       
 y   3  1    1478  0.012270             13( a  )   +-   +-   +-   +-   +-   +-   +    +-    -   +       
 y   3  1    1525  0.012693              4( a  )   +-   +-   +-   +-   +-   +-   +    +-   +     -      
 y   3  1    1527 -0.023024              6( a  )   +-   +-   +-   +-   +-   +-   +    +-   +     -      
 y   3  1    1534 -0.021007             13( a  )   +-   +-   +-   +-   +-   +-   +    +-   +     -      
 y   3  1    1774 -0.038029              1( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +-      
 y   3  1    1780  0.022411              7( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +-      
 y   3  1    1784 -0.012557             11( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +-      
 y   3  1    1785 -0.019661             12( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +-      
 y   3  1    1788 -0.014131             15( a  )   +-   +-   +-   +-   +-   +-   +     -   +    +-      
 y   3  1    2535 -0.022464              6( a  )   +-   +-   +-   +-   +-   +-        +-   +    +-      
 y   3  1    2542 -0.022759             13( a  )   +-   +-   +-   +-   +-   +-        +-   +    +-      
 y   3  1    2543  0.015738             14( a  )   +-   +-   +-   +-   +-   +-        +-   +    +-      
 y   3  1    3627  0.011535              6( a  )   +-   +-   +-   +-   +-    -   +    +-   +    +-      
 y   3  1    4385 -0.019285              8( a  )   +-   +-   +-   +-   +-   +    +-   +-    -   +       
 y   3  1    4437  0.010959              4( a  )   +-   +-   +-   +-   +-   +    +-   +-   +     -      
 y   3  1    4441  0.039233              8( a  )   +-   +-   +-   +-   +-   +    +-   +-   +     -      
 y   3  1    4447  0.020220             14( a  )   +-   +-   +-   +-   +-   +    +-   +-   +     -      
 y   3  1    5309 -0.015719              8( a  )   +-   +-   +-   +-   +-   +     -   +-   +    +-      
 y   3  1    6962  0.010197              9( a  )   +-   +-   +-   +-    -   +-   +-   +-        +    +  
 y   3  1    6982  0.034966              1( a  )   +-   +-   +-   +-    -   +-   +-   +    +-   +       
 y   3  1    7325 -0.021995              8( a  )   +-   +-   +-   +-    -   +-   +    +-   +-   +       
 y   3  1    7331 -0.011486             14( a  )   +-   +-   +-   +-    -   +-   +    +-   +-   +       
 y   3  1    7403  0.010452              2( a  )   +-   +-   +-   +-    -   +-   +    +-   +    +-      
 y   3  1    7408  0.010945              7( a  )   +-   +-   +-   +-    -   +-   +    +-   +    +-      
 y   3  1    8158  0.018392              1( a  )   +-   +-   +-   +-   +    +-   +-   +-    -   +       
 y   3  1    8159  0.019787              2( a  )   +-   +-   +-   +-   +    +-   +-   +-    -   +       
 y   3  1    8164  0.041435              7( a  )   +-   +-   +-   +-   +    +-   +-   +-    -   +       
 y   3  1    8166  0.016523              9( a  )   +-   +-   +-   +-   +    +-   +-   +-    -   +       
 y   3  1    8169 -0.015025             12( a  )   +-   +-   +-   +-   +    +-   +-   +-    -   +       
 y   3  1    8172  0.021240             15( a  )   +-   +-   +-   +-   +    +-   +-   +-    -   +       
 y   3  1    8173 -0.010703             16( a  )   +-   +-   +-   +-   +    +-   +-   +-    -   +       
 y   3  1    8245  0.010017              4( a  )   +-   +-   +-   +-   +    +-   +-   +-   +          - 
 y   3  1    8249  0.017513              8( a  )   +-   +-   +-   +-   +    +-   +-   +-   +          - 
 y   3  1    8255  0.010844             14( a  )   +-   +-   +-   +-   +    +-   +-   +-   +          - 
 y   3  1    8299 -0.011316              2( a  )   +-   +-   +-   +-   +    +-   +-   +-        +     - 
 y   3  1    8304 -0.012853              7( a  )   +-   +-   +-   +-   +    +-   +-   +-        +     - 
 y   3  1    8326 -0.066647              1( a  )   +-   +-   +-   +-   +    +-   +-    -   +-   +       
 y   3  1    8330  0.013976              5( a  )   +-   +-   +-   +-   +    +-   +-    -   +-   +       
 y   3  1    8332  0.021069              7( a  )   +-   +-   +-   +-   +    +-   +-    -   +-   +       
 y   3  1    8336 -0.011476             11( a  )   +-   +-   +-   +-   +    +-   +-    -   +-   +       
 y   3  1    8337 -0.014809             12( a  )   +-   +-   +-   +-   +    +-   +-    -   +-   +       
 y   3  1    8341 -0.010321             16( a  )   +-   +-   +-   +-   +    +-   +-    -   +-   +       
 y   3  1    8466 -0.017101              1( a  )   +-   +-   +-   +-   +    +-   +-    -   +    +     - 
 y   3  1    8472  0.010876              7( a  )   +-   +-   +-   +-   +    +-   +-    -   +    +     - 
 y   3  1    8550 -0.010960              1( a  )   +-   +-   +-   +-   +    +-   +-    -        +    +- 
 y   3  1    9005  0.031758              8( a  )   +-   +-   +-   +-   +    +-    -   +-   +-   +       
 y   3  1    9011  0.022159             14( a  )   +-   +-   +-   +-   +    +-    -   +-   +-   +       
 y   3  1    9143 -0.013687              6( a  )   +-   +-   +-   +-   +    +-    -   +-   +    +     - 
 y   3  1    9145 -0.010589              8( a  )   +-   +-   +-   +-   +    +-    -   +-   +    +     - 
 y   3  1    9150 -0.011961             13( a  )   +-   +-   +-   +-   +    +-    -   +-   +    +     - 
 y   3  1    9677 -0.015714              8( a  )   +-   +-   +-   +-   +    +-   +    +-   +-    -      
 y   3  1   10034  0.013706              1( a  )   +-   +-   +-   +-   +    +-   +     -    -   +-   +  
 y   3  1   10090  0.013160              1( a  )   +-   +-   +-   +-   +    +-   +     -   +    +-    - 
 y   3  1   10678  0.012863              1( a  )   +-   +-   +-    -   +-   +-   +-   +-   +    +       
 y   3  1   10762  0.010901              1( a  )   +-   +-   +-    -   +-   +-   +-   +    +-   +       
 y   3  1   11182  0.010769              1( a  )   +-   +-   +-    -   +-   +-   +    +-   +    +-      
 y   3  1   11944 -0.017374              7( a  )   +-   +-   +-   +    +-   +-   +-   +-    -   +       
 y   3  1   12002  0.013305              9( a  )   +-   +-   +-   +    +-   +-   +-   +-   +     -      
 y   3  1   12106 -0.012845              1( a  )   +-   +-   +-   +    +-   +-   +-    -   +-   +       
 y   3  1   12785 -0.011187              8( a  )   +-   +-   +-   +    +-   +-    -   +-   +-   +       
 y   3  1   14542  0.010108              1( a  )   +-   +-    -   +-   +-   +-   +-   +    +-   +       
 y   3  1   15718  0.010132              1( a  )   +-   +-   +    +-   +-   +-   +-   +-    -   +       
 y   3  1   15724 -0.011672              7( a  )   +-   +-   +    +-   +-   +-   +-   +-    -   +       
 y   3  1   15774 -0.017102              1( a  )   +-   +-   +    +-   +-   +-   +-   +-   +     -      
 y   3  1   15775  0.013650              2( a  )   +-   +-   +    +-   +-   +-   +-   +-   +     -      
 y   3  1   15782  0.018904              9( a  )   +-   +-   +    +-   +-   +-   +-   +-   +     -      
 y   3  1   15807  0.014407              6( a  )   +-   +-   +    +-   +-   +-   +-   +-   +          - 
 y   3  1   15859 -0.010153              2( a  )   +-   +-   +    +-   +-   +-   +-   +-        +     - 
 y   3  1   15886 -0.020322              1( a  )   +-   +-   +    +-   +-   +-   +-    -   +-   +       
 y   3  1   16703 -0.010965              6( a  )   +-   +-   +    +-   +-   +-    -   +-   +    +     - 
 y   3  1   18302 -0.011606              9( a  )   +-    -   +-   +-   +-   +-   +-   +-        +    +  
 y   3  1   18310  0.010999             17( a  )   +-    -   +-   +-   +-   +-   +-   +-        +    +  
 y   3  1   19499  0.011788              2( a  )   +-   +    +-   +-   +-   +-   +-   +-    -   +       
 y   3  1   19514  0.010258             17( a  )   +-   +    +-   +-   +-   +-   +-   +-    -   +       
 y   3  1   19524 -0.012814             27( a  )   +-   +    +-   +-   +-   +-   +-   +-    -   +       
 y   3  1   19684 -0.018512             19( a  )   +-   +    +-   +-   +-   +-   +-    -   +-   +       
 y   3  1   19769 -0.013729             20( a  )   +-   +    +-   +-   +-   +-   +-    -   +    +-      
 y   3  1   19824 -0.013427             19( a  )   +-   +    +-   +-   +-   +-   +-    -   +    +     - 
 y   3  1   20358  0.012120             21( a  )   +-   +    +-   +-   +-   +-    -   +-   +-   +       
 y   3  1   20361 -0.013158             24( a  )   +-   +    +-   +-   +-   +-    -   +-   +-   +       
 y   3  1   20443 -0.013755             22( a  )   +-   +    +-   +-   +-   +-    -   +-   +    +-      
 y   3  1   20490 -0.010721             13( a  )   +-   +    +-   +-   +-   +-    -   +-   +    +     - 
 y   3  1   20501 -0.012544             24( a  )   +-   +    +-   +-   +-   +-    -   +-   +    +     - 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             137
     0.01> rq > 0.001           1707
    0.001> rq > 0.0001          5374
   0.0001> rq > 0.00001         4669
  0.00001> rq > 0.000001        1727
 0.000001> rq                  12179
           all                 25797
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       21120 2x:           0 4x:           0
All internal counts: zz :       66424 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.851101823690   -465.583357085450
     2     2      0.000022085132     -0.012076488655
     3     3      0.027227080108    -14.880282466490
     4     4     -0.018279939362     10.006027091478
     5     5      0.000006109165     -0.003341254436
     6     6      0.002986535888     -1.630748778627
     7     7     -0.000041179020      0.022520498937
     8     8     -0.002794516672      1.530120936580
     9     9     -0.010139737882      5.540903284655
    10    10      0.000004509055     -0.002465279633
    11    11      0.000006303535     -0.003452173773
    12    12     -0.000290859275      0.161369130965
    13    13     -0.034587725086     18.923777135429
    14    14      0.000000648662     -0.000354577352
    15    15     -0.032088810146     17.546853723516
    16    16      0.000037257719     -0.020386241990
    17    17     -0.063746833973     34.872683324419
    18    18     -0.000039048835      0.021343369766
    19    19     -0.192619039645    105.334316478853
    20    20     -0.000036388099      0.019893054158
    21    21      0.000026478195     -0.014465289240
    22    22      0.011457215487     -6.264207773349
    23    23      0.095037080563    -52.043898544258
    24    24      0.000007174565     -0.003920770404
    25    25     -0.000013180174      0.007206184615
    26    26      0.000488661288     -0.268851422365
    27    27     -0.000000719632      0.000394742715
    28    28     -0.053641454738     29.336676365951
    29    29     -0.000020563557      0.011244132663
    30    30      0.000008894988     -0.004863083912
    31    31      0.002498109536     -1.365895151907
    32    32      0.003180990696     -1.745701410883
    33    33     -0.000001198383      0.000656634840
    34    34      0.005159399116     -2.826644750538
    35    35      0.000001944752     -0.001063880721
    36    36      0.005561891763     -3.042836135282
    37    37     -0.002014192593      1.101940421075
    38    38     -0.000000017573      0.000009950211
    39    39     -0.001374970205      0.749793287930
    40    40     -0.022912385893     12.535592731108
    41    41      0.000006550212     -0.003582615352
    42    42     -0.032641487046     17.851325883339
    43    43      0.000003736249     -0.002043569221
    44    44      0.000042500301     -0.023343806962
    45    45      0.000001091461     -0.000595865678

 number of reference csfs (nref) is    45.  root number (iroot) is  3.
 c0**2 =   0.78267244  c**2 (all zwalks) =   0.94140867

 pople ci energy extrapolation is computed with 18 correlated electrons.

 eref      =   -547.032918645895   "relaxed" cnot**2         =   0.782672441358
 eci       =   -547.177597309011   deltae = eci - eref       =  -0.144678663116
 eci+dv1   =   -547.209039969654   dv1 = (1-cnot**2)*deltae  =  -0.031442660643
 eci+dv2   =   -547.217770769812   dv2 = dv1 / cnot**2       =  -0.040173460801
 eci+dv3   =   -547.233214086670   dv3 = dv1 / (2*cnot**2-1) =  -0.055616777659
 eci+pople =   -547.222592942504   ( 18e- scaled deltae )    =  -0.189674296609
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
                       Size (real*8) of d2temp for two-external contributions      74382
 bummer (warning):inf%bufszi too small ... resetting to                    1190
 
                       Size (real*8) of d2temp for all-internal contributions       2805
                       Size (real*8) of d2temp for one-external contributions      24024
                       Size (real*8) of d2temp for two-external contributions      74382
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   29    12096
                1    2    1    1   29    12096
                1    3    1    1   29    12096
                1    4    1    1   29    12096
                1    5    1    1   29    12096
                1    6    1    1   29    12096
                1    7    1    1   29    12096
                1    8    1    1   29    12096
                1    9    1    1   29    12096
                1   10    1    1   29    12096
                1   11    1    1   29    12096
                       Size (real*8) of d2temp for three-external contributions     133056
                       Size (real*8) of d2temp for four-external contributions      94395
 serial operation: forcing vdisk for temporary dd012 matrices
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=  104721
 files%d4ext =     unit=  24  vdsk=   1  filestart=  284721
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                  1191      4761     29751         1         1
d2rec                     3        21        63         3         2
recsize                1190      1190      1190     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                 404721  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -474.481013326253     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.24935119   -72.76833787
 residuum:     0.00007890
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880643    -0.00000955    -0.04318685    -0.05477766    -0.00111626    -0.07208183    -0.02412399     0.03757525
 ref:   2     0.00000210    -0.96373086    -0.00003796     0.00108880    -0.00045929     0.00486921    -0.04972163    -0.09586500
 ref:   3    -0.02552718    -0.00000072    -0.86895682     0.15401410     0.31648645     0.06066463     0.04056691    -0.00810969

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880643    -0.00000955    -0.04318685    -0.05477766    -0.00111626     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000210    -0.96373086    -0.00003796     0.00108880    -0.00045929     0.00000000     0.00000000     0.00000000
 ref:   3    -0.02552718    -0.00000072    -0.86895682     0.15401410     0.31648645     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=   13637  D0X=       0  D0W=       0
  DDZI=    3645 DDYI=    9339 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     909 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=  268506  D0X=       0  D0W=       0
  DDZI=   21120 DDYI=   52764 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    18.000000
   18  correlated and    14  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.99838268
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00185990
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00001272
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00024138
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00061751
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000005
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00331189
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00173014
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000005
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00076681
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00248567
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00037189
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00275824
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00116810
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00271856
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00039443
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00022136
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00032512
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00088776
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00031323
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00051615
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00248468
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00031396
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00004888
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00029253
   MO  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00027487
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  44     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00019436
   MO  45     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00023057
   MO  46     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00005163

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   8    -0.00185990     0.00001272     0.00024138     0.00061751     0.00000005     0.00000002    -0.00331189    -0.00173014
   MO   9     1.99204648    -0.00202387    -0.00011815    -0.00109106    -0.00000002    -0.00000001     0.00157518    -0.00684396
   MO  10    -0.00202387     1.99367727     0.00273572    -0.00187591     0.00000002     0.00000001    -0.00563778    -0.00217287
   MO  11    -0.00011815     0.00273572     1.99046991    -0.00431847    -0.00000062    -0.00000007     0.05086586    -0.00181549
   MO  12    -0.00109106    -0.00187591    -0.00431847     1.99206515    -0.00000042     0.00000000     0.03493525     0.00132118
   MO  13    -0.00000002     0.00000002    -0.00000062    -0.00000042     1.99006039    -0.00502914     0.00000696    -0.00000006
   MO  14    -0.00000001     0.00000001    -0.00000007     0.00000000    -0.00502914     1.96072995    -0.00000186    -0.00000005
   MO  15     0.00157518    -0.00563778     0.05086586     0.03493525     0.00000696    -0.00000186     1.00621215     0.00041557
   MO  16    -0.00684396    -0.00217287    -0.00181549     0.00132118    -0.00000006    -0.00000005     0.00041557     1.93265984
   MO  17     0.00000000     0.00000011    -0.00000002     0.00000009    -0.02461729    -0.10193813    -0.00000076     0.00000022
   MO  18     0.00093454    -0.00172791     0.01215655    -0.00438760    -0.00000024    -0.00000044     0.00407263     0.03333502
   MO  19     0.00200903    -0.00437461     0.01581923     0.00779607     0.00000063     0.00000249    -0.01140795     0.00728268
   MO  20    -0.00022692    -0.00140257     0.00042534    -0.00841551     0.00000010     0.00000025    -0.00500607     0.00128202
   MO  21     0.00003713    -0.00769054     0.00475869    -0.00257191    -0.00000001    -0.00000012     0.00032055    -0.00175715
   MO  22     0.00000000    -0.00000002    -0.00000002    -0.00000002    -0.00840239    -0.00027258    -0.00000012     0.00000004
   MO  23     0.00079959    -0.00192628     0.00101282     0.00062525    -0.00000018    -0.00000043    -0.00303779     0.00093443
   MO  24     0.00000001     0.00000001    -0.00000001    -0.00000006     0.00422877    -0.00173526     0.00000019     0.00000011
   MO  25    -0.00117914     0.00074063    -0.01148181    -0.00331449    -0.00000029    -0.00000037     0.01339194     0.00727750
   MO  26    -0.00000001    -0.00000005     0.00000011     0.00000001    -0.00480886     0.00460702    -0.00000022     0.00000000
   MO  27     0.00297958     0.00380401    -0.00299115     0.00692153     0.00000014     0.00000009    -0.00050555     0.00633692
   MO  28     0.00091007     0.00210543     0.00519445     0.00151542     0.00000005     0.00000011    -0.00270837     0.00280749
   MO  29     0.00047428    -0.00252812     0.00245656     0.00065089     0.00000014     0.00000042    -0.00927912     0.00398648
   MO  30    -0.00135185     0.00060733     0.00302143     0.00603756     0.00000010     0.00000021    -0.01663472    -0.00821061
   MO  31     0.00000000     0.00000000     0.00000000     0.00000001    -0.00353907     0.01091935     0.00000003     0.00000001
   MO  32     0.00000000    -0.00000001     0.00000002     0.00000001    -0.00248553     0.00192663    -0.00000018     0.00000000
   MO  33     0.00049480     0.00059275     0.00290599    -0.00075488     0.00000012     0.00000021    -0.01437626     0.00701339
   MO  34    -0.00038985     0.00239341     0.00195488     0.00140924     0.00000008    -0.00000002     0.00734705    -0.00507136
   MO  35    -0.00537855    -0.00286082     0.00018466    -0.00043941     0.00000001     0.00000001     0.00071988    -0.00493097
   MO  36    -0.00436150    -0.00026290    -0.00016156    -0.00271030    -0.00000004    -0.00000005    -0.00004942    -0.00294129
   MO  37    -0.00004722     0.00028391    -0.00036336    -0.00060155     0.00000001     0.00000000    -0.00046364    -0.00097091
   MO  38     0.00000004     0.00000001     0.00000000     0.00000001    -0.00017318     0.00014010     0.00000000    -0.00000003
   MO  39    -0.00000002     0.00000000     0.00000000    -0.00000001    -0.00061259    -0.00049189     0.00000002     0.00000001
   MO  40    -0.00104174    -0.00003579    -0.00029273     0.00010487    -0.00000002    -0.00000002    -0.00000714    -0.00024541
   MO  41    -0.00004521     0.00027338    -0.00038635    -0.00117048     0.00000001     0.00000001    -0.00044938    -0.00019876
   MO  42    -0.00000001     0.00000000     0.00000000    -0.00000001    -0.00091818    -0.00013737    -0.00000002     0.00000001
   MO  43     0.00000000     0.00000000     0.00000001     0.00000000     0.00007546    -0.00106620     0.00000001    -0.00000001
   MO  44    -0.00032236    -0.00000058    -0.00093151     0.00007704     0.00000000    -0.00000001    -0.00093217     0.00080053
   MO  45    -0.00030030    -0.00093099     0.00019538    -0.00060173     0.00000001    -0.00000002    -0.00030127    -0.00039049
   MO  46    -0.00007049     0.00005314    -0.00030471     0.00031752     0.00000002     0.00000000    -0.00013891    -0.00017203

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   8     0.00000005    -0.00076681    -0.00248567     0.00037189     0.00275824     0.00000002    -0.00116810     0.00000000
   MO   9     0.00000000     0.00093454     0.00200903    -0.00022692     0.00003713     0.00000000     0.00079959     0.00000001
   MO  10     0.00000011    -0.00172791    -0.00437461    -0.00140257    -0.00769054    -0.00000002    -0.00192628     0.00000001
   MO  11    -0.00000002     0.01215655     0.01581923     0.00042534     0.00475869    -0.00000002     0.00101282    -0.00000001
   MO  12     0.00000009    -0.00438760     0.00779607    -0.00841551    -0.00257191    -0.00000002     0.00062525    -0.00000006
   MO  13    -0.02461729    -0.00000024     0.00000063     0.00000010    -0.00000001    -0.00840239    -0.00000018     0.00422877
   MO  14    -0.10193813    -0.00000044     0.00000249     0.00000025    -0.00000012    -0.00027258    -0.00000043    -0.00173526
   MO  15    -0.00000076     0.00407263    -0.01140795    -0.00500607     0.00032055    -0.00000012    -0.00303779     0.00000019
   MO  16     0.00000022     0.03333502     0.00728268     0.00128202    -0.00175715     0.00000004     0.00093443     0.00000011
   MO  17     1.02811840     0.00000008    -0.00000050    -0.00000005     0.00000000    -0.00038570     0.00000017     0.00315556
   MO  18     0.00000008     0.07021486     0.00009708    -0.00094693    -0.00101871     0.00000001    -0.00010672     0.00000004
   MO  19    -0.00000050     0.00009708     0.00445758     0.00013211    -0.00000462     0.00000001    -0.00066398    -0.00000008
   MO  20    -0.00000005    -0.00094693     0.00013211     0.00183183     0.00071119     0.00000000     0.00022620    -0.00000001
   MO  21     0.00000000    -0.00101871    -0.00000462     0.00071119     0.00075843     0.00000000     0.00003276     0.00000000
   MO  22    -0.00038570     0.00000001     0.00000001     0.00000000     0.00000000     0.00090567     0.00000000    -0.00025989
   MO  23     0.00000017    -0.00010672    -0.00066398     0.00022620     0.00003276     0.00000000     0.00042718     0.00000002
   MO  24     0.00315556     0.00000004    -0.00000008    -0.00000001     0.00000000    -0.00025989     0.00000002     0.00512827
   MO  25     0.00000013    -0.00050406    -0.00120735     0.00048479    -0.00004252     0.00000000     0.00050215     0.00000001
   MO  26    -0.00023279     0.00000000    -0.00000001     0.00000000     0.00000000     0.00117288     0.00000000     0.00001355
   MO  27    -0.00000006    -0.00015367    -0.00049210     0.00105511     0.00018606     0.00000000     0.00022199     0.00000000
   MO  28    -0.00000001     0.00046132     0.00064279     0.00019858     0.00004429     0.00000000    -0.00008751     0.00000000
   MO  29    -0.00000008    -0.00016248     0.00084932     0.00028125     0.00021869     0.00000000    -0.00013344    -0.00000001
   MO  30    -0.00000007     0.00070968     0.00163451    -0.00052221    -0.00039747     0.00000000    -0.00018924     0.00000000
   MO  31     0.00336227     0.00000002    -0.00000006    -0.00000001     0.00000000    -0.00013847     0.00000002     0.00297205
   MO  32     0.00715803     0.00000001     0.00000002     0.00000000     0.00000000     0.00073546     0.00000000    -0.00123821
   MO  33    -0.00000007    -0.00047154     0.00135856     0.00079908     0.00011243     0.00000000     0.00015679    -0.00000001
   MO  34    -0.00000003     0.00001742    -0.00053120    -0.00041498    -0.00004234     0.00000000    -0.00013433     0.00000000
   MO  35    -0.00000002    -0.00112368    -0.00000376     0.00033266     0.00029468     0.00000000     0.00001657     0.00000000
   MO  36     0.00000003    -0.00069912    -0.00013552     0.00033063     0.00021465     0.00000000    -0.00002217     0.00000000
   MO  37     0.00000000    -0.00011037    -0.00039260    -0.00009712     0.00003962     0.00000000     0.00003348     0.00000000
   MO  38    -0.00012241     0.00000000     0.00000000     0.00000000     0.00000000     0.00004046     0.00000000     0.00019487
   MO  39    -0.00073184     0.00000000     0.00000000     0.00000000     0.00000000    -0.00004669     0.00000000     0.00039003
   MO  40     0.00000000     0.00028683    -0.00000388    -0.00006800    -0.00004958     0.00000000     0.00000066     0.00000000
   MO  41     0.00000001    -0.00008785    -0.00020839     0.00021360     0.00009378     0.00000000     0.00005342     0.00000000
   MO  42     0.00022947     0.00000000     0.00000000     0.00000000     0.00000000     0.00011650     0.00000000    -0.00038277
   MO  43    -0.00151043     0.00000000     0.00000000     0.00000000     0.00000000    -0.00012437     0.00000000    -0.00000154
   MO  44     0.00000000    -0.00010874    -0.00007359     0.00012480     0.00002993     0.00000000     0.00006384     0.00000000
   MO  45     0.00000001     0.00001569    -0.00020732    -0.00012090     0.00002791     0.00000000     0.00004523     0.00000000
   MO  46     0.00000001    -0.00005986    -0.00014192     0.00004891     0.00002674     0.00000000     0.00003237     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   8     0.00271856     0.00000000    -0.00039443     0.00022136    -0.00032512    -0.00088776     0.00000000     0.00000000
   MO   9    -0.00117914    -0.00000001     0.00297958     0.00091007     0.00047428    -0.00135185     0.00000000     0.00000000
   MO  10     0.00074063    -0.00000005     0.00380401     0.00210543    -0.00252812     0.00060733     0.00000000    -0.00000001
   MO  11    -0.01148181     0.00000011    -0.00299115     0.00519445     0.00245656     0.00302143     0.00000000     0.00000002
   MO  12    -0.00331449     0.00000001     0.00692153     0.00151542     0.00065089     0.00603756     0.00000001     0.00000001
   MO  13    -0.00000029    -0.00480886     0.00000014     0.00000005     0.00000014     0.00000010    -0.00353907    -0.00248553
   MO  14    -0.00000037     0.00460702     0.00000009     0.00000011     0.00000042     0.00000021     0.01091935     0.00192663
   MO  15     0.01339194    -0.00000022    -0.00050555    -0.00270837    -0.00927912    -0.01663472     0.00000003    -0.00000018
   MO  16     0.00727750     0.00000000     0.00633692     0.00280749     0.00398648    -0.00821061     0.00000001     0.00000000
   MO  17     0.00000013    -0.00023279    -0.00000006    -0.00000001    -0.00000008    -0.00000007     0.00336227     0.00715803
   MO  18    -0.00050406     0.00000000    -0.00015367     0.00046132    -0.00016248     0.00070968     0.00000002     0.00000001
   MO  19    -0.00120735    -0.00000001    -0.00049210     0.00064279     0.00084932     0.00163451    -0.00000006     0.00000002
   MO  20     0.00048479     0.00000000     0.00105511     0.00019858     0.00028125    -0.00052221    -0.00000001     0.00000000
   MO  21    -0.00004252     0.00000000     0.00018606     0.00004429     0.00021869    -0.00039747     0.00000000     0.00000000
   MO  22     0.00000000     0.00117288     0.00000000     0.00000000     0.00000000     0.00000000    -0.00013847     0.00073546
   MO  23     0.00050215     0.00000000     0.00022199    -0.00008751    -0.00013344    -0.00018924     0.00000002     0.00000000
   MO  24     0.00000001     0.00001355     0.00000000     0.00000000    -0.00000001     0.00000000     0.00297205    -0.00123821
   MO  25     0.00374333     0.00000001     0.00019393    -0.00022703    -0.00130615    -0.00164288     0.00000002     0.00000000
   MO  26     0.00000001     0.00555228     0.00000000     0.00000000     0.00000000     0.00000000     0.00158748     0.00287074
   MO  27     0.00019393     0.00000000     0.00276538     0.00044776    -0.00028492    -0.00024139    -0.00000001     0.00000000
   MO  28    -0.00022703     0.00000000     0.00044776     0.00072744    -0.00008167     0.00032155     0.00000000     0.00000000
   MO  29    -0.00130615     0.00000000    -0.00028492    -0.00008167     0.00129733     0.00054882    -0.00000001     0.00000000
   MO  30    -0.00164288     0.00000000    -0.00024139     0.00032155     0.00054882     0.00196435     0.00000000     0.00000000
   MO  31     0.00000002     0.00158748    -0.00000001     0.00000000    -0.00000001     0.00000000     0.00400043    -0.00056980
   MO  32     0.00000000     0.00287074     0.00000000     0.00000000     0.00000000     0.00000000    -0.00056980     0.00247816
   MO  33    -0.00003348     0.00000000     0.00014685     0.00020720     0.00047965     0.00066872    -0.00000001     0.00000000
   MO  34    -0.00025922    -0.00000001    -0.00013105    -0.00023959    -0.00006299    -0.00024477     0.00000000     0.00000000
   MO  35     0.00006342     0.00000000    -0.00061759    -0.00033682     0.00032352    -0.00016208     0.00000000     0.00000000
   MO  36     0.00008183     0.00000000    -0.00036844    -0.00005531     0.00012527    -0.00016862     0.00000000     0.00000000
   MO  37     0.00033484     0.00000000    -0.00003092    -0.00010972     0.00010040    -0.00000663     0.00000000     0.00000000
   MO  38     0.00000000    -0.00001534    -0.00000001     0.00000000     0.00000001     0.00000000     0.00006382     0.00002508
   MO  39     0.00000000     0.00000409     0.00000000     0.00000000     0.00000000     0.00000000     0.00003910    -0.00001772
   MO  40     0.00010149     0.00000000     0.00031715     0.00015923    -0.00034126     0.00019462     0.00000000     0.00000000
   MO  41     0.00000818     0.00000000     0.00021866     0.00021632    -0.00006211    -0.00000937     0.00000000     0.00000000
   MO  42     0.00000000     0.00000899     0.00000000     0.00000000     0.00000000     0.00000000    -0.00010395     0.00001566
   MO  43     0.00000000    -0.00056965     0.00000000     0.00000000     0.00000000     0.00000000    -0.00017479    -0.00030772
   MO  44     0.00040403     0.00000000     0.00017985     0.00002490    -0.00007086     0.00002363     0.00000000     0.00000000
   MO  45    -0.00005511     0.00000000    -0.00036354    -0.00022293     0.00028820    -0.00017598     0.00000000     0.00000000
   MO  46    -0.00000786     0.00000000     0.00004506     0.00012085    -0.00004526     0.00002583     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38         MO  39         MO  40
   MO   8     0.00031323    -0.00051615    -0.00248468     0.00031396    -0.00004888     0.00000000     0.00000000    -0.00029253
   MO   9     0.00049480    -0.00038985    -0.00537855    -0.00436150    -0.00004722     0.00000004    -0.00000002    -0.00104174
   MO  10     0.00059275     0.00239341    -0.00286082    -0.00026290     0.00028391     0.00000001     0.00000000    -0.00003579
   MO  11     0.00290599     0.00195488     0.00018466    -0.00016156    -0.00036336     0.00000000     0.00000000    -0.00029273
   MO  12    -0.00075488     0.00140924    -0.00043941    -0.00271030    -0.00060155     0.00000001    -0.00000001     0.00010487
   MO  13     0.00000012     0.00000008     0.00000001    -0.00000004     0.00000001    -0.00017318    -0.00061259    -0.00000002
   MO  14     0.00000021    -0.00000002     0.00000001    -0.00000005     0.00000000     0.00014010    -0.00049189    -0.00000002
   MO  15    -0.01437626     0.00734705     0.00071988    -0.00004942    -0.00046364     0.00000000     0.00000002    -0.00000714
   MO  16     0.00701339    -0.00507136    -0.00493097    -0.00294129    -0.00097091    -0.00000003     0.00000001    -0.00024541
   MO  17    -0.00000007    -0.00000003    -0.00000002     0.00000003     0.00000000    -0.00012241    -0.00073184     0.00000000
   MO  18    -0.00047154     0.00001742    -0.00112368    -0.00069912    -0.00011037     0.00000000     0.00000000     0.00028683
   MO  19     0.00135856    -0.00053120    -0.00000376    -0.00013552    -0.00039260     0.00000000     0.00000000    -0.00000388
   MO  20     0.00079908    -0.00041498     0.00033266     0.00033063    -0.00009712     0.00000000     0.00000000    -0.00006800
   MO  21     0.00011243    -0.00004234     0.00029468     0.00021465     0.00003962     0.00000000     0.00000000    -0.00004958
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00004046    -0.00004669     0.00000000
   MO  23     0.00015679    -0.00013433     0.00001657    -0.00002217     0.00003348     0.00000000     0.00000000     0.00000066
   MO  24    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00019487     0.00039003     0.00000000
   MO  25    -0.00003348    -0.00025922     0.00006342     0.00008183     0.00033484     0.00000000     0.00000000     0.00010149
   MO  26     0.00000000    -0.00000001     0.00000000     0.00000000     0.00000000    -0.00001534     0.00000409     0.00000000
   MO  27     0.00014685    -0.00013105    -0.00061759    -0.00036844    -0.00003092    -0.00000001     0.00000000     0.00031715
   MO  28     0.00020720    -0.00023959    -0.00033682    -0.00005531    -0.00010972     0.00000000     0.00000000     0.00015923
   MO  29     0.00047965    -0.00006299     0.00032352     0.00012527     0.00010040     0.00000001     0.00000000    -0.00034126
   MO  30     0.00066872    -0.00024477    -0.00016208    -0.00016862    -0.00000663     0.00000000     0.00000000     0.00019462
   MO  31    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00006382     0.00003910     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002508    -0.00001772     0.00000000
   MO  33     0.00141412    -0.00045179     0.00001756     0.00001656    -0.00004549     0.00000000     0.00000000    -0.00007278
   MO  34    -0.00045179     0.00063110     0.00002038    -0.00004927     0.00004967     0.00000000     0.00000000    -0.00011606
   MO  35     0.00001756     0.00002038     0.00089600     0.00051309     0.00003166     0.00000000     0.00000000    -0.00004246
   MO  36     0.00001656    -0.00004927     0.00051309     0.00045246    -0.00008105     0.00000000     0.00000000     0.00003864
   MO  37    -0.00004549     0.00004967     0.00003166    -0.00008105     0.00095135     0.00000000     0.00000000    -0.00003566
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00088968    -0.00026526     0.00000000
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00026526     0.00053453     0.00000000
   MO  40    -0.00007278    -0.00011606    -0.00004246     0.00003864    -0.00003566     0.00000000     0.00000000     0.00121696
   MO  41     0.00000919    -0.00007395    -0.00000732     0.00005954    -0.00000146     0.00000000     0.00000000     0.00017247
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00014455    -0.00030219     0.00000000
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00007522    -0.00003321     0.00000000
   MO  44     0.00009330    -0.00010165     0.00006751     0.00001425     0.00041744     0.00000000     0.00000000     0.00016315
   MO  45     0.00008769     0.00015297    -0.00019456    -0.00020901     0.00015219     0.00000001     0.00000000    -0.00040792
   MO  46    -0.00001165    -0.00002249     0.00003135     0.00006418    -0.00000658     0.00000000     0.00000000     0.00005868

                MO  41         MO  42         MO  43         MO  44         MO  45         MO  46
   MO   8     0.00027487     0.00000000     0.00000001     0.00019436     0.00023057     0.00005163
   MO   9    -0.00004521    -0.00000001     0.00000000    -0.00032236    -0.00030030    -0.00007049
   MO  10     0.00027338     0.00000000     0.00000000    -0.00000058    -0.00093099     0.00005314
   MO  11    -0.00038635     0.00000000     0.00000001    -0.00093151     0.00019538    -0.00030471
   MO  12    -0.00117048    -0.00000001     0.00000000     0.00007704    -0.00060173     0.00031752
   MO  13     0.00000001    -0.00091818     0.00007546     0.00000000     0.00000001     0.00000002
   MO  14     0.00000001    -0.00013737    -0.00106620    -0.00000001    -0.00000002     0.00000000
   MO  15    -0.00044938    -0.00000002     0.00000001    -0.00093217    -0.00030127    -0.00013891
   MO  16    -0.00019876     0.00000001    -0.00000001     0.00080053    -0.00039049    -0.00017203
   MO  17     0.00000001     0.00022947    -0.00151043     0.00000000     0.00000001     0.00000001
   MO  18    -0.00008785     0.00000000     0.00000000    -0.00010874     0.00001569    -0.00005986
   MO  19    -0.00020839     0.00000000     0.00000000    -0.00007359    -0.00020732    -0.00014192
   MO  20     0.00021360     0.00000000     0.00000000     0.00012480    -0.00012090     0.00004891
   MO  21     0.00009378     0.00000000     0.00000000     0.00002993     0.00002791     0.00002674
   MO  22     0.00000000     0.00011650    -0.00012437     0.00000000     0.00000000     0.00000000
   MO  23     0.00005342     0.00000000     0.00000000     0.00006384     0.00004523     0.00003237
   MO  24     0.00000000    -0.00038277    -0.00000154     0.00000000     0.00000000     0.00000000
   MO  25     0.00000818     0.00000000     0.00000000     0.00040403    -0.00005511    -0.00000786
   MO  26     0.00000000     0.00000899    -0.00056965     0.00000000     0.00000000     0.00000000
   MO  27     0.00021866     0.00000000     0.00000000     0.00017985    -0.00036354     0.00004506
   MO  28     0.00021632     0.00000000     0.00000000     0.00002490    -0.00022293     0.00012085
   MO  29    -0.00006211     0.00000000     0.00000000    -0.00007086     0.00028820    -0.00004526
   MO  30    -0.00000937     0.00000000     0.00000000     0.00002363    -0.00017598     0.00002583
   MO  31     0.00000000    -0.00010395    -0.00017479     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00001566    -0.00030772     0.00000000     0.00000000     0.00000000
   MO  33     0.00000919     0.00000000     0.00000000     0.00009330     0.00008769    -0.00001165
   MO  34    -0.00007395     0.00000000     0.00000000    -0.00010165     0.00015297    -0.00002249
   MO  35    -0.00000732     0.00000000     0.00000000     0.00006751    -0.00019456     0.00003135
   MO  36     0.00005954     0.00000000     0.00000000     0.00001425    -0.00020901     0.00006418
   MO  37    -0.00000146     0.00000000     0.00000000     0.00041744     0.00015219    -0.00000658
   MO  38     0.00000000    -0.00014455     0.00007522     0.00000000     0.00000001     0.00000000
   MO  39     0.00000000    -0.00030219    -0.00003321     0.00000000     0.00000000     0.00000000
   MO  40     0.00017247     0.00000000     0.00000000     0.00016315    -0.00040792     0.00005868
   MO  41     0.00035365     0.00000000     0.00000000     0.00006019    -0.00009696     0.00012041
   MO  42     0.00000000     0.00067752    -0.00004283     0.00000000     0.00000000     0.00000000
   MO  43     0.00000000    -0.00004283     0.00008641     0.00000000     0.00000000     0.00000000
   MO  44     0.00006019     0.00000000     0.00000000     0.00029820    -0.00013522     0.00002060
   MO  45    -0.00009696     0.00000000     0.00000000    -0.00013522     0.00082552    -0.00011386
   MO  46     0.00012041     0.00000000     0.00000000     0.00002060    -0.00011386     0.00009777

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99904196
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99829243     1.99389630     1.99106659     1.99086493     1.98997501     1.97151545     1.93246057     1.01654761
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.00335050     0.06959533     0.00804972     0.00772986     0.00665513     0.00414098     0.00323357     0.00262102
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00166936     0.00150698     0.00132325     0.00104755     0.00093680     0.00083191     0.00070798     0.00069165
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00049210     0.00036610     0.00029837     0.00021045     0.00020227     0.00017719     0.00015751     0.00013364
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO
  occ(*)=     0.00009149     0.00004300     0.00003202     0.00001663     0.00001412     0.00001264


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     1_ s       2.000501   0.000166   0.000087   2.000004   0.000000   0.000131
     1_ p       0.000134   0.000236   0.000149   0.000000   2.000010   1.999807
     1_ d       0.000001   0.000015   0.000016   0.000000   0.000000   0.000001
     3_ s       0.000033   0.000000   1.999749   0.000000   0.000000   0.000002
     3_ p      -0.000093  -0.000001   0.000001   0.000000   0.000002   0.000005
     3_ d      -0.000003   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s      -0.000055   1.999583   0.000000   0.000000   0.000000   0.000019
     4_ p      -0.000500   0.000003  -0.000001  -0.000003  -0.000013   0.000020
     4_ d      -0.000016   0.000000   0.000000   0.000000   0.000001   0.000015
 
   ao class       7a         8a         9a        10a        11a        12a  
     1_ s       0.000003   0.215267   0.148748   0.852629   0.000000   0.262443
     1_ p       2.000012   0.209181   0.188801   0.205571   0.950703   0.693513
     1_ d       0.000003   0.045504   0.008653   0.017417   0.028366   0.037638
     3_ s      -0.000008   0.002503   0.000633   0.589685   0.000000   0.003255
     3_ p      -0.000018   0.001073   0.001685   0.030199   0.093496   0.045154
     3_ d       0.000000   0.000064   0.000044   0.001669   0.000786   0.000603
     4_ s      -0.000006   1.387735   0.454214   0.020441   0.000000   0.001056
     4_ p      -0.000005   0.134703   1.190265   0.275421   0.912280   0.943183
     4_ d       0.000019   0.003012   0.005251   0.000864   0.005435   0.004019
 
   ao class      13a        14a        15a        16a        17a        18a  
     1_ s       0.304783   0.000000   0.005878   0.000000   0.004465   0.002031
     1_ p       0.020437   0.010485   0.400867   0.528064   0.007368   0.035497
     1_ d       0.016683   0.037291   0.067593   0.022714   0.008806  -0.000002
     3_ s       1.354471   0.000000  -0.001421   0.000000   0.000022   0.000371
     3_ p       0.095013   1.668083   1.208686   0.094508   0.956727   0.026014
     3_ d       0.000372   0.000824   0.002844   0.000150   0.000327   0.000168
     4_ s       0.005739   0.000000   0.000467   0.000000  -0.000406   0.000014
     4_ p       0.191815   0.254535   0.247804   0.371427   0.025996   0.005473
     4_ d       0.000661   0.000297  -0.000257  -0.000316   0.000045   0.000030
 
   ao class      19a        20a        21a        22a        23a        24a  
     1_ s       0.000000   0.000000   0.000185   0.000015   0.000062   0.000156
     1_ p       0.000008   0.000925   0.001488   0.000769   0.000245   0.000108
     1_ d       0.002886   0.002625   0.001468   0.002044   0.001806   0.000531
     3_ s       0.000000   0.000000   0.000000   0.000085   0.000503   0.001153
     3_ p       0.003658   0.001754   0.002597   0.000232   0.000063   0.000381
     3_ d       0.000060   0.000022   0.000134   0.000304   0.000049   0.000283
     4_ s       0.000000   0.000000  -0.000026   0.000001   0.000000  -0.000004
     4_ p       0.001372   0.002323   0.000789   0.000657   0.000489   0.000006
     4_ d       0.000066   0.000080   0.000018   0.000033   0.000016   0.000006
 
   ao class      25a        26a        27a        28a        29a        30a  
     1_ s       0.000013   0.000000   0.000008   0.000000   0.000047   0.000000
     1_ p      -0.000002  -0.000008   0.000017   0.000014   0.000137   0.000125
     1_ d       0.000050   0.000753   0.000051   0.000006   0.000070   0.000159
     3_ s       0.000241   0.000000   0.000012   0.000000   0.000000   0.000000
     3_ p       0.000056   0.000421   0.000254   0.000000   0.000278   0.000009
     3_ d       0.001280   0.000335   0.000964   0.001027   0.000172   0.000525
     4_ s       0.000007   0.000000   0.000000   0.000000   0.000024   0.000000
     4_ p       0.000020   0.000002   0.000016   0.000001   0.000176   0.000013
     4_ d       0.000004   0.000003   0.000001   0.000000   0.000033   0.000001
 
   ao class      31a        32a        33a        34a        35a        36a  
     1_ s       0.000102   0.000030   0.000000   0.000023   0.000102   0.000006
     1_ p       0.000041   0.000006   0.000358   0.000011   0.000044   0.000044
     1_ d       0.000175   0.000011   0.000014   0.000103   0.000005   0.000042
     3_ s       0.000011  -0.000001   0.000000   0.000002   0.000007   0.000000
     3_ p      -0.000001   0.000031   0.000020   0.000061   0.000009   0.000004
     3_ d       0.000024   0.000601   0.000074   0.000006   0.000005   0.000002
     4_ s       0.000127   0.000000   0.000000   0.000009   0.000009   0.000024
     4_ p       0.000070   0.000012   0.000026   0.000148   0.000073   0.000018
     4_ d       0.000159   0.000002   0.000000   0.000002   0.000045   0.000069
 
   ao class      37a        38a        39a        40a        41a        42a  
     1_ s       0.000000   0.000001   0.000000  -0.000002   0.000015   0.000003
     1_ p      -0.000003   0.000048   0.000000   0.000014   0.000027   0.000022
     1_ d       0.000093   0.000017   0.000002   0.000024   0.000006   0.000004
     3_ s       0.000000   0.000001   0.000000   0.000010   0.000003   0.000003
     3_ p       0.000000   0.000022   0.000000   0.000041   0.000011   0.000000
     3_ d       0.000001   0.000006   0.000000   0.000010   0.000003   0.000000
     4_ s       0.000000   0.000007   0.000000   0.000012   0.000005   0.000000
     4_ p       0.000101   0.000031   0.000002   0.000013   0.000011   0.000011
     4_ d       0.000011   0.000045   0.000153   0.000010   0.000010   0.000000
 
   ao class      43a        44a        45a        46a  
     1_ s       0.000001   0.000000   0.000000   0.000000
     1_ p       0.000003   0.000002   0.000000   0.000001
     1_ d       0.000000   0.000001   0.000001   0.000000
     3_ s       0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000
     4_ s       0.000000   0.000003   0.000000   0.000004
     4_ p       0.000001   0.000001   0.000000   0.000002
     4_ d       0.000027   0.000008   0.000013   0.000005


                        gross atomic populations
     ao            1_         3_         4_
      s         5.797904   3.951326   3.869003
      p         9.255280   4.230438   4.558783
      d         0.303645   0.013737   0.019885
    total      15.356829   8.195501   8.447670
 

 Total number of electrons:   32.00000000

 item #                     2 suffix=:.drt1.state2:
 read_civout: repnuc=  -474.481013326253     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.24935119   -72.76833787
 residuum:     0.00007890
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -547.24077884   -72.75976552
 residuum:     0.00007190
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880643    -0.00000955    -0.04318685    -0.05477766    -0.00111626    -0.07208183    -0.02412399     0.03757525
 ref:   2     0.00000210    -0.96373086    -0.00003796     0.00108880    -0.00045929     0.00486921    -0.04972163    -0.09586500
 ref:   3    -0.02552718    -0.00000072    -0.86895682     0.15401410     0.31648645     0.06066463     0.04056691    -0.00810969

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880643    -0.00000955    -0.04318685    -0.05477766    -0.00111626     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000210    -0.96373086    -0.00003796     0.00108880    -0.00045929     0.00000000     0.00000000     0.00000000
 ref:   3    -0.02552718    -0.00000072    -0.86895682     0.15401410     0.31648645     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=   13637  D0X=       0  D0W=       0
  DDZI=    3645 DDYI=    9339 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     909 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=  268506  D0X=       0  D0W=       0
  DDZI=   21120 DDYI=   52764 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    18.000000
   18  correlated and    14  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.99850439
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00177942
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002319
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00040705
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00028861
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000065
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000342
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00019868
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00189810
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000163
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00064809
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00338743
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00086528
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00308379
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000095
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00300938
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000036
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00063795
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000042
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00153014
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00168942
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00091968
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00075210
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000032
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000014
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00117137
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00166823
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00012223
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00161009
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00007601
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000039
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000009
   MO  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00037406
   MO  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00034742
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000009
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000063
   MO  44     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00009503
   MO  45     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00016424
   MO  46     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00021696

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   8    -0.00177942     0.00002319    -0.00040705     0.00028861     0.00000065     0.00000342    -0.00019868    -0.00189810
   MO   9     1.99250848    -0.00165877     0.00014245    -0.00050082     0.00000064     0.00000466     0.00024710    -0.00682956
   MO  10    -0.00165877     1.99429753     0.00242111    -0.00170817    -0.00000067     0.00000306    -0.00105529    -0.00160745
   MO  11     0.00014245     0.00242111     1.99750138     0.00024329    -0.00001639    -0.00008648     0.00160364     0.00031472
   MO  12    -0.00050082    -0.00170817     0.00024329     1.99587273     0.00000026     0.00000325     0.00039209     0.00427208
   MO  13     0.00000064    -0.00000067    -0.00001639     0.00000026     1.96794454    -0.10249331     0.00000259    -0.00000006
   MO  14     0.00000466     0.00000306    -0.00008648     0.00000325    -0.10249331     1.01754374    -0.00000351    -0.00000581
   MO  15     0.00024710    -0.00105529     0.00160364     0.00039209     0.00000259    -0.00000351     1.98190622    -0.00090190
   MO  16    -0.00682956    -0.00160745     0.00031472     0.00427208    -0.00000006    -0.00000581    -0.00090190     1.93363007
   MO  17     0.00000023    -0.00001088    -0.00003534    -0.00000423    -0.07479530     0.01065824     0.00000077    -0.00001933
   MO  18     0.00084466    -0.00556082     0.00473248    -0.02372505    -0.00000154    -0.00000944     0.00228846    -0.01323672
   MO  19    -0.00114300     0.00086539    -0.01901544     0.00844251     0.00000093    -0.00000211     0.04705563     0.01335114
   MO  20    -0.00044699    -0.00094344    -0.00011940    -0.00241748    -0.00000005     0.00000295    -0.00025291     0.00945465
   MO  21     0.00036315    -0.00989498     0.00523607     0.00122197     0.00000000     0.00000004    -0.00325825    -0.00024694
   MO  22     0.00000031     0.00000020    -0.00000132     0.00000012    -0.01153341    -0.00201228     0.00000014    -0.00000063
   MO  23     0.00152067     0.00065998     0.01343181    -0.00036735    -0.00000068     0.00000059    -0.01095754     0.00289579
   MO  24     0.00000015    -0.00000141     0.00000402    -0.00000037     0.01258786     0.01650901     0.00000163    -0.00000159
   MO  25    -0.00031226     0.00283836    -0.00369460     0.01028413    -0.00000029     0.00000169    -0.01110207     0.01926351
   MO  26     0.00000040    -0.00000206    -0.00000369    -0.00000136    -0.00985512     0.01556743    -0.00000067    -0.00000076
   MO  27    -0.00004060     0.00215406    -0.00235326     0.00613344    -0.00000032     0.00000106     0.00119484     0.00927702
   MO  28     0.00128137     0.00201160     0.00207280     0.00140382    -0.00000043    -0.00000132     0.00026237     0.00316942
   MO  29     0.00052143    -0.00161547    -0.00092709    -0.00167198     0.00000044     0.00000211     0.00319166     0.00557441
   MO  30    -0.00153259     0.00008422    -0.00181459    -0.00172302    -0.00000005    -0.00000244     0.00175062    -0.01305306
   MO  31    -0.00000014    -0.00000145     0.00000206    -0.00000093     0.00329952     0.02675314     0.00000117    -0.00000025
   MO  32    -0.00000008    -0.00000102    -0.00000212     0.00000003    -0.00378489     0.00177724    -0.00000046    -0.00000074
   MO  33     0.00033015    -0.00035036    -0.00085364     0.00383130    -0.00000036    -0.00000051    -0.00124997     0.01142057
   MO  34    -0.00174867    -0.00175600    -0.00354017    -0.00147147    -0.00000034    -0.00000088     0.00628254    -0.00763044
   MO  35    -0.00457834    -0.00117635     0.00002922    -0.00077461    -0.00000011     0.00000036     0.00103984    -0.00547776
   MO  36    -0.00175712    -0.00054137    -0.00002034    -0.00039263     0.00000020     0.00000039    -0.00151082    -0.00258948
   MO  37    -0.00018613     0.00016868    -0.00080770    -0.00085695     0.00000012     0.00000005    -0.00010942    -0.00132415
   MO  38    -0.00000046    -0.00000012     0.00000026     0.00000028     0.00045721     0.00021106    -0.00000048     0.00000082
   MO  39    -0.00000001    -0.00000020     0.00000033     0.00000065    -0.00061518    -0.00020926     0.00000028     0.00000075
   MO  40     0.00093627    -0.00071022     0.00017858    -0.00080493     0.00000017     0.00000104    -0.00002647     0.00010378
   MO  41    -0.00005866     0.00005534    -0.00004105    -0.00001107    -0.00000073    -0.00000063    -0.00032538     0.00036802
   MO  42     0.00000036     0.00000022    -0.00000029     0.00000059    -0.00038928     0.00070986    -0.00000021    -0.00000012
   MO  43    -0.00000028    -0.00000012    -0.00000067     0.00000031    -0.00018603    -0.00120139     0.00000020     0.00000025
   MO  44    -0.00020807     0.00024377    -0.00010022     0.00077757    -0.00000004     0.00000000    -0.00062293     0.00141901
   MO  45    -0.00019678    -0.00099897     0.00023399    -0.00050578    -0.00000015     0.00000035    -0.00035309    -0.00063538
   MO  46    -0.00010758    -0.00027450    -0.00046778     0.00018324    -0.00000032    -0.00000036    -0.00036577    -0.00022213

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   8     0.00000163    -0.00064809     0.00338743     0.00086528     0.00308379    -0.00000095    -0.00300938    -0.00000036
   MO   9     0.00000023     0.00084466    -0.00114300    -0.00044699     0.00036315     0.00000031     0.00152067     0.00000015
   MO  10    -0.00001088    -0.00556082     0.00086539    -0.00094344    -0.00989498     0.00000020     0.00065998    -0.00000141
   MO  11    -0.00003534     0.00473248    -0.01901544    -0.00011940     0.00523607    -0.00000132     0.01343181     0.00000402
   MO  12    -0.00000423    -0.02372505     0.00844251    -0.00241748     0.00122197     0.00000012    -0.00036735    -0.00000037
   MO  13    -0.07479530    -0.00000154     0.00000093    -0.00000005     0.00000000    -0.01153341    -0.00000068     0.01258786
   MO  14     0.01065824    -0.00000944    -0.00000211     0.00000295     0.00000004    -0.00201228     0.00000059     0.01650901
   MO  15     0.00000077     0.00228846     0.04705563    -0.00025291    -0.00325825     0.00000014    -0.01095754     0.00000163
   MO  16    -0.00001933    -0.01323672     0.01335114     0.00945465    -0.00024694    -0.00000063     0.00289579    -0.00000159
   MO  17     1.00263917     0.00001866    -0.00000180    -0.00000456    -0.00000052     0.00095929    -0.00000005    -0.00436553
   MO  18     0.00001866     0.07011907    -0.00043645    -0.00137504    -0.00107472     0.00000016    -0.00027321     0.00000043
   MO  19    -0.00000180    -0.00043645     0.00832592     0.00052368    -0.00033563    -0.00000004    -0.00125369     0.00000008
   MO  20    -0.00000456    -0.00137504     0.00052368     0.00187581     0.00066773     0.00000001     0.00025378    -0.00000014
   MO  21    -0.00000052    -0.00107472    -0.00033563     0.00066773     0.00076085    -0.00000001     0.00012991    -0.00000003
   MO  22     0.00095929     0.00000016    -0.00000004     0.00000001    -0.00000001     0.00090914     0.00000002    -0.00024289
   MO  23    -0.00000005    -0.00027321    -0.00125369     0.00025378     0.00012991     0.00000002     0.00052642     0.00000002
   MO  24    -0.00436553     0.00000043     0.00000008    -0.00000014    -0.00000003    -0.00024289     0.00000002     0.00373750
   MO  25    -0.00000117    -0.00055361    -0.00270162     0.00040091     0.00006257     0.00000002     0.00078039     0.00000000
   MO  26     0.01045416    -0.00000019    -0.00000009     0.00000002    -0.00000003     0.00123275     0.00000006    -0.00042944
   MO  27    -0.00000125    -0.00096467    -0.00003680     0.00118203     0.00025729     0.00000005     0.00016503    -0.00000019
   MO  28    -0.00000200    -0.00016351     0.00076209     0.00037124     0.00007657    -0.00000003    -0.00001863    -0.00000004
   MO  29    -0.00000429    -0.00076559     0.00184635     0.00039635     0.00014368     0.00000000    -0.00029582    -0.00000007
   MO  30     0.00000255     0.00097276     0.00284778    -0.00038241    -0.00045195     0.00000001    -0.00044359     0.00000018
   MO  31     0.00310996    -0.00000003     0.00000005    -0.00000005    -0.00000002    -0.00009619     0.00000001     0.00159292
   MO  32     0.01224586     0.00000000    -0.00000011     0.00000002     0.00000001     0.00075136     0.00000004    -0.00084180
   MO  33    -0.00000090    -0.00069913     0.00233712     0.00087426     0.00003995     0.00000002     0.00003597     0.00000004
   MO  34     0.00000100     0.00072766    -0.00061441    -0.00062512    -0.00012996     0.00000001    -0.00016148     0.00000002
   MO  35     0.00000015    -0.00066747     0.00002233     0.00024615     0.00023855     0.00000000    -0.00001041     0.00000005
   MO  36    -0.00000013    -0.00063209    -0.00029170     0.00029690     0.00020290     0.00000000     0.00003605     0.00000002
   MO  37     0.00000020     0.00000062    -0.00032308    -0.00009022     0.00000999     0.00000000    -0.00001211    -0.00000001
   MO  38     0.00015589     0.00000010     0.00000003    -0.00000002    -0.00000001     0.00003166     0.00000000     0.00020658
   MO  39    -0.00040450     0.00000000     0.00000002     0.00000002     0.00000001    -0.00002950     0.00000001     0.00040674
   MO  40    -0.00000011    -0.00008478    -0.00019168    -0.00006454     0.00000831     0.00000000     0.00000710     0.00000004
   MO  41    -0.00000106    -0.00015792    -0.00026571     0.00020834     0.00010613    -0.00000001     0.00006732     0.00000000
   MO  42    -0.00022673    -0.00000019    -0.00000001     0.00000002     0.00000000     0.00009161     0.00000002    -0.00040737
   MO  43    -0.00197424     0.00000001    -0.00000001     0.00000000     0.00000000    -0.00011665     0.00000000     0.00004920
   MO  44     0.00000003    -0.00013685    -0.00006175     0.00014666     0.00002997     0.00000000     0.00005444     0.00000001
   MO  45    -0.00000001     0.00027964    -0.00017916    -0.00016149     0.00000032     0.00000000     0.00003417     0.00000002
   MO  46    -0.00000033    -0.00009935    -0.00016719     0.00005951     0.00003349    -0.00000001     0.00004317     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   8     0.00063795     0.00000042     0.00153014    -0.00168942     0.00091968    -0.00075210     0.00000032     0.00000014
   MO   9    -0.00031226     0.00000040    -0.00004060     0.00128137     0.00052143    -0.00153259    -0.00000014    -0.00000008
   MO  10     0.00283836    -0.00000206     0.00215406     0.00201160    -0.00161547     0.00008422    -0.00000145    -0.00000102
   MO  11    -0.00369460    -0.00000369    -0.00235326     0.00207280    -0.00092709    -0.00181459     0.00000206    -0.00000212
   MO  12     0.01028413    -0.00000136     0.00613344     0.00140382    -0.00167198    -0.00172302    -0.00000093     0.00000003
   MO  13    -0.00000029    -0.00985512    -0.00000032    -0.00000043     0.00000044    -0.00000005     0.00329952    -0.00378489
   MO  14     0.00000169     0.01556743     0.00000106    -0.00000132     0.00000211    -0.00000244     0.02675314     0.00177724
   MO  15    -0.01110207    -0.00000067     0.00119484     0.00026237     0.00319166     0.00175062     0.00000117    -0.00000046
   MO  16     0.01926351    -0.00000076     0.00927702     0.00316942     0.00557441    -0.01305306    -0.00000025    -0.00000074
   MO  17    -0.00000117     0.01045416    -0.00000125    -0.00000200    -0.00000429     0.00000255     0.00310996     0.01224586
   MO  18    -0.00055361    -0.00000019    -0.00096467    -0.00016351    -0.00076559     0.00097276    -0.00000003     0.00000000
   MO  19    -0.00270162    -0.00000009    -0.00003680     0.00076209     0.00184635     0.00284778     0.00000005    -0.00000011
   MO  20     0.00040091     0.00000002     0.00118203     0.00037124     0.00039635    -0.00038241    -0.00000005     0.00000002
   MO  21     0.00006257    -0.00000003     0.00025729     0.00007657     0.00014368    -0.00045195    -0.00000002     0.00000001
   MO  22     0.00000002     0.00123275     0.00000005    -0.00000003     0.00000000     0.00000001    -0.00009619     0.00075136
   MO  23     0.00078039     0.00000006     0.00016503    -0.00001863    -0.00029582    -0.00044359     0.00000001     0.00000004
   MO  24     0.00000000    -0.00042944    -0.00000019    -0.00000004    -0.00000007     0.00000018     0.00159292    -0.00084180
   MO  25     0.00464776     0.00000001     0.00039003    -0.00029559    -0.00171801    -0.00231470    -0.00000009     0.00000001
   MO  26     0.00000001     0.00550215     0.00000000    -0.00000005     0.00000009    -0.00000007     0.00128896     0.00294209
   MO  27     0.00039003     0.00000000     0.00264095     0.00046243    -0.00016302    -0.00030344    -0.00000010     0.00000007
   MO  28    -0.00029559    -0.00000005     0.00046243     0.00070066     0.00006640     0.00033533    -0.00000002    -0.00000001
   MO  29    -0.00171801     0.00000009    -0.00016302     0.00006640     0.00147600     0.00090045     0.00000004     0.00000001
   MO  30    -0.00231470    -0.00000007    -0.00030344     0.00033533     0.00090045     0.00236202     0.00000008    -0.00000005
   MO  31    -0.00000009     0.00128896    -0.00000010    -0.00000002     0.00000004     0.00000008     0.00257151    -0.00012105
   MO  32     0.00000001     0.00294209     0.00000007    -0.00000001     0.00000001    -0.00000005    -0.00012105     0.00216965
   MO  33    -0.00042831    -0.00000007     0.00023969     0.00029683     0.00073272     0.00101047     0.00000000    -0.00000006
   MO  34    -0.00018533    -0.00000012    -0.00014106    -0.00032975    -0.00017347    -0.00020186    -0.00000003    -0.00000004
   MO  35    -0.00000062    -0.00000001    -0.00054502    -0.00026342     0.00027573    -0.00009039     0.00000002    -0.00000001
   MO  36     0.00011562     0.00000000    -0.00034216    -0.00005607     0.00011112    -0.00020566     0.00000001     0.00000000
   MO  37     0.00020223     0.00000001     0.00008148    -0.00006055    -0.00002137     0.00003366    -0.00000001     0.00000001
   MO  38    -0.00000001    -0.00000939     0.00000001     0.00000001    -0.00000002     0.00000002     0.00003088     0.00003782
   MO  39     0.00000007    -0.00002588    -0.00000001     0.00000000     0.00000001    -0.00000002    -0.00000930    -0.00000469
   MO  40     0.00004763     0.00000001    -0.00032839    -0.00022133     0.00025855    -0.00024334     0.00000002    -0.00000001
   MO  41     0.00000055    -0.00000011     0.00011273     0.00018983     0.00003082    -0.00007486    -0.00000002    -0.00000005
   MO  42     0.00000004     0.00004941     0.00000004     0.00000000     0.00000000    -0.00000002    -0.00000938    -0.00002018
   MO  43     0.00000000    -0.00050520    -0.00000001     0.00000003    -0.00000001     0.00000000    -0.00013348    -0.00027303
   MO  44     0.00032877     0.00000000     0.00015841     0.00002287    -0.00005853    -0.00000138    -0.00000001     0.00000000
   MO  45    -0.00010099    -0.00000004    -0.00030851    -0.00015979     0.00019745    -0.00011296     0.00000002    -0.00000003
   MO  46    -0.00000164    -0.00000006     0.00002655     0.00011743    -0.00003376     0.00001027    -0.00000002    -0.00000003

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38         MO  39         MO  40
   MO   8     0.00117137     0.00166823    -0.00012223    -0.00161009    -0.00007601    -0.00000039    -0.00000009     0.00037406
   MO   9     0.00033015    -0.00174867    -0.00457834    -0.00175712    -0.00018613    -0.00000046    -0.00000001     0.00093627
   MO  10    -0.00035036    -0.00175600    -0.00117635    -0.00054137     0.00016868    -0.00000012    -0.00000020    -0.00071022
   MO  11    -0.00085364    -0.00354017     0.00002922    -0.00002034    -0.00080770     0.00000026     0.00000033     0.00017858
   MO  12     0.00383130    -0.00147147    -0.00077461    -0.00039263    -0.00085695     0.00000028     0.00000065    -0.00080493
   MO  13    -0.00000036    -0.00000034    -0.00000011     0.00000020     0.00000012     0.00045721    -0.00061518     0.00000017
   MO  14    -0.00000051    -0.00000088     0.00000036     0.00000039     0.00000005     0.00021106    -0.00020926     0.00000104
   MO  15    -0.00124997     0.00628254     0.00103984    -0.00151082    -0.00010942    -0.00000048     0.00000028    -0.00002647
   MO  16     0.01142057    -0.00763044    -0.00547776    -0.00258948    -0.00132415     0.00000082     0.00000075     0.00010378
   MO  17    -0.00000090     0.00000100     0.00000015    -0.00000013     0.00000020     0.00015589    -0.00040450    -0.00000011
   MO  18    -0.00069913     0.00072766    -0.00066747    -0.00063209     0.00000062     0.00000010     0.00000000    -0.00008478
   MO  19     0.00233712    -0.00061441     0.00002233    -0.00029170    -0.00032308     0.00000003     0.00000002    -0.00019168
   MO  20     0.00087426    -0.00062512     0.00024615     0.00029690    -0.00009022    -0.00000002     0.00000002    -0.00006454
   MO  21     0.00003995    -0.00012996     0.00023855     0.00020290     0.00000999    -0.00000001     0.00000001     0.00000831
   MO  22     0.00000002     0.00000001     0.00000000     0.00000000     0.00000000     0.00003166    -0.00002950     0.00000000
   MO  23     0.00003597    -0.00016148    -0.00001041     0.00003605    -0.00001211     0.00000000     0.00000001     0.00000710
   MO  24     0.00000004     0.00000002     0.00000005     0.00000002    -0.00000001     0.00020658     0.00040674     0.00000004
   MO  25    -0.00042831    -0.00018533    -0.00000062     0.00011562     0.00020223    -0.00000001     0.00000007     0.00004763
   MO  26    -0.00000007    -0.00000012    -0.00000001     0.00000000     0.00000001    -0.00000939    -0.00002588     0.00000001
   MO  27     0.00023969    -0.00014106    -0.00054502    -0.00034216     0.00008148     0.00000001    -0.00000001    -0.00032839
   MO  28     0.00029683    -0.00032975    -0.00026342    -0.00005607    -0.00006055     0.00000001     0.00000000    -0.00022133
   MO  29     0.00073272    -0.00017347     0.00027573     0.00011112    -0.00002137    -0.00000002     0.00000001     0.00025855
   MO  30     0.00101047    -0.00020186    -0.00009039    -0.00020566     0.00003366     0.00000002    -0.00000002    -0.00024334
   MO  31     0.00000000    -0.00000003     0.00000002     0.00000001    -0.00000001     0.00003088    -0.00000930     0.00000002
   MO  32    -0.00000006    -0.00000004    -0.00000001     0.00000000     0.00000001     0.00003782    -0.00000469    -0.00000001
   MO  33     0.00164343    -0.00055473     0.00000961    -0.00002432    -0.00010390     0.00000001     0.00000002     0.00004400
   MO  34    -0.00055473     0.00073157     0.00001252    -0.00009734     0.00008595     0.00000000    -0.00000002     0.00001125
   MO  35     0.00000961     0.00001252     0.00078169     0.00047269     0.00003619     0.00000000     0.00000002    -0.00002024
   MO  36    -0.00002432    -0.00009734     0.00047269     0.00040785    -0.00004214     0.00000000     0.00000001     0.00000379
   MO  37    -0.00010390     0.00008595     0.00003619    -0.00004214     0.00069495    -0.00000004     0.00000002    -0.00008999
   MO  38     0.00000001     0.00000000     0.00000000     0.00000000    -0.00000004     0.00103745    -0.00029566     0.00000002
   MO  39     0.00000002    -0.00000002     0.00000002     0.00000001     0.00000002    -0.00029566     0.00066020     0.00000000
   MO  40     0.00004400     0.00001125    -0.00002024     0.00000379    -0.00008999     0.00000002     0.00000000     0.00099982
   MO  41     0.00001489    -0.00006502    -0.00000416     0.00005092     0.00000265    -0.00000001     0.00000000     0.00004772
   MO  42     0.00000000     0.00000001    -0.00000003    -0.00000002    -0.00000003    -0.00019810    -0.00041604    -0.00000002
   MO  43     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00008985    -0.00003245    -0.00000001
   MO  44     0.00008878    -0.00008642     0.00007619     0.00003073     0.00031475    -0.00000001     0.00000004    -0.00004731
   MO  45     0.00008284     0.00008709    -0.00024471    -0.00019125    -0.00002466    -0.00000002    -0.00000001     0.00041614
   MO  46    -0.00001658    -0.00002744     0.00003908     0.00006139     0.00001166     0.00000000     0.00000000    -0.00008535

                MO  41         MO  42         MO  43         MO  44         MO  45         MO  46
   MO   8     0.00034742     0.00000009     0.00000063    -0.00009503     0.00016424     0.00021696
   MO   9    -0.00005866     0.00000036    -0.00000028    -0.00020807    -0.00019678    -0.00010758
   MO  10     0.00005534     0.00000022    -0.00000012     0.00024377    -0.00099897    -0.00027450
   MO  11    -0.00004105    -0.00000029    -0.00000067    -0.00010022     0.00023399    -0.00046778
   MO  12    -0.00001107     0.00000059     0.00000031     0.00077757    -0.00050578     0.00018324
   MO  13    -0.00000073    -0.00038928    -0.00018603    -0.00000004    -0.00000015    -0.00000032
   MO  14    -0.00000063     0.00070986    -0.00120139     0.00000000     0.00000035    -0.00000036
   MO  15    -0.00032538    -0.00000021     0.00000020    -0.00062293    -0.00035309    -0.00036577
   MO  16     0.00036802    -0.00000012     0.00000025     0.00141901    -0.00063538    -0.00022213
   MO  17    -0.00000106    -0.00022673    -0.00197424     0.00000003    -0.00000001    -0.00000033
   MO  18    -0.00015792    -0.00000019     0.00000001    -0.00013685     0.00027964    -0.00009935
   MO  19    -0.00026571    -0.00000001    -0.00000001    -0.00006175    -0.00017916    -0.00016719
   MO  20     0.00020834     0.00000002     0.00000000     0.00014666    -0.00016149     0.00005951
   MO  21     0.00010613     0.00000000     0.00000000     0.00002997     0.00000032     0.00003349
   MO  22    -0.00000001     0.00009161    -0.00011665     0.00000000     0.00000000    -0.00000001
   MO  23     0.00006732     0.00000002     0.00000000     0.00005444     0.00003417     0.00004317
   MO  24     0.00000000    -0.00040737     0.00004920     0.00000001     0.00000002     0.00000000
   MO  25     0.00000055     0.00000004     0.00000000     0.00032877    -0.00010099    -0.00000164
   MO  26    -0.00000011     0.00004941    -0.00050520     0.00000000    -0.00000004    -0.00000006
   MO  27     0.00011273     0.00000004    -0.00000001     0.00015841    -0.00030851     0.00002655
   MO  28     0.00018983     0.00000000     0.00000003     0.00002287    -0.00015979     0.00011743
   MO  29     0.00003082     0.00000000    -0.00000001    -0.00005853     0.00019745    -0.00003376
   MO  30    -0.00007486    -0.00000002     0.00000000    -0.00000138    -0.00011296     0.00001027
   MO  31    -0.00000002    -0.00000938    -0.00013348    -0.00000001     0.00000002    -0.00000002
   MO  32    -0.00000005    -0.00002018    -0.00027303     0.00000000    -0.00000003    -0.00000003
   MO  33     0.00001489     0.00000000     0.00000001     0.00008878     0.00008284    -0.00001658
   MO  34    -0.00006502     0.00000001     0.00000000    -0.00008642     0.00008709    -0.00002744
   MO  35    -0.00000416    -0.00000003     0.00000000     0.00007619    -0.00024471     0.00003908
   MO  36     0.00005092    -0.00000002     0.00000000     0.00003073    -0.00019125     0.00006139
   MO  37     0.00000265    -0.00000003     0.00000000     0.00031475    -0.00002466     0.00001166
   MO  38    -0.00000001    -0.00019810     0.00008985    -0.00000001    -0.00000002     0.00000000
   MO  39     0.00000000    -0.00041604    -0.00003245     0.00000004    -0.00000001     0.00000000
   MO  40     0.00004772    -0.00000002    -0.00000001    -0.00004731     0.00041614    -0.00008535
   MO  41     0.00034141    -0.00000001     0.00000005     0.00002637     0.00003442     0.00011364
   MO  42    -0.00000001     0.00089141    -0.00005934    -0.00000001     0.00000003    -0.00000001
   MO  43     0.00000005    -0.00005934     0.00008504     0.00000000     0.00000001     0.00000002
   MO  44     0.00002637    -0.00000001     0.00000000     0.00022685    -0.00012838     0.00001905
   MO  45     0.00003442     0.00000003     0.00000001    -0.00012838     0.00072625    -0.00009392
   MO  46     0.00011364    -0.00000001     0.00000002     0.00001905    -0.00009392     0.00009843

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99950898
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99889996     1.99717426     1.99425928     1.99113666     1.98489136     1.98298701     1.93309774     1.00857774
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.99639415     0.06980790     0.01185787     0.00735614     0.00437664     0.00408483     0.00287904     0.00232367
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00152510     0.00145863     0.00121058     0.00106112     0.00095198     0.00092032     0.00066283     0.00053355
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00044528     0.00033688     0.00030318     0.00018850     0.00018172     0.00015585     0.00014776     0.00010469
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO
  occ(*)=     0.00007699     0.00003728     0.00002875     0.00002297     0.00001899     0.00001383


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     1_ s       2.000501   0.000166   0.000087   2.000004   0.000000   0.000131
     1_ p       0.000134   0.000236   0.000149   0.000000   2.000010   1.999807
     1_ d       0.000001   0.000015   0.000016   0.000000   0.000000   0.000001
     3_ s       0.000033   0.000000   1.999749   0.000000   0.000000   0.000002
     3_ p      -0.000093  -0.000001   0.000001   0.000000   0.000002   0.000005
     3_ d      -0.000003   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s      -0.000055   1.999583   0.000000   0.000000   0.000000   0.000019
     4_ p      -0.000500   0.000003  -0.000001  -0.000003  -0.000013   0.000020
     4_ d      -0.000016   0.000000   0.000000   0.000000   0.000001   0.000015
 
   ao class       7a         8a         9a        10a        11a        12a  
     1_ s       0.000003   0.198248   0.049630   0.033968   1.165099   0.300827
     1_ p       2.000012   0.533340  -0.030598   0.413987   0.248689   0.056724
     1_ d       0.000003   0.046207   0.007057   0.053074   0.011782   0.010098
     3_ s      -0.000008   0.003069   0.000224  -0.000823   0.461732   1.490247
     3_ p      -0.000018   0.009791   0.000584   0.054808   0.046593   0.073474
     3_ d       0.000000   0.000262   0.000018   0.000538   0.001522   0.000217
     4_ s      -0.000006   0.020941   1.805697   0.011753   0.023693   0.003314
     4_ p      -0.000005   1.179039   0.166269   1.425205   0.035067   0.056088
     4_ d       0.000019   0.008610   0.000021   0.004663   0.000082   0.000148
 
   ao class      13a        14a        15a        16a        17a        18a  
     1_ s       0.000000   0.053263   0.003616   0.000000   0.000000   0.002160
     1_ p       1.070726   0.021680   0.483790   0.105961   0.355903   0.035300
     1_ d       0.026574   0.027834   0.059123   0.020910   0.014176   0.000102
     3_ s       0.000000   0.000209  -0.004022   0.000000   0.000000   0.000250
     3_ p       0.057204   1.823880   1.199619   0.577323   0.386659   0.027037
     3_ d       0.000789   0.000676   0.003133   0.000673  -0.000125   0.000127
     4_ s       0.000000   0.003584   0.000081   0.000000   0.000000   0.000022
     4_ p       0.824012   0.051835   0.187958   0.303798   0.239953   0.004778
     4_ d       0.005586   0.000028  -0.000200  -0.000086  -0.000172   0.000030
 
   ao class      19a        20a        21a        22a        23a        24a  
     1_ s       0.000377   0.000000   0.000003   0.000000   0.000026   0.000181
     1_ p       0.003247   0.000984   0.000711   0.000011   0.000248   0.000126
     1_ d       0.001797   0.002953   0.002488   0.001848   0.001783   0.000214
     3_ s       0.000003   0.000000   0.000061   0.000000   0.000260   0.001293
     3_ p       0.005543   0.000389   0.000138   0.001848   0.000022   0.000326
     3_ d       0.000057   0.000008   0.000246   0.000194   0.000030   0.000163
     4_ s      -0.000089   0.000000   0.000000   0.000000  -0.000001   0.000003
     4_ p       0.000910   0.002920   0.000691   0.000173   0.000485   0.000008
     4_ d       0.000014   0.000102   0.000038   0.000011   0.000025   0.000010
 
   ao class      25a        26a        27a        28a        29a        30a  
     1_ s       0.000008   0.000000   0.000000   0.000031   0.000021   0.000000
     1_ p      -0.000005  -0.000007   0.000004   0.000127   0.000101   0.000051
     1_ d       0.000039   0.000233   0.000005   0.000071   0.000128   0.000369
     3_ s       0.000117   0.000000   0.000000   0.000001   0.000030   0.000000
     3_ p       0.000069   0.000509   0.000002   0.000397   0.000018   0.000101
     3_ d       0.001278   0.000717   0.001199   0.000301   0.000357   0.000390
     4_ s       0.000001   0.000000   0.000000   0.000021   0.000090   0.000000
     4_ p       0.000017   0.000003   0.000000   0.000079   0.000113   0.000007
     4_ d       0.000002   0.000002   0.000000   0.000032   0.000094   0.000003
 
   ao class      31a        32a        33a        34a        35a        36a  
     1_ s       0.000083   0.000057   0.000000   0.000008   0.000085   0.000022
     1_ p       0.000010   0.000019   0.000364   0.000011   0.000021   0.000051
     1_ d       0.000216   0.000018   0.000033   0.000044   0.000035   0.000058
     3_ s       0.000007  -0.000002   0.000000   0.000001   0.000008   0.000000
     3_ p       0.000040   0.000005   0.000019   0.000004   0.000052   0.000009
     3_ d       0.000192   0.000410   0.000019   0.000006   0.000040   0.000004
     4_ s       0.000021   0.000000   0.000000   0.000004   0.000000   0.000008
     4_ p       0.000044   0.000026   0.000009   0.000129   0.000049   0.000029
     4_ d       0.000050   0.000001   0.000001   0.000130   0.000012   0.000007
 
   ao class      37a        38a        39a        40a        41a        42a  
     1_ s       0.000000   0.000001   0.000000   0.000007   0.000009   0.000003
     1_ p       0.000000   0.000013  -0.000002   0.000038   0.000013   0.000021
     1_ d       0.000000   0.000002   0.000061   0.000009   0.000004   0.000002
     3_ s       0.000000   0.000001   0.000000   0.000003   0.000009   0.000002
     3_ p       0.000000   0.000017   0.000000   0.000019   0.000026  -0.000002
     3_ d       0.000000   0.000002   0.000000   0.000004   0.000005   0.000000
     4_ s       0.000000   0.000010   0.000000   0.000009   0.000000   0.000001
     4_ p       0.000000   0.000052   0.000085   0.000008   0.000004   0.000009
     4_ d       0.000181   0.000057   0.000003   0.000006   0.000008   0.000001
 
   ao class      43a        44a        45a        46a  
     1_ s       0.000000   0.000000   0.000000   0.000000
     1_ p       0.000002   0.000000   0.000003   0.000000
     1_ d       0.000000   0.000001   0.000001   0.000001
     3_ s       0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000
     4_ s       0.000001   0.000000   0.000005   0.000005
     4_ p       0.000004   0.000000   0.000001   0.000001
     4_ d       0.000022   0.000022   0.000009   0.000007


                        gross atomic populations
     ao            1_         3_         4_
      s         5.808624   3.952458   3.868714
      p         9.302010   4.266417   4.479358
      d         0.289387   0.013452   0.019580
    total      15.400021   8.232327   8.367651
 

 Total number of electrons:   32.00000000

 item #                     3 suffix=:.drt1.state3:
 read_civout: repnuc=  -474.481013326253     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -547.24935119   -72.76833787
 residuum:     0.00007890
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -547.24077884   -72.75976552
 residuum:     0.00007190
 deltae:     0.00000000
================================================================================
  Reading record                      3  of civout
 INFO:ref#  3vector#  3 method:  0 last record  1max overlap with ref# 87% root-following 0
 MR-CISD energy:  -547.17759731   -72.69658398
 residuum:     0.00008135
 deltae:     0.00000001
 apxde:     0.00000001

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880643    -0.00000955    -0.04318685    -0.05477766    -0.00111626    -0.07208183    -0.02412399     0.03757525
 ref:   2     0.00000210    -0.96373086    -0.00003796     0.00108880    -0.00045929     0.00486921    -0.04972163    -0.09586500
 ref:   3    -0.02552718    -0.00000072    -0.86895682     0.15401410     0.31648645     0.06066463     0.04056691    -0.00810969

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96880643    -0.00000955    -0.04318685    -0.05477766    -0.00111626     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000210    -0.96373086    -0.00003796     0.00108880    -0.00045929     0.00000000     0.00000000     0.00000000
 ref:   3    -0.02552718    -0.00000072    -0.86895682     0.15401410     0.31648645     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=   13637  D0X=       0  D0W=       0
  DDZI=    3645 DDYI=    9339 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     909 DDXE=       0 DDWE=       0
================================================================================
--------------------------------------------------------------------------------
  2e-density  for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=  268506  D0X=       0  D0W=       0
  DDZI=   21120 DDYI=   52764 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:    18.000000
   18  correlated and    14  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.99829987
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00222296
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00106504
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00122600
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00697895
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000464
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000043
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00022837
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01628263
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000025
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00026548
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00266730
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00058454
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00068414
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000043
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00114375
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000062
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00008395
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000015
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00102781
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00169157
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00009158
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00042789
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000021
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00002441
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00171730
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00094877
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00182645
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00029421
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000018
   MO  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000034
   MO  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00033919
   MO  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00048835
   MO  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000036
   MO  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  44     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00012684
   MO  45     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00043761
   MO  46     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00008541

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   8    -0.00222296     0.00106504    -0.00122600     0.00697895    -0.00000464     0.00000043    -0.00022837     0.01628263
   MO   9     1.99161988     0.00115569    -0.00203692     0.01717975    -0.00001184     0.00000178     0.00077230     0.04266512
   MO  10     0.00115569     1.98986667     0.00580778    -0.02804846     0.00001581     0.00000171     0.00132628    -0.04581325
   MO  11    -0.00203692     0.00580778     1.99443890     0.01734712    -0.00001172    -0.00000084     0.00116083     0.03767373
   MO  12     0.01717975    -0.02804846     0.01734712     1.83434345     0.00009083    -0.00000205     0.00975038    -0.29333171
   MO  13    -0.00001184     0.00001581    -0.00001172     0.00009083     1.99377627    -0.00483573    -0.00000097     0.00019820
   MO  14     0.00000178     0.00000171    -0.00000084    -0.00000205    -0.00483573     1.86324221     0.00001612    -0.00002773
   MO  15     0.00077230     0.00132628     0.00116083     0.00975038    -0.00000097     0.00001612     1.96370583    -0.00478137
   MO  16     0.04266512    -0.04581325     0.03767373    -0.29333171     0.00019820    -0.00002773    -0.00478137     1.14049735
   MO  17    -0.00000122    -0.00000374     0.00000666     0.00000193     0.02477577     0.22667460    -0.00004959    -0.00000159
   MO  18    -0.00492936     0.00633539    -0.00011793     0.03927050    -0.00003970    -0.00002177    -0.00252945    -0.03338211
   MO  19    -0.00016802     0.00052547    -0.01300815    -0.00756952     0.00000593     0.00000671     0.09707230     0.01750550
   MO  20     0.00431364     0.00113121    -0.00180979    -0.01052614     0.00000835     0.00000464     0.00302691     0.02088418
   MO  21     0.00208717     0.00032103    -0.00089542    -0.00040178     0.00000177    -0.00000026    -0.00463637     0.00878899
   MO  22    -0.00000008    -0.00000026    -0.00000013     0.00000081    -0.00187728     0.00449820    -0.00000103    -0.00000117
   MO  23     0.00035310     0.00306088     0.00585749    -0.00368362     0.00000075    -0.00000041    -0.02031028     0.00384548
   MO  24    -0.00000046     0.00000045    -0.00000040     0.00000092     0.00141613    -0.01560081     0.00000120     0.00000677
   MO  25     0.00132295     0.00316702    -0.00323629    -0.01211744     0.00000563     0.00000500    -0.02083451     0.02760545
   MO  26     0.00000029     0.00000030     0.00000034    -0.00000174     0.01204316     0.02515837    -0.00000026    -0.00000631
   MO  27     0.00230333     0.00270842    -0.00058931    -0.00937967     0.00000662    -0.00000034     0.00437180     0.00927059
   MO  28     0.00205002     0.00029457    -0.00007499    -0.00359512     0.00000261     0.00000061     0.00304329     0.00486564
   MO  29     0.00008028    -0.00251626     0.00052387    -0.00614911     0.00000576     0.00000299     0.01004025     0.00372910
   MO  30    -0.00041672     0.00052259    -0.00053199     0.00549185    -0.00000476    -0.00000079     0.00562269    -0.02017784
   MO  31    -0.00000028    -0.00000007     0.00000013    -0.00000003     0.00514544     0.00649051    -0.00000055    -0.00000016
   MO  32     0.00000020     0.00000016    -0.00000028    -0.00000122     0.00603078     0.01683675     0.00000037    -0.00000345
   MO  33    -0.00012605     0.00028415    -0.00207806    -0.00311348     0.00000218    -0.00000074     0.00297239     0.01696766
   MO  34    -0.00179434    -0.00182427    -0.00445547     0.00511575    -0.00000310    -0.00000024     0.00870591    -0.01211448
   MO  35    -0.00230944    -0.00043833     0.00002606    -0.00030244    -0.00000109     0.00000095     0.00128562    -0.00123582
   MO  36    -0.00046276    -0.00140130     0.00019795    -0.00031143     0.00000015     0.00000061    -0.00438499     0.00132338
   MO  37    -0.00014581    -0.00014103    -0.00053280    -0.00044971     0.00000016     0.00000028     0.00437283    -0.00008732
   MO  38    -0.00000012    -0.00000005     0.00000004     0.00000017     0.00054174     0.00018568    -0.00000004     0.00000001
   MO  39    -0.00000048    -0.00000030    -0.00000008     0.00000003     0.00056740     0.00178335     0.00000010     0.00000048
   MO  40     0.00041084     0.00041093    -0.00033985     0.00039164    -0.00000021    -0.00000004    -0.00054449    -0.00024009
   MO  41     0.00063348    -0.00067171     0.00017606    -0.00033200     0.00000034    -0.00000020    -0.00033542     0.00153199
   MO  42     0.00000046     0.00000017    -0.00000011     0.00000005    -0.00016476    -0.00209221    -0.00000015    -0.00000014
   MO  43    -0.00000003    -0.00000003    -0.00000004     0.00000005    -0.00095508    -0.00243908     0.00000017     0.00000037
   MO  44     0.00000429    -0.00005378     0.00014953    -0.00106585     0.00000014    -0.00000013     0.00114957     0.00320903
   MO  45    -0.00015444     0.00138674    -0.00054085     0.00237180    -0.00000134     0.00000028     0.00044008    -0.00490127
   MO  46    -0.00005529    -0.00054537    -0.00001655    -0.00024147     0.00000009    -0.00000014    -0.00071986     0.00038843

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   8     0.00000025    -0.00026548     0.00266730    -0.00058454    -0.00068414     0.00000043    -0.00114375     0.00000062
   MO   9    -0.00000122    -0.00492936    -0.00016802     0.00431364     0.00208717    -0.00000008     0.00035310    -0.00000046
   MO  10    -0.00000374     0.00633539     0.00052547     0.00113121     0.00032103    -0.00000026     0.00306088     0.00000045
   MO  11     0.00000666    -0.00011793    -0.01300815    -0.00180979    -0.00089542    -0.00000013     0.00585749    -0.00000040
   MO  12     0.00000193     0.03927050    -0.00756952    -0.01052614    -0.00040178     0.00000081    -0.00368362     0.00000092
   MO  13     0.02477577    -0.00003970     0.00000593     0.00000835     0.00000177    -0.00187728     0.00000075     0.00141613
   MO  14     0.22667460    -0.00002177     0.00000671     0.00000464    -0.00000026     0.00449820    -0.00000041    -0.01560081
   MO  15    -0.00004959    -0.00252945     0.09707230     0.00302691    -0.00463637    -0.00000103    -0.02031028     0.00000120
   MO  16    -0.00000159    -0.03338211     0.01750550     0.02088418     0.00878899    -0.00000117     0.00384548     0.00000677
   MO  17     1.11091562     0.00002060    -0.00000390    -0.00000693    -0.00000138     0.00255101    -0.00000047     0.00646743
   MO  18     0.00002060     0.06070261    -0.00326405    -0.00341547    -0.00105199     0.00000018    -0.00061482     0.00000081
   MO  19    -0.00000390    -0.00326405     0.01759889     0.00146511    -0.00048937    -0.00000012    -0.00290108    -0.00000035
   MO  20    -0.00000693    -0.00341547     0.00146511     0.00193530     0.00063267    -0.00000007     0.00021077    -0.00000037
   MO  21    -0.00000138    -0.00105199    -0.00048937     0.00063267     0.00063201     0.00000000     0.00018007     0.00000002
   MO  22     0.00255101     0.00000018    -0.00000012    -0.00000007     0.00000000     0.00093381     0.00000000    -0.00054679
   MO  23    -0.00000047    -0.00061482    -0.00290108     0.00021077     0.00018007     0.00000000     0.00086632     0.00000001
   MO  24     0.00646743     0.00000081    -0.00000035    -0.00000037     0.00000002    -0.00054679     0.00000001     0.00524396
   MO  25     0.00000067    -0.00327749    -0.00502983     0.00070629     0.00036456     0.00000000     0.00150081    -0.00000017
   MO  26     0.00971399     0.00000133    -0.00000025    -0.00000018    -0.00000003     0.00110878    -0.00000010    -0.00055771
   MO  27    -0.00000493    -0.00200382     0.00039239     0.00084264     0.00009314     0.00000001     0.00021060    -0.00000026
   MO  28    -0.00000093    -0.00094224     0.00131333     0.00036379     0.00004367     0.00000000    -0.00013162    -0.00000004
   MO  29    -0.00000410    -0.00196336     0.00331691     0.00045947     0.00001713    -0.00000005    -0.00053218    -0.00000020
   MO  30     0.00000260     0.00293332     0.00366081    -0.00046610    -0.00048148     0.00000000    -0.00072832     0.00000003
   MO  31    -0.00125348     0.00000067    -0.00000040    -0.00000021     0.00000001    -0.00027342     0.00000002     0.00252425
   MO  32     0.01274510     0.00000043     0.00000002    -0.00000013    -0.00000006     0.00058947    -0.00000007    -0.00123307
   MO  33    -0.00000219    -0.00220502     0.00354695     0.00111836     0.00009622    -0.00000003    -0.00013143    -0.00000003
   MO  34     0.00000110     0.00205729    -0.00020349    -0.00067023    -0.00016340     0.00000000    -0.00031426    -0.00000002
   MO  35    -0.00000002     0.00032690     0.00014898     0.00029177     0.00021545    -0.00000001    -0.00003718    -0.00000002
   MO  36    -0.00000004    -0.00006800    -0.00060920     0.00029466     0.00019320     0.00000000     0.00011325     0.00000000
   MO  37     0.00000024     0.00017345    -0.00052475    -0.00011925     0.00002107     0.00000000     0.00003005     0.00000000
   MO  38    -0.00033965     0.00000010    -0.00000003    -0.00000002     0.00000000     0.00000361     0.00000000     0.00017030
   MO  39    -0.00144903    -0.00000009    -0.00000007     0.00000002     0.00000001    -0.00004692     0.00000003     0.00043118
   MO  40     0.00000002     0.00024330    -0.00026073    -0.00003370     0.00000707     0.00000001     0.00002701     0.00000001
   MO  41    -0.00000014    -0.00018129    -0.00006008     0.00014090     0.00006340     0.00000000     0.00001634     0.00000000
   MO  42     0.00285322    -0.00000009    -0.00000002     0.00000000     0.00000000     0.00009464     0.00000002    -0.00046603
   MO  43    -0.00193226    -0.00000009     0.00000004     0.00000002     0.00000000    -0.00011771     0.00000000     0.00003969
   MO  44    -0.00000013    -0.00022164    -0.00021700     0.00012622     0.00005924    -0.00000001     0.00006533     0.00000001
   MO  45    -0.00000015     0.00038370    -0.00028460    -0.00028620    -0.00006037     0.00000000     0.00003547    -0.00000001
   MO  46    -0.00000002    -0.00002884    -0.00018909     0.00007552     0.00003477     0.00000000     0.00006107     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   8    -0.00008395    -0.00000015    -0.00102781    -0.00169157     0.00009158     0.00042789    -0.00000021     0.00000003
   MO   9     0.00132295     0.00000029     0.00230333     0.00205002     0.00008028    -0.00041672    -0.00000028     0.00000020
   MO  10     0.00316702     0.00000030     0.00270842     0.00029457    -0.00251626     0.00052259    -0.00000007     0.00000016
   MO  11    -0.00323629     0.00000034    -0.00058931    -0.00007499     0.00052387    -0.00053199     0.00000013    -0.00000028
   MO  12    -0.01211744    -0.00000174    -0.00937967    -0.00359512    -0.00614911     0.00549185    -0.00000003    -0.00000122
   MO  13     0.00000563     0.01204316     0.00000662     0.00000261     0.00000576    -0.00000476     0.00514544     0.00603078
   MO  14     0.00000500     0.02515837    -0.00000034     0.00000061     0.00000299    -0.00000079     0.00649051     0.01683675
   MO  15    -0.02083451    -0.00000026     0.00437180     0.00304329     0.01004025     0.00562269    -0.00000055     0.00000037
   MO  16     0.02760545    -0.00000631     0.00927059     0.00486564     0.00372910    -0.02017784    -0.00000016    -0.00000345
   MO  17     0.00000067     0.00971399    -0.00000493    -0.00000093    -0.00000410     0.00000260    -0.00125348     0.01274510
   MO  18    -0.00327749     0.00000133    -0.00200382    -0.00094224    -0.00196336     0.00293332     0.00000067     0.00000043
   MO  19    -0.00502983    -0.00000025     0.00039239     0.00131333     0.00331691     0.00366081    -0.00000040     0.00000002
   MO  20     0.00070629    -0.00000018     0.00084264     0.00036379     0.00045947    -0.00046610    -0.00000021    -0.00000013
   MO  21     0.00036456    -0.00000003     0.00009314     0.00004367     0.00001713    -0.00048148     0.00000001    -0.00000006
   MO  22     0.00000000     0.00110878     0.00000001     0.00000000    -0.00000005     0.00000000    -0.00027342     0.00058947
   MO  23     0.00150081    -0.00000010     0.00021060    -0.00013162    -0.00053218    -0.00072832     0.00000002    -0.00000007
   MO  24    -0.00000017    -0.00055771    -0.00000026    -0.00000004    -0.00000020     0.00000003     0.00252425    -0.00123307
   MO  25     0.00593129    -0.00000040     0.00060797    -0.00037756    -0.00180723    -0.00281470    -0.00000016    -0.00000018
   MO  26    -0.00000040     0.00550957    -0.00000003    -0.00000006    -0.00000008     0.00000018     0.00107311     0.00289269
   MO  27     0.00060797    -0.00000003     0.00195667     0.00024320     0.00003173    -0.00037852    -0.00000011    -0.00000009
   MO  28    -0.00037756    -0.00000006     0.00024320     0.00064248     0.00022277     0.00025632    -0.00000008     0.00000000
   MO  29    -0.00180723    -0.00000008     0.00003173     0.00022277     0.00143533     0.00097248    -0.00000014     0.00000002
   MO  30    -0.00281470     0.00000018    -0.00037852     0.00025632     0.00097248     0.00249408     0.00000004     0.00000010
   MO  31    -0.00000016     0.00107311    -0.00000011    -0.00000008    -0.00000014     0.00000004     0.00289638    -0.00037386
   MO  32    -0.00000018     0.00289269    -0.00000009     0.00000000     0.00000002     0.00000010    -0.00037386     0.00236368
   MO  33    -0.00019548    -0.00000020     0.00047735     0.00039287     0.00085126     0.00077419    -0.00000008    -0.00000013
   MO  34    -0.00050233     0.00000015    -0.00008245    -0.00034149    -0.00012158     0.00010566     0.00000007     0.00000005
   MO  35    -0.00008665     0.00000004    -0.00039513    -0.00020844     0.00018913     0.00005417     0.00000003     0.00000000
   MO  36     0.00022521     0.00000003    -0.00027977    -0.00001412    -0.00002862    -0.00019848     0.00000001     0.00000000
   MO  37     0.00038461     0.00000002     0.00003576    -0.00011229     0.00000323     0.00001876     0.00000001     0.00000001
   MO  38    -0.00000001     0.00000757    -0.00000004     0.00000001     0.00000000    -0.00000001     0.00005758     0.00001631
   MO  39     0.00000003    -0.00001621    -0.00000003     0.00000005     0.00000000    -0.00000002     0.00004264     0.00000164
   MO  40     0.00008262     0.00000001    -0.00012115    -0.00008253     0.00005317    -0.00009269     0.00000000     0.00000000
   MO  41     0.00004918    -0.00000001     0.00012558     0.00000470     0.00001766    -0.00006110     0.00000000     0.00000000
   MO  42     0.00000001    -0.00004563     0.00000000     0.00000004    -0.00000001     0.00000001    -0.00009394    -0.00000530
   MO  43     0.00000001    -0.00051053     0.00000002     0.00000000     0.00000002    -0.00000001    -0.00012969    -0.00028675
   MO  44     0.00037167    -0.00000002     0.00010725     0.00000449    -0.00000409    -0.00005319     0.00000000    -0.00000001
   MO  45    -0.00009274     0.00000004    -0.00023605    -0.00015497     0.00005083     0.00002648     0.00000003     0.00000000
   MO  46     0.00002751     0.00000000     0.00001047     0.00011674    -0.00002088    -0.00000932     0.00000000    -0.00000001

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38         MO  39         MO  40
   MO   8    -0.00002441     0.00171730     0.00094877    -0.00182645     0.00029421     0.00000018     0.00000034     0.00033919
   MO   9    -0.00012605    -0.00179434    -0.00230944    -0.00046276    -0.00014581    -0.00000012    -0.00000048     0.00041084
   MO  10     0.00028415    -0.00182427    -0.00043833    -0.00140130    -0.00014103    -0.00000005    -0.00000030     0.00041093
   MO  11    -0.00207806    -0.00445547     0.00002606     0.00019795    -0.00053280     0.00000004    -0.00000008    -0.00033985
   MO  12    -0.00311348     0.00511575    -0.00030244    -0.00031143    -0.00044971     0.00000017     0.00000003     0.00039164
   MO  13     0.00000218    -0.00000310    -0.00000109     0.00000015     0.00000016     0.00054174     0.00056740    -0.00000021
   MO  14    -0.00000074    -0.00000024     0.00000095     0.00000061     0.00000028     0.00018568     0.00178335    -0.00000004
   MO  15     0.00297239     0.00870591     0.00128562    -0.00438499     0.00437283    -0.00000004     0.00000010    -0.00054449
   MO  16     0.01696766    -0.01211448    -0.00123582     0.00132338    -0.00008732     0.00000001     0.00000048    -0.00024009
   MO  17    -0.00000219     0.00000110    -0.00000002    -0.00000004     0.00000024    -0.00033965    -0.00144903     0.00000002
   MO  18    -0.00220502     0.00205729     0.00032690    -0.00006800     0.00017345     0.00000010    -0.00000009     0.00024330
   MO  19     0.00354695    -0.00020349     0.00014898    -0.00060920    -0.00052475    -0.00000003    -0.00000007    -0.00026073
   MO  20     0.00111836    -0.00067023     0.00029177     0.00029466    -0.00011925    -0.00000002     0.00000002    -0.00003370
   MO  21     0.00009622    -0.00016340     0.00021545     0.00019320     0.00002107     0.00000000     0.00000001     0.00000707
   MO  22    -0.00000003     0.00000000    -0.00000001     0.00000000     0.00000000     0.00000361    -0.00004692     0.00000001
   MO  23    -0.00013143    -0.00031426    -0.00003718     0.00011325     0.00003005     0.00000000     0.00000003     0.00002701
   MO  24    -0.00000003    -0.00000002    -0.00000002     0.00000000     0.00000000     0.00017030     0.00043118     0.00000001
   MO  25    -0.00019548    -0.00050233    -0.00008665     0.00022521     0.00038461    -0.00000001     0.00000003     0.00008262
   MO  26    -0.00000020     0.00000015     0.00000004     0.00000003     0.00000002     0.00000757    -0.00001621     0.00000001
   MO  27     0.00047735    -0.00008245    -0.00039513    -0.00027977     0.00003576    -0.00000004    -0.00000003    -0.00012115
   MO  28     0.00039287    -0.00034149    -0.00020844    -0.00001412    -0.00011229     0.00000001     0.00000005    -0.00008253
   MO  29     0.00085126    -0.00012158     0.00018913    -0.00002862     0.00000323     0.00000000     0.00000000     0.00005317
   MO  30     0.00077419     0.00010566     0.00005417    -0.00019848     0.00001876    -0.00000001    -0.00000002    -0.00009269
   MO  31    -0.00000008     0.00000007     0.00000003     0.00000001     0.00000001     0.00005758     0.00004264     0.00000000
   MO  32    -0.00000013     0.00000005     0.00000000     0.00000000     0.00000001     0.00001631     0.00000164     0.00000000
   MO  33     0.00183200    -0.00052509    -0.00002373    -0.00006891    -0.00007161    -0.00000001     0.00000000    -0.00002290
   MO  34    -0.00052509     0.00090388     0.00005891    -0.00012868     0.00008718     0.00000001    -0.00000003    -0.00000360
   MO  35    -0.00002373     0.00005891     0.00062847     0.00033527     0.00001134     0.00000001    -0.00000001     0.00003536
   MO  36    -0.00006891    -0.00012868     0.00033527     0.00033931    -0.00009185     0.00000001     0.00000001     0.00003505
   MO  37    -0.00007161     0.00008718     0.00001134    -0.00009185     0.00099479    -0.00000003     0.00000002     0.00003769
   MO  38    -0.00000001     0.00000001     0.00000001     0.00000001    -0.00000003     0.00044442    -0.00001655     0.00000000
   MO  39     0.00000000    -0.00000003    -0.00000001     0.00000001     0.00000002    -0.00001655     0.00051365     0.00000002
   MO  40    -0.00002290    -0.00000360     0.00003536     0.00003505     0.00003769     0.00000000     0.00000002     0.00048233
   MO  41     0.00003404    -0.00002935     0.00002288     0.00002770     0.00002512    -0.00000001    -0.00000001     0.00002270
   MO  42     0.00000000    -0.00000001    -0.00000001    -0.00000001     0.00000001    -0.00011570    -0.00035444    -0.00000003
   MO  43     0.00000002     0.00000000     0.00000000     0.00000000    -0.00000001     0.00003629     0.00000384     0.00000000
   MO  44     0.00006571    -0.00009088     0.00004379     0.00000590     0.00045818    -0.00000002     0.00000001     0.00002928
   MO  45     0.00000381     0.00013948    -0.00017989    -0.00015612     0.00009741     0.00000001    -0.00000001     0.00013601
   MO  46    -0.00000615    -0.00004631     0.00002354     0.00005507    -0.00000005     0.00000002     0.00000004    -0.00001691

                MO  41         MO  42         MO  43         MO  44         MO  45         MO  46
   MO   8    -0.00048835     0.00000036     0.00000001     0.00012684    -0.00043761     0.00008541
   MO   9     0.00063348     0.00000046    -0.00000003     0.00000429    -0.00015444    -0.00005529
   MO  10    -0.00067171     0.00000017    -0.00000003    -0.00005378     0.00138674    -0.00054537
   MO  11     0.00017606    -0.00000011    -0.00000004     0.00014953    -0.00054085    -0.00001655
   MO  12    -0.00033200     0.00000005     0.00000005    -0.00106585     0.00237180    -0.00024147
   MO  13     0.00000034    -0.00016476    -0.00095508     0.00000014    -0.00000134     0.00000009
   MO  14    -0.00000020    -0.00209221    -0.00243908    -0.00000013     0.00000028    -0.00000014
   MO  15    -0.00033542    -0.00000015     0.00000017     0.00114957     0.00044008    -0.00071986
   MO  16     0.00153199    -0.00000014     0.00000037     0.00320903    -0.00490127     0.00038843
   MO  17    -0.00000014     0.00285322    -0.00193226    -0.00000013    -0.00000015    -0.00000002
   MO  18    -0.00018129    -0.00000009    -0.00000009    -0.00022164     0.00038370    -0.00002884
   MO  19    -0.00006008    -0.00000002     0.00000004    -0.00021700    -0.00028460    -0.00018909
   MO  20     0.00014090     0.00000000     0.00000002     0.00012622    -0.00028620     0.00007552
   MO  21     0.00006340     0.00000000     0.00000000     0.00005924    -0.00006037     0.00003477
   MO  22     0.00000000     0.00009464    -0.00011771    -0.00000001     0.00000000     0.00000000
   MO  23     0.00001634     0.00000002     0.00000000     0.00006533     0.00003547     0.00006107
   MO  24     0.00000000    -0.00046603     0.00003969     0.00000001    -0.00000001     0.00000000
   MO  25     0.00004918     0.00000001     0.00000001     0.00037167    -0.00009274     0.00002751
   MO  26    -0.00000001    -0.00004563    -0.00051053    -0.00000002     0.00000004     0.00000000
   MO  27     0.00012558     0.00000000     0.00000002     0.00010725    -0.00023605     0.00001047
   MO  28     0.00000470     0.00000004     0.00000000     0.00000449    -0.00015497     0.00011674
   MO  29     0.00001766    -0.00000001     0.00000002    -0.00000409     0.00005083    -0.00002088
   MO  30    -0.00006110     0.00000001    -0.00000001    -0.00005319     0.00002648    -0.00000932
   MO  31     0.00000000    -0.00009394    -0.00012969     0.00000000     0.00000003     0.00000000
   MO  32     0.00000000    -0.00000530    -0.00028675    -0.00000001     0.00000000    -0.00000001
   MO  33     0.00003404     0.00000000     0.00000002     0.00006571     0.00000381    -0.00000615
   MO  34    -0.00002935    -0.00000001     0.00000000    -0.00009088     0.00013948    -0.00004631
   MO  35     0.00002288    -0.00000001     0.00000000     0.00004379    -0.00017989     0.00002354
   MO  36     0.00002770    -0.00000001     0.00000000     0.00000590    -0.00015612     0.00005507
   MO  37     0.00002512     0.00000001    -0.00000001     0.00045818     0.00009741    -0.00000005
   MO  38    -0.00000001    -0.00011570     0.00003629    -0.00000002     0.00000001     0.00000002
   MO  39    -0.00000001    -0.00035444     0.00000384     0.00000001    -0.00000001     0.00000004
   MO  40     0.00002270    -0.00000003     0.00000000     0.00002928     0.00013601    -0.00001691
   MO  41     0.00022422    -0.00000001     0.00000001     0.00003995    -0.00002932    -0.00000152
   MO  42    -0.00000001     0.00079932    -0.00003160     0.00000000     0.00000002     0.00000002
   MO  43     0.00000001    -0.00003160     0.00008065     0.00000000     0.00000000     0.00000000
   MO  44     0.00003995     0.00000000     0.00000000     0.00028626    -0.00006168     0.00001448
   MO  45    -0.00002932     0.00000002     0.00000000    -0.00006168     0.00053006    -0.00006393
   MO  46    -0.00000152     0.00000002     0.00000000     0.00001448    -0.00006393     0.00009219

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99912715
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99882885     1.99462724     1.99437471     1.99187885     1.97241426     1.93792232     1.92689379     1.04741794
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.02785085     0.05977923     0.01782331     0.00755500     0.00652345     0.00358313     0.00212198     0.00186053
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00143360     0.00134726     0.00103440     0.00085370     0.00079598     0.00063130     0.00060883     0.00046813
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00044517     0.00032081     0.00026517     0.00024807     0.00020940     0.00020895     0.00018941     0.00010099
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO
  occ(*)=     0.00009399     0.00005666     0.00003305     0.00002842     0.00002452     0.00001959


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     1_ s       2.000501   0.000166   0.000087   2.000004   0.000000   0.000131
     1_ p       0.000134   0.000236   0.000149   0.000000   2.000010   1.999807
     1_ d       0.000001   0.000015   0.000016   0.000000   0.000000   0.000001
     3_ s       0.000033   0.000000   1.999749   0.000000   0.000000   0.000002
     3_ p      -0.000093  -0.000001   0.000001   0.000000   0.000002   0.000005
     3_ d      -0.000003   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s      -0.000055   1.999583   0.000000   0.000000   0.000000   0.000019
     4_ p      -0.000500   0.000003  -0.000001  -0.000003  -0.000013   0.000020
     4_ d      -0.000016   0.000000   0.000000   0.000000   0.000001   0.000015
 
   ao class       7a         8a         9a        10a        11a        12a  
     1_ s       0.000003   0.374594   0.030822   0.000027   0.795359   0.422003
     1_ p       2.000012   0.385535   0.015920   0.779451   0.272092   0.342718
     1_ d       0.000003   0.051044  -0.002797   0.034525   0.028544   0.006658
     3_ s      -0.000008   0.007583   0.005060   0.000022   0.728801   1.126490
     3_ p      -0.000018   0.011638   0.003232   0.018733   0.057199   0.003616
     3_ d       0.000000   0.000384   0.000132   0.000467   0.001111   0.000086
     4_ s      -0.000006   0.532198   1.318104   0.000000   0.009011   0.010347
     4_ p      -0.000005   0.629095   0.626578   1.155982   0.101929   0.079258
     4_ d       0.000019   0.007056   0.001779   0.005420   0.000328   0.000703
 
   ao class      13a        14a        15a        16a        17a        18a  
     1_ s       0.063918   0.055587   0.000000   0.000000   0.032571   0.001680
     1_ p       0.041832   0.555423   0.382160   0.432014   0.105571   0.029646
     1_ d       0.048726   0.012262   0.039913   0.015315   0.036736   0.000196
     3_ s       0.001045   0.071429   0.000000   0.000000   0.005853  -0.000149
     3_ p       1.743159   0.540167   1.119082   0.413564   0.406420   0.022941
     3_ d       0.000982   0.001802   0.001469  -0.000024   0.000439   0.000122
     4_ s       0.000344   0.000510   0.000000   0.000000  -0.000189   0.000009
     4_ p       0.072028   0.697236   0.384557   0.186644   0.440454   0.005295
     4_ d       0.000381   0.003506  -0.000287  -0.000095  -0.000004   0.000039
 
   ao class      19a        20a        21a        22a        23a        24a  
     1_ s       0.000797   0.000000   0.000000   0.000017   0.000020   0.000274
     1_ p       0.006561   0.000894   0.000112   0.000719   0.000147   0.000204
     1_ d       0.002725   0.002619   0.002951   0.001690   0.001629   0.000081
     3_ s       0.000009   0.000000   0.000000   0.000013   0.000010   0.001125
     3_ p       0.005891   0.003102   0.001132   0.000084   0.000152   0.000051
     3_ d       0.000157   0.000074   0.000066   0.000097   0.000090   0.000102
     4_ s      -0.000164   0.000000   0.000000  -0.000007   0.000001   0.000002
     4_ p       0.001837   0.000841   0.002165   0.000947   0.000065   0.000020
     4_ d       0.000011   0.000027   0.000098   0.000024   0.000007   0.000001
 
   ao class      25a        26a        27a        28a        29a        30a  
     1_ s       0.000003   0.000000   0.000018   0.000006   0.000000   0.000059
     1_ p       0.000015   0.000000   0.000133   0.000001   0.000344   0.000004
     1_ d       0.000054   0.000263   0.000168   0.000119   0.000245   0.000229
     3_ s       0.000004   0.000000   0.000070   0.000014   0.000000   0.000015
     3_ p       0.000305   0.000343   0.000222   0.000086   0.000021   0.000154
     3_ d       0.001047   0.000738   0.000081   0.000607   0.000132   0.000140
     4_ s       0.000000   0.000000   0.000098   0.000001   0.000000   0.000018
     4_ p       0.000006   0.000001   0.000214   0.000021   0.000051   0.000009
     4_ d       0.000001   0.000002   0.000031  -0.000001   0.000002   0.000003
 
   ao class      31a        32a        33a        34a        35a        36a  
     1_ s       0.000000   0.000000   0.000080   0.000034   0.000026   0.000000
     1_ p       0.000237   0.000000   0.000015   0.000003   0.000004   0.000004
     1_ d       0.000076   0.000001   0.000015   0.000013   0.000010   0.000055
     3_ s       0.000000   0.000000   0.000001   0.000007   0.000006   0.000000
     3_ p       0.000088   0.000000   0.000002   0.000089   0.000019   0.000001
     3_ d       0.000160   0.000464   0.000288   0.000083   0.000005   0.000002
     4_ s       0.000000   0.000000   0.000002   0.000013   0.000034   0.000000
     4_ p       0.000045   0.000002   0.000040   0.000075   0.000114   0.000053
     4_ d       0.000002   0.000000   0.000002   0.000003   0.000047   0.000133
 
   ao class      37a        38a        39a        40a        41a        42a  
     1_ s       0.000000   0.000025   0.000007   0.000007   0.000007   0.000004
     1_ p       0.000004   0.000052   0.000031   0.000027   0.000040   0.000017
     1_ d       0.000057   0.000022   0.000028   0.000001   0.000012   0.000008
     3_ s       0.000000   0.000000   0.000000   0.000002   0.000001   0.000012
     3_ p       0.000000   0.000002   0.000003   0.000024   0.000006   0.000005
     3_ d       0.000000   0.000001   0.000001   0.000009   0.000005   0.000001
     4_ s       0.000000   0.000007   0.000000  -0.000002   0.000001   0.000000
     4_ p       0.000052   0.000015   0.000016   0.000017   0.000010   0.000009
     4_ d       0.000096   0.000085   0.000102   0.000018   0.000012   0.000000
 
   ao class      43a        44a        45a        46a  
     1_ s       0.000002  -0.000001   0.000000  -0.000001
     1_ p       0.000008   0.000000   0.000000   0.000000
     1_ d       0.000000   0.000001   0.000001   0.000002
     3_ s       0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000
     4_ s       0.000001   0.000007   0.000000   0.000009
     4_ p       0.000003   0.000004   0.000000   0.000003
     4_ d       0.000019   0.000018   0.000024   0.000006


                        gross atomic populations
     ao            1_         3_         4_
      s         5.778835   3.947197   3.869897
      p         9.352285   4.351430   4.385189
      d         0.284230   0.011319   0.019619
    total      15.415349   8.309946   8.274705
 

 Total number of electrons:   32.00000000

 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   2logvrecs(*)=   1   2
 item #                     4 suffix=:.trd1to2:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   2)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=   13637  D0X=       0  D0W=       0
  DDZI=    3645 DDYI=    9339 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     909 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   2)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=  268506  D0X=       0  D0W=       0
  DDZI=   21120 DDYI=   52764 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  8.306068832265867E-006
 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   3logvrecs(*)=   1   3
 item #                     5 suffix=:.trd1to3:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=   13637  D0X=       0  D0W=       0
  DDZI=    3645 DDYI=    9339 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     909 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   1,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=  268506  D0X=       0  D0W=       0
  DDZI=   21120 DDYI=   52764 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  2.375488436872093E-002
 accstate=                     4
 accpdens=                     4
logrecs(*)=   2   3logvrecs(*)=   2   3
 item #                     6 suffix=:.trd2to3:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   2,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=   13637  D0X=       0  D0W=       0
  DDZI=    3645 DDYI=    9339 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=     909 DDXE=       0 DDWE=       0
================================================================================
2e-transition density (   2,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3896  DYX=       0  DYW=       0
   D0Z=    4196  D0Y=  268506  D0X=       0  D0W=       0
  DDZI=   21120 DDYI=   52764 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 maximum diagonal element=  3.463243706150476E-005
 DA ...
