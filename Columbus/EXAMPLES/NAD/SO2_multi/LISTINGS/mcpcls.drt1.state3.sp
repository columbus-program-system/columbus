

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        50

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        45

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -547.1119819469    nuclear repulsion=    97.8132332455
 demc=             0.0000000000    wnorm=                 0.0000003850
 knorm=            0.0000014402    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.1667 0.1667 0.1667
  2    a      3   0.1667 0.1667 0.1667

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  3 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2 -0.9869067679  0.9739849686  33120
     27  0.1169302008  0.0136726719  13032
     23  0.0550982129  0.0030358131  13230
     16 -0.0486142012  0.0023633406  30312
      5  0.0485353145  0.0023556768  33012
     42 -0.0389946820  0.0015205852  03312
     19  0.0305179504  0.0009313453  30123
     22 -0.0280571298  0.0007872025  13302
     45  0.0272583740  0.0007430190  03123
      7 -0.0152281405  0.0002318963  31320
     38 -0.0115642729  0.0001337324  10332
     29 -0.0110158895  0.0001213498  12330
     25 -0.0072094363  0.0000519760  13203
     40  0.0050232433  0.0000252330  10233
     32  0.0045495853  0.0000206987  12132
     14 -0.0032297171  0.0000104311  31023
     36  0.0024261342  0.0000058861  11232
     12 -0.0016245099  0.0000026390  31122
     10  0.0011673713  0.0000013628  31212

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: