
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /fhgfs/global/lv70151/plasser1/1684021/WORK/mcdrtky                             
 
 input the spin multiplicity [  0]: spin multiplicity:    1    singlet 
 input the total number of electrons [  0]: nelt:     32
 input the number of irreps (1-8) [  0]: nsym:      1
 enter symmetry labels:(y,[n]) enter 1 labels (a4):
 enter symmetry label, default=   1
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:     13
 number of inactive electrons:     26
 number of active electrons:      6
 level(*)        1   2   3   4   5   6   7   8   9  10  11  12  13
 symd(*)         1   1   1   1   1   1   1   1   1   1   1   1   1
 slabel(*)     a   a   a   a   a   a   a   a   a   a   a   a   a  
 doub(*)         1   2   3   4   5   6   7   8   9  10  11  12  13
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      5
 level(*)        1   2   3   4   5
 syml(*)         1   1   1   1   1
 slabel(*)     a   a   a   a   a  
 modrt(*)       14  15  16  17  18
 input the minimum cumulative occupation for each active level:
  a   a   a   a   a  
   14  15  16  17  18
 input the maximum cumulative occupation for each active level:
  a   a   a   a   a  
   14  15  16  17  18
 slabel(*)     a   a   a   a   a  
 modrt(*)       14  15  16  17  18
 occmin(*)       0   0   0   0   6
 occmax(*)       6   6   6   6   6
 input the minimum b value for each active level:
  a   a   a   a   a  
   14  15  16  17  18
 input the maximum b value for each active level:
  a   a   a   a   a  
   14  15  16  17  18
 slabel(*)     a   a   a   a   a  
 modrt(*)       14  15  16  17  18
 bmin(*)         0   0   0   0   0
 bmax(*)         6   6   6   6   6
 input the step masks for each active level:
 modrt:smask=
  14:1111  15:1111  16:1111  17:1111  18:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     20
 are any arcs to be manually removed?(y,[n])
 nwalk=      50
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   5

 level  0 through level  5 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  20   5 3 0   1 a    18    0   0   0   0   1     1     0     0     0    50     0
 ........................................

  17   4 3 0   1 a    17   20   0   0   0   1     1     0     0     0    10     0

  18   4 2 1   1 a    17    0   0  20   0   1     1     1     1     0    20    10

  19   4 2 0   1 a    17    0   0   0  20   1     1     1     1     1    20    30
 ........................................

  11   3 3 0   1 a    16   17   0   0   0   1     1     0     0     0     1     0

  12   3 2 1   1 a    16   18   0  17   0   1     2     1     1     0     3     1

  13   3 2 0   1 a    16   19  18   0  17   1     3     2     1     1     6     4

  14   3 1 2   1 a    16    0   0  18   0   1     1     1     1     0     3    19

  15   3 1 1   1 a    16    0   0  19  18   1     2     2     2     1     8    22

  16   3 1 0   1 a    16    0   0   0  19   1     1     1     1     1     6    44
 ........................................

   5   2 2 0   1 a    15   13  12   0  11   1     6     3     1     1     1     0

   6   2 1 1   1 a    15   15  14  13  12   1     8     6     5     2     2     2

   7   2 1 0   1 a    15   16  15   0  13   1     6     5     3     3     3     7

   8   2 0 2   1 a    15    0   0  15  14   1     3     3     3     1     1    21

   9   2 0 1   1 a    15    0   0  16  15   1     3     3     3     2     2    28

  10   2 0 0   1 a    15    0   0   0  16   1     1     1     1     1     1    49
 ........................................

   2   1 1 0   1 a    14    7   6   0   5   1    20    14     6     6     1     0

   3   1 0 1   1 a    14    9   8   7   6   1    20    17    14     8     1     3

   4   1 0 0   1 a    14   10   9   0   7   1    10     9     6     6     1     9
 ........................................

   1   0 0 0   0       0    4   3   0   2   1    50    40    20    20     1     0
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=      50
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
       50 csfs selected from      50 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
       50 csfs selected from      50 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
       50 csfs selected from      50 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /fhgfs/global/lv70151/plasser1/1684021/WORK/mcdrtfl                             
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 3 3 0 0
   CSF#     2    3 3 1 2 0
   CSF#     3    3 3 1 0 2
   CSF#     4    3 3 0 3 0
   CSF#     5    3 3 0 1 2
   CSF#     6    3 3 0 0 3
   CSF#     7    3 1 3 2 0
   CSF#     8    3 1 3 0 2
   CSF#     9    3 1 2 3 0
   CSF#    10    3 1 2 1 2
   CSF#    11    3 1 2 0 3
   CSF#    12    3 1 1 2 2
   CSF#    13    3 1 0 3 2
   CSF#    14    3 1 0 2 3
   CSF#    15    3 0 3 3 0
   CSF#    16    3 0 3 1 2
   CSF#    17    3 0 3 0 3
   CSF#    18    3 0 1 3 2
   CSF#    19    3 0 1 2 3
   CSF#    20    3 0 0 3 3
   CSF#    21    1 3 3 2 0
   CSF#    22    1 3 3 0 2
   CSF#    23    1 3 2 3 0
   CSF#    24    1 3 2 1 2
   CSF#    25    1 3 2 0 3
   CSF#    26    1 3 1 2 2
   CSF#    27    1 3 0 3 2
   CSF#    28    1 3 0 2 3
   CSF#    29    1 2 3 3 0
   CSF#    30    1 2 3 1 2
   CSF#    31    1 2 3 0 3
   CSF#    32    1 2 1 3 2
   CSF#    33    1 2 1 2 3
   CSF#    34    1 2 0 3 3
   CSF#    35    1 1 3 2 2
   CSF#    36    1 1 2 3 2
   CSF#    37    1 1 2 2 3
   CSF#    38    1 0 3 3 2
   CSF#    39    1 0 3 2 3
   CSF#    40    1 0 2 3 3
   CSF#    41    0 3 3 3 0
   CSF#    42    0 3 3 1 2
   CSF#    43    0 3 3 0 3
   CSF#    44    0 3 1 3 2
   CSF#    45    0 3 1 2 3
   CSF#    46    0 3 0 3 3
   CSF#    47    0 1 3 3 2
   CSF#    48    0 1 3 2 3
   CSF#    49    0 1 2 3 3
   CSF#    50    0 0 3 3 3
