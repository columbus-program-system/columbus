
 Work memory size (LMWORK) :   301465600 =    0.00 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   12



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


                      SYMGRP:Point group information
                      ------------------------------

Point group: C1 

   * Character table

        |  E 
   -----+-----
    A   |   1

   * Direct product table

        | A  
   -----+-----
    A   | A  


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  S  1        1      16      41      18      [12s8p1d|4s3p1d]                             
  O  1        1       8      26      14      [9s4p1d|3s2p1d]                              
  O  2        1       8      26      14      [9s4p1d|3s2p1d]                              
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3      32      93      46

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   S  1     x     -1.4375210400
   2            y      0.0000020000
   3            z     -0.1115289900

   4   O  1     x      0.5835774100
   5            y      0.0000000000
   6            z      2.5063739400

   7   O  2     x      0.3491585800
   8            y      0.0000000000
   9            z     -2.2669912100



   Interatomic separations (in Angstroms):
   ---------------------------------------

            S  1        O  1        O  2

   S  1    0.000000
   O  1    1.750149    0.000000
   O  2    1.481530    2.529000    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    O  1       S  1                           1.750149
  bond distance:    O  2       S  1                           1.481530


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       O  2       S  1       O  1                 102.675


  Nuclear repulsion energy :   97.813233245533


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  S  1   1s    1   110800.000000    0.0002 -0.0001  0.0000  0.0000
   gen. cont.  2    16610.000000    0.0019 -0.0005  0.0002  0.0000
               3     3781.000000    0.0100 -0.0028  0.0008  0.0000
               4     1071.000000    0.0403 -0.0113  0.0033  0.0000
               5      349.800000    0.1286 -0.0389  0.0113  0.0000
               6      126.300000    0.3035 -0.0995  0.0296  0.0000
               7       49.260000    0.4214 -0.1997  0.0600  0.0000
               8       20.160000    0.2308 -0.1234  0.0413  0.0000
               9        5.720000    0.0179  0.5132 -0.2075  0.0000
              10        2.182000   -0.0030  0.6071 -0.3929  0.0000
              11        0.432700    0.0008  0.0397  0.6328  0.0000
              12        0.157000   -0.0004 -0.0095  0.5569  1.0000

  S  1   2px  13      399.700000    0.0045 -0.0012  0.0000
   gen. cont. 14       94.190000    0.0342 -0.0087  0.0000
              15       29.750000    0.1442 -0.0391  0.0000
              16       10.770000    0.3539 -0.0935  0.0000
              17        4.119000    0.4591 -0.1480  0.0000
              18        1.625000    0.2064  0.0302  0.0000
              19        0.472600    0.0102  0.5616  0.0000
              20        0.140700   -0.0001  0.5348  1.0000

  S  1   2py  21      399.700000    0.0045 -0.0012  0.0000
   gen. cont. 22       94.190000    0.0342 -0.0087  0.0000
              23       29.750000    0.1442 -0.0391  0.0000
              24       10.770000    0.3539 -0.0935  0.0000
              25        4.119000    0.4591 -0.1480  0.0000
              26        1.625000    0.2064  0.0302  0.0000
              27        0.472600    0.0102  0.5616  0.0000
              28        0.140700   -0.0001  0.5348  1.0000

  S  1   2pz  29      399.700000    0.0045 -0.0012  0.0000
   gen. cont. 30       94.190000    0.0342 -0.0087  0.0000
              31       29.750000    0.1442 -0.0391  0.0000
              32       10.770000    0.3539 -0.0935  0.0000
              33        4.119000    0.4591 -0.1480  0.0000
              34        1.625000    0.2064  0.0302  0.0000
              35        0.472600    0.0102  0.5616  0.0000
              36        0.140700   -0.0001  0.5348  1.0000

  S  1   3d2- 37        0.479000    1.0000

  S  1   3d1- 38        0.479000    1.0000

  S  1   3d0  39        0.479000    1.0000

  S  1   3d1+ 40        0.479000    1.0000

  S  1   3d2+ 41        0.479000    1.0000

  O  1   1s   42    11720.000000    0.0007 -0.0002  0.0000
   gen. cont. 43     1759.000000    0.0055 -0.0013  0.0000
              44      400.800000    0.0278 -0.0063  0.0000
              45      113.700000    0.1048 -0.0257  0.0000
              46       37.030000    0.2831 -0.0709  0.0000
              47       13.270000    0.4487 -0.1654  0.0000
              48        5.025000    0.2710 -0.1170  0.0000
              49        1.013000    0.0155  0.5574  0.0000
              50        0.302300   -0.0026  0.5728  1.0000

  O  1   2px  51       17.700000    0.0430  0.0000
   gen. cont. 52        3.854000    0.2289  0.0000
              53        1.046000    0.5087  0.0000
              54        0.275300    0.4605  1.0000

  O  1   2py  55       17.700000    0.0430  0.0000
   gen. cont. 56        3.854000    0.2289  0.0000
              57        1.046000    0.5087  0.0000
              58        0.275300    0.4605  1.0000

  O  1   2pz  59       17.700000    0.0430  0.0000
   gen. cont. 60        3.854000    0.2289  0.0000
              61        1.046000    0.5087  0.0000
              62        0.275300    0.4605  1.0000

  O  1   3d2- 63        1.185000    1.0000

  O  1   3d1- 64        1.185000    1.0000

  O  1   3d0  65        1.185000    1.0000

  O  1   3d1+ 66        1.185000    1.0000

  O  1   3d2+ 67        1.185000    1.0000

  O  2   1s   68    11720.000000    0.0007 -0.0002  0.0000
   gen. cont. 69     1759.000000    0.0055 -0.0013  0.0000
              70      400.800000    0.0278 -0.0063  0.0000
              71      113.700000    0.1048 -0.0257  0.0000
              72       37.030000    0.2831 -0.0709  0.0000
              73       13.270000    0.4487 -0.1654  0.0000
              74        5.025000    0.2710 -0.1170  0.0000
              75        1.013000    0.0155  0.5574  0.0000
              76        0.302300   -0.0026  0.5728  1.0000

  O  2   2px  77       17.700000    0.0430  0.0000
   gen. cont. 78        3.854000    0.2289  0.0000
              79        1.046000    0.5087  0.0000
              80        0.275300    0.4605  1.0000

  O  2   2py  81       17.700000    0.0430  0.0000
   gen. cont. 82        3.854000    0.2289  0.0000
              83        1.046000    0.5087  0.0000
              84        0.275300    0.4605  1.0000

  O  2   2pz  85       17.700000    0.0430  0.0000
   gen. cont. 86        3.854000    0.2289  0.0000
              87        1.046000    0.5087  0.0000
              88        0.275300    0.4605  1.0000

  O  2   3d2- 89        1.185000    1.0000

  O  2   3d1- 90        1.185000    1.0000

  O  2   3d0  91        1.185000    1.0000

  O  2   3d1+ 92        1.185000    1.0000

  O  2   3d2+ 93        1.185000    1.0000


  Contracted Orbitals
  -------------------

   1  S  1    1s       1     2     3     4     5     6     7     8     9    10    11    12
   2  S  1    1s       1     2     3     4     5     6     7     8     9    10    11    12
   3  S  1    1s       1     2     3     4     5     6     7     8     9    10    11    12
   4  S  1    1s      12
   5  S  1    2px     13    14    15    16    17    18    19    20
   6  S  1    2py     21    22    23    24    25    26    27    28
   7  S  1    2pz     29    30    31    32    33    34    35    36
   8  S  1    2px     13    14    15    16    17    18    19    20
   9  S  1    2py     21    22    23    24    25    26    27    28
  10  S  1    2pz     29    30    31    32    33    34    35    36
  11  S  1    2px     20
  12  S  1    2py     28
  13  S  1    2pz     36
  14  S  1    3d2-    37
  15  S  1    3d1-    38
  16  S  1    3d0     39
  17  S  1    3d1+    40
  18  S  1    3d2+    41
  19  O  1    1s      42    43    44    45    46    47    48    49    50
  20  O  1    1s      42    43    44    45    46    47    48    49    50
  21  O  1    1s      50
  22  O  1    2px     51    52    53    54
  23  O  1    2py     55    56    57    58
  24  O  1    2pz     59    60    61    62
  25  O  1    2px     54
  26  O  1    2py     58
  27  O  1    2pz     62
  28  O  1    3d2-    63
  29  O  1    3d1-    64
  30  O  1    3d0     65
  31  O  1    3d1+    66
  32  O  1    3d2+    67
  33  O  2    1s      68    69    70    71    72    73    74    75    76
  34  O  2    1s      68    69    70    71    72    73    74    75    76
  35  O  2    1s      76
  36  O  2    2px     77    78    79    80
  37  O  2    2py     81    82    83    84
  38  O  2    2pz     85    86    87    88
  39  O  2    2px     80
  40  O  2    2py     84
  41  O  2    2pz     88
  42  O  2    3d2-    89
  43  O  2    3d1-    90
  44  O  2    3d0     91
  45  O  2    3d1+    92
  46  O  2    3d2+    93




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        46


  Symmetry  A  ( 1)

    1     S  1     1s         1
    2     S  1     1s         2
    3     S  1     1s         3
    4     S  1     1s         4
    5     S  1     2px        5
    6     S  1     2py        6
    7     S  1     2pz        7
    8     S  1     2px        8
    9     S  1     2py        9
   10     S  1     2pz       10
   11     S  1     2px       11
   12     S  1     2py       12
   13     S  1     2pz       13
   14     S  1     3d2-      14
   15     S  1     3d1-      15
   16     S  1     3d0       16
   17     S  1     3d1+      17
   18     S  1     3d2+      18
   19     O  1     1s        19
   20     O  1     1s        20
   21     O  1     1s        21
   22     O  1     2px       22
   23     O  1     2py       23
   24     O  1     2pz       24
   25     O  1     2px       25
   26     O  1     2py       26
   27     O  1     2pz       27
   28     O  1     3d2-      28
   29     O  1     3d1-      29
   30     O  1     3d0       30
   31     O  1     3d1+      31
   32     O  1     3d2+      32
   33     O  2     1s        33
   34     O  2     1s        34
   35     O  2     1s        35
   36     O  2     2px       36
   37     O  2     2py       37
   38     O  2     2pz       38
   39     O  2     2px       39
   40     O  2     2py       40
   41     O  2     2pz       41
   42     O  2     3d2-      42
   43     O  2     3d1-      43
   44     O  2     3d0       44
   45     O  2     3d1+      45
   46     O  2     3d2+      46

  Symmetries of electric field:  A  (1)  A  (1)  A  (1)

  Symmetries of magnetic field:  A  (1)  A  (1)  A  (1)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    0           0.10D-14                                                   
      16.0    1    3    1    1    1                                             
S  1  -1.437521040000000   0.000002000000000  -0.111528990000000       *        
H  12   4                                                                       
     110800.00000000         0.00024764        -0.00006870         0.00001991   
                             0.00000000                                         
      16610.00000000         0.00192026        -0.00052768         0.00015348   
                             0.00000000                                         
       3781.00000000         0.00996192        -0.00279671         0.00080950   
                             0.00000000                                         
       1071.00000000         0.04029750        -0.01126510         0.00328974   
                             0.00000000                                         
        349.80000000         0.12860400        -0.03888340         0.01129670   
                             0.00000000                                         
        126.30000000         0.30348000        -0.09950250         0.02963850   
                             0.00000000                                         
         49.26000000         0.42143200        -0.19974000         0.05998510   
                             0.00000000                                         
         20.16000000         0.23078100        -0.12336000         0.04132480   
                             0.00000000                                         
          5.72000000         0.01789710         0.51319400        -0.20747400   
                             0.00000000                                         
          2.18200000        -0.00297516         0.60712000        -0.39288900   
                             0.00000000                                         
          0.43270000         0.00084952         0.03967530         0.63284000   
                             0.00000000                                         
          0.15700000        -0.00036794        -0.00946864         0.55692400   
                             1.00000000                                         
H   8   3                                                                       
        399.70000000         0.00447541        -0.00116251         0.00000000   
         94.19000000         0.03417080        -0.00865664         0.00000000   
         29.75000000         0.14425000        -0.03908860         0.00000000   
         10.77000000         0.35392800        -0.09346250         0.00000000   
          4.11900000         0.45908500        -0.14799400         0.00000000   
          1.62500000         0.20638300         0.03019040         0.00000000   
          0.47260000         0.01021410         0.56157300         0.00000000   
          0.14070000        -0.00006031         0.53477600         1.00000000   
H   1   1                                                                       
          0.47900000         1.00000000                                         
       8.0    2    3    1    1    1                                             
O  1   0.583577410000000   0.000000000000000   2.506373940000000       *        
O  2   0.349158580000000   0.000000000000000  -2.266991210000000       *        
H   9   3                                                                       
      11720.00000000         0.00071000        -0.00016000         0.00000000   
       1759.00000000         0.00547000        -0.00126300         0.00000000   
        400.80000000         0.02783700        -0.00626700         0.00000000   
        113.70000000         0.10480000        -0.02571600         0.00000000   
         37.03000000         0.28306200        -0.07092400         0.00000000   
         13.27000000         0.44871900        -0.16541100         0.00000000   
          5.02500000         0.27095200        -0.11695500         0.00000000   
          1.01300000         0.01545800         0.55736800         0.00000000   
          0.30230000        -0.00258500         0.57275900         1.00000000   
H   4   2                                                                       
         17.70000000         0.04301800         0.00000000                      
          3.85400000         0.22891300         0.00000000                      
          1.04600000         0.50872800         0.00000000                      
          0.27530000         0.46053100         1.00000000                      
H   1   1                                                                       
          1.18500000         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

found     695 non-vanashing overlap integrals
found    1081 non-vanashing nuclear attraction integrals
found     710 non-vanashing kinetic energy integrals






 found     746 non-vanashing integrals ( typea=  1 typeb=  0)
 found     669 non-vanashing integrals ( typea=  1 typeb=  1)
 found     741 non-vanashing integrals ( typea=  1 typeb=  2)


 found     771 non-vanashing integrals ( typea=  1 typeb=  3)
 found     726 non-vanashing integrals ( typea=  1 typeb=  4)
 found     828 non-vanashing integrals ( typea=  1 typeb=  5)
 found     740 non-vanashing integrals ( typea=  1 typeb=  6)
 found     727 non-vanashing integrals ( typea=  1 typeb=  7)
 found     752 non-vanashing integrals ( typea=  1 typeb=  8)


 found     771 non-vanashing integrals ( typea=  1 typeb=  9)
 found     735 non-vanashing integrals ( typea=  1 typeb= 10)
 found     856 non-vanashing integrals ( typea=  1 typeb= 11)
 found     803 non-vanashing integrals ( typea=  1 typeb= 12)
 found     820 non-vanashing integrals ( typea=  1 typeb= 13)
 found     839 non-vanashing integrals ( typea=  1 typeb= 14)
 found     681 non-vanashing integrals ( typea=  1 typeb= 15)
 found     808 non-vanashing integrals ( typea=  1 typeb= 16)
 found     732 non-vanashing integrals ( typea=  1 typeb= 17)
 found     765 non-vanashing integrals ( typea=  1 typeb= 18)


 found     776 non-vanashing integrals ( typea=  1 typeb= 19)
 found     738 non-vanashing integrals ( typea=  1 typeb= 20)
 found     856 non-vanashing integrals ( typea=  1 typeb= 21)
 found     803 non-vanashing integrals ( typea=  1 typeb= 22)
 found     832 non-vanashing integrals ( typea=  1 typeb= 23)
 found     856 non-vanashing integrals ( typea=  1 typeb= 24)
 found     735 non-vanashing integrals ( typea=  1 typeb= 25)
 found     912 non-vanashing integrals ( typea=  1 typeb= 26)
 found     824 non-vanashing integrals ( typea=  1 typeb= 27)
 found     841 non-vanashing integrals ( typea=  1 typeb= 28)
 found     744 non-vanashing integrals ( typea=  1 typeb= 29)
 found     737 non-vanashing integrals ( typea=  1 typeb= 30)
 found     808 non-vanashing integrals ( typea=  1 typeb= 31)
 found     735 non-vanashing integrals ( typea=  1 typeb= 32)
 found     774 non-vanashing integrals ( typea=  1 typeb= 33)


 found     686 non-vanashing integrals ( typea=  2 typeb=  6)
 found     747 non-vanashing integrals ( typea=  2 typeb=  7)
 found     686 non-vanashing integrals ( typea=  2 typeb=  8)




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:    527461 (90.2%)
 Kilobytes written:                             8476




 >>>> Total CPU  time used in HERMIT:   0.27 seconds
 >>>> Total wall time used in HERMIT:   1.00 seconds

- End of Integral Section
