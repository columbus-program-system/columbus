 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 programmed by: Thomas Müller (FZ Juelich)
 based on the cidrt code for single DRTs.

 version date: 16-jul-04


 This Version of Program CIDRT-MS is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT-MS    **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to files:
                       
 cidrtky                                                                        
  
 
 number of DRTs to be constructed [  0]: DRT infos are being written to files:
 cidrtfl.1                                                                      
  
 starting with the input common to all drts ... 
******************** ALL   DRTs    ********************
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :     4
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  a1 ) ( 2,  b1 ) ( 3,  b2 ) ( 4,  a2 ) 
 input nmpsy(*):
 nmpsy(*)=         8   3   3   0
 
   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      a1  b1  b2  a2 
 nmpsy(*)=         8   3   3   0
 
 total molecular orbitals            :    14
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered
 
 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :     4
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     8
 
 modrt(*)=         1   2   3   4   9  10  12  13
 slabel(*)=      a1  a1  a1  a1  b1  b1  b2  b2 
 
 total number of orbitals            :    14
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     8
 number of external orbitals         :     6
 
 orbital-to-level mapping vector
 map(*)=           7   8   9  10   1   2   3   4  11  12   5  13  14   6
==================== START DRT#   1====================
================================================================================
  Input for DRT number                      1
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a1 
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     0
 
 no. of internal orbitals            :     8
 no. of doubly-occ. (ref) orbitals   :     0
 no. active (ref) orbitals           :     8
 no. of active electrons             :     4
 
 input the active-orbital, active-electron occmnr(*):
   1  2  3  4  9 10 12 13
 input the active-orbital, active-electron occmxr(*):
   1  2  3  4  9 10 12 13
 
 actmo(*) =        1   2   3   4   9  10  12  13
 occmnr(*)=        0   0   0   0   0   0   0   0
 occmxr(*)=        4   4   4   4   4   4   4   4
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   9  10  12  13
 occmnr(*)=        0   0   0   0   0   0   0   0
 occmxr(*)=        4   4   4   4   4   4   4   4
 
 input the active-orbital bminr(*):
   1  2  3  4  9 10 12 13
 input the active-orbital bmaxr(*):
   1  2  3  4  9 10 12 13
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4   9  10  12  13
 bminr(*)=         0   0   0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4   4   4
 input the active orbital smaskr(*):
   1  2  3  4  9 10 12 13
 modrt:smaskr=
   1:1000   2:1111   3:1111   4:1111   9:1111  10:1111  12:1111  13:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     0
 number of internal electrons:       :     4
 
 input the internal-orbital mrsdci occmin(*):
   1  2  3  4  9 10 12 13
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4  9 10 12 13
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   9  10  12  13
 occmin(*)=        0   0   0   0   0   0   0   0
 occmax(*)=        4   4   4   4   4   4   4   4
 
 input the internal-orbital mrsdci bmin(*):
   1  2  3  4  9 10 12 13
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4  9 10 12 13
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4   9  10  12  13
 bmin(*)=          0   0   0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4   4   4
 
 input the internal-orbital smask(*):
   1  2  3  4  9 10 12 13
 modrt:smask=
   1:1000   2:1111   3:1111   4:1111   9:1111  10:1111  12:1111  13:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   2   2   3   3
 slabel(*)=      a1  a1  a1  a1  b1  b1  b2  b2 
 rmo(*)=           1   2   3   4   1   2   1   2
 modrt(*)=         1   2   3   4   9  10  12  13
 
 reference csf info:
 occmnr(*)=        0   0   0   0   0   0   0   0
 occmxr(*)=        4   4   4   4   4   4   4   4
 
 bminr(*)=         0   0   0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4   4   4
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0
 occmax(*)=        4   4   4   4   4   4   4   4
 
 bmin(*)=          0   0   0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4   4   4
 
******************** END   DRT#   1********************
******************** ALL   DRTs    ********************
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal

 number of rows in the drt :  26

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=      28
 xbary=       7
 xbarx=       0
 xbarw=       1
        --------
 nwalk=      36
------------------------------------------------------------------------
Core address array: 
             1             2
------------------------------------------------------------------------
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0
==================== START DRT#   1====================
  INPUT FOR DRT #                      1

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=      28

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a1 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
       12 csfs initially selected from      28 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4  9 10 12 13

 step 2 reference csf selection complete.
       12 csfs currently selected from      28 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
       12 reference csfs selected from      28 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            0   0   0   0   0   0   0   0
 
------------------------------------------------------------------------
Core address array: 
             1             2           110           218           246

------------------------------------------------------------------------
      1: 33000000
      2: 31200000
      3: 31020000
      4: 30300000
      5: 30120000
      6: 30030000
      7: 30003000
      8: 30001200
      9: 30000300
     10: 30000030
     11: 30000012
     12: 30000003
 number of step vectors saved:     12

 exlimw: beginning excitation-based walk selection...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y       0       0       0       0
 x       0       0       0       0
 w       0       0       0       0

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y       0       0       0       0
 x       0       0       0       0
 w       0       0       0       0

 total csf counts:
 z-vertex:       12
 y-vertex:        0
 x-vertex:        0
 w-vertex:        0
           --------
 total:          12
******************** END   DRT#   1********************
******************** ALL   DRTs    ********************
 Generating 3-DRT at exlimw level ... 

 xbarz=      12
 xbary=       0
 xbarx=       0
 xbarw=       0
        --------
 nwalk=      12
 levprt(*)        -1   0
------------------------------------------------------------------------
Core address array: 
             1             2           110           218           230
           242
------------------------------------------------------------------------
==================== START DRT#   1====================
 Converting reference vector for DRT #                     1

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  33000000
        2       2  31200000
        3       3  31020000
        4       4  30300000
        5       5  30120000
        6       6  30030000
        7       7  30003000
        8       8  30001200
        9       9  30000300
       10      10  30000030
       11      11  30000012
       12      12  30000003
 indx01:    12 elements set in vec01(*)
******************** END   DRT#   1********************
******************** ALL   DRTs    ********************
==================== START DRT#   1====================
------------------------------------------------------------------------
Core address array: 
             1             2           110           218           230

------------------------------------------------------------------------
      1: 33000000
      2: 31200000
      3: 31020000
      4: 30300000
      5: 30120000
      6: 30030000
      7: 30003000
      8: 30001200
      9: 30000300
     10: 30000030
     11: 30000012
     12: 30000003
 number of step vectors saved:     12

 exlimw: beginning excitation-based walk selection...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y       0       0       0       0
 x       0       0       0       0
 w       0       0       0       0

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y       0       0       0       0
 x       0       0       0       0
 w       0       0       0       0

 total csf counts:
 z-vertex:       12
 y-vertex:        0
 x-vertex:        0
 w-vertex:        0
           --------
 total:          12

 final mrsdci walk selection step:

 nvalw(*)=      12       0       0       0 nvalwt=      12

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=      12       0       0       0 nvalwt=      12


 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y       0       0       0       0
 x       0       0       0       0
 w       0       0       0       0

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y       0       0       0       0
 x       0       0       0       0
 w       0       0       0       0

 total csf counts:
 z-vertex:       12
 y-vertex:        0
 x-vertex:        0
 w-vertex:        0
           --------
 total:          12
 drt file is being written...
------------------------------------------------------------------------
Core address array: 
             1             2           110           218           230
           242          1842
------------------------------------------------------------------------
 
 input a title card, default=cidrt_title
 title card for DRT #                      1
  cidrt_title DRT#1                                                             
  
 drt file is being written... for DRT #                     1
 CI version:                     6
 wrtstr:  a1  b1  b2  a2 
nwalk=      12 cpos=       2 maxval=    9 cmprfactor=   83.33 %.
nwalk=      12 cpos=       1 maxval=   99 cmprfactor=   83.33 %.
 compressed with: nwalk=      12 cpos=       2 maxval=    9 cmprfactor=   83.33 %.
initial index vector length:        12
compressed index vector length:         2reduction:  83.33%
nwalk=      12 cpos=       2 maxval=    9 cmprfactor=   83.33 %.
nwalk=      12 cpos=       1 maxval=   99 cmprfactor=   83.33 %.
 compressed with: nwalk=      12 cpos=       2 maxval=    9 cmprfactor=   83.33 %.
initial ref vector length:        12
compressed ref vector length:         2reduction:  83.33%
******************** END   DRT#   1********************
 !timer: cidrt required                  cpu_time=     0.004 walltime=     0.027
