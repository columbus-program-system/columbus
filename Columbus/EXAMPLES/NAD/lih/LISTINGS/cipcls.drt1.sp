
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title DRT#1                                                              
 nmot  =    14 niot  =     8 nfct  =     0 nfvt  =     0
 nrow  =    26 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         12       12        0        0        0
 nvalwt,nvalw:       12       12        0        0        0
 ncsft:              12
 map(*)=     7  8  9 10  1  2  3  4 11 12  5 13 14  6
 mu(*)=      0  0  0  0  0  0  0  0
 syml(*) =   1  1  1  1  2  2  3  3
 rmo(*)=     1  2  3  4  1  2  1  2

 indx01:    12 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title DRT#1                                                              
 energy computed by program ciudg.       visnu             16:35:24.922 21-May-09

 lenrec =   32768 lenci =        12 ninfo =  6 nenrgy =  4 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0      100% overlap
 energy( 1)=  7.500000000000E-01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.984418898854E+00, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  3.101237854554E-15, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)=  3.854373553204E-29, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================
===================================ROOT # 2===================================

 rdhciv: CI vector file information:
  cidrt_title DRT#1                                                              
 energy computed by program ciudg.       visnu             16:35:24.922 21-May-09

 lenrec =   32768 lenci =        12 ninfo =  6 nenrgy =  6 ntitle =  2

 Max. overlap with ref vector #        2
 Valid ci vector #        2
 Method:        0      100% overlap
 energy( 1)=  7.500000000000E-01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.984418898854E+00, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  3.101237854554E-15, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)=  3.854373553204E-29, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 5)= -7.888955657490E+00, ietype=-1026,   total energy of type: MRSDCI  
 energy( 6)=  4.230354302544E-15, ietype=-2055, cnvginf energy of type: CI-Resid
==================================================================================

 space is available for   4358116 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:       9 coefficients were selected.
 workspace: ncsfmx=      12
 ncsfmx=                    12

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =      12 ncsft =      12 ncsf =       9
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       1 *
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       2 **
 3.1250E-02 <= |c| < 6.2500E-02       0
 1.5625E-02 <= |c| < 3.1250E-02       2 **
 7.8125E-03 <= |c| < 1.5625E-02       2 **
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =        9 total stored =       9

 from the selected csfs,
 min(|csfvec(:)|) = 1.0323E-02    max(|csfvec(:)|) = 9.1287E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =      12 ncsft =      12 ncsf =       9

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 
 
 internal level =    1    2    3    4    5    6    7    8
 syml(*)        =    1    1    1    1    2    2    3    3
 label          =  a1   a1   a1   a1   b1   b1   b2   b2 
 rmo(*)         =    1    2    3    4    1    2    1    2

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1 -0.91287 0.83332 z*                    33000000
        2 -0.34691 0.12035 z*                    31200000
        3 -0.15260 0.02329 z*                    31020000
        6  0.12106 0.01466 z*                    30030000
        5  0.08137 0.00662 z*                    30120000
       10  0.02783 0.00077 z*                    30000030
        7  0.02783 0.00077 z*                    30003000
        9  0.01032 0.00011 z*                    30000300
       12  0.01032 0.00011 z*                    30000003
            9 csfs were printed in this range.

================================================================================
===================================VECTOR # 2===================================
================================================================================


 rdcivnew:       7 coefficients were selected.
 workspace: ncsfmx=      12
 ncsfmx=                    12

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =      12 ncsft =      12 ncsf =       7
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       1 *
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       2 **
 1.5625E-02 <= |c| < 3.1250E-02       0
 7.8125E-03 <= |c| < 1.5625E-02       2 **
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =        7 total stored =       7

 from the selected csfs,
 min(|csfvec(:)|) = 1.4525E-02    max(|csfvec(:)|) = 9.1637E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =      12 ncsft =      12 ncsf =       7

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 
 
 internal level =    1    2    3    4    5    6    7    8
 syml(*)        =    1    1    1    1    2    2    3    3
 label          =  a1   a1   a1   a1   b1   b1   b2   b2 
 rmo(*)         =    1    2    3    4    1    2    1    2

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        2 -0.91637 0.83974 z*                    31200000
        1  0.34741 0.12069 z*                    33000000
        5  0.18111 0.03280 z*                    30120000
        3  0.05649 0.00319 z*                    31020000
        6 -0.05548 0.00308 z*                    30030000
       11  0.01453 0.00021 z*                    30000012
        8  0.01453 0.00021 z*                    30001200
            7 csfs were printed in this range.
