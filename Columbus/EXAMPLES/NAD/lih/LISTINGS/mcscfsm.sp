 total ao core energy =    0.750000000
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          2             0.500 0.500

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:     4
 Spin multiplicity:            1
 Number of active orbitals:    7
 Number of active electrons:   2
 Total number of CSFs:        12

 Number of active-double rotations:         3
 Number of active-active rotations:         0
 Number of double-virtual rotations:        4
 Number of active-virtual rotations:       16
 
 iter=    1 emc=  -70.9641840174 demc= 7.0964E+01 wnorm= 1.3809E+00 knorm= 1.0000E+00 apxde= 2.2334E+01    *not converged* 
 iter=    2 emc=   -7.1417679507 demc=-6.3822E+01 wnorm= 1.1103E+00 knorm= 8.2744E-01 apxde= 7.1182E-01    *not converged* 
 iter=    3 emc=   -7.8740747832 demc= 7.3231E-01 wnorm= 4.0718E-01 knorm= 1.9383E-01 apxde= 3.5627E-02    *not converged* 
 iter=    4 emc=   -7.9114321197 demc= 3.7357E-02 wnorm= 3.7239E-02 knorm= 5.9943E-01 apxde= 3.9051E-03    *not converged* 
 iter=    5 emc=   -7.9238488390 demc= 1.2417E-02 wnorm= 5.7482E-02 knorm= 5.5603E-01 apxde= 8.9770E-03    *not converged* 
 iter=    6 emc=   -7.9334603542 demc= 9.6115E-03 wnorm= 1.8600E-02 knorm= 3.9568E-01 apxde= 1.1138E-03    *not converged* 
 iter=    7 emc=   -7.9349340094 demc= 1.4737E-03 wnorm= 4.0804E-03 knorm= 8.1233E-01 apxde= 1.2472E-03    *not converged* 
 iter=    8 emc=   -7.9362531111 demc= 1.3191E-03 wnorm= 2.6987E-03 knorm= 3.2641E-01 apxde= 1.7181E-04    *not converged* 
 iter=    9 emc=   -7.9364597264 demc= 2.0662E-04 wnorm= 2.9151E-04 knorm= 8.9267E-01 apxde= 4.2549E-05    *not converged* 
 iter=   10 emc=   -7.9365409948 demc= 8.1268E-05 wnorm= 3.0466E-04 knorm= 8.4001E-01 apxde= 1.0510E-04    *not converged* 
 iter=   11 emc=   -7.9366760855 demc= 1.3509E-04 wnorm= 3.8114E-04 knorm= 1.7998E-01 apxde= 1.0564E-05    *not converged* 
 iter=   12 emc=   -7.9366872420 demc= 1.1157E-05 wnorm= 3.3089E-05 knorm= 1.0041E-02 apxde= 3.5940E-08    *not converged* 
 iter=   13 emc=   -7.9366872782 demc= 3.6158E-08 wnorm= 8.2295E-08 knorm= 7.6050E-05 apxde= 1.6382E-12    *not converged* 

 final mcscf convergence values:
 iter=   14 emc=   -7.9366872782 demc= 1.6351E-12 wnorm= 7.1331E-12 knorm= 2.8648E-12 apxde= 4.9365E-17    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.500000 total energy=       -7.984418899
   DRT #1 state # 2 weight 0.500000 total energy=       -7.888955657
   ------------------------------------------------------------


