1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    12)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1     -7.9844188989  1.7764E-15  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  1.7764E-15  0.0000E+00  4.2304E-15  1.0000E-05

 mr-sdci  convergence criteria satisfied after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1     -7.9844188989  1.7764E-15  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  1.7764E-15  0.0000E+00  4.2304E-15  1.0000E-05

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1     -7.9844188989  0.0000E+00  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  0.0000E+00  0.0000E+00  4.2304E-15  1.0000E-05

 mr-sdci  convergence criteria satisfied after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1     -7.9844188989  0.0000E+00  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  0.0000E+00  0.0000E+00  4.2304E-15  1.0000E-05

 number of reference csfs (nref) is    12.  root number (iroot) is  1.
 c0**2 =   1.00000000  c**2 (all zwalks) =   1.00000000

 eref      =     -7.984418898854   "relaxed" cnot**2         =   1.000000000000
 eci       =     -7.984418898854   deltae = eci - eref       =   0.000000000000
 eci+dv1   =     -7.984418898854   dv1 = (1-cnot**2)*deltae  =   0.000000000000
 eci+dv2   =     -7.984418898854   dv2 = dv1 / cnot**2       =   0.000000000000
 eci+dv3   =     -7.984418898854   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =     -7.984418898854   (  4e- scaled deltae )    =   0.000000000000

 number of reference csfs (nref) is    12.  root number (iroot) is  2.
 c0**2 =   1.00000000  c**2 (all zwalks) =   1.00000000

 eref      =     -7.888955657490   "relaxed" cnot**2         =   1.000000000000
 eci       =     -7.888955657490   deltae = eci - eref       =   0.000000000000
 eci+dv1   =     -7.888955657490   dv1 = (1-cnot**2)*deltae  =   0.000000000000
 eci+dv2   =     -7.888955657490   dv2 = dv1 / cnot**2       =   0.000000000000
 eci+dv3   =     -7.888955657490   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =     -7.888955657490   (  4e- scaled deltae )    =   0.000000000000
