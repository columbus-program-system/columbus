
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /home/szalay/lih_mcscf/WORK/mcdrtky                                             
 
 input the spin multiplicity [  0]: spin multiplicity:    1    singlet 
 input the total number of electrons [  0]: nelt:      4
 input the number of irreps (1-8) [  0]: nsym:      4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:      1
 number of inactive electrons:      2
 number of active electrons:      2
 level(*)        1
 symd(*)         1
 slabel(*)     a1 
 doub(*)         1
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      7
 level(*)        1   2   3   4   5   6   7
 syml(*)         1   1   1   2   2   3   3
 slabel(*)     a1  a1  a1  b1  b1  b2  b2 
 modrt(*)        2   3   4   1   2   1   2
 input the minimum cumulative occupation for each active level:
  a1  a1  a1  b1  b1  b2  b2 
    2   3   4   1   2   1   2
 input the maximum cumulative occupation for each active level:
  a1  a1  a1  b1  b1  b2  b2 
    2   3   4   1   2   1   2
 slabel(*)     a1  a1  a1  b1  b1  b2  b2 
 modrt(*)        2   3   4   1   2   1   2
 occmin(*)       0   0   0   0   0   0   0
 occmax(*)       2   2   2   2   2   2   2
 input the minimum b value for each active level:
  a1  a1  a1  b1  b1  b2  b2 
    2   3   4   1   2   1   2
 input the maximum b value for each active level:
  a1  a1  a1  b1  b1  b2  b2 
    2   3   4   1   2   1   2
 slabel(*)     a1  a1  a1  b1  b1  b2  b2 
 modrt(*)        2   3   4   1   2   1   2
 bmin(*)         0   0   0   0   0   0   0
 bmax(*)         2   2   2   2   2   2   2
 input the step masks for each active level:
 modrt:smask=
   2:1111   3:1111   4:1111   1:1111   2:1111   1:1111   2:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     20
 are any arcs to be manually removed?(y,[n])
 nwalk=      12
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   7

 level  0 through level  7 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  20   7 1 0   3 b2    2    0   0   0   0   1     1     0     0     0    12     0
                                            2     0     0     0     0     6     0
                                            3     0     0     0     0     6     0
                                            4     0     0     0     0     4     0
 ........................................

  17   6 1 0   3 b2    1   20   0   0   0   1     1     0     0     0    10     0
                                            2     0     0     0     0     6     0
                                            3     0     0     0     0     3     0
                                            4     0     0     0     0     2     0

  18   6 0 1   3 b2    1    0   0  20   0   1     0     0     0     0     3     0
                                            2     0     0     0     0     2     0
                                            3     1     1     1     0     1    10
                                            4     0     0     0     0     0     0

  19   6 0 0   3 b2    1    0   0   0  20   1     1     1     1     1     1    11
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

  14   5 1 0   2 b1    2   17   0   0   0   1     1     0     0     0     9     0
                                            2     0     0     0     0     6     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  15   5 0 1   2 b1    2   18   0  17   0   1     0     0     0     0     3     0
                                            2     0     0     0     0     2     0
                                            3     2     1     1     0     0     9
                                            4     0     0     0     0     0     0

  16   5 0 0   2 b1    2   19  18   0  17   1     3     2     1     1     1     9
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

  11   4 1 0   2 b1    1   14   0   0   0   1     1     0     0     0     7     0
                                            2     0     0     0     0     3     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  12   4 0 1   2 b1    1   15   0  14   0   1     0     0     0     0     3     0
                                            2     1     1     1     0     1     7
                                            3     2     0     0     0     0     9
                                            4     0     0     0     0     0     0

  13   4 0 0   2 b1    1   16  15   0  14   1     4     1     1     1     1     8
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     2     2     0     0     0     9
 ........................................

   8   3 1 0   1 a1    4   11   0   0   0   1     1     0     0     0     6     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

   9   3 0 1   1 a1    4   12   0  11   0   1     0     0     0     0     3     0
                                            2     2     1     1     0     0     6
                                            3     2     0     0     0     0     9
                                            4     0     0     0     0     0     0

  10   3 0 0   1 a1    4   13  12   0  11   1     6     2     1     1     1     6
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     4     2     0     0     0     9
 ........................................

   5   2 1 0   1 a1    3    8   0   0   0   1     1     0     0     0     3     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

   6   2 0 1   1 a1    3    9   0   8   0   1     1     1     1     0     2     3
                                            2     2     0     0     0     0     6
                                            3     2     0     0     0     0     9
                                            4     0     0     0     0     0     0

   7   2 0 0   1 a1    3   10   9   0   8   1     7     1     1     1     1     5
                                            2     2     2     0     0     0     6
                                            3     2     2     0     0     0     9
                                            4     4     0     0     0     0     9
 ........................................

   2   1 1 0   1 a1    2    5   0   0   0   1     1     0     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

   3   1 0 1   1 a1    2    6   0   5   0   1     2     1     1     0     1     1
                                            2     2     0     0     0     0     6
                                            3     2     0     0     0     0     9
                                            4     0     0     0     0     0     0

   4   1 0 0   1 a1    2    7   6   0   5   1     9     2     1     1     1     2
                                            2     4     2     0     0     0     6
                                            3     4     2     0     0     0     9
                                            4     4     0     0     0     0     9
 ........................................

   1   0 0 0   0       0    4   3   0   2   1    12     3     1     1     1     0
                                            2     6     2     0     0     0     6
                                            3     6     2     0     0     0     9
                                            4     4     0     0     0     0     9
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=      12
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
       12 csfs selected from      12 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
       12 csfs selected from      12 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
       12 csfs selected from      12 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /home/szalay/lih_mcscf/WORK/mcdrtfl                                             
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 0 0 0 0 0 0
   CSF#     2    1 2 0 0 0 0 0
   CSF#     3    1 0 2 0 0 0 0
   CSF#     4    0 3 0 0 0 0 0
   CSF#     5    0 1 2 0 0 0 0
   CSF#     6    0 0 3 0 0 0 0
   CSF#     7    0 0 0 3 0 0 0
   CSF#     8    0 0 0 1 2 0 0
   CSF#     9    0 0 0 0 3 0 0
   CSF#    10    0 0 0 0 0 3 0
   CSF#    11    0 0 0 0 0 1 2
   CSF#    12    0 0 0 0 0 0 3
