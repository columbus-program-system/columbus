

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  visnu             16:35:23.428 21-May-09
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:    4
 Spin multiplicity:            1
 Number of active orbitals:    7
 Number of active electrons:   2
 Total number of CSFs:        12

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    b1    b2    a2 

 List of doubly occupied orbitals:
  1 a1 

 List of active orbitals:
  2 a1   3 a1   4 a1   1 b1   2 b1   1 b2   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=    -7.9366872782    nuclear repulsion=     0.7500000000
 demc=             0.0000000000    wnorm=                 0.0000000000
 knorm=            0.0000000000    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     2   0.5000 0.5000

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  a1  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 a1   3 a1   4 a1   1 b1   2 b1   1 b2   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1 -0.9128663145  0.8333249081  3000000
      2 -0.3469127657  0.1203484670  1200000
      3 -0.1525984659  0.0232862918  1020000
      6  0.1210623287  0.0146560874  0030000
      5  0.0813730206  0.0066215685  0120000
     10  0.0278317262  0.0007746050  0000030
      7  0.0278317258  0.0007746050  0003000
      9  0.0103229394  0.0001065631  0000300
     12  0.0103229391  0.0001065631  0000003

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: