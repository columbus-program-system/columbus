

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       222822400 of real*8 words ( 1700.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,30
   ncoupl=5,
   tol(9)=5.e-3,
  FCIORB=  1,7,20,1,8,20,3,5,20,3,6,20
   NAVST(1) = 2,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /scratch/runlocal/WORK/aoints                            
    

 Integral file header information:
 Hermit Integral Program : SIFS version  opteron1.itc.univ 12:39:36.972 26-Jan-11

 Core type energy values:
 energy( 1)=  8.489294820576E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   84.892948206


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions: 112

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    38   18   38   18


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  5.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          2             0.500 0.500

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        3

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       7(  7)    20
       1       8(  8)    20
       3       5( 61)    20
       3       6( 62)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   31
 Spin multiplicity:            2
 Number of active orbitals:    4
 Number of active electrons:   3
 Total number of CSFs:        10
 
 !timer: initialization                  cpu_time=     0.001 walltime=     0.002

 faar:   0 active-active rotations allowed out of:   2 possible.


 Number of active-double rotations:        20
 Number of active-active rotations:         0
 Number of double-virtual rotations:      372
 Number of active-virtual rotations:      124

 Size of orbital-Hessian matrix B:                   140364
 Size of the orbital-state Hessian matrix C:          10320
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         150684



   ****** Integral transformation section ******


 Source of the initial MO coeficients:

 Input MO coefficient file: /scratch/runlocal/WORK/mocoef                               
 

               starting mcscf iteration...   1
 !timer:                                 cpu_time=     0.005 walltime=     0.006

 orbital-state coupling will not be calculated this iteration.

 program input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    38, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222788021

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222682661
 address segment size,           sizesg = 222677205
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     649372 transformed 1/r12    array elements were written in     119 records.

 !timer: 2-e transformation              cpu_time=     1.354 walltime=     1.364

 Size of orbital-Hessian matrix B:                   140364
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con         140364


 mosort: allocated sort2 space, avc2is=   222715407 available sort2 space, avcisx=   222715659
 !timer: mosrt1                          cpu_time=     0.066 walltime=     0.067
 !timer: mosrt2                          cpu_time=     0.012 walltime=     0.012
 !timer: mosort                          cpu_time=     0.080 walltime=     0.080
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 ciiter=   6 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.001 walltime=     0.001

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -155.7757422939     -240.6686904996        0.0000000000        0.0000010000
    2      -155.7754348074     -240.6683830131        0.0000000000        0.0000010000
    3      -155.3439643880     -240.2369125937        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.001 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.001 walltime=     0.001
 !timer: hbcon                           cpu_time=     0.008 walltime=     0.008
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.372473047174228E-003
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99994558 pnorm= 0.0000E+00 rznorm= 2.8770E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.007 walltime=     0.007

 fdd(*) eigenvalues. symmetry block  1
   -22.877887  -22.877887   -2.437298   -2.437293   -1.550018   -1.550017

 qvv(*) eigenvalues. symmetry block  1
    -0.112484   -0.054800   -0.011180    0.049100    0.058512    0.079807    0.212037    0.273535    0.792084    0.819344
     1.018408    1.073267    1.105436    1.217680    1.512524    1.520115    2.457490    2.457549    2.998114    3.003634
     3.580270    3.585568    5.020886    5.122018    5.747138    5.809274    6.716827    6.720614   48.508057   48.510053

 fdd(*) eigenvalues. symmetry block  2
    -1.666947   -1.666944

 qvv(*) eigenvalues. symmetry block  2
    -0.040750   -0.016481    0.285428    0.305043    0.765043    0.789496    1.295321    1.302428    2.417245    2.417245
     3.010797    3.023212    5.201277    5.219949    6.748979    6.751222

 fdd(*) eigenvalues. symmetry block  3
   -22.875229  -22.875229   -1.971281   -1.971281

 qvv(*) eigenvalues. symmetry block  3
    -0.060822   -0.034831    0.052702    0.129472    0.175753    0.215739    0.336912    0.403849    0.597473    0.600860
     1.027236    1.087735    1.182697    1.246009    1.514756    1.526869    1.802710    1.808696    3.594996    3.639116
     4.088984    4.095327    4.574707    4.576052    5.267162    5.317984    6.118632    6.150164    7.879409    7.884903
    49.940857   49.941179

 fdd(*) eigenvalues. symmetry block  4
    -1.404448   -1.404445

 qvv(*) eigenvalues. symmetry block  4
     0.031743    0.053652    0.496418    0.523919    1.514895    1.516132    1.542939    1.563688    3.030601    3.030602
     4.010790    4.014220    5.850389    5.862852    7.041380    7.043775
 !timer: motran                          cpu_time=     0.009 walltime=     0.009

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     1.466 walltime=     1.476

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -155.7755885506 demc= 1.5578E+02 wnorm= 1.8980E-02 knorm= 1.0432E-02 apxde= 4.2991E-05    *not conv.*     

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     1.471 walltime=     1.481

 orbital-state coupling will not be calculated this iteration.

 program input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    38, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222788021

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222682661
 address segment size,           sizesg = 222677205
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     649369 transformed 1/r12    array elements were written in     119 records.

 !timer: 2-e transformation              cpu_time=     0.654 walltime=     0.656

 Size of orbital-Hessian matrix B:                   140364
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con         140364


 mosort: allocated sort2 space, avc2is=   222715407 available sort2 space, avcisx=   222715659
 !timer: mosrt1                          cpu_time=     0.067 walltime=     0.067
 !timer: mosrt2                          cpu_time=     0.011 walltime=     0.012
 !timer: mosort                          cpu_time=     0.080 walltime=     0.080
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 30).
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 ciiter=   5 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -155.7757851122     -240.6687333179        0.0000000000        0.0000010000
    2      -155.7754782398     -240.6684264455        0.0000000000        0.0000010000
    3      -155.3440519815     -240.2370001873        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.001 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.001 walltime=     0.001
 !timer: hbcon                           cpu_time=     0.009 walltime=     0.009
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.276369794322931E-006
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99999998 pnorm= 0.0000E+00 rznorm= 8.6336E-07 rpnorm= 0.0000E+00 noldr=  4 nnewr=  4 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.005 walltime=     0.005

 fdd(*) eigenvalues. symmetry block  1
   -22.877970  -22.877970   -2.437313   -2.437308   -1.549992   -1.549991

 qvv(*) eigenvalues. symmetry block  1
    -0.112153   -0.054828   -0.011230    0.049095    0.058422    0.080105    0.212104    0.273629    0.792148    0.819395
     1.018602    1.073105    1.105361    1.218026    1.512564    1.520076    2.457499    2.457558    2.998146    3.003667
     3.580281    3.585580    5.020945    5.122078    5.747126    5.809258    6.716799    6.720585   48.507979   48.509975

 fdd(*) eigenvalues. symmetry block  2
    -1.666922   -1.666919

 qvv(*) eigenvalues. symmetry block  2
    -0.040667   -0.016425    0.285492    0.305094    0.765104    0.789552    1.295363    1.302468    2.417275    2.417276
     3.010823    3.023237    5.201304    5.219976    6.748994    6.751238

 fdd(*) eigenvalues. symmetry block  3
   -22.875312  -22.875312   -1.971265   -1.971265

 qvv(*) eigenvalues. symmetry block  3
    -0.060637   -0.034801    0.052769    0.129495    0.175723    0.215930    0.337052    0.403788    0.597506    0.600905
     1.027190    1.087994    1.182861    1.246040    1.514804    1.526785    1.802727    1.808722    3.595034    3.639165
     4.088994    4.095341    4.574705    4.576051    5.267204    5.318033    6.118625    6.150144    7.879410    7.884910
    49.940778   49.941100

 fdd(*) eigenvalues. symmetry block  4
    -1.404434   -1.404432

 qvv(*) eigenvalues. symmetry block  4
     0.031815    0.053703    0.496474    0.523964    1.514946    1.516191    1.542988    1.563725    3.030631    3.030632
     4.010821    4.014252    5.850413    5.862876    7.041397    7.043791
 !timer: motran                          cpu_time=     0.009 walltime=     0.009

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.763 walltime=     0.765

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -155.7756316760 demc= 4.3125E-05 wnorm= 3.4211E-05 knorm= 1.9809E-04 apxde= 2.0401E-09    *not conv.*     

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     2.234 walltime=     2.246

 orbital-state coupling will not be calculated this iteration.

 program input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    38, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222788021

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222682661
 address segment size,           sizesg = 222677205
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     648126 transformed 1/r12    array elements were written in     119 records.

 !timer: 2-e transformation              cpu_time=     0.653 walltime=     0.653

 Size of orbital-Hessian matrix B:                   140364
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con         140364


 mosort: allocated sort2 space, avc2is=   222715407 available sort2 space, avcisx=   222715659
 !timer: mosrt1                          cpu_time=     0.066 walltime=     0.066
 !timer: mosrt2                          cpu_time=     0.012 walltime=     0.012
 !timer: mosort                          cpu_time=     0.080 walltime=     0.080
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 30).
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 !timer: hmcxv                           cpu_time=     0.000 walltime=     0.000
 ciiter=   5 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.001 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -155.7757851154     -240.6687333212        0.0000000000        0.0000010000
    2      -155.7754782406     -240.6684264464        0.0000000000        0.0000010000
    3      -155.3440536958     -240.2370019016        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.001 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.001 walltime=     0.001
 !timer: hbcon                           cpu_time=     0.009 walltime=     0.009
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.097467326927030E-007
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 2.2307E-07 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.003 walltime=     0.003

 fdd(*) eigenvalues. symmetry block  1
   -22.877972  -22.877972   -2.437315   -2.437309   -1.549993   -1.549992

 qvv(*) eigenvalues. symmetry block  1
    -0.112153   -0.054829   -0.011230    0.049095    0.058422    0.080105    0.212104    0.273629    0.792148    0.819395
     1.018601    1.073105    1.105361    1.218025    1.512563    1.520075    2.457498    2.457556    2.998145    3.003666
     3.580280    3.585579    5.020945    5.122078    5.747124    5.809256    6.716798    6.720584   48.507977   48.509973

 fdd(*) eigenvalues. symmetry block  2
    -1.666923   -1.666920

 qvv(*) eigenvalues. symmetry block  2
    -0.040667   -0.016425    0.285492    0.305093    0.765104    0.789551    1.295362    1.302467    2.417274    2.417274
     3.010822    3.023237    5.201303    5.219975    6.748993    6.751237

 fdd(*) eigenvalues. symmetry block  3
   -22.875315  -22.875315   -1.971266   -1.971266

 qvv(*) eigenvalues. symmetry block  3
    -0.060636   -0.034801    0.052769    0.129496    0.175722    0.215933    0.337055    0.403783    0.597506    0.600905
     1.027186    1.088000    1.182867    1.246040    1.514803    1.526782    1.802726    1.808721    3.595033    3.639165
     4.088992    4.095338    4.574704    4.576049    5.267204    5.318033    6.118623    6.150143    7.879409    7.884909
    49.940776   49.941098

 fdd(*) eigenvalues. symmetry block  4
    -1.404435   -1.404432

 qvv(*) eigenvalues. symmetry block  4
     0.031815    0.053703    0.496474    0.523964    1.514945    1.516190    1.542987    1.563724    3.030630    3.030631
     4.010820    4.014251    5.850412    5.862875    7.041395    7.043790
 !timer: motran                          cpu_time=     0.009 walltime=     0.009

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.760 walltime=     0.760

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    3 emc=   -155.7756316780 demc= 2.0401E-09 wnorm= 8.7797E-07 knorm= 9.1436E-07 apxde= 1.1275E-13    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.500 total energy=     -155.775785115, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.500 total energy=     -155.775478241, rel. (eV)=   0.008350
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.45076092     1.45066890

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18
  occ(*)=     0.00000000     0.00000000

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.04929140     0.04927877     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration, block  4

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18
  occ(*)=     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        395 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 !timer: writing the mc density files reqcpu_time=     0.000 walltime=     0.001
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     1
 Number of unique bra states (ndbra):                     1
 qind: F
 !timer: rdft_grd                        cpu_time=     0.001 walltime=     0.000
 (Transition) density matrices:
 d1(*) written to the 1-particle density matrix file.
 d1(*) written to the 1-particle density matrix file.
        394 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st02-st01                                      

          NOs of symm. trans. dens. mat.: DRT 1, State  2 - DRT  1, state   1
 *** warning *** small active-orbital occupation. i=  1 nocc=-4.5072E-01
 *** warning *** small active-orbital occupation. i=  1 nocc=-4.9283E-02
 !timer: stspnos                         cpu_time=     0.009 walltime=     0.008

          block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.45071660    -0.45071660     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  2

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18
  occ(*)=     0.00000000     0.00000000

          block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     0.00000000     0.00000000     0.00000000     0.04928339    -0.04928339     0.00000000     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               MO   17        MO   18
  occ(*)=     0.00000000     1.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    10_ s       1.006285   0.993501   0.832205   0.834473   0.015580   0.015324
    10_ p       0.000028   0.000027   0.060550   0.060549   0.747608   0.747477
    10_ d       0.000030   0.000030   0.007183   0.007174   0.009463   0.009459
    10_ s       0.993505   1.006281   0.832190   0.834488   0.015580   0.015324
    10_ p       0.000027   0.000028   0.060549   0.060550   0.747569   0.747516
    10_ d       0.000030   0.000031   0.007183   0.007174   0.009463   0.009459
    10_ s       0.000048   0.000050   0.100071   0.097795   0.227374   0.227714
    11_ s       0.000047   0.000051   0.100069   0.097797   0.227362   0.227726
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    10_ s       0.000181   0.000034   0.000000   0.000000   0.000000   0.000000
    10_ p       0.718180   0.718244   0.000000   0.000000   0.000000   0.000000
    10_ d       0.007078   0.007091   0.000000   0.000000   0.000000   0.000000
    10_ s       0.000181   0.000034   0.000000   0.000000   0.000000   0.000000
    10_ p       0.718183   0.718242   0.000000   0.000000   0.000000   0.000000
    10_ d       0.007078   0.007091   0.000000   0.000000   0.000000   0.000000
    10_ s      -0.000060  -0.000033   0.000000   0.000000   0.000000   0.000000
    11_ s      -0.000060  -0.000033   0.000000   0.000000   0.000000   0.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    10_ p       0.662750   0.662564   0.000000   0.000000   0.000000   0.000000
    10_ d       0.000236   0.000236   0.000000   0.000000   0.000000   0.000000
    10_ p       0.662741   0.662573   0.000000   0.000000   0.000000   0.000000
    10_ d       0.000236   0.000236   0.000000   0.000000   0.000000   0.000000
    10_ s       0.337020   0.337193   0.000000   0.000000   0.000000   0.000000
    11_ s       0.337016   0.337198   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    10_ s       1.013605   0.986415   0.461962   0.456595   0.000009   0.000001
    10_ p      -0.000016  -0.000020   0.222217   0.226177   0.024564   0.024565
    10_ d      -0.000028  -0.000027   0.008054   0.008056   0.000074   0.000073
    10_ s       0.986407   1.013614   0.461941   0.456615   0.000009   0.000001
    10_ p      -0.000015  -0.000021   0.222207   0.226187   0.024563   0.024565
    10_ d      -0.000027  -0.000028   0.008053   0.008057   0.000074   0.000073
    10_ s       0.000037   0.000033   0.307790   0.309149   0.000000   0.000001
    11_ s       0.000036   0.000034   0.307776   0.309163   0.000000   0.000001
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    10_ p       0.539218   0.539707   0.000000   0.000000   0.000000   0.000000
    10_ d       0.028584   0.028581   0.000000   0.000000   0.000000   0.000000
    10_ p       0.539214   0.539711   0.000000   0.000000   0.000000   0.000000
    10_ d       0.028584   0.028582   0.000000   0.000000   0.000000   0.000000
    10_ s       0.432202   0.431708   0.000000   0.000000   0.000000   0.000000
    11_ s       0.432198   0.431711   0.000000   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 


                        gross atomic populations
     ao           10_        10_        10_        11_
      s         6.616170   6.616170   2.808094   2.808094
      p         5.954389   5.954389   0.000000   0.000000
      d         0.121348   0.121348   0.000000   0.000000
    total      12.691906  12.691906   2.808094   2.808094
 

 Total number of electrons:   31.00000000

 Off-diagonal Mulliken population for:
DRT 1,state 02 - DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    10_ s       0.000005  -0.000062   0.000000   0.000000   0.000000   0.000000
    10_ p       0.000008  -0.446269   0.000000   0.000000   0.000000   0.000000
    10_ d       0.000000  -0.004402   0.000000   0.000000   0.000000   0.000000
    10_ s       0.000062  -0.000005   0.000000   0.000000   0.000000   0.000000
    10_ p       0.446269  -0.000008   0.000000   0.000000   0.000000   0.000000
    10_ d       0.004402   0.000000   0.000000   0.000000   0.000000   0.000000
    10_ s      -0.000001   0.000028   0.000000   0.000000   0.000000   0.000000
    11_ s      -0.000028   0.000001   0.000000   0.000000   0.000000   0.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    10_ s       0.000000   0.000000   0.000000   0.000000   0.000004  -0.000006
    10_ p       0.000000   0.000000   0.000000   0.000000  -0.000002  -0.049128
    10_ d       0.000000   0.000000   0.000000   0.000000   0.000000  -0.000147
    10_ s       0.000000   0.000000   0.000000   0.000000   0.000006  -0.000004
    10_ p       0.000000   0.000000   0.000000   0.000000   0.049128   0.000002
    10_ d       0.000000   0.000000   0.000000   0.000000   0.000147   0.000000
    10_ s       0.000000   0.000000   0.000000   0.000000   0.000000  -0.000001
    11_ s       0.000000   0.000000   0.000000   0.000000   0.000001   0.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 


                        gross atomic populations
     ao           10_        10_        10_        11_
      s        -0.000059   0.000059   0.000026  -0.000026
      p        -0.495391   0.495391   0.000000   0.000000
      d        -0.004549   0.004549   0.000000   0.000000
    total      -0.499998   0.499998   0.000026  -0.000026
 

 Total number of electrons:    0.00000000

 !timer: mcscf                           cpu_time=     3.024 walltime=     3.037
