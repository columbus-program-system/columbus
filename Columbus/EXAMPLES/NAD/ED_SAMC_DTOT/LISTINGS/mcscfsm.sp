 total ao core energy =   84.892948206
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          2             0.500 0.500

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    31
 Spin multiplicity:            2
 Number of active orbitals:    4
 Number of active electrons:   3
 Total number of CSFs:        10

 Number of active-double rotations:        20
 Number of active-active rotations:         0
 Number of double-virtual rotations:      372
 Number of active-virtual rotations:      124
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -155.7755885506  1.558E+02  1.898E-02  1.043E-02  4.299E-05  F   *not conv.*     
    2   -155.7756316760  4.313E-05  3.421E-05  1.981E-04  2.040E-09  F   *not conv.*     

 final mcscf convergence values:
    3   -155.7756316780  2.040E-09  8.780E-07  9.144E-07  1.127E-13  F   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.500 total energy=     -155.775785115, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.500 total energy=     -155.775478241, rel. (eV)=   0.008350
   ------------------------------------------------------------


