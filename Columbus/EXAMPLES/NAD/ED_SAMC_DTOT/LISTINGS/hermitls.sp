
 Work memory size (LMWORK) :   222822400 = 1700.00 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   12



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:   12

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        2       6      35      22      [12s6p1d|5s4p1d]                       
  C  2        2       6      35      22      [12s6p1d|5s4p1d]                       
  H  1        4       1       5       3      [5s|3s]                                
  H  2        4       1       5       3      [5s|3s]                                
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:     12      32     180     112

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 36


   1   C  1 1   x      0.0000000000
   2            y      1.3005735900
   3            z      6.6140409600

   4   C  1 2   x      0.0000000000
   5            y     -1.3005735900
   6            z      6.6140409600

   7   C  2 1   x      0.0000000000
   8            y      1.3005735900
   9            z     -6.6140409600

  10   C  2 2   x      0.0000000000
  11            y     -1.3005735900
  12            z     -6.6140409600

  13   H  1 1   x      1.7366309400
  14            y      2.3500879800
  15            z      6.6140409600

  16   H  1 2   x     -1.7366309400
  17            y      2.3500879800
  18            z      6.6140409600

  19   H  1 3   x      1.7366309400
  20            y     -2.3500879800
  21            z      6.6140409600

  22   H  1 4   x     -1.7366309400
  23            y     -2.3500879800
  24            z      6.6140409600

  25   H  2 1   x      1.7366309400
  26            y      2.3500879800
  27            z     -6.6140409600

  28   H  2 2   x     -1.7366309400
  29            y      2.3500879800
  30            z     -6.6140409600

  31   H  2 3   x      1.7366309400
  32            y     -2.3500879800
  33            z     -6.6140409600

  34   H  2 4   x     -1.7366309400
  35            y     -2.3500879800
  36            z     -6.6140409600



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:  10  8 10  8


  Symmetry 1

   1   C  1  y    [ 2  -  5 ]/2
   2   C  1  z    [ 3  +  6 ]/2
   3   C  2  y    [ 8  - 11 ]/2
   4   C  2  z    [ 9  + 12 ]/2
   5   H  1  x    [13  - 16  + 19  - 22 ]/4
   6   H  1  y    [14  + 17  - 20  - 23 ]/4
   7   H  1  z    [15  + 18  + 21  + 24 ]/4
   8   H  2  x    [25  - 28  + 31  - 34 ]/4
   9   H  2  y    [26  + 29  - 32  - 35 ]/4
  10   H  2  z    [27  + 30  + 33  + 36 ]/4


  Symmetry 2

  11   C  1  x    [ 1  +  4 ]/2
  12   C  2  x    [ 7  + 10 ]/2
  13   H  1  x    [13  + 16  + 19  + 22 ]/4
  14   H  1  y    [14  - 17  - 20  + 23 ]/4
  15   H  1  z    [15  - 18  + 21  - 24 ]/4
  16   H  2  x    [25  + 28  + 31  + 34 ]/4
  17   H  2  y    [26  - 29  - 32  + 35 ]/4
  18   H  2  z    [27  - 30  + 33  - 36 ]/4


  Symmetry 3

  19   C  1  y    [ 2  +  5 ]/2
  20   C  1  z    [ 3  -  6 ]/2
  21   C  2  y    [ 8  + 11 ]/2
  22   C  2  z    [ 9  - 12 ]/2
  23   H  1  x    [13  - 16  - 19  + 22 ]/4
  24   H  1  y    [14  + 17  + 20  + 23 ]/4
  25   H  1  z    [15  + 18  - 21  - 24 ]/4
  26   H  2  x    [25  - 28  - 31  + 34 ]/4
  27   H  2  y    [26  + 29  + 32  + 35 ]/4
  28   H  2  z    [27  + 30  - 33  - 36 ]/4


  Symmetry 4

  29   C  1  x    [ 1  -  4 ]/2
  30   C  2  x    [ 7  - 10 ]/2
  31   H  1  x    [13  + 16  - 19  - 22 ]/4
  32   H  1  y    [14  - 17  + 20  - 23 ]/4
  33   H  1  z    [15  - 18  - 21  + 24 ]/4
  34   H  2  x    [25  + 28  - 31  - 34 ]/4
  35   H  2  y    [26  - 29  + 32  - 35 ]/4
  36   H  2  z    [27  - 30  - 33  + 36 ]/4


   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        C  2        C  1        C  2        H  1        H  2

   C  1    0.000000
   C  2    1.376467    0.000000
   C  1    6.999997    7.134047    0.000000
   C  2    7.134047    6.999997    1.376467    0.000000
   H  1    1.073769    2.139291    7.081874    7.319599    0.000000
   H  2    1.073769    2.139291    7.081874    7.319599    1.837971    0.000000
   H  3    2.139291    1.073769    7.319599    7.081874    2.487225    3.092641
   H  4    2.139291    1.073769    7.319599    7.081874    3.092641    2.487225
   H  1    7.081874    7.319599    1.073769    2.139291    6.999997    7.237272
   H  2    7.081874    7.319599    1.073769    2.139291    7.237272    6.999997
   H  3    7.319599    7.081874    2.139291    1.073769    7.428745    7.652737
   H  4    7.319599    7.081874    2.139291    1.073769    7.652737    7.428745

            H  3        H  4        H  1        H  2        H  3        H  4

   H  3    0.000000
   H  4    1.837971    0.000000
   H  1    7.428745    7.652737    0.000000
   H  2    7.652737    7.428745    1.837971    0.000000
   H  3    6.999997    7.237272    2.487225    3.092641    0.000000
   H  4    7.237272    6.999997    3.092641    2.487225    1.837971    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    C  2       C  1                           1.376467
  bond distance:    C  2       C  1                           1.376467
  bond distance:    H  1       C  1                           1.073769
  bond distance:    H  2       C  1                           1.073769
  bond distance:    H  3       C  2                           1.073769
  bond distance:    H  4       C  2                           1.073769
  bond distance:    H  1       C  1                           1.073769
  bond distance:    H  2       C  1                           1.073769
  bond distance:    H  3       C  2                           1.073769
  bond distance:    H  4       C  2                           1.073769


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       C  2                 121.146
  bond angle:       H  2       C  1       C  2                 121.146
  bond angle:       H  2       C  1       H  1                 117.708
  bond angle:       H  3       C  2       C  1                 121.146
  bond angle:       H  4       C  2       C  1                 121.146
  bond angle:       H  4       C  2       H  3                 117.708
  bond angle:       H  1       C  1       C  2                 121.146
  bond angle:       H  2       C  1       C  2                 121.146
  bond angle:       H  2       C  1       H  1                 117.708
  bond angle:       H  3       C  2       C  1                 121.146
  bond angle:       H  4       C  2       C  1                 121.146
  bond angle:       H  4       C  2       H  3                 117.708


  Nuclear repulsion energy :   84.892948205759


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1#1 1s    1     4563.240000    0.0020  0.0000  0.0000  0.0000  0.0000
   seg. cont.  2      682.024000    0.0152  0.0000  0.0000  0.0000  0.0000
               3      154.973000    0.0761  0.0000  0.0000  0.0000  0.0000
               4       44.455300    0.2608  0.0000  0.0000  0.0000  0.0000
               5       13.029000    0.6165  0.0000  0.0000  0.0000  0.0000
               6        1.827730    0.2210  0.0000  0.0000  0.0000  0.0000
               7       20.964200    0.0000  0.1147  0.0000  0.0000  0.0000
               8        4.803310    0.0000  0.9200  0.0000  0.0000  0.0000
               9        1.459330    0.0000 -0.0030  0.0000  0.0000  0.0000
              10        0.483456    0.0000  0.0000  1.0000  0.0000  0.0000
              11        0.145585    0.0000  0.0000  0.0000  1.0000  0.0000
              12        0.043800    0.0000  0.0000  0.0000  0.0000  1.0000

  C  1#2 1s   13     4563.240000    0.0020  0.0000  0.0000  0.0000  0.0000
   seg. cont. 14      682.024000    0.0152  0.0000  0.0000  0.0000  0.0000
              15      154.973000    0.0761  0.0000  0.0000  0.0000  0.0000
              16       44.455300    0.2608  0.0000  0.0000  0.0000  0.0000
              17       13.029000    0.6165  0.0000  0.0000  0.0000  0.0000
              18        1.827730    0.2210  0.0000  0.0000  0.0000  0.0000
              19       20.964200    0.0000  0.1147  0.0000  0.0000  0.0000
              20        4.803310    0.0000  0.9200  0.0000  0.0000  0.0000
              21        1.459330    0.0000 -0.0030  0.0000  0.0000  0.0000
              22        0.483456    0.0000  0.0000  1.0000  0.0000  0.0000
              23        0.145585    0.0000  0.0000  0.0000  1.0000  0.0000
              24        0.043800    0.0000  0.0000  0.0000  0.0000  1.0000

  C  1#1 2px  25       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont. 26        4.803310    0.2376  0.0000  0.0000  0.0000
              27        1.459330    0.8159  0.0000  0.0000  0.0000
              28        0.483456    0.0000  1.0000  0.0000  0.0000
              29        0.145585    0.0000  0.0000  1.0000  0.0000
              30        0.043800    0.0000  0.0000  0.0000  1.0000

  C  1#2 2px  31       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont. 32        4.803310    0.2376  0.0000  0.0000  0.0000
              33        1.459330    0.8159  0.0000  0.0000  0.0000
              34        0.483456    0.0000  1.0000  0.0000  0.0000
              35        0.145585    0.0000  0.0000  1.0000  0.0000
              36        0.043800    0.0000  0.0000  0.0000  1.0000

  C  1#1 2py  37       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont. 38        4.803310    0.2376  0.0000  0.0000  0.0000
              39        1.459330    0.8159  0.0000  0.0000  0.0000
              40        0.483456    0.0000  1.0000  0.0000  0.0000
              41        0.145585    0.0000  0.0000  1.0000  0.0000
              42        0.043800    0.0000  0.0000  0.0000  1.0000

  C  1#2 2py  43       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont. 44        4.803310    0.2376  0.0000  0.0000  0.0000
              45        1.459330    0.8159  0.0000  0.0000  0.0000
              46        0.483456    0.0000  1.0000  0.0000  0.0000
              47        0.145585    0.0000  0.0000  1.0000  0.0000
              48        0.043800    0.0000  0.0000  0.0000  1.0000

  C  1#1 2pz  49       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont. 50        4.803310    0.2376  0.0000  0.0000  0.0000
              51        1.459330    0.8159  0.0000  0.0000  0.0000
              52        0.483456    0.0000  1.0000  0.0000  0.0000
              53        0.145585    0.0000  0.0000  1.0000  0.0000
              54        0.043800    0.0000  0.0000  0.0000  1.0000

  C  1#2 2pz  55       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont. 56        4.803310    0.2376  0.0000  0.0000  0.0000
              57        1.459330    0.8159  0.0000  0.0000  0.0000
              58        0.483456    0.0000  1.0000  0.0000  0.0000
              59        0.145585    0.0000  0.0000  1.0000  0.0000
              60        0.043800    0.0000  0.0000  0.0000  1.0000

  C  1#1 3d2- 61        0.626000    1.0000

  C  1#2 3d2- 62        0.626000    1.0000

  C  1#1 3d1- 63        0.626000    1.0000

  C  1#2 3d1- 64        0.626000    1.0000

  C  1#1 3d0  65        0.626000    1.0000

  C  1#2 3d0  66        0.626000    1.0000

  C  1#1 3d1+ 67        0.626000    1.0000

  C  1#2 3d1+ 68        0.626000    1.0000

  C  1#1 3d2+ 69        0.626000    1.0000

  C  1#2 3d2+ 70        0.626000    1.0000

  C  2#1 1s   71     4563.240000    0.0020  0.0000  0.0000  0.0000  0.0000
   seg. cont. 72      682.024000    0.0152  0.0000  0.0000  0.0000  0.0000
              73      154.973000    0.0761  0.0000  0.0000  0.0000  0.0000
              74       44.455300    0.2608  0.0000  0.0000  0.0000  0.0000
              75       13.029000    0.6165  0.0000  0.0000  0.0000  0.0000
              76        1.827730    0.2210  0.0000  0.0000  0.0000  0.0000
              77       20.964200    0.0000  0.1147  0.0000  0.0000  0.0000
              78        4.803310    0.0000  0.9200  0.0000  0.0000  0.0000
              79        1.459330    0.0000 -0.0030  0.0000  0.0000  0.0000
              80        0.483456    0.0000  0.0000  1.0000  0.0000  0.0000
              81        0.145585    0.0000  0.0000  0.0000  1.0000  0.0000
              82        0.043800    0.0000  0.0000  0.0000  0.0000  1.0000

  C  2#2 1s   83     4563.240000    0.0020  0.0000  0.0000  0.0000  0.0000
   seg. cont. 84      682.024000    0.0152  0.0000  0.0000  0.0000  0.0000
              85      154.973000    0.0761  0.0000  0.0000  0.0000  0.0000
              86       44.455300    0.2608  0.0000  0.0000  0.0000  0.0000
              87       13.029000    0.6165  0.0000  0.0000  0.0000  0.0000
              88        1.827730    0.2210  0.0000  0.0000  0.0000  0.0000
              89       20.964200    0.0000  0.1147  0.0000  0.0000  0.0000
              90        4.803310    0.0000  0.9200  0.0000  0.0000  0.0000
              91        1.459330    0.0000 -0.0030  0.0000  0.0000  0.0000
              92        0.483456    0.0000  0.0000  1.0000  0.0000  0.0000
              93        0.145585    0.0000  0.0000  0.0000  1.0000  0.0000
              94        0.043800    0.0000  0.0000  0.0000  0.0000  1.0000

  C  2#1 2px  95       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont. 96        4.803310    0.2376  0.0000  0.0000  0.0000
              97        1.459330    0.8159  0.0000  0.0000  0.0000
              98        0.483456    0.0000  1.0000  0.0000  0.0000
              99        0.145585    0.0000  0.0000  1.0000  0.0000
             100        0.043800    0.0000  0.0000  0.0000  1.0000

  C  2#2 2px 101       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont.102        4.803310    0.2376  0.0000  0.0000  0.0000
             103        1.459330    0.8159  0.0000  0.0000  0.0000
             104        0.483456    0.0000  1.0000  0.0000  0.0000
             105        0.145585    0.0000  0.0000  1.0000  0.0000
             106        0.043800    0.0000  0.0000  0.0000  1.0000

  C  2#1 2py 107       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont.108        4.803310    0.2376  0.0000  0.0000  0.0000
             109        1.459330    0.8159  0.0000  0.0000  0.0000
             110        0.483456    0.0000  1.0000  0.0000  0.0000
             111        0.145585    0.0000  0.0000  1.0000  0.0000
             112        0.043800    0.0000  0.0000  0.0000  1.0000

  C  2#2 2py 113       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont.114        4.803310    0.2376  0.0000  0.0000  0.0000
             115        1.459330    0.8159  0.0000  0.0000  0.0000
             116        0.483456    0.0000  1.0000  0.0000  0.0000
             117        0.145585    0.0000  0.0000  1.0000  0.0000
             118        0.043800    0.0000  0.0000  0.0000  1.0000

  C  2#1 2pz 119       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont.120        4.803310    0.2376  0.0000  0.0000  0.0000
             121        1.459330    0.8159  0.0000  0.0000  0.0000
             122        0.483456    0.0000  1.0000  0.0000  0.0000
             123        0.145585    0.0000  0.0000  1.0000  0.0000
             124        0.043800    0.0000  0.0000  0.0000  1.0000

  C  2#2 2pz 125       20.964200    0.0402  0.0000  0.0000  0.0000
   seg. cont.126        4.803310    0.2376  0.0000  0.0000  0.0000
             127        1.459330    0.8159  0.0000  0.0000  0.0000
             128        0.483456    0.0000  1.0000  0.0000  0.0000
             129        0.145585    0.0000  0.0000  1.0000  0.0000
             130        0.043800    0.0000  0.0000  0.0000  1.0000

  C  2#1 3d2-131        0.626000    1.0000

  C  2#2 3d2-132        0.626000    1.0000

  C  2#1 3d1-133        0.626000    1.0000

  C  2#2 3d1-134        0.626000    1.0000

  C  2#1 3d0 135        0.626000    1.0000

  C  2#2 3d0 136        0.626000    1.0000

  C  2#1 3d1+137        0.626000    1.0000

  C  2#2 3d1+138        0.626000    1.0000

  C  2#1 3d2+139        0.626000    1.0000

  C  2#2 3d2+140        0.626000    1.0000

  H  1#1 1s  141       33.865000    0.0255  0.0000  0.0000
   seg. cont.142        5.094790    0.1904  0.0000  0.0000
             143        1.158790    0.8522  0.0000  0.0000
             144        0.325840    0.0000  1.0000  0.0000
             145        0.102741    0.0000  0.0000  1.0000

  H  1#2 1s  146       33.865000    0.0255  0.0000  0.0000
   seg. cont.147        5.094790    0.1904  0.0000  0.0000
             148        1.158790    0.8522  0.0000  0.0000
             149        0.325840    0.0000  1.0000  0.0000
             150        0.102741    0.0000  0.0000  1.0000

  H  1#3 1s  151       33.865000    0.0255  0.0000  0.0000
   seg. cont.152        5.094790    0.1904  0.0000  0.0000
             153        1.158790    0.8522  0.0000  0.0000
             154        0.325840    0.0000  1.0000  0.0000
             155        0.102741    0.0000  0.0000  1.0000

  H  1#4 1s  156       33.865000    0.0255  0.0000  0.0000
   seg. cont.157        5.094790    0.1904  0.0000  0.0000
             158        1.158790    0.8522  0.0000  0.0000
             159        0.325840    0.0000  1.0000  0.0000
             160        0.102741    0.0000  0.0000  1.0000

  H  2#1 1s  161       33.865000    0.0255  0.0000  0.0000
   seg. cont.162        5.094790    0.1904  0.0000  0.0000
             163        1.158790    0.8522  0.0000  0.0000
             164        0.325840    0.0000  1.0000  0.0000
             165        0.102741    0.0000  0.0000  1.0000

  H  2#2 1s  166       33.865000    0.0255  0.0000  0.0000
   seg. cont.167        5.094790    0.1904  0.0000  0.0000
             168        1.158790    0.8522  0.0000  0.0000
             169        0.325840    0.0000  1.0000  0.0000
             170        0.102741    0.0000  0.0000  1.0000

  H  2#3 1s  171       33.865000    0.0255  0.0000  0.0000
   seg. cont.172        5.094790    0.1904  0.0000  0.0000
             173        1.158790    0.8522  0.0000  0.0000
             174        0.325840    0.0000  1.0000  0.0000
             175        0.102741    0.0000  0.0000  1.0000

  H  2#4 1s  176       33.865000    0.0255  0.0000  0.0000
   seg. cont.177        5.094790    0.1904  0.0000  0.0000
             178        1.158790    0.8522  0.0000  0.0000
             179        0.325840    0.0000  1.0000  0.0000
             180        0.102741    0.0000  0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  C  1#1  1s     1   2   3   4   5   6
   2  C  1#2  1s    13  14  15  16  17  18
   3  C  1#1  1s     7   8   9
   4  C  1#2  1s    19  20  21
   5  C  1#1  1s    10
   6  C  1#2  1s    22
   7  C  1#1  1s    11
   8  C  1#2  1s    23
   9  C  1#1  1s    12
  10  C  1#2  1s    24
  11  C  1#1  2px   25  26  27
  12  C  1#2  2px   31  32  33
  13  C  1#1  2py   37  38  39
  14  C  1#2  2py   43  44  45
  15  C  1#1  2pz   49  50  51
  16  C  1#2  2pz   55  56  57
  17  C  1#1  2px   28
  18  C  1#2  2px   34
  19  C  1#1  2py   40
  20  C  1#2  2py   46
  21  C  1#1  2pz   52
  22  C  1#2  2pz   58
  23  C  1#1  2px   29
  24  C  1#2  2px   35
  25  C  1#1  2py   41
  26  C  1#2  2py   47
  27  C  1#1  2pz   53
  28  C  1#2  2pz   59
  29  C  1#1  2px   30
  30  C  1#2  2px   36
  31  C  1#1  2py   42
  32  C  1#2  2py   48
  33  C  1#1  2pz   54
  34  C  1#2  2pz   60
  35  C  1#1  3d2-  61
  36  C  1#2  3d2-  62
  37  C  1#1  3d1-  63
  38  C  1#2  3d1-  64
  39  C  1#1  3d0   65
  40  C  1#2  3d0   66
  41  C  1#1  3d1+  67
  42  C  1#2  3d1+  68
  43  C  1#1  3d2+  69
  44  C  1#2  3d2+  70
  45  C  2#1  1s    71  72  73  74  75  76
  46  C  2#2  1s    83  84  85  86  87  88
  47  C  2#1  1s    77  78  79
  48  C  2#2  1s    89  90  91
  49  C  2#1  1s    80
  50  C  2#2  1s    92
  51  C  2#1  1s    81
  52  C  2#2  1s    93
  53  C  2#1  1s    82
  54  C  2#2  1s    94
  55  C  2#1  2px   95  96  97
  56  C  2#2  2px  101 102 103
  57  C  2#1  2py  107 108 109
  58  C  2#2  2py  113 114 115
  59  C  2#1  2pz  119 120 121
  60  C  2#2  2pz  125 126 127
  61  C  2#1  2px   98
  62  C  2#2  2px  104
  63  C  2#1  2py  110
  64  C  2#2  2py  116
  65  C  2#1  2pz  122
  66  C  2#2  2pz  128
  67  C  2#1  2px   99
  68  C  2#2  2px  105
  69  C  2#1  2py  111
  70  C  2#2  2py  117
  71  C  2#1  2pz  123
  72  C  2#2  2pz  129
  73  C  2#1  2px  100
  74  C  2#2  2px  106
  75  C  2#1  2py  112
  76  C  2#2  2py  118
  77  C  2#1  2pz  124
  78  C  2#2  2pz  130
  79  C  2#1  3d2- 131
  80  C  2#2  3d2- 132
  81  C  2#1  3d1- 133
  82  C  2#2  3d1- 134
  83  C  2#1  3d0  135
  84  C  2#2  3d0  136
  85  C  2#1  3d1+ 137
  86  C  2#2  3d1+ 138
  87  C  2#1  3d2+ 139
  88  C  2#2  3d2+ 140
  89  H  1#1  1s   141 142 143
  90  H  1#2  1s   146 147 148
  91  H  1#3  1s   151 152 153
  92  H  1#4  1s   156 157 158
  93  H  1#1  1s   144
  94  H  1#2  1s   149
  95  H  1#3  1s   154
  96  H  1#4  1s   159
  97  H  1#1  1s   145
  98  H  1#2  1s   150
  99  H  1#3  1s   155
 100  H  1#4  1s   160
 101  H  2#1  1s   161 162 163
 102  H  2#2  1s   166 167 168
 103  H  2#3  1s   171 172 173
 104  H  2#4  1s   176 177 178
 105  H  2#1  1s   164
 106  H  2#2  1s   169
 107  H  2#3  1s   174
 108  H  2#4  1s   179
 109  H  2#1  1s   165
 110  H  2#2  1s   170
 111  H  2#3  1s   175
 112  H  2#4  1s   180




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        38 18 38 18


  Symmetry  A1 ( 1)

    1     C  1     1s         1  +   2
    2     C  1     1s         3  +   4
    3     C  1     1s         5  +   6
    4     C  1     1s         7  +   8
    5     C  1     1s         9  +  10
    6     C  1     2py       13  -  14
    7     C  1     2pz       15  +  16
    8     C  1     2py       19  -  20
    9     C  1     2pz       21  +  22
   10     C  1     2py       25  -  26
   11     C  1     2pz       27  +  28
   12     C  1     2py       31  -  32
   13     C  1     2pz       33  +  34
   14     C  1     3d1-      37  -  38
   15     C  1     3d0       39  +  40
   16     C  1     3d2+      43  +  44
   17     C  2     1s        45  +  46
   18     C  2     1s        47  +  48
   19     C  2     1s        49  +  50
   20     C  2     1s        51  +  52
   21     C  2     1s        53  +  54
   22     C  2     2py       57  -  58
   23     C  2     2pz       59  +  60
   24     C  2     2py       63  -  64
   25     C  2     2pz       65  +  66
   26     C  2     2py       69  -  70
   27     C  2     2pz       71  +  72
   28     C  2     2py       75  -  76
   29     C  2     2pz       77  +  78
   30     C  2     3d1-      81  -  82
   31     C  2     3d0       83  +  84
   32     C  2     3d2+      87  +  88
   33     H  1     1s        89  +  90  +  91  +  92
   34     H  1     1s        93  +  94  +  95  +  96
   35     H  1     1s        97  +  98  +  99  + 100
   36     H  2     1s       101  + 102  + 103  + 104
   37     H  2     1s       105  + 106  + 107  + 108
   38     H  2     1s       109  + 110  + 111  + 112


  Symmetry  B1 ( 2)

   39     C  1     2px       11  +  12
   40     C  1     2px       17  +  18
   41     C  1     2px       23  +  24
   42     C  1     2px       29  +  30
   43     C  1     3d2-      35  -  36
   44     C  1     3d1+      41  +  42
   45     C  2     2px       55  +  56
   46     C  2     2px       61  +  62
   47     C  2     2px       67  +  68
   48     C  2     2px       73  +  74
   49     C  2     3d2-      79  -  80
   50     C  2     3d1+      85  +  86
   51     H  1     1s        89  -  90  +  91  -  92
   52     H  1     1s        93  -  94  +  95  -  96
   53     H  1     1s        97  -  98  +  99  - 100
   54     H  2     1s       101  - 102  + 103  - 104
   55     H  2     1s       105  - 106  + 107  - 108
   56     H  2     1s       109  - 110  + 111  - 112


  Symmetry  B2 ( 3)

   57     C  1     1s         1  -   2
   58     C  1     1s         3  -   4
   59     C  1     1s         5  -   6
   60     C  1     1s         7  -   8
   61     C  1     1s         9  -  10
   62     C  1     2py       13  +  14
   63     C  1     2pz       15  -  16
   64     C  1     2py       19  +  20
   65     C  1     2pz       21  -  22
   66     C  1     2py       25  +  26
   67     C  1     2pz       27  -  28
   68     C  1     2py       31  +  32
   69     C  1     2pz       33  -  34
   70     C  1     3d1-      37  +  38
   71     C  1     3d0       39  -  40
   72     C  1     3d2+      43  -  44
   73     C  2     1s        45  -  46
   74     C  2     1s        47  -  48
   75     C  2     1s        49  -  50
   76     C  2     1s        51  -  52
   77     C  2     1s        53  -  54
   78     C  2     2py       57  +  58
   79     C  2     2pz       59  -  60
   80     C  2     2py       63  +  64
   81     C  2     2pz       65  -  66
   82     C  2     2py       69  +  70
   83     C  2     2pz       71  -  72
   84     C  2     2py       75  +  76
   85     C  2     2pz       77  -  78
   86     C  2     3d1-      81  +  82
   87     C  2     3d0       83  -  84
   88     C  2     3d2+      87  -  88
   89     H  1     1s        89  +  90  -  91  -  92
   90     H  1     1s        93  +  94  -  95  -  96
   91     H  1     1s        97  +  98  -  99  - 100
   92     H  2     1s       101  + 102  - 103  - 104
   93     H  2     1s       105  + 106  - 107  - 108
   94     H  2     1s       109  + 110  - 111  - 112


  Symmetry  A2 ( 4)

   95     C  1     2px       11  -  12
   96     C  1     2px       17  -  18
   97     C  1     2px       23  -  24
   98     C  1     2px       29  -  30
   99     C  1     3d2-      35  +  36
  100     C  1     3d1+      41  -  42
  101     C  2     2px       55  -  56
  102     C  2     2px       61  -  62
  103     C  2     2px       67  -  68
  104     C  2     2px       73  -  74
  105     C  2     3d2-      79  +  80
  106     C  2     3d1+      85  -  86
  107     H  1     1s        89  -  90  -  91  +  92
  108     H  1     1s        93  -  94  -  95  +  96
  109     H  1     1s        97  -  98  -  99  + 100
  110     H  2     1s       101  - 102  - 103  + 104
  111     H  2     1s       105  - 106  - 107  + 108
  112     H  2     1s       109  - 110  - 111  + 112

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10D-14                                                   
       6.0    2    3    5    4    1                                             
C  1   0.000000000000000   1.300573590000000   6.614040960000000       *        
C  2   0.000000000000000   1.300573590000000  -6.614040960000000       *        
H   6   1                                                                       
       4563.24000000         0.00196665                                         
        682.02400000         0.01523060                                         
        154.97300000         0.07612690                                         
         44.45530000         0.26080100                                         
         13.02900000         0.61646200                                         
          1.82773000         0.22100600                                         
H   3   1                                                                       
         20.96420000         0.11466000                                         
          4.80331000         0.91999900                                         
          1.45933000        -0.00303068                                         
H   1   1                                                                       
          0.48345600         1.00000000                                         
H   1   1                                                                       
          0.14558500         1.00000000                                         
H   1   1                                                                       
          0.04380000         1.00000000                                         
H   3   1                                                                       
         20.96420000         0.04024870                                         
          4.80331000         0.23759400                                         
          1.45933000         0.81585400                                         
H   1   1                                                                       
          0.48345600         1.00000000                                         
H   1   1                                                                       
          0.14558500         1.00000000                                         
H   1   1                                                                       
          0.04380000         1.00000000                                         
H   1   1                                                                       
          0.62600000         1.00000000                                         
       1.0    2    1    3                                                       
H  1   1.736630940000000   2.350087980000000   6.614040960000000       *        
H  2   1.736630940000000   2.350087980000000  -6.614040960000000       *        
H   3   1                                                                       
         33.86500000         0.02549380                                         
          5.09479000         0.19037300                                         
          1.15879000         0.85216100                                         
H   1   1                                                                       
          0.32584000         1.00000000                                         
H   1   1                                                                       
          0.10274100         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

found    1280 non-vanashing overlap integrals
found    1496 non-vanashing nuclear attraction integrals
found    1250 non-vanashing kinetic energy integrals






 found     936 non-vanashing integrals ( typea=  1 typeb=  0)
 found    1258 non-vanashing integrals ( typea=  1 typeb=  1)
 found    1458 non-vanashing integrals ( typea=  1 typeb=  2)


 found    1272 non-vanashing integrals ( typea=  1 typeb=  3)
 found     974 non-vanashing integrals ( typea=  1 typeb=  4)
 found    1084 non-vanashing integrals ( typea=  1 typeb=  5)
 found    1362 non-vanashing integrals ( typea=  1 typeb=  6)
 found    1408 non-vanashing integrals ( typea=  1 typeb=  7)
 found    1496 non-vanashing integrals ( typea=  1 typeb=  8)


 found     966 non-vanashing integrals ( typea=  1 typeb=  9)
 found    1242 non-vanashing integrals ( typea=  1 typeb= 10)
 found    1460 non-vanashing integrals ( typea=  1 typeb= 11)
 found    1018 non-vanashing integrals ( typea=  1 typeb= 12)
 found    1088 non-vanashing integrals ( typea=  1 typeb= 13)
 found    1110 non-vanashing integrals ( typea=  1 typeb= 14)
 found    1326 non-vanashing integrals ( typea=  1 typeb= 15)
 found    1464 non-vanashing integrals ( typea=  1 typeb= 16)
 found    1440 non-vanashing integrals ( typea=  1 typeb= 17)
 found    1464 non-vanashing integrals ( typea=  1 typeb= 18)


 found    1286 non-vanashing integrals ( typea=  1 typeb= 19)
 found    1000 non-vanashing integrals ( typea=  1 typeb= 20)
 found    1088 non-vanashing integrals ( typea=  1 typeb= 21)
 found    1326 non-vanashing integrals ( typea=  1 typeb= 22)
 found    1408 non-vanashing integrals ( typea=  1 typeb= 23)
 found    1498 non-vanashing integrals ( typea=  1 typeb= 24)
 found    1040 non-vanashing integrals ( typea=  1 typeb= 25)
 found    1092 non-vanashing integrals ( typea=  1 typeb= 26)
 found    1114 non-vanashing integrals ( typea=  1 typeb= 27)
 found    1092 non-vanashing integrals ( typea=  1 typeb= 28)
 found    1400 non-vanashing integrals ( typea=  1 typeb= 29)
 found    1416 non-vanashing integrals ( typea=  1 typeb= 30)
 found    1502 non-vanashing integrals ( typea=  1 typeb= 31)
 found    1412 non-vanashing integrals ( typea=  1 typeb= 32)
 found    1502 non-vanashing integrals ( typea=  1 typeb= 33)


 found    1440 non-vanashing integrals ( typea=  2 typeb=  6)
 found    1102 non-vanashing integrals ( typea=  2 typeb=  7)
 found     948 non-vanashing integrals ( typea=  2 typeb=  8)




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:   2810053 (14.0%)
 Kilobytes written:                            44999




 >>>> Total CPU  time used in HERMIT:   1.18 seconds
 >>>> Total wall time used in HERMIT:   2.00 seconds

- End of Integral Section
