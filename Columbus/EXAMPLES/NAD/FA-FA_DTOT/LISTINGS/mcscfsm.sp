 total ao core energy =  100.585552274
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          3             0.333 0.333 0.333

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105

 Number of active-double rotations:        72
 Number of active-active rotations:         0
 Number of double-virtual rotations:      312
 Number of active-virtual rotations:      156
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -227.5825318684  2.276E+02  2.750E-07  2.214E-07  1.528E-14  F   *not conv.*     
    2   -227.5825318684  2.274E-13  1.731E-08  1.201E-08 -5.559E-17  F   *not conv.*     

 final mcscf convergence values:
    3   -227.5825318684  2.558E-13  3.778E-09  1.512E-09  1.480E-16  F   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=     -227.683128255, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.333 total energy=     -227.532446591, rel. (eV)=   4.100258
   DRT #1 state # 3 wt 0.333 total energy=     -227.532020759, rel. (eV)=   4.111846
   ------------------------------------------------------------


