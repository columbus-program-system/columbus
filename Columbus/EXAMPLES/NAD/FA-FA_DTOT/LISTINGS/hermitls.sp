
 Work memory size (LMWORK) :   222822400 =    0.00 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   10



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


                      SYMGRP:Point group information
                      ------------------------------

Point group: C1 

   * Character table

        |  E 
   -----+-----
    A   |   1

   * Direct product table

        | A  
   -----+-----
    A   | A  


  Atoms and basis sets
  --------------------

  Number of atom types:     3
  Total number of atoms:    8

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        1       6      22       9      [10s4p|3s2p]                           
  C  2        1       6      22       9      [10s4p|3s2p]                           
  O  1        1       8      22       9      [10s4p|3s2p]                           
  O  2        1       8      22       9      [10s4p|3s2p]                           
  H  1        1       1       4       2      [4s|2s]                                
  H  2        1       1       4       2      [4s|2s]                                
  H  3        1       1       4       2      [4s|2s]                                
  H  4        1       1       4       2      [4s|2s]                                
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      8      32     104      44

  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 24


   1   C  1     x      3.3070204300
   2            y      0.0000000000
   3            z      1.1191877500

   4   C  2     x     -3.3070204300
   5            y      0.0000000000
   6            z      1.1191877500

   7   O  1     x      3.3070204300
   8            y      0.0000000000
   9            z     -1.1250939700

  10   O  2     x     -3.3070204300
  11            y      0.0000000000
  12            z     -1.1250939700

  13   H  1     x      3.3070204300
  14            y      1.7478091600
  15            z      2.2123537900

  16   H  2     x     -3.3070204300
  17            y      1.7478091600
  18            z      2.2123537900

  19   H  3     x      3.3070204300
  20            y     -1.7478091600
  21            z      2.2123537900

  22   H  4     x     -3.3070204300
  23            y     -1.7478091600
  24            z      2.2123537900



   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        C  2        O  1        O  2        H  1        H  2

   C  1    0.000000
   C  2    3.499999    0.000000
   O  1    1.187622    3.696003    0.000000
   O  2    3.696003    1.187622    3.499999    0.000000
   H  1    1.090907    3.666070    1.993628    4.027970    0.000000
   H  2    3.666070    1.090907    4.027970    1.993628    3.499999    0.000000
   H  3    1.090907    3.666070    1.993628    4.027970    1.849801    3.958757
   H  4    3.666070    1.090907    4.027970    1.993628    3.958757    1.849801

            H  3        H  4

   H  3    0.000000
   H  4    3.499999    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    O  1       C  1                           1.187622
  bond distance:    O  2       C  2                           1.187622
  bond distance:    H  1       C  1                           1.090907
  bond distance:    H  2       C  2                           1.090907
  bond distance:    H  3       C  1                           1.090907
  bond distance:    H  4       C  2                           1.090907


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       O  1                 122.024
  bond angle:       H  3       C  1       O  1                 122.024
  bond angle:       H  3       C  1       H  1                 115.952
  bond angle:       H  2       C  2       O  2                 122.024
  bond angle:       H  4       C  2       O  2                 122.024
  bond angle:       H  4       C  2       H  2                 115.952


  Nuclear repulsion energy :  100.585552273588


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1   1s    1     3047.524900    0.0018  0.0000  0.0000
   seg. cont.  2      457.369510    0.0140  0.0000  0.0000
               3      103.948690    0.0688  0.0000  0.0000
               4       29.210155    0.2322  0.0000  0.0000
               5        9.286663    0.4679  0.0000  0.0000
               6        3.163927    0.3623  0.0000  0.0000
               7        7.868272    0.0000 -0.1193  0.0000
               8        1.881288    0.0000 -0.1609  0.0000
               9        0.544249    0.0000  1.1435  0.0000
              10        0.168714    0.0000  0.0000  1.0000

  C  1   2px  11        7.868272    0.0690  0.0000
   seg. cont. 12        1.881288    0.3164  0.0000
              13        0.544249    0.7443  0.0000
              14        0.168714    0.0000  1.0000

  C  1   2py  15        7.868272    0.0690  0.0000
   seg. cont. 16        1.881288    0.3164  0.0000
              17        0.544249    0.7443  0.0000
              18        0.168714    0.0000  1.0000

  C  1   2pz  19        7.868272    0.0690  0.0000
   seg. cont. 20        1.881288    0.3164  0.0000
              21        0.544249    0.7443  0.0000
              22        0.168714    0.0000  1.0000

  C  2   1s   23     3047.524900    0.0018  0.0000  0.0000
   seg. cont. 24      457.369510    0.0140  0.0000  0.0000
              25      103.948690    0.0688  0.0000  0.0000
              26       29.210155    0.2322  0.0000  0.0000
              27        9.286663    0.4679  0.0000  0.0000
              28        3.163927    0.3623  0.0000  0.0000
              29        7.868272    0.0000 -0.1193  0.0000
              30        1.881288    0.0000 -0.1609  0.0000
              31        0.544249    0.0000  1.1435  0.0000
              32        0.168714    0.0000  0.0000  1.0000

  C  2   2px  33        7.868272    0.0690  0.0000
   seg. cont. 34        1.881288    0.3164  0.0000
              35        0.544249    0.7443  0.0000
              36        0.168714    0.0000  1.0000

  C  2   2py  37        7.868272    0.0690  0.0000
   seg. cont. 38        1.881288    0.3164  0.0000
              39        0.544249    0.7443  0.0000
              40        0.168714    0.0000  1.0000

  C  2   2pz  41        7.868272    0.0690  0.0000
   seg. cont. 42        1.881288    0.3164  0.0000
              43        0.544249    0.7443  0.0000
              44        0.168714    0.0000  1.0000

  O  1   1s   45     5484.671700    0.0018  0.0000  0.0000
   seg. cont. 46      825.234950    0.0140  0.0000  0.0000
              47      188.046960    0.0684  0.0000  0.0000
              48       52.964500    0.2327  0.0000  0.0000
              49       16.897570    0.4702  0.0000  0.0000
              50        5.799635    0.3585  0.0000  0.0000
              51       15.539616    0.0000 -0.1108  0.0000
              52        3.599934    0.0000 -0.1480  0.0000
              53        1.013762    0.0000  1.1308  0.0000
              54        0.270006    0.0000  0.0000  1.0000

  O  1   2px  55       15.539616    0.0709  0.0000
   seg. cont. 56        3.599934    0.3398  0.0000
              57        1.013762    0.7272  0.0000
              58        0.270006    0.0000  1.0000

  O  1   2py  59       15.539616    0.0709  0.0000
   seg. cont. 60        3.599934    0.3398  0.0000
              61        1.013762    0.7272  0.0000
              62        0.270006    0.0000  1.0000

  O  1   2pz  63       15.539616    0.0709  0.0000
   seg. cont. 64        3.599934    0.3398  0.0000
              65        1.013762    0.7272  0.0000
              66        0.270006    0.0000  1.0000

  O  2   1s   67     5484.671700    0.0018  0.0000  0.0000
   seg. cont. 68      825.234950    0.0140  0.0000  0.0000
              69      188.046960    0.0684  0.0000  0.0000
              70       52.964500    0.2327  0.0000  0.0000
              71       16.897570    0.4702  0.0000  0.0000
              72        5.799635    0.3585  0.0000  0.0000
              73       15.539616    0.0000 -0.1108  0.0000
              74        3.599934    0.0000 -0.1480  0.0000
              75        1.013762    0.0000  1.1308  0.0000
              76        0.270006    0.0000  0.0000  1.0000

  O  2   2px  77       15.539616    0.0709  0.0000
   seg. cont. 78        3.599934    0.3398  0.0000
              79        1.013762    0.7272  0.0000
              80        0.270006    0.0000  1.0000

  O  2   2py  81       15.539616    0.0709  0.0000
   seg. cont. 82        3.599934    0.3398  0.0000
              83        1.013762    0.7272  0.0000
              84        0.270006    0.0000  1.0000

  O  2   2pz  85       15.539616    0.0709  0.0000
   seg. cont. 86        3.599934    0.3398  0.0000
              87        1.013762    0.7272  0.0000
              88        0.270006    0.0000  1.0000

  H  1   1s   89       18.731137    0.0335  0.0000
   seg. cont. 90        2.825394    0.2347  0.0000
              91        0.640122    0.8138  0.0000
              92        0.161278    0.0000  1.0000

  H  2   1s   93       18.731137    0.0335  0.0000
   seg. cont. 94        2.825394    0.2347  0.0000
              95        0.640122    0.8138  0.0000
              96        0.161278    0.0000  1.0000

  H  3   1s   97       18.731137    0.0335  0.0000
   seg. cont. 98        2.825394    0.2347  0.0000
              99        0.640122    0.8138  0.0000
             100        0.161278    0.0000  1.0000

  H  4   1s  101       18.731137    0.0335  0.0000
   seg. cont.102        2.825394    0.2347  0.0000
             103        0.640122    0.8138  0.0000
             104        0.161278    0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  C  1    1s       1     2     3     4     5     6
   2  C  1    1s       7     8     9
   3  C  1    1s      10
   4  C  1    2px     11    12    13
   5  C  1    2py     15    16    17
   6  C  1    2pz     19    20    21
   7  C  1    2px     14
   8  C  1    2py     18
   9  C  1    2pz     22
  10  C  2    1s      23    24    25    26    27    28
  11  C  2    1s      29    30    31
  12  C  2    1s      32
  13  C  2    2px     33    34    35
  14  C  2    2py     37    38    39
  15  C  2    2pz     41    42    43
  16  C  2    2px     36
  17  C  2    2py     40
  18  C  2    2pz     44
  19  O  1    1s      45    46    47    48    49    50
  20  O  1    1s      51    52    53
  21  O  1    1s      54
  22  O  1    2px     55    56    57
  23  O  1    2py     59    60    61
  24  O  1    2pz     63    64    65
  25  O  1    2px     58
  26  O  1    2py     62
  27  O  1    2pz     66
  28  O  2    1s      67    68    69    70    71    72
  29  O  2    1s      73    74    75
  30  O  2    1s      76
  31  O  2    2px     77    78    79
  32  O  2    2py     81    82    83
  33  O  2    2pz     85    86    87
  34  O  2    2px     80
  35  O  2    2py     84
  36  O  2    2pz     88
  37  H  1    1s      89    90    91
  38  H  1    1s      92
  39  H  2    1s      93    94    95
  40  H  2    1s      96
  41  H  3    1s      97    98    99
  42  H  3    1s     100
  43  H  4    1s     101   102   103
  44  H  4    1s     104




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        44


  Symmetry  A  ( 1)

    1     C  1     1s         1
    2     C  1     1s         2
    3     C  1     1s         3
    4     C  1     2px        4
    5     C  1     2py        5
    6     C  1     2pz        6
    7     C  1     2px        7
    8     C  1     2py        8
    9     C  1     2pz        9
   10     C  2     1s        10
   11     C  2     1s        11
   12     C  2     1s        12
   13     C  2     2px       13
   14     C  2     2py       14
   15     C  2     2pz       15
   16     C  2     2px       16
   17     C  2     2py       17
   18     C  2     2pz       18
   19     O  1     1s        19
   20     O  1     1s        20
   21     O  1     1s        21
   22     O  1     2px       22
   23     O  1     2py       23
   24     O  1     2pz       24
   25     O  1     2px       25
   26     O  1     2py       26
   27     O  1     2pz       27
   28     O  2     1s        28
   29     O  2     1s        29
   30     O  2     1s        30
   31     O  2     2px       31
   32     O  2     2py       32
   33     O  2     2pz       33
   34     O  2     2px       34
   35     O  2     2py       35
   36     O  2     2pz       36
   37     H  1     1s        37
   38     H  1     1s        38
   39     H  2     1s        39
   40     H  2     1s        40
   41     H  3     1s        41
   42     H  3     1s        42
   43     H  4     1s        43
   44     H  4     1s        44

  Symmetries of electric field:  A  (1)  A  (1)  A  (1)

  Symmetries of magnetic field:  A  (1)  A  (1)  A  (1)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   3    0           0.10D-14                                                   
       6.0    2    2    3    2                                                  
C  1   3.307020430000000   0.000000000000000   1.119187750000000       *        
C  2  -3.307020430000000   0.000000000000000   1.119187750000000       *        
H   6   1                                                                       
       3047.52490000         0.00183470                                         
        457.36951000         0.01403730                                         
        103.94869000         0.06884260                                         
         29.21015500         0.23218440                                         
          9.28666300         0.46794130                                         
          3.16392700         0.36231200                                         
H   3   1                                                                       
          7.86827240        -0.11933240                                         
          1.88128850        -0.16085420                                         
          0.54424930         1.14345640                                         
H   1   1                                                                       
          0.16871440         1.00000000                                         
H   3   1                                                                       
          7.86827240         0.06899910                                         
          1.88128850         0.31642400                                         
          0.54424930         0.74430830                                         
H   1   1                                                                       
          0.16871440         1.00000000                                         
       8.0    2    2    3    2                                                  
O  1   3.307020430000000   0.000000000000000  -1.125093970000000       *        
O  2  -3.307020430000000   0.000000000000000  -1.125093970000000       *        
H   6   1                                                                       
       5484.67170000         0.00183110                                         
        825.23495000         0.01395010                                         
        188.04696000         0.06844510                                         
         52.96450000         0.23271430                                         
         16.89757000         0.47019300                                         
          5.79963530         0.35852090                                         
H   3   1                                                                       
         15.53961600        -0.11077750                                         
          3.59993360        -0.14802630                                         
          1.01376180         1.13076700                                         
H   1   1                                                                       
          0.27000580         1.00000000                                         
H   3   1                                                                       
         15.53961600         0.07087430                                         
          3.59993360         0.33975280                                         
          1.01376180         0.72715860                                         
H   1   1                                                                       
          0.27000580         1.00000000                                         
       1.0    4    1    2                                                       
H  1   3.307020430000000   1.747809160000000   2.212353790000000       *        
H  2  -3.307020430000000   1.747809160000000   2.212353790000000       *        
H  3   3.307020430000000  -1.747809160000000   2.212353790000000       *        
H  4  -3.307020430000000  -1.747809160000000   2.212353790000000       *        
H   3   1                                                                       
         18.73113700         0.03349460                                         
          2.82539370         0.23472695                                         
          0.64012170         0.81375733                                         
H   1   1                                                                       
          0.16127780         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

found     572 non-vanashing overlap integrals
found     754 non-vanashing nuclear attraction integrals
found     584 non-vanashing kinetic energy integrals






 found     648 non-vanashing integrals ( typea=  1 typeb=  0)
 found     436 non-vanashing integrals ( typea=  1 typeb=  1)
 found     634 non-vanashing integrals ( typea=  1 typeb=  2)


 found     674 non-vanashing integrals ( typea=  1 typeb=  3)
 found     492 non-vanashing integrals ( typea=  1 typeb=  4)
 found     716 non-vanashing integrals ( typea=  1 typeb=  5)
 found     572 non-vanashing integrals ( typea=  1 typeb=  6)
 found     468 non-vanashing integrals ( typea=  1 typeb=  7)
 found     634 non-vanashing integrals ( typea=  1 typeb=  8)


 found     656 non-vanashing integrals ( typea=  1 typeb=  9)
 found     500 non-vanashing integrals ( typea=  1 typeb= 10)
 found     750 non-vanashing integrals ( typea=  1 typeb= 11)
 found     644 non-vanashing integrals ( typea=  1 typeb= 12)
 found     516 non-vanashing integrals ( typea=  1 typeb= 13)
 found     716 non-vanashing integrals ( typea=  1 typeb= 14)
 found     436 non-vanashing integrals ( typea=  1 typeb= 15)
 found     634 non-vanashing integrals ( typea=  1 typeb= 16)
 found     468 non-vanashing integrals ( typea=  1 typeb= 17)
 found     644 non-vanashing integrals ( typea=  1 typeb= 18)


 found     688 non-vanashing integrals ( typea=  1 typeb= 19)
 found     498 non-vanashing integrals ( typea=  1 typeb= 20)
 found     722 non-vanashing integrals ( typea=  1 typeb= 21)
 found     668 non-vanashing integrals ( typea=  1 typeb= 22)
 found     532 non-vanashing integrals ( typea=  1 typeb= 23)
 found     750 non-vanashing integrals ( typea=  1 typeb= 24)
 found     492 non-vanashing integrals ( typea=  1 typeb= 25)
 found     716 non-vanashing integrals ( typea=  1 typeb= 26)
 found     516 non-vanashing integrals ( typea=  1 typeb= 27)
 found     716 non-vanashing integrals ( typea=  1 typeb= 28)
 found     576 non-vanashing integrals ( typea=  1 typeb= 29)
 found     468 non-vanashing integrals ( typea=  1 typeb= 30)
 found     634 non-vanashing integrals ( typea=  1 typeb= 31)
 found     468 non-vanashing integrals ( typea=  1 typeb= 32)
 found     652 non-vanashing integrals ( typea=  1 typeb= 33)


 found     460 non-vanashing integrals ( typea=  2 typeb=  6)
 found     676 non-vanashing integrals ( typea=  2 typeb=  7)
 found     494 non-vanashing integrals ( typea=  2 typeb=  8)




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:    335881 (68.5%)
 Kilobytes written:                             5418




 >>>> Total CPU  time used in HERMIT:   0.28 seconds
 >>>> Total wall time used in HERMIT:   1.00 seconds

- End of Integral Section
