1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       665       49140           0           0       49805
      internal walks       665        1890           0           0        2555
valid internal walks       665        1890           0           0        2555
  MR-CIS calculation - skip 3- and 4-external 2e integrals
 lcore1,lcore2=             222783937             222761411
 lencor,maxblo             222822400                 60000
========================================
 current settings:
 minbl3        1050
 minbl4        1050
 locmaxbl3    18204
 locmaxbuf     9102
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================
 Orig.  diagonal integrals:  1electron:        44
                             0ext.    :       342
                             2ext.    :       936
                             4ext.    :       702


 Orig. off-diag. integrals:  4ext.    :     70200
                             3ext.    :    175500
                             2ext.    :    180063
                             1ext.    :     88920
                             0ext.    :     17442
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       351


 Sorted integrals            3ext.  w :         0 x :         0
                             4ext.  w :         0 x :         0


Cycle #  1 sortfile size=    884709(      27 records of    32767) #buckets=   2
Cycle #  2 sortfile size=    884709(      27 records of    32767) #buckets=   2

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 221529477
 minimum size of srtscr:    819175 WP (    25 records)
 maximum size of srtscr:    884709 WP (    27 records)
 compressed index vector length=                     3
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 24
  NROOT = 3
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 3
  RTOLBK = 1e-4,1e-4,1e-4,
  NITER = 90
  NVCIMN = 5
  RTOLCI = 1e-5,1e-5,1e-5,
  NVCIMX = 8
  NVRFMX = 8
  NVBKMX = 8
   iden=2
  CSFPRN = 10,
 /&end
 transition
   1  1  1  2
   1  1  1  3
   1  2  1  3
 ------------------------------------------------------------------------
transition densities: resetting nroot to    3
lodens (list->root)=  1  2  3
invlodens (root->list)=  1  2  3
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    3      noldv  =   0      noldhv =   0
 nunitv =    3      nbkitr =    1      niter  =  90      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    8      ibktv  =  -1      ibkthv =  -1
 nvcimx =    8      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    3      nvcimn =    5      maxseg =   4      nrfitr =  30
 ncorel =   24      nvrfmx =    8      nvrfmn =   5      iden   =   2
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-05
    2        1.000E-04    1.000E-05
    3        1.000E-04    1.000E-05
 Computing density:                    .drt1.state1
 Computing density:                    .drt1.state2
 Computing density:                    .drt1.state3
 Computing transition density:          drt1.state1-> drt1.state2 (.trd1to2)
 Computing transition density:          drt1.state1-> drt1.state3 (.trd1to3)
 Computing transition density:          drt1.state2-> drt1.state3 (.trd2to3)
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  3830                288782
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          222822399 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 222822399

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  hawk6.itc.univie. 15:40:44.981 05-Nov-11
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  total ao core energy =  100.585552274                                          
 SIFS file created by program tran.      hawk6.itc.univie. 15:40:47.172 05-Nov-11

 input energy(*) values:
 energy( 1)=  1.005855522736E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   1.005855522736E+02

 nsym = 1 nmot=  44

 symmetry  =    1
 slabel(*) =  A  
 nmpsy(*)  =   44

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=     766
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  44 nfctd =   0 nfvtc =   0 nmot  =  44
 nlevel =  44 niot  =  18 lowinl=  27
 orbital-to-level map(*)
   27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44   1   2
    3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22
   23  24  25  26
 compressed map(*)
   27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44   1   2
    3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22
   23  24  25  26
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1
 repartitioning mu(*)=
   2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  0.  0.  0.  0.  0.  0.
  ... transition density matrix calculation requested 
  ... setting rmu(*) to zero 

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1980
 number with all external indices:       702
 number with half external - half internal indices:       936
 number with all internal indices:       342

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      70200 strt=          1
    3-external integrals: num=     175500 strt=      70201
    2-external integrals: num=     180063 strt=     245701
    1-external integrals: num=      88920 strt=     425764
    0-external integrals: num=      17442 strt=     514684

 total number of off-diagonal integrals:      532125


 indxof(2nd)  ittp=   3 numx(ittp)=      180063
 indxof(2nd)  ittp=   4 numx(ittp)=       88920
 indxof(2nd)  ittp=   5 numx(ittp)=       17442

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 222646785
 pro2e        1     991    1981    2971    3961    4132    4303    5293   48981   92669
   125436  133628  139088  160927

 pro2e:    344076 integrals read in    64 records.
 pro1e        1     991    1981    2971    3961    4132    4303    5293   48981   92669
   125436  133628  139088  160927
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                    20  records of                  32767 
 WP =               5242720 Bytes
 putdg        1     991    1981    2971    3737   36504   58349    5293   48981   92669
   125436  133628  139088  160927

 putf:       5 buffers of length     766 written to file 12
 diagonal integral file completed.

 putf:     377 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:     702 2ext:     936 0ext:     342
 fil4w,fil4x  :   70200 fil3w,fil3x :  175500
 ofdgint  2ext:  180063 1ext:   88920 0ext:   17442so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1050 minbl3    1050 maxbl2    1053nbas:  26   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 222822399

 core energy values from the integral file:
 energy( 1)=  1.005855522736E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  1.005855522736E+02
 nmot  =    44 niot  =    18 nfct  =     0 nfvt  =     0
 nrow  =   135 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       2555      665     1890        0        0
 nvalwt,nvalw:     2555      665     1890        0        0
 ncsft:           49805
 total number of valid internal walks:    2555
 nvalz,nvaly,nvalx,nvalw =      665    1890       0       0

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   1532
 nd4ext,nd2ext,nd0ext   702   936   342
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    70200   175500   180063    88920    17442        0        0        0
 minbl4,minbl3,maxbl2  1050  1050  1053
 maxbuf 30006
 number of external orbitals per symmetry block:  26
 nmsym   1 number of internal orbitals  18
 bummer (warning):transition densities: resetting ref occupation number to 0                      0
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    18                     1
 block size     0
 pthz,pthy,pthx,pthw:   665  1890     0     0 total internal walks:    2555
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1

 setref:      105 references kept,
                0 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext   236
 nmb.of records 1-ext   117
 nmb.of records 0-ext    23
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61805
    threx             60019
    twoex              8007
    onex               2223
    allin               766
    diagon             2113
               =======
   maximum            61805
 
  __ static summary __ 
   reflst               665
   hrfspc               665
               -------
   static->             665
 
  __ core required  __ 
   totstc               665
   max n-ex           61805
               -------
   totnec->           62470
 
  __ core available __ 
   totspc         222822399
   totnec -           62470
               -------
   totvec->       222759929

 number of external paths / symmetry
 vertex x     325
 vertex w     351
segment: free space=   222759929
 reducing frespc by                 12565 to              222747364 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         665|       665|         0|       665|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1890|     49140|       665|      1890|       665|         2|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=        7560DP  drtbuffer=        5001 DP

dimension of the ci-matrix ->>>     49805

 executing brd_struct for civct
 gentasklist: ntask=                     6
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1    1890     665      49140        665    1890     665
     2  1   1     1      allint zz    OX  1 1     665     665        665        665     665     665
     3  2   2     5      0ex2ex yy    OX  2 2    1890    1890      49140      49140    1890    1890
     4  2   2    42      four-ext y   4X  2 2    1890    1890      49140      49140    1890    1890
     5  1   1    75      dg-024ext z  OX  1 1     665     665        665        665     665     665
     6  2   2    76      dg-024ext y  OX  2 2    1890    1890      49140      49140    1890    1890
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=   5.000 N=  1 (task/type/sgbra)=(   1/11/0) (
REDTASK #   2 TIME=   4.000 N=  1 (task/type/sgbra)=(   2/ 1/0) (
REDTASK #   3 TIME=   3.000 N=  1 (task/type/sgbra)=(   3/ 5/0) (
REDTASK #   4 TIME=   2.000 N=  1 (task/type/sgbra)=(   4/42/1) (
REDTASK #   5 TIME=   1.000 N=  1 (task/type/sgbra)=(   5/75/1) (
REDTASK #   6 TIME=   0.000 N=  1 (task/type/sgbra)=(   6/76/1) (
 initializing v-file: 1:                 49805

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      3 vectors will be written to unit 11 beginning with logical record   1

            3 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:           0    task #
 reference space has dimension     105
 dsyevx: computed roots 1 to    6(converged:   6)

    root           eigenvalues
    ----           ------------
       1        -227.6831282554
       2        -227.5324465907
       3        -227.5320207590
       4        -227.4089739885
       5        -227.3797381305
       6        -227.3397142652

 strefv generated    3 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   665

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   665

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   665

         vector  3 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   665)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             49805
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    1.994913
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    2.736610
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    2.738462
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:           0    task #     4:           0
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1  -328.26868053
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
Spectrum of overlapmatrix:    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000        0.00000       2.406222E-18
 refs   2    0.00000        1.00000       1.052222E-17
 refs   3    0.00000        0.00000        1.00000    

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   -1.00000       2.920268E-14  -2.406222E-18
 civs   2   2.921274E-14    1.00000      -1.052220E-17
 civs   3    0.00000        0.00000        1.00000    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   -1.00000       2.920268E-14   9.834887E-26
 ref    2   2.921274E-14    1.00000       1.309137E-23
 ref    3    0.00000        0.00000        1.00000    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -1.00000000     0.00000000     0.00000000
 ref:   2     0.00000000     1.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -227.6831282554  1.1369E-13  9.0965E-02  4.3527E-01  1.0000E-04
 mr-sdci #  1  2   -227.5324465907  2.8422E-13  0.0000E+00  4.0708E-01  1.0000E-04
 mr-sdci #  1  3   -227.5320207590 -2.2737E-13  0.0000E+00  4.0682E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.00     0.00     0.00     0.00         0.    0.0000
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0000
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3600s 
time spent in multnx:                   0.3600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1500s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -227.6831282554  1.1369E-13  9.0965E-02  4.3527E-01  1.0000E-04
 mr-sdci #  1  2   -227.5324465907  2.8422E-13  0.0000E+00  4.0708E-01  1.0000E-04
 mr-sdci #  1  3   -227.5320207590 -2.2737E-13  0.0000E+00  4.0682E-01  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    4 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             49805
 number of initial trial vectors:                         4
 number of initial matrix-vector products:                3
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                   90
 residual norm convergence criteria:               0.000010  0.000010  0.000010

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.040076
ci vector #   2dasum_wr=    5.516989
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     0.04732314
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1  -328.26868053
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.09096479     0.00000000     0.00000000   -15.43720133
Spectrum of overlapmatrix:    0.047323    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000        0.00000       2.406222E-18   1.886918E-14
 refs   2    0.00000        1.00000       1.052222E-17  -1.120700E-14
 refs   3    0.00000        0.00000        1.00000      -6.618473E-16

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.981473       1.077678E-10  -7.130463E-11   0.191599    
 civs   2  -9.832102E-11   -1.00000       1.171798E-10   5.881206E-11
 civs   3  -6.503428E-11   1.171799E-10    1.00000       3.901536E-11
 civs   4   0.880759       1.785093E-10  -1.187327E-10    4.51172    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.981473       1.077678E-10  -7.130463E-11   0.191599    
 ref    2  -9.833089E-11   -1.00000       1.171798E-10   5.876150E-11
 ref    3  -6.503487E-11   1.171799E-10    1.00000       3.901238E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.98147323     0.00000000     0.00000000     0.19159929
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -227.7647586647  8.1630E-02  1.1282E-02  1.0885E-01  1.0000E-05
 mr-sdci #  1  2   -227.5324465907 -3.9790E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  1  3   -227.5320207590  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  1  4   -225.5411202588  3.2613E+02  0.0000E+00  8.0773E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.43     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.01     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.348379
ci vector #   2dasum_wr=    1.205241
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     0.04732314
 sovl   5    -0.12057940     0.00000000     0.00000000    -0.00009861     0.02857553
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1  -328.26868053
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.09096479     0.00000000     0.00000000   -15.43720133
   ht   5    39.58244989     0.00000001     0.00000001     0.00860980    -9.36615469
Spectrum of overlapmatrix:    0.013832    0.047323    1.000000    1.000000    1.014743

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000        0.00000       2.406222E-18   1.886918E-14  -0.120579    
 refs   2    0.00000        1.00000       1.052222E-17  -1.120700E-14  -3.423399E-11
 refs   3    0.00000        0.00000        1.00000      -6.618473E-16  -2.647522E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.876803       1.354084E-10  -7.178382E-11    1.11023       0.185656    
 civs   2  -7.517631E-11   -1.00000       7.439822E-11   3.193854E-10   4.846411E-11
 civs   3  -4.288324E-11   7.439823E-11    1.00000       2.286919E-10   4.160766E-11
 civs   4   0.969793       1.924981E-10  -1.190432E-10    1.49086       -4.23892    
 civs   5   0.797723       1.591252E-10  -3.015494E-12    7.87971        2.91883    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.972992       1.162211E-10  -7.142021E-11   0.160103      -0.166295    
 ref    2  -1.024964E-10   -1.00000       7.439823E-11   4.961487E-11  -5.141158E-11
 ref    3  -6.400378E-11   7.439823E-11    1.00000       2.007390E-11  -3.566621E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.97299185     0.00000000     0.00000000     0.16010290    -0.16629469
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -227.7738006131  9.0419E-03  1.1676E-03  4.7599E-02  1.0000E-05
 mr-sdci #  2  2   -227.5324465907 -5.6843E-14  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  2  3   -227.5320207590 -1.7053E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  2  4   -226.8356794081  1.2946E+00  0.0000E+00  7.1323E-01  1.0000E-04
 mr-sdci #  2  5   -225.3645383585  3.2595E+02  0.0000E+00  7.1636E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0900s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.078463
ci vector #   2dasum_wr=    0.573515
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     0.04732314
 sovl   5    -0.12057940     0.00000000     0.00000000    -0.00009861     0.02857553
 sovl   6    -0.00176211     0.00000000     0.00000000     0.00042572     0.00002993     0.00071419
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.26868053
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.09096479     0.00000000     0.00000000   -15.43720133
   ht   5    39.58244989     0.00000001     0.00000001     0.00860980    -9.36615469
   ht   6     0.57840848    -0.00000001     0.00000000    -0.13998871    -0.01126601    -0.23293270
Spectrum of overlapmatrix:    0.000705    0.013834    0.047327    1.000000    1.000000    1.014746

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000        0.00000       2.406222E-18   1.886918E-14  -0.120579      -1.762115E-03
 refs   2    0.00000        1.00000       1.052222E-17  -1.120700E-14  -3.423399E-11   1.833500E-11
 refs   3    0.00000        0.00000        1.00000      -6.618473E-16  -2.647522E-11   1.272408E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.863481       1.917172E-10  -1.011015E-10   -1.11167      -0.246986      -9.549341E-02
 civs   2  -9.614869E-11   -1.00000       2.958186E-10  -1.682483E-10  -6.350605E-10   2.663444E-10
 civs   3  -5.601273E-11   2.958187E-10    1.00000      -1.145259E-10  -4.528886E-10   1.817556E-10
 civs   4   0.978448       2.210552E-10  -1.334619E-10   -1.36345        1.22389        4.11479    
 civs   5   0.880800       4.545462E-10  -1.549840E-10   -7.65412       -2.93204       -1.87752    
 civs   6   0.816140       2.278828E-09  -1.175071E-09   -10.6206        32.1989       -16.3826    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.971125       1.328928E-10  -8.034302E-11  -0.170026       4.982005E-02   0.159765    
 ref    2  -1.113490E-10   -1.00000       2.958187E-10  -1.009314E-10   5.566850E-11   3.019767E-11
 ref    3  -6.894812E-11   2.958187E-10    1.00000      -4.701839E-11   3.443862E-11   2.300664E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97112522     0.00000000     0.00000000    -0.17002641     0.04982005     0.15976479
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -227.7747538187  9.5321E-04  1.4190E-04  1.6429E-02  1.0000E-05
 mr-sdci #  3  2   -227.5324465907 -1.1369E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  3  3   -227.5320207590  1.7053E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  3  4   -226.9557237104  1.2004E-01  0.0000E+00  4.9837E-01  1.0000E-04
 mr-sdci #  3  5   -225.4738890716  1.0935E-01  0.0000E+00  1.1664E+00  1.0000E-04
 mr-sdci #  3  6   -225.3364414352  3.2592E+02  0.0000E+00  6.9643E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.46     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.01     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.027704
ci vector #   2dasum_wr=    0.242921
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     0.04732314
 sovl   5    -0.12057940     0.00000000     0.00000000    -0.00009861     0.02857553
 sovl   6    -0.00176211     0.00000000     0.00000000     0.00042572     0.00002993     0.00071419
 sovl   7     0.00039333     0.00000000     0.00000000     0.00011732     0.00013974    -0.00001917     0.00009086
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.26868053
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.09096479     0.00000000     0.00000000   -15.43720133
   ht   5    39.58244989     0.00000001     0.00000001     0.00860980    -9.36615469
   ht   6     0.57840848    -0.00000001     0.00000000    -0.13998871    -0.01126601    -0.23293270
   ht   7    -0.12912864     0.00000000     0.00000000    -0.03849523    -0.04589017     0.00612015    -0.02964329
Spectrum of overlapmatrix:    0.000087    0.000705    0.013837    0.047328    1.000000    1.000000    1.014746

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000        0.00000       2.406222E-18   1.886918E-14  -0.120579      -1.762115E-03   3.933299E-04
 refs   2    0.00000        1.00000       1.052222E-17  -1.120700E-14  -3.423399E-11   1.833500E-11   7.906536E-12
 refs   3    0.00000        0.00000        1.00000      -6.618473E-16  -2.647522E-11   1.272408E-11   5.900378E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.861891       1.562459E-10   7.033561E-11    1.02237       0.511909       0.144057       0.151236    
 civs   2   9.951617E-11   -1.00000       1.313103E-10  -1.653931E-10   1.225523E-09  -4.931042E-11   1.480822E-10
 civs   3   5.771140E-11  -1.313103E-10   -1.00000      -1.396422E-10   9.009964E-10  -3.410504E-11   1.332344E-10
 civs   4  -0.979319       1.991903E-10   1.148758E-10    1.20582       0.764897       -4.17550      -0.947455    
 civs   5  -0.890934       2.520674E-10  -1.875994E-11    6.97691        4.00678        2.65505       0.922783    
 civs   6  -0.916512       1.025333E-09   9.764598E-11    13.0701       -23.3455       -3.05946        26.4445    
 civs   7  -0.826978      -8.697749E-09  -7.475966E-09    25.3980       -76.7738        16.3469       -67.9803    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970609       1.206239E-10   6.948509E-11   0.168061       3.971386E-02  -0.164266      -3.336941E-02
 ref    2   1.066846E-10   -1.00000       1.313103E-10   3.619755E-11   5.329135E-11  -6.700431E-11   6.387407E-11
 ref    3   6.475847E-11  -1.313103E-10   -1.00000      -8.194675E-12   4.487104E-11  -4.687115E-11   4.417701E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97060878     0.00000000     0.00000000     0.16806144     0.03971386    -0.16426578    -0.03336941
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -227.7748711749  1.1736E-04  2.8707E-05  7.3773E-03  1.0000E-05
 mr-sdci #  4  2   -227.5324465907  3.4106E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  4  3   -227.5320207590 -5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  4  4   -227.0347593914  7.9036E-02  0.0000E+00  4.5926E-01  1.0000E-04
 mr-sdci #  4  5   -225.8868453180  4.1296E-01  0.0000E+00  1.1755E+00  1.0000E-04
 mr-sdci #  4  6   -225.3706602948  3.4219E-02  0.0000E+00  7.7056E-01  1.0000E-04
 mr-sdci #  4  7   -225.0919035697  3.2568E+02  0.0000E+00  1.1976E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.46     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.060000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.8100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0900s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.015720
ci vector #   2dasum_wr=    0.111814
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     0.04732314
 sovl   5    -0.12057940     0.00000000     0.00000000    -0.00009861     0.02857553
 sovl   6    -0.00176211     0.00000000     0.00000000     0.00042572     0.00002993     0.00071419
 sovl   7     0.00039333     0.00000000     0.00000000     0.00011732     0.00013974    -0.00001917     0.00009086
 sovl   8     0.00012592     0.00000000     0.00000000    -0.00002304    -0.00004679    -0.00001347    -0.00000443     0.00001876
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.26868053
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.09096479     0.00000000     0.00000000   -15.43720133
   ht   5    39.58244989     0.00000001     0.00000001     0.00860980    -9.36615469
   ht   6     0.57840848    -0.00000001     0.00000000    -0.13998871    -0.01126601    -0.23293270
   ht   7    -0.12912864     0.00000000     0.00000000    -0.03849523    -0.04589017     0.00612015    -0.02964329
   ht   8    -0.04133348     0.00000000     0.00000000     0.00757871     0.01536416     0.00442281     0.00148896    -0.00612255
Spectrum of overlapmatrix:    0.000018    0.000088    0.000705    0.013837    0.047328    1.000000    1.000000    1.014747

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000        0.00000       2.406222E-18   1.886918E-14  -0.120579      -1.762115E-03   3.933299E-04   1.259200E-04
 refs   2    0.00000        1.00000       1.052222E-17  -1.120700E-14  -3.423399E-11   1.833500E-11   7.906536E-12  -4.340506E-12
 refs   3    0.00000        0.00000        1.00000      -6.618473E-16  -2.647522E-11   1.272408E-11   5.900378E-12  -2.843180E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.861314       4.720077E-11  -1.810267E-11   0.821822       0.772336      -0.212106      -3.503664E-02   0.185816    
 civs   2   9.854447E-11   -1.00000      -2.901419E-10  -1.019843E-09   1.568751E-09  -1.446234E-10   1.278625E-10  -6.113666E-11
 civs   3   5.573776E-11   2.901415E-10   -1.00000      -7.676542E-10   1.124601E-09  -1.138191E-10   9.451822E-11   1.724486E-11
 civs   4  -0.979462       1.364613E-10   6.447890E-11   0.904466        1.10695        2.95475        3.09686       9.288782E-02
 civs   5  -0.893299      -3.024656E-10  -4.660642E-10    5.45007        5.66177       -3.01947      -0.916622        1.04959    
 civs   6  -0.940097      -2.071883E-09  -2.401636E-09    13.3472       -3.37446        21.6643       -28.0047        1.98532    
 civs   7   -1.02150      -2.825513E-08  -2.325235E-08    41.8364       -52.6498       -5.57163        4.21000       -83.6105    
 civs   8   0.961708       7.779750E-08   6.273199E-08   -72.8190        133.320        48.5672       -64.3429       -159.377    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970404       8.600547E-11   4.108041E-11   0.148422       9.166837E-02   0.117729       0.118390       2.802880E-03
 ref    2   9.964915E-11   -1.00000      -2.901419E-10  -3.148574E-10   3.180893E-10   1.010677E-10  -4.169212E-11  -2.996181E-11
 ref    3   5.866531E-11   2.901415E-10   -1.00000      -2.882276E-10   2.420598E-10   7.081790E-11  -2.977148E-11  -2.547856E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97040379     0.00000000     0.00000000     0.14842218     0.09166837     0.11772888     0.11839047     0.00280288
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -227.7748987834  2.7608E-05  3.7403E-06  2.6836E-03  1.0000E-05
 mr-sdci #  5  2   -227.5324465907 -1.1369E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  5  3   -227.5320207590  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  5  4   -227.1360428459  1.0128E-01  0.0000E+00  4.1224E-01  1.0000E-04
 mr-sdci #  5  5   -226.5735063110  6.8666E-01  0.0000E+00  6.8406E-01  1.0000E-04
 mr-sdci #  5  6   -225.4060282697  3.5368E-02  0.0000E+00  1.0138E+00  1.0000E-04
 mr-sdci #  5  7   -225.2961336538  2.0423E-01  0.0000E+00  8.3801E-01  1.0000E-04
 mr-sdci #  5  8   -224.4924184256  3.2508E+02  0.0000E+00  1.0884E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.68     0.37     0.01     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1200s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.006878
ci vector #   2dasum_wr=    0.048988
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00026425     0.00000000     0.00000000    -0.00006874     0.00005029     0.00000247
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045106
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.72159512
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.15905858
   ht   6     0.08676498     0.00000000     0.00000000     0.02282825    -0.01706277    -0.00080509
Spectrum of overlapmatrix:    0.000002    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970404       8.600547E-11   4.108041E-11   0.148422       9.166837E-02  -2.714693E-04
 refs   2   9.964915E-11   -1.00000      -2.901419E-10  -3.148574E-10   3.180893E-10  -1.735642E-12
 refs   3   5.866531E-11   2.901415E-10   -1.00000      -2.882276E-10   2.420598E-10  -9.296731E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00025      -7.808014E-11   6.144073E-11   3.160449E-02  -6.520260E-02   0.153289    
 civs   2  -2.317635E-12   -1.00000      -6.317862E-10  -5.595065E-10   7.552786E-10  -1.335857E-09
 civs   3  -2.661219E-12  -6.317863E-10    1.00000      -3.899597E-10   4.891184E-10  -8.031657E-10
 civs   4   3.752656E-04   2.156062E-10  -1.710009E-10  -0.965544      -0.207919       0.162705    
 civs   5  -4.300062E-04  -1.842913E-10   1.460679E-10   0.118608      -0.889728      -0.442013    
 civs   6  -0.937610      -3.137998E-07   2.485965E-07    122.485       -250.179        584.093    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970372      -6.148067E-11   2.122589E-11  -0.135017      -0.107777      -2.618101E-02
 ref    2  -9.598366E-11    1.00000       3.416443E-10   6.918018E-10  -5.451027E-10   1.455263E-10
 ref    3  -5.535913E-11   3.416448E-10   -1.00000       5.849492E-10  -4.157979E-10   1.152527E-10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037219     0.00000000     0.00000000    -0.13501746    -0.10777659    -0.02618101
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -227.7749022903  3.5069E-06  1.3115E-06  1.5212E-03  1.0000E-05
 mr-sdci #  6  2   -227.5324465907  1.1369E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  6  3   -227.5320207590  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  6  4   -227.1737219174  3.7679E-02  0.0000E+00  3.9664E-01  1.0000E-04
 mr-sdci #  6  5   -226.7430255671  1.6952E-01  0.0000E+00  6.4348E-01  1.0000E-04
 mr-sdci #  6  6   -225.7080757448  3.0205E-01  0.0000E+00  1.0974E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.060000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0900s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.005337
ci vector #   2dasum_wr=    0.026070
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00026425     0.00000000     0.00000000    -0.00006874     0.00005029     0.00000247
 sovl   7    -0.00017233     0.00000000     0.00000000     0.00016151    -0.00011476     0.00000025     0.00000096
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045106
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.72159512
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.15905858
   ht   6     0.08676498     0.00000000     0.00000000     0.02282825    -0.01706277    -0.00080509
   ht   7     0.05658647     0.00000000     0.00000000    -0.05300450     0.03768708    -0.00008014    -0.00031407
Spectrum of overlapmatrix:    0.000001    0.000002    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970404       8.600547E-11   4.108041E-11   0.148422       9.166837E-02  -2.714693E-04  -1.723652E-04
 refs   2   9.964915E-11   -1.00000      -2.901419E-10  -3.148574E-10   3.180893E-10  -1.735642E-12   5.289502E-13
 refs   3   5.866531E-11   2.901415E-10   -1.00000      -2.882276E-10   2.420598E-10  -9.296731E-13  -1.361766E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00018      -3.193889E-11   2.464436E-11   1.803406E-02  -2.423158E-02  -1.767292E-02   0.229638    
 civs   2  -4.009047E-12   -1.00000      -2.882216E-10  -1.565339E-09   1.379211E-09   1.575344E-09   1.481801E-11
 civs   3  -4.759944E-12  -2.882217E-10    1.00000      -1.026124E-09   7.946192E-10   7.507713E-10  -3.080960E-10
 civs   4   4.531524E-04   4.380579E-10  -3.480818E-10  -0.846477      -0.485592      -0.270201      -9.428403E-02
 civs   5  -5.637254E-04  -3.813556E-10   3.027712E-10   0.213714      -0.708108       0.685398       2.264232E-02
 civs   6   -1.22501      -6.114195E-07   4.851159E-07    216.347       -295.570       -432.697        327.184    
 civs   7   0.819628       6.974712E-07  -5.540354E-07   -218.534        305.532        555.641        833.513    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970372      -4.117798E-11   4.889234E-12  -0.109609      -0.132923       2.726663E-02  -2.156552E-02
 ref    2  -9.342032E-11    1.00000      -1.920283E-12   1.410541E-09  -7.793610E-10  -2.290987E-10  -8.203318E-11
 ref    3  -5.315575E-11  -1.919816E-12   -1.00000       1.151519E-09  -5.943076E-10  -1.814197E-10  -6.345523E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037171     0.00000000     0.00000000    -0.10960878    -0.13292309     0.02726663    -0.02156552
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -227.7749033653  1.0750E-06  3.4062E-07  7.5618E-04  1.0000E-05
 mr-sdci #  7  2   -227.5324465907 -1.7053E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  7  3   -227.5320207590  5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  7  4   -227.2264269924  5.2705E-02  0.0000E+00  3.7725E-01  1.0000E-04
 mr-sdci #  7  5   -226.8686006040  1.2558E-01  0.0000E+00  5.5094E-01  1.0000E-04
 mr-sdci #  7  6   -226.0021893030  2.9411E-01  0.0000E+00  8.3032E-01  1.0000E-04
 mr-sdci #  7  7   -225.1099879470 -1.8615E-01  0.0000E+00  1.5752E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.01     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.35     0.00     0.66         0.    0.7752
    4   42    0     0.01     0.00     0.00     0.01         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.060000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.002652
ci vector #   2dasum_wr=    0.012488
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00026425     0.00000000     0.00000000    -0.00006874     0.00005029     0.00000247
 sovl   7    -0.00017233     0.00000000     0.00000000     0.00016151    -0.00011476     0.00000025     0.00000096
 sovl   8     0.00009521     0.00000000     0.00000000     0.00001876    -0.00005519    -0.00000005     0.00000004     0.00000026
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045106
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.72159512
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.15905858
   ht   6     0.08676498     0.00000000     0.00000000     0.02282825    -0.01706277    -0.00080509
   ht   7     0.05658647     0.00000000     0.00000000    -0.05300450     0.03768708    -0.00008014    -0.00031407
   ht   8    -0.03126236     0.00000000     0.00000000    -0.00616018     0.01811584     0.00001503    -0.00001315    -0.00008384
Spectrum of overlapmatrix:    0.000000    0.000001    0.000002    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970404       8.600547E-11   4.108041E-11   0.148422       9.166837E-02  -2.714693E-04  -1.723652E-04   1.013892E-04
 refs   2   9.964915E-11   -1.00000      -2.901419E-10  -3.148574E-10   3.180893E-10  -1.735642E-12   5.289502E-13   1.731110E-13
 refs   3   5.866531E-11   2.901415E-10   -1.00000      -2.882276E-10   2.420598E-10  -9.296731E-13  -1.361766E-13  -3.897323E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00022      -1.007653E-10   8.100456E-11   3.693919E-02  -4.022075E-02  -1.601701E-02  -9.450464E-02  -0.290073    
 civs   2  -4.527229E-12   -1.00000      -7.860997E-10  -2.282651E-09   1.569300E-09   1.240285E-09   7.633761E-10  -9.957562E-11
 civs   3  -4.412930E-12  -7.860998E-10    1.00000      -1.381070E-09   7.195294E-10   1.592228E-10   9.465983E-10  -5.360736E-10
 civs   4   4.694941E-04   5.575617E-10  -4.441387E-10  -0.745736      -0.640736      -0.235942      -6.912574E-02   8.635639E-02
 civs   5  -5.860569E-04  -4.812235E-10   3.829616E-10   0.246426      -0.554470       0.785369       0.204153       1.965495E-02
 civs   6   -1.27973      -7.522786E-07   5.981406E-07    248.614       -254.025       -159.672       -523.060       -60.0358    
 civs   7   0.975680       1.017039E-06  -8.103228E-07   -314.758        370.635        596.567       -222.972       -717.397    
 civs   8   0.600874       9.332135E-07  -7.479411E-07   -286.664        400.225        808.932       -853.162        1582.20    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970365      -2.161081E-11  -9.665536E-12  -9.455073E-02  -0.139303       4.396717E-02   1.067305E-02   3.350167E-02
 ref    2  -9.263727E-11    1.00000       4.959578E-10   1.951897E-09  -8.417109E-10  -1.850537E-10  -4.387708E-11   4.836285E-11
 ref    3  -5.372004E-11   4.959583E-10   -1.00000       1.581284E-09  -6.417183E-10  -1.501141E-10  -3.365750E-11   3.579370E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97036550     0.00000000     0.00000000    -0.09455073    -0.13930307     0.04396717     0.01067305     0.03350167
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -227.7749035700  2.0467E-07  7.9495E-08  3.7473E-04  1.0000E-05
 mr-sdci #  8  2   -227.5324465907  2.8422E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  8  3   -227.5320207590 -1.7053E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  8  4   -227.2602722584  3.3845E-02  0.0000E+00  3.0749E-01  1.0000E-04
 mr-sdci #  8  5   -226.9398561335  7.1256E-02  0.0000E+00  4.9832E-01  1.0000E-04
 mr-sdci #  8  6   -226.2270535590  2.2486E-01  0.0000E+00  8.2935E-01  1.0000E-04
 mr-sdci #  8  7   -225.6267138787  5.1673E-01  0.0000E+00  1.0343E+00  1.0000E-04
 mr-sdci #  8  8   -223.9292618513 -5.6316E-01  0.0000E+00  1.7390E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.17         0.    0.2041
    2    1    0     0.13     0.10     0.00     0.13         0.    0.1775
    3    5    0     0.68     0.48     0.01     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1200s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001042
ci vector #   2dasum_wr=    0.006107
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00004682     0.00000000     0.00000000     0.00002554     0.00002546     0.00000006
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045584
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.84582453
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.52540841
   ht   6     0.01537442     0.00000000     0.00000000    -0.00834556    -0.00840875    -0.00001882
Spectrum of overlapmatrix:    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970365      -2.161081E-11  -9.665536E-12  -9.455073E-02  -0.139303       4.525168E-05
 refs   2  -9.263727E-11    1.00000       4.959578E-10   1.951897E-09  -8.417109E-10   7.281375E-14
 refs   3  -5.372004E-11   4.959583E-10   -1.00000       1.581284E-09  -6.417183E-10   1.089103E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00004      -7.028091E-11   1.175270E-11   1.895057E-02  -4.556442E-02   0.194890    
 civs   2   3.826224E-13   -1.00000       1.903695E-10  -2.144267E-10   2.768903E-10  -4.494087E-10
 civs   3   2.437395E-13  -1.903695E-10   -1.00000       6.573896E-11  -1.188176E-10   3.873786E-10
 civs   4  -6.145118E-05   1.895286E-10  -3.106871E-11   -1.00209      -8.156733E-02  -3.463619E-02
 civs   5   4.669171E-05  -1.425405E-10   2.335626E-11   7.658648E-02  -0.943242      -0.341171    
 civs   6   0.784385      -1.511608E-06   2.474656E-07    406.106       -975.310        4165.75    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970366       2.334244E-11   9.143331E-12   8.406802E-02   0.139189       5.019322E-02
 ref    2  -9.236018E-11   -1.00000      -3.055882E-10  -2.207062E-09   8.448214E-10   5.542247E-11
 ref    3  -5.400746E-11  -3.055888E-10    1.00000      -1.656271E-09   4.913587E-10   2.200112E-10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97036631     0.00000000     0.00000000     0.08406802     0.13918853     0.05019322
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -227.7749036323  6.2355E-08  4.2438E-08  2.5972E-04  1.0000E-05
 mr-sdci #  9  2   -227.5324465907  0.0000E+00  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci #  9  3   -227.5320207590 -1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci #  9  4   -227.2714002213  1.1128E-02  0.0000E+00  2.7104E-01  1.0000E-04
 mr-sdci #  9  5   -227.0113519771  7.1496E-02  0.0000E+00  4.8267E-01  1.0000E-04
 mr-sdci #  9  6   -225.6825511124 -5.4450E-01  0.0000E+00  1.5930E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.38     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000911
ci vector #   2dasum_wr=    0.004185
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00004682     0.00000000     0.00000000     0.00002554     0.00002546     0.00000006
 sovl   7     0.00003800     0.00000000     0.00000000    -0.00000876     0.00001833     0.00000000     0.00000003
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045584
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.84582453
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.52540841
   ht   6     0.01537442     0.00000000     0.00000000    -0.00834556    -0.00840875    -0.00001882
   ht   7    -0.01247879     0.00000000     0.00000000     0.00286980    -0.00600951     0.00000075    -0.00001102
Spectrum of overlapmatrix:    0.000000    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970365      -2.161081E-11  -9.665536E-12  -9.455073E-02  -0.139303       4.525168E-05  -4.004806E-05
 refs   2  -9.263727E-11    1.00000       4.959578E-10   1.951897E-09  -8.417109E-10   7.281375E-14  -4.056636E-14
 refs   3  -5.372004E-11   4.959583E-10   -1.00000       1.581284E-09  -6.417183E-10   1.089103E-13   7.045369E-14

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00003      -5.543555E-11  -2.186556E-11   1.676892E-02  -1.978869E-02  -2.083764E-03  -0.288357    
 civs   2  -5.477256E-13   -1.00000       2.417613E-10  -5.504888E-10   6.125690E-10   4.034760E-10   2.237163E-10
 civs   3  -1.378591E-14   2.417615E-10    1.00000       5.046790E-11  -1.648441E-10  -3.779897E-10   1.213638E-10
 civs   4   8.452220E-05   2.984921E-10  -4.728247E-11  -0.947455      -0.327954      -5.426996E-02   7.778766E-02
 civs   5  -6.456390E-05  -2.374273E-10   4.485204E-11   0.190660      -0.679451       0.722600       5.323253E-02
 civs   6   -1.23189      -2.655224E-06   5.735944E-07    1069.62       -1891.69       -2525.17       -2716.51    
 civs   7  -0.838280      -1.789760E-06   1.284455E-06    871.973       -1803.79       -3051.78        4242.43    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970369       3.177833E-11  -1.570921E-11   6.023228E-02   0.131496      -8.555758E-02  -2.778616E-02
 ref    2   9.225556E-11   -1.00000       7.377191E-10  -2.519348E-09   4.816032E-10  -3.705484E-10  -1.244275E-11
 ref    3   5.371707E-11  -7.377198E-10   -1.00000      -1.493988E-09  -2.497728E-10  -6.614470E-10  -1.398987E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036932     0.00000000     0.00000000     0.06023228     0.13149645    -0.08555758    -0.02778616
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -227.7749036679  3.5575E-08  1.0395E-08  1.4339E-04  1.0000E-05
 mr-sdci # 10  2   -227.5324465907 -2.2737E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci # 10  3   -227.5320207590  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 10  4   -227.2889227368  1.7523E-02  0.0000E+00  2.5832E-01  1.0000E-04
 mr-sdci # 10  5   -227.1319228518  1.2057E-01  0.0000E+00  5.0309E-01  1.0000E-04
 mr-sdci # 10  6   -226.6090754111  9.2652E-01  0.0000E+00  7.8536E-01  1.0000E-04
 mr-sdci # 10  7   -224.2731543390 -1.3536E+00  0.0000E+00  1.5431E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.01     0.16         0.    0.2041
    2    1    0     0.13     0.09     0.00     0.13         0.    0.1775
    3    5    0     0.68     0.48     0.00     0.68         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000384
ci vector #   2dasum_wr=    0.002344
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00004682     0.00000000     0.00000000     0.00002554     0.00002546     0.00000006
 sovl   7     0.00003800     0.00000000     0.00000000    -0.00000876     0.00001833     0.00000000     0.00000003
 sovl   8    -0.00001584     0.00000000     0.00000000    -0.00000055     0.00000055     0.00000000     0.00000000     0.00000001
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045584
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.84582453
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.52540841
   ht   6     0.01537442     0.00000000     0.00000000    -0.00834556    -0.00840875    -0.00001882
   ht   7    -0.01247879     0.00000000     0.00000000     0.00286980    -0.00600951     0.00000075    -0.00001102
   ht   8     0.00520278     0.00000000     0.00000000     0.00018272    -0.00017884     0.00000024     0.00000028    -0.00000205

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   6.001248E-09   3.190440E-08   5.422483E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   1.688540E-05   3.627640E-05  -4.781751E-05  -5.285301E-07  -8.482101E-07   0.316760       0.271252      -0.908892    
   x:   2  -6.600600E-14   2.620092E-15   3.676071E-14  -0.850648       0.525736      -1.004176E-08   4.638102E-10   6.642040E-10
   x:   3   2.522616E-13  -1.040756E-13  -6.982241E-14   0.525736       0.850648       3.333405E-06  -2.197312E-07  -3.418575E-09
   x:   4  -1.481063E-07  -7.723631E-06   2.587707E-05  -1.561374E-06  -2.509778E-06   0.901147       0.212940       0.377612    
   x:   5  -1.530699E-06   1.932696E-05   2.466595E-05   6.304307E-07   1.013341E-06  -0.295967       0.938658       0.176988    
   x:   6   3.086024E-02  -4.011689E-02  -0.998718       8.443079E-13   1.366078E-12   6.482854E-07   1.663293E-05   5.670540E-05
   x:   7   1.041637E-02  -0.999127       4.045517E-02   5.093715E-12   8.241759E-12  -1.282630E-06   2.564656E-05  -3.460577E-05
   x:   8   0.999469       1.165147E-02   3.041543E-02   9.395293E-12   1.520195E-11  -5.677857E-06  -3.894372E-06   1.429195E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970365      -2.161081E-11  -9.665536E-12  -9.455073E-02  -0.139303       4.525168E-05  -4.004806E-05   1.570288E-05
 refs   2  -9.263727E-11    1.00000       4.959578E-10   1.951897E-09  -8.417109E-10   7.281375E-14  -4.056636E-14   7.192815E-14
 refs   3  -5.372004E-11   4.959583E-10   -1.00000       1.581284E-09  -6.417183E-10   1.089103E-13   7.045369E-14   2.541942E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00001      -4.518439E-11  -2.046102E-12  -5.905148E-03   3.103269E-02  -6.918679E-02   0.336198       0.109939    
 civs   2   5.794523E-13   -1.00000       3.291848E-10  -6.376022E-10   4.061821E-10  -3.507563E-11  -9.565524E-10   3.808891E-10
 civs   3   4.537848E-14  -3.291854E-10   -1.00000      -1.219890E-10   2.484366E-10  -3.801643E-10   2.474811E-09  -1.963266E-09
 civs   4  -8.596514E-05   3.050186E-10   3.184733E-11  -0.847814      -0.537008       3.683088E-02  -3.008214E-02  -6.923887E-02
 civs   5   7.112213E-05  -2.505340E-10  -1.390409E-11   0.271581      -0.491715      -0.830617      -0.133611       1.463982E-03
 civs   6    1.32632      -2.778073E-06  -2.837636E-07    1531.15       -1789.36        1704.28        2760.86        1590.75    
 civs   7    1.01515      -1.978670E-06  -8.389478E-07    1508.09       -2096.84        2651.54       -1333.12       -3982.24    
 civs   8  -0.722088       5.543599E-07  -1.306339E-06   -1263.95        2201.46       -3034.86        9867.32       -7311.51    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970371       3.375111E-11   1.082086E-11   3.710268E-02   0.126731       0.102638       2.848948E-02   1.631496E-02
 ref    2  -9.228313E-11   -1.00000      -1.667729E-10  -2.561098E-09  -1.178766E-10   5.406030E-10   3.089374E-11  -1.420412E-11
 ref    3  -5.391522E-11  -1.667729E-10    1.00000      -1.440888E-09  -5.667337E-10   5.761236E-10   2.602787E-10  -1.189224E-10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037090     0.00000000     0.00000000     0.03710268     0.12673123     0.10263833     0.02848948     0.01631496
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -227.7749036754  7.5062E-09  2.0000E-09  5.9036E-05  1.0000E-05
 mr-sdci # 11  2   -227.5324465907 -1.1369E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci # 11  3   -227.5320207590  5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 11  4   -227.3015124372  1.2590E-02  0.0000E+00  2.2692E-01  1.0000E-04
 mr-sdci # 11  5   -227.1954618444  6.3539E-02  0.0000E+00  3.8065E-01  1.0000E-04
 mr-sdci # 11  6   -226.7416317354  1.3256E-01  0.0000E+00  5.4295E-01  1.0000E-04
 mr-sdci # 11  7   -224.9964053031  7.2325E-01  0.0000E+00  1.1322E+00  1.0000E-04
 mr-sdci # 11  8   -223.8927701411 -3.6492E-02  0.0000E+00  1.6014E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.43     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000176
ci vector #   2dasum_wr=    0.001010
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00001639     0.00000000     0.00000000    -0.00000183    -0.00000604     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.88706471
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.78101412
   ht   6     0.00538080     0.00000000     0.00000000     0.00059495     0.00198729    -0.00000056

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   1.411678E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -1.638688E-05   1.712073E-09  -2.771771E-09  -7.222942E-02   0.351797       0.933286    
   x:   2   4.328198E-14    1.00000       7.320964E-18   1.907766E-10   1.712073E-09  -2.465049E-09
   x:   3  -7.007162E-14   7.320964E-18    1.00000      -3.088589E-10  -2.771771E-09   3.990806E-09
   x:   4  -1.825992E-06   1.907766E-10  -3.088589E-10   0.991951      -7.222942E-02   0.103996    
   x:   5  -6.035618E-06   2.465049E-09  -3.990806E-09  -0.103996      -0.933286       0.343748    
   x:   6   -1.00000        0.00000        0.00000        0.00000        0.00000      -1.755826E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970371       3.375111E-11   1.082086E-11   3.710268E-02   0.126731       1.778139E-05
 refs   2  -9.228313E-11   -1.00000      -1.667729E-10  -2.561098E-09  -1.178766E-10  -3.628422E-14
 refs   3  -5.391522E-11  -1.667729E-10    1.00000      -1.440888E-09  -5.667337E-10  -6.064695E-14

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00001      -9.063656E-11   1.408799E-11  -3.834225E-02   6.038310E-02   0.430209    
 civs   2   6.455270E-15   -1.00000       2.802855E-10   2.347340E-10  -3.133749E-10  -1.313641E-09
 civs   3  -1.723035E-13  -2.802856E-10   -1.00000      -1.850906E-10   2.825662E-10   1.867690E-09
 civs   4   7.098276E-06  -1.004774E-10   1.588639E-11  -0.983698      -0.185874      -1.232975E-02
 civs   5  -6.796552E-06   1.132514E-10  -1.790386E-11   0.167523      -0.949211       0.311030    
 civs   6   0.724854      -5.533832E-06   8.733831E-07   -2340.43        3685.67        26254.8    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370      -3.357473E-11  -1.064103E-11  -1.967736E-02  -0.120249       8.834400E-02
 ref    2  -9.233437E-11    1.00000      -1.135126E-10   2.353324E-09   7.620017E-10   3.162196E-10
 ref    3  -5.413850E-11  -1.135127E-10   -1.00000       1.281374E-09   8.615593E-10   9.371567E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97037013     0.00000000     0.00000000    -0.01967736    -0.12024877     0.08834400
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -227.7749036769  1.4497E-09  5.0463E-10  3.1634E-05  1.0000E-05
 mr-sdci # 12  2   -227.5324465907 -5.6843E-14  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci # 12  3   -227.5320207590  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 12  4   -227.3105244981  9.0121E-03  0.0000E+00  2.1394E-01  1.0000E-04
 mr-sdci # 12  5   -227.2293434399  3.3882E-02  0.0000E+00  3.5723E-01  1.0000E-04
 mr-sdci # 12  6   -225.6586509686 -1.0830E+00  0.0000E+00  1.2964E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.17         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.66     0.39     0.00     0.66         0.    0.7752
    4   42    0     0.01     0.00     0.00     0.01         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.059999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000087
ci vector #   2dasum_wr=    0.000540
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00001639     0.00000000     0.00000000    -0.00000183    -0.00000604     0.00000000
 sovl   7     0.00000707     0.00000000     0.00000000    -0.00000034    -0.00000072     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.88706471
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.78101412
   ht   6     0.00538080     0.00000000     0.00000000     0.00059495     0.00198729    -0.00000056
   ht   7    -0.00232034     0.00000000     0.00000000     0.00011108     0.00023765     0.00000005    -0.00000011

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   2.924585E-10   1.413509E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -6.398421E-06   1.665907E-05  -1.259469E-02  -9.576253E-10   6.037237E-09   0.301360      -0.953427    
   x:   2  -2.573405E-14  -4.227650E-14   1.506256E-08  -0.894427      -0.447214       1.220238E-08   1.724508E-09
   x:   3  -4.168626E-14   7.181500E-14   1.414641E-09   0.447214      -0.894427       5.656335E-09  -4.343651E-09
   x:   4   4.120539E-07   1.810819E-06  -0.947088      -1.505206E-08  -1.056659E-08  -0.309435      -8.529539E-02
   x:   5   9.657116E-07   6.001493E-06   0.320728      -4.452259E-09  -6.017487E-09  -0.901905      -0.289312    
   x:   6   4.041402E-02   0.999183      -2.984355E-11   3.262886E-21  -1.011023E-20   1.070230E-06   1.752562E-05
   x:   7   0.999183      -4.041402E-02  -6.462918E-11    0.00000        0.00000       2.885817E-06  -6.499482E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970371       3.375111E-11   1.082086E-11   3.710268E-02   0.126731       1.778139E-05  -6.919569E-06
 refs   2  -9.228313E-11   -1.00000      -1.667729E-10  -2.561098E-09  -1.178766E-10  -3.628422E-14  -2.120247E-14
 refs   3  -5.391522E-11  -1.667729E-10    1.00000      -1.440888E-09  -5.667337E-10  -6.064695E-14   4.452842E-14

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00001      -2.642398E-11  -1.011137E-10  -3.648135E-02   3.852687E-02  -0.121611      -0.564534    
 civs   2   3.096768E-14   -1.00000       4.335556E-10   7.516724E-10  -8.560004E-10   2.315274E-09  -6.735896E-10
 civs   3   5.363596E-13  -4.335553E-10   -1.00000      -7.022540E-10   7.493766E-10  -8.780095E-10  -2.649056E-09
 civs   4   9.135769E-06  -2.701776E-10   3.234698E-10  -0.941857      -0.339035       2.758741E-02   9.965452E-03
 civs   5  -9.214990E-06   3.611642E-10  -4.672499E-10   0.294378      -0.879589      -0.402360      -8.025815E-02
 civs   6   0.918624      -1.484948E-05   1.773187E-05   -3986.14        4944.25       -21807.8       -13999.0    
 civs   7   0.767982      -3.067878E-05   5.549397E-05   -4078.65        6010.82       -33357.9        47427.2    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370      -2.412407E-11  -2.861378E-11  -4.894681E-03  -0.115112      -8.891163E-02  -3.909190E-02
 ref    2  -9.238692E-11    1.00000      -2.667827E-10   1.860295E-09   1.517586E-09  -8.287264E-10   2.119927E-10
 ref    3  -5.340884E-11  -2.667824E-10   -1.00000       5.501217E-10   1.702102E-09  -8.459698E-10   3.733641E-10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037005     0.00000000     0.00000000    -0.00489468    -0.11511241    -0.08891163    -0.03909190
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -227.7749036773  3.8727E-10  8.8258E-11  1.2278E-05  1.0000E-05
 mr-sdci # 13  2   -227.5324465907  1.1369E-13  0.0000E+00  4.0708E-01  1.0000E-05
 mr-sdci # 13  3   -227.5320207590 -3.4106E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 13  4   -227.3172984060  6.7739E-03  0.0000E+00  1.8586E-01  1.0000E-04
 mr-sdci # 13  5   -227.2495215826  2.0178E-02  0.0000E+00  3.2094E-01  1.0000E-04
 mr-sdci # 13  6   -226.2878390205  6.2919E-01  0.0000E+00  8.5777E-01  1.0000E-04
 mr-sdci # 13  7   -224.4099110753 -5.8649E-01  0.0000E+00  1.5347E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.37     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000043
ci vector #   2dasum_wr=    0.000204
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00001639     0.00000000     0.00000000    -0.00000183    -0.00000604     0.00000000
 sovl   7     0.00000707     0.00000000     0.00000000    -0.00000034    -0.00000072     0.00000000     0.00000000
 sovl   8     0.00000159     0.00000000     0.00000000     0.00000037    -0.00000033     0.00000000     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.88706471
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.78101412
   ht   6     0.00538080     0.00000000     0.00000000     0.00059495     0.00198729    -0.00000056
   ht   7    -0.00232034     0.00000000     0.00000000     0.00011108     0.00023765     0.00000005    -0.00000011
   ht   8    -0.00052275     0.00000000     0.00000000    -0.00012071     0.00010925     0.00000001    -0.00000001    -0.00000002

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   6.050041E-11   2.925744E-10   1.413509E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -1.448009E-06  -6.432337E-06  -1.665915E-05   1.316921E-06   1.306892E-06   1.011364E-03  -0.298848       0.954300    
   x:   2  -9.978296E-15  -2.596323E-14   4.227600E-14  -0.705615       0.708595       1.123618E-06  -1.217653E-08  -1.669476E-09
   x:   3   3.402202E-13  -3.410144E-14  -7.179905E-14  -0.708595      -0.705615      -5.414117E-04  -8.040553E-06  -1.037785E-11
   x:   4  -3.777729E-07   4.037235E-07  -1.810836E-06  -3.677759E-04  -3.677410E-04   0.956474       0.278768       8.628502E-02
   x:   5   3.113807E-07   9.729050E-07  -6.001477E-06   1.065127E-04   1.065438E-04  -0.291814       0.912677       0.286122    
   x:   6  -8.550267E-04   4.040523E-02  -0.999183       7.125077E-12   7.098526E-12  -1.807514E-09  -1.120415E-06  -1.752248E-05
   x:   7  -2.230247E-02   0.998934       4.041425E-02   5.681999E-11   5.674887E-11  -1.058528E-07  -2.865464E-06   6.507620E-06
   x:   8   0.999751       2.231880E-02   4.702097E-05  -1.687295E-10  -1.687291E-10   4.514100E-07  -6.766465E-07   1.455855E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970371       3.375111E-11   1.082086E-11   3.710268E-02   0.126731       1.778139E-05  -6.919569E-06  -1.486253E-06
 refs   2  -9.228313E-11   -1.00000      -1.667729E-10  -2.561098E-09  -1.178766E-10  -3.628422E-14  -2.120247E-14  -9.914760E-15
 refs   3  -5.391522E-11  -1.667729E-10    1.00000      -1.440888E-09  -5.667337E-10  -6.064695E-14   4.452842E-14  -3.379210E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00001       1.772527E-10  -5.142415E-10  -3.091628E-02   1.882591E-02   3.787845E-02  -0.528161      -0.301159    
 civs   2  -5.098940E-14   -1.00000      -3.956216E-10   1.055744E-09  -1.375358E-09   3.213799E-09  -9.466939E-10   2.478041E-10
 civs   3  -1.094380E-12  -3.956217E-10    1.00000      -1.226030E-09   2.269971E-09  -1.432974E-08   2.666377E-08  -2.826155E-08
 civs   4  -8.945207E-06  -2.644616E-10  -3.403495E-10  -0.924057      -0.385064       4.021528E-02  -1.699573E-02   3.426921E-02
 civs   5   9.526741E-06   5.542464E-10  -1.174120E-10   0.327249      -0.848074      -0.429670      -0.126958      -5.712132E-02
 civs   6  -0.944393      -2.131687E-05   1.819450E-06   -4375.70        5218.07       -17739.3       -17010.8       -7840.36    
 civs   7  -0.870112      -5.192044E-05   8.691722E-06   -5037.19        7617.35       -36084.8        15247.8        42450.3    
 civs   8  -0.583948      -1.001892E-04   3.026080E-04   -3249.66        8061.73       -46200.1        88990.6       -79955.9    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370      -1.619380E-11   4.775598E-12  -9.329674E-04  -0.111938      -8.678897E-02  -4.445515E-02  -2.804734E-02
 ref    2   9.241519E-11    1.00000       2.288487E-10   1.572923E-09   2.029004E-09  -1.402837E-09   4.655396E-10  -1.238717E-10
 ref    3   5.304464E-11  -2.288488E-10    1.00000       1.060840E-09   6.029217E-10   9.347997E-10  -1.572498E-09   1.122201E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037002     0.00000000     0.00000000    -0.00093297    -0.11193840    -0.08678897    -0.04445515    -0.02804734
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -227.7749036773  5.1841E-11  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 14  2   -227.5324465907  5.6843E-14  8.0719E-02  4.0708E-01  1.0000E-05
 mr-sdci # 14  3   -227.5320207590  2.8422E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 14  4   -227.3184213460  1.1229E-03  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 14  5   -227.2571013177  7.5797E-03  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 14  6   -226.4899791174  2.0214E-01  0.0000E+00  8.5983E-01  1.0000E-04
 mr-sdci # 14  7   -225.3110176047  9.0111E-01  0.0000E+00  1.1661E+00  1.0000E-04
 mr-sdci # 14  8   -223.7602907394 -1.3248E-01  0.0000E+00  1.6355E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.39     0.01     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.11     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.139999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1700s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.202781
ci vector #   2dasum_wr=    8.536255
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.04384228
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6    -0.00000041    -0.08071941     0.00000000     0.00000004    -0.00000008   -14.30575968
Spectrum of overlapmatrix:    0.043842    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370      -1.619380E-11   4.775598E-12  -9.329674E-04  -0.111938       1.224200E-09
 refs   2   9.241519E-11    1.00000       2.288487E-10   1.572923E-09   2.029004E-09   3.377861E-13
 refs   3   5.304464E-11  -2.288488E-10    1.00000       1.060840E-09   6.029217E-10   2.841338E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000       1.187116E-09  -6.201134E-13  -1.511553E-14  -2.010509E-14  -5.837712E-09
 civs   2   6.183751E-14  -0.979946      -3.707550E-10   1.252413E-09  -9.214094E-10  -0.199264    
 civs   3   6.200871E-13  -3.629870E-10    1.00000      -7.535209E-13  -5.169961E-13  -7.551508E-11
 civs   4  -1.511952E-14  -1.192013E-09  -7.536570E-13   -1.00000       2.371891E-12  -4.230800E-10
 civs   5   2.010111E-14  -5.422176E-10   5.171301E-13   2.372329E-12    1.00000      -1.957528E-09
 civs   6   1.849665E-13  -0.951662       1.496537E-12  -3.320737E-09   3.143153E-09    4.68010    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370       6.459233E-11   4.116675E-12   9.329674E-04  -0.111938       2.873890E-10
 ref    2   9.247703E-11  -0.979946      -1.419064E-10  -3.205104E-10   1.107594E-09  -0.199264    
 ref    3   5.366473E-11  -1.389980E-10    1.00000      -1.061594E-09   6.024047E-10  -2.858392E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037002     0.00000000     0.00000000     0.00093297    -0.11193840     0.00000000
 ref:   2     0.00000000    -0.97994579     0.00000000     0.00000000     0.00000000    -0.19926428
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -227.7749036773  0.0000E+00  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 15  2   -227.6108362543  7.8390E-02  1.0322E-02  1.1183E-01  1.0000E-05
 mr-sdci # 15  3   -227.5320207590 -2.8422E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 15  4   -227.3184213460 -1.7053E-13  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 15  5   -227.2571013177  3.4106E-13  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 15  6   -225.6365965903 -8.5338E-01  0.0000E+00  8.5895E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.35     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.03     0.02     0.01     0.02         0.    0.0314
    6   76    0     0.04     0.08     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.442981
ci vector #   2dasum_wr=    2.067113
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.04384228
 sovl   7     0.00000001    -0.05311436     0.00000000     0.00000000     0.00000000    -0.00028220     0.01402661
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6    -0.00000041    -0.08071941     0.00000000     0.00000004    -0.00000008   -14.30575968
   ht   7    -0.00000354    17.42775678     0.00000001     0.00000021    -0.00000020     0.10777430    -4.59203948
Spectrum of overlapmatrix:    0.011171    0.043845    1.000000    1.000000    1.000000    1.000000    1.002853

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370      -1.619380E-11   4.775598E-12  -9.329674E-04  -0.111938       1.224200E-09   1.073561E-08
 refs   2   9.241519E-11    1.00000       2.288487E-10   1.572923E-09   2.029004E-09   3.377861E-13  -5.311436E-02
 refs   3   5.304464E-11  -2.288488E-10    1.00000       1.060840E-09   6.029217E-10   2.841338E-13  -1.509206E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       9.678008E-09   3.862003E-13   1.478088E-14  -2.064349E-14  -9.076314E-08  -4.567952E-08
 civs   2  -6.122732E-14   0.914079       9.085419E-10  -1.202246E-09  -1.089458E-09   0.644356       3.271084E-02
 civs   3  -3.862309E-13   6.698116E-10   -1.00000      -6.228416E-14  -9.348661E-14   6.556305E-10   9.131169E-11
 civs   4   1.479051E-14   5.092200E-10  -6.239471E-14    1.00000       1.279433E-12   5.733930E-09   2.008346E-09
 civs   5  -2.065971E-14   1.155110E-09  -9.348961E-14  -1.279326E-12    1.00000      -4.335644E-09  -4.099058E-09
 civs   6  -1.557109E-13    1.06797      -4.315940E-12   3.262700E-09   3.371043E-09   -1.68791        4.33856    
 civs   7  -1.715922E-13   -1.02160       2.874628E-09   5.334444E-10  -1.898968E-09    8.61700        3.73633    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370      -4.134566E-10  -4.390286E-12  -9.329674E-04  -0.111938       2.838146E-09   1.553509E-09
 ref    2  -9.246730E-11   0.968341       5.270092E-10   3.423440E-10   1.040408E-09   0.186669      -0.165742    
 ref    3  -5.343087E-11   4.763473E-10   -1.00000       1.060778E-09   6.028282E-10   3.776426E-10   2.866961E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037002     0.00000000     0.00000000    -0.00093297    -0.11193840     0.00000000     0.00000000
 ref:   2     0.00000000     0.96834092     0.00000000     0.00000000     0.00000000     0.18666922    -0.16574216
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 16  2   -227.6214479960  1.0612E-02  2.2111E-03  5.9317E-02  1.0000E-05
 mr-sdci # 16  3   -227.5320207590  2.2737E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 16  4   -227.3184213460  3.4106E-13  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 16  5   -227.2571013177  5.6843E-14  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 16  6   -226.8035814394  1.1670E+00  0.0000E+00  7.1866E-01  1.0000E-04
 mr-sdci # 16  7   -225.4189909591  1.0797E-01  0.0000E+00  7.8099E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.12     0.01     0.17         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.44     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.191041
ci vector #   2dasum_wr=    1.140511
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.04384228
 sovl   7     0.00000001    -0.05311436     0.00000000     0.00000000     0.00000000    -0.00028220     0.01402661
 sovl   8     0.00000007     0.00201963     0.00000000     0.00000000     0.00000001     0.00092567     0.00010475     0.00211213
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.11799886
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6    -0.00000041    -0.08071941     0.00000000     0.00000004    -0.00000008   -14.30575968
   ht   7    -0.00000354    17.42775678     0.00000001     0.00000021    -0.00000020     0.10777430    -4.59203948
   ht   8    -0.00002415    -0.66259522    -0.00000001     0.00000102    -0.00000188    -0.30405105    -0.03223226    -0.68980422
Spectrum of overlapmatrix:    0.002082    0.011177    0.043865    1.000000    1.000000    1.000000    1.000000    1.002857

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370      -1.619380E-11   4.775598E-12  -9.329674E-04  -0.111938       1.224200E-09   1.073561E-08   7.351245E-08
 refs   2   9.241519E-11    1.00000       2.288487E-10   1.572923E-09   2.029004E-09   3.377861E-13  -5.311436E-02   2.019632E-03
 refs   3   5.304464E-11  -2.288488E-10    1.00000       1.060840E-09   6.029217E-10   2.841338E-13  -1.509206E-11   2.664076E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       4.357951E-08   3.867330E-13   1.683034E-14  -5.034739E-14  -5.353348E-07  -1.507122E-06   1.699953E-07
 civs   2  -6.347098E-14  -0.900269       1.495388E-09  -9.339460E-10  -1.602210E-08  -0.645861       0.168207       1.905625E-02
 civs   3  -3.857690E-13  -9.548919E-10   -1.00000       9.759004E-14   4.107641E-15  -1.439298E-09  -7.611397E-10   1.996065E-10
 civs   4   1.626713E-14  -2.694260E-09   9.736915E-14    1.00000       3.722992E-12   2.136337E-08   6.487480E-08  -7.237256E-09
 civs   5  -2.055367E-14   1.417959E-09   4.234375E-15  -3.721766E-12    1.00000      -6.807766E-08  -1.279086E-07   1.388450E-08
 civs   6  -1.227902E-13   -1.08409      -2.149576E-11   2.950039E-09   2.393945E-08    1.41426      -0.507043        4.42682    
 civs   7  -3.736500E-13    1.17754       5.789337E-09   3.118797E-09  -1.558953E-07   -8.02493        3.45525        3.42246    
 civs   8   1.039704E-12  -0.746738      -1.396233E-08  -8.153044E-09   4.275546E-07    8.43114        19.9951       -2.88810    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370      -1.433471E-09  -4.401853E-12  -9.329674E-04  -0.111938       2.351050E-08   5.815004E-08  -6.739356E-09
 ref    2  -9.245672E-11  -0.964321       9.308436E-10   4.568581E-10  -4.849309E-09  -0.202594       2.506565E-02  -0.168559    
 ref    3  -5.343041E-11  -7.868398E-10   -1.00000       1.060938E-09   6.029258E-10  -9.453673E-10  -3.192403E-10   6.791004E-11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037002     0.00000000     0.00000000    -0.00093297    -0.11193840     0.00000002     0.00000006    -0.00000001
 ref:   2     0.00000000    -0.96432113     0.00000000     0.00000000     0.00000000    -0.20259430     0.02506565    -0.16855866
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 17  2   -227.6231002893  1.6523E-03  7.0992E-04  3.1578E-02  1.0000E-05
 mr-sdci # 17  3   -227.5320207590  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 17  4   -227.3184213460 -5.6843E-14  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 17  5   -227.2571013177  5.6843E-14  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 17  6   -226.9732707550  1.6969E-01  0.0000E+00  5.3921E-01  1.0000E-04
 mr-sdci # 17  7   -225.8369369527  4.1795E-01  0.0000E+00  1.2710E+00  1.0000E-04
 mr-sdci # 17  8   -225.4106712746  1.6504E+00  0.0000E+00  7.5054E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.10     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.47     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.111837
ci vector #   2dasum_wr=    0.648269
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000032     0.00110930     0.00000000    -0.00000001     0.00000003     0.00085012
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20865256
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6     0.00010463    -0.36479291    -0.00000001     0.00000392    -0.00000835    -0.27778938
Spectrum of overlapmatrix:    0.000849    1.000000    1.000000    1.000000    1.000000    1.000001

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970370      -1.433471E-09  -4.401853E-12  -9.329674E-04  -0.111938       3.186498E-07
 refs   2  -9.245672E-11  -0.964321       9.308436E-10   4.568581E-10  -4.849309E-09  -6.750083E-05
 refs   3  -5.343041E-11  -7.868398E-10   -1.00000       1.060938E-09   6.029258E-10  -4.514802E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -1.840251E-07  -7.257042E-15  -3.727516E-14   1.303872E-12   1.093537E-05
 civs   2  -1.494211E-13  -0.999218       3.475844E-10   4.125089E-10  -1.247589E-08  -5.489437E-02
 civs   3  -5.187399E-15  -2.595105E-10   -1.00000       2.062585E-13  -9.081025E-15  -2.662897E-09
 civs   4   9.393554E-16  -6.677966E-09   2.058471E-13    1.00000      -1.281979E-12   4.134348E-07
 civs   5   8.478501E-18   8.774139E-09  -7.767681E-15   1.334513E-12    1.00000      -9.930876E-07
 civs   6  -4.188658E-11  -0.577507      -3.903766E-08  -1.199375E-07   4.091781E-06    34.3173    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370      -4.993670E-09   4.397132E-12  -9.329674E-04  -0.111938       4.347077E-07
 ref    2   9.260363E-11   0.963606      -1.263392E-09   6.716290E-11   6.905257E-09   5.061935E-02
 ref    3   5.343560E-11   1.071808E-09    1.00000       1.060731E-09   6.029347E-10   1.156731E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037002     0.00000000     0.00000000    -0.00093297    -0.11193840     0.00000043
 ref:   2     0.00000000     0.96360582     0.00000000     0.00000000     0.00000001     0.05061935
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 18  2   -227.6235103317  4.1004E-04  2.3109E-04  1.7944E-02  1.0000E-05
 mr-sdci # 18  3   -227.5320207590  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 18  4   -227.3184213460  0.0000E+00  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 18  5   -227.2571013177  5.6843E-14  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 18  6   -226.1751937529 -7.9808E-01  0.0000E+00  1.3853E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.10     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  19

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.066143
ci vector #   2dasum_wr=    0.357174
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000032     0.00110930     0.00000000    -0.00000001     0.00000003     0.00085012
 sovl   7    -0.00000102    -0.00032236     0.00000000    -0.00000004     0.00000008     0.00011512     0.00023957
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20865256
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6     0.00010463    -0.36479291    -0.00000001     0.00000392    -0.00000835    -0.27778938
   ht   7     0.00033400     0.10580040     0.00000000     0.00001214    -0.00002679    -0.03738222    -0.07829998
Spectrum of overlapmatrix:    0.000218    0.000870    1.000000    1.000000    1.000000    1.000000    1.000001

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970370      -1.433471E-09  -4.401853E-12  -9.329674E-04  -0.111938       3.186498E-07   1.017263E-06
 refs   2  -9.245672E-11  -0.964321       9.308436E-10   4.568581E-10  -4.849309E-09  -6.750083E-05  -5.108124E-04
 refs   3  -5.343041E-11  -7.868398E-10   -1.00000       1.060938E-09   6.029258E-10  -4.514802E-11  -1.353671E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -8.709806E-07   2.296352E-13   2.837419E-12   1.069478E-10   4.643666E-05  -4.691258E-05
 civs   2   4.922594E-13   0.998152       1.244870E-09   4.621877E-09   1.659848E-07   7.546941E-02   2.114371E-02
 civs   3   3.804567E-15   8.289865E-10   -1.00000      -2.427519E-14   1.450529E-14   6.534069E-09   5.445812E-10
 civs   4   8.110414E-16  -3.015456E-08   3.265402E-14   -1.00000       3.296941E-12   1.729405E-06  -1.727857E-06
 civs   5   4.238338E-17   3.677019E-08  -1.632703E-14  -9.056490E-13    1.00000      -5.609507E-06   4.186107E-06
 civs   6   2.124716E-10   0.953097      -1.300136E-07  -1.051286E-06  -4.192974E-05   -27.4182       -22.5397    
 civs   7  -8.249947E-10   -1.15484       2.702332E-07   3.119636E-06   1.182760E-04    54.2413       -39.0588    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -3.141750E-08   4.414286E-12   9.329674E-04  -0.111938       2.006289E-06  -1.859821E-06
 ref    2   9.238909E-11  -0.962014      -2.260560E-09  -6.436418E-09  -2.224985E-07  -9.863312E-02   1.083865E-03
 ref    3   5.342661E-11  -1.655839E-09    1.00000      -1.060913E-09   6.029129E-10  -5.429001E-09   5.092816E-10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97037002    -0.00000003     0.00000000     0.00093297    -0.11193840     0.00000201    -0.00000186
 ref:   2     0.00000000    -0.96201371     0.00000000    -0.00000001    -0.00000022    -0.09863312     0.00108387
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000    -0.00000001     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 19  2   -227.6237772503  2.6692E-04  6.7357E-05  1.0104E-02  1.0000E-05
 mr-sdci # 19  3   -227.5320207590  3.9790E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 19  4   -227.3184213460  1.7053E-13  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 19  5   -227.2571013177  1.3074E-12  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 19  6   -226.9169732821  7.4178E-01  0.0000E+00  7.6843E-01  1.0000E-04
 mr-sdci # 19  7   -225.7906440434 -4.6293E-02  0.0000E+00  1.4604E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.01     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.35     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.059999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  20

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.030932
ci vector #   2dasum_wr=    0.197587
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000032     0.00110930     0.00000000    -0.00000001     0.00000003     0.00085012
 sovl   7    -0.00000102    -0.00032236     0.00000000    -0.00000004     0.00000008     0.00011512     0.00023957
 sovl   8    -0.00000657     0.00186238     0.00000000    -0.00000024     0.00000053     0.00007269    -0.00000465     0.00006458
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20865256
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6     0.00010463    -0.36479291    -0.00000001     0.00000392    -0.00000835    -0.27778938
   ht   7     0.00033400     0.10580040     0.00000000     0.00001214    -0.00002679    -0.03738222    -0.07829998
   ht   8     0.00215702    -0.61124795    -0.00000001     0.00007777    -0.00017325    -0.02385941     0.00158306    -0.02111099
Spectrum of overlapmatrix:    0.000054    0.000220    0.000876    1.000000    1.000000    1.000000    1.000000    1.000005

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970370      -1.433471E-09  -4.401853E-12  -9.329674E-04  -0.111938       3.186498E-07   1.017263E-06   6.569748E-06
 refs   2  -9.245672E-11  -0.964321       9.308436E-10   4.568581E-10  -4.849309E-09  -6.750083E-05  -5.108124E-04  -1.118535E-03
 refs   3  -5.343041E-11  -7.868398E-10   -1.00000       1.060938E-09   6.029258E-10  -4.514802E-11  -1.353671E-12  -2.400895E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -5.334508E-06  -5.276782E-12  -5.436134E-10   3.295378E-08   3.263986E-04   1.658516E-04   8.194077E-04
 civs   2   2.959071E-11  -0.995893       3.889680E-09   2.490546E-07  -1.616587E-05  -0.175073      -4.660745E-02  -0.183169    
 civs   3   8.971758E-14  -1.310985E-09   -1.00000       1.186248E-13  -1.622757E-12  -1.583693E-08  -9.571015E-10  -3.007867E-09
 civs   4   9.404970E-16  -1.854513E-07  -8.813453E-14    1.00000       1.411867E-09   1.295437E-05   6.054152E-06   2.976649E-05
 civs   5   5.955111E-15   2.297409E-07   1.470166E-13  -7.872990E-11    1.00000      -1.103326E-04  -1.487077E-05  -7.229753E-05
 civs   6  -8.540782E-10   -1.04628      -2.351432E-07  -1.976825E-05   1.562108E-03    20.1924        18.6615       -25.6419    
 civs   7   3.066021E-09    1.44154       5.679540E-07   4.705331E-05  -3.605089E-03   -45.4804        43.6867        23.6305    
 civs   8  -1.699148E-08  -0.984531      -8.661496E-07  -8.908161E-05   5.498982E-03    55.7503        17.5775        122.322    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370      -1.827635E-07   4.318348E-12  -9.329674E-04  -0.111938       2.204555E-05   6.588707E-06   3.243212E-05
 ref    2   8.141884E-11   0.960797      -3.987171E-09  -1.627719E-07   1.116951E-05   0.128337       1.708131E-03   2.947209E-02
 ref    3   5.334069E-11   2.163518E-09    1.00000       1.060822E-09   6.043636E-10   1.378602E-08  -3.299225E-10   1.340796E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97037002    -0.00000018     0.00000000    -0.00093297    -0.11193840     0.00002205     0.00000659     0.00003243
 ref:   2     0.00000000     0.96079663     0.00000000    -0.00000016     0.00001117     0.12833706     0.00170813     0.02947209
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000001     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 20  2   -227.6238435681  6.6318E-05  1.8350E-05  5.7113E-03  1.0000E-05
 mr-sdci # 20  3   -227.5320207590 -5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 20  4   -227.3184213460  1.1369E-13  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 20  5   -227.2571013182  4.5139E-10  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 20  6   -227.2037785763  2.8681E-01  0.0000E+00  4.4731E-01  1.0000E-04
 mr-sdci # 20  7   -225.7969050316  6.2610E-03  0.0000E+00  1.4670E+00  1.0000E-04
 mr-sdci # 20  8   -225.5003763237  8.9705E-02  0.0000E+00  1.0896E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.38     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.1100s 

          starting ci iteration  21

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.015438
ci vector #   2dasum_wr=    0.122549
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00003797    -0.00164497     0.00000000    -0.00000137     0.00000300     0.00001531
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20939584
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6    -0.01246911     0.53987739    -0.00000001     0.00044885    -0.00098411    -0.00500019
Spectrum of overlapmatrix:    0.000013    1.000000    1.000000    1.000000    1.000000    1.000003

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370      -1.827635E-07   4.318348E-12  -9.329674E-04  -0.111938       3.797852E-05
 refs   2   8.141884E-11   0.960797      -3.987171E-09  -1.627719E-07   1.116951E-05  -2.208033E-03
 refs   3   5.334069E-11   2.163518E-09    1.00000       1.060822E-09   6.043636E-10   2.818637E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000       2.748360E-05  -7.915151E-11   2.438735E-08  -7.361490E-07  -1.069585E-02
 civs   2  -5.222420E-10   -1.00119       3.001759E-09  -1.017841E-06   3.091897E-05   0.460760    
 civs   3   2.739108E-13  -3.664641E-10    1.00000       8.657071E-13  -4.329220E-12  -1.893155E-08
 civs   4  -4.015027E-13  -9.579505E-07   2.020496E-12    1.00000       3.090296E-08   3.878235E-04
 civs   5   6.575020E-13   1.378532E-06  -3.206723E-12  -2.283709E-09    1.00000      -9.139760E-04
 civs   6  -2.956449E-07  -0.723752       2.077157E-06  -6.422146E-04   1.938570E-02    281.664    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370      -7.881963E-07   6.755951E-12  -9.329679E-04  -0.111938       4.200983E-04
 ref    2   2.324442E-10  -0.960339      -5.689521E-09   2.773213E-07  -1.927897E-06  -0.179226    
 ref    3   5.361459E-11  -2.552949E-09    1.00000       1.061667E-09   6.006477E-10  -9.996320E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037002    -0.00000079     0.00000000    -0.00093297    -0.11193837     0.00042010
 ref:   2     0.00000000    -0.96033927    -0.00000001     0.00000028    -0.00000193    -0.17922632
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000    -0.00000001
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -227.7749036773  3.9790E-13  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 21  2   -227.6238568493  1.3281E-05  5.2865E-06  2.8287E-03  1.0000E-05
 mr-sdci # 21  3   -227.5320207590  2.8422E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 21  4   -227.3184213460  9.2655E-12  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 21  5   -227.2571013260  7.7919E-09  0.0000E+00  3.1721E-01  1.0000E-04
 mr-sdci # 21  6   -225.6123596064 -1.5914E+00  0.0000E+00  1.3651E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.39     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.060001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  22

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.009728
ci vector #   2dasum_wr=    0.051490
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00003797    -0.00164497     0.00000000    -0.00000137     0.00000300     0.00001531
 sovl   7     0.00015910    -0.00014326     0.00000000    -0.00000573     0.00001281     0.00000060     0.00000447
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20939584
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6    -0.01246911     0.53987739    -0.00000001     0.00044885    -0.00098411    -0.00500019
   ht   7    -0.05224331     0.04701812     0.00000000     0.00188050    -0.00420237    -0.00018943    -0.00145830
Spectrum of overlapmatrix:    0.000004    0.000013    1.000000    1.000000    1.000000    1.000000    1.000003

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370      -1.827635E-07   4.318348E-12  -9.329674E-04  -0.111938       3.797852E-05   1.591323E-04
 refs   2   8.141884E-11   0.960797      -3.987171E-09  -1.627719E-07   1.116951E-05  -2.208033E-03   9.441494E-05
 refs   3   5.334069E-11   2.163518E-09    1.00000       1.060822E-09   6.043636E-10   2.818637E-11   1.043359E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       9.524239E-05  -1.212749E-09  -1.917376E-06  -6.854484E-05  -4.861784E-02   5.839236E-02
 civs   2  -1.000029E-09    1.00146      -5.546497E-09  -3.986096E-06  -1.588810E-04  -0.273162      -0.373168    
 civs   3  -1.300672E-13   8.125372E-10   -1.00000       3.845151E-12   9.522051E-11   3.391541E-08   6.246438E-09
 civs   4   2.426351E-11  -3.298014E-06   4.076267E-11   -1.00000       2.936954E-06   1.780402E-03  -2.114188E-03
 civs   5  -3.720870E-11   3.866933E-06  -3.563571E-11   2.798733E-07   0.999994      -4.955280E-03   5.007658E-03
 civs   6  -9.361575E-07   0.962372      -4.705078E-06  -3.683819E-03  -0.141342       -198.949       -199.839    
 civs   7   4.562773E-06  -0.828313       8.746188E-06   1.293036E-02   0.464554        353.058       -319.312    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370      -3.454312E-06   3.592741E-11   9.329932E-04  -0.111936       2.002856E-03  -2.298797E-03
 ref    2   1.455617E-09   0.959994       9.872852E-09   5.687760E-06   2.144653E-04   0.210167       5.256434E-02
 ref    3  -5.347074E-11   2.997695E-09   -1.00000      -1.056954E-09   7.000992E-10   3.139671E-08  -3.521329E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97037002    -0.00000345     0.00000000     0.00093299    -0.11193567     0.00200286    -0.00229880
 ref:   2     0.00000000     0.95999369     0.00000001     0.00000569     0.00021447     0.21016731     0.05256434
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000003     0.00000000
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6539E-06  1.0000E-05
 mr-sdci # 22  2   -227.6238612281  4.3788E-06  1.5943E-06  1.4218E-03  1.0000E-05
 mr-sdci # 22  3   -227.5320207590 -5.6843E-14  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 22  4   -227.3184213468  7.9029E-10  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 22  5   -227.2571022718  9.4579E-07  0.0000E+00  3.1722E-01  1.0000E-04
 mr-sdci # 22  6   -226.5885634246  9.7620E-01  0.0000E+00  9.2947E-01  1.0000E-04
 mr-sdci # 22  7   -224.8138523338 -9.8305E-01  0.0000E+00  1.4859E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.01     0.00     0.00     0.01         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.01     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.059999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.1000s 

          starting ci iteration  23

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.005403
ci vector #   2dasum_wr=    0.025240
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00003797    -0.00164497     0.00000000    -0.00000137     0.00000300     0.00001531
 sovl   7     0.00015910    -0.00014326     0.00000000    -0.00000573     0.00001281     0.00000060     0.00000447
 sovl   8     0.00072137     0.00051393     0.00000000    -0.00002601     0.00005803    -0.00000022    -0.00000021     0.00000237
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20939584
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397362
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84265359
   ht   6    -0.01246911     0.53987739    -0.00000001     0.00044885    -0.00098411    -0.00500019
   ht   7    -0.05224331     0.04701812     0.00000000     0.00188050    -0.00420237    -0.00018943    -0.00145830
   ht   8    -0.23686940    -0.16867779    -0.00000001     0.00852782    -0.01903548     0.00007374     0.00007122    -0.00077631
Spectrum of overlapmatrix:    0.000002    0.000004    0.000013    1.000000    1.000000    1.000000    1.000001    1.000003

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370      -1.827635E-07   4.318348E-12  -9.329674E-04  -0.111938       3.797852E-05   1.591323E-04   7.214548E-04
 refs   2   8.141884E-11   0.960797      -3.987171E-09  -1.627719E-07   1.116951E-05  -2.208033E-03   9.441494E-05   4.542691E-04
 refs   3   5.334069E-11   2.163518E-09    1.00000       1.060822E-09   6.043636E-10   2.818637E-11   1.043359E-11   9.962780E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -4.218324E-04  -1.785652E-08  -1.412263E-04   5.456957E-03  -0.248886       0.435257      -0.315660    
 civs   2  -2.796621E-08    1.00114      -6.189713E-09  -7.424572E-05   2.655443E-03  -3.934106E-02   0.654885       8.126131E-02
 civs   3  -1.224595E-13   1.219237E-09    1.00000      -6.207350E-11   1.993967E-09  -7.637789E-08   5.851248E-09  -1.216890E-08
 civs   4   1.518161E-09   1.464520E-05   6.084180E-10  -0.999995      -2.334586E-04   9.306751E-03  -1.578972E-02   1.142751E-02
 civs   5  -2.326606E-09  -1.832871E-05  -5.441078E-10   2.030808E-05  -0.999457      -3.391856E-02   3.842607E-02  -2.702643E-02
 civs   6   3.226859E-06    1.02876       7.608893E-06   2.900581E-02   -1.31057        132.200        217.108        128.248    
 civs   7  -1.161584E-05   -1.05876      -1.819813E-05  -7.944445E-02    3.48157       -308.270       -63.7428        360.466    
 civs   8   6.226925E-05   0.764129       2.836665E-05   0.211770       -8.26361        406.051       -600.746        351.330    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370       1.439365E-05   5.955459E-10   9.348897E-04   0.111716       1.118883E-02  -1.723516E-02   1.240839E-02
 ref    2  -6.886006E-09   0.959866      -1.556700E-08  -4.651785E-05   2.008766E-03  -0.174350      -0.129089      -1.146771E-02
 ref    3  -5.346262E-11   3.410762E-09    1.00000      -1.120947E-09   1.312776E-09  -7.193164E-08   6.767122E-09  -1.138126E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97037002     0.00001439     0.00000000     0.00093489     0.11171557     0.01118883    -0.01723516     0.01240839
 ref:   2    -0.00000001     0.95986565    -0.00000002    -0.00004652     0.00200877    -0.17435019    -0.12908873    -0.01146771
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000    -0.00000007     0.00000001     0.00000000

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6512E-06  1.0000E-05
 mr-sdci # 23  2   -227.6238624464  1.2183E-06  7.9473E-07  9.6490E-04  1.0000E-05
 mr-sdci # 23  3   -227.5320207590  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 23  4   -227.3184214042  5.7422E-08  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 23  5   -227.2571763407  7.4069E-05  0.0000E+00  3.1776E-01  1.0000E-04
 mr-sdci # 23  6   -227.0115539059  4.2299E-01  0.0000E+00  6.6967E-01  1.0000E-04
 mr-sdci # 23  7   -225.4863975132  6.7255E-01  0.0000E+00  9.6644E-01  1.0000E-04
 mr-sdci # 23  8   -224.6120534380 -8.8832E-01  0.0000E+00  1.5153E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.17         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.1100s 

          starting ci iteration  24

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.005754
ci vector #   2dasum_wr=    0.022729
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00298819     0.00000994     0.00000000    -0.00010775     0.00024076     0.00001080
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941472
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397368
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84272861
   ht   6    -0.98120213    -0.00326420    -0.00000001     0.03533168    -0.07896359    -0.00354190
Spectrum of overlapmatrix:    0.000002    1.000000    1.000000    1.000000    1.000000    1.000009

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970370       1.439365E-05   5.955459E-10   9.348897E-04   0.111716      -2.988569E-03
 refs   2  -6.886006E-09   0.959866      -1.556700E-08  -4.651785E-05   2.008766E-03  -2.853553E-05
 refs   3  -5.346262E-11   3.410762E-09    1.00000      -1.120947E-09   1.312776E-09   1.909327E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -8.195439E-04  -6.952750E-08  -1.304026E-03   4.351847E-02   -2.22844    
 civs   2  -8.762506E-10   0.999997      -4.325151E-10  -5.474560E-06   1.764111E-04  -7.782198E-03
 civs   3   1.545555E-13   1.824120E-10    1.00000      -1.422957E-10   3.756841E-09  -5.075273E-08
 civs   4   1.975026E-08   2.863622E-05   2.395083E-09  -0.999953      -1.813293E-03   8.093282E-02
 civs   5  -3.325565E-08  -4.160009E-05  -2.836650E-09   1.276744E-04  -0.996303      -0.199071    
 civs   6   1.871788E-04   0.274262       2.326751E-05   0.436394       -14.5635        745.751    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370      -1.461628E-05  -1.788304E-09  -9.593886E-04  -0.110009      -8.847859E-02
 ref    2   6.359605E-10   0.959855      -1.665192E-08   2.906456E-05  -1.416347E-03  -2.915389E-02
 ref    3   5.362072E-11   3.598358E-09    1.00000       9.871495E-10   2.171160E-09  -3.677337E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97037000    -0.00001462     0.00000000    -0.00095939    -0.11000920    -0.08847859
 ref:   2     0.00000000     0.95985505    -0.00000002     0.00002906    -0.00141635    -0.02915389
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000    -0.00000004
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -227.7749036773  2.8422E-13  0.0000E+00  6.6186E-06  1.0000E-05
 mr-sdci # 24  2   -227.6238626643  2.1797E-07  5.6398E-07  7.3861E-04  1.0000E-05
 mr-sdci # 24  3   -227.5320207590  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 24  4   -227.3184218488  4.4453E-07  0.0000E+00  1.9336E-01  1.0000E-04
 mr-sdci # 24  5   -227.2576521306  4.7579E-04  0.0000E+00  3.2224E-01  1.0000E-04
 mr-sdci # 24  6   -226.0095994415 -1.0020E+00  0.0000E+00  1.0642E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.15     0.13     0.01     0.14         0.    0.1775
    3    5    0     0.66     0.39     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.059999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  25

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.005222
ci vector #   2dasum_wr=    0.017398
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00298819     0.00000994     0.00000000    -0.00010775     0.00024076     0.00001080
 sovl   7    -0.00288701     0.00003054     0.00000000     0.00010411    -0.00023529    -0.00000987     0.00000995
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941472
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397368
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84272861
   ht   6    -0.98120213    -0.00326420    -0.00000001     0.03533168    -0.07896359    -0.00354190
   ht   7     0.94798066    -0.01002468    -0.00000001    -0.03413785     0.07717659     0.00323820    -0.00326547
Spectrum of overlapmatrix:    0.000000    0.000003    1.000000    1.000000    1.000000    1.000000    1.000017

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970370       1.439365E-05   5.955459E-10   9.348897E-04   0.111716      -2.988569E-03   2.887658E-03
 refs   2  -6.886006E-09   0.959866      -1.556700E-08  -4.651785E-05   2.008766E-03  -2.853553E-05  -5.177010E-06
 refs   3  -5.346262E-11   3.410762E-09    1.00000      -1.120947E-09   1.312776E-09   1.909327E-11  -1.140449E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       3.708591E-04  -3.053849E-10   1.764442E-03   5.982334E-02   0.296821        2.44774    
 civs   2   5.058618E-09  -0.999975      -4.439134E-09  -1.928557E-05  -7.218592E-04  -4.264639E-02  -5.953242E-03
 civs   3  -1.719091E-13  -6.757438E-10    1.00000      -1.242666E-10  -4.082785E-09  -1.391958E-07   1.494992E-08
 civs   4   2.450820E-08  -1.333521E-05   9.384799E-11   0.999936      -2.569843E-03  -1.106273E-02  -8.893001E-02
 civs   5  -3.929441E-08   2.936534E-05  -2.291695E-09  -2.485650E-04  -0.994765       4.050479E-02   0.220939    
 civs   6   6.355910E-05  -0.689682       8.847203E-05   1.400376E-02    2.03409        935.682       -474.430    
 civs   7  -1.753614E-04  -0.585393       9.146677E-05   0.625660        22.8269        1071.29        356.788    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -2.500720E-07   3.558532E-10   9.597411E-04  -0.109347       1.364230E-02   9.753811E-02
 ref    2   1.075569E-08  -0.959819      -2.283070E-08  -6.916441E-05  -2.867239E-03  -7.309917E-02   6.424634E-03
 ref    3   5.329210E-11  -4.098888E-09    1.00000      -1.246075E-09  -5.378665E-09  -1.226481E-07   5.723171E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999    -0.00000025     0.00000000     0.00095974    -0.10934664     0.01364230     0.09753811
 ref:   2     0.00000001    -0.95981892    -0.00000002    -0.00006916    -0.00286724    -0.07309917     0.00642463
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000    -0.00000001    -0.00000012     0.00000001
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -227.7749036773  5.6843E-14  0.0000E+00  6.6069E-06  1.0000E-05
 mr-sdci # 25  2   -227.6238629945  3.3015E-07  2.0045E-07  5.1953E-04  1.0000E-05
 mr-sdci # 25  3   -227.5320207590 -3.4106E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 25  4   -227.3184221300  2.8125E-07  0.0000E+00  1.9339E-01  1.0000E-04
 mr-sdci # 25  5   -227.2580037165  3.5159E-04  0.0000E+00  3.2314E-01  1.0000E-04
 mr-sdci # 25  6   -226.4282557092  4.1866E-01  0.0000E+00  1.1889E+00  1.0000E-04
 mr-sdci # 25  7   -225.9631692022  4.7677E-01  0.0000E+00  1.0059E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.09     0.00     0.13         0.    0.1775
    3    5    0     0.68     0.48     0.01     0.68         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  26

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.002092
ci vector #   2dasum_wr=    0.008892
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00298819     0.00000994     0.00000000    -0.00010775     0.00024076     0.00001080
 sovl   7    -0.00288701     0.00003054     0.00000000     0.00010411    -0.00023529    -0.00000987     0.00000995
 sovl   8    -0.00033342    -0.00007627     0.00000000     0.00001210    -0.00002737    -0.00000116     0.00000108     0.00000031
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941472
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397368
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84272861
   ht   6    -0.98120213    -0.00326420    -0.00000001     0.03533168    -0.07896359    -0.00354190
   ht   7     0.94798066    -0.01002468    -0.00000001    -0.03413785     0.07717659     0.00323820    -0.00326547
   ht   8     0.10948045     0.02503211     0.00000000    -0.00396800     0.00898188     0.00037937    -0.00035458    -0.00010014
Spectrum of overlapmatrix:    0.000000    0.000000    0.000003    1.000000    1.000000    1.000000    1.000000    1.000018

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970370       1.439365E-05   5.955459E-10   9.348897E-04   0.111716      -2.988569E-03   2.887658E-03   3.329384E-04
 refs   2  -6.886006E-09   0.959866      -1.556700E-08  -4.651785E-05   2.008766E-03  -2.853553E-05  -5.177010E-06  -4.904392E-05
 refs   3  -5.346262E-11   3.410762E-09    1.00000      -1.120947E-09   1.312776E-09   1.909327E-11  -1.140449E-12   2.307898E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000       4.198198E-04  -1.761772E-08   1.764662E-03   5.993016E-02   1.330137E-02    2.47061      -1.226045E-03
 civs   2   9.569808E-09  -0.999903      -3.208820E-08  -1.885671E-05  -4.887358E-04   0.131003      -1.048983E-04   0.133695    
 civs   3  -8.412655E-15  -1.221735E-09    1.00000      -1.234091E-10  -3.354848E-09   2.664966E-07   2.660720E-09  -3.532801E-08
 civs   4  -2.489653E-08  -1.508773E-05   7.113974E-10   0.999936      -2.573579E-03  -3.234231E-04  -8.980490E-02  -2.209679E-04
 civs   5   3.946874E-08   2.936456E-05  -2.025144E-09  -2.485963E-04  -0.994771      -9.168981E-03   0.224728       6.816855E-03
 civs   6  -9.996360E-05  -0.892143       1.711806E-04   1.237726E-02    1.08660       -741.921       -342.441        679.669    
 civs   7   1.219952E-04  -0.870692       2.053482E-04   0.623533        21.6042       -907.125        484.484        468.423    
 civs   8   1.681949E-04   0.802694      -2.967505E-04   4.503463E-03    2.41642        1245.24        145.843        2031.72    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370       7.106198E-07   5.606247E-11   9.597412E-04  -0.109346      -1.524148E-03   9.860085E-02  -2.001019E-04
 ref    2  -3.647835E-09  -0.959782      -3.776547E-08  -6.891627E-05  -2.728625E-03   9.052259E-02   4.657480E-04   6.879487E-03
 ref    3  -5.346913E-11  -4.629648E-09    1.00000      -1.245140E-09  -4.610869E-09   2.825387E-07  -8.009812E-10   2.447029E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97036999     0.00000071     0.00000000     0.00095974    -0.10934563    -0.00152415     0.09860085    -0.00020010
 ref:   2     0.00000000    -0.95978172    -0.00000004    -0.00006892    -0.00272863     0.09052259     0.00046575     0.00687949
 ref:   3     0.00000000     0.00000000     1.00000000     0.00000000     0.00000000     0.00000028     0.00000000     0.00000002

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -227.7749036773  5.6843E-14  0.0000E+00  6.6056E-06  1.0000E-05
 mr-sdci # 26  2   -227.6238631554  1.6090E-07  5.6399E-08  2.9174E-04  1.0000E-05
 mr-sdci # 26  3   -227.5320207590  3.4106E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 26  4   -227.3184221300  3.2401E-12  0.0000E+00  1.9339E-01  1.0000E-04
 mr-sdci # 26  5   -227.2580044673  7.5075E-07  0.0000E+00  3.2314E-01  1.0000E-04
 mr-sdci # 26  6   -227.0007237059  5.7247E-01  0.0000E+00  6.9940E-01  1.0000E-04
 mr-sdci # 26  7   -225.9723851677  9.2160E-03  0.0000E+00  9.9534E-01  1.0000E-04
 mr-sdci # 26  8   -224.8775840201  2.6553E-01  0.0000E+00  1.1219E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.35     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.09     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  27

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000867
ci vector #   2dasum_wr=    0.005787
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00001999     0.00001246     0.00000000    -0.00000075    -0.00000236     0.00000005
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941543
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397440
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84355674
   ht   6    -0.00656507    -0.00408939     0.00000001     0.00024531     0.00077371    -0.00001673
Spectrum of overlapmatrix:    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970370       7.106198E-07   5.606247E-11   9.597412E-04  -0.109346      -2.022024E-05
 refs   2  -3.647835E-09  -0.959782      -3.776547E-08  -6.891627E-05  -2.728625E-03   1.468382E-05
 refs   3  -5.346913E-11  -4.629648E-09    1.00000      -1.245140E-09  -4.610869E-09  -8.807657E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       1.401168E-05  -1.501576E-08  -4.623848E-06  -2.259784E-04  -8.882216E-02
 civs   2   7.549556E-10  -0.999991      -9.818597E-09  -2.924187E-06  -1.425674E-04  -5.550986E-02
 civs   3   3.311737E-13   4.108496E-10   -1.00000       6.796459E-11   2.732451E-09   3.197920E-07
 civs   4  -4.446830E-11  -4.897758E-07   5.105093E-10   -1.00000       1.126756E-05   3.375442E-03
 civs   5  -6.267939E-11  -3.113557E-07  -1.480427E-10  -2.134569E-06  -0.999970       1.301913E-02
 civs   6  -6.246361E-05  -0.700810       7.510152E-04   0.231267        11.3026        4442.55    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370      -1.029925E-07  -6.542617E-10  -9.596970E-04   0.109333      -5.059498E-03
 ref    2   2.006212E-09   0.959763       5.821733E-08   7.512455E-05   3.031342E-03   0.118475    
 ref    3   5.380085E-11   5.046631E-09   -1.00000       1.311092E-09   7.244291E-09   2.808610E-07

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036999    -0.00000010     0.00000000    -0.00095970     0.10933312    -0.00505950
 ref:   2     0.00000000     0.95976304     0.00000006     0.00007512     0.00303134     0.11847525
 ref:   3     0.00000000     0.00000001    -1.00000000     0.00000000     0.00000001     0.00000028
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6056E-06  1.0000E-05
 mr-sdci # 27  2   -227.6238631949  3.9526E-08  2.5598E-08  1.8899E-04  1.0000E-05
 mr-sdci # 27  3   -227.5320207590  0.0000E+00  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 27  4   -227.3184221335  3.4761E-09  0.0000E+00  1.9339E-01  1.0000E-04
 mr-sdci # 27  5   -227.2580123803  7.9130E-06  0.0000E+00  3.2314E-01  1.0000E-04
 mr-sdci # 27  6   -226.0354979567 -9.6523E-01  0.0000E+00  1.4367E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.13     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.43     0.01     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  28

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000764
ci vector #   2dasum_wr=    0.003350
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00001999     0.00001246     0.00000000    -0.00000075    -0.00000236     0.00000005
 sovl   7     0.00008318     0.00001759     0.00000000    -0.00000312    -0.00000758     0.00000000     0.00000003
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941543
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397440
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84355674
   ht   6    -0.00656507    -0.00408939     0.00000001     0.00024531     0.00077371    -0.00001673
   ht   7    -0.02731391    -0.00577343     0.00000000     0.00102250     0.00248539    -0.00000116    -0.00000984
Spectrum of overlapmatrix:    0.000000    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970370       7.106198E-07   5.606247E-11   9.597412E-04  -0.109346      -2.022024E-05  -8.299424E-05
 refs   2  -3.647835E-09  -0.959782      -3.776547E-08  -6.891627E-05  -2.728625E-03   1.468382E-05  -3.525641E-05
 refs   3  -5.346913E-11  -4.629648E-09    1.00000      -1.245140E-09  -4.610869E-09  -8.807657E-12  -2.853786E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -4.895196E-05   1.579963E-07  -4.150883E-05   5.959546E-04  -0.282605      -0.476469    
 civs   2  -5.808684E-10   -1.00000       1.679444E-08  -8.340129E-06  -2.453424E-05  -3.024574E-02  -0.122838    
 civs   3   2.605275E-13   9.789227E-10   -1.00000      -1.018457E-10   5.817899E-09  -6.384441E-07   1.024025E-07
 civs   4   2.514624E-10   1.838701E-06  -5.853389E-09  -0.999998      -2.140477E-05   1.065434E-02   1.791163E-02
 civs   5   6.578268E-10   5.483836E-06  -1.644026E-08   2.629701E-06   -1.00004       1.970575E-02   4.606117E-02
 civs   6  -1.069113E-04   -1.08643       1.878593E-03  -5.201540E-02    17.9546       -3553.68        2675.61    
 civs   7   1.064781E-04   0.849619      -2.350924E-03   0.511510       -11.4799        4251.55        5084.88    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -2.352511E-06   5.548664E-09  -9.611489E-04   0.109361      -8.911068E-03  -1.878558E-02
 ref    2  -1.120371E-09   0.959737       1.321618E-07   5.811589E-05   3.420654E-03  -0.173101      -2.221596E-02
 ref    3   5.372756E-11   5.593875E-09   -1.00000       1.129182E-09   1.059851E-08  -7.284237E-07  -6.591555E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999    -0.00000235     0.00000001    -0.00096115     0.10936090    -0.00891107    -0.01878558
 ref:   2     0.00000000     0.95973712     0.00000013     0.00005812     0.00342065    -0.17310130    -0.02221596
 ref:   3     0.00000000     0.00000001    -1.00000000     0.00000000     0.00000001    -0.00000073    -0.00000007
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -227.7749036773 -2.2737E-13  0.0000E+00  6.6054E-06  1.0000E-05
 mr-sdci # 28  2   -227.6238632167  2.1749E-08  1.0186E-08  1.0226E-04  1.0000E-05
 mr-sdci # 28  3   -227.5320207590  1.1369E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 28  4   -227.3184221387  5.1816E-09  0.0000E+00  1.9339E-01  1.0000E-04
 mr-sdci # 28  5   -227.2580146946  2.3143E-06  0.0000E+00  3.2303E-01  1.0000E-04
 mr-sdci # 28  6   -226.8669807318  8.3148E-01  0.0000E+00  7.8393E-01  1.0000E-04
 mr-sdci # 28  7   -224.8461256775 -1.1263E+00  0.0000E+00  1.4012E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.36     0.01     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  29

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000693
ci vector #   2dasum_wr=    0.002407
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00001999     0.00001246     0.00000000    -0.00000075    -0.00000236     0.00000005
 sovl   7     0.00008318     0.00001759     0.00000000    -0.00000312    -0.00000758     0.00000000     0.00000003
 sovl   8    -0.00036176     0.00000889     0.00000000     0.00001312     0.00003183    -0.00000001    -0.00000003     0.00000016
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941543
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397440
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84355674
   ht   6    -0.00656507    -0.00408939     0.00000001     0.00024531     0.00077371    -0.00001673
   ht   7    -0.02731391    -0.00577343     0.00000000     0.00102250     0.00248539    -0.00000116    -0.00000984
   ht   8     0.11878725    -0.00291641     0.00000000    -0.00430348    -0.01043563     0.00000388     0.00001084    -0.00005167
Spectrum of overlapmatrix:    0.000000    0.000000    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970370       7.106198E-07   5.606247E-11   9.597412E-04  -0.109346      -2.022024E-05  -8.299424E-05   3.620498E-04
 refs   2  -3.647835E-09  -0.959782      -3.776547E-08  -6.891627E-05  -2.728625E-03   1.468382E-05  -3.525641E-05  -1.551771E-05
 refs   3  -5.346913E-11  -4.629648E-09    1.00000      -1.245140E-09  -4.610869E-09  -8.807657E-12  -2.853786E-11  -5.244861E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       4.712948E-05  -5.097317E-07   3.616256E-05  -2.970842E-03  -0.499508        2.22395       0.305217    
 civs   2  -7.067076E-10   -1.00001       4.472397E-08  -1.198828E-05   1.520166E-04   5.283810E-02  -5.245894E-02   0.127816    
 civs   3   4.087264E-13   1.170807E-09   -1.00000      -1.970649E-10   9.620725E-09   8.773145E-07  -6.632822E-08  -9.599449E-08
 civs   4   1.369573E-10  -1.641364E-06   1.834105E-08   -1.00000       1.060941E-04   1.768183E-02  -8.075713E-02  -1.169963E-02
 civs   5   3.819955E-10  -2.856106E-06   4.114079E-08  -3.305661E-06  -0.999711       5.077026E-02  -0.194883      -3.103546E-02
 civs   6  -1.082079E-04   -1.13866       2.316677E-03  -0.138373        22.7387        3189.46        1527.90       -2754.30    
 civs   7   1.097624E-04   0.964688      -3.240749E-03   0.657923       -19.2030       -4184.55       -1120.77       -5072.22    
 civs   8   9.423787E-06   0.289168      -2.026177E-03   0.243598       -11.3710       -2166.70        5974.33       -474.824    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370       1.520699E-06  -2.136595E-08  -9.580831E-04   0.109214      -2.247763E-02   8.830950E-02   1.195634E-02
 ref    2  -1.279901E-09   0.959732       1.744432E-07   5.142355E-05   3.769300E-03   0.177135       2.012821E-02   2.316248E-02
 ref    3   5.387563E-11   5.781475E-09   -1.00000       1.029311E-09   1.463696E-08   9.795308E-07  -7.801248E-08   7.505471E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000152    -0.00000002    -0.00095808     0.10921405    -0.02247763     0.08830950     0.01195634
 ref:   2     0.00000000     0.95973161     0.00000017     0.00005142     0.00376930     0.17713492     0.02012821     0.02316248
 ref:   3     0.00000000     0.00000001    -1.00000000     0.00000000     0.00000001     0.00000098    -0.00000008     0.00000008

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -227.7749036773  5.6843E-14  0.0000E+00  6.6055E-06  1.0000E-05
 mr-sdci # 29  2   -227.6238632196  2.9453E-09  7.8198E-09  8.9910E-05  1.0000E-05
 mr-sdci # 29  3   -227.5320207590 -1.7053E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 29  4   -227.3184221402  1.4871E-09  0.0000E+00  1.9339E-01  1.0000E-04
 mr-sdci # 29  5   -227.2580176135  2.9189E-06  0.0000E+00  3.2349E-01  1.0000E-04
 mr-sdci # 29  6   -226.9919220009  1.2494E-01  0.0000E+00  6.9900E-01  1.0000E-04
 mr-sdci # 29  7   -225.9142462970  1.0681E+00  0.0000E+00  9.7360E-01  1.0000E-04
 mr-sdci # 29  8   -224.8397933345 -3.7791E-02  0.0000E+00  1.4001E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.12     0.01     0.17         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.43     0.01     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.01     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0300s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  30

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000603
ci vector #   2dasum_wr=    0.002136
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00032215    -0.00000043     0.00000000     0.00001168     0.00002868     0.00000012
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941549
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397441
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84356989
   ht   6     0.10578203     0.00013998     0.00000000    -0.00383036    -0.00940184    -0.00004088
Spectrum of overlapmatrix:    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370       1.520699E-06  -2.136595E-08  -9.580831E-04   0.109214      -3.221784E-04
 refs   2  -1.279901E-09   0.959732       1.744432E-07   5.142355E-05   3.769300E-03  -4.642715E-06
 refs   3   5.387563E-11   5.781475E-09   -1.00000       1.029311E-09   1.463696E-08   1.892807E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       7.956257E-05  -9.710846E-07  -2.683103E-04   3.678396E-03    2.28649    
 civs   2  -1.288803E-11    1.00000      -1.027862E-09  -3.339016E-07   4.625884E-06   2.992361E-03
 civs   3  -3.397526E-14  -2.420481E-10    1.00000      -3.523886E-10   3.792752E-09   4.522970E-07
 civs   4   3.135062E-10  -2.867713E-06   3.491239E-08  -0.999990      -1.374096E-04  -8.302587E-02
 civs   5   7.873024E-10  -7.270909E-06   8.951310E-08   2.772708E-05   -1.00033      -0.201939    
 civs   6  -2.694607E-05   0.246972      -3.014366E-03  -0.832868        11.4182        7097.55    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370      -1.634562E-06   1.722902E-08   9.690739E-04  -0.109359      -8.991011E-02
 ref    2   1.395619E-09   0.959731       1.877908E-07  -4.777222E-05  -3.819109E-03  -3.084548E-02
 ref    3  -5.384169E-11   6.023886E-09   -1.00000      -6.780997E-10  -1.841279E-08  -4.417634E-07

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97036999    -0.00000163     0.00000002     0.00096907    -0.10935883    -0.08991011
 ref:   2     0.00000000     0.95973053     0.00000019    -0.00004777    -0.00381911    -0.03084548
 ref:   3     0.00000000     0.00000001    -1.00000000     0.00000000    -0.00000002    -0.00000044
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6052E-06  1.0000E-05
 mr-sdci # 30  2   -227.6238632215  1.9317E-09  4.5785E-09  6.9043E-05  1.0000E-05
 mr-sdci # 30  3   -227.5320207590  8.5265E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 30  4   -227.3184221579  1.7757E-08  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 30  5   -227.2580207947  3.1812E-06  0.0000E+00  3.2300E-01  1.0000E-04
 mr-sdci # 30  6   -226.0288324683 -9.6309E-01  0.0000E+00  1.0486E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.17         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.42     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  31

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000468
ci vector #   2dasum_wr=    0.001646
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00032215    -0.00000043     0.00000000     0.00001168     0.00002868     0.00000012
 sovl   7     0.00024313     0.00000199     0.00000000    -0.00000879    -0.00002156    -0.00000009     0.00000007
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941549
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397441
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84356989
   ht   6     0.10578203     0.00013998     0.00000000    -0.00383036    -0.00940184    -0.00004088
   ht   7    -0.07983289    -0.00065178     0.00000000     0.00288297     0.00706753     0.00002936    -0.00002331

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   4.382414E-09   2.686537E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -2.172384E-05  -4.030139E-04   1.890915E-02   8.904460E-02  -2.926287E-02  -1.289143E-03   0.995417    
   x:   2  -1.408976E-06  -1.462993E-06  -7.488193E-03  -5.149966E-02   1.136693E-02  -0.998573       3.790056E-03
   x:   3   1.266058E-11   3.837785E-12   0.850468      -7.751772E-03   0.525969       9.403928E-06  -1.113220E-08
   x:   4   7.680805E-07   1.460013E-05   0.525630       1.201585E-02  -0.849743      -1.437096E-02  -3.605883E-02
   x:   5   1.862672E-06   3.582918E-05  -1.802392E-03   0.994593       1.757369E-02  -5.141666E-02  -8.848654E-02
   x:   6   0.558558      -0.829466      -9.086490E-11   2.076877E-14   1.470077E-10  -8.012333E-07  -3.236363E-04
   x:   7   0.829466       0.558558      -1.203993E-10    0.00000       1.947903E-10  -1.061678E-06   2.442437E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370       1.520699E-06  -2.136595E-08  -9.580831E-04   0.109214      -3.221784E-04   2.433396E-04
 refs   2  -1.279901E-09   0.959732       1.744432E-07   5.142355E-05   3.769300E-03  -4.642715E-06  -1.861197E-06
 refs   3   5.387563E-11   5.781475E-09   -1.00000       1.029311E-09   1.463696E-08   1.892807E-12  -3.498861E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       4.189826E-05  -3.125509E-07  -1.268960E-04   2.090349E-03  -0.170721       -2.47472    
 civs   2   1.146607E-10   0.999999       2.101788E-08   4.616307E-06  -5.218065E-05  -2.073474E-02  -1.024334E-02
 civs   3  -6.181748E-15  -7.919352E-10    1.00000      -1.405859E-09   1.336906E-08   1.263237E-06  -1.648079E-07
 civs   4   1.609789E-10  -1.488336E-06   1.055240E-08  -0.999995      -8.797326E-05   5.645284E-03   8.970322E-02
 civs   5   4.400841E-10  -4.240207E-06   3.827183E-08   2.210569E-05   -1.00018       1.715769E-02   0.218892    
 civs   6  -7.026848E-05   0.651577      -1.029328E-02   -2.53438        31.1086        8742.90       -4512.33    
 civs   7  -7.466201E-05   0.691036      -1.235349E-02   -2.83622        32.6225        12286.9        4199.74    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370      -5.177113E-08  -1.030639E-08   9.637129E-04  -0.109290       9.324420E-03   9.816245E-02
 ref    2   1.856809E-09   0.959726       2.655406E-07  -2.986441E-05  -4.025222E-03  -8.329396E-02   4.131714E-03
 ref    3  -5.386696E-11   6.550397E-09   -1.00000       4.713345E-10  -2.909152E-08  -1.676462E-06   1.242743E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97036999    -0.00000005    -0.00000001     0.00096371    -0.10928982     0.00932442     0.09816245
 ref:   2     0.00000000     0.95972623     0.00000027    -0.00002986    -0.00402522    -0.08329396     0.00413171
 ref:   3     0.00000000     0.00000001    -1.00000000     0.00000000    -0.00000003    -0.00000168     0.00000001
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -227.7749036773  0.0000E+00  0.0000E+00  6.6053E-06  1.0000E-05
 mr-sdci # 31  2   -227.6238632247  3.1638E-09  1.6934E-09  4.7387E-05  1.0000E-05
 mr-sdci # 31  3   -227.5320207590  3.4106E-13  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 31  4   -227.3184221964  3.8500E-08  0.0000E+00  1.9339E-01  1.0000E-04
 mr-sdci # 31  5   -227.2580254998  4.7051E-06  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 31  6   -226.5476235753  5.1879E-01  0.0000E+00  1.1176E+00  1.0000E-04
 mr-sdci # 31  7   -225.9682214805  5.3975E-02  0.0000E+00  1.0011E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.01     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  32

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000197
ci vector #   2dasum_wr=    0.000805
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00032215    -0.00000043     0.00000000     0.00001168     0.00002868     0.00000012
 sovl   7     0.00024313     0.00000199     0.00000000    -0.00000879    -0.00002156    -0.00000009     0.00000007
 sovl   8    -0.00002565     0.00000884     0.00000000     0.00000097     0.00000233     0.00000001    -0.00000001     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941549
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397441
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84356989
   ht   6     0.10578203     0.00013998     0.00000000    -0.00383036    -0.00940184    -0.00004088
   ht   7    -0.07983289    -0.00065178     0.00000000     0.00288297     0.00706753     0.00002936    -0.00002331
   ht   8     0.00842302    -0.00290108     0.00000000    -0.00031748    -0.00076269    -0.00000316     0.00000227    -0.00000075

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.475061E-09   4.393585E-09   2.694635E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   4.231247E-06   2.168580E-05   4.038093E-04  -1.819367E-02  -1.272698E-02   9.296137E-02  -1.678542E-03   0.995421    
   x:   2  -8.803256E-06   1.957917E-06   9.615086E-07   3.156351E-03   2.470822E-04  -6.945126E-03   0.999968       2.395658E-03
   x:   3  -3.329017E-12  -1.248029E-11  -3.982256E-12  -0.357666      -0.913257      -0.195030       6.113679E-08  -1.151972E-08
   x:   4  -1.908811E-07  -7.643361E-07  -1.463115E-05  -0.918132       0.306499       0.248536       4.634881E-03  -3.606504E-02
   x:   5  -4.174710E-07  -1.856599E-06  -3.590250E-05   0.169612      -0.268063       0.944194       6.300629E-03  -8.849402E-02
   x:   6  -8.123264E-02  -0.554219       0.828397      -9.272217E-10  -7.468921E-09   3.671394E-08   3.490873E-07  -3.236371E-04
   x:   7  -1.990176E-02  -0.830076      -0.557294      -1.205223E-09  -9.704220E-09   4.770258E-08   1.401126E-06   2.442420E-04
   x:   8   0.996496      -6.175706E-02   5.639935E-02   2.225912E-10   1.825847E-09  -8.967908E-09   8.901019E-06  -2.575389E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370       1.520699E-06  -2.136595E-08  -9.580831E-04   0.109214      -3.221784E-04   2.433396E-04  -2.559041E-05
 refs   2  -1.279901E-09   0.959732       1.744432E-07   5.142355E-05   3.769300E-03  -4.642715E-06  -1.861197E-06   6.209835E-06
 refs   3   5.387563E-11   5.781475E-09   -1.00000       1.029311E-09   1.463696E-08   1.892807E-12  -3.498861E-11  -2.397743E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       4.595977E-05  -4.903879E-07  -1.588834E-04   2.215067E-03  -2.287778E-02   -2.48393      -9.281348E-03
 civs   2   3.181270E-09   0.999992       3.397460E-07   7.107618E-05  -3.312998E-04   0.125942       5.768644E-05  -0.193907    
 civs   3   3.573196E-14  -1.317771E-09    1.00000      -3.951670E-09   2.210781E-08  -2.694701E-06  -1.196246E-07  -7.800592E-08
 civs   4   2.383627E-10  -1.652794E-06   1.737551E-08  -0.999994      -9.886315E-05   2.158676E-03   9.005505E-02  -3.720233E-04
 civs   5   6.216083E-10  -4.654471E-06   5.732750E-08   2.849676E-05   -1.00019      -9.446781E-04   0.219893      -8.386960E-04
 civs   6  -1.296938E-04   0.811717      -1.838335E-02   -4.76728        41.3543       -7090.46       -3714.98       -5981.14    
 civs   7  -1.802379E-04   0.964543      -2.585904E-02   -6.38326        48.6860       -10588.4        5152.62       -5473.02    
 civs   8  -3.264331E-04   0.739472      -3.333690E-02   -6.82322        28.4384       -12201.2       -1341.42        22880.4    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370      -1.175733E-07  -7.682380E-09   9.642389E-04  -0.109289      -2.253474E-03   9.864488E-02   5.740310E-04
 ref    2   3.245916E-09   0.959723       4.271860E-07   8.540818E-06  -4.194000E-03   9.772521E-02   2.163885E-04  -6.063526E-03
 ref    3  -5.389744E-11   7.049189E-09   -1.00000       3.301106E-09  -3.905654E-08   3.345024E-06  -3.234843E-08  -2.915685E-07

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97036999    -0.00000012    -0.00000001     0.00096424    -0.10928947    -0.00225347     0.09864488     0.00057403
 ref:   2     0.00000000     0.95972283     0.00000043     0.00000854    -0.00419400     0.09772521     0.00021639    -0.00606353
 ref:   3     0.00000000     0.00000001    -1.00000000     0.00000000    -0.00000004     0.00000335    -0.00000003    -0.00000029

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -227.7749036773 -2.2737E-13  0.0000E+00  6.6053E-06  1.0000E-05
 mr-sdci # 32  2   -227.6238632259  1.2524E-09  6.1801E-10  3.0766E-05  1.0000E-05
 mr-sdci # 32  3   -227.5320207590  2.4443E-12  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 32  4   -227.3184222625  6.6030E-08  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 32  5   -227.2580264730  9.7324E-07  0.0000E+00  3.2318E-01  1.0000E-04
 mr-sdci # 32  6   -227.0132218684  4.6560E-01  0.0000E+00  7.3801E-01  1.0000E-04
 mr-sdci # 32  7   -225.9734381434  5.2167E-03  0.0000E+00  9.9498E-01  1.0000E-04
 mr-sdci # 32  8   -224.8941204070  5.4327E-02  0.0000E+00  1.2702E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.37     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  33

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000093
ci vector #   2dasum_wr=    0.000612
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000216     0.00000114     0.00000000     0.00000009     0.00000025     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397454
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84357875
   ht   6     0.00070767    -0.00037297     0.00000001    -0.00002983    -0.00008143    -0.00000018

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   5.548737E-10    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   2.155160E-06   4.598203E-02   0.280733       0.177118       0.338157       0.879405    
   x:   2  -1.136377E-06   0.187139       0.345023       0.421731       0.673104      -0.463694    
   x:   3   3.592135E-11  -0.707090      -0.391167      -0.231056       0.541872       1.465756E-05
   x:   4  -9.098939E-08  -0.643162       0.291371       0.600990      -0.372663      -3.712828E-02
   x:   5  -2.483118E-07  -0.221881       0.751158      -0.613349      -8.816119E-03  -0.101268    
   x:   6    1.00000      -2.875180E-11   1.002870E-10  -8.179425E-11    0.00000      -2.450715E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970370      -1.175733E-07  -7.682380E-09   9.642389E-04  -0.109289       2.195729E-06
 refs   2   3.245916E-09   0.959723       4.271860E-07   8.540818E-06  -4.194000E-03  -1.440847E-06
 refs   3  -5.389744E-11   7.049189E-09   -1.00000       3.301106E-09  -3.905654E-08   5.511453E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       1.448165E-06  -1.594275E-07   2.339643E-05  -7.708144E-05   9.149182E-02
 civs   2  -7.590205E-10   0.999999       8.456183E-08  -1.235847E-05   4.070406E-05  -4.825781E-02
 civs   3  -3.293262E-13  -4.470467E-10    1.00000       3.655993E-09  -9.673863E-09   3.267471E-06
 civs   4  -7.295089E-11  -7.905741E-08   9.551305E-09   -1.00000      -1.568876E-06  -3.607005E-03
 civs   5  -1.332052E-10  -1.197688E-07   1.144773E-08   1.912086E-06  -0.999991      -1.138394E-02
 civs   6   6.703202E-04   0.671951      -7.397463E-02    10.8560       -35.7660        42452.5    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370      -3.439280E-08  -1.664889E-08  -9.633151E-04   0.109285       5.673849E-03
 ref    2  -4.939636E-09   0.959721       6.148800E-07  -3.605139E-05   4.284559E-03  -0.107434    
 ref    3   5.423047E-11   7.499938E-09   -1.00000      -6.897433E-09   4.853320E-08  -3.033408E-06

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036999    -0.00000003    -0.00000002    -0.00096332     0.10928472     0.00567385
 ref:   2     0.00000000     0.95972113     0.00000061    -0.00003605     0.00428456    -0.10743388
 ref:   3     0.00000000     0.00000001    -1.00000000    -0.00000001     0.00000005    -0.00000303
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6052E-06  1.0000E-05
 mr-sdci # 33  2   -227.6238632264  4.1484E-10  2.5543E-10  1.8933E-05  1.0000E-05
 mr-sdci # 33  3   -227.5320207590  4.8317E-12  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 33  4   -227.3184223509  8.8417E-08  0.0000E+00  1.9339E-01  1.0000E-04
 mr-sdci # 33  5   -227.2580273899  9.1685E-07  0.0000E+00  3.2319E-01  1.0000E-04
 mr-sdci # 33  6   -225.9663255500 -1.0469E+00  0.0000E+00  1.4759E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.06     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  34

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000079
ci vector #   2dasum_wr=    0.000338
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000216     0.00000114     0.00000000     0.00000009     0.00000025     0.00000000
 sovl   7     0.00000863    -0.00000166     0.00000000    -0.00000035    -0.00000080     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397454
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84357875
   ht   6     0.00070767    -0.00037297     0.00000001    -0.00002983    -0.00008143    -0.00000018
   ht   7    -0.00283380     0.00054531    -0.00000001     0.00011344     0.00026256     0.00000001    -0.00000010

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   2.262823E-10   5.557553E-10    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   8.507109E-06  -2.598692E-06   7.948754E-02   1.914296E-02   3.352644E-02   0.212776      -0.973097    
   x:   2  -1.600465E-06   1.220800E-06  -6.636923E-02   1.127218E-03  -7.900036E-03   0.975902       0.207718    
   x:   3   1.399632E-11  -3.669447E-11   8.893857E-02   0.759544      -0.644346      -4.425385E-05  -2.692710E-06
   x:   4  -3.407892E-07   1.087633E-07   8.290750E-03   0.645826       0.762432      -2.340697E-03   3.913858E-02
   x:   5  -7.870005E-07   2.894090E-07   0.990605      -7.505917E-02   4.825016E-02   4.833424E-02   9.167220E-02
   x:   6  -5.172773E-02  -0.998661       1.224491E-12   1.227309E-10   1.452491E-10   6.622144E-07   2.359550E-06
   x:   7  -0.998661       5.172773E-02    0.00000       3.269029E-11   3.864355E-11   1.769552E-07  -8.830046E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970370      -1.175733E-07  -7.682380E-09   9.642389E-04  -0.109289       2.195729E-06  -8.601767E-06
 refs   2   3.245916E-09   0.959723       4.271860E-07   8.540818E-06  -4.194000E-03  -1.440847E-06  -3.353880E-06
 refs   3  -5.389744E-11   7.049189E-09   -1.00000       3.301106E-09  -3.905654E-08   5.511453E-12  -4.604103E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -5.151481E-06   1.642004E-06  -1.851617E-04   1.283930E-03   0.306650      -0.487792    
 civs   2  -1.953775E-09    1.00000      -1.879252E-07   1.780892E-05  -1.529103E-04  -3.502773E-02   0.113022    
 civs   3  -1.094039E-13  -1.022125E-09    1.00000       1.244584E-08  -5.582974E-08  -5.813477E-06  -1.719068E-06
 civs   4  -3.656785E-10   1.704417E-07  -5.671327E-08  -0.999992      -7.280243E-05  -1.283565E-02   1.961819E-02
 civs   5  -8.734576E-10   5.446541E-07  -1.765895E-07   3.514171E-05   -1.00010      -2.284270E-02   4.643751E-02
 civs   6   2.997851E-04    1.02440      -0.176042        24.6560       -130.361       -33241.0       -26483.6    
 civs   7  -9.716106E-04   0.852736      -0.234226        27.6124       -181.327       -43833.5        49908.3    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -2.638156E-07   4.642642E-08  -9.717747E-04   0.109328       8.977690E-03  -1.916769E-02
 ref    2  -2.290618E-09   0.959719       1.286785E-06  -1.197307E-04   4.843667E-03   0.161386      -2.095189E-02
 ref    3   5.405325E-11   8.037681E-09   -1.00000      -1.688357E-08   1.025190E-07   7.648997E-06  -7.256503E-07

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999    -0.00000026     0.00000005    -0.00097177     0.10932846     0.00897769    -0.01916769
 ref:   2     0.00000000     0.95971874     0.00000129    -0.00011973     0.00484367     0.16138628    -0.02095189
 ref:   3     0.00000000     0.00000001    -1.00000000    -0.00000002     0.00000010     0.00000765    -0.00000073
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6053E-06  1.0000E-05
 mr-sdci # 34  2   -227.6238632266  2.1771E-10  1.1345E-10  1.0760E-05  1.0000E-05
 mr-sdci # 34  3   -227.5320207590  1.4666E-11  0.0000E+00  4.0682E-01  1.0000E-05
 mr-sdci # 34  4   -227.3184225033  1.5240E-07  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 34  5   -227.2580332589  5.8691E-06  0.0000E+00  3.2299E-01  1.0000E-04
 mr-sdci # 34  6   -226.8379263682  8.7160E-01  0.0000E+00  8.4497E-01  1.0000E-04
 mr-sdci # 34  7   -224.8364118679 -1.1370E+00  0.0000E+00  1.4227E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.35     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.06     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.6600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  35

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000074
ci vector #   2dasum_wr=    0.000250
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000216     0.00000114     0.00000000     0.00000009     0.00000025     0.00000000
 sovl   7     0.00000863    -0.00000166     0.00000000    -0.00000035    -0.00000080     0.00000000     0.00000000
 sovl   8    -0.00003766    -0.00000064     0.00000000     0.00000138     0.00000331     0.00000000     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397454
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84357875
   ht   6     0.00070767    -0.00037297     0.00000001    -0.00002983    -0.00008143    -0.00000018
   ht   7    -0.00283380     0.00054531    -0.00000001     0.00011344     0.00026256     0.00000001    -0.00000010
   ht   8     0.01236526     0.00021081     0.00000000    -0.00045118    -0.00108632    -0.00000005     0.00000012    -0.00000056

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   2.134527E-10   2.802844E-10   5.674633E-10    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   8.382833E-06   3.638749E-05  -1.014372E-05   5.508513E-02  -3.388239E-02   7.003582E-02  -1.630177E-03  -0.995445    
   x:   2   1.805265E-06   9.244420E-08   1.094013E-06  -2.128876E-02   1.219037E-02  -2.409080E-02   0.999397      -4.924579E-03
   x:   3  -1.250245E-11   4.964285E-12  -3.744970E-11  -0.220210      -0.934702      -0.278999      -1.496840E-05  -3.029453E-07
   x:   4  -2.783533E-07  -1.340079E-06   3.846398E-07   0.946371      -0.136328      -0.290232       1.500615E-02   3.656554E-02
   x:   5  -6.986177E-07  -3.207358E-06   9.526961E-07   0.228915      -0.326268       0.912381       3.128246E-02   8.791343E-02
   x:   6  -2.623196E-02  -0.207342      -0.977917       5.068720E-11  -6.512322E-09   2.184233E-08   1.148338E-06   2.164903E-06
   x:   7   0.902050      -0.426520       6.623568E-02   1.194730E-11  -4.200659E-09   1.410756E-08  -1.704784E-06  -8.665710E-06
   x:   8   0.430835       0.880392      -0.198221        0.00000      -5.897280E-10   1.982068E-09  -4.562208E-07   3.783080E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970370      -1.175733E-07  -7.682380E-09   9.642389E-04  -0.109289       2.195729E-06  -8.601767E-06   3.769555E-05
 refs   2   3.245916E-09   0.959723       4.271860E-07   8.540818E-06  -4.194000E-03  -1.440847E-06  -3.353880E-06  -1.358399E-06
 refs   3  -5.389744E-11   7.049189E-09   -1.00000       3.301106E-09  -3.905654E-08   5.511453E-12  -4.604103E-11  -1.270552E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       5.200009E-06   5.087417E-06   4.415528E-04  -1.310903E-03  -0.556065        2.20083      -0.285711    
 civs   2  -1.268212E-09    1.00000       4.270717E-07   4.350670E-05  -2.663521E-04  -5.238005E-02   3.074884E-02   0.117120    
 civs   3  -2.341508E-13  -1.232807E-09   -1.00000       2.006144E-08  -8.311947E-08  -8.329534E-06   3.641728E-07  -1.669385E-06
 civs   4  -1.129934E-09  -2.076082E-07  -1.901128E-07   -1.00001       1.305869E-05   1.837373E-02  -8.070872E-02   1.223731E-02
 civs   5  -2.709127E-09  -3.580262E-07  -4.076558E-07  -1.492059E-05  -0.999871       5.419710E-02  -0.192574       2.872810E-02
 civs   6   3.846436E-04    1.07993       0.219561        31.4492       -164.061       -29030.6       -15655.7       -27458.3    
 civs   7  -7.355925E-04   0.987089       0.331377        40.3242       -241.468       -42562.1       -13911.4        49635.8    
 civs   8   6.064126E-04   0.302497       0.198474        19.1669       -80.7601       -22859.1        56151.1        5359.61    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370       1.587423E-07   2.286409E-07  -9.463911E-04   0.109220      -2.563545E-02   8.728188E-02  -1.109541E-02
 ref    2  -3.362568E-09   0.959718      -1.712966E-06  -1.733164E-04   5.093779E-03   0.165131       2.325655E-02  -2.190745E-02
 ref    3   5.415997E-11   8.238675E-09    1.00000      -2.528849E-08   1.334085E-07   1.041718E-05  -5.160376E-07  -8.355727E-07

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000016     0.00000023    -0.00094639     0.10921992    -0.02563545     0.08728188    -0.01109541
 ref:   2     0.00000000     0.95971814    -0.00000171    -0.00017332     0.00509378     0.16513107     0.02325655    -0.02190745
 ref:   3     0.00000000     0.00000001     1.00000000    -0.00000003     0.00000013     0.00001042    -0.00000052    -0.00000084

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6059E-06  1.0000E-05
 mr-sdci # 35  2   -227.6238632266  3.4390E-11  0.0000E+00  9.7067E-06  1.0000E-05
 mr-sdci # 35  3   -227.5320207590  1.3870E-11  8.0626E-02  4.0682E-01  1.0000E-05
 mr-sdci # 35  4   -227.3184225990  9.5719E-08  0.0000E+00  1.9338E-01  1.0000E-04
 mr-sdci # 35  5   -227.2580347748  1.5159E-06  0.0000E+00  3.2333E-01  1.0000E-04
 mr-sdci # 35  6   -226.9928789310  1.5495E-01  0.0000E+00  7.3824E-01  1.0000E-04
 mr-sdci # 35  7   -225.8989832306  1.0626E+00  0.0000E+00  9.7430E-01  1.0000E-04
 mr-sdci # 35  8   -224.8274619804 -6.6658E-02  0.0000E+00  1.4220E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.39     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.15     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.130001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.8100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1700s 

          starting ci iteration  36

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.202159
ci vector #   2dasum_wr=    8.539180
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00001531     0.00000056     0.00000000    -0.00000052    -0.00000097     0.04380697
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397487
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358705
   ht   6    -0.00502760    -0.00018514    -0.08062623     0.00016947     0.00031775   -14.29418329
Spectrum of overlapmatrix:    0.043807    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370       1.587423E-07   2.286409E-07  -9.463911E-04   0.109220       1.494466E-05
 refs   2  -3.362568E-09   0.959718      -1.712966E-06  -1.733164E-04   5.093779E-03   1.062507E-06
 refs   3   5.415997E-11   8.238675E-09    1.00000      -2.528849E-08   1.334085E-07   9.905894E-12

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -2.592690E-14  -1.456154E-05   3.343091E-12  -1.703270E-11  -7.169017E-05
 civs   2   2.794125E-14    1.00000      -5.396356E-07   2.597187E-13  -6.587163E-13  -2.641255E-06
 civs   3  -3.341836E-12   3.086971E-09   0.979989       8.241428E-08  -3.273627E-07  -0.199053    
 civs   4   1.077272E-16  -1.349704E-13   4.197922E-07    1.00000       2.684409E-12   2.480776E-06
 civs   5  -2.707380E-16  -2.862961E-14   6.470066E-07   1.861067E-12   -1.00000       4.829980E-06
 civs   6  -9.902531E-12   3.518217E-09   0.951036      -2.183351E-07   1.112449E-06    4.68219    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370       1.587423E-07   3.771705E-07  -9.463911E-04  -0.109220       8.875109E-07
 ref    2   3.362595E-09   0.959718      -1.182879E-06  -1.733164E-04  -5.093779E-03   2.805149E-06
 ref    3  -5.750181E-11   1.132565E-08   0.979989       5.712579E-08  -4.607713E-07  -0.199053    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97036999     0.00000016     0.00000038    -0.00094639    -0.10921992     0.00000089
 ref:   2     0.00000000     0.95971814    -0.00000118    -0.00017332    -0.00509378     0.00000281
 ref:   3     0.00000000     0.00000001     0.97998873     0.00000006    -0.00000046    -0.19905300
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -227.7749036773 -1.7053E-13  0.0000E+00  6.6059E-06  1.0000E-05
 mr-sdci # 36  2   -227.6238632266 -3.4106E-13  0.0000E+00  9.7067E-06  1.0000E-05
 mr-sdci # 36  3   -227.6102649974  7.8244E-02  1.0237E-02  1.1156E-01  1.0000E-05
 mr-sdci # 36  4   -227.3184225990 -1.7053E-13  0.0000E+00  1.9338E-01  1.0000E-04
 mr-sdci # 36  5   -227.2580347748  3.9790E-13  0.0000E+00  3.2333E-01  1.0000E-04
 mr-sdci # 36  6   -225.6355023516 -1.3574E+00  0.0000E+00  8.5742E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.15     0.12     0.01     0.15         0.    0.1775
    3    5    0     0.66     0.39     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  37

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.442281
ci vector #   2dasum_wr=    2.062139
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00001531     0.00000056     0.00000000    -0.00000052    -0.00000097     0.04380697
 sovl   7    -0.00015200    -0.00000020     0.05275587     0.00000543     0.00001285     0.00024483     0.01377785
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397487
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358705
   ht   6    -0.00502760    -0.00018514    -0.08062623     0.00016947     0.00031775   -14.29418329
   ht   7     0.04991133     0.00006436   -17.31010901    -0.00178082    -0.00421360    -0.09538789    -4.51054055
Spectrum of overlapmatrix:    0.010962    0.043809    1.000000    1.000000    1.000000    1.000000    1.002814

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370       1.587423E-07   2.286409E-07  -9.463911E-04   0.109220       1.494466E-05  -1.515362E-04
 refs   2  -3.362568E-09   0.959718      -1.712966E-06  -1.733164E-04   5.093779E-03   1.062507E-06   2.610808E-07
 refs   3   5.415997E-11   8.238675E-09    1.00000      -2.528849E-08   1.334085E-07   9.905894E-12   5.275587E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       1.305696E-11  -1.397138E-04  -9.090675E-10  -5.763180E-09   1.296455E-03   6.396060E-04
 civs   2   4.270724E-15    1.00000       4.753631E-07  -1.035749E-12  -5.575290E-12   7.475073E-07   3.186069E-06
 civs   3  -3.352833E-12   6.707250E-08  -0.914335       4.800510E-07   3.028533E-06  -0.645017      -3.340199E-02
 civs   4   1.264121E-16  -5.628231E-13   4.956790E-06   -1.00000       2.291610E-10  -4.701364E-05  -2.285576E-05
 civs   5  -2.324193E-16  -1.124124E-12   1.178579E-05   5.662756E-11   -1.00000      -1.142943E-04  -5.339395E-05
 civs   6  -1.047143E-11   8.208477E-08   -1.06715      -4.312579E-07  -3.441274E-06    1.69578       -4.33772    
 civs   7  -3.366171E-12   9.414050E-08   -1.02666      -6.024083E-06  -3.826190E-05    8.70004        3.77095    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370       1.587418E-07   5.126615E-06   9.463911E-04  -0.109220      -4.757292E-05  -2.142421E-05
 ref    2   3.362573E-09   0.959718       6.797160E-07   1.733164E-04  -5.093779E-03   5.321430E-06  -7.774057E-07
 ref    3  -5.769039E-11   8.027764E-08  -0.968497       1.875338E-07   8.765847E-07  -0.186039       0.165538    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97036999     0.00000016     0.00000513     0.00094639    -0.10921992    -0.00004757    -0.00002142
 ref:   2     0.00000000     0.95971814     0.00000068     0.00017332    -0.00509378     0.00000532    -0.00000078
 ref:   3     0.00000000     0.00000008    -0.96849717     0.00000019     0.00000088    -0.18603903     0.16553765
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6059E-06  1.0000E-05
 mr-sdci # 37  2   -227.6238632266  2.2737E-13  0.0000E+00  9.7067E-06  1.0000E-05
 mr-sdci # 37  3   -227.6208397672  1.0575E-02  2.0939E-03  5.8110E-02  1.0000E-05
 mr-sdci # 37  4   -227.3184225990 -1.1369E-13  0.0000E+00  1.9338E-01  1.0000E-04
 mr-sdci # 37  5   -227.2580347748  8.0718E-12  0.0000E+00  3.2333E-01  1.0000E-04
 mr-sdci # 37  6   -226.7979952566  1.1625E+00  0.0000E+00  7.1305E-01  1.0000E-04
 mr-sdci # 37  7   -225.4188704732 -4.8011E-01  0.0000E+00  7.7891E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.38     0.00     0.66         0.    0.7752
    4   42    0     0.01     0.00     0.00     0.01         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.059998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0900s 

          starting ci iteration  38

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.185768
ci vector #   2dasum_wr=    1.116976
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00001531     0.00000056     0.00000000    -0.00000052    -0.00000097     0.04380697
 sovl   7    -0.00015200    -0.00000020     0.05275587     0.00000543     0.00001285     0.00024483     0.01377785
 sovl   8    -0.00104113    -0.00000009    -0.00200242     0.00003759     0.00009133    -0.00091132     0.00004722     0.00197309
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.11757303
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397487
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358705
   ht   6    -0.00502760    -0.00018514    -0.08062623     0.00016947     0.00031775   -14.29418329
   ht   7     0.04991133     0.00006436   -17.31010901    -0.00178082    -0.00421360    -0.09538789    -4.51054055
   ht   8     0.34186710     0.00002909     0.65694880    -0.01232525    -0.02994229     0.29933677    -0.01347371    -0.64445965
Spectrum of overlapmatrix:    0.001945    0.010965    0.043829    1.000000    1.000000    1.000000    1.000001    1.002818

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370       1.587423E-07   2.286409E-07  -9.463911E-04   0.109220       1.494466E-05  -1.515362E-04  -1.040807E-03
 refs   2  -3.362568E-09   0.959718      -1.712966E-06  -1.733164E-04   5.093779E-03   1.062507E-06   2.610808E-07  -1.965352E-07
 refs   3   5.415997E-11   8.238675E-09    1.00000      -2.528849E-08   1.334085E-07   9.905894E-12   5.275587E-02  -2.002421E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000       8.633920E-10  -6.395193E-04  -6.731874E-08  -1.906852E-08   8.114382E-03   2.202151E-02   2.244645E-03
 civs   2   6.189835E-13    1.00000       1.225212E-07  -4.061125E-12  -6.176469E-12  -5.968175E-09   2.193773E-06  -2.935056E-06
 civs   3  -8.814457E-12  -5.057912E-07   0.900693      -1.616141E-06   2.580683E-06   0.642931      -0.179082       2.011579E-02
 civs   4   8.971840E-14  -3.090579E-11   2.285059E-05   -1.00000       7.397021E-10  -2.960681E-04  -7.969050E-04  -8.146449E-05
 civs   5   2.180977E-13  -7.592646E-11   5.652091E-05   5.854168E-09   -1.00000      -7.120916E-04  -1.932293E-03  -1.996909E-04
 civs   6   6.289738E-11  -6.246113E-07    1.08289       2.006680E-06  -2.826819E-06   -1.40479       0.584757        4.42337    
 civs   7   4.520879E-10  -7.525996E-07    1.18036       1.424793E-05  -3.362917E-05   -8.03048        3.62734       -3.46858    
 civs   8  -2.463084E-09   9.293772E-07  -0.770655      -6.670972E-05  -1.344699E-05    8.94556        20.6305        2.72741    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370       1.587092E-07   2.520758E-05   9.463937E-04  -0.109220      -3.180819E-04  -8.546277E-04  -9.058367E-05
 ref    2   3.363164E-09   0.959718       4.688795E-07   1.733164E-04  -5.093779E-03  -1.003032E-05  -9.778742E-06  -5.961001E-07
 ref    3  -3.419200E-11  -5.391176E-07   0.964507      -7.056096E-07   7.000646E-07   0.201363      -2.902916E-02  -0.168334    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97036999     0.00000016     0.00002521     0.00094639    -0.10921992    -0.00031808    -0.00085463    -0.00009058
 ref:   2     0.00000000     0.95971814     0.00000047     0.00017332    -0.00509378    -0.00001003    -0.00000978    -0.00000060
 ref:   3     0.00000000    -0.00000054     0.96450712    -0.00000071     0.00000070     0.20136304    -0.02902916    -0.16833376

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -227.7749036773  1.7053E-13  0.0000E+00  6.6059E-06  1.0000E-05
 mr-sdci # 38  2   -227.6238632266 -3.9790E-13  0.0000E+00  9.7066E-06  1.0000E-05
 mr-sdci # 38  3   -227.6224545899  1.6148E-03  6.8234E-04  3.0899E-02  1.0000E-05
 mr-sdci # 38  4   -227.3184225990  8.6970E-12  0.0000E+00  1.9338E-01  1.0000E-04
 mr-sdci # 38  5   -227.2580347748  5.6843E-13  0.0000E+00  3.2333E-01  1.0000E-04
 mr-sdci # 38  6   -226.9738975813  1.7590E-01  0.0000E+00  5.3852E-01  1.0000E-04
 mr-sdci # 38  7   -225.8527738567  4.3390E-01  0.0000E+00  1.2573E+00  1.0000E-04
 mr-sdci # 38  8   -225.4116667665  5.8420E-01  0.0000E+00  7.5185E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.6800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  39

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.116012
ci vector #   2dasum_wr=    0.637735
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00475706    -0.00000013     0.00099815    -0.00017205    -0.00042036     0.00083164
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20800686
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397487
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358705
   ht   6    -1.56203088     0.00004303    -0.32828202     0.05641735     0.13781270    -0.27175105
Spectrum of overlapmatrix:    0.000808    1.000000    1.000000    1.000000    1.000000    1.000024

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970370       1.587092E-07   2.520758E-05   9.463937E-04  -0.109220      -4.757415E-03
 refs   2   3.363164E-09   0.959718       4.688795E-07   1.733164E-04  -5.093779E-03  -5.564147E-08
 refs   3  -3.419200E-11  -5.391176E-07   0.964507      -7.056096E-07   7.000646E-07   3.392292E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000      -3.019203E-08   2.694991E-03  -2.438310E-06   3.317064E-06  -0.167351    
 civs   2   5.584790E-13    1.00000       3.002514E-06   5.784445E-11  -8.079954E-11   4.482338E-06
 civs   3  -9.304746E-11   3.067963E-06  -0.999305      -1.661967E-06   2.001608E-06  -5.121605E-02
 civs   4  -4.575055E-12   1.081684E-09  -9.655656E-05   -1.00000      -1.256491E-07   6.067330E-03
 civs   5  -1.128072E-11   2.678980E-09  -2.391322E-04   2.208515E-07   -1.00000       1.476834E-02
 civs   6  -2.675542E-08   6.346664E-06  -0.566524       5.125664E-04  -6.972927E-04    35.1795    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370       1.575986E-07   8.088969E-05  -9.464902E-04   0.109220      -6.579610E-03
 ref    2   3.363758E-09   0.959718       3.645897E-06  -1.733175E-04   5.093780E-03  -7.185535E-05
 ref    3  -1.248446E-10   2.420170E-06  -0.963856      -8.799817E-07   1.206846E-06  -4.820485E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97036999     0.00000016     0.00008089    -0.00094649     0.10922005    -0.00657961
 ref:   2     0.00000000     0.95971814     0.00000365    -0.00017332     0.00509378    -0.00007186
 ref:   3     0.00000000     0.00000242    -0.96385589    -0.00000088     0.00000121    -0.04820485
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -227.7749036773  0.0000E+00  0.0000E+00  6.6059E-06  1.0000E-05
 mr-sdci # 39  2   -227.6238632266 -1.1369E-13  0.0000E+00  9.7035E-06  1.0000E-05
 mr-sdci # 39  3   -227.6228412010  3.8661E-04  2.2393E-04  1.7413E-02  1.0000E-05
 mr-sdci # 39  4   -227.3184225993  2.5221E-10  0.0000E+00  1.9338E-01  1.0000E-04
 mr-sdci # 39  5   -227.2580347753  4.4281E-10  0.0000E+00  3.2333E-01  1.0000E-04
 mr-sdci # 39  6   -226.1316604546 -8.4224E-01  0.0000E+00  1.3943E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.09     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.46     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.06     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.009998
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  40

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.082101
ci vector #   2dasum_wr=    0.361199
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00475706    -0.00000013     0.00099815    -0.00017205    -0.00042036     0.00083164
 sovl   7     0.01504303     0.00000063    -0.00037160    -0.00054419    -0.00133109     0.00018874     0.00048824
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20800686
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397487
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358705
   ht   6    -1.56203088     0.00004303    -0.32828202     0.05641735     0.13781270    -0.27175105
   ht   7    -4.93953719    -0.00020761     0.12196056     0.17844520     0.43638816    -0.06156050    -0.15991783
Spectrum of overlapmatrix:    0.000236    0.000832    1.000000    1.000000    1.000000    1.000001    1.000251

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970370       1.587092E-07   2.520758E-05   9.463937E-04  -0.109220      -4.757415E-03  -1.504555E-02
 refs   2   3.363164E-09   0.959718       4.688795E-07   1.733164E-04  -5.093779E-03  -5.564147E-08   2.686094E-07
 refs   3  -3.419200E-11  -5.391176E-07   0.964507      -7.056096E-07   7.000646E-07   3.392292E-05   4.425274E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000      -1.264767E-06   1.038264E-02   1.327983E-04  -2.512920E-04  -0.661763       0.662231    
 civs   2   6.849083E-14    1.00000       1.865510E-05   7.217149E-09  -1.366574E-08  -3.814510E-05   2.048703E-05
 civs   3   1.895923E-10  -1.782282E-05   0.998419      -1.393242E-05   2.584733E-05   7.167461E-02   1.878207E-02
 civs   4  -1.976372E-10   4.530523E-08  -3.718863E-04   0.999995       9.543121E-06   2.409523E-02  -2.400226E-02
 civs   5  -4.885911E-10   1.125706E-07  -9.247832E-04  -1.215437E-05    1.00002       5.826922E-02  -5.850803E-02
 civs   6   9.679412E-08  -3.693323E-05   0.884539       3.169585E-03  -6.571545E-03   -27.6595       -23.6289    
 civs   7  -3.961769E-07   9.575603E-05  -0.969914      -9.830211E-03   1.878300E-02    52.7381       -36.5503    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370       1.083043E-07   4.355896E-04   9.516748E-04  -0.109230      -2.607070E-02   2.609037E-02
 ref    2   3.365573E-09   0.959718       2.270825E-05   1.733816E-04  -5.093898E-03  -3.135066E-04   3.050368E-04
 ref    3  -2.336469E-11  -1.768823E-05   0.962583      -1.838615E-05   3.371907E-05   9.153047E-02   1.139349E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97036999     0.00000011     0.00043559     0.00095167    -0.10922982    -0.02607070     0.02609037
 ref:   2     0.00000000     0.95971814     0.00002271     0.00017338    -0.00509390    -0.00031351     0.00030504
 ref:   3     0.00000000    -0.00001769     0.96258276    -0.00001839     0.00003372     0.09153047     0.00113935
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6057E-06  1.0000E-05
 mr-sdci # 40  2   -227.6238632266  1.6485E-12  0.0000E+00  9.4755E-06  1.0000E-05
 mr-sdci # 40  3   -227.6230584190  2.1722E-04  2.8442E-04  1.1953E-02  1.0000E-05
 mr-sdci # 40  4   -227.3184226134  1.4171E-08  0.0000E+00  1.9338E-01  1.0000E-04
 mr-sdci # 40  5   -227.2580348209  4.5593E-08  0.0000E+00  3.2330E-01  1.0000E-04
 mr-sdci # 40  6   -226.8509936485  7.1933E-01  0.0000E+00  8.1870E-01  1.0000E-04
 mr-sdci # 40  7   -225.7862054492 -6.6568E-02  0.0000E+00  1.4426E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.68     0.46     0.01     0.68         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.09     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.8000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  41

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.126138
ci vector #   2dasum_wr=    0.235363
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00475706    -0.00000013     0.00099815    -0.00017205    -0.00042036     0.00083164
 sovl   7     0.01504303     0.00000063    -0.00037160    -0.00054419    -0.00133109     0.00018874     0.00048824
 sovl   8     0.08028776     0.00000386     0.00143551    -0.00290479    -0.00710903     0.00045661     0.00135793     0.00758879
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20800686
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397487
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358705
   ht   6    -1.56203088     0.00004303    -0.32828202     0.05641735     0.13781270    -0.27175105
   ht   7    -4.93953719    -0.00020761     0.12196056     0.17844520     0.43638816    -0.06156050    -0.15991783
   ht   8   -26.36332450    -0.00126598    -0.47114605     0.95250137     2.33063254    -0.14992194    -0.44557054    -2.48990930
Spectrum of overlapmatrix:    0.000218    0.000801    0.001122    1.000000    1.000000    1.000000    1.000001    1.006766

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970370       1.587092E-07   2.520758E-05   9.463937E-04  -0.109220      -4.757415E-03  -1.504555E-02  -8.030574E-02
 refs   2   3.363164E-09   0.959718       4.688795E-07   1.733164E-04  -5.093779E-03  -5.564147E-08   2.686094E-07   2.272286E-06
 refs   3  -3.419200E-11  -5.391176E-07   0.964507      -7.056096E-07   7.000646E-07   3.392292E-05   4.425274E-04   7.305585E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000      -4.987000E-06   2.603829E-03  -4.887901E-04  -9.669185E-04  -0.226513       -2.45073       3.240645E-02
 civs   2   1.524469E-12    1.00000       4.071612E-06  -2.535048E-08  -4.929791E-08  -6.923970E-06  -1.196853E-04   1.380561E-05
 civs   3   4.076435E-10   4.072196E-06  -0.997841      -1.294127E-05  -3.182949E-05  -9.089298E-02  -3.029976E-02  -9.323469E-03
 civs   4  -9.844486E-10   1.786834E-07  -9.343329E-05  -0.999982       3.669210E-05   8.266509E-03   8.885936E-02  -1.178044E-03
 civs   5  -2.435556E-09   4.442953E-07  -2.325462E-04   4.490183E-05    1.00009       1.987450E-02   0.216614      -2.903788E-03
 civs   6  -2.169131E-08   8.638386E-06  -0.970135       1.095265E-03   3.678040E-03    26.0253       -3.42887        25.1839    
 civs   7  -8.373472E-09  -4.943468E-06    1.23057      -1.169470E-03  -6.720397E-03   -54.6184        14.0122        34.9279    
 civs   8  -3.382596E-07   6.252848E-05  -0.205514       6.242200E-03   1.308440E-02    11.5128        28.1022       -8.44001    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370      -3.842443E-08   7.824624E-05  -9.658740E-04  -0.109258      -8.955314E-03  -9.672988E-02   1.330833E-03
 ref    2   3.376094E-09   0.959718       4.525622E-06  -1.735526E-04  -5.094228E-03  -9.645081E-05  -1.135060E-03   1.663489E-05
 ref    3   1.074223E-10   3.432331E-06  -0.962063      -7.696398E-06  -2.328994E-05  -0.102543      -2.609492E-03   1.152401E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97036999    -0.00000004     0.00007825    -0.00096587    -0.10925809    -0.00895531    -0.09672988     0.00133083
 ref:   2     0.00000000     0.95971814     0.00000453    -0.00017355    -0.00509423    -0.00009645    -0.00113506     0.00001663
 ref:   3     0.00000000     0.00000343    -0.96206285    -0.00000770    -0.00002329    -0.10254344    -0.00260949     0.00115240

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 41  2   -227.6238632266  5.1159E-12  0.0000E+00  8.9050E-06  1.0000E-05
 mr-sdci # 41  3   -227.6231168737  5.8455E-05  5.4136E-05  8.6935E-03  1.0000E-05
 mr-sdci # 41  4   -227.3184226512  3.7800E-08  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 41  5   -227.2580349696  1.4876E-07  0.0000E+00  3.2321E-01  1.0000E-04
 mr-sdci # 41  6   -226.9903172825  1.3932E-01  0.0000E+00  6.8324E-01  1.0000E-04
 mr-sdci # 41  7   -225.9590833128  1.7288E-01  0.0000E+00  1.0041E+00  1.0000E-04
 mr-sdci # 41  8   -225.7709788556  3.5931E-01  0.0000E+00  1.4627E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.03     0.02     0.01     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  42

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.041893
ci vector #   2dasum_wr=    0.188128
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.01437247     0.00000072     0.00124405     0.00052085    -0.00127154     0.00028368
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20866915
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397492
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358724
   ht   6    -4.71935190    -0.00023677    -0.40836107    -0.17078773     0.41686627    -0.09302659
Spectrum of overlapmatrix:    0.000074    1.000000    1.000000    1.000000    1.000000    1.000210

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970370      -3.842443E-08   7.824624E-05  -9.658740E-04  -0.109258      -1.437589E-02
 refs   2   3.376094E-09   0.959718       4.525622E-06  -1.735526E-04  -5.094228E-03   3.678035E-07
 refs   3   1.074223E-10   3.432331E-06  -0.962063      -7.696398E-06  -2.328994E-05  -5.256819E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000       4.003309E-07   6.943364E-03  -2.129816E-05  -3.568305E-05   -1.67436    
 civs   2  -7.200773E-13   -1.00000       2.369863E-06  -1.083756E-09  -1.811434E-09  -8.427132E-05
 civs   3   1.595093E-10  -1.985732E-06  -0.999390      -2.106809E-06  -3.456793E-06  -0.149076    
 civs   4   9.356493E-11   1.449545E-08   2.514112E-04   0.999999      -1.298582E-06  -6.069023E-02
 civs   5  -2.286343E-10  -3.543363E-08  -6.145631E-04   1.889447E-06    1.00000       0.148111    
 civs   6  -1.797501E-07  -2.785406E-05  -0.483102       1.481872E-03   2.482736E-03    116.498    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370       5.408439E-08   1.960889E-04  -9.667160E-04  -0.109260      -6.614516E-02
 ref    2   3.376486E-09  -0.959718       6.609660E-07  -1.735626E-04  -5.094245E-03  -7.826845E-04
 ref    3   4.846029E-11  -1.507289E-06   0.961730      -6.448547E-06  -2.126948E-05   8.217635E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97036999     0.00000005     0.00019609    -0.00096672    -0.10925950    -0.06614516
 ref:   2     0.00000000    -0.95971814     0.00000066    -0.00017356    -0.00509424    -0.00078268
 ref:   3     0.00000000    -0.00000151     0.96173034    -0.00000645    -0.00002127     0.08217635
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -227.7749036773 -2.2737E-13  0.0000E+00  6.6049E-06  1.0000E-05
 mr-sdci # 42  2   -227.6238632266  1.7053E-13  0.0000E+00  8.8940E-06  1.0000E-05
 mr-sdci # 42  3   -227.6231430273  2.6154E-05  6.0803E-05  5.8079E-03  1.0000E-05
 mr-sdci # 42  4   -227.3184226514  1.9679E-10  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 42  5   -227.2580349702  5.2518E-10  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 42  6   -226.1022493442 -8.8807E-01  0.0000E+00  1.1934E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.45     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.01     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0900s 

          starting ci iteration  43

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.057980
ci vector #   2dasum_wr=    0.121827
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.01437247     0.00000072     0.00124405     0.00052085    -0.00127154     0.00028368
 sovl   7     0.03612990     0.00000174     0.00016332     0.00130961    -0.00319563     0.00059104     0.00153890
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20866915
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397492
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358724
   ht   6    -4.71935190    -0.00023677    -0.40836107    -0.17078773     0.41686627    -0.09302659
   ht   7   -11.86363070    -0.00057271    -0.05360387    -0.42942700     1.04766649    -0.19393715    -0.50491877
Spectrum of overlapmatrix:    0.000048    0.000247    1.000000    1.000000    1.000000    1.000001    1.001527

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970370      -3.842443E-08   7.824624E-05  -9.658740E-04  -0.109258      -1.437589E-02  -3.613855E-02
 refs   2   3.376094E-09   0.959718       4.525622E-06  -1.735526E-04  -5.094228E-03   3.678035E-07   1.014570E-06
 refs   3   1.074223E-10   3.432331E-06  -0.962063      -7.696398E-06  -2.328994E-05  -5.256819E-04  -7.796329E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000       1.238689E-07  -1.235077E-03  -6.187679E-06   5.951192E-05  -0.539886        2.41365    
 civs   2  -8.204404E-13   -1.00000      -3.306334E-06  -3.869466E-10   2.911190E-09  -2.238033E-05   1.181814E-04
 civs   3   2.301326E-10  -3.169980E-06   0.999029      -3.294133E-06   1.583123E-06   0.150779       7.742238E-02
 civs   4   2.991922E-11   4.466582E-09  -4.435041E-05    1.00000       2.159792E-06  -1.956758E-02   8.748828E-02
 civs   5  -7.340902E-11  -1.097863E-08   1.096160E-04   5.507119E-07   -1.00001       4.770941E-02  -0.213477    
 civs   6  -2.752798E-07  -4.442883E-05   0.803698       2.407310E-03  -9.900511E-04   -125.163       -54.9054    
 civs   7   8.657454E-08   1.424533E-05  -0.285527      -7.863659E-04  -1.253323E-03    64.7326       -44.9635    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.970370       4.307138E-08   2.936290E-05  -9.661190E-04   0.109260      -2.131075E-02   9.533905E-02
 ref    2   3.375663E-09  -0.959718       8.032803E-07  -1.735557E-04   5.094256E-03  -2.408043E-04   1.120284E-03
 ref    3  -3.676499E-11  -3.703618E-07  -0.961329      -5.179649E-06   2.326457E-05  -0.129732      -1.056309E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.97036999     0.00000004     0.00002936    -0.00096612     0.10926044    -0.02131075     0.09533905
 ref:   2     0.00000000    -0.95971814     0.00000080    -0.00017356     0.00509426    -0.00024080     0.00112028
 ref:   3     0.00000000    -0.00000037    -0.96132886    -0.00000518     0.00002326    -0.12973171    -0.01056309
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 43  2   -227.6238632266 -1.7053E-13  0.0000E+00  8.8883E-06  1.0000E-05
 mr-sdci # 43  3   -227.6231603883  1.7361E-05  1.3463E-05  4.3258E-03  1.0000E-05
 mr-sdci # 43  4   -227.3184226515  1.0027E-10  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 43  5   -227.2580349704  2.3977E-10  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 43  6   -226.4221723966  3.1992E-01  0.0000E+00  1.2001E+00  1.0000E-04
 mr-sdci # 43  7   -225.9478955961 -1.1188E-02  0.0000E+00  1.0353E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.38     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.05     0.07     0.01     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.060001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  44

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.021528
ci vector #   2dasum_wr=    0.081031
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.01437247     0.00000072     0.00124405     0.00052085    -0.00127154     0.00028368
 sovl   7     0.03612990     0.00000174     0.00016332     0.00130961    -0.00319563     0.00059104     0.00153890
 sovl   8     0.00536575     0.00000024    -0.00110157     0.00019450    -0.00047452     0.00008768     0.00021991     0.00004548
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20866915
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397492
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358724
   ht   6    -4.71935190    -0.00023677    -0.40836107    -0.17078773     0.41686627    -0.09302659
   ht   7   -11.86363070    -0.00057271    -0.05360387    -0.42942700     1.04766649    -0.19393715    -0.50491877
   ht   8    -1.76189896    -0.00007736     0.36154463    -0.06377794     0.15556693    -0.02878819    -0.07215917    -0.01490383
Spectrum of overlapmatrix:    0.000012    0.000048    0.000250    1.000000    1.000000    1.000000    1.000003    1.001555

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970370      -3.842443E-08   7.824624E-05  -9.658740E-04  -0.109258      -1.437589E-02  -3.613855E-02  -5.367166E-03
 refs   2   3.376094E-09   0.959718       4.525622E-06  -1.735526E-04  -5.094228E-03   3.678035E-07   1.014570E-06   9.896590E-08
 refs   3   1.074223E-10   3.432331E-06  -0.962063      -7.696398E-06  -2.328994E-05  -5.256819E-04  -7.796329E-04   1.553658E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000       1.234531E-07   2.041477E-03   2.574463E-06  -6.043045E-05  -2.541552E-02   -2.49409       9.080745E-04
 civs   2  -6.433245E-13   -1.00000       3.323352E-06   2.749379E-10  -2.939534E-09  -3.261840E-07  -1.202803E-04   8.308502E-06
 civs   3   1.962681E-10  -3.161392E-06  -0.999719       6.378974E-06  -7.671599E-07   5.863088E-02   1.484250E-03   0.377779    
 civs   4   3.121546E-11   4.451449E-09   7.372851E-05   -1.00000      -2.192095E-06  -9.031443E-04  -9.040286E-02   3.500092E-05
 civs   5  -7.656017E-11  -1.094192E-08  -1.809658E-04  -2.300996E-07    1.00001       2.259684E-03   0.220578      -1.256442E-04
 civs   6  -2.829251E-07  -4.431524E-05   -1.01650      -7.005744E-04   1.503060E-03    97.1810        22.3581       -95.7005    
 civs   7   9.394767E-08   1.414773E-05   0.474951      -5.071212E-04   8.766587E-04   -61.9026        54.8798        3.37462    
 civs   8  -3.580780E-08   4.304406E-07  -0.855762       4.811402E-03   1.333304E-03    161.248        35.4003        233.448    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.970370       4.305541E-08   2.547414E-06   9.659758E-04  -0.109260      -1.020139E-03  -9.851401E-02   3.693769E-05
 ref    2   3.375849E-09  -0.959718      -4.024715E-07   1.735538E-04  -5.094256E-03  -2.250529E-05  -1.156018E-03   1.645639E-06
 ref    3  -6.154758E-11  -3.779387E-07   0.960627       9.798349E-06  -2.195410E-05   0.191293      -9.716749E-04   4.692806E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97036999     0.00000004     0.00000255     0.00096598    -0.10926048    -0.00102014    -0.09851401     0.00003694
 ref:   2     0.00000000    -0.95971814    -0.00000040     0.00017355    -0.00509426    -0.00002251    -0.00115602     0.00000165
 ref:   3     0.00000000    -0.00000038     0.96062749     0.00000980    -0.00002195     0.19129316    -0.00097167     0.04692806

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 44  2   -227.6238632266  1.7053E-13  0.0000E+00  8.8883E-06  1.0000E-05
 mr-sdci # 44  3   -227.6231719095  1.1521E-05  5.7303E-06  2.9425E-03  1.0000E-05
 mr-sdci # 44  4   -227.3184226518  2.2379E-10  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 44  5   -227.2580349704  1.4893E-11  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 44  6   -226.9820945063  5.5992E-01  0.0000E+00  7.5614E-01  1.0000E-04
 mr-sdci # 44  7   -225.9756221483  2.7727E-02  0.0000E+00  9.9504E-01  1.0000E-04
 mr-sdci # 44  8   -225.1731148516 -5.9786E-01  0.0000E+00  1.3739E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.12     0.00     0.17         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.44     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.090000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1200s 

          starting ci iteration  45

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.010960
ci vector #   2dasum_wr=    0.057949
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00048783     0.00000001     0.00001980    -0.00001778    -0.00004326     0.00000581
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20872418
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358724
   ht   6    -0.16018466    -0.00000415    -0.00650468     0.00582885     0.01418288    -0.00189745
Spectrum of overlapmatrix:    0.000006    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.970370       4.305541E-08   2.547414E-06   9.659758E-04  -0.109260      -4.878977E-04
 refs   2   3.375849E-09  -0.959718      -4.024715E-07   1.735538E-04  -5.094256E-03  -3.912123E-09
 refs   3  -6.154758E-11  -3.779387E-07   0.960627       9.798349E-06  -2.195410E-05  -1.325629E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000       2.564107E-09   2.810919E-04  -1.904743E-06  -1.528882E-06  -0.206685    
 civs   2  -2.616474E-13   -1.00000       5.087897E-08  -5.010396E-11  -4.015448E-11  -5.372300E-06
 civs   3   1.811483E-12  -4.346849E-08  -0.999988      -1.507325E-07  -1.112429E-07  -9.749406E-03
 civs   4   1.787420E-12  -9.284521E-11  -1.018175E-05   -1.00000       5.737962E-08   7.540543E-03
 civs   5   4.369895E-12  -2.278258E-10  -2.496651E-05   1.705146E-07    1.00000       1.832151E-02
 civs   6   1.009261E-07  -5.256663E-06  -0.576207       3.904508E-03   3.134036E-03    423.680    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.970370      -4.295414E-08   8.537353E-06  -9.660510E-04  -0.109261      -8.146540E-03
 ref    2   3.376078E-09   0.959718       4.813109E-07  -1.735546E-04  -5.094257E-03  -8.852414E-05
 ref    3  -7.318655E-11   3.368785E-07  -0.960539      -1.046074E-05  -2.247642E-05  -6.553014E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97036999    -0.00000004     0.00000854    -0.00096605    -0.10926054    -0.00814654
 ref:   2     0.00000000     0.95971814     0.00000048    -0.00017355    -0.00509426    -0.00008852
 ref:   3     0.00000000     0.00000034    -0.96053925    -0.00001046    -0.00002248    -0.06553014
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -227.7749036773  2.8422E-13  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 45  2   -227.6238632266  2.8422E-13  0.0000E+00  8.8884E-06  1.0000E-05
 mr-sdci # 45  3   -227.6231752114  3.3019E-06  1.9016E-06  1.6126E-03  1.0000E-05
 mr-sdci # 45  4   -227.3184226519  1.2597E-10  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 45  5   -227.2580349705  7.7932E-11  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 45  6   -225.8380052759 -1.1441E+00  0.0000E+00  1.5237E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.10     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.06     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  46

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.007378
ci vector #   2dasum_wr=    0.031301
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00048783     0.00000001     0.00001980    -0.00001778    -0.00004326     0.00000581
 sovl   7     0.00155442    -0.00000007    -0.00009510    -0.00005638    -0.00013744     0.00000131     0.00000449
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20872418
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358724
   ht   6    -0.16018466    -0.00000415    -0.00650468     0.00582885     0.01418288    -0.00189745
   ht   7    -0.51040871     0.00002183     0.03121199     0.01848813     0.04505782    -0.00042805    -0.00146982
Spectrum of overlapmatrix:    0.000002    0.000006    1.000000    1.000000    1.000000    1.000000    1.000003

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.970370       4.305541E-08   2.547414E-06   9.659758E-04  -0.109260      -4.878977E-04  -1.554842E-03
 refs   2   3.375849E-09  -0.959718      -4.024715E-07   1.735538E-04  -5.094256E-03  -3.912123E-09   2.670118E-08
 refs   3  -6.154758E-11  -3.779387E-07   0.960627       9.798349E-06  -2.195410E-05  -1.325629E-04   6.844272E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -8.764361E-08   9.321774E-04   9.929636E-07   4.417560E-05  -0.733122       0.804905    
 civs   2   2.760110E-13   -1.00000      -2.917735E-07  -2.020736E-10  -2.208253E-09   4.185733E-05  -2.448487E-05
 civs   3  -1.278921E-11  -2.159691E-07   0.999898      -3.830363E-07  -3.298606E-06   6.230553E-02  -3.372761E-02
 civs   4  -8.021961E-12   3.177822E-09  -3.380773E-05   -1.00000      -1.601490E-06   2.655843E-02  -2.922687E-02
 civs   5  -1.957973E-11   7.752879E-09  -8.237218E-05  -8.704334E-08   -1.00000       6.473698E-02  -7.119075E-02
 civs   6  -6.244521E-08  -2.686256E-05   0.865337       4.708370E-03   9.121135E-03   -309.586       -297.625    
 civs   7  -1.227055E-07   6.481389E-05  -0.871270      -2.116458E-03  -3.128196E-02    568.798       -424.413    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -4.652240E-08   3.944891E-05  -9.659363E-04   0.109262      -2.899159E-02   3.179969E-02
 ref    2  -3.376019E-09   0.959718       2.647016E-07  -1.735532E-04   5.094277E-03  -3.489775E-04   3.709384E-04
 ref    3   5.670035E-11   1.744772E-07   0.960408      -1.080494E-05   1.736221E-05   0.104784       4.150797E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999    -0.00000005     0.00003945    -0.00096594     0.10926223    -0.02899159     0.03179969
 ref:   2     0.00000000     0.95971814     0.00000026    -0.00017355     0.00509428    -0.00034898     0.00037094
 ref:   3     0.00000000     0.00000017     0.96040841    -0.00001080     0.00001736     0.10478385     0.00415080
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 46  2   -227.6238632266 -5.6843E-14  0.0000E+00  8.8884E-06  1.0000E-05
 mr-sdci # 46  3   -227.6231768682  1.6568E-06  2.4375E-06  1.1387E-03  1.0000E-05
 mr-sdci # 46  4   -227.3184226519  6.8212E-12  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 46  5   -227.2580349718  1.2905E-09  0.0000E+00  3.2319E-01  1.0000E-04
 mr-sdci # 46  6   -226.7718425944  9.3384E-01  0.0000E+00  9.1731E-01  1.0000E-04
 mr-sdci # 46  7   -225.3180894956 -6.5753E-01  0.0000E+00  1.5130E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.13     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.12     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.01     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.05     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  47

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.011788
ci vector #   2dasum_wr=    0.021998
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00048783     0.00000001     0.00001980    -0.00001778    -0.00004326     0.00000581
 sovl   7     0.00155442    -0.00000007    -0.00009510    -0.00005638    -0.00013744     0.00000131     0.00000449
 sovl   8     0.00725727    -0.00000035     0.00014256    -0.00026309    -0.00064188     0.00000433     0.00001271     0.00006213
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20872418
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358724
   ht   6    -0.16018466    -0.00000415    -0.00650468     0.00582885     0.01418288    -0.00189745
   ht   7    -0.51040871     0.00002183     0.03121199     0.01848813     0.04505782    -0.00042805    -0.00146982
   ht   8    -2.38300053     0.00011476    -0.04678828     0.08626974     0.21043585    -0.00142085    -0.00417136    -0.02038425
Spectrum of overlapmatrix:    0.000002    0.000005    0.000009    1.000000    1.000000    1.000000    1.000000    1.000056

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1  -0.970370       4.305541E-08   2.547414E-06   9.659758E-04  -0.109260      -4.878977E-04  -1.554842E-03  -7.259079E-03
 refs   2   3.375849E-09  -0.959718      -4.024715E-07   1.735538E-04  -5.094256E-03  -3.912123E-09   2.670118E-08   2.025129E-07
 refs   3  -6.154758E-11  -3.779387E-07   0.960627       9.798349E-06  -2.195410E-05  -1.325629E-04   6.844272E-06   5.774094E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   -1.00000      -2.151533E-08  -3.006362E-04   5.529514E-06  -1.800048E-05   0.310214       -2.43094       4.589281E-02
 civs   2   9.042716E-13    1.00000      -3.043740E-07  -4.104680E-10   1.016765E-09  -9.334479E-06   1.178430E-04   1.272531E-05
 civs   3   2.411580E-13   2.813323E-07   0.999835      -8.215566E-08   5.179968E-06   8.498481E-02  -2.491400E-02  -4.864359E-02
 civs   4  -1.871274E-11   7.803200E-10   1.089273E-05   -1.00000       6.530110E-07  -1.125930E-02   8.812482E-02  -1.713068E-03
 civs   5  -4.566311E-11   1.903012E-09   2.667931E-05  -4.879471E-07    1.00000      -2.755359E-02   0.214979      -4.063070E-03
 civs   6  -8.125100E-08   3.540583E-05   0.961164       4.096415E-03  -1.324177E-02   -286.727       -60.2338       -314.999    
 civs   7  -6.096680E-08  -9.044486E-05   -1.16003      -5.722011E-04   4.131463E-02    584.938        175.240       -426.034    
 civs   8  -5.258994E-08   1.995697E-05   0.225281      -9.147292E-04  -5.478628E-03   -148.758        301.481        106.102    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370       4.221060E-08  -9.242303E-06  -9.657572E-04  -0.109261       1.223046E-02  -9.604862E-02   1.812156E-03
 ref    2  -3.376500E-09  -0.959718      -2.334258E-07  -1.735511E-04  -5.094265E-03   1.339511E-04  -1.126988E-03   1.955168E-05
 ref    3   6.909706E-11  -1.118433E-07   0.960347      -1.047703E-05  -1.525631E-05   0.115063       2.655062E-03  -1.760553E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004    -0.00000924    -0.00096576    -0.10926119     0.01223046    -0.09604862     0.00181216
 ref:   2     0.00000000    -0.95971814    -0.00000023    -0.00017355    -0.00509426     0.00013395    -0.00112699     0.00001955
 ref:   3     0.00000000    -0.00000011     0.96034679    -0.00001048    -0.00001526     0.11506265     0.00265506    -0.00176055

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 47  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 47  2   -227.6238632266  0.0000E+00  0.0000E+00  8.8879E-06  1.0000E-05
 mr-sdci # 47  3   -227.6231774173  5.4913E-07  6.4275E-07  9.3391E-04  1.0000E-05
 mr-sdci # 47  4   -227.3184226519  6.3665E-12  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 47  5   -227.2580349720  2.0128E-10  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 47  6   -226.9591740081  1.8733E-01  0.0000E+00  7.7813E-01  1.0000E-04
 mr-sdci # 47  7   -225.9511342411  6.3304E-01  0.0000E+00  9.9704E-01  1.0000E-04
 mr-sdci # 47  8   -225.2464136423  7.3299E-02  0.0000E+00  1.5339E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.12     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.37     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.08     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  48

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.004741
ci vector #   2dasum_wr=    0.019223
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00169665     0.00000009     0.00010027    -0.00006146     0.00015011     0.00000385
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20872969
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6    -0.55711237    -0.00002798    -0.03290973     0.02015297    -0.04921414    -0.00126326
Spectrum of overlapmatrix:    0.000001    1.000000    1.000000    1.000000    1.000000    1.000003

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370       4.221060E-08  -9.242303E-06  -9.657572E-04  -0.109261       1.697072E-03
 refs   2  -3.376500E-09  -0.959718      -2.334258E-07  -1.735511E-04  -5.094265E-03  -4.276269E-08
 refs   3   6.909706E-11  -1.118433E-07   0.960347      -1.047703E-05  -1.525631E-05   2.435030E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000      -6.344467E-08  -7.627014E-04  -1.024668E-05   1.442836E-05   -1.75273    
 civs   2   1.614971E-12    1.00000      -7.338966E-08  -5.158433E-10   7.263927E-10  -8.811462E-05
 civs   3   7.478527E-11   3.129715E-08   0.999955      -6.182969E-07   8.676588E-07  -0.104018    
 civs   4  -4.781690E-11   2.297470E-09   2.761815E-05   -1.00000      -5.236341E-07   6.349729E-02
 civs   5   1.169169E-10  -5.614309E-09  -6.749307E-05  -9.075255E-07    1.00000      -0.155068    
 civs   6  -7.787492E-07   3.739320E-05   0.449534       6.039362E-03  -8.504033E-03    1033.05    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370       4.471566E-08   2.089483E-05   9.661621E-04  -0.109262       6.925298E-02
 ref    2  -3.378604E-09  -0.959718       1.568318E-07   1.735559E-04  -5.094272E-03   8.193585E-04
 ref    3   1.219528E-10  -8.087659E-08   0.960314       1.003032E-05  -1.463015E-05  -7.473666E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036999     0.00000004     0.00002089     0.00096616    -0.10926176     0.06925298
 ref:   2     0.00000000    -0.95971814     0.00000016     0.00017356    -0.00509427     0.00081936
 ref:   3     0.00000000    -0.00000008     0.96031436     0.00001003    -0.00001463    -0.07473666
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 48  1   -227.7749036773 -5.6843E-14  0.0000E+00  6.6049E-06  1.0000E-05
 mr-sdci # 48  2   -227.6238632266 -6.2528E-13  0.0000E+00  8.8876E-06  1.0000E-05
 mr-sdci # 48  3   -227.6231777062  2.8894E-07  6.9947E-07  6.3519E-04  1.0000E-05
 mr-sdci # 48  4   -227.3184226519  4.1553E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 48  5   -227.2580349721  7.8785E-11  0.0000E+00  3.2319E-01  1.0000E-04
 mr-sdci # 48  6   -226.0972691680 -8.6190E-01  0.0000E+00  1.1471E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.11     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.46     0.00     0.67         0.    0.7752
    4   42    0     0.01     0.00     0.01     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  49

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.006168
ci vector #   2dasum_wr=    0.013039
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00169665     0.00000009     0.00010027    -0.00006146     0.00015011     0.00000385
 sovl   7    -0.00384325    -0.00000019    -0.00000929     0.00013930    -0.00033993    -0.00000742     0.00001742
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20872969
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6    -0.55711237    -0.00002798    -0.03290973     0.02015297    -0.04921414    -0.00126326
   ht   7     1.26197167     0.00006095     0.00304987    -0.04567620     0.11144437     0.00243478    -0.00571631
Spectrum of overlapmatrix:    0.000001    0.000003    1.000000    1.000000    1.000000    1.000000    1.000018

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370       4.221060E-08  -9.242303E-06  -9.657572E-04  -0.109261       1.697072E-03  -3.844211E-03
 refs   2  -3.376500E-09  -0.959718      -2.334258E-07  -1.735511E-04  -5.094265E-03  -4.276269E-08   1.083550E-07
 refs   3   6.909706E-11  -1.118433E-07   0.960347      -1.047703E-05  -1.525631E-05   2.435030E-05  -7.114516E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000      -2.528635E-08  -1.631358E-04  -5.610923E-06   1.046740E-05   0.477189        2.42979    
 civs   2   1.096401E-12    1.00000      -6.500844E-08  -3.011800E-10   4.253003E-10   1.928796E-05   1.190055E-04
 civs   3   9.023463E-11   4.889022E-08   0.999925      -8.773923E-07  -2.286324E-06  -0.108595       5.250501E-02
 civs   4  -3.567815E-11   9.135603E-10   5.871874E-06   -1.00000      -3.782720E-07  -1.732856E-02  -8.805386E-02
 civs   5   8.732796E-11  -2.241325E-09  -1.449536E-05  -4.985035E-07  -0.999999       4.217336E-02   0.214936    
 civs   6  -9.556070E-07   5.820140E-05   0.776274       8.789831E-03   2.360574E-02    1134.95       -482.956    
 civs   7  -1.651649E-07   1.911456E-05   0.300248       2.420433E-03   1.314461E-02    625.199        419.016    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370       4.320862E-08  -2.789559E-06   9.659791E-04   0.109261      -1.884922E-02  -9.600073E-02
 ref    2  -3.377968E-09  -0.959718      -9.885595E-08   1.735538E-04   5.094260E-03  -2.111121E-04  -1.127835E-03
 ref    3   1.442340E-10  -6.483440E-08   0.960272       9.676265E-06   1.270027E-05  -0.121133       8.849584E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999     0.00000004    -0.00000279     0.00096598     0.10926078    -0.01884922    -0.09600073
 ref:   2     0.00000000    -0.95971814    -0.00000010     0.00017355     0.00509426    -0.00021111    -0.00112783
 ref:   3     0.00000000    -0.00000006     0.96027207     0.00000968     0.00001270    -0.12113289     0.00884958
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 49  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6049E-06  1.0000E-05
 mr-sdci # 49  2   -227.6238632266  2.8422E-13  0.0000E+00  8.8873E-06  1.0000E-05
 mr-sdci # 49  3   -227.6231779163  2.1001E-07  1.4640E-07  4.4358E-04  1.0000E-05
 mr-sdci # 49  4   -227.3184226519  1.0743E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 49  5   -227.2580349724  2.8786E-10  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 49  6   -226.4171127823  3.1984E-01  0.0000E+00  1.1730E+00  1.0000E-04
 mr-sdci # 49  7   -225.9536003778  2.4661E-03  0.0000E+00  1.0203E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.060001
time for eigenvalue solver             0.009998
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  50

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.002203
ci vector #   2dasum_wr=    0.008000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00169665     0.00000009     0.00010027    -0.00006146     0.00015011     0.00000385
 sovl   7    -0.00384325    -0.00000019    -0.00000929     0.00013930    -0.00033993    -0.00000742     0.00001742
 sovl   8     0.00051085     0.00000002    -0.00009074    -0.00001855     0.00004516     0.00000099    -0.00000223     0.00000043
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20872969
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6    -0.55711237    -0.00002798    -0.03290973     0.02015297    -0.04921414    -0.00126326
   ht   7     1.26197167     0.00006095     0.00304987    -0.04567620     0.11144437     0.00243478    -0.00571631
   ht   8    -0.16774268    -0.00000717     0.02978269     0.00608261    -0.01480530    -0.00032462     0.00073095    -0.00014147
Spectrum of overlapmatrix:    0.000000    0.000001    0.000003    1.000000    1.000000    1.000000    1.000000    1.000018

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370       4.221060E-08  -9.242303E-06  -9.657572E-04  -0.109261       1.697072E-03  -3.844211E-03   5.109831E-04
 refs   2  -3.376500E-09  -0.959718      -2.334258E-07  -1.735511E-04  -5.094265E-03  -4.276269E-08   1.083550E-07  -8.846441E-09
 refs   3   6.909706E-11  -1.118433E-07   0.960347      -1.047703E-05  -1.525631E-05   2.435030E-05  -7.114516E-05  -1.344626E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000      -2.059445E-08  -2.415108E-04   3.381196E-06   6.533121E-07  -3.013853E-02    2.49345       9.074248E-03
 civs   2   1.192143E-12    1.00000      -5.624813E-08   2.455988E-10  -1.525879E-10   2.405424E-08   1.203185E-04   9.610622E-06
 civs   3   3.528941E-10   3.458743E-08   0.999983       2.624569E-06  -6.683040E-06   5.660111E-02  -1.535340E-03   0.287671    
 civs   4  -2.298132E-11   7.415341E-10   8.745129E-06    1.00000      -2.042231E-08   1.106880E-03  -9.037913E-02  -2.300344E-04
 civs   5   5.662449E-11  -1.825242E-09  -2.144603E-05   2.946353E-07    1.00000      -2.576580E-03   0.220547       9.028982E-04
 civs   6  -2.574589E-07   4.670271E-05   0.968465       6.284319E-04  -7.830107E-02    883.063       -219.643       -849.720    
 civs   7   5.263541E-07   8.549111E-06   0.476855       5.027498E-03  -5.513810E-02    587.858        510.244       -75.4840    
 civs   8   3.562566E-06  -5.048146E-05   0.843774       2.911723E-02  -0.156041        1548.75       -312.815        2236.47    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370       4.302285E-08   3.136162E-07  -9.658901E-04  -0.109261       1.189710E-03  -9.851555E-02  -3.574444E-04
 ref    2  -3.377892E-09  -0.959718      -6.891360E-08  -1.735526E-04  -5.094266E-03   2.513180E-05  -1.155873E-03  -5.477893E-06
 ref    3  -1.147513E-10  -7.131049E-08   0.960207      -1.221409E-05   1.323485E-06  -0.174213      -1.064766E-03  -3.977817E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000031    -0.00096589    -0.10926122     0.00118971    -0.09851555    -0.00035744
 ref:   2     0.00000000    -0.95971814    -0.00000007    -0.00017355    -0.00509427     0.00002513    -0.00115587    -0.00000548
 ref:   3     0.00000000    -0.00000007     0.96020714    -0.00001221     0.00000132    -0.17421250    -0.00106477    -0.03977817

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 50  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 50  2   -227.6238632266 -5.6843E-14  0.0000E+00  8.8872E-06  1.0000E-05
 mr-sdci # 50  3   -227.6231780398  1.2353E-07  7.7822E-08  3.4193E-04  1.0000E-05
 mr-sdci # 50  4   -227.3184226520  9.1973E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 50  5   -227.2580349747  2.2988E-09  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 50  6   -226.9603367755  5.4322E-01  0.0000E+00  8.3803E-01  1.0000E-04
 mr-sdci # 50  7   -225.9754540621  2.1854E-02  0.0000E+00  9.9503E-01  1.0000E-04
 mr-sdci # 50  8   -225.2245001992 -2.1913E-02  0.0000E+00  1.2578E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.020000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  51

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001290
ci vector #   2dasum_wr=    0.006408
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00005850     0.00000000     0.00001295     0.00000217    -0.00000518     0.00000008
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873031
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6     0.01920935    -0.00000042    -0.00425036    -0.00071156     0.00169734    -0.00002479
Spectrum of overlapmatrix:    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370       4.302285E-08   3.136162E-07  -9.658901E-04  -0.109261      -5.851211E-05
 refs   2  -3.377892E-09  -0.959718      -6.891360E-08  -1.735526E-04  -5.094266E-03  -2.050015E-10
 refs   3  -1.147513E-10  -7.131049E-08   0.960207      -1.221409E-05   1.323485E-06  -4.919882E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000       2.531692E-09  -3.392468E-05  -8.591179E-07  -3.134367E-06   0.217543    
 civs   2   1.391523E-12   -1.00000      -4.178539E-09   1.829708E-11   6.714402E-11  -4.706262E-06
 civs   3   5.763459E-11   4.352340E-09  -0.999992       1.939270E-07   7.052527E-07  -4.831192E-02
 civs   4   1.003923E-11  -9.357037E-11   1.255271E-06   -1.00000       1.177286E-07  -8.073503E-03
 civs   5  -2.404387E-11   2.245959E-10  -3.011502E-06  -7.743703E-08   -1.00000       1.923811E-02
 civs   6  -4.634096E-06   4.325250E-05  -0.579901      -1.468557E-02  -5.357817E-02    3718.63    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370      -4.312142E-08   1.025948E-06   9.659242E-04   0.109261      -8.581979E-03
 ref    2  -3.379106E-09   0.959718       8.816584E-08   1.735530E-04   5.094268E-03  -9.284591E-05
 ref    3  -3.661113E-11   7.527684E-08  -0.960197       1.247255E-05  -3.826995E-07  -6.468455E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036999    -0.00000004     0.00000103     0.00096592     0.10926134    -0.00858198
 ref:   2     0.00000000     0.95971814     0.00000009     0.00017355     0.00509427    -0.00009285
 ref:   3     0.00000000     0.00000008    -0.96019707     0.00001247    -0.00000038    -0.06468455
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 51  1   -227.7749036773  2.8422E-13  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 51  2   -227.6238632266  1.7053E-13  0.0000E+00  8.8872E-06  1.0000E-05
 mr-sdci # 51  3   -227.6231780849  4.5129E-08  2.5728E-08  1.8474E-04  1.0000E-05
 mr-sdci # 51  4   -227.3184226521  2.5125E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 51  5   -227.2580349750  3.0985E-10  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 51  6   -225.7674536432 -1.1929E+00  0.0000E+00  1.5238E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.17         0.    0.2041
    2    1    0     0.13     0.09     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.47     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  52

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000855
ci vector #   2dasum_wr=    0.003391
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00005850     0.00000000     0.00001295     0.00000217    -0.00000518     0.00000008
 sovl   7    -0.00018803    -0.00000001    -0.00001187     0.00000685    -0.00001662     0.00000002     0.00000006
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873031
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6     0.01920935    -0.00000042    -0.00425036    -0.00071156     0.00169734    -0.00002479
   ht   7     0.06174147     0.00000264     0.00389514    -0.00224651     0.00544901    -0.00000529    -0.00002072
Spectrum of overlapmatrix:    0.000000    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370       4.302285E-08   3.136162E-07  -9.658901E-04  -0.109261      -5.851211E-05  -1.880785E-04
 refs   2  -3.377892E-09  -0.959718      -6.891360E-08  -1.735526E-04  -5.094266E-03  -2.050015E-10   3.202120E-09
 refs   3  -1.147513E-10  -7.131049E-08   0.960207      -1.221409E-05   1.323485E-06  -4.919882E-06   7.916910E-07

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000       1.367163E-08   1.059972E-04   1.506741E-06  -7.918945E-05   0.744090      -0.861644    
 civs   2  -2.339354E-13   -1.00000       5.323428E-09   1.503709E-11  -3.873365E-09   4.182783E-05  -2.688885E-05
 civs   3  -8.785925E-11   2.939458E-09  -0.999979      -1.278813E-07  -7.153560E-06   9.127601E-02  -1.022705E-02
 civs   4   6.713607E-11  -4.986656E-10  -3.831984E-06    1.00000       2.882393E-06  -2.700838E-02   3.150037E-02
 civs   5  -1.629746E-10   1.210985E-09   9.375829E-06   1.352427E-07   -1.00001       6.568199E-02  -7.615400E-02
 civs   6  -1.757532E-06   2.142238E-05  -0.854125       1.319604E-02   0.127873       -2648.20       -2648.30    
 civs   7  -9.244957E-06   6.604581E-05   0.829466       3.907691E-03  -0.460939        4781.23       -3758.54    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -4.356341E-08  -4.505978E-06  -9.659498E-04   0.109264      -2.940190E-02   3.403463E-02
 ref    2  -3.376878E-09   0.959718       1.953604E-08  -1.735533E-04   5.094304E-03  -3.542129E-04   3.967987E-04
 ref    3  -1.977877E-10   7.407988E-08  -0.960182      -1.239871E-05  -9.186469E-06   0.104458       2.331653E-04

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999    -0.00000004    -0.00000451    -0.00096595     0.10926435    -0.02940190     0.03403463
 ref:   2     0.00000000     0.95971814     0.00000002    -0.00017355     0.00509430    -0.00035421     0.00039680
 ref:   3     0.00000000     0.00000007    -0.96018218    -0.00001240    -0.00000919     0.10445837     0.00023317
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 52  1   -227.7749036773 -2.2737E-13  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 52  2   -227.6238632266 -5.6843E-14  0.0000E+00  8.8872E-06  1.0000E-05
 mr-sdci # 52  3   -227.6231781063  2.1341E-08  3.2228E-08  1.3453E-04  1.0000E-05
 mr-sdci # 52  4   -227.3184226521 -3.9790E-13  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 52  5   -227.2580349790  4.0299E-09  0.0000E+00  3.2319E-01  1.0000E-04
 mr-sdci # 52  6   -226.7611800987  9.9373E-01  0.0000E+00  9.5039E-01  1.0000E-04
 mr-sdci # 52  7   -225.1533726306 -8.2208E-01  0.0000E+00  1.4611E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.12     0.01     0.17         0.    0.2041
    2    1    0     0.13     0.09     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.47     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.01     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.06     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  53

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001330
ci vector #   2dasum_wr=    0.002659
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00005850     0.00000000     0.00001295     0.00000217    -0.00000518     0.00000008
 sovl   7    -0.00018803    -0.00000001    -0.00001187     0.00000685    -0.00001662     0.00000002     0.00000006
 sovl   8     0.00082810     0.00000004    -0.00000985    -0.00003004     0.00007324    -0.00000006    -0.00000018     0.00000081
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873031
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6     0.01920935    -0.00000042    -0.00425036    -0.00071156     0.00169734    -0.00002479
   ht   7     0.06174147     0.00000264     0.00389514    -0.00224651     0.00544901    -0.00000529    -0.00002072
   ht   8    -0.27191481    -0.00001310     0.00323230     0.00984867    -0.02401029     0.00001965     0.00005761    -0.00026543
Spectrum of overlapmatrix:    0.000000    0.000000    0.000000    1.000000    1.000000    1.000000    1.000000    1.000001

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370       4.302285E-08   3.136162E-07  -9.658901E-04  -0.109261      -5.851211E-05  -1.880785E-04   8.283074E-04
 refs   2  -3.377892E-09  -0.959718      -6.891360E-08  -1.735526E-04  -5.094266E-03  -2.050015E-10   3.202120E-09  -2.317574E-08
 refs   3  -1.147513E-10  -7.131049E-08   0.960207      -1.221409E-05   1.323485E-06  -4.919882E-06   7.916910E-07   6.916773E-07

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000      -7.261071E-09   3.724131E-05  -2.565417E-06  -3.803115E-05  -0.331887       -2.42772      -6.232423E-02
 civs   2   8.586090E-13   -1.00000      -1.167956E-10  -1.738470E-10  -1.982358E-09  -1.102864E-05  -1.177265E-04   1.236349E-05
 civs   3  -6.194854E-11   2.361864E-09   0.999972       1.487820E-07  -1.025919E-05   0.103299       1.696172E-04  -1.877440E-02
 civs   4   8.848263E-11   2.599853E-10  -1.359451E-06    1.00000       1.391950E-06   1.200059E-02   8.807401E-02   2.519156E-03
 civs   5  -2.150818E-10  -6.405724E-10   3.293342E-06  -2.243505E-07   -1.00000      -2.949772E-02  -0.214664      -5.466543E-03
 civs   6  -2.080400E-06   7.379448E-06   0.950299       8.425573E-03   0.184129       -2434.56        559.722       -2808.26    
 civs   7  -8.173343E-06   1.085376E-04   -1.12037       1.585875E-02  -0.596701        4908.66       -1546.90       -3829.65    
 civs   8   9.319068E-07   3.393558E-05  -0.232231       7.294100E-03  -7.655428E-02    1343.36        2619.98       -992.694    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370      -4.273532E-08  -1.152759E-06  -9.657890E-04   0.109263       1.311297E-02   9.591927E-02   2.453370E-03
 ref    2  -3.377683E-09   0.959718      -8.374117E-08  -1.735514E-04   5.094285E-03   1.438487E-04   1.125476E-03   2.686599E-05
 ref    3  -1.698270E-10   7.365146E-08   0.960175      -1.209508E-05  -1.260570E-05   0.115981      -2.004741E-03  -7.929563E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999    -0.00000004    -0.00000115    -0.00096579     0.10926272     0.01311297     0.09591927     0.00245337
 ref:   2     0.00000000     0.95971814    -0.00000008    -0.00017355     0.00509428     0.00014385     0.00112548     0.00002687
 ref:   3     0.00000000     0.00000007     0.96017460    -0.00001210    -0.00001261     0.11598117    -0.00200474    -0.00792956

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 53  1   -227.7749036773  5.6843E-14  0.0000E+00  6.6050E-06  1.0000E-05
 mr-sdci # 53  2   -227.6238632266  0.0000E+00  0.0000E+00  8.8872E-06  1.0000E-05
 mr-sdci # 53  3   -227.6231781137  7.4841E-09  9.5590E-09  1.1624E-04  1.0000E-05
 mr-sdci # 53  4   -227.3184226521  5.6843E-12  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 53  5   -227.2580349795  4.9778E-10  0.0000E+00  3.2319E-01  1.0000E-04
 mr-sdci # 53  6   -226.9615282323  2.0035E-01  0.0000E+00  8.0584E-01  1.0000E-04
 mr-sdci # 53  7   -225.9470206790  7.9365E-01  0.0000E+00  9.9983E-01  1.0000E-04
 mr-sdci # 53  8   -225.0512233985 -1.7328E-01  0.0000E+00  1.4663E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.10     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.47     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7800s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.1100s 

          starting ci iteration  54

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000583
ci vector #   2dasum_wr=    0.002352
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00021222    -0.00000001     0.00000501    -0.00000767    -0.00001878     0.00000006
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873039
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6    -0.06968514     0.00000350    -0.00164589     0.00251438     0.00615648    -0.00001947
Spectrum of overlapmatrix:    0.000000    1.000000    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370      -4.273532E-08  -1.152759E-06  -9.657890E-04   0.109263       2.122728E-04
 refs   2  -3.377683E-09   0.959718      -8.374117E-08  -1.735514E-04   5.094285E-03  -5.385108E-09
 refs   3  -1.698270E-10   7.365146E-08   0.960175      -1.209508E-05  -1.260570E-05  -4.102277E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000      -2.188588E-08  -9.380709E-05   6.965854E-06  -6.060304E-05   -1.80050    
 civs   2  -6.934362E-13   -1.00000       6.151843E-09  -3.505053E-10   3.048038E-09   9.051715E-05
 civs   3   1.200871E-10   9.187100E-10   0.999998       1.656300E-07  -1.439506E-06  -4.259741E-02
 civs   4  -1.859330E-10   7.904617E-10   3.388637E-06    1.00000       2.192431E-06   6.506017E-02
 civs   5  -4.551923E-10   1.935236E-09   8.294921E-06  -6.138090E-07  -0.999995       0.159354    
 civs   6  -2.425164E-05   1.031255E-04   0.442024      -3.282351E-02   0.285565        8484.08    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370       4.359934E-08   2.552497E-06  -9.660639E-04  -0.109260       7.113313E-02
 ref    2  -3.380504E-09  -0.959718      -3.854839E-08  -1.735547E-04  -5.094257E-03   8.416940E-04
 ref    3   4.497256E-11  -7.319242E-08   0.960171      -1.180139E-05   1.005197E-05  -7.570777E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036999     0.00000004     0.00000255    -0.00096606    -0.10926033     0.07113313
 ref:   2     0.00000000    -0.95971814    -0.00000004    -0.00017355    -0.00509426     0.00084169
 ref:   3     0.00000000    -0.00000007     0.96017065    -0.00001180     0.00001005    -0.07570777
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 54  1   -227.7749036773 -5.6843E-13  0.0000E+00  6.6049E-06  1.0000E-05
 mr-sdci # 54  2   -227.6238632266 -1.7053E-13  0.0000E+00  8.8872E-06  1.0000E-05
 mr-sdci # 54  3   -227.6231781180  4.2254E-09  1.0461E-08  7.8522E-05  1.0000E-05
 mr-sdci # 54  4   -227.3184226521  1.8474E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 54  5   -227.2580349808  1.3498E-09  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 54  6   -226.0665917369 -8.9494E-01  0.0000E+00  1.1659E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.38     0.00     0.67         0.    0.7752
    4   42    0     0.01     0.00     0.01     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.10     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.060001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0400s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  55

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000753
ci vector #   2dasum_wr=    0.001603
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00021222    -0.00000001     0.00000501    -0.00000767    -0.00001878     0.00000006
 sovl   7    -0.00046913     0.00000002    -0.00000186     0.00001700     0.00004149    -0.00000011     0.00000026
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873039
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6    -0.06968514     0.00000350    -0.00164589     0.00251438     0.00615648    -0.00001947
   ht   7     0.15404387    -0.00000742     0.00060890    -0.00557307    -0.01360327     0.00003720    -0.00008518

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   8.221504E-09   4.318008E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -5.290496E-06   5.148721E-04   6.292011E-02  -3.273750E-02  -6.341925E-02   7.230864E-03  -0.995437    
   x:   2   6.548648E-10  -2.499699E-08   0.710285       6.553791E-05   0.703914       1.042650E-04   4.833632E-05
   x:   3  -3.842720E-06   3.717910E-06   1.352592E-03   4.966843E-03  -1.216682E-03  -0.999960      -7.264052E-03
   x:   4   1.729192E-07  -1.864495E-05   2.546107E-02  -0.998684      -2.560024E-02  -5.156780E-03   3.604718E-02
   x:   5   4.755821E-07  -4.554221E-05   0.700633       3.915626E-02  -0.706984       1.362783E-03   8.804999E-02
   x:   6   0.915298      -0.402778      -4.969552E-15  -3.804153E-11   4.973130E-15  -3.466028E-06  -2.132194E-04
   x:   7   0.402778       0.915298        0.00000      -1.720694E-11    0.00000      -1.568157E-06   4.712694E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370      -4.273532E-08  -1.152759E-06  -9.657890E-04   0.109263       2.122728E-04  -4.692483E-04
 refs   2  -3.377683E-09   0.959718      -8.374117E-08  -1.735514E-04   5.094285E-03  -5.385108E-09   1.317606E-08
 refs   3  -1.698270E-10   7.365146E-08   0.960175      -1.209508E-05  -1.260570E-05  -4.102277E-06  -9.028150E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000      -1.143457E-08   2.115622E-05   4.314162E-06   8.747254E-05   0.519555        2.42337    
 civs   2  -7.913278E-13   -1.00000      -3.470209E-09  -2.283070E-10  -4.283348E-09  -2.094240E-05  -1.186827E-04
 civs   3   1.327638E-10   1.432088E-09  -0.999997       2.106104E-07   9.736290E-07  -3.712851E-02   2.728268E-02
 civs   4  -1.542704E-10   4.109742E-10  -7.494244E-07    1.00000      -3.168057E-06  -1.901100E-02  -8.771163E-02
 civs   5  -3.781521E-10   1.011636E-09  -1.874642E-06  -3.799599E-07   0.999992      -4.587507E-02  -0.214411    
 civs   6  -2.817229E-05   1.514063E-04  -0.777678      -4.600842E-02  -0.149396        9366.86       -4233.05    
 civs   7  -3.631630E-06   4.411921E-05  -0.306703      -1.161684E-02   0.118874        5344.79        3250.75    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370       4.318626E-08   3.180361E-07  -9.659592E-04   0.109259      -2.053794E-02  -9.574761E-02
 ref    2  -3.380238E-09  -0.959718       7.113724E-08  -1.735535E-04   5.094244E-03  -2.305170E-04  -1.125333E-03
 ref    3   1.060135E-10  -7.329585E-08  -0.960165      -1.159924E-05  -1.213107E-05  -0.122328       1.421684E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999     0.00000004     0.00000032    -0.00096596     0.10925927    -0.02053794    -0.09574761
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017355     0.00509424    -0.00023052    -0.00112533
 ref:   3     0.00000000    -0.00000007    -0.96016544    -0.00001160    -0.00001213    -0.12232812     0.01421684
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 55  1   -227.7749036773  5.6843E-14  0.0000E+00  6.6049E-06  1.0000E-05
 mr-sdci # 55  2   -227.6238632266 -1.7053E-13  0.0000E+00  8.8871E-06  1.0000E-05
 mr-sdci # 55  3   -227.6231781212  3.2084E-09  2.1014E-09  5.3464E-05  1.0000E-05
 mr-sdci # 55  4   -227.3184226521  3.4674E-12  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 55  5   -227.2580349812  3.4743E-10  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 55  6   -226.3812245083  3.1463E-01  0.0000E+00  1.2146E+00  1.0000E-04
 mr-sdci # 55  7   -225.9502038107  3.1831E-03  0.0000E+00  1.0279E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.01     0.17         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.08     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.6900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  56

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000261
ci vector #   2dasum_wr=    0.000961
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00021222    -0.00000001     0.00000501    -0.00000767    -0.00001878     0.00000006
 sovl   7    -0.00046913     0.00000002    -0.00000186     0.00001700     0.00004149    -0.00000011     0.00000026
 sovl   8    -0.00005847     0.00000000     0.00000663     0.00000214     0.00000517    -0.00000001     0.00000003     0.00000001
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873039
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358725
   ht   6    -0.06968514     0.00000350    -0.00164589     0.00251438     0.00615648    -0.00001947
   ht   7     0.15404387    -0.00000742     0.00060890    -0.00557307    -0.01360327     0.00003720    -0.00008518
   ht   8     0.01920037    -0.00000082    -0.00217649    -0.00070335    -0.00169352     0.00000473    -0.00001020    -0.00000187

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.815495E-09   8.241914E-09   4.354733E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   9.615677E-06  -5.341102E-06   5.180921E-04   8.650703E-02   4.334786E-03   3.938433E-02  -5.663313E-03   0.995447    
   x:   2  -1.143328E-10   6.380728E-10  -2.512254E-08  -6.720011E-02   0.996974       3.908458E-02  -5.991091E-05  -4.826476E-05
   x:   3  -7.156544E-06  -3.441590E-06   3.082943E-06  -2.194369E-03  -2.317927E-04   3.679599E-03   0.999974       5.735194E-03
   x:   4  -3.766005E-07   1.763233E-07  -1.876412E-05   0.723380       7.562463E-02  -0.685334       4.333533E-03  -3.605349E-02
   x:   5  -8.436382E-07   4.796812E-07  -4.582643E-05   0.681701       1.747960E-02   0.726102      -6.668462E-04  -8.804944E-02
   x:   6   8.933666E-02   0.911322      -0.401884      -1.324838E-11  -3.329107E-13  -3.845220E-09   3.792013E-06   2.132139E-04
   x:   7  -6.298513E-02   0.407859       0.910870      -6.028558E-12  -1.526270E-13  -2.030001E-09   8.476296E-07  -4.712712E-04
   x:   8   0.994008      -5.606133E-02   9.383650E-02    0.00000        0.00000       2.330828E-09   6.968263E-06  -5.870135E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370      -4.273532E-08  -1.152759E-06  -9.657890E-04   0.109263       2.122728E-04  -4.692483E-04  -5.848853E-05
 refs   2  -3.377683E-09   0.959718      -8.374117E-08  -1.735514E-04   5.094285E-03  -5.385108E-09   1.317606E-08   1.008941E-09
 refs   3  -1.698270E-10   7.365146E-08   0.960175      -1.209508E-05  -1.260570E-05  -4.102277E-06  -9.028150E-06   1.140174E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000      -7.625382E-09  -3.022853E-05   2.985096E-06  -4.360317E-05   3.286753E-02    2.49339      -1.205037E-02
 civs   2   4.968671E-13   -1.00000       2.654987E-09  -1.900742E-10   3.014836E-09  -7.070752E-07  -1.202272E-04   9.787109E-06
 civs   3   9.158395E-10  -1.702576E-09    1.00000       1.010624E-06  -2.860785E-05  -5.830471E-02   3.973454E-04  -0.162675    
 civs   4  -9.795497E-11   2.649203E-10   1.097225E-06    1.00000       1.497359E-06  -1.381257E-03  -9.036273E-02  -2.198021E-04
 civs   5  -2.468286E-10   6.693974E-10   2.690006E-06  -2.456364E-07  -0.999996      -2.593210E-03  -0.220554       1.314856E-03
 civs   6  -3.790876E-06   7.461301E-05   0.960810      -4.239609E-04   -1.59029       -7248.13       -1833.96        7278.22    
 civs   7   2.041180E-05  -2.607405E-05   0.474040       2.427977E-02   -1.45016       -4821.64        4174.15        865.527    
 civs   8  -1.295648E-04   3.495698E-04  -0.833036      -0.145284        5.11715        12939.8        2496.31        19265.1    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370       4.303645E-08   4.175090E-08  -9.659049E-04  -0.109261      -1.254393E-03  -9.851452E-02   4.821751E-04
 ref    2  -3.378288E-09  -0.959718      -6.744850E-08  -1.735527E-04  -5.094268E-03  -2.508737E-05  -1.155882E-03   7.790559E-06
 ref    3  -9.364516E-10  -7.137122E-08   0.960158      -1.299866E-05   6.309759E-05   0.164818      -1.313800E-03   2.578838E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096590    -0.10926099    -0.00125439    -0.09851452     0.00048218
 ref:   2     0.00000000    -0.95971814    -0.00000007    -0.00017355    -0.00509427    -0.00002509    -0.00115588     0.00000779
 ref:   3     0.00000000    -0.00000007     0.96015839    -0.00001300     0.00006310     0.16481824    -0.00131380     0.02578838

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 56  1   -227.7749036773  2.2737E-13  0.0000E+00  6.6049E-06  1.0000E-05
 mr-sdci # 56  2   -227.6238632266  2.2737E-13  0.0000E+00  8.8870E-06  1.0000E-05
 mr-sdci # 56  3   -227.6231781229  1.7505E-09  1.2280E-09  4.2813E-05  1.0000E-05
 mr-sdci # 56  4   -227.3184226521  3.3936E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 56  5   -227.2580350179  3.6677E-08  0.0000E+00  3.2320E-01  1.0000E-04
 mr-sdci # 56  6   -226.9482982333  5.6707E-01  0.0000E+00  8.7775E-01  1.0000E-04
 mr-sdci # 56  7   -225.9754361270  2.5232E-02  0.0000E+00  9.9502E-01  1.0000E-04
 mr-sdci # 56  8   -225.0552868483  4.0634E-03  0.0000E+00  1.2898E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.66     0.36     0.00     0.66         0.    0.7752
    4   42    0     0.01     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.01     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.079998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.7100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  57

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000167
ci vector #   2dasum_wr=    0.000780
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000742     0.00000000     0.00000293     0.00000030    -0.00000065     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873040
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358729
   ht   6     0.00243784    -0.00000003    -0.00096311    -0.00009869     0.00021363    -0.00000039

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   1.124501E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   7.424278E-06   3.206509E-03  -0.253402       0.116718       0.253386      -0.926256    
   x:   2  -9.890722E-11  -3.009310E-06  -0.707108      -8.906467E-07  -0.707105       1.233969E-05
   x:   3  -2.934428E-06  -2.252478E-02  -0.656685       5.460040E-02   0.656694       0.366101    
   x:   4  -3.009617E-07  -0.847972       4.681780E-02   0.524547      -4.681440E-02   3.754812E-02
   x:   5   6.516497E-07  -0.529552      -4.856737E-02  -0.841574       4.856947E-02  -8.130681E-02
   x:   6    1.00000      -2.836922E-11    0.00000      -4.593101E-11    0.00000       8.015360E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370       4.303645E-08   4.175090E-08  -9.659049E-04  -0.109261      -7.426731E-06
 refs   2  -3.378288E-09  -0.959718      -6.744850E-08  -1.735527E-04  -5.094268E-03   2.096458E-11
 refs   3  -9.364516E-10  -7.137122E-08   0.960158      -1.299866E-05   6.309759E-05   7.736671E-07

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000       1.179877E-09  -4.320886E-06   7.983450E-07  -1.876306E-05   0.221398    
 civs   2   1.856077E-13   -1.00000      -2.473841E-10  -1.051592E-11   2.476466E-10  -2.944169E-06
 civs   3   6.063144E-10  -1.614348E-10  -0.999998      -3.159777E-07   7.424553E-06  -8.752668E-02
 civs   4   6.244660E-11  -4.792309E-11   1.755213E-07   -1.00000       7.526545E-07  -8.971329E-03
 civs   5  -1.367510E-10   1.054124E-10  -3.861017E-07   7.772033E-08   -1.00000       1.934800E-02
 civs   6  -2.072116E-04   1.588992E-04  -0.581994       0.107532       -2.52726        29820.9    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370      -4.308311E-08   1.297191E-07   9.658725E-04   0.109262      -8.738497E-03
 ref    2  -3.377785E-09   0.959718       6.961006E-08   1.735523E-04   5.094276E-03  -9.355099E-05
 ref    3  -5.146159E-10   7.133916E-08  -0.960157       1.277847E-05  -5.792421E-05  -6.096672E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036999    -0.00000004     0.00000013     0.00096587     0.10926173    -0.00873850
 ref:   2     0.00000000     0.95971814     0.00000007     0.00017355     0.00509428    -0.00009355
 ref:   3     0.00000000     0.00000007    -0.96015720     0.00001278    -0.00005792    -0.06096672
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 57  1   -227.7749036773 -1.7053E-13  0.0000E+00  6.6049E-06  1.0000E-05
 mr-sdci # 57  2   -227.6238632266 -1.1369E-13  0.0000E+00  8.8870E-06  1.0000E-05
 mr-sdci # 57  3   -227.6231781236  7.1441E-10  4.0583E-10  2.3090E-05  1.0000E-05
 mr-sdci # 57  4   -227.3184226521  2.0123E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 57  5   -227.2580350287  1.0854E-08  0.0000E+00  3.2319E-01  1.0000E-04
 mr-sdci # 57  6   -225.7468257527 -1.2015E+00  0.0000E+00  1.5391E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.13     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.48     0.00     0.67         0.    0.7752
    4   42    0     0.01     0.00     0.01     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.060005
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.8200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0900s 

          starting ci iteration  58

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000109
ci vector #   2dasum_wr=    0.000415
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000742     0.00000000     0.00000293     0.00000030    -0.00000065     0.00000000
 sovl   7    -0.00002398     0.00000000    -0.00000148     0.00000089    -0.00000212     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873040
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358729
   ht   6     0.00243784    -0.00000003    -0.00096311    -0.00009869     0.00021363    -0.00000039
   ht   7     0.00787549     0.00000034     0.00048660    -0.00029335     0.00069380    -0.00000008    -0.00000033

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   4.257013E-10   1.130162E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -2.322219E-05  -9.544358E-06  -7.712728E-02  -4.764522E-02  -2.993661E-02   2.182774E-02   0.995193    
   x:   2  -1.038063E-09   5.878066E-12   0.525893      -0.850551      -2.872810E-05  -1.226060E-04   3.801376E-05
   x:   3  -1.739658E-06   2.789715E-06  -4.156043E-04  -1.121114E-04   7.181517E-03  -0.999730       2.210570E-02
   x:   4   8.640525E-07   3.799456E-07  -7.117378E-02  -4.397343E-02  -0.995760      -7.945474E-03  -3.740062E-02
   x:   5  -2.049336E-06  -8.387304E-07   0.844051       0.521881      -8.668062E-02   9.087016E-04   8.777167E-02
   x:   6   8.964081E-02  -0.995974       7.234484E-13   4.482223E-13   1.310899E-10  -3.098674E-06  -7.392173E-06
   x:   7  -0.995974      -8.964081E-02    0.00000        0.00000      -3.944477E-11   9.496283E-07  -2.412095E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370       4.303645E-08   4.175090E-08  -9.659049E-04  -0.109261      -7.426731E-06  -2.399025E-05
 refs   2  -3.378288E-09  -0.959718      -6.744850E-08  -1.735527E-04  -5.094268E-03   2.096458E-11   4.241571E-10
 refs   3  -9.364516E-10  -7.137122E-08   0.960158      -1.299866E-05   6.309759E-05   7.736671E-07   1.584152E-07

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000       8.549420E-09   1.317649E-05   9.061666E-06  -3.992998E-04   0.741692      -0.892904    
 civs   2   8.404386E-13   -1.00000       8.413750E-10   4.085360E-10  -1.916092E-08   4.074431E-05  -2.952835E-05
 civs   3  -3.289377E-10   4.554589E-10  -0.999996       7.045058E-07  -4.047872E-05   0.117059       1.732395E-02
 civs   4   3.762302E-10  -3.202211E-10  -4.710703E-07   -1.00000       1.473565E-05  -2.715494E-02   3.381491E-02
 civs   5  -8.904838E-10   7.613865E-10   1.171617E-06   8.434059E-07   -1.00004       6.514901E-02  -7.857010E-02
 civs   6  -8.589691E-05   4.583600E-05  -0.850567      -4.249196E-02    4.65039       -20976.5       -21367.7    
 civs   7  -3.914109E-04   3.422195E-04   0.812672       0.390970       -18.0879        37417.3       -30614.4    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370      -4.337357E-08  -5.625024E-07   9.655423E-04   0.109277      -2.924070E-02   3.524425E-02
 ref    2  -3.374791E-09   0.959718       6.108082E-08   1.735482E-04   5.094455E-03  -3.508561E-04   4.092959E-04
 ref    3  -1.380806E-09   7.189826E-08  -0.960155       1.370421E-05  -1.012335E-04   0.102098      -4.752951E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999    -0.00000004    -0.00000056     0.00096554     0.10927675    -0.02924070     0.03524425
 ref:   2     0.00000000     0.95971814     0.00000006     0.00017355     0.00509446    -0.00035086     0.00040930
 ref:   3     0.00000000     0.00000007    -0.96015537     0.00001370    -0.00010123     0.10209785    -0.00475295
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 58  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6051E-06  1.0000E-05
 mr-sdci # 58  2   -227.6238632266  2.2737E-13  0.0000E+00  8.8871E-06  1.0000E-05
 mr-sdci # 58  3   -227.6231781240  3.2958E-10  5.0275E-10  1.6996E-05  1.0000E-05
 mr-sdci # 58  4   -227.3184226522  5.1728E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 58  5   -227.2580351284  9.9696E-08  0.0000E+00  3.2314E-01  1.0000E-04
 mr-sdci # 58  6   -226.7675823816  1.0208E+00  0.0000E+00  9.5205E-01  1.0000E-04
 mr-sdci # 58  7   -225.0635006596 -9.1194E-01  0.0000E+00  1.4523E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.13     0.10     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.46     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.02     0.02     0.00     0.02         0.    0.0314
    6   76    0     0.04     0.07     0.00     0.04         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.010002
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0200s 
time spent in multnx:                   1.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  59

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000165
ci vector #   2dasum_wr=    0.000340
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7       sovl   8
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6    -0.00000742     0.00000000     0.00000293     0.00000030    -0.00000065     0.00000000
 sovl   7    -0.00002398     0.00000000    -0.00000148     0.00000089    -0.00000212     0.00000000     0.00000000
 sovl   8     0.00010308     0.00000000    -0.00000049    -0.00000375     0.00000911     0.00000000     0.00000000     0.00000001
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873040
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358729
   ht   6     0.00243784    -0.00000003    -0.00096311    -0.00009869     0.00021363    -0.00000039
   ht   7     0.00787549     0.00000034     0.00048660    -0.00029335     0.00069380    -0.00000008    -0.00000033
   ht   8    -0.03384641    -0.00000164     0.00016217     0.00122911    -0.00298780     0.00000031     0.00000092    -0.00000411

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   3.698025E-10   1.082297E-09   1.917312E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -2.997178E-06  -1.671489E-05  -1.047228E-04   6.820357E-02  -4.706545E-02  -4.681683E-02  -3.399492E-03  -0.995455    
   x:   2  -3.774139E-11  -1.252293E-09  -4.933507E-09   0.621132       0.773061       0.128729      -1.039988E-04  -4.755544E-05
   x:   3  -1.655139E-06  -2.757544E-06   8.423404E-07   5.505658E-04  -1.385237E-03   4.855624E-03  -0.999982       3.289806E-03
   x:   4   1.298763E-07   5.779195E-07   3.819742E-06  -2.092483E-02   0.180448      -0.982681      -4.913788E-03   3.626752E-02
   x:   5  -2.609608E-07  -1.482435E-06  -9.257092E-06  -0.780452       0.606299       0.124709      -9.535377E-04  -8.800055E-02
   x:   6   3.590987E-02   0.973174      -0.227251      -2.401792E-09   4.652342E-09  -1.517359E-08  -2.909993E-06   7.468446E-06
   x:   7  -0.979879      -1.038802E-02  -0.199324      -4.344572E-09   8.428749E-09  -2.752113E-08   1.561712E-06   2.408905E-05
   x:   8  -0.196338       0.229836       0.953219      -1.183882E-09   2.296339E-09  -7.496799E-09   1.534266E-07  -1.035480E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1   0.970370       4.303645E-08   4.175090E-08  -9.659049E-04  -0.109261      -7.426731E-06  -2.399025E-05   1.031037E-04
 refs   2  -3.378288E-09  -0.959718      -6.744850E-08  -1.735527E-04  -5.094268E-03   2.096458E-11   4.241571E-10  -2.882293E-09
 refs   3  -9.364516E-10  -7.137122E-08   0.960158      -1.299866E-05   6.309759E-05   7.736671E-07   1.584152E-07   8.918882E-07

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1    1.00000      -2.349858E-09   4.802097E-06   9.947181E-06  -2.401482E-04  -0.334578       -2.42594      -7.214116E-02
 civs   2   6.954779E-13   -1.00000      -8.497053E-11   4.501674E-10  -1.174511E-08  -1.220546E-05  -1.178139E-04   1.086265E-05
 civs   3  -2.083338E-10   8.537582E-10   0.999995       6.426128E-07  -5.310856E-05   0.118836      -2.097791E-02   1.583710E-02
 civs   4   4.837896E-10   7.394856E-11  -1.791980E-07   -1.00000       8.988443E-06   1.190509E-02   8.834783E-02   4.029062E-03
 civs   5  -1.153470E-09  -2.024568E-10   4.182613E-07   9.212204E-07   -1.00002      -3.008987E-02  -0.214303      -6.017943E-03
 civs   6  -9.883162E-05  -1.250806E-05   0.946905      -3.410426E-02    6.41594       -19242.9        4527.38       -22686.8    
 civs   7  -3.482454E-04   5.188399E-04   -1.10418       0.370126       -22.3087        38442.6       -12193.4       -31475.8    
 civs   8   3.795791E-05   1.426334E-04  -0.235309      -1.283680E-02   -2.39895        10804.8        21024.2       -8258.06    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.970370      -4.294280E-08  -1.480683E-07   9.655074E-04   0.109270       1.329573E-02   9.583405E-02   2.814384E-03
 ref    2  -3.373422E-09   0.959718      -6.923656E-08   1.735478E-04   5.094382E-03   1.476862E-04   1.123786E-03   2.950769E-05
 ref    3  -1.234340E-09   7.239068E-08   0.960154       1.363653E-05  -1.148015E-04   0.114939       1.654765E-04  -1.469782E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999    -0.00000004    -0.00000015     0.00096551     0.10927045     0.01329573     0.09583405     0.00281438
 ref:   2     0.00000000     0.95971814    -0.00000007     0.00017355     0.00509438     0.00014769     0.00112379     0.00002951
 ref:   3     0.00000000     0.00000007     0.96015439     0.00001364    -0.00011480     0.11493860     0.00016548    -0.01469782

 trial vector basis is being transformed.  new dimension:   5
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6052E-06  1.0000E-05
 mr-sdci # 59  2   -227.6238632266 -3.9790E-13  0.0000E+00  8.8870E-06  1.0000E-05
 mr-sdci # 59  3   -227.6231781241  1.1858E-10  1.5698E-10  1.4961E-05  1.0000E-05
 mr-sdci # 59  4   -227.3184226522  5.6843E-14  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 59  5   -227.2580351359  7.4527E-09  0.0000E+00  3.2316E-01  1.0000E-04
 mr-sdci # 59  6   -226.9700075857  2.0243E-01  0.0000E+00  8.0779E-01  1.0000E-04
 mr-sdci # 59  7   -225.9480886186  8.8459E-01  0.0000E+00  9.9853E-01  1.0000E-04
 mr-sdci # 59  8   -224.9419224649 -1.1336E-01  0.0000E+00  1.4504E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.12     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.38     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.07     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.080002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  60

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000076
ci vector #   2dasum_wr=    0.000303
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00002750     0.00000000    -0.00000008     0.00000098    -0.00000244     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873040
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358741
   ht   6    -0.00903011     0.00000045     0.00002556    -0.00032139     0.00079833    -0.00000033

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   2.294854E-10    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   2.750061E-05   4.551191E-05   2.579121E-03  -3.245773E-02   8.930930E-02  -0.995472    
   x:   2  -1.374347E-09    1.00000      -1.288920E-07   1.622080E-06   4.551191E-05   4.974884E-05
   x:   3  -7.788311E-08  -1.288920E-07   0.999993       9.192190E-05   2.579121E-03   2.819225E-03
   x:   4   9.801433E-07   1.622080E-06   9.192190E-05   0.998843      -3.245773E-02  -3.547938E-02
   x:   5  -2.435073E-06  -4.974884E-05  -2.819225E-03   3.547938E-02   0.995472       8.814518E-02
   x:   6   -1.00000        0.00000        0.00000        0.00000        0.00000      -2.762571E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.970370      -4.294280E-08  -1.480683E-07   9.655074E-04   0.109270       2.750662E-05
 refs   2  -3.373422E-09   0.959718      -6.923656E-08   1.735478E-04   5.094382E-03  -7.090170E-10
 refs   3  -1.234340E-09   7.239068E-08   0.960154       1.363653E-05  -1.148015E-04  -1.256308E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1    1.00000      -6.993068E-09   1.206455E-05   2.178432E-05  -4.271178E-04   -1.81537    
 civs   2  -2.393402E-13   -1.00000      -6.602558E-10  -1.088894E-09   2.134914E-08   9.072717E-05
 civs   3  -7.778586E-11   7.711053E-11   -1.00000      -6.128609E-08   1.202942E-06   5.134568E-03
 civs   4   9.657017E-10  -2.491050E-10   4.296614E-07  -0.999999      -1.528147E-05  -6.471315E-02
 civs   5  -2.392303E-09   6.163494E-10  -1.063155E-06  -1.873087E-06  -0.999962       0.160979    
 civs   6  -9.857692E-04   2.543283E-04  -0.438701      -0.792139        15.5312        66011.9    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.970370       4.321976E-08  -3.277981E-07  -9.663616E-04  -0.109254       7.171384E-02
 ref    2  -3.384973E-09  -0.959718       6.357236E-08  -1.735577E-04  -5.094183E-03   8.491333E-04
 ref    3  -7.030926E-11  -7.263623E-08  -0.960154      -1.269998E-05   9.643998E-05  -7.802067E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97036999     0.00000004    -0.00000033    -0.00096636    -0.10925358     0.07171384
 ref:   2     0.00000000    -0.95971814     0.00000006    -0.00017356    -0.00509418     0.00084913
 ref:   3     0.00000000    -0.00000007    -0.96015387    -0.00001270     0.00009644    -0.07802067
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 60  1   -227.7749036773  1.1369E-13  0.0000E+00  6.6042E-06  1.0000E-05
 mr-sdci # 60  2   -227.6238632266  0.0000E+00  0.0000E+00  8.8871E-06  1.0000E-05
 mr-sdci # 60  3   -227.6231781242  6.9235E-11  1.7219E-10  1.0126E-05  1.0000E-05
 mr-sdci # 60  4   -227.3184226524  1.8099E-10  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 60  5   -227.2580352020  6.6103E-08  0.0000E+00  3.2322E-01  1.0000E-04
 mr-sdci # 60  6   -226.0638909018 -9.0612E-01  0.0000E+00  1.1611E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.16     0.11     0.00     0.16         0.    0.2041
    2    1    0     0.14     0.13     0.00     0.14         0.    0.1775
    3    5    0     0.67     0.37     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

          starting ci iteration  61

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000097
ci vector #   2dasum_wr=    0.000207
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      405240 2x:       32580 4x:        1890
All internal counts: zz :      249654 yy:     1017991 xx:           0 ww:           0
One-external counts: yz :      302304 yx:           0 yw:           0
Two-external counts: yy :       89533 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:       90155    task #
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   6     0.00002750     0.00000000    -0.00000008     0.00000098    -0.00000244     0.00000000
 sovl   7     0.00006013     0.00000000     0.00000036     0.00000217    -0.00000532     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1  -328.36045595
   ht   2     0.00000000  -328.20941550
   ht   3     0.00000000     0.00000000  -328.20873040
   ht   4     0.00000000     0.00000000     0.00000000  -327.90397493
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000  -327.84358741
   ht   6    -0.00903011     0.00000045     0.00002556    -0.00032139     0.00079833    -0.00000033
   ht   7    -0.01974359     0.00000095    -0.00011720    -0.00071267     0.00174339    -0.00000062    -0.00000140

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   1.342728E-10   7.116227E-10    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -7.132667E-07  -6.611450E-05   7.510940E-02  -4.655820E-02   3.496879E-02   5.472891E-03   0.995459    
   x:   2   8.305069E-11   3.197407E-09   0.527348       0.849649      -4.227277E-05  -2.213512E-04  -4.814991E-05
   x:   3   2.161824E-07  -2.946867E-07  -1.004331E-02   5.976766E-03   6.044395E-02  -0.998093       4.401407E-03
   x:   4  -1.307394E-08  -2.384158E-06  -5.034072E-03   3.061198E-03  -0.997524      -6.018221E-02   3.589523E-02
   x:   5   6.574226E-08   5.848367E-06   0.846249      -0.525246      -8.293950E-03  -1.255123E-02  -8.805688E-02
   x:   6   0.913831       0.406096       3.365634E-13  -2.089158E-13  -5.649319E-10   1.998188E-07   2.762499E-05
   x:   7  -0.406096       0.913831        0.00000        0.00000       2.583266E-10  -9.138665E-08   6.040260E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.970370      -4.294280E-08  -1.480683E-07   9.655074E-04   0.109270       2.750662E-05   6.014314E-05
 refs   2  -3.373422E-09   0.959718      -6.923656E-08   1.735478E-04   5.094382E-03  -7.090170E-10  -1.669427E-09
 refs   3  -1.234340E-09   7.239068E-08   0.960154       1.363653E-05  -1.148015E-04  -1.256308E-06   1.253146E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1    1.00000      -4.205916E-09   2.782198E-06   1.704024E-05  -5.050295E-04  -0.513605        2.42538    
 civs   2  -1.159786E-12   -1.00000      -2.834439E-10  -8.710142E-10   2.492426E-08   2.075843E-05  -1.182677E-04
 civs   3  -1.410334E-10   1.691809E-10   -1.00000      -1.344065E-07  -1.218105E-08  -2.069694E-02   6.433097E-03
 civs   4   8.068318E-10  -1.468848E-10   8.935053E-08  -0.999999      -1.813789E-05  -1.957362E-02   8.721825E-02
 civs   5  -2.009715E-09   3.703829E-10  -2.443186E-07  -1.457155E-06  -0.999955       4.514133E-02  -0.214821    
 civs   6  -1.140324E-03   3.563848E-04  -0.778499      -0.978879        12.4062       -73199.9       -33058.4    
 civs   7   1.427900E-04  -9.304999E-05   0.309790       0.164309        2.72507        42021.3       -25217.2    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.970370       4.310846E-08   3.909361E-08  -9.661743E-04  -0.109251       2.033649E-02  -9.583886E-02
 ref    2  -3.384064E-09  -0.959718       6.777019E-08  -1.735555E-04  -5.094147E-03   2.282442E-04  -1.127216E-03
 ref    3   2.420221E-10  -7.279262E-08  -0.960153      -1.232973E-05   1.026134E-04   0.124743       1.613321E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096617    -0.10925050     0.02033649    -0.09583886
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017356    -0.00509415     0.00022824    -0.00112722
 ref:   3     0.00000000    -0.00000007    -0.96015319    -0.00001233     0.00010261     0.12474265     0.01613321
 NCSF=                   665                 49140                     0
                     0

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 61  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6043E-06  1.0000E-05
 mr-sdci # 61  2   -227.6238632266  5.6843E-14  0.0000E+00  8.8870E-06  1.0000E-05
 mr-sdci # 61  3   -227.6231781242  5.3319E-11  3.4018E-11  6.7535E-06  1.0000E-05
 mr-sdci # 61  4   -227.3184226524  1.1255E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 61  5   -227.2580352050  2.9765E-09  0.0000E+00  3.2323E-01  1.0000E-04
 mr-sdci # 61  6   -226.3787590821  3.1487E-01  0.0000E+00  1.2140E+00  1.0000E-04
 mr-sdci # 61  7   -225.9504981168  2.4095E-03  0.0000E+00  1.0266E+00  1.0000E-04
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   11    0     0.17     0.11     0.00     0.17         0.    0.2041
    2    1    0     0.13     0.11     0.00     0.13         0.    0.1775
    3    5    0     0.67     0.45     0.00     0.67         0.    0.7752
    4   42    0     0.00     0.00     0.00     0.00         0.    0.0423
    5   75    0     0.01     0.01     0.00     0.01         0.    0.0314
    6   76    0     0.05     0.09     0.00     0.05         0.    0.0050
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     1.0300s 
time spent in multnx:                   1.0300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.7700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1000s 

 mr-sdci  convergence criteria satisfied after 61 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 61  1   -227.7749036773 -1.1369E-13  0.0000E+00  6.6043E-06  1.0000E-05
 mr-sdci # 61  2   -227.6238632266  5.6843E-14  0.0000E+00  8.8870E-06  1.0000E-05
 mr-sdci # 61  3   -227.6231781242  5.3319E-11  3.4018E-11  6.7535E-06  1.0000E-05
 mr-sdci # 61  4   -227.3184226524  1.1255E-11  0.0000E+00  1.9340E-01  1.0000E-04
 mr-sdci # 61  5   -227.2580352050  2.9765E-09  0.0000E+00  3.2323E-01  1.0000E-04
 mr-sdci # 61  6   -226.3787590821  3.1487E-01  0.0000E+00  1.2140E+00  1.0000E-04
 mr-sdci # 61  7   -225.9504981168  2.4095E-03  0.0000E+00  1.0266E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -227.774903677305
   ci vector at position   2 energy= -227.623863226619
   ci vector at position   3 energy= -227.623178124219

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    3 of the   8 expansion vectors are transformed.
    3 of the   7 matrix-vector products are transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.97037)

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.95972)

 information on vector: 2 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    3(overlap= 0.96015)

 information on vector: 3 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -227.7749036773

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                          orbital     1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.943740                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-      
                                                
 z*  1  1       4 -0.014216                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +- 
                                                
 z*  1  1       6 -0.012661                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
                                             +- 
 z*  1  1      10  0.026736                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +  
                                              - 
 z*  1  1      15 -0.014282                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +- 
                                                
 z*  1  1      17 -0.012357                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
                                             +- 
 z*  1  1      22 -0.082056                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 z*  1  1      41 -0.079239                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +- 
                                                
 z*  1  1      43 -0.074817                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
                                             +- 
 z*  1  1      51 -0.086824                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 z*  1  1      72 -0.148900                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-   +  
                                              - 
 z*  1  1      88 -0.011579                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-   +-    - 
                                             +- 
 z*  1  1      91 -0.073785                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-   +- 
                                                
 z*  1  1      93 -0.070784                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-      
                                             +- 
 z*  1  1     101 -0.012932                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-   +-   +- 
                                              - 
 z*  1  1     105  0.022520                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +-   +- 
                                             +- 
 y   1  1     670  0.033286              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     676 -0.011516             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     682  0.012696             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     746 -0.034325              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     752 -0.011913              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     759 -0.012496             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1    1214 -0.010946              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     - 
                                                
 y   1  1    1346  0.010915              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    - 
                                                
 y   1  1    1708  0.022631              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1714  0.018420              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1721  0.014030             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1728 -0.017050             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1736  0.019894              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1742 -0.017082             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1748  0.013693             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1755  0.012969             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                              - 
 y   1  1    1788 -0.022405              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1794  0.018293             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1800 -0.014985             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1807 -0.015091             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1812 -0.020279              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    1818 -0.016892              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    1825 -0.012825             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    1832  0.014728             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                              - 
 y   1  1    2114 -0.016033             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-    - 
                                                
 y   1  1    2143  0.013162             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
                                              - 
 y   1  1    3157  0.010212             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    - 
                                                
 y   1  1    3920  0.020247              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3926 -0.016719             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3932  0.013798             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3939  0.012873             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -    - 
                                                
 y   1  1    3944  0.019967              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3950  0.017279              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3957  0.013013             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3964 -0.015292             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    3996 -0.020602              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4002 -0.016508              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4009 -0.012924             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4016  0.014621             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-    - 
                                                
 y   1  1    4024 -0.019932              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4030  0.017119             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4036 -0.013874             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4043 -0.013488             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4327  0.015885             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-    - 
                                                
 y   1  1    4338  0.012397              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-      
                                              - 
 y   1  1    4350 -0.017960             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +-      
                                              - 
 y   1  1    5104 -0.012805             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-    - 
                                                
 y   1  1    5133  0.012538             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +-      
                                              - 
 y   1  1    6131 -0.011623              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-      
                                                
 y   1  1    6137  0.013893             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-      
                                                
 y   1  1    6649  0.014017              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6658  0.022585             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6660  0.011001             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6665 -0.025559             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    - 
                                                
 y   1  1    6677 -0.016317              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    6683  0.020604             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    6689  0.018044             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    6692  0.019248             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-      
                                              - 
 y   1  1    7431 -0.016456              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7437  0.019514             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7443  0.017928             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7446  0.018643             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-    - 
                                                
 y   1  1    7455  0.012937              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1    7464  0.023147             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1    7466  0.010042             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1    7471 -0.024160             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +-      
                                              - 
 y   1  1   11589  0.010055              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-      
                                                
 y   1  1   11598  0.016591             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-      
                                                
 y   1  1   11605 -0.011493             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-      
                                                
 y   1  1   12111 -0.015764              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12117  0.020045             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12123  0.019135             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12126  0.020668             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-    - 
                                                
 y   1  1   12135  0.014727              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12144  0.023328             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12146  0.010043             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12151 -0.023982             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-      
                                              - 
 y   1  1   12889  0.014341              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12898  0.022454             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12900  0.010316             15( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12905 -0.023308             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-    - 
                                                
 y   1  1   12917 -0.015170              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   12923  0.020499             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   12929  0.017806             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   12932  0.019667             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +-      
                                              - 
 y   1  1   17568  0.012769              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   17596  0.012681              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   18350  0.011056              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   18374  0.014964              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   23030  0.012011              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   23054  0.012973              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   23808  0.011328              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   23836  0.014137              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   28486 -0.012107              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   28513  0.011617              2( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   28529 -0.010165             18( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   29267  0.010899              2( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   29292 -0.013097              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   31086 -0.014681              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   31113  0.015645              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   31867  0.014607              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   31892 -0.015031              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +-      
                                              - 
 y   1  1   33947  0.011155              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   33963 -0.010626             18( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   33972 -0.012193              1( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   34726 -0.010862              1( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   34753  0.012803              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   34769 -0.010714             18( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   36547  0.015512              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   36572 -0.014896              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   37326 -0.014889              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   37353  0.014691              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +-   +-      
                                              - 
 y   1  1   39409 -0.015005              4( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   39418 -0.014684             13( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   39430  0.010089             25( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   39437  0.014104              6( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   39443 -0.014132             12( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   40191  0.013981              6( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   40197 -0.013659             12( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   40215 -0.014087              4( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   40224 -0.014589             13( a  )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   42018 -0.010563             13( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   42043 -0.010492             12( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   42797 -0.010313             12( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   42824 -0.010127             13( a  )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +-   +-      
                                              - 
 y   1  1   44871 -0.015382              6( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   44877  0.014533             12( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
                                                
 y   1  1   44895  0.013544              4( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   44904  0.014500             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-      
                                              - 
 y   1  1   45649  0.013700              4( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   45658  0.013686             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    - 
                                                
 y   1  1   45677 -0.014316              6( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   45683  0.014258             12( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-      
                                              - 
 y   1  1   47477  0.010727             12( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-    - 
                                                
 y   1  1   47504  0.010339             13( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-      
                                              - 
 y   1  1   48258  0.010303             13( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-    - 
                                                
 y   1  1   48283  0.010117             12( a  )   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-      
                                              - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             151
     0.01> rq > 0.001            868
    0.001> rq > 0.0001          2619
   0.0001> rq > 0.00001         4570
  0.00001> rq > 0.000001        2964
 0.000001> rq                  38631
           all                 49805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:        4961    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.943739603721   -214.850046167876
     2     2      0.000000004266     -0.000000968703
     3     3      0.000000004183     -0.000000949712
     4     4     -0.014215885556      3.234996653419
     5     5     -0.000000000043      0.000000009773
     6     6     -0.012660825684      2.881122718457
     7     7      0.000000005248     -0.000001191385
     8     8     -0.000000005532      0.000001257445
     9     9      0.000000000029     -0.000000006678
    10    10      0.026736220944     -6.084158335104
    11    11     -0.000000000029      0.000000006499
    12    12     -0.000359806869      0.081897050985
    13    13     -0.000000001266      0.000000286972
    14    14      0.000000000205     -0.000000046505
    15    15     -0.014281822384      3.250006222838
    16    16      0.000000000050     -0.000000011285
    17    17     -0.012357148092      2.812006529277
    18    18     -0.000000000206      0.000000046774
    19    19      0.000000001325     -0.000000300425
    20    20      0.000720546568     -0.163894618086
    21    21      0.000000000050     -0.000000011214
    22    22     -0.082055888098     18.696849559066
    23    23      0.000000006444     -0.000001456254
    24    24     -0.000000003501      0.000000787086
    25    25      0.000000004694     -0.000001060148
    26    26     -0.000000002925      0.000000663063
    27    27      0.001072026440     -0.244114533596
    28    28      0.000000000005     -0.000000001061
    29    29     -0.000000000066      0.000000009736
    30    30      0.000000005857     -0.000001321361
    31    31     -0.000000007265      0.000001644855
    32    32     -0.000000000002      0.000000000418
    33    33     -0.000563189442      0.128282062625
    34    34      0.000000000121     -0.000000027149
    35    35     -0.000000001615      0.000000364835
    36    36     -0.000000000002      0.000000000375
    37    37     -0.000888046595      0.202238325838
    38    38      0.001062106844     -0.241827877904
    39    39      0.000000000001     -0.000000000204
    40    40      0.000000000517     -0.000000117104
    41    41     -0.079238890689     18.064286907618
    42    42     -0.000000001967      0.000000448490
    43    43     -0.074816709740     17.055492042614
    44    44     -0.000000001364      0.000000305382
    45    45      0.000000009777     -0.000002208599
    46    46      0.002235410296     -0.509317320121
    47    47      0.000000007363     -0.000001663557
    48    48     -0.000000000702      0.000000157050
    49    49      0.000000000056     -0.000000012751
    50    50      0.002216123989     -0.504922464070
    51    51     -0.086823636728     19.780690867461
    52    52      0.000000001608     -0.000000366280
    53    53      0.000000007176     -0.000001625861
    54    54     -0.000000005356      0.000001208068
    55    55      0.000000002066     -0.000000462595
    56    56      0.000000002662     -0.000000602651
    57    57     -0.000000000025      0.000000005640
    58    58     -0.000959555160      0.218491214046
    59    59     -0.000000005867      0.000001325595
    60    60      0.000000005559     -0.000001254044
    61    61     -0.000000004346      0.000000981216
    62    62      0.000868730917     -0.197791453397
    63    63      0.000000000012     -0.000000002744
    64    64      0.000000000551     -0.000000124763
    65    65      0.000000001047     -0.000000236148
    66    66      0.001571182583     -0.357783920215
    67    67      0.000000000021     -0.000000004680
    68    68     -0.000000000019      0.000000004236
    69    69     -0.001002598022      0.228304864826
    70    70      0.000000000255     -0.000000057529
    71    71     -0.000000000157      0.000000035666
    72    72     -0.148900100671     33.944850118351
    73    73      0.000000000209     -0.000000047750
    74    74      0.000000001056     -0.000000247304
    75    75      0.000000002221     -0.000000500573
    76    76     -0.000000000009      0.000000001941
    77    77      0.000000003590     -0.000000808257
    78    78     -0.000000001721      0.000000380094
    79    79      0.004253724526     -0.969207920718
    80    80      0.000000000006     -0.000000001420
    81    81     -0.000457866471      0.105993020156
    82    82      0.000000000136     -0.000000030207
    83    83     -0.000000000491      0.000000111318
    84    84      0.000000000580     -0.000000131576
    85    85      0.000000000497     -0.000000112336
    86    86      0.000005138574     -0.001174711041
    87    87     -0.000000000266      0.000000060731
    88    88     -0.011579006098      2.642401796014
    89    89      0.000000000763     -0.000000172306
    90    90      0.000000001566     -0.000000354079
    91    91     -0.073784793600     16.820122227968
    92    92      0.000000001907     -0.000000434719
    93    93     -0.070784310798     16.137063276155
    94    94     -0.000000001602      0.000000359632
    95    95      0.000000010050     -0.000002270482
    96    96      0.002056657931     -0.468605133334
    97    97      0.000000007424     -0.000001676474
    98    98     -0.000000000264      0.000000058189
    99    99     -0.000000000054      0.000000012364
   100   100      0.002044719578     -0.465887237769
   101   101     -0.012932030275      2.951470836955
   102   102     -0.000000000016      0.000000003573
   103   103      0.000000001674     -0.000000378460
   104   104      0.000000000884     -0.000000198460
   105   105      0.022519960318     -5.142958674615

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.95170629  c**2 (all zwalks) =   0.95175994

 pople ci energy extrapolation is computed with 32 correlated electrons.

 eref      =   -227.675886573642   "relaxed" cnot**2         =   0.951706294785
 eci       =   -227.774903677305   deltae = eci - eref       =  -0.099017103663
 eci+dv1   =   -227.779685580121   dv1 = (1-cnot**2)*deltae  =  -0.004781902816
 eci+dv2   =   -227.779928234610   dv2 = dv1 / cnot**2       =  -0.005024557305
 eci+dv3   =   -227.780196832250   dv3 = dv1 / (2*cnot**2-1) =  -0.005293154945
 eci+pople =   -227.779832257065   ( 32e- scaled deltae )    =  -0.103945683423


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -227.6238632266

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                          orbital     1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.715661                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     - 
                                                
 z*  1  1       8 -0.604833                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-      
                                              - 
 z*  1  1      13  0.018813                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +- 
                                              - 
 z*  1  1      19 -0.017245                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
                                             +- 
 z*  1  1      24  0.080614                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +  
                                              - 
 z*  1  1      26 -0.044128                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     - 
                                              - 
 z*  1  1      29 -0.089693                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +- 
                                                
 z*  1  1      31 -0.037864                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
                                             +- 
 z*  1  1      45 -0.060094                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +     - 
                                             +- 
 z*  1  1      47 -0.046400                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-   +- 
                                              - 
 z*  1  1      53  0.038205                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +- 
                                                
 z*  1  1      55  0.069636                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                             +- 
 z*  1  1      60 -0.084158                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-   +  
                                              - 
 z*  1  1      65 -0.047826                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +    +-    - 
                                              - 
 z*  1  1      74 -0.079214                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +    +- 
                                              - 
 z*  1  1      78 -0.067600                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +    +-    - 
                                             +- 
 z*  1  1      82  0.011512                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +    +-    -   +- 
                                              - 
 z*  1  1      89  0.017730                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    -   +- 
                                             +- 
 z*  1  1      95 -0.048845                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +     - 
                                             +- 
 z*  1  1      97 -0.052637                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +    +-   +- 
                                              - 
 z*  1  1     104  0.018106                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     -   +-   +- 
                                             +- 
 z   1  1     246 -0.073527                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    - 
                                                
 z   1  1     267  0.031094                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +  
                                              - 
 z   1  1     276 -0.010228                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +    +-   +-    - 
                                              - 
 z   1  1     286  0.026731                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +-   +-   +-   +- 
                                                
 z   1  1     288  0.015981                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +-   +-   +-      
                                             +- 
 z   1  1     307 -0.010434                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +     -   +-   +-   +- 
                                              - 
 z   1  1     317 -0.059831                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-      
                                              - 
 z   1  1     336  0.019942                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +- 
                                                
 z   1  1     338  0.021637                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-      
                                             +- 
 z   1  1     357  0.028802                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +-   +  
                                              - 
 y   1  1     672  0.021650              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     684 -0.020915             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     751 -0.012402              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     765 -0.017542             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     772 -0.020802              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     800  0.012968              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +       
                                              - 
 y   1  1     902  0.014855              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1     908  0.011987              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1     922 -0.014663             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1     982 -0.011620              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-    - 
                                                
 y   1  1     988  0.010377             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-    - 
                                                
 y   1  1    1001 -0.011109             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-    - 
                                                
 y   1  1    1474  0.012737              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +     - 
                                              - 
 y   1  1    1727  0.011406             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    - 
                                                
 y   1  1    1762  0.011082              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-         - 
                                              - 
 y   1  1    1892  0.011123              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
                                             +- 
 y   1  1    1916 -0.017958              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1922 -0.015006              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1929 -0.011167             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1936  0.013456             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1994  0.011756              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
                                              - 
 y   1  1    2022 -0.010119              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
                                              - 
 y   1  1    2906  0.011814              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +     - 
                                              - 
 y   1  1    3963  0.010997             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
                                              - 
 y   1  1    4048 -0.014982              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4054 -0.010315              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4128 -0.016061              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4134  0.013708             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4140 -0.011334             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4147 -0.010674             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4206  0.011654              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     -    - 
                                              - 
 y   1  1    4230 -0.010261              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-        +-    - 
                                              - 
 y   1  1    4454 -0.011545             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +     - 
                                              - 
 y   1  1    6190 -0.011881             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +       
                                              - 
 y   1  1    6294  0.011047             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    - 
                                                
 y   1  1    6301 -0.013240             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    - 
                                                
 y   1  1    6717 -0.012075             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-    -   +- 
                                                
 y   1  1    6781 -0.011460              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6787  0.014440             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6793  0.012369             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6796  0.013385             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    7022  0.011593             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7029 -0.013311             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7489  0.010672             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -   +- 
                                                
 y   1  1    7495  0.010178             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -   +- 
                                                
 y   1  1    7498  0.010573             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -   +- 
                                                
 y   1  1    7568  0.014146             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7575 -0.014925             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7801  0.011813             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1    7807  0.010522             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1    7810  0.011078             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1   11649 -0.010875             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +       
                                              - 
 y   1  1   11762  0.011025             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-    - 
                                                
 y   1  1   12239  0.010313              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12248  0.016326             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12255 -0.016600             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12378 -0.010368             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -   +-      
                                             +- 
 y   1  1   12385  0.011238             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -   +-      
                                             +- 
 y   1  1   12481  0.010245             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12487  0.010103             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12490  0.010728             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12950  0.012225             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    -   +- 
                                                
 y   1  1   12957 -0.013262             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-    -   +- 
                                                
 y   1  1   13027  0.012536             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13033  0.011053             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13036  0.012100             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13262  0.013568             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   13269 -0.013836             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   17064  0.011709             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   18478  0.010261              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   19780 -0.024912              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19792 -0.016127             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19804 -0.019831              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19817 -0.013620             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19856  0.024823              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19869  0.016109             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19884  0.019872              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   19896  0.013288             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   22527 -0.010718             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   25238 -0.020531              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25251 -0.015021             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25266 -0.022249              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25278 -0.014150             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25318  0.020840              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25330  0.014700             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25342  0.022047              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   25355  0.014098             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   28018  0.010985              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +       
                                              - 
 y   1  1   30697 -0.018633              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   30722  0.017234              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   30736  0.010334             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   30774 -0.017606              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   30788 -0.010986             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   30801  0.016992              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   31217  0.010484              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-   +     - 
                                              - 
 y   1  1   33479 -0.010448              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +       
                                              - 
 y   1  1   36156  0.017816              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   36170  0.010944             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   36183 -0.017791              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   36235  0.017730              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   36260 -0.016809              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   36274 -0.010192             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   36676 -0.010046              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-   +     - 
                                              - 
 y   1  1   45008  0.010060             13( a  )   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     - 
                                              - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             133
     0.01> rq > 0.001           2170
    0.001> rq > 0.0001          4284
   0.0001> rq > 0.00001         3611
  0.00001> rq > 0.000001        1892
 0.000001> rq                  37713
           all                 49805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:        4961    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.000000032904      0.000007452655
     2     2      0.715661093355   -162.813189457930
     3     3     -0.000000029830      0.000006778746
     4     4      0.000000021091     -0.000004781390
     5     5     -0.000000000148      0.000000033741
     6     6      0.000000012989     -0.000002940861
     7     7     -0.000000020300      0.000004603233
     8     8     -0.604833153363    137.610594305758
     9     9      0.000000000276     -0.000000062721
    10    10     -0.000000018680      0.000004250980
    11    11      0.000000000123     -0.000000028064
    12    12      0.000000000524     -0.000000119080
    13    13      0.018813258958     -4.278375897989
    14    14      0.000000007868     -0.000001784995
    15    15      0.000000018709     -0.000004239701
    16    16     -0.000000000233      0.000000053022
    17    17      0.000000013870     -0.000003141267
    18    18     -0.000000009297      0.000002109392
    19    19     -0.017245070171      3.921696880847
    20    20     -0.000000000807      0.000000182961
    21    21      0.000000000407     -0.000000092529
    22    22     -0.000000200902      0.000045637707
    23    23     -0.000000003529      0.000000800032
    24    24      0.080613854626    -18.361008963218
    25    25     -0.000000012441      0.000002829444
    26    26     -0.044127904767     10.046555551487
    27    27      0.000000003264     -0.000000749606
    28    28      0.000000000041     -0.000000009421
    29    29     -0.089692939484     20.426376354575
    30    30      0.000000013320     -0.000003024771
    31    31     -0.037863606749      8.625575178354
    32    32     -0.000000000073      0.000000016645
    33    33     -0.000000007178      0.000001621280
    34    34      0.003679983032     -0.837704410457
    35    35      0.000000024370     -0.000005521016
    36    36      0.000000000020     -0.000000004481
    37    37     -0.000000013922      0.000003145990
    38    38      0.000000002959     -0.000000680116
    39    39     -0.000000000014      0.000000003263
    40    40     -0.000000003237      0.000000734231
    41    41     -0.000000049837      0.000011324760
    42    42      0.000000000069     -0.000000015749
    43    43     -0.000000007615      0.000001732470
    44    44     -0.000000012016      0.000002722169
    45    45     -0.060094095146     13.689666545712
    46    46      0.000000002963     -0.000000664779
    47    47     -0.046399597680     10.571387008404
    48    48     -0.000000018787      0.000004246446
    49    49     -0.000000000035      0.000000007948
    50    50      0.000000002946     -0.000000660919
    51    51     -0.000000212807      0.000048344306
    52    52      0.000000000365     -0.000000083079
    53    53      0.038204891390     -8.703150153305
    54    54     -0.000000013875      0.000003150285
    55    55      0.069635842117    -15.859873019526
    56    56      0.000000007557     -0.000001721846
    57    57      0.000000000010     -0.000000002182
    58    58     -0.000000001206      0.000000281998
    59    59      0.000000022126     -0.000005023234
    60    60     -0.084158447874     19.169137261560
    61    61      0.000000000806     -0.000000191100
    62    62      0.000000006698     -0.000001512364
    63    63     -0.000000000046      0.000000010433
    64    64     -0.000000002790      0.000000632980
    65    65     -0.047825763203     10.891286446873
    66    66      0.000000013362     -0.000003018889
    67    67      0.000000000062     -0.000000014061
    68    68     -0.000000000057      0.000000012922
    69    69     -0.000000001332      0.000000310726
    70    70      0.003279743930     -0.746647098837
    71    71      0.000000000071     -0.000000016147
    72    72     -0.000000052526      0.000011929872
    73    73      0.000000000067     -0.000000015233
    74    74     -0.079213717472     18.045055852355
    75    75      0.000000002621     -0.000000573377
    76    76     -0.000000000026      0.000000005944
    77    77     -0.000000005099      0.000001164323
    78    78     -0.067600003301     15.402057557185
    79    79     -0.000000000770      0.000000165291
    80    80     -0.000000000040      0.000000009059
    81    81     -0.000000043692      0.000009923867
    82    82      0.011511630097     -2.624503630041
    83    83      0.000000000020     -0.000000005461
    84    84     -0.000000000820      0.000000185510
    85    85     -0.009884479558      2.252629781450
    86    86      0.000000000141     -0.000000032025
    87    87     -0.000000000053      0.000000011988
    88    88     -0.000000058834      0.000013360528
    89    89      0.017729764947     -4.045347800302
    90    90      0.000000017510     -0.000003967561
    91    91     -0.000000000333      0.000000078622
    92    92      0.000000000068     -0.000000015507
    93    93     -0.000000035405      0.000008046831
    94    94     -0.000000010869      0.000002458941
    95    95     -0.048845471550     11.126294125661
    96    96      0.000000002072     -0.000000463228
    97    97     -0.052636574777     11.993203860024
    98    98     -0.000000018976      0.000004286481
    99    99     -0.000000000033      0.000000007615
   100   100      0.000000002290     -0.000000512710
   101   101     -0.000000060192      0.000013670392
   102   102     -0.000000000051      0.000000011650
   103   103      0.000000015920     -0.000003608131
   104   104      0.018106006764     -4.130862743211
   105   105      0.000000014925     -0.000003400853

 number of reference csfs (nref) is   105.  root number (iroot) is  2.
 c0**2 =   0.93491031  c**2 (all zwalks) =   0.94816295

 pople ci energy extrapolation is computed with 32 correlated electrons.

 eref      =   -227.524406732075   "relaxed" cnot**2         =   0.934910306080
 eci       =   -227.623863226619   deltae = eci - eref       =  -0.099456494544
 eci+dv1   =   -227.630336819407   dv1 = (1-cnot**2)*deltae  =  -0.006473592788
 eci+dv2   =   -227.630787519512   dv2 = dv1 / cnot**2       =  -0.006924292893
 eci+dv3   =   -227.631305672449   dv3 = dv1 / (2*cnot**2-1) =  -0.007442445830
 eci+pople =   -227.630773634722   ( 32e- scaled deltae )    =  -0.106366902647


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -227.6231781242

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                          orbital     1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18

                                         symmetry   a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a    a  
                                              a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       3 -0.613926                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +       
                                              - 
 z*  1  1       7  0.708056                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    - 
                                                
 z*  1  1      14  0.017681                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          - 
                                             +- 
 z*  1  1      18 -0.019187                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +- 
                                              - 
 z*  1  1      23 -0.089327                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +- 
                                                
 z*  1  1      25 -0.038775                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
                                             +- 
 z*  1  1      30  0.080868                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +  
                                              - 
 z*  1  1      35 -0.044699                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +    +-    - 
                                              - 
 z*  1  1      44 -0.047062                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +    +- 
                                              - 
 z*  1  1      48 -0.059557                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-    - 
                                             +- 
 z*  1  1      54 -0.084628                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +  
                                              - 
 z*  1  1      56 -0.047621                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +     - 
                                              - 
 z*  1  1      59  0.037894                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-   +- 
                                                
 z*  1  1      61  0.070151                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                             +- 
 z*  1  1      75 -0.068482                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +     - 
                                             +- 
 z*  1  1      77 -0.078475                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +    +-   +- 
                                              - 
 z*  1  1      84  0.011582                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -   +-   +- 
                                              - 
 z*  1  1      90  0.017770                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -   +-   +- 
                                             +- 
 z*  1  1      94 -0.053252                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +    +- 
                                              - 
 z*  1  1      98 -0.048330                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +    +-    - 
                                             +- 
 z*  1  1     103  0.018189                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +    +-    -   +- 
                                             +- 
 z   1  1     247  0.059958                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-      
                                              - 
 z   1  1     266 -0.019744                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +- 
                                                
 z   1  1     268 -0.022039                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-      
                                             +- 
 z   1  1     287 -0.028901                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -   +-   +-   +-   +  
                                              - 
 z   1  1     316  0.071674                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    - 
                                                
 z   1  1     337 -0.030600                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-   +-   +  
                                              - 
 z   1  1     356 -0.026013                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +-   +- 
                                                
 z   1  1     358 -0.016087                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-    -   +-   +-   +-      
                                             +- 
 z   1  1     377  0.010137                        +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +     -   +-   +-   +- 
                                              - 
 y   1  1     673 -0.012584              8( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     687 -0.017840             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
                                                
 y   1  1     694  0.011908              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-         - 
                                                
 y   1  1     700  0.010071              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-         - 
                                                
 y   1  1     714 -0.011760             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-         - 
                                                
 y   1  1     750  0.020832              7( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     762 -0.020057             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-      
                                                
 y   1  1     774 -0.021401              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     780  0.012093             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     793 -0.010900             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     - 
                                                
 y   1  1     798  0.014868              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +       
                                              - 
 y   1  1     904  0.010410              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    - 
                                                
 y   1  1    1476  0.012677              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +     - 
                                              - 
 y   1  1    1760  0.010292              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-         - 
                                              - 
 y   1  1    1805  0.011326             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-    - 
                                                
 y   1  1    1890  0.011499              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
                                             +- 
 y   1  1    1918 -0.016875              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1924  0.013578             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1930 -0.011251             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1937 -0.010772             24( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
                                              - 
 y   1  1    1996  0.012652              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
                                              - 
 y   1  1    2002 -0.011214             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
                                              - 
 y   1  1    2020 -0.011279              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
                                              - 
 y   1  1    2904  0.011822              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +     - 
                                              - 
 y   1  1    3972  0.010150              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-         - 
                                              - 
 y   1  1    4041  0.010834             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +-      
                                              - 
 y   1  1    4050 -0.014432              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4056  0.010239             11( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -    -   +- 
                                                
 y   1  1    4126 -0.016649              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4132 -0.013815              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4139 -0.010424             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4146  0.012240             23( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -   +     - 
                                              - 
 y   1  1    4204  0.011169              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +     -    - 
                                              - 
 y   1  1    4688 -0.011371             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +    +-    - 
                                              - 
 y   1  1    6164  0.011137             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +     - 
                                                
 y   1  1    6171 -0.012897             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +     - 
                                                
 y   1  1    6320 -0.011830             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-      
                                              - 
 y   1  1    6788  0.011774             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6795 -0.013505             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +     - 
                                              - 
 y   1  1    6873 -0.011946             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-   +- 
                                                
 y   1  1    7015 -0.011357              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7021  0.014299             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7027  0.012251             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7030  0.013248             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +    +-    - 
                                              - 
 y   1  1    7549  0.010107             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-    -      
                                             +- 
 y   1  1    7561 -0.010081              6( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7567  0.011979             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7573  0.010667             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7576  0.011240             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +-   +     - 
                                              - 
 y   1  1    7645  0.010570             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    -   +-   +- 
                                                
 y   1  1    7651  0.010088             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    -   +-   +- 
                                                
 y   1  1    7654  0.010476             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    -   +-   +- 
                                                
 y   1  1    7802  0.013994             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1    7809 -0.014769             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-   +    +-    - 
                                              - 
 y   1  1   11632  0.010783             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +     - 
                                                
 y   1  1   11779 -0.010790             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +    +-      
                                              - 
 y   1  1   12222 -0.010521             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    -      
                                             +- 
 y   1  1   12229  0.011393             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-    -      
                                             +- 
 y   1  1   12247  0.010411             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12253  0.010238             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12256  0.010883             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +-   +     - 
                                              - 
 y   1  1   12473  0.010227              4( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12482  0.016172             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   12489 -0.016439             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +    +    +-    - 
                                              - 
 y   1  1   13028  0.013752             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13035 -0.014028             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +-   +     - 
                                              - 
 y   1  1   13106  0.012093             13( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-    -   +-   +- 
                                                
 y   1  1   13113 -0.013132             20( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-    -   +-   +- 
                                                
 y   1  1   13261  0.012399             12( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   13267  0.010950             18( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   13270  0.011978             21( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +    +-   +    +-    - 
                                              - 
 y   1  1   17067  0.010795             22( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   19778  0.020769              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19791  0.014910             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   19806  0.022436              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19818  0.014306             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   19858 -0.020999              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19870 -0.014669             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   19882 -0.022284              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   19895 -0.014445             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   22524 -0.011371             19( a  )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-      
                                                
 y   1  1   25240  0.024600              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25252  0.015866             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   25264  0.019755              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25277  0.013547             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   25316 -0.024459              3( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25322 -0.010008              9( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25329 -0.015921             16( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   25344 -0.019866              5( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   25356 -0.013402             17( a  )   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   28148  0.010365              1( a  )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +    +-      
                                              - 
 y   1  1   30696 -0.016988              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   30710 -0.010978             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   30723  0.017717              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   30775 -0.017882              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   30800  0.017740              1( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   30814  0.010361             15( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-    -   +-      
                                              - 
 y   1  1   31451  0.010401              2( a  )   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-    -   +    +-    - 
                                              - 
 y   1  1   33609 -0.010450              2( a  )   +-   +-   +-   +-   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +    +-      
                                              - 
 y   1  1   36157  0.018425              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -    - 
                                                
 y   1  1   36182 -0.016461              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   36196 -0.010338             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-   +-    -      
                                              - 
 y   1  1   36234  0.018382              1( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   36248  0.010811             15( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-    - 
                                                
 y   1  1   36261 -0.017237              2( a  )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +-   +-   +-   +-    -   +-      
                                              - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             133
     0.01> rq > 0.001           2182
    0.001> rq > 0.0001          4273
   0.0001> rq > 0.00001         3606
  0.00001> rq > 0.000001        1887
 0.000001> rq                  37722
           all                 49805
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      107790 2x:           0 4x:           0
All internal counts: zz :      249654 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:      204115    task #     2:      177533    task #     3:      775173    task #     4:       42312
task #     5:       31433    task #     6:        4961    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000000128543     -0.000029197539
     2     2     -0.000000006952      0.000001582107
     3     3     -0.613925746722    139.677811606117
     4     4      0.000000023724     -0.000005365437
     5     5     -0.000000000307      0.000000069742
     6     6      0.000000021284     -0.000004810303
     7     7      0.708056024071   -161.083670737142
     8     8      0.000000005721     -0.000001301609
     9     9      0.000000000366     -0.000000083201
    10    10      0.000000003773     -0.000000822262
    11    11      0.000000000172     -0.000000039166
    12    12      0.000000000842     -0.000000191459
    13    13     -0.000000000213      0.000000048405
    14    14      0.017680581661     -4.020699500309
    15    15      0.000000024071     -0.000005444433
    16    16     -0.000000000206      0.000000046779
    17    17      0.000000021336     -0.000004822104
    18    18     -0.019187362956      4.363391317182
    19    19      0.000000000256     -0.000000058199
    20    20     -0.000000000022      0.000000005714
    21    21      0.000000000578     -0.000000131312
    22    22      0.000000050491     -0.000011471029
    23    23     -0.089327243398     20.342974077973
    24    24     -0.000000003623      0.000000823522
    25    25     -0.038775487021      8.833021795970
    26    26      0.000000002245     -0.000000510597
    27    27     -0.000000007845      0.000001757695
    28    28     -0.000000000011      0.000000002430
    29    29      0.000000000053     -0.000000012674
    30    30      0.080867951385    -18.418795105560
    31    31      0.000000003436     -0.000000781537
    32    32      0.000000000069     -0.000000015768
    33    33     -0.000000010052      0.000002263231
    34    34     -0.000000000150      0.000000034046
    35    35     -0.044698850985     10.176797684124
    36    36     -0.000000000072      0.000000016316
    37    37     -0.000000012379      0.000002779590
    38    38     -0.000000007870      0.000001763501
    39    39      0.000000000054     -0.000000012219
    40    40     -0.003713238195      0.845292436731
    41    41      0.000000083525     -0.000018946086
    42    42      0.000000000086     -0.000000019606
    43    43      0.000000087242     -0.000019777893
    44    44     -0.047061809565     10.721992627027
    45    45      0.000000000022     -0.000000004873
    46    46      0.000000005985     -0.000001339720
    47    47     -0.000000000669      0.000000152781
    48    48     -0.059557163746     13.567512591672
    49    49     -0.000000000046      0.000000010526
    50    50      0.000000005795     -0.000001296436
    51    51      0.000000055231     -0.000012546976
    52    52      0.000000000410     -0.000000093254
    53    53     -0.000000002499      0.000000567946
    54    54     -0.084628242974     19.275834146673
    55    55      0.000000000315     -0.000000071450
    56    56     -0.047621225633     10.844401189759
    57    57     -0.000000000074      0.000000016811
    58    58      0.000000007819     -0.000001752443
    59    59      0.037893766090     -8.632356929061
    60    60      0.000000001558     -0.000000353416
    61    61      0.070151194452    -15.977173449056
    62    62      0.000000009780     -0.000002201968
    63    63      0.000000000098     -0.000000022253
    64    64     -0.003337550153      0.759822619756
    65    65      0.000000002822     -0.000000641864
    66    66      0.000000012918     -0.000002902353
    67    67     -0.000000000010      0.000000002178
    68    68      0.000000000021     -0.000000004667
    69    69      0.000000007784     -0.000001744514
    70    70     -0.000000000081      0.000000018467
    71    71      0.000000000067     -0.000000015275
    72    72      0.000000155719     -0.000035321440
    73    73      0.000000000090     -0.000000020386
    74    74      0.000000000455     -0.000000104081
    75    75     -0.068481729960     15.602639419437
    76    76     -0.000000000052      0.000000011925
    77    77     -0.078474890916     17.876986435273
    78    78      0.000000000892     -0.000000203371
    79    79     -0.000000002529      0.000000552874
    80    80     -0.000000000036      0.000000008199
    81    81     -0.000000014577      0.000003296180
    82    82     -0.000000000284      0.000000064578
    83    83     -0.009869057979      2.249230061514
    84    84      0.011581871341     -2.640342769632
    85    85      0.000000000260     -0.000000059000
    86    86      0.000000000153     -0.000000034698
    87    87     -0.000000000067      0.000000015239
    88    88     -0.000000004138      0.000000922249
    89    89     -0.000000000508      0.000000115503
    90    90      0.017770082021     -4.054523549528
    91    91      0.000000088169     -0.000019990157
    92    92      0.000000000073     -0.000000016527
    93    93      0.000000077024     -0.000017463941
    94    94     -0.053251679333     12.133174074953
    95    95      0.000000001239     -0.000000281986
    96    96      0.000000005296     -0.000001184490
    97    97      0.000000001901     -0.000000432769
    98    98     -0.048330395024     11.009144881373
    99    99     -0.000000000045      0.000000010266
   100   100      0.000000005259     -0.000001176002
   101   101     -0.000000003506      0.000000776690
   102   102     -0.000000000069      0.000000015703
   103   103      0.018188534180     -4.149641050254
   104   104     -0.000000000675      0.000000153794
   105   105     -0.000000016590      0.000003803931

 number of reference csfs (nref) is   105.  root number (iroot) is  3.
 c0**2 =   0.93542083  c**2 (all zwalks) =   0.94834861

 pople ci energy extrapolation is computed with 32 correlated electrons.

 eref      =   -227.524093248728   "relaxed" cnot**2         =   0.935420827424
 eci       =   -227.623178124219   deltae = eci - eref       =  -0.099084875491
 eci+dv1   =   -227.629576943493   dv1 = (1-cnot**2)*deltae  =  -0.006398819274
 eci+dv2   =   -227.630018702369   dv2 = dv1 / cnot**2       =  -0.006840578151
 eci+dv3   =   -227.630525980443   dv3 = dv1 / (2*cnot**2-1) =  -0.007347856224
 eci+pople =   -227.630001313148   ( 32e- scaled deltae )    =  -0.105908064420
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
                       Size (real*8) of d2temp for two-external contributions     169767
 bummer (warning):inf%bufszi too small ... resetting to                    1027
 
                       Size (real*8) of d2temp for all-internal contributions      17442
                       Size (real*8) of d2temp for one-external contributions      88920
                       Size (real*8) of d2temp for two-external contributions     169767
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   27     9750
                1    2    1    1   27     9750
                1    3    1    1   27     9750
                1    4    1    1   27     9750
                1    5    1    1   27     9750
                1    6    1    1   27     9750
                1    7    1    1   27     9750
                1    8    1    1   27     9750
                1    9    1    1   27     9750
                1   10    1    1   27     9750
                1   11    1    1   27     9750
                1   12    1    1   27     9750
                1   13    1    1   27     9750
                1   14    1    1   27     9750
                1   15    1    1   27     9750
                1   16    1    1   27     9750
                1   17    1    1   27     9750
                1   18    1    1   27     9750
                       Size (real*8) of d2temp for three-external contributions     175500
                       Size (real*8) of d2temp for four-external contributions      71253
 serial operation: forcing vdisk for temporary dd012 matrices
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=  278318
 files%d4ext =     unit=  24  vdsk=   1  filestart=  458318
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                  1028     18487    107836         1         1
d2rec                    17        87       166         3         2
recsize                1027      1027      1027     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                 578318  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -227.77490368  -328.36045595
 residuum:     0.00000660
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096617    -0.10925050     0.02033649    -0.09583886     0.00281438
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017356    -0.00509415     0.00022824    -0.00112722     0.00002951
 ref:   3     0.00000000    -0.00000007    -0.96015319    -0.00001233     0.00010261     0.12474265     0.01613321    -0.01469782

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096617    -0.10925050     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017356    -0.00509415     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000    -0.00000007    -0.96015319    -0.00001233     0.00010261     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
executing partial task #   1  11
skipping partial task #   2  91
executing partial task #   1   1
skipping partial task #   2  81
executing partial task #   1   2
skipping partial task #   2  21
skipping partial task #   3 101
skipping partial task #   4  82
executing partial task #   1  71
executing partial task #   1  52
skipping partial task #   2  62
executing partial task #   3  72
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 0.00to den1(    1)=   2.0000000
adding (  2) 0.00to den1(    3)=   2.0000000
adding (  3) 0.00to den1(    6)=   2.0000000
adding (  4) 0.00to den1(   10)=   2.0000000
adding (  5) 0.00to den1(   15)=   1.9963946
adding (  6) 0.00to den1(   21)=   1.9963860
adding (  7) 0.00to den1(   28)=   1.9966896
adding (  8) 0.00to den1(   36)=   1.9966508
adding (  9) 0.00to den1(   45)=   1.9984250
adding ( 10) 0.00to den1(   55)=   1.9983765
adding ( 11) 0.00to den1(   66)=   1.9928634
adding ( 12) 0.00to den1(   78)=   1.9928337
adding ( 13) 0.00to den1(   91)=   1.9241374
adding ( 14) 0.00to den1(  105)=   1.9208021
adding ( 15) 0.00to den1(  120)=   1.9911320
adding ( 16) 0.00to den1(  136)=   1.9911823
adding ( 17) 0.00to den1(  153)=   0.0797149
adding ( 18) 0.00to den1(  171)=   0.0761718
--------------------------------------------------------------------------------
  2e-density  for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(     5)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    11)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    19)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    29)=  1.99278926 rmuval=  0.00000000den1=  1.99639463 fact=  0.00000000
d2il(    41)=  1.99277191 rmuval=  0.00000000den1=  1.99638595 fact=  0.00000000
d2il(    55)=  1.99337915 rmuval=  0.00000000den1=  1.99668957 fact=  0.00000000
d2il(    71)=  1.99330153 rmuval=  0.00000000den1=  1.99665076 fact=  0.00000000
d2il(    89)=  1.99684995 rmuval=  0.00000000den1=  1.99842498 fact=  0.00000000
d2il(   109)=  1.99675295 rmuval=  0.00000000den1=  1.99837648 fact=  0.00000000
d2il(   131)=  1.98572686 rmuval=  0.00000000den1=  1.99286343 fact=  0.00000000
d2il(   155)=  1.98566749 rmuval=  0.00000000den1=  1.99283375 fact=  0.00000000
d2il(   181)=  1.87314523 rmuval=  0.00000000den1=  1.92413741 fact=  0.00000000
d2il(   209)=  1.86958059 rmuval=  0.00000000den1=  1.92080209 fact=  0.00000000
d2il(   239)=  1.98309871 rmuval=  0.00000000den1=  1.99113197 fact=  0.00000000
d2il(   271)=  1.98321302 rmuval=  0.00000000den1=  1.99118228 fact=  0.00000000
d2il(   305)=  0.02739171 rmuval=  0.00000000den1=  0.07971486 fact=  0.00000000
d2il(   341)=  0.02478795 rmuval=  0.00000000den1=  0.07617179 fact=  0.00000000
Trace of MO density:    32.000000
   32  correlated and     0  frozen core electrons

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99877590     1.99873573     1.99840296     1.99838402
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99679960     1.99676003     1.99185141     1.99182897     1.99117454     1.99115643     1.92905392     1.92501028
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.07582487     0.07268622     0.00941363     0.00939697     0.00683386     0.00659674     0.00211552     0.00205737
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00165349     0.00149676     0.00084609     0.00078277     0.00055756     0.00044121     0.00043136     0.00032944
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00013512     0.00012847     0.00010991     0.00010715     0.00005394     0.00003292     0.00000976     0.00000832
              MO    41       MO    42       MO    43       MO    44       MO
  occ(*)=     0.00000719     0.00000496     0.00000265     0.00000199


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
      _ s       0.000362   0.000315   0.999685   0.999809   0.140928   0.135972
      _ p       0.000382   0.000356   0.000003   0.000003   0.065779   0.063812
     1_ s       0.000363   0.000315   0.999549   0.999944   0.140929   0.135971
     1_ p       0.000383   0.000356   0.000003   0.000003   0.065779   0.063812
     2_ s       0.998715   0.999863   0.000019  -0.000079   0.721641   0.727268
     2_ p       0.000002   0.000002  -0.000181  -0.000270   0.069459   0.070496
     3_ s       0.999792   0.998787   0.000019  -0.000079   0.721642   0.727268
     3_ p       0.000002   0.000002  -0.000181  -0.000270   0.069459   0.070496
     3_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
 
   ao class       7a         8a         9a        10a        11a        12a  
      _ s       0.441562   0.445809   0.000000   0.000000   0.021073   0.020584
      _ p       0.088987   0.085046   0.538865   0.543465   0.226491   0.230339
     1_ s       0.441562   0.445809   0.000000   0.000000   0.021073   0.020584
     1_ p       0.088987   0.085046   0.538865   0.543465   0.226491   0.230339
     2_ s       0.125726   0.127616   0.000000   0.000000   0.118769   0.118784
     2_ p       0.087007   0.088844   0.059031   0.059621   0.530613   0.526040
     3_ s       0.125726   0.127616   0.000000   0.000000   0.118769   0.118784
     3_ p       0.087007   0.088844   0.059031   0.059621   0.530614   0.526040
     3_ s       0.127959   0.125939   0.200252   0.197647   0.049489   0.050084
     4_ s       0.127959   0.125939   0.200252   0.197647   0.049489   0.050084
     4_ s       0.127959   0.125939   0.200252   0.197647   0.049489   0.050084
     4_ s       0.127959   0.125939   0.200252   0.197647   0.049489   0.050084
 
   ao class      13a        14a        15a        16a        17a        18a  
      _ s       0.000157   0.000208   0.000000   0.000000   0.000001  -0.000004
      _ p       0.355023   0.336296   0.003182   0.003513   0.024676   0.024211
     1_ s       0.000157   0.000208   0.000000   0.000000   0.000001  -0.000004
     1_ p       0.355023   0.336296   0.003182   0.003513   0.024676   0.024211
     2_ s      -0.000033   0.000004   0.000000   0.000000   0.000000   0.000000
     2_ p       0.640399   0.659039   0.858130   0.857662   0.013227   0.012127
     3_ s      -0.000033   0.000004   0.000000   0.000000   0.000000   0.000000
     3_ p       0.640399   0.659039   0.858131   0.857662   0.013227   0.012127
     3_ s       0.000021   0.000016   0.051607   0.050665   0.000004   0.000005
     4_ s       0.000021   0.000016   0.051607   0.050665   0.000004   0.000005
     4_ s       0.000021   0.000016   0.051607   0.050665   0.000004   0.000005
     4_ s       0.000021   0.000016   0.051607   0.050665   0.000004   0.000005
 
   ao class      19a        20a        21a        22a        23a        24a  
      _ s      -0.000114  -0.000054   0.000000   0.001178   0.000000   0.000304
      _ p      -0.000090  -0.000089  -0.000153   0.003373  -0.000037   0.001120
     1_ s      -0.000114  -0.000054   0.000000   0.001178   0.000000   0.000304
     1_ p      -0.000090  -0.000089  -0.000153   0.003373  -0.000037   0.001120
     2_ s       0.000002   0.000002   0.000000  -0.001153   0.000000  -0.000370
     2_ p       0.000013   0.000011   0.000090  -0.000108   0.000027  -0.000022
     3_ s       0.000002   0.000002   0.000000  -0.001153   0.000000  -0.000370
     3_ p       0.000013   0.000011   0.000090  -0.000108   0.000027  -0.000022
     3_ s       0.002448   0.002414   0.001740   0.000004   0.000534  -0.000002
     4_ s       0.002448   0.002414   0.001740   0.000004   0.000534  -0.000002
     4_ s       0.002448   0.002414   0.001740   0.000004   0.000534  -0.000002
     4_ s       0.002448   0.002414   0.001740   0.000004   0.000534  -0.000002
 
   ao class      25a        26a        27a        28a        29a        30a  
      _ s       0.000064   0.000144   0.000000   0.000239   0.000000   0.000036
      _ p       0.000778   0.000610   0.000528   0.000134   0.000343   0.000125
     1_ s       0.000064   0.000144   0.000000   0.000239   0.000000   0.000036
     1_ p       0.000778   0.000610   0.000528   0.000134   0.000343   0.000125
     2_ s       0.000001   0.000003   0.000000   0.000002   0.000000   0.000011
     2_ p      -0.000014  -0.000018  -0.000008   0.000001   0.000002   0.000043
     3_ s       0.000001   0.000003   0.000000   0.000002   0.000000   0.000011
     3_ p      -0.000014  -0.000018  -0.000008   0.000001   0.000002   0.000043
     3_ s      -0.000001   0.000004  -0.000048   0.000008  -0.000033   0.000003
     4_ s      -0.000001   0.000004  -0.000048   0.000008  -0.000033   0.000003
     4_ s      -0.000001   0.000004  -0.000048   0.000008  -0.000033   0.000003
     4_ s      -0.000001   0.000004  -0.000048   0.000008  -0.000033   0.000003
 
   ao class      31a        32a        33a        34a        35a        36a  
      _ s       0.000054   0.000037   0.000017   0.000000   0.000000   0.000027
      _ p       0.000087   0.000043   0.000003   0.000017   0.000012   0.000001
     1_ s       0.000054   0.000037   0.000017   0.000000   0.000000   0.000027
     1_ p       0.000087   0.000043   0.000003   0.000017   0.000012   0.000001
     2_ s       0.000008   0.000003  -0.000001   0.000000   0.000000  -0.000001
     2_ p       0.000065   0.000024   0.000017   0.000005   0.000007   0.000019
     3_ s       0.000008   0.000003  -0.000001   0.000000   0.000000  -0.000001
     3_ p       0.000065   0.000024   0.000017   0.000005   0.000007   0.000019
     3_ s       0.000001   0.000029   0.000016   0.000021   0.000018   0.000003
     4_ s       0.000001   0.000029   0.000016   0.000021   0.000018   0.000003
     4_ s       0.000001   0.000029   0.000016   0.000021   0.000018   0.000003
     4_ s       0.000001   0.000029   0.000016   0.000021   0.000018   0.000003
 
   ao class      37a        38a        39a        40a        41a        42a  
      _ s       0.000001   0.000000   0.000001   0.000000   0.000000   0.000000
      _ p       0.000000   0.000002   0.000001   0.000000   0.000000   0.000000
     1_ s       0.000001   0.000000   0.000001   0.000000   0.000000   0.000000
     1_ p       0.000000   0.000002   0.000001   0.000000   0.000000   0.000000
     2_ s       0.000000   0.000001   0.000000   0.000000   0.000000   0.000000
     2_ p       0.000024   0.000013   0.000003   0.000004   0.000003   0.000002
     3_ s       0.000000   0.000001   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000024   0.000013   0.000003   0.000004   0.000003   0.000002
     3_ s       0.000001   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s       0.000001   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s       0.000001   0.000000   0.000000   0.000000   0.000000   0.000000
     4_ s       0.000001   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      43a        44a  
      _ s       0.000000   0.000000
      _ p       0.000000   0.000000
     1_ s       0.000000   0.000000
     1_ p       0.000000   0.000000
     2_ s       0.000002   0.000001
     2_ p       0.000000   0.000000
     3_ s       0.000002   0.000001
     3_ p       0.000000   0.000000
     3_ s       0.000000   0.000000
     4_ s       0.000000   0.000000
     4_ s       0.000000   0.000000
     4_ s       0.000000   0.000000


                        gross atomic populations
     ao             _         1_         2_         3_         3_         4_
      s         3.208397   3.208397   3.936804   3.936804   0.863053   0.863053
      p         2.597238   2.597238   4.531454   4.531454   0.000000   0.000000
    total       5.805635   5.805635   8.468259   8.468259   0.863053   0.863053
 
 
     ao            4_         4_
      s         0.863053   0.863053
    total       0.863053   0.863053
 

 Total number of electrons:   32.00000000

 item #                     2 suffix=:.drt1.state2:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -227.77490368  -328.36045595
 residuum:     0.00000660
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -227.62386323  -328.20941550
 residuum:     0.00000889
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096617    -0.10925050     0.02033649    -0.09583886     0.00281438
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017356    -0.00509415     0.00022824    -0.00112722     0.00002951
 ref:   3     0.00000000    -0.00000007    -0.96015319    -0.00001233     0.00010261     0.12474265     0.01613321    -0.01469782

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096617    -0.10925050     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017356    -0.00509415     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000    -0.00000007    -0.96015319    -0.00001233     0.00010261     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
executing partial task #   1  11
skipping partial task #   2  91
executing partial task #   1   1
skipping partial task #   2  81
executing partial task #   1   2
skipping partial task #   2  21
skipping partial task #   3 101
skipping partial task #   4  82
executing partial task #   1  71
executing partial task #   1  52
skipping partial task #   2  62
executing partial task #   3  72
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 0.00to den1(    1)=   2.0000000
adding (  2) 0.00to den1(    3)=   2.0000000
adding (  3) 0.00to den1(    6)=   2.0000000
adding (  4) 0.00to den1(   10)=   2.0000000
adding (  5) 0.00to den1(   15)=   1.9967908
adding (  6) 0.00to den1(   21)=   1.9967806
adding (  7) 0.00to den1(   28)=   1.9951846
adding (  8) 0.00to den1(   36)=   1.9950933
adding (  9) 0.00to den1(   45)=   1.9895844
adding ( 10) 0.00to den1(   55)=   1.9867875
adding ( 11) 0.00to den1(   66)=   1.9942934
adding ( 12) 0.00to den1(   78)=   1.9942553
adding ( 13) 0.00to den1(   91)=   1.9421096
adding ( 14) 0.00to den1(  105)=   1.9376387
adding ( 15) 0.00to den1(  120)=   1.5768023
adding ( 16) 0.00to den1(  136)=   1.4272644
adding ( 17) 0.00to den1(  153)=   0.6385622
adding ( 18) 0.00to den1(  171)=   0.4770159
--------------------------------------------------------------------------------
  2e-density  for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(     5)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    11)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    19)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    29)=  1.99358158 rmuval=  0.00000000den1=  1.99679079 fact=  0.00000000
d2il(    41)=  1.99356119 rmuval=  0.00000000den1=  1.99678060 fact=  0.00000000
d2il(    55)=  1.99036915 rmuval=  0.00000000den1=  1.99518458 fact=  0.00000000
d2il(    71)=  1.99018660 rmuval=  0.00000000den1=  1.99509330 fact=  0.00000000
d2il(    89)=  1.97916871 rmuval=  0.00000000den1=  1.98958436 fact=  0.00000000
d2il(   109)=  1.97357500 rmuval=  0.00000000den1=  1.98678750 fact=  0.00000000
d2il(   131)=  1.98858685 rmuval=  0.00000000den1=  1.99429342 fact=  0.00000000
d2il(   155)=  1.98851055 rmuval=  0.00000000den1=  1.99425527 fact=  0.00000000
d2il(   181)=  1.89719427 rmuval=  0.00000000den1=  1.94210962 fact=  0.00000000
d2il(   209)=  1.88966917 rmuval=  0.00000000den1=  1.93763874 fact=  0.00000000
d2il(   239)=  1.15676460 rmuval=  0.00000000den1=  1.57680230 fact=  0.00000000
d2il(   271)=  0.85763855 rmuval=  0.00000000den1=  1.42726438 fact=  0.00000000
d2il(   305)=  0.05909574 rmuval=  0.00000000den1=  0.63856217 fact=  0.00000000
d2il(   341)=  0.04697885 rmuval=  0.00000000den1=  0.47701592 fact=  0.00000000
Trace of MO density:    32.000000
   32  correlated and     0  frozen core electrons

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99859277     1.99857102     1.99478779     1.99468974
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99293865     1.99291899     1.99199376     1.99131656     1.94340982     1.93865289     1.57495939     1.42309014
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.63802005     0.47639159     0.00807082     0.00802706     0.00777015     0.00723726     0.00373207     0.00366955
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00213979     0.00157488     0.00147900     0.00130077     0.00084719     0.00084155     0.00080361     0.00052581
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00047146     0.00033818     0.00025363     0.00021324     0.00013991     0.00012333     0.00003167     0.00002512
              MO    41       MO    42       MO    43       MO    44       MO
  occ(*)=     0.00002424     0.00001666     0.00000554     0.00000437


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
      _ s       0.000362   0.000315   0.999685   0.999809   0.140916   0.135960
      _ p       0.000382   0.000356   0.000003   0.000003   0.065773   0.063806
     1_ s       0.000363   0.000315   0.999549   0.999944   0.140916   0.135960
     1_ p       0.000383   0.000356   0.000003   0.000003   0.065773   0.063806
     2_ s       0.998715   0.999863   0.000019  -0.000079   0.721575   0.727208
     2_ p       0.000002   0.000002  -0.000181  -0.000270   0.069453   0.070491
     3_ s       0.999792   0.998787   0.000019  -0.000079   0.721575   0.727208
     3_ p       0.000002   0.000002  -0.000181  -0.000270   0.069453   0.070491
     3_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
 
   ao class       7a         8a         9a        10a        11a        12a  
      _ s       0.440763   0.444985   0.000000   0.000000   0.021074   0.020579
      _ p       0.088826   0.084888   0.537823   0.542420   0.226508   0.230280
     1_ s       0.440763   0.444985   0.000000   0.000000   0.021074   0.020579
     1_ p       0.088826   0.084888   0.537823   0.542420   0.226508   0.230280
     2_ s       0.125499   0.127380   0.000000   0.000000   0.118778   0.118753
     2_ p       0.086850   0.088680   0.058917   0.059506   0.530651   0.525904
     3_ s       0.125499   0.127380   0.000000   0.000000   0.118778   0.118753
     3_ p       0.086850   0.088680   0.058917   0.059506   0.530652   0.525904
     3_ s       0.127728   0.125706   0.199865   0.197267   0.049493   0.050071
     4_ s       0.127728   0.125706   0.199865   0.197267   0.049493   0.050071
     4_ s       0.127728   0.125706   0.199865   0.197267   0.049493   0.050071
     4_ s       0.127728   0.125706   0.199865   0.197267   0.049493   0.050071
 
   ao class      13a        14a        15a        16a        17a        18a  
      _ s       0.000154   0.000203   0.000000   0.000000   0.000010  -0.000028
      _ p       0.346506   0.327428   0.002598   0.002597   0.207631   0.158682
     1_ s       0.000154   0.000203   0.000000   0.000000   0.000010  -0.000028
     1_ p       0.346506   0.327428   0.002598   0.002597   0.207631   0.158682
     2_ s      -0.000032   0.000004   0.000000   0.000000   0.000001   0.000001
     2_ p       0.625037   0.641661   0.700613   0.634038   0.111295   0.079481
     3_ s      -0.000032   0.000004   0.000000   0.000000   0.000001   0.000001
     3_ p       0.625037   0.641661   0.700613   0.634038   0.111295   0.079481
     3_ s       0.000020   0.000015   0.042134   0.037455   0.000036   0.000030
     4_ s       0.000020   0.000015   0.042134   0.037455   0.000036   0.000030
     4_ s       0.000020   0.000015   0.042134   0.037455   0.000036   0.000030
     4_ s       0.000020   0.000015   0.042134   0.037455   0.000036   0.000030
 
   ao class      19a        20a        21a        22a        23a        24a  
      _ s      -0.000098  -0.000046   0.000000   0.001293   0.000000   0.000542
      _ p      -0.000077  -0.000076  -0.000174   0.003700  -0.000065   0.001999
     1_ s      -0.000098  -0.000046   0.000000   0.001293   0.000000   0.000542
     1_ p      -0.000077  -0.000076  -0.000174   0.003700  -0.000065   0.001999
     2_ s       0.000001   0.000002   0.000000  -0.001265   0.000000  -0.000659
     2_ p       0.000011   0.000009   0.000103  -0.000119   0.000047  -0.000039
     3_ s       0.000001   0.000002   0.000000  -0.001265   0.000000  -0.000659
     3_ p       0.000011   0.000009   0.000103  -0.000119   0.000047  -0.000039
     3_ s       0.002099   0.002062   0.001978   0.000005   0.000942  -0.000004
     4_ s       0.002099   0.002062   0.001978   0.000005   0.000942  -0.000004
     4_ s       0.002099   0.002062   0.001978   0.000005   0.000942  -0.000004
     4_ s       0.002099   0.002062   0.001978   0.000005   0.000942  -0.000004
 
   ao class      25a        26a        27a        28a        29a        30a  
      _ s       0.000083   0.000152   0.000000   0.000397   0.000000   0.000069
      _ p       0.001007   0.000642   0.000922   0.000223   0.000521   0.000238
     1_ s       0.000083   0.000152   0.000000   0.000397   0.000000   0.000069
     1_ p       0.001007   0.000642   0.000922   0.000223   0.000521   0.000238
     2_ s       0.000001   0.000003   0.000000   0.000003   0.000000   0.000020
     2_ p      -0.000018  -0.000019  -0.000015   0.000002   0.000003   0.000082
     3_ s       0.000001   0.000003   0.000000   0.000003   0.000000   0.000020
     3_ p      -0.000018  -0.000019  -0.000015   0.000002   0.000003   0.000082
     3_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000051   0.000006
     4_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000051   0.000006
     4_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000051   0.000006
     4_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000051   0.000006
 
   ao class      31a        32a        33a        34a        35a        36a  
      _ s       0.000101   0.000059   0.000060   0.000000   0.000000   0.000054
      _ p       0.000162   0.000069   0.000010   0.000045   0.000029   0.000002
     1_ s       0.000101   0.000059   0.000060   0.000000   0.000000   0.000054
     1_ p       0.000162   0.000069   0.000010   0.000045   0.000029   0.000002
     2_ s       0.000014   0.000005  -0.000002   0.000000   0.000000  -0.000001
     2_ p       0.000121   0.000038   0.000058   0.000014   0.000016   0.000039
     3_ s       0.000014   0.000005  -0.000002   0.000000   0.000000  -0.000001
     3_ p       0.000121   0.000038   0.000058   0.000014   0.000016   0.000039
     3_ s       0.000002   0.000046   0.000055   0.000055   0.000041   0.000007
     4_ s       0.000002   0.000046   0.000055   0.000055   0.000041   0.000007
     4_ s       0.000002   0.000046   0.000055   0.000055   0.000041   0.000007
     4_ s       0.000002   0.000046   0.000055   0.000055   0.000041   0.000007
 
   ao class      37a        38a        39a        40a        41a        42a  
      _ s       0.000002   0.000001   0.000003   0.000000   0.000000   0.000000
      _ p       0.000000   0.000007   0.000002   0.000000   0.000000   0.000000
     1_ s       0.000002   0.000001   0.000003   0.000000   0.000000   0.000000
     1_ p       0.000000   0.000007   0.000002   0.000000   0.000000   0.000000
     2_ s       0.000001   0.000003   0.000001   0.000000   0.000000   0.000000
     2_ p       0.000062   0.000050   0.000008   0.000013   0.000012   0.000007
     3_ s       0.000001   0.000003   0.000001   0.000000   0.000000   0.000000
     3_ p       0.000062   0.000050   0.000008   0.000013   0.000012   0.000007
     3_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
     4_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
     4_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
     4_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
 
   ao class      43a        44a  
      _ s       0.000000   0.000000
      _ p      -0.000001  -0.000001
     1_ s       0.000000   0.000000
     1_ p      -0.000001  -0.000001
     2_ s       0.000004   0.000003
     2_ p       0.000000   0.000000
     3_ s       0.000004   0.000003
     3_ p       0.000000   0.000000
     3_ s       0.000000   0.000000
     4_ s       0.000000   0.000000
     4_ s       0.000000   0.000000
     4_ s       0.000000   0.000000


                        gross atomic populations
     ao             _         1_         2_         3_         3_         4_
      s         3.207458   3.207458   3.935817   3.935817   0.839204   0.839204
      p         2.895698   2.895697   4.282619   4.282619   0.000000   0.000000
    total       6.103156   6.103156   8.218437   8.218436   0.839204   0.839204
 
 
     ao            4_         4_
      s         0.839204   0.839204
    total       0.839204   0.839204
 

 Total number of electrons:   32.00000001

 item #                     3 suffix=:.drt1.state3:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 97% root-following 0
 MR-CISD energy:  -227.77490368  -328.36045595
 residuum:     0.00000660
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  0max overlap with ref# 96% root-following 0
 MR-CISD energy:  -227.62386323  -328.20941550
 residuum:     0.00000889
 deltae:     0.00000000
================================================================================
  Reading record                      3  of civout
 INFO:ref#  3vector#  3 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:  -227.62317812  -328.20873040
 residuum:     0.00000675
 deltae:     0.00000000
 apxde:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096617    -0.10925050     0.02033649    -0.09583886     0.00281438
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017356    -0.00509415     0.00022824    -0.00112722     0.00002951
 ref:   3     0.00000000    -0.00000007    -0.96015319    -0.00001233     0.00010261     0.12474265     0.01613321    -0.01469782

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97036999     0.00000004     0.00000004    -0.00096617    -0.10925050     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000    -0.95971814     0.00000007    -0.00017356    -0.00509415     0.00000000     0.00000000     0.00000000
 ref:   3     0.00000000    -0.00000007    -0.96015319    -0.00001233     0.00010261     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    3
--------------------------------------------------------------------------------
executing partial task #   1  11
skipping partial task #   2  91
executing partial task #   1   1
skipping partial task #   2  81
executing partial task #   1   2
skipping partial task #   2  21
skipping partial task #   3 101
skipping partial task #   4  82
executing partial task #   1  71
executing partial task #   1  52
skipping partial task #   2  62
executing partial task #   3  72
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 0.00to den1(    1)=   2.0000000
adding (  2) 0.00to den1(    3)=   2.0000000
adding (  3) 0.00to den1(    6)=   2.0000000
adding (  4) 0.00to den1(   10)=   2.0000000
adding (  5) 0.00to den1(   15)=   1.9967887
adding (  6) 0.00to den1(   21)=   1.9967795
adding (  7) 0.00to den1(   28)=   1.9951629
adding (  8) 0.00to den1(   36)=   1.9951150
adding (  9) 0.00to den1(   45)=   1.9873014
adding ( 10) 0.00to den1(   55)=   1.9894399
adding ( 11) 0.00to den1(   66)=   1.9942947
adding ( 12) 0.00to den1(   78)=   1.9942627
adding ( 13) 0.00to den1(   91)=   1.9419948
adding ( 14) 0.00to den1(  105)=   1.9376285
adding ( 15) 0.00to den1(  120)=   1.4386668
adding ( 16) 0.00to den1(  136)=   1.5650480
adding ( 17) 0.00to den1(  153)=   0.6269052
adding ( 18) 0.00to den1(  171)=   0.4889604
--------------------------------------------------------------------------------
  2e-density  for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(     5)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    11)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    19)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(    29)=  1.99357747 rmuval=  0.00000000den1=  1.99678874 fact=  0.00000000
d2il(    41)=  1.99355906 rmuval=  0.00000000den1=  1.99677953 fact=  0.00000000
d2il(    55)=  1.99032572 rmuval=  0.00000000den1=  1.99516286 fact=  0.00000000
d2il(    71)=  1.99023004 rmuval=  0.00000000den1=  1.99511502 fact=  0.00000000
d2il(    89)=  1.97460272 rmuval=  0.00000000den1=  1.98730136 fact=  0.00000000
d2il(   109)=  1.97887985 rmuval=  0.00000000den1=  1.98943992 fact=  0.00000000
d2il(   131)=  1.98858939 rmuval=  0.00000000den1=  1.99429469 fact=  0.00000000
d2il(   155)=  1.98852540 rmuval=  0.00000000den1=  1.99426270 fact=  0.00000000
d2il(   181)=  1.89698873 rmuval=  0.00000000den1=  1.94199476 fact=  0.00000000
d2il(   209)=  1.88963379 rmuval=  0.00000000den1=  1.93762854 fact=  0.00000000
d2il(   239)=  0.88048656 rmuval=  0.00000000den1=  1.43866683 fact=  0.00000000
d2il(   271)=  1.13334362 rmuval=  0.00000000den1=  1.56504801 fact=  0.00000000
d2il(   305)=  0.05862408 rmuval=  0.00000000den1=  0.62690520 fact=  0.00000000
d2il(   341)=  0.04757567 rmuval=  0.00000000den1=  0.48896044 fact=  0.00000000
Trace of MO density:    32.000000
   32  correlated and     0  frozen core electrons

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.99859382     1.99857545     1.99476625     1.99470911
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99293677     1.99292251     1.99189280     1.99155907     1.94327364     1.93868932     1.56311643     1.43477773
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.62631423     0.48831433     0.00807612     0.00803501     0.00748513     0.00746664     0.00373460     0.00367433
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00209616     0.00157872     0.00147384     0.00130002     0.00084483     0.00083260     0.00080711     0.00052665
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00046943     0.00033692     0.00023492     0.00022370     0.00013578     0.00012184     0.00003069     0.00002421
              MO    41       MO    42       MO    43       MO    44       MO
  occ(*)=     0.00001961     0.00001934     0.00000588     0.00000445


 total number of electrons =   32.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
      _ s       0.000362   0.000315   0.999685   0.999809   0.140916   0.135961
      _ p       0.000382   0.000356   0.000003   0.000003   0.065773   0.063806
     1_ s       0.000363   0.000315   0.999549   0.999944   0.140916   0.135961
     1_ p       0.000383   0.000356   0.000003   0.000003   0.065773   0.063806
     2_ s       0.998715   0.999863   0.000019  -0.000079   0.721575   0.727210
     2_ p       0.000002   0.000002  -0.000181  -0.000270   0.069453   0.070491
     3_ s       0.999792   0.998787   0.000019  -0.000079   0.721576   0.727209
     3_ p       0.000002   0.000002  -0.000181  -0.000270   0.069453   0.070491
     3_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000910
 
   ao class       7a         8a         9a        10a        11a        12a  
      _ s       0.440759   0.444989   0.000000   0.000000   0.021073   0.020581
      _ p       0.088825   0.084889   0.537823   0.542421   0.226496   0.230308
     1_ s       0.440759   0.444989   0.000000   0.000000   0.021073   0.020581
     1_ p       0.088825   0.084889   0.537823   0.542421   0.226496   0.230308
     2_ s       0.125497   0.127381   0.000000   0.000000   0.118772   0.118768
     2_ p       0.086849   0.088681   0.058917   0.059507   0.530625   0.525969
     3_ s       0.125497   0.127381   0.000000   0.000000   0.118772   0.118768
     3_ p       0.086849   0.088681   0.058917   0.059507   0.530625   0.525968
     3_ s       0.127727   0.125707   0.199864   0.197267   0.049490   0.050077
     4_ s       0.127727   0.125707   0.199864   0.197267   0.049490   0.050077
     4_ s       0.127727   0.125707   0.199864   0.197267   0.049490   0.050077
     4_ s       0.127727   0.125707   0.199864   0.197267   0.049490   0.050077
 
   ao class      13a        14a        15a        16a        17a        18a  
      _ s       0.000154   0.000203   0.000000   0.000000   0.000010  -0.000029
      _ p       0.346482   0.327435   0.002579   0.002618   0.203822   0.162654
     1_ s       0.000154   0.000203   0.000000   0.000000   0.000010  -0.000029
     1_ p       0.346482   0.327435   0.002579   0.002618   0.203822   0.162654
     2_ s      -0.000032   0.000004   0.000000   0.000000   0.000001   0.000001
     2_ p       0.624993   0.641673   0.695345   0.639245   0.109253   0.081470
     3_ s      -0.000032   0.000004   0.000000   0.000000   0.000001   0.000001
     3_ p       0.624993   0.641673   0.695345   0.639245   0.109253   0.081470
     3_ s       0.000020   0.000015   0.041817   0.037763   0.000036   0.000030
     4_ s       0.000020   0.000015   0.041817   0.037763   0.000036   0.000030
     4_ s       0.000020   0.000015   0.041817   0.037763   0.000036   0.000030
     4_ s       0.000020   0.000015   0.041817   0.037763   0.000036   0.000030
 
   ao class      19a        20a        21a        22a        23a        24a  
      _ s      -0.000098  -0.000046   0.000000   0.001334   0.000000   0.000543
      _ p      -0.000077  -0.000076  -0.000168   0.003818  -0.000065   0.002001
     1_ s      -0.000098  -0.000046   0.000000   0.001334   0.000000   0.000543
     1_ p      -0.000077  -0.000076  -0.000168   0.003818  -0.000065   0.002001
     2_ s       0.000001   0.000002   0.000000  -0.001305   0.000000  -0.000660
     2_ p       0.000011   0.000009   0.000099  -0.000123   0.000047  -0.000039
     3_ s       0.000001   0.000002   0.000000  -0.001305   0.000000  -0.000660
     3_ p       0.000011   0.000009   0.000099  -0.000123   0.000047  -0.000039
     3_ s       0.002100   0.002064   0.001906   0.000005   0.000942  -0.000004
     4_ s       0.002100   0.002064   0.001906   0.000005   0.000942  -0.000004
     4_ s       0.002100   0.002064   0.001906   0.000005   0.000942  -0.000004
     4_ s       0.002100   0.002064   0.001906   0.000005   0.000942  -0.000004
 
   ao class      25a        26a        27a        28a        29a        30a  
      _ s       0.000082   0.000152   0.000000   0.000397   0.000000   0.000069
      _ p       0.000986   0.000644   0.000919   0.000223   0.000520   0.000235
     1_ s       0.000082   0.000152   0.000000   0.000397   0.000000   0.000069
     1_ p       0.000986   0.000644   0.000919   0.000223   0.000520   0.000235
     2_ s       0.000001   0.000003   0.000000   0.000003   0.000000   0.000020
     2_ p      -0.000018  -0.000019  -0.000015   0.000002   0.000003   0.000081
     3_ s       0.000001   0.000003   0.000000   0.000003   0.000000   0.000020
     3_ p      -0.000018  -0.000019  -0.000015   0.000002   0.000003   0.000081
     3_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000050   0.000006
     4_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000050   0.000006
     4_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000050   0.000006
     4_ s      -0.000002   0.000004  -0.000084   0.000013  -0.000050   0.000006
 
   ao class      31a        32a        33a        34a        35a        36a  
      _ s       0.000101   0.000059   0.000060   0.000000   0.000000   0.000056
      _ p       0.000163   0.000069   0.000010   0.000045   0.000027   0.000002
     1_ s       0.000101   0.000059   0.000060   0.000000   0.000000   0.000056
     1_ p       0.000163   0.000069   0.000010   0.000045   0.000027   0.000002
     2_ s       0.000014   0.000005  -0.000002   0.000000   0.000000  -0.000002
     2_ p       0.000122   0.000039   0.000058   0.000014   0.000015   0.000041
     3_ s       0.000014   0.000005  -0.000002   0.000000   0.000000  -0.000002
     3_ p       0.000122   0.000039   0.000058   0.000014   0.000015   0.000041
     3_ s       0.000002   0.000046   0.000055   0.000055   0.000038   0.000007
     4_ s       0.000002   0.000046   0.000055   0.000055   0.000038   0.000007
     4_ s       0.000002   0.000046   0.000055   0.000055   0.000038   0.000007
     4_ s       0.000002   0.000046   0.000055   0.000055   0.000038   0.000007
 
   ao class      37a        38a        39a        40a        41a        42a  
      _ s       0.000002   0.000001   0.000003   0.000000   0.000000   0.000000
      _ p       0.000000   0.000007   0.000002   0.000000   0.000000   0.000000
     1_ s       0.000002   0.000001   0.000003   0.000000   0.000000   0.000000
     1_ p       0.000000   0.000007   0.000002   0.000000   0.000000   0.000000
     2_ s       0.000001   0.000003   0.000001   0.000000   0.000000   0.000000
     2_ p       0.000060   0.000049   0.000008   0.000012   0.000009   0.000009
     3_ s       0.000001   0.000003   0.000001   0.000000   0.000000   0.000000
     3_ p       0.000060   0.000049   0.000008   0.000012   0.000009   0.000009
     3_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
     4_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
     4_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
     4_ s       0.000002   0.000001   0.000001   0.000000   0.000000   0.000000
 
   ao class      43a        44a  
      _ s       0.000000   0.000000
      _ p      -0.000001  -0.000001
     1_ s       0.000000   0.000000
     1_ p      -0.000001  -0.000001
     2_ s       0.000004   0.000003
     2_ p       0.000000   0.000000
     3_ s       0.000004   0.000003
     3_ p       0.000000   0.000000
     3_ s       0.000000   0.000000
     4_ s       0.000000   0.000000
     4_ s       0.000000   0.000000
     4_ s       0.000000   0.000000


                        gross atomic populations
     ao             _         1_         2_         3_         3_         4_
      s         3.207501   3.207501   3.935787   3.935787   0.839127   0.839127
      p         2.895958   2.895958   4.282500   4.282500   0.000000   0.000000
    total       6.103459   6.103459   8.218287   8.218286   0.839127   0.839127
 
 
     ao            4_         4_
      s         0.839127   0.839127
    total       0.839127   0.839127
 

 Total number of electrons:   32.00000001

 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   2logvrecs(*)=   1   2
 item #                     4 suffix=:.trd1to2:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   2)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
executing partial task #   1  11
skipping partial task #   2  91
executing partial task #   1   1
skipping partial task #   2  81
executing partial task #   1   2
skipping partial task #   2  21
skipping partial task #   3 101
skipping partial task #   4  82
executing partial task #   1  71
executing partial task #   1  52
skipping partial task #   2  62
executing partial task #   3  72
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 0.00to den1(    1)=   0.0000000
adding (  2) 0.00to den1(    3)=   0.0000000
adding (  3) 0.00to den1(    6)=   0.0000000
adding (  4) 0.00to den1(   10)=   0.0000000
adding (  5) 0.00to den1(   15)=   0.0000000
adding (  6) 0.00to den1(   21)=   0.0000000
adding (  7) 0.00to den1(   28)=   0.0000000
adding (  8) 0.00to den1(   36)=   0.0000000
adding (  9) 0.00to den1(   45)=   0.0000000
adding ( 10) 0.00to den1(   55)=   0.0000000
adding ( 11) 0.00to den1(   66)=   0.0000000
adding ( 12) 0.00to den1(   78)=   0.0000000
adding ( 13) 0.00to den1(   91)=  -0.0000001
adding ( 14) 0.00to den1(  105)=  -0.0000001
adding ( 15) 0.00to den1(  120)=   0.0000000
adding ( 16) 0.00to den1(  136)=   0.0000000
adding ( 17) 0.00to den1(  153)=   0.0000001
adding ( 18) 0.00to den1(  171)=   0.0000001
2e-transition density (   1,   2)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(     5)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    11)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    19)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    29)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    41)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    55)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    71)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    89)=  0.00000001 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(   109)=  0.00000001 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(   131)=  0.00000001 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(   155)=  0.00000001 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(   181)= -0.00000009 rmuval=  0.00000000den1= -0.00000005 fact=  0.00000000
d2il(   209)= -0.00000009 rmuval=  0.00000000den1= -0.00000006 fact=  0.00000000
d2il(   239)=  0.00000002 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(   271)=  0.00000003 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(   305)=  0.00000001 rmuval=  0.00000000den1=  0.00000005 fact=  0.00000000
d2il(   341)=  0.00000001 rmuval=  0.00000000den1=  0.00000005 fact=  0.00000000
 maximum diagonal element=  2.701059585719090E-008
 accstate=                     4
 accpdens=                     4
logrecs(*)=   1   3logvrecs(*)=   1   3
 item #                     5 suffix=:.trd1to3:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
executing partial task #   1  11
skipping partial task #   2  91
executing partial task #   1   1
skipping partial task #   2  81
executing partial task #   1   2
skipping partial task #   2  21
skipping partial task #   3 101
skipping partial task #   4  82
executing partial task #   1  71
executing partial task #   1  52
skipping partial task #   2  62
executing partial task #   3  72
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 0.00to den1(    1)=   0.0000000
adding (  2) 0.00to den1(    3)=   0.0000000
adding (  3) 0.00to den1(    6)=   0.0000000
adding (  4) 0.00to den1(   10)=   0.0000000
adding (  5) 0.00to den1(   15)=   0.0000000
adding (  6) 0.00to den1(   21)=   0.0000000
adding (  7) 0.00to den1(   28)=   0.0000000
adding (  8) 0.00to den1(   36)=   0.0000000
adding (  9) 0.00to den1(   45)=   0.0000000
adding ( 10) 0.00to den1(   55)=   0.0000000
adding ( 11) 0.00to den1(   66)=   0.0000000
adding ( 12) 0.00to den1(   78)=   0.0000000
adding ( 13) 0.00to den1(   91)=   0.0000002
adding ( 14) 0.00to den1(  105)=   0.0000002
adding ( 15) 0.00to den1(  120)=   0.0000000
adding ( 16) 0.00to den1(  136)=   0.0000000
adding ( 17) 0.00to den1(  153)=  -0.0000002
adding ( 18) 0.00to den1(  171)=  -0.0000002
2e-transition density (   1,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(     5)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    11)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    19)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    29)=  0.00000002 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(    41)=  0.00000002 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(    55)=  0.00000002 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(    71)=  0.00000002 rmuval=  0.00000000den1=  0.00000001 fact=  0.00000000
d2il(    89)=  0.00000001 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(   109)=  0.00000001 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(   131)=  0.00000004 rmuval=  0.00000000den1=  0.00000002 fact=  0.00000000
d2il(   155)=  0.00000004 rmuval=  0.00000000den1=  0.00000002 fact=  0.00000000
d2il(   181)=  0.00000028 rmuval=  0.00000000den1=  0.00000017 fact=  0.00000000
d2il(   209)=  0.00000028 rmuval=  0.00000000den1=  0.00000017 fact=  0.00000000
d2il(   239)=  0.00000003 rmuval=  0.00000000den1=  0.00000002 fact=  0.00000000
d2il(   271)=  0.00000006 rmuval=  0.00000000den1=  0.00000003 fact=  0.00000000
d2il(   305)= -0.00000006 rmuval=  0.00000000den1= -0.00000017 fact=  0.00000000
d2il(   341)= -0.00000006 rmuval=  0.00000000den1= -0.00000017 fact=  0.00000000
 maximum diagonal element=  8.604278151080891E-008
 accstate=                     4
 accpdens=                     4
logrecs(*)=   2   3logvrecs(*)=   2   3
 item #                     6 suffix=:.trd2to3:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   2,   3)
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
executing partial task #   1  11
skipping partial task #   2  91
executing partial task #   1   1
skipping partial task #   2  81
executing partial task #   1   2
skipping partial task #   2  21
skipping partial task #   3 101
skipping partial task #   4  82
executing partial task #   1  71
executing partial task #   1  52
skipping partial task #   2  62
executing partial task #   3  72
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y=   35668  D0X=       0  D0W=       0
  DDZI=   11640 DDYI=   32580 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1890 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 0.00to den1(    1)=   0.0000000
adding (  2) 0.00to den1(    3)=   0.0000000
adding (  3) 0.00to den1(    6)=   0.0000000
adding (  4) 0.00to den1(   10)=   0.0000000
adding (  5) 0.00to den1(   15)=   0.0000000
adding (  6) 0.00to den1(   21)=   0.0000000
adding (  7) 0.00to den1(   28)=   0.0000000
adding (  8) 0.00to den1(   36)=   0.0000000
adding (  9) 0.00to den1(   45)=   0.0000000
adding ( 10) 0.00to den1(   55)=   0.0000000
adding ( 11) 0.00to den1(   66)=   0.0000000
adding ( 12) 0.00to den1(   78)=   0.0000000
adding ( 13) 0.00to den1(   91)=   0.0000000
adding ( 14) 0.00to den1(  105)=   0.0000000
adding ( 15) 0.00to den1(  120)=   0.0000000
adding ( 16) 0.00to den1(  136)=   0.0000000
adding ( 17) 0.00to den1(  153)=   0.0000000
adding ( 18) 0.00to den1(  171)=   0.0000000
2e-transition density (   2,   3)
--------------------------------------------------------------------------------
  2e-transition density (root #    2 -> root #   3)
--------------------------------------------------------------------------------
================================================================================
   DYZ=    8674  DYX=       0  DYW=       0
   D0Z=   10348  D0Y= 1017991  D0X=       0  D0W=       0
  DDZI=  107790 DDYI=  297450 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(     5)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    11)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    19)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    29)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    41)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    55)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    71)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(    89)= -0.00000001 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(   109)= -0.00000001 rmuval=  0.00000000den1= -0.00000001 fact=  0.00000000
d2il(   131)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(   155)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(   181)= -0.00000001 rmuval=  0.00000000den1= -0.00000001 fact=  0.00000000
d2il(   209)=  0.00000000 rmuval=  0.00000000den1= -0.00000001 fact=  0.00000000
d2il(   239)=  0.00000007 rmuval=  0.00000000den1=  0.00000003 fact=  0.00000000
d2il(   271)= -0.00000005 rmuval=  0.00000000den1= -0.00000003 fact=  0.00000000
d2il(   305)=  0.00000001 rmuval=  0.00000000den1= -0.00000002 fact=  0.00000000
d2il(   341)=  0.00000001 rmuval=  0.00000000den1=  0.00000004 fact=  0.00000000
 maximum diagonal element=  1.951159535329428E-008
========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0      68.6      68.8       0.5       0.0      76.9
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0      68.6      68.8       0.5       0.0      76.9
 DA ...
