

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       222822400 of real*8 words ( 1700.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=50,
   tol(3)=1.e-8,
   tol(2)=1.e-8,
   tol(1)=1.e-10
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,13,20,1,14,20,1,15,20,1,16,20,1,17,20,1,18,20
   NAVST(1) = 3,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   WAVST(1,3)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /scratch/plasserf/74907/WORK/aoints                      
    

 Integral file header information:
 Hermit Integral Program : SIFS version  hawk6.itc.univie. 15:40:44.981 05-Nov-11

 Core type energy values:
 energy( 1)=  1.005855522736E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =  100.585552274


   ******  Basis set information:  ******

 Number of irreps:                  1
 Total number of basis functions:  44

 irrep no.              1
 irrep label           A  
 no. of bas.fcions.    44
 warning: resetting tol(6) due to possible inconsistency.


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-10. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-08. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-08. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.2500E-09. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging information: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          3             0.333 0.333 0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        4

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1      13( 13)    20
       1      14( 14)    20
       1      15( 15)    20
       1      16( 16)    20
       1      17( 17)    20
       1      18( 18)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=50 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105
 

 faar:   0 active-active rotations allowed out of:  15 possible.


 Number of active-double rotations:        72
 Number of active-active rotations:         0
 Number of double-virtual rotations:      312
 Number of active-virtual rotations:      156

 Size of orbital-Hessian matrix B:                   152316
 Size of the orbital-state Hessian matrix C:         170100
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         322416


 Source of the initial MO coeficients:

 Input MO coefficient file: /scratch/plasserf/74907/WORK/mocoef                         
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    44, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222813728

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222706892
 address segment size,           sizesg = 222701436
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     249111 transformed 1/r12    array elements were written in      46 records.


 Size of orbital-Hessian matrix B:                   152316
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con         152316


 mosort: allocated sort2 space, avc2is=   222675578 available sort2 space, avcisx=   222675830

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=  45 noldhv= 10 noldv= 10

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -227.6831282544     -328.2686805280        0.0000000004        0.0000000013
    2      -227.5324465912     -328.1179988648        0.0000000012        0.0000000013
    3      -227.5320207595     -328.1175730331        0.0000000010        0.0000000013
    4      -227.4089739544     -327.9945262280        0.0001765185        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.437900683657302E-008
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-1.00000000 pnorm= 0.0000E+00 rznorm= 7.7608E-10 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -41.222529  -41.222522  -22.610593  -22.610567   -2.901461   -2.899636   -1.725318   -1.717514   -1.332982   -1.327002
    -1.319599   -1.315689

 qvv(*) eigenvalues. symmetry block  1
     0.480238    0.531546    0.650378    0.690873    0.700701    0.804992    1.318126    1.580707    1.595981    1.688008
     1.729763    1.965927    2.001911    2.147856    2.291084    2.297641    2.346782    2.386135    2.388179    2.450726
     2.474051    2.577502    2.718732    2.784167    3.746411    3.865262

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -227.5825318684 demc= 2.2758E+02 wnorm= 2.7503E-07 knorm= 2.2135E-07 apxde= 1.5279E-14    *not conv.*     

               starting mcscf iteration...   2

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    44, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222813728

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222706892
 address segment size,           sizesg = 222701436
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     232775 transformed 1/r12    array elements were written in      43 records.


 Size of orbital-Hessian matrix B:                   152316
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con         152316


 mosort: allocated sort2 space, avc2is=   222675578 available sort2 space, avcisx=   222675830

   4 trial vectors read from nvfile (unit= 29).
 ciiter=  17 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -227.6831282554     -328.2686805290        0.0000000008        0.0000000013
    2      -227.5324465908     -328.1179988644        0.0000000006        0.0000000013
    3      -227.5320207590     -328.1175730326        0.0000000004        0.0000000013
    4      -227.4089739620     -327.9945262356        0.0001261808        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.163950907512157E-009
 Total number of micro iterations:    3

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 5.8199E-10 rpnorm= 0.0000E+00 noldr=  3 nnewr=  3 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -41.222529  -41.222522  -22.610593  -22.610567   -2.901461   -2.899636   -1.725318   -1.717514   -1.332982   -1.327002
    -1.319599   -1.315689

 qvv(*) eigenvalues. symmetry block  1
     0.480238    0.531546    0.650378    0.690873    0.700701    0.804992    1.318125    1.580707    1.595981    1.688008
     1.729763    1.965927    2.001911    2.147856    2.291084    2.297641    2.346782    2.386135    2.388179    2.450726
     2.474051    2.577502    2.718732    2.784167    3.746411    3.865262

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -227.5825318684 demc= 2.2737E-13 wnorm= 1.7312E-08 knorm= 1.2012E-08 apxde=-5.5591E-17    *not conv.*     

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     0.403 walltime=     0.528

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    44, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 222813728

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 222706892
 address segment size,           sizesg = 222701436
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     183330 transformed 1/r12    array elements were written in      34 records.


 Size of orbital-Hessian matrix B:                   152316
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con         152316


 mosort: allocated sort2 space, avc2is=   222675578 available sort2 space, avcisx=   222675830

   4 trial vectors read from nvfile (unit= 29).
 ciiter=  17 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -227.6831282554     -328.2686805290        0.0000000008        0.0000000013
    2      -227.5324465907     -328.1179988643        0.0000000006        0.0000000013
    3      -227.5320207590     -328.1175730326        0.0000000004        0.0000000013
    4      -227.4089739615     -327.9945262351        0.0001262086        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.722992367939158E-010
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 5.2554E-10 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -41.222529  -41.222522  -22.610593  -22.610567   -2.901461   -2.899636   -1.725318   -1.717514   -1.332982   -1.327002
    -1.319599   -1.315689

 qvv(*) eigenvalues. symmetry block  1
     0.480238    0.531546    0.650378    0.690873    0.700701    0.804992    1.318125    1.580707    1.595981    1.688008
     1.729763    1.965927    2.001911    2.147856    2.291084    2.297641    2.346782    2.386135    2.388179    2.450726
     2.474051    2.577502    2.718732    2.784167    3.746411    3.865262

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    3 emc=   -227.5825318684 demc= 2.5580E-13 wnorm= 3.7784E-09 knorm= 1.5124E-09 apxde= 1.4801E-16    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=     -227.683128255, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.333 total energy=     -227.532446591, rel. (eV)=   4.100258
   DRT #1 state # 3 wt 0.333 total energy=     -227.532020759, rel. (eV)=   4.111846
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A  
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.90765259     1.90096545     1.66823938     1.66394868
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.46382214     0.39537176     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        721 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
      _ s       0.000362   0.000315   0.999634   0.999859   0.141015   0.136058
      _ p       0.000383   0.000356   0.000003   0.000003   0.065820   0.063852
     1_ s       0.000363   0.000315   0.999600   0.999894   0.141015   0.136057
     1_ p       0.000383   0.000356   0.000003   0.000003   0.065820   0.063852
     2_ s       0.999174   0.999405   0.000019  -0.000079   0.722083   0.727728
     2_ p       0.000002   0.000002  -0.000181  -0.000270   0.069502   0.070541
     3_ s       0.999333   0.999245   0.000019  -0.000079   0.722083   0.727728
     3_ p       0.000002   0.000002  -0.000181  -0.000270   0.069502   0.070541
     3_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
     4_ s       0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
 
   ao class       7A         8A         9A        10A        11A        12A  
      _ s       0.441915   0.446169   0.000000   0.000000   0.021159   0.020669
      _ p       0.089058   0.085114   0.539729   0.544347   0.227418   0.231284
     1_ s       0.441915   0.446169   0.000000   0.000000   0.021159   0.020669
     1_ p       0.089058   0.085114   0.539729   0.544347   0.227418   0.231284
     2_ s       0.125827   0.127719   0.000000   0.000000   0.119255   0.119271
     2_ p       0.087077   0.088916   0.059126   0.059718   0.532784   0.528198
     3_ s       0.125827   0.127719   0.000000   0.000000   0.119255   0.119271
     3_ p       0.087077   0.088916   0.059126   0.059718   0.532784   0.528198
     3_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
     4_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
     4_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
     4_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
 
   ao class      13A        14A        15A        16A        17A        18A  
      _ s       0.000151   0.000199   0.000000   0.000000   0.000007  -0.000023
      _ p       0.340131   0.321063   0.002752   0.003037   0.150942   0.131695
     1_ s       0.000151   0.000199   0.000000   0.000000   0.000007  -0.000023
     1_ p       0.340131   0.321063   0.002752   0.003037   0.150942   0.131695
     2_ s      -0.000032   0.000003   0.000000   0.000000   0.000001   0.000001
     2_ p       0.613537   0.629187   0.742108   0.741349   0.080908   0.065963
     3_ s      -0.000032   0.000003   0.000000   0.000000   0.000001   0.000001
     3_ p       0.613537   0.629187   0.742108   0.741349   0.080908   0.065963
     3_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
     4_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
     4_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
     4_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A  


                        gross atomic populations
     ao             _         1_         2_         3_         3_         4_
      s         3.207490   3.207490   3.940375   3.940375   0.843340   0.843340
      p         2.796986   2.796986   4.368468   4.368468   0.000000   0.000000
    total       6.004476   6.004476   8.308843   8.308843   0.843340   0.843340
 
 
     ao            4_         4_
      s         0.843340   0.843340
    total       0.843340   0.843340
 

 Total number of electrons:   32.00000000

 !timer: mcscf                           cpu_time=     0.553 walltime=     0.679
