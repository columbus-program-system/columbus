 program "tran"

 this program transforms one- and two-electron
 integral and density matrix arrays.

 references:  i. shavitt, in "methods of electronic structure theory",
                  h. f. schaefer, ed. (plenum, new york, 1977), p. 189.
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 original author: Frank B. Brown.
 SIFS version:    Ron Shepard.

 version date: 20-feb-02

 This Version of Program TRAN is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              TRAN        **
     **    PROGRAM VERSION:      5.6b2       **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lcore=   2500000

 === echo of the input records ===
 ------------------------------------------------------------------------
  &input
 /&end
 ------------------------------------------------------------------------

 program input parameters:

 prnopt    =     0, chkopt    =     0, ortopt    =     0, denopt    =     0
 mapin(1 ) =    -1, nsymao    =    -1, naopsy(1) =    -1, freeze(1) =    -2
 mapout(1) =    -1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     4, tr1e      =     0
 LDAMIN    =   127, LDAMAX    =  8191, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 file description                 unit filename
 ---- -----------                 ---- --------
   1: input file:                   5  tranin                                                      
   2: listing file:                 6  tranls                                                      
   3: input ao integral file:      30  aoints                                                      
   4: input mo density file:       30  modens                                                      
   5: output mo integral file:     31  moints                                                      
   6: output ao density file:      31  aodens                                                      
   7: scratch file:                34  trscr1                                                      
   8: ci drt file:                 33  cidrtfl                                                     
   9: orbital coefficient file:    32  mocoef                                                      
  10: da1 scratch file:            20  trda1s                                                      
  11: da2 scratch file:            21  trda2s                                                      
  12: split input 2-e ao file:     35  aoints2                                                     
  13: split input d2mo file:       35  modens2                                                     
  14: FSPLIT=2 output 2-e mo file: 31  moints2                                                     
  15: FSPLIT=2 output d2ao file:   31  aodens2                                                     
  16: input file:                   0                                                              


 input file header information:
 Hermit Integral Program : SIFS version  zam489            18:23:06.800 05-Apr-11

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total ao core energy =   0.000000000000E+00

 nsym = 1 nbft=  14

 symmetry  =    1
 SLABEL(*) =  A  
 NBPSY(*)  =   14

 INFOAO(*) =          2      4096      3272      4096      2730         0

 input basis function labels, i:AOLAB(i)=
   1:  1Bes     2:  2Bes     3:  3Bes     4:  4Bepx    5:  5Bepy    6:  6Bepz    7:  7Bepx    8:  8Bepy    9:  9Bepz   10: 10Bed2-
  11: 11Bed1-  12: 12Bed0   13: 13Bed1+  14: 14Bed2+

 FREEZE(*) will be determined from the cidrt file.
 *** rddrt error ***  be                                                                             

 input ci drt file information:
  be                                                                             
 nmotd =  14 nfct  =   0 nfvt  =   0 nbft  =  14
 mo-to-level map(*)
   11  12  13  14   1   2   3   4   5   6   7   8   9  10
 
NMPSY(*) from cidrtfl:  14
NEXO(*)  from cidrtfl:  10
  be                                                                             
 !timer: input processing required       cpu_time=     0.001 walltime=     0.000

 transformation information:
 number of symmetry irreps in point group, nsym   =  1
 total number of basis functions,          nbft   = 14
 number of orbitals to be transformed,     norbt  = 14
 number of frozen core orbitals,           nfrzct =  0
 number of frozen virtual orbitals,        nfrzvt =  0

 symmetry blocking information:
 symmetry  =    1
 SLABEL(*) =  A  
 NBPSY(*)  =   14
 NMPSY(*)  =   14
 NFCPSY(*) =    0
 INFOMO=                     1                  8192                  6552
                  8192                  5460                     0
                     0                     0                     0
                     0
 IFMTM1,IFMTM2=                     0                     0
                    14

 mocoef file titles:
 Hermit Integral Program : SIFS version  zam489            18:23:06.800 05-Apr-11
 mo coefficients generated by scfpq                                              

 job title for this run:
 SIFS file created by program tran.      zam489            18:26:28.933 05-Apr-11

 trmain: score =   0.000000000000E+00
 !timer: chkmo required                  cpu_time=     0.000 walltime=     0.001

 inoutp: segmentation information:
 in-core transformation space,   avcinc =   2398188
 address segment size,           sizesg =   2415155
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0

 inoutp: approximate total flop counts:
           type      in-core   out-of-core
         ------  ------------  ------------
          total      1293278.            0. grand total =      1293278.

 input file sort statistics:
 length of records,           csort%LENDAR =  8191
 number of buckets,           csort%NBUK   =     1
 number of values per bucket, csort%NPBUK  =  5460

 fixed workspace needed for the ao sort,  itotal =     37249
 workspace available for da sort buffers, avcore =   2481862

 beginning the first half-sort of the input 1/r12    array elements...

        776 array elements in     1 records were read by twoaof,
        776 array elements were written into     1 da records of length    8191.
 !timer: twoaof required                 cpu_time=     0.001 walltime=     0.000

 output file header information:
 Hermit Integral Program : SIFS version  zam489            18:23:06.800 05-Apr-11
  be                                                                             
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      zam489            18:26:28.933 05-Apr-11

 output energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total mo core energy =   0.000000000000E+00

 inputc%NSYMMO = 1 norbt=  14

 symmetry  =    1
 SLABEL(*) =  A  
 NMOPSY(*) =   14

 INFOMO(*) =          1      8192      6552      8192      5460         0

 output orbital labels, i:MOLAB(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014

 tran1e: S1(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   20 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: V1(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   22 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: T1(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   20 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: X(*)     array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Y(*)     array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Z(*)     array: ITYPEA= 3 ITYPEB=   0 numaot=   12 nummot=   12 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XX(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   27 nummot=   27 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XY(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=    9 nummot=    9 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZ(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   10 nummot=   10 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YY(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   27 nummot=   27 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YZ(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   10 nummot=   10 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: ZZ(*)    array: ITYPEA= 3 ITYPEB=   0 numaot=   23 nummot=   23 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXX(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXY(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   12 nummot=   12 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXZ(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYY(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   12 nummot=   12 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYZ(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=    6 nummot=    6 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZZ(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYY(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYZ(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YZZ(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   14 nummot=   14 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: ZZZ(*)   array: ITYPEA= 3 ITYPEB=   0 numaot=   12 nummot=   12 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXXX(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   27 nummot=   27 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXXY(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   10 nummot=   10 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXXZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=    9 nummot=    9 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXYY(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   23 nummot=   23 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXYZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=    8 nummot=    8 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXZZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   26 nummot=   26 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYYY(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   10 nummot=   10 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYYZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=    8 nummot=    8 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYZZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=    8 nummot=    8 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZZZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   10 nummot=   10 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYYY(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   27 nummot=   27 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYYZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=    9 nummot=    9 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYZZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   26 nummot=   26 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YZZZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   10 nummot=   10 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YZZZ(*)  array: ITYPEA= 3 ITYPEB=   0 numaot=   23 nummot=   23 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Im(lx)   array: ITYPEA= 3 ITYPEB=   0 numaot=    7 nummot=    5 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Im(ly)   array: ITYPEA= 3 ITYPEB=   0 numaot=    7 nummot=    5 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Im(lz)   array: ITYPEA= 3 ITYPEB=   0 numaot=    6 nummot=    4 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: transformation of the input 1-e arrays is complete.
 !timer: 1-e transformation required:    cpu_time=     0.003 walltime=     0.004

 beginning the last half-sort of the in-core 1/r12    array elements...

 srtinc:       776 array elements were read, and              5565 array elements were written in     1 records.
 !timer: srtinc required                 cpu_time=     0.002 walltime=     0.001

 beginning the transformation of the in-core 1/r12    array elements...
 executing in-core transformation trainc

 trainc:      5565 sorted array elements were read,       776 transformed array elements were written.
 !timer: trainc required                 cpu_time=     0.001 walltime=     0.001

 1-e and 2-e transformation complete.
 !timer: final cleanup required          cpu_time=     0.000 walltime=     0.000

 trmain:        776 transformed 1/r12    array elements were written in       1 records.

 !timer: tran required                   cpu_time=     0.008 walltime=     0.008
