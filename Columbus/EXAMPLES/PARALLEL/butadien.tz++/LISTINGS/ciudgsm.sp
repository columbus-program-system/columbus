1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   579)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0180130005  1.8119E-13  4.7767E-01  1.3807E+00  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -155.0180130005  1.8119E-13  4.7767E-01  1.3807E+00  1.0000E-03

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4047600194  3.8675E-01  2.0332E-02  2.6434E-01  1.0000E-03
 mr-sdci #  2  1   -155.4195718468  1.4812E-02  1.6834E-03  8.1734E-02  1.0000E-03
 mr-sdci #  3  1   -155.4209190628  1.3472E-03  2.0708E-04  2.5367E-02  1.0000E-03
 mr-sdci #  4  1   -155.4210935364  1.7447E-04  4.9442E-05  1.1376E-02  1.0000E-03
 mr-sdci #  5  1   -155.4211435658  5.0029E-05  1.5645E-05  7.3078E-03  1.0000E-03
 mr-sdci #  6  1   -155.4211562070  1.2641E-05  5.1813E-06  3.7119E-03  1.0000E-03
 mr-sdci #  7  1   -155.4211625837  6.3767E-06  2.0418E-06  2.3894E-03  1.0000E-03
 mr-sdci #  8  1   -155.4211645558  1.9721E-06  4.8235E-07  1.3255E-03  1.0000E-03
 mr-sdci #  9  1   -155.4211650171  4.6137E-07  7.1678E-08  4.5874E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1   -155.4211650171  4.6137E-07  7.1678E-08  4.5874E-04  1.0000E-03

 number of reference csfs (nref) is   172.  root number (iroot) is  1.

 eref      =   -155.016115522359   "relaxed" cnot**2         =   0.885680458693
 eci       =   -155.421165017143   deltae = eci - eref       =  -0.405049494784
 eci+dv1   =   -155.467470089593   dv1 = (1-cnot**2)*deltae  =  -0.046305072450
 eci+dv2   =   -155.473446934392   dv2 = dv1 / cnot**2       =  -0.052281917249
 eci+dv3   =   -155.481195375703   dv3 = dv1 / (2*cnot**2-1) =  -0.060030358561
 eci+pople =   -155.474214955429   ( 22e- scaled deltae )    =  -0.458099433070
