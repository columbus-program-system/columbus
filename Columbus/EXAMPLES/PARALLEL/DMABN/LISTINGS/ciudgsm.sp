1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=  2955)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -455.3557136666  3.0198E-14  1.7634E+00  2.2142E+00  1.0000E-03   

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -455.3557136666  3.0198E-14  1.7634E+00  2.2142E+00  1.0000E-03   

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -455.3557136666 -8.8818E-16  1.7634E+00  2.2142E+00  5.0000E-03   
 mr-sdci #  2  1   -456.4311271764  1.0754E+00  8.6700E-02  5.3756E-01  5.0000E-03   
 mr-sdci #  3  1   -456.5032016154  7.2074E-02  4.6326E-03  1.2564E-01  5.0000E-03   
 mr-sdci #  4  1   -456.5067764563  3.5748E-03  4.3285E-04  3.6961E-02  5.0000E-03   
 mr-sdci #  5  1   -456.5071063377  3.2988E-04  4.8594E-05  1.2594E-02  5.0000E-03   
 mr-sdci #  6  1   -456.5071549931  4.8655E-05  1.4422E-05  6.2432E-03  5.0000E-03   
 mr-sdci #  7  1   -456.5071756227  2.0630E-05  1.5676E-05  7.3053E-03  5.0000E-03   
 mr-sdci #  8  1   -456.5072067610  3.1138E-05  1.6257E-05  6.7721E-03  5.0000E-03   
 mr-sdci #  9  1   -456.5072299940  2.3233E-05  6.7186E-06  4.6644E-03  5.0000E-03   

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1   -456.5072299940  2.3233E-05  6.7186E-06  4.6644E-03  5.0000E-03   

 number of reference csfs (nref) is    22.  root number (iroot) is  1.
 c0**2 =   0.77062525  c**2 (all zwalks) =   0.77388583

 eref      =   -455.355357038300   "relaxed" cnot**2         =   0.770625250870
 eci       =   -456.507229993963   deltae = eci - eref       =  -1.151872955662
 eci+dv1   =   -456.771440564198   dv1 = (1-cnot**2)*deltae  =  -0.264210570235
 eci+dv2   =   -456.850082203824   dv2 = dv1 / cnot**2       =  -0.342852209861
 eci+dv3   =   -456.995378401006   dv3 = dv1 / (2*cnot**2-1) =  -0.488148407043
 eci+pople =   -456.961482767134   ( 56e- scaled deltae )    =  -1.606125728834
