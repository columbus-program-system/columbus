 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt2.F9 Revision: 1.1.6.6           Date: 2015/02/26 17:04:32   * 
* cidrt3.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt4.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lencor=2097152000 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/parallel/DMABN.tutorial/RUN2.20
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    78
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  a1 ) ( 2,  b1 ) ( 3,  b2 ) ( 4,  a2 ) 
 input nmpsy(*):
 nmpsy(*)=        82  35  62  25
 
   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      a1  b1  b2  a2 
 nmpsy(*)=        82  35  62  25
 
 total molecular orbitals            :   204
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  b2 
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :    11
 
 fcorb(*)=         1   2   3   4   5   6   7   8 118 119 120
 slabel(*)=      a1  a1  a1  a1  a1  a1  a1  a1  b2  b2  b2 
 
 number of frozen core orbitals      :    11
 number of frozen core electrons     :    22
 number of internal electrons        :    56
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    30
 
 modrt(*)=         9  10  11  12  13  14  15  16  17  18  19  20  83  84  85
                 121 122 123 124 125 126 127 128 129 180  86  87  88 181 182
 slabel(*)=      a1  a1  a1  a1  a1  a1  a1  a1  a1  a1  a1  a1  b1  b1  b1 
                 b2  b2  b2  b2  b2  b2  b2  b2  b2  a2  b1  b1  b1  a2  a2 
 
 total number of orbitals            :   204
 number of frozen core orbitals      :    11
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    30
 number of external orbitals         :   163
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  -1  -1  -1  -1  -1  -1 164 165 166 167 168 169 170
                 171 172 173 174 175   1   2   3   4   5   6   7   8   9  10
                  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25
                  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40
                  41  42  43  44  45  46  47  48  49  50  51  52  53  54  55
                  56  57  58  59  60  61  62 176 177 178 189 190 191  63  64
                  65  66  67  68  69  70  71  72  73  74  75  76  77  78  79
                  80  81  82  83  84  85  86  87  88  89  90  91  -1  -1  -1
                 179 180 181 182 183 184 185 186 187  92  93  94  95  96  97
                  98  99 100 101 102 103 104 105 106 107 108 109 110 111 112
                 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127
                 128 129 130 131 132 133 134 135 136 137 138 139 140 141 188
                 192 193 142 143 144 145 146 147 148 149 150 151 152 153 154
                 155 156 157 158 159 160 161 162 163
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :    25
 
 no. of internal orbitals            :    30
 no. of doubly-occ. (ref) orbitals   :    25
 no. active (ref) orbitals           :     5
 no. of active electrons             :     6
 
 input the active-orbital, active-electron occmnr(*):
  86 87 88181182
 input the active-orbital, active-electron occmxr(*):
  86 87 88181182
 
 actmo(*) =       86  87  88 181 182
 occmnr(*)=        0   0   0   0   6
 occmxr(*)=        6   6   6   6   6
 reference csf cumulative electron occupations:
 modrt(*)=         9  10  11  12  13  14  15  16  17  18  19  20  83  84  85
                 121 122 123 124 125 126 127 128 129 180  86  87  88 181 182
 occmnr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  38  40  42  44  46  48  50  50  50  50  50  56
 occmxr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  38  40  42  44  46  48  50  56  56  56  56  56
 
 input the active-orbital bminr(*):
  86 87 88181182
 input the active-orbital bmaxr(*):
  86 87 88181182
 reference csf b-value constraints:
 modrt(*)=         9  10  11  12  13  14  15  16  17  18  19  20  83  84  85
                 121 122 123 124 125 126 127 128 129 180  86  87  88 181 182
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   6   6   6   6   6
 input the active orbital smaskr(*):
  86 87 88181182
 modrt:smaskr=
   9:1000  10:1000  11:1000  12:1000  13:1000  14:1000  15:1000  16:1000
  17:1000  18:1000  19:1000  20:1000  83:1000  84:1000  85:1000 121:1000
 122:1000 123:1000 124:1000 125:1000 126:1000 127:1000 128:1000 129:1000
 180:1000  86:1111  87:1111  88:1111 181:1111 182:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    56
 
 input the internal-orbital mrsdci occmin(*):
   9 10 11 12 13 14 15 16 17 18 19 20 83 84 85121122123124125
 126127128129180 86 87 88181182
 input the internal-orbital mrsdci occmax(*):
   9 10 11 12 13 14 15 16 17 18 19 20 83 84 85121122123124125
 126127128129180 86 87 88181182
 mrsdci csf cumulative electron occupations:
 modrt(*)=         9  10  11  12  13  14  15  16  17  18  19  20  83  84  85
                 121 122 123 124 125 126 127 128 129 180  86  87  88 181 182
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   0   0   0   0  54
 occmax(*)=       56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
                  56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
 
 input the internal-orbital mrsdci bmin(*):
   9 10 11 12 13 14 15 16 17 18 19 20 83 84 85121122123124125
 126127128129180 86 87 88181182
 input the internal-orbital mrsdci bmax(*):
   9 10 11 12 13 14 15 16 17 18 19 20 83 84 85121122123124125
 126127128129180 86 87 88181182
 mrsdci b-value constraints:
 modrt(*)=         9  10  11  12  13  14  15  16  17  18  19  20  83  84  85
                 121 122 123 124 125 126 127 128 129 180  86  87  88 181 182
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
                  56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
 
 input the internal-orbital smask(*):
   9 10 11 12 13 14 15 16 17 18 19 20 83 84 85121122123124125
 126127128129180 86 87 88181182
 modrt:smask=
   9:1111  10:1111  11:1111  12:1111  13:1111  14:1111  15:1111  16:1111
  17:1111  18:1111  19:1111  20:1111  83:1111  84:1111  85:1111 121:1111
 122:1111 123:1111 124:1111 125:1111 126:1111 127:1111 128:1111 129:1111
 180:1111  86:1111  87:1111  88:1111 181:1111 182:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1   1   1   1   1   1   1   1   2   2   2
                   3   3   3   3   3   3   3   3   3   4   2   2   2   4   4
 slabel(*)=      a1  a1  a1  a1  a1  a1  a1  a1  a1  a1  a1  a1  b1  b1  b1 
                 b2  b2  b2  b2  b2  b2  b2  b2  b2  a2  b1  b1  b1  a2  a2 
 rmo(*)=           9  10  11  12  13  14  15  16  17  18  19  20   1   2   3
                   4   5   6   7   8   9  10  11  12   1   4   5   6   2   3
 modrt(*)=         9  10  11  12  13  14  15  16  17  18  19  20  83  84  85
                 121 122 123 124 125 126 127 128 129 180  86  87  88 181 182
 
 reference csf info:
 occmnr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  38  40  42  44  46  48  50  50  50  50  50  56
 occmxr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  26  28  30
                  32  34  36  38  40  42  44  46  48  50  56  56  56  56  56
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   6   6   6   6   6
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   0   0   0   0  54
 occmax(*)=       56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
                  56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
                  56  56  56  56  56  56  56  56  56  56  56  56  56  56  56
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will be imposed.
 multp(*)=
  hmult                     0
 lxyzir   0   0   0
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt : 235
    49 arcs removed due to generalized interacting space restrictions.

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=        8925
 xbary=      154450
 xbarx=      950920
 xbarw=     1026175
        --------
 nwalk=     2140470
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=    8925

 input the list of allowed reference symmetries:
 allowed reference symmetries:             3
 allowed reference symmetry labels:      b2 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
       22 csfs initially selected from    8925 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   9 10 11 12 13 14 15 16 17 18 19 20 83 84 85121122123124125
 126127128129180 86 87 88181182

 step 2 reference csf selection complete.
       22 csfs currently selected from    8925 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
       22 reference csfs selected from    8925 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   2   2   2   2   2   2   2   2   2   2   2   2   2
                   2   2   2   2   2   2   2   2   2   2   0   0   0   0   0
 
 interacting space determination:
 checking diagonal loops...
 checking 2-internal loops...
 checking 3-internal loops...
 checking 4-internal loops...
 limint: nvalw=                  2955                 27450
                  8445                  8999
 post-limint icd(*)=                     1                     1
                  8928               2149400                     0
                     0
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=    2955   27450    8445    8999 nvalwt=   47849

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=    2955   27450    8445    8999 nvalwt=   47849

 lprune input numv1,nwalk=                 47849               2140470
 lprune input xbar(1,1),nref=                  8925                    22

 lprune: l(*,*,*) pruned with nwalk= 2140470 nvalwt=   47849=2955****84458999
 lprune:  z-drt, nprune=   317
 lprune:  y-drt, nprune=   268
 lprune: wx-drt, nprune=   295

 xbarz=        8556
 xbary=       27450
 xbarx=       15045
 xbarw=       16149
        --------
 nwalk=       67200
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  333333333333333333333333333120
        2       2  333333333333333333333333333102
        3       3  333333333333333333333333331320
        4       4  333333333333333333333333331302
        5       9  333333333333333333333333331032
        6      10  333333333333333333333333331023
        7      14  333333333333333333333333330132
        8      15  333333333333333333333333330123
        9      17  333333333333333333333333313320
       10      18  333333333333333333333333313302
       11      23  333333333333333333333333313032
       12      24  333333333333333333333333313023
       13      28  333333333333333333333333312132
       14      29  333333333333333333333333312123
       15      32  333333333333333333333333311232
       16      33  333333333333333333333333311223
       17      34  333333333333333333333333310332
       18      35  333333333333333333333333310323
       19      40  333333333333333333333333303132
       20      41  333333333333333333333333303123
       21      43  333333333333333333333333301332
       22      44  333333333333333333333333301323
 indx01:    22 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01: 47849 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z               0               0            2955               0
 y            4362            9372            4353            9363
 x            2607            1614            2478            1746
 w            2613            1614            3026            1746

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z               0               0            2955               0
 y          218100          206184          269886          271527
 x         9744966         4541796         9299934         5059908
 w         9767394         4541796        11849816         5059908

 total csf counts:
 z-vertex:            2955
 y-vertex:          965697
 x-vertex:        28646604
 w-vertex:        31218914
           --------
 total:        60834170
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/parallel/DMABN.tutorial/RUN2.20
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  a1  b1  b2  a2 
nwalk=   67200 cpos=   16631 maxval=    9 cmprfactor=   75.25 %.
nwalk=   67200 cpos=   12712 maxval=   99 cmprfactor=   62.17 %.
 compressed with: nwalk=   67200 cpos=   16631 maxval=    9 cmprfactor=   75.25 %.
initial index vector length:     67200
compressed index vector length:     16631reduction:  75.25%
nwalk=    8556 cpos=    1081 maxval=    9 cmprfactor=   87.37 %.
nwalk=    8556 cpos=     104 maxval=   99 cmprfactor=   97.57 %.
nwalk=    8556 cpos=      26 maxval=  999 cmprfactor=   99.09 %.
nwalk=    8556 cpos=      18 maxval= 9999 cmprfactor=   99.16 %.
 compressed with: nwalk=    8556 cpos=      26 maxval=  999 cmprfactor=   99.09 %.
initial ref vector length:      8556
compressed ref vector length:        26reduction:  99.70%
