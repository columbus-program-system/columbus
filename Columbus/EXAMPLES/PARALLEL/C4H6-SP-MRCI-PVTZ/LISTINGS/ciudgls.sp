1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      -varseg3    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 compressed index vector length=                     2781
 using llenci=                       -1
================================================================================
four external integ   76.75 MB location: distr. (dual) 
three external inte   31.25 MB location: distr. (dual) 
four external integ   74.00 MB location: distr. (dual) 
three external inte   30.75 MB location: distr. (dual) 
diagonal integrals     0.25 MB location: virtual disk  
off-diagonal integr    5.00 MB location: virtual disk  
computed file size in DP units
fil3w:     4095875
fil3x:     4030341
fil4w:    10059469
fil4x:     9699032
ofdgint:       32760
diagint:      655200
computed file size in bytes 
fil3w:    32767000
fil3x:    32242728
fil4w:    80475752
fil4x:    77592256
ofdgint:      262080
diagint:     5241600
 nsubmx=                        6  lenci=                  8741625
global arrays:              113641125   (            867.01 MB)
vdisk:                         687960   (              5.25 MB)
drt:                           234312   (              0.89 MB)
================================================================================
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 22
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  NVRFMX = 6
  RTOLBK = 1e-3,1e-3,
  NITER =  10
  NVCIMN = 1
  NVCIMX =  6
  RTOLCI = 1e-3,1e-3,
  IDEN  = 1
  CSFPRN = 10,
  FTCALC = 1
  MAXSEG = 20
  nsegwx= 1,1,2,2
  nseg0x = 1,1,2,2
  nseg1x = 1,1,2,2
  nseg2x = 1,1,2,2
  nseg3x = 1,1,2,2
  nseg4x = 1,1,2,2,
  c2ex0ex=0
  c3ex1ex=0
  cdg4ex=1
  fileloc=1,1,4,4,4,4
  finalv=-1
  finalw=-1
  &end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   10
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =   20
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   22      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 USING SEGMENTS OF EQUAL SIZE

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------
 Main memory management:
 global         56937726 DP per process
 vdisk            687960 DP per process
 stack                 0 DP per process
 core          242374314 DP per process
 allocating vdisk of size                   687970  DP
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 242374314 mem1=         0 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  zam216            16:06:19.938 18-Feb-08
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      zam216            16:06:25.343 18-Feb-08

 core energy values from the integral file:
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274375457176E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.191864289308E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.431809767001E+02

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult  F  F                        0 
                        0                        0                        0
 nmot  =   146 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   104 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      14155      800     5110     3772     4473
 nvalwt,nvalw:     8735      420     4326     1882     2107
 ncsft:         8741625
 total number of valid internal walks:    8735
 nvalz,nvaly,nvalx,nvalw =      420    4326    1882    2107

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld  20475
 nd4ext,nd2ext,nd0ext 16770  3354   182
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int  9468510  3698334   597417    46410     1461        0        0        0
 minbl4,minbl3,maxbl2  7650  7650  7653
 maxbuf 32767
 number of external orbitals per symmetry block:  46  47  18  18
 nmsym   4 number of internal orbitals  13
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   800  5110  3772  4473 total internal walks:   14155
 maxlp3,n2lp,n1lp,n0lp  2879     0     0     0
 orbsym(*)= 1 1 1 2 2 2 2 2 2 3 3 4 4

 setref:       33 references kept,
                0 references were marked as invalid, out of
               33 total.
 limcnvrt: found                      420  valid internal walksout of  
                      800  walks (skipping trailing invalids)
  ... adding                       420  segmentation marks segtype= 
                        1
 limcnvrt: found                     4326  valid internal walksout of  
                     5110  walks (skipping trailing invalids)
  ... adding                      4326  segmentation marks segtype= 
                        2
 limcnvrt: found                     1882  valid internal walksout of  
                     3772  walks (skipping trailing invalids)
  ... adding                      1882  segmentation marks segtype= 
                        3
 limcnvrt: found                     2107  valid internal walksout of  
                     4473  walks (skipping trailing invalids)
  ... adding                      2107  segmentation marks segtype= 
                        4
loaded      142 onel-dg  integrals(   1records ) at       1 on virtual disk
loaded    16770 4ext-dg  integrals(   5records ) at    4096 on virtual disk
loaded     3354 2ext-dg  integrals(   1records ) at   24571 on virtual disk
loaded      182 0ext-dg  integrals(   1records ) at   28666 on virtual disk
loaded     2551 onel-of  integrals(   1records ) at   32761 on virtual disk
loaded   597417 2ext-og  integrals( 146records ) at   36856 on virtual disk
loaded    46410 1ext-og  integrals(  12records ) at  634726 on virtual disk
loaded     1461 0ext-og  integrals(   1records ) at  683866 on virtual disk
loaded 10059470 word integrals from unit 31fil4w                                                        to distributed 
loaded  9699033 word integrals from unit 32fil4x                                                        to distributed 
loaded  4095876 word integrals from unit 33fil3w                                                        to distributed 
loaded  4030342 word integrals from unit 34fil3x                                                        to distributed 
VDISK: unit(12) offset        1
VDISK: unit(13) offset    32761
 Top vdisk address:                    687960
 initvdsk=    1.000000000000000       seconds.
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            74389
    threx             50042
    twoex             43201
    onex               8702
    allin              4095
    diagon            21257
               =======
   maximum            74389

  __ static summary __ 
   reflst               420
   hrfspc               420
               -------
   static->             420

  __ core required  __ 
   totstc               420
   max n-ex           74389
               -------
   totnec->           74809

  __ core available __ 
   totspc         242374314
   totnec -           74809
               -------
   totvec->       242299505

 number of external paths / symmetry
 vertex x    2422    2486    1674    1674
 vertex w    2551    2486    1674    1674
segment: free space=   242299505
 reducing frespc by                    28762 to                 242270743 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      2043|
 -------------------------------------------------------------------------------
  X 3        1599|   1910026|    127492|       943|      4746|     12225|
 -------------------------------------------------------------------------------
  X 4        2081|   2073906|   2037518|       939|      5689|     15369|
 -------------------------------------------------------------------------------
  W 5        1875|   2253713|   4111424|      1055|      6628|     18727|
 -------------------------------------------------------------------------------
  W 6        2448|   2376488|   6365137|      1052|      7683|     22187|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        1276DP  conft+indsym=       17304DP  drtbuffer=       10182 DP
GA space requirements for DRT:       12935DP out of        39052 allocated.

dimension of the ci-matrix ->>>   8741625

segment: free space=   242299505
 reducing frespc by                    28762 to                 242270743 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type four-external+diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      2043|
 -------------------------------------------------------------------------------
  X 3        1599|   1910026|    127492|       943|      4746|     12225|
 -------------------------------------------------------------------------------
  X 4        2081|   2073906|   2037518|       939|      5689|     15369|
 -------------------------------------------------------------------------------
  W 5        1875|   2253713|   4111424|      1055|      6628|     18727|
 -------------------------------------------------------------------------------
  W 6        2448|   2376488|   6365137|      1052|      7683|     22187|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        1276DP  conft+indsym=       17304DP  drtbuffer=       10182 DP
GA space requirements for DRT:       12935DP out of        39052 allocated.

dimension of the ci-matrix ->>>   8741625

segment: free space=   242299505
 reducing frespc by                    28762 to                 242270743 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      2043|
 -------------------------------------------------------------------------------
  X 3        1599|   1910026|    127492|       943|      4746|     12225|
 -------------------------------------------------------------------------------
  X 4        2081|   2073906|   2037518|       939|      5689|     15369|
 -------------------------------------------------------------------------------
  W 5        1875|   2253713|   4111424|      1055|      6628|     18727|
 -------------------------------------------------------------------------------
  W 6        2448|   2376488|   6365137|      1052|      7683|     22187|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        1276DP  conft+indsym=       17304DP  drtbuffer=       10182 DP
GA space requirements for DRT:       12935DP out of        39052 allocated.

dimension of the ci-matrix ->>>   8741625

segment: free space=   242299505
 reducing frespc by                    28762 to                 242270743 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      2043|
 -------------------------------------------------------------------------------
  X 3        1599|   1910026|    127492|       943|      4746|     12225|
 -------------------------------------------------------------------------------
  X 4        2081|   2073906|   2037518|       939|      5689|     15369|
 -------------------------------------------------------------------------------
  W 5        1875|   2253713|   4111424|      1055|      6628|     18727|
 -------------------------------------------------------------------------------
  W 6        2448|   2376488|   6365137|      1052|      7683|     22187|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        1276DP  conft+indsym=       17304DP  drtbuffer=       10182 DP
GA space requirements for DRT:       12935DP out of        39052 allocated.

dimension of the ci-matrix ->>>   8741625

segment: free space=   242299505
 reducing frespc by                    28762 to                 242270743 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      2043|
 -------------------------------------------------------------------------------
  X 3        1599|   1910026|    127492|       943|      4746|     12225|
 -------------------------------------------------------------------------------
  X 4        2081|   2073906|   2037518|       939|      5689|     15369|
 -------------------------------------------------------------------------------
  W 5        1875|   2253713|   4111424|      1055|      6628|     18727|
 -------------------------------------------------------------------------------
  W 6        2448|   2376488|   6365137|      1052|      7683|     22187|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        1276DP  conft+indsym=       17304DP  drtbuffer=       10182 DP
GA space requirements for DRT:       12935DP out of        39052 allocated.

dimension of the ci-matrix ->>>   8741625

segment: free space=   242299505
 reducing frespc by                    28762 to                 242270743 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type two-external wx
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      2043|
 -------------------------------------------------------------------------------
  X 3        1599|   1910026|    127492|       943|      4746|     12225|
 -------------------------------------------------------------------------------
  X 4        2081|   2073906|   2037518|       939|      5689|     15369|
 -------------------------------------------------------------------------------
  W 5        1875|   2253713|   4111424|      1055|      6628|     18727|
 -------------------------------------------------------------------------------
  W 6        2448|   2376488|   6365137|      1052|      7683|     22187|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        1276DP  conft+indsym=       17304DP  drtbuffer=       10182 DP
GA space requirements for DRT:       12935DP out of        39052 allocated.

dimension of the ci-matrix ->>>   8741625

 prepdnew=    0.000000000000000       seconds.
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   2    21      two-ext yy   2X  2 2    5110    5110     127072     127072    4326    4326
     2  3   3    22      two-ext xx*  2X  3 3    1599    1599    1910026    1910026     943     943
     3  3   3    28      two-ext xx+  2X  3 3    1599    1599    1910026    1910026     943     943
     4  4   3    22      two-ext xx*  2X  3 3    2081    1599    2073906    1910026     939     943
     5  4   3    28      two-ext xx+  2X  3 3    2081    1599    2073906    1910026     939     943
     6  4   4    22      two-ext xx*  2X  3 3    2081    2081    2073906    2073906     939     939
     7  4   4    28      two-ext xx+  2X  3 3    2081    2081    2073906    2073906     939     939
     8  5   5    23      two-ext ww*  2X  4 4    1875    1875    2253713    2253713    1055    1055
     9  5   5    29      two-ext ww+  2X  4 4    1875    1875    2253713    2253713    1055    1055
    10  6   5    23      two-ext ww*  2X  4 4    2448    1875    2376488    2253713    1052    1055
    11  6   5    29      two-ext ww+  2X  4 4    2448    1875    2376488    2253713    1052    1055
    12  6   6    23      two-ext ww*  2X  4 4    2448    2448    2376488    2376488    1052    1052
    13  6   6    29      two-ext ww+  2X  4 4    2448    2448    2376488    2376488    1052    1052
    14  3   1    24      two-ext xz   2X  3 1    1599     800    1910026        420     943     420
    15  4   1    24      two-ext xz   2X  3 1    2081     800    2073906        420     939     420
    16  5   1    25      two-ext wz   2X  4 1    1875     800    2253713        420    1055     420
    17  6   1    25      two-ext wz   2X  4 1    2448     800    2376488        420    1052     420
    18  5   3    26      two-ext wx*  WX  4 3    1875    1599    2253713    1910026    1055     943
    19  5   3    27      two-ext wx+  WX  4 3    1875    1599    2253713    1910026    1055     943
    20  6   3    26      two-ext wx*  WX  4 3    2448    1599    2376488    1910026    1052     943
    21  6   3    27      two-ext wx+  WX  4 3    2448    1599    2376488    1910026    1052     943
    22  5   4    26      two-ext wx*  WX  4 3    1875    2081    2253713    2073906    1055     939
    23  5   4    27      two-ext wx+  WX  4 3    1875    2081    2253713    2073906    1055     939
    24  6   4    26      two-ext wx*  WX  4 3    2448    2081    2376488    2073906    1052     939
    25  6   4    27      two-ext wx+  WX  4 3    2448    2081    2376488    2073906    1052     939
    26  2   1    11      one-ext yz   1X  2 1    5110     800     127072        420    4326     420
    27  3   2    13      one-ext yx   1X  3 2    1599    5110    1910026     127072     943    4326
    28  4   2    13      one-ext yx   1X  3 2    2081    5110    2073906     127072     939    4326
    29  5   2    14      one-ext yw   1X  4 2    1875    5110    2253713     127072    1055    4326
    30  6   2    14      one-ext yw   1X  4 2    2448    5110    2376488     127072    1052    4326
    31  3   2    31      thr-ext yx   3X  3 2    1599    5110    1910026     127072     943    4326
    32  4   2    31      thr-ext yx   3X  3 2    2081    5110    2073906     127072     939    4326
    33  5   2    32      thr-ext yw   3X  4 2    1875    5110    2253713     127072    1055    4326
    34  6   2    32      thr-ext yw   3X  4 2    2448    5110    2376488     127072    1052    4326
    35  1   1     1      allint zz    OX  1 1     800     800        420        420     420     420
    36  2   2     2      allint yy    OX  2 2    5110    5110     127072     127072    4326    4326
    37  3   3     3      allint xx*   OX  3 3    1599    1599    1910026    1910026     943     943
    38  3   3     8      allint xx+   OX  3 3    1599    1599    1910026    1910026     943     943
    39  4   3     3      allint xx*   OX  3 3    2081    1599    2073906    1910026     939     943
    40  4   3     8      allint xx+   OX  3 3    2081    1599    2073906    1910026     939     943
    41  4   4     3      allint xx*   OX  3 3    2081    2081    2073906    2073906     939     939
    42  4   4     8      allint xx+   OX  3 3    2081    2081    2073906    2073906     939     939
    43  5   5     4      allint ww*   OX  4 4    1875    1875    2253713    2253713    1055    1055
    44  5   5     9      allint ww+   OX  4 4    1875    1875    2253713    2253713    1055    1055
    45  6   5     4      allint ww*   OX  4 4    2448    1875    2376488    2253713    1052    1055
    46  6   5     9      allint ww+   OX  4 4    2448    1875    2376488    2253713    1052    1055
    47  6   6     4      allint ww*   OX  4 4    2448    2448    2376488    2376488    1052    1052
    48  6   6     9      allint ww+   OX  4 4    2448    2448    2376488    2376488    1052    1052
    49  1   1    75      dg-024ext z  4X  1 1     800     800        420        420     420     420
    50  2   2    45      4exdg024 y   4X  2 2    5110    5110     127072     127072    4326    4326
    51  3   3    46      4exdg024 x   4X  3 3    1599    1599    1910026    1910026     943     943
    52  4   4    46      4exdg024 x   4X  3 3    2081    2081    2073906    2073906     939     939
    53  5   5    47      4exdg024 w   4X  4 4    1875    1875    2253713    2253713    1055    1055
    54  6   6    47      4exdg024 w   4X  4 4    2448    2448    2376488    2376488    1052    1052
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X=                   214466 
                    53393                     8315
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       11918 2x:           0 4x:           0
All internal counts: zz :       32118 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      33

    root           eigenvalues
    ----           ------------
       1        -155.0173791292

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                      420

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   420)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:           8741625
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:           0 xx:           0 ww:           0
One-external counts: yz :      161582 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:        5228 wz:        6717 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0173791292  8.1712E-14  4.7844E-01  1.3812E+00  1.0000E-03

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    0     0.00     0.00     0.00     0.00         0.    0.0000
    2   22    0     0.00     0.00     0.00     0.00         0.    0.0000
    3   28    0     0.00     0.00     0.00     0.00         0.    0.0000
    4   22    0     0.00     0.00     0.00     0.00         0.    0.0000
    5   28    0     0.00     0.00     0.00     0.00         0.    0.0000
    6   22    0     0.00     0.00     0.00     0.00         0.    0.0000
    7   28    0     0.00     0.00     0.00     0.00         0.    0.0000
    8   23    0     0.00     0.00     0.00     0.00         0.    0.0000
    9   29    0     0.00     0.00     0.00     0.00         0.    0.0000
   10   23    0     0.00     0.00     0.00     0.00         0.    0.0000
   11   29    0     0.00     0.00     0.00     0.00         0.    0.0000
   12   23    0     0.00     0.00     0.00     0.00         0.    0.0000
   13   29    0     0.00     0.00     0.00     0.00         0.    0.0000
   14   24    0     0.08     0.00     0.01     0.05         0.    0.0033
   15   24    1     0.10     0.00     0.00     0.04         0.    0.0031
   16   25    0     0.13     0.00     0.00     0.06         0.    0.0042
   17   25    1     0.11     0.00     0.00     0.06         0.    0.0036
   18   26    0     0.00     0.00     0.00     0.00         0.    0.0000
   19   27    0     0.00     0.00     0.00     0.00         0.    0.0000
   20   26    0     0.00     0.00     0.00     0.00         0.    0.0000
   21   27    0     0.00     0.00     0.00     0.00         0.    0.0000
   22   26    0     0.00     0.00     0.00     0.00         0.    0.0000
   23   27    0     0.00     0.00     0.00     0.00         0.    0.0000
   24   26    0     0.00     0.00     0.00     0.00         0.    0.0000
   25   27    0     0.00     0.00     0.00     0.00         0.    0.0000
   26   11    0     0.16     0.10     0.00     0.16         0.    0.1309
   27   13    0     0.00     0.00     0.00     0.00         0.    0.0000
   28   13    0     0.00     0.00     0.00     0.00         0.    0.0000
   29   14    0     0.00     0.00     0.00     0.00         0.    0.0000
   30   14    0     0.00     0.00     0.00     0.00         0.    0.0000
   31   31    0     0.00     0.00     0.00     0.00         0.    0.0000
   32   31    0     0.00     0.00     0.00     0.00         0.    0.0000
   33   32    0     0.00     0.00     0.00     0.00         0.    0.0000
   34   32    0     0.00     0.00     0.00     0.00         0.    0.0000
   35    1    1     0.03     0.03     0.00     0.03         0.    0.0244
   36    2    0     0.00     0.00     0.00     0.00         0.    0.0000
   37    3    0     0.00     0.00     0.00     0.00         0.    0.0000
   38    8    0     0.00     0.00     0.00     0.00         0.    0.0000
   39    3    0     0.00     0.00     0.00     0.00         0.    0.0000
   40    8    0     0.00     0.00     0.00     0.00         0.    0.0000
   41    3    0     0.00     0.00     0.00     0.00         0.    0.0000
   42    8    0     0.00     0.00     0.00     0.00         0.    0.0000
   43    4    0     0.00     0.00     0.00     0.00         0.    0.0000
   44    9    0     0.00     0.00     0.00     0.00         0.    0.0000
   45    4    0     0.00     0.00     0.00     0.00         0.    0.0000
   46    9    0     0.00     0.00     0.00     0.00         0.    0.0000
   47    4    0     0.00     0.00     0.00     0.00         0.    0.0000
   48    9    0     0.00     0.00     0.00     0.00         0.    0.0000
   49   75    1     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    1     0.07     0.03     0.00     0.07         0.    0.0712
   51   46    1     4.60     0.00     0.12     4.55         0.    0.0109
   52   46    0     5.96     0.01     0.12     5.93         0.    0.0126
   53   47    1     5.64     0.00     0.12     5.61         0.    0.0122
   54   47    0     6.85     0.00     0.10     6.79         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.00     0.06     0.29     0.00     0.00     0.31    13.60
  2     2.60     0.06     0.29     0.00     0.00     0.27    13.60
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    23.7344s 
time spent in multnx:                  23.3594s 
integral transfer time:                 0.4688s 
time spent for loop construction:       0.1875s 
syncronization time in mult:            2.6016s 
total time per CI iteration:           27.2031s 
 ** parallelization degree for mult section:0.9012

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         182.90        0.11       1734.116
    v  (write)         66.69        0.11        588.741
    w  (read)         133.39        0.07       2008.645
    w  (write)         49.51        0.05        974.873
    dg (read)          66.69        0.05       1422.791
    dg (write)          0.00        0.00          0.000
    drt(read)           0.10        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          499.27        0.38       1304.228
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          83.90        0.09        976.260
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         83.90        0.12        715.924
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.14        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          167.93        0.20        826.732
========================================================


 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0173791292  8.1712E-14  4.7844E-01  1.3812E+00  1.0000E-03

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 I/O during diagon finalize 

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.21          0.002
    v  (write)          0.00        0.23          0.001
    w  (read)           0.00        0.13          0.002
    w  (write)          0.00        0.10          0.001
    dg (read)           0.00        0.09          0.001
    dg (write)          0.00        0.00          0.000
    drt(read)           0.00        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.00        0.77          0.001
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.17          0.001
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.23          0.001
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.00        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.00        0.41          0.001
========================================================


 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:           8741625
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   10
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94308219    -0.33255974

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4045208009  3.8714E-01  1.9788E-02  2.5888E-01  1.0000E-03
 mr-sdci #  1  2   -151.9040168701  8.7230E+00  0.0000E+00  0.0000E+00  1.0000E-03

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    0     1.55     0.04     0.00     1.55         0.    0.1070
    2   22    1     1.98     0.00     0.00     1.95         0.    0.0252
    3   28    0     1.45     0.01     0.00     1.41         0.    0.0252
    4   22    1     0.75     0.00     0.00     0.71         0.    0.0065
    5   28    1     0.79     0.00     0.00     0.76         0.    0.0065
    6   22    1     1.72     0.00     0.02     1.70         0.    0.0170
    7   28    0     1.10     0.00     0.00     1.07         0.    0.0170
    8   23    0     1.98     0.00     0.00     1.95         0.    0.0212
    9   29    1     1.38     0.00     0.02     1.35         0.    0.0212
   10   23    1     0.82     0.00     0.01     0.79         0.    0.0062
   11   29    0     0.86     0.00     0.01     0.82         0.    0.0062
   12   23    0     1.89     0.00     0.01     1.85         0.    0.0159
   13   29    1     1.16     0.00     0.02     1.12         0.    0.0159
   14   24    0     0.09     0.00     0.00     0.05         0.    0.0033
   15   24    0     0.09     0.00     0.00     0.06         0.    0.0031
   16   25    0     0.13     0.00     0.00     0.08         0.    0.0042
   17   25    0     0.11     0.00     0.00     0.07         0.    0.0036
   18   26    1     2.77     0.01     0.01     2.73         0.    0.0373
   19   27    0     2.74     0.01     0.01     2.71         0.    0.0186
   20   26    1     0.75     0.00     0.00     0.71         0.    0.0064
   21   27    0     0.78     0.00     0.01     0.75         0.    0.0032
   22   26    1     0.87     0.00     0.01     0.84         0.    0.0062
   23   27    0     0.80     0.00     0.00     0.77         0.    0.0031
   24   26    1     2.21     0.00     0.00     2.18         0.    0.0224
   25   27    0     2.17     0.00     0.01     2.14         0.    0.0112
   26   11    0     0.18     0.11     0.00     0.17         0.    0.1309
   27   13    0     1.41     0.09     0.00     1.38         0.    0.1884
   28   13    1     1.56     0.11     0.00     1.52         0.    0.1798
   29   14    0     1.54     0.09     0.00     1.49         0.    0.1908
   30   14    1     1.68     0.11     0.01     1.63         0.    0.1881
   31   31    0     3.00     0.00     0.04     2.97         0.    0.0014
   32   31    1     3.17     0.00     0.05     3.14         0.    0.0013
   33   32    0     3.29     0.00     0.04     3.25         0.    0.0015
   34   32    1     3.30     0.00     0.07     3.25         0.    0.0014
   35    1    1     0.03     0.03     0.00     0.03         0.    0.0244
   36    2    0     0.39     0.18     0.00     0.39         0.    0.3369
   37    3    1     0.66     0.02     0.00     0.63         0.    0.0314
   38    8    0     0.69     0.02     0.00     0.65         0.    0.0314
   39    3    1     0.25     0.01     0.00     0.21         0.    0.0196
   40    8    1     0.25     0.02     0.00     0.21         0.    0.0196
   41    3    0     0.70     0.02     0.00     0.66         0.    0.0238
   42    8    1     0.71     0.02     0.00     0.67         0.    0.0238
   43    4    0     0.93     0.02     0.00     0.89         0.    0.0349
   44    9    1     0.90     0.02     0.00     0.86         0.    0.0349
   45    4    0     0.31     0.01     0.00     0.27         0.    0.0190
   46    9    1     0.31     0.01     0.00     0.26         0.    0.0190
   47    4    0     0.80     0.02     0.00     0.76         0.    0.0258
   48    9    1     0.76     0.02     0.00     0.72         0.    0.0258
   49   75    0     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    0     0.07     0.03     0.00     0.07         0.    0.0712
   51   46    0     6.04     0.00     0.11     6.01         0.    0.0109
   52   46    1     6.53     0.00     0.10     6.48         0.    0.0126
   53   47    1     8.05     0.00     0.13     8.00         0.    0.0122
   54   47    0     7.92     0.00     0.09     7.86         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.39     0.14     0.38     0.00     0.00     0.90    43.98
  2     0.00     0.14     0.38     0.00     0.00     0.96    43.98
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.3477s 
time spent in multnx:                  84.5547s 
integral transfer time:                 0.8164s 
time spent for loop construction:       1.1016s 
syncronization time in mult:            0.3906s 
total time per CI iteration:           87.9609s 
 ** parallelization degree for mult section:0.9955

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         500.11        0.30       1641.384
    v  (write)         66.69        0.17        397.058
    w  (read)         266.77        0.16       1626.046
    w  (write)        330.39        0.32       1031.447
    dg (read)          66.69        0.03       2439.070
    dg (write)          0.00        0.00          0.000
    drt(read)           0.71        0.00        181.127
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1231.36        0.99       1245.963
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         566.97        0.32       1748.729
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        469.92        0.54        865.461
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.85        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1037.74        0.87       1196.677
========================================================


          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94106063    -0.04020517     0.33583990

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -155.4188820489  1.4361E-02  1.6304E-03  8.0467E-02  1.0000E-03
 mr-sdci #  2  2   -152.6218678548  7.1785E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  2  3   -151.8991802420  8.7182E+00  0.0000E+00  2.2541E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    0     1.56     0.05     0.01     1.56         0.    0.1070
    2   22    1     1.99     0.00     0.00     1.97         0.    0.0252
    3   28    1     1.48     0.01     0.01     1.44         0.    0.0252
    4   22    1     0.76     0.00     0.01     0.73         0.    0.0065
    5   28    0     0.78     0.00     0.00     0.75         0.    0.0065
    6   22    1     1.72     0.00     0.00     1.69         0.    0.0170
    7   28    1     1.14     0.00     0.01     1.10         0.    0.0170
    8   23    0     1.98     0.00     0.00     1.94         0.    0.0212
    9   29    0     1.37     0.00     0.00     1.33         0.    0.0212
   10   23    1     0.81     0.00     0.00     0.77         0.    0.0062
   11   29    0     0.86     0.00     0.00     0.83         0.    0.0062
   12   23    0     1.89     0.00     0.01     1.84         0.    0.0159
   13   29    1     1.16     0.01     0.01     1.13         0.    0.0159
   14   24    0     0.09     0.00     0.00     0.06         0.    0.0033
   15   24    1     0.10     0.00     0.00     0.06         0.    0.0031
   16   25    0     0.13     0.00     0.01     0.09         0.    0.0042
   17   25    1     0.12     0.00     0.01     0.08         0.    0.0036
   18   26    0     2.73     0.01     0.00     2.70         0.    0.0373
   19   27    1     2.75     0.01     0.00     2.73         0.    0.0186
   20   26    0     0.75     0.00     0.01     0.71         0.    0.0064
   21   27    1     0.79     0.00     0.02     0.74         0.    0.0032
   22   26    1     0.88     0.00     0.01     0.85         0.    0.0062
   23   27    1     0.82     0.00     0.01     0.79         0.    0.0031
   24   26    0     2.19     0.01     0.00     2.16         0.    0.0224
   25   27    1     2.19     0.01     0.00     2.16         0.    0.0112
   26   11    1     0.17     0.11     0.00     0.17         0.    0.1309
   27   13    0     1.37     0.09     0.00     1.34         0.    0.1884
   28   13    0     1.63     0.10     0.00     1.59         0.    0.1798
   29   14    1     1.44     0.09     0.00     1.40         0.    0.1908
   30   14    1     1.71     0.11     0.00     1.67         0.    0.1881
   31   31    0     2.98     0.00     0.04     2.95         0.    0.0014
   32   31    1     3.20     0.00     0.04     3.14         0.    0.0013
   33   32    0     3.30     0.00     0.05     3.26         0.    0.0015
   34   32    1     3.29     0.00     0.05     3.26         0.    0.0014
   35    1    1     0.04     0.02     0.00     0.04         0.    0.0244
   36    2    1     0.39     0.18     0.00     0.38         0.    0.3369
   37    3    0     0.67     0.02     0.00     0.64         0.    0.0314
   38    8    1     0.67     0.02     0.00     0.62         0.    0.0314
   39    3    0     0.23     0.01     0.00     0.21         0.    0.0196
   40    8    1     0.25     0.01     0.00     0.22         0.    0.0196
   41    3    0     0.67     0.02     0.00     0.64         0.    0.0238
   42    8    1     0.67     0.02     0.00     0.62         0.    0.0238
   43    4    0     0.84     0.02     0.00     0.80         0.    0.0349
   44    9    0     0.85     0.02     0.00     0.82         0.    0.0349
   45    4    0     0.31     0.01     0.00     0.27         0.    0.0190
   46    9    1     0.30     0.01     0.00     0.26         0.    0.0190
   47    4    0     0.73     0.02     0.00     0.69         0.    0.0258
   48    9    0     0.73     0.02     0.00     0.69         0.    0.0258
   49   75    0     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    0     0.08     0.03     0.00     0.08         0.    0.0712
   51   46    1     6.03     0.01     0.12     5.98         0.    0.0109
   52   46    0     6.38     0.00     0.09     6.35         0.    0.0126
   53   47    0     7.94     0.00     0.11     7.89         0.    0.0122
   54   47    1     8.17     0.00     0.17     8.13         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.00     0.15     0.48     0.00     0.00     0.97    43.77
  2     0.00     0.15     0.48     0.00     0.00     0.94    43.77
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.1406s 
time spent in multnx:                  84.3320s 
integral transfer time:                 0.8164s 
time spent for loop construction:       1.0977s 
syncronization time in mult:            0.0078s 
total time per CI iteration:           87.5313s 
 ** parallelization degree for mult section:0.9999

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         732.16        0.37       1972.974
    v  (write)         66.69        0.16        426.837
    w  (read)         266.77        0.16       1665.706
    w  (write)        250.77        0.25       1018.985
    dg (read)          66.69        0.05       1313.345
    dg (write)          0.00        0.00          0.000
    drt(read)           0.59        0.00        152.188
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1383.68        0.99       1400.086
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         468.31        0.27       1737.491
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        549.54        0.64        852.616
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.97        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1018.81        0.91       1114.599
========================================================


          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94081904    -0.04304852    -0.02765025     0.33502511

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -155.4201869407  1.3049E-03  1.9775E-04  2.4644E-02  1.0000E-03
 mr-sdci #  3  2   -152.7608796579  1.3901E-01  0.0000E+00  1.3885E+00  1.0000E-03
 mr-sdci #  3  3   -152.1019770098  2.0280E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  3  4   -151.8979459224  8.7170E+00  0.0000E+00  2.2751E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    0     1.55     0.05     0.00     1.55         0.    0.1070
    2   22    0     1.95     0.00     0.01     1.93         0.    0.0252
    3   28    1     1.45     0.01     0.00     1.43         0.    0.0252
    4   22    1     0.78     0.00     0.01     0.72         0.    0.0065
    5   28    0     0.78     0.00     0.00     0.75         0.    0.0065
    6   22    1     1.73     0.00     0.00     1.70         0.    0.0170
    7   28    1     1.11     0.00     0.01     1.08         0.    0.0170
    8   23    1     2.04     0.00     0.00     2.00         0.    0.0212
    9   29    0     1.37     0.00     0.00     1.32         0.    0.0212
   10   23    1     0.81     0.00     0.00     0.77         0.    0.0062
   11   29    0     0.87     0.00     0.01     0.83         0.    0.0062
   12   23    0     1.88     0.00     0.03     1.85         0.    0.0159
   13   29    1     1.16     0.00     0.00     1.13         0.    0.0159
   14   24    1     0.09     0.00     0.00     0.05         0.    0.0033
   15   24    0     0.10     0.00     0.00     0.07         0.    0.0031
   16   25    0     0.13     0.00     0.01     0.09         0.    0.0042
   17   25    1     0.12     0.00     0.01     0.08         0.    0.0036
   18   26    0     2.73     0.01     0.01     2.70         0.    0.0373
   19   27    1     2.78     0.00     0.01     2.74         0.    0.0186
   20   26    0     0.76     0.00     0.01     0.71         0.    0.0064
   21   27    0     0.78     0.00     0.01     0.74         0.    0.0032
   22   26    0     0.86     0.00     0.01     0.82         0.    0.0062
   23   27    1     0.82     0.00     0.01     0.79         0.    0.0031
   24   26    1     2.21     0.00     0.00     2.17         0.    0.0224
   25   27    0     2.17     0.01     0.01     2.14         0.    0.0112
   26   11    0     0.18     0.11     0.00     0.17         0.    0.1309
   27   13    0     1.46     0.08     0.00     1.42         0.    0.1884
   28   13    0     1.64     0.09     0.00     1.60         0.    0.1798
   29   14    1     1.53     0.09     0.00     1.50         0.    0.1908
   30   14    1     1.72     0.11     0.00     1.67         0.    0.1881
   31   31    0     3.00     0.01     0.03     2.95         0.    0.0014
   32   31    1     3.19     0.00     0.02     3.14         0.    0.0013
   33   32    1     3.26     0.00     0.06     3.22         0.    0.0015
   34   32    0     3.25     0.00     0.03     3.21         0.    0.0014
   35    1    1     0.03     0.03     0.00     0.03         0.    0.0244
   36    2    1     0.39     0.18     0.00     0.39         0.    0.3369
   37    3    0     0.79     0.02     0.00     0.77         0.    0.0314
   38    8    0     0.68     0.02     0.00     0.65         0.    0.0314
   39    3    1     0.25     0.01     0.00     0.21         0.    0.0196
   40    8    1     0.24     0.02     0.00     0.21         0.    0.0196
   41    3    0     0.70     0.02     0.00     0.66         0.    0.0238
   42    8    1     0.70     0.02     0.00     0.65         0.    0.0238
   43    4    0     0.84     0.02     0.00     0.80         0.    0.0349
   44    9    1     0.81     0.02     0.00     0.77         0.    0.0349
   45    4    1     0.33     0.01     0.00     0.29         0.    0.0190
   46    9    0     0.29     0.01     0.00     0.25         0.    0.0190
   47    4    1     0.84     0.02     0.00     0.80         0.    0.0258
   48    9    1     0.69     0.02     0.00     0.66         0.    0.0258
   49   75    0     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    0     0.08     0.04     0.00     0.07         0.    0.0712
   51   46    1     6.03     0.01     0.13     5.98         0.    0.0109
   52   46    0     6.39     0.01     0.07     6.35         0.    0.0126
   53   47    1     8.10     0.01     0.13     8.05         0.    0.0122
   54   47    0     7.97     0.01     0.10     7.91         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.01     0.19     0.58     0.00     0.00     0.97    44.06
  2     0.00     0.19     0.58     0.00     0.00     1.07    44.06
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.4180s 
time spent in multnx:                  84.5469s 
integral transfer time:                 0.7500s 
time spent for loop construction:       1.0977s 
syncronization time in mult:            0.0078s 
total time per CI iteration:           88.1211s 
 ** parallelization degree for mult section:0.9999

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         827.95        0.46       1811.576
    v  (write)         66.69        0.18        371.163
    w  (read)         333.47        0.18       1855.814
    w  (write)        332.97        0.34        968.635
    dg (read)          66.69        0.04       1897.054
    dg (write)          0.00        0.00          0.000
    drt(read)           0.70        0.00        180.211
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1628.47        1.20       1357.944
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         505.91        0.29       1726.827
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        467.34        0.55        848.496
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.86        0.00        219.553
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          974.10        0.85       1149.168
========================================================


          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94032319    -0.09435292    -0.10624395     0.22027921     0.21697722

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -155.4203528526  1.6591E-04  4.7145E-05  1.1016E-02  1.0000E-03
 mr-sdci #  4  2   -153.6657715561  9.0489E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  4  3   -152.2859004440  1.8392E-01  0.0000E+00  1.8794E+00  1.0000E-04
 mr-sdci #  4  4   -151.9946244076  9.6678E-02  0.0000E+00  2.0714E+00  1.0000E-04
 mr-sdci #  4  5   -151.4749468769  8.2940E+00  0.0000E+00  1.9627E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    0     1.55     0.04     0.01     1.55         0.    0.1070
    2   22    0     1.95     0.01     0.00     1.93         0.    0.0252
    3   28    0     1.45     0.00     0.00     1.42         0.    0.0252
    4   22    1     0.75     0.00     0.00     0.72         0.    0.0065
    5   28    0     0.78     0.00     0.00     0.75         0.    0.0065
    6   22    1     1.73     0.00     0.00     1.70         0.    0.0170
    7   28    0     1.10     0.00     0.01     1.07         0.    0.0170
    8   23    1     2.04     0.00     0.01     2.01         0.    0.0212
    9   29    1     1.39     0.00     0.00     1.35         0.    0.0212
   10   23    1     0.83     0.00     0.02     0.79         0.    0.0062
   11   29    1     0.87     0.00     0.01     0.83         0.    0.0062
   12   23    0     1.89     0.00     0.00     1.84         0.    0.0159
   13   29    0     1.15     0.01     0.00     1.11         0.    0.0159
   14   24    1     0.09     0.00     0.01     0.05         0.    0.0033
   15   24    1     0.10     0.00     0.00     0.06         0.    0.0031
   16   25    1     0.12     0.00     0.00     0.08         0.    0.0042
   17   25    0     0.13     0.00     0.01     0.07         0.    0.0036
   18   26    0     2.73     0.01     0.02     2.70         0.    0.0373
   19   27    1     2.78     0.01     0.01     2.73         0.    0.0186
   20   26    1     0.76     0.00     0.00     0.73         0.    0.0064
   21   27    0     0.78     0.00     0.01     0.74         0.    0.0032
   22   26    1     0.88     0.00     0.00     0.84         0.    0.0062
   23   27    0     0.82     0.00     0.00     0.78         0.    0.0031
   24   26    0     2.20     0.00     0.00     2.15         0.    0.0224
   25   27    1     2.18     0.00     0.01     2.15         0.    0.0112
   26   11    0     0.18     0.11     0.00     0.17         0.    0.1309
   27   13    1     1.36     0.08     0.00     1.33         0.    0.1884
   28   13    0     1.63     0.10     0.00     1.59         0.    0.1798
   29   14    1     1.45     0.11     0.00     1.41         0.    0.1908
   30   14    1     1.71     0.11     0.00     1.66         0.    0.1881
   31   31    0     2.98     0.00     0.05     2.95         0.    0.0014
   32   31    1     3.19     0.00     0.05     3.14         0.    0.0013
   33   32    1     3.27     0.00     0.05     3.23         0.    0.0015
   34   32    0     3.25     0.00     0.04     3.21         0.    0.0014
   35    1    1     0.03     0.02     0.00     0.03         0.    0.0244
   36    2    0     0.39     0.18     0.00     0.39         0.    0.3369
   37    3    1     0.66     0.02     0.00     0.63         0.    0.0314
   38    8    1     0.67     0.02     0.00     0.63         0.    0.0314
   39    3    0     0.25     0.01     0.00     0.21         0.    0.0196
   40    8    1     0.24     0.02     0.00     0.21         0.    0.0196
   41    3    0     0.67     0.02     0.00     0.64         0.    0.0238
   42    8    1     0.71     0.02     0.00     0.66         0.    0.0238
   43    4    1     0.80     0.02     0.00     0.77         0.    0.0349
   44    9    0     0.85     0.03     0.00     0.81         0.    0.0349
   45    4    0     0.31     0.01     0.00     0.27         0.    0.0190
   46    9    1     0.30     0.01     0.00     0.26         0.    0.0190
   47    4    0     0.72     0.02     0.00     0.69         0.    0.0258
   48    9    0     0.77     0.02     0.00     0.72         0.    0.0258
   49   75    0     0.01     0.00     0.00     0.01         0.    0.0082
   50   45    0     0.07     0.03     0.00     0.07         0.    0.0712
   51   46    1     6.02     0.01     0.16     5.97         0.    0.0109
   52   46    0     6.38     0.01     0.16     6.34         0.    0.0126
   53   47    1     8.09     0.00     0.13     8.05         0.    0.0122
   54   47    0     7.96     0.01     0.12     7.91         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.02     0.23     0.65     0.00     0.00     1.02    43.96
  2     0.00     0.23     0.66     0.00     0.00     1.12    43.97
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    85.9883s 
time spent in multnx:                  84.1289s 
integral transfer time:                 0.9180s 
time spent for loop construction:       1.1094s 
syncronization time in mult:            0.0156s 
total time per CI iteration:           87.9336s 
 ** parallelization degree for mult section:0.9998

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         914.05        0.52       1746.253
    v  (write)         66.69        0.13        533.546
    w  (read)         400.16        0.24       1652.273
    w  (write)        228.99        0.22       1028.433
    dg (read)          66.69        0.04       1897.054
    dg (write)          0.00        0.00          0.000
    drt(read)           0.62        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1677.20        1.15       1460.422
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         553.19        0.33       1666.064
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        571.32        0.66        865.426
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.95        0.00        242.178
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1125.45        1.00       1129.862
========================================================


          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.93986960     0.09710010    -0.07794346    -0.11844623     0.18546504    -0.22959689

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -155.4204009270  4.8074E-05  1.5046E-05  7.1418E-03  1.0000E-03
 mr-sdci #  5  2   -154.5211801101  8.5541E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  5  3   -152.4086252820  1.2272E-01  0.0000E+00  1.7574E+00  1.0000E-04
 mr-sdci #  5  4   -152.0198140094  2.5190E-02  0.0000E+00  2.0345E+00  1.0000E-04
 mr-sdci #  5  5   -151.9634908887  4.8854E-01  0.0000E+00  1.8809E+00  1.0000E-04
 mr-sdci #  5  6   -151.4437214745  8.2627E+00  0.0000E+00  2.6765E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    1     1.57     0.04     0.00     1.56         0.    0.1070
    2   22    1     1.99     0.01     0.00     1.96         0.    0.0252
    3   28    0     1.44     0.00     0.00     1.41         0.    0.0252
    4   22    0     0.75     0.00     0.01     0.71         0.    0.0065
    5   28    0     0.79     0.00     0.00     0.76         0.    0.0065
    6   22    1     1.73     0.00     0.00     1.70         0.    0.0170
    7   28    1     1.12     0.00     0.02     1.09         0.    0.0170
    8   23    0     1.99     0.00     0.00     1.95         0.    0.0212
    9   29    1     1.38     0.00     0.01     1.35         0.    0.0212
   10   23    0     0.80     0.00     0.00     0.76         0.    0.0062
   11   29    0     0.87     0.00     0.00     0.83         0.    0.0062
   12   23    0     1.89     0.00     0.01     1.85         0.    0.0159
   13   29    1     1.18     0.00     0.01     1.14         0.    0.0159
   14   24    0     0.09     0.00     0.01     0.06         0.    0.0033
   15   24    1     0.10     0.00     0.00     0.07         0.    0.0031
   16   25    0     0.13     0.00     0.00     0.09         0.    0.0042
   17   25    1     0.13     0.00     0.00     0.09         0.    0.0036
   18   26    0     2.74     0.01     0.00     2.70         0.    0.0373
   19   27    1     2.76     0.01     0.00     2.73         0.    0.0186
   20   26    1     0.75     0.00     0.01     0.71         0.    0.0064
   21   27    1     0.80     0.01     0.01     0.77         0.    0.0032
   22   26    0     0.86     0.00     0.00     0.82         0.    0.0062
   23   27    1     0.82     0.00     0.01     0.78         0.    0.0031
   24   26    1     2.21     0.01     0.00     2.18         0.    0.0224
   25   27    0     2.18     0.01     0.01     2.14         0.    0.0112
   26   11    1     0.18     0.11     0.00     0.17         0.    0.1309
   27   13    0     1.38     0.09     0.00     1.35         0.    0.1884
   28   13    0     1.63     0.11     0.00     1.59         0.    0.1798
   29   14    0     1.43     0.09     0.00     1.40         0.    0.1908
   30   14    1     1.72     0.10     0.00     1.68         0.    0.1881
   31   31    0     2.98     0.00     0.04     2.95         0.    0.0014
   32   31    1     3.18     0.00     0.03     3.13         0.    0.0013
   33   32    1     3.27     0.00     0.05     3.23         0.    0.0015
   34   32    0     3.25     0.00     0.07     3.21         0.    0.0014
   35    1    0     0.03     0.03     0.00     0.03         0.    0.0244
   36    2    0     0.39     0.19     0.00     0.39         0.    0.3369
   37    3    1     0.66     0.02     0.00     0.63         0.    0.0314
   38    8    0     0.73     0.02     0.00     0.70         0.    0.0314
   39    3    0     0.25     0.01     0.00     0.21         0.    0.0196
   40    8    0     0.24     0.01     0.00     0.21         0.    0.0196
   41    3    1     0.71     0.02     0.00     0.67         0.    0.0238
   42    8    0     0.81     0.02     0.00     0.77         0.    0.0238
   43    4    0     0.84     0.02     0.00     0.81         0.    0.0349
   44    9    1     0.82     0.02     0.00     0.79         0.    0.0349
   45    4    0     0.29     0.01     0.00     0.25         0.    0.0190
   46    9    1     0.29     0.01     0.00     0.26         0.    0.0190
   47    4    1     0.86     0.03     0.00     0.82         0.    0.0258
   48    9    1     0.69     0.02     0.00     0.66         0.    0.0258
   49   75    1     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    1     0.07     0.03     0.00     0.07         0.    0.0712
   51   46    1     6.04     0.00     0.11     5.99         0.    0.0109
   52   46    0     6.38     0.01     0.09     6.34         0.    0.0126
   53   47    1     8.08     0.00     0.11     8.04         0.    0.0122
   54   47    0     7.98     0.00     0.07     7.92         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.00     0.27     0.72     0.00     0.00     1.14    44.20
  2     0.01     0.27     0.72     0.00     0.00     1.04    44.20
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.2578s 
time spent in multnx:                  84.4961s 
integral transfer time:                 0.7031s 
time spent for loop construction:       1.0820s 
syncronization time in mult:            0.0078s 
total time per CI iteration:           88.3984s 
 ** parallelization degree for mult section:0.9999

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1144.58        0.67       1703.564
    v  (write)        133.39        0.05       2439.069
    w  (read)         466.85        0.32       1439.933
    w  (write)        396.38        0.34       1179.923
    dg (read)          66.69        0.04       1897.054
    dg (write)          0.00        0.00          0.000
    drt(read)           0.70        0.00        178.428
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         2208.59        1.43       1549.040
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         456.04        0.26       1742.498
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        470.62        0.50        948.645
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.86        0.00        221.336
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          927.53        0.76       1217.675
========================================================


          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.93987620    -0.00211908

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -155.4204130277  1.2101E-05  4.8763E-06  3.6061E-03  1.0000E-03
 mr-sdci #  6  2   -152.7284105780 -1.7928E+00  0.0000E+00  0.0000E+00  1.0000E-03

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    0     1.55     0.05     0.00     1.55         0.    0.1070
    2   22    0     1.95     0.01     0.00     1.93         0.    0.0252
    3   28    1     1.45     0.00     0.01     1.43         0.    0.0252
    4   22    0     0.75     0.00     0.00     0.71         0.    0.0065
    5   28    0     0.79     0.00     0.01     0.76         0.    0.0065
    6   22    1     1.73     0.00     0.00     1.70         0.    0.0170
    7   28    1     1.13     0.01     0.00     1.08         0.    0.0170
    8   23    1     2.05     0.00     0.00     2.02         0.    0.0212
    9   29    0     1.38     0.00     0.00     1.33         0.    0.0212
   10   23    0     0.79     0.00     0.00     0.76         0.    0.0062
   11   29    0     0.86     0.00     0.00     0.82         0.    0.0062
   12   23    0     1.89     0.00     0.00     1.84         0.    0.0159
   13   29    1     1.19     0.00     0.00     1.14         0.    0.0159
   14   24    0     0.09     0.00     0.00     0.06         0.    0.0033
   15   24    1     0.10     0.00     0.00     0.06         0.    0.0031
   16   25    0     0.13     0.00     0.00     0.09         0.    0.0042
   17   25    1     0.13     0.00     0.02     0.09         0.    0.0036
   18   26    0     2.74     0.01     0.01     2.70         0.    0.0373
   19   27    1     2.78     0.01     0.01     2.74         0.    0.0186
   20   26    1     0.76     0.00     0.00     0.73         0.    0.0064
   21   27    1     0.79     0.00     0.00     0.75         0.    0.0032
   22   26    1     0.90     0.00     0.01     0.86         0.    0.0062
   23   27    1     0.83     0.00     0.00     0.79         0.    0.0031
   24   26    1     2.22     0.00     0.01     2.18         0.    0.0224
   25   27    0     2.18     0.01     0.02     2.15         0.    0.0112
   26   11    0     0.18     0.11     0.00     0.18         0.    0.1309
   27   13    0     1.40     0.09     0.00     1.38         0.    0.1884
   28   13    0     1.63     0.11     0.00     1.59         0.    0.1798
   29   14    1     1.48     0.09     0.00     1.44         0.    0.1908
   30   14    1     1.73     0.11     0.00     1.68         0.    0.1881
   31   31    0     2.98     0.00     0.06     2.95         0.    0.0014
   32   31    1     3.17     0.00     0.04     3.15         0.    0.0013
   33   32    1     3.28     0.00     0.07     3.24         0.    0.0015
   34   32    0     3.28     0.00     0.04     3.23         0.    0.0014
   35    1    0     0.03     0.03     0.00     0.03         0.    0.0244
   36    2    0     0.39     0.19     0.00     0.39         0.    0.3369
   37    3    1     0.66     0.02     0.00     0.63         0.    0.0314
   38    8    1     0.64     0.02     0.00     0.62         0.    0.0314
   39    3    0     0.26     0.01     0.00     0.23         0.    0.0196
   40    8    1     0.24     0.01     0.00     0.21         0.    0.0196
   41    3    1     0.71     0.02     0.00     0.68         0.    0.0238
   42    8    1     0.69     0.02     0.00     0.66         0.    0.0238
   43    4    0     0.85     0.02     0.00     0.82         0.    0.0349
   44    9    0     0.88     0.02     0.00     0.83         0.    0.0349
   45    4    1     0.32     0.01     0.00     0.28         0.    0.0190
   46    9    0     0.31     0.01     0.00     0.27         0.    0.0190
   47    4    0     0.74     0.02     0.00     0.70         0.    0.0258
   48    9    0     0.80     0.02     0.00     0.75         0.    0.0258
   49   75    1     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    1     0.07     0.02     0.00     0.07         0.    0.0712
   51   46    1     6.02     0.00     0.06     5.99         0.    0.0109
   52   46    0     6.38     0.01     0.11     6.35         0.    0.0126
   53   47    1     8.10     0.01     0.11     8.05         0.    0.0122
   54   47    0     7.96     0.00     0.13     7.91         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.00     0.11     0.31     0.00     0.00     0.78    43.63
  2     0.00     0.11     0.31     0.00     0.00     0.86    43.63
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.3438s 
time spent in multnx:                  84.5703s 
integral transfer time:                 0.7539s 
time spent for loop construction:       1.1055s 
syncronization time in mult:            0.0000s 
total time per CI iteration:           87.2500s 
 ** parallelization degree for mult section:1.0000

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         495.57        0.28       1786.861
    v  (write)         66.69        0.04       1897.054
    w  (read)         200.08        0.13       1506.484
    w  (write)        267.56        0.27       1007.289
    dg (read)          66.69        0.04       1552.135
    dg (write)          0.00        0.00          0.000
    drt(read)           0.60        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1097.20        0.75       1455.357
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         571.51        0.31       1851.967
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        532.74        0.59        897.251
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.96        0.00        246.109
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1105.21        0.91       1219.541
========================================================


          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.93979305    -0.03219220     0.02668144

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -155.4204190204  5.9927E-06  1.9196E-06  2.2919E-03  1.0000E-03
 mr-sdci #  7  2   -154.4184481086  1.6900E+00  0.0000E+00  1.6743E+00  1.0000E-03
 mr-sdci #  7  3   -151.6920228208 -7.1660E-01  0.0000E+00  1.6149E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    1     1.58     0.05     0.01     1.58         0.    0.1070
    2   22    1     1.99     0.01     0.00     1.96         0.    0.0252
    3   28    0     1.46     0.00     0.01     1.42         0.    0.0252
    4   22    0     0.76     0.00     0.00     0.71         0.    0.0065
    5   28    0     0.78     0.00     0.00     0.75         0.    0.0065
    6   22    1     1.75     0.00     0.01     1.71         0.    0.0170
    7   28    0     1.11     0.01     0.01     1.08         0.    0.0170
    8   23    0     1.99     0.00     0.00     1.96         0.    0.0212
    9   29    1     1.39     0.00     0.01     1.36         0.    0.0212
   10   23    0     0.80     0.00     0.00     0.77         0.    0.0062
   11   29    0     0.88     0.00     0.00     0.84         0.    0.0062
   12   23    0     1.88     0.00     0.00     1.85         0.    0.0159
   13   29    0     1.14     0.00     0.00     1.11         0.    0.0159
   14   24    1     0.09     0.00     0.01     0.05         0.    0.0033
   15   24    1     0.10     0.01     0.01     0.06         0.    0.0031
   16   25    1     0.11     0.00     0.00     0.08         0.    0.0042
   17   25    0     0.13     0.00     0.00     0.08         0.    0.0036
   18   26    1     2.79     0.00     0.01     2.75         0.    0.0373
   19   27    0     2.73     0.01     0.00     2.70         0.    0.0186
   20   26    1     0.75     0.00     0.00     0.72         0.    0.0064
   21   27    1     0.78     0.00     0.00     0.75         0.    0.0032
   22   26    1     0.88     0.00     0.00     0.84         0.    0.0062
   23   27    0     0.82     0.00     0.00     0.78         0.    0.0031
   24   26    0     2.19     0.00     0.01     2.15         0.    0.0224
   25   27    1     2.19     0.00     0.02     2.16         0.    0.0112
   26   11    0     0.18     0.11     0.00     0.17         0.    0.1309
   27   13    1     1.36     0.09     0.00     1.33         0.    0.1884
   28   13    1     1.58     0.10     0.00     1.54         0.    0.1798
   29   14    0     1.45     0.10     0.00     1.41         0.    0.1908
   30   14    0     1.68     0.11     0.00     1.63         0.    0.1881
   31   31    0     2.98     0.00     0.05     2.95         0.    0.0014
   32   31    1     3.20     0.00     0.03     3.16         0.    0.0013
   33   32    0     3.32     0.00     0.04     3.27         0.    0.0015
   34   32    1     3.32     0.00     0.06     3.29         0.    0.0014
   35    1    1     0.04     0.03     0.00     0.04         0.    0.0244
   36    2    0     0.39     0.18     0.00     0.39         0.    0.3369
   37    3    0     0.68     0.02     0.00     0.65         0.    0.0314
   38    8    1     0.66     0.02     0.00     0.62         0.    0.0314
   39    3    0     0.25     0.02     0.00     0.22         0.    0.0196
   40    8    1     0.24     0.01     0.00     0.21         0.    0.0196
   41    3    0     0.78     0.02     0.00     0.73         0.    0.0238
   42    8    1     0.70     0.02     0.00     0.66         0.    0.0238
   43    4    1     0.81     0.02     0.00     0.78         0.    0.0349
   44    9    1     0.80     0.02     0.00     0.77         0.    0.0349
   45    4    0     0.32     0.01     0.00     0.27         0.    0.0190
   46    9    1     0.31     0.01     0.00     0.27         0.    0.0190
   47    4    1     0.80     0.02     0.00     0.76         0.    0.0258
   48    9    1     0.70     0.02     0.00     0.66         0.    0.0258
   49   75    0     0.01     0.00     0.00     0.01         0.    0.0082
   50   45    0     0.07     0.03     0.00     0.07         0.    0.0712
   51   46    1     6.04     0.00     0.08     6.00         0.    0.0109
   52   46    0     6.38     0.00     0.13     6.35         0.    0.0126
   53   47    0     7.92     0.00     0.12     7.87         0.    0.0122
   54   47    1     8.15     0.00     0.09     8.11         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.01     0.15     0.41     0.00     0.00     0.87    43.70
  2     0.00     0.15     0.41     0.00     0.00     0.93    43.70
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.1914s 
time spent in multnx:                  84.4102s 
integral transfer time:                 0.7344s 
time spent for loop construction:       1.0859s 
syncronization time in mult:            0.0117s 
total time per CI iteration:           87.3984s 
 ** parallelization degree for mult section:0.9999

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         685.85        0.42       1640.917
    v  (write)         66.69        0.02       2845.581
    w  (read)         266.77        0.21       1264.703
    w  (write)        265.25        0.27        998.586
    dg (read)          66.69        0.04       1707.349
    dg (write)          0.00        0.00          0.000
    drt(read)           0.67        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1351.93        0.96       1412.632
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         514.61        0.29       1780.288
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        535.05        0.56        951.208
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.89        0.00        227.795
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1050.56        0.86       1228.050
========================================================


          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.93969610     0.05614189     0.04294181     0.03005050

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.4204208664  1.8460E-06  4.4963E-07  1.2724E-03  1.0000E-03
 mr-sdci #  8  2   -154.8647846548  4.4634E-01  0.0000E+00  2.0184E+00  1.0000E-03
 mr-sdci #  8  3   -152.1635558744  4.7153E-01  0.0000E+00  1.3194E+00  1.0000E-04
 mr-sdci #  8  4   -151.6895785207 -3.3024E-01  0.0000E+00  2.3318E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    0     1.57     0.05     0.02     1.56         0.    0.1070
    2   22    0     1.95     0.00     0.00     1.93         0.    0.0252
    3   28    1     1.45     0.01     0.02     1.43         0.    0.0252
    4   22    1     0.77     0.00     0.02     0.74         0.    0.0065
    5   28    0     0.78     0.00     0.01     0.75         0.    0.0065
    6   22    1     1.74     0.00     0.02     1.70         0.    0.0170
    7   28    1     1.12     0.00     0.01     1.08         0.    0.0170
    8   23    1     2.06     0.00     0.01     2.02         0.    0.0212
    9   29    0     1.39     0.00     0.01     1.35         0.    0.0212
   10   23    0     0.83     0.01     0.00     0.78         0.    0.0062
   11   29    0     0.86     0.00     0.00     0.82         0.    0.0062
   12   23    0     1.88     0.00     0.00     1.84         0.    0.0159
   13   29    1     1.18     0.00     0.00     1.14         0.    0.0159
   14   24    1     0.09     0.00     0.00     0.05         0.    0.0033
   15   24    0     0.10     0.00     0.01     0.07         0.    0.0031
   16   25    1     0.14     0.00     0.00     0.10         0.    0.0042
   17   25    0     0.12     0.00     0.00     0.08         0.    0.0036
   18   26    0     2.73     0.00     0.01     2.70         0.    0.0373
   19   27    1     2.77     0.01     0.00     2.74         0.    0.0186
   20   26    0     0.75     0.00     0.01     0.71         0.    0.0064
   21   27    1     0.79     0.00     0.00     0.75         0.    0.0032
   22   26    0     0.86     0.00     0.00     0.83         0.    0.0062
   23   27    1     0.81     0.00     0.00     0.78         0.    0.0031
   24   26    1     2.22     0.00     0.01     2.19         0.    0.0224
   25   27    0     2.18     0.00     0.00     2.15         0.    0.0112
   26   11    1     0.18     0.12     0.00     0.18         0.    0.1309
   27   13    0     1.39     0.08     0.00     1.35         0.    0.1884
   28   13    1     1.59     0.10     0.00     1.55         0.    0.1798
   29   14    1     1.48     0.09     0.00     1.43         0.    0.1908
   30   14    0     1.67     0.11     0.00     1.62         0.    0.1881
   31   31    0     2.99     0.00     0.03     2.95         0.    0.0014
   32   31    1     3.20     0.00     0.05     3.17         0.    0.0013
   33   32    0     3.30     0.00     0.05     3.27         0.    0.0015
   34   32    1     3.32     0.00     0.05     3.28         0.    0.0014
   35    1    1     0.04     0.03     0.00     0.04         0.    0.0244
   36    2    1     0.39     0.17     0.00     0.39         0.    0.3369
   37    3    1     0.66     0.02     0.00     0.62         0.    0.0314
   38    8    0     0.68     0.02     0.00     0.65         0.    0.0314
   39    3    1     0.25     0.01     0.00     0.21         0.    0.0196
   40    8    0     0.24     0.01     0.00     0.21         0.    0.0196
   41    3    0     0.67     0.02     0.00     0.64         0.    0.0238
   42    8    0     0.67     0.02     0.00     0.63         0.    0.0238
   43    4    1     0.80     0.02     0.00     0.76         0.    0.0349
   44    9    1     0.89     0.02     0.00     0.86         0.    0.0349
   45    4    1     0.32     0.01     0.00     0.29         0.    0.0190
   46    9    0     0.30     0.01     0.00     0.27         0.    0.0190
   47    4    0     0.80     0.02     0.00     0.75         0.    0.0258
   48    9    1     0.71     0.02     0.00     0.67         0.    0.0258
   49   75    0     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    0     0.07     0.03     0.00     0.07         0.    0.0712
   51   46    1     6.03     0.00     0.07     6.00         0.    0.0109
   52   46    0     6.41     0.01     0.12     6.38         0.    0.0126
   53   47    0     7.92     0.01     0.11     7.88         0.    0.0122
   54   47    1     8.16     0.01     0.14     8.11         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.01     0.19     0.52     0.00     0.00     0.89    43.88
  2     0.00     0.19     0.52     0.00     0.00     0.92    43.88
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.2734s 
time spent in multnx:                  84.5234s 
integral transfer time:                 0.7813s 
time spent for loop construction:       1.0977s 
syncronization time in mult:            0.0078s 
total time per CI iteration:           87.7656s 
 ** parallelization degree for mult section:0.9999

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         828.22        0.47       1766.879
    v  (write)         66.69        0.02       3414.697
    w  (read)         333.47        0.18       1816.328
    w  (write)        281.16        0.27       1028.245
    dg (read)          66.69        0.04       1707.349
    dg (write)          0.00        0.00          0.000
    drt(read)           0.63        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1576.87        0.98       1601.899
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         505.63        0.25       2054.618
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        519.14        0.58        891.950
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.93        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1025.70        0.83       1238.584
========================================================


          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:      214466 2x:       53393 4x:        8315
All internal counts: zz :       32118 yy:      490036 xx:      220950 ww:      246856
One-external counts: yz :      161582 yx:      486981 yw:      504573
Two-external counts: yy :      191370 ww:       90378 xx:       91398 xz:        5228 wz:        6717 wx:      139846
Three-ext.   counts: yx :      573671 yw:      607988

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================



          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.93965441     0.06603857    -0.06620718     0.00420354     0.02094175

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4204212964  4.3000E-07  6.5437E-08  4.3538E-04  1.0000E-03
 mr-sdci #  9  2   -154.9303509593  6.5566E-02  0.0000E+00  1.8033E+00  1.0000E-03
 mr-sdci #  9  3   -152.6481781607  4.8462E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  9  4   -151.9923059050  3.0273E-01  0.0000E+00  1.6467E+00  1.0000E-04
 mr-sdci #  9  5   -151.6299720951 -3.3352E-01  0.0000E+00  2.0976E+00  1.0000E-04

======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   21    1     1.57     0.04     0.02     1.57         0.    0.1070
    2   22    1     1.99     0.00     0.00     1.96         0.    0.0252
    3   28    0     1.45     0.00     0.00     1.41         0.    0.0252
    4   22    0     0.74     0.00     0.00     0.71         0.    0.0065
    5   28    1     0.79     0.00     0.00     0.76         0.    0.0065
    6   22    1     1.72     0.00     0.00     1.70         0.    0.0170
    7   28    1     1.12     0.00     0.01     1.10         0.    0.0170
    8   23    0     1.98     0.01     0.01     1.95         0.    0.0212
    9   29    0     1.36     0.00     0.01     1.32         0.    0.0212
   10   23    0     0.79     0.00     0.00     0.76         0.    0.0062
   11   29    1     0.87     0.00     0.02     0.83         0.    0.0062
   12   23    0     1.88     0.00     0.00     1.85         0.    0.0159
   13   29    1     1.17     0.00     0.01     1.14         0.    0.0159
   14   24    0     0.09     0.00     0.01     0.05         0.    0.0033
   15   24    1     0.10     0.00     0.00     0.07         0.    0.0031
   16   25    1     0.14     0.00     0.02     0.10         0.    0.0042
   17   25    0     0.12     0.00     0.01     0.08         0.    0.0036
   18   26    1     2.77     0.01     0.00     2.75         0.    0.0373
   19   27    0     2.74     0.01     0.01     2.71         0.    0.0186
   20   26    1     0.75     0.00     0.00     0.71         0.    0.0064
   21   27    0     0.78     0.00     0.01     0.74         0.    0.0032
   22   26    0     0.86     0.00     0.01     0.82         0.    0.0062
   23   27    1     0.82     0.00     0.00     0.78         0.    0.0031
   24   26    0     2.19     0.01     0.01     2.15         0.    0.0224
   25   27    1     2.18     0.01     0.01     2.15         0.    0.0112
   26   11    1     0.18     0.10     0.00     0.17         0.    0.1309
   27   13    1     1.37     0.08     0.00     1.34         0.    0.1884
   28   13    0     1.63     0.10     0.00     1.59         0.    0.1798
   29   14    0     1.43     0.09     0.00     1.40         0.    0.1908
   30   14    1     1.70     0.11     0.00     1.66         0.    0.1881
   31   31    0     2.99     0.00     0.02     2.96         0.    0.0014
   32   31    1     3.18     0.00     0.05     3.14         0.    0.0013
   33   32    0     3.30     0.00     0.06     3.26         0.    0.0015
   34   32    1     3.30     0.00     0.04     3.26         0.    0.0014
   35    1    0     0.04     0.03     0.00     0.04         0.    0.0244
   36    2    0     0.39     0.19     0.00     0.39         0.    0.3369
   37    3    1     0.65     0.02     0.00     0.62         0.    0.0314
   38    8    1     0.65     0.02     0.00     0.62         0.    0.0314
   39    3    0     0.25     0.01     0.00     0.22         0.    0.0196
   40    8    0     0.25     0.01     0.00     0.21         0.    0.0196
   41    3    0     0.71     0.02     0.00     0.66         0.    0.0238
   42    8    1     0.70     0.02     0.00     0.65         0.    0.0238
   43    4    0     1.00     0.02     0.00     0.96         0.    0.0349
   44    9    0     0.82     0.02     0.00     0.80         0.    0.0349
   45    4    0     0.29     0.01     0.00     0.25         0.    0.0190
   46    9    1     0.30     0.01     0.00     0.26         0.    0.0190
   47    4    1     0.86     0.03     0.00     0.82         0.    0.0258
   48    9    0     0.72     0.02     0.00     0.68         0.    0.0258
   49   75    1     0.00     0.00     0.00     0.00         0.    0.0082
   50   45    1     0.07     0.03     0.00     0.07         0.    0.0712
   51   46    1     6.01     0.01     0.08     5.98         0.    0.0109
   52   46    0     6.38     0.00     0.09     6.35         0.    0.0126
   53   47    0     7.94     0.00     0.12     7.90         0.    0.0122
   54   47    1     8.14     0.00     0.10     8.09         0.    0.0139
================================================================
================ TIMING STATISTICS PER NODE    ================
node#   twait      ths     tcin     teig    tvect    tga     ttotal 
  1     0.00     0.23     0.59     0.00     0.00     0.98    43.97
  2     0.00     0.23     0.59     0.00     0.00     0.92    43.97
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    86.2344s 
time spent in multnx:                  84.5039s 
integral transfer time:                 0.7500s 
time spent for loop construction:       1.1016s 
syncronization time in mult:            0.0000s 
total time per CI iteration:           87.9375s 
 ** parallelization degree for mult section:1.0000

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         984.64        0.52       1895.247
    v  (write)         66.69        0.03       2439.069
    w  (read)         400.16        0.26       1528.969
    w  (write)        280.19        0.29        956.386
    dg (read)          66.69        0.04       1897.054
    dg (write)          0.00        0.00          0.000
    drt(read)           0.60        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1798.97        1.14       1582.602
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         482.60        0.22       2167.465
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        520.11        0.52       1001.119
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.97        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1003.68        0.74       1352.324
========================================================


 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4204212964  4.3000E-07  6.5437E-08  4.3538E-04  1.0000E-03
 mr-sdci #  9  2   -154.9303509593  6.5566E-02  0.0000E+00  1.8033E+00  1.0000E-03
 mr-sdci #  9  3   -152.6481781607  4.8462E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  9  4   -151.9923059050  3.0273E-01  0.0000E+00  1.6467E+00  1.0000E-04
 mr-sdci #  9  5   -151.6299720951 -3.3352E-01  0.0000E+00  2.0976E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -155.420421296366

################END OF CIUDGINFO################


    1 of the   6 expansion vectors are transformed.
    1 of the   5 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 I/O during diagon finalize 

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         333.47        1.24        269.299
    v  (write)         66.69        0.07        948.529
    w  (read)         333.47        0.70        474.265
    w  (write)         66.69        0.61        110.152
    dg (read)           0.00        0.07          0.002
    dg (write)          0.00        0.00          0.000
    drt(read)           0.00        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          800.32        2.69        297.795
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.45          0.002
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        1.04          0.001
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.00        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.00        1.48          0.001
========================================================

 maximum overlap with reference                         1 (overlap= 
   0.9396544144511727      )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -155.4204212964

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13

                                          orbital     5    6    7   54   55   56   57   58   59  107  108  127  128

                                         symmetry   Ag   Ag   Ag                                                   

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.153133                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       2 -0.906638                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       3 -0.055582                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       4  0.054105                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       5  0.062528                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 z*  1  1       6  0.103927                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1       8 -0.106376                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1       9  0.062245                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      11 -0.024875                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      12  0.062547                        +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +- 
 y   1  1     422  0.013397              2(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     493 -0.012670              1(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     548 -0.010159              2(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     583  0.012231              1(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     584 -0.014557              2(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     620 -0.011788              2(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     621  0.010627              3(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     674 -0.014636              2(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +-      
 y   1  1     829  0.013675              2( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     832  0.013006              5( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     836 -0.010184              9( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    2817  0.010506              1( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    2965 -0.012357             10(    )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    2967 -0.013120             12(    )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3146 -0.010969              5(    )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3151 -0.011897             10(    )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3153 -0.013586             12(    )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3341 -0.010047             12( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1   10728  0.011966              7(    )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1   11054 -0.014454              6( Ag )   +-   +-   +-   +-   +-   +-   +    +-   +-    -        +-    - 
 y   1  1   15604 -0.010075             12(    )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-        +     - 
 y   1  1   15790 -0.010234             12(    )   +-   +-   +-   +-   +-    -   +-   +-   +-   +     -   +-      
 y   1  1   20426 -0.013107              7( Ag )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   58273 -0.013748              5(    )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   58413  0.011317              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   58599  0.013056              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   58784 -0.010922              6(    )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   58791 -0.011221             13(    )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   78375 -0.011391              5(    )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78376  0.012565              6(    )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78380 -0.016418             10(    )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78382 -0.018018             12(    )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78517  0.010210              7( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   78521  0.011117             11( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   78522  0.012381             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   78708  0.014234             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   78712  0.010173             16( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   78892  0.012050             12(    )   +-    -   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  101795 -0.010977              9( Ag )    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  101976  0.010893              6(    )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  101983  0.011355             13(    )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 x   1  1 3351659 -0.013938    7(    )   3(    )   +-    -   +-   +-   +-   +-    -   +-             +-   +-   +- 
 x   1  1 3360026  0.010967    6( Ag )   3(    )   +-    -   +-   +-   +-   +-    -   +    +-   +-    -        +- 
 w   1  1 4112533  0.010485    7(    )   7(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1 4113805  0.010550    1(    )   1(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1 4116808 -0.017679    6( Ag )   7(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 4118689 -0.015654    1(    )   1(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 4129042  0.013500    6( Ag )   6( Ag )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1 4131231  0.010131    1(    )   1(    )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1 4384478  0.010125    5(    )   5(    )   +-   +-   +-   +-   +-   +-   +-        +-   +-        +-      
 w   1  1 7036798  0.011906    5(    )   5(    )   +-   +-   +     -   +-   +-   +-        +-   +-   +    +-    - 
 w   1  1 7722695  0.010153    5(    )   9(    )   +-   +    +-    -   +-   +-   +-        +-   +-   +    +-    - 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              58
     0.01> rq > 0.001          16767
    0.001> rq > 0.0001        248367
   0.0001> rq > 0.00001      1116968
  0.00001> rq > 0.000001     2014799
 0.000001> rq                5344662
           all               8741625
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:       11918 2x:           0 4x:           0
All internal counts: zz :       32118 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.153132805806    -23.749698472787
     2     2     -0.906637528366    140.535086863724
     3     3     -0.055581697059      8.619309699665
     4     4      0.054105400902     -8.394675403409
     5     5      0.062527530868     -9.694523017966
     6     6      0.103927247615    -16.126798057043
     7     7     -0.004893509382      0.759445835648
     8     8     -0.106376068186     16.504157730711
     9     9      0.062245277057     -9.656203142345
    10    10      0.004890336885     -0.759499918618
    11    11     -0.024875001323      3.862702625742
    12    12      0.062546712953     -9.706601860005
    13    13      0.005556844741     -0.861534677024
    14    14      0.000350510053     -0.053953449969
    15    15     -0.001068375083      0.165738452561
    16    16      0.004054005799     -0.628146239169
    17    17     -0.000393673725      0.061149206833
    18    18     -0.000634386725      0.098435866898
    19    19     -0.000067331589      0.010106817614
    20    20     -0.000035845580      0.005603258267
    21    21      0.000211017886     -0.032760719835
    22    22     -0.001225129846      0.190027975989
    23    23      0.000070506439     -0.010723246082
    24    24      0.000087007807     -0.013498737530
    25    25      0.000195682605     -0.030301513527
    26    26     -0.000097614866      0.015076857263
    27    27      0.005140612725     -0.797259854070
    28    28      0.000247081437     -0.038033632803
    29    29     -0.000972534493      0.150990195503
    30    30      0.003858650926     -0.598166952910
    31    31     -0.000472486243      0.073109508233
    32    32     -0.000590950265      0.091732505415
    33    33     -0.000048210967      0.007484850811

 number of reference csfs (nref) is    33.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 22 correlated electrons.

 eref      =   -155.015472364017   "relaxed" cnot**2         =   0.886031253238
 eci       =   -155.420421296366   deltae = eci - eref       =  -0.404948932349
 eci+dv1   =   -155.466572818688   dv1 = (1-cnot**2)*deltae  =  -0.046151522323
 eci+dv2   =   -155.472509213303   dv2 = dv1 / cnot**2       =  -0.052087916937
 eci+dv3   =   -155.480198221078   dv3 = dv1 / (2*cnot**2-1) =  -0.059776924712
 eci+pople =   -155.473253479140   ( 22e- scaled deltae )    =  -0.457781115123
 ims=                        1 imnl=                        2
 mtype:13_14_14_14_14_
input orbital labels, i:bfnlab(i)=
   1:  1C1s     2:  2C1s     3:  3C1s     4:  4C1s     5:  5C1px    6:  6C1py 
   7:  7C1px    8:  8C1py    9:  9C1px   10: 10C1py   11: 11C1d2-  12: 12C1d0 
  13: 13C1d2+  14: 14C1d2-  15: 15C1d0   16: 16C1d2+  17: 17C2s    18: 18C2s  
  19: 19C2s    20: 20C2s    21: 21C2px   22: 22C2py   23: 23C2px   24: 24C2py 
  25: 25C2px   26: 26C2py   27: 27C2d2-  28: 28C2d0   29: 29C2d2+  30: 30C2d2-
  31: 31C2d0   32: 32C2d2+  33: 33H1s    34: 34H1s    35: 35H1s    36: 36H1px 
  37: 37H1py   38: 38H1px   39: 39H1py   40: 40H2s    41: 41H2s    42: 42H2s  
  43: 43H2px   44: 44H2py   45: 45H2px   46: 46H2py   47: 47H3s    48: 48H3s  
  49: 49H3s    50: 50H3px   51: 51H3py   52: 52H3px   53: 53H3py   54: 54C1s  
  55: 55C1s    56: 56C1s    57: 57C1s    58: 58C1px   59: 59C1py   60: 60C1px 
  61: 61C1py   62: 62C1px   63: 63C1py   64: 64C1d2-  65: 65C1d0   66: 66C1d2+
  67: 67C1d2-  68: 68C1d0   69: 69C1d2+  70: 70C2s    71: 71C2s    72: 72C2s  
  73: 73C2s    74: 74C2px   75: 75C2py   76: 76C2px   77: 77C2py   78: 78C2px 
  79: 79C2py   80: 80C2d2-  81: 81C2d0   82: 82C2d2+  83: 83C2d2-  84: 84C2d0 
  85: 85C2d2+  86: 86H1s    87: 87H1s    88: 88H1s    89: 89H1px   90: 90H1py 
  91: 91H1px   92: 92H1py   93: 93H2s    94: 94H2s    95: 95H2s    96: 96H2px 
  97: 97H2py   98: 98H2px   99: 99H2py  100:100H3s   101:101H3s   102:102H3s  
 103:103H3px  104:104H3py  105:105H3px  106:106H3py  107:107C1pz  108:108C1pz 
 109:109C1pz  110:110C1d1- 111:111C1d1+ 112:112C1d1- 113:113C1d1+ 114:114C2pz 
 115:115C2pz  116:116C2pz  117:117C2d1- 118:118C2d1+ 119:119C2d1- 120:120C2d1+
 121:121H1pz  122:122H1pz  123:123H2pz  124:124H2pz  125:125H3pz  126:126H3pz 
 127:127C1pz  128:128C1pz  129:129C1pz  130:130C1d1- 131:131C1d1+ 132:132C1d1-
 133:133C1d1+ 134:134C2pz  135:135C2pz  136:136C2pz  137:137C2d1- 138:138C2d1+
 139:139C2d1- 140:140C2d1+ 141:141H1pz  142:142H1pz  143:143H2pz  144:144H2pz 
 145:145H3pz  146:146H3pz 
bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  1   9:  1  10:  1
  11:  1  12:  1  13:  1  14:  1  15:  1  16:  1  17:  2  18:  2  19:  2  20:  2
  21:  2  22:  2  23:  2  24:  2  25:  2  26:  2  27:  2  28:  2  29:  2  30:  2
  31:  2  32:  2  33:  3  34:  3  35:  3  36:  3  37:  3  38:  3  39:  3  40:  4
  41:  4  42:  4  43:  4  44:  4  45:  4  46:  4  47:  5  48:  5  49:  5  50:  5
  51:  5  52:  5  53:  5  54:  1  55:  1  56:  1  57:  1  58:  1  59:  1  60:  1
  61:  1  62:  1  63:  1  64:  1  65:  1  66:  1  67:  1  68:  1  69:  1  70:  2
  71:  2  72:  2  73:  2  74:  2  75:  2  76:  2  77:  2  78:  2  79:  2  80:  2
  81:  2  82:  2  83:  2  84:  2  85:  2  86:  3  87:  3  88:  3  89:  3  90:  3
  91:  3  92:  3  93:  4  94:  4  95:  4  96:  4  97:  4  98:  4  99:  4 100:  5
 101:  5 102:  5 103:  5 104:  5 105:  5 106:  5 107:  1 108:  1 109:  1 110:  1
 111:  1 112:  1 113:  1 114:  2 115:  2 116:  2 117:  2 118:  2 119:  2 120:  2
 121:  3 122:  3 123:  4 124:  4 125:  5 126:  5 127:  1 128:  1 129:  1 130:  1
 131:  1 132:  1 133:  1 134:  2 135:  2 136:  2 137:  2 138:  2 139:  2 140:  2
 141:  3 142:  3 143:  4 144:  4 145:  5 146:  5
bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  2   6:  2   7:  2   8:  2   9:  2  10:  2
  11:  4  12:  4  13:  4  14:  4  15:  4  16:  4  17:  1  18:  1  19:  1  20:  1
  21:  2  22:  2  23:  2  24:  2  25:  2  26:  2  27:  4  28:  4  29:  4  30:  4
  31:  4  32:  4  33:  1  34:  1  35:  1  36:  2  37:  2  38:  2  39:  2  40:  1
  41:  1  42:  1  43:  2  44:  2  45:  2  46:  2  47:  1  48:  1  49:  1  50:  2
  51:  2  52:  2  53:  2  54:  1  55:  1  56:  1  57:  1  58:  2  59:  2  60:  2
  61:  2  62:  2  63:  2  64:  4  65:  4  66:  4  67:  4  68:  4  69:  4  70:  1
  71:  1  72:  1  73:  1  74:  2  75:  2  76:  2  77:  2  78:  2  79:  2  80:  4
  81:  4  82:  4  83:  4  84:  4  85:  4  86:  1  87:  1  88:  1  89:  2  90:  2
  91:  2  92:  2  93:  1  94:  1  95:  1  96:  2  97:  2  98:  2  99:  2 100:  1
 101:  1 102:  1 103:  2 104:  2 105:  2 106:  2 107:  2 108:  2 109:  2 110:  4
 111:  4 112:  4 113:  4 114:  2 115:  2 116:  2 117:  4 118:  4 119:  4 120:  4
 121:  2 122:  2 123:  2 124:  2 125:  2 126:  2 127:  2 128:  2 129:  2 130:  4
 131:  4 132:  4 133:  4 134:  2 135:  2 136:  2 137:  4 138:  4 139:  4 140:  4
 141:  2 142:  2 143:  2 144:  2 145:  2 146:  2
  Number of symmetry-distinct centers:                        5
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref=  T  F
 computing final density

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=      242369589

 space required:

    space required for calls in multd2:
       onex         2491
       allin           0
       diagon      12755
    max.      ---------
       maxnex      12755

    total core space usage:
       maxnex      12755
    max k-seg    2376488
    max k-ind       4326
              ---------
       totmax    4774383
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
 1-DENSITY: executing task                        11
 1-DENSITY: executing task                        13
 1-DENSITY: executing task                        14
 1-DENSITY: executing task                         4
 1-DENSITY: executing task                         4
 1-DENSITY: executing task                        53
 1-DENSITY: executing task                        73
 1-DENSITY: executing task                        54
 1-DENSITY: executing task                        74
================================================================================
   DYZ=    5435  DYX=   16031  DYW=   16939
   D0Z=    1746  D0Y=   22064  D0X=    7405  D0W=    8533
  DDZI=    2933 DDYI=   29580 DDXI=   11282 DDWI=   12531
  DDZE=       0 DDYE=    4326 DDXE=    1882 DDWE=    2107
================================================================================
 Trace of MO density:     22.00000000000001     


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      1.9755411
   1.9728170      1.9717178      0.0125607      0.0116454      0.0103244
   0.0093066      0.0064497      0.0051293      0.0043826      0.0025321
   0.0021013      0.0015760      0.0011119      0.0009752      0.0009082
   0.0007113      0.0005582      0.0004520      0.0004419      0.0004174
   0.0003267      0.0002762      0.0002509      0.0002315      0.0001546
   0.0001379      0.0001154      0.0001075      0.0000944      0.0000842
   0.0000778      0.0000694      0.0000638      0.0000595      0.0000535
   0.0000494      0.0000469      0.0000440      0.0000352      0.0000300
   0.0000247      0.0000199      0.0000174      0.0000161      0.0000100
   0.0000093      0.0000081      0.0000046


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9995655      1.9995616      1.9851383      1.9784006      1.9754717
   1.9729353      0.0142228      0.0123892      0.0105242      0.0093159
   0.0087276      0.0066669      0.0051558      0.0039709      0.0023037
   0.0016997      0.0011466      0.0009649      0.0009239      0.0007253
   0.0006625      0.0005462      0.0004665      0.0004334      0.0003909
   0.0002767      0.0002359      0.0001830      0.0001705      0.0001364
   0.0001218      0.0001093      0.0000864      0.0000796      0.0000722
   0.0000686      0.0000630      0.0000615      0.0000592      0.0000579
   0.0000507      0.0000466      0.0000350      0.0000330      0.0000263
   0.0000216      0.0000178      0.0000158      0.0000129      0.0000090
   0.0000080      0.0000048      0.0000044


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9287718      0.1018458      0.0061610      0.0053503      0.0032942
   0.0027349      0.0019929      0.0006932      0.0005184      0.0004503
   0.0003337      0.0002644      0.0002127      0.0001687      0.0001098
   0.0000861      0.0000654      0.0000328      0.0000275      0.0000224


*****   symmetry block SYM4   *****

 occupation numbers of nos
   1.8845092      0.0542992      0.0060270      0.0043436      0.0031750
   0.0021586      0.0009323      0.0006368      0.0005636      0.0004487
   0.0003311      0.0002607      0.0002110      0.0001720      0.0001143
   0.0000817      0.0000646      0.0000314      0.0000251      0.0000213


 total number of electrons =   30.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    13_ s       0.696346   0.014298   1.984628   0.630311   0.005739   0.004628
    13_ p       0.124755  -0.000284  -0.000033   0.106959   0.994890   0.231203
    13_ d       0.006978  -0.000223  -0.000018   0.004766   0.006039   0.011348
    14_ s       0.303953   1.985439   0.014422   1.074213   0.009538   0.005760
    14_ p       0.364436   0.000181   0.000030   0.034994   0.090296   0.840879
    14_ d       0.013876  -0.000117  -0.000604   0.002097   0.006862   0.029600
    14_ s       0.162137  -0.000025   0.000374   0.036988   0.485578   0.138016
    14_ p       0.003485  -0.000010   0.000343   0.000015   0.005407   0.000350
    14_ s       0.246121  -0.000054   0.000446   0.031111   0.338454  -0.003465
    14_ p       0.005256  -0.000015   0.000376   0.000540   0.003998   0.000428
    14_ s       0.069210   0.000437   0.000023   0.074918   0.027193   0.710549
    14_ p       0.003447   0.000372   0.000015   0.003087   0.001547   0.003521

   ao class       7Ag        8Ag        9Ag       10Ag       11Ag       12Ag 
    13_ s       0.006404   0.000095   0.000165   0.000663   0.002130  -0.000007
    13_ p       0.567560   0.002570   0.005746   0.001788   0.001045   0.001077
    13_ d       0.020311   0.000436   0.000407   0.000640   0.001016   0.001558
    14_ s       0.003328   0.002117   0.000036   0.000111   0.000461   0.000039
    14_ p       1.071074   0.002466   0.000355   0.005544   0.000568   0.000881
    14_ d       0.014598   0.001131   0.000484   0.000046   0.000037   0.001874
    14_ s       0.015808   0.000075   0.003960   0.000014   0.001153   0.000298
    14_ p       0.003005   0.000159  -0.000439   0.000113   0.000005   0.000076
    14_ s       0.231846   0.000909   0.000840   0.000004   0.002568   0.000167
    14_ p       0.003213  -0.000107  -0.000073   0.000099  -0.000089   0.000126
    14_ s       0.028878   0.002745   0.000109   0.001356   0.000458   0.000097
    14_ p       0.005693  -0.000035   0.000056  -0.000054  -0.000044   0.000264

   ao class      13Ag       14Ag       15Ag       16Ag       17Ag       18Ag 
    13_ s       0.000259   0.000363   0.000044   0.000058   0.000341   0.000089
    13_ p       0.000106   0.000711   0.000049   0.000176   0.000045   0.000113
    13_ d       0.000931   0.002135   0.000534   0.000324   0.000246   0.000186
    14_ s       0.001402   0.000096   0.000008   0.000004   0.000005   0.000071
    14_ p      -0.000081  -0.000091   0.000273   0.000384   0.000032   0.000007
    14_ d       0.001827   0.000131   0.001045   0.000338   0.000542   0.000399
    14_ s       0.000088   0.000129   0.000175   0.000212   0.000080  -0.000004
    14_ p       0.000039   0.000248   0.000020   0.000030   0.000042   0.000034
    14_ s       0.000131   0.000470   0.000016   0.000036   0.000050   0.000134
    14_ p      -0.000006   0.000168   0.000006   0.000021   0.000088   0.000022
    14_ s       0.000423   0.000003   0.000145   0.000439   0.000009   0.000052
    14_ p       0.000011   0.000020   0.000218   0.000079   0.000096   0.000008

   ao class      19Ag       20Ag       21Ag       22Ag       23Ag       24Ag 
    13_ s       0.000033  -0.000027   0.000045   0.000045   0.000013   0.000014
    13_ p       0.000067   0.000121   0.000027   0.000022   0.000097   0.000163
    13_ d       0.000212   0.000195   0.000090   0.000080   0.000094   0.000089
    14_ s       0.000094   0.000151   0.000247   0.000015  -0.000001   0.000012
    14_ p       0.000143   0.000186   0.000048   0.000186   0.000070   0.000032
    14_ d      -0.000002   0.000037   0.000110   0.000081   0.000054   0.000022
    14_ s       0.000016   0.000109   0.000046   0.000043   0.000011   0.000002
    14_ p       0.000135   0.000055   0.000014   0.000026   0.000043   0.000023
    14_ s       0.000074   0.000018   0.000056   0.000002   0.000007   0.000021
    14_ p       0.000171   0.000037   0.000018  -0.000004   0.000049   0.000029
    14_ s       0.000025   0.000034   0.000006   0.000031   0.000010   0.000002
    14_ p       0.000008  -0.000008   0.000004   0.000031   0.000004   0.000032

   ao class      25Ag       26Ag       27Ag       28Ag       29Ag       30Ag 
    13_ s       0.000051   0.000012   0.000007   0.000005   0.000006   0.000002
    13_ p       0.000042   0.000017   0.000049   0.000055   0.000073   0.000016
    13_ d       0.000018   0.000058   0.000042   0.000055   0.000055   0.000028
    14_ s       0.000020  -0.000003  -0.000007  -0.000004   0.000005  -0.000001
    14_ p       0.000158   0.000028   0.000054   0.000004  -0.000001   0.000048
    14_ d       0.000046   0.000132   0.000074   0.000071   0.000046   0.000037
    14_ s       0.000012   0.000001   0.000013   0.000002   0.000003  -0.000003
    14_ p       0.000019   0.000016   0.000012  -0.000003   0.000015   0.000007
    14_ s       0.000001   0.000002  -0.000002   0.000003   0.000013  -0.000001
    14_ p       0.000009   0.000014  -0.000003   0.000013  -0.000001   0.000015
    14_ s       0.000007   0.000005   0.000029   0.000002   0.000010  -0.000002
    14_ p       0.000035   0.000045   0.000008   0.000049   0.000007   0.000009

   ao class      31Ag       32Ag       33Ag       34Ag       35Ag       36Ag 
    13_ s       0.000013   0.000013   0.000001   0.000004  -0.000001   0.000005
    13_ p       0.000008   0.000001   0.000013  -0.000003   0.000000   0.000011
    13_ d       0.000012   0.000024   0.000018   0.000014   0.000020   0.000003
    14_ s       0.000048   0.000006   0.000001   0.000004   0.000025   0.000003
    14_ p       0.000013   0.000040   0.000026   0.000001  -0.000001   0.000010
    14_ d       0.000008   0.000013   0.000010   0.000032   0.000012   0.000007
    14_ s       0.000001   0.000005   0.000010   0.000008  -0.000001   0.000001
    14_ p       0.000014  -0.000001   0.000008   0.000015   0.000004   0.000003
    14_ s      -0.000001   0.000007   0.000002   0.000000   0.000009   0.000013
    14_ p       0.000012   0.000001   0.000006   0.000005   0.000006   0.000019
    14_ s      -0.000001   0.000006   0.000008   0.000000   0.000004   0.000000
    14_ p       0.000011   0.000001   0.000004   0.000014   0.000008   0.000004

   ao class      37Ag       38Ag       39Ag       40Ag       41Ag       42Ag 
    13_ s       0.000000   0.000003   0.000004   0.000010   0.000000   0.000000
    13_ p       0.000004   0.000007   0.000008   0.000002   0.000007   0.000000
    13_ d      -0.000004   0.000002   0.000001   0.000000   0.000011   0.000000
    14_ s      -0.000002   0.000002   0.000002   0.000003   0.000002   0.000001
    14_ p       0.000016   0.000007   0.000003   0.000000   0.000001   0.000001
    14_ d       0.000002   0.000010   0.000010   0.000002  -0.000001  -0.000006
    14_ s       0.000016   0.000000   0.000003   0.000001   0.000000   0.000009
    14_ p       0.000003   0.000013   0.000000   0.000023   0.000011   0.000000
    14_ s       0.000024   0.000008  -0.000004   0.000002   0.000006   0.000000
    14_ p       0.000006   0.000006   0.000022   0.000006   0.000001   0.000000
    14_ s       0.000001   0.000002   0.000002  -0.000001   0.000002   0.000033
    14_ p       0.000002   0.000004   0.000008   0.000006   0.000008   0.000008

   ao class      43Ag       44Ag       45Ag       46Ag       47Ag       48Ag 
    13_ s       0.000000   0.000000   0.000001   0.000002   0.000001   0.000000
    13_ p       0.000003   0.000005   0.000001   0.000001   0.000000   0.000001
    13_ d       0.000005   0.000004   0.000004   0.000004   0.000001   0.000006
    14_ s       0.000000   0.000000   0.000000   0.000002   0.000000   0.000000
    14_ p       0.000005   0.000004   0.000000   0.000000  -0.000001   0.000002
    14_ d       0.000002   0.000004   0.000006   0.000005   0.000002   0.000001
    14_ s       0.000004  -0.000001   0.000000   0.000001  -0.000001   0.000001
    14_ p       0.000009   0.000004   0.000009   0.000000   0.000006   0.000001
    14_ s       0.000002  -0.000001   0.000001   0.000003   0.000001   0.000001
    14_ p       0.000008   0.000008   0.000003   0.000004   0.000005   0.000001
    14_ s       0.000001   0.000000   0.000000   0.000001   0.000000   0.000003
    14_ p       0.000004   0.000009   0.000004   0.000001   0.000006   0.000002

   ao class      49Ag       50Ag       51Ag       52Ag       53Ag 
    13_ s       0.000001   0.000000   0.000002   0.000000   0.000000
    13_ p       0.000000   0.000001   0.000003   0.000000   0.000000
    13_ d       0.000000   0.000000   0.000000   0.000000   0.000000
    14_ s       0.000000   0.000001  -0.000001   0.000000   0.000000
    14_ p       0.000000   0.000000   0.000000   0.000000   0.000000
    14_ d       0.000002   0.000000   0.000000   0.000000   0.000000
    14_ s       0.000001   0.000000   0.000000   0.000001   0.000001
    14_ p       0.000001   0.000001   0.000003   0.000001   0.000001
    14_ s       0.000001   0.000000  -0.000001   0.000002   0.000000
    14_ p       0.000004   0.000001   0.000002   0.000003   0.000001
    14_ s       0.000000   0.000000   0.000001   0.000000   0.000000
    14_ p       0.000005   0.000006   0.000000   0.000000   0.000000

                        Bu  partial gross atomic populations
   ao class       1Bu        2Bu        3Bu        4Bu        5Bu        6Bu 
    13_ s       0.391210   1.608554   1.178555   0.074654   0.001842   0.003224
    13_ p       0.000005  -0.000412   0.001849   0.257202   1.011782   0.691416
    13_ d      -0.000058  -0.000179   0.004395   0.007177   0.010392   0.017623
    14_ s       1.608032   0.391386   0.172515   0.476193   0.124703   0.008007
    14_ p      -0.000052  -0.000662   0.233986   0.453620   0.144163   0.597780
    14_ d      -0.000254  -0.000200   0.001398   0.001657   0.017740   0.008903
    14_ s       0.000012   0.000212   0.137598   0.192791   0.089395   0.396408
    14_ p       0.000073   0.000319   0.003144   0.003239   0.004883   0.003997
    14_ s       0.000049   0.000155   0.220509   0.005086   0.565631   0.013098
    14_ p       0.000056   0.000326   0.003728   0.001137   0.004810   0.002830
    14_ s       0.000164   0.000010   0.026053   0.497980   0.000216   0.227469
    14_ p       0.000327   0.000052   0.001409   0.007664  -0.000086   0.002182

   ao class       7Bu        8Bu        9Bu       10Bu       11Bu       12Bu 
    13_ s       0.000003   0.000902   0.000059   0.000078   0.002101  -0.000001
    13_ p       0.002384   0.003992   0.002101   0.003354   0.000231   0.000517
    13_ d       0.001147   0.000261   0.000446   0.000259   0.000783   0.001556
    14_ s       0.000191   0.000202   0.002253   0.000329   0.001841   0.000238
    14_ p       0.004565   0.005297   0.000925   0.002413   0.000178   0.001479
    14_ d       0.000938   0.000663   0.000742   0.000504   0.000336   0.001958
    14_ s       0.001950   0.000111   0.001401   0.000527   0.001331   0.000051
    14_ p      -0.000226   0.000122  -0.000104  -0.000018  -0.000023   0.000250
    14_ s       0.000659   0.000490   0.000180   0.001821   0.001475   0.000114
    14_ p      -0.000030  -0.000028   0.000043  -0.000154  -0.000053   0.000128
    14_ s       0.002998   0.000142   0.002653   0.000132   0.000510   0.000160
    14_ p      -0.000355   0.000234  -0.000175   0.000070   0.000017   0.000218

   ao class      13Bu       14Bu       15Bu       16Bu       17Bu       18Bu 
    13_ s       0.000086   0.000680   0.000023   0.000048   0.000282   0.000057
    13_ p       0.000933   0.000262   0.000144   0.000053   0.000028   0.000020
    13_ d       0.001346   0.001695   0.000616   0.000377   0.000085   0.000133
    14_ s       0.001200   0.000220   0.000058  -0.000011   0.000100  -0.000007
    14_ p      -0.000490   0.000072   0.000315   0.000176  -0.000051   0.000262
    14_ d       0.001112   0.000176   0.000592   0.000558   0.000333   0.000221
    14_ s       0.000110   0.000467   0.000011   0.000071   0.000129  -0.000007
    14_ p       0.000035   0.000070   0.000113   0.000000   0.000101   0.000025
    14_ s       0.000058   0.000260   0.000205   0.000013   0.000086   0.000086
    14_ p       0.000142   0.000057   0.000022   0.000024   0.000036   0.000018
    14_ s       0.000562   0.000008   0.000170   0.000105   0.000011   0.000163
    14_ p       0.000062   0.000003   0.000034   0.000286   0.000006  -0.000006

   ao class      19Bu       20Bu       21Bu       22Bu       23Bu       24Bu 
    13_ s       0.000001   0.000101   0.000007   0.000034   0.000000   0.000082
    13_ p       0.000233  -0.000002   0.000103   0.000018   0.000097   0.000082
    13_ d       0.000181   0.000273   0.000122   0.000038   0.000017   0.000084
    14_ s       0.000002   0.000101   0.000109   0.000051   0.000044   0.000001
    14_ p      -0.000016   0.000021   0.000011   0.000009   0.000139   0.000016
    14_ d      -0.000012   0.000068   0.000138   0.000183   0.000006   0.000021
    14_ s       0.000119   0.000037   0.000014   0.000008   0.000009   0.000017
    14_ p       0.000166   0.000011   0.000038   0.000002   0.000005   0.000076
    14_ s       0.000092   0.000021   0.000001   0.000038   0.000013   0.000017
    14_ p       0.000155   0.000009   0.000063   0.000036   0.000032   0.000036
    14_ s       0.000001  -0.000001   0.000001   0.000073   0.000038  -0.000002
    14_ p       0.000003   0.000085   0.000056   0.000056   0.000067   0.000004

   ao class      25Bu       26Bu       27Bu       28Bu       29Bu       30Bu 
    13_ s       0.000002   0.000006   0.000000   0.000038   0.000007   0.000000
    13_ p       0.000149   0.000103   0.000064   0.000001   0.000015   0.000012
    13_ d       0.000040   0.000069   0.000068   0.000056   0.000014   0.000050
    14_ s      -0.000001  -0.000001   0.000000   0.000025   0.000011   0.000009
    14_ p       0.000090   0.000029   0.000012   0.000000   0.000043   0.000020
    14_ d       0.000053   0.000037   0.000052   0.000035   0.000032   0.000043
    14_ s       0.000003  -0.000001   0.000009   0.000005   0.000000  -0.000005
    14_ p       0.000009   0.000031  -0.000007   0.000008   0.000027   0.000001
    14_ s       0.000002   0.000008  -0.000001   0.000002   0.000000  -0.000002
    14_ p      -0.000006  -0.000006   0.000021   0.000006   0.000018   0.000008
    14_ s       0.000004  -0.000001   0.000004   0.000004  -0.000003  -0.000001
    14_ p       0.000047   0.000003   0.000012   0.000003   0.000007   0.000001

   ao class      31Bu       32Bu       33Bu       34Bu       35Bu       36Bu 
    13_ s       0.000000   0.000007   0.000002   0.000000   0.000002   0.000000
    13_ p       0.000001   0.000016   0.000004  -0.000005   0.000004   0.000018
    13_ d       0.000004   0.000004   0.000034   0.000001   0.000001   0.000009
    14_ s      -0.000001   0.000016   0.000014   0.000011   0.000008   0.000000
    14_ p       0.000067   0.000012  -0.000003   0.000025   0.000006   0.000013
    14_ d       0.000042   0.000007   0.000002   0.000011   0.000006   0.000004
    14_ s      -0.000001   0.000010  -0.000002   0.000006   0.000014   0.000001
    14_ p       0.000003   0.000004   0.000009   0.000011   0.000015   0.000005
    14_ s       0.000002   0.000003   0.000004   0.000004   0.000012   0.000004
    14_ p       0.000003   0.000003   0.000011   0.000007   0.000000   0.000013
    14_ s      -0.000003   0.000017   0.000006   0.000003   0.000003   0.000000
    14_ p       0.000004   0.000011   0.000005   0.000006   0.000002   0.000002

   ao class      37Bu       38Bu       39Bu       40Bu       41Bu       42Bu 
    13_ s       0.000000   0.000010   0.000004   0.000000   0.000006   0.000000
    13_ p      -0.000003   0.000003   0.000003   0.000009   0.000000   0.000000
    13_ d       0.000004   0.000004   0.000002   0.000002   0.000001   0.000003
    14_ s       0.000000  -0.000001   0.000000   0.000009   0.000000   0.000000
    14_ p       0.000011   0.000005   0.000009   0.000000   0.000003   0.000003
    14_ d       0.000000   0.000017   0.000005   0.000000   0.000009   0.000000
    14_ s       0.000000  -0.000002   0.000018   0.000000   0.000003   0.000005
    14_ p       0.000008   0.000012  -0.000001   0.000004   0.000006   0.000014
    14_ s       0.000006   0.000002  -0.000002   0.000011   0.000009   0.000000
    14_ p       0.000004   0.000003   0.000014  -0.000002   0.000010   0.000008
    14_ s       0.000009   0.000003   0.000006  -0.000003   0.000001   0.000006
    14_ p       0.000023   0.000007   0.000001   0.000027   0.000003   0.000006

   ao class      43Bu       44Bu       45Bu       46Bu       47Bu       48Bu 
    13_ s       0.000001   0.000001   0.000000   0.000000   0.000001   0.000000
    13_ p       0.000003  -0.000003   0.000001   0.000001   0.000000   0.000000
    13_ d       0.000000   0.000010   0.000002   0.000000   0.000003   0.000003
    14_ s      -0.000001   0.000002   0.000000   0.000000   0.000001   0.000001
    14_ p       0.000006   0.000004   0.000000   0.000000  -0.000001  -0.000001
    14_ d       0.000008   0.000001   0.000007   0.000001   0.000004   0.000002
    14_ s       0.000003   0.000001   0.000001   0.000000   0.000003   0.000000
    14_ p       0.000006   0.000001   0.000003   0.000010   0.000000   0.000003
    14_ s       0.000000   0.000003   0.000000   0.000000   0.000003   0.000000
    14_ p       0.000003   0.000012   0.000006   0.000006   0.000001   0.000004
    14_ s       0.000000  -0.000001   0.000000   0.000000   0.000003   0.000002
    14_ p       0.000007   0.000002   0.000006   0.000004   0.000000   0.000002

   ao class      49Bu       50Bu       51Bu       52Bu       53Bu 
    13_ s      -0.000001   0.000000   0.000001   0.000000   0.000001
    13_ p      -0.000001   0.000001   0.000002   0.000001   0.000000
    13_ d       0.000000   0.000000   0.000000   0.000000   0.000000
    14_ s      -0.000001   0.000000   0.000000   0.000000   0.000001
    14_ p       0.000009   0.000000   0.000000   0.000000   0.000000
    14_ d       0.000000   0.000000   0.000000   0.000000   0.000000
    14_ s      -0.000001   0.000001   0.000000   0.000002   0.000000
    14_ p       0.000001   0.000001   0.000003   0.000001   0.000000
    14_ s       0.000000   0.000000   0.000002   0.000001   0.000001
    14_ p       0.000001   0.000002   0.000001   0.000000   0.000001
    14_ s       0.000000   0.000003   0.000000   0.000000   0.000000
    14_ p       0.000005   0.000002   0.000000   0.000000   0.000001

                        Au  partial gross atomic populations
   ao class       1Au        2Au        3Au        4Au        5Au        6Au 
    13_ p       0.665644   0.064712   0.001108   0.000419   0.000000   0.000249
    13_ d       0.017542   0.000037   0.001520   0.001753   0.001424   0.000793
    14_ p       1.210464   0.034081   0.001375   0.000392   0.000218   0.000824
    14_ d       0.009442   0.001455   0.001836   0.002158   0.001029   0.000653
    14_ p       0.006995   0.000558   0.000287   0.000048   0.000137   0.000115
    14_ p       0.005463   0.000641   0.000018   0.000203   0.000291   0.000000
    14_ p       0.013222   0.000361   0.000017   0.000377   0.000194   0.000102

   ao class       7Au        8Au        9Au       10Au       11Au       12Au 
    13_ p       0.000894   0.000121   0.000040   0.000017   0.000001   0.000044
    13_ d       0.000081   0.000189   0.000012   0.000090   0.000051   0.000061
    14_ p       0.000332   0.000124   0.000006   0.000009   0.000013   0.000116
    14_ d       0.000398   0.000152   0.000056   0.000023   0.000091   0.000036
    14_ p       0.000142   0.000025   0.000005   0.000301   0.000003   0.000004
    14_ p       0.000037   0.000005   0.000270   0.000001   0.000087   0.000000
    14_ p       0.000107   0.000078   0.000130   0.000010   0.000087   0.000002

   ao class      13Au       14Au       15Au       16Au       17Au       18Au 
    13_ p       0.000032   0.000024   0.000050   0.000000   0.000000   0.000000
    13_ d       0.000106   0.000062   0.000034   0.000033   0.000008   0.000001
    14_ p       0.000001   0.000068   0.000006  -0.000001   0.000004   0.000000
    14_ d       0.000068   0.000015   0.000014   0.000045   0.000031   0.000000
    14_ p       0.000004   0.000000   0.000001   0.000002   0.000006   0.000010
    14_ p       0.000000  -0.000001   0.000003   0.000004   0.000001   0.000012
    14_ p       0.000001   0.000001   0.000001   0.000002   0.000016   0.000010

   ao class      19Au       20Au 
    13_ p       0.000000   0.000000
    13_ d       0.000001   0.000001
    14_ p       0.000000   0.000000
    14_ d       0.000002   0.000002
    14_ p       0.000016   0.000000
    14_ p       0.000006   0.000009
    14_ p       0.000002   0.000010

                        Bg  partial gross atomic populations
   ao class       1Bg        2Bg        3Bg        4Bg        5Bg        6Bg 
    13_ p       1.210289   0.017711   0.001123   0.000932   0.001121   0.000009
    13_ d       0.009496   0.000733   0.001707   0.002487   0.000509   0.000793
    14_ p       0.588886   0.034501   0.000737   0.000054   0.000131   0.000631
    14_ d       0.043829   0.000458   0.001878   0.000282   0.001062   0.000363
    14_ p       0.011996   0.000225   0.000008   0.000538   0.000059   0.000004
    14_ p       0.013694   0.000173   0.000278   0.000053   0.000086   0.000187
    14_ p       0.006319   0.000498   0.000296  -0.000003   0.000207   0.000171

   ao class       7Bg        8Bg        9Bg       10Bg       11Bg       12Bg 
    13_ p       0.000044   0.000071   0.000042   0.000044   0.000002   0.000039
    13_ d       0.000013   0.000101   0.000061   0.000060   0.000123   0.000068
    14_ p       0.000170   0.000097   0.000097   0.000017   0.000003   0.000046
    14_ d       0.000526   0.000194   0.000036   0.000046   0.000039   0.000101
    14_ p       0.000169   0.000006   0.000018   0.000169   0.000051   0.000001
    14_ p      -0.000001   0.000058   0.000099   0.000099   0.000066   0.000001
    14_ p       0.000012   0.000109   0.000212   0.000014   0.000048   0.000004

   ao class      13Bg       14Bg       15Bg       16Bg       17Bg       18Bg 
    13_ p       0.000127   0.000004   0.000002   0.000009   0.000001   0.000000
    13_ d       0.000008   0.000055   0.000046   0.000041   0.000010   0.000000
    14_ p       0.000003   0.000047   0.000035   0.000013   0.000004   0.000000
    14_ d       0.000052   0.000058   0.000028   0.000012   0.000032   0.000000
    14_ p       0.000005   0.000001   0.000001   0.000002   0.000012   0.000000
    14_ p       0.000015   0.000001   0.000002   0.000001   0.000005   0.000014
    14_ p       0.000000   0.000006   0.000001   0.000004   0.000001   0.000017

   ao class      19Bg       20Bg 
    13_ p       0.000000   0.000000
    13_ d       0.000001   0.000002
    14_ p       0.000000   0.000000
    14_ d       0.000001   0.000001
    14_ p       0.000020   0.000001
    14_ p       0.000001   0.000010
    14_ p       0.000001   0.000008


                        gross atomic populations
     ao           13_        14_        14_        14_        14_
      s         6.609488   6.189485   1.668222   1.660280   1.676941
      p         5.981017   5.731332   0.051846   0.050002   0.053708
      d         0.148024   0.179655   0.000000   0.000000   0.000000
    total      12.738529  12.100472   1.720068   1.710281   1.730650


 Total number of electrons:   30.00000000

 !timer: cumulative execution time       user+sys=   410.750 walltime=   413.000
========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.6     492.0      36.0    7361.8     409.5       0.2       0.0     412.2
   #   2         0.6     492.0      36.0    7361.8     409.5       0.2       0.0     412.2
--------------------------------------------------------------------------------
       2         1.1     984.0      72.0   14723.6     819.0       0.4       0.0     412.2
