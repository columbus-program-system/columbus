1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   630)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0180228620 -5.5067E-14  4.7766E-01  1.3806E+00  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -155.0180228620 -5.5067E-14  4.7766E-01  1.3806E+00  1.0000E-03

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4047614913  3.8674E-01  2.0341E-02  2.6469E-01  1.0000E-03
 mr-sdci #  2  1   -155.4195807093  1.4819E-02  1.6847E-03  8.1846E-02  1.0000E-03
 mr-sdci #  3  1   -155.4209290326  1.3483E-03  2.0729E-04  2.5399E-02  1.0000E-03
 mr-sdci #  4  1   -155.4211036776  1.7464E-04  4.9481E-05  1.1388E-02  1.0000E-03
 mr-sdci #  5  1   -155.4211537376  5.0060E-05  1.5654E-05  7.3159E-03  1.0000E-03
 mr-sdci #  6  1   -155.4211663869  1.2649E-05  5.1860E-06  3.7141E-03  1.0000E-03
 mr-sdci #  7  1   -155.4211727701  6.3832E-06  2.0440E-06  2.3944E-03  1.0000E-03
 mr-sdci #  8  1   -155.4211747444  1.9743E-06  4.8284E-07  1.3280E-03  1.0000E-03
 mr-sdci #  9  1   -155.4211752062  4.6182E-07  7.1775E-08  4.5952E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1   -155.4211752062  4.6182E-07  7.1775E-08  4.5952E-04  1.0000E-03

 number of reference csfs (nref) is   273.  root number (iroot) is  1.

 eref      =   -155.016125613887   "relaxed" cnot**2         =   0.885673512603
 eci       =   -155.421175206181   deltae = eci - eref       =  -0.405049592294
 eci+dv1   =   -155.467483103290   dv1 = (1-cnot**2)*deltae  =  -0.046307897109
 eci+dv2   =   -155.473460722740   dv2 = dv1 / cnot**2       =  -0.052285516558
 eci+dv3   =   -155.481210307887   dv3 = dv1 / (2*cnot**2-1) =  -0.060035101706
 eci+pople =   -155.474229212477   ( 22e- scaled deltae )    =  -0.458103598589
