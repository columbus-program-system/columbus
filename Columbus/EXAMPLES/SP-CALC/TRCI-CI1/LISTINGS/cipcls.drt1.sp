
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=   8000000 mem1=1108557832 ifirst=-264082974

 drt header information:
 /                                                                               
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       56        5       16       15       20
 ncsft:             358
 map(*)=    -1  9 10  1  2  3  4 11  5 12  6  7  8
 mu(*)=      0  0  0  0
 syml(*) =   1  1  2  3
 rmo(*)=     2  3  1  1

 indx01:    56 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003
 energy computed by program ciudg.       hochtor2        Thu Jun  5 14:27:14 2003

 lenrec =    4096 lenci =       358 ninfo =  6 nenrgy =  5 ntitle =  7

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       98% overlap
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -3.871965490428E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  5.098708288298E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  3.824496275229E-12, ietype=-2056, cnvginf energy of type: CI-D.E. 
==================================================================================
===================================ROOT # 2===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003
 energy computed by program ciudg.       hochtor2        Thu Jun  5 14:27:14 2003

 lenrec =    4096 lenci =       358 ninfo =  6 nenrgy =  6 ntitle =  7

 Max. overlap with ref vector #        2
 Valid ci vector #        2
 Method:        0       97% overlap
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -3.849489684555E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  9.513144450291E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  2.282317090874E-08, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  4.850208388428E-09, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   3997894 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      53 coefficients were selected.
 workspace: ncsfmx=     358
 ncsfmx= 358

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      53
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       2 **
 3.1250E-02 <= |c| < 6.2500E-02       8 ********
 1.5625E-02 <= |c| < 3.1250E-02      20 ********************
 7.8125E-03 <= |c| < 1.5625E-02      21 *********************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       53 total stored =      53

 from the selected csfs,
 min(|csfvec(:)|) = 1.0235E-02    max(|csfvec(:)|) = 9.6377E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      53

 i:slabel(i) =  1:   1  2:   2  3:   3  4:   4

 frozen orbital =    1
 symfc(*)       =    1
 label          =    1
 rmo(*)         =    1

 internal level =    1    2    3    4
 syml(*)        =    1    1    2    3
 label          =    1    1    2    3
 rmo(*)         =    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        2 -0.96377 0.92886 z*                    3303
        3  0.15776 0.02489 z*                    3033
        4  0.08331 0.00694 z*                    1233
      158  0.07831 0.00613 w             3:  2  33300
        5  0.05876 0.00345 z*                    0333
      168 -0.05876 0.00345 w    1:  4    3:  2 123102
      221  0.05732 0.00329 w    1:  4    3:  2 121302
      211  0.04483 0.00201 w             3:  2  33003
      202  0.04314 0.00186 w             1:  5  33003
      309  0.03585 0.00129 w             1:  4  30303
      253 -0.03163 0.00100 w             1:  4  31203
      147  0.03133 0.00098 w             1:  4  33300
      169  0.02925 0.00086 w    1:  5    3:  2 123102
       55 -0.02704 0.00073 x    1:  5    3:  2 113202
      160  0.02554 0.00065 w             3:  3  33300
      227 -0.02451 0.00060 w    1:  6    3:  3 121302
      200  0.02418 0.00058 w             1:  4  33003
        9 -0.02411 0.00058 y             3:  4  13302
       97 -0.02410 0.00058 x    1:  4    1:  5 112203
       54  0.02352 0.00055 x    1:  4    3:  2 113202
       21 -0.02104 0.00044 y             2:  2  13023
       77 -0.02097 0.00044 x    1:  4    3:  3 112302
       59 -0.02094 0.00044 x    1:  5    3:  3 113202
      173  0.02091 0.00044 w    1:  5    3:  3 123102
       41 -0.02014 0.00041 y             2:  2  11223
      320  0.01913 0.00037 w             3:  2  30303
       17  0.01909 0.00036 y             1:  7  13203
      255  0.01875 0.00035 w             1:  5  31203
       15  0.01843 0.00034 y             1:  5  13203
      201 -0.01820 0.00033 w    1:  4    1:  5 123003
        8  0.01799 0.00032 y             3:  3  13302
       45 -0.01687 0.00028 y             2:  2  10323
       73 -0.01369 0.00019 x    1:  4    3:  2 112302
      191 -0.01354 0.00018 w             3:  2  33030
       16 -0.01349 0.00018 y             1:  6  13203
      152  0.01340 0.00018 w             1:  6  33300
      254  0.01318 0.00017 w    1:  4    1:  5 121203
      265  0.01308 0.00017 w    3:  2    3:  3 121203
      273 -0.01304 0.00017 w    1:  4    3:  2 121032
      212 -0.01292 0.00017 w    3:  2    3:  3 123003
      174  0.01247 0.00016 w    1:  6    3:  3 123102
       14 -0.01241 0.00015 y             1:  4  13203
      222  0.01177 0.00014 w    1:  5    3:  2 121302
      223  0.01160 0.00013 w    1:  6    3:  2 121302
      225  0.01140 0.00013 w    1:  4    3:  3 121302
      214  0.01136 0.00013 w    3:  2    3:  4 123003
       26  0.01123 0.00013 y             1:  4  12303
      178  0.01105 0.00012 w    1:  6    3:  4 123102
      314  0.01095 0.00012 w             1:  6  30303
      258 -0.01069 0.00011 w             1:  6  31203
      231 -0.01062 0.00011 w    1:  6    3:  4 121302
      342 -0.01030 0.00011 w             1:  4  30033
        1  0.01024 0.00010 z*                    3330
           53 csfs were printed in this range.

================================================================================
===================================VECTOR # 2===================================
================================================================================


 rdcivnew:      51 coefficients were selected.
 workspace: ncsfmx=     358
 ncsfmx= 358

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      51
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       2 **
 6.2500E-02 <= |c| < 1.2500E-01       3 ***
 3.1250E-02 <= |c| < 6.2500E-02      11 ***********
 1.5625E-02 <= |c| < 3.1250E-02      17 *****************
 7.8125E-03 <= |c| < 1.5625E-02      17 *****************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       51 total stored =      51

 from the selected csfs,
 min(|csfvec(:)|) = 1.0125E-02    max(|csfvec(:)|) = 9.5073E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      51

 i:slabel(i) =  1:   1  2:   2  3:   3  4:   4

 frozen orbital =    1
 symfc(*)       =    1
 label          =    1
 rmo(*)         =    1

 internal level =    1    2    3    4
 syml(*)        =    1    1    2    3
 label          =    1    1    2    3
 rmo(*)         =    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        3  0.95073 0.90389 z*                    3033
        2  0.14850 0.02205 z*                    3303
        1 -0.13976 0.01953 z*                    3330
       38 -0.10507 0.01104 y             3:  2  11232
      191 -0.07006 0.00491 w             3:  2  33030
        5 -0.06664 0.00444 z*                    0333
       21 -0.05577 0.00311 y             2:  2  13023
      273 -0.05434 0.00295 w    1:  4    3:  2 121032
      210 -0.05415 0.00293 w             2:  2  33003
       10  0.05022 0.00252 y             1:  4  13230
       14  0.04022 0.00162 y             1:  4  13203
      285 -0.03923 0.00154 w    1:  4    2:  2 121023
      197 -0.03467 0.00120 w    2:  2    3:  2 123012
      342 -0.03433 0.00118 w             1:  4  30033
       66  0.03230 0.00104 x    2:  2    3:  2 113022
       39  0.03228 0.00104 y             3:  3  11232
       18 -0.03183 0.00101 y             3:  2  13032
       67  0.02765 0.00076 x    2:  2    3:  3 113022
       34  0.02699 0.00073 y             1:  4  12033
      121  0.02633 0.00069 x    1:  4    2:  2 112023
      353 -0.02573 0.00066 w             3:  2  30033
      198 -0.02536 0.00064 w    2:  2    3:  3 123012
      193 -0.02437 0.00059 w             3:  3  33030
      200 -0.02350 0.00055 w             1:  4  33003
      279  0.02316 0.00054 w    1:  6    3:  3 121032
      180 -0.02223 0.00049 w             1:  4  33030
       17  0.01977 0.00039 y             1:  7  13203
        4 -0.01976 0.00039 z*                    1233
      113  0.01918 0.00037 x    1:  4    3:  3 112032
      286 -0.01864 0.00035 w    1:  5    2:  2 121023
      202 -0.01803 0.00033 w             1:  5  33003
       30  0.01767 0.00031 y             3:  2  12132
       12  0.01715 0.00029 y             1:  6  13230
       48 -0.01590 0.00025 y             1:  6  10233
      109  0.01548 0.00024 x    1:  4    3:  2 112032
      211 -0.01423 0.00020 w             3:  2  33003
      277 -0.01390 0.00019 w    1:  4    3:  3 121032
      300  0.01356 0.00018 w             3:  2  30330
       41 -0.01333 0.00018 y             2:  2  11223
      287  0.01324 0.00018 w    1:  6    2:  2 121023
      274 -0.01316 0.00017 w    1:  5    3:  2 121032
       36 -0.01297 0.00017 y             1:  6  12033
       15  0.01183 0.00014 y             1:  5  13203
      185 -0.01163 0.00014 w             1:  6  33030
      158 -0.01158 0.00013 w             3:  2  33300
      123 -0.01128 0.00013 x    1:  6    2:  2 112023
       33  0.01109 0.00012 y             2:  2  12123
      221 -0.01086 0.00012 w    1:  4    3:  2 121302
      347 -0.01050 0.00011 w             1:  6  30033
      275 -0.01031 0.00011 w    1:  6    3:  2 121032
        7  0.01013 0.00010 y             3:  2  13302
           51 csfs were printed in this range.
