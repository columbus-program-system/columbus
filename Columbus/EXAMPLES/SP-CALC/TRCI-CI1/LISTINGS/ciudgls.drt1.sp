1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 358
global arrays:        11814   (    0.09 MB)
vdisk:                    0   (    0.00 MB)
drt:                 278425   (    1.06 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NROOT = 2
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 2
  NVBKMX = 7
  RTOLBK = 1e-4,1e-4,
  NITER = 90
  NVCIMN = 4
  NVCIMX = 16
  RTOLCI = 1e-4,1e-4,
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    7      nvrfmn =    4
 lvlprt =    0      nroot  =    2      noldv  =    0      noldhv =    0
 nunitv =    2      ntype  =    0      nbkitr =    1      niter  =   90
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    7      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    7      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082506

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003

 core energy values from the integral file:
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -2.792842564975E+01

 drt header information:
 /                                                                               
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       56        5       16       15       20
 ncsft:             358
 total number of valid internal walks:      56
 nvalz,nvaly,nvalx,nvalw =        5      16      15      20

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext    72    64    20
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      330      432      360      156       27        0        0        0
 minbl4,minbl3,maxbl2    48    48    51
 maxbuf 32767
 number of external orbitals per symmetry block:   4   1   3   0
 nmsym   4 number of internal orbitals   4

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     7    20    15    20 total internal walks:      62
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 3

 setref:        5 references kept,
                0 references were marked as invalid, out of
                5 total.
 limcnvrt: found 5 valid internal walksout of  7
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 1
 limcnvrt: found 16 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  16 segmentation marks segtype= 2
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 3
 limcnvrt: found 20 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  20 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x       9       4      12       3
 vertex w      17       4      12       3



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           5|         5|         0|         5|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          16|        44|         5|        16|         5|         2|
 -------------------------------------------------------------------------------
  X 3          15|        97|        49|        15|        21|         3|
 -------------------------------------------------------------------------------
  W 4          20|       212|       146|        20|        36|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>       358


 297 dimension of the ci-matrix ->>>       358


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       5         97          5      15       5
     2  4   1    25      two-ext wz   2X  4 1      20       5        212          5      20       5
     3  4   3    26      two-ext wx   2X  4 3      20      15        212         97      20      15
     4  2   1    11      one-ext yz   1X  2 1      16       5         44          5      16       5
     5  3   2    15      1ex3ex  yx   3X  3 2      15      16         97         44      15      16
     6  4   2    16      1ex3ex  yw   3X  4 2      20      16        212         44      20      16
     7  1   1     1      allint zz    OX  1 1       5       5          5          5       5       5
     8  2   2     5      0ex2ex yy    OX  2 2      16      16         44         44      16      16
     9  3   3     6      0ex2ex xx    OX  3 3      15      15         97         97      15      15
    10  4   4     7      0ex2ex ww    OX  4 4      20      20        212        212      20      20
    11  1   1    75      dg-024ext z  DG  1 1       5       5          5          5       5       5
    12  2   2    45      4exdg024 y   DG  2 2      16      16         44         44      16      16
    13  3   3    46      4exdg024 x   DG  3 3      15      15         97         97      15      15
    14  4   4    47      4exdg024 w   DG  4 4      20      20        212        212      20      20
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 358 156 51
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        34 2x:         0 4x:         0
All internal counts: zz :        14 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        13    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        29    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       5

    root           eigenvalues
    ----           ------------
       1         -38.6450051117
       2         -38.4074031068

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  2 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     5)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               358
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:         0 xx:         0 ww:         0
One-external counts: yz :        92 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:        18 wz:        32 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:         0    task #   4:        86
task #   5:         0    task #   6:         0    task #   7:        13    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        29    task #  12:        51
task #  13:        48    task #  14:        56    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:         0 xx:         0 ww:         0
One-external counts: yz :        92 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:        18 wz:        32 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:         0    task #   4:        86
task #   5:         0    task #   6:         0    task #   7:        13    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        29    task #  12:        51
task #  13:        48    task #  14:        56    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000
 ref:   2     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.6450051117 -7.1054E-15  8.8869E-02  4.2072E-01  1.0000E-04
 mr-sdci #  1  2    -38.4074031068 -1.4211E-14  0.0000E+00  4.3501E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0400s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.6450051117 -7.1054E-15  8.8869E-02  4.2072E-01  1.0000E-04
 mr-sdci #  1  2    -38.4074031068 -1.4211E-14  0.0000E+00  4.3501E-01  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               358
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                   90
 residual norm convergence criteria:               0.000100  0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.98512817     0.00486855    -0.17175210
 ref:   2    -0.00430408    -0.99998404    -0.00365878

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.7153953050  7.0390E-02  3.9119E-03  9.1455E-02  1.0000E-04
 mr-sdci #  1  2    -38.4074252190  2.2112E-05  0.0000E+00  4.3490E-01  1.0000E-04
 mr-sdci #  1  3    -36.3294361182  8.4010E+00  0.0000E+00  8.4470E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.98289702    -0.00126161     0.13798897    -0.12194629
 ref:   2     0.00153439     0.99993201    -0.00860660    -0.00771646

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -38.7191416002  3.7463E-03  4.2111E-04  2.9680E-02  1.0000E-04
 mr-sdci #  2  2    -38.4076537038  2.2848E-04  0.0000E+00  4.3406E-01  1.0000E-04
 mr-sdci #  2  3    -36.9225987763  5.9316E-01  0.0000E+00  8.3008E-01  1.0000E-04
 mr-sdci #  2  4    -36.0341590355  8.1057E+00  0.0000E+00  8.6877E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.01
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98207357     0.00302005     0.11296942     0.12428024    -0.08552611
 ref:   2    -0.00248451    -0.99988783     0.00520590    -0.00788169    -0.01135519

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -38.7195877936  4.4619E-04  5.4651E-05  1.0722E-02  1.0000E-04
 mr-sdci #  3  2    -38.4078735227  2.1982E-04  0.0000E+00  4.3339E-01  1.0000E-04
 mr-sdci #  3  3    -37.4574442226  5.3485E-01  0.0000E+00  6.4734E-01  1.0000E-04
 mr-sdci #  3  4    -36.2233146709  1.8916E-01  0.0000E+00  8.8355E-01  1.0000E-04
 mr-sdci #  3  5    -35.9968746735  8.0684E+00  0.0000E+00  8.9175E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98196616    -0.00242461    -0.07588391     0.13623868    -0.00421721     0.10676824
 ref:   2     0.00221618     0.99985088     0.00557415     0.00576996    -0.01503815    -0.00167171

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -38.7196450327  5.7239E-05  6.6940E-06  3.7096E-03  1.0000E-04
 mr-sdci #  4  2    -38.4080151393  1.4162E-04  0.0000E+00  4.3313E-01  1.0000E-04
 mr-sdci #  4  3    -37.7620293285  3.0459E-01  0.0000E+00  5.3162E-01  1.0000E-04
 mr-sdci #  4  4    -36.7943316983  5.7102E-01  0.0000E+00  7.9951E-01  1.0000E-04
 mr-sdci #  4  5    -36.0534952301  5.6621E-02  0.0000E+00  9.9916E-01  1.0000E-04
 mr-sdci #  4  6    -35.7948855356  7.8665E+00  0.0000E+00  7.7959E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98194178     0.00236401    -0.05452848    -0.10934559    -0.10966554     0.00298750     0.09391208
 ref:   2     0.00220779    -0.99983836     0.00787743    -0.00290995    -0.00420883     0.01502132    -0.00212303

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -38.7196529025  7.8698E-06  1.3630E-06  1.6042E-03  1.0000E-04
 mr-sdci #  5  2    -38.4080196561  4.5168E-06  0.0000E+00  4.3313E-01  1.0000E-04
 mr-sdci #  5  3    -37.9358268572  1.7380E-01  0.0000E+00  5.2145E-01  1.0000E-04
 mr-sdci #  5  4    -37.1719508146  3.7762E-01  0.0000E+00  7.6915E-01  1.0000E-04
 mr-sdci #  5  5    -36.4330465116  3.7955E-01  0.0000E+00  8.8503E-01  1.0000E-04
 mr-sdci #  5  6    -36.0534472871  2.5856E-01  0.0000E+00  9.9984E-01  1.0000E-04
 mr-sdci #  5  7    -35.7391272370  7.8107E+00  0.0000E+00  8.0064E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.01   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98195633    -0.00234235     0.01918766     0.11098858    -0.04720816
 ref:   2     0.00215794     0.99916908    -0.03651120     0.00763014     0.00473557

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -38.7196545445  1.6420E-06  3.2662E-07  7.7575E-04  1.0000E-04
 mr-sdci #  6  2    -38.4083567441  3.3709E-04  0.0000E+00  4.3280E-01  1.0000E-04
 mr-sdci #  6  3    -38.1563773986  2.2055E-01  0.0000E+00  4.2568E-01  1.0000E-04
 mr-sdci #  6  4    -37.4958314635  3.2388E-01  0.0000E+00  4.8555E-01  1.0000E-04
 mr-sdci #  6  5    -36.3530977171 -7.9949E-02  0.0000E+00  8.2604E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98193154    -0.00157759     0.02549135     0.07808473     0.11292129     0.00249852
 ref:   2     0.00214627     0.99846243    -0.05254563     0.00463672     0.00433842    -0.00689034

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -38.7196548638  3.1929E-07  4.0866E-08  2.7322E-04  1.0000E-04
 mr-sdci #  7  2    -38.4085232875  1.6654E-04  0.0000E+00  4.3177E-01  1.0000E-04
 mr-sdci #  7  3    -38.2285300161  7.2153E-02  0.0000E+00  2.7827E-01  1.0000E-04
 mr-sdci #  7  4    -37.6126949099  1.1686E-01  0.0000E+00  4.2688E-01  1.0000E-04
 mr-sdci #  7  5    -36.8388120727  4.8571E-01  0.0000E+00  7.4022E-01  1.0000E-04
 mr-sdci #  7  6    -36.2222917285  1.6884E-01  0.0000E+00  8.9458E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98193962    -0.00182147     0.01534918     0.08700588    -0.07636557    -0.09437527     0.03027855
 ref:   2     0.00214701     0.99810011    -0.05939383     0.00471998    -0.00077001    -0.00865672    -0.00386583

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -38.7196548979  3.4176E-08  7.3490E-09  1.1966E-04  1.0000E-04
 mr-sdci #  8  2    -38.4085618609  3.8573E-05  0.0000E+00  4.3143E-01  1.0000E-04
 mr-sdci #  8  3    -38.2525033230  2.3973E-02  0.0000E+00  2.5316E-01  1.0000E-04
 mr-sdci #  8  4    -37.6642172800  5.1522E-02  0.0000E+00  4.4305E-01  1.0000E-04
 mr-sdci #  8  5    -36.9250971817  8.6285E-02  0.0000E+00  7.2728E-01  1.0000E-04
 mr-sdci #  8  6    -36.6177345820  3.9544E-01  0.0000E+00  9.4611E-01  1.0000E-04
 mr-sdci #  8  7    -36.1522465235  4.1312E-01  0.0000E+00  8.6640E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.01
    9    6    0   0.02   0.01   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98193595     0.00212497    -0.01984638    -0.06718727    -0.09867469
 ref:   2     0.00214901    -0.99831952     0.05371694     0.00288641    -0.01607900

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -38.7196549039  5.9915E-09  0.0000E+00  5.5207E-05  1.0000E-04
 mr-sdci #  9  2    -38.4085923606  3.0500E-05  1.0233E-01  4.3126E-01  1.0000E-04
 mr-sdci #  9  3    -38.2680715395  1.5568E-02  0.0000E+00  2.0379E-01  1.0000E-04
 mr-sdci #  9  4    -37.7233892408  5.9172E-02  0.0000E+00  4.2309E-01  1.0000E-04
 mr-sdci #  9  5    -36.5655643687 -3.5953E-01  0.0000E+00  1.1287E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98193596    -0.00240566     0.01979860    -0.06707222    -0.09416103     0.02985960
 ref:   2     0.00214822     0.98058621    -0.03346916    -0.00578722     0.05006075     0.18597092

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -38.7196549039  6.4126E-13  0.0000E+00  5.5199E-05  1.0000E-04
 mr-sdci # 10  2    -38.4888970061  8.0305E-02  5.0976E-03  9.7550E-02  1.0000E-04
 mr-sdci # 10  3    -38.2682195912  1.4805E-04  0.0000E+00  2.0185E-01  1.0000E-04
 mr-sdci # 10  4    -37.7237194698  3.3023E-04  0.0000E+00  4.2376E-01  1.0000E-04
 mr-sdci # 10  5    -36.5922173708  2.6653E-02  0.0000E+00  1.1091E+00  1.0000E-04
 mr-sdci # 10  6    -36.2886393243 -3.2910E-01  0.0000E+00  9.2765E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98193594    -0.00163759    -0.01973409     0.06716969    -0.01427851    -0.09542813     0.02388142
 ref:   2     0.00214768     0.97645105     0.03158245     0.00465378     0.13048860     0.02114605     0.16686876

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -38.7196549039  4.3325E-12  0.0000E+00  5.5304E-05  1.0000E-04
 mr-sdci # 11  2    -38.4941083304  5.2113E-03  6.4533E-04  3.5916E-02  1.0000E-04
 mr-sdci # 11  3    -38.2682435103  2.3919E-05  0.0000E+00  2.0158E-01  1.0000E-04
 mr-sdci # 11  4    -37.7237446789  2.5209E-05  0.0000E+00  4.2407E-01  1.0000E-04
 mr-sdci # 11  5    -37.2958673501  7.0365E-01  0.0000E+00  8.9092E-01  1.0000E-04
 mr-sdci # 11  6    -36.5805220073  2.9188E-01  0.0000E+00  1.1236E+00  1.0000E-04
 mr-sdci # 11  7    -36.1649813175  1.2735E-02  0.0000E+00  1.0104E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98193597     0.00186244     0.01953234    -0.06751154    -0.00966360
 ref:   2     0.00214733    -0.97610978    -0.03354138    -0.00610499    -0.02388753

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -38.7196549039  1.3646E-11  0.0000E+00  5.5330E-05  1.0000E-04
 mr-sdci # 12  2    -38.4947023362  5.9401E-04  1.5598E-04  1.4437E-02  1.0000E-04
 mr-sdci # 12  3    -38.2685192379  2.7573E-04  0.0000E+00  2.0322E-01  1.0000E-04
 mr-sdci # 12  4    -37.7247404069  9.9573E-04  0.0000E+00  4.2732E-01  1.0000E-04
 mr-sdci # 12  5    -36.6647815860 -6.3109E-01  0.0000E+00  1.1101E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98193591     0.00170743     0.01974778     0.00127455    -0.06794516    -0.01380848
 ref:   2     0.00214764    -0.97445380    -0.03758624     0.10856128     0.00875313     0.01077536

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1    -38.7196549040  3.5014E-11  0.0000E+00  5.5156E-05  1.0000E-04
 mr-sdci # 13  2    -38.4948685376  1.6620E-04  3.2466E-05  7.2980E-03  1.0000E-04
 mr-sdci # 13  3    -38.2688800921  3.6085E-04  0.0000E+00  2.0259E-01  1.0000E-04
 mr-sdci # 13  4    -37.8514280238  1.2669E-01  0.0000E+00  5.7226E-01  1.0000E-04
 mr-sdci # 13  5    -37.7222543664  1.0575E+00  0.0000E+00  4.3765E-01  1.0000E-04
 mr-sdci # 13  6    -36.5066273003 -7.3895E-02  0.0000E+00  1.1552E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1300s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98193603     0.00180067     0.01884063    -0.00808183    -0.06837988    -0.02530940     0.01608450
 ref:   2     0.00214722    -0.97463710    -0.03715760    -0.09162635     0.01344934     0.07275490    -0.09493065

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1    -38.7196549040  4.0215E-11  0.0000E+00  5.5352E-05  1.0000E-04
 mr-sdci # 14  2    -38.4948902921  2.1754E-05  4.9983E-06  2.9445E-03  1.0000E-04
 mr-sdci # 14  3    -38.2701128127  1.2327E-03  0.0000E+00  2.0384E-01  1.0000E-04
 mr-sdci # 14  4    -37.8883855061  3.6957E-02  0.0000E+00  4.9437E-01  1.0000E-04
 mr-sdci # 14  5    -37.7226927633  4.3840E-04  0.0000E+00  4.4391E-01  1.0000E-04
 mr-sdci # 14  6    -36.6798072831  1.7318E-01  0.0000E+00  1.1567E+00  1.0000E-04
 mr-sdci # 14  7    -36.0234946111 -1.4149E-01  0.0000E+00  1.0410E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.01   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98193604    -0.00179833    -0.01850753     0.00886439     0.00002074
 ref:   2     0.00214821     0.97438781     0.04558812     0.10965485     0.08213750

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -38.7196549041  1.2048E-10  0.0000E+00  5.3755E-05  1.0000E-04
 mr-sdci # 15  2    -38.4948946731  4.3810E-06  2.1863E-06  1.6865E-03  1.0000E-04
 mr-sdci # 15  3    -38.2734040579  3.2912E-03  0.0000E+00  2.0255E-01  1.0000E-04
 mr-sdci # 15  4    -37.9407338363  5.2348E-02  0.0000E+00  4.2823E-01  1.0000E-04
 mr-sdci # 15  5    -36.6392947418 -1.0834E+00  0.0000E+00  9.7736E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.03   0.00   0.00   0.02
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1800s 
time spent in multnx:                   0.1600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0400s 
total time per CI iteration:            0.2300s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98193632    -0.00183837    -0.01630354     0.01723237    -0.02379127    -0.02037366
 ref:   2     0.00214743     0.97447054     0.04299174     0.08050167     0.08447900     0.15817662

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1    -38.7196549042  1.0776E-10  0.0000E+00  5.2213E-05  1.0000E-04
 mr-sdci # 16  2    -38.4948966570  1.9840E-06  1.8257E-07  5.9512E-04  1.0000E-04
 mr-sdci # 16  3    -38.2765372634  3.1332E-03  0.0000E+00  1.9530E-01  1.0000E-04
 mr-sdci # 16  4    -37.9839643255  4.3230E-02  0.0000E+00  3.4037E-01  1.0000E-04
 mr-sdci # 16  5    -37.4140960050  7.7480E-01  0.0000E+00  5.9128E-01  1.0000E-04
 mr-sdci # 16  6    -36.1462671343 -5.3354E-01  0.0000E+00  8.5427E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.02   0.00   0.00   0.02
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.010000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98193621    -0.00183037    -0.01685690     0.01518710    -0.01538519    -0.03377567     0.02229493
 ref:   2     0.00214736     0.97447226     0.04289913     0.07531535     0.09144662    -0.01197202    -0.15722927

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1    -38.7196549043  3.7186E-11  0.0000E+00  5.1233E-05  1.0000E-04
 mr-sdci # 17  2    -38.4948968227  1.6568E-07  2.4765E-08  2.2871E-04  1.0000E-04
 mr-sdci # 17  3    -38.2778717093  1.3344E-03  0.0000E+00  1.8697E-01  1.0000E-04
 mr-sdci # 17  4    -37.9948923035  1.0928E-02  0.0000E+00  3.7045E-01  1.0000E-04
 mr-sdci # 17  5    -37.4540601979  3.9964E-02  0.0000E+00  4.1948E-01  1.0000E-04
 mr-sdci # 17  6    -36.8054703425  6.5920E-01  0.0000E+00  1.1451E+00  1.0000E-04
 mr-sdci # 17  7    -36.1438080236  1.2031E-01  0.0000E+00  8.5421E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98193622     0.00182923     0.01693889    -0.01438909     0.01199375
 ref:   2     0.00214733    -0.97446835    -0.04347466    -0.07605366    -0.00906856

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1    -38.7196549043  3.8245E-12  0.0000E+00  5.0987E-05  1.0000E-04
 mr-sdci # 18  2    -38.4948968456  2.2823E-08  4.8502E-09  9.5131E-05  1.0000E-04
 mr-sdci # 18  3    -38.2781606124  2.8890E-04  0.0000E+00  1.8756E-01  1.0000E-04
 mr-sdci # 18  4    -38.0021928347  7.3005E-03  0.0000E+00  3.0493E-01  1.0000E-04
 mr-sdci # 18  5    -36.7606841510 -6.9338E-01  0.0000E+00  1.2231E+00  1.0000E-04


 mr-sdci  convergence criteria satisfied after 18 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1    -38.7196549043  3.8245E-12  0.0000E+00  5.0987E-05  1.0000E-04
 mr-sdci # 18  2    -38.4948968456  2.2823E-08  4.8502E-09  9.5131E-05  1.0000E-04
 mr-sdci # 18  3    -38.2781606124  2.8890E-04  0.0000E+00  1.8756E-01  1.0000E-04
 mr-sdci # 18  4    -38.0021928347  7.3005E-03  0.0000E+00  3.0493E-01  1.0000E-04
 mr-sdci # 18  5    -36.7606841510 -6.9338E-01  0.0000E+00  1.2231E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -38.719654904282
   ci vector at position   2 energy=  -38.494896845555

################END OF CIUDGINFO################


    2 of the   6 expansion vectors are transformed.
    2 of the   5 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.98193622)

information on vector: 1from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference  2(overlap=  0.974468346)

information on vector: 2from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -38.7196549043

                                                       internal orbitals

                                          level       1    2    3    4

                                          orbital     2    3    8   10

                                         symmetry     1    1    2    3

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.010235                        +-   +-   +-      
 z*  1  1       2 -0.963774                        +-   +-        +- 
 z*  1  1       3  0.157764                        +-        +-   +- 
 z*  1  1       4  0.083305                        +     -   +-   +- 
 z*  1  1       5  0.058759                             +-   +-   +- 
 y   1  1       8  0.017989              2(   3)   +-   +-         - 
 y   1  1       9 -0.024107              3(   3)   +-   +-         - 
 y   1  1      14 -0.012405              1(   1)   +-    -        +- 
 y   1  1      15  0.018430              2(   1)   +-    -        +- 
 y   1  1      16 -0.013493              3(   1)   +-    -        +- 
 y   1  1      17  0.019087              4(   1)   +-    -        +- 
 y   1  1      21 -0.021038              1(   2)   +-         -   +- 
 y   1  1      26  0.011232              1(   1)    -   +-        +- 
 y   1  1      41 -0.020138              1(   2)   +     -    -   +- 
 y   1  1      45 -0.016869              1(   2)        +-    -   +- 
 x   1  1      54  0.023519    1(   1)   1(   3)   +-    -         - 
 x   1  1      55 -0.027041    2(   1)   1(   3)   +-    -         - 
 x   1  1      59 -0.020937    2(   1)   2(   3)   +-    -         - 
 x   1  1      73 -0.013691    1(   1)   1(   3)    -   +-         - 
 x   1  1      77 -0.020965    1(   1)   2(   3)    -   +-         - 
 x   1  1      97 -0.024097    1(   1)   2(   1)    -    -        +- 
 w   1  1     147  0.031329    1(   1)   1(   1)   +-   +-           
 w   1  1     152  0.013405    3(   1)   3(   1)   +-   +-           
 w   1  1     158  0.078311    1(   3)   1(   3)   +-   +-           
 w   1  1     160  0.025544    2(   3)   2(   3)   +-   +-           
 w   1  1     168 -0.058758    1(   1)   1(   3)   +-   +          - 
 w   1  1     169  0.029251    2(   1)   1(   3)   +-   +          - 
 w   1  1     173  0.020914    2(   1)   2(   3)   +-   +          - 
 w   1  1     174  0.012469    3(   1)   2(   3)   +-   +          - 
 w   1  1     178  0.011046    3(   1)   3(   3)   +-   +          - 
 w   1  1     191 -0.013541    1(   3)   1(   3)   +-        +-      
 w   1  1     200  0.024180    1(   1)   1(   1)   +-             +- 
 w   1  1     201 -0.018202    1(   1)   2(   1)   +-             +- 
 w   1  1     202  0.043141    2(   1)   2(   1)   +-             +- 
 w   1  1     211  0.044826    1(   3)   1(   3)   +-             +- 
 w   1  1     212 -0.012919    1(   3)   2(   3)   +-             +- 
 w   1  1     214  0.011358    1(   3)   3(   3)   +-             +- 
 w   1  1     221  0.057318    1(   1)   1(   3)   +    +-         - 
 w   1  1     222  0.011768    2(   1)   1(   3)   +    +-         - 
 w   1  1     223  0.011599    3(   1)   1(   3)   +    +-         - 
 w   1  1     225  0.011404    1(   1)   2(   3)   +    +-         - 
 w   1  1     227 -0.024510    3(   1)   2(   3)   +    +-         - 
 w   1  1     231 -0.010622    3(   1)   3(   3)   +    +-         - 
 w   1  1     253 -0.031629    1(   1)   1(   1)   +     -        +- 
 w   1  1     254  0.013183    1(   1)   2(   1)   +     -        +- 
 w   1  1     255  0.018745    2(   1)   2(   1)   +     -        +- 
 w   1  1     258 -0.010693    3(   1)   3(   1)   +     -        +- 
 w   1  1     265  0.013080    1(   3)   2(   3)   +     -        +- 
 w   1  1     273 -0.013042    1(   1)   1(   3)   +         +-    - 
 w   1  1     309  0.035847    1(   1)   1(   1)        +-        +- 
 w   1  1     314  0.010955    3(   1)   3(   1)        +-        +- 
 w   1  1     320  0.019127    1(   3)   1(   3)        +-        +- 
 w   1  1     342 -0.010301    1(   1)   1(   1)             +-   +- 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              51
     0.01> rq > 0.001            164
    0.001> rq > 0.0001           112
   0.0001> rq > 0.00001           27
  0.00001> rq > 0.000001           2
 0.000001> rq                      0
           all                   358
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        34 2x:         0 4x:         0
All internal counts: zz :        14 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        13    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        29    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.010235253069     -0.398123863784
     2     2     -0.963773610107     37.245107202112
     3     3      0.157763653567     -6.097487752922
     4     4      0.083305171318     -3.217261627498
     5     5      0.058758572692     -2.269732172966

 number of reference csfs (nref) is     5.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with  6 correlated electrons.

 eref      =    -38.644980748205   "relaxed" cnot**2         =   0.964246023765
 eci       =    -38.719654904282   deltae = eci - eref       =  -0.074674156078
 eci+dv1   =    -38.722324802284   dv1 = (1-cnot**2)*deltae  =  -0.002669898002
 eci+dv2   =    -38.722423801365   dv2 = dv1 / cnot**2       =  -0.002768897082
 eci+dv3   =    -38.722530424874   dv3 = dv1 / (2*cnot**2-1) =  -0.002875520592
 eci+pople =    -38.721523351670   (  6e- scaled deltae )    =  -0.076542603465


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -38.4948968456

                                                       internal orbitals

                                          level       1    2    3    4

                                          orbital     2    3    8   10

                                         symmetry     1    1    2    3

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.139760                        +-   +-   +-      
 z*  1  1       2  0.148504                        +-   +-        +- 
 z*  1  1       3  0.950730                        +-        +-   +- 
 z*  1  1       4 -0.019761                        +     -   +-   +- 
 z*  1  1       5 -0.066640                             +-   +-   +- 
 y   1  1       7  0.010125              1(   3)   +-   +-         - 
 y   1  1      10  0.050223              1(   1)   +-    -   +-      
 y   1  1      12  0.017152              3(   1)   +-    -   +-      
 y   1  1      14  0.040216              1(   1)   +-    -        +- 
 y   1  1      15  0.011835              2(   1)   +-    -        +- 
 y   1  1      17  0.019773              4(   1)   +-    -        +- 
 y   1  1      18 -0.031830              1(   3)   +-        +-    - 
 y   1  1      21 -0.055769              1(   2)   +-         -   +- 
 y   1  1      30  0.017671              1(   3)    -   +    +-    - 
 y   1  1      33  0.011093              1(   2)    -   +     -   +- 
 y   1  1      34  0.026994              1(   1)    -        +-   +- 
 y   1  1      36 -0.012966              3(   1)    -        +-   +- 
 y   1  1      38 -0.105071              1(   3)   +     -   +-    - 
 y   1  1      39  0.032280              2(   3)   +     -   +-    - 
 y   1  1      41 -0.013334              1(   2)   +     -    -   +- 
 y   1  1      48 -0.015897              3(   1)         -   +-   +- 
 x   1  1      66  0.032298    1(   2)   1(   3)   +-         -    - 
 x   1  1      67  0.027654    1(   2)   2(   3)   +-         -    - 
 x   1  1     109  0.015477    1(   1)   1(   3)    -        +-    - 
 x   1  1     113  0.019179    1(   1)   2(   3)    -        +-    - 
 x   1  1     121  0.026335    1(   1)   1(   2)    -         -   +- 
 x   1  1     123 -0.011283    3(   1)   1(   2)    -         -   +- 
 w   1  1     158 -0.011582    1(   3)   1(   3)   +-   +-           
 w   1  1     180 -0.022228    1(   1)   1(   1)   +-        +-      
 w   1  1     185 -0.011632    3(   1)   3(   1)   +-        +-      
 w   1  1     191 -0.070055    1(   3)   1(   3)   +-        +-      
 w   1  1     193 -0.024373    2(   3)   2(   3)   +-        +-      
 w   1  1     197 -0.034668    1(   2)   1(   3)   +-        +     - 
 w   1  1     198 -0.025364    1(   2)   2(   3)   +-        +     - 
 w   1  1     200 -0.023498    1(   1)   1(   1)   +-             +- 
 w   1  1     202 -0.018030    2(   1)   2(   1)   +-             +- 
 w   1  1     210 -0.054146    1(   2)   1(   2)   +-             +- 
 w   1  1     211 -0.014226    1(   3)   1(   3)   +-             +- 
 w   1  1     221 -0.010864    1(   1)   1(   3)   +    +-         - 
 w   1  1     273 -0.054339    1(   1)   1(   3)   +         +-    - 
 w   1  1     274 -0.013161    2(   1)   1(   3)   +         +-    - 
 w   1  1     275 -0.010305    3(   1)   1(   3)   +         +-    - 
 w   1  1     277 -0.013905    1(   1)   2(   3)   +         +-    - 
 w   1  1     279  0.023165    3(   1)   2(   3)   +         +-    - 
 w   1  1     285 -0.039226    1(   1)   1(   2)   +          -   +- 
 w   1  1     286 -0.018639    2(   1)   1(   2)   +          -   +- 
 w   1  1     287  0.013244    3(   1)   1(   2)   +          -   +- 
 w   1  1     300  0.013559    1(   3)   1(   3)        +-   +-      
 w   1  1     342 -0.034329    1(   1)   1(   1)             +-   +- 
 w   1  1     347 -0.010505    3(   1)   3(   1)             +-   +- 
 w   1  1     353 -0.025727    1(   3)   1(   3)             +-   +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              47
     0.01> rq > 0.001            187
    0.001> rq > 0.0001           103
   0.0001> rq > 0.00001           17
  0.00001> rq > 0.000001           0
 0.000001> rq                      0
           all                   358
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        34 2x:         0 4x:         0
All internal counts: zz :        14 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        13    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        29    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.139760005071      5.361414524616
     2     2      0.148504364008     -5.703228793202
     3     3      0.950729687760    -36.515926233108
     4     4     -0.019761362829      0.763476930135
     5     5     -0.066639864066      2.557239835237

 number of reference csfs (nref) is     5.  root number (iroot) is  2.

 pople ci energy extrapolation is computed with  6 correlated electrons.

 eref      =    -38.407198495048   "relaxed" cnot**2         =   0.950304727279
 eci       =    -38.494896845555   deltae = eci - eref       =  -0.087698350507
 eci+dv1   =    -38.499255039000   dv1 = (1-cnot**2)*deltae  =  -0.004358193446
 eci+dv2   =    -38.499482946540   dv2 = dv1 / cnot**2       =  -0.004586100985
 eci+dv3   =    -38.499736005774   dv3 = dv1 / (2*cnot**2-1) =  -0.004839160219
 eci+pople =    -38.498006530117   (  6e- scaled deltae )    =  -0.090808035069
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999698

 space required:

    space required for calls in multd2:
       onex           40
       allin           0
       diagon         85
    max.      ---------
       maxnex         85

    total core space usage:
       maxnex         85
    max k-seg        212
    max k-ind         20
              ---------
       totmax        549
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      18  DYX=      44  DYW=      54
   D0Z=       2  D0Y=      11  D0X=       8  D0W=      10
  DDZI=      16 DDYI=      52 DDXI=      48 DDWI=      56
  DDZE=       0 DDYE=      16 DDXE=      15 DDWE=      20
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9789199      1.9113872      0.0190541      0.0077463
   0.0025447      0.0005981


*****   symmetry block SYM2   *****

 occupation numbers of nos
   0.0753722      0.0005135


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9693785      0.0284955      0.0048919      0.0010980


 total number of electrons =    8.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.994144   0.676262   1.176468   0.000737   0.004400   0.000420
    C1_ p      -0.000029   0.511021   0.760850   0.008997   0.002929   0.000197
    H1_ s       0.005885   0.791638  -0.025931   0.009320   0.000417   0.001928

   ao class       7A1 
    C1_ s       0.000164
    C1_ p       0.000249
    H1_ s       0.000185

                        B1  partial gross atomic populations
   ao class       1B1        2B1 
    C1_ p       0.075372   0.000514

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    C1_ p       1.014680   0.014220   0.002685   0.000480
    H1_ s       0.954699   0.014275   0.002207   0.000618


                        gross atomic populations
     ao           C1_        H1_
      s         3.852595   1.755241
      p         2.392164   0.000000
    total       6.244759   1.755241


 Total number of electrons:    8.00000000

NO coefficients and occupation numbers are written to nocoef_ci.2
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=      18  DYX=      44  DYW=      54
   D0Z=       2  D0Y=      11  D0X=       8  D0W=      10
  DDZI=      16 DDYI=      52 DDXI=      48 DDWI=      56
  DDZE=       0 DDYE=      16 DDXE=      15 DDWE=      20
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9634827      0.1151705      0.0152330      0.0022251
   0.0011081      0.0006551


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9332431      0.0138008


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9188044      0.0304848      0.0052886      0.0005037


 total number of electrons =    8.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.994144   1.310180   0.033622   0.003239   0.001175   0.000252
    C1_ p      -0.000029   0.112537   0.054739   0.004993   0.000635   0.000804
    H1_ s       0.005885   0.540766   0.026810   0.007002   0.000415   0.000052

   ao class       7A1 
    C1_ s       0.000049
    C1_ p       0.000084
    H1_ s       0.000522

                        B1  partial gross atomic populations
   ao class       1B1        2B1 
    C1_ p       1.933243   0.013801

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    C1_ p       1.038742   0.014318   0.003596   0.000156
    H1_ s       0.880063   0.016167   0.001692   0.000348


                        gross atomic populations
     ao           C1_        H1_
      s         3.342662   1.479720
      p         3.177618   0.000000
    total       6.520280   1.479720


 Total number of electrons:    8.00000000

