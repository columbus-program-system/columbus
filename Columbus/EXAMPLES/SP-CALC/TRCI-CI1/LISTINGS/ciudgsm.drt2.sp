1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003
 cidrt_title                                                                     

 297 dimension of the ci-matrix ->>>       280


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     2)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.5360197176 -1.7764E-15  1.0625E-01  4.3642E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -38.5360197176 -1.7764E-15  1.0625E-01  4.3642E-01  1.0000E-04

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.6166978443  8.0678E-02  5.1602E-03  1.0135E-01  1.0000E-04
 mr-sdci #  2  1    -38.6216680418  4.9702E-03  6.0893E-04  3.4928E-02  1.0000E-04
 mr-sdci #  3  1    -38.6223083314  6.4029E-04  7.7402E-05  1.2522E-02  1.0000E-04
 mr-sdci #  4  1    -38.6223851752  7.6844E-05  9.0604E-06  4.1260E-03  1.0000E-04
 mr-sdci #  5  1    -38.6223950777  9.9025E-06  1.4732E-06  1.6909E-03  1.0000E-04
 mr-sdci #  6  1    -38.6223970400  1.9623E-06  2.5481E-07  7.1027E-04  1.0000E-04
 mr-sdci #  7  1    -38.6223973264  2.8639E-07  4.7392E-08  2.9197E-04  1.0000E-04
 mr-sdci #  8  1    -38.6223973733  4.6950E-08  3.8157E-09  8.9596E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  8  1    -38.6223973733  4.6950E-08  3.8157E-09  8.9596E-05  1.0000E-04

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 eref      =    -38.536019717397   "relaxed" cnot**2         =   0.952894288637
 eci       =    -38.622397373313   deltae = eci - eref       =  -0.086377655915
 eci+dv1   =    -38.626466254241   dv1 = (1-cnot**2)*deltae  =  -0.004068880928
 eci+dv2   =    -38.626667396731   dv2 = dv1 / cnot**2       =  -0.004270023418
 eci+dv3   =    -38.626889460131   dv3 = dv1 / (2*cnot**2-1) =  -0.004492086818
 eci+pople =    -38.625290126363   (  6e- scaled deltae )    =  -0.089270408965
