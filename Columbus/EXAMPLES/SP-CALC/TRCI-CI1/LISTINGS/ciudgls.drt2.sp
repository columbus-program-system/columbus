1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 280
global arrays:         9240   (    0.07 MB)
vdisk:                    0   (    0.00 MB)
drt:                 278229   (    1.06 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 2
  NVBKMX = 7
  RTOLBK = 1e-4,1e-4,
  NITER = 90
  NVCIMN = 4
  NVCIMX = 16
  RTOLCI = 1e-4,1e-4,
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    7      nvrfmn =    4
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   90
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    7      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    7      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082858

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003

 core energy values from the integral file:
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -2.792842564975E+01

 drt header information:
 cidrt_title                                                                     
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       42        2       15       12       13
 ncsft:             280
 total number of valid internal walks:      42
 nvalz,nvaly,nvalx,nvalw =        2      15      12      13

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext    72    64    20
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      330      432      360      156       27        0        0        0
 minbl4,minbl3,maxbl2    48    48    51
 maxbuf 32767
 number of external orbitals per symmetry block:   4   1   3   0
 nmsym   4 number of internal orbitals   4

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003
 cidrt_title                                                                     
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     7    20    15    20 total internal walks:      62
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 3

 setref:        2 references kept,
                0 references were marked as invalid, out of
                2 total.
 limcnvrt: found 2 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  2 segmentation marks segtype= 1
 limcnvrt: found 15 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 2
 limcnvrt: found 12 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  12 segmentation marks segtype= 3
 limcnvrt: found 13 valid internal walksout of  19
  walks (skipping trailing invalids)
  ... adding  13 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x       9       4      12       3
 vertex w      17       4      12       3



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           2|         2|         0|         2|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          15|        38|         2|        15|         2|         2|
 -------------------------------------------------------------------------------
  X 3          12|       106|        40|        12|        17|         3|
 -------------------------------------------------------------------------------
  W 4          13|       134|       146|        13|        29|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>       280


 297 dimension of the ci-matrix ->>>       280


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      12       2        106          2      12       2
     2  4   1    25      two-ext wz   2X  4 1      13       2        134          2      13       2
     3  4   3    26      two-ext wx   2X  4 3      13      12        134        106      13      12
     4  2   1    11      one-ext yz   1X  2 1      15       2         38          2      15       2
     5  3   2    15      1ex3ex  yx   3X  3 2      12      15        106         38      12      15
     6  4   2    16      1ex3ex  yw   3X  4 2      13      15        134         38      13      15
     7  1   1     1      allint zz    OX  1 1       2       2          2          2       2       2
     8  2   2     5      0ex2ex yy    OX  2 2      15      15         38         38      15      15
     9  3   3     6      0ex2ex xx    OX  3 3      12      12        106        106      12      12
    10  4   4     7      0ex2ex ww    OX  4 4      13      13        134        134      13      13
    11  1   1    75      dg-024ext z  DG  1 1       2       2          2          2       2       2
    12  2   2    45      4exdg024 y   DG  2 2      15      15         38         38      15      15
    13  3   3    46      4exdg024 x   DG  3 3      12      12        106        106      12      12
    14  4   4    47      4exdg024 w   DG  4 4      13      13        134        134      13      13
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 298 129 40
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        20 2x:         0 4x:         0
All internal counts: zz :         3 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:         2    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        19    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       2

    root           eigenvalues
    ----           ------------
       1         -38.5360197176

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     2)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               280
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:         0 xx:         0 ww:         0
One-external counts: yz :        62 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:        13 wz:        18 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:         0    task #   4:        61
task #   5:         0    task #   6:         0    task #   7:         2    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        19    task #  12:        51
task #  13:        40    task #  14:        39    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.5360197176 -1.7764E-15  1.0625E-01  4.3642E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.01   0.00   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0400s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0400s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.5360197176 -1.7764E-15  1.0625E-01  4.3642E-01  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               280
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             1
 number of iterations:                                   90
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.98126689    -0.19265328

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.6166978443  8.0678E-02  5.1602E-03  1.0135E-01  1.0000E-04
 mr-sdci #  1  2    -36.4429808315  8.5146E+00  0.0000E+00  9.1047E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.01   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97793215     0.16311753     0.13054264

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -38.6216680418  4.9702E-03  6.0893E-04  3.4928E-02  1.0000E-04
 mr-sdci #  2  2    -37.0076585921  5.6468E-01  0.0000E+00  8.1537E-01  1.0000E-04
 mr-sdci #  2  3    -36.1157769429  8.1874E+00  0.0000E+00  8.9346E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.97648090     0.14597083     0.08319834     0.13511330

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -38.6223083314  6.4029E-04  7.7402E-05  1.2522E-02  1.0000E-04
 mr-sdci #  3  2    -37.4990015523  4.9134E-01  0.0000E+00  6.0514E-01  1.0000E-04
 mr-sdci #  3  3    -36.2258153497  1.1004E-01  0.0000E+00  9.0412E-01  1.0000E-04
 mr-sdci #  3  4    -36.1153886387  8.1870E+00  0.0000E+00  8.8783E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97629821     0.11551395     0.15073752     0.01050074    -0.10327761

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -38.6223851752  7.6844E-05  9.0604E-06  4.1260E-03  1.0000E-04
 mr-sdci #  4  2    -37.6971691103  1.9817E-01  0.0000E+00  4.7076E-01  1.0000E-04
 mr-sdci #  4  3    -36.7333696981  5.0755E-01  0.0000E+00  7.3105E-01  1.0000E-04
 mr-sdci #  4  4    -36.1899365099  7.4548E-02  0.0000E+00  9.5277E-01  1.0000E-04
 mr-sdci #  4  5    -35.7319810156  7.8036E+00  0.0000E+00  8.2748E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97620107     0.09793438    -0.09808800     0.13612148    -0.03724800     0.08889655

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -38.6223950777  9.9025E-06  1.4732E-06  1.6909E-03  1.0000E-04
 mr-sdci #  5  2    -37.8590497992  1.6188E-01  0.0000E+00  4.6787E-01  1.0000E-04
 mr-sdci #  5  3    -37.1418218652  4.0845E-01  0.0000E+00  7.6097E-01  1.0000E-04
 mr-sdci #  5  4    -36.6122486588  4.2231E-01  0.0000E+00  8.3666E-01  1.0000E-04
 mr-sdci #  5  5    -36.0714844698  3.3950E-01  0.0000E+00  9.5233E-01  1.0000E-04
 mr-sdci #  5  6    -35.5824143403  7.6540E+00  0.0000E+00  7.1313E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97617309     0.06517856    -0.10961522    -0.14769647     0.02010125     0.02814775    -0.08838412

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -38.6223970400  1.9623E-06  2.5481E-07  7.1027E-04  1.0000E-04
 mr-sdci #  6  2    -38.0505680042  1.9152E-01  0.0000E+00  4.0090E-01  1.0000E-04
 mr-sdci #  6  3    -37.4812868955  3.3947E-01  0.0000E+00  4.7705E-01  1.0000E-04
 mr-sdci #  6  4    -36.6300735803  1.7825E-02  0.0000E+00  7.0168E-01  1.0000E-04
 mr-sdci #  6  5    -36.5599518513  4.8847E-01  0.0000E+00  8.6284E-01  1.0000E-04
 mr-sdci #  6  6    -35.8562850024  2.7387E-01  0.0000E+00  9.3344E-01  1.0000E-04
 mr-sdci #  6  7    -35.5819675832  7.6535E+00  0.0000E+00  7.1209E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97616352    -0.05533609     0.09824791     0.12873548     0.09396696

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -38.6223973264  2.8639E-07  4.7392E-08  2.9197E-04  1.0000E-04
 mr-sdci #  7  2    -38.1306251946  8.0057E-02  0.0000E+00  2.8184E-01  1.0000E-04
 mr-sdci #  7  3    -37.6055645609  1.2428E-01  0.0000E+00  3.9548E-01  1.0000E-04
 mr-sdci #  7  4    -36.9179131944  2.8784E-01  0.0000E+00  7.4100E-01  1.0000E-04
 mr-sdci #  7  5    -36.3174374719 -2.4251E-01  0.0000E+00  7.8409E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97616304    -0.04996001     0.09324293     0.08755782    -0.13021388    -0.05141657

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -38.6223973733  4.6950E-08  3.8157E-09  8.9596E-05  1.0000E-04
 mr-sdci #  8  2    -38.1644873131  3.3862E-02  0.0000E+00  2.0898E-01  1.0000E-04
 mr-sdci #  8  3    -37.6526635556  4.7099E-02  0.0000E+00  3.6501E-01  1.0000E-04
 mr-sdci #  8  4    -37.1948336338  2.7692E-01  0.0000E+00  6.2891E-01  1.0000E-04
 mr-sdci #  8  5    -36.6059761679  2.8854E-01  0.0000E+00  7.9130E-01  1.0000E-04
 mr-sdci #  8  6    -36.2104485981  3.5416E-01  0.0000E+00  8.1156E-01  1.0000E-04


 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -38.6223973733  4.6950E-08  3.8157E-09  8.9596E-05  1.0000E-04
 mr-sdci #  8  2    -38.1644873131  3.3862E-02  0.0000E+00  2.0898E-01  1.0000E-04
 mr-sdci #  8  3    -37.6526635556  4.7099E-02  0.0000E+00  3.6501E-01  1.0000E-04
 mr-sdci #  8  4    -37.1948336338  2.7692E-01  0.0000E+00  6.2891E-01  1.0000E-04
 mr-sdci #  8  5    -36.6059761679  2.8854E-01  0.0000E+00  7.9130E-01  1.0000E-04
 mr-sdci #  8  6    -36.2104485981  3.5416E-01  0.0000E+00  8.1156E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -38.622397373313

################END OF CIUDGINFO################


    1 of the   7 expansion vectors are transformed.
    1 of the   6 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.976163044)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -38.6223973733

                                                       internal orbitals

                                          level       1    2    3    4

                                          orbital     2    3    8   10

                                         symmetry     1    1    2    3

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.976064                        +-   +     -   +- 
 z*  1  1       2 -0.013898                        +    +-    -   +- 
 y   1  1       3  0.031065              1(   1)   +-   +-    -      
 y   1  1       5  0.011017              3(   1)   +-   +-    -      
 y   1  1       8 -0.016315              1(   3)   +-    -   +     - 
 y   1  1      10  0.016592              3(   3)   +-    -   +     - 
 y   1  1      11 -0.041644              1(   2)   +-    -        +- 
 y   1  1      13 -0.017410              2(   3)   +-   +     -    - 
 y   1  1      14  0.012005              3(   3)   +-   +     -    - 
 y   1  1      15  0.067150              1(   1)   +-         -   +- 
 y   1  1      16 -0.013899              2(   1)   +-         -   +- 
 y   1  1      18  0.019592              4(   1)   +-         -   +- 
 y   1  1      20  0.014574              1(   3)    -   +-   +     - 
 y   1  1      24  0.011071              1(   1)    -   +     -   +- 
 y   1  1      29 -0.061077              1(   3)   +    +-    -    - 
 y   1  1      30  0.019416              2(   3)   +    +-    -    - 
 y   1  1      32  0.070589              1(   1)   +     -    -   +- 
 y   1  1      34  0.019637              3(   1)   +     -    -   +- 
 y   1  1      35  0.025323              4(   1)   +     -    -   +- 
 y   1  1      38 -0.010468              3(   1)        +-    -   +- 
 x   1  1      50  0.026143    1(   2)   1(   3)   +-    -         - 
 x   1  1      51  0.020677    1(   2)   2(   3)   +-    -         - 
 x   1  1      54  0.018753    2(   1)   1(   3)   +-         -    - 
 x   1  1      58  0.015222    2(   1)   2(   3)   +-         -    - 
 x   1  1      81  0.010910    1(   1)   2(   3)    -    -   +     - 
 x   1  1      89  0.018638    1(   1)   1(   2)    -    -        +- 
 x   1  1      93  0.014201    1(   1)   1(   3)    -   +     -    - 
 x   1  1      97  0.017200    1(   1)   2(   3)    -   +     -    - 
 x   1  1     105  0.017528    1(   1)   2(   1)    -         -   +- 
 w   1  1     147 -0.031025    1(   1)   1(   1)   +-   +     -      
 w   1  1     152 -0.013317    3(   1)   3(   1)   +-   +     -      
 w   1  1     158 -0.080121    1(   3)   1(   3)   +-   +     -      
 w   1  1     160 -0.025941    2(   3)   2(   3)   +-   +     -      
 w   1  1     164 -0.026696    1(   2)   1(   3)   +-   +          - 
 w   1  1     165 -0.016907    1(   2)   2(   3)   +-   +          - 
 w   1  1     167  0.032531    1(   1)   1(   3)   +-        +     - 
 w   1  1     168 -0.020433    2(   1)   1(   3)   +-        +     - 
 w   1  1     172 -0.016249    2(   1)   2(   3)   +-        +     - 
 w   1  1     180 -0.045968    2(   1)   1(   2)   +-             +- 
 w   1  1     203 -0.036478    1(   1)   1(   3)   +     -   +     - 
 w   1  1     209  0.012842    3(   1)   2(   3)   +     -   +     - 
 w   1  1     215 -0.031179    1(   1)   1(   2)   +     -        +- 
 w   1  1     216 -0.012626    2(   1)   1(   2)   +     -        +- 
 w   1  1     219 -0.049188    1(   1)   1(   3)   +    +     -    - 
 w   1  1     220 -0.010939    2(   1)   1(   3)   +    +     -    - 
 w   1  1     223 -0.010545    1(   1)   2(   3)   +    +     -    - 
 w   1  1     225  0.020953    3(   1)   2(   3)   +    +     -    - 
 w   1  1     231  0.014664    1(   1)   1(   1)   +          -   +- 
 w   1  1     233 -0.013302    2(   1)   2(   1)   +          -   +- 
 w   1  1     264 -0.040556    1(   1)   1(   1)        +     -   +- 
 w   1  1     269 -0.011546    3(   1)   3(   1)        +     -   +- 
 w   1  1     275 -0.024532    1(   3)   1(   3)        +     -   +- 
 w   1  1     277 -0.010199    2(   3)   2(   3)        +     -   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              52
     0.01> rq > 0.001            134
    0.001> rq > 0.0001            77
   0.0001> rq > 0.00001           12
  0.00001> rq > 0.000001           3
 0.000001> rq                      1
           all                   280
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        20 2x:         0 4x:         0
All internal counts: zz :         3 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:         2    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        19    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.976064105496    -37.613625773144
     2     2     -0.013897863082      0.535557208525

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with  6 correlated electrons.

 eref      =    -38.536019717397   "relaxed" cnot**2         =   0.952894288637
 eci       =    -38.622397373313   deltae = eci - eref       =  -0.086377655915
 eci+dv1   =    -38.626466254241   dv1 = (1-cnot**2)*deltae  =  -0.004068880928
 eci+dv2   =    -38.626667396731   dv2 = dv1 / cnot**2       =  -0.004270023418
 eci+dv3   =    -38.626889460131   dv3 = dv1 / (2*cnot**2-1) =  -0.004492086818
 eci+pople =    -38.625290126363   (  6e- scaled deltae )    =  -0.089270408965
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999700

 space required:

    space required for calls in multd2:
       onex           40
       allin           0
       diagon         85
    max.      ---------
       maxnex         85

    total core space usage:
       maxnex         85
    max k-seg        134
    max k-ind         15
              ---------
       totmax        383
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      11  DYX=      39  DYW=      41
   D0Z=       1  D0Y=       9  D0X=       7  D0W=       7
  DDZI=       8 DDYI=      51 DDXI=      39 DDWI=      39
  DDZE=       0 DDYE=      15 DDXE=      12 DDWE=      13
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9746515      0.9986756      0.0205119      0.0049431
   0.0027638      0.0008238


*****   symmetry block SYM2   *****

 occupation numbers of nos
   0.9939758      0.0064822


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9652593      0.0263208      0.0049446      0.0006475


 total number of electrons =    8.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.994144   1.332031   0.218001   0.002892   0.002203   0.001082
    C1_ p      -0.000029   0.120636   0.680330   0.004789   0.002732   0.000811
    H1_ s       0.005885   0.521985   0.100345   0.012831   0.000008   0.000871

   ao class       7A1 
    C1_ s       0.000109
    C1_ p       0.000147
    H1_ s       0.000569

                        B1  partial gross atomic populations
   ao class       1B1        2B1 
    C1_ p       0.993976   0.006482

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    C1_ p       1.021597   0.013471   0.002585   0.000289
    H1_ s       0.943662   0.012850   0.002360   0.000359


                        gross atomic populations
     ao           C1_        H1_
      s         3.550461   1.601723
      p         2.847816   0.000000
    total       6.398277   1.601723


 Total number of electrons:    8.00000000

