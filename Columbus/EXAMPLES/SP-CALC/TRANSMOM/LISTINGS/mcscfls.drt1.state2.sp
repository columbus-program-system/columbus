

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
         8000000 of real*8 words (   61.04 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
  NPATH =  1, 2, 3, 9,10,12,13,17,19,21,23,
  
  
  
  
  
  NSTATE =1, NUNITV=2 ,
  NITER=1,NMITER=0
  TOL(1)= .00000001,
  NCOUPL =   1,
  FCIORB=
          2   1 40, 2   2 40, 2   3 40, 6   1 40, 6   2 40,
  6   3 40,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:40 2003

 Core type energy values:
 energy( 1)=  3.329664742176E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   33.296647422


   ******  Basis set informations:  ******

 Number of irreps:                  8
 Total number of basis functions:  48

 irrep no.              1    2    3    4    5    6    7    8
 irrep label           Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
 no. of bas.fcions.    11    4    7    2   11    4    7    2


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=     1

 maximum number of psci micro-iterations:   nmiter=    0
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   1.excit. state        1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  Ag         3

 orbital coefficients are optimized for the  1 (=nstate) excited state.

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       1( 12)    40
       2       2( 13)    40
       2       3( 14)    40
       6       1( 36)    40
       6       2( 37)    40
       6       3( 38)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  1
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 2 nciitr=25 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:   b1u 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:         9

 !timer: initialization                  user+sys=     0.000 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   6 possible.


 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    54
 Number of active-virtual rotations:     6

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:            540
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2556



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is 106
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  2824

 twoao_o processed      92764 two electron ao integrals.

      92764 ao integrals were written into   87 records

 srtinc_o read in      93892 integrals and wrote out      93892 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.080 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:            540
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2556

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 trial vector  2 is unit matrix column     2
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   5 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1       -77.7055319238     -111.0021793456        0.0000000000        0.0100000000
    2*      -77.2512809123     -110.5479283341        0.0000000000        0.0000010000
    3       -76.9158595738     -110.2125069955        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.0100376267
 *** psci convergence not reached ***
 Total number of micro iterations:    0

 ***  micro: final psci convergence values:  ***
    imxov=  0 z0= 0.00000000 pnorm= 0.0000E+00 rznorm= 0.0000E+00 rpnorm= 0.0000E+00 noldr=  0 nnewr=  1 nolds=  0 nnews=  0
 *** warning *** small z0.

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.465008   -2.115703   -1.225077

 qvv(*) eigenvalues. symmetry block  1
     0.382809    1.114316    1.378798    1.969181    3.131853    3.647367    4.943326    5.369445
 i,qaaresolved 1 -0.36180126
 i,qaaresolved 2  1.22114031
 i,qaaresolved 3  2.54172875

 qvv(*) eigenvalues. symmetry block  2
     3.806998

 fdd(*) eigenvalues. symmetry block  3
    -1.339828

 qvv(*) eigenvalues. symmetry block  3
     0.437081    1.171043    1.666315    3.447186    3.763467    5.573869

 qvv(*) eigenvalues. symmetry block  4
     2.210825    3.886816

 fdd(*) eigenvalues. symmetry block  5
   -22.462515   -1.614107

 qvv(*) eigenvalues. symmetry block  5
     0.458874    0.894786    1.508404    1.899047    2.398137    3.662030    4.442495    5.198053    6.197436
 i,qaaresolved 1  0.231815918
 i,qaaresolved 2  1.33683585
 i,qaaresolved 3  3.89525104

 qvv(*) eigenvalues. symmetry block  6
     4.051683

 fdd(*) eigenvalues. symmetry block  7
    -1.055442

 qvv(*) eigenvalues. symmetry block  7
     0.589581    1.821285    2.454620    3.413146    4.840053    6.701412

 qvv(*) eigenvalues. symmetry block  8
     2.531129    4.663381
 *** warning *** small active-orbital occupation. i=  1 nocc= 5.0118E-05
 *** warning *** small active-orbital occupation. i=  1 nocc= 5.0118E-05
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.120 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -77.2512809123 demc= 7.7251E+01 wnorm= 8.0301E-02 knorm= 0.0000E+00 apxde=-5.0188E-03    *not converged* 

 final mcscf convergence values:
 iter=    1 emc=  -77.2512809123 demc= 7.7251E+01 wnorm= 8.0301E-02 knorm= 0.0000E+00 apxde=-5.0188E-03    *not converged* 




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -77.251280912
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  Ag  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1C1s       0.70789518    -0.02125812    -0.00419445     0.04430833     0.08307301    -0.18292772     0.48704798    -0.13481072
   2C1s       0.00581352     0.60895592    -0.04527383    -0.12265239     0.35863539    -0.63805014     2.25414431    -0.47025737
   3C1s      -0.00547862    -0.16589846     0.04523067    -1.31481302     0.17135522     0.87512771    -3.58613701     1.23194586
   4C1pz      0.00009656    -0.19215706     0.60524810    -0.19758765    -0.62248467     0.39670575     0.34528030     0.00446522
   5C1pz     -0.00034704     0.06340722    -0.14182451    -0.43686892     1.30687341    -0.26120556    -0.82020586     0.58755204
   6C1d0      0.00005798     0.00626140    -0.00649825    -0.00111976     0.01568525    -0.00887776     0.03693396    -0.00141581
   7C1d2+    -0.00013011    -0.00045070    -0.00596111     0.00709739     0.01743193     0.10416193     0.03786626    -0.31728394
   8H1s      -0.00029124     0.18611069     0.28707364     0.01914449    -0.39766694    -0.82103730    -0.05394613    -0.90606010
   9H1s       0.00123292    -0.08687924    -0.08065989     0.92002316     0.16984638     0.55627963     0.85471213     0.29957990
  10H1py      0.00026532    -0.01400472    -0.01377760    -0.00951912    -0.00544873    -0.08674980    -0.14849809    -0.00318511
  11H1pz      0.00005891    -0.01122019     0.00216074    -0.01003212     0.03168852    -0.02487705    -0.05390124    -0.17152526

               MO    9        MO   10        MO   11
   1C1s      -0.34971450    -0.18329942     0.38539758
   2C1s      -1.41194045    -0.62456947     1.81165451
   3C1s       1.10255577     1.03905435    -0.29708401
   4C1pz      0.37263706     0.56080416     0.81853354
   5C1pz     -0.86083433    -0.06780179    -0.14021489
   6C1d0      0.14899851     0.21481558    -0.07376167
   7C1d2+    -0.11166505     0.10298365    -0.30122434
   8H1s       0.02447667    -0.06193176    -1.24170895
   9H1s      -0.01491009    -0.20760172     0.37429927
  10H1py     -0.16434376     0.43109355     0.62560333
  11H1pz      0.28379097    -0.40700884     0.54466797

          mcscf orbitals of the final iteration,  B3u block   2

               MO   12        MO   13        MO   14        MO   15
  12C1px      0.55554859    -1.02179412     0.39636809    -0.27254125
  13C1px      0.04336479     1.08506687    -0.27439335    -0.09046099
  14C1d1+    -0.02801880     0.10915493     0.57900662    -0.27863586
  15H1px      0.01283761    -0.00908218     0.03954878     0.58469177

          mcscf orbitals of the final iteration,  B2u block   3

               MO   16        MO   17        MO   18        MO   19        MO   20        MO   21        MO   22
  16C1py      0.47042675    -0.33298454    -0.09388374     0.88527301     0.03173955    -0.12953450     1.61867682
  17C1py     -0.10000397    -0.61887841     0.44593272    -1.80014931    -0.62745284     0.26171177    -0.11109381
  18C1d1-    -0.00317254     0.00605824    -0.15277815    -0.23917063    -0.35471794    -0.45718470     0.78834029
  19H1s       0.35459512     0.05570864    -0.87738569    -0.02167612     0.71217866     0.37110562    -1.46892220
  20H1s      -0.12374527     1.14843080     0.66300896     1.11237275    -0.21015399    -0.46196206     0.29500536
  21H1py     -0.01160227    -0.01036981    -0.01506969    -0.14307120     0.34445877    -0.08179831     0.69655815
  22H1pz     -0.01152725    -0.00685101    -0.06786469    -0.06700664    -0.20784379     0.45500240     0.42185114

          mcscf orbitals of the final iteration,  B1g block   4

               MO   23        MO   24
  23C1d2-     0.48444497    -0.59271976
  24H1px      0.19466564     0.55631469

          mcscf orbitals of the final iteration,  B1u block   5

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  25C1s       0.70855286    -0.01340781     0.04957164     0.06712526    -0.00265026     0.11792502     0.63736523     0.19741054
  26C1s       0.00733862     0.47983603    -0.17797654     0.02399408    -0.43511677     0.46080648     2.75322050     0.94911157
  27C1s      -0.00863951    -0.10291268    -1.46014848    -3.53706913    -0.27286196     0.40020848    -7.69384757     0.15726372
  28C1pz     -0.00041518     0.23643329    -0.11392983     0.23623729     0.26276425     0.99063643    -0.11844411    -0.50693321
  29C1pz      0.00125221    -0.04570261    -0.26761926     3.17972852     0.18443628    -2.58411140     2.19910640    -0.95389396
  30C1d0      0.00015912    -0.00357693     0.00085945    -0.01103128     0.02996459     0.08147676    -0.06643666     0.02893278
  31C1d2+    -0.00033814    -0.00302584     0.01225314     0.01809291    -0.09247722     0.05553872    -0.04158997     0.16300339
  32H1s      -0.00059218     0.33394434     0.03304199    -0.18414621     0.90536724    -0.14168963     0.41288788     0.46566414
  33H1s       0.00101711    -0.13267896     1.06271345    -0.60904854    -0.89405861     0.99460089     0.07369108    -0.03778478
  34H1py      0.00045712    -0.02080505    -0.01753765    -0.01815173     0.09108746    -0.13358612    -0.09325182    -0.06654849
  35H1pz      0.00025877    -0.00930595    -0.00528388    -0.00288432     0.05891838     0.00124859    -0.08301979     0.43804277

               MO   33        MO   34        MO   35
  25C1s      -0.15261112     0.44546851    -0.05184853
  26C1s      -0.38768118     2.08504832     0.12585198
  27C1s       4.46681380    -0.86057892     2.94599375
  28C1pz     -1.32117745    -0.42221342     1.15502950
  29C1pz     -1.63861053     0.60881006    -1.14096970
  30C1d0      0.23222081     0.26577348    -0.15668465
  31C1d2+    -0.24406196    -0.00407239    -0.57292515
  32H1s      -0.46012827    -0.44423080    -1.73308582
  33H1s       0.52651807    -0.08155227     0.60429476
  34H1py     -0.25663262     0.62928046     0.43108333
  35H1pz      0.06431219    -0.14738179     0.69593616

          mcscf orbitals of the final iteration,  B2g block   6

               MO   36        MO   37        MO   38        MO   39
  36C1px      0.54721127    -1.13513676    -0.40746041     0.03501410
  37C1px      0.46705047     1.67057417    -0.36222421     0.32643906
  38C1d1+     0.02218513    -0.08183134     1.00284503     0.56819168
  39H1px      0.01667897    -0.02662566     0.13533991    -0.59088408

          mcscf orbitals of the final iteration,  B3g block   7

               MO   40        MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  40C1py      0.44984201    -0.29584348    -1.02821525     0.25254009     0.10916849     1.19420809     0.86200186
  41C1py     -0.08398036    -1.49710054     2.57439494    -3.10809629    -2.17097898     0.00472679     0.25520307
  42C1d1-     0.03737217    -0.02945877     0.12792420     0.25520872    -0.05252463    -0.29906002     1.94531629
  43H1s       0.43044374    -0.02923889     0.21514072     1.12190930     0.91815107    -0.35986859    -1.99134944
  44H1s      -0.09564102     1.68862659    -1.38213846     0.34516886     0.18813390    -0.29172929     0.79500990
  45H1py     -0.00747523    -0.01458727     0.12306619    -0.03082209     0.34835783     0.39398100     0.77370936
  46H1pz     -0.01274156    -0.01608321     0.13342660     0.14373917    -0.33825644     0.49060157     0.08832708

          mcscf orbitals of the final iteration,  Au  block   8

               MO   47        MO   48
  47C1d2-     0.39770309    -0.87759076
  48H1px      0.30607526     0.54314409

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1C1s       0.70788933    -0.02139841    -0.00445993     0.03978782     0.09160582    -0.18670347     0.48649580    -0.13296809
   2C1s       0.00600225     0.60962970    -0.03502050    -0.14226958     0.39185918    -0.65534860     2.25072217    -0.46015835
   3C1s      -0.00553446    -0.16663435     0.04243219    -1.28943551     0.10702601     0.91808332    -3.59552652     1.22204018
   4C1pz     -0.00004246    -0.20231363     0.60192923    -0.19352377    -0.62688786     0.38174571     0.35345367     0.00901359
   5C1pz     -0.00030895     0.06578465    -0.14073760    -0.44189228     1.29717577    -0.22844129    -0.83816718     0.58187736
   6C1d0      0.00006073     0.00636984    -0.00639196    -0.00107099     0.01677427    -0.00879723     0.03610769    -0.00099537
   7C1d2+    -0.00012945    -0.00035029    -0.00596787     0.00645200     0.01716837     0.10412682     0.04066493    -0.31921732
   8H1s      -0.00027403     0.18125420     0.29016442     0.01903770    -0.37811175    -0.82719527    -0.05468547    -0.91342051
   9H1s       0.00121771    -0.08551015    -0.08211012     0.91534796     0.17218152     0.54855330     0.86273696     0.30584500
  10H1py      0.00026297    -0.01377100    -0.01401126    -0.00863256    -0.00501374    -0.08569005    -0.14857756    -0.00395447
  11H1pz      0.00005525    -0.01125497     0.00197165    -0.01003278     0.03158697    -0.02248075    -0.05462533    -0.16624308

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

   1C1s      -0.34787567    -0.18787601     0.38293648
   2C1s      -1.40321960    -0.64647177     1.80308386
   3C1s       1.08614681     1.04238126    -0.28476923
   4C1pz      0.37695852     0.54763407     0.82663027
   5C1pz     -0.86883403    -0.06101626    -0.14292941
   6C1d0      0.15037528     0.21503267    -0.07044747
   7C1d2+    -0.10872150     0.10726929    -0.29842678
   8H1s       0.03308655    -0.04576430    -1.23880549
   9H1s      -0.01789770    -0.21299018     0.36943014
  10H1py     -0.16246455     0.42241132     0.63212204
  11H1pz      0.28238223    -0.41738616     0.53919498

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.70582998     0.29411990     0.00005012     0.00000000

  12C1px      0.38366607    -1.10703281     0.37024835    -0.27254125
  13C1px      0.21611077     1.07220484    -0.24127834    -0.09046099
  14C1d1+    -0.01824913     0.09574701     0.58176284    -0.27863586
  15H1px      0.01073632    -0.01209827     0.03936698     0.58469177

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  16C1py      0.47042675    -0.33623126    -0.10178723     0.88114823     0.02157587    -0.13935325     1.61913040
  17C1py     -0.10000397    -0.61127984     0.44250248    -1.80447765    -0.61822228     0.27666055    -0.11218307
  18C1d1-    -0.00317254     0.01198784    -0.15544318    -0.24276668    -0.36421781    -0.45186373     0.78538279
  19H1s       0.35459512     0.06391349    -0.87257885    -0.01877057     0.72496886     0.36790339    -1.46603728
  20H1s      -0.12374527     1.13317721     0.67216819     1.11974619    -0.22278204    -0.46422292     0.29254431
  21H1py     -0.01160227    -0.00858344    -0.01504096    -0.14287527     0.34129532    -0.08990661     0.69718083
  22H1pz     -0.01152725    -0.00574440    -0.06618391    -0.06639990    -0.20054396     0.45772800     0.42281438

          natural orbitals of the final iteration, block  4

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  23C1d2-     0.48843407    -0.58943685
  24H1px      0.19090676     0.55761577

          natural orbitals of the final iteration, block  5

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  25C1s       0.70855021    -0.01354676     0.04577207     0.06532451    -0.00706299     0.11216039     0.64099320     0.19646681
  26C1s       0.00743272     0.47983458    -0.19757450     0.01427522    -0.45095652     0.44262241     2.76360708     0.94047449
  27C1s      -0.00865969    -0.10291099    -1.42665992    -3.49438473    -0.18677169     0.49192187    -7.71463814     0.15866022
  28C1pz     -0.00036881     0.23643336    -0.11112496     0.24714741     0.27551177     0.98728091    -0.11270068    -0.50186865
  29C1pz      0.00124325    -0.04570285    -0.26685911     3.14622704     0.12208611    -2.63132233     2.18554230    -0.95184596
  30C1d0      0.00015842    -0.00357697     0.00143454    -0.00961910     0.03119647     0.08147126    -0.06475419     0.02938470
  31C1d2+    -0.00033874    -0.00302578     0.01145335     0.01830640    -0.09242376     0.05752855    -0.04058522     0.16670062
  32H1s      -0.00052669     0.33394445     0.03372767    -0.17999009     0.90096740    -0.15217151     0.41813385     0.47524704
  33H1s       0.00099109    -0.13267916     1.05505962    -0.61038776    -0.88396443     1.00952588     0.08001278    -0.04647957
  34H1py      0.00045304    -0.02080514    -0.01636986    -0.01867379     0.08980306    -0.13380998    -0.09309378    -0.06304443
  35H1pz      0.00025695    -0.00930600    -0.00467416    -0.00176840     0.05757655     0.00071909    -0.08043823     0.43516880

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

  25C1s      -0.14924784     0.44427387    -0.04883204
  26C1s      -0.36931153     2.07665316     0.13822759
  27C1s       4.48447579    -0.87695170     2.91994849
  28C1pz     -1.31969261    -0.41622358     1.15957145
  29C1pz     -1.65374588     0.61956893    -1.13462022
  30C1d0      0.23313325     0.26512515    -0.15689944
  31C1d2+    -0.24575671    -0.00157686    -0.57104016
  32H1s      -0.46883447    -0.43838745    -1.73020662
  33H1s       0.53027992    -0.08689318     0.60100313
  34H1py     -0.25171349     0.62975205     0.43406924
  35H1pz      0.07026531    -0.15131694     0.69674295

          natural orbitals of the final iteration, block  6

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.70582998     0.29411990     0.00005012     0.00000000

  36C1px      1.21515145     0.37700956    -0.36779119     0.03501410
  37C1px     -1.57033695     0.72520748    -0.38506693     0.32643906
  38C1d1+     0.05873203    -0.01452020     1.00460262     0.56819168
  39H1px      0.02540347     0.00918128     0.13628783    -0.59088408

          natural orbitals of the final iteration, block  7

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  40C1py      0.44984201    -0.30118627    -1.02652698     0.25644349     0.10797519     1.18863264     0.86884069
  41C1py     -0.08398036    -1.48387061     2.56942185    -3.11507942    -2.17590617     0.00170052     0.25552213
  42C1d1-     0.03737217    -0.02683013     0.13260891     0.25828086    -0.05389425    -0.30723003     1.94332351
  43H1s       0.43044374    -0.02847916     0.21855269     1.11764707     0.92130727    -0.34749963    -1.99412284
  44H1s      -0.09564102     1.68096927    -1.38923618     0.35090092     0.18905229    -0.29766870     0.79393662
  45H1py     -0.00747523    -0.01322212     0.12351725    -0.03110639     0.34710392     0.39104164     0.77570280
  46H1pz     -0.01274156    -0.01496999     0.13298513     0.14281759    -0.33880859     0.49037169     0.08983101

          natural orbitals of the final iteration, block  8

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  47C1d2-     0.40220279    -0.87553766
  48H1px      0.30328310     0.54470811
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        298 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       1.999273   1.476691   0.013468   0.000000   0.000000   0.000000
    C1_ p       0.000131   0.202296   1.362603   0.000000   0.000000   0.000000
    C1_ d       0.000071   0.015832   0.015687   0.000000   0.000000   0.000000
    H1_ s       0.000679   0.278583   0.591386   0.000000   0.000000   0.000000
    H1_ p      -0.000153   0.026598   0.016857   0.000000   0.000000   0.000000

   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u
    C1_ p       0.694699   0.282650   0.000002   0.000000
    C1_ d       0.004808   0.011998   0.000046   0.000000
    H1_ p       0.006323  -0.000528   0.000002   0.000000

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u       5B2u       6B2u
    C1_ p       1.165733   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000142   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.815930   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.018195   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B2u

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       1.999921   0.762526   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.000377   0.387364   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000181   0.007790   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000196   0.805446   0.000000   0.000000   0.000000   0.000000
    H1_ p      -0.000314   0.036873   0.000000   0.000000   0.000000   0.000000

   ao class       7B1u       8B1u       9B1u      10B1u      11B1u

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g
    C1_ p       0.702737   0.293635   0.000000   0.000000
    C1_ d       0.003589  -0.002377   0.000044   0.000000
    H1_ p      -0.000496   0.002862   0.000006   0.000000

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g       5B3g       6B3g
    C1_ p       0.816159   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.052081   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.120003   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011757   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B3g

                        Au  partial gross atomic populations
   ao class       1Au        2Au 


                        gross atomic populations
     ao           C1_        H1_
      s         6.251879   3.612222
      p         5.908385   0.117982
      d         0.109531   0.000000
    total      12.269795   3.730205


 Total number of electrons:   16.00000000

 !timer: mcscf                           user+sys=     0.210 walltime=     1.000
