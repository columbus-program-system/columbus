

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        13107200 of real*8 words (  100.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,30
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,1,40,1,2,40,1,3,40,1,4,40,2,1,40,3,1,40,3,2,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /global/rene/tests/Columbus/test/SP-CALC/CH2-TRIP-SPIND/W
 ORK

 Integral file header information:
 Hermit Integral Program : SIFS version  n001              23:34:20.729 20-Jun-22

 Core type energy values:
 energy( 1)=  6.143886296433E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =    6.143886296


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  24

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    11    4    7    2


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       1(  1)    40
       1       2(  2)    40
       1       3(  3)    40
       1       4(  4)    40
       2       1( 12)    40
       3       1( 16)    40
       3       2( 17)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:    8
 Spin multiplicity:            3
 Number of active orbitals:    7
 Number of active electrons:   8
 Total number of CSFs:       148
 

 faar:   0 active-active rotations allowed out of:   7 possible.


 Number of active-double rotations:         0
 Number of active-active rotations:         0
 Number of double-virtual rotations:        0
 Number of active-virtual rotations:       41
 lenbfsdef=                131071  lenbfs=                    87
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         154
 class  2 (pq|ri):         #           0
 class  3 (pq|ia):         #           0
 class  4 (pi|qa):         #           0
 class  5 (pq|ra):         #         992
 class  6 (pq|ij)/(pi|qj): #           0
 class  7 (pq|ab):         #        1238
 class  8 (pa|qb):         #        2238
 class  9 p(bp,ai)         #           0
 class 10p(ai,jp):        #           0
 class 11p(ai,bj):        #           0

 Size of orbital-Hessian matrix B:                      968
 Size of the orbital-state Hessian matrix C:           6068
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           7036


 Source of the initial MO coeficients:

 Input MO coefficient file: /global/rene/tests/Columbus/test/SP-CALC/CH2-TRIP-SPIND/WORK
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     4
 ciiter=   9 noldhv= 15 noldv= 15

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9282301391      -45.0721164355        0.0000001816        0.0000010000
    2       -38.5814599226      -44.7253462191        0.0049723152        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.895256103956557E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.69006175 pnorm= 0.0000E+00 rznorm= 3.1184E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 *** warning *** small z0.
 
 i,qaaresolved                     1  -22.5164392944977     
 i,qaaresolved                     2  -1.71197239048657     
 i,qaaresolved                     3 -0.301062996492927     
 i,qaaresolved                     4  0.416074266240931     

 qvv(*) eigenvalues. symmetry block  1
     1.239664    1.459840    2.037276    2.677294    3.507120    4.513312    5.816984
 i,qaaresolved                     1 -0.217453566800958     

 qvv(*) eigenvalues. symmetry block  2
     1.429983    2.717978    3.798486
 i,qaaresolved                     1  -1.19665933676605     
 i,qaaresolved                     2  0.530041048397124     

 qvv(*) eigenvalues. symmetry block  3
     1.357638    1.825752    3.139292    4.190751    5.531575

 qvv(*) eigenvalues. symmetry block  4
    -5.950332   -2.827905
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=    -38.9282301391 demc= 3.8928E+01 wnorm= 3.1162E-02 knorm= 7.2375E-01 apxde= 1.0610E-02    *not conv.*     

               starting mcscf iteration...   2

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 12 noldv= 12

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9503086739      -45.0941949703        0.0000005643        0.0000010000
    2       -38.5444116518      -44.6882979483        0.0028565415        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.815758808641139E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.90266495 pnorm= 0.0000E+00 rznorm= 1.4140E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 i,qaaresolved                     1  -22.4994311413125     
 i,qaaresolved                     2  -1.70164963795626     
 i,qaaresolved                     3 -0.297568441227418     
 i,qaaresolved                     4  0.752374486198324     

 qvv(*) eigenvalues. symmetry block  1
     1.140478    1.406777    1.903720    2.678825    3.507601    4.507942    5.773538
 i,qaaresolved                     1 -0.212690825134312     

 qvv(*) eigenvalues. symmetry block  2
     1.430283    2.720108    3.798635
 i,qaaresolved                     1  -1.18754888808998     
 i,qaaresolved                     2  0.903277280871355     

 qvv(*) eigenvalues. symmetry block  3
     1.357723    1.554335    3.110652    4.188120    5.463749

 qvv(*) eigenvalues. symmetry block  4
    -5.949063   -2.827741
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=    -38.9503086739 demc= 2.2079E-02 wnorm= 4.6526E-02 knorm= 4.3034E-01 apxde= 7.2489E-03    *not conv.*     

               starting mcscf iteration...   3

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 14 noldv= 14

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9593013733      -45.1031876697        0.0000006659        0.0000010000
    2       -38.4491633618      -44.5930496582        0.0043326041        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.509339543468672E-003
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.98536136 pnorm= 0.0000E+00 rznorm= 5.6055E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 i,qaaresolved                     1  -22.4736115833738     
 i,qaaresolved                     2  -1.69021780038767     
 i,qaaresolved                     3 -0.288156811955467     
 i,qaaresolved                     4   1.07276498427604     

 qvv(*) eigenvalues. symmetry block  1
     0.924209    1.388886    1.860926    2.686610    3.516188    4.516857    5.762253
 i,qaaresolved                     1 -0.202397318732933     

 qvv(*) eigenvalues. symmetry block  2
     1.436276    2.728555    3.806478
 i,qaaresolved                     1  -1.17701867520889     
 i,qaaresolved                     2   1.36410509724104     

 qvv(*) eigenvalues. symmetry block  3
     1.147837    1.374073    3.101834    4.195404    5.437873

 qvv(*) eigenvalues. symmetry block  4
    -5.948637   -2.828471
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=    -38.9593013733 demc= 8.9927E-03 wnorm= 1.2075E-02 knorm= 1.7048E-01 apxde= 8.0539E-04    *not conv.*     

               starting mcscf iteration...   4

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 15 noldv= 15

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9602043509      -45.1040906473        0.0000005220        0.0000010000
    2       -38.4401943827      -44.5840806791        0.0031446399        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  6.004641279989511E-004
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99946418 pnorm= 0.0000E+00 rznorm= 5.5799E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 i,qaaresolved                     1  -22.4693015357330     
 i,qaaresolved                     2  -1.68775922458036     
 i,qaaresolved                     3 -0.285595102499701     
 i,qaaresolved                     4   1.17081095892282     

 qvv(*) eigenvalues. symmetry block  1
     0.832692    1.392226    1.867432    2.688918    3.518712    4.519227    5.758089
 i,qaaresolved                     1 -0.199902105829777     

 qvv(*) eigenvalues. symmetry block  2
     1.438670    2.731247    3.809322
 i,qaaresolved                     1  -1.17466741116953     
 i,qaaresolved                     2   1.54844341824973     

 qvv(*) eigenvalues. symmetry block  3
     0.973376    1.382844    3.106251    4.199930    5.424928

 qvv(*) eigenvalues. symmetry block  4
    -5.948753   -2.828661
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    4 emc=    -38.9602043509 demc= 9.0298E-04 wnorm= 4.8037E-03 knorm= 3.2732E-02 apxde= 2.4875E-05    *not conv.*     

               starting mcscf iteration...   5

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 14 noldv= 14

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9602337435      -45.1041200399        0.0000005543        0.0000010000
    2       -38.4405779614      -44.5844642579        0.0017537274        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.802188606939933E-004
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99997881 pnorm= 0.0000E+00 rznorm= 1.5008E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 i,qaaresolved                     1  -22.4698365516389     
 i,qaaresolved                     2  -1.68770609801379     
 i,qaaresolved                     3 -0.285479824444126     
 i,qaaresolved                     4   1.18232882552291     

 qvv(*) eigenvalues. symmetry block  1
     0.818554    1.394414    1.871386    2.689162    3.519089    4.518883    5.756077
 i,qaaresolved                     1 -0.199847440757350     

 qvv(*) eigenvalues. symmetry block  2
     1.439106    2.731538    3.809577
 i,qaaresolved                     1  -1.17462855464583     
 i,qaaresolved                     2   1.58019150423629     

 qvv(*) eigenvalues. symmetry block  3
     0.943887    1.386485    3.108086    4.200840    5.417308

 qvv(*) eigenvalues. symmetry block  4
    -5.948858   -2.828633
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    5 emc=    -38.9602337435 demc= 2.9393E-05 wnorm= 1.4418E-03 knorm= 6.5103E-03 apxde= 1.0970E-06    *not conv.*     

               starting mcscf iteration...   6
 !timer:                                 cpu_time=     0.149 walltime=     0.537

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 14 noldv= 14

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9602351256      -45.1041214220        0.0000005571        0.0000010000
    2       -38.4407981761      -44.5846844726        0.0017471395        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.126488781594258E-005
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999863 pnorm= 0.0000E+00 rznorm= 4.8682E-07 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0
 
 i,qaaresolved                     1  -22.4701168824431     
 i,qaaresolved                     2  -1.68774637821150     
 i,qaaresolved                     3 -0.285515786174531     
 i,qaaresolved                     4   1.18338289244407     

 qvv(*) eigenvalues. symmetry block  1
     0.815977    1.395217    1.872504    2.689210    3.519170    4.518709    5.755835
 i,qaaresolved                     1 -0.199893688977666     

 qvv(*) eigenvalues. symmetry block  2
     1.439189    2.731568    3.809590
 i,qaaresolved                     1  -1.17466081610496     
 i,qaaresolved                     2   1.58422693639340     

 qvv(*) eigenvalues. symmetry block  3
     0.939561    1.387430    3.108681    4.201044    5.415813

 qvv(*) eigenvalues. symmetry block  4
    -5.948887   -2.828619
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    6 emc=    -38.9602351256 demc= 1.3821E-06 wnorm= 4.1012E-04 knorm= 1.6564E-03 apxde= 8.3535E-08    *not conv.*     

               starting mcscf iteration...   7
 !timer:                                 cpu_time=     0.180 walltime=     0.636

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 14 noldv= 14

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9602352348      -45.1041215313        0.0000005559        0.0000010000
    2       -38.4408821671      -44.5847684635        0.0016908657        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.501096779827460E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999973 pnorm= 3.0577E-04 rznorm= 6.1116E-07 rpnorm= 2.0464E-07 noldr=  6 nnewr=  6 nolds=  6 nnews=  6
 
 i,qaaresolved                     1  -22.4702001947200     
 i,qaaresolved                     2  -1.68776015884270     
 i,qaaresolved                     3 -0.285530293537313     
 i,qaaresolved                     4   1.18338240595305     

 qvv(*) eigenvalues. symmetry block  1
     0.815386    1.395489    1.872821    2.689227    3.519193    4.518656    5.755849
 i,qaaresolved                     1 -0.199909842589099     

 qvv(*) eigenvalues. symmetry block  2
     1.439207    2.731573    3.809589
 i,qaaresolved                     1  -1.17467007487227     
 i,qaaresolved                     2   1.58463988977119     

 qvv(*) eigenvalues. symmetry block  3
     0.938793    1.387678    3.108880    4.201104    5.415627

 qvv(*) eigenvalues. symmetry block  4
    -5.948894   -2.828614
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    7 emc=    -38.9602352348 demc= 1.0925E-07 wnorm= 1.2009E-04 knorm= 7.3180E-04 apxde= 1.2132E-08    *not conv.*     

               starting mcscf iteration...   8
 !timer:                                 cpu_time=     0.219 walltime=     0.749

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 14 noldv= 14

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9602352470      -45.1041215434        0.0000004384        0.0000010000
    2       -38.4409188960      -44.5848051924        0.0038551551        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  7.060905876858895E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 6.5621E-07 rpnorm= 2.5240E-08 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 
 i,qaaresolved                     1  -22.4702325957813     
 i,qaaresolved                     2  -1.68776526883480     
 i,qaaresolved                     3 -0.285535999644726     
 i,qaaresolved                     4   1.18329611499415     

 qvv(*) eigenvalues. symmetry block  1
     0.815160    1.395626    1.872964    2.689238    3.519204    4.518634    5.755881
 i,qaaresolved                     1 -0.199915893371742     

 qvv(*) eigenvalues. symmetry block  2
     1.439213    2.731575    3.809588
 i,qaaresolved                     1  -1.17467252271414     
 i,qaaresolved                     2   1.58462638700291     

 qvv(*) eigenvalues. symmetry block  3
     0.938561    1.387785    3.108979    4.201133    5.415621

 qvv(*) eigenvalues. symmetry block  4
    -5.948897   -2.828612
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    8 emc=    -38.9602352470 demc= 1.2137E-08 wnorm= 5.6487E-07 knorm= 3.6460E-08 apxde= 1.0352E-14    *not conv.*     

               starting mcscf iteration...   9
 !timer:                                 cpu_time=     0.250 walltime=     0.851

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13104807

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12938952
 address segment size,           sizesg =  12787466
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       4335 transformed 1/r12    array elements were written in       1 records.


 mosort: allocated sort2 space, avc2is=    12966660 available sort2 space, avcisx=    12966912

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 14 noldv= 14

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -38.9602352470      -45.1041215434        0.0000005549        0.0000010000
    2       -38.4409258611      -44.5848121575        0.0016491244        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  8.371570266847590E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 3.7001E-07 rpnorm= 1.0337E-08 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 
 i,qaaresolved                     1  -22.4702325813479     
 i,qaaresolved                     2  -1.68776527963441     
 i,qaaresolved                     3 -0.285536002871027     
 i,qaaresolved                     4   1.18329610730573     

 qvv(*) eigenvalues. symmetry block  1
     0.815160    1.395626    1.872964    2.689238    3.519204    4.518634    5.755881
 i,qaaresolved                     1 -0.199915898592069     

 qvv(*) eigenvalues. symmetry block  2
     1.439213    2.731575    3.809588
 i,qaaresolved                     1  -1.17467251850756     
 i,qaaresolved                     2   1.58462638170137     

 qvv(*) eigenvalues. symmetry block  3
     0.938561    1.387785    3.108979    4.201133    5.415621

 qvv(*) eigenvalues. symmetry block  4
    -5.948897   -2.828612
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    9 emc=    -38.9602352470 demc=-7.8160E-14 wnorm= 6.6973E-07 knorm= 1.8271E-08 apxde= 8.0666E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 1.000 total energy=      -38.960235247, rel. (eV)=   0.000000
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     1.99998256     1.98054614     0.99991610     0.02487996     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  B1 
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     1.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -  B2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     1.97693539     0.01773985     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  A2 
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        153 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     1
 Number of unique bra states (ndbra):                     1
 *** Starting rdft_grd
 qind: F
  
 Spin density matrix:

              < 1|SD| 1>            block   1

                mo   1         mo   2         mo   3         mo   4
   mo   1     0.00000464
   mo   2     0.00015364     0.01051648
   mo   3     0.00090644     0.07340780     0.99021593
   mo   4    -0.00183829    -0.07215579     0.02161880     0.00302577

              < 1|SD| 1>            block   2

                mo   5
   mo   5     0.99310703

              < 1|SD| 1>            block   3

                mo   6         mo   7
   mo   6     0.00095429
   mo   7    -0.04826947     0.00217586

              < 1|SD| 1>            block   4
  
 Alpha density matrix:

              < 1|alpha| 1>         block   1

                mo   1         mo   2         mo   3         mo   4
   mo   1     0.99999217
   mo   2    -0.00005385     0.99322081
   mo   3     0.00002603     0.00317484     0.99728843
   mo   4    -0.00087492    -0.03281063     0.01689313     0.01404238

              < 1|alpha| 1>         block   2

                mo   5
   mo   5     0.99655351

              < 1|alpha| 1>         block   3

                mo   6         mo   7
   mo   6     0.98894477
   mo   7    -0.02440240     0.00995793

              < 1|alpha| 1>         block   4
  
  
 Beta density matrix:

              < 1|beta| 1>          block   1

                mo   1         mo   2         mo   3         mo   4
   mo   1     0.99998753
   mo   2    -0.00020750     0.98270433
   mo   3    -0.00088041    -0.07023296     0.00707249
   mo   4     0.00096337     0.03934516    -0.00472568     0.01101661

              < 1|beta| 1>          block   2

                mo   5
   mo   5     0.00344649

              < 1|beta| 1>          block   3

                mo   6         mo   7
   mo   6     0.98799047
   mo   7     0.02386707     0.00778207

              < 1|beta| 1>          block   4
  
  
 One particle density matrix:

              < 1|E_s| 1>           block   1

                mo   1         mo   2         mo   3         mo   4
   mo   1     1.99997970
   mo   2    -0.00026135     1.97592514
   mo   3    -0.00085438    -0.06705812     1.00436092
   mo   4     0.00008846     0.00653453     0.01216745     0.02505900

              < 1|E_s| 1>           block   2

                mo   5
   mo   5     1.00000000

              < 1|E_s| 1>           block   3

                mo   6         mo   7
   mo   6     1.97693524
   mo   7    -0.00053532     0.01774000

              < 1|E_s| 1>           block   4
  
 *** rdft_grd finished
 d1(*) and spind(*) written to the 1-particle density matrix file.
        153 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           

          state spec. NOs: DRT 1, State  1
 *** warning *** small active-orbital occupation. i=  1 nocc=-6.9681E-02
 *** warning *** small active-orbital occupation. i=  2 nocc= 2.6749E-06
 *** warning *** small active-orbital occupation. i=  1 nocc=-4.6708E-02

          state spec. NOs: DRT 1, State  1

          state spec. NOs: DRT 1, State  1

          state spec. NOs: DRT 1, State  1
 *** warning *** large active-orbital occupation. i=  4 nocc= 2.0000E+00

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     1.99998256     1.98054614     0.99991610     0.02487996     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     1.00000000     0.00000000     0.00000000     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     1.97693539     0.01773985     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       2.000734   1.118064   0.170084   0.007073   0.000000   0.000000
    C1_ p       0.000003   0.085541   0.792609   0.003546   0.000000   0.000000
    C1_ d      -0.000003   0.002567   0.001908   0.000603   0.000000   0.000000
    H1_ s      -0.000344   0.744461   0.028769   0.013592   0.000000   0.000000
    H1_ p      -0.000408   0.029913   0.006546   0.000066   0.000000   0.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1 
    C1_ p       0.988391   0.000000   0.000000   0.000000
    C1_ d      -0.000012   0.000000   0.000000   0.000000
    H1_ p       0.011621   0.000000   0.000000   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ p       0.948885   0.009631   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.009333  -0.000266   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.007122   0.008236   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011594   0.000139   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2 


                        gross atomic populations
     ao           C1_        H1_
      s         3.295956   1.801836
      p         2.828607   0.059472
      d         0.014130   0.000000
    total       6.138692   1.861308
 

 Trace:    8.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       2.000734   1.118064   0.170084   0.007073   0.000000   0.000000
    C1_ p       0.000003   0.085541   0.792609   0.003546   0.000000   0.000000
    C1_ d      -0.000003   0.002567   0.001908   0.000603   0.000000   0.000000
    H1_ s      -0.000344   0.744461   0.028769   0.013592   0.000000   0.000000
    H1_ p      -0.000408   0.029913   0.006546   0.000066   0.000000   0.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1 
    C1_ p       0.988391   0.000000   0.000000   0.000000
    C1_ d      -0.000012   0.000000   0.000000   0.000000
    H1_ p       0.011621   0.000000   0.000000   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ p       0.948885   0.009631   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.009333  -0.000266   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.007122   0.008236   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011594   0.000139   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2 


                        gross atomic populations
     ao           C1_        H1_
      s         3.295956   1.801836
      p         2.828607   0.059472
      d         0.014130   0.000000
    total       6.138692   1.861308
 

 Trace:    8.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population based on the Spin-Density for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       0.171086   0.074241   0.000003   0.007767   0.000000   0.000000
    C1_ p       0.790119   0.014978   0.000000   0.000575   0.000000   0.000000
    C1_ d       0.001829   0.000506  -0.000000  -0.001329   0.000000   0.000000
    H1_ s       0.026355  -0.013818  -0.000000  -0.076876   0.000000   0.000000
    H1_ p       0.006563   0.001582  -0.000000   0.000182   0.000000   0.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1 
    C1_ p       0.981578   0.000000   0.000000   0.000000
    C1_ d      -0.000012   0.000000   0.000000   0.000000
    H1_ p       0.011541   0.000000   0.000000   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ p       0.056399   0.005080   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000216   0.000278   0.000000   0.000000   0.000000   0.000000
    H1_ s      -0.007127  -0.052160   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.000783   0.000094   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2 


                        gross atomic populations
     ao           C1_        H1_
      s         0.253098  -0.123626
      p         1.848729   0.020745
      d         0.001056   0.000000
    total       2.102882  -0.102882
 

 Trace:    2.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population based on the Alpha Spin-Density for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.000223   0.514697   0.256501   0.003106   0.000000   0.000000
    C1_ p       0.000000   0.501292   0.340706   0.001688   0.000000   0.000000
    C1_ d      -0.000001   0.000239   0.002490   0.000312   0.000000   0.000000
    H1_ s      -0.000056  -0.025031   0.378634   0.007521   0.000000   0.000000
    H1_ p      -0.000173   0.007829   0.014543   0.000023   0.000000   0.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1 
    C1_ p       0.984984   0.000000   0.000000   0.000000
    C1_ d      -0.000012   0.000000   0.000000   0.000000
    H1_ p       0.011581   0.000000   0.000000   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ p       0.505207   0.004790   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.004705  -0.000141   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.473404   0.004631   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.006236   0.000069   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2 


                        gross atomic populations
     ao           C1_        H1_
      s         1.774527   0.839105
      p         2.338668   0.040108
      d         0.007593   0.000000
    total       4.120787   0.879213
 

 Trace:    5.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population based on the Beta Spin-Density for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.000287   0.516902   0.004114   0.000126   0.000000   0.000000
    C1_ p       0.000002   0.036326   0.000235   0.001451   0.000000   0.000000
    C1_ d      -0.000001   0.001781   0.000255  -0.000000   0.000000   0.000000
    H1_ s      -0.000097   0.420253   0.005264  -0.000011   0.000000   0.000000
    H1_ p      -0.000200   0.014073   0.000008   0.000015   0.000000   0.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1 
    C1_ p       0.003406   0.000000   0.000000   0.000000
    C1_ d      -0.000000   0.000000   0.000000   0.000000
    H1_ p       0.000040   0.000000   0.000000   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    C1_ p       0.444390   0.004129   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.004610  -0.000108   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.534202   0.003121   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.005369   0.000059   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2 


                        gross atomic populations
     ao           C1_        H1_
      s         1.521429   0.962731
      p         0.489939   0.019363
      d         0.006537   0.000000
    total       2.017905   0.982095
 

 Trace:    3.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 !timer: mcscf                           cpu_time=     0.289 walltime=     0.971
 *** cpu_time / walltime =      0.298
 bummer (warning):timer: cpu_time << walltime.  If possible, increase core memory. event =1
