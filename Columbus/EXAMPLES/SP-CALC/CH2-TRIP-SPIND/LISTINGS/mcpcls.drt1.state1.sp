

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  n001              23:34:20.729 20-Jun-22
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym2
 Total number of electrons:    8
 Spin multiplicity:            3
 Number of active orbitals:    7
 Number of active electrons:   8
 Total number of CSFs:       148

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    b1    b2    a2 

 List of active orbitals:
  1 a1   2 a1   3 a1   4 a1   1 b1   1 b2   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -38.9602352470    nuclear repulsion=     6.1438862964
 demc=            -0.0000000000    wnorm=                 0.0000006697
 knorm=            0.0000000183    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    b1     1   1.0000

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  1 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  a1  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  1 a1   2 a1   3 a1   4 a1   1 b1   1 b2   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      4  0.9845357743  0.9693106908  3310130
     15 -0.0704250010  0.0049596808  3130130
     29  0.0605841158  0.0036704351  3111230
      7 -0.0586054436  0.0034345980  3310103
      2  0.0522909872  0.0027343473  3313100
     46  0.0490965280  0.0024104691  3013130
     26 -0.0402536853  0.0016203592  3112121
     33 -0.0399119292  0.0015929621  3111122
      5 -0.0334966257  0.0011220239  3310121
     22  0.0322765373  0.0010417749  3121112
     20  0.0322160068  0.0010378711  3121130
     35  0.0309173513  0.0009558826  3103130
     27 -0.0297800040  0.0008868486  3112112
     16  0.0297643204  0.0008859148  3130121
      6  0.0255253887  0.0006515455  3310112
     25  0.0238130959  0.0005670635  3112130
     11  0.0229351788  0.0005260224  3301112
     50 -0.0226177000  0.0005115604  3010133
      1 -0.0211586136  0.0004476869  3331100
      9  0.0163143321  0.0002661574  3301130
     30 -0.0148314952  0.0002199733  3111221
      8  0.0145912732  0.0002129053  3301211
     10 -0.0140870563  0.0001984452  3301121
     19  0.0122369417  0.0001497427  3121211
     17  0.0103327380  0.0001067655  3130112
     31  0.0088299465  0.0000779680  3111212
     14 -0.0083761930  0.0000701606  3130211
     24  0.0076215518  0.0000580881  3112211
     49 -0.0072872601  0.0000531042  3013103
     18  0.0069725555  0.0000486165  3130103
      3 -0.0060342810  0.0000364125  3310211
     32 -0.0047318247  0.0000223902  3111203
     47 -0.0042952746  0.0000184494  3013121
     36 -0.0039892920  0.0000159145  3103121
     28 -0.0036644136  0.0000134279  3112103
     42  0.0033700596  0.0000113573  3031121
     38 -0.0033679513  0.0000113431  3103103
     43 -0.0028262210  0.0000079875  3031112
     44  0.0022456867  0.0000050431  3031103
     89  0.0019270658  0.0000037136  1210133
     40 -0.0017313475  0.0000029976  3031211
     48  0.0016704106  0.0000027903  3013112
     68 -0.0014569280  0.0000021226  1311230
    135 -0.0014284362  0.0000020404  0310133
     41 -0.0013590624  0.0000018471  3031130
    131  0.0012789567  0.0000016357  0313130
     80 -0.0011620171  0.0000013503  1231130
     51 -0.0011447216  0.0000013104  3001133
     23 -0.0011194843  0.0000012532  3121103
     59 -0.0010896619  0.0000011874  1321130

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: