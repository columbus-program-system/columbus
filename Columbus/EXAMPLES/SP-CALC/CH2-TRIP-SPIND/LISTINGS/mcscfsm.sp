 total ao core energy =    6.143886296
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:     8
 Spin multiplicity:            3
 Number of active orbitals:    7
 Number of active electrons:   8
 Total number of CSFs:       148

 Number of active-double rotations:         0
 Number of active-active rotations:         0
 Number of double-virtual rotations:        0
 Number of active-virtual rotations:       41
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1    -38.9282301391  3.893E+01  3.116E-02  7.238E-01  1.061E-02  F   *not conv.*     
    2    -38.9503086739  2.208E-02  4.653E-02  4.303E-01  7.249E-03  F   *not conv.*     
    3    -38.9593013733  8.993E-03  1.207E-02  1.705E-01  8.054E-04  F   *not conv.*     
    4    -38.9602043509  9.030E-04  4.804E-03  3.273E-02  2.488E-05  F   *not conv.*     
    5    -38.9602337435  2.939E-05  1.442E-03  6.510E-03  1.097E-06  F   *not conv.*     
    6    -38.9602351256  1.382E-06  4.101E-04  1.656E-03  8.354E-08  F   *not conv.*     
    7    -38.9602352348  1.092E-07  1.201E-04  7.318E-04  1.213E-08  T   *not conv.*     
    8    -38.9602352470  1.214E-08  5.649E-07  3.646E-08  1.035E-14  T   *not conv.*     

 final mcscf convergence values:
    9    -38.9602352470 -7.816E-14  6.697E-07  1.827E-08  8.067E-15  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 1.000 total energy=      -38.960235247, rel. (eV)=   0.000000
   ------------------------------------------------------------


