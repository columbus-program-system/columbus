
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /global/rene/tests/Columbus/test/SP-CALC/CH2-TRIP-SPIND/WORK/mcdrtky            
 
 input the spin multiplicity [  0]: spin multiplicity:    3    triplet 
 input the total number of electrons [  0]: nelt:      8
 input the number of irreps (1-8) [  0]: nsym:      4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      2
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:      0
 number of inactive electrons:      0
 number of active electrons:      8
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      7
 level(*)        1   2   3   4   5   6   7
 syml(*)         1   1   1   1   2   3   3
 slabel(*)     a1  a1  a1  a1  b1  b2  b2 
 modrt(*)        1   2   3   4   1   1   2
 input the minimum cumulative occupation for each active level:
  a1  a1  a1  a1  b1  b2  b2 
    1   2   3   4   1   1   2
 input the maximum cumulative occupation for each active level:
  a1  a1  a1  a1  b1  b2  b2 
    1   2   3   4   1   1   2
 slabel(*)     a1  a1  a1  a1  b1  b2  b2 
 modrt(*)        1   2   3   4   1   1   2
 occmin(*)       0   0   0   0   0   0   8
 occmax(*)       8   8   8   8   8   8   8
 input the minimum b value for each active level:
  a1  a1  a1  a1  b1  b2  b2 
    1   2   3   4   1   1   2
 input the maximum b value for each active level:
  a1  a1  a1  a1  b1  b2  b2 
    1   2   3   4   1   1   2
 slabel(*)     a1  a1  a1  a1  b1  b2  b2 
 modrt(*)        1   2   3   4   1   1   2
 bmin(*)         0   0   0   0   0   0   0
 bmax(*)         8   8   8   8   8   8   8
 input the step masks for each active level:
 modrt:smask=
   1:1111   2:1111   3:1111   4:1111   1:1111   1:1111   2:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     44
 are any arcs to be manually removed?(y,[n])
 nwalk=     148
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   7

 level  0 through level  7 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  44   7 3 2   3 b2    2    0   0   0   0   1     1     0     0     0   148     0
                                            2     0     0     0     0   142     0
                                            3     0     0     0     0   146     0
                                            4     0     0     0     0   152     0
 ........................................

  40   6 3 2   3 b2    1   44   0   0   0   1     1     0     0     0    28     0
                                            2     0     0     0     0    27     0
                                            3     0     0     0     0    22     0
                                            4     0     0     0     0    28     0

  41   6 3 1   3 b2    1    0  44   0   0   1     0     0     0     0    51     0
                                            2     0     0     0     0    64     0
                                            3     1     1     0     0    44    28
                                            4     0     0     0     0    51     0

  42   6 2 3   3 b2    1    0   0  44   0   1     0     0     0     0    22     0
                                            2     0     0     0     0    12     0
                                            3     1     1     1     0    28    72
                                            4     0     0     0     0    22     0

  43   6 2 2   3 b2    1    0   0   0  44   1     1     1     1     1    48   100
                                            2     0     0     0     0    42     0
                                            3     0     0     0     0    51     0
                                            4     0     0     0     0    48     0
 ........................................

  31   5 3 2   2 b1    1   40   0   0   0   1     1     0     0     0     4     0
                                            2     0     0     0     0     6     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  32   5 3 1   2 b1    1   41  40   0   0   1     0     0     0     0    16     0
                                            2     0     0     0     0    24     0
                                            3     2     1     0     0     0     4
                                            4     0     0     0     0     0     0

  33   5 3 0   2 b1    1    0  41   0   0   1     1     1     0     0    20    28
                                            2     0     0     0     0    30     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  34   5 2 3   2 b1    1   42   0  40   0   1     0     0     0     0     6     0
                                            2     0     0     0     0     4     0
                                            3     2     1     1     0     0     4
                                            4     0     0     0     0     0     0

  35   5 2 2   2 b1    1   43  42  41  40   1     4     3     2     1    24     4
                                            2     0     0     0     0    21     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  36   5 2 1   2 b1    1    0  43   0  41   1     0     0     0     0    35     0
                                            2     0     0     0     0    40     0
                                            3     2     2     1     1     0    72
                                            4     0     0     0     0     0     0

  37   5 1 4   2 b1    1    0   0  42   0   1     1     1     1     0     4    96
                                            2     0     0     0     0     1     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  38   5 1 3   2 b1    1    0   0  43  42   1     0     0     0     0    16     0
                                            2     0     0     0     0     8     0
                                            3     2     2     2     1     0   100
                                            4     0     0     0     0     0     0

  39   5 1 2   2 b1    1    0   0   0  43   1     1     1     1     1    24   124
                                            2     0     0     0     0    21     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

  20   4 3 1   1 a1    4   32  31   0   0   1     0     0     0     0     0     0
                                            2     1     1     0     0     4     0
                                            3     2     0     0     0     0     4
                                            4     0     0     0     0     0     0

  21   4 3 0   1 a1    4   33  32   0   0   1     1     0     0     0     0    28
                                            2     0     0     0     0    10     0
                                            3     0     0     0     0     0     0
                                            4     2     2     0     0     0     4

  22   4 2 2   1 a1    4   35  34  32  31   1     5     1     1     1     0     4
                                            2     0     0     0     0     6     0
                                            3     0     0     0     0     0     0
                                            4     4     4     2     0     0     4

  23   4 2 1   1 a1    4   36  35  33  32   1     0     0     0     0     0     0
                                            2     5     5     1     0    20    28
                                            3     4     2     2     2     0     4
                                            4     0     0     0     0     0     0

  24   4 2 0   1 a1    4    0  36   0  33   1     1     1     1     1     0    48
                                            2     0     0     0     0    20     0
                                            3     0     0     0     0     0     0
                                            4     2     2     0     0     0    72

  25   4 1 3   1 a1    4   38  37  35  34   1     0     0     0     0     0     0
                                            2     5     5     4     0     4    24
                                            3     4     2     2     2     0     4
                                            4     0     0     0     0     0     0

  26   4 1 2   1 a1    4   39  38  36  35   1     5     4     4     4     0    28
                                            2     0     0     0     0    15     0
                                            3     0     0     0     0     0     0
                                            4     4     4     2     0     0    72

  27   4 1 1   1 a1    4    0  39   0  36   1     0     0     0     0     0     0
                                            2     1     1     0     0    20   124
                                            3     2     2     2     2     0    72
                                            4     0     0     0     0     0     0

  28   4 0 4   1 a1    4    0   0  38  37   1     1     1     1     1     0   100
                                            2     0     0     0     0     1     0
                                            3     0     0     0     0     0     0
                                            4     2     2     2     0     0   100

  29   4 0 3   1 a1    4    0   0  39  38   1     0     0     0     0     0     0
                                            2     1     1     1     0     4   144
                                            3     2     2     2     2     0   100
                                            4     0     0     0     0     0     0

  30   4 0 2   1 a1    4    0   0   0  39   1     1     1     1     1     0   148
                                            2     0     0     0     0     6     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
 ........................................

  11   3 3 0   1 a1    3   21  20   0   0   1     1     0     0     0     0    28
                                            2     1     1     0     0     1     0
                                            3     2     2     0     0     0     4
                                            4     2     0     0     0     0     4

  12   3 2 1   1 a1    3   23  22  21  20   1     6     6     1     0     0    28
                                            2     6     1     1     1     3     1
                                            3     6     2     2     2     0     4
                                            4     6     6     2     0     0     4

  13   3 2 0   1 a1    3   24  23   0  21   1     2     1     1     1     0    28
                                            2     5     5     0     0     6    31
                                            3     4     4     0     0     0     4
                                            4     4     2     2     2     0     4

  14   3 1 2   1 a1    3   26  25  23  22   1    10     5     5     5     0     4
                                            2    10    10     5     0     3    37
                                            3     8     8     4     0     0     4
                                            4     8     4     4     4     0     4

  15   3 1 1   1 a1    3   27  26  24  23   1     6     6     1     0     0    48
                                            2     6     5     5     5     8    40
                                            3     6     4     4     4     0     4
                                            4     6     6     2     0     0    72

  16   3 1 0   1 a1    3    0  27   0  24   1     1     1     1     1     0    48
                                            2     1     1     0     0     6   132
                                            3     2     2     0     0     0    72
                                            4     2     2     2     2     0    72

  17   3 0 3   1 a1    3   29  28  26  25   1     6     6     5     0     0    28
                                            2     6     5     5     5     1    27
                                            3     6     4     4     4     0     4
                                            4     6     6     4     0     0    72

  18   3 0 2   1 a1    3   30  29  27  26   1     6     5     5     5     0    28
                                            2     2     2     1     0     3   138
                                            3     4     4     2     0     0    72
                                            4     4     4     4     4     0    72

  19   3 0 1   1 a1    3    0  30   0  27   1     1     1     0     0     0   148
                                            2     1     1     1     1     3   141
                                            3     2     2     2     2     0    72
                                            4     0     0     0     0     0     0
 ........................................

   5   2 2 0   1 a1    2   13  12   0  11   1     9     7     1     1     0    28
                                            2    12     7     1     1     1     0
                                            3    12     8     2     2     0     4
                                            4    12     8     2     2     0     4

   6   2 1 1   1 a1    2   15  14  13  12   1    24    18     8     6     0    28
                                            2    27    21    11     6     2     2
                                            3    24    18    10     6     0     4
                                            4    24    18    10     6     0     4

   7   2 1 0   1 a1    2   16  15   0  13   1     9     8     2     2     0    28
                                            2    12    11     5     5     3    34
                                            3    12    10     4     4     0     4
                                            4    12    10     4     4     0     4

   8   2 0 2   1 a1    2   18  17  15  14   1    28    22    16    10     0     4
                                            2    24    22    16    10     1    39
                                            3    24    20    14     8     0     4
                                            4    24    20    14     8     0     4

   9   2 0 1   1 a1    2   19  18  16  15   1    14    13     7     6     0    48
                                            2    10     9     7     6     2    46
                                            3    14    12     8     6     0     4
                                            4    12    12     8     6     0    72

  10   2 0 0   1 a1    2    0  19   0  16   1     2     2     1     1     0    48
                                            2     2     2     1     1     1   137
                                            3     4     4     2     2     0    72
                                            4     2     2     2     2     0    72
 ........................................

   2   1 1 0   1 a1    1    7   6   0   5   1    42    33     9     9     0    28
                                            2    51    39    12    12     1     0
                                            3    48    36    12    12     0     4
                                            4    48    36    12    12     0     4

   3   1 0 1   1 a1    1    9   8   7   6   1    75    61    33    24     0    28
                                            2    73    63    39    27     1     3
                                            3    74    60    36    24     0     4
                                            4    72    60    36    24     0     4

   4   1 0 0   1 a1    1   10   9   0   7   1    25    23     9     9     0    28
                                            2    24    22    12    12     1    36
                                            3    30    26    12    12     0     4
                                            4    26    24    12    12     0     4
 ........................................

   1   0 0 0   0       0    4   3   0   2   1   142   117    42    42     0    28
                                            2   148   124    51    51     1     0
                                            3   152   122    48    48     0     4
                                            4   146   120    48    48     0     4
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=     148
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
      148 csfs selected from     148 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
      148 csfs selected from     148 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
      148 csfs selected from     148 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /global/rene/tests/Columbus/test/SP-CALC/CH2-TRIP-SPIND/WORK/mcdrtfl            
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 3 3 1 1 0 0
   CSF#     2    3 3 1 3 1 0 0
   CSF#     3    3 3 1 0 2 1 1
   CSF#     4    3 3 1 0 1 3 0
   CSF#     5    3 3 1 0 1 2 1
   CSF#     6    3 3 1 0 1 1 2
   CSF#     7    3 3 1 0 1 0 3
   CSF#     8    3 3 0 1 2 1 1
   CSF#     9    3 3 0 1 1 3 0
   CSF#    10    3 3 0 1 1 2 1
   CSF#    11    3 3 0 1 1 1 2
   CSF#    12    3 3 0 1 1 0 3
   CSF#    13    3 1 3 3 1 0 0
   CSF#    14    3 1 3 0 2 1 1
   CSF#    15    3 1 3 0 1 3 0
   CSF#    16    3 1 3 0 1 2 1
   CSF#    17    3 1 3 0 1 1 2
   CSF#    18    3 1 3 0 1 0 3
   CSF#    19    3 1 2 1 2 1 1
   CSF#    20    3 1 2 1 1 3 0
   CSF#    21    3 1 2 1 1 2 1
   CSF#    22    3 1 2 1 1 1 2
   CSF#    23    3 1 2 1 1 0 3
   CSF#    24    3 1 1 2 2 1 1
   CSF#    25    3 1 1 2 1 3 0
   CSF#    26    3 1 1 2 1 2 1
   CSF#    27    3 1 1 2 1 1 2
   CSF#    28    3 1 1 2 1 0 3
   CSF#    29    3 1 1 1 2 3 0
   CSF#    30    3 1 1 1 2 2 1
   CSF#    31    3 1 1 1 2 1 2
   CSF#    32    3 1 1 1 2 0 3
   CSF#    33    3 1 1 1 1 2 2
   CSF#    34    3 1 0 3 2 1 1
   CSF#    35    3 1 0 3 1 3 0
   CSF#    36    3 1 0 3 1 2 1
   CSF#    37    3 1 0 3 1 1 2
   CSF#    38    3 1 0 3 1 0 3
   CSF#    39    3 1 0 0 1 3 3
   CSF#    40    3 0 3 1 2 1 1
   CSF#    41    3 0 3 1 1 3 0
   CSF#    42    3 0 3 1 1 2 1
   CSF#    43    3 0 3 1 1 1 2
   CSF#    44    3 0 3 1 1 0 3
   CSF#    45    3 0 1 3 2 1 1
   CSF#    46    3 0 1 3 1 3 0
   CSF#    47    3 0 1 3 1 2 1
   CSF#    48    3 0 1 3 1 1 2
   CSF#    49    3 0 1 3 1 0 3
   CSF#    50    3 0 1 0 1 3 3
   CSF#    51    3 0 0 1 1 3 3
   CSF#    52    1 3 3 3 1 0 0
   CSF#    53    1 3 3 0 2 1 1
   CSF#    54    1 3 3 0 1 3 0
   CSF#    55    1 3 3 0 1 2 1
   CSF#    56    1 3 3 0 1 1 2
   CSF#    57    1 3 3 0 1 0 3
   CSF#    58    1 3 2 1 2 1 1
   CSF#    59    1 3 2 1 1 3 0
   CSF#    60    1 3 2 1 1 2 1
   CSF#    61    1 3 2 1 1 1 2
   CSF#    62    1 3 2 1 1 0 3
   CSF#    63    1 3 1 2 2 1 1
   CSF#    64    1 3 1 2 1 3 0
   CSF#    65    1 3 1 2 1 2 1
   CSF#    66    1 3 1 2 1 1 2
   CSF#    67    1 3 1 2 1 0 3
   CSF#    68    1 3 1 1 2 3 0
   CSF#    69    1 3 1 1 2 2 1
   CSF#    70    1 3 1 1 2 1 2
   CSF#    71    1 3 1 1 2 0 3
   CSF#    72    1 3 1 1 1 2 2
   CSF#    73    1 3 0 3 2 1 1
   CSF#    74    1 3 0 3 1 3 0
   CSF#    75    1 3 0 3 1 2 1
   CSF#    76    1 3 0 3 1 1 2
   CSF#    77    1 3 0 3 1 0 3
   CSF#    78    1 3 0 0 1 3 3
   CSF#    79    1 2 3 1 2 1 1
   CSF#    80    1 2 3 1 1 3 0
   CSF#    81    1 2 3 1 1 2 1
   CSF#    82    1 2 3 1 1 1 2
   CSF#    83    1 2 3 1 1 0 3
   CSF#    84    1 2 1 3 2 1 1
   CSF#    85    1 2 1 3 1 3 0
   CSF#    86    1 2 1 3 1 2 1
   CSF#    87    1 2 1 3 1 1 2
   CSF#    88    1 2 1 3 1 0 3
   CSF#    89    1 2 1 0 1 3 3
   CSF#    90    1 2 0 1 1 3 3
   CSF#    91    1 1 3 2 2 1 1
   CSF#    92    1 1 3 2 1 3 0
   CSF#    93    1 1 3 2 1 2 1
   CSF#    94    1 1 3 2 1 1 2
   CSF#    95    1 1 3 2 1 0 3
   CSF#    96    1 1 3 1 2 3 0
   CSF#    97    1 1 3 1 2 2 1
   CSF#    98    1 1 3 1 2 1 2
   CSF#    99    1 1 3 1 2 0 3
   CSF#   100    1 1 3 1 1 2 2
   CSF#   101    1 1 2 3 2 1 1
   CSF#   102    1 1 2 3 1 3 0
   CSF#   103    1 1 2 3 1 2 1
   CSF#   104    1 1 2 3 1 1 2
   CSF#   105    1 1 2 3 1 0 3
   CSF#   106    1 1 2 0 1 3 3
   CSF#   107    1 1 1 3 2 3 0
   CSF#   108    1 1 1 3 2 2 1
   CSF#   109    1 1 1 3 2 1 2
   CSF#   110    1 1 1 3 2 0 3
   CSF#   111    1 1 1 3 1 2 2
   CSF#   112    1 1 1 0 2 3 3
   CSF#   113    1 1 0 2 1 3 3
   CSF#   114    1 1 0 1 2 3 3
   CSF#   115    1 0 3 3 2 1 1
   CSF#   116    1 0 3 3 1 3 0
   CSF#   117    1 0 3 3 1 2 1
   CSF#   118    1 0 3 3 1 1 2
   CSF#   119    1 0 3 3 1 0 3
   CSF#   120    1 0 3 0 1 3 3
   CSF#   121    1 0 2 1 1 3 3
   CSF#   122    1 0 1 2 1 3 3
   CSF#   123    1 0 1 1 2 3 3
   CSF#   124    1 0 0 3 1 3 3
   CSF#   125    0 3 3 1 2 1 1
   CSF#   126    0 3 3 1 1 3 0
   CSF#   127    0 3 3 1 1 2 1
   CSF#   128    0 3 3 1 1 1 2
   CSF#   129    0 3 3 1 1 0 3
   CSF#   130    0 3 1 3 2 1 1
   CSF#   131    0 3 1 3 1 3 0
   CSF#   132    0 3 1 3 1 2 1
   CSF#   133    0 3 1 3 1 1 2
   CSF#   134    0 3 1 3 1 0 3
   CSF#   135    0 3 1 0 1 3 3
   CSF#   136    0 3 0 1 1 3 3
   CSF#   137    0 1 3 3 2 1 1
   CSF#   138    0 1 3 3 1 3 0
   CSF#   139    0 1 3 3 1 2 1
   CSF#   140    0 1 3 3 1 1 2
   CSF#   141    0 1 3 3 1 0 3
   CSF#   142    0 1 3 0 1 3 3
   CSF#   143    0 1 2 1 1 3 3
   CSF#   144    0 1 1 2 1 3 3
   CSF#   145    0 1 1 1 2 3 3
   CSF#   146    0 1 0 3 1 3 3
   CSF#   147    0 0 3 1 1 3 3
   CSF#   148    0 0 1 3 1 3 3
