1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.50 MB location: local disk    
three external inte    0.50 MB location: local disk    
four external integ    0.50 MB location: local disk    
three external inte    0.50 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.22 MB location: local disk    
 nsubmx= 16 lenci= 14558
global arrays:       480414   (    3.67 MB)
vdisk:                    0   (    0.00 MB)
drt:                 786835   (    3.00 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 0,
   RTOLCI=0.00010000,
   VOUT  = 1,
   NBKITR= 1,
   NITER= 20,
  IDEN=   1,
   IVMODE=   3,
   NVCIMX = 16
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   20
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =   1      ctol   = 1.00E-02  davcor  =   1


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082410

 integral file titles:
 ethylen pvdz                                                                    
 aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003
  ethylen cas(2,2) reference                                                     
 ethylen cas(2,2)                                                                
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Jun  5 14:26:46 2003
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:46 2003

 core energy values from the integral file:
 energy( 1)=  3.322006051733E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.100513157221E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.683125520480E+01

 drt header information:
  ethylen cas(2,2) reference                                                     
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    48 niot  =     9 nfct  =     0 nfvt  =     0
 nrow  =    62 nsym  =     8 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        268       13      128       56       71
 nvalwt,nvalw:      268       13      128       56       71
 ncsft:           14558
 total number of valid internal walks:     268
 nvalz,nvaly,nvalx,nvalw =       13     128      56      71

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext  1560   702    90
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    45750    39009    14913     2850      252        0        0        0
 minbl4,minbl3,maxbl2   420   420   423
 maxbuf 32767
 number of external orbitals per symmetry block:   8   2   6   3   2   9   3   6
 nmsym   8 number of internal orbitals   9

 formula file title:
 ethylen pvdz                                                                    
 aoints SIFS file created by argos.      hochtor2        Thu Jun  5 14:26:44 2003
  ethylen cas(2,2) reference                                                     
 ethylen cas(2,2)                                                                
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Jun  5 14:26:46 2003
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:46 2003
  ethylen cas(2,2) reference                                                     
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:    13   128    56    71 total internal walks:     268
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 3 6 6 8 4 7

 setref:        2 references kept,
                0 references were marked as invalid, out of
                2 total.
 limcnvrt: found 13 valid internal walksout of  13
  walks (skipping trailing invalids)
  ... adding  13 segmentation marks segtype= 1
 limcnvrt: found 128 valid internal walksout of  128
  walks (skipping trailing invalids)
  ... adding  128 segmentation marks segtype= 2
 limcnvrt: found 56 valid internal walksout of  56
  walks (skipping trailing invalids)
  ... adding  56 segmentation marks segtype= 3
 limcnvrt: found 71 valid internal walksout of  71
  walks (skipping trailing invalids)
  ... adding  71 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x     102      70     114      75      70     121      75     114
 vertex w     141      70     114      75      70     121      75     114



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          13|        13|         0|        13|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         128|       478|        13|       128|        13|         2|
 -------------------------------------------------------------------------------
  X 3          56|      5820|       491|        56|       141|         3|
 -------------------------------------------------------------------------------
  W 4          71|      8247|      6311|        71|       197|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>     14558


 297 dimension of the ci-matrix ->>>     14558


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      56      13       5820         13      56      13
     2  4   1    25      two-ext wz   2X  4 1      71      13       8247         13      71      13
     3  4   3    26      two-ext wx   2X  4 3      71      56       8247       5820      71      56
     4  2   1    11      one-ext yz   1X  2 1     128      13        478         13     128      13
     5  3   2    15      1ex3ex  yx   3X  3 2      56     128       5820        478      56     128
     6  4   2    16      1ex3ex  yw   3X  4 2      71     128       8247        478      71     128
     7  1   1     1      allint zz    OX  1 1      13      13         13         13      13      13
     8  2   2     5      0ex2ex yy    OX  2 2     128     128        478        478     128     128
     9  3   3     6      0ex2ex xx    OX  3 3      56      56       5820       5820      56      56
    10  4   4     7      0ex2ex ww    OX  4 4      71      71       8247       8247      71      71
    11  1   1    75      dg-024ext z  DG  1 1      13      13         13         13      13      13
    12  2   2    45      4exdg024 y   DG  2 2     128     128        478        478     128     128
    13  3   3    46      4exdg024 x   DG  3 3      56      56       5820       5820      56      56
    14  4   4    47      4exdg024 w   DG  4 4      71      71       8247       8247      71      71
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 1766 786 255
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        84 2x:         0 4x:         0
All internal counts: zz :       103 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       102    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        82    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       2

    root           eigenvalues
    ----           ------------
       1         -78.0668950712

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    13)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             14558
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:         0 xx:         0 ww:         0
One-external counts: yz :       668 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:        56 wz:        94 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:         0    task #   4:       695
task #   5:         0    task #   6:         0    task #   7:       102    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        82    task #  12:       430
task #  13:       139    task #  14:       175    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.0668950712 -4.2188E-15  3.0754E-01  9.4916E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0500s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0700s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.0668950712 -4.2188E-15  3.0754E-01  9.4916E-01  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             14558
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95746437    -0.28855152

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.3235468672  2.5665E-01  1.1023E-02  1.6978E-01  1.0000E-04
 mr-sdci #  1  2    -75.2410859000 -1.5902E+00  0.0000E+00  1.2841E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.05   0.01   0.00   0.05
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.03   0.00   0.00   0.03
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.05   0.01   0.00   0.05
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3800s 
time spent in multnx:                   0.3800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3900s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.95548272    -0.09173344     0.28042425

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -78.3319062860  8.3594E-03  1.0873E-03  5.3067E-02  1.0000E-04
 mr-sdci #  2  2    -75.8117678828  5.7068E-01  0.0000E+00  1.5321E+00  1.0000E-04
 mr-sdci #  2  3    -75.2315611282 -1.5997E+00  0.0000E+00  1.2268E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.04   0.00   0.00   0.04
    4   11    0   0.02   0.01   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.03   0.00   0.00   0.03
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3700s 
time spent in multnx:                   0.3600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.4000s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.95520082     0.09052139     0.03572811     0.27950094

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -78.3327676632  8.6138E-04  5.3231E-05  1.2047E-02  1.0000E-04
 mr-sdci #  3  2    -75.8849069675  7.3139E-02  0.0000E+00  1.2518E+00  1.0000E-04
 mr-sdci #  3  3    -75.7355332922  5.0397E-01  0.0000E+00  1.6582E+00  1.0000E-04
 mr-sdci #  3  4    -75.2286106427 -1.6026E+00  0.0000E+00  1.2073E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.05   0.01   0.00   0.05
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.03   0.00   0.00   0.03
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.05   0.00   0.00   0.05
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3800s 
time spent in multnx:                   0.3800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.4000s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.95488333    -0.11517401    -0.01495936    -0.11947480     0.24583484

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -78.3328230834  5.5420E-05  3.3527E-06  3.1283E-03  1.0000E-04
 mr-sdci #  4  2    -76.7023164958  8.1741E-01  0.0000E+00  1.0953E+00  1.0000E-04
 mr-sdci #  4  3    -75.7974579061  6.1925E-02  0.0000E+00  1.4452E+00  1.0000E-04
 mr-sdci #  4  4    -75.4360594834  2.0745E-01  0.0000E+00  1.1043E+00  1.0000E-04
 mr-sdci #  4  5    -74.9840418555 -1.8472E+00  0.0000E+00  1.5593E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.05   0.01   0.00   0.05
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.03   0.00   0.00   0.03
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.05   0.00   0.00   0.05
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3800s 
time spent in multnx:                   0.3800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.4100s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.95484564    -0.09507312    -0.03794920    -0.12261060     0.05231102    -0.24499175

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -78.3328264758  3.3924E-06  3.0582E-07  8.9229E-04  1.0000E-04
 mr-sdci #  5  2    -76.9941884424  2.9187E-01  0.0000E+00  8.9428E-01  1.0000E-04
 mr-sdci #  5  3    -75.8059397979  8.4819E-03  0.0000E+00  1.4193E+00  1.0000E-04
 mr-sdci #  5  4    -75.6256280072  1.8957E-01  0.0000E+00  1.1724E+00  1.0000E-04
 mr-sdci #  5  5    -75.3319694910  3.4793E-01  0.0000E+00  1.3289E+00  1.0000E-04
 mr-sdci #  5  6    -74.9792884433 -1.8520E+00  0.0000E+00  1.5306E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.12   0.01   0.00   0.12
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.05   0.01   0.00   0.04
    6   16    0   0.06   0.01   0.00   0.06
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.05   0.00   0.00   0.04
    9    6    0   0.04   0.01   0.00   0.04
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.05   0.00   0.00   0.05
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.4800s 
time spent in multnx:                   0.4600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5100s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.95483707     0.06327997     0.10167993    -0.12355162     0.06689558    -0.02466736     0.23150488

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -78.3328268118  3.3599E-07  4.1056E-08  3.2919E-04  1.0000E-04
 mr-sdci #  6  2    -77.3621559450  3.6797E-01  0.0000E+00  7.7763E-01  1.0000E-04
 mr-sdci #  6  3    -76.1640799562  3.5814E-01  0.0000E+00  1.1369E+00  1.0000E-04
 mr-sdci #  6  4    -75.6284576869  2.8297E-03  0.0000E+00  1.1719E+00  1.0000E-04
 mr-sdci #  6  5    -75.4213554019  8.9386E-02  0.0000E+00  1.1643E+00  1.0000E-04
 mr-sdci #  6  6    -75.2676774241  2.8839E-01  0.0000E+00  1.4191E+00  1.0000E-04
 mr-sdci #  6  7    -74.9099686115 -1.9213E+00  0.0000E+00  1.6128E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.00   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.03   0.01   0.00   0.03
    9    6    0   0.04   0.01   0.00   0.04
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.05   0.00   0.00   0.05
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3800s 
time spent in multnx:                   0.3800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.4100s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95483514    -0.04115249    -0.09942669    -0.11764527    -0.09561727     0.02748823     0.04912049    -0.22485366

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -78.3328268593  4.7418E-08  5.0672E-09  1.1751E-04  1.0000E-04
 mr-sdci #  7  2    -77.6207393882  2.5858E-01  0.0000E+00  5.5817E-01  1.0000E-04
 mr-sdci #  7  3    -76.5744422073  4.1036E-01  0.0000E+00  8.2432E-01  1.0000E-04
 mr-sdci #  7  4    -75.6294546478  9.9696E-04  0.0000E+00  1.1702E+00  1.0000E-04
 mr-sdci #  7  5    -75.4368313568  1.5476E-02  0.0000E+00  1.1127E+00  1.0000E-04
 mr-sdci #  7  6    -75.3769408267  1.0926E-01  0.0000E+00  1.3918E+00  1.0000E-04
 mr-sdci #  7  7    -75.0785110308  1.6854E-01  0.0000E+00  1.3817E+00  1.0000E-04
 mr-sdci #  7  8    -74.9038820022 -1.9274E+00  0.0000E+00  1.5686E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.05   0.01   0.00   0.04
    6   16    0   0.04   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.03   0.00   0.00   0.03
    9    6    0   0.04   0.00   0.00   0.03
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.05   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3800s 
time spent in multnx:                   0.3500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.4200s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       868 yw:       998

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95483439     0.03561283     0.08488014    -0.12999597    -0.06205303    -0.09763718     0.04770962    -0.05327350

                ci   9
 ref:   1    -0.21076419

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -78.3328268645  5.2132E-09  4.6092E-10  3.5865E-05  1.0000E-04
 mr-sdci #  8  2    -77.7188753509  9.8136E-02  0.0000E+00  3.8472E-01  1.0000E-04
 mr-sdci #  8  3    -76.7628154151  1.8837E-01  0.0000E+00  6.7182E-01  1.0000E-04
 mr-sdci #  8  4    -75.7092387339  7.9784E-02  0.0000E+00  1.2483E+00  1.0000E-04
 mr-sdci #  8  5    -75.6059436246  1.6911E-01  0.0000E+00  1.1189E+00  1.0000E-04
 mr-sdci #  8  6    -75.4366889496  5.9748E-02  0.0000E+00  1.1163E+00  1.0000E-04
 mr-sdci #  8  7    -75.2332018700  1.5469E-01  0.0000E+00  1.2876E+00  1.0000E-04
 mr-sdci #  8  8    -75.0783682737  1.7449E-01  0.0000E+00  1.3875E+00  1.0000E-04
 mr-sdci #  8  9    -74.8411629782 -1.9901E+00  0.0000E+00  1.6707E+00  1.0000E-04


 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -78.3328268645  5.2132E-09  4.6092E-10  3.5865E-05  1.0000E-04
 mr-sdci #  8  2    -77.7188753509  9.8136E-02  0.0000E+00  3.8472E-01  1.0000E-04
 mr-sdci #  8  3    -76.7628154151  1.8837E-01  0.0000E+00  6.7182E-01  1.0000E-04
 mr-sdci #  8  4    -75.7092387339  7.9784E-02  0.0000E+00  1.2483E+00  1.0000E-04
 mr-sdci #  8  5    -75.6059436246  1.6911E-01  0.0000E+00  1.1189E+00  1.0000E-04
 mr-sdci #  8  6    -75.4366889496  5.9748E-02  0.0000E+00  1.1163E+00  1.0000E-04
 mr-sdci #  8  7    -75.2332018700  1.5469E-01  0.0000E+00  1.2876E+00  1.0000E-04
 mr-sdci #  8  8    -75.0783682737  1.7449E-01  0.0000E+00  1.3875E+00  1.0000E-04
 mr-sdci #  8  9    -74.8411629782 -1.9901E+00  0.0000E+00  1.6707E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -78.332826864464

################END OF CIUDGINFO################


    1 of the  10 expansion vectors are transformed.
    1 of the   9 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.954834393)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -78.3328268645

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9

                                          orbital     1    2    3   14   27   28   42   21   38

                                         symmetry   ag   ag   ag  b2g  b1u  b1u  b3u  b3g  b2u 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.168862                        +-   +-   +-   +-   +-   +-   +-   +-      
 z*  1  1       2 -0.940211                        +-   +-   +-   +-   +-   +-   +-        +- 
 z   1  1       4  0.020656                        +-   +-   +-   +-   +-        +-   +-   +- 
 z   1  1      10  0.012546                        +-        +-   +-   +-   +-   +-   +-   +- 
 y   1  1      26 -0.024803              1(b2g )   +-   +-   +-   +-   +-   +-    -   +     - 
 y   1  1      27 -0.020213              2(b2g )   +-   +-   +-   +-   +-   +-    -   +     - 
 y   1  1      59  0.028462              1( ag )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      62 -0.020558              4( ag )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      80 -0.018280              1( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      82 -0.033394              3( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      84 -0.014071              5( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      85 -0.013420              6( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      90 -0.021900              1(b1g )   +-   +-   +-   +-   +-   +     -    -   +- 
 y   1  1     165  0.031018              2(b3u )   +-   +-   +-    -   +-   +-   +-   +     - 
 y   1  1     166 -0.013504              3(b3u )   +-   +-   +-    -   +-   +-   +-   +     - 
 y   1  1     184  0.017498              1( au )   +-   +-   +-    -   +-   +    +-    -   +- 
 y   1  1     190 -0.012594              1(b3u )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     204  0.011873              1( au )   +-   +-   +-   +    +-    -   +-    -   +- 
 y   1  1     225 -0.031092              2(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     226  0.024232              3(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     227  0.040535              4(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     228  0.013932              5(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     229 -0.010335              6(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     249 -0.019829              2(b2u )   +-   +-    -   +-   +-   +    +-    -   +- 
 y   1  1     278 -0.012621              2(b2u )   +-   +-   +    +-   +-    -   +-    -   +- 
 y   1  1     304  0.020747              1(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     305 -0.014665              2(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     306  0.019998              3(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     307  0.010456              4(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     308  0.017625              5(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     347 -0.020467              1(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     348  0.011798              2(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     349 -0.034472              3(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     352  0.011751              6(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     358 -0.014357              1( au )   +-   +    +-   +-   +-   +-    -    -   +- 
 y   1  1     364  0.012287              2(b2u )   +-   +    +-   +-   +-    -   +-    -   +- 
 y   1  1     374  0.015556              1(b1g )   +-   +    +-    -   +-   +-   +-    -   +- 
 x   1  1     564  0.021876    3( ag )   1(b1g )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     566  0.014179    5( ag )   1(b1g )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     600 -0.011009    1( au )   3(b1u )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     614 -0.018949    1(b2u )   1(b3u )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     618  0.011640    2(b2u )   2(b3u )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     620  0.017145    1(b2u )   3(b3u )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     743 -0.013068    1(b1u )   1(b2u )   +-   +-   +-   +-   +-    -   +-         - 
 x   1  1     754 -0.011273    3(b1u )   2(b2u )   +-   +-   +-   +-   +-    -   +-         - 
 x   1  1     966  0.011103    2(b1u )   2(b3u )   +-   +-   +-   +-   +-    -    -        +- 
 x   1  1    1664 -0.018386    3( ag )   1( au )   +-   +-   +-    -   +-   +-   +-         - 
 x   1  1    1682  0.014488    1(b1g )   3(b1u )   +-   +-   +-    -   +-   +-   +-         - 
 x   1  1    1696 -0.016100    1(b2g )   1(b2u )   +-   +-   +-    -   +-   +-   +-         - 
 x   1  1    1697 -0.018147    2(b2g )   1(b2u )   +-   +-   +-    -   +-   +-   +-         - 
 x   1  1    1938  0.010109    1(b2g )   1(b3u )   +-   +-   +-    -   +-   +-    -        +- 
 x   1  1    2544  0.011883    1( ag )   1(b2u )   +-   +-    -   +-   +-   +-   +-         - 
 x   1  1    2545  0.020177    2( ag )   1(b2u )   +-   +-    -   +-   +-   +-   +-         - 
 x   1  1    2546  0.015290    3( ag )   1(b2u )   +-   +-    -   +-   +-   +-   +-         - 
 x   1  1    2554  0.015981    3( ag )   2(b2u )   +-   +-    -   +-   +-   +-   +-         - 
 x   1  1    2557  0.011126    6( ag )   2(b2u )   +-   +-    -   +-   +-   +-   +-         - 
 x   1  1    2570  0.012960    1(b1g )   2(b3u )   +-   +-    -   +-   +-   +-   +-         - 
 x   1  1    2756  0.010357    1(b1g )   2(b2u )   +-   +-    -   +-   +-   +-    -        +- 
 x   1  1    2761 -0.020488    2( ag )   1(b3u )   +-   +-    -   +-   +-   +-    -        +- 
 x   1  1    2769  0.014615    2( ag )   2(b3u )   +-   +-    -   +-   +-   +-    -        +- 
 x   1  1    2777  0.015657    2( ag )   3(b3u )   +-   +-    -   +-   +-   +-    -        +- 
 x   1  1    3407  0.010897    2( ag )   1(b2g )   +-   +-    -    -   +-   +-   +-        +- 
 x   1  1    3415  0.015423    2( ag )   2(b2g )   +-   +-    -    -   +-   +-   +-        +- 
 x   1  1    3467 -0.010219    2(b1u )   1(b3u )   +-   +-    -    -   +-   +-   +-        +- 
 x   1  1    3476  0.020215    2(b1u )   2(b3u )   +-   +-    -    -   +-   +-   +-        +- 
 x   1  1    3478 -0.014927    4(b1u )   2(b3u )   +-   +-    -    -   +-   +-   +-        +- 
 x   1  1    3637  0.010556    4( ag )   1(b2u )   +-    -   +-   +-   +-   +-   +-         - 
 w   1  1    6317  0.014860    3( ag )   3( ag )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6324  0.011184    3( ag )   5( ag )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6348  0.011701    1(b1g )   1(b1g )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6386  0.010770    3(b1u )   3(b1u )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6426  0.022921    1(b2u )   1(b2u )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6428  0.011103    2(b2u )   2(b2u )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6575 -0.015215    1(b2u )   1(b3u )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    6581  0.015389    1(b2u )   3(b3u )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    6739  0.011357    3( ag )   3( ag )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6770  0.011471    1(b1g )   1(b1g )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6774  0.011624    1(b2g )   2(b2g )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6775  0.014575    2(b2g )   2(b2g )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6808  0.010148    3(b1u )   3(b1u )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6854  0.016751    1(b3u )   1(b3u )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6855 -0.010615    1(b3u )   2(b3u )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6856  0.016173    2(b3u )   2(b3u )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6857 -0.016015    1(b3u )   3(b3u )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6859  0.012529    3(b3u )   3(b3u )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    6986  0.013984    1(b1u )   1(b2u )   +-   +-   +-   +-   +-   +    +-         - 
 w   1  1    6990  0.010306    5(b1u )   1(b2u )   +-   +-   +-   +-   +-   +    +-         - 
 w   1  1    7147 -0.011794    1( ag )   2(b2g )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7150  0.012812    4( ag )   2(b2g )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7195 -0.010069    1( au )   2(b2u )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7199 -0.015387    1(b1u )   1(b3u )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7201 -0.011441    3(b1u )   1(b3u )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7210  0.017808    3(b1u )   2(b3u )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7217  0.012091    1(b1u )   3(b3u )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7219  0.010916    3(b1u )   3(b3u )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    7394  0.010159    1( ag )   1( ag )   +-   +-   +-   +-   +-        +-        +- 
 w   1  1    7400 -0.012214    1( ag )   4( ag )   +-   +-   +-   +-   +-        +-        +- 
 w   1  1    7463  0.010553    1(b1u )   1(b1u )   +-   +-   +-   +-   +-        +-        +- 
 w   1  1    7469 -0.011248    1(b1u )   4(b1u )   +-   +-   +-   +-   +-        +-        +- 
 w   1  1    7472  0.011222    4(b1u )   4(b1u )   +-   +-   +-   +-   +-        +-        +- 
 w   1  1    8565  0.010462    1(b1g )   2(b1u )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1    8581  0.012338    1(b2g )   1(b2u )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1    8582  0.015170    2(b2g )   1(b2u )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1    8738 -0.019846    1(b1g )   1( au )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8742 -0.013396    1( ag )   1(b1u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8751  0.011790    2( ag )   2(b1u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8752 -0.013534    3( ag )   2(b1u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8760 -0.024379    3( ag )   3(b1u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8768  0.010495    3( ag )   4(b1u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8823 -0.021162    1(b2g )   1(b3u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8824 -0.027382    2(b2g )   1(b3u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8830  0.014783    2(b2g )   2(b3u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8831  0.019257    3(b2g )   2(b3u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8835  0.013489    1(b2g )   3(b3u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8836  0.024027    2(b2g )   3(b3u )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1    8974  0.011451    2(b3g )   1( au )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    8979  0.015048    1(b2g )   1(b1u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    8980  0.017709    2(b2g )   1(b1u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    8992  0.017755    2(b2g )   3(b1u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    8993  0.011065    3(b2g )   3(b1u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    9035  0.011051    1(b1g )   2(b2u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    9039  0.010792    1( ag )   1(b3u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    9049 -0.020209    3( ag )   2(b3u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    9058  0.010360    4( ag )   3(b3u )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1    9461  0.016288    3( ag )   3( ag )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9495  0.012938    1(b2g )   1(b2g )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9496  0.015042    1(b2g )   2(b2g )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9497  0.017395    2(b2g )   2(b2g )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9522  0.011301    1( au )   1( au )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9528  0.010141    1(b1u )   3(b1u )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9530  0.016164    3(b1u )   3(b1u )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9576  0.013863    1(b3u )   1(b3u )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9577 -0.011106    1(b3u )   2(b3u )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9578  0.021839    2(b3u )   2(b3u )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9579 -0.011837    1(b3u )   3(b3u )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1    9693  0.010257    1(b3g )   4(b1u )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1    9712 -0.015146    2( ag )   1(b2u )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1    9737  0.010268    1(b1g )   2(b3u )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1    9886 -0.014750    2(b2g )   4(b1u )   +-   +-   +    +-   +-   +-    -        +- 
 w   1  1    9923  0.012970    1(b1g )   2(b2u )   +-   +-   +    +-   +-   +-    -        +- 
 w   1  1    9927  0.013089    1( ag )   1(b3u )   +-   +-   +    +-   +-   +-    -        +- 
 w   1  1    9929  0.013320    3( ag )   1(b3u )   +-   +-   +    +-   +-   +-    -        +- 
 w   1  1    9937 -0.017947    3( ag )   2(b3u )   +-   +-   +    +-   +-   +-    -        +- 
 w   1  1    9945 -0.013072    3( ag )   3(b3u )   +-   +-   +    +-   +-   +-    -        +- 
 w   1  1   10100 -0.016596    1( ag )   1(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10102 -0.010152    3( ag )   1(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10103  0.013202    4( ag )   1(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10109  0.012209    2( ag )   2(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10118 -0.015100    3( ag )   3(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10124  0.021398    1( ag )   4(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10126  0.010034    3( ag )   4(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10127 -0.020461    4( ag )   4(b1u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10189  0.011332    3(b2g )   2(b3u )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   10573 -0.011750    1( ag )   1(b2g )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10581 -0.011123    1( ag )   2(b2g )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10583 -0.016825    3( ag )   2(b2g )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10629 -0.012355    1( au )   2(b2u )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10633 -0.012422    1(b1u )   1(b3u )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10636  0.012260    4(b1u )   1(b3u )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10644  0.023020    3(b1u )   2(b3u )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10654 -0.012714    4(b1u )   3(b3u )   +-   +-   +     -   +-   +-   +-        +- 
 w   1  1   10828  0.010319    1( ag )   1( ag )   +-   +-        +-   +-   +-   +-        +- 
 w   1  1   10830  0.016035    2( ag )   2( ag )   +-   +-        +-   +-   +-   +-        +- 
 w   1  1   10899  0.011412    2(b1u )   2(b1u )   +-   +-        +-   +-   +-   +-        +- 
 w   1  1   10904 -0.012736    2(b1u )   4(b1u )   +-   +-        +-   +-   +-   +-        +- 
 w   1  1   10906  0.019301    4(b1u )   4(b1u )   +-   +-        +-   +-   +-   +-        +- 
 w   1  1   10944  0.011724    2(b2u )   2(b2u )   +-   +-        +-   +-   +-   +-        +- 
 w   1  1   10950  0.013568    2(b3u )   2(b3u )   +-   +-        +-   +-   +-   +-        +- 
 w   1  1   11086 -0.012556    4( ag )   1(b2u )   +-   +    +-   +-   +-   +-   +-         - 
 w   1  1   11240 -0.011095    2(b2g )   1(b1u )   +-   +    +-   +-   +-   +-    -        +- 
 w   1  1   11252 -0.012985    2(b2g )   3(b1u )   +-   +    +-   +-   +-   +-    -        +- 
 w   1  1   11264 -0.012292    2(b2g )   5(b1u )   +-   +    +-   +-   +-   +-    -        +- 
 w   1  1   11318 -0.010255    4( ag )   3(b3u )   +-   +    +-   +-   +-   +-    -        +- 
 w   1  1   11468  0.010320    1(b1g )   1( au )   +-   +    +-   +-   +-    -   +-        +- 
 w   1  1   11472  0.011264    1( ag )   1(b1u )   +-   +    +-   +-   +-    -   +-        +- 
 w   1  1   11475 -0.010921    4( ag )   1(b1u )   +-   +    +-   +-   +-    -   +-        +- 
 w   1  1   11490  0.010285    3( ag )   3(b1u )   +-   +    +-   +-   +-    -   +-        +- 
 w   1  1   11956 -0.010726    4( ag )   2(b2g )   +-   +    +-    -   +-   +-   +-        +- 
 w   1  1   12016 -0.010306    3(b1u )   2(b3u )   +-   +    +-    -   +-   +-   +-        +- 
 w   1  1   12025 -0.010039    3(b1u )   3(b3u )   +-   +    +-    -   +-   +-   +-        +- 
 w   1  1   12275  0.011669    1(b1u )   4(b1u )   +-   +     -   +-   +-   +-   +-        +- 
 w   1  1   12277  0.012520    3(b1u )   4(b1u )   +-   +     -   +-   +-   +-   +-        +- 
 w   1  1   12282  0.010704    4(b1u )   5(b1u )   +-   +     -   +-   +-   +-   +-        +- 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             181
     0.01> rq > 0.001           3336
    0.001> rq > 0.0001          5852
   0.0001> rq > 0.00001         4021
  0.00001> rq > 0.000001         951
 0.000001> rq                    215
           all                 14558
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        84 2x:         0 4x:         0
All internal counts: zz :       103 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       102    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        82    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.168862013820    -13.201141772939
     2     2     -0.940210784416     73.395421769484

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =    -78.066304920239   "relaxed" cnot**2         =   0.912510698844
 eci       =    -78.332826864464   deltae = eci - eref       =  -0.266521944225
 eci+dv1   =    -78.356144683107   dv1 = (1-cnot**2)*deltae  =  -0.023317818643
 eci+dv2   =    -78.358380338711   dv2 = dv1 / cnot**2       =  -0.025553474247
 eci+dv3   =    -78.361090154008   dv3 = dv1 / (2*cnot**2-1) =  -0.028263289544
 eci+pople =    -78.356890487025   ( 16e- scaled deltae )    =  -0.290585566786
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999521

 space required:

    space required for calls in multd2:
       onex          135
       allin           0
       diagon        705
    max.      ---------
       maxnex        705

    total core space usage:
       maxnex        705
    max k-seg       8247
    max k-ind        128
              ---------
       totmax      17455
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      46  DYX=     224  DYW=     254
   D0Z=      11  D0Y=     132  D0X=      48  D0W=      64
  DDZI=      39 DDYI=     450 DDXI=     154 DDWI=     182
  DDZE=       0 DDYE=     128 DDXE=      56 DDWE=      71
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9998487      1.9806929      1.9668960      0.0168461      0.0075901
   0.0064485      0.0018723      0.0010486      0.0006292      0.0003133
   0.0001139


*****   symmetry block SYM2   *****

 occupation numbers of nos
   0.0058963      0.0008116


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9669071      0.0148824      0.0032496      0.0015120      0.0005705
   0.0002271      0.0001080


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0807530      0.0027117      0.0009200      0.0005458


*****   symmetry block SYM5   *****

 occupation numbers of nos
   0.0040240      0.0004178


*****   symmetry block SYM6   *****

 occupation numbers of nos
   1.9998418      1.9733294      0.0180279      0.0151793      0.0047847
   0.0022683      0.0009869      0.0005368      0.0002780      0.0000891
   0.0000635


*****   symmetry block SYM7   *****

 occupation numbers of nos
   1.9055816      0.0076404      0.0040984      0.0007377


*****   symmetry block SYM8   *****

 occupation numbers of nos
   1.9702629      0.0192234      0.0077153      0.0016330      0.0012476
   0.0005524      0.0000849


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                         ag partial gross atomic populations
   ao class       1 ag       2 ag       3 ag       4 ag       5 ag       6 ag
    C__ s       2.000547   1.459908   0.023518   0.003849   0.000496   0.002409
    C__ p      -0.000250   0.062379   1.465224   0.004335   0.002310   0.000076
    C__ d      -0.000011   0.009492   0.012737   0.000729   0.004096   0.002948
    H__ s      -0.000089   0.421843   0.452491   0.007740  -0.000023   0.000583
    H__ p      -0.000348   0.027072   0.012926   0.000194   0.000712   0.000434

   ao class       7 ag       8 ag       9 ag      10 ag      11 ag
    C__ s       0.000192   0.000302   0.000051   0.000012   0.000008
    C__ p       0.000070   0.000310   0.000106   0.000007   0.000015
    C__ d       0.000746   0.000338   0.000094   0.000021   0.000001
    H__ s       0.000877   0.000014   0.000032  -0.000005   0.000056
    H__ p      -0.000013   0.000085   0.000347   0.000279   0.000033

                        b1g partial gross atomic populations
   ao class       1b1g       2b1g
    C__ d       0.004862   0.000142
    H__ p       0.001034   0.000669

                        b2g partial gross atomic populations
   ao class       1b2g       2b2g       3b2g       4b2g       5b2g       6b2g
    C__ p       0.761244   0.009596   0.000872   0.000108   0.000165   0.000055
    C__ d       0.054963  -0.000065   0.001187   0.000142   0.000184   0.000027
    H__ s       1.141960   0.004709   0.000569   0.000468   0.000038   0.000045
    H__ p       0.008741   0.000643   0.000622   0.000795   0.000183   0.000100

   ao class       7b2g
    C__ p       0.000010
    C__ d       0.000008
    H__ s       0.000038
    H__ p       0.000051

                        b3g partial gross atomic populations
   ao class       1b3g       2b3g       3b3g       4b3g
    C__ p       0.078498   0.001246   0.000500   0.000014
    C__ d       0.000971   0.000843   0.000360   0.000156
    H__ p       0.001283   0.000623   0.000061   0.000376

                         au partial gross atomic populations
   ao class       1 au       2 au
    C__ d       0.002617   0.000146
    H__ p       0.001407   0.000272

                        b1u partial gross atomic populations
   ao class       1b1u       2b1u       3b1u       4b1u       5b1u       6b1u
    C__ s       2.001585   0.709556   0.010119   0.000697   0.000588   0.000543
    C__ p      -0.000271   0.418763   0.003386   0.007826   0.001356   0.000124
    C__ d      -0.000239   0.005723   0.001318   0.001011   0.001772   0.000937
    H__ s      -0.000743   0.805755   0.003173   0.005176   0.000370   0.000247
    H__ p      -0.000491   0.033532   0.000031   0.000471   0.000700   0.000418

   ao class       7b1u       8b1u       9b1u      10b1u      11b1u
    C__ s       0.000084   0.000051   0.000043   0.000020   0.000007
    C__ p       0.000088   0.000077   0.000007   0.000044   0.000000
    C__ d       0.000610   0.000039   0.000091   0.000002   0.000002
    H__ s       0.000240   0.000026   0.000002   0.000001   0.000037
    H__ p      -0.000034   0.000344   0.000134   0.000022   0.000018

                        b2u partial gross atomic populations
   ao class       1b2u       2b2u       3b2u       4b2u
    C__ p       1.865111   0.004272   0.001826   0.000012
    C__ d       0.020924   0.002940   0.002046   0.000077
    H__ p       0.019547   0.000428   0.000226   0.000648

                        b3u partial gross atomic populations
   ao class       1b3u       2b3u       3b3u       4b3u       5b3u       6b3u
    C__ p       1.137487   0.007172   0.003416   0.000447   0.000014   0.000128
    C__ d       0.000040   0.003191   0.002912   0.000758   0.000002  -0.000006
    H__ s       0.817973   0.008661   0.000651   0.000419   0.000203   0.000042
    H__ p       0.014763   0.000199   0.000736   0.000009   0.001028   0.000389

   ao class       7b3u
    C__ p       0.000008
    C__ d       0.000000
    H__ s       0.000047
    H__ p       0.000030


                        gross atomic populations
     ao           C__        H__
      s         6.214584   3.673624
      p         5.838179   0.131725
      d         0.141888   0.000000
    total      12.194650   3.805350


 Total number of electrons:   16.00000000

