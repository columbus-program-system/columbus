 total ao core energy =   33.220060517
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
 ethylen cas(2,2)                                                                
 Molecular symmetry group:    ag 
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         2

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    54
 Number of active-virtual rotations:     6

 iter=    1 emc=  -78.0569742091 demc= 7.8057E+01 wnorm= 6.7321E-02 knorm= 5.7747E-01 apxde= 1.8066E-02    *not converged* 
 iter=    2 emc=  -78.0627172819 demc= 5.7431E-03 wnorm= 5.7628E-02 knorm= 3.7339E-01 apxde= 5.7424E-03    *not converged* 
 iter=    3 emc=  -78.0649926051 demc= 2.2753E-03 wnorm= 4.6234E-02 knorm= 1.2396E-01 apxde= 1.8375E-03    *not converged* 
 iter=    4 emc=  -78.0668863466 demc= 1.8937E-03 wnorm= 5.3163E-03 knorm= 7.9782E-03 apxde= 8.6469E-06    *not converged* 
 iter=    5 emc=  -78.0668950696 demc= 8.7230E-06 wnorm= 3.1106E-05 knorm= 1.3523E-04 apxde= 1.6291E-09    *not converged* 

 final mcscf convergence values:
 iter=    6 emc=  -78.0668950712 demc= 1.6293E-09 wnorm= 6.4132E-07 knorm= 3.7312E-08 apxde= 1.1579E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -78.066895071
   ------------------------------------------------------------


