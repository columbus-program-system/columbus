1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   144)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -108.6882585469  2.8422E-14  3.4908E-01  1.0765E+00  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -108.6882585469  2.8422E-14  3.4908E-01  1.0765E+00  1.0000E-04

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -108.9741011146  2.8584E-01  2.6275E-02  2.9798E-01  1.0000E-04
 mr-sdci #  2  1   -108.9925376363  1.8437E-02  4.9698E-03  1.1684E-01  1.0000E-04
 mr-sdci #  3  1   -108.9955596070  3.0220E-03  9.3074E-04  5.3791E-02  1.0000E-04
 mr-sdci #  4  1   -108.9963225727  7.6297E-04  1.7125E-04  2.3270E-02  1.0000E-04
 mr-sdci #  5  1   -108.9964803744  1.5780E-04  4.1659E-05  1.1482E-02  1.0000E-04
 mr-sdci #  6  1   -108.9965159082  3.5534E-05  1.3538E-05  6.5667E-03  1.0000E-04
 mr-sdci #  7  1   -108.9965292760  1.3368E-05  4.2699E-06  3.7649E-03  1.0000E-04
 mr-sdci #  8  1   -108.9965326384  3.3625E-06  1.5023E-06  2.0235E-03  1.0000E-04
 mr-sdci #  9  1   -108.9965343316  1.6931E-06  6.6385E-07  1.4661E-03  1.0000E-04
 mr-sdci # 10  1   -108.9965349628  6.3123E-07  1.9534E-07  8.2665E-04  1.0000E-04
 mr-sdci # 11  1   -108.9965351413  1.7853E-07  5.5126E-08  3.8926E-04  1.0000E-04
 mr-sdci # 12  1   -108.9965351852  4.3858E-08  1.7734E-08  2.2432E-04  1.0000E-04
 mr-sdci # 13  1   -108.9965351975  1.2324E-08  3.6634E-09  1.0776E-04  1.0000E-04
 mr-sdci # 14  1   -108.9965352004  2.8762E-09  3.4730E-10  3.3522E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after 14 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 14  1   -108.9965352004  2.8762E-09  3.4730E-10  3.3522E-05  1.0000E-04

 number of reference csfs (nref) is    59.  root number (iroot) is  1.
 c0**2 =   0.89059754  c**2 (all zwalks) =   0.90001503

 eref      =   -108.686199278648   "relaxed" cnot**2         =   0.890597540371
 eci       =   -108.996535200391   deltae = eci - eref       =  -0.310335921742
 eci+dv1   =   -109.030486713541   dv1 = (1-cnot**2)*deltae  =  -0.033951513150
 eci+dv2   =   -109.034657372989   dv2 = dv1 / cnot**2       =  -0.038122172598
 eci+dv3   =   -109.039996192385   dv3 = dv1 / (2*cnot**2-1) =  -0.043460991994
 eci+pople =   -109.029367504080   ( 10e- scaled deltae )    =  -0.343168225432
