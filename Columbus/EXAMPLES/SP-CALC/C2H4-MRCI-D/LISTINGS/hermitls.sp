
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "8000000             "

 Work memory size (LMWORK) :     8000000 =   61.04 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 2 and lower


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   10



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

  ethylen pvdz                                                            
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 3



                      SYMGRP:Point group information
                      ------------------------------

Point group: D2h

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane
      Reflection in the xy-plane

   * Group multiplication table

        |  E   C2z  C2y  C2x   i   Oxy  Oxz  Oyz
   -----+----------------------------------------
     E  |  E 
    C2z | C2z   E 
    C2y | C2y  C2x   E 
    C2x | C2x  C2y  C2z   E 
     i  |  i   Oxy  Oxz  Oyz   E 
    Oxy | Oxy   i   Oyz  Oxz  C2z   E 
    Oxz | Oxz  Oyz   i   Oxy  C2y  C2x   E 
    Oyz | Oyz  Oxz  Oxy   i   C2x  C2y  C2z   E 

   * Character table

        |  E   C2z  C2y  C2x   i   Oxy  Oxz  Oyz
   -----+----------------------------------------
    Ag  |   1    1    1    1    1    1    1    1
    B3u |   1   -1   -1    1   -1    1    1   -1
    B2u |   1   -1    1   -1   -1    1   -1    1
    B1g |   1    1   -1   -1    1    1   -1   -1
    B1u |   1    1   -1   -1   -1   -1    1    1
    B2g |   1   -1    1   -1    1   -1    1   -1
    B3g |   1   -1   -1    1    1   -1   -1    1
    Au  |   1    1    1    1   -1   -1   -1   -1

   * Direct product table

        | Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
   -----+----------------------------------------
    Ag  | Ag 
    B3u | B3u  Ag 
    B2u | B2u  B1g  Ag 
    B1g | B1g  B2u  B3u  Ag 
    B1u | B1u  B2g  B3g  Au   Ag 
    B2g | B2g  B1u  Au   B3g  B3u  Ag 
    B3g | B3g  Au   B1u  B2g  B2u  B1g  Ag 
    Au  | Au   B3g  B2g  B1u  B1g  B2u  B3u  Ag 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    6

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        2       6      26      14      [9s4p1d|3s2p1d]                        
  H  1        4       1       7       5      [4s1p|2s1p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      6      16      80      48

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 18


   1   C  1 1   x      0.0000000000
   2            y      0.0000000000
   3            z      1.2530600000

   4   C  1 2   x      0.0000000000
   5            y      0.0000000000
   6            z     -1.2530600000

   7   H  1 1   x      1.7464600000
   8            y      0.0000000000
   9            z      2.3750000000

  10   H  1 2   x     -1.7464600000
  11            y      0.0000000000
  12            z      2.3750000000

  13   H  1 3   x      1.7464600000
  14            y      0.0000000000
  15            z     -2.3750000000

  16   H  1 4   x     -1.7464600000
  17            y      0.0000000000
  18            z     -2.3750000000



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   3  3  2  1  3  3  2  1


  Symmetry 1

   1   C  1  z    [ 3  -  6 ]/2
   2   H  1  x    [ 7  - 10  + 13  - 16 ]/4
   3   H  1  z    [ 9  + 12  - 15  - 18 ]/4


  Symmetry 2

   4   C  1  x    [ 1  +  4 ]/2
   5   H  1  x    [ 7  + 10  + 13  + 16 ]/4
   6   H  1  z    [ 9  - 12  - 15  + 18 ]/4


  Symmetry 3

   7   C  1  y    [ 2  +  5 ]/2
   8   H  1  y    [ 8  + 11  + 14  + 17 ]/4


  Symmetry 4

   9   H  1  y    [ 8  - 11  + 14  - 17 ]/4


  Symmetry 5

  10   C  1  z    [ 3  +  6 ]/2
  11   H  1  x    [ 7  - 10  - 13  + 16 ]/4
  12   H  1  z    [ 9  + 12  + 15  + 18 ]/4


  Symmetry 6

  13   C  1  x    [ 1  -  4 ]/2
  14   H  1  x    [ 7  + 10  - 13  - 16 ]/4
  15   H  1  z    [ 9  - 12  + 15  - 18 ]/4


  Symmetry 7

  16   C  1  y    [ 2  -  5 ]/2
  17   H  1  y    [ 8  + 11  - 14  - 17 ]/4


  Symmetry 8

  18   H  1  y    [ 8  - 11  - 14  + 17 ]/4


   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        C  2        H  1        H  2        H  3        H  4

   C  1    0.000000
   C  2    1.326181    0.000000
   H  1    1.098456    2.130747    0.000000
   H  2    1.098456    2.130747    1.848373    0.000000
   H  3    2.130747    1.098456    2.513591    3.120036    0.000000
   H  4    2.130747    1.098456    3.120036    2.513591    1.848373    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    C  2       C  1                           1.326181
  bond distance:    H  1       C  1                           1.098456
  bond distance:    H  2       C  1                           1.098456
  bond distance:    H  3       C  2                           1.098456
  bond distance:    H  4       C  2                           1.098456


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       C  2                 122.717
  bond angle:       H  2       C  1       C  2                 122.717
  bond angle:       H  2       C  1       H  1                 114.566
  bond angle:       H  3       C  2       C  1                 122.717
  bond angle:       H  4       C  2       C  1                 122.717
  bond angle:       H  4       C  2       H  3                 114.566


  Nuclear repulsion energy :   33.220060517325


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1#1 1s    1     6665.000000    0.0007 -0.0001  0.0000
   gen. cont.  2     1000.000000    0.0053 -0.0012  0.0000
               3      228.000000    0.0271 -0.0057  0.0000
               4       64.710000    0.1017 -0.0233  0.0000
               5       21.060000    0.2747 -0.0640  0.0000
               6        7.495000    0.4486 -0.1500  0.0000
               7        2.797000    0.2851 -0.1273  0.0000
               8        0.521500    0.0152  0.5445  0.0000
               9        0.159600   -0.0032  0.5805  1.0000

  C  1#2 1s   10     6665.000000    0.0007 -0.0001  0.0000
   gen. cont. 11     1000.000000    0.0053 -0.0012  0.0000
              12      228.000000    0.0271 -0.0057  0.0000
              13       64.710000    0.1017 -0.0233  0.0000
              14       21.060000    0.2747 -0.0640  0.0000
              15        7.495000    0.4486 -0.1500  0.0000
              16        2.797000    0.2851 -0.1273  0.0000
              17        0.521500    0.0152  0.5445  0.0000
              18        0.159600   -0.0032  0.5805  1.0000

  C  1#1 2px  19        9.439000    0.0381  0.0000
   gen. cont. 20        2.002000    0.2095  0.0000
              21        0.545600    0.5086  0.0000
              22        0.151700    0.4688  1.0000

  C  1#2 2px  23        9.439000    0.0381  0.0000
   gen. cont. 24        2.002000    0.2095  0.0000
              25        0.545600    0.5086  0.0000
              26        0.151700    0.4688  1.0000

  C  1#1 2py  27        9.439000    0.0381  0.0000
   gen. cont. 28        2.002000    0.2095  0.0000
              29        0.545600    0.5086  0.0000
              30        0.151700    0.4688  1.0000

  C  1#2 2py  31        9.439000    0.0381  0.0000
   gen. cont. 32        2.002000    0.2095  0.0000
              33        0.545600    0.5086  0.0000
              34        0.151700    0.4688  1.0000

  C  1#1 2pz  35        9.439000    0.0381  0.0000
   gen. cont. 36        2.002000    0.2095  0.0000
              37        0.545600    0.5086  0.0000
              38        0.151700    0.4688  1.0000

  C  1#2 2pz  39        9.439000    0.0381  0.0000
   gen. cont. 40        2.002000    0.2095  0.0000
              41        0.545600    0.5086  0.0000
              42        0.151700    0.4688  1.0000

  C  1#1 3d2- 43        0.550000    1.0000

  C  1#2 3d2- 44        0.550000    1.0000

  C  1#1 3d1- 45        0.550000    1.0000

  C  1#2 3d1- 46        0.550000    1.0000

  C  1#1 3d0  47        0.550000    1.0000

  C  1#2 3d0  48        0.550000    1.0000

  C  1#1 3d1+ 49        0.550000    1.0000

  C  1#2 3d1+ 50        0.550000    1.0000

  C  1#1 3d2+ 51        0.550000    1.0000

  C  1#2 3d2+ 52        0.550000    1.0000

  H  1#1 1s   53       13.010000    0.0197  0.0000
   gen. cont. 54        1.962000    0.1380  0.0000
              55        0.444600    0.4781  0.0000
              56        0.122000    0.5012  1.0000

  H  1#2 1s   57       13.010000    0.0197  0.0000
   gen. cont. 58        1.962000    0.1380  0.0000
              59        0.444600    0.4781  0.0000
              60        0.122000    0.5012  1.0000

  H  1#3 1s   61       13.010000    0.0197  0.0000
   gen. cont. 62        1.962000    0.1380  0.0000
              63        0.444600    0.4781  0.0000
              64        0.122000    0.5012  1.0000

  H  1#4 1s   65       13.010000    0.0197  0.0000
   gen. cont. 66        1.962000    0.1380  0.0000
              67        0.444600    0.4781  0.0000
              68        0.122000    0.5012  1.0000

  H  1#1 2px  69        0.727000    1.0000

  H  1#2 2px  70        0.727000    1.0000

  H  1#3 2px  71        0.727000    1.0000

  H  1#4 2px  72        0.727000    1.0000

  H  1#1 2py  73        0.727000    1.0000

  H  1#2 2py  74        0.727000    1.0000

  H  1#3 2py  75        0.727000    1.0000

  H  1#4 2py  76        0.727000    1.0000

  H  1#1 2pz  77        0.727000    1.0000

  H  1#2 2pz  78        0.727000    1.0000

  H  1#3 2pz  79        0.727000    1.0000

  H  1#4 2pz  80        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  C  1#1  1s     1   2   3   4   5   6   7   8   9
   2  C  1#2  1s    10  11  12  13  14  15  16  17  18
   3  C  1#1  1s     1   2   3   4   5   6   7   8   9
   4  C  1#2  1s    10  11  12  13  14  15  16  17  18
   5  C  1#1  1s     9
   6  C  1#2  1s    18
   7  C  1#1  2px   19  20  21  22
   8  C  1#2  2px   23  24  25  26
   9  C  1#1  2py   27  28  29  30
  10  C  1#2  2py   31  32  33  34
  11  C  1#1  2pz   35  36  37  38
  12  C  1#2  2pz   39  40  41  42
  13  C  1#1  2px   22
  14  C  1#2  2px   26
  15  C  1#1  2py   30
  16  C  1#2  2py   34
  17  C  1#1  2pz   38
  18  C  1#2  2pz   42
  19  C  1#1  3d2-  43
  20  C  1#2  3d2-  44
  21  C  1#1  3d1-  45
  22  C  1#2  3d1-  46
  23  C  1#1  3d0   47
  24  C  1#2  3d0   48
  25  C  1#1  3d1+  49
  26  C  1#2  3d1+  50
  27  C  1#1  3d2+  51
  28  C  1#2  3d2+  52
  29  H  1#1  1s    53  54  55  56
  30  H  1#2  1s    57  58  59  60
  31  H  1#3  1s    61  62  63  64
  32  H  1#4  1s    65  66  67  68
  33  H  1#1  1s    56
  34  H  1#2  1s    60
  35  H  1#3  1s    64
  36  H  1#4  1s    68
  37  H  1#1  2px   69
  38  H  1#2  2px   70
  39  H  1#3  2px   71
  40  H  1#4  2px   72
  41  H  1#1  2py   73
  42  H  1#2  2py   74
  43  H  1#3  2py   75
  44  H  1#4  2py   76
  45  H  1#1  2pz   77
  46  H  1#2  2pz   78
  47  H  1#3  2pz   79
  48  H  1#4  2pz   80




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        11  7  4  2 11  7  4  2


  Symmetry  Ag ( 1)

    1     C  1     1s         1  +   2
    2     C  1     1s         3  +   4
    3     C  1     1s         5  +   6
    4     C  1     2pz       11  -  12
    5     C  1     2pz       17  -  18
    6     C  1     3d0       23  +  24
    7     C  1     3d2+      27  +  28
    8     H  1     1s        29  +  30  +  31  +  32
    9     H  1     1s        33  +  34  +  35  +  36
   10     H  1     2px       37  -  38  +  39  -  40
   11     H  1     2pz       45  +  46  -  47  -  48


  Symmetry  B3u( 2)

   12     C  1     2px        7  +   8
   13     C  1     2px       13  +  14
   14     C  1     3d1+      25  -  26
   15     H  1     1s        29  -  30  +  31  -  32
   16     H  1     1s        33  -  34  +  35  -  36
   17     H  1     2px       37  +  38  +  39  +  40
   18     H  1     2pz       45  -  46  -  47  +  48


  Symmetry  B2u( 3)

   19     C  1     2py        9  +  10
   20     C  1     2py       15  +  16
   21     C  1     3d1-      21  -  22
   22     H  1     2py       41  +  42  +  43  +  44


  Symmetry  B1g( 4)

   23     C  1     3d2-      19  +  20
   24     H  1     2py       41  -  42  +  43  -  44


  Symmetry  B1u( 5)

   25     C  1     1s         1  -   2
   26     C  1     1s         3  -   4
   27     C  1     1s         5  -   6
   28     C  1     2pz       11  +  12
   29     C  1     2pz       17  +  18
   30     C  1     3d0       23  -  24
   31     C  1     3d2+      27  -  28
   32     H  1     1s        29  +  30  -  31  -  32
   33     H  1     1s        33  +  34  -  35  -  36
   34     H  1     2px       37  -  38  -  39  +  40
   35     H  1     2pz       45  +  46  +  47  +  48


  Symmetry  B2g( 6)

   36     C  1     2px        7  -   8
   37     C  1     2px       13  -  14
   38     C  1     3d1+      25  +  26
   39     H  1     1s        29  -  30  -  31  +  32
   40     H  1     1s        33  -  34  -  35  +  36
   41     H  1     2px       37  +  38  -  39  -  40
   42     H  1     2pz       45  -  46  +  47  -  48


  Symmetry  B3g( 7)

   43     C  1     2py        9  -  10
   44     C  1     2py       15  -  16
   45     C  1     3d1-      21  +  22
   46     H  1     2py       41  +  42  -  43  -  44


  Symmetry  Au ( 8)

   47     C  1     3d2-      19  -  20
   48     H  1     2py       41  -  42  -  43  +  44

  Symmetries of electric field:  B3u(2)  B2u(3)  B1u(5)

  Symmetries of magnetic field:  B3g(7)  B2g(6)  B1g(4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
ethylen pvdz                                                                    
                                                                                
s   2    3 X  Y  Z   0.10E-14                                                   
       6.0    1    3    1    1    1                                             
C  1   0.000000000000000   0.000000000000000   1.253060000000000       *        
H   9   3                                                                       
       6665.00000000         0.00069200        -0.00014600         0.00000000   
       1000.00000000         0.00532900        -0.00115400         0.00000000   
        228.00000000         0.02707700        -0.00572500         0.00000000   
         64.71000000         0.10171800        -0.02331200         0.00000000   
         21.06000000         0.27474000        -0.06395500         0.00000000   
          7.49500000         0.44856400        -0.14998100         0.00000000   
          2.79700000         0.28507400        -0.12726200         0.00000000   
          0.52150000         0.01520400         0.54452900         0.00000000   
          0.15960000        -0.00319100         0.58049600         1.00000000   
H   4   2                                                                       
          9.43900000         0.03810900         0.00000000                      
          2.00200000         0.20948000         0.00000000                      
          0.54560000         0.50855700         0.00000000                      
          0.15170000         0.46884200         1.00000000                      
H   1   1                                                                       
          0.55000000         1.00000000                                         
       1.0    1    2    1    1                                                  
H  1   1.746460000000000   0.000000000000000   2.375000000000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         


 herdrv: noofopt= 2


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T 1


   202 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  17.18
 prop, itype T 2


   214 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  18.20
 prop, itype T 3


   202 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  17.18

 >>> Time used in ONEDRV is   0.01 seconds

 inttyp= 1noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+


 >>> Time used in OVERLA is   0.00 seconds

 finopt,noofopt,last1= 0 2 0
 inttyp= 8noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+


 >>> Time used in CARMOM is   0.00 seconds

 finopt,noofopt,last1= 1 2 0
 inttyp= 8noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=  1typeb=  0last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=  1typeb=  1last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=  1typeb=  2last= 2
 lstflg= 1

 >>> Time used in CARMOM is   0.01 seconds

 finopt,noofopt,last1= 2 2 2
 inttyp= 8noptyp= 6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=  1typeb=  3last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=  1typeb=  4last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=  1typeb=  5last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=  1typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=  1typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=  1typeb=  8last= 2
 lstflg= 2

 >>> Time used in CARMOM is   0.01 seconds



 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 2674 2 33

 Number of two-electron integrals written:     92764 (13.4%)
 Kilobytes written:                             1114



 >>> Time used in TWOINT is   0.61 seconds

 >>>> Total CPU  time used in HERMIT:   0.66 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
