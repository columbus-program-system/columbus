1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  grimming2       Sun Sep  4 00:36:46 2016
 cidrt_title                                                                     
 ethylen                                                                         
 mofmt: formatted orbitals label=morbl   grimming2       Sun Sep  4 00:36:48 2016
 SIFS file created by program tran.      grimming2       Sun Sep  4 00:36:48 2016

 formula file title:
 Hermit Integral Program : SIFS version  grimming2       Sun Sep  4 00:36:46 2016
 cidrt_title                                                                     
 ethylen                                                                         
 mofmt: formatted orbitals label=morbl   grimming2       Sun Sep  4 00:36:48 2016
 SIFS file created by program tran.      grimming2       Sun Sep  4 00:36:48 2016
 cidrt_title                                                                     

 297 dimension of the ci-matrix ->>>     14558


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    13)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.0668950712  4.2188E-15  3.0752E-01  9.4916E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -78.0668950712  4.2188E-15  3.0752E-01  9.4916E-01  1.0000E-04

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.3235452853  2.5665E-01  1.1026E-02  1.6980E-01  1.0000E-04
 mr-sdci #  2  1    -78.3319064116  8.3611E-03  1.0871E-03  5.3064E-02  1.0000E-04
 mr-sdci #  3  1    -78.3327676520  8.6124E-04  5.3248E-05  1.2049E-02  1.0000E-04
 mr-sdci #  4  1    -78.3328230808  5.5429E-05  3.3550E-06  3.1297E-03  1.0000E-04
 mr-sdci #  5  1    -78.3328264755  3.3947E-06  3.0605E-07  8.9275E-04  1.0000E-04
 mr-sdci #  6  1    -78.3328268118  3.3626E-07  4.1085E-08  3.2933E-04  1.0000E-04
 mr-sdci #  7  1    -78.3328268592  4.7453E-08  5.0714E-09  1.1755E-04  1.0000E-04
 mr-sdci #  8  1    -78.3328268645  5.2171E-09  4.6117E-10  3.5878E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  8  1    -78.3328268645  5.2171E-09  4.6117E-10  3.5878E-05  1.0000E-04

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 eref      =    -78.066304920090   "relaxed" cnot**2         =   0.912510698863
 eci       =    -78.332826864464   deltae = eci - eref       =  -0.266521944374
 eci+dv1   =    -78.356144683115   dv1 = (1-cnot**2)*deltae  =  -0.023317818651
 eci+dv2   =    -78.358380338719   dv2 = dv1 / cnot**2       =  -0.025553474255
 eci+dv3   =    -78.361090154017   dv3 = dv1 / (2*cnot**2-1) =  -0.028263289553
 eci+pople =    -78.356890487032   ( 16e- scaled deltae )    =  -0.290585566942
