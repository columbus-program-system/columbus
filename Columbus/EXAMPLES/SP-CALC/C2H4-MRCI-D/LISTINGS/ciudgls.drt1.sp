1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.50 MB location: local disk    
three external inte    0.50 MB location: local disk    
four external integ    0.50 MB location: local disk    
three external inte    0.50 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.22 MB location: local disk    
 nsubmx= 16 lenci= 14558
global arrays:       480414   (    3.67 MB)
vdisk:                    0   (    0.00 MB)
drt:                 786835   (    3.00 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 0,
   RTOLCI=0.00010000,
   VOUT  = 1,
   NBKITR= 1,
   NITER= 20,
  IDEN=   1,
   IVMODE=   3,
   NVCIMX = 16
  &end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   20
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =   1      ctol   = 1.00E-02  davcor  =   1


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108586504 ifirst=-264078644

 integral file titles:
 Hermit Integral Program : SIFS version  grimming2       Sun Sep  4 00:36:46 2016
 cidrt_title                                                                     
 ethylen                                                                         
 mofmt: formatted orbitals label=morbl   grimming2       Sun Sep  4 00:36:48 2016
 SIFS file created by program tran.      grimming2       Sun Sep  4 00:36:48 2016

 core energy values from the integral file:
 energy( 1)=  3.322006051733E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.100513157221E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.683125520480E+01

 drt header information:
 cidrt_title                                                                     
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    48 niot  =     9 nfct  =     0 nfvt  =     0
 nrow  =    62 nsym  =     8 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        268       13      128       56       71
 nvalwt,nvalw:      268       13      128       56       71
 ncsft:           14558
 total number of valid internal walks:     268
 nvalz,nvaly,nvalx,nvalw =       13     128      56      71

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext  1560   702    90
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    45750    39009    14913     2850      252        0        0        0
 minbl4,minbl3,maxbl2   420   411   423
 maxbuf 32767
 number of external orbitals per symmetry block:   8   6   3   2   9   6   3   2
 nmsym   8 number of internal orbitals   9

 formula file title:
 Hermit Integral Program : SIFS version  grimming2       Sun Sep  4 00:36:46 2016
 cidrt_title                                                                     
 ethylen                                                                         
 mofmt: formatted orbitals label=morbl   grimming2       Sun Sep  4 00:36:48 2016
 SIFS file created by program tran.      grimming2       Sun Sep  4 00:36:48 2016
 cidrt_title                                                                     
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:    13   128    56    71 total internal walks:     268
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 5 5 6 3 7

 setref:        2 references kept,
                0 references were marked as invalid, out of
                2 total.
 limcnvrt: found 13 valid internal walksout of  13
  walks (skipping trailing invalids)
  ... adding  13 segmentation marks segtype= 1
 limcnvrt: found 128 valid internal walksout of  128
  walks (skipping trailing invalids)
  ... adding  128 segmentation marks segtype= 2
 limcnvrt: found 56 valid internal walksout of  56
  walks (skipping trailing invalids)
  ... adding  56 segmentation marks segtype= 3
 limcnvrt: found 71 valid internal walksout of  71
  walks (skipping trailing invalids)
  ... adding  71 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x     102     114      75      70     121     114      75      70
 vertex w     141     114      75      70     121     114      75      70



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          13|        13|         0|        13|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         128|       478|        13|       128|        13|         2|
 -------------------------------------------------------------------------------
  X 3          56|      5820|       491|        56|       141|         3|
 -------------------------------------------------------------------------------
  W 4          71|      8247|      6311|        71|       197|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>     14558


 297 dimension of the ci-matrix ->>>     14558


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      56      13       5820         13      56      13
     2  4   1    25      two-ext wz   2X  4 1      71      13       8247         13      71      13
     3  4   3    26      two-ext wx   2X  4 3      71      56       8247       5820      71      56
     4  2   1    11      one-ext yz   1X  2 1     128      13        478         13     128      13
     5  3   2    15      1ex3ex  yx   3X  3 2      56     128       5820        478      56     128
     6  4   2    16      1ex3ex  yw   3X  4 2      71     128       8247        478      71     128
     7  1   1     1      allint zz    OX  1 1      13      13         13         13      13      13
     8  2   2     5      0ex2ex yy    OX  2 2     128     128        478        478     128     128
     9  3   3     6      0ex2ex xx    OX  3 3      56      56       5820       5820      56      56
    10  4   4     7      0ex2ex ww    OX  4 4      71      71       8247       8247      71      71
    11  1   1    75      dg-024ext z  DG  1 1      13      13         13         13      13      13
    12  2   2    45      4exdg024 y   DG  2 2     128     128        478        478     128     128
    13  3   3    46      4exdg024 x   DG  3 3      56      56       5820       5820      56      56
    14  4   4    47      4exdg024 w   DG  4 4      71      71       8247       8247      71      71
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 1766 786 255
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        84 2x:         0 4x:         0
All internal counts: zz :       103 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       102    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        82    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       2

    root           eigenvalues
    ----           ------------
       1         -78.0668950712

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    13)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             14558
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:         0 xx:         0 ww:         0
One-external counts: yz :       668 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:        56 wz:        94 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:         0    task #   4:       695
task #   5:         0    task #   6:         0    task #   7:       102    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        82    task #  12:       430
task #  13:       139    task #  14:       175    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.0668950712  4.2188E-15  3.0752E-01  9.4916E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.02   0.00   0.00   0.01
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005859
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0664s 
time spent in multnx:                   0.0605s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0098s 
time for vector access in mult:         0.0039s 
total time per CI iteration:            0.0801s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.0668950712  4.2188E-15  3.0752E-01  9.4916E-01  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             14558
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95746487    -0.28854987

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -78.3235452853  2.5665E-01  1.1026E-02  1.6980E-01  1.0000E-04
 mr-sdci #  1  2    -75.2410681079 -1.5902E+00  0.0000E+00  1.2842E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.02   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001953
time for cinew                         0.009765
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3984s 
time spent in multnx:                   0.3926s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0410s 
time for vector access in mult:         0.0059s 
total time per CI iteration:            0.4180s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.95548293    -0.09174520     0.28041966

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -78.3319064116  8.3611E-03  1.0871E-03  5.3064E-02  1.0000E-04
 mr-sdci #  2  2    -75.8116403573  5.7057E-01  0.0000E+00  1.5320E+00  1.0000E-04
 mr-sdci #  2  3    -75.2315410182 -1.5997E+00  0.0000E+00  1.2268E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.02   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.005859
time for cinew                         0.013672
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3965s 
time spent in multnx:                   0.3867s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0391s 
time for vector access in mult:         0.0039s 
total time per CI iteration:            0.4219s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.95520088     0.09053978     0.03575006     0.27949197

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -78.3327676520  8.6124E-04  5.3248E-05  1.2049E-02  1.0000E-04
 mr-sdci #  3  2    -75.8848534576  7.3213E-02  0.0000E+00  1.2516E+00  1.0000E-04
 mr-sdci #  3  3    -75.7355199796  5.0398E-01  0.0000E+00  1.6583E+00  1.0000E-04
 mr-sdci #  3  4    -75.2285738879 -1.6027E+00  0.0000E+00  1.2073E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.05   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.005859
time for cinew                         0.017578
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.4004s 
time spent in multnx:                   0.3887s 
integral transfer time:                 0.0020s 
time spent for loop construction:       0.0410s 
time for vector access in mult:         0.0039s 
total time per CI iteration:            0.4297s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.95488334    -0.11521209    -0.01501994    -0.11946473     0.24581815

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -78.3328230808  5.5429E-05  3.3550E-06  3.1297E-03  1.0000E-04
 mr-sdci #  4  2    -76.7019997376  8.1715E-01  0.0000E+00  1.0957E+00  1.0000E-04
 mr-sdci #  4  3    -75.7975441443  6.2024E-02  0.0000E+00  1.4452E+00  1.0000E-04
 mr-sdci #  4  4    -75.4360879811  2.0751E-01  0.0000E+00  1.1044E+00  1.0000E-04
 mr-sdci #  4  5    -74.9838470679 -1.8474E+00  0.0000E+00  1.5591E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.04
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.007812
time for cinew                         0.021484
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3965s 
time spent in multnx:                   0.3906s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0391s 
time for vector access in mult:         0.0039s 
total time per CI iteration:            0.4316s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.95484565    -0.09509572    -0.03804433    -0.12280190     0.05197016    -0.24494493

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -78.3328264755  3.3947E-06  3.0605E-07  8.9275E-04  1.0000E-04
 mr-sdci #  5  2    -76.9939495818  2.9195E-01  0.0000E+00  8.9456E-01  1.0000E-04
 mr-sdci #  5  3    -75.8060950599  8.5509E-03  0.0000E+00  1.4192E+00  1.0000E-04
 mr-sdci #  5  4    -75.6247098577  1.8862E-01  0.0000E+00  1.1723E+00  1.0000E-04
 mr-sdci #  5  5    -75.3319277873  3.4808E-01  0.0000E+00  1.3296E+00  1.0000E-04
 mr-sdci #  5  6    -74.9790121783 -1.8522E+00  0.0000E+00  1.5303E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009766
time for cinew                         0.025390
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3984s 
time spent in multnx:                   0.3887s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0410s 
time for vector access in mult:         0.0039s 
total time per CI iteration:            0.4395s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.95483707     0.06327244     0.10180289    -0.12377030     0.06655636    -0.02485850     0.23141338

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -78.3328268118  3.3626E-07  4.1085E-08  3.2933E-04  1.0000E-04
 mr-sdci #  6  2    -77.3621852390  3.6824E-01  0.0000E+00  7.7768E-01  1.0000E-04
 mr-sdci #  6  3    -76.1633790356  3.5728E-01  0.0000E+00  1.1369E+00  1.0000E-04
 mr-sdci #  6  4    -75.6276212690  2.9114E-03  0.0000E+00  1.1716E+00  1.0000E-04
 mr-sdci #  6  5    -75.4210819281  8.9154E-02  0.0000E+00  1.1650E+00  1.0000E-04
 mr-sdci #  6  6    -75.2677547364  2.8874E-01  0.0000E+00  1.4192E+00  1.0000E-04
 mr-sdci #  6  7    -74.9094638946 -1.9218E+00  0.0000E+00  1.6124E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.04
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009765
time for cinew                         0.029298
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3984s 
time spent in multnx:                   0.3887s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0391s 
time for vector access in mult:         0.0059s 
total time per CI iteration:            0.4434s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95483514    -0.04115633    -0.09945188    -0.11791106    -0.09540743     0.02795774     0.04961520    -0.22462511

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -78.3328268592  4.7453E-08  5.0714E-09  1.1755E-04  1.0000E-04
 mr-sdci #  7  2    -77.6207362553  2.5855E-01  0.0000E+00  5.5825E-01  1.0000E-04
 mr-sdci #  7  3    -76.5740851557  4.1071E-01  0.0000E+00  8.2448E-01  1.0000E-04
 mr-sdci #  7  4    -75.6285975570  9.7629E-04  0.0000E+00  1.1699E+00  1.0000E-04
 mr-sdci #  7  5    -75.4363867071  1.5305E-02  0.0000E+00  1.1130E+00  1.0000E-04
 mr-sdci #  7  6    -75.3770107417  1.0926E-01  0.0000E+00  1.3927E+00  1.0000E-04
 mr-sdci #  7  7    -75.0782663646  1.6880E-01  0.0000E+00  1.3822E+00  1.0000E-04
 mr-sdci #  7  8    -74.9032064333 -1.9280E+00  0.0000E+00  1.5674E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.01   0.01   0.00   0.01
    5   15    0   0.04   0.01   0.00   0.04
    6   16    0   0.05   0.01   0.00   0.05
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.04   0.00   0.00   0.04
   10    7    0   0.05   0.00   0.00   0.05
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.04   0.00   0.00   0.03
   14   47    0   0.04   0.00   0.00   0.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.013672
time for cinew                         0.027343
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3984s 
time spent in multnx:                   0.3887s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0371s 
time for vector access in mult:         0.0039s 
total time per CI iteration:            0.4453s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1766 2x:       786 4x:       255
All internal counts: zz :       103 yy:      1477 xx:       225 ww:       368
One-external counts: yz :       668 yx:      2758 yw:      3166
Two-external counts: yy :      2208 ww:       637 xx:       497 xz:        56 wz:        94 wx:       910
Three-ext.   counts: yx :       898 yw:      1046

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        34    task #   2:        65    task #   3:       475    task #   4:       695
task #   5:      2540    task #   6:      2899    task #   7:       102    task #   8:      1112
task #   9:       139    task #  10:       233    task #  11:        82    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95483439     0.03561211     0.08490064    -0.13005210    -0.06245879    -0.09751555     0.04745072    -0.05334347

                ci   9
 ref:   1    -0.21069863

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -78.3328268645  5.2171E-09  4.6117E-10  3.5878E-05  1.0000E-04
 mr-sdci #  8  2    -77.7189084810  9.8172E-02  0.0000E+00  3.8464E-01  1.0000E-04
 mr-sdci #  8  3    -76.7625539178  1.8847E-01  0.0000E+00  6.7181E-01  1.0000E-04
 mr-sdci #  8  4    -75.7087592479  8.0162E-02  0.0000E+00  1.2482E+00  1.0000E-04
 mr-sdci #  8  5    -75.6051468359  1.6876E-01  0.0000E+00  1.1187E+00  1.0000E-04
 mr-sdci #  8  6    -75.4362337757  5.9223E-02  0.0000E+00  1.1166E+00  1.0000E-04
 mr-sdci #  8  7    -75.2326168700  1.5435E-01  0.0000E+00  1.2885E+00  1.0000E-04
 mr-sdci #  8  8    -75.0781502254  1.7494E-01  0.0000E+00  1.3874E+00  1.0000E-04
 mr-sdci #  8  9    -74.8407913922 -1.9905E+00  0.0000E+00  1.6698E+00  1.0000E-04


 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -78.3328268645  5.2171E-09  4.6117E-10  3.5878E-05  1.0000E-04
 mr-sdci #  8  2    -77.7189084810  9.8172E-02  0.0000E+00  3.8464E-01  1.0000E-04
 mr-sdci #  8  3    -76.7625539178  1.8847E-01  0.0000E+00  6.7181E-01  1.0000E-04
 mr-sdci #  8  4    -75.7087592479  8.0162E-02  0.0000E+00  1.2482E+00  1.0000E-04
 mr-sdci #  8  5    -75.6051468359  1.6876E-01  0.0000E+00  1.1187E+00  1.0000E-04
 mr-sdci #  8  6    -75.4362337757  5.9223E-02  0.0000E+00  1.1166E+00  1.0000E-04
 mr-sdci #  8  7    -75.2326168700  1.5435E-01  0.0000E+00  1.2885E+00  1.0000E-04
 mr-sdci #  8  8    -75.0781502254  1.7494E-01  0.0000E+00  1.3874E+00  1.0000E-04
 mr-sdci #  8  9    -74.8407913922 -1.9905E+00  0.0000E+00  1.6698E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -78.332826864464

################END OF CIUDGINFO################


    1 of the  10 expansion vectors are transformed.
    1 of the   9 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.954834393)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -78.3328268645

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9

                                          orbital     1    2    3   12   25   26   36   19   43

                                         symmetry   ag   ag   ag  b3u  b1u  b1u  b2g  b2u  b3g 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.940211                        +-   +-   +-   +-   +-   +-   +-   +-      
 z*  1  1       2  0.168862                        +-   +-   +-   +-   +-   +-   +-        +- 
 z   1  1       4  0.020656                        +-   +-   +-   +-   +-        +-   +-   +- 
 z   1  1      10  0.012546                        +-        +-   +-   +-   +-   +-   +-   +- 
 y   1  1      27 -0.031018              2(b3u )   +-   +-   +-   +-   +-   +-    -   +     - 
 y   1  1      28  0.013504              3(b3u )   +-   +-   +-   +-   +-   +-    -   +     - 
 y   1  1      38 -0.012594              1(b3u )   +-   +-   +-   +-   +-   +-   +     -    - 
 y   1  1      59  0.028462              1( ag )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      62 -0.020558              4( ag )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      80  0.018280              1( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      82  0.033394              3( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      84  0.014071              5( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      85  0.013420              6( ag )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      88  0.021091              1( au )   +-   +-   +-   +-   +-   +     -   +-    - 
 y   1  1     164  0.024803              1(b2g )   +-   +-   +-    -   +-   +-   +-   +     - 
 y   1  1     165  0.020213              2(b2g )   +-   +-   +-    -   +-   +-   +-   +     - 
 y   1  1     182 -0.019110              1(b1g )   +-   +-   +-    -   +-   +    +-   +-    - 
 y   1  1     202 -0.010701              1(b1g )   +-   +-   +-   +    +-    -   +-   +-    - 
 y   1  1     225  0.031092              2(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     226  0.024232              3(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     227  0.040535              4(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     228  0.013932              5(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     229 -0.010335              6(b1u )   +-   +-    -   +-   +-   +-   +-   +     - 
 y   1  1     246  0.019829              2(b2u )   +-   +-    -   +-   +-   +    +-   +-    - 
 y   1  1     275  0.012621              2(b2u )   +-   +-   +    +-   +-    -   +-   +-    - 
 y   1  1     304  0.020747              1(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     305  0.014665              2(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     306  0.019998              3(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     307  0.010456              4(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     308  0.017625              5(b1u )   +-    -   +-   +-   +-   +-   +-   +     - 
 y   1  1     347  0.020467              1(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     348  0.011798              2(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     349  0.034472              3(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     352 -0.011751              6(b1u )   +-   +    +-   +-   +-   +-   +-    -    - 
 y   1  1     356  0.015556              1(b1g )   +-   +    +-   +-   +-   +-    -   +-    - 
 y   1  1     361 -0.012287              2(b2u )   +-   +    +-   +-   +-    -   +-   +-    - 
 y   1  1     372 -0.014357              1( au )   +-   +    +-    -   +-   +-   +-   +-    - 
 x   1  1     496 -0.014488    1(b1g )   3(b1u )   +-   +-   +-   +-   +-   +-    -    -      
 x   1  1     510 -0.016100    1(b2u )   1(b2g )   +-   +-   +-   +-   +-   +-    -    -      
 x   1  1     513 -0.018147    1(b2u )   2(b2g )   +-   +-   +-   +-   +-   +-    -    -      
 x   1  1     548  0.018386    3( ag )   1( au )   +-   +-   +-   +-   +-   +-    -    -      
 x   1  1     632  0.013068    1(b2u )   1(b1u )   +-   +-   +-   +-   +-    -   +-    -      
 x   1  1     639  0.011273    2(b2u )   3(b1u )   +-   +-   +-   +-   +-    -   +-    -      
 x   1  1    1592 -0.018949    1(b3u )   1(b2u )   +-   +-   +-    -   +-   +-   +-    -      
 x   1  1    1594  0.017145    3(b3u )   1(b2u )   +-   +-   +-    -   +-   +-   +-    -      
 x   1  1    1599  0.011640    2(b3u )   2(b2u )   +-   +-   +-    -   +-   +-   +-    -      
 x   1  1    1612 -0.021876    3( ag )   1(b1g )   +-   +-   +-    -   +-   +-   +-    -      
 x   1  1    1614 -0.014179    5( ag )   1(b1g )   +-   +-   +-    -   +-   +-   +-    -      
 x   1  1    1646 -0.011009    3(b1u )   1( au )   +-   +-   +-    -   +-   +-   +-    -      
 x   1  1    1804  0.010109    1(b3u )   1(b2g )   +-   +-   +-    -   +-   +-    -   +-      
 x   1  1    1981  0.011103    2(b3u )   2(b1u )   +-   +-   +-    -   +-    -   +-   +-      
 x   1  1    2430  0.011883    1( ag )   1(b2u )   +-   +-    -   +-   +-   +-   +-    -      
 x   1  1    2431  0.020177    2( ag )   1(b2u )   +-   +-    -   +-   +-   +-   +-    -      
 x   1  1    2432  0.015290    3( ag )   1(b2u )   +-   +-    -   +-   +-   +-   +-    -      
 x   1  1    2440  0.015981    3( ag )   2(b2u )   +-   +-    -   +-   +-   +-   +-    -      
 x   1  1    2443  0.011126    6( ag )   2(b2u )   +-   +-    -   +-   +-   +-   +-    -      
 x   1  1    2455 -0.012960    2(b3u )   1(b1g )   +-   +-    -   +-   +-   +-   +-    -      
 x   1  1    2586  0.010219    1(b3u )   2(b1u )   +-   +-    -   +-   +-   +-    -   +-      
 x   1  1    2587 -0.020215    2(b3u )   2(b1u )   +-   +-    -   +-   +-   +-    -   +-      
 x   1  1    2599 -0.014927    2(b3u )   4(b1u )   +-   +-    -   +-   +-   +-    -   +-      
 x   1  1    2635 -0.010897    2( ag )   1(b2g )   +-   +-    -   +-   +-   +-    -   +-      
 x   1  1    2643 -0.015423    2( ag )   2(b2g )   +-   +-    -   +-   +-   +-    -   +-      
 x   1  1    3293  0.020488    2( ag )   1(b3u )   +-   +-    -    -   +-   +-   +-   +-      
 x   1  1    3301 -0.014615    2( ag )   2(b3u )   +-   +-    -    -   +-   +-   +-   +-      
 x   1  1    3309 -0.015657    2( ag )   3(b3u )   +-   +-    -    -   +-   +-   +-   +-      
 x   1  1    3341  0.010357    2(b2u )   1(b1g )   +-   +-    -    -   +-   +-   +-   +-      
 x   1  1    3523  0.010556    4( ag )   1(b2u )   +-    -   +-   +-   +-   +-   +-    -      
 w   1  1    6317  0.014860    3( ag )   3( ag )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6324  0.011184    3( ag )   5( ag )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6369  0.022921    1(b2u )   1(b2u )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6371  0.011103    2(b2u )   2(b2u )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6375  0.011701    1(b1g )   1(b1g )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6383  0.010770    3(b1u )   3(b1u )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    6455  0.010462    1(b1g )   2(b1u )   +-   +-   +-   +-   +-   +-   +     -      
 w   1  1    6471 -0.012338    1(b2u )   1(b2g )   +-   +-   +-   +-   +-   +-   +     -      
 w   1  1    6474 -0.015170    1(b2u )   2(b2g )   +-   +-   +-   +-   +-   +-   +     -      
 w   1  1    6598  0.016288    3( ag )   3( ag )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6629  0.013863    1(b3u )   1(b3u )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6630 -0.011106    1(b3u )   2(b3u )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6631  0.021839    2(b3u )   2(b3u )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6632 -0.011837    1(b3u )   3(b3u )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6662  0.010141    1(b1u )   3(b1u )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6664  0.016164    3(b1u )   3(b1u )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6704  0.012938    1(b2g )   1(b2g )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6705  0.015042    1(b2g )   2(b2g )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6706  0.017395    2(b2g )   2(b2g )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6731  0.011301    1( au )   1( au )   +-   +-   +-   +-   +-   +-        +-      
 w   1  1    6875  0.013984    1(b2u )   1(b1u )   +-   +-   +-   +-   +-   +    +-    -      
 w   1  1    6887  0.010306    1(b2u )   5(b1u )   +-   +-   +-   +-   +-   +    +-    -      
 w   1  1    7025 -0.010792    1( ag )   1(b3u )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7035  0.020209    3( ag )   2(b3u )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7044 -0.010360    4( ag )   3(b3u )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7074 -0.011051    2(b2u )   1(b1g )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7079 -0.015048    1(b1u )   1(b2g )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7088 -0.017709    1(b1u )   2(b2g )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7090 -0.017755    3(b1u )   2(b2g )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7099 -0.011065    3(b1u )   3(b2g )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7134 -0.011451    2(b3g )   1( au )   +-   +-   +-   +-   +-   +     -   +-      
 w   1  1    7253  0.010159    1( ag )   1( ag )   +-   +-   +-   +-   +-        +-   +-      
 w   1  1    7259 -0.012214    1( ag )   4( ag )   +-   +-   +-   +-   +-        +-   +-      
 w   1  1    7319  0.010553    1(b1u )   1(b1u )   +-   +-   +-   +-   +-        +-   +-      
 w   1  1    7325 -0.011248    1(b1u )   4(b1u )   +-   +-   +-   +-   +-        +-   +-      
 w   1  1    7328  0.011222    4(b1u )   4(b1u )   +-   +-   +-   +-   +-        +-   +-      
 w   1  1    8477  0.015215    1(b3u )   1(b2u )   +-   +-   +-   +    +-   +-   +-    -      
 w   1  1    8479 -0.015389    3(b3u )   1(b2u )   +-   +-   +-   +    +-   +-   +-    -      
 w   1  1    8617 -0.013396    1( ag )   1(b1u )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8626 -0.011790    2( ag )   2(b1u )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8627  0.013534    3( ag )   2(b1u )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8635 -0.024379    3( ag )   3(b1u )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8643  0.010495    3( ag )   4(b1u )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8689 -0.021162    1(b3u )   1(b2g )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8691  0.013489    3(b3u )   1(b2g )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8695 -0.027382    1(b3u )   2(b2g )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8696  0.014783    2(b3u )   2(b2g )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8697  0.024027    3(b3u )   2(b2g )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8702  0.019257    2(b3u )   3(b2g )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8734 -0.019846    1(b1g )   1( au )   +-   +-   +-   +    +-   +-    -   +-      
 w   1  1    8859  0.015387    1(b3u )   1(b1u )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    8861 -0.012091    3(b3u )   1(b1u )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    8871  0.011441    1(b3u )   3(b1u )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    8872 -0.017808    2(b3u )   3(b1u )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    8873 -0.010916    3(b3u )   3(b1u )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    8921  0.011794    1( ag )   2(b2g )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    8924 -0.012812    4( ag )   2(b2g )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    8968  0.010069    2(b2u )   1( au )   +-   +-   +-   +    +-    -   +-   +-      
 w   1  1    9320  0.011357    3( ag )   3( ag )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9351  0.016751    1(b3u )   1(b3u )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9352 -0.010615    1(b3u )   2(b3u )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9353  0.016173    2(b3u )   2(b3u )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9354 -0.016015    1(b3u )   3(b3u )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9356  0.012529    3(b3u )   3(b3u )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9378  0.011471    1(b1g )   1(b1g )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9386  0.010148    3(b1u )   3(b1u )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9427  0.011624    1(b2g )   2(b2g )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9428  0.014575    2(b2g )   2(b2g )   +-   +-   +-        +-   +-   +-   +-      
 w   1  1    9598 -0.015146    2( ag )   1(b2u )   +-   +-   +    +-   +-   +-   +-    -      
 w   1  1    9622  0.010268    2(b3u )   1(b1g )   +-   +-   +    +-   +-   +-   +-    -      
 w   1  1    9636  0.010257    4(b1u )   1(b3g )   +-   +-   +    +-   +-   +-   +-    -      
 w   1  1    9747  0.012422    1(b3u )   1(b1u )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9760 -0.023020    2(b3u )   3(b1u )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9765 -0.012260    1(b3u )   4(b1u )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9767  0.012714    3(b3u )   4(b1u )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9801  0.011750    1( ag )   1(b2g )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9809  0.011123    1( ag )   2(b2g )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9811  0.016825    3( ag )   2(b2g )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9856  0.012355    2(b2u )   1( au )   +-   +-   +    +-   +-   +-    -   +-      
 w   1  1    9975 -0.016596    1( ag )   1(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1    9977 -0.010152    3( ag )   1(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1    9978  0.013202    4( ag )   1(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1    9984 -0.012209    2( ag )   2(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1    9993 -0.015100    3( ag )   3(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1    9999  0.021398    1( ag )   4(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1   10001  0.010034    3( ag )   4(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1   10002 -0.020461    4( ag )   4(b1u )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1   10060  0.011332    2(b3u )   3(b2g )   +-   +-   +    +-   +-    -   +-   +-      
 w   1  1   10459 -0.013089    1( ag )   1(b3u )   +-   +-   +     -   +-   +-   +-   +-      
 w   1  1   10461 -0.013320    3( ag )   1(b3u )   +-   +-   +     -   +-   +-   +-   +-      
 w   1  1   10469  0.017947    3( ag )   2(b3u )   +-   +-   +     -   +-   +-   +-   +-      
 w   1  1   10477  0.013072    3( ag )   3(b3u )   +-   +-   +     -   +-   +-   +-   +-      
 w   1  1   10508 -0.012970    2(b2u )   1(b1g )   +-   +-   +     -   +-   +-   +-   +-      
 w   1  1   10525  0.014750    4(b1u )   2(b2g )   +-   +-   +     -   +-   +-   +-   +-      
 w   1  1   10687  0.010319    1( ag )   1( ag )   +-   +-        +-   +-   +-   +-   +-      
 w   1  1   10689  0.016035    2( ag )   2( ag )   +-   +-        +-   +-   +-   +-   +-      
 w   1  1   10725  0.013568    2(b3u )   2(b3u )   +-   +-        +-   +-   +-   +-   +-      
 w   1  1   10746  0.011724    2(b2u )   2(b2u )   +-   +-        +-   +-   +-   +-   +-      
 w   1  1   10755  0.011412    2(b1u )   2(b1u )   +-   +-        +-   +-   +-   +-   +-      
 w   1  1   10760  0.012736    2(b1u )   4(b1u )   +-   +-        +-   +-   +-   +-   +-      
 w   1  1   10762  0.019301    4(b1u )   4(b1u )   +-   +-        +-   +-   +-   +-   +-      
 w   1  1   10972 -0.012556    4( ag )   1(b2u )   +-   +    +-   +-   +-   +-   +-    -      
 w   1  1   11132  0.010306    2(b3u )   3(b1u )   +-   +    +-   +-   +-   +-    -   +-      
 w   1  1   11133  0.010039    3(b3u )   3(b1u )   +-   +    +-   +-   +-   +-    -   +-      
 w   1  1   11184  0.010726    4( ag )   2(b2g )   +-   +    +-   +-   +-   +-    -   +-      
 w   1  1   11347  0.011264    1( ag )   1(b1u )   +-   +    +-   +-   +-    -   +-   +-      
 w   1  1   11350 -0.010921    4( ag )   1(b1u )   +-   +    +-   +-   +-    -   +-   +-      
 w   1  1   11365  0.010285    3( ag )   3(b1u )   +-   +    +-   +-   +-    -   +-   +-      
 w   1  1   11464  0.010320    1(b1g )   1( au )   +-   +    +-   +-   +-    -   +-   +-      
 w   1  1   11850  0.010255    4( ag )   3(b3u )   +-   +    +-    -   +-   +-   +-   +-      
 w   1  1   11894  0.011095    1(b1u )   2(b2g )   +-   +    +-    -   +-   +-   +-   +-      
 w   1  1   11896  0.012985    3(b1u )   2(b2g )   +-   +    +-    -   +-   +-   +-   +-      
 w   1  1   11898  0.012292    5(b1u )   2(b2g )   +-   +    +-    -   +-   +-   +-   +-      
 w   1  1   12131  0.011669    1(b1u )   4(b1u )   +-   +     -   +-   +-   +-   +-   +-      
 w   1  1   12133  0.012520    3(b1u )   4(b1u )   +-   +     -   +-   +-   +-   +-   +-      
 w   1  1   12138  0.010704    4(b1u )   5(b1u )   +-   +     -   +-   +-   +-   +-   +-      

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             181
     0.01> rq > 0.001           3336
    0.001> rq > 0.0001          5849
   0.0001> rq > 0.00001         4024
  0.00001> rq > 0.000001         953
 0.000001> rq                    213
           all                 14558
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        84 2x:         0 4x:         0
All internal counts: zz :       103 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       102    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        82    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.940210785060     73.395421819250
     2     2      0.168862010290    -13.201141499701

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =    -78.066304920090   "relaxed" cnot**2         =   0.912510698863
 eci       =    -78.332826864464   deltae = eci - eref       =  -0.266521944374
 eci+dv1   =    -78.356144683115   dv1 = (1-cnot**2)*deltae  =  -0.023317818651
 eci+dv2   =    -78.358380338719   dv2 = dv1 / cnot**2       =  -0.025553474255
 eci+dv3   =    -78.361090154017   dv3 = dv1 / (2*cnot**2-1) =  -0.028263289553
 eci+pople =    -78.356890487032   ( 16e- scaled deltae )    =  -0.290585566942
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999521

 space required:

    space required for calls in multd2:
       onex          135
       allin           0
       diagon        705
    max.      ---------
       maxnex        705

    total core space usage:
       maxnex        705
    max k-seg       8247
    max k-ind        128
              ---------
       totmax      17455
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      46  DYX=     224  DYW=     254
   D0Z=      11  D0Y=     132  D0X=      48  D0W=      64
  DDZI=      39 DDYI=     450 DDXI=     154 DDWI=     182
  DDZE=       0 DDYE=     128 DDXE=      56 DDWE=      71
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9998487      1.9806929      1.9668960      0.0168461      0.0075901
   0.0064485      0.0018723      0.0010486      0.0006292      0.0003133
   0.0001139


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9702629      0.0192234      0.0077153      0.0016330      0.0012476
   0.0005524      0.0000849


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9055816      0.0076404      0.0040984      0.0007377


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0058963      0.0008116


*****   symmetry block SYM5   *****

 occupation numbers of nos
   1.9998418      1.9733294      0.0180279      0.0151793      0.0047847
   0.0022683      0.0009869      0.0005368      0.0002780      0.0000891
   0.0000635


*****   symmetry block SYM6   *****

 occupation numbers of nos
   1.9669071      0.0148824      0.0032496      0.0015120      0.0005705
   0.0002271      0.0001080


*****   symmetry block SYM7   *****

 occupation numbers of nos
   0.0807530      0.0027117      0.0009200      0.0005458


*****   symmetry block SYM8   *****

 occupation numbers of nos
   0.0040240      0.0004178


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       2.000547   1.459908   0.023518   0.003849   0.000496   0.002409
    C1_ p      -0.000250   0.062379   1.465224   0.004335   0.002310   0.000076
    C1_ d      -0.000011   0.009492   0.012737   0.000729   0.004096   0.002948
    H1_ s      -0.000089   0.421843   0.452491   0.007740  -0.000023   0.000583
    H1_ p      -0.000348   0.027072   0.012926   0.000194   0.000712   0.000434

   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 
    C1_ s       0.000192   0.000302   0.000051   0.000012   0.000008
    C1_ p       0.000070   0.000310   0.000106   0.000007   0.000015
    C1_ d       0.000746   0.000338   0.000094   0.000021   0.000001
    H1_ s       0.000877   0.000014   0.000032  -0.000005   0.000056
    H1_ p      -0.000013   0.000085   0.000347   0.000279   0.000033

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u       5B3u       6B3u
    C1_ p       1.137487   0.007172   0.003416   0.000447   0.000014   0.000128
    C1_ d       0.000040   0.003191   0.002912   0.000758   0.000002  -0.000006
    H1_ s       0.817973   0.008661   0.000651   0.000419   0.000203   0.000042
    H1_ p       0.014763   0.000199   0.000736   0.000009   0.001028   0.000389

   ao class       7B3u
    C1_ p       0.000008
    C1_ d       0.000000
    H1_ s       0.000047
    H1_ p       0.000030

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u
    C1_ p       1.865111   0.004272   0.001826   0.000012
    C1_ d       0.020924   0.002940   0.002046   0.000077
    H1_ p       0.019547   0.000428   0.000226   0.000648

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g
    C1_ d       0.004862   0.000142
    H1_ p       0.001034   0.000669

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       2.001585   0.709556   0.010119   0.000697   0.000588   0.000543
    C1_ p      -0.000271   0.418763   0.003386   0.007826   0.001356   0.000124
    C1_ d      -0.000239   0.005723   0.001318   0.001011   0.001772   0.000937
    H1_ s      -0.000743   0.805755   0.003173   0.005176   0.000370   0.000247
    H1_ p      -0.000491   0.033532   0.000031   0.000471   0.000700   0.000418

   ao class       7B1u       8B1u       9B1u      10B1u      11B1u
    C1_ s       0.000084   0.000051   0.000043   0.000020   0.000007
    C1_ p       0.000088   0.000077   0.000007   0.000044   0.000000
    C1_ d       0.000610   0.000039   0.000091   0.000002   0.000002
    H1_ s       0.000240   0.000026   0.000002   0.000001   0.000037
    H1_ p      -0.000034   0.000344   0.000134   0.000022   0.000018

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g       5B2g       6B2g
    C1_ p       0.761244   0.009596   0.000872   0.000108   0.000165   0.000055
    C1_ d       0.054963  -0.000065   0.001187   0.000142   0.000184   0.000027
    H1_ s       1.141960   0.004709   0.000569   0.000468   0.000038   0.000045
    H1_ p       0.008741   0.000643   0.000622   0.000795   0.000183   0.000100

   ao class       7B2g
    C1_ p       0.000010
    C1_ d       0.000008
    H1_ s       0.000038
    H1_ p       0.000051

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g
    C1_ p       0.078498   0.001246   0.000500   0.000014
    C1_ d       0.000971   0.000843   0.000360   0.000156
    H1_ p       0.001283   0.000623   0.000061   0.000376

                        Au  partial gross atomic populations
   ao class       1Au        2Au 
    C1_ d       0.002617   0.000146
    H1_ p       0.001407   0.000272


                        gross atomic populations
     ao           C1_        H1_
      s         6.214584   3.673624
      p         5.838179   0.131725
      d         0.141888   0.000000
    total      12.194650   3.805350


 Total number of electrons:   16.00000000

