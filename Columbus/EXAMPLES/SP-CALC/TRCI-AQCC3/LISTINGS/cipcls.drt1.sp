
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans@itc.univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=   8000000 mem1=1075314696 ifirst=-268237604

 drt header information:
 /                                                                               
 *** warning: rdhdrt: drt version .ne. 4 *** vrsion= 3
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       56        5       16       15       20
 ncsft:             358
 map(*)=    -1  9 10  1  2  3  4 11  5 12  6  7  8
 mu(*)=      0  0  0  0
 syml(*) =   1  1  2  3
 rmo(*)=     2  3  1  1

 indx01:    56 indices saved in indxv(*)
===================================ROOT #-1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Sep  5 16:35:22 2002
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Sep  5 16:35:22 2002
 energy computed by program ciudg.       hochtor2        Thu Sep  5 16:35:25 2002

 lenrec =    4096 lenci =       358 ninfo =  6 nenrgy =  8 ntitle =  7

 Max. overlap with ref vector #        2
 Valid ci vector #        2
 Method:        3       97% overlap
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -3.849672947105E+01, ietype=-1038,   total energy of type: MR-AQCC 
 energy( 4)=  5.519431513899E-07, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  1.000088900582E-12, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  1.686774167471E-13, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.021664246843E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -3.840740310681E+01, ietype=-1041,   total energy of type: Unknown 
==================================================================================

 space is available for   3997894 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 2===================================
================================================================================


 rdcivnew:      52 coefficients were selected.
 workspace: ncsfmx=     358
 ncsfmx= 358

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      52
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       2 **
 6.2500E-02 <= |c| < 1.2500E-01       3 ***
 3.1250E-02 <= |c| < 6.2500E-02      11 ***********
 1.5625E-02 <= |c| < 3.1250E-02      18 ******************
 7.8125E-03 <= |c| < 1.5625E-02      17 *****************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       52 total stored =      52

 from the selected csfs,
 min(|csfvec(:)|) = 1.0045E-02    max(|csfvec(:)|) = 9.4893E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      52

 i:slabel(i) =  1:   1  2:   2  3:   3  4:   4

 frozen orbital =    1
 symfc(*)       =    1
 label          =    1
 rmo(*)         =    1

 internal level =    1    2    3    4
 syml(*)        =    1    1    2    3
 label          =    1    1    2    3
 rmo(*)         =    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        3 -0.94893 0.90046 z*                    3033
        2 -0.14839 0.02202 z*                    3303
        1  0.14040 0.01971 z*                    3330
       38  0.10822 0.01171 y             3:  2  11232
      191  0.07161 0.00513 w             3:  2  33030
        5  0.06650 0.00442 z*                    0333
       21  0.05771 0.00333 y             2:  2  13023
      273  0.05536 0.00306 w    1:  4    3:  2 121032
      210  0.05491 0.00301 w             2:  2  33003
       10 -0.05264 0.00277 y             1:  4  13230
       14 -0.04806 0.00231 y             1:  4  13203
      285  0.03990 0.00159 w    1:  4    2:  2 121023
      197  0.03524 0.00124 w    2:  2    3:  2 123012
      342  0.03481 0.00121 w             1:  4  30033
       18  0.03307 0.00109 y             3:  2  13032
       39 -0.03291 0.00108 y             3:  3  11232
       66 -0.03242 0.00105 x    2:  2    3:  2 113022
       34 -0.02874 0.00083 y             1:  4  12033
       67 -0.02798 0.00078 x    2:  2    3:  3 113022
      121 -0.02673 0.00071 x    1:  4    2:  2 112023
      353  0.02632 0.00069 w             3:  2  30033
      198  0.02574 0.00066 w    2:  2    3:  3 123012
      193  0.02466 0.00061 w             3:  3  33030
       17 -0.02350 0.00055 y             1:  7  13203
      200  0.02347 0.00055 w             1:  4  33003
      279 -0.02340 0.00055 w    1:  6    3:  3 121032
      180  0.02252 0.00051 w             1:  4  33030
      113 -0.01931 0.00037 x    1:  4    3:  3 112032
        4  0.01928 0.00037 z*                    1233
      286  0.01886 0.00036 w    1:  5    2:  2 121023
      202  0.01850 0.00034 w             1:  5  33003
       30 -0.01819 0.00033 y             3:  2  12132
       12 -0.01707 0.00029 y             1:  6  13230
       48  0.01640 0.00027 y             1:  6  10233
      109 -0.01567 0.00025 x    1:  4    3:  2 112032
      277  0.01419 0.00020 w    1:  4    3:  3 121032
      211  0.01406 0.00020 w             3:  2  33003
      300 -0.01396 0.00019 w             3:  2  30330
       41  0.01377 0.00019 y             2:  2  11223
       36  0.01354 0.00018 y             1:  6  12033
      287 -0.01352 0.00018 w    1:  6    2:  2 121023
      274  0.01334 0.00018 w    1:  5    3:  2 121032
        7 -0.01281 0.00016 y             3:  2  13302
       15 -0.01269 0.00016 y             1:  5  13203
      158  0.01198 0.00014 w             3:  2  33300
      185  0.01171 0.00014 w             1:  6  33030
      221  0.01156 0.00013 w    1:  4    3:  2 121302
      123  0.01145 0.00013 x    1:  6    2:  2 112023
       33 -0.01139 0.00013 y             2:  2  12123
      347  0.01062 0.00011 w             1:  6  30033
      275  0.01033 0.00011 w    1:  6    3:  2 121032
        9 -0.01004 0.00010 y             3:  4  13302
           52 csfs were printed in this range.
