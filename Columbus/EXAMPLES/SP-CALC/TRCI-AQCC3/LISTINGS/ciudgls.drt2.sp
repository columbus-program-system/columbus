1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans@itc.univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 280
global arrays:           9240   (    0.07 MB)
vdisk:                      0   (    0.00 MB)
drt:                    28749   (    0.11 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9 (20-05-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 3,
   gset =3,
   RTOLCI=0.0000010000,0.000001,0.00001,0.00001
   VOUT  = 1,
   froot = 1,
   NBKITR= 0,
   NITER= 80,
   ivmode=3,
  &end
 ------------------------------------------------------------------------
 froot operation modus: follow reference vector.
 bummer (warning):2:changed keyword: nbkitr=         0
 bummer (warning):changed keyword: davcor=         0

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    3      nbkitr =    0      niter  =   80
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    1      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   0
 ncorel =    0      csfprn  =   1      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-06
 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1075314696 ifirst=-268237538

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Sep  5 16:35:22 2002
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Sep  5 16:35:22 2002

 core energy values from the integral file:
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -2.792842564975E+01

 drt header information:
 cidrt_title                                                                     
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       42        2       15       12       13
 ncsft:             280
 total number of valid internal walks:      42
 nvalz,nvaly,nvalx,nvalw =        2      15      12      13

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext    72    64    20
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      330      432      360      156       27        0        0        0
 minbl4,minbl3,maxbl2    48    48    51
 maxbuf 32767
 number of external orbitals per symmetry block:   4   1   3   0
 nmsym   4 number of internal orbitals   4

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Sep  5 16:35:22 2002
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Sep  5 16:35:22 2002
 cidrt_title                                                                     
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     7    20    15    20 total internal walks:      62
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 3

 setref:        2 references kept,
                0 references were marked as invalid, out of
                2 total.
 limcnvrt: found 2 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  2 segmentation marks segtype= 1
 limcnvrt: found 15 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 2
 limcnvrt: found 12 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  12 segmentation marks segtype= 3
 limcnvrt: found 13 valid internal walksout of  19
  walks (skipping trailing invalids)
  ... adding  13 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x       9       4      12       3
 vertex w      17       4      12       3



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           2|         2|         0|         2|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          15|        38|         2|        15|         2|         2|
 -------------------------------------------------------------------------------
  X 3          12|       106|        40|        12|        17|         3|
 -------------------------------------------------------------------------------
  W 4          13|       134|       146|        13|        29|         4|
 -------------------------------------------------------------------------------

 dimension of the ci-matrix ->>>       280


 297 dimension of the ci-matrix ->>>       280


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL               SEGCI            VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      12       2        106          2      12       2
     2  4   1    25      two-ext wz   2X  4 1      13       2        134          2      13       2
     3  4   3    26      two-ext wx   2X  4 3      13      12        134        106      13      12
     4  2   1    11      one-ext yz   1X  2 1      15       2         38          2      15       2
     5  3   2    15      1ex3ex  yx   3X  3 2      12      15        106         38      12      15
     6  4   2    16      1ex3ex  yw   3X  4 2      13      15        134         38      13      15
     7  1   1     1      allint zz    OX  1 1       2       2          2          2       2       2
     8  2   2     5      0ex2ex yy    OX  2 2      15      15         38         38      15      15
     9  3   3     6      0ex2ex xx    OX  3 3      12      12        106        106      12      12
    10  4   4     7      0ex2ex ww    OX  4 4      13      13        134        134      13      13
    11  1   1    75      dg-024ext z  DG  1 1       2       2          2          2       2       2
    12  2   2    45      4exdg024 y   DG  2 2      15      15         38         38      15      15
    13  3   3    46      4exdg024 x   DG  3 3      12      12        106        106      12      12
    14  4   4    47      4exdg024 w   DG  4 4      13      13        134        134      13      13
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 298 129 40
 diagonal elements written to file   4
 calling dmain

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        20 2x:         0 4x:         0
All internal counts: zz :         3 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:         2    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        19    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       2

    root           eigenvalues
    ----           ------------
       1         -38.5360197176

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.6

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -38.5360197176

  ### active excitation selection ###

 Inactive orbitals: 4
    there are    2 all-active excitations of which    2 are references.

    the    2 reference all-active excitation csfs

    --------
       1   2
    --------
  1:   2   1
  2:   1   2
  3:  -1  -1

  ### end active excitation selection ###


################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999996
 the acpf density matrix will be calculated

 space required:

    space required for calls in multd2:
       onex           40
       allin           0
       diagon         85
    max.      ---------
       maxnex         85

    total core space usage:
       maxnex         85
    max k-seg        134
    max k-ind         15
              ---------
       totmax        383
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       1  D0Y=       0  D0X=       0  D0W=       0
  DDZI=       8 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               280
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 one root will be followed:                               1
 number of roots to converge:                             1
 number of iterations:                                   80
 residual norm convergence criteria:               0.000001

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 follow root  1 (overlap=  1.)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -38.5360197176 -1.7764E-15  1.0625E-01  4.3642E-01  1.0000E-06

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.01    0.00    0.00    0.01
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.01    0.00    0.00    0.01
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.02    0.00    0.00    0.02
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.01    0.00    0.00    0.01
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.01    0.00    0.00    0.01
   14   47    0    0.00    0.00    0.00    0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.98126689    -0.19265328
 follow root  1 (overlap=  0.981266893)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -38.6166978443  8.0678E-02  5.2638E-03  1.0154E-01  1.0000E-06
 mraqcc  #  2  2    -36.4429808315  8.5146E+00  0.0000E+00  9.1047E-01  1.0000E-06

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.01    0.00    0.00    0.01
    2   25    0    0.00    0.00    0.00    0.00
    3   26    0    0.01    0.00    0.00    0.00
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.02    0.00    0.00    0.01
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.03    0.00    0.00    0.03
    9    6    0    0.03    0.00    0.00    0.03
   10    7    0    0.03    0.00    0.00    0.03
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.01    0.00    0.00    0.01
   14   47    0    0.01    0.00    0.00    0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1800s 
time spent in multnx:                   0.1600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1800s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97703548     0.16718523     0.13210135
 follow root  1 (overlap=  0.977035481)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -38.6231415536  6.4437E-03  6.7053E-04  3.6174E-02  1.0000E-06
 mraqcc  #  3  2    -37.0623597911  6.1938E-01  0.0000E+00  2.9511E-01  1.0000E-06
 mraqcc  #  3  3    -36.1306104014  8.2022E+00  0.0000E+00  5.2182E-01  1.0000E-05

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.01    0.00    0.00    0.01
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.00    0.00    0.00    0.00
    5   15    0    0.02    0.00    0.00    0.01
    6   16    0    0.01    0.00    0.00    0.01
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.01    0.00    0.00    0.01
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.97531540    -0.15063950    -0.09234411     0.13243930
 follow root  1 (overlap=  0.975315405)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -38.6239720679  8.3051E-04  9.2001E-05  1.3473E-02  1.0000E-06
 mraqcc  #  4  2    -37.5552050481  4.9285E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  4  3    -36.2774301931  1.4682E-01  0.0000E+00  5.0923E-01  1.0000E-05
 mraqcc  #  4  4    -36.1331390577  8.2047E+00  0.0000E+00  4.7999E-01  1.0000E-05

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.00    0.00    0.00    0.00
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.02    0.00    0.00    0.02
    7    1    0    0.00    0.00    0.00    0.00
    8    5    0    0.02    0.00    0.00    0.02
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.01    0.00    0.00    0.01
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.01    0.00    0.00    0.01
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1400s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97507715    -0.11959527     0.15316968     0.00854000    -0.10671292
 follow root  1 (overlap=  0.975077147)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -38.6240804519  1.0838E-04  1.2028E-05  4.6663E-03  1.0000E-06
 mraqcc  #  5  2    -37.7540083174  1.9880E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  5  3    -36.7970556481  5.1963E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  5  4    -36.2249049128  9.1766E-02  0.0000E+00  5.9430E-01  1.0000E-05
 mraqcc  #  5  5    -35.7633178488  7.8349E+00  0.0000E+00  3.7028E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.01    0.00    0.00    0.01
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.01    0.00    0.00    0.01
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.02    0.00    0.00    0.02
    9    6    0    0.01    0.00    0.00    0.01
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.01    0.00    0.00    0.01
   14   47    0    0.00    0.00    0.00    0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97494574     0.10035878    -0.10330896     0.13704109    -0.03759861     0.09242433
 follow root  1 (overlap=  0.974945743)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -38.6240960690  1.5617E-05  2.2263E-06  2.0608E-03  1.0000E-06
 mraqcc  #  6  2    -37.9244802752  1.7047E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  6  3    -37.2115413751  4.1449E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  6  4    -36.6683833228  4.4348E-01  0.0000E+00  2.9280E-01  1.0000E-05
 mraqcc  #  6  5    -36.1143902753  3.5107E-01  0.0000E+00  6.0975E-01  1.0000E-04
 mraqcc  #  6  6    -35.6181671381  7.6897E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.00    0.00    0.00    0.00
    2   25    0    0.01    0.00    0.00    0.00
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.01    0.00    0.01
    6   16    0    0.02    0.00    0.00    0.02
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.01    0.00    0.00    0.01
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97490431    -0.06839445     0.11447231     0.14973208    -0.01141594     0.02718054     0.09214702
 follow root  1 (overlap=  0.974904311)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -38.6240993890  3.3200E-06  4.0634E-07  8.8929E-04  1.0000E-06
 mraqcc  #  7  2    -38.1083495566  1.8387E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  7  3    -37.5285617642  3.1702E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  7  4    -36.6830860290  1.4703E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  7  5    -36.6120467869  4.9766E-01  0.0000E+00  3.3698E-01  1.0000E-04
 mraqcc  #  7  6    -35.9072814872  2.8911E-01  0.0000E+00  5.8884E-01  1.0000E-04
 mraqcc  #  7  7    -35.6180023013  7.6896E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.00    0.00    0.00    0.00
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.01    0.00    0.01
    6   16    0    0.02    0.00    0.00    0.02
    7    1    0    0.00    0.00    0.00    0.00
    8    5    0    0.02    0.00    0.00    0.01
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1400s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97489313    -0.05739289    -0.10435044    -0.11772744     0.00244029     0.12095504    -0.01092895     0.08237203
 follow root  1 (overlap=  0.974893135)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -38.6240999280  5.3894E-07  5.6896E-08  3.3651E-04  1.0000E-06
 mraqcc  #  8  2    -38.1802166679  7.1867E-02  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  8  3    -37.6556292684  1.2707E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  8  4    -37.0009661735  3.1788E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  8  5    -36.6125851656  5.3838E-04  0.0000E+00  2.8970E-01  1.0000E-04
 mraqcc  #  8  6    -36.4141723114  5.0689E-01  0.0000E+00  2.6634E-01  1.0000E-04
 mraqcc  #  8  7    -35.8421503376  2.2415E-01  0.0000E+00  4.2903E-01  1.0000E-04
 mraqcc  #  8  8    -35.5437855573  7.6154E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.00    0.00    0.00    0.00
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.02    0.01    0.00    0.02
    7    1    0    0.00    0.00    0.00    0.00
    8    5    0    0.02    0.00    0.00    0.01
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1400s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97488993     0.05432760    -0.09576401    -0.09134949     0.09721836    -0.09885627    -0.05789834     0.01369382

                ci   9
 ref:   1    -0.07974308
 follow root  1 (overlap=  0.974889926)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -38.6240999964  6.8407E-08  5.9022E-09  1.1144E-04  1.0000E-06
 mraqcc  #  9  2    -38.2077731369  2.7556E-02  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  9  3    -37.7028214175  4.7192E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  9  4    -37.2632029967  2.6224E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  9  5    -36.7097474027  9.7162E-02  0.0000E+00  3.4086E-01  1.0000E-04
 mraqcc  #  9  6    -36.5464829196  1.3231E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  7    -36.1006161204  2.5847E-01  0.0000E+00  2.4553E-01  1.0000E-04
 mraqcc  #  9  8    -35.7466269516  2.0284E-01  0.0000E+00  2.8413E-01  1.0000E-04
 mraqcc  #  9  9    -35.5338864081  7.6055E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.00    0.00    0.00    0.00
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.02    0.00    0.00    0.02
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.01    0.00    0.00    0.01
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97488949    -0.05373419    -0.07713195     0.09547654     0.13259741     0.02801762    -0.07189898    -0.04114249

                ci   9         ci  10
 ref:   1     0.00708258    -0.07974102
 follow root  1 (overlap=  0.974889487)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1    -38.6241000039  7.4943E-09  5.5575E-10  3.5020E-05  1.0000E-06
 mraqcc  # 10  2    -38.2118111640  4.0380E-03  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 10  3    -37.7545904151  5.1769E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 10  4    -37.4594707573  1.9627E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 10  5    -36.7327092840  2.2962E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  6    -36.6695443746  1.2306E-01  0.0000E+00  1.2265E-01  1.0000E-04
 mraqcc  # 10  7    -36.2679747233  1.6736E-01  0.0000E+00  3.5496E-01  1.0000E-04
 mraqcc  # 10  8    -36.0777857459  3.3116E-01  0.0000E+00  4.1578E-01  1.0000E-04
 mraqcc  # 10  9    -35.7150328052  1.8115E-01  0.0000E+00  1.7109E-01  1.0000E-04
 mraqcc  # 10 10    -35.5298003306  7.6014E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.00    0.00    0.00    0.00
    2   25    0    0.01    0.01    0.00    0.00
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.02    0.00    0.00    0.02
    6   16    0    0.01    0.00    0.00    0.01
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.01    0.00    0.00    0.01
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97488944    -0.05343495    -0.06839479     0.09326006    -0.09390380    -0.05726905     0.10953432     0.04950585

                ci   9         ci  10         ci  11
 ref:   1     0.02192435    -0.01242337    -0.07860479
 follow root  1 (overlap=  0.974889443)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 11  1    -38.6241000046  7.5199E-10  6.8234E-11  1.1642E-05  1.0000E-06
 mraqcc  # 11  2    -38.2134508823  1.6397E-03  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 11  3    -37.7770678652  2.2477E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 11  4    -37.5294500866  6.9979E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 11  5    -36.9665890571  2.3388E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 11  6    -36.6904459686  2.0902E-02  0.0000E+00  3.5976E-01  1.0000E-04
 mraqcc  # 11  7    -36.5102554792  2.4228E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 11  8    -36.0819517354  4.1660E-03  0.0000E+00  2.8926E-01  1.0000E-04
 mraqcc  # 11  9    -35.9623132598  2.4728E-01  0.0000E+00  4.2714E-01  1.0000E-04
 mraqcc  # 11 10    -35.7038345141  1.7403E-01  0.0000E+00  1.6152E-01  1.0000E-04
 mraqcc  # 11 11    -35.5255304589  7.5971E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.01    0.00    0.00    0.01
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.00    0.00    0.00    0.00
    5   15    0    0.02    0.01    0.00    0.02
    6   16    0    0.01    0.00    0.00    0.01
    7    1    0    0.02    0.01    0.00    0.02
    8    5    0    0.05    0.00    0.00    0.05
    9    6    0    0.03    0.00    0.00    0.03
   10    7    0    0.02    0.00    0.00    0.02
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.01    0.00    0.00    0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1900s 
time spent in multnx:                   0.1900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.2100s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.97488948     0.05309604     0.05055895     0.07693024     0.09278059     0.04335212     0.13002596     0.04262822

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.04988781     0.01457269     0.02723199     0.07511687
 follow root  1 (overlap=  0.97488948)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 12  1    -38.6241000047  9.2808E-11  7.7999E-12  4.0206E-06  1.0000E-06
 mraqcc  # 12  2    -38.2140608643  6.0998E-04  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 12  3    -37.8203436280  4.3276E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 12  4    -37.6595330348  1.3008E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 12  5    -37.3536112525  3.8702E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12  6    -36.6912643316  8.1836E-04  0.0000E+00  3.4459E-01  1.0000E-04
 mraqcc  # 12  7    -36.6317570632  1.2150E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12  8    -36.2788639438  1.9691E-01  0.0000E+00  2.2317E-01  1.0000E-04
 mraqcc  # 12  9    -36.0819259506  1.1961E-01  0.0000E+00  2.8207E-01  1.0000E-04
 mraqcc  # 12 10    -35.8914431357  1.8761E-01  0.0000E+00  4.7708E-01  1.0000E-04
 mraqcc  # 12 11    -35.6151238706  8.9593E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12 12    -35.5213261411  7.5929E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.02    0.00    0.00    0.02
    2   25    0    0.02    0.00    0.00    0.02
    3   26    0    0.02    0.00    0.00    0.02
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.03    0.00    0.00    0.02
    7    1    0    0.02    0.00    0.00    0.02
    8    5    0    0.02    0.00    0.00    0.02
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.03    0.00    0.00    0.03
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.00    0.00    0.00    0.00
   13   46    0    0.01    0.00    0.00    0.01
   14   47    0    0.00    0.00    0.00    0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2100s 
time spent in multnx:                   0.2000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.2200s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97488949     0.05300898     0.03437484    -0.07530877    -0.09029056     0.12346402    -0.02069740    -0.08861550

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.01802866     0.04159443    -0.01758949    -0.01477476     0.07528506
 follow root  1 (overlap=  0.974889489)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 13  1    -38.6241000047  9.4467E-12  6.9659E-13  1.1591E-06  1.0000E-06
 mraqcc  # 13  2    -38.2141099680  4.9104E-05  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 13  3    -37.8816419719  6.1298E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 13  4    -37.7098364164  5.0303E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 13  5    -37.4513346888  9.7723E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13  6    -36.7726209676  8.1357E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13  7    -36.6824739081  5.0717E-02  0.0000E+00  4.8090E-02  1.0000E-04
 mraqcc  # 13  8    -36.3799398206  1.0108E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13  9    -36.1933760175  1.1145E-01  0.0000E+00  3.8671E-01  1.0000E-04
 mraqcc  # 13 10    -35.9441906577  5.2748E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13 11    -35.8300801953  2.1496E-01  0.0000E+00  4.5085E-01  1.0000E-04
 mraqcc  # 13 12    -35.5531779099  3.1852E-02  0.0000E+00  8.5869E-02  1.0000E-04
 mraqcc  # 13 13    -35.5071645051  7.5787E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node   tmult   tloop    tint   tmnx 
    1   24    0    0.01    0.00    0.00    0.01
    2   25    0    0.01    0.00    0.00    0.01
    3   26    0    0.01    0.00    0.00    0.01
    4   11    0    0.01    0.00    0.00    0.01
    5   15    0    0.01    0.00    0.00    0.01
    6   16    0    0.01    0.00    0.00    0.01
    7    1    0    0.01    0.00    0.00    0.01
    8    5    0    0.05    0.00    0.00    0.05
    9    6    0    0.02    0.00    0.00    0.02
   10    7    0    0.01    0.00    0.00    0.01
   11   75    0    0.00    0.00    0.00    0.00
   12   45    0    0.01    0.00    0.00    0.01
   13   46    0    0.00    0.00    0.00    0.00
   14   47    0    0.00    0.00    0.00    0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1600s 
time spent in multnx:                   0.1600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1700s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       298 2x:       129 4x:        40
All internal counts: zz :         3 yy:        50 xx:        37 ww:        31
One-external counts: yz :        62 yx:       197 yw:       223
Two-external counts: yy :       104 ww:        77 xx:        86 xz:        13 wz:        18 wx:       105
Three-ext.   counts: yx :        70 yw:        73

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================




LOOPCOUNT per task:
task #   1:        12    task #   2:        17    task #   3:        93    task #   4:        61
task #   5:       243    task #   6:       242    task #   7:         2    task #   8:        47
task #   9:        35    task #  10:        26    task #  11:        19    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97488949     0.05239968     0.02692963     0.07286664     0.08915318     0.06790239     0.09568243     0.08124879

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.06916378     0.00183374    -0.03053694     0.04345241    -0.00259185    -0.06985818
 follow root  1 (overlap=  0.974889488)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 14  1    -38.6241000047  8.8995E-13  7.6305E-14  3.9529E-07  1.0000E-06
 mraqcc  # 14  2    -38.2161580936  2.0481E-03  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 14  3    -37.9451816593  6.3540E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 14  4    -37.7345491648  2.4713E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 14  5    -37.4950001072  4.3665E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14  6    -37.0135185060  2.4090E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14  7    -36.7456477743  6.3174E-02  0.0000E+00  2.2757E-01  1.0000E-04
 mraqcc  # 14  8    -36.5397872776  1.5985E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14  9    -36.3052324195  1.1186E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14 10    -36.1720207219  2.2783E-01  0.0000E+00  3.3563E-01  1.0000E-04
 mraqcc  # 14 11    -35.8851068304  5.5027E-02  0.0000E+00  2.9016E-01  1.0000E-04
 mraqcc  # 14 12    -35.7834237686  2.3025E-01  0.0000E+00  1.6298E-01  1.0000E-04
 mraqcc  # 14 13    -35.5510009030  4.3836E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14 14    -35.4790353415  7.5506E+00  0.0000E+00  0.0000E+00  1.0000E-04


 mraqcc   convergence criteria satisfied after 14 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 14  1    -38.6241000047  8.8995E-13  7.6305E-14  3.9529E-07  1.0000E-06

####################CIUDGINFO####################

   followed vector at position   1 energy=  -38.624100004732

################END OF CIUDGINFO################


 a4den factor =  1.020237630 for root   1
    1 of the  15 expansion vectors are transformed.
    1 of the  14 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 root follwoing: root to follow is on position   1
 maximum overlap with reference  1(overlap=  0.974889488)
 reference energy= -38.5360197
 total aqcc energy= -38.6241
 diagonal element shift (lrtshift) =  -0.0880802871

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -38.6241000047

                                                       internal orbitals

                                          level       1    2    3    4

                                          orbital     2    3    8   10

                                         symmetry     1    1    2    3

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1        1  0.974790                       +-   +     -   +- 
 z*  1  1        2 -0.013941                       +    +-    -   +- 
 y   1  1        3  0.032844             1(   1)   +-   +-    -      
 y   1  1        5  0.010873             3(   1)   +-   +-    -      
 y   1  1        8 -0.016480             1(   3)   +-    -   +     - 
 y   1  1       10  0.017154             3(   3)   +-    -   +     - 
 y   1  1       11 -0.043217             1(   2)   +-    -        +- 
 y   1  1       13 -0.017981             2(   3)   +-   +     -    - 
 y   1  1       14  0.012525             3(   3)   +-   +     -    - 
 y   1  1       15  0.071246             1(   1)   +-         -   +- 
 y   1  1       16 -0.013947             2(   1)   +-         -   +- 
 y   1  1       18  0.021272             4(   1)   +-         -   +- 
 y   1  1       20  0.015292             1(   3)    -   +-   +     - 
 y   1  1       24  0.011982             1(   1)    -   +     -   +- 
 y   1  1       29 -0.062671             1(   3)   +    +-    -    - 
 y   1  1       30  0.019767             2(   3)   +    +-    -    - 
 y   1  1       32  0.072547             1(   1)   +     -    -   +- 
 y   1  1       34  0.019633             3(   1)   +     -    -   +- 
 y   1  1       35  0.026065             4(   1)   +     -    -   +- 
 y   1  1       38 -0.010636             3(   1)        +-    -   +- 
 x   1  1       50  0.026429    1(   2)   1(   3)   +-    -         - 
 x   1  1       51  0.020933    1(   2)   2(   3)   +-    -         - 
 x   1  1       54  0.018875    2(   1)   1(   3)   +-         -    - 
 x   1  1       58  0.015417    2(   1)   2(   3)   +-         -    - 
 x   1  1       81  0.011013    1(   1)   2(   3)    -    -   +     - 
 x   1  1       89  0.018835    1(   1)   1(   2)    -    -        +- 
 x   1  1       93  0.014340    1(   1)   1(   3)    -   +     -    - 
 x   1  1       97  0.017332    1(   1)   2(   3)    -   +     -    - 
 x   1  1      105  0.017674    1(   1)   2(   1)    -         -   +- 
 w   1  1      147 -0.031796    1(   1)   1(   1)   +-   +     -      
 w   1  1      152 -0.013436    3(   1)   3(   1)   +-   +     -      
 w   1  1      158 -0.082110    1(   3)   1(   3)   +-   +     -      
 w   1  1      160 -0.026223    2(   3)   2(   3)   +-   +     -      
 w   1  1      164 -0.027213    1(   2)   1(   3)   +-   +          - 
 w   1  1      165 -0.017108    1(   2)   2(   3)   +-   +          - 
 w   1  1      167  0.032907    1(   1)   1(   3)   +-        +     - 
 w   1  1      168 -0.020765    2(   1)   1(   3)   +-        +     - 
 w   1  1      172 -0.016489    2(   1)   2(   3)   +-        +     - 
 w   1  1      180 -0.046673    2(   1)   1(   2)   +-             +- 
 w   1  1      203 -0.037449    1(   1)   1(   3)   +     -   +     - 
 w   1  1      209  0.012968    3(   1)   2(   3)   +     -   +     - 
 w   1  1      215 -0.031784    1(   1)   1(   2)   +     -        +- 
 w   1  1      216 -0.012787    2(   1)   1(   2)   +     -        +- 
 w   1  1      219 -0.050198    1(   1)   1(   3)   +    +     -    - 
 w   1  1      220 -0.011076    2(   1)   1(   3)   +    +     -    - 
 w   1  1      223 -0.010694    1(   1)   2(   3)   +    +     -    - 
 w   1  1      225  0.021164    3(   1)   2(   3)   +    +     -    - 
 w   1  1      231  0.014720    1(   1)   1(   1)   +          -   +- 
 w   1  1      233 -0.013434    2(   1)   2(   1)   +          -   +- 
 w   1  1      264 -0.041205    1(   1)   1(   1)        +     -   +- 
 w   1  1      269 -0.011640    3(   1)   3(   1)        +     -   +- 
 w   1  1      275 -0.024911    1(   3)   1(   3)        +     -   +- 
 w   1  1      277 -0.010278    2(   3)   2(   3)        +     -   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              52
     0.01> rq > 0.001            135
    0.001> rq > 0.0001            76
   0.0001> rq > 0.00001           14
  0.00001> rq > 0.000001           2
 0.000001> rq                      0
           all                   280
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      11  DYX=      39  DYW=      41
   D0Z=       1  D0Y=       9  D0X=       7  D0W=       7
  DDZI=       8 DDYI=      51 DDXI=      39 DDWI=      39
  DDZE=       0 DDYE=      15 DDXE=      12 DDWE=      13
================================================================================
 root #  1: Scaling(2) with   1.02023763corresponding to ref #  1


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9730992      0.9988645      0.0218408      0.0051712
   0.0028639      0.0008633


*****   symmetry block SYM2   *****

 occupation numbers of nos
   0.9937042      0.0067707


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9629126      0.0280632      0.0051674      0.0006791


 total number of electrons =    8.0000000000

NO coefficients and occupation numbers are written to nocoef_ci.1
 1:after moread
 2:after moread


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.994144   1.326563   0.214673   0.003137   0.002243   0.001142
    C1_ p      -0.000029   0.119938   0.687518   0.004929   0.002924   0.000842
    H1_ s       0.005885   0.526598   0.096673   0.013775   0.000005   0.000881

   ao class       7A1 
    C1_ s       0.000121
    C1_ p       0.000143
    H1_ s       0.000599

                        B1  partial gross atomic populations
   ao class       1B1        2B1 
    C1_ p       0.993704   0.006771

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    C1_ p       1.018167   0.014318   0.002706   0.000304
    H1_ s       0.944746   0.013746   0.002462   0.000375


                        gross atomic populations
     ao           C1_        H1_
      s         3.542023   1.605743
      p         2.852233   0.000000
    total       6.394257   1.605743


 Total number of electrons:    8.00000000

