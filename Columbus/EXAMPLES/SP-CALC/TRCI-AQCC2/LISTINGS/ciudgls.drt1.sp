1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 358
global arrays:        11814   (    0.09 MB)
vdisk:                    0   (    0.00 MB)
drt:                 278425   (    1.06 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 3,
   gset =3,
   RTOLCI=0.0000010000,0.000001,0.00001,0.00001
   VOUT  = 1,
   froot = 2,
   NBKITR= 0,
   NITER= 80,
   ivmode=3,
   NVCIMX=16,
 /&end
 ------------------------------------------------------------------------
 froot operation modus: follow reference vector.
 bummer (warning):2:changed keyword: nbkitr=         0
 bummer (warning):changed keyword: davcor=         0

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    2
 lvlprt =    0      nroot  =    2      noldv  =    0      noldhv =    0
 nunitv =    2      ntype  =    3      nbkitr =    0      niter  =   80
 ivmode =    3      vout   =    2      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    2      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =   2      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-06
    2        1.000E-04    1.000E-06
 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264083034

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:16 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:16 2003

 core energy values from the integral file:
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -2.792842564975E+01

 drt header information:
 /                                                                               
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       56        5       16       15       20
 ncsft:             358
 total number of valid internal walks:      56
 nvalz,nvaly,nvalx,nvalw =        5      16      15      20

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext    72    64    20
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      330      432      360      156       27        0        0        0
 minbl4,minbl3,maxbl2    48    48    51
 maxbuf 32767
 number of external orbitals per symmetry block:   4   1   3   0
 nmsym   4 number of internal orbitals   4

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:16 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:16 2003
 /                                                                               
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     7    20    15    20 total internal walks:      62
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 3

 setref:        5 references kept,
                0 references were marked as invalid, out of
                5 total.
 limcnvrt: found 5 valid internal walksout of  7
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 1
 limcnvrt: found 16 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  16 segmentation marks segtype= 2
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 3
 limcnvrt: found 20 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  20 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x       9       4      12       3
 vertex w      17       4      12       3



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           5|         5|         0|         5|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          16|        44|         5|        16|         5|         2|
 -------------------------------------------------------------------------------
  X 3          15|        97|        49|        15|        21|         3|
 -------------------------------------------------------------------------------
  W 4          20|       212|       146|        20|        36|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>       358


 297 dimension of the ci-matrix ->>>       358


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       5         97          5      15       5
     2  4   1    25      two-ext wz   2X  4 1      20       5        212          5      20       5
     3  4   3    26      two-ext wx   2X  4 3      20      15        212         97      20      15
     4  2   1    11      one-ext yz   1X  2 1      16       5         44          5      16       5
     5  3   2    15      1ex3ex  yx   3X  3 2      15      16         97         44      15      16
     6  4   2    16      1ex3ex  yw   3X  4 2      20      16        212         44      20      16
     7  1   1     1      allint zz    OX  1 1       5       5          5          5       5       5
     8  2   2     5      0ex2ex yy    OX  2 2      16      16         44         44      16      16
     9  3   3     6      0ex2ex xx    OX  3 3      15      15         97         97      15      15
    10  4   4     7      0ex2ex ww    OX  4 4      20      20        212        212      20      20
    11  1   1    75      dg-024ext z  DG  1 1       5       5          5          5       5       5
    12  2   2    45      4exdg024 y   DG  2 2      16      16         44         44      16      16
    13  3   3    46      4exdg024 x   DG  3 3      15      15         97         97      15      15
    14  4   4    47      4exdg024 w   DG  4 4      20      20        212        212      20      20
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 358 156 51
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        34 2x:         0 4x:         0
All internal counts: zz :        14 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        13    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        29    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       5

    root           eigenvalues
    ----           ------------
       1         -38.6450051117
       2         -38.4074031068

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  2 from unit 11 written to unit 49 filename cirefv                                                      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.6

    ------------------------------------------------------------


    reference energies:

  reference state used is:   2with energy      -38.4074031068

  ### active excitation selection ###

 Inactive orbitals:
    there are    5 all-active excitations of which    5 are references.

    the    5 reference all-active excitation csfs

    --------------------
       1   2   3   4   5
    --------------------
  1:   2   2   2   1   0
  2:   2   2   0  -1   2
  3:   2   0   2   2   2
  4:   0   2   2   2   2

  ### end active excitation selection ###


################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999992
 the acpf density matrix will be calculated

 space required:

    space required for calls in multd2:
       onex           40
       allin           0
       diagon         85
    max.      ---------
       maxnex         85

    total core space usage:
       maxnex         85
    max k-seg        212
    max k-ind         20
              ---------
       totmax        549
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       2  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      16 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       2  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      16 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               358
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 one root will be followed:                               2
 number of roots to converge:                             2
 number of iterations:                                   80
 residual norm convergence criteria:               0.000001

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000
 ref:   2     0.00000000     1.00000000
 follow root  2 (overlap=  1.)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -38.6450051117 -7.1054E-15  0.0000E+00  4.2072E-01  1.0000E-06
 mraqcc  #  1  2    -38.4074031068 -1.4211E-14  1.0486E-01  4.3501E-01  1.0000E-06

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.2500s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.99993656     0.01066784    -0.00361652
 ref:   2     0.00977125    -0.98121436    -0.19267305
 follow root  2 (overlap=  0.981214359)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -38.6450536095  4.8498E-05  0.0000E+00  4.2053E-01  1.0000E-06
 mraqcc  #  2  2    -38.4887501007  8.1347E-02  5.5302E-03  1.0221E-01  1.0000E-06
 mraqcc  #  2  3    -36.2970595589  8.3686E+00  0.0000E+00  9.1150E-01  1.0000E-05

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.99957724    -0.00812293    -0.02403196    -0.01420643
 ref:   2    -0.00575833    -0.97615251     0.17009740    -0.13475906
 follow root  2 (overlap=  0.976152506)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -38.6464754617  1.4219E-03  0.0000E+00  4.1678E-01  1.0000E-06
 mraqcc  #  3  2    -38.4954529758  6.7029E-03  9.1036E-04  3.9267E-02  1.0000E-06
 mraqcc  #  3  3    -37.0731782413  7.7612E-01  0.0000E+00  2.3268E-01  1.0000E-05
 mraqcc  #  3  4    -35.9126325986  7.9842E+00  0.0000E+00  5.3039E-01  1.0000E-05

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.99686675     0.01000435    -0.05542229     0.05288744     0.01696711
 ref:   2     0.00583578    -0.97414958    -0.14206864    -0.11667835     0.13115336
 follow root  2 (overlap=  0.974149579)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -38.6549279911  8.4525E-03  0.0000E+00  3.9224E-01  1.0000E-06
 mraqcc  #  4  2    -38.4964228680  9.6989E-04  1.9978E-04  1.8792E-02  1.0000E-06
 mraqcc  #  4  3    -37.5930797908  5.1990E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  4  4    -36.5613629689  6.4873E-01  0.0000E+00  6.2872E-01  1.0000E-05
 mraqcc  #  4  5    -35.9123629574  7.9839E+00  0.0000E+00  4.9388E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.01   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.01   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.99380493    -0.00082259     0.02771897    -0.04594969     0.09625620    -0.01435648
 ref:   2    -0.00019221    -0.97356558    -0.12187559    -0.14787309    -0.02362804     0.12203130
 follow root  2 (overlap=  0.973565576)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -38.6711596831  1.6232E-02  0.0000E+00  3.4357E-01  1.0000E-06
 mraqcc  #  5  2    -38.4966317236  2.0886E-04  4.3270E-05  8.5810E-03  1.0000E-06
 mraqcc  #  5  3    -37.8073152344  2.1424E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  5  4    -36.7692032683  2.0784E-01  0.0000E+00  2.7060E-01  1.0000E-05
 mraqcc  #  5  5    -36.4170811453  5.0472E-01  0.0000E+00  6.5870E-01  1.0000E-04
 mraqcc  #  5  6    -35.8067972709  7.8784E+00  0.0000E+00  5.0198E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.99018993    -0.00366350     0.05756239     0.04355909    -0.11704742     0.00601670     0.02373506
 ref:   2    -0.00090599     0.97322738     0.08887398    -0.13994850    -0.05782131    -0.10494431    -0.10481825
 follow root  2 (overlap=  0.97322738)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -38.6817180320  1.0558E-02  0.0000E+00  2.9492E-01  1.0000E-06
 mraqcc  #  6  2    -38.4966903436  5.8620E-05  1.6726E-05  5.4561E-03  1.0000E-06
 mraqcc  #  6  3    -38.0450226652  2.3771E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  6  4    -37.2541622290  4.8496E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  6  5    -36.4803460892  6.3265E-02  0.0000E+00  4.6919E-01  1.0000E-04
 mraqcc  #  6  6    -36.2647179413  4.5792E-01  0.0000E+00  7.2839E-01  1.0000E-04
 mraqcc  #  6  7    -35.7605540587  7.8321E+00  0.0000E+00  4.6060E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1300s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98968937     0.00118057     0.02668425    -0.02208114     0.01979897    -0.12386768     0.05857633     0.01214648
 ref:   2     0.00179423    -0.97293117    -0.06987647    -0.12320837    -0.14193342    -0.00396823    -0.00945964     0.11440443
 follow root  2 (overlap=  0.972931167)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -38.6911691170  9.4511E-03  0.0000E+00  2.5934E-01  1.0000E-06
 mraqcc  #  7  2    -38.4967153688  2.5025E-05  6.8317E-06  3.5286E-03  1.0000E-06
 mraqcc  #  7  3    -38.2285274436  1.8350E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  7  4    -37.5379677462  2.8381E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  7  5    -36.6661687648  1.8582E-01  0.0000E+00  1.6742E-01  1.0000E-04
 mraqcc  #  7  6    -36.4594273524  1.9471E-01  0.0000E+00  5.3224E-01  1.0000E-04
 mraqcc  #  7  7    -35.8355184566  7.4964E-02  0.0000E+00  5.3706E-01  1.0000E-04
 mraqcc  #  7  8    -35.7230655421  7.7946E+00  0.0000E+00  4.8673E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.01   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98949250     0.00207536    -0.00942428     0.01425154     0.01000977     0.11152128    -0.06466582     0.05818180
 ref:   2    -0.00192523    -0.97282156    -0.05967659    -0.09908462     0.14055696     0.03324016     0.09332025    -0.00737560

                ci   9
 ref:   1    -0.02245860
 ref:   2    -0.10301075
 follow root  2 (overlap=  0.972821559)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -38.6946194702  3.4504E-03  0.0000E+00  2.4219E-01  1.0000E-06
 mraqcc  #  8  2    -38.4967250238  9.6550E-06  2.3946E-06  2.0924E-03  1.0000E-06
 mraqcc  #  8  3    -38.2919599233  6.3432E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  8  4    -37.7818960507  2.4393E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  8  5    -37.0004636569  3.3429E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  6    -36.4679191913  8.4918E-03  0.0000E+00  5.3252E-01  1.0000E-04
 mraqcc  #  8  7    -36.2236847738  3.8817E-01  0.0000E+00  4.2975E-01  1.0000E-04
 mraqcc  #  8  8    -35.8354534937  1.1239E-01  0.0000E+00  5.3787E-01  1.0000E-04
 mraqcc  #  8  9    -35.6852372073  7.7568E+00  0.0000E+00  5.5094E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98956372     0.00169391    -0.01100647     0.00109832    -0.00255521     0.04844273    -0.07566501    -0.11167347
 ref:   2     0.00217665    -0.97275048     0.05468313     0.08280746    -0.11624296    -0.09952164    -0.09460906     0.03145979

                ci   9         ci  10
 ref:   1     0.00253968     0.00908758
 ref:   2     0.06584517     0.07881165
 follow root  2 (overlap=  0.972750484)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -38.6964365894  1.8171E-03  0.0000E+00  2.3652E-01  1.0000E-06
 mraqcc  #  9  2    -38.4967285377  3.5140E-06  5.9721E-07  1.0918E-03  1.0000E-06
 mraqcc  #  9  3    -38.3305658703  3.8606E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  9  4    -37.9066507389  1.2475E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  9  5    -37.3255740810  3.2511E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  6    -36.6867321717  2.1881E-01  0.0000E+00  4.5024E-01  1.0000E-04
 mraqcc  #  9  7    -36.4015746355  1.7789E-01  0.0000E+00  2.9133E-01  1.0000E-04
 mraqcc  #  9  8    -36.0303313956  1.9488E-01  0.0000E+00  5.5786E-01  1.0000E-04
 mraqcc  #  9  9    -35.7347339732  4.9497E-02  0.0000E+00  5.6628E-01  1.0000E-04
 mraqcc  #  9 10    -35.6210570552  7.6926E+00  0.0000E+00  2.3842E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.01   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.01   0.01   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1300s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98962192     0.00174487     0.00773598     0.00316003    -0.00122710    -0.00998757     0.06923528    -0.04573020
 ref:   2     0.00221231    -0.97274018    -0.05219522    -0.07267711    -0.11051469    -0.11340814    -0.04649424    -0.10329501

                ci   9         ci  10         ci  11
 ref:   1    -0.11651484    -0.00269276     0.00256772
 ref:   2     0.01989865     0.04737123    -0.07224184
 follow root  2 (overlap=  0.972740177)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1    -38.6966214865  1.8490E-04  0.0000E+00  2.3465E-01  1.0000E-06
 mraqcc  # 10  2    -38.4967293422  8.0448E-07  9.6712E-08  4.3702E-04  1.0000E-06
 mraqcc  # 10  3    -38.3394542959  8.8884E-03  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 10  4    -37.9860522838  7.9402E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 10  5    -37.4504945816  1.2492E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  6    -36.8119610282  1.2523E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  7    -36.6536866996  2.5211E-01  0.0000E+00  5.9591E-01  1.0000E-04
 mraqcc  # 10  8    -36.1414835703  1.1115E-01  0.0000E+00  1.5358E-01  1.0000E-04
 mraqcc  # 10  9    -36.0287113803  2.9398E-01  0.0000E+00  5.6501E-01  1.0000E-04
 mraqcc  # 10 10    -35.7279648771  1.0691E-01  0.0000E+00  5.2084E-01  1.0000E-04
 mraqcc  # 10 11    -35.5363280798  7.6079E+00  0.0000E+00  8.7414E-02  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.01   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98938815     0.00168659     0.01412384    -0.00618864    -0.00579494    -0.00540764    -0.06754984     0.00476734
 ref:   2    -0.00215435    -0.97273843    -0.05095213    -0.06989009     0.10354307    -0.09993015     0.00341940    -0.11868422

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.12107412    -0.03874687     0.00760240    -0.00196559
 ref:   2     0.01165403    -0.06533464    -0.04279924     0.07248121
 follow root  2 (overlap=  0.972738426)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 11  1    -38.6979665599  1.3451E-03  0.0000E+00  2.2827E-01  1.0000E-06
 mraqcc  # 11  2    -38.4967294534  1.1120E-07  1.3135E-08  1.5901E-04  1.0000E-06
 mraqcc  # 11  3    -38.3430137569  3.5595E-03  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 11  4    -38.0151446389  2.9092E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 11  5    -37.5011626145  5.0668E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 11  6    -37.0097193399  1.9776E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 11  7    -36.6706721550  1.6985E-02  0.0000E+00  5.3358E-01  1.0000E-04
 mraqcc  # 11  8    -36.4330046792  2.9152E-01  0.0000E+00  2.6502E-01  1.0000E-04
 mraqcc  # 11  9    -36.0293296561  6.1828E-04  0.0000E+00  5.4479E-01  1.0000E-04
 mraqcc  # 11 10    -35.9211648897  1.9320E-01  0.0000E+00  4.1746E-01  1.0000E-04
 mraqcc  # 11 11    -35.7237531770  1.8743E-01  0.0000E+00  5.1650E-01  1.0000E-04
 mraqcc  # 11 12    -35.5361754777  7.6077E+00  0.0000E+00  3.9357E-02  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98946411    -0.00169764     0.01357274    -0.00528600    -0.00435096     0.00672551    -0.06053709     0.02535142
 ref:   2    -0.00220783     0.97273847    -0.05090587    -0.06944094     0.08301992     0.10826961     0.04179714     0.06379018

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.00585627     0.11005910    -0.06376426    -0.01222359     0.00339660
 ref:   2     0.10640043    -0.03495955    -0.04435565    -0.03856383     0.07702546
 follow root  2 (overlap=  0.972738471)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 12  1    -38.6983996174  4.3306E-04  0.0000E+00  2.2448E-01  1.0000E-06
 mraqcc  # 12  2    -38.4967294687  1.5327E-08  1.7958E-09  5.7822E-05  1.0000E-06
 mraqcc  # 12  3    -38.3431235849  1.0983E-04  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 12  4    -38.0167149358  1.5703E-03  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 12  5    -37.5672156470  6.6053E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12  6    -37.1859529622  1.7623E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12  7    -36.7002778060  2.9606E-02  0.0000E+00  3.0825E-01  1.0000E-04
 mraqcc  # 12  8    -36.6011998886  1.6820E-01  0.0000E+00  4.6767E-01  1.0000E-04
 mraqcc  # 12  9    -36.3742835351  3.4495E-01  0.0000E+00  6.4244E-01  1.0000E-04
 mraqcc  # 12 10    -36.0101848071  8.9020E-02  0.0000E+00  4.9856E-01  1.0000E-04
 mraqcc  # 12 11    -35.8896924416  1.6594E-01  0.0000E+00  3.9039E-01  1.0000E-04
 mraqcc  # 12 12    -35.6823850745  1.4621E-01  0.0000E+00  4.2320E-01  1.0000E-04
 mraqcc  # 12 13    -35.5286648530  7.6002E+00  0.0000E+00  0.0000E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98917260     0.00169072    -0.01514624     0.01357887     0.00985124    -0.01475547    -0.00312155     0.06428138
 ref:   2     0.00199259    -0.97273888     0.05052296     0.06855187    -0.07643226    -0.07135505     0.12858946    -0.00964409

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.01294299    -0.01613957     0.12467433    -0.01934158     0.01526748    -0.00924227
 ref:   2     0.01552199     0.09788442    -0.00199800    -0.05216763     0.00729051    -0.07975329
 follow root  2 (overlap=  0.972738879)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 13  1    -38.6994200999  1.0205E-03  0.0000E+00  2.2047E-01  1.0000E-06
 mraqcc  # 13  2    -38.4967294708  2.0221E-09  1.9626E-10  1.9606E-05  1.0000E-06
 mraqcc  # 13  3    -38.3433654313  2.4185E-04  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 13  4    -38.0378571313  2.1142E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 13  5    -37.5818202934  1.4605E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13  6    -37.3899544099  2.0400E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13  7    -36.8731064197  1.7283E-01  0.0000E+00  4.1616E-01  1.0000E-04
 mraqcc  # 13  8    -36.6925818982  9.1382E-02  0.0000E+00  3.3054E-01  1.0000E-04
 mraqcc  # 13  9    -36.5650813456  1.9080E-01  0.0000E+00  8.3066E-01  1.0000E-04
 mraqcc  # 13 10    -36.2142288502  2.0404E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13 11    -35.9696786455  7.9986E-02  0.0000E+00  2.3427E-01  1.0000E-04
 mraqcc  # 13 12    -35.8093226646  1.2694E-01  0.0000E+00  3.8551E-01  1.0000E-04
 mraqcc  # 13 13    -35.6169945183  8.8330E-02  0.0000E+00  3.3466E-01  1.0000E-04
 mraqcc  # 13 14    -35.5138590594  7.5854E+00  0.0000E+00  1.8922E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98908004     0.00168978    -0.01542084     0.01522309    -0.01684911    -0.01114012     0.00586127    -0.01427527
 ref:   2     0.00196136    -0.97273892     0.05048055     0.06786544     0.05324033    -0.08677341     0.06171173     0.11641458

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.06207176     0.01452086    -0.08874889     0.08955894     0.01661277    -0.01154840     0.01522695
 ref:   2    -0.00015988     0.08311726     0.04770864     0.02143077     0.05143447     0.02290121     0.07798995
 follow root  2 (overlap=  0.972738916)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 14  1    -38.6995476350  1.2754E-04  0.0000E+00  2.1989E-01  1.0000E-06
 mraqcc  # 14  2    -38.4967294710  2.4955E-10  2.8109E-11  7.1711E-06  1.0000E-06
 mraqcc  # 14  3    -38.3434056147  4.0183E-05  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 14  4    -38.0405623250  2.7052E-03  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 14  5    -37.6610491646  7.9229E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14  6    -37.4546710172  6.4717E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14  7    -37.0860396363  2.1293E-01  0.0000E+00  8.9791E-02  1.0000E-04
 mraqcc  # 14  8    -36.7898512921  9.7269E-02  0.0000E+00  9.3173E-01  1.0000E-04
 mraqcc  # 14  9    -36.6904446384  1.2536E-01  0.0000E+00  3.1537E-01  1.0000E-04
 mraqcc  # 14 10    -36.4391073418  2.2488E-01  0.0000E+00  2.7905E-01  1.0000E-04
 mraqcc  # 14 11    -36.0295481495  5.9870E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14 12    -35.9320004234  1.2268E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14 13    -35.8051522612  1.8816E-01  0.0000E+00  3.9060E-01  1.0000E-04
 mraqcc  # 14 14    -35.5814463346  6.7587E-02  0.0000E+00  1.0864E-01  1.0000E-04
 mraqcc  # 14 15    -35.4621478517  7.5337E+00  0.0000E+00  2.5746E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.01   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98911802    -0.00168994    -0.01535897     0.01467714     0.01267542    -0.00195689     0.01462665     0.00362841
 ref:   2    -0.00196416     0.97273893     0.05040595     0.06411486    -0.04351408     0.07400083     0.09270752     0.05900355

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.05173043    -0.04184745     0.03200874     0.12183212     0.00154044     0.01325094     0.01533414     0.00958128
 ref:   2    -0.07928682    -0.08401814    -0.06879960     0.00172859     0.01554477     0.05071091    -0.01523888     0.08093611
 follow root  2 (overlap=  0.972738933)

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 15  1    -38.6996065835  5.8948E-05  0.0000E+00  2.1917E-01  1.0000E-06
 mraqcc  # 15  2    -38.4967294710  3.3296E-11  3.8382E-12  2.7048E-06  1.0000E-06
 mraqcc  # 15  3    -38.3434376519  3.2037E-05  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 15  4    -38.0596660031  1.9104E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 15  5    -37.7309330533  6.9884E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 15  6    -37.5413336792  8.6663E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 15  7    -37.2157900672  1.2975E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 15  8    -36.8704339172  8.0583E-02  0.0000E+00  1.0030E+00  1.0000E-04
 mraqcc  # 15  9    -36.7307196255  4.0275E-02  0.0000E+00  3.5383E-01  1.0000E-04
 mraqcc  # 15 10    -36.5781941144  1.3909E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 15 11    -36.2130159123  1.8347E-01  0.0000E+00  3.7361E-01  1.0000E-04
 mraqcc  # 15 12    -35.9511283233  1.9128E-02  0.0000E+00  5.6704E-02  1.0000E-04
 mraqcc  # 15 13    -35.8931241038  8.7972E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 15 14    -35.8031663858  2.2172E-01  0.0000E+00  3.6311E-01  1.0000E-04
 mraqcc  # 15 15    -35.5752705351  1.1312E-01  0.0000E+00  1.1839E-01  1.0000E-04
 mraqcc  # 15 16    -35.4296218926  7.5012E+00  0.0000E+00  1.0693E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.98891106     0.00168965     0.02030123
 ref:   2     0.00207397    -0.97273893    -0.00493583
 follow root  2 (overlap=  0.972738927)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 16  1    -38.7005419823  9.3540E-04  0.0000E+00  2.1376E-01  1.0000E-06
 mraqcc  # 16  2    -38.4967294711  4.1620E-12  7.8795E-13  1.1312E-06  1.0000E-06
 mraqcc  # 16  3    -36.8028830372 -1.5406E+00  0.0000E+00  8.7053E-01  1.0000E-05

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.04   0.01   0.00   0.04
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1400s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1500s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.98893681    -0.00168965    -0.01394156     0.01383753
 ref:   2    -0.00214739     0.97273896    -0.02316054    -0.02399401
 follow root  2 (overlap=  0.972738956)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 17  1    -38.7005490996  7.1173E-06  0.0000E+00  2.1373E-01  1.0000E-06
 mraqcc  # 17  2    -38.4967294711  9.9654E-13  1.6868E-13  5.5194E-07  1.0000E-06
 mraqcc  # 17  3    -37.6920915199  8.8921E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 17  4    -36.2794237626 -1.7802E+00  0.0000E+00  9.2576E-01  1.0000E-05


 mraqcc   convergence criteria satisfied after 17 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 17  2    -38.4967294711  9.9654E-13  1.6868E-13  5.5194E-07  1.0000E-06

####################CIUDGINFO####################

   followed vector at position   2 energy=  -38.496729471054

################END OF CIUDGINFO################


 a4den factor =  1.021664247 for root   2
    2 of the   5 expansion vectors are transformed.
    2 of the   4 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 root follwoing: root to follow is on position   2
 maximum overlap with reference  2(overlap=  0.972738956)
 reference energy= -38.4074031
 total aqcc energy= -38.4967295
 diagonal element shift (lrtshift) =  -0.0893263642

information on vector: 2from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -38.4967294711

                                                       internal orbitals

                                          level       1    2    3    4

                                          orbital     2    3    8   10

                                         symmetry     1    1    2    3

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.140399                        +-   +-   +-      
 z*  1  1       2 -0.148385                        +-   +-        +- 
 z*  1  1       3 -0.948928                        +-        +-   +- 
 z*  1  1       4  0.019277                        +     -   +-   +- 
 z*  1  1       5  0.066500                             +-   +-   +- 
 y   1  1       7 -0.012808              1(   3)   +-   +-         - 
 y   1  1       9 -0.010045              3(   3)   +-   +-         - 
 y   1  1      10 -0.052636              1(   1)   +-    -   +-      
 y   1  1      12 -0.017066              3(   1)   +-    -   +-      
 y   1  1      14 -0.048058              1(   1)   +-    -        +- 
 y   1  1      15 -0.012687              2(   1)   +-    -        +- 
 y   1  1      17 -0.023505              4(   1)   +-    -        +- 
 y   1  1      18  0.033066              1(   3)   +-        +-    - 
 y   1  1      21  0.057709              1(   2)   +-         -   +- 
 y   1  1      30 -0.018188              1(   3)    -   +    +-    - 
 y   1  1      33 -0.011389              1(   2)    -   +     -   +- 
 y   1  1      34 -0.028744              1(   1)    -        +-   +- 
 y   1  1      36  0.013539              3(   1)    -        +-   +- 
 y   1  1      38  0.108223              1(   3)   +     -   +-    - 
 y   1  1      39 -0.032908              2(   3)   +     -   +-    - 
 y   1  1      41  0.013769              1(   2)   +     -    -   +- 
 y   1  1      48  0.016396              3(   1)         -   +-   +- 
 x   1  1      66 -0.032425    1(   2)   1(   3)   +-         -    - 
 x   1  1      67 -0.027983    1(   2)   2(   3)   +-         -    - 
 x   1  1     109 -0.015669    1(   1)   1(   3)    -        +-    - 
 x   1  1     113 -0.019308    1(   1)   2(   3)    -        +-    - 
 x   1  1     121 -0.026726    1(   1)   1(   2)    -         -   +- 
 x   1  1     123  0.011454    3(   1)   1(   2)    -         -   +- 
 w   1  1     158  0.011979    1(   3)   1(   3)   +-   +-           
 w   1  1     180  0.022519    1(   1)   1(   1)   +-        +-      
 w   1  1     185  0.011709    3(   1)   3(   1)   +-        +-      
 w   1  1     191  0.071608    1(   3)   1(   3)   +-        +-      
 w   1  1     193  0.024655    2(   3)   2(   3)   +-        +-      
 w   1  1     197  0.035239    1(   2)   1(   3)   +-        +     - 
 w   1  1     198  0.025740    1(   2)   2(   3)   +-        +     - 
 w   1  1     200  0.023475    1(   1)   1(   1)   +-             +- 
 w   1  1     202  0.018502    2(   1)   2(   1)   +-             +- 
 w   1  1     210  0.054909    1(   2)   1(   2)   +-             +- 
 w   1  1     211  0.014061    1(   3)   1(   3)   +-             +- 
 w   1  1     221  0.011563    1(   1)   1(   3)   +    +-         - 
 w   1  1     273  0.055357    1(   1)   1(   3)   +         +-    - 
 w   1  1     274  0.013341    2(   1)   1(   3)   +         +-    - 
 w   1  1     275  0.010331    3(   1)   1(   3)   +         +-    - 
 w   1  1     277  0.014194    1(   1)   2(   3)   +         +-    - 
 w   1  1     279 -0.023400    3(   1)   2(   3)   +         +-    - 
 w   1  1     285  0.039896    1(   1)   1(   2)   +          -   +- 
 w   1  1     286  0.018861    2(   1)   1(   2)   +          -   +- 
 w   1  1     287 -0.013518    3(   1)   1(   2)   +          -   +- 
 w   1  1     300 -0.013956    1(   3)   1(   3)        +-   +-      
 w   1  1     342  0.034815    1(   1)   1(   1)             +-   +- 
 w   1  1     347  0.010617    3(   1)   3(   1)             +-   +- 
 w   1  1     353  0.026319    1(   3)   1(   3)             +-   +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              48
     0.01> rq > 0.001            186
    0.001> rq > 0.0001           102
   0.0001> rq > 0.00001           15
  0.00001> rq > 0.000001           3
 0.000001> rq                      0
           all                   358
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=      18  DYX=      44  DYW=      54
   D0Z=       2  D0Y=      11  D0X=       8  D0W=      10
  DDZI=      16 DDYI=      52 DDXI=      48 DDWI=      56
  DDZE=       0 DDYE=      16 DDXE=      15 DDWE=      20
================================================================================
 root #  2: Scaling(2) with   1.02166425corresponding to ref #  2


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9616703      0.1184204      0.0170981      0.0022942
   0.0011676      0.0007288


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9303775      0.0144542


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9149701      0.0327209      0.0055468      0.0005512


 total number of electrons =    8.0000000000

NO coefficients and occupation numbers are written to nocoef_ci.2


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.994144   1.301990   0.034123   0.003309   0.001325   0.000206
    C1_ p      -0.000029   0.111221   0.057131   0.005519   0.000650   0.000954
    H1_ s       0.005885   0.548459   0.027166   0.008270   0.000319   0.000008

   ao class       7A1 
    C1_ s       0.000076
    C1_ p       0.000028
    H1_ s       0.000625

                        B1  partial gross atomic populations
   ao class       1B1        2B1 
    C1_ p       1.930377   0.014454

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    C1_ p       1.038247   0.015265   0.003808   0.000168
    H1_ s       0.876723   0.017456   0.001739   0.000383


                        gross atomic populations
     ao           C1_        H1_
      s         3.335172   1.487033
      p         3.177794   0.000000
    total       6.512967   1.487033


 Total number of electrons:    8.00000000

