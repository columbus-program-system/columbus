
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "8000000             "

 Work memory size (LMWORK) :     8000000 =   61.04 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 2 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    9



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        1       6      15       9      [6s3p|3s2p]                            
  H  1        2       1       3       2      [3s|2s]                                
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3       8      21      13

  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   C  1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000

   4   H  1 1   x      0.0000000000
   5            y      1.3240154000
   6            z      1.5779001000

   7   H  1 2   x      0.0000000000
   8            y     -1.3240154000
   9            z      1.5779001000



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   3  2  3  1


  Symmetry 1

   1   C  1  z    3
   2   H  1  y    [ 5  -  8 ]/2
   3   H  1  z    [ 6  +  9 ]/2


  Symmetry 2

   4   C  1  x    1
   5   H  1  x    [ 4  +  7 ]/2


  Symmetry 3

   6   C  1  y    2
   7   H  1  y    [ 5  +  8 ]/2
   8   H  1  z    [ 6  -  9 ]/2


  Symmetry 4

   9   H  1  x    [ 4  -  7 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        H  1        H  2

   C  1    0.000000
   H  1    1.090000    0.000000
   H  2    1.090000    1.401277    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       C  1                           1.090000
  bond distance:    H  2       C  1                           1.090000


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       C  1       H  1                  80.000


  Nuclear repulsion energy :    6.203440944703


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1   1s    1      172.256000    0.0618  0.0000  0.0000
   seg. cont.  2       25.910900    0.3588  0.0000  0.0000
               3        5.533350    0.7007  0.0000  0.0000
               4        3.664980    0.0000 -0.3959  0.0000
               5        0.770545    0.0000  1.2158  0.0000
               6        0.195857    0.0000  0.0000  1.0000

  C  1   2px   7        3.664980    0.2365  0.0000
   seg. cont.  8        0.770545    0.8606  0.0000
               9        0.195857    0.0000  1.0000

  C  1   2py  10        3.664980    0.2365  0.0000
   seg. cont. 11        0.770545    0.8606  0.0000
              12        0.195857    0.0000  1.0000

  C  1   2pz  13        3.664980    0.2365  0.0000
   seg. cont. 14        0.770545    0.8606  0.0000
              15        0.195857    0.0000  1.0000

  H  1#1 1s   16        5.447178    0.1563  0.0000
   seg. cont. 17        0.824547    0.9047  0.0000
              18        0.183192    0.0000  1.0000

  H  1#2 1s   19        5.447178    0.1563  0.0000
   seg. cont. 20        0.824547    0.9047  0.0000
              21        0.183192    0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  C  1    1s       1     2     3
   2  C  1    1s       4     5
   3  C  1    1s       6
   4  C  1    2px      7     8
   5  C  1    2py     10    11
   6  C  1    2pz     13    14
   7  C  1    2px      9
   8  C  1    2py     12
   9  C  1    2pz     15
  10  H  1#1  1s    16  17
  11  H  1#2  1s    19  20
  12  H  1#1  1s    18
  13  H  1#2  1s    21




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:         7  2  4  0


  Symmetry  A1 ( 1)

    1     C  1     1s         1
    2     C  1     1s         2
    3     C  1     1s         3
    4     C  1     2pz        6
    5     C  1     2pz        9
    6     H  1     1s        10  +  11
    7     H  1     1s        12  +  13


  Symmetry  B1 ( 2)

    8     C  1     2px        4
    9     C  1     2px        7


  Symmetry  B2 ( 3)

   10     C  1     2py        5
   11     C  1     2py        8
   12     H  1     1s        10  -  11
   13     H  1     1s        12  -  13


  No orbitals in symmetry  A2 ( 4)

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10E-14                                                   
       6.0    1    2    3    2                                                  
C  1   0.000000000000000   0.000000000000000   0.000000000000000       *        
H   3   1                                                                       
        172.25600000         0.06176690                                         
         25.91090000         0.35879400                                         
          5.53335000         0.70071300                                         
H   2   1                                                                       
          3.66498000        -0.39589700                                         
          0.77054500         1.21584000                                         
H   1   1                                                                       
          0.19585700         1.00000000                                         
H   2   1                                                                       
          3.66498000         0.23646000                                         
          0.77054500         0.86061900                                         
H   1   1                                                                       
          0.19585700         1.00000000                                         
       1.0    1    1    2                                                       
H  1   0.000000000000000   1.324015400000000   1.577900100000000       *        
H   2   1                                                                       
          5.44717800         0.15628500                                         
          0.82454700         0.90469100                                         
H   1   1                                                                       
          0.18319200         1.00000000                                         


 herdrv: noofopt= 3


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T 1


    35 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  38.46
 prop, itype T 2


    41 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  45.05
 prop, itype T 3


    35 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  38.46

 >>> Time used in ONEDRV is   0.00 seconds

 inttyp= 1noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+


 >>> Time used in OVERLA is   0.00 seconds

 finopt,noofopt,last1= 0 3 0
 inttyp= 8noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+


 >>> Time used in CARMOM is   0.00 seconds

 finopt,noofopt,last1= 1 3 0
 inttyp= 8noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=  1typeb=  0last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=  1typeb=  1last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=  1typeb=  2last= 2
 lstflg= 1

 >>> Time used in CARMOM is   0.00 seconds

 finopt,noofopt,last1= 2 3 0
 inttyp= 8noptyp= 6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=  1typeb=  3last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=  1typeb=  4last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=  1typeb=  5last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=  1typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=  1typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=  1typeb=  8last= 2
 lstflg= 1

 >>> Time used in CARMOM is   0.01 seconds

 finopt,noofopt,last1= 3 3 2
 inttyp= 18noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: XANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: YANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: ZANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  8last= 2
 lstflg= 2

 >>> Time used in ANGMOM is   0.00 seconds



 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 1270 2 0

 Number of two-electron integrals written:      1270 (30.3%)
 Kilobytes written:                               33



 >>> Time used in TWOINT is   0.01 seconds

 >>>> Total CPU  time used in HERMIT:   0.03 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
