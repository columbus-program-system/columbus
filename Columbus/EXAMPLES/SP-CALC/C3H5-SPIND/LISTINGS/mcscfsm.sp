 total ao core energy =   64.723385571
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:    23
 Spin multiplicity:            2
 Number of active orbitals:    3
 Number of active electrons:   3
 Total number of CSFs:         4

 Number of active-double rotations:         0
 Number of active-active rotations:         0
 Number of double-virtual rotations:      204
 Number of active-virtual rotations:       22
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -116.4834463827  1.165E+02  7.987E-02  3.030E-01  9.374E-03  F   *not conv.*     
    2   -116.4943138333  1.087E-02  1.340E-02  2.705E-02  9.472E-05  F   *not conv.*     
    3   -116.4944098010  9.597E-05  8.446E-04  7.884E-04  1.171E-07  F   *not conv.*     
    4   -116.4944099361  1.351E-07  1.888E-04  9.229E-05  3.415E-09  F   *not conv.*     

 final mcscf convergence values:
    5   -116.4944099402  4.117E-09  4.186E-05  2.021E-05  1.906E-10  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 1.000 total energy=     -116.494409940, rel. (eV)=   0.000000
   ------------------------------------------------------------


